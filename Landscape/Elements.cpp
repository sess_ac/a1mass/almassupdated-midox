/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#define _CRT_SECURE_NO_DEPRECATE



#define __WEED_CURVE 99 //99  // 99 is the weed curve

// The default no better information values (Mean of the four crop values)
#define EL_BUG_PERCENT_A 0.0
#define EL_BUG_PERCENT_B 0.2975
#define EL_BUG_PERCENT_C 0.095916647275
#define EL_BUG_PERCENT_D 0

// SBarley
#define EL_BUG_PERCENT_SB_A 0
#define EL_BUG_PERCENT_SB_B 0.380763296
#define EL_BUG_PERCENT_SB_C 0
#define EL_BUG_PERCENT_D 0

// WWheat
#define EL_BUG_PERCENT_WW_A 0.0
#define EL_BUG_PERCENT_WW_B 0.1283
#define EL_BUG_PERCENT_WW_C 0.0
#define EL_BUG_PERCENT_D 0

// WRye
#define EL_BUG_PERCENT_WRy_A 0.0
#define EL_BUG_PERCENT_WRy_B 0.395651915
#define EL_BUG_PERCENT_WRy_C 0.0
#define EL_BUG_PERCENT_D 0

//WRape
#define EL_BUG_PERCENT_WR_A 0.0
#define EL_BUG_PERCENT_WR_B 0.028271643
#define EL_BUG_PERCENT_WR_C 0.0
#define EL_BUG_PERCENT_D 0

//Cropped/Grazed Grass
#define EL_BUG_PERCENT_G_A 4.123817127
#define EL_BUG_PERCENT_G_B 0.151015629
#define EL_BUG_PERCENT_G_C -0.228228353
#define EL_BUG_PERCENT_D 0

//Edges
#define EL_BUG_PERCENT_Edges_A 10.72459109
#define EL_BUG_PERCENT_Edges_B 0.4
#define EL_BUG_PERCENT_Edges_C 2.529631141
#define EL_BUG_PERCENT_D 0

#include <math.h>
#include "../Landscape/ls.h"
#include "Elements.h"


using namespace std;

extern TTypesOfPopulation g_Species;

extern class PollenNectarDevelopmentData * g_nectarpollen;

//extern void FloatToDouble(double &, float);
extern CfgInt cfg_pest_productapplic_startdate;
extern CfgInt cfg_pest_productapplic_period;



/** \brief Flag to determine whether nectar and pollen models are used - should be set to true for pollinator models! */
CfgBool cfg_pollen_nectar_on("ELE_POLLENNECTAR_ON", CFG_CUSTOM, false);
/** \brief Flag to determine whether to calculate pond pesticide concentration */
CfgBool cfg_calc_pond_pesticide("POND_PEST_CALC_ON", CFG_CUSTOM, false);
/** \brief The multiplication factor assumed to account for ingress of pesticide from run-off and soil water to a pond*/
CfgFloat cfg_pondpesticiderunoff("POND_PEST_RUNOFFFACTOR", CFG_CUSTOM, 10.0);
/** \brief Controls whether random pond quality is used */
CfgBool cfg_randompondquality("POND_RANDOMQUALITY", CFG_CUSTOM, false);
/** \brief The number of days a goose count can be used */
CfgInt cfg_goosecountperiod("GOOSE_GOOSECOUNTPERIOD",CFG_CUSTOM,1);
/** \brief Scales the growth of vegetation - max value */
CfgFloat cfg_PermanentVegGrowthMaxScaler("VEG_GROWTHSCALERMAX", CFG_CUSTOM, 1.0);
/** \brief Scales the growth of vegetation - min value */
CfgFloat cfg_PermanentVegGrowthMinScaler("VEG_GROWTHSCALERMIN", CFG_CUSTOM, 1.0);
/** \brief Scales the growth of vegetation - max value */
CfgFloat cfg_PermanentVegGrowthMaxScalerField("VEG_FIELDGROWTHSCALERMAX", CFG_CUSTOM, 1.100);
/** \brief Scales the growth of vegetation - min value */
CfgFloat cfg_PermanentVegGrowthMinScalerField("VEG_FIELDGROWTHSCALERMIN", CFG_CUSTOM, 0.9);
/** \brief A scaling value used to change the insext density crop relationship which was based on DK 2000 data */
CfgFloat cfg_insectbiomassscaling("ELE_INSECTBIOMASSSCALER", CFG_CUSTOM, 1.0);
/** \brief Starting month for varying base development temperature for flower resource model. */
CfgInt cfg_month_varying_flower_base_temp("MONTH_VARING_FLOWER_BASE_TEMP", CFG_CUSTOM, 10);
/** \brief Flower resource base temperature increment per month from the starting month. */
CfgFloat cfg_base_temp_increment_flower("BASE_TEMP_INCREMENT_FLOWER", CFG_CUSTOM, 5);
/** \brief Weight to shorten flowering period and decreasing resource amount when it is cold. */
CfgFloat cfg_weight_cold_flower_resource("WEIGHT_CODE_FLOWER_RESOURCE", CFG_CUSTOM, 0.8);
/** \brief Growth stage for non-crop habitat, this is used for aphid*/
CfgFloat cfg_noncrop_growth_stage_with_green_biomass("NONCROP_GROWTH_STAGE_WITH_GREEN_BIOMASS", CFG_CUSTOM, 50);
/** \brief The extiction coefficient that is used in Beer's law. */
CfgFloat cfg_beer_law_extinction_coef("BEER_LAW_EXTINCTION_COEF", CFG_CUSTOM, 0.6);
/** \brief Monthly base development temperature for pollen.*/
CfgArray_Double cfg_PollenMonthBaseTemp("POLLEN_MONTH_BASE_TEMP", CFG_CUSTOM, 12, vector<double> {3.9, 3.9, 4, 3.5, 8, 9, 9, 6, 5, 0, 2, 2});
/** \brief Monthly base development temperature for NECTAR.*/
CfgArray_Double cfg_NectarMonthBaseTemp("NECTAR_MONTH_BASE_TEMP", CFG_CUSTOM, 12, vector<double> {3.6, 3.6, 3.9, 3.9, 9, 9, 9, 6, 5, 0, 2, 2});

const double c_SolarConversion[ 2 ] [ 81 ] = {
   {
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.28,
     0.56,0.84,1.12,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,
     1.4,1.4,1.26,1.12,0.98,0.84,0.7,0.56,0.42,0.28,0.14,0,0,0,0,0,0,0,0,0,0,0,
     0,0,0,0,0
   },
   {
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
     0,0.242857,0.485714,0.728571,0.971429,1.214286,1.457143,1.7,1.7,1.7,1.7,
     1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,
     1.53,1.36,1.19,1.02,0.85,0.68,0.51,0.34,0.17,0,0,0,0,0,0
   }
  };

extern CfgFloat l_pest_insecticide_amount;
static CfgFloat l_pest_productOrchard_amount( "PEST_PRODUCTORCHARD_AMOUNT", CFG_CUSTOM, 0.0 );
extern CfgInt cfg_pest_productapplic_startdate;
extern CfgInt cfg_pest_productapplic_startdate2;
extern CfgInt cfg_pest_productapplic_startdate3;
extern CfgInt cfg_pest_productapplic_period;
//extern CfgFloat cfg_goose_GrainDecayRateWinter;
//extern CfgFloat cfg_goose_GrainDecayRateSpring;
//extern CfgFloat cfg_goose_grass_to_winter_cereal_scaler;
/** \brief The decay rate for spilled grain for Harvest to Spring*/
CfgFloat cfg_goose_GrainDecayRateWinter{"GOOSE_GRAINDECAYRATEWINTER", CFG_CUSTOM, 0.958, 0.0, 1.0};// Halflife of 463 days
/** \brief The decay rate for spilled maize for Harvest to Spring*/
CfgFloat cfg_goose_MaizeDecayRateWinter{"GOOSE_MAIZEDECAYRATEWINTER", CFG_CUSTOM, 0.9, 0.0, 1.0};//
/** \brief The decay rate for spilled grain for  Spring*/
CfgFloat cfg_goose_MaizeDecayRateSpring{"GOOSE_MAIZEDECAYRATESPRING", CFG_CUSTOM, 0.9, 0.0, 1.0};//
/** \brief The decay rate for spilled grain for Spring until 1st July */
CfgFloat cfg_goose_GrainDecayRateSpring{"GOOSE_GRAINDECAYRATESPRING", CFG_CUSTOM, 0.95, 0.0, 1.0}; // Halflife of approx 17 days
/** \brief Use the same grain and maize decay rate (the winter one also for spring) */
CfgBool cfg_goose_UniformDecayRate{"GOOSE_UNIFORMDECAYRATE", CFG_CUSTOM, true};
/** \brief The scaler to go from energy intake from grass forage to winter cereal
* The default value of 1.0325 is a quick fix to account for higher energy intake on winter cereal - Based on Therkildsen & Madsen 2000 Energetics of feeding...*/
CfgFloat cfg_goose_grass_to_winter_cereal_scaler{"GOOSE_GRASS_TO_WINTER_CEREAL_SCALER", CFG_CUSTOM, 1.0325, 0.0,
                                                 10.0};


static CfgFloat cfg_beetlebankinsectscaler("ELE_BBINSECTSCALER",CFG_CUSTOM,1.0); // 1.0 means beetlebank is the same as hedgebank
static double g_biomass_scale[ tov_Undefined ];
static double g_weed_percent[ tov_Undefined ];
static double g_bug_percent_a[ tov_Undefined ];
static double g_bug_percent_b[ tov_Undefined ];
static double g_bug_percent_c[ tov_Undefined ];
static double g_bug_percent_d[ tov_Undefined ];
static CfgInt cfg_OrchardSprayDay( "TOX_ORCHARDSPRAYDAY", CFG_CUSTOM, 150 );
static CfgInt cfg_OrchardSprayDay2( "TOX_ORCHARDSPRAYDAYTWO", CFG_CUSTOM, 200000 );
CfgInt cfg_OrchardNoCutsDay( "TOX_ORCHARDNOCUTS", CFG_CUSTOM, -1 );
static CfgInt cfg_MownGrassNoCutsDay( "ELE_MOWNGRASSNOCUTS", CFG_CUSTOM, -1 );
static CfgInt cfg_UMPatchyChance( "UMPATCHYCHANCE", CFG_CUSTOM, 0 );
/** \brief The chance that a beetlebank being created is patchy or not */
static CfgFloat cfg_BBPatchyChance( "BEETLEBANKBPATCHYCHANCE", CFG_CUSTOM, 0.5 );
/** \brief The chance that a beetlebank being created is patchy or not */
static CfgFloat cfg_MGPatchyChance( "MOWNGRASSPATCHYCHANCE", CFG_CUSTOM, 0.5 );
/** \brief The chance that a setaside being created is patchy or not */
static CfgFloat cfg_SetAsidePatchyChance("SETASIDEPATCHYCHANCE", CFG_CUSTOM, 1.0);
static CfgFloat cfg_ele_weedscaling( "ELE_WEEDSCALING", CFG_CUSTOM, 1.0 );
/** \brief A constant relating the proportion of food units per m2. The value is calibrated to estimates of newt density. */
CfgFloat cfg_PondLarvalFoodBiomassConst("POND_LARVALFOODBIOMASSCONST", CFG_CUSTOM, 215.0);
/** \brief The instanteous rate of growth for larval food (r from logistic equation) */
CfgFloat cfg_PondLarvalFoodR("POND_LARVALFOODFOODR", CFG_CUSTOM, 0.15);
// Docs in Elements.h
CfgInt g_el_tramline_decaytime_days( "ELEM_TRAMLINE_DECAYTIME_DAYS", CFG_PRIVATE, 21 );
CfgInt g_el_herbicide_delaytime_days( "ELEM_HERBICIDE_DELAYTIME_DAYS", CFG_PRIVATE, 35 );
CfgInt g_el_strigling_delaytime_days( "ELEM_STRIGLING_DELAYTIME_DAYS", CFG_PRIVATE, 28 );

// Daydegree sum set on an element by ReduceVeg().
#define EL_GROWTH_DAYDEG_MAGIC l_el_growth_daydeg_magic.value()
static CfgInt l_el_growth_daydeg_magic( "ELEM_GROWTH_DAYDEG_MAGIC", CFG_PRIVATE, 100 );

// Date after which ReduceVeg() automatically sets the growth phase
// to harvest1. Cannot become a global configuration variable as it
// is calculated at runtime.
#define EL_GROWTH_DATE_MAGIC   (g_date->DayInYear(1,9))

// If the fraction used in the call to ReduceVeg() is *below* this
// value, then a phase transition to harvest1 is considered if
// there has been no previous 'forced' phase transition before this
// year.
#define EL_GROWTH_PHASE_SHIFT_LEVEL (l_el_growth_phase_shift_level.value())
static CfgFloat l_el_growth_phase_shift_level( "ELEM_GROWTH_PHASE_SHIFT_LEVEL", CFG_PRIVATE, 0.5 );


// Types of landscape elements. Default is 'unknown'.
// The conversion arrays are at the top in 'elements.cpp'!
// Change when adding or deleting element types.
// Outdated, not used anywhere in the landscape. *FN*
//#define EL_MAX_ELEM_TYPES 18

// Constant of proportionality between leaf area total and plant
// biomass.
#define EL_PLANT_BIOMASS  (l_el_plant_biomass_proport.value())  // Scaled to dry matter on Spring Barley for 2001 & 2002
// All other values are scaled relative to this as of 29/03/05
static CfgFloat l_el_plant_biomass_proport( "ELEM_PLANT_BIOMASS_PROPORT", CFG_PRIVATE, 41.45 );

// Default starting LAI Total. (NB LAIGreen=LAITotal/4)
#define EL_VEG_START_LAIT (l_el_veg_start_lait.value())
static CfgFloat l_el_veg_start_lait( "ELEM_VEG_START_LAIT", CFG_PRIVATE, 1.08 );

// Constant * biomass to get height
#define EL_VEG_HEIGHTSCALE  (l_el_veg_heightscale.value())
static CfgInt l_el_veg_heightscale( "ELEM_VEG_HEIGHTSCALE", CFG_PRIVATE, 16 );
//May+21
#define RV_CUT_MAY (l_el_rv_cut_may.value())
static CfgInt l_el_rv_cut_may( "ELEM_RV_CUT_MAY", CFG_PRIVATE, 142 );

#define RV_CUT_JUN (l_el_rv_cut_jun.value())
static CfgInt l_el_rv_cut_jun( "ELEM_RV_CUT_JUN", CFG_PRIVATE, 28 );

#define RV_CUT_JUL (l_el_rv_cut_jul.value())
static CfgInt l_el_rv_cut_jul( "ELEM_RV_CUT_JUL", CFG_PRIVATE, 35 );

#define RV_CUT_AUG (l_el_rv_cut_aug.value())
static CfgInt l_el_rv_cut_aug( "ELEM_RV_CUT_AUG", CFG_PRIVATE, 42 );

#define RV_CUT_SEP (l_el_rv_cut_sep.value())
static CfgInt l_el_rv_cut_sep( "ELEM_RV_CUT_SEP", CFG_PRIVATE, 49 );

#define RV_CUT_OCT (l_el_rv_cut_oct.value())
static CfgInt l_el_rv_cut_oct( "ELEM_RV_CUT_OCT", CFG_PRIVATE, 49 );

#define RV_MAY_1ST (l_el_rv_may_1st.value())
static CfgInt l_el_rv_may_1st( "ELEM_RV_MAY_1ST", CFG_PRIVATE, 121 );

#define RV_CUT_HEIGHT (l_el_rv_cut_height.value())
static CfgFloat l_el_rv_cut_height( "ELEM_RV_CUT_HEIGHT", CFG_PRIVATE, 10.0 );
#define RV_CUT_GREEN (l_el_rv_cut_green.value())
static CfgFloat l_el_rv_cut_green( "ELEM_RV_CUT_GREEN", CFG_PRIVATE, 1.5 );
#define RV_CUT_TOTAL (l_el_rv_cut_total.value())
static CfgFloat l_el_rv_cut_total( "ELEM_RV_CUT_TOTAL", CFG_PRIVATE, 2.0 );

CfgFloat l_el_o_cut_height( "ELEM_RV_CUT_HEIGHT", CFG_PRIVATE, 10.0 );
CfgFloat l_el_o_cut_green( "ELEM_RV_CUT_GREEN", CFG_PRIVATE, 1.5 );
CfgFloat l_el_o_cut_total( "ELEM_RV_CUT_TOTAL", CFG_PRIVATE, 2.0 );

// Default fraction between crop and weed biomasses.
#define EL_WEED_PERCENT (l_el_weed_percent.value())
static CfgFloat l_el_weed_percent( "ELEM_WEED_PERCENT", CFG_PRIVATE, 0.1 );

// Weed biomass regrowth slope after herbacide application.
#define EL_WEED_SLOPE (l_el_weed_slope.value())
static CfgFloat l_el_weed_slope( "ELEM_WEED_SLOPE", CFG_PRIVATE, 0.15 );

// Bug biomass regrowth slope after insecticide application.
#define EL_BUG_SLOPE (l_el_bug_slope.value())
static CfgFloat l_el_bug_slope( "ELEM_BUG_SLOPE", CFG_PRIVATE, 0.2 );

// Fraction of the weed biomass below which we are in pesticide
// regrowth phase, above we are in proportionality mode.
// CANNOT be 1.00!
#define EL_WEED_GLUE (l_el_weed_glue.value())
static CfgFloat l_el_weed_glue( "ELEM_WEED_GLUE", CFG_PRIVATE, 0.99 );

// Same as for weed, but bugs this time.
#define EL_BUG_GLUE (l_el_bug_glue.value())
static CfgFloat l_el_bug_glue( "ELEM_BUG_GLUE", CFG_PRIVATE, 0.50 );

/** \brief Used for birds that feed on grain on cereal fields 3% spill is expected
*
* Yield	%	kg/Ha spill	kJ/kg	kj/m
* 0.85	0.01	8.5	13680	11.628
* 0.85	0.02	17	13680	23.256
* 0.85	0.03	25.5	13680	34.884
* 0.85	0.04	34	13680	46.512
* 0.85	0.05	42.5	13680	58.14
* 0.85	0.06	51	13680	69.768
*/


// This is inversed prior to use. A multiplication is very much less
// expensive compared to a division.
//
// The original array supplied is:
// {1.11,1.06,1.01,0.99,0.96,0.92,0.92,0.93,0.97,0.99,1.02,1.06}
double LE::m_monthly_traffic[ 12 ] =
  {0.9009, 0.9434, 0.9901, 1.0101, 1.0417, 1.0870,
 1.0870, 1.0753, 1.0753, 1.0101, 0.9804,  0.9434};

double LE::m_largeroad_load[ 24 ] =
  {15,9,4,5,14,54,332,381,252,206,204,215,
 231,256,335,470,384,270,191,130,91,100,99,60};

double LE::m_smallroad_load[ 24 ] =
  {4,3,1,1,4,15,94,108,71,58,58,61,
 65,73,95,133,109,76,54,37,26,28,28,17};

class LE_TypeClass * g_letype;

LE::LE(Landscape *L ) {

	m_Landscape = L;
	/**
	* The major job of this constructor is simply to provide default values for all members
	*/
	m_signal_mask = 0;
	m_lasttreat.resize(1);
	m_lasttreat[0] = sleep_all_day;
	m_lastindex = 0;
	m_running = 0;
	m_poison = false;
	m_owner_file = -1;
	m_owner_index = -1;
	
	// ATTRIBUTES
	m_att_high = false;
	m_att_water = false;
	m_att_forest = false;
	m_att_woody = false;
	m_att_urbannoveg = false;
	m_att_userdefinedbool = false;
	m_att_userdefinedint = 0;

	m_totalPollen = 0.0;
	m_totalNectar = 0.0;
	
	m_cattle_grazing = 0;
	m_default_grazing_level = 0; // this means any grazed elements must set this in their constructor.
	m_pig_grazing = false;
	m_yddegs = 0.0;
	m_vegddegs = -1.0;
	m_olddays = 0;
	m_days_since_insecticide_spray = 0;
	m_tramlinesdecay = 0;
	m_mowndecay = 0;
	m_herbicidedelay = 0;
	m_border = NULL;
	m_unsprayedmarginpolyref = -1;
	m_valid_x = -1;
	m_valid_y = -1;
	m_is_in_map = false;
	m_squares_in_map = 0;
	m_management_loop_detect_date = 0;
	m_management_loop_detect_count = 0;
	m_repeat_start = false;
	m_skylarkscrapes = false;
	m_type = tole_Foobar;
	SetALMaSSEleType(-1);
	m_ddegs = 0.0;
	m_maxx = -1; // a very small number
	m_maxy = -1;
	m_minx = 9999999; // a very big number
	m_miny = 9999999;
	m_countrydesignation = -1; // default not set
	m_soiltype = -1;
	m_area = 0;
	m_centroidx = -1;
	m_centroidy = -1;
	m_vege_danger_store = -1;
	m_PesticideGridCell = -1;
	m_subtype = -1;
	m_owner = NULL;
	m_rot_index = -1;
	m_poly = -1;
	m_map_index = -1;
	m_almass_le_type = -1;
	m_farmfunc_tried_to_do = -1;
	SetStubble(false);
	m_birdseedforage = -1;
	m_birdmaizeforage = -1;
	m_openness = -1;
	m_vegage = -1;
	for (int i = 0; i<10; i++) SetMConstants(i, 1);
	for (int i = 0; i < 366; i++)
	{
		m_gooseNos[i] = 0;
		m_gooseNosTimed[i] = 0;
		for (int l = 0; l < gs_foobar; l++)
		{
			m_gooseSpNos[i][l] = 0;
			m_gooseSpNosTimed[i][l] = 0;
		}
	}
	for (int l = 0; l < gs_foobar; l++)
	{
		m_goosegrazingforage[l] = 0;
	}
	for (int i = 0; i < 25; i++)
	{
		MDates[0][i] = -1;
		MDates[1][i] = -1;
	}
	SetLastSownVeg(tov_Undefined);
	ClearManagementActionSum();
#ifdef FMDEBUG
	m_pindex = 0;
	for ( int i = 0; i < 256; i++ ) {
		m_pdates[ i ] = 0;
		m_ptrace[ i ] = 0;
	}
#endif
}

void LE::DoCopy(const LE* a_LE) {

	/**
	* The major job of this method is simply to copy values for all members from one LE to another
	*/
	m_signal_mask = a_LE->m_signal_mask;
	m_lasttreat = a_LE->m_lasttreat;
	m_lastindex = a_LE->m_lastindex;
	m_running = a_LE->m_running;
	m_poison = a_LE->m_poison;
	m_owner_file = a_LE->m_owner_file;
	m_owner_index = a_LE->m_owner_index;
	
	/* Copy Attributes */
	m_att_high = a_LE->m_att_high;
	m_att_water = a_LE->m_att_water;
	m_att_forest = a_LE->m_att_forest;
	m_att_woody = a_LE->m_att_woody;
	m_att_urbannoveg = a_LE->m_att_urbannoveg;
	m_att_userdefinedbool = a_LE->m_att_userdefinedbool;
	m_att_userdefinedint = a_LE->m_att_userdefinedint;

	m_cattle_grazing = a_LE->m_cattle_grazing;
	m_default_grazing_level = a_LE->m_default_grazing_level; // this means any grazed elements must set this in their constructor.
	m_pig_grazing = a_LE->m_pig_grazing;
	m_yddegs = a_LE->m_yddegs;
	m_olddays = a_LE->m_olddays;
	m_vegddegs = a_LE->m_vegddegs;
	m_days_since_insecticide_spray = a_LE->m_days_since_insecticide_spray;
	m_tramlinesdecay = a_LE->m_tramlinesdecay;
	m_mowndecay = a_LE->m_mowndecay;
	m_herbicidedelay = a_LE->m_herbicidedelay;
	m_border = a_LE->m_border;
	m_unsprayedmarginpolyref = a_LE->m_unsprayedmarginpolyref;
	m_valid_x = a_LE->m_valid_x;
	m_valid_y = a_LE->m_valid_y;
	m_is_in_map = a_LE->m_is_in_map;
	m_squares_in_map = a_LE->m_squares_in_map;
	m_management_loop_detect_date = a_LE->m_management_loop_detect_date;
	m_management_loop_detect_count = a_LE->m_management_loop_detect_count;
	m_repeat_start = a_LE->m_repeat_start;
	m_skylarkscrapes = a_LE->m_skylarkscrapes;
	m_type = a_LE->m_type;
	m_owner_tole = a_LE->m_owner_tole;
	m_birdseedforage = a_LE->m_birdseedforage;
	m_birdmaizeforage = a_LE->m_birdmaizeforage; 
	m_ddegs = a_LE->m_ddegs;
	m_maxx = a_LE->m_maxx; 
	m_maxy = a_LE->m_maxy;
	m_minx = a_LE->m_minx; 
	m_miny = a_LE->m_miny;
	m_countrydesignation = a_LE->m_countrydesignation; 
	m_soiltype = a_LE->m_soiltype;
	m_area = a_LE->m_area;
	m_centroidx = a_LE->m_centroidx;
	m_centroidy = a_LE->m_centroidy;
	m_vege_danger_store = a_LE->m_vege_danger_store;
	m_PesticideGridCell = a_LE->m_PesticideGridCell;
	m_subtype = a_LE->m_subtype;
	m_owner = a_LE->m_owner;
	m_rot_index = a_LE->m_rot_index;
	m_poly = a_LE->m_poly;
	m_map_index = a_LE->m_map_index;
	m_almass_le_type = a_LE->m_almass_le_type;
	m_farmfunc_tried_to_do = a_LE->m_farmfunc_tried_to_do;
	m_openness = a_LE->m_openness;
	m_vegage = a_LE->m_vegage;
	m_elevation = a_LE->m_elevation;
	m_aspect = a_LE->m_aspect;
	m_slope = a_LE->m_slope;
	m_OurPollenNectarCurveSet = a_LE->m_OurPollenNectarCurveSet;

	for (int i = 0; i < 366; i++)
	{
		m_gooseNos[i] = a_LE->m_gooseNos[i];
		m_gooseNosTimed[i] = a_LE->m_gooseNosTimed[i];
		for (int l = 0; l < gs_foobar; l++)
		{
			m_gooseSpNos[i][l] = a_LE->m_gooseSpNos[i][l];
			m_gooseSpNosTimed[i][l] = a_LE->m_gooseSpNosTimed[i][l];
		}
	}
	for (int l = 0; l < gs_foobar; l++)
	{
		m_goosegrazingforage[l] = a_LE->m_goosegrazingforage[l];
	}
	for (int i = 0; i < 25; i++)
	{
		MDates[0][i] = a_LE->MDates[0][i];
		MDates[1][i] = a_LE->MDates[1][i];
	}
	for (int i = 0; i<10; i++) SetMConstants(i, a_LE->MConsts[i]);
}


LE::~LE( void ) {
}


#ifdef FMDEBUG
void LE::Trace( int a_value ) {
  m_farmfunc_tried_to_do = a_value;
#ifdef __RECORDFARMEVENTS
  m_Landscape->RecordEvent( m_owner_index, m_poly, ((VegElement*)this)->GetVegType(), a_value, g_date->DayInYear(), g_date->GetYearNumber());
#endif
  m_pdates[ m_pindex ] = g_date->DayInYear();
  m_ptrace[ m_pindex++ ] = a_value;
  m_pindex &= 0xff; // Circular buffer if need be.
}

void LE::ResetTrace( void ) {
  m_pindex = 0;
  for ( int i = 0; i < 256; i++ ) {
    m_pdates[ i ] = 0;
    m_ptrace[ i ] = 0;
  }
}

#else
// Compiles into nothing if FMDEBUG is #undef.
void LE::Trace( int a_value ) {
  m_farmfunc_tried_to_do = a_value;
}

void LE::ResetTrace( void ) {
}

#endif

void LE::SetCopyTreatment( int a_treatment ) {
  SetLastTreatment( a_treatment );
}

void LE::SetLastTreatment( int a_treatment ) {
  unsigned sz = (int) m_lasttreat.size();
  if ( m_lastindex == sz )
    m_lasttreat.resize( m_lastindex + 1 );

  m_lasttreat[ m_lastindex++ ] = a_treatment;

  // Count this treatment in the grand scope of things.
  m_Landscape->IncTreatCounter( a_treatment );
  // If we have a field margin then we need to tell it about this
  // but not if it is an insecticide spray etc..
  /* if (m_unsprayedmarginpolyref!=-1) { switch (a_treatment) { case  herbicide_treat: case  growth_regulator:
  case  fungicide_treat: case  insecticide_treat: case trial_insecticidetreat: case syninsecticide_treat: case  molluscicide:
  break; // Do not add sprayings default: LE* le=g_landscape_p->SupplyLEPointer(m_unsprayedmarginpolyref);
  le->SetCopyTreatment(a_treatment); // Now we also need to do something with the treatment

  break; }

  } */
}

int LE::GetLastTreatment() {
	/**
	* This will give the last event recorded for this LE 
	*  In most cases this will return sleep_all_day
	*/
	return m_lasttreat.back();
}

int LE::GetLastTreatment(int* a_index) {
	if (*a_index == (int)m_lastindex)
		return sleep_all_day;
	int i = (*a_index)++;
	int treat = m_lasttreat[i];
	return treat;
}

void LE::Tick( void ) {
  m_lastindex = 0;
  m_lasttreat[ 0 ] = sleep_all_day;
  //reset if there is flower resource and it is not a farm when it is the first day in a year
  if (m_OurPollenNectarCurveSet && g_date->DayInYear() == 0 && m_owner_index<0) {
	PollenNectarReset();
  }
}

void VegElement::Tick(void) {
	LE::Tick();
	if (m_mowndecay > 0) m_mowndecay--;
	if (m_herbicidedelay > 0) m_herbicidedelay--;
	if (g_date->DayInYear() == 0)
	{
		// Reset the pollen and nectar day degrees indices
		m_ddegsNec = 0;
		m_ddegsSug = 0;
		m_ddegsPol = 0;
	}
}

void Field::Tick(void) {
	LE::Tick();
	if (m_mowndecay > 0) m_mowndecay--;
	if (m_herbicidedelay > 0) m_herbicidedelay--;
	if (m_tramlinesdecay > 0) m_tramlinesdecay--;
}


void LE::DoDevelopment( void ) {
}

APoint LE::GetCentroid()
{
    APoint p;
    p.m_x=m_centroidx;
    p.m_y=m_centroidy;
    return p;
}

int LE::GetGooseNos( ) {
	/**
	* This simply looks X days behind at the moment and sums the total number of geese seen.The length of the backward count can be altered by
	* changing the config variable value cfg_goosecountperiod (default 1, only care about yesterday).
	*/
	int geese = 0;
	for (unsigned i = 1; i <= (unsigned)cfg_goosecountperiod.value( ); i++) {
		unsigned ind = ((unsigned)g_date->DayInYear( ) - i) % 365;
		geese += m_gooseNos[ ind ];
	}
	return geese;
}

int LE::GetQuarryNos() {
	/**
	* This simply looks X days behind at the moment and sums the total number of legal quarry species seen.The length of the backward count can be altered by
	* changing the config variable value cfg_goosecountperiod (default 1, only care about yesterday).
	*/
	int geese = 0;
	for (unsigned i = 1; i <= (unsigned)cfg_goosecountperiod.value(); i++) {
		unsigned ind = ((unsigned)g_date->DayInYear() - i) % 365;
		geese += m_gooseSpNos[ind][gs_Pinkfoot];
		geese += m_gooseSpNos[ind][gs_Greylag];
	}
	return geese;
}

int LE::GetGooseNosToday() {
	/**
	* This simply sums the total number of geese seen today.
	*/
	int geese = 0;
	for (unsigned i = 0; i < (unsigned)gs_foobar; i++) {
		geese += m_gooseSpNos[g_date->DayInYear()][i];
	}
	return geese;
}

int LE::GetGooseNosTodayTimed() {
	/**
	* This simply sums the total number of geese seen today at our predefined timepoint.
	*/
	int geese = 0;
	for (unsigned i = 0; i < (unsigned)gs_foobar; i++) {
		geese += m_gooseSpNosTimed[g_date->DayInYear()][i];
	}
	return geese;
}

/** \brief Returns the number of geese of a specific species on a field today.*/
int LE::GetGooseSpNosToday(GooseSpecies a_goose) {
	return m_gooseSpNos[g_date->DayInYear()][a_goose];
}

/** \brief Returns the number of geese of a specific species on a field today.*/
int LE::GetGooseSpNosTodayTimed(GooseSpecies a_goose) {
	return m_gooseSpNosTimed[g_date->DayInYear()][a_goose];
}
/** \brief Returns the distance to closest roost from the field.*/
int LE::GetGooseRoostDist(GooseSpecies a_goose) {
	return int(m_dist_to_closest_roost[a_goose]);
}

void LE::SetPollenNectarType(TTypesOfVegetation a_new_veg)
{
	SetPollenNectarCurves(g_nectarpollen->tovGetPollenNectarCurve(a_new_veg));
	PollenNectarReset();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void VegElement::SetVegGrowthScalerRand()
{
	m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}

VegElement::VegElement(TTypesOfLandscapeElement tole, Landscape * L) : LE(L) {
	m_type = tole;
	m_owner_tole = m_type;
	// Set default Attribute Flags 
	Set_Att_Veg(true);
	Set_Att_VegPatchy(false);
	Set_Att_VegCereal(false);
	Set_Att_VegMatureCereal(false);
	Set_Att_VegGooseGrass(false);
	Set_Att_VegGrass(false);
	Set_Att_VegMaize(false);
	m_growth_scaler = 1.0; // default
	m_veg_biomass = 0.0;
	m_weed_biomass = 0.0;
	m_veg_height = 0.0;
	m_veg_cover = 0.0;
	m_insect_pop = 1.0;
	m_vege_type = tov_NoGrowth;
	m_curve_num = g_crops->VegTypeToCurveNum(m_vege_type);
	m_weed_curve_num = 99; // 99 used as default weed curve
	m_yddegs = 0.0;
	m_vegddegs = -1.0;
	m_ddegs = 0.0;
	m_LAgreen = 0.0;
	m_LAtotal = 0.0;
	m_digestability = 1.0;
	for (int i = 0; i < 32; i++) m_oldnewgrowth[i] = 0.5;
	for (int i = 0; i < 14; i++) m_oldnewgrowth2[i] = 0.0;
	m_newoldgrowthindex = 0;
	m_newgrowthsum = 8.0;
	m_forced_phase_shift = false;
	m_force_growth = false;
	SetGrowthPhase(janfirst);
	m_total_biomass = 0.0;
	m_total_biomass_old = 0.0;
	m_CropType = toc_Foobar;
	// Set default for species specific calculations
	SpeciesSpecificCalculations = &VegElement::DoNothing;

	g_biomass_scale[tov_Carrots] = 0.7857;
	g_biomass_scale[tov_CloverGrassGrazed1] = 1.2;

	g_biomass_scale[tov_CloverGrassGrazed2] = 1.2;
	g_biomass_scale[tov_FodderGrass] = 1.2;
	g_biomass_scale[ tov_BroadBeans ] = 0.857;
	g_biomass_scale[ tov_FieldPeas ] = 0.857;
	g_biomass_scale[ tov_FieldPeasSilage ] = 0.857;
	g_biomass_scale[tov_FodderBeet] = 0.857;
	g_biomass_scale[tov_SugarBeet] = 0.857;
	g_biomass_scale[tov_OFodderBeet] = 0.857;
	g_biomass_scale[tov_Maize] = 1.00;
	g_biomass_scale[tov_MaizeSilage] = 1.00;
	g_biomass_scale[tov_OMaizeSilage] = 1.00;
	g_biomass_scale[tov_NaturalGrass] = 0.567; //0.567 is scaled for actual yield
	g_biomass_scale[tov_Heath] = 0.567; //0.567 is scaled for actual yield
	g_biomass_scale[tov_NoGrowth] = 0.0;
	g_biomass_scale[tov_None] = 0.0;
	g_biomass_scale[tov_OrchardCrop] = 0.7857;
	g_biomass_scale[tov_Oats] = 0.857;
	g_biomass_scale[tov_OBarleyPeaCloverGrass] = 0.857;
	g_biomass_scale[tov_OSBarleySilage] = 0.857 * 0.8;
	g_biomass_scale[tov_OCarrots] = 0.7857 * 0.8;
	g_biomass_scale[tov_OCloverGrassGrazed1] = 1.1;
	g_biomass_scale[tov_OCloverGrassGrazed2] = 1.1;
	g_biomass_scale[tov_OCloverGrassSilage1] = 1.1;
	g_biomass_scale[tov_OFieldPeas] = 0.857 * 0.8;
	g_biomass_scale[tov_OFieldPeasSilage] = 0.857 * 0.8;
	g_biomass_scale[tov_OFirstYearDanger] = 0.0;
	g_biomass_scale[tov_OGrazingPigs] = 0.7857 * 0.8;
	g_biomass_scale[tov_OOats] = 0.857 * 0.8;
	g_biomass_scale[tov_OPermanentGrassGrazed] = 0.7857;
	g_biomass_scale[tov_OPotatoes] = 0.857 * 0.8;
	g_biomass_scale[tov_OSeedGrass1] = 0.7857;
	g_biomass_scale[tov_OSeedGrass2] = 0.7857;
	g_biomass_scale[tov_OSetAside] = 0.7857;
	g_biomass_scale[tov_OSpringBarley] = 0.857 * 0.8;
	g_biomass_scale[tov_OSpringBarleyExt] = 0.857 * 0.8 * 0.8;
	g_biomass_scale[tov_OSpringBarleyClover] = 0.857 * 0.8;
	g_biomass_scale[tov_OSpringBarleyGrass] = 0.857 * 0.8;
	g_biomass_scale[tov_OSpringBarleyPigs] = 0.857 * 0.8;
	g_biomass_scale[tov_OTriticale] = 1.00 * 0.8;
	g_biomass_scale[tov_OWinterBarley] = 0.857 * 0.8;
	g_biomass_scale[tov_OWinterBarleyExt] = 0.857 * 0.8 * 0.8;
	g_biomass_scale[tov_OWinterRape] = 1.071 * 0.8;
	g_biomass_scale[tov_OWinterRye] = 0.857 * 0.8;
	g_biomass_scale[ tov_OWinterWheat ] = 1.00 * 0.8;
	g_biomass_scale[ tov_OWinterWheatUndersown ] = 1.00 * 0.8;
	g_biomass_scale[ tov_OWinterWheatUndersownExt ] = 1.00 * 0.8 * 0.8;
	g_biomass_scale[tov_PermanentGrassGrazed] = 1.1;
	g_biomass_scale[tov_PermanentGrassLowYield] = 1.0;
	g_biomass_scale[tov_PermanentGrassTussocky] = 0.7857;
	g_biomass_scale[tov_PermanentSetAside] = 0.7857;
	g_biomass_scale[tov_Potatoes] = 0.857;
	g_biomass_scale[tov_PotatoesIndustry] = 0.857;
	g_biomass_scale[tov_SeedGrass1] = 0.7857;
	g_biomass_scale[tov_SeedGrass2] = 0.7857;
	g_biomass_scale[tov_OSeedGrass1] = 0.7857;
	g_biomass_scale[tov_OSeedGrass2] = 0.7857;
	g_biomass_scale[tov_SetAside] = 0.7857;
	g_biomass_scale[tov_SpringBarley] = 0.857;
	g_biomass_scale[tov_SpringBarleySpr] = 0.857;
	g_biomass_scale[tov_SpringBarleyPTreatment] = 0.857;
	g_biomass_scale[tov_SpringBarleySKManagement] = 0.857;
	g_biomass_scale[tov_SpringBarleyCloverGrass] = 0.857;
	g_biomass_scale[tov_SpringBarleyGrass] = 0.857;
	g_biomass_scale[tov_SpringBarleySeed] = 0.857;
	g_biomass_scale[tov_SpringBarleySilage] = 0.857;
	g_biomass_scale[tov_SpringRape] = 1.071;
	g_biomass_scale[tov_SpringWheat] = 1.00;
	g_biomass_scale[tov_Triticale] = 1.00;
	g_biomass_scale[tov_WinterBarley] = 0.857;
	g_biomass_scale[tov_WinterRape] = 1.071;
	g_biomass_scale[tov_WinterRye] = 0.857;
	g_biomass_scale[tov_WinterWheat] = 1.00;  // This gives approx 18 tonnes biomass for WW
	g_biomass_scale[tov_WinterWheatShort] = 1.00;
	g_biomass_scale[tov_AgroChemIndustryCereal] = 1.00;
	g_biomass_scale[tov_WWheatPControl] = 1.00;
	g_biomass_scale[tov_WWheatPToxicControl] = 1.00;
	g_biomass_scale[tov_WWheatPTreatment] = 1.00;
	g_biomass_scale[tov_WinterWheatStrigling] = 1.00;
	g_biomass_scale[tov_WinterWheatStriglingCulm] = 1.00;
	g_biomass_scale[tov_WinterWheatStriglingSingle] = 1.00;
	g_biomass_scale[tov_SpringBarleyCloverGrassStrigling] = 0.857;
	g_biomass_scale[tov_SpringBarleyStrigling] = 0.857;
	g_biomass_scale[tov_SpringBarleyStriglingSingle] = 0.857;
	g_biomass_scale[tov_SpringBarleyStriglingCulm] = 0.857;
	g_biomass_scale[tov_MaizeStrigling] = 0.857;
	g_biomass_scale[tov_WinterRapeStrigling] = 1.071;
	g_biomass_scale[tov_WinterRyeStrigling] = 0.857;
	g_biomass_scale[tov_WinterBarleyStrigling] = 0.857;
	g_biomass_scale[tov_FieldPeasStrigling] = 0.857;
	g_biomass_scale[tov_SpringBarleyPeaCloverGrassStrigling] = 0.857;
	g_biomass_scale[tov_YoungForest] = 0.7857 * 0.67;
	g_biomass_scale[tov_Wasteland] = 0.7857 * 0.67;
	g_biomass_scale[tov_WaterBufferZone] = 0.567;

	g_biomass_scale[tov_PLWinterWheat] = 1.00;
	g_biomass_scale[tov_PLWinterRape] = 1.071;
	g_biomass_scale[tov_PLWinterBarley] = 0.857;
	g_biomass_scale[tov_PLWinterTriticale] = 1.00;
	g_biomass_scale[tov_PLWinterRye] = 0.857;
	g_biomass_scale[tov_PLSpringWheat] = 1.00;
	g_biomass_scale[tov_PLSpringBarley] = 0.857;
	g_biomass_scale[tov_PLMaize] = 1.00;
	g_biomass_scale[tov_PLMaizeSilage] = 1.00;
	g_biomass_scale[tov_PLPotatoes] = 0.857;
	g_biomass_scale[tov_PLBeet] = 0.857;
	g_biomass_scale[tov_PLFodderLucerne1] = 1.2;
	g_biomass_scale[tov_PLFodderLucerne2] = 1.2;
	g_biomass_scale[tov_PLCarrots] = 0.7857;
	g_biomass_scale[tov_PLSpringBarleySpr] = 0.857;
	g_biomass_scale[tov_PLWinterWheatLate] = 1.00;
	g_biomass_scale[tov_PLBeetSpr] = 0.857;
	g_biomass_scale[tov_PLBeans] = 0.857;

	g_biomass_scale[tov_NLWinterWheat] = 1.00;
	g_biomass_scale[tov_NLSpringBarley] = 0.857;
	g_biomass_scale[tov_NLMaize] = 1.00;
	g_biomass_scale[tov_NLPotatoes] = 0.857;
	g_biomass_scale[tov_NLBeet] = 0.857;
	g_biomass_scale[tov_NLCarrots] = 0.7857;
	g_biomass_scale[tov_NLCabbage] = 0.7857;
	g_biomass_scale[tov_NLTulips] = 0.7857;
	g_biomass_scale[tov_NLGrassGrazed1] = 1.2;
	g_biomass_scale[tov_NLGrassGrazed1Spring] = 1.2;
	g_biomass_scale[tov_NLGrassGrazed2] = 1.2;
	g_biomass_scale[tov_NLGrassGrazedLast] = 1.2;
	g_biomass_scale[tov_NLPermanentGrassGrazed] = 1.1;
	g_biomass_scale[tov_NLSpringBarleySpring] = 0.857;
	g_biomass_scale[tov_NLMaizeSpring] = 1.00;
	g_biomass_scale[tov_NLPotatoesSpring] = 0.857;
	g_biomass_scale[tov_NLBeetSpring] = 0.857;
	g_biomass_scale[tov_NLCarrotsSpring] = 0.7857;
	g_biomass_scale[tov_NLCabbageSpring] = 0.7857;
	g_biomass_scale[tov_NLCatchCropPea] = 0.857;
	g_biomass_scale[tov_NLOrchardCrop] = 0.7857;
	g_biomass_scale[tov_NLPermanentGrassGrazedExtensive] = 1.1;
	g_biomass_scale[tov_NLGrassGrazedExtensive1] = 1.2;
	g_biomass_scale[tov_NLGrassGrazedExtensive1Spring] = 1.2;
	g_biomass_scale[tov_NLGrassGrazedExtensive2] = 1.2;
	g_biomass_scale[tov_NLGrassGrazedExtensiveLast] = 1.2;

	g_biomass_scale[tov_UKBeans] = 0.857;
	g_biomass_scale[tov_UKBeet] = 0.857;
	g_biomass_scale[tov_UKMaize] = 1.00;
	g_biomass_scale[tov_UKPermanentGrass] = 1.1;
	g_biomass_scale[tov_UKPotatoes] = 0.857;
	g_biomass_scale[tov_UKSpringBarley] = 0.857;
	g_biomass_scale[tov_UKTempGrass] = 1.2;
	g_biomass_scale[tov_UKWinterBarley] = 0.857;
	g_biomass_scale[tov_UKWinterRape] = 1.071;
	g_biomass_scale[tov_UKWinterWheat] = 1.00;

	g_biomass_scale[tov_BEBeet] = 0.857;
	g_biomass_scale[tov_BEBeetSpring] = 0.857;
	g_biomass_scale[tov_BECatchPeaCrop] = 0.857;
	g_biomass_scale[tov_BEGrassGrazed1] = 1.2;
	g_biomass_scale[tov_BEGrassGrazed1Spring] = 1.2;
	g_biomass_scale[tov_BEGrassGrazed2] = 1.2;
	g_biomass_scale[tov_BEGrassGrazedLast] = 1.2;
	g_biomass_scale[tov_BEMaize] = 1.00;
	g_biomass_scale[tov_BEMaizeSpring] = 1.00;
	g_biomass_scale[tov_BEOrchardCrop] = 0.7857;
	g_biomass_scale[tov_BEPotatoes] = 0.857;
	g_biomass_scale[tov_BEPotatoesSpring] = 0.857;
	g_biomass_scale[tov_BEWinterBarley] = 0.857;
	g_biomass_scale[tov_BEWinterWheat] = 1.00;
	g_biomass_scale[tov_BEWinterWheatCC] = 1.00;
	g_biomass_scale[tov_BEWinterBarleyCC] = 1.00;
	g_biomass_scale[tov_BEMaizeCC] = 1.00;

	g_biomass_scale[tov_DESugarBeet] = 0.857;
	g_biomass_scale[tov_DECabbage] = 0.7857;
	g_biomass_scale[tov_DECarrots] = 0.7857;
	g_biomass_scale[tov_DEGrasslandSilageAnnual] = 1.1;
	g_biomass_scale[tov_DEGreenFallow_1year] = 0.7857;
	g_biomass_scale[tov_DELegumes] = 0.857;
	g_biomass_scale[tov_DEMaize] = 1.0;
	g_biomass_scale[tov_DEMaizeSilage] = 1.0;
	g_biomass_scale[tov_DEOats] = 0.857;
	g_biomass_scale[tov_DEOCabbages] = 0.7857 * 0.8;
	g_biomass_scale[tov_DEOCarrots] = 0.7857 * 0.8;
	g_biomass_scale[tov_DEOGrasslandSilageAnnual] = 1.1 * 0.8;
	g_biomass_scale[tov_DEOGreenFallow_1year] = 0.857 * 0.8;
	g_biomass_scale[tov_DEOLegume] = 0.857 * 0.8;
	g_biomass_scale[tov_DEOMaize] = 1.0 * 0.8;
	g_biomass_scale[tov_DEOMaizeSilage] = 1.0 * 0.8;
	g_biomass_scale[tov_DEOOats] = 0.857 * 0.8;;
	g_biomass_scale[tov_DEOPermanentGrassGrazed] = 1.0;
	g_biomass_scale[tov_DEOPotatoes] = 0.857 * 0.8;;
	g_biomass_scale[tov_DEOSpringRye] = 0.857 * 0.8;
	g_biomass_scale[tov_DEOSugarBeet] = 0.857 * 0.8;
	g_biomass_scale[tov_DEOTriticale] = 0.8;
	g_biomass_scale[tov_DEOWinterBarley] = 0.8;
	g_biomass_scale[tov_DEOWinterRape] = 0.8;
	g_biomass_scale[tov_DEOWinterRye] = 0.857 * 0.8;
	g_biomass_scale[tov_DEOWinterWheat] = 0.8;
	g_biomass_scale[tov_DEPermanentGrassGrazed] = 1.0;
	g_biomass_scale[tov_DEPermanentGrassLowYield] = 1.0;
	g_biomass_scale[tov_DEOPermanentGrassLowYield] = 1.0;
	g_biomass_scale[tov_DEPotatoes] = 0.857;
	g_biomass_scale[tov_DEPotatoesIndustry] = 0.857;
	g_biomass_scale[tov_DESpringRye] = 0.857;
	g_biomass_scale[tov_DETriticale] = 1.0;
	g_biomass_scale[tov_DEWinterRye] = 0.857;
	g_biomass_scale[tov_DEWinterBarley] = 1.0;
	g_biomass_scale[tov_DEWinterRape] = 1.0;
	g_biomass_scale[tov_DEWinterWheat] = 1.0;
	g_biomass_scale[tov_DEWinterWheatLate] = 1.0;
	g_biomass_scale[tov_DEAsparagusEstablishedPlantation] = 0.857 * 0.8;
	g_biomass_scale[tov_DEOAsparagusEstablishedPlantation] = 0.857 * 0.8;
	g_biomass_scale[tov_DEHerbsPerennial_1year] = 0.857 * 0.8;
	g_biomass_scale[tov_DEHerbsPerennial_after1year] = 0.857 * 0.8;
	g_biomass_scale[tov_DEOHerbsPerennial_1year] = 0.857 * 0.8;
	g_biomass_scale[tov_DEOHerbsPerennial_after1year] = 0.857 * 0.8;
	g_biomass_scale[tov_DESpringBarley] = 0.857;
	g_biomass_scale[tov_DEOrchard] = 0.7857;
	g_biomass_scale[tov_DEOOrchard] = 0.7857;
	g_biomass_scale[tov_DEPeas] = 0.857;
	g_biomass_scale[tov_DEOPeas] = 0.857;
	g_biomass_scale[tov_DEBushFruitPerm] = 0.7857;
	g_biomass_scale[tov_DEOBushFruitPerm] = 0.7857;

	g_biomass_scale[tov_DKOFodderBeets] = 0.857;
	g_biomass_scale[tov_DKCatchCrop] = 0.857;
	g_biomass_scale[tov_DKOCatchCrop] = 0.857;
	g_biomass_scale[tov_DKOLegume_Beans] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOLentils] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOLupines] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOLegume_Beans_CC] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOLegume_Peas] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOLegume_Whole] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOLegume_Peas_CC] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOLegume_Whole_CC] = 0.857 * 0.8;
	g_biomass_scale[tov_DKSugarBeets] = 0.857;
	g_biomass_scale[tov_DKFodderBeets] = 0.857;
	g_biomass_scale[tov_DKOSugarBeets] = 0.857 * 0.8;
	g_biomass_scale[tov_DKCabbages] = 0.7857;
	g_biomass_scale[tov_DKOCabbages] = 0.7857 * 0.8;
	g_biomass_scale[tov_DKCarrots] = 0.7857;
	g_biomass_scale[tov_DKOLegumeCloverGrass_Whole] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOCarrots] = 0.7857 * 0.8;
	g_biomass_scale[tov_DKLegume_Whole] = 0.857;
	g_biomass_scale[tov_DKLegume_Peas] = 0.857;
	g_biomass_scale[tov_DKLegume_Beans] = 0.857;
	g_biomass_scale[tov_DKWinterWheat] = 1.00;
	g_biomass_scale[tov_DKOWinterWheat] = 1.00 * 0.8;
	g_biomass_scale[tov_DKWinterWheat_CC] = 1.00;
	g_biomass_scale[tov_DKOWinterWheat_CC] = 1.00 * 0.8;
	g_biomass_scale[tov_DKSpringBarley] = 0.857;
	g_biomass_scale[tov_DKSpringBarley_CC] = 0.857;
	g_biomass_scale[tov_DKSpringBarleyCloverGrass] = 0.857;
	g_biomass_scale[tov_DKOSpringBarleyCloverGrass] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOSpringBarley] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOSpringBarley_CC] = 0.857 * 0.8;
	g_biomass_scale[tov_DKCerealLegume] = 0.857;
	g_biomass_scale[tov_DKOCerealLegume] = 0.857 * 0.8;
	g_biomass_scale[tov_DKCerealLegume_Whole] = 0.857;
	g_biomass_scale[tov_DKOCerealLegume_Whole] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOCloverGrassGrazed2] = 1.1;
	g_biomass_scale[tov_DKOCloverGrassGrazed1] = 1.1;
	g_biomass_scale[tov_DKOWinterCloverGrassGrazedSown] = 1.1;
	g_biomass_scale[tov_DKCloverGrassGrazed2] = 1.1;
	g_biomass_scale[tov_DKCloverGrassGrazed1] = 1.1;
	g_biomass_scale[tov_DKWinterCloverGrassGrazedSown] = 1.1;
	g_biomass_scale[tov_DKSpringFodderGrass] = 1.2;
	g_biomass_scale[tov_DKWinterFodderGrass] = 1.2;
	g_biomass_scale[tov_DKOSpringFodderGrass] = 1.2;
	g_biomass_scale[tov_DKOWinterFodderGrass] = 1.2;
	g_biomass_scale[tov_DKGrazingPigs] = 0.7857;
	g_biomass_scale[tov_DKMaize] = 1.00;
	g_biomass_scale[tov_DKMaizeSilage] = 1.00;
	g_biomass_scale[tov_DKMixedVeg] = 0.7857;
	g_biomass_scale[tov_DKOGrazingPigs] = 0.7857 * 0.8;
	g_biomass_scale[tov_DKOMaize] = 1.00 * 0.8;
	g_biomass_scale[tov_DKOMaizeSilage] = 1.00 * 0.8;
	g_biomass_scale[tov_DKOMixedVeg] = 0.7857 * 0.8;
	g_biomass_scale[tov_DKOPotato] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOPotatoIndustry] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOPotatoSeed] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOSeedGrassRye_Spring] = 0.7857;
	g_biomass_scale[tov_DKOSetAside] = 0.7857;
	g_biomass_scale[tov_DKOSetAside_AnnualFlower] = 0.7857;
	g_biomass_scale[tov_DKOSetAside_PerennialFlower] = 0.7857;
	g_biomass_scale[tov_DKOSetAside_SummerMow] = 0.7857;
	g_biomass_scale[tov_DKOSpringBarleySilage] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOSpringOats] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOSpringOats_CC] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOSpringWheat] = 1.0 * 0.8;
	g_biomass_scale[tov_DKOVegSeeds] = 0.7857;
	g_biomass_scale[tov_DKOWinterBarley] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOWinterRape] = 1.071 * 0.8;
	g_biomass_scale[tov_DKOWinterRye] = 0.857 * 0.8;
	g_biomass_scale[tov_DKOWinterRye_CC] = 0.857 * 0.8;
	g_biomass_scale[tov_DKPotato] = 0.857;
	g_biomass_scale[tov_DKPotatoIndustry] = 0.857;
	g_biomass_scale[tov_DKPotatoSeed] = 0.857;
	g_biomass_scale[tov_DKSeedGrassFescue_Spring] = 0.7857;
	g_biomass_scale[tov_DKSeedGrassRye_Spring] = 0.7857;
	g_biomass_scale[tov_DKSetAside] = 0.7857;
	g_biomass_scale[tov_DKSetAside_SummerMow] = 0.7857;
	g_biomass_scale[tov_DKSpringBarley_Green] = 0.857;
	g_biomass_scale[tov_DKSpringBarleySilage] = 0.857;
	g_biomass_scale[tov_DKSpringOats] = 0.857;
	g_biomass_scale[tov_DKSpringOats_CC] = 0.857;
	g_biomass_scale[tov_DKSpringWheat] = 1.0;
	g_biomass_scale[tov_DKUndefined] = 0.0;
	g_biomass_scale[tov_DKVegSeeds] = 0.7857;
	g_biomass_scale[tov_DKWinterBarley] = 0.857;
	g_biomass_scale[tov_DKWinterRape] = 1.071;
	g_biomass_scale[tov_DKWinterRye] = 0.857;
	g_biomass_scale[tov_DKWinterRye_CC] = 0.857;
	g_biomass_scale[tov_DKOOrchardCrop_Perm] = 0.7857 * 0.8;
	g_biomass_scale[tov_DKOrchardCrop_Perm] = 0.7857;
	g_biomass_scale[tov_DKOrchApple] = 0.7857;
	g_biomass_scale[tov_DKOrchPear] = 0.7857;
	g_biomass_scale[tov_DKOrchCherry] = 0.7857;
	g_biomass_scale[tov_DKOrchOther] = 0.7857;
	g_biomass_scale[tov_DKOOrchApple] = 0.7857;
	g_biomass_scale[tov_DKOOrchPear] = 0.7857;
	g_biomass_scale[tov_DKOOrchCherry] = 0.7857;
	g_biomass_scale[tov_DKOOrchOther] = 0.7857;
	g_biomass_scale[tov_DKBushFruit_Perm1] = 0.7857;
	g_biomass_scale[tov_DKBushFruit_Perm2] = 0.7857;
	g_biomass_scale[tov_DKOBushFruit_Perm1] = 0.7857 * 0.8;
	g_biomass_scale[tov_DKOBushFruit_Perm2] = 0.7857 * 0.8;
	g_biomass_scale[tov_DKChristmasTrees_Perm] = 0.7857 * 0.67;
	g_biomass_scale[tov_DKOChristmasTrees_Perm] = 0.7857 * 0.67;
	g_biomass_scale[tov_DKEnergyCrop_Perm] = 0.7857 * 0.67;
	g_biomass_scale[tov_DKOEnergyCrop_Perm] = 0.7857 * 0.67;
	g_biomass_scale[tov_DKFarmForest_Perm] = 0.7857 * 0.67;
	g_biomass_scale[tov_DKOFarmForest_Perm] = 0.7857 * 0.67;
	g_biomass_scale[tov_DKGrazingPigs_Perm] = 0.7857;
	g_biomass_scale[tov_DKOGrazingPigs_Perm] = 0.7857 * 0.8;
	g_biomass_scale[tov_DKOGrassGrazed_Perm] = 1.1;
	g_biomass_scale[tov_DKOGrassLowYield_Perm] = 1.0;
	g_biomass_scale[tov_DKFarmYoungForest_Perm] = 0.7857 * 0.67;
	g_biomass_scale[tov_DKOFarmYoungForest_Perm] = 0.7857 * 0.67;
	g_biomass_scale[tov_DKPlantNursery_Perm] = 0.7857;
	g_biomass_scale[tov_DKGrassGrazed_Perm] = 1.1;
	g_biomass_scale[tov_DKGrassLowYield_Perm] = 1.0;
	g_biomass_scale[tov_DKCatchCrop] = 1.0;

	g_biomass_scale[tov_PTPermanentGrassGrazed] = 1.1;
	g_biomass_scale[tov_PTWinterWheat] = 1.00;
	g_biomass_scale[tov_PTGrassGrazed] = 1.2;
	g_biomass_scale[tov_PTSorghum] = 1.00;
	g_biomass_scale[tov_PTFodderMix] = 1.2;
	g_biomass_scale[tov_PTTurnipGrazed] = 0.7857;
	g_biomass_scale[tov_PTCloverGrassGrazed1] = 1.2;
	g_biomass_scale[tov_PTCloverGrassGrazed2] = 1.2;
	g_biomass_scale[tov_PTTriticale] = 1.00;
	g_biomass_scale[tov_PTOtherDryBeans] = 0.857;
	g_biomass_scale[tov_PTShrubPastures] = 0.567;
	g_biomass_scale[tov_PTCorkOak] = 0.7857;
	g_biomass_scale[tov_PTVineyards] = 0.7857;
	g_biomass_scale[tov_PTWinterBarley] = 1.00;
	g_biomass_scale[tov_PTBeans] = 0.857;
	g_biomass_scale[tov_PTWinterRye] = 0.857;
	g_biomass_scale[tov_PTRyegrass] = 1.2;
	g_biomass_scale[tov_PTYellowLupin] = 1.2;
	g_biomass_scale[tov_PTMaize] = 1.00;
	g_biomass_scale[tov_PTOats] = 0.857;
	g_biomass_scale[tov_PTPotatoes] = 0.857;
	g_biomass_scale[tov_PTHorticulture] = 0.857;

	g_biomass_scale[tov_FIWinterWheat] = 1.0;
	g_biomass_scale[tov_FIOWinterWheat] = 1.0 * 0.8;
	g_biomass_scale[tov_FISugarBeet] = 0.857;
	g_biomass_scale[tov_FIStarchPotato_North] = 0.857;
	g_biomass_scale[tov_FIStarchPotato_South] = 0.857;
	g_biomass_scale[tov_FIOStarchPotato_North] = 0.857 * 0.8;
	g_biomass_scale[tov_FIOStarchPotato_South] = 0.857 * 0.8;
	g_biomass_scale[tov_FISpringWheat] = 1.0;
	g_biomass_scale[tov_FIOSpringWheat] = 1.0 * 0.8;
	g_biomass_scale[tov_FITurnipRape] = 1.071;
	g_biomass_scale[tov_FIOTurnipRape] = 1.071 * 0.8;
	g_biomass_scale[tov_FISpringRape] = 1.071;
	g_biomass_scale[tov_FIOSpringRape] = 1.071 * 0.8;
	g_biomass_scale[tov_FIWinterRye] = 0.857;
	g_biomass_scale[tov_FIOWinterRye] = 0.857 * 0.8;
	g_biomass_scale[tov_FIPotato_North] = 0.857;
	g_biomass_scale[tov_FIPotato_South] = 0.857;
	g_biomass_scale[tov_FIOPotato_North] = 0.857 * 0.8;
	g_biomass_scale[tov_FIOPotato_South] = 0.857 * 0.8;
	g_biomass_scale[tov_FIPotatoIndustry_North] = 0.857;
	g_biomass_scale[tov_FIPotatoIndustry_South] = 0.857;
	g_biomass_scale[tov_FIOPotatoIndustry_North] = 0.857 * 0.8;
	g_biomass_scale[tov_FIOPotatoIndustry_South] = 0.857 * 0.8;
	g_biomass_scale[tov_FISpringOats] = 0.857;
	g_biomass_scale[tov_FIOSpringOats] = 0.857 * 0.8;
	g_biomass_scale[tov_FISpringBarley_Malt] = 0.857;
	g_biomass_scale[tov_FIOSpringBarley_Malt] = 0.857 * 0.8;
	g_biomass_scale[tov_FIFabaBean] = 0.857;
	g_biomass_scale[tov_FIOFabaBean] = 0.857 * 0.8;
	g_biomass_scale[tov_FISpringBarley_Fodder] = 0.857;
	g_biomass_scale[tov_FISprSpringBarley_Fodder] = 0.857;
	g_biomass_scale[tov_FIOSpringBarley_Fodder] = 0.857 * 0.8;
	g_biomass_scale[tov_FIGrasslandPasturePerennial1] = 1.1;
	g_biomass_scale[tov_FIGrasslandPasturePerennial2] = 1.1;
	g_biomass_scale[tov_FIGrasslandSilagePerennial1] = 1.1;
	g_biomass_scale[tov_FIGrasslandSilagePerennial2] = 1.1;
	g_biomass_scale[tov_FINaturalGrassland] = 0.567; // actual yield
	g_biomass_scale[tov_FIFeedingGround] = 1.1;
	g_biomass_scale[tov_FIGreenFallow_1year] = 1.1;
	g_biomass_scale[tov_FIBufferZone] = 1.1;
	g_biomass_scale[tov_FIGrasslandSilageAnnual] = 1.1;
	g_biomass_scale[tov_FICaraway1] = 0.7857;
	g_biomass_scale[tov_FICaraway2] = 0.7857;
	g_biomass_scale[tov_FIOCaraway1] = 0.7857;
	g_biomass_scale[tov_FIOCaraway2] = 0.7857;
	g_biomass_scale[tov_FINaturalGrassland_Perm] = 0.567; // actual yield
	g_biomass_scale[tov_FIGreenFallow_Perm] = 1.1;
	g_biomass_scale[tov_FIBufferZone_Perm] = 1.1;

	g_biomass_scale[tov_SESpringBarley] = 0.857;
	g_biomass_scale[tov_SEWinterRape_Seed] = 1.071;
	g_biomass_scale[tov_SEWinterWheat] = 1.0;

	g_biomass_scale[tov_IRSpringWheat] = 1.0;
	g_biomass_scale[tov_IRSpringBarley] = 0.857;
	g_biomass_scale[tov_IRSpringOats] = 0.857;
	g_biomass_scale[tov_IRGrassland_no_reseed] = 1.1;
	g_biomass_scale[tov_IRGrassland_reseed] = 1.1;
	g_biomass_scale[tov_IRWinterWheat] = 1.0;
	g_biomass_scale[tov_IRWinterBarley] = 1.00;
	g_biomass_scale[tov_IRWinterOats] = 0.857;
		
	g_biomass_scale[tov_FRWinterWheat] = 1.0;
	g_biomass_scale[tov_FRWinterBarley] = 0.857;
	g_biomass_scale[tov_FRWinterTriticale] = 1.0;
	g_biomass_scale[tov_FRWinterRape] = 1.071;
	g_biomass_scale[tov_FRMaize] = 1.0;
	g_biomass_scale[tov_FRMaize_Silage] = 1.0;
	g_biomass_scale[tov_FRSpringBarley] = 0.857;
	g_biomass_scale[tov_FRGrassland] = 1.1;
	g_biomass_scale[tov_FRGrassland_Perm] = 1.1;
	g_biomass_scale[tov_FRSpringOats] = 0.857;
	g_biomass_scale[tov_FRSunflower] = 1.0; // need to check this
	g_biomass_scale[tov_FRSpringWheat] = 1.0;
	g_biomass_scale[tov_FRPotatoes] = 0.857;
	g_biomass_scale[tov_FRSorghum] = 1.0; // should be similar to maize

	g_biomass_scale[tov_ITGrassland] = 1.1;
	g_biomass_scale[tov_ITOrchard] = 0.7857;
	g_biomass_scale[tov_ITOOrchard] = 0.7857 * 0.8;

	g_biomass_scale[tov_DummyCropPestTesting] = 1.00;	// just for testing of pesticide spraying distribution

	if (l_el_read_bug_percentage_file.value()) {
		//ReadBugPercentageFile();
		return;
	}

	g_weed_percent[tov_Carrots] = 0.1;
	g_weed_percent[ tov_BroadBeans ] = 0.1;
	g_weed_percent[ tov_Maize ] = 0.05;
	g_weed_percent[tov_MaizeSilage] = 0.05;
	g_weed_percent[tov_OMaizeSilage] = 0.05;
	g_weed_percent[tov_OCarrots] = 0.1;
	g_weed_percent[tov_Potatoes] = 0.1;
	g_weed_percent[tov_OPotatoes] = 0.1;
	g_weed_percent[tov_FodderGrass] = 0.1;
	g_weed_percent[tov_CloverGrassGrazed1] = 0.1;
	g_weed_percent[tov_CloverGrassGrazed2] = 0.1;
	g_weed_percent[tov_OCloverGrassGrazed1] = 0.1;
	g_weed_percent[tov_OCloverGrassGrazed2] = 0.1;
	g_weed_percent[tov_SpringBarley] = 0.1;
	g_weed_percent[tov_SpringBarleySpr] = 0.1;
	g_weed_percent[tov_SpringBarleyPTreatment] = 0.1;
	g_weed_percent[tov_SpringBarleySKManagement] = 0.1;
	g_weed_percent[tov_WinterWheat] = 0.1;
	g_weed_percent[tov_WinterWheatShort] = 0.1;
	g_weed_percent[tov_SpringBarleySilage] = 0.1;
	g_weed_percent[tov_SpringBarleySeed] = 0.1;
	g_weed_percent[tov_OGrazingPigs] = 0.1;
	g_weed_percent[tov_OCloverGrassSilage1] = 0.1;
	g_weed_percent[tov_SpringBarleyCloverGrass] = 0.1;
	g_weed_percent[tov_OSpringBarleyPigs] = 0.1;
	g_weed_percent[tov_OBarleyPeaCloverGrass] = 0.1;
	g_weed_percent[tov_OSpringBarley] = 0.1;
	g_weed_percent[tov_OSpringBarleyExt] = 0.1;
	g_weed_percent[tov_OSBarleySilage] = 0.1;
	g_weed_percent[ tov_OWinterWheat ] = 0.1;
	g_weed_percent[ tov_OWinterWheatUndersown ] = 0.1;
	g_weed_percent[ tov_OWinterWheatUndersownExt ] = 0.1;
	g_weed_percent[tov_WinterRape] = 0.05;
	g_weed_percent[tov_OWinterRape] = 0.05;
	g_weed_percent[tov_OWinterRye] = 0.1;
	g_weed_percent[tov_OWinterBarley] = 0.1;
	g_weed_percent[tov_OWinterBarleyExt] = 0.1;
	g_weed_percent[tov_WinterBarley] = 0.1;
	g_weed_percent[tov_WinterRye] = 0.1;
	g_weed_percent[ tov_OFieldPeas ] = 0.1;
	g_weed_percent[tov_OFieldPeas] = 0.1;
	g_weed_percent[ tov_OFieldPeasSilage ] = 0.1;
	g_weed_percent[tov_OOats] = 0.1;
	g_weed_percent[tov_Oats] = 0.1;
	g_weed_percent[tov_Heath] = 0.1;
	g_weed_percent[tov_OrchardCrop] = 0.1;
	g_weed_percent[ tov_FieldPeas ] = 0.1;
	g_weed_percent[ tov_FieldPeasSilage ] = 0.1;
	g_weed_percent[tov_SeedGrass1] = 0.1;
	g_weed_percent[tov_SeedGrass2] = 0.1;
	g_weed_percent[tov_OSeedGrass1] = 0.15;
	g_weed_percent[tov_OSeedGrass2] = 0.15;
	g_weed_percent[tov_OPermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_SetAside] = 0.1;
	g_weed_percent[tov_OSetAside] = 0.1;
	g_weed_percent[tov_PermanentSetAside] = 0.1;
	g_weed_percent[tov_PermanentGrassLowYield] = 0.1;
	g_weed_percent[tov_PermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_PermanentGrassTussocky] = 0.1;
	g_weed_percent[tov_FodderBeet] = 0.1;
	g_weed_percent[tov_SugarBeet] = 0.1;
	g_weed_percent[tov_OFodderBeet] = 0.1;
	g_weed_percent[tov_NaturalGrass] = 0.1;
	g_weed_percent[tov_None] = 0.1;
	g_weed_percent[tov_NoGrowth] = 0.1;
	g_weed_percent[tov_OFirstYearDanger] = 0.1;
	g_weed_percent[tov_Triticale] = 0.1;
	g_weed_percent[tov_OTriticale] = 0.1;
	g_weed_percent[tov_WWheatPControl] = 0.1;
	g_weed_percent[tov_WWheatPToxicControl] = 0.1;
	g_weed_percent[tov_WWheatPTreatment] = 0.1;
	g_weed_percent[tov_AgroChemIndustryCereal] = 0.1;
	g_weed_percent[tov_WinterWheatStrigling] = 0.1;
	g_weed_percent[tov_WinterWheatStriglingSingle] = 0.1;
	g_weed_percent[tov_WinterWheatStriglingCulm] = 0.1;
	g_weed_percent[tov_SpringBarleyCloverGrassStrigling] = 0.1;
	g_weed_percent[tov_SpringBarleyStrigling] = 0.1;
	g_weed_percent[tov_SpringBarleyStriglingSingle] = 0.1;
	g_weed_percent[tov_SpringBarleyStriglingCulm] = 0.1;
	g_weed_percent[tov_MaizeStrigling] = 0.1;
	g_weed_percent[tov_WinterRapeStrigling] = 0.1;
	g_weed_percent[tov_WinterRyeStrigling] = 0.1;
	g_weed_percent[tov_WinterBarleyStrigling] = 0.1;
	g_weed_percent[tov_FieldPeasStrigling] = 0.1;
	g_weed_percent[tov_SpringBarleyPeaCloverGrassStrigling] = 0.1;
	g_weed_percent[tov_YoungForest] = 0.1;
	g_weed_percent[tov_Wasteland] = 0.1;
	g_weed_percent[tov_WaterBufferZone] = 0.1;

	g_weed_percent[tov_PLWinterWheat] = 0.1;
	g_weed_percent[tov_PLWinterRape] = 0.05;
	g_weed_percent[tov_PLWinterBarley] = 0.1;
	g_weed_percent[tov_PLWinterTriticale] = 0.1;
	g_weed_percent[tov_PLWinterRye] = 0.1;
	g_weed_percent[tov_PLSpringWheat] = 0.1;
	g_weed_percent[tov_PLSpringBarley] = 0.1;
	g_weed_percent[tov_PLMaize] = 0.1;
	g_weed_percent[tov_PLMaizeSilage] = 0.1;
	g_weed_percent[tov_PLPotatoes] = 0.1;
	g_weed_percent[tov_PLBeet] = 0.1;
	g_weed_percent[tov_PLFodderLucerne1] = 0.1;
	g_weed_percent[tov_PLFodderLucerne2] = 0.1;
	g_weed_percent[tov_PLCarrots] = 0.1;
	g_weed_percent[tov_PLSpringBarleySpr] = 0.1;
	g_weed_percent[tov_PLWinterWheatLate] = 0.1;
	g_weed_percent[tov_PLBeetSpr] = 0.1;
	g_weed_percent[tov_PLBeans] = 0.1;

	g_weed_percent[tov_NLWinterWheat] = 0.1;
	g_weed_percent[tov_NLSpringBarley] = 0.1;
	g_weed_percent[tov_NLMaize] = 0.1;
	g_weed_percent[tov_NLPotatoes] = 0.1;
	g_weed_percent[tov_NLBeet] = 0.1;
	g_weed_percent[tov_NLCarrots] = 0.1;
	g_weed_percent[tov_NLCabbage] = 0.1;
	g_weed_percent[tov_NLTulips] = 0.1;
	g_weed_percent[tov_NLGrassGrazed1] = 0.1;
	g_weed_percent[tov_NLGrassGrazed1Spring] = 0.1;
	g_weed_percent[tov_NLGrassGrazed2] = 0.1;
	g_weed_percent[tov_NLGrassGrazedLast] = 0.1;
	g_weed_percent[tov_NLPermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_NLSpringBarleySpring] = 0.1;
	g_weed_percent[tov_NLMaizeSpring] = 0.1;
	g_weed_percent[tov_NLPotatoesSpring] = 0.1;
	g_weed_percent[tov_NLBeetSpring] = 0.1;
	g_weed_percent[tov_NLCarrotsSpring] = 0.1;
	g_weed_percent[tov_NLCabbageSpring] = 0.1;
	g_weed_percent[tov_NLCatchCropPea] = 0.1;
	g_weed_percent[tov_NLOrchardCrop] = 0.1;
	g_weed_percent[tov_NLPermanentGrassGrazedExtensive] = 0.1;
	g_weed_percent[tov_NLGrassGrazedExtensive1] = 0.1;
	g_weed_percent[tov_NLGrassGrazedExtensive1Spring] = 0.1;
	g_weed_percent[tov_NLGrassGrazedExtensive2] = 0.1;
	g_weed_percent[tov_NLGrassGrazedExtensiveLast] = 0.1;

	g_weed_percent[tov_UKBeans] = 0.1;
	g_weed_percent[tov_UKBeet] = 0.1;
	g_weed_percent[tov_UKMaize] = 0.1;
	g_weed_percent[tov_UKPermanentGrass] = 0.1;
	g_weed_percent[tov_UKPotatoes] = 0.1;
	g_weed_percent[tov_UKSpringBarley] = 0.1;
	g_weed_percent[tov_UKTempGrass] = 0.1;
	g_weed_percent[tov_UKWinterBarley] = 0.1;
	g_weed_percent[tov_UKWinterRape] = 0.1;
	g_weed_percent[tov_UKWinterWheat] = 0.1;

	g_weed_percent[tov_BEBeet] = 0.1;
	g_weed_percent[tov_BEBeetSpring] = 0.1;
	g_weed_percent[tov_BECatchPeaCrop] = 0.1;
	g_weed_percent[tov_BEGrassGrazed1] = 0.1;
	g_weed_percent[tov_BEGrassGrazed1Spring] = 0.1;
	g_weed_percent[tov_BEGrassGrazed2] = 0.1;
	g_weed_percent[tov_BEGrassGrazedLast] = 0.1;
	g_weed_percent[tov_BEMaize] = 0.1;
	g_weed_percent[tov_BEMaizeSpring] = 0.1;
	g_weed_percent[tov_BEOrchardCrop] = 0.1;
	g_weed_percent[tov_BEPotatoes] = 0.1;
	g_weed_percent[tov_BEPotatoesSpring] = 0.1;
	g_weed_percent[tov_BEWinterBarley] = 0.1;
	g_weed_percent[tov_BEWinterWheat] = 0.1;
	g_weed_percent[tov_BEWinterBarleyCC] = 0.1;
	g_weed_percent[tov_BEWinterWheatCC] = 0.1;
	g_weed_percent[tov_BEMaizeCC] = 0.1;

	g_weed_percent[tov_PTPermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_PTWinterWheat] = 0.1;
	g_weed_percent[tov_PTGrassGrazed] = 0.1;
	g_weed_percent[tov_PTSorghum] = 0.1;
	g_weed_percent[tov_PTFodderMix] = 0.1;
	g_weed_percent[tov_PTTurnipGrazed] = 0.1;
	g_weed_percent[tov_PTCloverGrassGrazed1] = 0.1;
	g_weed_percent[tov_PTCloverGrassGrazed2] = 0.1;
	g_weed_percent[tov_PTTriticale] = 0.1;
	g_weed_percent[tov_PTOtherDryBeans] = 0.1;
	g_weed_percent[tov_PTCorkOak] = 0.1;
	g_weed_percent[tov_PTVineyards] = 0.1;
	g_weed_percent[tov_PTWinterBarley] = 0.1;
	g_weed_percent[tov_PTBeans] = 0.1;
	g_weed_percent[tov_PTWinterRye] = 0.1;
	g_weed_percent[tov_PTRyegrass] = 0.1;
	g_weed_percent[tov_PTYellowLupin] = 0.1;
	g_weed_percent[tov_PTMaize] = 0.1;
	g_weed_percent[tov_PTOats] = 0.1;
	g_weed_percent[tov_PTPotatoes] = 0.1;
	g_weed_percent[tov_PTHorticulture] = 0.1;

	g_weed_percent[tov_DESugarBeet] = 0.1;
	g_weed_percent[tov_DECabbage] = 0.1;
	g_weed_percent[tov_DECarrots] = 0.1;
	g_weed_percent[tov_DEGrasslandSilageAnnual] = 0.1;
	g_weed_percent[tov_DEGreenFallow_1year] = 0.1;
	g_weed_percent[tov_DELegumes] = 0.1;
	g_weed_percent[tov_DEMaize] = 0.1;
	g_weed_percent[tov_DEMaizeSilage] = 0.1;
	g_weed_percent[tov_DEOats] = 0.1;
	g_weed_percent[tov_DEOCabbages] = 0.1;
	g_weed_percent[tov_DEOCarrots] = 0.1;
	g_weed_percent[tov_DEOGrasslandSilageAnnual] = 0.1;
	g_weed_percent[tov_DEOGreenFallow_1year] = 0.1;
	g_weed_percent[tov_DEOLegume] = 0.1;
	g_weed_percent[tov_DEOMaize] = 0.1;
	g_weed_percent[tov_DEOMaizeSilage] = 0.1;
	g_weed_percent[tov_DEOOats] = 0.1;
	g_weed_percent[tov_DEOPermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_DEOPotatoes] = 0.1;
	g_weed_percent[tov_DEOSpringRye] = 0.1;
	g_weed_percent[tov_DEOSugarBeet] = 0.1;
	g_weed_percent[tov_DEOTriticale] = 0.1;
	g_weed_percent[tov_DEOWinterBarley] = 0.1;
	g_weed_percent[tov_DEOWinterRape] = 0.1;
	g_weed_percent[tov_DEOWinterRye] = 0.1;
	g_weed_percent[tov_DEOWinterWheat] = 0.1;
	g_weed_percent[tov_DEPermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_DEPermanentGrassLowYield] = 0.1;
	g_weed_percent[tov_DEOPermanentGrassLowYield] = 0.1;
	g_weed_percent[tov_DEPotatoes] = 0.1;
	g_weed_percent[tov_DEPotatoesIndustry] = 0.1;
	g_weed_percent[tov_DESpringRye] = 0.1;
	g_weed_percent[tov_DETriticale] = 0.1;
	g_weed_percent[tov_DEWinterRye] = 0.1;
	g_weed_percent[tov_DEWinterBarley] = 0.1;
	g_weed_percent[tov_DEWinterRape] = 0.1;
	g_weed_percent[tov_DEWinterWheat] = 0.1;
	g_weed_percent[tov_DEWinterWheatLate] = 0.1;
	g_weed_percent[tov_DEAsparagusEstablishedPlantation] = 0.1;
	g_weed_percent[tov_DEOAsparagusEstablishedPlantation] = 0.1;
	g_weed_percent[tov_DEHerbsPerennial_1year] = 0.1;
	g_weed_percent[tov_DEHerbsPerennial_after1year] = 0.1;
	g_weed_percent[tov_DEOHerbsPerennial_1year] = 0.1;
	g_weed_percent[tov_DEOHerbsPerennial_after1year] = 0.1;
	g_weed_percent[tov_DESpringBarley] = 0.1;
	g_weed_percent[tov_DEOrchard] = 0.1;
	g_weed_percent[tov_DEOOrchard] = 0.1;
	g_weed_percent[tov_DEPeas] = 0.1;
	g_weed_percent[tov_DEOPeas] = 0.1;
	g_weed_percent[tov_DEBushFruitPerm] = 0.1;
	g_weed_percent[tov_DEOBushFruitPerm] = 0.1;

	g_weed_percent[tov_DKOLupines] = 0.1;
	g_weed_percent[tov_DKOLentils] = 0.1;
	g_weed_percent[tov_DKOLegume_Peas] = 0.1;
	g_weed_percent[tov_DKOLegume_Beans] = 0.1;
	g_weed_percent[tov_DKOLegume_Whole] = 0.1;
	g_weed_percent[tov_DKOLegume_Peas_CC] = 0.1;
	g_weed_percent[tov_DKOLegume_Beans_CC] = 0.1;
	g_weed_percent[tov_DKOLegume_Whole_CC] = 0.1;
	g_weed_percent[tov_DKCatchCrop] = 0.1;
	g_weed_percent[tov_DKOCatchCrop] = 0.1;
	g_weed_percent[tov_DKSugarBeets] = 0.1;
	g_weed_percent[tov_DKFodderBeets] = 0.1;
	g_weed_percent[tov_DKOFodderBeets] = 0.1;
	g_weed_percent[tov_DKOSugarBeets] = 0.1;
	g_weed_percent[tov_DKCabbages] = 0.1;
	g_weed_percent[tov_DKOCabbages] = 0.1;
	g_weed_percent[tov_DKCarrots] = 0.1;
	g_weed_percent[tov_DKOLegumeCloverGrass_Whole] = 0.1;
	g_weed_percent[tov_DKOCarrots] = 0.1;
	g_weed_percent[tov_DKLegume_Whole] = 0.1;
	g_weed_percent[tov_DKLegume_Peas] = 0.1;
	g_weed_percent[tov_DKLegume_Beans] = 0.1;
	g_weed_percent[tov_DKWinterWheat] = 0.1;
	g_weed_percent[tov_DKOWinterWheat] = 0.1;
	g_weed_percent[tov_DKWinterWheat_CC] = 0.1;
	g_weed_percent[tov_DKOWinterWheat_CC] = 0.1;
	g_weed_percent[tov_DKSpringBarley] = 0.1;
	g_weed_percent[tov_DKOSpringBarley] = 0.1;
	g_weed_percent[tov_DKSpringBarley_CC] = 0.1;
	g_weed_percent[tov_DKOSpringBarley_CC] = 0.1;
	g_weed_percent[tov_DKSpringBarleyCloverGrass] = 0.1;
	g_weed_percent[tov_DKOSpringBarleyCloverGrass] = 0.1;
	g_weed_percent[tov_DKCerealLegume] = 0.1;
	g_weed_percent[tov_DKOCerealLegume] = 0.1;
	g_weed_percent[tov_DKCerealLegume_Whole] = 0.1;
	g_weed_percent[tov_DKOCerealLegume_Whole] = 0.1;
	g_weed_percent[tov_DKOCloverGrassGrazed2] = 0.1;
	g_weed_percent[tov_DKOCloverGrassGrazed1] = 0.1;
	g_weed_percent[tov_DKOWinterCloverGrassGrazedSown] = 0.1;
	g_weed_percent[tov_DKCloverGrassGrazed2] = 0.1;
	g_weed_percent[tov_DKCloverGrassGrazed1] = 0.1;
	g_weed_percent[tov_DKWinterCloverGrassGrazedSown] = 0.1;
	g_weed_percent[tov_DKSpringFodderGrass] = 0.1;
	g_weed_percent[tov_DKWinterFodderGrass] = 0.1;
	g_weed_percent[tov_DKOSpringFodderGrass] = 0.1;
	g_weed_percent[tov_DKOWinterFodderGrass] = 0.1;
	g_weed_percent[tov_DKGrazingPigs] = 0.1;
	g_weed_percent[tov_DKMaize] = 0.05;
	g_weed_percent[tov_DKMaizeSilage] = 0.05;
	g_weed_percent[tov_DKMixedVeg] = 0.1;
	g_weed_percent[tov_DKOGrazingPigs] = 0.1;
	g_weed_percent[tov_DKOMaize] = 0.05;
	g_weed_percent[tov_DKOMaizeSilage] = 0.05;
	g_weed_percent[tov_DKOMixedVeg] = 0.1;
	g_weed_percent[tov_DKOPotato] = 0.1;
	g_weed_percent[tov_DKOPotatoIndustry] = 0.1;
	g_weed_percent[tov_DKOPotatoSeed] = 0.1;
	g_weed_percent[tov_DKOSeedGrassRye_Spring] = 0.1;
	g_weed_percent[tov_DKOSetAside] = 0.1;
	g_weed_percent[tov_DKOSetAside_AnnualFlower] = 0.1;
	g_weed_percent[tov_DKOSetAside_PerennialFlower] = 0.1;
	g_weed_percent[tov_DKOSetAside_SummerMow] = 0.1;
	g_weed_percent[tov_DKOSpringBarleySilage] = 0.1;
	g_weed_percent[tov_DKOSpringOats] = 0.1;
	g_weed_percent[tov_DKOSpringOats_CC] = 0.1;
	g_weed_percent[tov_DKOSpringWheat] = 0.1;
	g_weed_percent[tov_DKOVegSeeds] = 0.1;
	g_weed_percent[tov_DKOWinterBarley] = 0.1;
	g_weed_percent[tov_DKOWinterRape] = 0.05;
	g_weed_percent[tov_DKOWinterRye] = 0.1;
	g_weed_percent[tov_DKOWinterRye_CC] = 0.1;
	g_weed_percent[tov_DKPotato] = 0.1;
	g_weed_percent[tov_DKPotatoIndustry] = 0.1;
	g_weed_percent[tov_DKPotatoSeed] = 0.1;
	g_weed_percent[tov_DKSeedGrassFescue_Spring] = 0.1;
	g_weed_percent[tov_DKSeedGrassRye_Spring] = 0.1;
	g_weed_percent[tov_DKSetAside] = 0.1;
	g_weed_percent[tov_DKSetAside_SummerMow] = 0.1;
	g_weed_percent[tov_DKSpringBarley_Green] = 0.1;
	g_weed_percent[tov_DKSpringBarleySilage] = 0.1;
	g_weed_percent[tov_DKSpringOats] = 0.1;
	g_weed_percent[tov_DKSpringOats_CC] = 0.1;
	g_weed_percent[tov_DKSpringWheat] = 0.1;
	g_weed_percent[tov_DKUndefined] = 0.1;
	g_weed_percent[tov_DKVegSeeds] = 0.1;
	g_weed_percent[tov_DKWinterBarley] = 0.1;
	g_weed_percent[tov_DKWinterRape] = 0.05;
	g_weed_percent[tov_DKWinterRye] = 0.1;
	g_weed_percent[tov_DKWinterRye_CC] = 0.1;
	g_weed_percent[tov_DKOOrchardCrop_Perm] = 0.1;
	g_weed_percent[tov_DKOrchardCrop_Perm] = 0.1;
	g_weed_percent[tov_DKOrchApple] = 0.1;
	g_weed_percent[tov_DKOrchPear] = 0.1;
	g_weed_percent[tov_DKOrchCherry] = 0.1;
	g_weed_percent[tov_DKOrchOther] = 0.1;
	g_weed_percent[tov_DKOOrchApple] = 0.1;
	g_weed_percent[tov_DKOOrchPear] = 0.1;
	g_weed_percent[tov_DKOOrchCherry] = 0.1;
	g_weed_percent[tov_DKOOrchOther] = 0.1;
	g_weed_percent[tov_DKBushFruit_Perm1] = 0.1;
	g_weed_percent[tov_DKBushFruit_Perm2] = 0.1;
	g_weed_percent[tov_DKOBushFruit_Perm1] = 0.1;
	g_weed_percent[tov_DKOBushFruit_Perm2] = 0.1;
	g_weed_percent[tov_DKChristmasTrees_Perm] = 0.1;
	g_weed_percent[tov_DKOChristmasTrees_Perm] = 0.1;
	g_weed_percent[tov_DKEnergyCrop_Perm] = 0.1;
	g_weed_percent[tov_DKOEnergyCrop_Perm] = 0.1;
	g_weed_percent[tov_DKFarmForest_Perm] = 0.1;
	g_weed_percent[tov_DKOFarmForest_Perm] = 0.1;
	g_weed_percent[tov_DKGrazingPigs_Perm] = 0.1;
	g_weed_percent[tov_DKOGrazingPigs_Perm] = 0.1;
	g_weed_percent[tov_DKOGrassGrazed_Perm] = 0.1;
	g_weed_percent[tov_DKOGrassLowYield_Perm] = 0.1;
	g_weed_percent[tov_DKFarmYoungForest_Perm] = 0.1;
	g_weed_percent[tov_DKOFarmYoungForest_Perm] = 0.1;
	g_weed_percent[tov_DKPlantNursery_Perm] = 0.1;
	g_weed_percent[tov_DKGrassGrazed_Perm] = 0.1;
	g_weed_percent[tov_DKGrassLowYield_Perm] = 0.1;
	g_weed_percent[tov_DKCatchCrop] = 0.1;

	g_weed_percent[tov_FIWinterWheat] = 0.1;
	g_weed_percent[tov_FIOWinterWheat] = 0.1;
	g_weed_percent[tov_FISugarBeet] = 0.1;
	g_weed_percent[tov_FIStarchPotato_North] = 0.1;
	g_weed_percent[tov_FIStarchPotato_South] = 0.1;
	g_weed_percent[tov_FIOStarchPotato_North] = 0.1;
	g_weed_percent[tov_FIOStarchPotato_South] = 0.1;
	g_weed_percent[tov_FISpringWheat] = 0.1;
	g_weed_percent[tov_FIOSpringWheat] = 0.1;
	g_weed_percent[tov_FITurnipRape] = 0.05;
	g_weed_percent[tov_FIOTurnipRape] = 0.05;
	g_weed_percent[tov_FISpringRape] = 0.05;
	g_weed_percent[tov_FIOSpringRape] = 0.05;
	g_weed_percent[tov_FIWinterRye] = 0.1;
	g_weed_percent[tov_FIOWinterRye] = 0.1;
	g_weed_percent[tov_FIPotato_North] = 0.1;
	g_weed_percent[tov_FIPotato_South] = 0.1;
	g_weed_percent[tov_FIOPotato_North] = 0.1;
	g_weed_percent[tov_FIOPotato_South] = 0.1;
	g_weed_percent[tov_FIPotatoIndustry_North] = 0.1;
	g_weed_percent[tov_FIPotatoIndustry_South] = 0.1;
	g_weed_percent[tov_FIOPotatoIndustry_North] = 0.1;
	g_weed_percent[tov_FIOPotatoIndustry_South] = 0.1;
	g_weed_percent[tov_FISpringOats] = 0.1;
	g_weed_percent[tov_FIOSpringOats] = 0.1;
	g_weed_percent[tov_FISpringBarley_Malt] = 0.1;
	g_weed_percent[tov_FIOSpringBarley_Malt] = 0.1;
	g_weed_percent[tov_FIFabaBean] = 0.1;
	g_weed_percent[tov_FIOFabaBean] = 0.1;
	g_weed_percent[tov_FISpringBarley_Fodder] = 0.1;
	g_weed_percent[tov_FISprSpringBarley_Fodder] = 0.1;
	g_weed_percent[tov_FIOSpringBarley_Fodder] = 0.1;
	g_weed_percent[tov_FIGrasslandPasturePerennial1] = 0.1;
	g_weed_percent[tov_FIGrasslandPasturePerennial2] = 0.1;
	g_weed_percent[tov_FIGrasslandSilagePerennial1] = 0.1;
	g_weed_percent[tov_FIGrasslandSilagePerennial2] = 0.1;
	g_weed_percent[tov_FINaturalGrassland] = 0.1; 
	g_weed_percent[tov_FIFeedingGround] = 0.1;
	g_weed_percent[tov_FIGreenFallow_1year] = 0.1;
	g_weed_percent[tov_FIBufferZone] = 0.1;
	g_weed_percent[tov_FIGrasslandSilageAnnual] = 0.1;
	g_weed_percent[tov_FICaraway1] = 0.1;
	g_weed_percent[tov_FICaraway2] = 0.1;
	g_weed_percent[tov_FIOCaraway1] = 0.1;
	g_weed_percent[tov_FIOCaraway2] = 0.1;
	g_weed_percent[tov_FINaturalGrassland_Perm] = 0.1; 
	g_weed_percent[tov_FIGreenFallow_Perm] = 0.1;
	g_weed_percent[tov_FIBufferZone_Perm] = 0.1;

	g_weed_percent[tov_SESpringBarley] = 0.1;
	g_weed_percent[tov_SEWinterRape_Seed] = 0.05;
	g_weed_percent[tov_SEWinterWheat] = 0.1;

	g_weed_percent[tov_IRSpringWheat] = 0.1;
	g_weed_percent[tov_IRSpringBarley] = 0.1;
	g_weed_percent[tov_IRSpringOats] = 0.1;
	g_weed_percent[tov_IRGrassland_no_reseed] = 0.1;
	g_weed_percent[tov_IRGrassland_reseed] = 0.1;
	g_weed_percent[tov_IRWinterWheat] = 0.1;
	g_weed_percent[tov_IRWinterBarley] = 0.1;
	g_weed_percent[tov_IRWinterOats] = 0.1;
	g_weed_percent[tov_FRWinterWheat] = 0.1;
	g_weed_percent[tov_FRWinterBarley] = 0.1;
	g_weed_percent[tov_FRWinterTriticale] = 0.1;
	g_weed_percent[tov_FRWinterRape] = 0.05;
	g_weed_percent[tov_FRMaize] = 0.05;
	g_weed_percent[tov_FRMaize_Silage] = 0.05;
	g_weed_percent[tov_FRSpringBarley] = 0.1;
	g_weed_percent[tov_FRGrassland] = 0.1;
	g_weed_percent[tov_FRGrassland_Perm] = 0.1;
	g_weed_percent[tov_FRSpringOats] = 0.1;
	g_weed_percent[tov_FRSunflower] = 0.05;
	g_weed_percent[tov_FRSpringWheat] = 0.1;
	g_weed_percent[tov_FRPotatoes] = 0.1;
	g_weed_percent[tov_FRSorghum] = 0.05;

	g_weed_percent[tov_ITGrassland] = 0.1;
	g_weed_percent[tov_ITOrchard] = 0.1;
	g_weed_percent[tov_ITOOrchard] = 0.1;

	g_bug_percent_a[tov_Carrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_MaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OMaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_Maize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_Potatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FodderGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_CloverGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_CloverGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OCloverGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OCloverGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_SpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleySpr] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleyPTreatment] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleySKManagement] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_WinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterWheatShort] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_SpringBarleySilage] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleySeed] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OGrazingPigs] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OCloverGrassSilage1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_SpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OSpringBarleyPigs] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OBarleyPeaCloverGrass] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OSpringBarleyExt] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OSBarleySilage] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[ tov_OWinterWheat ] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[ tov_OWinterWheatUndersown ] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[ tov_OWinterWheatUndersownExt ] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_SpringRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_OWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_OWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_OWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OWinterBarleyExt] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_WinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_WinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_OFieldPeas] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OFieldPeasSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OrchardCrop] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_Oats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[ tov_BroadBeans ] = EL_BUG_PERCENT_A;
	g_bug_percent_a[ tov_FieldPeas ] = EL_BUG_PERCENT_A;
	g_bug_percent_a[ tov_FieldPeasSilage ] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OSeedGrass1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OSeedGrass2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_SeedGrass1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_SeedGrass2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OPermanentGrassGrazed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_Heath] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_b[tov_Heath] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_c[tov_Heath] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_d[tov_Heath] = EL_BUG_PERCENT_D;
	g_bug_percent_a[tov_SetAside] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_OSetAside] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_PermanentSetAside] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_PermanentGrassLowYield] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PermanentGrassGrazed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PermanentGrassTussocky] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FodderBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_SugarBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OFodderBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NaturalGrass] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_None] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NoGrowth] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OFirstYearDanger] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_Triticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OTriticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_WWheatPControl] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WWheatPToxicControl] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WWheatPTreatment] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_AgroChemIndustryCereal] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterWheatStrigling] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterWheatStriglingCulm] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterWheatStriglingSingle] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_SpringBarleyCloverGrassStrigling] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleyStrigling] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleyStriglingSingle] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleyStriglingCulm] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_MaizeStrigling] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_WinterRapeStrigling] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_WinterRyeStrigling] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_WinterBarleyStrigling] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FieldPeasStrigling] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_SpringBarleyPeaCloverGrassStrigling] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_YoungForest] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_Wasteland] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_WaterBufferZone] = EL_BUG_PERCENT_Edges_A;

	g_bug_percent_a[tov_PLWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_PLWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_PLWinterTriticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_PLWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_PLSpringWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLMaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLFodderLucerne1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PLFodderLucerne2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PLCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLSpringBarleySpr] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_PLWinterWheatLate] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_PLBeetSpr] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLBeans] = EL_BUG_PERCENT_A;

	g_bug_percent_a[tov_NLWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_NLSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_NLMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCabbage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLTulips] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazed1Spring] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazedLast] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLPermanentGrassGrazed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLSpringBarleySpring] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_NLMaizeSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLPotatoesSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLBeetSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCarrotsSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCabbageSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCatchCropPea] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLOrchardCrop] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLPermanentGrassGrazedExtensive] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazedExtensive1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazedExtensive1Spring] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazedExtensive2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazedExtensiveLast] = EL_BUG_PERCENT_G_A;

	g_bug_percent_a[tov_UKBeans] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKPermanentGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_UKPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_UKTempGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_UKWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_UKWinterWheat] = EL_BUG_PERCENT_WW_A;

	g_bug_percent_a[tov_BEBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEBeetSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BECatchPeaCrop] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_BEGrassGrazed1Spring] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_BEGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_BEGrassGrazedLast] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_BEMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEMaizeCC] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEMaizeSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEOrchardCrop] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_BEPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEPotatoesSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEWinterBarleyCC] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_BEWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_BEWinterWheatCC] = EL_BUG_PERCENT_WW_A;

	g_bug_percent_a[tov_PTPermanentGrassGrazed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_PTGrassGrazed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTSorghum] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PTFodderMix] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTTurnipGrazed] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PTCloverGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTCloverGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTTriticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PTOtherDryBeans] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PTShrubPastures] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTCorkOak] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTVineyards] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PTBeans] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_PTRyegrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTYellowLupin] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PTMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PTOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PTPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PTHorticulture] = EL_BUG_PERCENT_A;

	g_bug_percent_a[tov_DESugarBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DECabbage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DECarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEGrasslandSilageAnnual] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEGreenFallow_1year] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DELegumes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEMaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOCabbages] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOGrasslandSilageAnnual] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOGreenFallow_1year] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOLegume] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOMaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOPermanentGrassGrazed] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOSpringRye] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOSugarBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOTriticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_DEOWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_DEOWinterWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEPermanentGrassGrazed] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEPermanentGrassLowYield] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOPermanentGrassLowYield] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEPotatoesIndustry] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DESpringRye] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DETriticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_DEWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_DEWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_DEWinterWheatLate] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_DEAsparagusEstablishedPlantation] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOAsparagusEstablishedPlantation] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEHerbsPerennial_1year] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEHerbsPerennial_after1year] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOHerbsPerennial_1year] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOHerbsPerennial_after1year] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DESpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DEOrchard] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DEOOrchard] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DEPeas] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOPeas] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEBushFruitPerm] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DEOBushFruitPerm] = EL_BUG_PERCENT_A;

	g_bug_percent_a[tov_DKCatchCrop] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOCatchCrop] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLupines] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLentils] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLegume_Peas] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLegume_Beans] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLegume_Whole] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLegume_Peas_CC] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLegume_Beans_CC] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLegume_Whole_CC] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKSugarBeets] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKFodderBeets] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOFodderBeets] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOSugarBeets] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKCabbages] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOCabbages] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOLegumeCloverGrass_Whole] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKLegume_Whole] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKLegume_Peas] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKLegume_Beans] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_DKOWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_DKWinterWheat_CC] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_DKOWinterWheat_CC] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_DKSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKOSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKSpringBarley_CC] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKOSpringBarley_CC] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKSpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKOSpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKCerealLegume] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKOCerealLegume] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKCerealLegume_Whole] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKOCerealLegume_Whole] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKOCloverGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOCloverGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOWinterCloverGrassGrazedSown] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKCloverGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKCloverGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKWinterCloverGrassGrazedSown] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKSpringFodderGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKWinterFodderGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOSpringFodderGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOWinterFodderGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKGrazingPigs] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKMaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKMixedVeg] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOGrazingPigs] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOMaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOMixedVeg] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOPotato] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOPotatoIndustry] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOPotatoSeed] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOSeedGrassRye_Spring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOSetAside] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKOSetAside_AnnualFlower] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKOSetAside_PerennialFlower] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKOSetAside_SummerMow] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKOSpringBarleySilage] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKOSpringOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOSpringOats_CC] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOSpringWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOVegSeeds] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_DKOWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_DKOWinterRye_CC] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_DKPotato] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKPotatoIndustry] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKPotatoSeed] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKSeedGrassFescue_Spring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKSeedGrassRye_Spring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKSetAside] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKSetAside_SummerMow] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKSpringBarley_Green] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKSpringBarleySilage] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_DKSpringOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKSpringOats_CC] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKSpringWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKUndefined] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKVegSeeds] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_DKWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_DKWinterRye_CC] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_DKOOrchardCrop_Perm] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOrchardCrop_Perm] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOrchApple] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOrchPear] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOrchCherry] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOrchOther] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOOrchApple] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOOrchPear] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOOrchCherry] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOOrchOther] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKBushFruit_Perm1] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKBushFruit_Perm2] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOBushFruit_Perm1] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKOBushFruit_Perm2] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKChristmasTrees_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKOChristmasTrees_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKEnergyCrop_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKOEnergyCrop_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKFarmForest_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKOFarmForest_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKGrazingPigs_Perm] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOGrazingPigs_Perm] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOGrassGrazed_Perm] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKOGrassLowYield_Perm] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKFarmYoungForest_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKOFarmYoungForest_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_DKPlantNursery_Perm] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_DKGrassGrazed_Perm] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_DKGrassLowYield_Perm] = EL_BUG_PERCENT_G_A;

	g_bug_percent_a[tov_UKBeans] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKPermanentGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_UKPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_UKTempGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_UKWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_UKWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_UKWinterWheat] = EL_BUG_PERCENT_WW_A;

	g_bug_percent_a[tov_FIWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_FIOWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_FISugarBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIStarchPotato_North] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIStarchPotato_South] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOStarchPotato_North] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOStarchPotato_South] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FISpringWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOSpringWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FITurnipRape] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOTurnipRape] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FISpringRape] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOSpringRape] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_FIOWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_FIPotato_North] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIPotato_South] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOPotato_North] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOPotato_South] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIPotatoIndustry_North] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIPotatoIndustry_South] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOPotatoIndustry_North] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOPotatoIndustry_South] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FISpringOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOSpringOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FISpringBarley_Malt] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_FIOSpringBarley_Malt] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_FIFabaBean] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOFabaBean] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FISpringBarley_Fodder] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_FISprSpringBarley_Fodder] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_FIOSpringBarley_Fodder] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_FIGrasslandPasturePerennial1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FIGrasslandPasturePerennial2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FIGrasslandSilagePerennial1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FIGrasslandSilagePerennial2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FINaturalGrassland] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_FIFeedingGround] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FIGreenFallow_1year] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIBufferZone] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_FIGrasslandSilageAnnual] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FICaraway1] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FICaraway2] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOCaraway1] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIOCaraway2] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FINaturalGrassland_Perm] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_FIGreenFallow_Perm] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FIBufferZone_Perm] = EL_BUG_PERCENT_Edges_A;

	g_bug_percent_a[tov_SESpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SEWinterRape_Seed] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_SEWinterWheat] = EL_BUG_PERCENT_WW_A;

	g_bug_percent_a[tov_IRSpringWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_IRSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_IRSpringOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_IRGrassland_no_reseed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_IRGrassland_reseed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_IRWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_IRWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_IRWinterOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_FRWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRWinterTriticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_FRMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRMaize_Silage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_FRGrassland] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FRGrassland_Perm] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FRSpringOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRSunflower] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRSpringWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FRSorghum] = EL_BUG_PERCENT_A;

	g_bug_percent_a[tov_ITGrassland] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_ITOrchard] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_ITOOrchard] = EL_BUG_PERCENT_G_A;

	g_bug_percent_b[tov_Carrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_MaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OMaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_Maize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_Potatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FodderGrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_CloverGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_CloverGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OCloverGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OCloverGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_SpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleySpr] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleyPTreatment] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleySKManagement] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_WinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterWheatShort] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_SpringBarleySilage] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleySeed] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OGrazingPigs] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OCloverGrassSilage1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_SpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OSpringBarleyPigs] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OBarleyPeaCloverGrass] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OSBarleySilage] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OSpringBarleyExt] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[ tov_OWinterWheat ] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[ tov_OWinterWheatUndersown ] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[ tov_OWinterWheatUndersownExt ] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_SpringRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_OWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_OWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_OWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OWinterBarleyExt] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_WinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_WinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_OFieldPeas] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OFieldPeasSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OrchardCrop] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_Oats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[ tov_BroadBeans ] = EL_BUG_PERCENT_B;
	g_bug_percent_b[ tov_FieldPeas ] = EL_BUG_PERCENT_B;
	g_bug_percent_b[ tov_FieldPeasSilage ] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_SeedGrass1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_SeedGrass2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OSeedGrass1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OSeedGrass2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OPermanentGrassGrazed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_SetAside] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_OSetAside] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_PermanentSetAside] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_PermanentGrassGrazed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PermanentGrassLowYield] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PermanentGrassTussocky] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FodderBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_SugarBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OFodderBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NaturalGrass] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_None] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NoGrowth] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OFirstYearDanger] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_Triticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OTriticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_WWheatPControl] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WWheatPToxicControl] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WWheatPTreatment] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_AgroChemIndustryCereal] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterWheatStrigling] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterWheatStriglingCulm] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterWheatStriglingSingle] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_SpringBarleyCloverGrassStrigling] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleyStrigling] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleyStriglingSingle] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleyStriglingCulm] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_MaizeStrigling] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_WinterRapeStrigling] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_WinterRyeStrigling] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_WinterBarleyStrigling] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FieldPeasStrigling] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_SpringBarleyPeaCloverGrassStrigling] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_YoungForest] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_Wasteland] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_WaterBufferZone] = EL_BUG_PERCENT_Edges_B;

	g_bug_percent_b[tov_PLWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_PLWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_PLWinterTriticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_PLWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_PLSpringWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLMaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLFodderLucerne1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PLFodderLucerne2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PLCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLSpringBarleySpr] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_PLWinterWheatLate] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_PLBeetSpr] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLBeans] = EL_BUG_PERCENT_B;

	g_bug_percent_b[tov_NLWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_NLSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_NLMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCabbage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLTulips] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazed1Spring] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazedLast] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLPermanentGrassGrazed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLSpringBarleySpring] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_NLMaizeSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLPotatoesSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLBeetSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCarrotsSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCabbageSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCatchCropPea] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLOrchardCrop] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLPermanentGrassGrazedExtensive] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazedExtensive1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazedExtensive1Spring] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazedExtensive2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazedExtensiveLast] = EL_BUG_PERCENT_G_B;

	g_bug_percent_b[tov_UKBeans] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_UKBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_UKMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_UKPermanentGrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_UKPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_UKSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_UKTempGrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_UKWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_UKWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_UKWinterWheat] = EL_BUG_PERCENT_WW_B;

	g_bug_percent_b[tov_BEBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEBeetSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BECatchPeaCrop] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_BEGrassGrazed1Spring] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_BEGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_BEGrassGrazedLast] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_BEMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEMaizeCC] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEMaizeSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEOrchardCrop] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_BEPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEPotatoesSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEWinterBarleyCC] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_BEWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_BEWinterWheatCC] = EL_BUG_PERCENT_WW_B;

	g_bug_percent_b[tov_PTPermanentGrassGrazed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_PTGrassGrazed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTSorghum] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PTFodderMix] = EL_BUG_PERCENT_G_B;
	g_bug_percent_a[tov_PTTurnipGrazed] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PTCloverGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTCloverGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTTriticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PTOtherDryBeans] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PTShrubPastures] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTCorkOak] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTVineyards] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTWinterBarley] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTBeans] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTWinterRye] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTRyegrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTYellowLupin] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PTMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PTOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PTPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PTHorticulture] = EL_BUG_PERCENT_B;

	g_bug_percent_b[tov_DESugarBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DECabbage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DECarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEGrasslandSilageAnnual] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEGreenFallow_1year] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DELegumes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEMaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOCabbages] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOGrasslandSilageAnnual] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOGreenFallow_1year] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOLegume] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOMaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOPermanentGrassGrazed] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOSpringRye] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOSugarBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOTriticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_DEOWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_DEOWinterWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEPermanentGrassGrazed] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEPermanentGrassLowYield] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOPermanentGrassLowYield] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEPotatoesIndustry] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DESpringRye] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DETriticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_DEWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_DEWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_DEWinterWheatLate] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_DEAsparagusEstablishedPlantation] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOAsparagusEstablishedPlantation] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEHerbsPerennial_1year] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEHerbsPerennial_after1year] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOHerbsPerennial_1year] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOHerbsPerennial_after1year] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DESpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DEOrchard] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DEOOrchard] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DEPeas] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOPeas] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEBushFruitPerm] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DEOBushFruitPerm] = EL_BUG_PERCENT_B;

	g_bug_percent_b[tov_DKCatchCrop] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOCatchCrop] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLupines] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLentils] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLegume_Peas] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLegume_Beans] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLegume_Whole] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLegume_Peas_CC] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLegume_Beans_CC] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLegume_Whole_CC] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKSugarBeets] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKFodderBeets] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOFodderBeets] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOSugarBeets] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKCabbages] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOCabbages] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOLegumeCloverGrass_Whole] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKLegume_Whole] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKLegume_Peas] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKLegume_Beans] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_DKOWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_DKWinterWheat_CC] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_DKOWinterWheat_CC] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_DKSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKOSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKSpringBarley_CC] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKOSpringBarley_CC] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKSpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKOSpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKCerealLegume] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKOCerealLegume] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKCerealLegume_Whole] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKOCerealLegume_Whole] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKOCloverGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOCloverGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOWinterCloverGrassGrazedSown] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKCloverGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKCloverGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKWinterCloverGrassGrazedSown] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKSpringFodderGrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKWinterFodderGrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOSpringFodderGrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOWinterFodderGrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKGrazingPigs] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKMaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKMixedVeg] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOGrazingPigs] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOMaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOMixedVeg] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOPotato] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOPotatoIndustry] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOPotatoSeed] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOSeedGrassRye_Spring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOSetAside] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKOSetAside_AnnualFlower] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKOSetAside_PerennialFlower] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKOSetAside_SummerMow] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKOSpringBarleySilage] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKOSpringOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOSpringOats_CC] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOSpringWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOVegSeeds] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_DKOWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_DKOWinterRye_CC] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_DKPotato] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKPotatoIndustry] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKPotatoSeed] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKSeedGrassFescue_Spring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKSeedGrassRye_Spring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKSetAside] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKSetAside_SummerMow] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKSpringBarley_Green] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKSpringBarleySilage] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_DKSpringOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKSpringOats_CC] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKSpringWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKUndefined] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKVegSeeds] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_DKWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_DKWinterRye_CC] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_DKOOrchardCrop_Perm] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOrchardCrop_Perm] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOrchApple] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOrchPear] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOrchCherry] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOrchOther] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOOrchApple] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOOrchPear] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOOrchCherry] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOOrchOther] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKBushFruit_Perm1] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKBushFruit_Perm2] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOBushFruit_Perm1] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKOBushFruit_Perm2] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKChristmasTrees_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKOChristmasTrees_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKEnergyCrop_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKOEnergyCrop_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKFarmForest_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKOFarmForest_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKGrazingPigs_Perm] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOGrazingPigs_Perm] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOGrassGrazed_Perm] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKOGrassLowYield_Perm] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKFarmYoungForest_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKOFarmYoungForest_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_DKPlantNursery_Perm] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_DKGrassGrazed_Perm] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_DKGrassLowYield_Perm] = EL_BUG_PERCENT_G_B;

	g_bug_percent_b[tov_FIWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_FIOWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_FISugarBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIStarchPotato_North] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIStarchPotato_South] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOStarchPotato_North] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOStarchPotato_South] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FISpringWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOSpringWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FITurnipRape] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOTurnipRape] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FISpringRape] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOSpringRape] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_FIOWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_FIPotato_North] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIPotato_South] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOPotato_North] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOPotato_South] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIPotatoIndustry_North] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIPotatoIndustry_South] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOPotatoIndustry_North] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOPotatoIndustry_South] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FISpringOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOSpringOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FISpringBarley_Malt] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_FIOSpringBarley_Malt] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_FIFabaBean] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOFabaBean] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FISpringBarley_Fodder] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_FISprSpringBarley_Fodder] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_FIOSpringBarley_Fodder] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_FIGrasslandPasturePerennial1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FIGrasslandPasturePerennial2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FIGrasslandSilagePerennial1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FIGrasslandSilagePerennial2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FINaturalGrassland] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_FIFeedingGround] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FIGreenFallow_1year] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIBufferZone] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_FIGrasslandSilageAnnual] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FICaraway1] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FICaraway2] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOCaraway1] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIOCaraway2] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FINaturalGrassland_Perm] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_FIGreenFallow_Perm] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FIBufferZone_Perm] = EL_BUG_PERCENT_Edges_B;

	g_bug_percent_b[tov_SESpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SEWinterRape_Seed] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_SEWinterWheat] = EL_BUG_PERCENT_WW_B;

	g_bug_percent_b[tov_IRSpringWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_IRSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_IRSpringOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_IRGrassland_no_reseed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_IRGrassland_reseed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_IRWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_IRWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_IRWinterOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_FRWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRWinterTriticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_FRMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRMaize_Silage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_FRGrassland] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FRGrassland_Perm] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FRSpringOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRSunflower] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRSpringWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FRSorghum] = EL_BUG_PERCENT_B;

	g_bug_percent_b[tov_ITGrassland] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_ITOrchard] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_ITOOrchard] = EL_BUG_PERCENT_G_B;

	g_bug_percent_c[tov_Carrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_MaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OMaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_Maize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_Potatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FodderGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_CloverGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_CloverGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OCloverGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OCloverGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_SpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleySpr] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleyPTreatment] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleySKManagement] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_WinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterWheatShort] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_SpringBarleySilage] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleySeed] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OGrazingPigs] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OCloverGrassSilage1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_SpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OSpringBarleyPigs] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OBarleyPeaCloverGrass] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OSBarleySilage] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OSpringBarleyExt] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[ tov_OWinterWheat ] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[ tov_OWinterWheatUndersown ] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[ tov_OWinterWheatUndersownExt ] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_SpringRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_OWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_OWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_OWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OWinterBarleyExt] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_WinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_WinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_OFieldPeas] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OFieldPeasSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OrchardCrop] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_Oats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[ tov_FieldPeas ] = EL_BUG_PERCENT_C;
	g_bug_percent_c[ tov_BroadBeans ] = EL_BUG_PERCENT_C;
	g_bug_percent_c[ tov_FieldPeasSilage ] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_SeedGrass1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_SeedGrass2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OSeedGrass1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OSeedGrass2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OPermanentGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_SetAside] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_OSetAside] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_PermanentSetAside] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_PermanentGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PermanentGrassLowYield] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PermanentGrassTussocky] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FodderBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_SugarBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OFodderBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NaturalGrass] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_None] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NoGrowth] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OFirstYearDanger] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_Triticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OTriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_WWheatPControl] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WWheatPToxicControl] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WWheatPTreatment] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_AgroChemIndustryCereal] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterWheatStrigling] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterWheatStriglingCulm] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterWheatStriglingSingle] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_SpringBarleyCloverGrassStrigling] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleyStrigling] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleyStriglingSingle] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleyStriglingCulm] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_MaizeStrigling] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_WinterRapeStrigling] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_WinterRyeStrigling] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterBarleyStrigling] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FieldPeasStrigling] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_SpringBarleyPeaCloverGrassStrigling] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_YoungForest] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_Wasteland] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_WaterBufferZone] = EL_BUG_PERCENT_Edges_C;

	g_bug_percent_c[tov_PLWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_PLWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_PLWinterTriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_PLWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_PLSpringWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLMaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLFodderLucerne1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PLFodderLucerne2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PLCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLSpringBarleySpr] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_PLWinterWheatLate] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_PLBeetSpr] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLBeans] = EL_BUG_PERCENT_C;

	g_bug_percent_c[tov_NLWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_NLSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_NLMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCabbage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLTulips] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazed1Spring] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazedLast] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLPermanentGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLSpringBarleySpring] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_NLMaizeSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLPotatoesSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLBeetSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCarrotsSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCabbageSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCatchCropPea] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLOrchardCrop] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLPermanentGrassGrazedExtensive] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazedExtensive1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazedExtensive1Spring] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazedExtensive2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazedExtensiveLast] = EL_BUG_PERCENT_G_C;

	g_bug_percent_c[tov_UKBeans] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKPermanentGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_UKPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_UKTempGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_UKWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_UKWinterWheat] = EL_BUG_PERCENT_WW_C;

	g_bug_percent_c[tov_BEBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEBeetSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BECatchPeaCrop] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_BEGrassGrazed1Spring] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_BEGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_BEGrassGrazedLast] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_BEMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEMaizeCC] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEMaizeSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEOrchardCrop] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_BEPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEPotatoesSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEWinterBarleyCC] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_BEWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_BEWinterWheatCC] = EL_BUG_PERCENT_WW_C;

	g_bug_percent_c[tov_PTPermanentGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_PTGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTSorghum] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTFodderMix] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTTurnipGrazed] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTCloverGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTCloverGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTTriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTOtherDryBeans] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTShrubPastures] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTCorkOak] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTVineyards] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTWinterBarley] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTBeans] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTWinterRye] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTRyegrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTYellowLupin] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTHorticulture] = EL_BUG_PERCENT_C;

	g_bug_percent_c[tov_DESugarBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DECabbage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DECarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEGrasslandSilageAnnual] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEGreenFallow_1year] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DELegumes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEMaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOCabbages] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOGrasslandSilageAnnual] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOGreenFallow_1year] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOLegume] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOMaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOPermanentGrassGrazed] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOSpringRye] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOSugarBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOTriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_DEOWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_DEOWinterWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEPermanentGrassGrazed] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEPermanentGrassLowYield] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOPermanentGrassLowYield] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEPotatoesIndustry] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DESpringRye] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DETriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_DEWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_DEWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_DEWinterWheatLate] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_DEAsparagusEstablishedPlantation] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOAsparagusEstablishedPlantation] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEHerbsPerennial_1year] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEHerbsPerennial_after1year] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOHerbsPerennial_1year] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOHerbsPerennial_after1year] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DESpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DEOrchard] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DEOOrchard] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DEPeas] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOPeas] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEBushFruitPerm] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DEOBushFruitPerm] = EL_BUG_PERCENT_C;

	g_bug_percent_c[tov_DKCatchCrop] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOCatchCrop] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLupines] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLentils] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLegume_Peas] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLegume_Beans] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLegume_Whole] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLegume_Peas_CC] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLegume_Beans_CC] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLegume_Whole_CC] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKSugarBeets] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKFodderBeets] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOFodderBeets] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOSugarBeets] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKCabbages] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOCabbages] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOLegumeCloverGrass_Whole] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKLegume_Whole] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKLegume_Peas] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKLegume_Beans] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_DKOWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_DKSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKOSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKWinterWheat_CC] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_DKOWinterWheat_CC] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_DKSpringBarley_CC] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKOSpringBarley_CC] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKSpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKOSpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKCerealLegume] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKOCerealLegume] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKCerealLegume_Whole] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKOCerealLegume_Whole] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKOCloverGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOCloverGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOWinterCloverGrassGrazedSown] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKCloverGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKCloverGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKWinterCloverGrassGrazedSown] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKSpringFodderGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKWinterFodderGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOSpringFodderGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOWinterFodderGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKGrazingPigs] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKMaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKMixedVeg] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOGrazingPigs] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOMaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOMixedVeg] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOPotato] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOPotatoIndustry] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOPotatoSeed] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOSeedGrassRye_Spring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOSetAside] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKOSetAside_AnnualFlower] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKOSetAside_PerennialFlower] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKOSetAside_SummerMow] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKOSpringBarleySilage] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKOSpringOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKSpringOats_CC] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOSpringOats_CC] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOSpringWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOVegSeeds] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_DKOWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_DKWinterRye_CC] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_DKOWinterRye_CC] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_DKPotato] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKPotatoIndustry] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKPotatoSeed] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKSeedGrassFescue_Spring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKSeedGrassRye_Spring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKSetAside] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKSetAside_SummerMow] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKSpringBarley_Green] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKSpringBarleySilage] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_DKSpringOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKSpringWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKUndefined] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKVegSeeds] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_DKWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_DKOOrchardCrop_Perm] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOrchardCrop_Perm] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOrchApple] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOrchPear] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOrchCherry] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOrchOther] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOOrchApple] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOOrchPear] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOOrchCherry] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOOrchOther] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKBushFruit_Perm1] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKBushFruit_Perm2] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOBushFruit_Perm1] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKOBushFruit_Perm2] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKChristmasTrees_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKOChristmasTrees_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKEnergyCrop_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKOEnergyCrop_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKFarmForest_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKOFarmForest_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKGrazingPigs_Perm] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOGrazingPigs_Perm] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOGrassGrazed_Perm] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKOGrassLowYield_Perm] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKFarmYoungForest_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKOFarmYoungForest_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_DKPlantNursery_Perm] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_DKGrassGrazed_Perm] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_DKGrassLowYield_Perm] = EL_BUG_PERCENT_G_C;

	g_bug_percent_c[tov_FIWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_FIOWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_FISugarBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIStarchPotato_North] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIStarchPotato_South] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOStarchPotato_North] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOStarchPotato_South] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FISpringWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOSpringWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FITurnipRape] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOTurnipRape] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FISpringRape] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOSpringRape] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_FIOWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_FIPotato_North] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIPotato_South] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOPotato_North] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOPotato_South] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIPotatoIndustry_North] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIPotatoIndustry_South] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOPotatoIndustry_North] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOPotatoIndustry_South] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FISpringOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOSpringOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FISpringBarley_Malt] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_FIOSpringBarley_Malt] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_FIFabaBean] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOFabaBean] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FISpringBarley_Fodder] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_FISprSpringBarley_Fodder] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_FIOSpringBarley_Fodder] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_FIGrasslandPasturePerennial1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FIGrasslandPasturePerennial2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FIGrasslandSilagePerennial1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FIGrasslandSilagePerennial2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FINaturalGrassland] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_FIFeedingGround] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FIGreenFallow_1year] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIBufferZone] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_FIGrasslandSilageAnnual] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FICaraway1] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FICaraway2] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOCaraway1] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIOCaraway2] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FINaturalGrassland_Perm] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_FIGreenFallow_Perm] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FIBufferZone_Perm] = EL_BUG_PERCENT_Edges_C;

	g_bug_percent_c[tov_SESpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SEWinterRape_Seed] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_SEWinterWheat] = EL_BUG_PERCENT_WW_C;

	g_bug_percent_c[tov_IRSpringWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_IRSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_IRSpringOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_IRGrassland_no_reseed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_IRGrassland_reseed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_IRWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_IRWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_IRWinterOats] = EL_BUG_PERCENT_C;

	g_bug_percent_c[tov_FRWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_FRWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FRWinterTriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FRWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_FRMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FRMaize_Silage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FRSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_FRGrassland] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FRGrassland_Perm] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FRSpringOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FRSunflower] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FRSpringWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FRPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FRSorghum] = EL_BUG_PERCENT_C;

	g_bug_percent_c[tov_ITGrassland] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_ITOrchard] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_ITOOrchard] = EL_BUG_PERCENT_G_C;

	g_bug_percent_c[tov_UKBeans] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKPermanentGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_UKPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_UKTempGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_UKWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_UKWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_UKWinterWheat] = EL_BUG_PERCENT_WW_C;

	g_bug_percent_c[tov_PTPermanentGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_PTGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTSorghum] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTFodderMix] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTTurnipGrazed] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTCloverGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTCloverGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTTriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTOtherDryBeans] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTShrubPastures] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTCorkOak] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTVineyards] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTWinterBarley] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTBeans] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTWinterRye] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTRyegrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTYellowLupin] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PTMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PTHorticulture] = EL_BUG_PERCENT_C;

	g_bug_percent_d[tov_Carrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Maize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_MaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OMaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Potatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FodderGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_CloverGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_CloverGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OCloverGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OCloverGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleySpr] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyPTreatment] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleySKManagement] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheatShort] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleySilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleySeed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OGrazingPigs] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OCloverGrassSilage1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyCloverGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSpringBarleyPigs] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OBarleyPeaCloverGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSBarleySilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSpringBarleyExt] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_OWinterWheat ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_OWinterWheatUndersown ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_OWinterWheatUndersownExt ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OWinterBarleyExt] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OFieldPeas] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OFieldPeasSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OrchardCrop] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Oats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_BroadBeans ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_FieldPeas ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_FieldPeasSilage ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SeedGrass1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SeedGrass2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSeedGrass1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSeedGrass2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OPermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SetAside] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSetAside] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PermanentSetAside] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PermanentGrassLowYield] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PermanentGrassTussocky] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FodderBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SugarBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OFodderBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NaturalGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_None] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NoGrowth] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OFirstYearDanger] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Triticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OTriticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WWheatPControl] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WWheatPToxicControl] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WWheatPTreatment] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_AgroChemIndustryCereal] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheatStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheatStriglingCulm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheatStriglingSingle] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyCloverGrassStrigling] = -EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyStriglingSingle] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyStriglingCulm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_MaizeStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterRapeStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterRyeStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterBarleyStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FieldPeasStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyPeaCloverGrassStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_YoungForest] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Wasteland] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WaterBufferZone] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_PLWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterTriticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLSpringWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLMaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLFodderLucerne1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLFodderLucerne2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLSpringBarleySpr] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterWheatLate] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLBeetSpr] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLBeans] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_NLWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCabbage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLTulips] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazed1Spring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazedLast] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLPermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLSpringBarleySpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLMaizeSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLPotatoesSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLBeetSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCarrotsSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCabbageSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCatchCropPea] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLOrchardCrop] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLPermanentGrassGrazedExtensive] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazedExtensive1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazedExtensive1Spring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazedExtensive2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazedExtensiveLast] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_UKBeans] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKPermanentGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKTempGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_UKWinterWheat] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_BEBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEBeetSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BECatchPeaCrop] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEGrassGrazed1Spring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEGrassGrazedLast] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEMaizeCC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEMaizeSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEOrchardCrop] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEPotatoesSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEWinterBarleyCC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_BEWinterWheatCC] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_PTPermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTSorghum] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTFodderMix] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTTurnipGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTCloverGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTCloverGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTTriticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTOtherDryBeans] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTShrubPastures] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTCorkOak] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTVineyards] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTBeans] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTRyegrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTYellowLupin] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PTHorticulture] = EL_BUG_PERCENT_D;


	g_bug_percent_d[tov_DESugarBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DECabbage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DECarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEGrasslandSilageAnnual] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEGreenFallow_1year] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DELegumes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEMaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOCabbages] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOGrasslandSilageAnnual] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOGreenFallow_1year] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOLegume] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOMaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOPermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOSpringRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOSugarBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOTriticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEPermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEPermanentGrassLowYield] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOPermanentGrassLowYield] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEPotatoesIndustry] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DESpringRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DETriticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEWinterWheatLate] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEAsparagusEstablishedPlantation] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOAsparagusEstablishedPlantation] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEHerbsPerennial_1year] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEHerbsPerennial_after1year] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOHerbsPerennial_1year] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOHerbsPerennial_after1year] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DESpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOrchard] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOOrchard] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEPeas] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOPeas] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEBushFruitPerm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DEOBushFruitPerm] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_DKCatchCrop] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOCatchCrop] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLupines] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLentils] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLegume_Peas] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLegume_Beans] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLegume_Whole] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLegume_Peas_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLegume_Beans_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLegume_Whole_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSugarBeets] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKFodderBeets] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOFodderBeets] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSugarBeets] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKCabbages] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOCabbages] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOLegumeCloverGrass_Whole] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKLegume_Whole] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKLegume_Peas] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKLegume_Beans] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKWinterWheat_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOWinterWheat_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringBarley_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSpringBarley_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringBarleyCloverGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSpringBarleyCloverGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKCerealLegume] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOCerealLegume] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKCerealLegume_Whole] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOCerealLegume_Whole] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOCloverGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOCloverGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOWinterCloverGrassGrazedSown] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKCloverGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKCloverGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKWinterCloverGrassGrazedSown] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringFodderGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKWinterFodderGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSpringFodderGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOWinterFodderGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKGrazingPigs] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKMaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKMixedVeg] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOGrazingPigs] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOMaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOMixedVeg] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOPotato] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOPotatoIndustry] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOPotatoSeed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSeedGrassRye_Spring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSetAside] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSetAside_AnnualFlower] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSetAside_PerennialFlower] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSetAside_SummerMow] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSpringBarleySilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSpringOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringOats_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSpringOats_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOSpringWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOVegSeeds] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKWinterRye_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOWinterRye_CC] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKPotato] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKPotatoIndustry] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKPotatoSeed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSeedGrassFescue_Spring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSeedGrassRye_Spring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSetAside] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSetAside_SummerMow] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringBarley_Green] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringBarleySilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKSpringWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKUndefined] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKVegSeeds] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOOrchardCrop_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOrchardCrop_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOrchApple] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOrchPear] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOrchCherry] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOrchOther] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOOrchApple] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOOrchPear] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOOrchCherry] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOOrchOther] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKBushFruit_Perm1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKBushFruit_Perm2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOBushFruit_Perm1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOBushFruit_Perm2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKChristmasTrees_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOChristmasTrees_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKEnergyCrop_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOEnergyCrop_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKFarmForest_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOFarmForest_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKGrazingPigs_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOGrazingPigs_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOGrassGrazed_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOGrassLowYield_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKFarmYoungForest_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKOFarmYoungForest_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKPlantNursery_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKGrassGrazed_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_DKGrassLowYield_Perm] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_FIWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FISugarBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIStarchPotato_North] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIStarchPotato_South] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOStarchPotato_North] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOStarchPotato_South] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FISpringWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOSpringWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FITurnipRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOTurnipRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FISpringRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOSpringRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIPotato_North] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIPotato_South] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOPotato_North] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOPotato_South] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIPotatoIndustry_North] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIPotatoIndustry_South] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOPotatoIndustry_North] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOPotatoIndustry_South] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FISpringOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOSpringOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FISpringBarley_Malt] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOSpringBarley_Malt] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIFabaBean] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOFabaBean] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FISpringBarley_Fodder] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FISprSpringBarley_Fodder] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOSpringBarley_Fodder] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIGrasslandPasturePerennial1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIGrasslandPasturePerennial2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIGrasslandSilagePerennial1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIGrasslandSilagePerennial2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FINaturalGrassland] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIFeedingGround] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIGreenFallow_1year] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIBufferZone] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIGrasslandSilageAnnual] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FICaraway1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FICaraway2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOCaraway1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIOCaraway2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FINaturalGrassland_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIGreenFallow_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FIBufferZone_Perm] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_SESpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SEWinterRape_Seed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SEWinterWheat] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_IRSpringWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_IRSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_IRSpringOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_IRGrassland_no_reseed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_IRGrassland_reseed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_IRWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_IRWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_IRWinterOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRWinterTriticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRMaize_Silage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRGrassland] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRGrassland_Perm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRSpringOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRSunflower] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRSpringWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FRSorghum] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_ITGrassland] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_ITOrchard] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_ITOOrchard] = EL_BUG_PERCENT_D;

	m_newoldgrowthindex2 = 0;
	//reset if there is flower resource
	if (m_OurPollenNectarCurveSet) {
		PollenNectarReset();
	}


}

void VegElement::ReadBugPercentageFile( void ) {
    FILE* lm_ifile=fopen(l_el_bug_percentage_file.value(), "r" );
  if ( !lm_ifile ) {
    g_msg->Warn( WARN_FILE, "PlantGrowthData::ReadBugPercentageFile(): Unable to open file", l_el_bug_percentage_file.value() );
    exit( 1 );
  }
  for ( int i = 0; i < tov_Undefined; i++ ) {
    int vegnum;
    // **cjt** modified 31/01/2004
    double weedpercent, bugpercent_a, bugpercent_b, bugpercent_c, bugpercent_d;
    if ( 2 != fscanf( lm_ifile, "%d %lf %lf %lf %lf %lf", & vegnum, & weedpercent, & bugpercent_a, & bugpercent_b,
         & bugpercent_c, & bugpercent_d ) ) {
           char vegnums[ 20 ];
           sprintf( vegnums, "%d", tov_Undefined );
           g_msg->Warn( WARN_FILE,
                "VegElement::ReadBugPercentageFile(): Unable to read"
                " sufficient number of int/double pairs from bug percentage file."" Lines expected:", vegnums );
           exit( 1 );
    }
    //FloatToDouble( g_weed_percent[ vegnum ], weedpercent);
	//FloatToDouble( g_bug_percent_a[ vegnum ] , bugpercent_a);
    //FloatToDouble( g_bug_percent_b[ vegnum ] , bugpercent_b);
    //FloatToDouble( g_bug_percent_c[ vegnum ] , bugpercent_c);
    //FloatToDouble( g_bug_percent_d[ vegnum ] , bugpercent_d);
	g_weed_percent[ vegnum ] = weedpercent;
	g_bug_percent_a[ vegnum ] = bugpercent_a;
    g_bug_percent_b[ vegnum ] = bugpercent_b;
    g_bug_percent_c[ vegnum ] = bugpercent_c;
    g_bug_percent_d[ vegnum ] = bugpercent_d;
  }
  fclose( lm_ifile );
}

void VegElement::RecalculateBugsNStuff(void) {
	/** This is the heart of the dynamics of vegetation elements. It calculates vegetation cover and uses this to determine vegetation biomass.
	It also calculates spilled grain and goose forage, as well a calculating insect biomass, vegetation density and dead biomass*/
	m_newgrowth = 0;
	m_veg_cover = 1.0 - (exp(-cfg_beer_law_extinction_coef.value()*m_LAtotal )); // Beer's Law to give cover
	double usefull_veg_cover = 1.0 - (exp(m_LAgreen * -0.4)); // This is used to calc growth rate
	// Need gloabal radiation today
	double glrad = m_Landscape->SupplyGlobalRadiation();
	// This is different for maize (a C4 plant)
	int ptype;
	if ((m_vege_type == tov_Maize) || (m_vege_type == tov_OMaizeSilage) || (m_vege_type == tov_MaizeSilage) || (m_vege_type == tov_MaizeStrigling)) ptype = 1; else ptype = 0;
	int index = (int)floor(0.5 + m_Landscape->SupplyTemp()) + 30; // There are 30 negative temps
	double radconv = c_SolarConversion[ptype][index];
	if (m_LAtotal >= m_oldLAtotal) {
		// we are in positive growth so grow depending on our equation
		m_newgrowth = usefull_veg_cover * glrad * radconv * g_biomass_scale[m_vege_type];
		if (m_owner_index != -1) { // This only works because only crops and similar structures have owners
			int fintensity = m_Landscape->SupplyFarmIntensity(m_poly);
			if (fintensity == 1) {
				// 1 means extensive, so reduce vegetation biomass by 20%
				// NB this cannot be used with extensive crop types otherwise you get an additional 20% reduction
				// This way of doing things provides a quick and dirty general effect.
				m_veg_biomass += m_newgrowth * 0.8;
			}
			else m_veg_biomass += m_newgrowth;
		}
		else m_veg_biomass += m_newgrowth;
	}
	else {
		// Negative growth - so shrink proportional to the loss in LAI Total
		if (m_oldLAtotal > 0) {
			double temp_propotion = m_LAtotal / m_oldLAtotal;
			m_newgrowth = m_veg_biomass*(temp_propotion-1);
			m_veg_biomass *= temp_propotion;
		}
	}
	/** Here we also want to know how much biomass we have on the field in total. So we multiply the current biomass by area */
	m_total_biomass = m_veg_biomass * m_area;
	m_total_biomass_old = m_total_biomass;
	// NB The m_weed_biomass is calculated directly from the curve in Curves.pre
	// rather than going through the rigmarole of converting leaf-area index
		m_veg_density = (int)(floor(0.5 + (m_veg_biomass / (1 + m_veg_height))));
	if (m_veg_density > 100) m_veg_density = 100; // to stop array bounds problems
	if (m_LAtotal == 0.0) m_green_biomass = 0.0;
	else m_green_biomass = m_veg_biomass * (m_LAgreen / (m_LAtotal));
	m_dead_biomass = m_veg_biomass - m_green_biomass;
	// Here we use our member function pointer to call the correct piece of code for our current species
	(this->*(SpeciesSpecificCalculations))(); 
}

void VegElement::SetSpeciesFunction(TTypesOfPopulation a_species)
{
	switch (a_species)
	{
    case TOP_NoSpecies: // nospecies still calculate geese forage resources. Unless pollen and nectar are specified - Jordan
		if (cfg_pollen_nectar_on.value()) { //JC
			SpeciesSpecificCalculations = &VegElement::PollenNectarPhenologyCalculation; //JC
			break; //JC
		} //JC
	case TOP_Goose:
		SpeciesSpecificCalculations = &VegElement::CalcGooseForageResources;
		break;
	case TOP_Bombus:
		if (!cfg_pollen_nectar_on.value()) {
			break;
		}
	case TOP_ApisRAM:
	case TOP_Osmia:
		SpeciesSpecificCalculations = &VegElement::PollenNectarPhenologyCalculation; // This needs an override for VegLE that have crops
		break;
	case TOP_Ladybird:
		// Andrey to add something here
		//break;
	case TOP_Hare:
	case TOP_Vole:
		SpeciesSpecificCalculations = &VegElement::CalculateDigestibility; 
		break;
	case TOP_Skylark:
	case TOP_Partridge:
	case TOP_Erigone:
		SpeciesSpecificCalculations = &VegElement::CalculateInsectBiomass;
		break;
	case TOP_Aphid:
		SpeciesSpecificCalculations = &VegElement::CalculateNewgrowth;
		break;
	default:
		SpeciesSpecificCalculations = &VegElement::DoNothing;
	}
}

void VegElement::CalculateDigestibility()
{
	/** This is used for hare and voles. It is a 32-day running sum of the amount of new growth per day divided by total veg biomass with a minimum value of 0.5 */
	++m_newoldgrowthindex &= 31;
	if (m_veg_biomass > 0) {
		switch (m_vege_type) {
		case tov_NoGrowth:
		case tov_None:
		case tov_OFirstYearDanger:
			m_digestability = 0.0;
			break;
		case tov_OPotatoes:
		case tov_Potatoes:
		case tov_PotatoesIndustry:
		case tov_PLMaizeSilage:
		case tov_PLPotatoes:
		case tov_NLPotatoes:
		case tov_NLPotatoesSpring:
		case tov_UKPotatoes:
		case tov_DEPotatoes:
			m_digestability = 0.5;
			break;
		default:
			//m_oldnewgrowth[m_newoldgrowthindex]=(newgrowth/m_veg_biomass);
			m_oldnewgrowth[m_newoldgrowthindex] = (m_newgrowth);
			m_newgrowthsum = 0.0;
			for (int i = 0; i < 32; i++) {
				m_newgrowthsum += m_oldnewgrowth[i];
			}
			m_digestability = m_newgrowthsum / m_veg_biomass;
			m_digestability += 0.5;
			if (m_digestability > 0.8) m_digestability = 0.8;
		}
	}
	else {
		m_oldnewgrowth[m_newoldgrowthindex] = 0.0;
		m_digestability = 0.0;
	}
}

void VegElement::CalculateNewgrowth()
{
	/** This is used for aphids. It is a 14-day running average of the amount of new growth per day */

	//Calculation of the green biomass percentage to the total biomass.
	m_greenbiomass_per = 0;
	if(m_veg_biomass>0){
		m_greenbiomass_per = m_green_biomass/m_veg_biomass;
	}


	if (++m_newoldgrowthindex2 > 13)  m_newoldgrowthindex2 = 0;
	if (m_veg_biomass > 0) {
		if (m_newgrowth >= 0){
			m_oldnewgrowth2[m_newoldgrowthindex2] = m_newgrowth;
		}
		else{
			m_oldnewgrowth2[m_newoldgrowthindex2] = 0.0;
		}
		m_newgrowthruningaveragesum = 0.0;
		for (int i = 0; i < 14; i++) {
			m_newgrowthruningaveragesum += m_oldnewgrowth2[i];
		}
		m_growth_stage = (1-m_newgrowthruningaveragesum/m_veg_biomass)*100;
		if(m_growth_stage<0) m_growth_stage = 0;
		if(m_growth_stage>cfg_noncrop_growth_stage_with_green_biomass.value()) m_growth_stage = cfg_noncrop_growth_stage_with_green_biomass.value();

		if(m_greenbiomass_per>0.9 && m_growth_stage>50) {
			m_growth_stage = 50;
		}
	}

	//else if(m_veg_biomass > 0 && m_newgrowth < 0){
	//	m_growth_stage = cfg_noncrop_growth_stage_with_green_biomass.value();
	//	m_oldnewgrowth2[m_newoldgrowthindex2] = 0.0;
	//}

	else {
		m_growth_stage = 100;
		m_oldnewgrowth2[m_newoldgrowthindex2] = 0.0;
	}

	//use the green biosmass to calculate the growth stage
	//double temp_max_ddeg = g_crops->GetMaxDdegLAgreen(m_curve_num, m_veg_phase);
	/*
	double temp_max_ddeg = g_crops->GetMaxDdegHeight(m_curve_num, m_veg_phase);
	if(temp_max_ddeg > 0){
		m_growth_stage = m_ddegs / temp_max_ddeg * 100;
		if(m_growth_stage < 0) m_growth_stage = 0;
		if(m_growth_stage > 100 ) m_growth_stage = 100;
	}
	else if (m_green_biomass > 0){
		m_growth_stage = cfg_noncrop_growth_stage_with_green_biomass.value();
	}
	else{
		m_growth_stage = 100;
	}
	*/
}

void VegElement::CalculateInsectBiomass()
{
	// The insect calculation part
	// Bugmass = a + b(biomass) + c(height)
	double temp_bugmass = //g_bug_percent_d[ m_vege_type ] // This was used as a scaler - now not used
		g_bug_percent_a[m_vege_type] + ((m_veg_biomass + m_weed_biomass) * g_bug_percent_b[m_vege_type])
		+ (m_veg_height * g_bug_percent_c[m_vege_type]);
	// Set a minimum value (regressions will otherwise sometimes give a -ve value
	if (temp_bugmass < 0.05) temp_bugmass = 0.05;
	temp_bugmass *= cfg_insectbiomassscaling.value();
	// Now need to check for deviations caused by management
	// First spot the deviation - this is easy because the only deviation that does
	// not affect the vegetation too is insecticide spraying
	if (m_days_since_insecticide_spray > 0) {
		// Need to change insects still, so grow towards the target, but only when 21 days from zero effect
		if (m_days_since_insecticide_spray < 21) m_insect_pop += (temp_bugmass - m_insect_pop) / m_days_since_insecticide_spray;
		m_days_since_insecticide_spray--;
	}
	else {
		m_insect_pop = temp_bugmass;
	}
}
void VegElement::PollenNectarReset()
{	/**
	* Reset its the first day of the year.
	* This relies on the fact that the first entry is always a constant to set the first value
	* in each curve to, the second is always zero in the m_index of day degrees, so the first interesting value is the
	* slope between second (0) and third entry.
	*/
	//if(m_Landscape->SupplyMonth()>=8) return;
	m_ddegsNec = 0;
	m_ddegsSug = 0;
	m_ddegsPol = 0;
	m_polleninfo.m_quantity = m_OurPollenNectarCurveSet->GetPollenQuantity(0);
	m_polleninfo.m_quality = m_OurPollenNectarCurveSet->GetPollenQuality(0);
	m_nectarinfo.m_quantity = m_OurPollenNectarCurveSet->GetNectar(0);
	m_nectarinfo.m_quality = m_OurPollenNectarCurveSet->GetSugar(0);
	m_PN_index_Nectar = 1; // where we stored the slope relative to the 2nd and 3rd entry.
	m_PN_index_Sugar = 1; // where we stored the slope relative to the 2nd and 3rd entry.
	m_PN_index_Pollen = 1; // where we stored the slope relative to the 2nd and 3rd entry.
	m_PN_DD_NecTarget = m_OurPollenNectarCurveSet->GetDDTargetNectar(2);
	m_PN_DD_SugTarget = m_OurPollenNectarCurveSet->GetDDTargetSugar(2);
	m_PN_DD_PolTarget = m_OurPollenNectarCurveSet->GetDDTargetPollen(2);

	m_PN_Cold_Counter_Nec = 0;
  	m_PN_Cold_Counter_Sug = 0;
	m_PN_Cold_Counter_Pol = 0;

	m_flower_weight = 1.0;
}

double VegElement::SupplyFlowerBaseTemp(void){
	int current_month = m_Landscape->SupplyMonth();
	double current_base_temp =  m_OurPollenNectarCurveSet->GetThreshold();

	//starting from Sept, we need to increase the base development temperature
	if (current_month >= cfg_month_varying_flower_base_temp.value()){
		current_base_temp += cfg_base_temp_increment_flower.value()*(current_month-cfg_month_varying_flower_base_temp.value()+1);
	}
	double base_temp_array[12] = {4.5, 4.5, 4.5, 4.5, 8, 8, 8, 5, 5, 0, 0, 0};
	return base_temp_array[current_month-1];

	return current_base_temp;
}

void VegElement::UpdateDayDegreeBelowMin(double &updated_ddegree)
{
	if (updated_ddegree <= 0 && m_Landscape->SupplyMonth() >= cfg_month_varying_flower_base_temp.value()){
		m_flower_weight *= cfg_weight_cold_flower_resource.value(); // if it is too cold, the flowering period and resource amount will be decreased
		updated_ddegree = SupplyFlowerBaseTemp() + 5; // at least accumulate 
	}

	if (updated_ddegree >= 0 && m_Landscape->SupplyMonth() >= cfg_month_varying_flower_base_temp.value() && m_flower_weight<1.0)
	{
		m_flower_weight /= cfg_weight_cold_flower_resource.value(); //if it is warm, increase the flower resource a bit
	}

	/*
	//check the conditions
	if ((a_curve_set->*getCurveSlopeFuncP)(PN_index+1)< (a_curve_set->*getCurveSlopeFuncP)(PN_index)&& updated_ddegree <= 0.001) { //it is cold, decrease it
		reverse_flag = 1;
		updated_ddegree = target_ddegree-accu_ddegree;
		int p_index = PN_index+1;
		return;			
	}
	//slope is positive
	else if ((a_curve_set->*getCurveSlopeFuncP)(PN_index+1)>(a_curve_set->*getCurveSlopeFuncP)(PN_index) && updated_ddegree <= 0.001){ //it is cold, but it is still in the increaing area
		//add 10 day-degres
		updated_ddegree = SupplyFlowerBaseTemp();
		reverse_flag = -1;
	}*/
}

void VegElement::UpdateFlowerResource(PollenNectarDevelopmentCurveSet* a_curve_set, double (PollenNectarDevelopmentCurveSet::*getCurveSlopeFuncP)(int i),   double (PollenNectarDevelopmentCurveSet::*getDDTargetFuncP)(int i) , int &PN_index, double &updated_ddegree, double &target_ddegree, double accu_ddegree, double &resource_value,  bool pollen_flag)
{
	if(target_ddegree >= INT_MAX-100) return; //no flower resource, just return
	//Check whether multiple inflection points are passed
	if (accu_ddegree+updated_ddegree > m_flower_weight*target_ddegree){
		//We need to update it peice by pecie since the slopes are different
		//update the first piece
		double temp_newddegs = updated_ddegree;
		double temp_accddegs = accu_ddegree;
		
		//go to next inflection point and lope until the last one is reached
		temp_newddegs -= (m_flower_weight*target_ddegree-temp_accddegs);
		temp_accddegs = m_flower_weight*target_ddegree; 
		PN_index++;
		target_ddegree =(a_curve_set->*getDDTargetFuncP)(PN_index + 1);
		while(temp_accddegs+temp_newddegs > m_flower_weight*target_ddegree){
			temp_newddegs -= (m_flower_weight*target_ddegree-temp_accddegs);
			temp_accddegs = m_flower_weight*target_ddegree;
			PN_index++;
			target_ddegree = (a_curve_set->*getDDTargetFuncP)(PN_index + 1);
		}
	}

	//update the result
	resource_value = m_flower_weight*(a_curve_set->*getCurveSlopeFuncP)(PN_index);
	if(pollen_flag) m_polleninfo.m_quality = m_OurPollenNectarCurveSet->GetPollenQuality(PN_index);
	//accu_ddegree += updated_ddegree;
}


void VegElement::PollenNectarPhenologyCalculation()
{
	/**
	* This calculates the daily pollen and nectar amounts and qualities from the pollen and nectar curves.
	*
	*/
	//double newddegs = g_weather->GetTemp() - SupplyFlowerBaseTemp(); //m_OurPollenNectarCurveSet->GetThreshold();

	//too cold, remove resource instead of increasing 
	//if (newddegs < 0.0) {
	//	newddegs = 0.0;
		/***
		m_polleninfo.m_quantity *= 0.5;
		m_polleninfo.m_quality *= 0.5;
		m_nectarinfo.m_quantity *= 0.5;
		m_nectarinfo.m_quality *= 0.5;
		if(m_ddegsPol > 0) m_PN_Cold_Counter_Nec += 1;
  		if(m_ddegsSug > 0) m_PN_Cold_Counter_Sug += 1;
		if(m_ddegsNec > 0) m_PN_Cold_Counter_Pol += 1;

		if(m_PN_Cold_Counter_Pol > 4 && m_ddegsPol > 0){
			m_polleninfo.m_quantity = 0;
			if (g_date->GetMonth() > 4)
			m_PN_DD_PolTarget = 99999; //too cold, this year is finished even the curve is not finished
			
		}
		if(m_PN_Cold_Counter_Nec > 10 && m_ddegsNec > 0){
			m_nectarinfo.m_quantity = 0;
			if (g_date->GetMonth() > 4)
			m_PN_DD_NecTarget = 99999;  //too cold, this year is finished even the curve is not finished
		}
		if(m_PN_Cold_Counter_Sug > 10 && m_ddegsSug > 0){
			m_nectarinfo.m_quality = 0;
			if (g_date->GetMonth() > 4)
			m_PN_DD_SugTarget = 99999;  //too cold, this year is finished even the curve is not finished
		}
		***/
	//}
	
	//only do this when that day it can accumulate day degrees
	//else{
		//m_PN_Cold_Counter_Nec = 0;
  		//m_PN_Cold_Counter_Sug = 0;
		//m_PN_Cold_Counter_Pol = 0;
	//if (newddegs > m_OurPollenNectarCurveSet->GetMaxDDeg()) newddegs = m_OurPollenNectarCurveSet->GetMaxDDeg();

	//Pollen, nectar, sugar have their own copy of the accumulated day-degrees. We need to decrease the resource amout even the accumulated day-degree is zero but the curve slope is negative.
	//double base_temp_array_nectar[12] = {3.6, 3.6, 3.9, 3.9, 9, 9, 9, 6, 5, 0, 0, 0};
	//double base_temp_array_pollen[12] = {3.9, 3.9, 4, 4, 9, 9, 9, 6, 5, 0, 0, 0};
	double current_temp = g_weather->GetTemp();
	int current_month = m_Landscape->SupplyMonth()-1;
	double pol_new_ddegs = current_temp - cfg_PollenMonthBaseTemp.value(current_month);
	//reset when it is in nov. and cold
	if (pol_new_ddegs<0 && current_month>=10){
		PollenNectarReset();
		return;
	}
	if (pol_new_ddegs<0) pol_new_ddegs = 0;
	if (pol_new_ddegs > m_OurPollenNectarCurveSet->GetMaxDDeg()) pol_new_ddegs = m_OurPollenNectarCurveSet->GetMaxDDeg();
	double nec_new_ddegs = current_temp - cfg_NectarMonthBaseTemp.value(current_month);
	//reset when it is in nov. and cold
	if (nec_new_ddegs<0 && current_month>=10){
		PollenNectarReset();
		return;
	}
	if (nec_new_ddegs<0) nec_new_ddegs = 0;
	if (nec_new_ddegs > m_OurPollenNectarCurveSet->GetMaxDDeg()) nec_new_ddegs = m_OurPollenNectarCurveSet->GetMaxDDeg();
	double sug_new_ddegs = nec_new_ddegs;

	

	/**
	* Calculates for each curve the increment/decrement associated with the change in ddegs and the current gradient
	* To do this we need to track both where we are in terms of the current day degrees and today's addition
	* The slow way would be to look through the index curve until we exceed or are equal to an inflection point,
	* but this can be speeded up by saving the current next target in m_PN_DD_target
	*/

	/**
	 * Check whether it is in the decreasing period (negative slop) and current temperature is below the minimum development time.
	 * If yes, the accumulated day-degrees will be increased to to reach the next non-negative inflection point.
	 */
	
	//Check for pollen
	//UpdateDayDegreeBelowMin(pol_new_ddegs);
	//Check for nectar
	//UpdateDayDegreeBelowMin(nec_new_ddegs);
	//Check for sugar
	//UpdateDayDegreeBelowMin(sug_new_ddegs);

	bool pollen_flag = true;
	UpdateFlowerResource(m_OurPollenNectarCurveSet, &PollenNectarDevelopmentCurveSet::GetPollenQuantity, &PollenNectarDevelopmentCurveSet::GetDDTargetPollen, m_PN_index_Pollen, pol_new_ddegs, m_PN_DD_PolTarget, m_ddegsPol, m_polleninfo.m_quantity, pollen_flag);
	//UpdateFlowerResource(m_OurPollenNectarCurveSet, &PollenNectarDevelopmentCurveSet::GetPollenQuality, &PollenNectarDevelopmentCurveSet::GetDDTargetPollen, temp_PN_index_Pollen, temp_pol_new_ddegs, m_PN_DD_PolTarget, m_ddegsPol, m_polleninfo.m_quality, reverse_flag);
	pollen_flag = false;
	//Update for nectar
	UpdateFlowerResource(m_OurPollenNectarCurveSet, &PollenNectarDevelopmentCurveSet::GetNectar, &PollenNectarDevelopmentCurveSet::GetDDTargetNectar, m_PN_index_Nectar, nec_new_ddegs, m_PN_DD_NecTarget, m_ddegsNec, m_nectarinfo.m_quantity, pollen_flag);
	//Check for sugar
	UpdateFlowerResource(m_OurPollenNectarCurveSet, &PollenNectarDevelopmentCurveSet::GetSugar, &PollenNectarDevelopmentCurveSet::GetDDTargetSugar, m_PN_index_Sugar, sug_new_ddegs, m_PN_DD_SugTarget, m_ddegsSug, m_nectarinfo.m_quality, pollen_flag);


	// Check for slight overun
	if (m_polleninfo.m_quantity < 0) {
		m_polleninfo.m_quantity = 0;
	}
	if (m_polleninfo.m_quality < 0) {
		m_polleninfo.m_quality = 0;
	}
	if (m_nectarinfo.m_quantity < 0) {
		m_nectarinfo.m_quantity = 0;
	}
	if (m_nectarinfo.m_quality < 0) {
		m_nectarinfo.m_quality = 0;
	}

	// Now do the ddegs check for inflection point being reached
	m_ddegsNec += nec_new_ddegs;
	m_ddegsSug += sug_new_ddegs;
	m_ddegsPol += pol_new_ddegs;

	//below we check whether we reach the last inflection points
	//while(true){
	//if (m_ddegsNec >= m_PN_DD_NecTarget) {
	//	m_PN_index_Nectar++;
	//	m_PN_DD_NecTarget = m_OurPollenNectarCurveSet->GetDDTargetNectar(m_PN_index_Nectar + 1);
	if (m_PN_DD_NecTarget > 99998) m_nectarinfo.m_quantity = 0;
	if (m_OurPollenNectarCurveSet->GetNectar(m_PN_index_Nectar)==0 && m_OurPollenNectarCurveSet->GetNectar(m_PN_index_Nectar+1)==0){
		m_PN_DD_NecTarget = 99999;
	}
	//}
	//else break;
	//}
	//while(true){
	//if (m_ddegsPol >= m_PN_DD_PolTarget) {
	//	m_PN_index_Pollen++;
	//	m_PN_DD_PolTarget = m_OurPollenNectarCurveSet->GetDDTargetPollen(m_PN_index_Pollen + 1);
	if (m_PN_DD_PolTarget > 99998) { m_polleninfo.m_quality = 0; m_polleninfo.m_quantity = 0; }
	if (m_OurPollenNectarCurveSet->GetPollenQuantity(m_PN_index_Pollen)==0 && m_OurPollenNectarCurveSet->GetPollenQuantity(m_PN_index_Pollen+1)==0){
		m_PN_DD_PolTarget = 99999;
	}
	//}
	//else break;
	//}
	//while(true){
	//if (m_ddegsSug >= m_PN_DD_SugTarget) {
	//	m_PN_index_Sugar++;
	//	m_PN_DD_SugTarget = m_OurPollenNectarCurveSet->GetDDTargetSugar(m_PN_index_Sugar + 1);
	if (m_PN_DD_SugTarget > 99998) m_nectarinfo.m_quality = 0;
	if (m_OurPollenNectarCurveSet->GetSugar(m_PN_index_Sugar)==0 && m_OurPollenNectarCurveSet->GetSugar(m_PN_index_Sugar+1)==0){
		m_PN_DD_SugTarget = 99999;
	}
	//}
	//else break;
	//}


	//curve reset, prepare for the next year
	//if(m_PN_DD_NecTarget > 99998 && m_PN_DD_PolTarget > 99998 && m_PN_DD_SugTarget > 99998 && g_date->DayInYear()>=364) PollenNectarReset();

	/**
	* Finally set the the total amounts of pollen and nectar for the LE
	*/
	/**
	* EZ - DEBUGG
	*/
	//m_polleninfo.m_quantity = 1000;
	
	m_totalPollen = double(m_area) * m_polleninfo.m_quantity;
	m_totalNectar = double(m_area) * m_nectarinfo.m_quantity;
	// DEBUG CODE
	//fstream ofilepn("PollenNectarResources.txt", ios::app);
	//ofilepn << g_date->GetYearNumber() << '\t' << g_date->DayInYear() << '\t'<<m_nectarinfo.m_quantity << '\t' << m_polleninfo.m_quantity << endl;
	//ofilepn.close();
}

void VegElement::CalcGooseForageResources()
{
	// For geese that eat spilled grain and maize we need to remove some of this daily (loss to other things than geese)
	// Get the Julian day
	int day = g_date->DayInYear();
	double rate, maizerate;
    if (cfg_goose_UniformDecayRate.value()){
        rate = cfg_goose_GrainDecayRateWinter.value();
        maizerate = cfg_goose_MaizeDecayRateWinter.value();
    }
    else{
        if ((day > March) && (day < July)){
            rate = cfg_goose_GrainDecayRateSpring.value();
            maizerate = cfg_goose_MaizeDecayRateSpring.value();
        }
        else {
            rate = cfg_goose_GrainDecayRateWinter.value();
            maizerate = cfg_goose_MaizeDecayRateWinter.value();
        }
    }

    //std::cout<<"DEBUG: grain decay rate is "<<rate<<"\n";
    if (m_birdseedforage>0){
        m_birdseedforage *= rate;
    }


	if (m_birdseedforage < 0.01) m_birdseedforage = 0.0;
    if (m_birdmaizeforage>0){
        m_birdmaizeforage *= maizerate;
    }

	if (m_birdmaizeforage < 0.01) m_birdmaizeforage = 0.0;
	// We also need to calculate non-grain forage for geese
	if (IsCereal()) {
		//if (m_green_biomass > 0.5)  //Testing if this could be a suitable fix for the cereals
		//{
			for (unsigned i = 0; i < gs_foobar; i++) {
				/** The 1.0325 is a quick fix to account for higher energy intake on winter cereal - Based on Therkildsen & Madsen 2000 Energetics of feeding...*/
				m_goosegrazingforage[ i ] = m_Landscape->SupplyGooseGrazingForageH( m_veg_height, (GooseSpecies)i ) * cfg_goose_grass_to_winter_cereal_scaler.value();
			}
		//}
		//else for (unsigned i = 0; i < gs_foobar; i++) {
		//	m_goosegrazingforage[i] = 0.0;
		//}
		//m_goosegrazingforage[gs_foobar] = 1; // Is cereal
	}
	/** or potentially it is a grazable grass */
	else {
		if (IsGooseGrass()) {
			for (unsigned i = 0; i < gs_foobar; i++) {
				//m_goosegrazingforage[ i ] = 0.0;
				m_goosegrazingforage[i] = m_Landscape->SupplyGooseGrazingForageH(m_veg_height, (GooseSpecies)i);
			}
		}
		else for (unsigned i = 0; i < gs_foobar; i++)	m_goosegrazingforage[i] = 0.0;
	}
}

void VegElement::RandomVegStartValues( double * a_LAtotal, double * a_LAgreen, double * a_veg_height, double * a_weed_biomass ) {
  * a_LAtotal = EL_VEG_START_LAIT * ( ( ( ( double )( random( 21 ) - 10 ) ) / 100.0 ) + 1.0 ); // +/- 10%
  * a_LAgreen = * a_LAtotal / 4.0;
  * a_veg_height = * a_LAgreen * EL_VEG_HEIGHTSCALE;
  * a_weed_biomass = * a_LAgreen * 0.1; // 10% weeds by biomass
}


void VegElement::SetGrowthPhase(int a_phase) {

	if (a_phase == sow) {
		m_vegddegs = 0.0;
	}
	else if (a_phase == harvest) m_vegddegs = -1;
	if (a_phase == janfirst) {
		m_forced_phase_shift = false;
		/**
		* If it is the first growth phase of the year then we might cause some unnecessary hops if e.g. our biomass is 0 and we suddenly jump up to
		* 20 cm To stop this happening we check here and if our settings are lower than the targets we do nothing.
		*/
		if (g_crops->StartValid(m_curve_num, a_phase)) {
			double temp_veg_height = g_crops->GetStartValue(m_curve_num, a_phase, 2);
			if (temp_veg_height < m_veg_height) { // Otherwise we are better off with the numbers we have to start with
				// Now with added variability
				m_LAgreen = g_crops->GetStartValue(m_curve_num, a_phase, 0);
				m_LAtotal = g_crops->GetStartValue(m_curve_num, a_phase, 1);
				m_veg_height = g_crops->GetStartValue(m_curve_num, a_phase, 2);
			}
		}

	}
	else if (g_crops->StartValid(m_curve_num, a_phase)) {
		m_LAgreen = g_crops->GetStartValue(m_curve_num, a_phase, 0);
		m_LAtotal = g_crops->GetStartValue(m_curve_num, a_phase, 1);
		m_veg_height = g_crops->GetStartValue(m_curve_num, a_phase, 2);
	}
	else if (!m_force_growth) {
		// If we are in forced growth mode (which is very likely),
		// then do not choose a new set of starting values, as we have
		// already calculated our way to a reasonable set of values.
		//RandomVegStartValues( & m_LAtotal, & m_LAgreen, & m_veg_height, & m_weed_biomass ); // **CJT** Testing removal 17/02/2015
	}
	m_veg_phase = a_phase;
	m_yddegs = 0.0;
	m_ddegs = g_weather->GetDDDegs(g_date->Date());
	m_force_growth = false;

	if (m_veg_phase == janfirst) {
		// For some growth curves there is no growth in the first
		// two months of the year. This will more likely than
		// not cause a discontinuous jump in the growth curves
		// come March first. ForceGrowthSpringTest() tries
		// to avoid that by checking for positive growth values
		// for the January growth phase. If none are found, then
		// it initializes a forced growth/transition to the March
		// 1st starting values.
		ForceGrowthSpringTest(); // Removal of this causes continuous increase in vegetation growth year on year for any curve that does not have a hard reset (e.g. harvest).
	}
}


void VegElement::ForceGrowthTest( void ) {
  // Called whenever the farmer does something 'destructive' to a
  // field, that reduced the vegetaion.
  if ( g_date->DayInYear() >= g_date->DayInYear( 1, 11 )
       || ( g_date->DayInYear() < g_date->DayInYear( 1, 3 ) && m_force_growth ) ) {
         ForceGrowthInitialize();
  }
}



void VegElement::ForceGrowthSpringTest(void) {
	// Check if there are any positive growth differentials in the curve
	// for the first two months of the year. Do nothing if there is.
	// If we have any positive growth then no need to force either
	if (g_crops->GetLAgreenDiff(90000.0, 0.0, m_curve_num, janfirst) > 0.001
		|| g_crops->GetLAtotalDiff(90000.0, 0.0, m_curve_num, janfirst) > 0.001
		|| g_crops->GetHeightDiff(90000.0, 0.0, m_curve_num, janfirst) > 0.001) {
		return;
	}
	// No growth, force it.
	ForceGrowthInitialize();
}



void VegElement::ForceGrowthInitialize( void ) {
  double LAgreen_target;
  double Weed_target;
  double LAtotal_target;
  double veg_height_target;
  int next_phase, daysleft;

  // Figure out what our target phase is.
  if ( g_date->DayInYear() < g_date->DayInYear( 3, 1 ) ) {
    daysleft = g_date->DayInYear( 1, 3 ) - g_date->DayInYear();
    next_phase = marchfirst;
  } else if ( g_date->DayInYear() >= g_date->DayInYear( 1, 11 ) ) {
    daysleft = 366 - g_date->DayInYear(); // Adjusted from 365 to prevent occaisional negative values
    next_phase = janfirst;
  } else {
    return;
  }
  if ( daysleft <= 0 )
       // Uh! Oh! This really shouldn't happen.
         return;

  if ( !g_crops->StartValid( m_curve_num, next_phase ) ) {
    // If no valid starting values for next phase, then
    // preinitialize the random starting values! Ie. make the
    // choice here and then do not choose another set come
    // next phase transition, but use the values we already
    // got at that point in time.
    RandomVegStartValues( & LAtotal_target, & LAgreen_target, & veg_height_target, & Weed_target );
  }
  else {
	  //add +/- 20% variation
	  double vari = (g_rand_uni() * 0.4) + 0.8;
	  Weed_target = g_crops->GetStartValue(m_weed_curve_num, next_phase, 0) * vari;
	  LAgreen_target = g_crops->GetStartValue(m_curve_num, next_phase, 0) * vari;
	  LAtotal_target = g_crops->GetStartValue(m_curve_num, next_phase, 1) * vari;
	  veg_height_target = g_crops->GetStartValue(m_curve_num, next_phase, 2) * vari;
  }

  m_force_growth = true;
  m_force_Weed = ( Weed_target - m_weed_biomass ) / ( double )daysleft;
  m_force_LAgreen = ( LAgreen_target - m_LAgreen ) / ( double )daysleft;
  m_force_LAtotal = ( LAtotal_target - m_LAtotal ) / ( double )daysleft;
  m_force_veg_height = ( veg_height_target - m_veg_height ) / ( double )daysleft;
}


void VegElement::ForceGrowthDevelopment( void ) {
  //if ( m_herbicidedelay == 0 ) m_weed_biomass += m_force_Weed; // ***CJT*** 12th Sept 2008 - rather than force growth, weeds might be allowed to grow on their own
  m_LAgreen += m_force_LAgreen;
  m_LAtotal += m_force_LAtotal;
  m_veg_height += m_force_veg_height;

  if (m_LAgreen < 0)  m_LAgreen = 0;
  if (m_LAtotal < 0)   m_LAtotal = 0;
  if (m_veg_height < 0)  m_veg_height = 0;
}



void VegElement::ZeroVeg( void ) {
  m_LAgreen = 0.0;
  m_LAtotal = 0.0;
  m_veg_height = 0.0;
  m_veg_cover = 0.0;
  m_veg_biomass = 0.0;
  m_weed_biomass = 0.0;
  m_birdseedforage = 0.0;
  m_birdmaizeforage = 0.0;
  SetStubble(false);
  ForceGrowthTest();
  RecalculateBugsNStuff();
  PollenNectarReset();
  g_pest->RemovePlantPesticide(GetMinX(), GetMaxX(), GetMinY(), GetMaxY(), GetMapIndex());
}


void VegElement::DoDevelopment(void) {
	if (!m_force_growth) {
		//** First does the day degree calculations */
		m_yddegs = m_ddegs;
		double pos_temp_today = g_weather->GetDDDegs(g_date->Date());
		if (m_vegddegs != -1.0) 
			m_vegddegs += pos_temp_today; // Sum up the vegetation day degrees since sowing
		m_ddegs = pos_temp_today + m_yddegs; // and sum up the phase ddegs

		double dLAG = g_crops->GetLAgreenDiffScaled(m_ddegs, m_yddegs, m_curve_num, m_veg_phase, m_growth_scaler);
		double dLAT = g_crops->GetLAtotalDiffScaled(m_ddegs, m_yddegs, m_curve_num, m_veg_phase, m_growth_scaler);
		double dHgt = g_crops->GetHeightDiffScaled(m_ddegs, m_yddegs, m_curve_num, m_veg_phase, m_growth_scaler);

		m_LAgreen += dLAG;
		if (m_LAgreen < 0.0)
			m_LAgreen = 0.0;
		m_LAtotal += dLAT;
		if (m_LAtotal < 0.0)
			m_LAtotal = 0.0;
		int fintensity = 0;
#ifdef __EXTSHRINKSHEIGHT
		if (this->m_owner_index != -1) { // This only works because only crops and similar structures have owners
			fintensity = m_Landscape->SupplyFarmIntensity(m_poly);
			if (fintensity == 1) {
				// 1 means extensive, so reduce vegetation height change by 10%
				dHgt *= 0.9;
			}
		}
#endif
		m_veg_height += dHgt;
		if (m_veg_height < 0.0)    m_veg_height = 0.0;
		/** Next grows weeds proportionally to day degrees and using the weed curve if no herbicide effect before calling RecalculateBugsNStuff to caculate insect biomass, cover, digestability etc..*/
		if (m_herbicidedelay == 0) {
			double dWee = g_crops->GetLAtotalDiff(m_ddegs, m_yddegs, m_weed_curve_num, m_veg_phase);
			m_weed_biomass += dWee * cfg_ele_weedscaling.value()* (1 + fintensity);
		}
		if (m_weed_biomass < 0.0) m_weed_biomass = 0.0;
	}
	else {
		ForceGrowthDevelopment();
	}

	// check here that m_LAtotal is bigger than m_LAgreen
	if (m_LAtotal < m_LAgreen)
	{
		char error_num[100];
		sprintf(error_num, "%f < %f (force growth = %d)", m_LAtotal, m_LAgreen, m_force_growth);
		g_msg->WarnAddInfo(WARN_TRIVIAL, "Landscape::DoDevelopment(): Leaf Area Total smaller than Leaf Area Green (Veg Growth Model inconsistent). Performing hack correction.", error_num);
		m_LAtotal = 1.1 * m_LAgreen;
		// exit(1);
	}
	RecalculateBugsNStuff();
	/** Here we need to set today's goose numbers to zero in case they are not written by the goose population manager (the normal situation) */
	ResetGeese();
	// Deal with any possible unsprayed margin, transferring info as necessary
	if (GetUnsprayedMarginPolyRef() != -1) {
		m_Landscape->SupplyLEPointer(GetUnsprayedMarginPolyRef())->SetCropDataAll(m_veg_height, m_veg_biomass, m_LAtotal, m_LAgreen, m_vege_type, m_weed_biomass, m_veg_cover,
			m_cattle_grazing, m_insect_pop, m_att_veg_patchy, m_veg_density, m_dead_biomass, m_green_biomass);
	}
}
void VegElement::ResetGeese( void ) {
	m_gooseNos[ g_date->DayInYear() ] = 0;
	for (unsigned i = 0; i < gs_foobar; i++) {
		m_gooseSpNos[ g_date->DayInYear() ][ (GooseSpecies)i ] = 0;
		m_gooseSpNosTimed[ g_date->DayInYear() ][ (GooseSpecies)i ] = 0;
	}
}
/** \brief The function that reduces the vegetation due to grazing
 *
 * On Nov 18 Andrey has updated the function to take into account the digestability.
 * the search revealed that there is no other model is using it apart from Goose model
 * so hopefully there would be no unintended effects
 * */
void VegElement::GrazeVegetationTotal( double a_grams )
{

	GrazeVegetation( (a_grams*m_digestability)/m_area, true );
}

void VegElement::GrazeVegetation( double a_reduc, bool a_force )
{
	/**
	* Used to calculate the change in vegetation height and biomass as a result of grazing.
	* Input parameter is the change in wet biomass/m2. The problem is to convert this into changes in LAtotal, LAgreen and height.
	* We have an area, biomass, total biomass, height and density. If biomass is missing we need to change height and biomass before continuing and
	* and do something with LA_total and LA_Green.
	* Some assumptions:
	* 1 - The grazing takes all LA equally
	* 2 - That biomass is evenly distributed
	* 3 - That LA is proportional to biomass in some way, so LA is also evenly distributed
	* 4 - That we can use the current grazing pressure to alter a_reduc
	*/
	if (!a_force) a_reduc *= m_default_grazing_level;
	if (a_reduc >= m_veg_biomass) return;
	double propreduc = 1.0 - (a_reduc / m_veg_biomass);
	m_veg_height *= propreduc;
	m_weed_biomass *= propreduc;
	m_veg_biomass -= a_reduc;
	// Need to do something with the LA too - 
	m_LAgreen *= propreduc;
	m_LAtotal *= propreduc;
	m_oldLAtotal = m_LAtotal; // this stops double reduction of biomass later in RecalculateBugsNStuff();
}

void VegElement::ReduceVeg(double a_reduc) {
	m_LAgreen *= a_reduc;
	m_LAtotal *= a_reduc;
	m_veg_height *= a_reduc;
	m_veg_biomass *= a_reduc;
	m_weed_biomass *= a_reduc;

	ForceGrowthTest();
	m_oldLAtotal = m_LAtotal; // this stops double reduction of biomass later in RecalculateBugsNStuff();
}

void VegElement::ReduceVeg_Extended(double a_reduc) {
  m_LAgreen *= a_reduc;
  m_LAtotal *= a_reduc;
  m_veg_height *= a_reduc;
  m_veg_biomass *= a_reduc;
  m_weed_biomass *= a_reduc;

  if ( a_reduc < EL_GROWTH_PHASE_SHIFT_LEVEL ) {
    m_yddegs = 0.0;
    m_ddegs = EL_GROWTH_DAYDEG_MAGIC;
  }

  if ( g_date->DayInYear() >= EL_GROWTH_DATE_MAGIC && a_reduc < EL_GROWTH_PHASE_SHIFT_LEVEL && !m_forced_phase_shift ) {
    SetGrowthPhase( harvest1 );
    m_forced_phase_shift = true;
  }

  ForceGrowthTest();
  m_oldLAtotal = m_LAtotal; // this stops double reduction of biomass later in RecalculateBugsNStuff();
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

PastureElement::PastureElement(TTypesOfLandscapeElement tole, Landscape* L) : VegElement(tole, L)

{

	SetVegGrowthScalerRand();

}


Field::Field(Landscape *L) : VegElement( tole_Field, L ) {
	SetGrazingLevel(3);
	m_CropType = toc_Foobar; // This will be replaced when a crop is allocated
    SetVegGrowthScalerRand();
}

void VegElement::SetVegType(TTypesOfVegetation a_vege_type)
{
	m_vege_type = a_vege_type;
	m_curve_num = g_crops->VegTypeToCurveNum(a_vege_type);
	if (m_unsprayedmarginpolyref != -1) {
		// Must have an unsprayed margin so need to pass the information on to it
		LE* um = m_Landscape->SupplyLEPointer(m_unsprayedmarginpolyref);
		um->SetVegType(a_vege_type);
	}
}

void  VegElement::SetVegType(TTypesOfVegetation a_vege_type, TTypesOfVegetation a_weed_type)
{
	SetVegType(a_vege_type);
	// -1 is used as a signal not to change the weed type
	// this is because it may be specific to that field
	if (a_weed_type != tov_Undefined) m_weed_curve_num = a_weed_type;
}


void Field::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  SetSprayedToday(false); // Reset the overspray flag in case it is set
}

void Field::SetVegGrowthScalerRand()
{
    m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScalerField.value() - cfg_PermanentVegGrowthMinScalerField.value())) + cfg_PermanentVegGrowthMinScalerField.value(); // Scales growth stochastically in a range given by the configs
}

TTypesOfVegetation Field::GetPreviousTov(int a_index) {
	return m_owner->GetPreviousTov(a_index); 
}


void VegElement::SetCropData( double a_veg_height, double a_LAtotal, double a_LAgreen, TTypesOfVegetation a_veg,
     double a_cover, int a_grazed ) {
       m_veg_height = a_veg_height;
       m_LAtotal = a_LAtotal;
       m_LAgreen = a_LAgreen;
       m_vege_type = a_veg;
       m_veg_cover = a_cover;
       m_cattle_grazing = a_grazed;
}

void VegElement::SetCropDataAll( double a_veg_height, double a_biomass, double a_LAtotal, double a_LAgreen, TTypesOfVegetation a_veg, double a_wb, double a_cover, int a_grazed, double a_ins, 
	bool a_patchy, double a_dens, double a_deadbiomass, double a_greenbiomass ) {
       m_veg_height = a_veg_height;
       m_veg_biomass = a_biomass;
       m_LAtotal = a_LAtotal;
       m_LAgreen = a_LAgreen;
       m_vege_type = a_veg;
       m_weed_biomass = a_wb;
       m_veg_cover = a_cover;
       m_cattle_grazing = a_grazed;
       m_insect_pop = a_ins;
       m_veg_density = (int) a_dens;
       m_att_veg_patchy = a_patchy;
	   m_dead_biomass = a_deadbiomass;
	   m_green_biomass = a_greenbiomass;
	   // set the veg attributes 
	   GetLandscape()->Set_TOV_Att(this);
}

void VegElement::InsectMortality( double a_fraction ) {
  m_insect_pop *= a_fraction;
}

Hedges::Hedges(Landscape *L ) : VegElement(tole_Hedges, L) {
	SetVegType(tov_NaturalGrass);
	SetSubType(0);
}

HedgeBank::HedgeBank(Landscape *L) : VegElement(tole_HedgeBank, L) {
	SetVegType(tov_NaturalGrass);
	SetSubType(0);
}

PermanentSetaside::PermanentSetaside(Landscape *L) : VegElement(tole_PermanentSetaside, L) {
  SetVegType(tov_PermanentSetAside);
  SetDigestibility(0.8);
  SetVegGrowthScalerRand();
  if (g_rand_uni() < cfg_SetAsidePatchyChance.value()) 
	  Set_Att_VegPatchy(true); 
}


BeetleBank::BeetleBank(Landscape *L) : VegElement(tole_BeetleBank, L) {
   if (g_rand_uni() < cfg_BBPatchyChance.value()) 
	   Set_Att_VegPatchy(true); 
   SetVegType(tov_NaturalGrass);
 }


RoadsideVerge::RoadsideVerge(TTypesOfLandscapeElement tole, Landscape * L) : VegElement(tole, L) {
	SetVegType(tov_NaturalGrass);
	Set_Att_VegPatchy(true);
	m_DateCut = 0;
}

void RoadsideVerge::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  // Add cutting functionality when ready.
  long today = g_date->DayInYear();

  if ( g_date->JanFirst() ) {
    // beginning of year so restart the cutting
    m_DateCut = 0;
  }

  if ( today > RV_MAY_1ST ) // No cutting before May 1st
  {
    long SinceCut = today - m_DateCut; // how many days since last cut
    int month = g_date->GetMonth();
    switch ( month ) {
      case 5:
        if ( random( 14 ) + SinceCut > RV_CUT_MAY ) Cutting( today );
      break;
      case 6:
        if ( random( 14 ) + SinceCut > RV_CUT_JUN ) Cutting( today );
      break;
      case 7:
        if ( random( 14 ) + SinceCut > RV_CUT_JUL ) Cutting( today );
      break;
      case 8:
        if ( random( 14 ) + SinceCut > RV_CUT_AUG ) Cutting( today );
      break;
      case 9:
        if ( random( 14 ) + SinceCut > RV_CUT_SEP ) Cutting( today );
      break;
      case 10:
        if ( random( 14 ) + SinceCut > RV_CUT_OCT ) Cutting( today );
      break;
      default:
      break;
    }
  }
}

void RoadsideVerge::Cutting( int a_today )
{
  SetLastTreatment( mow );
  m_DateCut = a_today;
  m_veg_height = RV_CUT_HEIGHT;
  m_LAgreen = RV_CUT_GREEN;
  m_LAtotal = RV_CUT_TOTAL;
  RecalculateBugsNStuff();
}

WaterBufferZone::WaterBufferZone(Landscape *L) : VegElement(tole_WaterBufferZone, L) {
	this->SetVegType(tov_WaterBufferZone);
	this->Set_Att_VegPatchy(true);
	this->SetVegGrowthScalerRand();
}

void WaterBufferZone::DoDevelopment(void) {
	VegElement::DoDevelopment();
	// Add reseting veg functionality on January 1st
	long today = g_date->DayInYear();

	if (g_date->JanFirst()) {
		// beginning of year so restart the cutting
		ResetingVeg(today);
	}
}

void WaterBufferZone::ResetingVeg(int a_today)
{
	ZeroVeg();
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

MownGrass::MownGrass(Landscape *L) : VegElement(tole_MownGrass, L) {
  this->SetVegType(tov_NaturalGrass);
  if (g_rand_uni() < cfg_MGPatchyChance.value()) 
	  this->Set_Att_VegPatchy(true); 
  m_DateCut = 0;
}

OrchardBand::OrchardBand( Landscape *L ) : VegElement(tole_OrchardBand, L) {
  this->SetVegType(tov_NaturalGrass);
  m_LastSprayed = 99999;
}

Orchard::Orchard(Landscape * L) : VegElement(tole_Orchard, L) {
	SetVegType(tov_NaturalGrass);
	m_DateCut = 0;
}

void Orchard::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  long today = g_date->DayInYear();
  // Spraying
  //int sprayday = cfg_OrchardSprayDay.value();
  int sprayday2 = cfg_pest_productapplic_startdate2.value();
  int sprayday3 = cfg_pest_productapplic_startdate3.value();
  int sprayday=cfg_pest_productapplic_startdate.value();
  //sprayday+=random(cfg_pest_productapplic_period.value());
  if ( ( today == sprayday ) || ( today == sprayday2 ) ||  ( today == sprayday3 ) )
    if ( m_Landscape->SupplyShouldSpray() ) {
      //g_pest->DailyQueueAdd( this, l_pest_insecticide_amount.value() );
	  g_pest->DailyQueueAdd( this, l_pest_productOrchard_amount.value(), ppp_1);
    }
  // Cutting functionality
  if ( g_date->JanFirst() ) {
    // beginning of year so restart the cutting
    m_DateCut = 0;
	PollenNectarReset();
  }

  switch ( cfg_OrchardNoCutsDay.value() ) {
    case 99:
      if ( today == ( sprayday - 7 ) ) Cutting( today );
    break;
    case 4:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) || ( today == 196 ) )
        Cutting( today );
    break;
    case 3:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) ) Cutting( today );
    break;
    case 2:
      if ( ( today == 259 ) || ( today == 122 ) ) Cutting( today );
    break;
    case 1:
      if ( ( today == 259 ) ) Cutting( today );
    break;
    default: // No cut
    break;
  }
}

void Orchard::Cutting( int a_today ) {
  SetLastTreatment( mow );
  SetMownDecay( 12 ); // 12 days of not suitable
  SetGrowthPhase( harvest1 );
  m_DateCut = a_today;
  m_veg_height = l_el_o_cut_height.value();
  m_LAgreen = l_el_o_cut_green.value();
  m_LAtotal = l_el_o_cut_total.value();
}

void MownGrass::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  long today = g_date->DayInYear();
  // Cutting functionality
  if ( g_date->JanFirst() ) {
    // beginning of year so restart the cutting
    m_DateCut = 0;
  }
  switch ( cfg_MownGrassNoCutsDay.value() ) {
    case 99:
		// Use to define special cutting behaviour e.g. cutting every 14 days after 1st May
		if (  ( today >= ( March + 15 ) ) && ( today % 42 == 0 )) {
			if (today < October) Cutting( today );
		}
    break;
	case 9:
		if ((today == 196)) // 15th July
			Cutting(today);
		break;
	case 8:
		if ((today == 259) || (today == 92)) Cutting(today);
		break;
	case 7:
		if ((today == 122)) // 1st May
			Cutting(today);
		break;
	case 6:
		if ((today == 92)) // 1st April
			Cutting(today);
		break;
    case 5:
      if ( ( today == 151 ) ) // 1st June
        Cutting( today );
    break;
    case 4:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) || ( today == 196 ) )
        Cutting( today );
    break;
    case 3:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) ) Cutting( today );
    break;
    case 2:
      if ( ( today == 259 ) || ( today == 122 ) ) Cutting( today );
    break;
    case 1:
      if ( ( today == 259 ) ) Cutting( today ); // 15 September
    break;
    default: // No cut
    break;
  }
}

void MownGrass::Cutting( int a_today ) {
  SetLastTreatment( mow );
  SetMownDecay( 21 ); // 21 days of not suitable
  SetGrowthPhase( harvest1 );
  m_DateCut = a_today;
  m_veg_height = l_el_o_cut_height.value();
  m_LAgreen = l_el_o_cut_green.value();
  m_LAtotal = l_el_o_cut_total.value();
}


void OrchardBand::DoDevelopment( void ) {
	VegElement::DoDevelopment();
	long today = g_date->DayInYear();
	if (m_LastSprayed<today) {
		m_herbicidedelay = today-m_LastSprayed;
		if (m_herbicidedelay > 5) m_herbicidedelay = 5;
			else if (m_herbicidedelay < 0) m_herbicidedelay = 0;
		this->ReduceVeg(0.9);
		if ((today == 0) || (today-m_LastSprayed > 90)) m_LastSprayed = 999999;
	}
  // Spraying
	int sprayday2 = cfg_pest_productapplic_startdate2.value();
	int sprayday3 = cfg_pest_productapplic_startdate3.value();
	int sprayday=cfg_pest_productapplic_startdate.value();
  //sprayday+=random(cfg_pest_productapplic_period.value());
	if ( ( today == sprayday ) || ( today == sprayday2 ) || ( today == sprayday3 ) ) {
		if ( m_Landscape->SupplyShouldSpray() ) {
			g_pest->DailyQueueAdd( this, l_pest_productOrchard_amount.value(), ppp_1);
		}
		m_LastSprayed = today; // Regardless of whether we spray our test compound, we want to have the herbicide effect

	}
}

NaturalGrass::NaturalGrass(TTypesOfLandscapeElement tole, Landscape *L) : VegElement(tole, L) {
	this->SetVegType(tov_NaturalGrass);
	this->Set_Att_VegPatchy(true);
	this->SetVegGrowthScalerRand();
}

void NaturalGrass::DoDevelopment(void) {
	VegElement::DoDevelopment();
	// The assumption is that natural grass has a range of species, which means
	// there should be good food all year - but still it should vary with season
	// So we add a constant to the digestability of 0.2
	m_digestability += 0.2;
	if (m_digestability > 0.8) m_digestability = 0.8;
}

UnsprayedFieldMargin::UnsprayedFieldMargin(Landscape * L) : VegElement(tole_UnsprayedFieldMargin, L) {
  this->SetVegType(tov_NaturalGrass);
  if (random(100) < cfg_UMPatchyChance.value()) 
	  this->Set_Att_VegPatchy(true);
}

void UnsprayedFieldMargin::DoDevelopment( void ) {
/**
* All development is controlled by the controlling field polygon, so do nothing here
*/
	return;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


ForestElement::ForestElement(TTypesOfLandscapeElement tole, Landscape * L) : VegElement(tole, L) {
	;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

NonVegElement::NonVegElement(TTypesOfLandscapeElement tole, Landscape *L) : LE( L ) {
	m_type = tole;
	m_owner_tole = m_type;
}

Pond::Pond( Landscape * L ) : NonVegElement (tole_Pond, L) {
	m_LarvalFood = 0.01;
	m_MaleNewtPresent = false;
	m_LarvalFoodScaler = 0.0;
	m_pondpesticide = 0.0;
	if (cfg_randompondquality.value()) m_pondquality = g_rand_uni(); else m_pondquality = 1.0;
}

void Pond::DoDevelopment()
{
	LE::DoDevelopment();
	CalcPondPesticide();
	CalcLarvalFood();
	m_MaleNewtPresent = false;
}

void Pond::CalcPondPesticide()
{
	/**
	* The pesticide is calculated based on the mean concentration per m2 which is then multiplied by a factor representing run-off from the surroundings
	* or other movements of pesticides through soil into the pond. This method assumes a uniform depth of water, which is then ignored (so can be seen
	* as being part of the run-off factor).
	* The pesticide concentration is calculated each day.
	* These calculations are heavy on CPU time because of the need to search the landscape map for pond 1m2 and sum pesticide. So this has to be set
	* to run by flagging using cfg_calc_pond_pesticide
	*/
	m_pondpesticide = 0.0;
	if (!cfg_calc_pond_pesticide.value())
	{
		return;
	}
	/** 
	* To create the sum of pesticide the map is searched from min x,y to max x,y coords, and pond cells are summed for their pesticide content.
	* Then the mean found.
	*/
	for (int x = m_minx; x <= m_maxx; x++)
	for (int y = m_miny; y <= m_maxy; y++)
	{
		if (m_Landscape->SupplyPolyRef(x, y) == m_poly) m_pondpesticide += m_Landscape->SupplyPesticide(x, y, ppp_1);
	}
	m_pondpesticide /= m_area;
	/** 
	* We assume a mean pond depth of 1 m, so pesticide per l = m_pondpesticide/1000 to get per litre then multiplied buy the run-off factor
	*/
	m_pondpesticide *= cfg_pondpesticiderunoff.value()/1000.0;
}

void Pond::CalcLarvalFood()
{
/**
* The larval food is calculated assuming a logistic equation in the form of Nt+1 = Nt+(N*r * (1-N/K))
* t = one day, N is a scaler which is multiplied by a constant and the area of the pond to get the total larval food, K & r are carrying capacity and instantaneous reproductive rate respectively.
* K can change with season and this is currently hard coded, but could be an input variable later. The values are held in LarvalFoodMonthlyK\n
* The steps in the calculation are:\n
* - Enforce a assumed pond size for newts as maximum 400 m2
* - Ensure we never get zero larval food, so there is always something to grow the curve from.
* - Back calculate the current scaler value. This is needed because between time steps, food may be eaten by larvae. This is done based on the area and a fixed constant held in cfg_PondLarvalFoodBiomassConst
* - Calculate the new scaler based on the logistic equation as described above
* - Re-calculate the new total food biomass based on the area and a fixed constant held in cfg_PondLarvalFoodBiomassConst
*
*/
	double area = m_area;
	if (m_area > 400) area = 400;
	const double LarvalFoodMonthlyK[12] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
	// Back calculate the scaler
	m_LarvalFoodScaler = m_LarvalFood / (cfg_PondLarvalFoodBiomassConst.value() * area * m_pondquality);
	// Calculate the new scaler
	m_LarvalFoodScaler = m_LarvalFoodScaler + (m_LarvalFoodScaler*cfg_PondLarvalFoodR.value() * (1 - (m_LarvalFoodScaler / LarvalFoodMonthlyK[g_date->GetMonth() - 1])));
	// Provide some contest competition by preventing all the food from being used up, leaving minimum 1%
	if (m_LarvalFoodScaler < 0.01) m_LarvalFoodScaler = 0.01;
	// Calculate the new food biomass
	m_LarvalFood = m_LarvalFoodScaler * (cfg_PondLarvalFoodBiomassConst.value() * area * m_pondquality);
}

bool Pond::SubtractLarvalFood(double a_food)
{
	/**
	* If the total amount of food is low then there is a probability test to determine if food can be found. If failed the return code false means no food was removed.
	* If passed then this removes the amount of food passed in a_food and return true.
	*/
	m_LarvalFood -= a_food;
	if (m_LarvalFood < 0) return false;
	return true;
}


LargeRoad::LargeRoad( Landscape *L ) : NonVegElement(tole_LargeRoad, L) {
	;
}


double LargeRoad::GetTrafficLoad( void ) {
  return m_largeroad_load[ g_date->GetHour() ] * m_monthly_traffic[ g_date->GetMonth() - 1 ];
}


SmallRoad::SmallRoad( Landscape *L  ) : NonVegElement(tole_SmallRoad, L) {
	;
}


double SmallRoad::GetTrafficLoad( void ) {
  return m_smallroad_load[ g_date->GetHour() ] * m_monthly_traffic[ g_date->GetMonth() - 1 ];
}


LE_TypeClass* CreateLETypeClass()
{
	if (g_letype == NULL)
	{
		g_letype = new LE_TypeClass;
	}

	return g_letype;
}
//--------------------------------------------------------------------
// created 25/08/00
TTypesOfLandscapeElement LE_TypeClass::TranslateEleTypes( int EleReference ) {
  // This returns the vegetation type (or crop type) as applicable
  switch ( EleReference ) {
  case	5:	return	tole_Building;
  case	8:	return	tole_UrbanNoVeg;
  case	9:	return	tole_UrbanVeg;
  case	11:	return	tole_Garden;
  case	12:	return	tole_AmenityGrass;
  case	13:	return	tole_RoadsideVerge;
  case	14:	return	tole_Parkland;
  case	15:	return	tole_StoneWall;
  case	16:	return	tole_BuiltUpWithParkland;
  case	17:	return	tole_UrbanPark;
  case	20:	return	tole_Field;
  case	27:	return	tole_PermPastureTussocky;
  case	26:	return	tole_PermPastureLowYield;
  case	31:	return	tole_UnsprayedFieldMargin;
  case	33:	return	tole_PermanentSetaside;
  case	35:	return	tole_PermPasture;
  case	40:	return	tole_DeciduousForest;
  case	41:	return	tole_Copse;
  case	50:	return	tole_ConiferousForest;
  case	55:	return	tole_YoungForest;
  case	56:	return	tole_Orchard;
  case	69:	return	tole_BareRock;
  case	57:	return	tole_OrchardBand;
  case	58:	return	tole_MownGrass;
  case	60:	return	tole_MixedForest;
  case	70:	return	tole_Scrub;
  case	75:	return	tole_PitDisused;
  case	80:	return	tole_Saltwater;
  case	90:	return	tole_Freshwater;
  case	94:	return	tole_Heath;
  case	95:	return	tole_Marsh;
  case	96:	return	tole_River;
  case	97:	return	tole_RiversideTrees;
  case	98:	return	tole_RiversidePlants;
  case	100:	return	tole_Coast;
  case	101:	return	tole_SandDune;
  case	110:	return	tole_NaturalGrassDry;
  case	115:	return	tole_ActivePit;
  case	118:	return	tole_Railway;
  case	121:	return	tole_LargeRoad;
  case	122:	return	tole_SmallRoad;
  case	123:	return	tole_Track;
  case	130:	return	tole_Hedges;
  case	140:	return	tole_HedgeBank;
  case	141:	return	tole_BeetleBank;
  case	150:	return	tole_Chameleon;
  case	160:	return	tole_FieldBoundary;
  case	201:	return	tole_RoadsideSlope;
  case	202:	return	tole_MetalledPath;
  case	203:	return	tole_Carpark;
  case	204:	return	tole_Churchyard;
  case	205:	return	tole_NaturalGrassWet;
  case	206:	return	tole_Saltmarsh;
  case	207:	return	tole_Stream;
  case	208:	return	tole_HeritageSite;
  case	209:	return	tole_Wasteland;
  case	210:	return	tole_UnknownGrass;
  case	211:	return	tole_WindTurbine;
  case	212:	return	tole_Pylon;
  case	213:	return	tole_IndividualTree;
  case	214:	return	tole_PlantNursery;
  case	215:	return	tole_Vildtager;
  case	216:	return	tole_WoodyEnergyCrop;
  case	217:	return	tole_WoodlandMargin;
  case	218:	return	tole_PermPastureTussockyWet;
  case	219:	return	tole_Pond;
  case	220:	return	tole_FishFarm;
  case	221:	return	tole_RiverBed;
  case	222:	return	tole_DrainageDitch;
  case	223:	return	tole_Canal;
  case	224:	return	tole_RefuseSite;
  case	225:	return	tole_Fence;
  case	226:	return	tole_WaterBufferZone;
  case	2112:	return	tole_Missing;
  // adding new elements ( year 2021 )
  case  300:	return  tole_Airport;
  case  301:	return  tole_Portarea;
  case  302:	return  tole_Saltpans;
  case  303:	return  tole_Pipeline;
  case  304:	return  tole_SolarPanel;
  // adding new forests 
  case  400:	return  tole_SwampForest;
  case  401:	return  tole_MontadoCorkOak;
  case  402:	return  tole_MontadoHolmOak;
  case  403:	return  tole_MontadoMixed;
  case  404:	return  tole_AgroForestrySystem;
  case  405:	return  tole_CorkOakForest;
  case  406:	return  tole_HolmOakForest;
  case  407:	return  tole_OtherOakForest;
  case  408:	return  tole_ChestnutForest;
  case  409:	return  tole_EucalyptusForest;
  case  410:	return  tole_InvasiveForest;
  case  411:	return  tole_MaritimePineForest;
  case  412:	return  tole_StonePineForest;
  case  413:	return  tole_ForestAisle;
  // adding new permanent crop fields
  case  500:	return  tole_Vineyard;
  case  501:	return  tole_OliveGrove;
  case  502:    return  tole_RiceField;
  case  503:    return  tole_OOrchard;
  case  504:    return  tole_BushFruit;
  case  505:    return  tole_OBushFruit;
  case  506:    return  tole_ChristmasTrees;
  case  507:    return  tole_OChristmasTrees;
  case  508:    return  tole_EnergyCrop;
  case  509:    return  tole_OEnergyCrop;
  case  510:    return  tole_FarmForest;
  case  511:    return  tole_OFarmForest;
  case  512:    return  tole_PermPasturePigs;
  case  513:    return  tole_OPermPasturePigs;
  case  514:    return  tole_OPermPasture;
  case  515:    return  tole_OPermPastureLowYield;
  case  516:    return  tole_FarmYoungForest;
  case  517:    return  tole_OFarmYoungForest;
  case	518:	return	tole_AlmondPlantation;
  case	519:	return	tole_WalnutPlantation;
  case  520:	return  tole_FarmBufferZone;
  case  521:	return  tole_NaturalFarmGrass;
  case  522:	return  tole_GreenFallow;
  case  523:	return  tole_FarmFeedingGround;
  case  524:	return  tole_FlowersPerm;
  case  525:	return  tole_AsparagusPerm;
  case  526:	return  tole_MushroomPerm;
  case  527:	return  tole_OtherPermCrop;
  case  528:	return  tole_OAsparagusPerm;
      //  case 999: return tole_Foobar;
      // !! type unknown - should not happen
    default:
      g_msg->Warn( WARN_FILE, "LE_TypeClass::TranslateEleTypes(): ""Unknown landscape element type:", int(EleReference));
      exit( 1 );
  }
}



//----------------------------------------------------------------------
// created 24/08/00
TTypesOfVegetation LE_TypeClass::TranslateVegTypes( int VegReference ) {
  // This returns the vegetation type (or crop type) as applicable
  switch ( VegReference ) {
    case 1:
      return tov_SpringBarley;
    case 2:
      return tov_WinterBarley;
    case 3:
      return tov_SpringWheat;
    case 4:
      return tov_WinterWheat;
    case 5:
      return tov_WinterRye;
    case 6:
      return tov_Oats;
    case 7:
      return tov_Triticale;
    case 8:
      return tov_Maize;
    case 13:
      return tov_SpringBarleySeed;
    case 14: return tov_SpringBarleyStrigling;
    case 15: return tov_SpringBarleyStriglingSingle;
    case 16: return tov_SpringBarleyStriglingCulm;
    case 17: return tov_WinterWheatStrigling;
    case 18: return tov_WinterWheatStriglingSingle;
    case 19: return tov_WinterWheatStriglingCulm;
    case 21:
      return tov_SpringRape;
    case 22:
      return tov_WinterRape;
    case 30:
      return tov_FieldPeas;
	case 31:
		return tov_FieldPeasSilage; //ok?
	case 32:
		return tov_BroadBeans;
	case 50:
      return tov_SetAside;
    case 54:
      return tov_PermanentSetAside;
    case 55:
      return tov_YoungForest;
	case 60:
		return tov_FodderBeet;
	case 61:
		return tov_SugarBeet;
	case 65:
      return tov_CloverGrassGrazed1;
    case 92:
      return tov_PotatoesIndustry;
    case 93:
      return tov_Potatoes;
    case 94:
      return tov_SeedGrass1;
    case 102:
      return tov_OWinterBarley;
    case 611:
      return tov_OWinterBarleyExt;
    case 103:
      return tov_OSBarleySilage;
    case 105:
      return tov_OWinterRye;
    case 106:
      return tov_OFieldPeasSilage;
    case 107:
      return tov_SpringBarleyGrass;
	case 108:
		return tov_SpringBarleyCloverGrass;
	case 109:
		return tov_SpringBarleySpr;
	case 113:
      return tov_OBarleyPeaCloverGrass;
    case 114:
      return tov_SpringBarleyPeaCloverGrassStrigling;
    case 115:
      return tov_SpringBarleySilage;
    case 122:
      return tov_OWinterRape;
    case 140:
      return tov_PermanentGrassGrazed;
    case 141:
      return tov_PermanentGrassLowYield;
    case 142:
      return tov_PermanentGrassTussocky;
    case 165:
      return tov_CloverGrassGrazed2;
    case 194:
      return tov_SeedGrass2;
    case 201:
      return tov_OSpringBarley;
	case 204:
		return tov_OWinterWheatUndersown;
	case 205:
		return tov_OWinterWheat;
	case 206:
      return tov_OOats;
    case 207:
      return tov_OTriticale;
	case 230:
		return tov_OFieldPeas;
	case 260:
		return tov_OFodderBeet;
	case 265:
      return tov_OCloverGrassGrazed1;
    case 270:
      return tov_OCarrots;
    case 271:
      return tov_Carrots;
	case 272:
		return tov_Wasteland;
    case 273:
      return tov_OGrazingPigs;
    case 293:
      return tov_OPotatoes;
    case 294:
      return tov_OSeedGrass1;
	case 306:
		return tov_OSpringBarleyPigs;
	case 307:
      return tov_OSpringBarleyGrass;
    case 308:
      return tov_OSpringBarleyClover;
    case 340:
      return tov_OPermanentGrassGrazed;
    case 365:
      return tov_OCloverGrassGrazed2;
	case 366:
		return tov_OCloverGrassSilage1;
    case 394:
      return tov_OSeedGrass2;
    case 400:
      return tov_NaturalGrass;
    case 401:
      return tov_None;
	case 402:
		return tov_NoGrowth;
	case 601:
      return tov_WWheatPControl;
    case 602:
      return tov_WWheatPToxicControl;
    case 603:
      return tov_WWheatPTreatment;
    case 604:
      return tov_AgroChemIndustryCereal;
    case 605:
      return tov_WinterWheatShort;
	case 606:
	  return tov_MaizeSilage;
	case 607:
		return tov_FodderGrass;
	case 608:
		return tov_SpringBarleyPTreatment;
    case 609:
      return tov_OSpringBarleyExt;
	case 610:
	  return tov_OMaizeSilage;
	case 612:
		return tov_SpringBarleySKManagement;
	case 613:
		return tov_Heath;
	case 700:
	  return tov_OrchardCrop;
	case 701:
		return tov_WaterBufferZone;

	case 801:
		return tov_PLWinterWheat;
	case 802:
		return tov_PLWinterRape;
	case 803:
		return tov_PLWinterBarley;
	case 804:
		return tov_PLWinterTriticale;
	case 805:
		return tov_PLWinterRye;
	case 806:
		return tov_PLSpringWheat;
	case 807:
		return tov_PLSpringBarley;
	case 808:
		return tov_PLMaize;
	case 809:
		return tov_PLMaizeSilage;
	case 810:
		return tov_PLPotatoes;
	case 811:
		return tov_PLBeet;
	case 812:
		return tov_PLFodderLucerne1;
	case 813:
		return tov_PLFodderLucerne2;
	case 814:
		return tov_PLCarrots;
	case 815:
		return tov_PLSpringBarleySpr;
	case 816:
		return tov_PLWinterWheatLate;
	case 817:
		return tov_PLBeetSpr;
	case 818:
		return tov_PLBeans;

	case 850:
		return tov_NLBeet;
	case 851:
		return tov_NLCarrots;
	case 852:
		return tov_NLMaize;
	case 853:
		return tov_NLPotatoes;
	case 854:
		return tov_NLSpringBarley;
	case 855:
		return tov_NLWinterWheat;
	case 856:
		return tov_NLCabbage;
	case 857:
		return tov_NLTulips;
	case 858:
		return tov_NLGrassGrazed1;
	case 859:
		return tov_NLGrassGrazed2;
	case 860:
		return tov_NLPermanentGrassGrazed;
	case 861:
		return tov_NLCatchCropPea;
	case 862:
		return tov_NLBeetSpring;
	case 863:
		return tov_NLCarrotsSpring;
	case 864:
		return tov_NLMaizeSpring;
	case 865:
		return tov_NLPotatoesSpring;
	case 866:
		return tov_NLSpringBarleySpring;
	case 867:
		return tov_NLCabbageSpring;
	case 868:
		return tov_NLGrassGrazed1Spring;
	case 869:
		return tov_NLGrassGrazedLast;
	case 870:
		return tov_NLOrchardCrop;
	case 871:
		return tov_NLPermanentGrassGrazedExtensive;
	case 872:
		return tov_NLGrassGrazedExtensive1;
	case 873:
		return tov_NLGrassGrazedExtensive2;
	case 874:
		return tov_NLGrassGrazedExtensive1Spring;
	case 875:
		return tov_NLGrassGrazedExtensiveLast;

	case 900:
		return tov_PTPermanentGrassGrazed;
	case 901:
		return tov_PTWinterWheat;
	case 902:
		return tov_PTGrassGrazed;
	case 903:
		return tov_PTSorghum;
	case 904:
		return tov_PTFodderMix;
	case 905:
		return tov_PTTurnipGrazed;
	case 906:
		return tov_PTCloverGrassGrazed1;
	case 907:
		return tov_PTCloverGrassGrazed2;
	case 908:
		return tov_PTTriticale;
	case 909:
		return tov_PTOtherDryBeans;
	case 910:
		return tov_PTShrubPastures;
	case 911:
		return tov_PTCorkOak;
	case 912:
		return tov_PTVineyards;
	case 913:
		return tov_PTWinterBarley;
	case 914:
		return tov_PTBeans;
	case 915:
		return tov_PTWinterRye;
	case 921:
		return tov_PTRyegrass;
	case 922:
		return tov_PTYellowLupin;
	case 923:
		return tov_PTMaize;
	case 924:
		return tov_PTOats;
	case 925:
		return tov_PTPotatoes;
	case 926:
		return tov_PTHorticulture;

	case 950:
		return tov_DEOats;
	case 951:
		return tov_DESpringRye;
	case 952:
		return tov_DEWinterWheat;
	case 953:
		return tov_DEMaizeSilage;
	case 954:
		return tov_DEPotatoes;
	case 955:
		return tov_DEMaize;
	case 956:
		return tov_DEWinterRye;
	case 957:
		return tov_DEWinterBarley;
	case 958:
		return tov_DESugarBeet;
	case 959:
		return tov_DEWinterRape;
	case 960:
		return tov_DETriticale;
	case	961:
		return tov_DECabbage;
	case	962:
		return tov_DECarrots;
	case	963:
		return tov_DEGrasslandSilageAnnual;
	case	964:
		return tov_DEGreenFallow_1year;
	case	965:
		return tov_DELegumes;
	case	966:
		return tov_DEOCabbages;
	case	967:
		return tov_DEOCarrots;
	case	968:
		return tov_DEOGrasslandSilageAnnual;
	case	969:
		return tov_DEOGreenFallow_1year;
	case	970:
		return tov_DEOLegume;
	case	971:
		return tov_DEOMaize;
	case	972:
		return tov_DEOMaizeSilage;
	case	973:
		return tov_DEOOats;
	case	974:
		return tov_DEOPermanentGrassGrazed;
	case	975:
		return tov_DEOPotatoes;
	case	976:
		return tov_DEOSpringRye;
	case	977:
		return tov_DEOSugarBeet;
	case	978:
		return tov_DEOTriticale;
	case	979:
		return tov_DEOWinterBarley;
	case	980:
		return tov_DEOWinterRape;
	case	981:
		return tov_DEOWinterRye;
	case	982:
		return tov_DEOWinterWheat;
	case	983:
		return tov_DEWinterWheatLate;
	case	984:
		return tov_DEPermanentGrassGrazed;
	case	985:
		return tov_DEPermanentGrassLowYield;
	case	986:
		return tov_DEPotatoesIndustry;
	case	987:
		return tov_DEPeas;
	case	988:
		return tov_DEOPeas;
	case	989:
		return tov_DEAsparagusEstablishedPlantation;
	case	990:
		return tov_DEHerbsPerennial_1year;
	case	991:
		return tov_DEHerbsPerennial_after1year;
	case	992:
		return tov_DESpringBarley;
	case	993:
		return tov_DEOrchard;
	case	994:
		return tov_DEBushFruitPerm;
	case	995:
		return tov_DEOAsparagusEstablishedPlantation;
	case	996:
		return tov_DEOOrchard;
	case	997:
		return tov_DEOBushFruitPerm;
	case	998:
		return tov_DEOPermanentGrassLowYield;
	case	999:
		return tov_DEOHerbsPerennial_1year;
	case	1000:
		return tov_DEOHerbsPerennial_after1year;

		
	case 1001: 
		return tov_DKOOrchardCrop_Perm;
	case 1002:
		return tov_DKOBushFruit_Perm1;
	case 1003: 
		return tov_DKOBushFruit_Perm2;
	case 1004: 
		return tov_DKChristmasTrees_Perm;
	case 1005: 
		return tov_DKOChristmasTrees_Perm;
	case 1006: 
		return tov_DKEnergyCrop_Perm;
	case 1007: 
		return tov_DKOEnergyCrop_Perm;
	case 1008: 
		return tov_DKFarmForest_Perm;
	case 1009: 
		return tov_DKOFarmForest_Perm;
	case 1010: 
		return tov_DKGrazingPigs_Perm;
	case 1011: 
		return tov_DKOGrazingPigs_Perm;
	case 1012: 
		return tov_DKOGrassGrazed_Perm;
	case 1013: 
		return tov_DKOGrassLowYield_Perm;
	case 1014: 
		return tov_DKFarmYoungForest_Perm;
	case 1015: 
		return tov_DKOFarmYoungForest_Perm;
	
		//1084-1090 not used as Christmas tree codes are merged

	case 1200:
		return tov_UKBeans;
	case 1201:
		return tov_UKBeet;
	case 1202:
		return tov_UKMaize;
	case 1203:
		return tov_UKPermanentGrass;
	case 1204:
		return tov_UKPotatoes;
	case 1205:
		return tov_UKSpringBarley;
	case 1206:
		return tov_UKTempGrass;
	case 1207:
		return tov_UKWinterBarley;
	case 1208:
		return tov_UKWinterRape;
	case 1209:
		return tov_UKWinterWheat;

	case 1250:
		return tov_BEBeet;
	case 1251:
		return tov_BEBeetSpring;
	case 1252:
		return tov_BECatchPeaCrop;
	case 1253:
		return tov_BEGrassGrazed1;
	case 1254:
		return tov_BEGrassGrazed1Spring;
	case 1255:
		return tov_BEGrassGrazed2;
	case 1256:
		return tov_BEGrassGrazedLast;
	case 1257:
		return tov_BEMaize;
	case 1258:
		return tov_BEMaizeSpring;
	case 1259:
		return tov_BEOrchardCrop;
	case 1260:
		return tov_BEPotatoes;
	case 1261:
		return tov_BEPotatoesSpring;
	case 1262:
		return tov_BEWinterBarley;
	case 1263:
		return tov_BEWinterWheat;
	case 1264:
		return tov_BEWinterWheatCC;
	case 1265:
		return tov_BEWinterBarleyCC;
	case 1266:
		return tov_BEMaizeCC;

	case 1016: 
		return tov_DKOLegume_Peas;
	case 1017: 
		return tov_DKOLegume_Whole;
	case 1018: 
		return tov_DKSugarBeets;
	case 1019: 
		return tov_DKOSugarBeets;
	case 1020: 
		return tov_DKCabbages;
	case 1021: 
		return tov_DKOCabbages;
	case 1022: 
		return tov_DKCarrots;
	case 1023: 
		return tov_DKOCarrots;
	case 1024: 
		return tov_DKLegume_Whole;
	case 1025: 
		return tov_DKLegume_Peas;
	case 1026: 
		return tov_DKWinterWheat;
	case 1027:
		return tov_DKOWinterWheat;
	case 1028:
		return tov_DKSpringBarley;
	case 1029:
		return tov_DKOSpringBarley;
	case 1030:
		return tov_DKCerealLegume;
	case 1031:
		return tov_DKOCerealLegume;
	case 1032:
		return tov_DKCerealLegume_Whole;
	case 1033:
		return tov_DKOCerealLegume_Whole;
	case 1034:
		return tov_DKBushFruit_Perm1;
	case 1035:
		return tov_DKBushFruit_Perm2;
	case 1036:
		return tov_DKSpringFodderGrass;
	case 1037:
		return tov_DKOWinterFodderGrass;
	case 1038:
		return tov_DKGrassGrazed_Perm;
	case 1039:
		return tov_DKGrassLowYield_Perm;
	case 1040:
		return tov_DKGrazingPigs;
	case 1041:
		return tov_DKMaize;
	case 1042:
		return tov_DKMaizeSilage;
	case 1043:
		return tov_DKMixedVeg;
	case 1044:
		return tov_DKOGrazingPigs;
	case 1045:
		return tov_DKOMaize;
	case 1046:
		return tov_DKOMaizeSilage;
	case 1047:
		return tov_DKOMixedVeg;
	case 1048:
		return tov_DKOPotato;
	case 1049:
		return tov_DKOPotatoIndustry;
	case 1050:
		return tov_DKOPotatoSeed;
	case 1051:
		return tov_DKOrchardCrop_Perm;
	case 1052:
		return tov_DKOSeedGrassRye_Spring;
	case 1053:
		return tov_DKOSetAside;
	case 1054:
		return tov_DKOSpringBarleySilage;
	case 1055:
		return tov_DKOSpringOats;
	case 1056:
		return tov_DKOSpringWheat;
	case 1057:
		return tov_DKOVegSeeds;
	case 1058:
		return tov_DKOWinterBarley;
	case 1059:
		return tov_DKOWinterRape;
	case 1060:
		return tov_DKOWinterRye;
	case 1061:
		return tov_DKPlantNursery_Perm;
	case 1062:
		return tov_DKPotato;
	case 1063:
		return tov_DKPotatoIndustry;
	case 1064:
		return tov_DKPotatoSeed;
	case 1065:
		return tov_DKSeedGrassFescue_Spring;
	case 1066:
		return tov_DKSeedGrassRye_Spring;
	case 1067:
		return tov_DKSetAside;
	case 1068:
		return tov_DKSetAside_SummerMow;
	case 1069:
		return tov_DKSpringBarley_Green;
	case 1070:
		return tov_DKSpringBarleySilage;
	case 1071:
		return tov_DKSpringOats;
	case 1072:
		return tov_DKSpringWheat;
	case 1073:
		return tov_DKUndefined;
	case 1074:
		return tov_DKVegSeeds;
	case 1075:
		return tov_DKWinterBarley;
	case 1076:
		return tov_DKWinterRape;
	case 1077:
		return tov_DKWinterRye;
	case 1078:
		return tov_DKOWinterCloverGrassGrazedSown;
	case 1079:
		return tov_DKOCloverGrassGrazed1;
	case 1080:
		return tov_DKOCloverGrassGrazed2;
	case 1081:
		return tov_DKCloverGrassGrazed1;
	case 1082:
		return tov_DKWinterCloverGrassGrazedSown;
	case 1083:
		return tov_DKCloverGrassGrazed2;
	case 1091:
		return tov_DKOLegumeCloverGrass_Whole;
	case 1092:
		return tov_DKLegume_Beans;
	case 1093:
		return tov_DKWinterFodderGrass;
	case 1094:
		return tov_DKOrchApple;
	case 1095:
		return tov_DKOrchPear;
	case 1096:
		return tov_DKOrchCherry;
	case 1097:
		return tov_DKOrchOther;
	case 1098:
		return tov_DKOOrchApple;
	case 1099:
		return tov_DKOOrchPear;
	case 1100:
		return tov_DKOOrchCherry;
	case 1101:
		return tov_DKOOrchOther;
	case 1102:
		return tov_DKFodderBeets;
	case 1103:
		return tov_DKOLegume_Beans;
	case 1104:
		return tov_DKOFodderBeets;
	case 1105:
		return tov_DKOSpringFodderGrass;
	case 1106:
		return tov_DKCatchCrop;
	case 1107:
		return tov_DKOCatchCrop;
	case 1108:
		return tov_DKSpringBarleyCloverGrass;
	case 1109:
		return tov_DKOSpringBarleyCloverGrass;
	case 1110:
		return tov_DKWinterWheat_CC;
	case 1111:
		return tov_DKWinterRye_CC;
	case 1112:
		return tov_DKSpringOats_CC;
	case 1113:
		return tov_DKSpringBarley_CC;
	case 1114:
		return tov_DKOWinterWheat_CC;
	case 1115:
		return tov_DKOWinterRye_CC;
	case 1116:
		return tov_DKOSpringOats_CC;
	case 1117:
		return tov_DKOSpringBarley_CC;
	case 1118:
		return tov_DKOLegume_Beans_CC;
	case 1119:
		return tov_DKOLegume_Peas_CC;
	case 1120:
		return tov_DKOLegume_Whole_CC;
	case 1121:
		return tov_DKOLupines;
	case 1122:
		return tov_DKOLentils;
	case 1123:
		return tov_DKOSetAside_AnnualFlower;
	case 1124:
		return tov_DKOSetAside_PerennialFlower;
	case 1125:
		return tov_DKOSetAside_SummerMow;

	case 1300:
		return tov_SESpringBarley;
	case 1301:
		return tov_SEWinterRape_Seed;
	case 1302:
		return tov_SEWinterWheat;
	case 1400:
		return tov_FIWinterWheat;
	case 1401:
		return tov_FIOWinterWheat;
	case 1402:
		return tov_FISugarBeet;
	case 1403:
		return tov_FIStarchPotato_North;
	case 1404:
		return tov_FIStarchPotato_South;
	case 1405:
		return tov_FIOStarchPotato_North;
	case 1406:
		return tov_FIOStarchPotato_South;
	case 1407:
		return tov_FISpringWheat;
	case 1408:
		return tov_FIOSpringWheat;
	case 1409:
	    return tov_FITurnipRape;
	case 1410:
		return tov_FIOTurnipRape;
	case 1411:
		return tov_FISpringRape;
	case 1412:
		return tov_FIOSpringRape;
	case 1413:
		return tov_FIWinterRye;
	case 1414:
		return tov_FIOWinterRye;
	case 1415:
		return tov_FIPotato_North;
	case 1416:
		return tov_FIPotato_South;
	case 1417:
		return tov_FIOPotato_North;
	case 1418:
		return tov_FIOPotato_South;
	case 1419:
		return tov_FIPotatoIndustry_North;
	case 1420:
		return tov_FIPotatoIndustry_South;
	case 1421:
		return tov_FIOPotatoIndustry_North;
	case 1422:
		return tov_FIOPotatoIndustry_South;
	case 1423:
		return tov_FISpringOats;
	case 1424:
		return tov_FIOSpringOats;
	case 1425:
		return tov_FISpringBarley_Malt;
	case 1426:
		return tov_FIOSpringBarley_Malt;
	case 1427:
		return tov_FIFabaBean;
	case 1428:
		return tov_FIOFabaBean;
	case 1429:
		return tov_FISpringBarley_Fodder;
	case 1430:
		return tov_FIOSpringBarley_Fodder;
	case 1431:
		return tov_FIGrasslandPasturePerennial1;
	case 1432:
		return tov_FIGrasslandPasturePerennial2;
	case 1433:
		return tov_FIGrasslandSilagePerennial1;
	case 1434:
		return tov_FIGrasslandSilagePerennial2;
	case 1435:
		return tov_FINaturalGrassland;
	case 1436:
		return tov_FINaturalGrassland_Perm;
	case 1437:
		return tov_FIFeedingGround;
	case 1438:
		return tov_FIBufferZone;
	case 1439:
		return tov_FIBufferZone_Perm;
	case 1440:
		return tov_FIGreenFallow_1year;
	case 1441:
		return tov_FIGreenFallow_Perm;
	case 1442:
		return tov_FIGrasslandSilageAnnual;
	case 1443:
		return tov_FICaraway1;
	case 1444:
		return tov_FICaraway2;
	case 1445:
		return tov_FIOCaraway1;
	case 1446:
		return tov_FIOCaraway2;
	case 1447:
		return tov_FISprSpringBarley_Fodder;
	case 1500:
		return tov_IRSpringWheat;
	case 1501:
		return tov_IRSpringBarley;
	case 1502:
		return tov_IRSpringOats;
	case 1503:
		return tov_IRGrassland_no_reseed;
	case 1504:
		return tov_IRGrassland_reseed;
	case 1505:
		return tov_IRWinterBarley;
	case 1506:
		return tov_IRWinterWheat;
	case 1507:
		return tov_IRWinterOats;
	case 1600:
		return tov_FRWinterWheat;
	case 1601:
		return tov_FRWinterBarley;
	case 1602:
		return tov_FRWinterTriticale;
	case 1603:
		return tov_FRWinterRape;
	case 1604:
		return tov_FRMaize;
	case 1605:
		return tov_FRMaize_Silage;
	case 1606:
		return tov_FRSpringBarley;
	case 1607:
		return tov_FRGrassland;
	case 1608:
		return tov_FRGrassland_Perm;
	case 1609:
		return tov_FRSpringOats;
	case 1610:
		return tov_FRSunflower;
	case 1611:
		return tov_FRSpringWheat;
	case 1612:
		return tov_FRPotatoes;
	case 1613:
		return tov_FRSorghum;
	case 88:
		return tov_DummyCropPestTesting;
	case 1700:
		return tov_ITGrassland;
	case 1701:
		return tov_ITOrchard;
	case 1702:
		return tov_ITOOrchard;

    case 9999:
      return tov_Undefined;
    default: // No matching code so we need an error message of some kind
      g_msg->Warn( WARN_FILE, "LE_TypeClass::TranslateVegTypes(): ""Unknown vegetation type:", VegReference);
      exit( 1 );
  }
}

//-----------------------------------------------------------------------
// created 25/08/00
int LE_TypeClass::BackTranslateVegTypes( TTypesOfVegetation VegReference ) {
  // This returns the vegetation type (or crop type) as applicable
  switch ( VegReference ) {
    case tov_SpringBarley:
      return 1;
    case tov_WinterBarley:
      return 2;
    case tov_SpringWheat:
      return 3;
    case tov_WinterWheat:
      return 4;
    case tov_WinterRye:
      return 5;
    case tov_Oats:
      return 6;
    case tov_Triticale:
      return 7;
    case tov_Maize:
      return 8;
    case tov_SpringBarleySeed:
      return 13;
    case tov_SpringBarleyStrigling:
      return 14;
    case tov_SpringBarleyStriglingSingle:
      return 15;
    case tov_SpringBarleyStriglingCulm:
      return 16;
    case tov_WinterWheatStrigling:
		return 17;
    case tov_WinterWheatStriglingSingle:
		return 18;
    case tov_WinterWheatStriglingCulm:
		return 19;
    case tov_SpringRape:
      return 21;
    case tov_WinterRape:
      return 22;
    case tov_FieldPeas:
      return 30;
	case tov_FieldPeasSilage:
		return 31;
	case tov_BroadBeans:
		return 32;
    case tov_SetAside:
      return 50;
    case tov_PermanentSetAside:
      return 54;
    case tov_YoungForest:
      return 55;
	case tov_FodderBeet:
		return 60;
	case tov_SugarBeet:
		return 61;
	case tov_CloverGrassGrazed1:
      return 65;
    case tov_PotatoesIndustry:
      return 92;
    case tov_Potatoes:
      return 93;
    case tov_SeedGrass1:
      return 94;
    case tov_OWinterBarley:
      return 102;
    case tov_OWinterBarleyExt:
      return 611;
    case tov_OWinterRye:
      return 105;
    case tov_SpringBarleyGrass:
      return 107;
    case tov_SpringBarleyCloverGrass:
      return 108;
	case tov_SpringBarleySpr:
		return 109;
	case tov_OSBarleySilage:
		return 103;
	case tov_OBarleyPeaCloverGrass:
      return 113;
    case tov_SpringBarleyPeaCloverGrassStrigling:
      return 114;
    case tov_SpringBarleySilage:
      return 115;
    case tov_OWinterRape:
      return 122;
    case tov_PermanentGrassGrazed:
      return 140;
    case tov_PermanentGrassLowYield:
      return 141;
    case tov_PermanentGrassTussocky:
      return 142;
    case tov_CloverGrassGrazed2:
      return 165;
    case tov_SeedGrass2:
      return 194;
    case tov_OSpringBarley:
      return 201;
	case tov_OWinterWheatUndersown:
		return 204;
	case tov_OWinterWheat:
		return 205;
	case tov_OOats:
      return 206;
    case tov_OTriticale:
      return 207;
    case tov_OFieldPeas:
      return 230;
    case tov_OFieldPeasSilage:
      return 106;
	case tov_OFodderBeet:
		return 260;
	case tov_OCloverGrassGrazed1:
      return 265;
    case tov_OCarrots:
      return 270;
    case tov_Carrots:
      return 271;
    case tov_OPotatoes:
      return 293;
    case tov_OSeedGrass1:
      return 294;
	case tov_OSpringBarleyPigs:
	  return 306;
	case tov_OSpringBarleyGrass:
      return 307;
	case tov_OSpringBarleyClover:
      return 308;
    case tov_OPermanentGrassGrazed:
      return 340;
    case tov_OCloverGrassGrazed2:
      return 365;
	case tov_OCloverGrassSilage1:
		return 366;
    case tov_OSeedGrass2:
      return 394;
    case tov_NaturalGrass:
      return 400;
    case tov_None:
      return 401;
    case tov_NoGrowth:
      return 402;
    case tov_WWheatPControl:
      return 601;
    case tov_WWheatPToxicControl:
      return 602;
    case tov_WWheatPTreatment:
      return 603;
    case tov_AgroChemIndustryCereal:
      return 604;
    case tov_WinterWheatShort:
      return 605;
    case tov_MaizeSilage:
      return 606;
	case tov_FodderGrass:
		return 607;
    case tov_SpringBarleyPTreatment:
      return 608;
    case tov_OSpringBarleyExt:
      return 609;
    case tov_OMaizeSilage:
      return 610;
	case tov_SpringBarleySKManagement:
		return 612;
	case tov_Heath:
		return 613;
	case tov_OrchardCrop:
		return 700;
	case tov_WaterBufferZone:
		return 701;

	case tov_PLWinterWheat:
		return 801;
	case tov_PLWinterRape:
		return 802;
	case tov_PLWinterBarley:
		return 803;
	case tov_PLWinterTriticale:
		return 804;
	case tov_PLWinterRye:
		return 805;
	case tov_PLSpringWheat:
		return 806;
	case tov_PLSpringBarley:
		return 807;
	case tov_PLMaize:
		return 808;
	case tov_PLMaizeSilage:
		return 809;
	case tov_PLPotatoes:
		return 810;
	case tov_PLBeet:
		return 811;
	case tov_PLFodderLucerne1:
		return 812;
	case tov_PLFodderLucerne2:
		return 813;
	case tov_PLCarrots:
		return 814;
	case tov_PLSpringBarleySpr:
		return 815;
	case tov_PLWinterWheatLate:
		return 816;
	case tov_PLBeetSpr:
		return 817;
	case tov_PLBeans:
		return 818;

	case tov_NLBeet:
		return 850;
	case tov_NLCarrots:
		return 851;
	case tov_NLMaize:
		return 852;
	case tov_NLPotatoes:
		return 853;
	case tov_NLSpringBarley:
		return 854;
	case tov_NLWinterWheat:
		return 855;
	case tov_NLCabbage:
		return 856;
	case tov_NLTulips:
		return 857;
	case tov_NLGrassGrazed1:
		return 858;
	case tov_NLGrassGrazed2:
		return 859;
	case tov_NLPermanentGrassGrazed:
		return 860;
	case tov_NLCatchCropPea:
		return 861;
	case tov_NLBeetSpring:
		return 862;
	case tov_NLCarrotsSpring:
		return 863;
	case tov_NLMaizeSpring:
		return 864;
	case tov_NLPotatoesSpring:
		return 865;
	case tov_NLSpringBarleySpring:
		return 866;
	case tov_NLCabbageSpring:
		return 867;
	case tov_NLGrassGrazed1Spring:
		return 868;
	case tov_NLGrassGrazedLast:
		return 869;
	case tov_NLOrchardCrop:
		return 870;
	case tov_NLPermanentGrassGrazedExtensive:
		return 871;
	case tov_NLGrassGrazedExtensive1:
		return 872;
	case tov_NLGrassGrazedExtensive2:
		return 873;
	case tov_NLGrassGrazedExtensive1Spring:
		return 874;
	case tov_NLGrassGrazedExtensiveLast:
		return 875;

	case tov_PTPermanentGrassGrazed:
		return 900;
	case tov_PTWinterWheat:
		return 901;
	case tov_PTGrassGrazed:
		return 902;
	case tov_PTSorghum:
		return 903;
	case tov_PTFodderMix:
		return 904;
	case tov_PTTurnipGrazed:
		return 905;
	case tov_PTCloverGrassGrazed1:
		return 906;
	case tov_PTCloverGrassGrazed2:
		return 907;
	case tov_PTTriticale:
		return 908;
	case tov_PTOtherDryBeans:
		return 909;
	case tov_PTShrubPastures:
		return 910;
	case tov_PTCorkOak:
		return 911;
	case tov_PTVineyards:
		return 912;
	case tov_PTWinterBarley:
		return 913;
	case tov_PTBeans:
		return 914;
	case tov_PTWinterRye:
		return 915;
	case tov_PTRyegrass:
		return 921;
	case tov_PTYellowLupin:
		return 922;
	case tov_PTMaize:
		return 923;
	case tov_PTOats:
		return 924;
	case tov_PTPotatoes:
		return 925;
	case tov_PTHorticulture:
		return 926;

	case tov_DEOats:
		return 950;
	case tov_DESpringRye:
		return 951;
	case tov_DEWinterWheat:
		return 952;
	case tov_DEMaizeSilage:
		return 953;
	case tov_DEPotatoes:
		return 954;
	case tov_DEMaize:
		return 955;
	case tov_DEWinterRye:
		return 956;
	case tov_DEWinterBarley:
		return 957;
	case tov_DESugarBeet:
		return 958;
	case tov_DEWinterRape:
		return 959;
	case tov_DETriticale:
		return 960;
	case tov_DECabbage:
		return 961;
	case tov_DECarrots:
		return 962;
	case tov_DEGrasslandSilageAnnual:
		return 963;
	case tov_DEGreenFallow_1year:
		return 964;
	case tov_DELegumes:
		return 965;
	case tov_DEOCabbages:
		return 966;
	case tov_DEOCarrots:
		return 967;
	case tov_DEOGrasslandSilageAnnual:
		return 968;
	case tov_DEOGreenFallow_1year:
		return 969;
	case tov_DEOLegume:
		return 970;
	case tov_DEOMaize:
		return 971;
	case tov_DEOMaizeSilage:
		return 972;
	case tov_DEOOats:
		return 973;
	case tov_DEOPermanentGrassGrazed:
		return 974;
	case tov_DEOPotatoes:
		return 975;
	case tov_DEOSpringRye:
		return 976;
	case tov_DEOSugarBeet:
		return 977;
	case tov_DEOTriticale:
		return 978;
	case tov_DEOWinterBarley:
		return 979;
	case tov_DEOWinterRape:
		return 980;
	case tov_DEOWinterRye:
		return 981;
	case tov_DEOWinterWheat:
		return 982;
	case tov_DEWinterWheatLate:
		return 983;
	case tov_DEPermanentGrassGrazed:
		return 984;
	case tov_DEPermanentGrassLowYield:
		return 985;
	case tov_DEPotatoesIndustry:
		return 986;
	case tov_DEPeas:
		return 987;
	case tov_DEOPeas:
		return 988;
	case tov_DEAsparagusEstablishedPlantation:
		return 989;
	case tov_DEHerbsPerennial_1year:
		return 990;
	case tov_DEHerbsPerennial_after1year:
		return 991;
	case tov_DESpringBarley:
		return 992;
	case tov_DEOrchard:
		return 993;
	case tov_DEBushFruitPerm:
		return 994;
	case tov_DEOAsparagusEstablishedPlantation:
		return 995;
	case tov_DEOOrchard:
		return 996;
	case tov_DEOBushFruitPerm:
		return 997;
	case tov_DEOPermanentGrassLowYield:
		return 998;
	case tov_DEOHerbsPerennial_1year:
		return 999;
	case tov_DEOHerbsPerennial_after1year:
		return 1000;

	case tov_OGrazingPigs:
		return 271;
	case tov_Wasteland:
		return 272;
	case tov_DummyCropPestTesting:
		return 888;
	case tov_DKOOrchardCrop_Perm:     
		return   1001;
	case tov_DKOBushFruit_Perm1:
		return   1002;
	case tov_DKOBushFruit_Perm2:       
		return   1003;
	case tov_DKChristmasTrees_Perm:   
		return   1004;
	case tov_DKOChristmasTrees_Perm:  
		return   1005;
	case tov_DKEnergyCrop_Perm:       
		return   1006;
	case tov_DKOEnergyCrop_Perm:      
		return   1007;
	case tov_DKFarmForest_Perm:       
		return   1008;
	case tov_DKOFarmForest_Perm:      
		return   1009;
	case tov_DKGrazingPigs_Perm:      
		return   1010;
	case tov_DKOGrazingPigs_Perm:     
		return   1011;
	case tov_DKOGrassGrazed_Perm:     
		return   1012;
	case tov_DKOGrassLowYield_Perm:   
		return   1013;
	case tov_DKFarmYoungForest_Perm:  
		return   1014;
	case tov_DKOFarmYoungForest_Perm: 
		return   1015;

		//1084-1090 not used as Christmas tree codes are merged

	case tov_UKBeans:
		return 1200;
	case tov_UKBeet:
		return 1201;
	case tov_UKMaize:
		return 1202;
	case tov_UKPermanentGrass:
		return 1203;
	case tov_UKPotatoes:
		return 1204;
	case tov_UKSpringBarley:
		return 1205;
	case tov_UKTempGrass:
		return 1206;
	case tov_UKWinterBarley:
		return 1207;
	case tov_UKWinterRape:
		return 1208;
	case tov_UKWinterWheat:
		return 1209;
        
	case tov_BEBeet:
		return 1250;
	case tov_BEBeetSpring:
		return 1251;
	case tov_BECatchPeaCrop:
		return 1252;
	case tov_BEGrassGrazed1:
		return 1253;
	case tov_BEGrassGrazed1Spring:
		return 1254;
	case tov_BEGrassGrazed2:
		return 1255;
	case tov_BEGrassGrazedLast:
		return 1256;
	case tov_BEMaize:
		return 1257;
	case tov_BEMaizeSpring:
		return 1258;
	case tov_BEOrchardCrop:
		return 1259;
	case tov_BEPotatoes:
		return 1260;
	case tov_BEPotatoesSpring:
		return 1261;
	case tov_BEWinterBarley:
		return 1262;
	case tov_BEWinterWheat:
		return 1263;
	case tov_BEWinterWheatCC:
		return 1264;
	case tov_BEWinterBarleyCC:
		return 1265;
	case tov_BEMaizeCC:
		return 1266;

	case tov_DKOLegume_Peas:         
		return  1016;
	case tov_DKOLegume_Whole:   
		return  1017;
	case tov_DKSugarBeets:      
		return  1018;
	case tov_DKOSugarBeets:     
		return  1019;
	case tov_DKCabbages:        
		return  1020;
	case tov_DKOCabbages:       
		return  1021;
	case tov_DKCarrots:         
		return  1022;
	case tov_DKOCarrots:        
		return  1023;
	case tov_DKLegume_Whole:    
		return  1024;
	case tov_DKLegume_Peas:          
		return  1025;
	case tov_DKWinterWheat:     
		return  1026;
	case tov_DKOWinterWheat:
		return 1027;
	case tov_DKSpringBarley:
		return 1028;
	case tov_DKOSpringBarley:
		return 1029;
	case tov_DKCerealLegume:
		return 1030;
	case tov_DKOCerealLegume:
		return 1031;
	case tov_DKCerealLegume_Whole:
		return 1032;
	case tov_DKOCerealLegume_Whole:
		return 1033;
	case tov_DKBushFruit_Perm1:
		return   1034;
	case tov_DKBushFruit_Perm2:
		return   1035;
	case tov_DKSpringFodderGrass:
		return 1036;
	case tov_DKOWinterFodderGrass:
		return 1037;
	case tov_DKGrassGrazed_Perm:
		return 1038;
	case tov_DKGrassLowYield_Perm:
		return 1039;
	case tov_DKGrazingPigs:
		return 1040;
	case tov_DKMaize:
		return 1041;
	case tov_DKMaizeSilage:
		return 1042;
	case tov_DKMixedVeg:
		return 1043;
	case tov_DKOGrazingPigs:
		return 1044;
	case tov_DKOMaize:
		return 1045;
	case tov_DKOMaizeSilage:
		return 1046;
	case tov_DKOMixedVeg:
		return 1047;
	case tov_DKOPotato:
		return 1048;
	case tov_DKOPotatoIndustry:
		return 1049;
	case tov_DKOPotatoSeed:
		return 1050;
	case tov_DKOrchardCrop_Perm:
		return 1051;
	case tov_DKOSeedGrassRye_Spring:
		return 1052;
	case tov_DKOSetAside:
		return 1053;
	case tov_DKOSpringBarleySilage:
		return 1054;
	case tov_DKOSpringOats:
		return 1055;
	case tov_DKOSpringWheat:
		return 1056;
	case tov_DKOVegSeeds:
		return 1057;
	case tov_DKOWinterBarley:
		return 1058;
	case tov_DKOWinterRape:
		return 1059;
	case tov_DKOWinterRye:
		return 1060;
	case tov_DKPlantNursery_Perm:
		return 1061;
	case tov_DKPotato:
		return 1062;
	case tov_DKPotatoIndustry:
		return 1063;
	case tov_DKPotatoSeed:
		return 1064;
	case tov_DKSeedGrassFescue_Spring:
		return 1065;
	case tov_DKSeedGrassRye_Spring:
		return 1066;
	case tov_DKSetAside:
		return 1067;
	case tov_DKSetAside_SummerMow:
		return 1068;
	case tov_DKSpringBarley_Green:
		return 1069;
	case tov_DKSpringBarleySilage:
		return 1070;
	case tov_DKSpringOats:
		return 1071;
	case tov_DKSpringWheat:
		return 1072;
	case tov_DKUndefined:
		return 1073;
	case tov_DKVegSeeds:
		return 1074;
	case tov_DKWinterBarley:
		return 1075;
	case tov_DKWinterRape:
		return 1076;
	case tov_DKWinterRye:
		return 1077;
	case tov_DKOWinterCloverGrassGrazedSown:
		return 1078;
	case tov_DKOCloverGrassGrazed1:
		return 1079;
	case tov_DKOCloverGrassGrazed2:
		return 1080;
	case tov_DKCloverGrassGrazed1:
		return 1081;
	case tov_DKWinterCloverGrassGrazedSown:
		return 1082;
	case tov_DKCloverGrassGrazed2:
		return 1083;
	case tov_DKOLegumeCloverGrass_Whole:
		return  1091;
	case tov_DKLegume_Beans:
		return  1092;
	case tov_DKWinterFodderGrass:
		return 1093;
	case tov_DKOrchApple:
		return 1094;
	case tov_DKOrchPear:
		return 1095;
	case tov_DKOrchCherry:
		return 1096;
	case tov_DKOrchOther:
		return 1097;
	case tov_DKOOrchApple:
		return 1098;
	case tov_DKOOrchPear:
		return 1099;
	case tov_DKOOrchCherry:
		return 1100;
	case tov_DKOOrchOther:
		return 1101;
	case tov_DKFodderBeets:
		return 1102;
	case tov_DKOLegume_Beans:
		return 1103;
	case tov_DKOFodderBeets:
		return 1104;
	case tov_DKOSpringFodderGrass:
		return 1105;
	case tov_DKCatchCrop:
		return 1106;
	case tov_DKOCatchCrop:
		return 1107;
	case tov_DKSpringBarleyCloverGrass:
		return 1108;
	case tov_DKOSpringBarleyCloverGrass:
		return 1109;
	case tov_DKWinterWheat_CC:
		return 1110;
	case tov_DKWinterRye_CC:
		return 1111;
	case tov_DKSpringOats_CC:
		return 1112;
	case tov_DKSpringBarley_CC:
		return 1113;
	case tov_DKOWinterWheat_CC:
		return 1114;
	case tov_DKOWinterRye_CC:
		return 1115;
	case tov_DKOSpringOats_CC:
		return 1116;
	case tov_DKOSpringBarley_CC:
		return 1117;
	case tov_DKOLegume_Beans_CC:
		return 1118;
	case tov_DKOLegume_Peas_CC:
		return 1119;
	case tov_DKOLegume_Whole_CC:
		return 1120;
	case tov_DKOLupines:
		return 1121;
	case tov_DKOLentils:
		return 1122;
	case tov_DKOSetAside_AnnualFlower:
		return 1123;
	case tov_DKOSetAside_PerennialFlower:
		return 1124;
	case tov_DKOSetAside_SummerMow:
		return 1125;

	case tov_FIWinterWheat:
		return 1400;
	case tov_FIOWinterWheat:
		return 1401;
	case tov_FISugarBeet:
		return 1402;
	case tov_FIStarchPotato_North:
		return 1403;
	case tov_FIStarchPotato_South:
		return 1404;
	case tov_FIOStarchPotato_North:
		return 1405;
	case tov_FIOStarchPotato_South:
		return 1406;
	case tov_FISpringWheat:
		return 1407;
	case tov_FIOSpringWheat:
		return 1408;
	case tov_FITurnipRape:
		return 1409;
	case tov_FIOTurnipRape:
		return 1410;
	case tov_FISpringRape:
		return 1411;
	case tov_FIOSpringRape:
		return 1412;
	case tov_FIWinterRye:
		return 1413;
	case tov_FIOWinterRye:
		return 1414;
	case tov_FIPotato_North:
		return 1415;
	case tov_FIPotato_South:
		return 1416;
	case tov_FIOPotato_North:
		return 1417;
	case tov_FIOPotato_South:
		return 1418;
	case tov_FIPotatoIndustry_North:
		return 1419;
	case tov_FIPotatoIndustry_South:
		return 1420;
	case tov_FIOPotatoIndustry_North:
		return 1421;
	case tov_FIOPotatoIndustry_South:
		return 1422;
	case tov_FISpringOats:
		return 1423;
	case tov_FIOSpringOats:
		return 1424;
	case tov_FISpringBarley_Malt:
		return 1425;
	case tov_FIOSpringBarley_Malt:
		return 1426;
	case tov_FIFabaBean:
		return 1427;
	case tov_FIOFabaBean:
		return 1428;
	case tov_FISpringBarley_Fodder:
		return 1429;
	case tov_FIOSpringBarley_Fodder:
		return 1430;
	case tov_FIGrasslandPasturePerennial1:
		return 1431;
	case tov_FIGrasslandPasturePerennial2:
		return 1432;
	case tov_FIGrasslandSilagePerennial1:
		return 1433;
	case tov_FIGrasslandSilagePerennial2:
		return 1434;
	case tov_FINaturalGrassland:
		return 1435;
	case tov_FINaturalGrassland_Perm:
		return 1436;
	case tov_FIFeedingGround:
		return 1437;
	case tov_FIBufferZone:
		return 1438;
	case tov_FIBufferZone_Perm:
		return 1439;
	case tov_FIGreenFallow_1year:
		return 1440;
	case tov_FIGreenFallow_Perm:
		return 1441;
	case tov_FIGrasslandSilageAnnual:
		return 1442;
	case tov_FICaraway1:
		return 1443;
	case tov_FICaraway2:
		return 1444;
	case tov_FIOCaraway1:
		return 1445;
	case tov_FIOCaraway2:
		return 1446;
	case tov_FISprSpringBarley_Fodder:
		return 1447;
	case tov_SESpringBarley:
		return 1300;
	case tov_SEWinterRape_Seed:
		return 1301;
	case tov_SEWinterWheat:
		return 1302;
	case tov_IRSpringWheat:
		return 1500;
	case tov_IRSpringBarley:
		return 1501;
	case tov_IRSpringOats:
		return 1502;
	case tov_IRGrassland_no_reseed:
		return 1503;
	case tov_IRGrassland_reseed:
		return 1504;
	case tov_IRWinterBarley:
		return 1505;
	case tov_IRWinterWheat:
		return 1506;
	case tov_IRWinterOats:
		return 1507;

	case tov_FRWinterWheat:
		return 1600;
	case tov_FRWinterBarley:
		return 1601;
	case tov_FRWinterTriticale:
		return 1602;
	case tov_FRWinterRape:
		return 1603;
	case tov_FRMaize:
		return 1604;
	case tov_FRMaize_Silage:
		return 1605;
	case tov_FRSpringBarley:
		return 1606;
	case tov_FRGrassland:
		return 1607;
	case tov_FRGrassland_Perm:
		return 1608;
	case tov_FRSpringOats:
		return 1609;
	case tov_FRSunflower:
		return 1610;
	case tov_FRSpringWheat:
		return 1611;
	case tov_FRPotatoes:
		return 1612;
	case tov_FRSorghum:
		return 1613;
	case tov_ITGrassland:
		return 1700;
	case tov_ITOrchard:
		return 1701;
	case tov_ITOOrchard:
		return 1702;
    case tov_Undefined:
      return 9999;
    default: // No matching code so we need an error message of some kind
      g_msg->Warn( WARN_FILE, "LE_TypeClass::BackTranslateVegTypes(): ""Unknown vegetation type:",int(VegReference));
      exit( 1 );
  }
}

//-----------------------------------------------------------------------
// created 25/08/00
int LE_TypeClass::BackTranslateEleTypes( TTypesOfLandscapeElement EleReference ) {

  // This returns the vegetation type (or crop type) as applicable
  switch ( EleReference )
  {
    case tole_Building:				return 5;
    case tole_UrbanNoVeg:			return 8;
	case tole_UrbanVeg:		return 9;
    case tole_Garden:				return 11;
    case tole_AmenityGrass:			return 12;
    case tole_RoadsideVerge:		return 13;
    case tole_Parkland:				return 14;
    case tole_StoneWall:			return 15;
    case tole_BuiltUpWithParkland:	return 16;
    case tole_UrbanPark:			return 17;
    case tole_Field:			    return 20;
    case tole_PermPastureTussocky:  return 27;
    case tole_PermPastureLowYield:  return 26;
    case tole_UnsprayedFieldMargin: return 31;
    case tole_PermanentSetaside:    return 33;
    case tole_PermPasture:		    return 35;
    case tole_DeciduousForest:      return 40;
	case tole_Copse:				return 41;
	case tole_ConiferousForest:     return 50;
    case tole_YoungForest:		    return 55;
    case tole_Orchard:			    return 56;
    case tole_BareRock:			    return 69;
	case tole_OrchardBand:			return 57;
	case tole_MownGrass:			return 58;
	case tole_MixedForest:		    return 60;
	case tole_Scrub:				return 70;
	case tole_PitDisused:		    return 75;
    case tole_Saltwater:		    return 80;
    case tole_Freshwater:		    return 90;
    case tole_Heath:			    return 94;
    case tole_Marsh:				return 95;
    case tole_River:				return 96;
    case tole_RiversideTrees:	    return 97;
    case tole_RiversidePlants:      return 98;
    case tole_Coast:			    return 100;
	case tole_SandDune:				return 101;
	case tole_NaturalGrassDry:      return 110;
    case tole_ActivePit:		    return 115;
    case tole_Railway:				return 118;
    case tole_LargeRoad:			return 121;
    case tole_SmallRoad:			return 122;
    case tole_Track:				return 123;
    case tole_Hedges:				return 130;
    case tole_HedgeBank:			return 140;
    case tole_BeetleBank:		    return 141;
	case tole_Chameleon:			return 150;
	case tole_FieldBoundary:	    return 160;
 	case tole_RoadsideSlope:		return 201;
	case tole_MetalledPath:			return 202;
	case tole_Carpark:				return 203;
	case tole_Churchyard:			return 204;
	case tole_NaturalGrassWet:		return 205;
	case tole_Saltmarsh:			return 206;
	case tole_Stream:				return 207;
	case tole_HeritageSite:			return 208;
	case tole_Wasteland:			return 209; 
	case tole_UnknownGrass:		return 210;
	case tole_WindTurbine:			return 211;
	case tole_Pylon:				return 212;
	case tole_IndividualTree:		return 213;
	case tole_PlantNursery:			return 214;
	case tole_Vildtager:			return 215;
	case tole_WoodyEnergyCrop:		return 216;
	case tole_WoodlandMargin:		return 217;
	case tole_PermPastureTussockyWet:		return 218;
	case tole_Pond:                 return 219;
	case tole_FishFarm:                 return 220;
	case tole_RiverBed:		return 221;
	case tole_DrainageDitch:		return 222;
	case tole_Canal:	return 223;
	case tole_RefuseSite:	return 224;
	case tole_Fence:				return 225;
	case tole_WaterBufferZone:		return 226;
    // adding new elements ( year 2021 )
	case  tole_Airport:				return 300;
	case  tole_Portarea:			return 301;
	case  tole_Saltpans:			return 302;
	case  tole_Pipeline:			return 303;
	case  tole_SolarPanel:			return 304;
	// adding new forests 
	case  tole_SwampForest:			return 400;
	case  tole_MontadoCorkOak:		return 401;
	case  tole_MontadoHolmOak:		return 402;
	case  tole_MontadoMixed:		return 403;
	case  tole_AgroForestrySystem:	return 404;
	case  tole_CorkOakForest:		return 405;
	case  tole_HolmOakForest:		return 406;
	case  tole_OtherOakForest:		return 407;
	case  tole_ChestnutForest:		return 408;
	case  tole_EucalyptusForest:	return 409;
	case  tole_InvasiveForest:		return 410;
	case  tole_MaritimePineForest:	return 411;
	case  tole_StonePineForest:		return 412;
	case  tole_ForestAisle:			return 413;
	// adding new permanent crop fields
	case  tole_Vineyard:			return 500;
	case  tole_OliveGrove:			return 501;
	case  tole_RiceField:			return 502;
	case  tole_OOrchard:             return 503;
	case  tole_BushFruit:            return 504;
	case  tole_OBushFruit:           return 505;
	case  tole_ChristmasTrees:       return 506;
	case  tole_OChristmasTrees:      return 507;
	case  tole_EnergyCrop:           return 508;
	case  tole_OEnergyCrop:          return 509;
	case  tole_FarmForest:           return 510;
	case  tole_OFarmForest:          return 511;
	case  tole_PermPasturePigs:      return 512;
	case  tole_OPermPasturePigs:     return 513;
	case  tole_OPermPasture:         return 514;
	case  tole_OPermPastureLowYield: return 515;
	case  tole_FarmYoungForest:      return 516;
	case  tole_OFarmYoungForest:     return 517;
	case  tole_AlmondPlantation:	 return 518;
	case  tole_WalnutPlantation:	 return 519;
	case  tole_FarmBufferZone:		 return 520;
	case  tole_NaturalFarmGrass:	 return 521;
	case  tole_GreenFallow:			 return 522;
	case  tole_FarmFeedingGround:	 return 523;
	case  tole_FlowersPerm:			 return 524;
	case  tole_AsparagusPerm:		 return 525;
	case  tole_MushroomPerm:		 return 526;
	case  tole_OtherPermCrop:		 return 527;
	case  tole_OAsparagusPerm:		 return 528;

	case tole_Missing:		return 2112;

	//case tole_Foobar: return 999;
	// !! type unknown - should not happen
	default:
      g_msg->Warn( WARN_FILE, "LE_TypeClass::BackTranslateEleTypes(): ""Unknown landscape element type:", int(EleReference) );
      exit( 1 );
  }
}

//------------------------------------------------------------------------

void BeetleBank::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  m_insect_pop = m_insect_pop * cfg_beetlebankinsectscaler.value()*3.0;
}

