//
// PollenNectar.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#ifndef POLLEN_NECTAR_H
#define POLLEN_NECTAR_H

#include <vector>
#include <algorithm>
#include <climits> // gcc built fails without explicitly including climits

/** \brief A general scaler to allow pollen and nectar resources to be altered for pollinator models */
static CfgFloat  cfg_PollenNectarResourcesCompetitionScaler("POLLENNECTARRESOURCESSCALER", CFG_CUSTOM, 1.0);

/** \brief 
* This class justs holds the set of resource curves related to a specific plant community or crop. 
* It is a data class, with no behaviour, but is convenient to group curves together. 
*/
class PollenNectarDevelopmentCurveSet {
public:
	/**
	* Contains an integer reference to a curve number, which is <10000 for plant communities and 10000 upwards for single crop curves.
	* It then contains four curves and an associated day degree index. This data is a core part of the the PollenNectarDevelopmentData class where it is held in a list, one for each resource model defined.
	*/
	/** \brief The reference number for the curve */
	int m_curve_number;
	/** \brief The threshold temperature to calculate day degrees */
	double m_DDthreshold = 0;
	/** \brief The maximum day degrees that one day can accumulate */
	double m_MaxDDeg;
	/** \brief the slopes for the pollen amount */
	vector<double> m_pollencurve;
	/** \brief the slopes for the nectar amount */
	vector<double> m_nectarcurve;
	/** \brief the slopes for the sugar amount */
	vector<double> m_sugarcurve;
	/** \brief the slopes for the pollen quality */
	vector<double> m_pollenqualitycurve;
	/** \brief the index values to the slopes */
	vector <double> m_DD_nectar;
	/** \brief the index values to the slopes */
	vector <double> m_DD_sugar;
	/** \brief the index values to the slopes */
	vector <double> m_DD_pollen;

	inline int closest_nectar(int a_value) {
		// returns the vector DD for m_DD where the value contained is the first number greater than a_value
		return int(std::distance(m_DD_nectar.begin(), upper_bound(m_DD_nectar.begin(), m_DD_nectar.end(), a_value)));
	}

	inline int closest_sugar(int a_value) {
		// returns the vector DD for m_DD where the value contained is the first number greater than a_value
		return int(std::distance(m_DD_sugar.begin(), upper_bound(m_DD_sugar.begin(), m_DD_sugar.end(), a_value)));
	}

	inline int closest_pollen(int a_value) {
		// returns the vector DD for m_DD where the value contained is the first number greater than a_value
		return int(std::distance(m_DD_pollen.begin(), upper_bound(m_DD_pollen.begin(), m_DD_pollen.end(), a_value)));
	}

	PollenNectarDevelopmentCurveSet() {
		// the default is simply an empty curve set that will return zero for all values of day degrees
		vector<double> empty = { 0,0,0 };
		m_pollencurve = empty;
		m_nectarcurve = empty;
		m_sugarcurve = empty;
		m_pollenqualitycurve = empty;
		m_DD_nectar.resize(3);
		m_DD_nectar[0] = -1;
		m_DD_nectar[1] = 0;
		m_DD_nectar[2] = double(INT_MAX); // a very large number that should not be possible to reach
		m_DD_sugar = m_DD_nectar;
		m_DD_pollen = m_DD_nectar;
		m_curve_number = 0;
		m_DDthreshold = 0;
		m_MaxDDeg = 0;
	}
	void ClearAll()
	{
		/** Just clears all data from the curves */
		m_pollencurve.clear();
		m_nectarcurve.clear();
		m_sugarcurve.clear();
		m_pollenqualitycurve.clear();
		m_DD_nectar.clear();
		m_DD_sugar.clear();
		m_DD_pollen.clear();
	}

	/** \brief Replaces the values in the vectors with slopes based on the day degree values and relative values of the inflection points */
	void CalculateSlopes() {
		vector<double>tempdiffs[3];
		vector<double>slopediffs[4];
		tempdiffs[0].push_back(m_DD_nectar[0]);
		tempdiffs[1].push_back(m_DD_sugar[0]);
		tempdiffs[2].push_back(m_DD_pollen[0]);
		for (int i = 0; i < 4; i++) slopediffs[i].push_back(0.0);
		for (int i = 1; i < int(m_DD_nectar.size() - 1); i++) 
		{
			tempdiffs[0].push_back(m_DD_nectar[i + 1] - m_DD_nectar[i]);
			slopediffs[0].push_back( m_nectarcurve[i + 1] - m_nectarcurve[i]) ;
		}
		for (int i = 1; i < int(m_DD_sugar.size() - 1); i++) 
		{
			tempdiffs[1].push_back(m_DD_sugar[i + 1] - m_DD_sugar[i]);
			slopediffs[1].push_back(m_sugarcurve[i + 1] - m_sugarcurve[i]);
		}
		for (int i = 1; i < int(m_DD_pollen.size() - 1); i++) 
		{
			tempdiffs[2].push_back(m_DD_pollen[i + 1] - m_DD_pollen[i]);
			slopediffs[2].push_back(m_pollencurve[i + 1] - m_pollencurve[i]);
			slopediffs[3].push_back(m_pollenqualitycurve[i + 1] - m_pollenqualitycurve[i]);
		}
		// Now divide the change by day-degrees diffs
		for (int i = 1; i < int(m_DD_nectar.size() - 1); i++)
		{
			m_nectarcurve[i] = (slopediffs[0][i] / tempdiffs[0][i]) * cfg_PollenNectarResourcesCompetitionScaler.value();
		}
		for (int i = 1; i < int(m_DD_sugar.size() - 1); i++)
		{
			m_sugarcurve[i] = slopediffs[1][i] / tempdiffs[1][i] * cfg_PollenNectarResourcesCompetitionScaler.value();
		}
		for (int i = 1; i < int(m_DD_pollen.size() - 1); i++)
		{
			m_pollencurve[i] = slopediffs[2][i] / tempdiffs[2][i] * cfg_PollenNectarResourcesCompetitionScaler.value();
			m_pollenqualitycurve[i] = slopediffs[3][i] / tempdiffs[2][i];
		}
		// This is just for neatness, these values should never be read
		m_nectarcurve[int(m_DD_nectar.size())-1] = 0.0;
		m_sugarcurve[int(m_DD_sugar.size()) - 1] = 0.0;
		m_pollencurve[int(m_DD_pollen.size()) - 1] = 0.0;
		m_pollenqualitycurve[int(m_DD_pollen.size()) - 1] = 0.0;
	}
	/** \brief Returns the day degrees temperature threshold */
	double GetThreshold() { return m_DDthreshold; }
	/** \brief Returns the maximum day degrees that one day can have. */
	double GetMaxDDeg() {return m_MaxDDeg;}
	/** \brief Returns pollen quantity in the curve for index i */
	double GetPollenQuantity(int i) { return m_pollencurve[i]; }
	/** \brief Returns pollen quality in the curve for index i */
	double GetPollenQuality(int i) { return m_pollenqualitycurve[i]; }
	/** \brief Returns nectar in the curve for index i */
	double GetNectar(int i) { return m_nectarcurve[i]; }
	/** \brief Returns sugar in the curve for index i */
	double GetSugar(int i) { return m_sugarcurve[i]; }
	/** \brief Returns day degrees target in the curve for index i */
	double GetDDTargetNectar(int i) { return m_DD_nectar[i]; }
	/** \brief Returns day degrees target in the curve for index i */
	double GetDDTargetSugar(int i) { return m_DD_sugar[i]; }
	/** \brief Returns day degrees target in the curve for index i */
	double GetDDTargetPollen(int i) { return m_DD_pollen[i]; }
	/** \brief Returns the reference number for the curve */
	int GetRef() { return m_curve_number; }
};

/** \brief A data class to store nectar or pollen data*/
class PollenNectarData {
public:
	PollenNectarData();
	PollenNectarData(double a_quantity, double a_quality);
	double m_quantity = 0.0; // The quantity of nectar or pollen
	double m_quality = 0.0;  // The quality of nectar or pollen
	/** \brief Returns volume of sugar in volume of nectar (vol/vol).*/ //Jordan
	double GetNectarSugarConc();
};

/** \brief A class to manage a range of pollen and nectar development curves based on indexed rates */
class PollenNectarDevelopmentData
{
protected:
	/** \brief This is the main data set, it contains the resource curves as slopes for each community or crop */
	vector<PollenNectarDevelopmentCurveSet> m_ResourceCurves;
	/** \brief this is a look-up table, it contains for each tov the corresponding curve number to use */
	vector<int> m_tov_PollenCurveTable;
public:
	PollenNectarDevelopmentData(string a_toleinputfile = "default", string a_tovinputfile = "default");
	~PollenNectarDevelopmentData();

	PollenNectarDevelopmentCurveSet* tovGetPollenNectarCurve(TTypesOfVegetation a_tov_ref)
	{
		/**
		* Uses the tov ref num to find the relevant curve set and return it as a pointer.
		*/
		int refnum = m_tov_PollenCurveTable[a_tov_ref];
		return GetPollenNectarCurve(refnum);
	}
	
	PollenNectarDevelopmentCurveSet* GetPollenNectarCurve(int a_ref)
	{
		int found = 0;
		for (int i=0; i< m_ResourceCurves.size(); i++)
		{
			if (m_ResourceCurves[i].GetRef() == a_ref)
			{
				found = i;
				break;
			}
		}
		return &m_ResourceCurves[found];
	}


};

PollenNectarDevelopmentData* CreatePollenNectarDevelopmentData();

#endif
