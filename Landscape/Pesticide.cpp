//
// pesticide.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2003, Christopher John Topping, EcoSol
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#define _CRT_SECURE_NO_DEPRECATE
//#define __WithinOrchardPesticideSim__

#include <iostream>     // std::cout
#include <algorithm>    // std::fill
#include <vector>       // std::vector
#include <execution>	// std::for_each
#include <cstdio> // Only needed for debugging purposes.
#include <math.h>
#include <fstream>
#include "ls.h"

using namespace std;

/** \brief Used to turn on or off the PPP functionality of ALMaSS */
CfgBool l_pest_enable_pesticide_engine("PEST_ENABLE_PESTICIDE_ENGINE", CFG_CUSTOM, false);
/** \brief The number of active Plant Protection Products to be tracked - a performance penalty if enabled with more than necessary (memory heavy) */
CfgInt l_pest_NoPPPs("PEST_NO_OF_PPPS", CFG_CUSTOM, 1,1,10);
#ifdef __DETAILED_PESTICIDE_FATE
/** \brief Half-life of pesticides in soil */
static CfgArray_Double l_pest_ai_half_life_soil("PEST_AI_HALF_LIFE_SOIL", CFG_CUSTOM, 10, vector<double>{10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0});
/** \brief Half-life of pesticides on plant surface */
static CfgArray_Double l_pest_ai_half_life_vegcanopy("PEST_AI_HALF_LIFE_VEG", CFG_CUSTOM, 10, vector<double>{10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0});
/** \brief Solubility of the pesticides used for the rain washoff calculation */
CfgArray_Double cfg_pest_solubility("PEST_SOLUBILITY", CFG_CUSTOM, 10, vector<double>{10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000});
#ifdef __FLOWER_PESTICIDE
/** \brief Half-life of pesticides inside plant */
static CfgArray_Double l_pest_ai_half_life_in_vegetation("PEST_AI_HALF_LIFE_WITHIN_VEG", CFG_CUSTOM, 10, vector<double>{10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0}); 
/** \brief Half-life of pesticides in nectar */
static CfgArray_Double l_pest_ai_half_life_nectar("PEST_AI_HALF_LIFE_IN_NECTAR", CFG_CUSTOM, 10, vector<double>{10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0});
/** \brief Half-life of pesticides in pollen */
static CfgArray_Double l_pest_ai_half_life_pollen("PEST_AI_HALF_LIFE_IN_POLLEN", CFG_CUSTOM, 10, vector<double>{10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0});
/** \brief Rate of transfer between soil and plant */
CfgFloat cfg_pest_transfer_rate_soil_to_plant("PEST_TRANSFER_RATE_SOIL_TO_PLANTS", CFG_CUSTOM, 0.1);
/** \brief Rate of transfer between plant surface and plant */
CfgFloat cfg_pest_absorption_rate_surface_to_plant("PEST_ABSORPTION_RATE_SURFACE_TO_PLANTS", CFG_CUSTOM, 0.1);
/** \brief Rate of transfer between plant and nectar */
CfgFloat cfg_pest_transfer_rate_plant_to_nectar("PEST_TRANSFER_RATE_PLANTS_TO_NECTAR", CFG_CUSTOM, 0.1);
/** \brief Rate of transfer between plant and pollen  */
CfgFloat cfg_pest_transfer_rate_plant_to_pollen("PEST_TRANSFER_RATE_PLANTS_TO_POLLEN", CFG_CUSTOM, 0.1);
/** \brief Rate of overspray on nectar */
CfgFloat cfg_pest_overspray_rate_to_nectar("PEST_OVERSPRAY_RATE_TO_NECTAR", CFG_CUSTOM, 0.1);
/** \brief Rate of overspray on pollen */
CfgFloat cfg_pest_overspray_rate_to_pollen("PEST_OVERSPRAY_RATE_TO_POLLEN", CFG_CUSTOM, 0.1);

/** \brief Used to turn on or off the seed coating functionality of ALMaSS */
CfgBool l_pest_enable_seed_coating("PEST_ENABLE_SEED_COATING", CFG_CUSTOM, false);
/** \brief Half-life of pesticides in seed coating */
static CfgArray_Double l_pest_ai_half_life_seed_coating("PEST_AI_HALF_LIFE_SEED_COATING", CFG_CUSTOM, 10, vector<double>{10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0});
/** \brief Rate of transfer between seed coating and plant */
CfgFloat cfg_pest_transfer_rate_seedcoating_to_plant("PEST_TRANSFER_RATE_SEEDCOATING_TO_PLANTS", CFG_CUSTOM, 0.1);
/** \brief Rate of transfer between seed coating and soil */
CfgFloat cfg_pest_transfer_rate_seedcoating_to_soil("PEST_TRANSFER_RATE_SEEDCOATING_TO_SOIL", CFG_CUSTOM, 0.1);
#endif
#else
/** \brief Half-life of pesticides for one compartment use */
static CfgArray_Double l_pest_ai_half_life("PEST_AI_HALF_LIFE", CFG_CUSTOM, 10, vector<double>{10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0});
#endif

/** \brief The distance away from the spraying point we want to consider the pesticide drift */
static CfgInt l_pest_diffusion_distance("PEST_DIFFUSION_DISTANCE",CFG_CUSTOM, 1 );
/** \brief The threshold for when we consider the pesticide concentration negligible */
static CfgFloat l_pest_zero_threshold("PEST_ZERO_THRESHOLD",CFG_CUSTOM, 0.00001 );
/** \brief Size of the grid used for recording the pesticide concentration in the landscape */
static CfgInt l_pest_record_grid_size("PEST_RECORD_GRID_SIZE", CFG_CUSTOM, 100);
/** \brief Used to turn on or off saving the pesticide load */
static CfgBool l_pest_record_used("PEST_RECORD_USED", CFG_CUSTOM, false);
/** \brief Used to turn on or off saving the pesticide map(s) daily */
static CfgBool l_pest_save_map_daily("PEST_SAVE_MAP_DAILY", CFG_CUSTOM, false);
/** \brief Amount of pesticide sprayed with type 1 */
CfgFloat cfg_pest_product_1_amount("PEST_PRODUCT_ONE_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 2 */
CfgFloat cfg_pest_product_2_amount("PEST_PRODUCT_TWO_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 3 */
CfgFloat cfg_pest_product_3_amount("PEST_PRODUCT_THREE_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 4 */
CfgFloat cfg_pest_product_4_amount("PEST_PRODUCT_FOUR_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 5 */
CfgFloat cfg_pest_product_5_amount("PEST_PRODUCT_FIVE_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 6 */
CfgFloat cfg_pest_product_6_amount("PEST_PRODUCT_SIX_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 7 */
CfgFloat cfg_pest_product_7_amount("PEST_PRODUCT_SEVEN_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 8 */
CfgFloat cfg_pest_product_8_amount("PEST_PRODUCT_EIGHT_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 9 */
CfgFloat cfg_pest_product_9_amount("PEST_PRODUCT_NINE_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of pesticide sprayed with type 10 */
CfgFloat cfg_pest_product_10_amount("PEST_PRODUCT_TEN_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 1 */
CfgFloat cfg_seed_coating_product_1_amount("SEED_COATING_PRODUCT_ONE_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 2 */
CfgFloat cfg_seed_coating_product_2_amount("SEED_COATING_PRODUCT_TWO_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 3 */
CfgFloat cfg_seed_coating_product_3_amount("SEED_COATING_PRODUCT_THREE_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 4 */
CfgFloat cfg_seed_coating_product_4_amount("SEED_COATING_PRODUCT_FOUR_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 5 */
CfgFloat cfg_seed_coating_product_5_amount("SEED_COATING_PRODUCT_FIVE_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 6 */
CfgFloat cfg_seed_coating_product_6_amount("SEED_COATING_PRODUCT_SIX_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 7 */
CfgFloat cfg_seed_coating_product_7_amount("SEED_COATING_PRODUCT_SEVEN_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 8 */
CfgFloat cfg_seed_coating_product_8_amount("SEED_COATING_PRODUCT_EIGHT_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 9 */
CfgFloat cfg_seed_coating_product_9_amount("SEED_COATING_PRODUCT_NINE_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Amount of seed coating pesticide added with type 10 */
CfgFloat cfg_seed_coating_product_10_amount("SEED_COATING_PRODUCT_TEN_AMOUNT", CFG_CUSTOM, 1.0);
/** \brief Turn on seed coating for winter wheat */
CfgBool cfg_seedcoating_winterwheat_on("SEEDCOATING_WINTERWHEAT_ON", CFG_CUSTOM, false);
/** \brief Turn on seed coating for winter rape */
CfgBool cfg_seedcoating_winterrape_on("SEEDCOATING_WINTERRAPE_ON", CFG_CUSTOM, false);

/** \brief Daily mortality of pesticide with type 1 */
CfgFloat l_pest_daily_mort( "PEST_DAILY_MORTALITY", CFG_CUSTOM, 0.25 );
/** \brief Daily mortality of pesticide with type 2 */
CfgFloat l_pest_daily_mort_2( "PEST_DAILY_MORTALITY_TWO", CFG_CUSTOM, 0.25 );
/** \brief This is a trigger value that can be used to trigger pesticides effects. Currently not used? */
CfgFloat l_pest_trigger_threshold1( "PEST_TRIGGER_THRESHOLD_ONE", CFG_CUSTOM, 1.0 );
/** \brief This is a trigger value that can be used to trigger pesticides effects. Currently not used? */
CfgFloat l_pest_trigger_threshold2( "PEST_TRIGGER_THRESHOLD_TWO", CFG_CUSTOM, 1.0 );

/** \brief Start date for applying pesticide the first time */
CfgInt cfg_pest_productapplic_startdate("PEST_PRODUCTAPPLIC_STARTDATE", CFG_CUSTOM, -1);
/** \brief Start date for applying pesticide the second */
CfgInt cfg_pest_productapplic_startdate2("PEST_PRODUCTAPPLIC_STARTDATE_TWO",CFG_CUSTOM,-1);
/** \brief Start date for applying pesticide the third time */
CfgInt cfg_pest_productapplic_startdate3("PEST_PRODUCTAPPLIC_STARTDATE_THREE",CFG_CUSTOM,-1);
/** \brief Period for applying pesticide */
CfgInt cfg_pest_productapplic_period("PEST_PRODUCTAPPLIC_PERIOD",CFG_CUSTOM,1);

/** \brief Use residue or rate. Used in Vole code. */
CfgBool cfg_pest_residue_or_rate("PEST_RESIDUE_OR_RATE",CFG_CUSTOM,true);

/** \brief Turn on pesticides for oats */
CfgBool cfg_pest_oats_on("PEST_SPRINGOATS_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for spring rye */
CfgBool cfg_pest_springrye_on("PEST_SPRINGRYE_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for spring barley */
CfgBool cfg_pest_springbarley_on("PEST_SPRINGBARLEY_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for spring wheat */
CfgBool cfg_pest_springwheat_on("PEST_SPRINGWHEAT_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for winter wheat */
CfgBool cfg_pest_winterwheat_on("PEST_WINTERWHEAT_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for winter barley */
CfgBool cfg_pest_winterbarley_on("PEST_WINTERBARLEY_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for winter triticale */
CfgBool cfg_pest_wintertriticale_on("PEST_WINTERTRITICALE_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for winter rye */
CfgBool cfg_pest_winterrye_on("PEST_WINTERRYE_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for winter rape */
CfgBool cfg_pest_winterrape_on("PEST_WINTERRAPE_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for potatoes */
CfgBool cfg_pest_potatoes_on("PEST_POTATOES_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for beet */
CfgBool cfg_pest_beet_on("PEST_BEET_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for cabbage */
CfgBool cfg_pest_cabbage_on("PEST_CABBAGE_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for maize */
CfgBool cfg_pest_maize_on("PEST_MAIZE_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for peas */
CfgBool cfg_pest_peas_on("PEST_PEAS_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for carrots */
CfgBool cfg_pest_carrots_on("PEST_CARROTS_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for tulips */
CfgBool cfg_pest_tulips_on("PEST_TULIPS_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for orchard */
CfgBool cfg_pest_orchard_on("PEST_ORCHARD_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for wineyard */
CfgBool cfg_pest_vineyard_on("PEST_VINEYARD_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for spring barley silage */
CfgBool cfg_pest_SBS_ERA("PEST_SBS_ERA", CFG_CUSTOM, false);
/** \brief Turn on pesticides for beans */
CfgBool cfg_pest_beans_on("PEST_BEANS_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for grass */
CfgBool cfg_pest_grass_on("PEST_GRASS_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for spring oats */
CfgBool cfg_pest_springoats_on("PEST_SPRINGOATS_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for bush fruit */
CfgBool cfg_pest_bushfruit_on("PEST_BUSHFRUIT_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for mixed vegetables */
CfgBool cfg_pest_veg_on("PEST_VEG_ON", CFG_CUSTOM, false);
/** \brief Turn on pesticides for asparagus */
CfgBool cfg_pest_asparagus_on("PEST_ASPARAGUS_ON", CFG_CUSTOM, false);

class Pesticide *g_pest;
extern Landscape *g_land;
extern Landscape *g_landscape_p;
extern CfgFloat cfg_beer_law_extinction_coef;

void Pesticide::Tick(void)
{
	/** 
	* Run once per day. If the pesticide engine is not on the daily queue is cleared and the method is ended.
	* First the daily weather conditions are read (rainfall, wind direction and temperature) and the temperature-dependent decay rate is calculated.
	* Then the pesticides, that are currently in the map, are decayed and the queued pesticides are sprayed whereafter the queue is emptied. 
	* If seed coating is turned on that is then decayed as well.
	* If turned on, the pesticide load and the pesticide concentration is saved to output files.
	* Then as a last step the pesticides are transfered between the different compartments (only for detailed pesticide model).
	*/
	if (!l_pest_enable_pesticide_engine.value()){
		for (int ppp = ppp_1; ppp < m_NoPPPs; ppp++)
		{
			DailyQueueClear((PlantProtectionProducts)ppp);
		}
		return;
	}

	// Get todays rainfall
	m_rainfallcategory = (unsigned)floor(g_land->SupplyRain() * 100 + 0.5);
	if (m_rainfallcategory >= 100) m_rainfallcategory = 99;
	// Now make it an index to the 100x100 array.
	m_rainfallcategory *= 100;
	// Get todays wind
	m_wind = g_land->SupplyWindDirection();

	double temp = g_weather->GetTemp();
	double tempFactor = exp(0.094779*(20 - temp));
	for (int ppp = ppp_1; ppp < m_NoPPPs; ppp++)
	{
		// Calculate DT50 rates
		// DT50 will change by the rate of 0.22 extra per degree different from 20
		/** \todo we should update temperature dependence on decay for vegcanopy. */
#ifdef __DETAILED_PESTICIDE_FATE
		m_pest_daily_decay_frac_vegcanopy.push_back(pow(10.0, log10(0.5) / (l_pest_ai_half_life_vegcanopy.value(ppp) * tempFactor)));
		m_pest_daily_decay_frac_soil.push_back(pow(10.0, log10(0.5) / (l_pest_ai_half_life_soil.value(ppp) * tempFactor)));
#ifdef __FLOWER_PESTICIDE
		m_pest_daily_decay_frac_in_vegetation.push_back(pow(10.0, log10(0.5) / (l_pest_ai_half_life_in_vegetation.value(ppp) * tempFactor)));
		m_pest_daily_decay_frac_nectar.push_back(pow(10.0, log10(0.5) / (l_pest_ai_half_life_nectar.value(ppp) * tempFactor)));
		m_pest_daily_decay_frac_pollen.push_back(pow(10.0, log10(0.5) / (l_pest_ai_half_life_pollen.value(ppp) * tempFactor)));
		if (l_pest_enable_seed_coating.value()) {
			m_pest_daily_decay_frac_seedcoating.push_back(pow(10.0, log10(0.5) / l_pest_ai_half_life_seed_coating.value(ppp)));
			// Decay seed coating
			SeedCoatingMapDecay((PlantProtectionProducts)ppp);
		}
#endif
#else
		m_pest_daily_decay_frac.push_back(pow(10.0, log10(0.5) / (l_pest_ai_half_life.value(ppp) * tempFactor)));
#endif
	
		// Decay pesticide
		MainMapDecay((PlantProtectionProducts)ppp);
		// Spray pesticide
		DailyQueueProcess((PlantProtectionProducts)ppp);
		// Clear queue
		DailyQueueClear((PlantProtectionProducts)ppp);

		// Save pesticide load 
		if (l_pest_record_used.value()) {
			if (g_date->GetDayInMonth() == 1) RecordPesticideLoad(ppp);
		}
		// Save the current pesticide concentration 
		if (l_pest_save_map_daily.value()) {
#ifdef __DETAILED_PESTICIDE_FATE
			RecordPesticideMap(ppp, m_pesticide_file_rec_soil, m_pest_map_soil);
			RecordPesticideMap(ppp, m_pesticide_file_rec_vegcanopy, m_pest_map_vegcanopy);
#ifdef __FLOWER_PESTICIDE
			RecordPesticideMap(ppp, m_pesticide_file_rec_in_vegetation, m_pest_map_in_vegetation);
			RecordPesticideMap(ppp, m_pesticide_file_rec_pollen, m_pest_map_pollen);
			RecordPesticideMap(ppp, m_pesticide_file_rec_nectar, m_pest_map_nectar);
			if (l_pest_enable_seed_coating.value()) {
				RecordPesticideMap(ppp, m_pesticide_file_rec_seed, m_pest_map_seed);
			}
#endif
#else
			RecordPesticideMap(ppp, m_pesticide_file_rec, m_pest_map_main);
#endif
		}
	}
	//transfer concentration between different compartments
	#ifdef __DETAILED_PESTICIDE_FATE
		#ifdef __FLOWER_PESTICIDE
			TransferPesticide();
		#endif
	#endif
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::DailyQueueAdd( LE*   a_element_sprayed, double a_amount, PlantProtectionProducts a_ppp )
{
	/**
	* Pesticide spraying event is added to the end of the daily spraying queue.
	*/
	PesticideEvent *l_event = new PesticideEvent( a_element_sprayed, a_amount, a_ppp );
    m_daily_spray_queue[a_ppp].push_back(l_event);
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::DailyQueueAddSeedCoating(LE* a_element_sprayed, double a_amount, PlantProtectionProducts a_ppp)
{
	/**
	* Pesticide seed coating event is added to the end of the daily seed coating queue.
	*/
	PesticideEvent* l_event = new PesticideEvent(a_element_sprayed, a_amount, a_ppp);
	m_daily_seedcoating_queue[a_ppp].push_back(l_event);
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::DailyQueueAddGranular(LE* a_element_sprayed, double a_amount, PlantProtectionProducts a_ppp)
{
	/**
	* Pesticide granular applicaiton event is added to the end of the daily granular queue.
	*/
	PesticideEvent* l_event = new PesticideEvent(a_element_sprayed, a_amount, a_ppp);
	m_daily_granular_queue[a_ppp].push_back(l_event);
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::RemovePlantPesticide(int minx, int maxx, int miny, int maxy, int map_index)
{
	/**
	* Sets every entry in vegcanopy, in_vegetation, nectar, pollen and seed maps to 0 
	*/

#ifdef __DETAILED_PESTICIDE_FATE
	for (int p = 0; p < m_NoPPPs; p++) {
		for (int y = miny; y <= maxy; y++) {
			int l_y = y >> PEST_GRIDSIZE_POW2;
			int t = l_y * m_pest_map_width;
			for (int x = minx; x <= maxx; x++) {
				int l_x = x >> PEST_GRIDSIZE_POW2;
				if (m_land->Get(x, y) == map_index) {
					m_pest_map_vegcanopy.at(p).at(t + l_x) = 0;
#ifdef __FLOWER_PESTICIDE		
					m_pest_map_in_vegetation.at(p).at(t + l_x) = 0;
					m_pest_map_nectar.at(p).at(t + l_x) = 0;
					m_pest_map_pollen.at(p).at(t + l_x) = 0;
					if (l_pest_enable_seed_coating.value()) {
						m_pest_map_seed.at(p).at(t + l_x) = 0;
					}
#endif
				}
			}
		}
	}
#endif

}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::MainMapDecay(PlantProtectionProducts a_ppp)
{
	/**
	* This is where the environmental decay takes place. Here we assume a first order decay based on a daily proportion of the cell total. 
	* If using detailed fate modelling first the rain runoff is calculated, transfering some of the pesticide from the plant surface to the soil.
	* The decay of each map is done by calling the DecayMap method.
	*/

	if (!m_something_to_decay[a_ppp]) return;
	double l_zero = l_pest_zero_threshold.value();
	m_something_to_decay[a_ppp] = false;

#ifdef __DETAILED_PESTICIDE_FATE
	// Calculate rain washoff  
	for (unsigned int i = 0; i < m_pest_map_size; i++) {
		if ((m_pest_map_vegcanopy[a_ppp][i] > l_zero)) {
			int x = (i % m_pest_map_width) << PEST_GRIDSIZE_POW2;
			int y = (i / m_pest_map_width) << PEST_GRIDSIZE_POW2;
			double cover = g_landscape_p->SupplyVegCover(x, y);
			unsigned cov = 100 * cover; // cov is steps of zero to 99
			if (cov == 100) {
				std::cout << "Warning: Cover is 100% so it is set to 99% to be within bounds" << std::endl;
				cov = 99;
			}
			// Look up rain washoff factor in precalculated table
			double Rwp = m_pest_map_vegcanopy[a_ppp][i] * m_RainWashoffFactor[a_ppp][m_rainfallcategory + cov];
			m_pest_map_soil[a_ppp][i] += Rwp;
			m_pest_map_vegcanopy[a_ppp][i] -= Rwp;
		}
	}

	// Decay the pesticide on plant surface and in soil
	DecayMap(m_pest_map_vegcanopy, m_pest_daily_decay_frac_vegcanopy, a_ppp);
	DecayMap(m_pest_map_soil, m_pest_daily_decay_frac_soil, a_ppp);
#ifdef __FLOWER_PESTICIDE
	// Decay the pesticide in plant, pollen and nectar
	DecayMap(m_pest_map_in_vegetation, m_pest_daily_decay_frac_in_vegetation, a_ppp);
	DecayMap(m_pest_map_nectar, m_pest_daily_decay_frac_nectar, a_ppp);
	DecayMap(m_pest_map_pollen, m_pest_daily_decay_frac_pollen, a_ppp);
#endif
#else
	// Decay pesticide
	DecayMap(m_pest_map_main, m_pest_daily_decay_frac, a_ppp);
#endif

}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::DecayMap(vector<vector<double>>& map, vector<double>& decay_frac, int a_ppp) 
{
	/**
	* Loops over each cell in the map and decays the pesticide if it is higher than the threshold, 
	* otherwise the pesticide concentration is set to 0.
	* If several cores are available, this is done in parallel.
	*/
	double l_zero = l_pest_zero_threshold.value();
	for_each(
		execution::par,
		begin(map[a_ppp]),
		end(map[a_ppp]),
		[&](double& val) {
			if (val > l_zero) {
				val *= decay_frac[a_ppp];
				m_something_to_decay[a_ppp] = true;
			}
			else {
				val = 0.0;
			}
		});
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::DailyQueueProcess( PlantProtectionProducts a_ppp )
{
	/**
	* Method runs if we are spraying at least one field. Sets something_to_decay to true so the decay method will run tomorrow.
	* First we make sure the twin map is cleared.
	* Then we add the amount m_amount to the twin map (all squares covered with the polygon get m_amount added) using #TwinMapSpray \n
	* Next this twin map is added to the main map taking into acount drift  and if necessary it is here we sort out the allocation between different compartments.
	* This is done by #TwinMapDiffusion \n
	*/
	if (0 == m_daily_spray_queue[a_ppp].size() && 0 == m_daily_seedcoating_queue[a_ppp].size() && 0 == m_daily_granular_queue[a_ppp].size())
    // Event queues empty, nobody sprayed anything today.
    return;

	// Spraying at least one field. Force the main pesticide map decay method to run tomorrow.
	m_something_to_decay[a_ppp] = true;

	for ( unsigned int i=0; i<m_daily_spray_queue[a_ppp].size(); i++ ){
		int minx=m_daily_spray_queue[a_ppp][i]->m_sprayed_elem->GetMinX();
		int maxx=m_daily_spray_queue[a_ppp][i]->m_sprayed_elem->GetMaxX();
		int miny=m_daily_spray_queue[a_ppp][i]->m_sprayed_elem->GetMinY();
		int maxy=m_daily_spray_queue[a_ppp][i]->m_sprayed_elem->GetMaxY();
		// For cover we use the crop, and save this for later
		double cover = m_daily_spray_queue[a_ppp][i]->m_sprayed_elem->GetVegCover();
		TwinMapClear(minx >> PEST_GRIDSIZE_POW2, miny >> PEST_GRIDSIZE_POW2, maxx >> PEST_GRIDSIZE_POW2, maxy >> PEST_GRIDSIZE_POW2);
		// Add the amount in m_amount to the twin map (all squares covered with the polygon get m_amount added.
		TwinMapSpray( m_daily_spray_queue[a_ppp][i]->m_sprayed_elem, m_daily_spray_queue[a_ppp][i]->m_amount, minx, miny, maxx, maxy);
		// This adds it to the main map and if necessary sorts out the allocation between veg and soil.
		TwinMapDiffusion(minx, miny, maxx, maxy, cover, a_ppp);
	}
#ifdef __DETAILED_PESTICIDE_FATE
#ifdef __FLOWER_PESTICIDE
	// Apply seed coating
	if (l_pest_enable_seed_coating.value()) {
		for (unsigned int i = 0; i < m_daily_seedcoating_queue[a_ppp].size(); i++) {
			int minx = m_daily_seedcoating_queue[a_ppp][i]->m_sprayed_elem->GetMinX();
			int maxx = m_daily_seedcoating_queue[a_ppp][i]->m_sprayed_elem->GetMaxX();
			int miny = m_daily_seedcoating_queue[a_ppp][i]->m_sprayed_elem->GetMinY();
			int maxy = m_daily_seedcoating_queue[a_ppp][i]->m_sprayed_elem->GetMaxY();
			TwinMapClear(minx >> PEST_GRIDSIZE_POW2, miny >> PEST_GRIDSIZE_POW2, maxx >> PEST_GRIDSIZE_POW2, maxy >> PEST_GRIDSIZE_POW2);
			// Add the amount in m_amount to the twin map (all squares covered with the polygon get m_amount added.
			TwinMapSpray(m_daily_seedcoating_queue[a_ppp][i]->m_sprayed_elem, m_daily_seedcoating_queue[a_ppp][i]->m_amount, minx, miny, maxx, maxy);
			// Add it to the seed coating map
			AddSeedCoating(minx, miny, maxx, maxy, a_ppp);
		}
	}
#endif
#endif
	// Apply granular pesticide
	for (unsigned int i = 0; i < m_daily_granular_queue[a_ppp].size(); i++) {
		int minx = m_daily_granular_queue[a_ppp][i]->m_sprayed_elem->GetMinX();
		int maxx = m_daily_granular_queue[a_ppp][i]->m_sprayed_elem->GetMaxX();
		int miny = m_daily_granular_queue[a_ppp][i]->m_sprayed_elem->GetMinY();
		int maxy = m_daily_granular_queue[a_ppp][i]->m_sprayed_elem->GetMaxY();
		TwinMapClear(minx >> PEST_GRIDSIZE_POW2, miny >> PEST_GRIDSIZE_POW2, maxx >> PEST_GRIDSIZE_POW2, maxy >> PEST_GRIDSIZE_POW2);
		// Add the amount in m_amount to the twin map (all squares covered with the polygon get m_amount added.
		TwinMapSpray(m_daily_granular_queue[a_ppp][i]->m_sprayed_elem, m_daily_granular_queue[a_ppp][i]->m_amount, minx, miny, maxx, maxy);
		// Add it to the seed coating map
		AddGranularPesticide(minx, miny, maxx, maxy, a_ppp);
	}
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::TwinMapClear(int a_minx, int a_miny, int a_maxx, int a_maxy)
{
	/**
	* Clears the part of the twin map where we will be spraying now
	*/
	for (int i = a_minx; i<=a_maxx; i++)
		for (int j = a_miny; j <= a_maxy; j++) {
			m_pest_map_twin[i + j*m_pest_map_width] = 0.0;
		}
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::TwinMapSpray( LE* a_element_sprayed, double a_amount, int a_minx, int a_miny, int a_maxx, int a_maxy )
{
	/**
	* This is where the initial pesticide concentration is applied to the map. A twin of the real maps is used for spraying the amount of pesticide that
	* is sprayed over each cell and then copied to the real one by using a 'diffusion' process to spread it out to all surrounding cells for drift.
	*
	* Going through the whole landscape is very slow and unnecessary for small polygons.
	* Since our polygons do not extend beyond the edge of the map
	* ie do not wrap round, then we only need a measure of minx, maxx, miny, maxy.
	* This is set up at the start of the simulation.
	*/

	double l_fractional_amount = a_amount * m_prop;
	int l_large_map_index = a_element_sprayed->GetMapIndex();
	for ( int y=a_miny; y<=a_maxy; y++ ) {
		for ( int x=a_minx; x<=a_maxx; x++ ) {
			if ( m_land->Get( x, y ) == l_large_map_index )
			{
				// This adds the l_fractional_amount to the twin map
				TwinMapSprayPixel( x, y, l_fractional_amount );
			}
		}
	}
	TwinMapSprayCorrectBorders();
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::TwinMapSprayCorrectBorders( void )
{
	/**
	* Checks if the borders of the pesticide map doesn't fit the landscape map and makes a correction to the affected cells if needed
	*/
	if ( m_x_excess ) {
		for ( unsigned int i=0; i<m_pest_map_width-1; i++ ) {
			m_pest_map_twin[ i * m_pest_map_width + m_pest_map_height-1 ] *=	m_corr_x;
		}
	}

	if ( m_y_excess ) {
		unsigned int l_additive = (m_pest_map_height-1)*m_pest_map_width;
		for ( unsigned int i=0; i<m_pest_map_height-1; i++ ) {
			m_pest_map_twin[ i + l_additive ] *=	m_corr_y;
		}
	}

	if ( m_x_excess && m_y_excess ) {
		m_pest_map_twin[ m_pest_map_size-1 ] *= (((double)(PEST_GRIDAREA)) / ((double)(m_x_excess*m_y_excess)));
	}
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::TwinMapDiffusion( int a_minx, int a_miny, int a_maxx, int a_maxy, double a_cover, PlantProtectionProducts a_ppp )
{
	/**
	* Applies drift (which depends on the wind direction) to the pesticide from the twin map and adds it to the main map by calling DiffusionSprayPixel
	*/
	int l_length = l_pest_diffusion_distance.value();
	// Sort out which direction the drift should be applied in
	int dx_min = 0, dx_max = 0;
	int dy_min = 0, dy_max = 0;
	if		(m_wind == 0){ dy_max = l_length;  } // North
	else if (m_wind == 1){ dx_min = -l_length; } // East
	else if (m_wind == 2){ dy_min = -l_length; } // South
	else if (m_wind == 3){ dx_max = l_length;  } // West
	// Loop over cells in twin map
	for ( int y=a_miny; y<a_maxy; y++ ){
		int l_y = y >> PEST_GRIDSIZE_POW2;
		int t = l_y*m_pest_map_width;
		for ( int x=a_minx; x<a_maxx; x++ ){
			int l_x = x >> PEST_GRIDSIZE_POW2;
			double l_amount = m_pest_map_twin[t + l_x];
			if ( l_amount > 0.0 ){
				// Loop over diffusion vector
				for (int dx = dx_min; dx <= dx_max; dx++) {
					for (int dy = dy_min; dy <= dy_max; dy++) {
						int i = max(abs(dx), abs(dy));
						DiffusionSprayPixel((x + dx) >> PEST_GRIDSIZE_POW2, m_pest_map_width, (y + dy) >> PEST_GRIDSIZE_POW2, m_pest_map_height, m_diffusion_vector[i] * l_amount, a_cover, a_ppp);
					}
				}
			}
		}
	}
}
//-----------------------------------------------------------------------------------------------------------------------------

#ifdef __DETAILED_PESTICIDE_FATE
inline void Pesticide::DiffusionSprayPixel(int a_x, int a_limit_x, int a_y, int a_limit_y, double a_amount, double a_cover, PlantProtectionProducts a_ppp)
#else
inline void Pesticide::DiffusionSprayPixel(int a_x, int a_limit_x, int a_y, int a_limit_y, double a_amount, double /* a_cover */, PlantProtectionProducts a_ppp)
#endif
{
	/**
	* This sprays the pesticide (a_amount) by placing this into a pesticide cell in the main map. If more detailed pesticide fate is needed
	* then a_amount will be partitioned between soil and canopy. \n
	* First a test is made to ensure that the coordinates given are within the landscape. If not the pesticide is sprayed off world, and lost. \n
	* Partioning, if occuring, is done based on two components - the canopy and the soil. The pesticide is partioned between the two based on the
	* asssumed vegetation cover of the crop based on Beer's Law.\n
	* The amount on the vegetation is further splitted into three components: vegetation surface, nectar and pollen if the flower model is used. \n
	*/
	// First we have to do the typical out of bounds checks - if these fail do nothing, the pesticide fell off the world
	if (a_x < 0 || a_x >= a_limit_x || a_y < 0 || a_y >= a_limit_y)	return;
	// Now calculate the coordinate entry in the array and store this as l_coord
	int l_coord = a_y * a_limit_x + a_x;
#ifdef __DETAILED_PESTICIDE_FATE
#ifdef __FLOWER_PESTICIDE
	// We deal the overspray on nectar and pollen if they exist
	double amount_on_surface = a_amount * a_cover;
	double amount_on_pollen = 0;
	double amount_on_nectar = 0;
	PollenNectarData current_nectar = m_map->SupplyNectar(a_x, a_y);
	if(current_nectar.m_quantity>0){
		amount_on_nectar = amount_on_surface * cfg_pest_overspray_rate_to_nectar.value();
		m_pest_map_nectar[a_ppp][l_coord] += amount_on_nectar;
	}
	PollenNectarData current_pollen = m_map->SupplyPollen(a_x, a_y);
	if(current_pollen.m_quantity>0){
		amount_on_pollen = amount_on_surface * cfg_pest_overspray_rate_to_pollen.value();
		m_pest_map_pollen[a_ppp][l_coord] += amount_on_pollen;
	}
	m_pest_map_vegcanopy[a_ppp][l_coord] += amount_on_surface*(1 - amount_on_pollen - amount_on_nectar);

	m_pest_map_soil[a_ppp][l_coord] += a_amount - amount_on_surface;
#else
	// Here we need to calculate the partition of pesticide into two compartments.
	m_pest_map_vegcanopy[a_ppp][l_coord] += a_amount * a_cover;
	m_pest_map_soil[a_ppp][l_coord] += a_amount * (1-a_cover);
#endif
	
#else
	m_pest_map_main[a_ppp][l_coord] += a_amount;
#endif
	if (l_pest_record_used.value()) {
		m_pest_map_record[a_ppp][l_coord] += a_amount;
	}
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::DailyQueueClear(PlantProtectionProducts a_ppp)
{
	/**
	* Empties and resets the pesticide action queue. On calling any event not yet carried out will be deleted.
	*/
	if (!m_daily_spray_queue[a_ppp].empty())  m_daily_spray_queue[a_ppp].resize(0);
#ifdef __DETAILED_PESTICIDE_FATE
#ifdef __FLOWER_PESTICIDE
	if (l_pest_enable_seed_coating.value()) {
		if (!m_daily_seedcoating_queue[a_ppp].empty()) m_daily_seedcoating_queue[a_ppp].resize(0);
	}
#endif
#endif
	if (!m_daily_granular_queue[a_ppp].empty())  m_daily_granular_queue[a_ppp].resize(0);
}
//-----------------------------------------------------------------------------------------------------------------------------

#ifdef __DETAILED_PESTICIDE_FATE
#ifdef __FLOWER_PESTICIDE
void Pesticide::TransferPesticide(void) {
	/**
	* Only called for the flower pesticide model. The pesticide is transfered between the different compartments with rates that are given in the config file.
	* The pesticide is either transfered from one compartment to another or into two others by calling TransferToOne or TransferToTwo.
	*/
	for (int ppp = ppp_1; ppp < m_NoPPPs; ppp++) {
		if (m_something_to_decay[ppp]) {
			//plant to pollen
			TransferToOne(m_pest_map_in_vegetation, cfg_pest_transfer_rate_plant_to_pollen.value(), m_pest_map_pollen, ppp, true, false);
			//plant to nectar
			TransferToOne(m_pest_map_in_vegetation, cfg_pest_transfer_rate_plant_to_nectar.value(), m_pest_map_nectar, ppp, false, true);
			//surface to plant
			TransferToOne(m_pest_map_vegcanopy, cfg_pest_absorption_rate_surface_to_plant.value(), m_pest_map_in_vegetation, ppp, false, false);
			//soil to plant
			TransferToOne(m_pest_map_soil, cfg_pest_transfer_rate_soil_to_plant.value(), m_pest_map_in_vegetation, ppp, false, false);
		}
		//seed coating to plant/soil
		if (l_pest_enable_seed_coating.value() && m_seedcoating_to_decay[ppp]) {
			TransferToTwo(m_pest_map_seed, cfg_pest_transfer_rate_seedcoating_to_plant.value(), m_pest_map_in_vegetation, cfg_pest_transfer_rate_seedcoating_to_soil.value(), m_pest_map_soil, ppp);
			m_something_to_decay[ppp] = true;
		}
	}
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::TransferToOne(vector<vector<double>>& map_from, const double& transfer_rate, vector<vector<double>>& map_to, int a_ppp, bool checkPollen, bool checkNectar) {
	/**
	* Loops over the cells in the map and transfers part of the pesticide to another map with the given rate. 
	* If several cores are available, this is done in parallel.
	*/
	double l_zero = l_pest_zero_threshold.value();
	for_each(
		execution::par,
		begin(m_pest_map_indices),
		end(m_pest_map_indices),
		[&](int& i) {
			if (map_from.at(a_ppp).at(i) > l_zero) {
				bool doTransfer = false;
				if (checkPollen) {
					int x = (i % m_pest_map_width) << PEST_GRIDSIZE_POW2;
					int y = (i / m_pest_map_width) << PEST_GRIDSIZE_POW2;
					PollenNectarData current_pollen = g_landscape_p->SupplyPollen(x, y);
					if (current_pollen.m_quantity > 0) {
						doTransfer = true;
					}
				}
				else if (checkNectar) {
					int x = (i % m_pest_map_width) << PEST_GRIDSIZE_POW2;
					int y = (i / m_pest_map_width) << PEST_GRIDSIZE_POW2;
					PollenNectarData current_nectar = g_landscape_p->SupplyNectar(x, y);
					if (current_nectar.m_quantity > 0) {
						doTransfer = true;
					}
				}
				else {
					doTransfer = true;
				}
				if (doTransfer) {
					map_to.at(a_ppp).at(i) += map_from.at(a_ppp).at(i) * transfer_rate;
					map_from.at(a_ppp).at(i) *= (1 - transfer_rate);
				}
			}
		});
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::TransferToTwo(vector<vector<double>>& map_from, const double& transfer_rate_1, vector<vector<double>>& map_to_1, const double& transfer_rate_2, vector<vector<double>>& map_to_2, int a_ppp) {
	/**
	* Loops over the cells in the map and transfers part of the pesticide to two other maps with the given rates.
	* If several cores are available, this is done in parallel.
	*/
	double l_zero = l_pest_zero_threshold.value();
	for_each(
		execution::par,
		begin(m_pest_map_indices),
		end(m_pest_map_indices),
		[&](int& i) {
			if (map_from.at(a_ppp).at(i) > l_zero) {
				map_to_1.at(a_ppp).at(i) += map_from.at(a_ppp).at(i) * transfer_rate_1;
				map_to_2.at(a_ppp).at(i) += map_from.at(a_ppp).at(i) * transfer_rate_2;
				map_from.at(a_ppp).at(i) *= (1 - transfer_rate_1 - transfer_rate_2);
			}
		});
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::AddSeedCoating(int minx, int miny, int maxx, int maxy, PlantProtectionProducts a_ppp)
{
	/**
	* Seed coating is added to each cell in polygon by looping over rectangle around polygon (minx, miny, maxx, maxy) and checking whether each cell is part of polygon.
	*/

	for (int y = miny; y <= maxy; y++) {
		int l_y = y >> PEST_GRIDSIZE_POW2;
		int t = l_y * m_pest_map_width;
		for (int x = minx; x <= maxx; x++) {
			int l_x = x >> PEST_GRIDSIZE_POW2;
			m_pest_map_seed.at(a_ppp).at(t + l_x) += m_pest_map_twin[t + l_x];
		}
	}
	m_seedcoating_to_decay[a_ppp] = true;
}
//-----------------------------------------------------------------------------------------------------------------------------

void Pesticide::SeedCoatingMapDecay(PlantProtectionProducts a_ppp)
{
	/**
	* Loops over each cell in the seed coating map and decays the pesticide if it is higher than the threshold,
	* otherwise the pesticide concentration is set to 0.
	* If several cores are available, this is done in parallel.
	*/
	if (!m_seedcoating_to_decay[a_ppp]) return;
	m_seedcoating_to_decay[a_ppp] = false;
	double l_zero = l_pest_zero_threshold.value();
	for_each(
		execution::par,
		begin(m_pest_map_seed[a_ppp]),
		end(m_pest_map_seed[a_ppp]),
		[&](double& val) {
			if (val > l_zero) {
				val *= m_pest_daily_decay_frac_seedcoating[a_ppp];
				m_seedcoating_to_decay[a_ppp] = true;
			}
			else {
				val = 0.0;
			}
		});
}
//-----------------------------------------------------------------------------------------------------------------------------
#endif
#endif

void Pesticide::AddGranularPesticide(int minx, int miny, int maxx, int maxy, PlantProtectionProducts a_ppp)
{
	/**
	* Granular pesticide is added to each cell in polygon by looping over rectangle around polygon (minx, miny, maxx, maxy) and checking whether each cell is part of polygon.
	*/

	for (int y = miny; y <= maxy; y++) {
		int l_y = y >> PEST_GRIDSIZE_POW2;
		int t = l_y * m_pest_map_width;
		for (int x = minx; x <= maxx; x++) {
			int l_x = x >> PEST_GRIDSIZE_POW2;
#ifdef __DETAILED_PESTICIDE_FATE
			m_pest_map_soil.at(a_ppp).at(t + l_x) += m_pest_map_twin[t + l_x];
#else
			m_pest_map_main.at(a_ppp).at(t + l_x) += m_pest_map_twin[t + l_x];
#endif
		}
	}
	m_something_to_decay[a_ppp] = true;
}
//-----------------------------------------------------------------------------------------------------------------------------

Pesticide::Pesticide(Landscape *a_map )
{
	/**
	* If the pesticide engine is on, then maps and vectors are initialized here.
	* The rain washoff factor and diffusion vector are also calculated.
	*/
	if (l_pest_enable_pesticide_engine.value())
	{
		/** First determine the size of the maps we need in terms of the number of PPPs to track */
		m_NoPPPs = l_pest_NoPPPs.value();
		m_map = a_map;
		m_land = a_map->SupplyRasterMap();
		for (int i = 0; i < m_NoPPPs; i++) m_something_to_decay[i] = false;
		for (int i = 0; i < m_NoPPPs; i++) m_seedcoating_to_decay[i] = false;
		m_daily_spray_queue.resize(m_NoPPPs);
		m_daily_seedcoating_queue.resize(m_NoPPPs);
		m_daily_granular_queue.resize(m_NoPPPs);

		// Figure out critical border coordinates, proportional fractions etc.
		m_prop = 1.0 / ((double)(PEST_GRIDAREA));
		m_x_excess = m_land->MapWidth() & (PEST_GRIDSIZE - 1);
		if (m_x_excess) {
			m_corr_x = (double)PEST_GRIDSIZE / (double)m_x_excess;
		}
		m_y_excess = m_land->MapHeight() & (PEST_GRIDSIZE - 1);
		if (m_y_excess) {
			m_corr_y = (double)PEST_GRIDSIZE / (double)m_y_excess;
		}

		// Calculate width, height and size of pesticide map
		m_pest_map_width = m_land->MapWidth() >> PEST_GRIDSIZE_POW2; // landscape width divided by the sqrt(cellsize)  e.g. cellsize 4 and landscape width = 10000, result is 10000/(sqrt(4) = 5000
		if (m_land->MapWidth() & (PEST_GRIDSIZE - 1))
			m_pest_map_width++;

		m_pest_map_height = m_land->MapHeight() >> PEST_GRIDSIZE_POW2;
		if (m_land->MapHeight() & (PEST_GRIDSIZE - 1))
			m_pest_map_height++;

		m_pest_map_size = m_pest_map_width * m_pest_map_height;

		// Initialize twin map
		m_pest_map_twin.resize(m_pest_map_size);
		for (double i : m_pest_map_twin) i = 0.0;

		// Initialize the pesticide load map
		if (l_pest_record_used.value()) {
			m_pest_map_record.resize(m_NoPPPs);
			for (int p = 0; p < m_NoPPPs; p++) {
				m_pest_map_record[p].resize(m_pest_map_size);
				std::fill(m_pest_map_record[p].begin(), m_pest_map_record[p].end(), 0.0);
			}
		}

#ifdef __DETAILED_PESTICIDE_FATE
		// Initialize maps and vectors for detailed pesticide model
		m_pest_map_indices.resize(m_pest_map_size);
		std::iota(m_pest_map_indices.begin(), m_pest_map_indices.end(), 0);
		m_pest_map_vegcanopy.resize(m_NoPPPs);
		m_pest_map_soil.resize(m_NoPPPs);
		m_RainWashoffFactor.resize(m_NoPPPs);
		for (int p = 0; p < m_NoPPPs; p++) {
			m_pest_map_vegcanopy[p].resize(m_pest_map_size);
			m_pest_map_soil[p].resize(m_pest_map_size);
			std::fill(m_pest_map_vegcanopy[p].begin(), m_pest_map_vegcanopy[p].end(), 0.0);
			std::fill(m_pest_map_soil[p].begin(), m_pest_map_soil[p].end(), 0.0);
			m_RainWashoffFactor.at(p).resize(10000);
		}
#ifdef __FLOWER_PESTICIDE		
		// Initialize maps for flower pesticide model
		m_pest_map_in_vegetation.resize(m_NoPPPs);
		m_pest_map_nectar.resize(m_NoPPPs);
		m_pest_map_pollen.resize(m_NoPPPs);
		for (int p = 0; p < m_NoPPPs; p++) {
			m_pest_map_in_vegetation.at(p).resize(m_pest_map_size);
			m_pest_map_nectar.at(p).resize(m_pest_map_size);
			m_pest_map_pollen.at(p).resize(m_pest_map_size);
			std::fill(m_pest_map_in_vegetation.at(p).begin(), m_pest_map_in_vegetation.at(p).end(), 0.0);
			std::fill(m_pest_map_nectar.at(p).begin(), m_pest_map_nectar.at(p).end(), 0.0);
			std::fill(m_pest_map_pollen.at(p).begin(), m_pest_map_pollen.at(p).end(), 0.0);
		}
		
		// Initialize the seed coating map
		if (l_pest_enable_seed_coating.value()) {
			m_pest_map_seed.resize(m_NoPPPs);
			for (int p = 0; p < m_NoPPPs; p++) {
				m_pest_map_seed.at(p).resize(m_pest_map_size);
				std::fill(m_pest_map_seed.at(p).begin(), m_pest_map_seed.at(p).end(), 0.0);
			}
		}
#endif
#else
		// Initialize map for one compartment pesticide model
		m_pest_map_main.resize(m_NoPPPs);
		for (int p = 0; p < m_NoPPPs; p++) {
			m_pest_map_main[p].resize(m_pest_map_size);
			fill(m_pest_map_main[p].begin(), m_pest_map_main[p].end(), 0.0);
		}
#endif
		// Initialize daily spray queue
		for (int p = 0; p < m_NoPPPs; p++) {
			m_daily_spray_queue[p].resize(0);
			m_daily_seedcoating_queue[p].resize(0);
			m_daily_granular_queue[p].resize(0);
		}
		
#ifdef __DETAILED_PESTICIDE_FATE
		// Calculate rain washoff factors
		for (int p = 0; p < m_NoPPPs; p++) {
			CalcRainWashOffFactors(p);
		}
#endif
		// Calculate diffusion vector due to drift
		DiffusionVectorInit();
#ifdef PEST_DEBUG
		DiffusionVectorTest();
#endif
		// If turned on, create output files for saving pesticide load and concentration
		if (l_pest_record_used.value()) {
			m_pesticideRecord.open("PesticideGridRecord.txt", ios::out);
			m_pesticideRecord << "GridSize:" << '\t' << l_pest_record_grid_size.value() << '\t' << " GridsWide:" << '\t' << int(m_land->MapWidth() / l_pest_record_grid_size.value()) << endl;
		}
		if (l_pest_save_map_daily.value()){
#ifdef __DETAILED_PESTICIDE_FATE
			m_pesticide_file_rec_soil.open("PesticideMapSoil.txt", ios::out);
			m_pesticide_file_rec_vegcanopy.open("PesticideMapVegCanopy.txt", ios::out);
#ifdef __FLOWER_PESTICIDE
			m_pesticide_file_rec_in_vegetation.open("PesticideMapInVegetation.txt", ios::out);
			m_pesticide_file_rec_nectar.open("PesticideMapNectar.txt", ios::out);
			m_pesticide_file_rec_pollen.open("PesticideMapPollen.txt", ios::out);
			if (l_pest_enable_seed_coating.value()) {
				m_pesticide_file_rec_seed.open("PesticideMapSeed.txt", ios::out);
			}
#endif
#else
			m_pesticide_file_rec.open("PesticideMap.txt", ios::out);
#endif
		}
	}
}
//-----------------------------------------------------------------------------------------------------------------------------

#ifdef __DETAILED_PESTICIDE_FATE
void Pesticide::CalcRainWashOffFactors(int a_ppp)
{
	/**
	* Calculates the proportion of pesticide that is washed off the canopy for 0.1 to 10mm of rain and for 0 to 100% cover in 1% steps.
	* Uses Beer's law and takes into account the water solubility of the pesticide.
	*/
	double rainsteps = 0.1; // mm, multiply rainfall by 100 to get this, save result as integer
	double coversteps = 0.01; // %, multiply cover by 100 to get this, save result as integer
	double w = 0.0016*pow(cfg_pest_solubility.value(a_ppp),0.3832); // wash-off factor dependent on pestiside water solubility (from EFSA Journal 2012;10(2):2562)
	//std::cout << "PPP" << a_ppp << ": wash-off factor = " << w << std::endl;
	for (int i = 0; i < 100; i++){
		double SC = i * coversteps; // goes from 0 to 1
		double LAI = 1/cfg_beer_law_extinction_coef.value() * log(1 / (1 - SC)); // This is the inverse of Beer's Law
		for (int r = 0; r < 100; r++){
			double P = r * rainsteps;
			double Pi = 0;
			if (LAI != 0) Pi = 0.25 * LAI * (1 - (1 / (1 + (SC * P / (0.25 * LAI))))); // Intercepted precipitation which doesn't reach the ground
			double Rw = w * SC * (P - Pi); // Rw is a proportion of canopy pesticide washed off. 
			//std::cout << Rw << '\t';
			m_RainWashoffFactor[a_ppp][r * 100 + i] = Rw;
		}
		//std::cout << std::endl;
	}
}
//-----------------------------------------------------------------------------------------------------------------------------
#endif

void Pesticide::DiffusionVectorInit(void)
{
	/**
	* l_pest_diffusion_distance contains the distance in meters away from the cell where the pesticide was applied to consider.
	* The length of the diffusion vector will be 1 + l_pest_diffusion_distance <br>
	*/
	int l_length = l_pest_diffusion_distance.value() + 1;
	m_diffusion_vector.resize(l_length);
	double sum = 0.0;
	for (int l = 1; l < l_length; l++) {
		if (l < l_length - 1) {
			m_diffusion_vector[l] = DiffusionFunction(l, false);
		}
		else {
			m_diffusion_vector[l] = DiffusionFunction(l, true);
		}
		sum += m_diffusion_vector[l];
	}
	m_diffusion_vector[0] = 1.0 - sum;
}

double Pesticide::DiffusionFunction(double a_dist_meters, bool a_is_last)
{
	/**
	The equation provided here is the one that determines the drift of pesticides with distance. It is important that if the
	drift is set to zero that the result of the equation is unity.
	*/
	double pp;
#ifdef __WithinOrchardPesticideSim__
	if (a_dist_meters == 0) pp = 0.7784;
	else pp = 0.0277;
#else
	// Fitting 5 first meter to integral of 2.7705 * pow(a_dist_meters, -0.9787) // german drift equation
	double A = 589;
	double B = 1.59;
	double C = 33.1;
	double D = 0.218;

	// For total pesticide outside field = (2.7705 * pow(a_dist_meters, -1.7)) * 0.01; // german drift equation - reduction of drift by 50%
	//double A = 1619;
	//double B = 2.16;
	//double C = 45.4;
	//double D = 0.408;

	/** These formulas calculate the pesticide drift in a 1 m resolution */
	if (!a_is_last) pp = 1. / 100 * (A / B * (exp(-(a_dist_meters - 0.5) * B) - exp(-(a_dist_meters + 0.5) * B)) + C / D * (exp(-(a_dist_meters - 0.5) * D) - exp(-(a_dist_meters + 0.5) * D))) * 0.01;
	else pp = 1. / 100 * (A / B * (exp(-(a_dist_meters - 0.5) * B) - exp(-(a_dist_meters + 10000) * B)) + C / D * (exp(-(a_dist_meters - 0.5) * D) - exp(-(a_dist_meters + 10000) * D))) * 0.01;

	if (pp < (l_pest_zero_threshold.value() / 10)) pp = 0;  // Don't bother with the little ones
#endif
	return pp;
}

bool Pesticide::RecordPesticideLoad(int a_ppp)
{
	/**
	* The challenge here is that we do not know in advance the size of the pesticide grid, so this needs to be included in the 
	* translation of the pesticide grid to the output grid. This is done here rather than when m_pest_map_record is filled since
	* this needs to be done only once here, rather than every time a pesticide amount is applied.
	* To get to the pesticide grid the formula is (PEST_GRID_SIZE * grid ref) = ALMaSS Coord.  ALMaSS Coord/Grid Size = index.
	*/

	int recordgridsz = l_pest_record_grid_size.value();
	int no_grids = (m_land->MapWidth() / recordgridsz) * (m_land->MapHeight() / recordgridsz);
	int grids_x = m_land->MapWidth() / recordgridsz;
	vector<double> pestrecoutput(no_grids);
	for (unsigned int i = 0; i < no_grids; i++) pestrecoutput[i] = 0.0;
	for (unsigned int j = 0; j < m_pest_map_height; j++)
		for (unsigned int i = 0; i < m_pest_map_width; i++)
		{
			int x = (i * PEST_GRIDSIZE) / recordgridsz;
			int y = (j * PEST_GRIDSIZE) / recordgridsz;
			int index = i + m_pest_map_width * j;
			pestrecoutput[x+y*grids_x] += m_pest_map_record[a_ppp][index];
	}
	m_pesticideRecord << "ppp" << a_ppp << '\t';
	m_pesticideRecord << g_date->Date() - 365 << '\t';
	for (unsigned int i = 0; i < no_grids; i++)
	{
		m_pesticideRecord << pestrecoutput[i] << '\t';
	}
	m_pesticideRecord << endl;
	for (int i = 0; i < m_pest_map_record[a_ppp].size(); i++) m_pest_map_record[a_ppp][i] = 0.0;
	return true;
}
//-----------------------------------------------------------------------------------------------------------------------------

bool Pesticide::RecordPesticideMap(int a_ppp, ofstream& outfile, vector<vector<double>>& map)
{
	/**
	* Saves the pesticide concentration together with the type of pesticide and the date. This is a very heavy operation if it is called every day.
	*/
	int recordgridsz = l_pest_record_grid_size.value();
	int no_grids = (m_land->MapWidth() / recordgridsz) * (m_land->MapHeight() / recordgridsz);
	int grids_x = m_land->MapWidth() / recordgridsz;
	vector<double> pestrecoutput(no_grids);
	for (unsigned int i = 0; i < no_grids; i++) pestrecoutput[i] = 0.0;
	for (unsigned int j = 0; j < m_pest_map_height; j++) {
		for (unsigned int i = 0; i < m_pest_map_width; i++) {
			int x = (i * PEST_GRIDSIZE) / recordgridsz;
			int y = (j * PEST_GRIDSIZE) / recordgridsz;
			int index = i + m_pest_map_width * j;
			pestrecoutput[x + y * grids_x] += map[a_ppp][index];
		}
	}
	outfile << "ppp" << a_ppp << '\t';
	outfile << g_date->Date() - 365 << '\t';
	for (unsigned int i = 0; i < no_grids; i++) {
		outfile << pestrecoutput[i] << '\t';
	}
	outfile << endl;
	
	return true;
}
//-----------------------------------------------------------------------------------------------------------------------------

#ifdef PEST_DEBUG
void Pesticide::DiffusionVectorTest(void) {
	/**
	* For debugging purposes. Allows us to see the diffusion used for the point source in each cell.
	*/
	int l_grid = l_pest_diffusion_distance.value();
	ofstream ofile("diffusion.txt", ios::out);

	for (int x = 0; x <= l_grid; x++) {
		ofile << m_diffusion_vector[x] << '\t';
	}
	ofile << endl;
	ofile.close();
}
//-----------------------------------------------------------------------------------------------------------------------------

// For file saving and loading.
#define CFG_CHANNEL_BITS      8
#define CFG_CHANNEL_MAXVAL    (2^CFG_CHANNEL_BITS-1)

#define SV_UINT32 unsigned int
#define SV_INT32  int
#define SV_UINT8  unsigned char
#define SV_INT8   char

bool Pesticide::SavePPM( double *a_map,
			 int a_beginx, int a_width,
			 int a_beginy, int a_height,
			 char* a_filename )
{
  a_beginx = 0;
  a_width  = m_pest_map_width;
  a_beginy = 0;
  a_height = m_pest_map_height;

  SV_UINT32 linesize   = a_width*3;
  SV_UINT8* linebuffer = (SV_UINT8*)malloc(sizeof(SV_UINT8)* linesize);

  if ( linebuffer == NULL ) {
    g_msg->Warn( WARN_FATAL,
		 "Pesticide::SavePPM(): Out of memory!", "" );
    exit(1);
  }

  FILE* l_file;
  l_file=fopen(a_filename, "w" );
  if ( !l_file ) {
    printf("PesticideTest::SavePPM(): "
	   "Unable to open file for writing: %s\n",
	   a_filename );
    exit(1);
  }

  fprintf( l_file, "P6\n%d %d %d\n",
	   a_width,
	   a_height,
	   255 );

  for ( int line=a_beginy; line< a_beginy + a_height; line++ ) {
    int i = 0;
    for ( int column=a_beginx; column < a_beginx + a_width; column++ ) {
      int localcolor = (int)( a_map[ line * m_pest_map_width + column ]
				  * 255.0);
      if ( localcolor <= 255 ) {
	linebuffer [ i++ ] =  char (localcolor        & 0xff);
	linebuffer [ i++ ] = 0;
	linebuffer [ i++ ] = 0;
      } else {
	linebuffer [ i++ ] = 255;
	localcolor -= 255;
	if ( localcolor <= 255 ) {
	  linebuffer [ i++ ] = char (localcolor);
	  linebuffer [ i++ ] = 0;
	} else {
	  linebuffer [ i++ ] = 255;
	  localcolor -= 255;
	  if ( localcolor <= 255 ) {
	    linebuffer [ i++ ] = char (localcolor);
	  } else {
	    linebuffer [ i++ ] = 255;
	  }
	}
      }
    }
    fwrite( linebuffer, sizeof(SV_UINT8), linesize, l_file );
  }

  fclose( l_file );
  free( linebuffer );
  return true;
}
//-----------------------------------------------------------------------------------------------------------------------------

/*
void Pesticide::Test( Landscape *a_map )
{
  for ( int i=0; i<730; i++) {

    if ( random(100) < 10 ) {
      int l_sprays = random(5)+1;
      for ( int j=0; j<l_sprays; j++ ) {
	int l_x = random(1500);
	int l_y = random(1500);
	DailyQueueAdd( a_map->
		       SupplyLEPointer( a_map->SupplyPolyRef( l_x, l_y )),
		       1.0 );
      }
      char l_filename[20];
      sprintf( l_filename, "ppms/p%04d.ppm", i );
      SavePPM( m_pest_map_main,
	       0, 0, 0, 0,
	       l_filename );
    }

    Tick();
  }
}
//-----------------------------------------------------------------------------------------------------------------------------
*/
#endif // PEST_DEBUG

PesticideOutput::PesticideOutput(int a_startyear, int a_noyears, int a_cellsize, Landscape* a_landscape, RasterMap* a_land)
{
	m_startyear = a_startyear;
	m_endyear = a_startyear + a_noyears;
	m_cellsize = a_cellsize;
	m_OurLandscape = a_landscape;
	m_Rastermap = a_land;
	m_pmap_width = m_OurLandscape->SupplySimAreaWidth() / m_cellsize;
	m_pmap_height = m_OurLandscape->SupplySimAreaHeight() / m_cellsize; ;
}

PesticideMap::PesticideMap(int a_startyear, int a_noyears, int a_cellsize, Landscape* a_landscape, RasterMap* a_land, bool a_typeofmap)
	: PesticideOutput(a_startyear, a_noyears, a_cellsize, a_landscape, a_land)
{
	m_typeofmap = a_typeofmap;

	// Need to create the grid to fill. This grid is always used.
	m_pmap_insecticides = new vector<double>;
	m_pmap_insecticides->resize(m_pmap_width*m_pmap_height);
	fill(m_pmap_insecticides->begin(), m_pmap_insecticides->end(), 0);
	ofstream * m_PMap;
	m_PMap = new ofstream("ALMaSS_InsecticideMap.txt", ios::out);
	(*m_PMap) << "Years" << ' ' << a_noyears << ' ' << "Height" << ' ' << m_pmap_height << ' ' << "Width" << ' ' << m_pmap_width << ' ' << endl;
	m_PMap->close();
	// These two grids are used only if we want all three types recoreded.
	if (!m_typeofmap)
	{
		// We need all three in this case
		m_pmap_fungicides = new vector<double>;
		m_pmap_fungicides->resize(m_pmap_width*m_pmap_height);
		fill(m_pmap_fungicides->begin(), m_pmap_fungicides->end(), 0);
		m_PMap = new ofstream("ALMaSS_FungicideMap.txt", ios::out);
		(*m_PMap) << "Years" << ' ' << a_noyears << ' ' << "Height" << ' ' << m_pmap_height << ' ' << "Width" << ' ' << m_pmap_width << ' ' << endl;
		m_PMap->close();
		m_pmap_herbicides = new vector<double>;
		m_pmap_herbicides->resize(m_pmap_width*m_pmap_height);
		fill(m_pmap_herbicides->begin(), m_pmap_herbicides->end(), 0);
		m_PMap = new ofstream("ALMaSS_HerbicideMap.txt", ios::out);
		(*m_PMap) << "Years" << ' ' << a_noyears << ' ' << "Height" << ' ' << m_pmap_height << ' ' << "Width" << ' ' << m_pmap_width << ' ' << endl;
		m_PMap->close();
	}
	else
	{
		m_pmap_fungicides = NULL;
		m_pmap_herbicides = NULL;
	}
}

PesticideMap::~PesticideMap()
{
	delete m_pmap_insecticides;
	if (!m_typeofmap)
	{
		// We need all three in this case
		delete m_pmap_fungicides;
		delete m_pmap_herbicides;

	}
}

bool PesticideMap::DumpPMap(vector<double>* a_map)
{
	string fname;
	if (a_map == m_pmap_insecticides) fname = "ALMaSS_InsecticideMap.txt";
	else if (a_map == m_pmap_fungicides) fname = "ALMaSS_FungicideMap.txt";
	else  fname = "ALMaSS_HerbicideMap.txt";
	{
		ofstream* m_PMap = new ofstream(fname.c_str(), ios::app);
		if (!(*m_PMap).is_open())
		{
			g_msg->Warn(WARN_FILE, "PesticideMap::DumpMap Unable to open file for append: ", fname.c_str());
			exit(1);
		}
		// File is OK, save the map
		(*m_PMap) << "Year " << g_date->GetYear() << "Month " << g_date->GetMonth() << endl;
		for (int y = 0; y < m_pmap_height; y++)
		{
			for (int x = 0; x < m_pmap_width; x++)
			{
				(*m_PMap) << (*a_map)[y * m_pmap_width + x] << ' ';
			}
		}
		(*m_PMap) << endl;
		m_PMap->close();
	}
	fill(a_map->begin(), a_map->end(), 0);
	return true;
}

void PesticideMap::Spray(LE* a_element_sprayed, TTypesOfPesticideCategory a_type)
{
	/**
	* This records a 1 in the map for every m2 where pesticide is applied.
	* This does not record drift.
	*
	* Going through the whole landscape is very slow and unnecessary for small polygons.
	* Since our polygons do not extend beyond the edge of the map
	* ie do not wrap round, then we only need a measure of minx, maxx, miny, maxy.
	* This is set up at the start of the simulation.
	*
	* This method first determines what type of pesticide and selects the correct map to record on.
	*/
	vector<double>* ourmap = m_pmap_insecticides; ;
	if (a_type == fungicide) ourmap = m_pmap_fungicides;
	else if (a_type == herbicide) ourmap = m_pmap_herbicides;
	int l_large_map_index = a_element_sprayed->GetMapIndex();
	int minx = a_element_sprayed->GetMinX();
	int miny = a_element_sprayed->GetMinY();
	int maxx = a_element_sprayed->GetMaxX();
	int maxy = a_element_sprayed->GetMaxY();
	for (int y = miny; y <= maxy; y++) {
		for (int x = minx; x <= maxx; x++) {
			if (m_Rastermap->Get(x, y) == l_large_map_index)
			{
				// Add one m spray value of 1.0 per m
				(*ourmap)[(x / m_cellsize) + (y / m_cellsize) * m_pmap_width]++;
			}
		}
	}
}

PesticideTable::PesticideTable(int a_startyear, int a_noyears, int a_cellsize, Landscape* a_landscape, RasterMap* a_land) 
	: PesticideOutput(a_startyear, a_noyears, a_cellsize, a_landscape, a_land)
{
	// We need all three in this case
	int sz = m_OurLandscape->SupplyLargestPolyNumUsed();
	m_pmap_insecticides = new vector<double>;
	m_pmap_insecticides->resize(sz+1);
	fill(m_pmap_insecticides->begin(), m_pmap_insecticides->end(), 0);
	m_pmap_herbicides = new vector<double>;
	m_pmap_herbicides->resize(sz);
	fill(m_pmap_herbicides->begin(), m_pmap_herbicides->end(), 0);
	m_pmap_fungicides = new vector<double>;
	m_pmap_fungicides->resize(sz);
	fill(m_pmap_fungicides->begin(), m_pmap_fungicides->end(), 0);

	m_PTableI = new ofstream("ALMaSS_InsecticideTable.txt", ios::out);
	(*m_PTableI) << "Years" << ' ' << a_noyears  << endl;
	m_PTableH = new ofstream("ALMaSS_HerbicideTable.txt", ios::out);
	(*m_PTableH) << "Years" << ' ' << a_noyears << endl;
	m_PTableF = new ofstream("ALMaSS_FungicideTable.txt", ios::out);
	(*m_PTableF) << "Years" << ' ' << a_noyears << endl;
}

void PesticideTable::Spray(LE* a_element_sprayed, TTypesOfPesticideCategory a_type)
{
	/**
	* This records a 1 in the polygon ref table for every field where pesticide is applied.
	* This does not record drift.
	*
	* This method first determines what type of pesticide and selects the correct map to record on.
	*/
	int index = a_element_sprayed->GetPoly();
	if (a_type == fungicide) (*m_pmap_fungicides)[index]+=1.0;
	else if (a_type == herbicide) (*m_pmap_herbicides)[index] += 1.0;
	else (*m_pmap_insecticides)[index] += 1.0;
}

void PesticideTable::PrintPTable()
{
	PrintITable();
	PrintHTable();
	PrintFTable();
}

void PesticideTable::PrintITable()
{
	string fname = "ALMaSS_InsecticideTable.txt";
	{
		if (!(*m_PTableI).is_open())
		{
			g_msg->Warn(WARN_FILE, "PesticideTable::TableFile Unable to open file for append: ", fname.c_str());
			exit(1);
		}
		// File is OK, save the table
		(*m_PTableI) << "Year " << g_date->GetYearNumber() << " Month " << g_date->GetMonth() << " Day " << g_date->GetDayInMonth() << endl;
		for (int i = 0; i < m_pmap_insecticides->size(); i++)
		{
			if ((*m_pmap_insecticides)[i] > 0.0)
			{
				(*m_PTableI) << i << '\t' << (*m_pmap_insecticides)[i] << endl;
			}
		}
	}
	fill(m_pmap_insecticides->begin(), m_pmap_insecticides->end(), 0);
}

void PesticideTable::PrintHTable()
{
	string fname = "ALMaSS_HerbicideTable.txt";
	{
		if (!(*m_PTableH).is_open())
		{
			g_msg->Warn(WARN_FILE, "HerbicideTable::TableFile Unable to open file for append: ", fname.c_str());
			exit(1);
		}
		// File is OK, save the table
		(*m_PTableH) << "Year " << g_date->GetYearNumber() << " Month " << g_date->GetMonth() << " Day " << g_date->GetDayInMonth() << endl;
		for (int i = 0; i < m_pmap_herbicides->size(); i++)
		{
			if ((*m_pmap_herbicides)[i] > 0.0)
			{
				(*m_PTableH) << i << '\t' << (*m_pmap_herbicides)[i] << endl;
			}
		}
	}
	fill(m_pmap_herbicides->begin(), m_pmap_herbicides->end(), 0);
}

void PesticideTable::PrintFTable()
{
	string fname = "ALMaSS_FungicideTable.txt";
	{
		if (!(*m_PTableF).is_open())
		{
			g_msg->Warn(WARN_FILE, "FungicideTable::TableFile Unable to open file for append: ", fname.c_str());
			exit(1);
		}
		// File is OK, save the table
		(*m_PTableF) << "Year " << g_date->GetYearNumber() << " Month " << g_date->GetMonth() << " Day " << g_date->GetDayInMonth() << endl;
		for (int i = 0; i < m_pmap_fungicides->size(); i++)
		{
			if ((*m_pmap_fungicides)[i] > 0.0)
			{
				(*m_PTableF) << i << '\t' << (*m_pmap_fungicides)[i] << endl;
			}
		}
	}
	fill(m_pmap_fungicides->begin(), m_pmap_fungicides->end(), 0);
}

Pesticide* CreatePesticide(Landscape *l)
{
	if (g_pest != NULL)
		delete g_pest;

	g_pest = new Pesticide(l);
	
	return g_pest;
	
}

