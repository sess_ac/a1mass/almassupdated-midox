//
// PollenNectar.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include <fstream>
#include "./Configurator.h"
#include "./MapErrorMsg.h"
#include "./ls.h"

extern TTypesOfPopulation g_Species;

// Configuration variables
extern CfgBool cfg_pollen_nectar_on;
static CfgStr l_map_tov_nectarpolen("MAP_TOV_NECTARPOLLEN_FILE", CFG_CUSTOM, "tovALMaSSNectarPollenInput.txt");
static CfgStr l_map_nectarpolen("MAP_NECTARPOLLEN_FILE", CFG_CUSTOM, "habitatALMaSSNectarPollenInput.txt");
// End configs

PollenNectarDevelopmentData::PollenNectarDevelopmentData(string a_inputfile, string a_tovinputfile)
{
	/** Reads in the number of pollen and nectar curves.
	Either we have one pair for each tov or we need a look-up table, but because many have the same curves a look up table was used.
	This however requires another input file for tov types to pollen/nectar curve number. If not using pollen and nectar then this is waste of space,
	so there is a cfg to turn it off and set all curves to a zero curve.
	*/
	// This forms an empty entry for no pollen or nectar tov types or for the case when pollen and nectar are not used
	PollenNectarDevelopmentCurveSet curveset;
	m_ResourceCurves.resize(1);
	m_ResourceCurves[0] = curveset;
	// make space in the lookup table for all tov types
	m_tov_PollenCurveTable.resize(tov_Undefined + 1);
	for (int i = 0; i < int(tov_Undefined); i++) m_tov_PollenCurveTable[i] = 0;
	// If pollen and nectar are not used skip the next section and use only the zero entries above
	if (cfg_pollen_nectar_on.value())
	{
		/**
			* There are two sets of data structures - some based on plant community types and the rest are crops that can be on a field tole.
			* The methods of input is duplicated as far as possible.
			*
			* tov \n
			* We need to open the input file and read in the tov types that have associated curves. Those not specified are assumed to have no nectar or pollen.
			* Numbers used are those defined in LE_TypeClass::TranslateVegTypes for each tov type.
			* First file entry is the number of defined tov_types, followed by pairs of tov_refnum & pollen curve refnum.
			*
			* Plant community curves and tov related curves \n
			* Plant community curves can be assigned to any polygon and so there is no similar file to the tov crop types file.
			* The format of both plant community and tov resource curves is the same.
			* There are six curves , with a list of day-degree totals for each inflextion point specific to the first three, the second three use the third values..
			* Each line starts with the number of entries.
			* Preceeding this there are two values, the first the curve reference number,  and the second is the temperature threshold for calculating day-degrees.
			Here is an example of an individual entry:\n
			* 10001	4				 \n			
			* 7	-1	0	150	350	550	650	99999	 \n
			* 7	0	0	0	30	50	0	0	 \n
			* 8	-1	0	150	350	550	675	650	99999	 \n
			* 8	0	0	0	15	25	10	15	0	 \n
			* 7	-1	0	150	350	550	650	99999	 \n
			* 7	0	0	0	5	10	5	0	 \n
			 7	1	1	1	1	1	1	1	 \n
			* 7	1	1	1	1	1	1	1	 \n
			* 7	1	1	1	1	1	1	1 \n
			* \n
			*/
		ifstream infile1, infile2;
		std::string filename;
		// First the habitat file
		if (strcmp(a_inputfile.c_str(), "default") == 0)
			filename = l_map_nectarpolen.value();
		else
			filename = a_inputfile.c_str();
		std::cout << "Reading Habitats Nectar Pollen File " << filename << "\n";
		infile1.open(filename, ios::in);
		//check if there is an input file
		if (!infile1.is_open()) {
			g_msg->Warn("PollenNectarDevelopmentData::PollenNectarDevelopmentData Cannot open the file", filename);
			exit(1);
		}
		// Now the tov file
		if (strcmp(a_tovinputfile.c_str(), "default") == 0)
			filename = l_map_tov_nectarpolen.value();
		else
			filename = a_tovinputfile.c_str();
		std::cout << "Reading TOV Nectar Pollen File " << filename << "\n";
		infile2.open(filename, ios::in);
		//check if there is an input file
		if (!infile2.is_open()) {
			g_msg->Warn("PollenNectarDevelopmentData::PollenNectarDevelopmentData Cannot open the file", filename);
			exit(1);
		}
		// Both input files are open so read the data
		//**CJT** To tidy duplicated code later
		int no_curves;
		int tov, curvenum, realtov, nopoints;
		double threshold, up_threshold, value;
		int tov_list[tov_Undefined];
		// First the habitat file
		infile1 >> no_curves; // read the number of data curves needed
		// Now read the curves themselves
		for (int i = 0; i < no_curves; i++)
		{
			curveset.ClearAll();
			infile1 >> curvenum >> threshold >> up_threshold;
			// Now read the day-degree values
			infile1 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile1 >> value;
				curveset.m_DD_nectar.push_back(value);
			}
			// Now read the nectar values
			infile1 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile1 >> value;
				curveset.m_nectarcurve.push_back(value);
			}
			// Now read the sugar DD values
			infile1 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile1 >> value;
				curveset.m_DD_sugar.push_back(value);
			}
			// Now read the sugar values
			infile1 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile1 >> value;
				curveset.m_sugarcurve.push_back(value);
			}
			// Now read the pollen DD values
			infile1 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile1 >> value;
				curveset.m_DD_pollen.push_back(value);
			}
			// Now read the pollen quantity values
			infile1 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile1 >> value;
				curveset.m_pollencurve.push_back(value);
			}
			// For the pollen quality we need to figure out which pollen quality curve we need of the three
			// To do this we check the species enum
			int rubbish = 0;
			int rubbish2 = 2 * (nopoints+1);
			if (g_Species == TOP_Bombus) { rubbish = nopoints+1; rubbish2 = nopoints+1; }
			if (g_Species == TOP_Osmia) { rubbish = 2*(nopoints+1); rubbish2 = 0; }
			// Skip the unused quality curves
			for (int j = 0; j < rubbish; j++)
			{
				infile1 >> value;
			}
			// Now read the pollen quality values
			infile1 >> value;
			for (int j = 0; j < nopoints; j++) {
				infile1 >> value;
				curveset.m_pollenqualitycurve.push_back(value);
			}
			// Skip the rest of the unused quality curves
			for (int j = 0; j < rubbish2; j++) infile1 >> value;
			// The values read in are total, but the model needs slopes to calculate the daily amount added
			//curveset.CalculateSlopes();
			curveset.m_curve_number = curvenum;
			curveset.m_DDthreshold = threshold;
			curveset.m_MaxDDeg = up_threshold;
			m_ResourceCurves.push_back(curveset);
		}		
		infile1.close();
		// Now the tov file
		infile2 >> no_curves; // read the number of pairs needed
		for (int i=0; i< no_curves; i++ )
		{
			infile2 >> tov >> curvenum;
			// convert from the reference number to tov type
			realtov = g_letype->TranslateVegTypes(tov);
			// store the reference in the pollen nectar lookup table
			m_tov_PollenCurveTable[realtov] = curvenum;
			tov_list[i] = tov;
		}
		// Now read the curves themselves
		infile2 >> no_curves; // read the number of data curves needed
		for (int i = 0; i < no_curves; i++)
		{
			curveset.ClearAll();
			infile2 >> curvenum >> threshold >> up_threshold;
			// Now read the day-degree nectar values
			infile2 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile2 >> value;
				curveset.m_DD_nectar.push_back(value);
			}
			// Now read the nectar values
			infile2 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile2 >> value;
				curveset.m_nectarcurve.push_back(value);
			}
			// Now read the day-degree sugar values
			infile2 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile2 >> value;
				curveset.m_DD_sugar.push_back(value);
			}
			// Now read the sugar values
			infile2 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile2 >> value;
				curveset.m_sugarcurve.push_back(value);
			}
			// Now read the day-degree pollen values
			infile2 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile2 >> value;
				curveset.m_DD_pollen.push_back(value);
			}
			// Now read the pollen quantity values
			infile2 >> nopoints;
			for (int j = 0; j < nopoints; j++) {
				infile2 >> value;
				curveset.m_pollencurve.push_back(value);
			}
			// For the pollen quality we need to figure out which pollen quality curve we need of the three
			// To do this we check the species enum
			int rubbish = 0;
			int rubbish2 = 2 * (nopoints +1);
			if (g_Species == TOP_Bombus) { rubbish = nopoints + 1; rubbish2 = nopoints + 1; }
			if (g_Species == TOP_Osmia) { rubbish = 2 * (nopoints + 1); rubbish2 = 0; }
			// Skip the unused quality curves
			for (int j = 0; j < rubbish; j++) infile2 >> value;
			// Now read the pollen quality values
			infile2 >> value;
			for (int j = 0; j < nopoints; j++) {
				infile2 >> value;
				curveset.m_pollenqualitycurve.push_back(value);
			}
			// Skip the rest of the unused quality curves
			for (int j = 0; j < rubbish2; j++) infile2 >> value;
			// The values read in are total, but the model needs slopes to calculate the daily amount added
			//curveset.CalculateSlopes();
			curveset.m_curve_number = curvenum;
			curveset.m_DDthreshold = threshold;
			curveset.m_MaxDDeg = up_threshold;
			m_ResourceCurves.push_back(curveset);
		}
		infile2.close();
	}
}

PollenNectarDevelopmentData::~PollenNectarDevelopmentData()
{
	m_ResourceCurves.clear();
}

PollenNectarData::PollenNectarData()
{
	m_quantity = 0;
	m_quality = 0;

}

PollenNectarData::PollenNectarData(double a_quantity, double a_quality)
{
	m_quantity = a_quantity;
	m_quality = a_quality;
}

PollenNectarDevelopmentData* CreatePollenNectarDevelopmentData()
{
	if (g_nectarpollen == NULL)
	{
		g_nectarpollen = new PollenNectarDevelopmentData;
	}
	return g_nectarpollen;
}

double PollenNectarData::GetNectarSugarConc(){
	
	if(m_quality == 0 ||  m_quantity == 0){
		return 0.0;
	}

	//water
	double w_mg_per_m2 =  m_quantity - m_quality;

	//Density of sucrose and waters
	const double s_mg_per_ul = 1.587;
	const double w_mg_per_ul = 0.997;

	double s_ul_per_m2 = m_quality / s_mg_per_ul;
	double w_ul_per_m2 = w_mg_per_m2 / w_mg_per_ul;
	//nectar
	double n_ul_per_m2 = s_ul_per_m2 + w_ul_per_m2;

	/** Bumblebee nectar quality in vol/vol of sugar to nectar.*/
	double PercSugar = 2.0 * s_ul_per_m2 / n_ul_per_m2;
	if(PercSugar > 1.0){
		PercSugar = 1.0;
	}

	//double PercSugar = 0.65;
	return(PercSugar );
}
