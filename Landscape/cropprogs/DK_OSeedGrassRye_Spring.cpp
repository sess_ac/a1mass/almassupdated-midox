//
// DK_OSeedGrassRye_Spring.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OSeedGrassRye_Spring.h"


bool DK_OSeedGrassRye_Spring::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  TTypesOfVegetation l_tov = tov_DKOSeedGrassRye_Spring;

  switch ( m_ev->m_todo ) {
  case dk_osgrs_start:
  {
	  DK_OSGRS_EVERY_2ND_YEAR = 0;
	  DK_OSGRS_FORCESPRING = false;
	  a_field->ClearManagementActionSum();
	  m_last_date = g_date->DayInYear(1, 9); // Should match the last flexdate below
   //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
	  std::vector<std::vector<int>> flexdates(3 + 1, std::vector<int>(2, 0));
	  // Set up the date management stuff
	  // Start and stop dates for all events after harvest
	  flexdates[0][1] = g_date->DayInYear(15, 8); // last possible day of swathing in this case 
	  // Now these are done in pairs, start & end for each operation. If its not used then -1
	  flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
	  flexdates[1][1] = g_date->DayInYear(31, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - harvest
	  flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
	  flexdates[2][1] = g_date->DayInYear(31, 8); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - straw chopping
	  flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
	  flexdates[3][1] = g_date->DayInYear(1, 9); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) - burn straw

	  // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
	  int isSpring = 0;
	  if (StartUpCrop(isSpring, flexdates, int(dk_osgrs_spring_plough))) break;
	  // OK, let's go.
	  // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
	  if (m_ev->m_forcespring) {
		  if ((DK_OSGRS_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 0)
		  {
			  d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
			  if (g_date->Date() >= d1) d1 += 365;
			  SimpleEvent(d1, dk_osgrs_ferti_s2, false);
		  }
		  else if ((DK_OSGRS_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 1)
		  {
			  d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
			  if (g_date->Date() >= d1) d1 += 365;
			  SimpleEvent(d1, dk_osgrs_ferti_s2, false);
		  }
		  else if ((DK_OSGRS_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 2)
		  {
			  d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
			  if (g_date->Date() >= d1) d1 += 365;
			  SimpleEvent(d1, dk_osgrs_spring_plough, false);
		  }
		  else if ((DK_OSGRS_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 3)
		  {
			  d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
			  if (g_date->Date() >= d1) d1 += 365;
			  SimpleEvent(d1, dk_osgrs_spring_plough, false);
		  }

		  DK_OSGRS_FORCESPRING = true;
		  break;
	  }
	  else

		  // End single block date checking code. Please see next line comment as well.
		  // Reinit d1 to first possible starting date
		  if ((DK_OSGRS_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 0)
		  {
			  d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
			  if (g_date->Date() >= d1) d1 += 365;
			  SimpleEvent(d1, dk_osgrs_ferti_s2, false);
		  }
		  else if ((DK_OSGRS_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 1)
		  {
			  d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
			  if (g_date->Date() >= d1) d1 += 365;
			  SimpleEvent(d1, dk_osgrs_ferti_s2_2y, false);
		  }
		  else if ((DK_OSGRS_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 2)
		  {
			  d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
			  SimpleEvent(d1, dk_osgrs_autumn_plough, false);
		  }
		  else if ((DK_OSGRS_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 3)
		  {
			  d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
			  SimpleEvent(d1, dk_osgrs_autumn_plough, false);
		  }
	  break;

  }
  break;
  // LKM: This is the first real farm operation
  case dk_osgrs_autumn_plough:
	  if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
	  {
		  if (a_ev->m_lock || a_farm->DoIt_prob(0.95)) { // % suggestion
			  if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, dk_osgrs_autumn_plough, true);
				  break;
			  }
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_osgrs_spring_plough, false);
	  break;

  case dk_osgrs_spring_plough:
	  if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_spring_plough, true);
		  break;
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_osgrs_sow, false);
	  break;

  case dk_osgrs_sow: // sow mix of seedgrass and cover crop OR just cover crop and then seedgrass later
	  if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_sow, true);
		  break;
	  }
		  SimpleEvent(g_date->Date() + 3, dk_osgrs_harrow1, false);
		  break;

  case dk_osgrs_harrow1: // if split sowing
	  if (a_ev->m_lock || a_farm->DoIt_prob(0.5)) {
		  if (!a_farm->ShallowHarrow(a_field, 0.0, g_date->DayInYear(19, 4) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_harrow1, true);
			  break;
		  }
		  SimpleEvent(g_date->Date() + 3, dk_osgrs_harrow2, false);
		  break;
	  } 
	  else SimpleEvent(g_date->Date() + 12, dk_osgrs_strigling1, false); // if sown together
		  break;

  case dk_osgrs_harrow2:
	  if (!a_farm->ShallowHarrow(a_field, 0.0, g_date->DayInYear(23, 4) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_harrow2, true);
		  break;
	  }
	  SimpleEvent(g_date->Date() + 1, dk_osgrs_sow_lo, false);
	  break;

  case dk_osgrs_sow_lo:
	  if (!a_farm->StriglingSow(a_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_sow_lo, true);
		  break;
	  }
	  SimpleEvent(g_date->Date() + 15, dk_osgrs_strigling2, false);
	  break;

  case dk_osgrs_strigling1:
	  if (!a_farm->Strigling(a_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_strigling1, true);
		  break;
	  }
	  SimpleEvent(g_date->Date() + 15, dk_osgrs_strigling2, false);
	  break;

  case dk_osgrs_strigling2:
	  if (!a_farm->Strigling(a_field, 0.0, g_date->DayInYear(16, 5) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_strigling2, true);
		  break;
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_osgrs_harvest_cc, false); // harvest cover crop
	  break;

  case dk_osgrs_harvest_cc:
	  if (!a_farm->Harvest(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_harvest_cc, true);
		  break;
	  }
	  SimpleEvent(g_date->Date(), dk_osgrs_hay_bailing1, false);
	  break;

  case dk_osgrs_hay_bailing1:
	  if (!a_farm->HayBailing(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_hay_bailing1, true);
		  break;
	  }
	  if (a_farm->IsStockFarmer()) {
		  SimpleEvent(g_date->Date(), dk_osgrs_ferti_s1, false);
		  break;
	  }
	  else SimpleEvent(g_date->Date(), dk_osgrs_ferti_p1, false);
	  break;

  case dk_osgrs_ferti_s1:
	  if (a_ev->m_lock || a_farm->DoIt_prob(0.1)) { //% suggestion - only done if weak grass lay-out
		  if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_s1, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), dk_osgrs_cutting1, false); // either cutting or grazing
	  break;

  case dk_osgrs_ferti_p1:
	  if (a_ev->m_lock || a_farm->DoIt_prob(0.1)) { //% suggestion - only done if weak grass lay-out
		  if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_p1, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), dk_osgrs_cutting1, false); // either cutting or grazing
	  break;

  case dk_osgrs_cutting1:
	  if (a_ev->m_lock || a_farm->DoIt_prob(0.5)) { //% suggestion 
		  if (!a_farm->CutToHay(a_field, 0.0, g_date->DayInYear(20, 10) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_cutting1, true);
			  break;
		  }
		  done = true;
		  break;
	  }
	  else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), dk_osgrs_cattle_out1, false);
	  break;

  case dk_osgrs_cattle_out1:
	  if (!m_farm->CattleOut(m_field, 0.0,
		  g_date->DayInYear(2, 10) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_cattle_out1, true);
		  break;
	  }
	  SimpleEvent(g_date->Date() + 1, dk_osgrs_cattle_is_out1, false);
	  break;

  case dk_osgrs_cattle_is_out1:    // Keep the cattle out there
							   // CattleIsOut() returns false if it is not time to stop grazing
	  if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear(), g_date->DayInYear(31, 10))) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_cattle_is_out1, false);
		  break;
	  }
	done = true;
    break;

    //first year of seedgrass (after establishment)

  case dk_osgrs_ferti_s2:
	  if (a_farm->IsStockFarmer()) {
		  if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) { //% suggestion 
			  if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_s2, true);
				  break;
			  }
		  }
		  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_osgrs_ferti_s3, false); 
		  break;
	  }
	  SimpleEvent(g_date->Date(), dk_osgrs_ferti_p2, false);
	  break;

  case dk_osgrs_ferti_p2:
	  if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) { //% suggestion 
		  if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_p2, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_osgrs_ferti_p3, false); 
	  break;

  case dk_osgrs_ferti_s3:
	  if (a_ev->m_lock || a_farm->DoIt_prob(.10)) { //% suggestion - only if field is lacking fertilizer
		  if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(29, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_s3, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_osgrs_weeding, false); 
	  break;

  case dk_osgrs_ferti_p3:
	  if (a_ev->m_lock || a_farm->DoIt_prob(.10)) { //% suggestion - only if field is lacking fertilizer
		  if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(29, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_p3, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_osgrs_weeding, false);
	  break;

  case dk_osgrs_weeding:
	  if (a_ev->m_lock || a_farm->DoIt_prob(.30)) { //% suggestion - only if needed
		  if (!a_farm->RowCultivation(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_weeding, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_osgrs_swathing, false);
	  break;

  case dk_osgrs_swathing:
	  if (a_ev->m_lock || a_farm->DoIt_prob(1.00)) { //% suggestion - only if needed
		  if (!a_farm->Swathing(a_field, 0.0, g_date->DayInYear(21, 8) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_swathing, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 8, dk_osgrs_harvest, false);
	  break;

  case dk_osgrs_harvest:
	  if (a_ev->m_lock || a_farm->DoIt_prob(1.00)) { 
		  if (!a_farm->Harvest(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_harvest, true);
			  break;
		  }
	  }
	  // refer back to establishment year if first year after establishment - if 2nd year cut: 
		  SimpleEvent(g_date->Date(), dk_osgrs_hay_bailing1, false);
		  break;

		  //second year of seedgrass

  case dk_osgrs_ferti_s2_2y:
	  if (a_farm->IsStockFarmer()) {
		  if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) { //% suggestion 
			  if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_s2_2y, true);
				  break;
			  }
		  }
		  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_osgrs_ferti_s3_2y, false);
		  break;
	  }
	  SimpleEvent(g_date->Date(), dk_osgrs_ferti_p2_2y, false);
	  break;

  case dk_osgrs_ferti_p2_2y:
	  if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) { //% suggestion 
		  if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_p2_2y, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_osgrs_ferti_p3_2y, false);
	  break;

  case dk_osgrs_ferti_s3_2y:
	  if (a_ev->m_lock || a_farm->DoIt_prob(.10)) { //% suggestion - only if field is lacking fertilizer
		  if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(29, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_s3_2y, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_osgrs_weeding_2y, false);
	  break;

  case dk_osgrs_ferti_p3_2y:
	  if (a_ev->m_lock || a_farm->DoIt_prob(.10)) { //% suggestion - only if field is lacking fertilizer
		  if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(29, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_ferti_p3_2y, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_osgrs_weeding_2y, false);
	  break;

  case dk_osgrs_weeding_2y:
	  if (a_ev->m_lock || a_farm->DoIt_prob(.30)) { //% suggestion - only if needed
		  if (!a_farm->RowCultivation(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_weeding_2y, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_osgrs_swathing_2y, false);
	  break;

  case dk_osgrs_swathing_2y:
	  if (a_ev->m_lock || a_farm->DoIt_prob(1.00)) { //% suggestion - only if needed
		  if (!a_farm->Swathing(a_field, 0.0, g_date->DayInYear(21, 8) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_swathing_2y, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 8, dk_osgrs_harvest_2y, false);
	  break;

  case dk_osgrs_harvest_2y:
	  if (a_ev->m_lock || a_farm->DoIt_prob(1.00)) {
		  if (!a_farm->Harvest(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_osgrs_harvest_2y, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date(), dk_osgrs_hay_bailing2_2y, false);
	  break;

  case dk_osgrs_hay_bailing2_2y:
	  if (!a_farm->HayBailing(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
		  SimpleEvent(g_date->Date() + 1, dk_osgrs_hay_bailing2_2y, true);
		  break;
	  }
	  done = true;
	  break;

  default:
    g_msg->Warn( WARN_BUG, "DK_OSeedGrassRye_Spring::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
  return done;
}


