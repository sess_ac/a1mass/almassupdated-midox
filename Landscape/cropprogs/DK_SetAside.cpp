/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_SetAside.cpp This file contains the source for the DK_SetAside class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of April 2022 \n
 \n
*/
//
// DK_SetAside.cpp
//
/*

Copyright (c) 2022, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_SetAside.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_SetAside_SkScrapes("DK_CROP_SA_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_SetAside_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_SA_InsecticideDay;
extern CfgInt   cfg_SA_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DK_SetAside::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKSetAside;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_sa_start:
	{
		DK_SA_EVERY_2ND_YEAR = 0;
		DK_SA_FORCESPRING = false;
		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(30, 6); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 6); // last possible day of strigling in this case 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(15, 6); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - herbicide
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(30, 6); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - swathing

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(dk_sa_harrow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
			// OK, let's go.
			if ((DK_SA_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 2)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
				if (g_date->Date() >= d1) {
					d1 = g_date->Date();
				}
				SimpleEvent(d1, dk_sa_harrow, false);
			}
			else if ((DK_SA_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 1)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
				if (g_date->Date() >= d1) {
					d1 = g_date->Date();
				}
				SimpleEvent(d1, dk_sa_harrow, false);
			}
			else if ((DK_SA_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 0)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 4) + 365;
				if (g_date->Date() >= d1) {
					d1 = g_date->Date();
				}
				SimpleEvent(d1, dk_sa_cutting, false);
			}
	}
	break;

		
	// LKM: This is the first real farm operation - soil cultivation only needed if establishment of flower og pollinator setaside (then cutting is not done in that year)- suggests 50%, otherwise straight to the cutting
	
	case dk_sa_harrow:
		if (a_ev->m_lock || a_farm->DoIt_prob(.50)) {
			if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sa_harrow, true);
				break;

			}
			SimpleEvent(g_date->Date(), dk_sa_roll, false);
			break;
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.50 / .50)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_sa_cutting, false);
			break;
		}
		
	case dk_sa_roll:
		if (!a_farm->SpringRoll(a_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sa_roll, true);
			break;
		}
			SimpleEvent(g_date->Date(), dk_sa_sow, false);
			break;

	case dk_sa_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sa_sow, true);
			break;
		}
		SimpleEvent(g_date->Date()+30, dk_sa_strigling, false);
		break;

	case dk_sa_cutting: // only allowed between 1-30th of April (when spring cutting)
		if (!a_farm->CutToHay(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sa_cutting, true);
			break;
		}
		SimpleEvent(g_date->Date()+1, dk_sa_strigling, false);
		break;

	case dk_sa_strigling:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->Strigling(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sa_strigling, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_sa_herbicide, false);
		break;

	case dk_sa_herbicide: // only allowed if risk of cross pollination - suggests 10%
		if (a_ev->m_lock || a_farm->DoIt(10)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sa_herbicide, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+14, dk_sa_swathing, false);
		break;

	case dk_sa_swathing: 
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->Swathing(a_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sa_swathing, true);
				break;
			}
		}
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_sa_wait3, false);
			// Because we are ending harvest before 1.7 so we need to wait until the 1.7
			break;
		}
		else {
			done = true; // end of plan
		}
	case dk_sa_wait3:
		// End of management
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "DK_SetAside::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}