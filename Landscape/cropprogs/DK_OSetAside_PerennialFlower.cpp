/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OSetAside_PerennialFlower.cpp This file contains the source for the DK_OSetAside_PerennialFlower class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of May 2022 \n
 \n
*/
//
// DK_OSetAside_PerennialFlower.cpp
//
/*

Copyright (c) 2022, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OSetAside_PerennialFlower.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..

CfgBool cfg_DK_OSetAside_PerennialFlower_SkScrapes("DK_CROP_OSAPF_SK_SCRAPES", CFG_CUSTOM, false);


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_OSetAside_PerennialFlower::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKOSetAside_PerennialFlower;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_osapf_start:
	{
		DK_OSAPF_EVERY_2ND_YEAR = 0;

		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(31, 7); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 7); // last possible day of strigling in this case 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(31, 7); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - swathing

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(dk_osapf_harrow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.

		if ((DK_OSAPF_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 2)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			if (g_date->Date() >= d1) {
				d1 = g_date->Date();
			}
			SimpleEvent(d1, dk_osapf_harrow, false);
		}
		else if ((DK_OSAPF_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			if (g_date->Date() >= d1) {
				d1 = g_date->Date();
			}
			SimpleEvent(d1, dk_osapf_harrow, false);
		}
		else if ((DK_OSAPF_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7) + 365;
			if (g_date->Date() >= d1) {
				d1 = g_date->Date();
			}
			SimpleEvent(d1, dk_osapf_wait1, false);
		}
		break;

	}
	break;
	// LKM: This is the first real farm operation - soil cultivation 
	
	case dk_osapf_harrow:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
			if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osapf_harrow, true);
				break;

			}
		}
			SimpleEvent(g_date->Date(), dk_osapf_roll, false);
			break;
		
	case dk_osapf_roll:
		if (!a_farm->SpringRoll(a_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_osapf_roll, true);
			break;
		}
			SimpleEvent(g_date->Date(), dk_osapf_sow, false);
			break;

	case dk_osapf_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_osapf_sow, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_osapf_wait1, false);
		break;

	case dk_osapf_wait1: //no requirements on cutting the field the following year
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->SleepAllDay(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osapf_wait1, true);
				break;
			}
		}
		// End of management
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "DK_OSetAside_PerennialFlower::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}