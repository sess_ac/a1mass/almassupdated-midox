/**
\file
\brief
<B>DK_OCabbages.h This file contains the source for the DK_OCabbages class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of March 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OCabbages.h
//


#ifndef DK_OCABBAGES_H
#define DK_OCABBAGES_H

#define DK_OCA_BASE 60400

/**
\brief A flag used to indicate autumn ploughing status
*/
#define DK_OCAB_WINTER_PLOUGH	m_field->m_user[1]
#define DK_OCAB_FORCESPRING	a_field->m_user[2]
/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_oca_start = 1, // Compulsory, must always be 1 (one).
	dk_oca_harvest = DK_OCA_BASE,
	dk_oca_autumn_plough_clay,
	dk_oca_molluscicide1,
	dk_oca_spring_harrow_sandy,
	dk_oca_sharrow1,
	dk_oca_sharrow2,
	dk_oca_sharrow3,
	dk_oca_sharrow4,
	dk_oca_plant,
	dk_oca_water,
	dk_oca_molluscicide2,
	dk_oca_strigling1,
	dk_oca_strigling2,
	dk_oca_strigling3,
	dk_oca_row_cultivation_clay,
	dk_oca_covering1,
	dk_oca_remove_cover1,
	dk_oca_manual_weeding,
	dk_oca_covering2,
	dk_oca_remove_cover2,
	dk_oca_wait,
	dk_oca_foobar,
} DK_OCabbagesToDo;


/**
\brief
DK_OCabbages class
\n
*/
/**
See DK_OCabbages.h::DK_OCabbagesToDo for a complete list of all possible events triggered codes by the cabbage management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OCabbages: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OCabbages(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st of September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,12 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_oca_foobar - DK_OCA_BASE);
	   m_base_elements_no = DK_OCA_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_oca_start = 1, // Compulsory, must always be 1 (one).
			fmc_Harvest,	//	dk_oca_harvest = DK_OCA_BASE,
			fmc_Cultivation,	//	dk_oca_autumn_plough_clay,
			fmc_Others,	//	dk_oca_molluscicide1,
			fmc_Cultivation,	//	dk_oca_spring_harrow_sandy,
			fmc_Cultivation,	//	dk_oca_sharrow1,
			fmc_Cultivation,	//	dk_oca_sharrow2,
			fmc_Cultivation,	//	dk_oca_sharrow3,
			fmc_Cultivation,	//	dk_oca_sharrow4,
			fmc_Others,	//	dk_oca_plant,
			fmc_Watering,	//	dk_oca_water,
			fmc_Others,	//	dk_oca_molluscicide2,
			fmc_Cultivation,	//	dk_oca_strigling1,
			fmc_Cultivation,	//	dk_oca_strigling2,
			fmc_Cultivation,	//	dk_oca_strigling3,
			fmc_Cultivation,	//	dk_oca_row_cultivation_clay,
			fmc_Others,	//	dk_oca_covering1,
			fmc_Others,	//	dk_oca_remove_cover1,
			fmc_Cultivation,	//	dk_oca_manual_weeding,
			fmc_Others,	//	dk_oca_covering2,
			fmc_Others,	//	dk_oca_remove_cover2,
			fmc_Others	//	dk_oca_wait,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OCABBAGES_H

