/**
\file
\brief
<B>PLWinterWheat.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLWinterWheat.h
//


#ifndef PLWINTERWHEAT_H
#define PLWINTERWHEAT_H

#define PLWINTERWHEAT_BASE 26600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_WW_FERTI_P1	a_field->m_user[1]
#define PL_WW_FERTI_S1	a_field->m_user[2]
#define PL_WW_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_WW_DECIDE_TO_GR a_field->m_user[4]

/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_ww_start = 1, // Compulsory, must always be 1 (one).
	pl_ww_sleep_all_day = PLWINTERWHEAT_BASE,
	pl_ww_ferti_p1,
	pl_ww_ferti_s1,
	pl_ww_stubble_plough,
	pl_ww_autumn_harrow1,
	pl_ww_autumn_harrow2,
	pl_ww_stubble_harrow,
	pl_ww_ferti_p2,
	pl_ww_ferti_s2,
	pl_ww_autumn_plough,
	pl_ww_autumn_roll,
	pl_ww_stubble_cultivator_heavy,
	pl_ww_ferti_p3,
	pl_ww_ferti_s3,
	pl_ww_preseeding_cultivator,
	pl_ww_preseeding_cultivator_sow,
	pl_ww_autumn_sow,
	pl_ww_herbicide1,
	pl_ww_fungicide1,
	pl_ww_insecticide1,
	pl_ww_ferti_p4,
	pl_ww_ferti_s4,
	pl_ww_ferti_p5,
	pl_ww_ferti_s5,
	pl_ww_ferti_p6,
	pl_ww_ferti_s6,
	pl_ww_ferti_p7,
	pl_ww_ferti_s7,
	pl_ww_ferti_p8,
	pl_ww_ferti_s8,
	pl_ww_ferti_p9,
	pl_ww_ferti_s9,
	pl_ww_herbicide2, 
	pl_ww_fungicide2,
	pl_ww_fungicide3,
	pl_ww_fungicide4,
	pl_ww_insecticide2,
	pl_ww_insecticide3,
	pl_ww_growth_regulator1,
	pl_ww_growth_regulator2,
	pl_ww_harvest,
	pl_ww_straw_chopping,
	pl_ww_hay_bailing,
	pl_ww_ferti_p10,
	pl_ww_ferti_s10,
	pl_ww_ferti_p11,
	pl_ww_ferti_s11,
	pl_ww_foobar       // this should always be the last in the list of operations
} PLWinterWheat1ToDo;


/**
\brief
PLWinterWheat class
\n
*/
/**
See PLWinterWheat.h::PLWinterWheatToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLWinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLWinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 20,9 );
		// Create the farm operation to farm management category translation
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_ww_foobar - PLWINTERWHEAT_BASE);
	   m_base_elements_no = PLWINTERWHEAT_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others, // this needs to be at the zero line
			fmc_Others, // pl_ww_start = 1, 
			fmc_Others, // pl_ww_sleep_all_day = PLWINTERWHEAT_BASE,
			fmc_Fertilizer, // pl_ww_ferti_p1,
			fmc_Fertilizer, // pl_ww_ferti_s1,
			fmc_Cultivation, // pl_ww_stubble_plough,
			fmc_Cultivation, // pl_ww_autumn_harrow1,
			fmc_Cultivation, // pl_ww_autumn_harrow2,
			fmc_Cultivation, // pl_ww_stubble_harrow,
			fmc_Fertilizer, //pl_ww_ferti_p2,
			fmc_Fertilizer, //pl_ww_ferti_s2,
			fmc_Cultivation, // pl_ww_autumn_plough,
			fmc_Others, // pl_ww_autumn_roll,
			fmc_Cultivation, // pl_ww_stubble_cultivator_heavy,
			fmc_Fertilizer, //pl_ww_ferti_p3,
			fmc_Fertilizer, // pl_ww_ferti_s3,
			fmc_Cultivation, // pl_ww_preseeding_cultivator,
			fmc_Cultivation, // pl_ww_preseeding_cultivator_sow,
			fmc_Others, // pl_ww_autumn_sow,
			fmc_Herbicide, // pl_ww_herbicide1,
			fmc_Fungicide, // pl_ww_fungicide1,
			fmc_Insecticide, // pl_ww_insecticide1,
			fmc_Fertilizer, //pl_ww_ferti_p4,
			fmc_Fertilizer, //pl_ww_ferti_s4,
			fmc_Fertilizer, //pl_ww_ferti_p5,
			fmc_Fertilizer, //pl_ww_ferti_s5,
			fmc_Fertilizer, //pl_ww_ferti_p6,
			fmc_Fertilizer, //pl_ww_ferti_s6,
			fmc_Fertilizer, //pl_ww_ferti_p7,
			fmc_Fertilizer, //pl_ww_ferti_s7,
			fmc_Fertilizer, //pl_ww_ferti_p8,
			fmc_Fertilizer, //pl_ww_ferti_s8,
			fmc_Fertilizer, //pl_ww_ferti_p9,
			fmc_Fertilizer, //pl_ww_ferti_s9,
			fmc_Herbicide, // pl_ww_herbicide2,
			fmc_Fungicide, // pl_ww_fungicide2,
			fmc_Fungicide, // pl_ww_fungicide3,
			fmc_Fungicide, // pl_ww_fungicide4,
			fmc_Insecticide, // pl_ww_insecticide2,
			fmc_Insecticide, // pl_ww_insecticide3,
			fmc_Others, // pl_ww_growth_regulator1,
			fmc_Others, // pl_ww_growth_regulator2,
			fmc_Harvest, //pl_ww_harvest,
			fmc_Others, // pl_ww_straw_chopping,
			fmc_Others, // pl_ww_hay_bailing,
			fmc_Fertilizer, // pl_ww_ferti_p10,
			fmc_Fertilizer, // pl_ww_ferti_s10,
			fmc_Fertilizer, // pl_ww_ferti_p11,
			fmc_Fertilizer // pl_ww_ferti_s11,
	   };
	   // Iterate over the catlist elements and copy them to vector
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};

#endif // PLWINTERWHEAT_H

