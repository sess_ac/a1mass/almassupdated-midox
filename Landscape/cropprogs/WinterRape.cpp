//
// WinterRape.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

//#define __EcoSol_02

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WinterRape.h"

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgInt cfg_pest_productapplic_startdate;
extern CfgInt cfg_pest_productapplic_period;
extern CfgBool cfg_pest_winterrape_on;
extern CfgInt 	cfg_OSR_InsecticideDay;
extern CfgInt   cfg_OSR_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;
extern CfgBool cfg_seedcoating_winterrape_on;
extern CfgFloat cfg_seed_coating_product_1_amount;
extern CfgFloat cfg_seed_coating_product_2_amount;


bool WinterRape::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  double ins_app_prop1 = cfg_ins_app_prop1.value();
  double ins_app_prop2 = cfg_ins_app_prop2.value();
  double ins_app_prop3 = cfg_ins_app_prop3.value();
  double herbi_app_prop = cfg_herbi_app_prop.value();
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int noDates=3;
  int d1=0;
  // DEBUG double bio=0;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case wr_start:
    {
      WR_DID_RC_CLEAN=false;
      WR_DID_HERBI_ZERO=false;
      WR_INSECT_DATE=0;
      WR_SWARTH_DATE=-1;
      WR_HARVEST_DATE=-1;
      a_field->ClearManagementActionSum();

      // Start single block date checking code to be cut-'n-pasted...
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(15,10);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(1,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(1,8));
      m_field->SetMDates(0,1,g_date->DayInYear(25,7));
      m_field->SetMDates(1,1,g_date->DayInYear(15,8));
      m_field->SetMDates(0,2,g_date->DayInYear(1,8));
      m_field->SetMDates(1,2,g_date->DayInYear(15,10));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "WinterRape::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
        if(m_field->GetMDates(0,i)>=m_ev->m_startday) {
          m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
        }
        if(m_field->GetMDates(1,i)>=m_ev->m_startday){
			m_field->SetMConstants(i,0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
			m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
        }
      }
    }
      // Now no operations can be timed after the start of the next crop.
      // Start single block date checking code to be cut-'n-pasted...
    if ( ! m_ev->m_first_year ) {
		// Are we before July 1st?
		d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
		if (g_date->Date() < d1) {
		  // Yes, too early. We assumme this is because the last crop was late
			  g_msg->Warn( WARN_BUG, "WinterRape::Do(): "
			 "Crop start attempt between 1st Jan & 1st July", "" );
			  exit( 1 );
		}
		else
		{
			d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
			if (g_date->Date() > d1) {
				// Yes too late - should not happen - raise an error
				g_msg->Warn( WARN_BUG, "WinterRape::Do(): "
				"Crop start attempt after last possible start date", "" );
				exit( 1 );
			}
		}
      }
      else
      {
        // Is the first year so must start in spring like nothing was unusual
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,7 )
                                                          ,wr_swarth, false );
        break;
      }
	}//if
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 21,8 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      SimpleEvent( d1, wr_ferti_zero, false );

    }

	#ifdef __EcoSol_02
		// THIS CODE IS ONLY NEEDED IF WE ARE TESTING A PRODUCT WITH THE PESTICIDE ENGINE & WR
		/* */
		if ( a_field->GetLandscape()->SupplyShouldSpray() ) {
			d1 = g_date->OldDays() + cfg_pest_productapplic_startdate.value();
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent( d1, wr_productapplic_one, false );
		}
		/* */
	#endif

   break;

  case wr_productapplic_one:
	if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->ProductApplication( m_field, 0.0, (cfg_pest_productapplic_startdate.value()+cfg_pest_productapplic_period.value()) - g_date->DayInYear(),cfg_pest_product_1_amount.value(), ppp_1)) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, wr_productapplic_one, true );
        break;
      }
    }
    break;


  // NB this is only stock farmers
  case wr_ferti_zero:
    if (( m_ev->m_lock || m_farm->DoIt( 10 ))&& (m_farm->IsStockFarmer()))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 24, 8 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, wr_ferti_zero, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 8 ),
                 wr_autumn_plough, false );
    break;
  case wr_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 95 ))
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wr_autumn_plough, true );
				break;
      }
    }
    SimpleEvent( g_date->Date() + 1, wr_autumn_harrow, false );
    break;
  case wr_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
         g_date->DayInYear( 25, 8 ) - g_date->DayInYear()))
    {
      SimpleEvent( g_date->Date() + 1, wr_autumn_harrow, false );
      break;
    }
    {
	    long newdate1 = g_date->OldDays() + g_date->DayInYear( 10, 8 );
  	  long newdate2 = g_date->Date();
  	  if ( newdate2 > newdate1 ) newdate1 = newdate2;
  	  SimpleEvent( newdate1, wr_autumn_sow, false );
    }
    break;
  case wr_autumn_sow:
    if (cfg_seedcoating_winterrape_on.value() && a_field->GetLandscape()->SupplyShouldSpray()) {
      if (!m_farm->AutumnSow(m_field, 0.0,
        g_date->DayInYear(25, 8) - g_date->DayInYear(),
        cfg_seed_coating_product_1_amount.value(), ppp_1)) {
        SimpleEvent(g_date->Date() + 1, wr_autumn_sow, false);
        break;
      }
    }
    else {
      if (!m_farm->AutumnSow(m_field, 0.0,
        g_date->DayInYear(25, 8) - g_date->DayInYear())) {
        SimpleEvent(g_date->Date() + 1, wr_autumn_sow, false);
        break;
        }
    }
	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 9 ),
                 wr_rowcol_clean, false );
    break;
  case wr_rowcol_clean:
    if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
      if (!m_farm->RowCultivation( m_field, 0.0,
           g_date->DayInYear( 30, 9 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, wr_rowcol_clean, true );
				break;
      }
      WR_DID_RC_CLEAN = true;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 9 ),
                 wr_herbi_zero, false );
    break;

  case wr_herbi_zero:
    HerbiZero();
    break;

  case wr_ferti_p1:
    if (!m_farm->FP_NPKS( m_field, 0.0,
           g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wr_ferti_p1, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 4 ),
                 wr_ferti_p2, false );
    break;

  case wr_ferti_p2:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
      if ( !m_farm->FP_NPKS( m_field, 0.0,
            g_date->DayInYear( 1, 5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wr_ferti_p2, true );
      }
    }
    // Execution in this thread stops here. No new events are queued up.
    break;
case wr_ferti_s1:
    if (!m_farm->FP_NPKS( m_field, 0.0,
           g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wr_ferti_s1, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
              wr_ferti_p2, false );
    break;

  case wr_ferti_s2:
    if ( !m_farm->FP_Slurry( m_field, 0.0,
          g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wr_ferti_s2, true );
    }
    // Execution in this thread stops here. No new events are queued up.
    break;

  case wr_herbi_one:
    if ( m_ev->m_lock || (m_farm->DoIt( (int)(45*herbi_app_prop * m_farm->Prob_multiplier()) ) && !WR_DID_HERBI_ZERO) ) { //modified probability
      if ( !m_farm->HerbicideTreat( m_field, 0.0,
            g_date->DayInYear( 5, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wr_herbi_one, true );
      }
    }
    // Execution in this thread stops here. No new events are queued up.
    break;
  case wr_insect_one:
	  if (m_ev->m_lock || m_farm->DoIt((int)(21 * ins_app_prop1 * m_farm->Prob_multiplier()))) //modified probability
	  {

		  // Here we check wheter we are using ERA pesticde or not
		  if (!cfg_pest_winterrape_on.value() || !a_field->GetLandscape()->SupplyShouldSpray())
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0,
				  g_date->DayInYear(cfg_OSR_InsecticideDay.value(), cfg_OSR_InsecticideMonth.value()) - g_date->DayInYear()))
			  {
				  SimpleEvent(g_date->Date() + 1, wr_insect_one, true);
				  break;
			  }
			  else
			  {
				  WR_INSECT_DATE = g_date->Date();
				  long newdate1 = g_date->OldDays() + g_date->DayInYear(1, 5);
				  long newdate2 = g_date->Date() + 7;
				  if (newdate2 > newdate1)
				  {
					  newdate1 = newdate2;
				  }
				  SimpleEvent(newdate1, wr_insect_one_b, false);
				  break;
			  }
		  }
		  else 
		  {
			  m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			  WR_INSECT_DATE = g_date->Date();
			  SimpleEvent(g_date->Date()+14, wr_insect_one_b, false);
			  break;
		  }

	  }
    break;    // End of thread if gets here

  case wr_insect_one_b:
    if ( g_date->Date() == WR_FUNGI_DATE )
    {
      // try again tomorrow.
  	  SimpleEvent( g_date->Date() + 1, wr_insect_one_b, false );
      break;
    }
	if (m_ev->m_lock || m_farm->DoIt((int)(30 * ins_app_prop2 * m_farm->Prob_multiplier()))) { //modified probability
		// Here we check wheter we are using ERA pesticde or not
		if (!cfg_pest_winterrape_on.value() || !a_field->GetLandscape()->SupplyShouldSpray())
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0,
				g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, wr_insect_one_b, true);
				break;
			}
			else
			{
				WR_INSECT_DATE = g_date->Date();
				long newdate1 = g_date->OldDays() + g_date->DayInYear(20, 6);
				long newdate2 = g_date->Date() + 7;
				if (newdate2 > newdate1) newdate1 = newdate2;
				SimpleEvent(newdate1, wr_insect_one_c, false);
				break;
			}
		}
		else
		{
			m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			WR_INSECT_DATE = g_date->Date();
			SimpleEvent(g_date->Date() + 14, wr_insect_one_c, false);
			break;
		}
	}
    break;    // End of thread if gets to here

  case wr_insect_one_c:
	  if (m_ev->m_lock || m_farm->DoIt((int)(12 * ins_app_prop3  * m_farm->Prob_multiplier()))) //modified probability
	  {
		  // Here we check wheter we are using ERA pesticde or not
		  if (!cfg_pest_winterrape_on.value() || !a_field->GetLandscape()->SupplyShouldSpray())
		  {
			  if (g_date->Date() == WR_FUNGI_DATE)
			  {
				  // try again tomorrow.
				  SimpleEvent(g_date->Date() + 1, wr_insect_one_c, false);
				  break;
			  }
			  if (!m_farm->InsecticideTreat(m_field, 0.0,
				  g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, wr_insect_one_c, true);
			  }
			  WR_INSECT_DATE = g_date->Date();
		  }
		  else
		  {
			  m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			  WR_INSECT_DATE = g_date->Date();
			  break;
		  }
	  }
	// END_OF_ThREAD
    break;

  case wr_fungi_one:
   if (m_farm->DoIt((int)(34*cfg_fungi_app_prop1.value() * m_farm->Prob_multiplier()) )) //modified probability
   {
    if ( g_date->Date() == WR_INSECT_DATE )
    {
      // try again tomorrow.
  	  SimpleEvent( g_date->Date() + 1, wr_fungi_one, false );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {

      if ( !m_farm->FungicideTreat( m_field, 0.0,
            g_date->DayInYear( 1, 6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wr_insect_one_c, true );
      }
      WR_FUNGI_DATE = g_date->Date();
    }
		// END_OF_ThREAD
   }
   break;

  case wr_rowcol_one:
    RowcolOne();
    break;

  case wr_rowcol_one_b:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if ( !m_farm->RowCultivation( m_field, 0.0,
            g_date->DayInYear( 1, 5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wr_rowcol_one_b, true );
      }
    }
	  // END_OF_ThREAD
    break;

  case wr_swarth:
    if (m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if ( !m_farm->Swathing( m_field, 0.0,
            g_date->DayInYear( 25, 7 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wr_swarth, true );
        break;
      }
      else WR_SWARTH_DATE=g_date->DayInYear();
    }
	ChooseNextCrop (3);
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
                 wr_harvest, false );
    break;

  case wr_harvest:
    {
	if (m_field->GetMConstants(1)==0) {
		if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "WinterRape::Do(): failure in 'Harvest' execution", "" );
			exit( 1 );
		}
	}
	else {
      long EndDate;
      if (WR_SWARTH_DATE!=-1)
      {
        EndDate=WR_SWARTH_DATE+14;
        if (EndDate>m_field->GetMDates(0,1)) EndDate=m_field->GetMDates(0,1);
      }
      else EndDate=g_date->DayInYear( 1, 8 );
      if ( !m_farm->Harvest( m_field, 0.0,
          EndDate - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, wr_harvest, false );
        break;
      }
	}
      // Must have done harvest so record the fact for Debug
/* DEBUG
      WR_HARVEST_DATE=g_date->DayInYear();
      bio=m_field->GetVegBiomass();
      if (bio>1000) {
        int rubbish=0;
      }
*/
      if ( m_farm->DoIt( 95 ))  // was 95
      {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
                   wr_cuttostraw, false );
      }
      else  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 7 ),
                   wr_compress, false);
      break;
    }

  case wr_cuttostraw:
    if ( !m_farm->StrawChopping( m_field, 0.0,
          g_date->DayInYear( 1, 8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wr_cuttostraw, false );
      break;
    }
    if (g_date->DayInYear()<g_date->DayInYear(15,7))
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 7 ),
                 wr_stub_harrow, false );
    else SimpleEvent( g_date->Date() + 1,wr_stub_harrow, false );
    break;

  case wr_compress:
    if ( !m_farm->HayBailing( m_field, 0.0,
          g_date->DayInYear( 1, 8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wr_compress, false );
      break;
    }
    if (g_date->DayInYear()<g_date->DayInYear(15,7))
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 7 ),
                 wr_stub_harrow, false );
    else
      SimpleEvent( g_date->Date(), wr_stub_harrow, false );
    break;

  case wr_stub_harrow:
    if ( m_ev->m_lock || m_farm->DoIt( 75 )) {
		if (m_field->GetMConstants(1)==0) {
			if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterRape::Do(): failure in 'StubbleHarrowing' execution", "" );
				exit( 1 );
			}
		}
		else {
			if ( !m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, wr_stub_harrow, true );
				break;
			}
		}
	}
	  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 8 ), wr_grubbing, false );
    break;

  case wr_grubbing:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
		if (m_field->GetMConstants(2)==0) {
			if (!m_farm->DeepPlough( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterRape::Do(): failure in 'DeepPlough' execution", "" );
				exit( 1 );
			}
		}
		else {
		  if ( !m_farm->DeepPlough( m_field, 0.0, m_field->GetMDates(1,2) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wr_grubbing, true );
		  }
		}
    }
    //
    // End of execution tree for this management plan.
    //
/*
if (WR_HARVEST_DATE==-1) {
      int rubbish=0;
    }
    bio=m_field->GetVegBiomass();
    if (bio>1000) {
      int rubbish=0;
    }
*/
done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "WinterRape::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}


void WinterRape::RowcolOne( void )
{
  if ( g_date->Date() == WR_INSECT_DATE )
  {
    SimpleEvent( g_date->Date() + 1, wr_rowcol_one, m_ev->m_lock );
    return;
  }
  if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
    if ( !m_farm->RowCultivation( m_field, 0.0,
          g_date->DayInYear( 25, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wr_rowcol_one, true );
      return;
    }
    else
    {
      long newdate1 = g_date->OldDays() + g_date->DayInYear( 25, 4 );
      long newdate2 = g_date->Date() + 5;
      if ( newdate2 > newdate1 )
      newdate1 = newdate2;
      SimpleEvent( newdate1, wr_rowcol_one_b, false );
      return;
    }
  }
  return; // End of thread
}


void WinterRape::HerbiZero( void )
{
  double herbi_app_prop=cfg_herbi_app_prop.value();
  if ( m_ev->m_lock || (m_farm->DoIt((int) (65*herbi_app_prop  * m_farm->Prob_multiplier()) && !WR_DID_RC_CLEAN ))) //modified probability
  {

		WR_DID_HERBI_ZERO = true;
		if (!m_farm->HerbicideTreat( m_field, 0.0,
			 g_date->DayInYear( 5, 10 ) - g_date->DayInYear())) {
		  // We didn't do it today, try again tomorrow.
		  SimpleEvent( g_date->Date() + 1, wr_herbi_zero, true );
		  return;
		}
  }
  //
  // Fork off 6 (six!) threads of execution for *next* year...
  //
  if (m_farm->IsStockFarmer())
         SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,3 )+365,
                      wr_ferti_s1, false );
  else SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 )+365,
                      wr_ferti_p1, false );

  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 )+365,
                      wr_herbi_one, false );
  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 )+365,
                      wr_insect_one, false );
  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,5 )+365,
                      wr_fungi_one, false );
  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 )+365,
                      wr_rowcol_one, false );

  // This is the main thread, that will lead to the end (eventually)...
SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,7 )+365,
                      wr_swarth, false );
}

