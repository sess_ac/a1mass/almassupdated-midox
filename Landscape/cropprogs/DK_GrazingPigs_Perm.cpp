//
// DK_GrazingPigs_Perm.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_GrazingPigs_Perm.h"

bool DK_GrazingPigs_Perm::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;
  TTypesOfVegetation l_tov = tov_DKGrazingPigs_Perm;

  switch ( m_ev->m_todo )
  {
  case dk_gpp_start:
  {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(2,11);
      m_field->SetMDates(0,0,g_date->DayInYear(2,11));
      m_field->SetMDates(1,0,g_date->DayInYear(2,11));

      // This is a nasty one because it runs one year from when it starts, less one
      // month
      // So if the next crop is an autumn one - then it will push the year on one
      // We cannot allow this - the rotation will be out of sync. So only spring crops
      // can follow this
	  if ((m_ev->m_startday>g_date->DayInYear(1, 7)) && m_ev->m_startday<g_date->DayInYear(1, 11)) // This allows pigs to follow pigs
	  {
          g_msg->Warn( WARN_BUG, "GrazingPigs::Do(): "
                 "Autumn Crop Following Grazing Pigs is not allowed!!!", "" );
          exit( 1 );
      }
      if ( ! m_ev->m_first_year )
      {
        int today=g_date->Date();
        // Are we before July 1st?
        int d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
        if (today < d1)
        {
          // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "GrazingPigs::Do(): "
                "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
        }
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "GrazingPigs::Do(): "
               "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      // OK so if we reach here it is after 1/7 and before 1,11

	  m_field->SetLastSownVeg( m_field->GetVegType() ); //Force last sown, needed for goose habitat classification

      int d1=g_date->OldDays() + m_first_date;
      GPP_FIRST_PIGS_IN_DATE=d1+334; // 31 days less than one year
      GPP_LAST_PIGS_IN_DATE=d1+355;  //  just less than one year later (So not near the max before 1,11)
      SimpleEvent( d1, dk_gpp_pigs_out, false );
      break;
  }
  case dk_gpp_pigs_out:
  {
    if (!m_farm->PigsOut( m_field, 0.0,
         GPP_LAST_PIGS_IN_DATE - g_date->Date())) {
      SimpleEvent( g_date->Date() + 1, dk_gpp_pigs_out, true );
      break;
    }
    SimpleEvent( g_date->Date() + 1, dk_gpp_pigs_are_out_forced, false );
    break;

  case dk_gpp_pigs_are_out_forced:
    // Keep the pigs out there
    m_farm->PigsAreOutForced( m_field, 0.0, 0 );
    // PGP_FIRST_PIGS_IN_DATE specifies the first day the pigs may come home.
    // However this event type forces them to stay out. Thus we want to
    // abandon this chain of events one day before PGP_FIRST_PIGS_IN_DATE.
    if ( g_date->Date() < GPP_FIRST_PIGS_IN_DATE - 1 )
    {
      SimpleEvent( g_date->Date() + 1, dk_gpp_pigs_are_out_forced, false );
      break;
		}
    SimpleEvent( g_date->Date() + 1, dk_gpp_pigs_are_out, false );
  }
  break;

  case dk_gpp_pigs_are_out:
    // Start testing for taking the pigs home.
    // PigsAreOut() returns false if it is not time to stop grazing
    if (!m_farm->PigsAreOut( m_field, 0.0,
         GPP_LAST_PIGS_IN_DATE - g_date->Date())) {
    	// --FN--
      SimpleEvent( g_date->Date() + 1, dk_gpp_pigs_are_out, true );
      break;
    }
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "DK_GrazingPigs_Perm::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}



