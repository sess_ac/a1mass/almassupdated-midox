/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>UKBeet.cpp This file contains the source for the UKBeet class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
* Adapted for UK Beet, 2021, Adam McVeigh, Nottingham Trent University
*/
//
// UKBeet.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/UKBeet.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_beet_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_BE_InsecticideDay;
extern CfgInt   cfg_BE_InsecticideMonth;
extern CfgFloat cfg_pest_product_3_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional beet.
*/
bool UKBeet::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_UKBeet; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case uk_be_start:
	{
		// uk_be_start just sets up all the starting conditions and reference dates that are needed to start a uk_be
		UK_BE_HERBI1 = false;
		UK_BE_FUNGI1 = false;

		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(19, 9)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(19, 9));

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "UKBeet::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "UKBeet::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "UKBeet::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), uk_be_spring_plough, false, m_farm, m_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.

		// here we could split first operation by soil type. As of this version soil data for the UK has not been obtained.
		// With soil data some if/else need to be adapted. autumnharrow, winterplough and springplough.
		// Here we queue up the first event - this differs depending on whether we have a
		// stock or arable farmer
		SimpleEvent_(d1, uk_be_autumn_harrow, false, m_farm, m_field);
	}
	break;


	// This is the first real farm operation
	// autumn harrow 60% September
	case uk_be_autumn_harrow:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.6))
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(20, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_be_autumn_harrow, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), uk_be_winter_plough, false, m_farm, m_field);
		break;

		// winter plough 50% October - December
	case uk_be_winter_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(25, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_be_winter_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				if (m_farm->IsStockFarmer()) //Stock Farmer // setting the next step which is NPK fertiliser application
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, uk_be_ferti_s1, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, uk_be_ferti_p1, false, m_farm, m_field);
				break;
			}
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, uk_be_spring_plough, false, m_farm, m_field);
		break;

	// spring plough 50% March
	case uk_be_spring_plough:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_be_spring_plough, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer // setting the next step which is NPK fertiliser application
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), uk_be_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), uk_be_ferti_p1, false, m_farm, m_field);
		break;

	// NPK 60% March-April
	case uk_be_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(13, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_be_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_be_preseeding_cultivator, false, m_farm, m_field);
		break;
	case uk_be_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(13, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_be_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_be_preseeding_cultivator, false, m_farm, m_field);
		break;

	// Preseeding cultivation 100% March - April - then sow immediately after
	case uk_be_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(14, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_be_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_be_spring_sow, false, m_farm, m_field);
		break;

	// Sow 100% March -  April
	// Then set up threads for herbicide, fungicide [main thread], and fertilizers.
	// Insecticide is pre-treated to the seed. Need to include this if possible.
	case uk_be_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_be_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, uk_be_herbicide1, false, m_farm, m_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), uk_be_fungicide1, false, m_farm, m_field);	// Fungicide thread = MAIN THREAD
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), uk_be_ferti_s2, false, m_farm, m_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), uk_be_ferti_p2, false, m_farm, m_field);
		break;

		// N fertiliser applied in April-May
	case uk_be_ferti_p2:
		// Here comes fertilizers thread
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, uk_be_ferti_p2, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_be_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case uk_be_ferti_s2:
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, uk_be_ferti_s2, false, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_be_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;

	// Herbicide thread starts here.
	// There are five possible herbicide applications.
	case uk_be_herbicide1:
		// Herbicide 1 75% pre-em April
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 5, uk_be_herbicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, uk_be_herbicide2, false, m_farm, m_field);
		break;
	case uk_be_herbicide2:
		// Herbicide 2 100% May
		if (m_ev->m_lock || m_farm->DoIt_prob(1))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 7, uk_be_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, uk_be_herbicide3, false, m_farm, m_field);
		break;
	case uk_be_herbicide3:
		// Herbicide 3 100% June 
		if (m_ev->m_lock || m_farm->DoIt_prob(1))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 7, uk_be_herbicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, uk_be_herbicide4, false, m_farm, m_field);
		break;
	case uk_be_herbicide4:
		// Herbicide 4 80% July
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_be_herbicide4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, uk_be_herbicide5, false, m_farm, m_field);
		break;
	case uk_be_herbicide5:
		// Herbicide 5 50% August 
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_be_herbicide5, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
		// fungicide thread
		// Fungicide1 100% July
	case uk_be_fungicide1:
		// Here comes the fungicide thread
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_be_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 21, uk_be_fungicide2, false, m_farm, m_field);
		break;
		// Fungicide2 75% August
	case uk_be_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_be_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), uk_be_harvest, false, m_farm, m_field);
		break;
	case uk_be_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_be_harvest, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "UKBeet::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}