/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University, modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_OOrchard.cpp This file contains the source for the DE_OOrchard class</B> \n
*/
/**
\file
by Chris J. Topping and Elzbieta Ziolkowska \n
 modified by Luna Kondrup Marcussen \n
 Version of November 2022 \n
 All rights reserved. \n
 \n
*/
//
// DE_OOrchard.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OOrchard.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_orchard_on;
extern CfgFloat cfg_pest_product_1_amount;
extern Landscape* g_landscape_p;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_OOrchard::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEOOrchard; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_oorch_start:
	{
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(1,11); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(1, 11); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(1, 11);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_oorch_ferti_s1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on sandy (2) or clay (1) soils
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_oorch_ferti_s1, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(d1, de_oorch_ferti_p1, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case de_oorch_ferti_p1:
		if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p1, true, m_farm, m_field);
			break;
		}
		//SimpleEvent_(g_date->Date()+1, de_oorch_fungicide1, false, m_farm, m_field); *turn on when organic approved pesticides used*
		//break;

	case de_oorch_ferti_s1:
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide1, false, m_farm, m_field); *turn on when organic approved pesticides used*
		//break;

	//case de_oorch_fungicide1:
		//if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		//{
			//if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			//	SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide1, true, m_farm, m_field);
				//break;
			//}
		//}
		//d1 = g_date->Date() + 5;
		//if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			//d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		//}
		// fork of events:
		//SimpleEvent_(d1, de_oorch_fungicide2, false, m_farm, m_field); // fungi+ferti // main thread *turn on when organic approved pesticides used*
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), de_oorch_row_cultivation1, false, m_farm, m_field); //mechanical weeding thread 
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), de_oorch_manual_weeding1, false, m_farm, m_field); //manual weeding thread
		//SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_oorch_pheromone, false, m_farm, m_field); // pheromone thread *turn on when organic approved pesticides used*
		if (a_farm->IsStockFarmer()) //Stock Farmer // *ferti thread turned off if pesticides turned on*
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), de_oorch_ferti_s2, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), de_oorch_ferti_s3, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), de_oorch_ferti_s4, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), de_oorch_ferti_s5, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), de_oorch_ferti_s8, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), de_oorch_ferti_p2, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), de_oorch_ferti_p3, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), de_oorch_ferti_p4, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), de_oorch_ferti_p5, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), de_oorch_ferti_p8, false);
		break;
	
	case de_oorch_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date()+5, de_oorch_fungicide3, false, m_farm, m_field);
		break;

		//mechanical weeding thread
	case de_oorch_row_cultivation1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_row_cultivation1, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 5)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 5);
		}
		SimpleEvent_(d1, de_oorch_row_cultivation2, false, m_farm, m_field);
		break;

	case de_oorch_row_cultivation2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_row_cultivation2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
		}
		SimpleEvent_(d1, de_oorch_row_cultivation3, false, m_farm, m_field);
		break;

	case de_oorch_row_cultivation3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_row_cultivation3, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		}
		SimpleEvent_(d1, de_oorch_row_cultivation4, false, m_farm, m_field);
		break;

	case de_oorch_row_cultivation4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_row_cultivation4, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 8)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		}
		SimpleEvent_(d1, de_oorch_row_cultivation5, false, m_farm, m_field);
		break;

	case de_oorch_row_cultivation5:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_row_cultivation5, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		}
		SimpleEvent_(d1, de_oorch_row_cultivation6, false, m_farm, m_field);
		break;

	case de_oorch_row_cultivation6:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_row_cultivation6, true, m_farm, m_field);
				break;
			}
		}
		break; 

		//manual weeding thread
	case de_oorch_manual_weeding1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_manual_weeding1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_oorch_manual_weeding2, false, m_farm, m_field);
		break;

	case de_oorch_manual_weeding2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_manual_weeding2, true, m_farm, m_field);
				break;
			}
		}
		break;

	case de_oorch_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 5, de_oorch_fungicide4, false, m_farm, m_field);
		break;

	case de_oorch_fungicide4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 5, de_oorch_fungicide5, false, m_farm, m_field);
		break;

	case de_oorch_fungicide5:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide5, true, m_farm, m_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent(g_date->Date(), de_oorch_ferti_s2, false);
			break;
		}
		else SimpleEvent(g_date->Date(), de_oorch_ferti_p2, false);
		break;

	case de_oorch_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date()+5, de_oorch_fungicide6, false, m_farm, m_field); *turn on if pesticides on*
		break;

	case de_oorch_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 5, de_oorch_fungicide6, false, m_farm, m_field); *turn on if pesticides on*
		break;

	case de_oorch_fungicide6:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide6, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 5, de_oorch_insecticide1, false, m_farm, m_field);
		break;

	case de_oorch_insecticide1:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 5, de_oorch_fungicide7, false, m_farm, m_field);
		break;

	case de_oorch_fungicide7:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide7, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 5, de_oorch_insecticide2, false, m_farm, m_field);
		break;

	case de_oorch_insecticide2:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 5)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 5);
		}
		SimpleEvent_(d1, de_oorch_insecticide3, false, m_farm, m_field); //insecti thread
		SimpleEvent_(d1, de_oorch_fungicide8, false, m_farm, m_field); // main thread
		break;

		//pheromone thread

	case de_oorch_pheromone:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) { // need new farmfunc!
				SimpleEvent_(g_date->Date() + 1, de_oorch_pheromone, true, m_farm, m_field);
				break;
			}
		}
		break;

	case de_oorch_insecticide3:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date()+7, de_oorch_insecticide4, false, m_farm, m_field);
		break;

	case de_oorch_insecticide4:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(14, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date-> Date() + 7, de_oorch_insecticide5, false, m_farm, m_field);
		break;

	case de_oorch_insecticide5:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(21, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide5, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_oorch_insecticide6, false, m_farm, m_field);
		break;

	case de_oorch_insecticide6:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide6, true, m_farm, m_field);
				break;
			}
		}
		break;

	case de_oorch_fungicide8:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide8, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide9, false, m_farm, m_field);
		break;

	case de_oorch_fungicide9:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide9, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide10, false, m_farm, m_field);
		break;

	case de_oorch_fungicide10:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide10, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide11, false, m_farm, m_field);
		break;

	case de_oorch_fungicide11:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide11, true, m_farm, m_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent(g_date->Date(), de_oorch_ferti_s3, false);
			break;
		}
		else SimpleEvent(g_date->Date(), de_oorch_ferti_p3, false);
		break;

	case de_oorch_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
		}
		//SimpleEvent_(d1, de_oorch_fungicide12, false, m_farm, m_field); *turn on if pesticides on*
		break;

	case de_oorch_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
		}
		//SimpleEvent_(d1, de_oorch_fungicide12, false, m_farm, m_field); *turn on if pesticides on*
		break;

	case de_oorch_fungicide12:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide12, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_oorch_insecticide7, false, m_farm, m_field);
		break;

	case de_oorch_insecticide7:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide7, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date()+7, de_oorch_fungicide13, false, m_farm, m_field);
		break;

	case de_oorch_fungicide13:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide13, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_oorch_insecticide8, false, m_farm, m_field);
		break;

	case de_oorch_insecticide8:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(14, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide8, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide14, false, m_farm, m_field);
		break;

	case de_oorch_fungicide14:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide14, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide15, false, m_farm, m_field);
		break;

	case de_oorch_fungicide15:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide15, true, m_farm, m_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent(g_date->Date(), de_oorch_ferti_s4, false);
			break;
		}
		else SimpleEvent(g_date->Date(), de_oorch_ferti_p4, false);
		break;

	case de_oorch_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		}
		//SimpleEvent_(d1, de_oorch_fungicide16, false, m_farm, m_field); *turn on if pesticides on*
		break;

	case de_oorch_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		}
		//SimpleEvent_(d1, de_oorch_fungicide16, false, m_farm, m_field); *turn on if pesticides on*
		break;

	case de_oorch_fungicide16:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide16, true, m_farm, m_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent(g_date->Date(), de_oorch_ferti_s5, false);
			break;
		}
		else SimpleEvent(g_date->Date(), de_oorch_ferti_p5, false);
		break;

	case de_oorch_ferti_p5:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_Calcium(m_field, 0.0, g_date->DayInYear(7, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p5, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date()+7, de_oorch_fungicide17, false, m_farm, m_field); *turn on if pesticides on*
		//break;

	case de_oorch_ferti_s5:
		if (a_farm->IsStockFarmer()){ //Stock Farmer
			if (!m_farm->FA_Calcium(m_field, 0.0, g_date->DayInYear(7, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s5, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide17, false, m_farm, m_field); *turn on if pesticides on*
		//break;

	//case de_oorch_fungicide17:
		//if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		//{
			//if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 7) - g_date->DayInYear())) {
				//SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide17, true, m_farm, m_field);
				//break;
			//}
		//}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			//SimpleEvent(g_date->Date(), de_oorch_ferti_s6, false); *turn on if pesticides on*
			SimpleEvent(g_date->Date() + 7, de_oorch_ferti_s6, false); //turn off if pesticides on
			break;
		}
		else //SimpleEvent(g_date->Date(), de_oorch_ferti_p6, false); *turn on if pesticides on*
			SimpleEvent(g_date->Date() + 7, de_oorch_ferti_p6, false); //turn off if pesticides on
		break;

	case de_oorch_ferti_p6:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_Calcium(m_field, 0.0, g_date->DayInYear(14, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p6, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide18, false, m_farm, m_field); *turn on if pesticides on*
		//break;

	case de_oorch_ferti_s6:
		if (a_farm->IsStockFarmer()) {//Stock Farmer
			if (!m_farm->FA_Calcium(m_field, 0.0, g_date->DayInYear(14, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s6, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide18, false, m_farm, m_field); *turn on if pesticides on*
		//break;

	//case de_oorch_fungicide18:
		//if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		//{
			//if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
			//	SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide18, true, m_farm, m_field);
			//	break;
			//}
	//	}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			//SimpleEvent(g_date->Date(), de_oorch_ferti_s7, false);  *turn on if pesticides on*
			SimpleEvent(g_date->Date() + 7, de_oorch_ferti_s7, false); //turn off if pesticides on
			break;
		}
		else //SimpleEvent(g_date->Date(), de_oorch_ferti_p7, false);  *turn on if pesticides on*
			SimpleEvent(g_date->Date() + 7, de_oorch_ferti_p7, false); //turn off if pesticides on
		break;

	case de_oorch_ferti_p7:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_Calcium(m_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p7, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide19, false, m_farm, m_field); *turn on if pesticides on*
		break;

	case de_oorch_ferti_s7:
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			if (!m_farm->FA_Calcium(m_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s7, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide19, false, m_farm, m_field); *turn on if pesticides on*
		break;

	case de_oorch_fungicide19:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide19, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_oorch_insecticide9, false, m_farm, m_field);
		break;

	case de_oorch_insecticide9:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide9, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 8)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		}
		SimpleEvent_(d1, de_oorch_fungicide20, false, m_farm, m_field);
		break;

	case de_oorch_fungicide20:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide20, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_oorch_insecticide10, false, m_farm, m_field);
		break;

	case de_oorch_insecticide10:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide10, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide21, false, m_farm, m_field); // mian thread
		SimpleEvent_(g_date->Date() + 14, de_oorch_insecticide11, false, m_farm, m_field);
		break;

	case de_oorch_insecticide11:
		// here we have organic insecticides applied
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_insecticide11, true, m_farm, m_field);
				break;
			}
		}
		break; 

	case de_oorch_fungicide21:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide21, true, m_farm, m_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent(g_date->Date(), de_oorch_ferti_s8, false);
			break;
		}
		else SimpleEvent(g_date->Date(), de_oorch_ferti_p8, false);
		break;

	case de_oorch_ferti_p8:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_Calcium(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p8, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide22, false, m_farm, m_field); *turn on if pesticides on*
		//break;

	case de_oorch_ferti_s8:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FA_Calcium(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s8, true, m_farm, m_field);
				break;
			}
		}
		//SimpleEvent_(g_date->Date() + 7, de_oorch_fungicide22, false, m_farm, m_field); *turn on if pesticides on*
		//break;

	//case de_oorch_fungicide22:
		//if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
	//	{
	//		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
	//			SimpleEvent_(g_date->Date() + 1, de_oorch_fungicide22, true, m_farm, m_field);
	//			break;
	//		}
	//	}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			//SimpleEvent(g_date->Date(), de_oorch_ferti_s9, false); *turn on if pesticides on*
			SimpleEvent(g_date->Date() + 7, de_oorch_ferti_s9, false); //turn off if pesticides on
			break;
		}
		else //SimpleEvent(g_date->Date(), de_oorch_ferti_p9, false); *turn on if pesticides on*
			SimpleEvent(g_date->Date() + 7, de_oorch_ferti_p9, false); //turn off if pesticides on
		break;

	case de_oorch_ferti_p9:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_Calcium(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_p9, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 9), de_oorch_harvest, false, m_farm, m_field);
		break;

	case de_oorch_ferti_s9:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FA_Calcium(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_oorch_ferti_s9, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 9), de_oorch_harvest, false, m_farm, m_field);
		break;

	case de_oorch_harvest:
		if (!a_farm->FruitHarvest(m_field, 0.0, g_date->DayInYear(1, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_oorch_harvest, true);
			break;
		}
			done = true;
			break;
		
	default:
		g_msg->Warn(WARN_BUG, "DE_OORCHARD::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}
