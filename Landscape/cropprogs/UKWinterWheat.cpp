/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>UKWinterWheat.cpp This file contains the source for the UKWinterWheat class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
* * * Adapted for UK Winter Wheat, 2021, Adam McVeigh, Nottingham Trent University
*/
//
// UKWinterWheat.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/UKWinterWheat.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_winterwheat_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_WW_InsecticideDay;
extern CfgInt   cfg_WW_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool UKWinterWheat::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_UKWinterWheat; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case uk_ww_start:
	{
		// uk_ww_start just sets up all the starting conditions and reference dates that are needed to start a uk_ww

		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 5 start and stop dates for all 'movable' events for this crop
		int noDates = 2;
		m_field->SetMDates(0, 0, g_date->DayInYear(25, 8)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(30, 8)); // last possible day of straw chopping
		m_field->SetMDates(0, 1, 0); // start day of hay bailing (not used as it depend on previous treatment)
		m_field->SetMDates(1, 1, g_date->DayInYear(30, 8)); // end day of hay bailing
															// Can be up to 10 of these. If the shortening code is triggered
															// then these will be reduced in value to 0

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a winter crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from winter to winter!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "UKWinterWheat::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "UKWinterWheat::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "UKWinterWheat::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				if (m_farm->IsStockFarmer()) //Stock Farmer // slurry application
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), uk_ww_ferti_s1, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), uk_ww_ferti_p1, false, m_farm, m_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		SimpleEvent_(d1, uk_ww_stubble_cultivator1, false, m_farm, m_field);
	}
	break;
	// This is the first real farm operation
	// Stubble cultivate 70%
	case uk_ww_stubble_cultivator1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.7))
		{
			if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(1, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_stubble_cultivator1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_ww_herbicide1, false, m_farm, m_field);
		break;
		// Herbicide 1 50%
	case uk_ww_herbicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_herbicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_ww_autumn_plough, false, m_farm, m_field);
		break;
		// Autumn Plough 50% (other 50% do stubble cultivate 2)
	case uk_ww_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_autumn_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				SimpleEvent_(g_date->Date() + 10, uk_ww_autumn_sow, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 11), uk_ww_stubble_cultivator2, false, m_farm, m_field);
		break;
		// Stubble cultivator 2 (100% which reach this which is 50% of total, or those who did not plough)
	case uk_ww_stubble_cultivator2:
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(13, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_ww_stubble_cultivator2, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, uk_ww_autumn_sow, false, m_farm, m_field);
		break;
		// Autumn sow 100%
	case uk_ww_autumn_sow:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(20, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_ww_autumn_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to parallel events. Harrowing. Herbicide. Fungicide. Insecticide. Growth Regulator.
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 11), uk_ww_herbicide2, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, uk_ww_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, uk_ww_insecticide1, false, m_farm, m_field);	// Insecticide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, uk_ww_growth_regulator1, false, m_farm, m_field);	// GR thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread = Main thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, uk_ww_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, uk_ww_ferti_p1, false, m_farm, m_field);
		break;

		// Fertliser thread : MAIN THREAD
		// Slurry 70%
	case uk_ww_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3), uk_ww_ferti_p2, false, m_farm, m_field);
		break;
	case uk_ww_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3), uk_ww_ferti_s2, false, m_farm, m_field);
		break;
		// PK 80%
	case uk_ww_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(30, 3), uk_ww_ferti_p3, false, m_farm, m_field);
		break;
	case uk_ww_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(30, 3), uk_ww_ferti_s3, false, m_farm, m_field);
		break;
		// NI 100%
	case uk_ww_ferti_p3:
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_ww_ferti_p3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 8), uk_ww_harvest, false, m_farm, m_field);
		break;
	case uk_ww_ferti_s3:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_ww_ferti_s3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 8), uk_ww_harvest, false, m_farm, m_field);
		break;

		// Herbicide thread
		// Herbicide 2 75%
		// Herbicide thread
	case uk_ww_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, uk_ww_herbicide3, false, m_farm, m_field);
		break;
		// Herbicide 3 100%
	case uk_ww_herbicide3:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_ww_herbicide3, true, m_farm, m_field);
			break;
		}
		break;
		// Fungicide thread
		// fungicide 1 100%
	case uk_ww_fungicide1:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_ww_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, uk_ww_fungicide2, false, m_farm, m_field);
		break;
		// Fungicide 2 90%
	case uk_ww_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.9))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, uk_ww_fungicide3, false, m_farm, m_field);
		break;
		// Fungicide 3 70%
	case uk_ww_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.7))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		break;
		// Insecticide thread
		// Insecticide 1 100%
	case uk_ww_insecticide1:
		// Here comes the insecticide thread = MAIN THREAD
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_winterwheat_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_ww_insecticide1, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), uk_ww_insecticide2, false, m_farm, m_field);
		break;
		// Insecticide 2
	case uk_ww_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_winterwheat_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_ww_insecticide2, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		break;
		// Growth regulator thread
		// Growth regulator 1 100%
	case uk_ww_growth_regulator1:
		if (!m_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_ww_growth_regulator1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), uk_ww_growth_regulator2, false, m_farm, m_field);
		break;
		// GR II 80%
	case uk_ww_growth_regulator2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			if (!m_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_growth_regulator2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
		// Back to main thread
		// Harvest 100% - then - straw chopping 50% - or - hay bailing 50%
	case uk_ww_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_ww_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_ww_straw_chopping, false, m_farm, m_field);
		break;
		// Straw chopping 50% after harvest
	case uk_ww_straw_chopping:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (m_field->GetMConstants(0) == 0) {
				if (!m_farm->StrawChopping(m_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "UKWinterWheat::Do(): failure in 'StrawChopping' execution", "");
					exit(1);
				}
			}
			else {
				if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_ww_straw_chopping, true, m_farm, m_field);
					break;
				}
			}
			done = true;
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_ww_hay_bailing, false, m_farm, m_field);
		break;
		// Hay bailing 50% after harvest
	case uk_ww_hay_bailing:
		if (m_field->GetMConstants(1) == 0) {
			if (!m_farm->HayBailing(m_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "UKWinterWheat::Do(): failure in 'HayBailing' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_ww_hay_bailing, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "UKWinterWheat::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}