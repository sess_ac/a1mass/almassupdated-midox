//
// DE_OPotatoes.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#ifndef DE_OPOTATOESEAT_H
#define DE_OPOTATOESEAT_H

#define DE_OPOTATOESEAT_BASE 37500
#define DE_OPOT_MANURE_DATE   m_field->m_user[0]
#define DE_OPOT_SOW_DATE      m_field->m_user[1]
#define DE_OPOT_HILLING_THREE m_field->m_user[2]

typedef enum {
  de_opot_start = 1, // Compulsory, start event must always be 1 (one).
  de_opot_sleep_all_day = DE_OPOTATOESEAT_BASE,
  de_opot_fa_slurry,
  de_opot_fa_manure,
  de_opot_fp_slurry,
  de_opot_fp_manure,
  de_opot_spring_plough,
  de_opot_spring_harrow,
  de_opot_spring_sow,
  de_opot_flaming_one,
  de_opot_strigling_one,
  de_opot_hilling_one,
  de_opot_strigling_two,
  de_opot_hilling_two,
  de_opot_strigling_three,
  de_opot_hilling_three,
  de_opot_insecticide,
  de_opot_fungicide,
  de_opot_fertiFA_S,
  de_opot_fertiFP_S,
  de_opot_fertiFA_B,
  de_opot_fertiFP_B,
  de_opot_water_one,
  de_opot_water_two,
  de_opot_flaming_two,
  de_opot_harvest,
  de_opot_foobar,
} DE_OPotatoesEatToDo;



class DE_OPotatoes: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OPotatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(20,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_opot_foobar - DE_OPOTATOESEAT_BASE);
	  m_base_elements_no = DE_OPOTATOESEAT_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_opot_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others, //   de_opot_sleep_all_day = DE_OPOTATOESEAT_BASE,
			fmc_Fertilizer,	//	  de_opot_fa_slurry,
			fmc_Fertilizer,	//	  de_opot_fa_manure,
			fmc_Fertilizer,	//	  de_opot_fp_slurry,
			fmc_Fertilizer,	//	  de_opot_fp_manure,
			fmc_Cultivation,	//	  de_opot_spring_plough,
			fmc_Cultivation,	//	  de_opot_spring_harrow,
			fmc_Others,	//	  de_opot_spring_sow,
			fmc_Others,	//	  de_opot_flaming_one,
			fmc_Cultivation,	//	  de_opot_strigling_one,
			fmc_Cultivation,	//	  de_opot_hilling_one,
			fmc_Cultivation,	//	  de_opot_strigling_two,
			fmc_Cultivation,	//	  de_opot_hilling_two,
			fmc_Cultivation,	//	  de_opot_strigling_three,
			fmc_Cultivation,	//	  de_opot_hilling_three,
			fmc_Insecticide,	//	  de_opot_insecticide,
			fmc_Fungicide,	//	  de_opot_fungicide,
			fmc_Fertilizer,	//	  de_opot_fertiFA_S,
			fmc_Fertilizer,	//	  de_opot_fertiFP_S,
			fmc_Fertilizer,	//	  de_opot_fertiFA_B,
			fmc_Fertilizer,	//	  de_opot_fertiFP_B,
			fmc_Others,	//	  de_opot_water_one,
			fmc_Others,	//	  de_opot_water_two,
			fmc_Others,	//	  de_opot_flaming_two,
			fmc_Harvest,	//	  de_opot_harvest,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }

};

#endif // OPOTATOESEAT_H
