/**
\file
\brief
<B>IRSpringBarley.h This file contains the headers for the SpringBarley class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2022 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// IRSpringBarley.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef IRSPRINGBARLEY_H
#define IRSPRINGBARLEY_H

#define IR_SB_BASE 90100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define IR_SB_MT        a_field->m_user[1]
#define IR_SB_CP       a_field->m_user[2]


/** Below is the list of things that a farmer can do if he is growing the crop, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	ir_sb_start = 1, // Compulsory, must always be 1 (one).
	ir_sb_sleep_all_day = IR_SB_BASE,
	ir_sb_ferti_s1,
	ir_sb_ferti_p1,
	ir_sb_stubble_harrow_mt,
	ir_sb_stubble_harrow_cp,
	ir_sb_sow_cover_crop,
	ir_sb_stubble_cultivator,
	ir_sb_herbicide1_mt,
	ir_sb_herbicide1_cp,
	ir_sb_stubble_cultivator_mt,
	ir_sb_cultivation_sow_mt,
	ir_sb_spring_plough_cp,
	ir_sb_spring_sow_cp,
	ir_sb_spring_harrow_cp,
	ir_sb_spring_roll,
	ir_sb_ferti_s2,
	ir_sb_ferti_p2,
	ir_sb_ferti_s3,
	ir_sb_ferti_p3,
	ir_sb_molluscicide,
	ir_sb_herbicide2,
	ir_sb_insecticide1,
	ir_sb_insecticide2,
	ir_sb_fungicide1,
	ir_sb_fungicide2,
	ir_sb_harvest,
	ir_sb_hay_bailing,
	ir_sb_foobar,
} IRSpringBarleyToDo;


/**
\brief
IRSpringBarley class
\n
*/
/**
See IRSpringBarley.h::IRSpringBarleyToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class IRSpringBarley : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	IRSpringBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(16, 8);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (ir_sb_foobar - IR_SB_BASE);
		m_base_elements_no = IR_SB_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others,	// zero element unused but must be here	
			 fmc_Others,	//	  ir_sb_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Others,	//	  ir_sb_sleep_all_day = IR_SB_BASE,
			 fmc_Fertilizer, //	ir_sb_ferti_s1
			 fmc_Fertilizer, //	ir_sb_ferti_p1
			 fmc_Cultivation,	// ir_sb_stubble_harrow_mt,	
			 fmc_Cultivation,	//	ir_sb_stubble_harrow_cp,
			 fmc_Others, //	ir_sb_sow_cover_crop,
			 fmc_Cultivation,	//	  ir_sb_stubble_cultivator,
			 fmc_Herbicide,// ir_sb_herbicide1_mt,
			 fmc_Herbicide,// ir_sb_herbicide1_cp,
			 fmc_Cultivation,	//	  ir_sb_stubble_cultivator_mt,
			 fmc_Cultivation,	//	  ir_sb_cultivation_sow_mt,
			 fmc_Cultivation,	//	  ir_sB_spring_plough_cp,
			 fmc_Others,	//	  ir_sb_spring_sow_cp,
			 fmc_Cultivation, // ir_sb_spring_harrow_cp,
			 fmc_Cultivation, // ir_sb_spring_roll,
			 fmc_Fertilizer, //	ir_sb_ferti_s2
			 fmc_Fertilizer, //	ir_sb_ferti_p2
			 fmc_Fertilizer, //	ir_sb_ferti_s3
			 fmc_Fertilizer, //	ir_sb_ferti_p3
			 fmc_Others, // ir_sb_molluscicide
			 fmc_Herbicide,	//	  ir_sb_herbicide2,
			 fmc_Insecticide,	//	  ir_sb_insecticide1,
			 fmc_Insecticide,	//	  ir_sb_insecticide2,
			 fmc_Fungicide,	//	  ir_sb_fungicide1,
			 fmc_Fungicide,	//	  ir_sb_fungicide2,
			 fmc_Harvest,	//	  ir_sb_harvest,
			 fmc_Others,	//	  ir_sb_hay_bailing,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // IRSPRINGBARLEY_H