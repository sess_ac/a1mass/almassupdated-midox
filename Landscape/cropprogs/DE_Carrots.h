/**
\file
\brief
<B>DE_Carrots.h This file contains the headers for the DE_Carrots class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Susanne Stein \n
 Version of August 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_Carrots.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_CARROTS_H
#define DE_CARROTS_H

#define DE_CAR_BASE 36200
/**
\brief Flags
*/
#define DE_CAR_WINTER_PLOUGH	a_field->m_user[1]
#define DE_CAR_HERBI1	a_field->m_user[2]
#define DE_CAR_HERBI2	a_field->m_user[3]
#define DE_CAR_FUNGI1	a_field->m_user[4]
#define DE_CAR_FUNGI2	a_field->m_user[5]

/** Below is the list of things that a farmer can do if he is growing carrot, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  de_car_start = 1, // Compulsory, must always be 1 (one).
  de_car_sleep_all_day = DE_CAR_BASE,
  de_car_spring_plough_sandy,
  de_car_winter_plough_clay,
  de_car_winter_deep_harrow_clay,
  de_car_ferti_p1,
  de_car_ferti_s1,
  de_car_preseeding_cultivator,
  de_car_bed_forming,
  de_car_spring_sow,
  de_car_ferti_p2,
  de_car_ferti_s2,
  de_car_herbicide1,
  de_car_herbicide2,
  de_car_herbicide3,
  de_car_fungicide1,
  de_car_fungicide2,
  de_car_fungicide3,
  de_car_harvest,
  de_car_foobar, // Obligatory, must be last
} DE_CarrotsToDo;


/**
\brief
DE_Carrots class
\n
*/
/**
See DE_Carrots.h::DECarrotsToDo for a complete list of all possible events triggered codes by the carrot management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_Carrots: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_Carrots(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,12 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_car_foobar - DE_CAR_BASE);
	   m_base_elements_no = DE_CAR_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			// ALL THE NECESSARY ENTRIES HERE
			fmc_Others,	//	  de_car_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	  de_car_sleep_all_day
			fmc_Cultivation,	//	  de_car_spring_plough_sandy
			fmc_Cultivation,	//	  de_car_winter_plough_clay
			fmc_Cultivation,	//	  de_car_winter_deep_harrow_clay
			fmc_Fertilizer,	//	  de_car_ferti_p1
			fmc_Fertilizer,	//	  de_car_ferti_s1
			fmc_Cultivation,	//	  de_car_preseeding_cultivator
			fmc_Cultivation,	//	  de_car_bed_forming
			fmc_Others,	//	  de_car_spring_sow
			fmc_Fertilizer,	//	  de_car_ferti_p2
			fmc_Fertilizer,	//	  de_car_ferti_s2
			fmc_Herbicide,	//	  de_car_herbicide1
			fmc_Herbicide,	//	  de_car_herbicide2
			fmc_Herbicide,	//	  de_car_herbicide3
			fmc_Fungicide,	//	  de_car_fungicide1
			fmc_Fungicide,	//	  de_car_fungicide2
			fmc_Fungicide,	//	  de_car_fungicide3
			fmc_Harvest,		//		de_car_harvest
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DE_CARROTS_H

