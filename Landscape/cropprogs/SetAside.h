/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>SetAside.h This file contains the headers for the SetAside class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of June 2003 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// SetAside.h
//

#ifndef SETASIDE_H
#define SETASIDE_H

#define SETASIDE_BASE 400

/** Below is the list of things that a farmer can do if he is has rotational setaside. For setaside this list is pretty short, because he can't do much with standard setaside (rules as of DK, 2000).
*/
typedef enum {
  sa_start = 1, // Compulsory, start event must always be 1 (one).
  sa_cut_to_hay = SETASIDE_BASE,
  sa_cattle_out,
  sa_cattle_is_out,
  sa_cut_to_silage,
  sa_wait,
  sa_foobar,
} SetAsideToDo;



/**
\brief
Rotational set-aside management class
\n
*/
/** 
SetAside.h::SetAsideToDo is the list of things that a farmer can do if he is has rotational setaside, at least following this basic plan. For setaside this list is pretty short, because he can't do much with standard setaside (rules as of DK, 2000).
So all we have to do is figure out when to do the different things using SetAside::Do Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds.
*/
class SetAside: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SetAside(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,7);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sa_foobar - SETASIDE_BASE);
	  m_base_elements_no = SETASIDE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  sa_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cutting,	//	  sa_cut_to_hay = SETASIDE_BASE,
			fmc_Grazing,	//	  sa_cattle_out,
			fmc_Grazing,	//	  sa_cattle_is_out,
			fmc_Cutting,	//	  sa_cut_to_silage,
			fmc_Others,	//	  sa_wait,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};
#endif // SETASIDE_H
