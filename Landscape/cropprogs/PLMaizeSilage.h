/**
\file
\brief
<B>PLMaizeSilage.h This file contains the headers for the MaizeSilage class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLMaizeSilage.h
//


#ifndef PLMAIZESILAGE_H
#define PLMAIZESILAGE_H

#define PLMAIZESILAGE_BASE 25700
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_MS_FERTI_P1	a_field->m_user[1]
#define PL_MS_FERTI_S1	a_field->m_user[2]
#define PL_MS_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_MS_WINTER_PLOUGH	a_field->m_user[4]
#define PL_MS_FERTI_P3	a_field->m_user[5]
#define PL_MS_FERTI_S3	a_field->m_user[6]
#define PL_MS_SPRING_FERTI a_field->m_user[7]
#define PL_MS_START_FERTI	a_field->m_user[8]

/** Below is the list of things that a farmer can do if he is growing mazie, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_ms_start = 1, // Compulsory, must always be 1 (one).
	pl_ms_sleep_all_day = PLMAIZESILAGE_BASE,
	pl_ms_ferti_p1, // 20801
	pl_ms_ferti_s1,
	pl_ms_stubble_plough,
	pl_ms_autumn_harrow1,
	pl_ms_autumn_harrow2,
	pl_ms_stubble_harrow,
	pl_ms_ferti_p2,
	pl_ms_ferti_s2,
	pl_ms_winter_plough,
	pl_ms_winter_stubble_cultivator_heavy,	// 20810
	pl_ms_ferti_p3,
	pl_ms_ferti_s3,
	pl_ms_spring_harrow,
	pl_ms_ferti_p4,
	pl_ms_ferti_s4,
	pl_ms_ferti_p5,
	pl_ms_ferti_s5,
	pl_ms_heavy_cultivator,
	pl_ms_preseeding_cultivator,
	pl_ms_spring_sow_with_ferti,
	pl_ms_spring_sow,	// 20821	
	pl_ms_herbicide1,
	pl_ms_herbicide2,
	pl_ms_fungicide1,
	pl_ms_insecticide1,
	pl_ms_insecticide2,
	pl_ms_biocide,
	pl_ms_ferti_p6,
	pl_ms_ferti_s6,	
	pl_ms_ferti_p7,
	pl_ms_ferti_s7,	// 20831
	pl_ms_harvest,
	pl_ms_straw_chopping,
	pl_ms_hay_bailing,
	pl_ms_ferti_p8,
	pl_ms_ferti_s8,
	pl_ms_ferti_p9,	
	pl_ms_ferti_s9,	// 20838
	pl_ms_foobar,
} PLMaizeSilageToDo;


/**
\brief
PLMaizeSilage class
\n
*/
/**
See PLMaizeSilage.h::PLMaizeSilageToDo for a complete list of all possible events triggered codes by the maize management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLMaizeSilage: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLMaizeSilage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_ms_foobar - PLMAIZESILAGE_BASE);
	   m_base_elements_no = PLMAIZESILAGE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_ms_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_ms_sleep_all_day = PLMAIZESILAGE_BASE,
			fmc_Fertilizer,//pl_ms_ferti_p1, // 20801
			fmc_Fertilizer,//pl_ms_ferti_s1,
			fmc_Cultivation,//pl_ms_stubble_plough,
			fmc_Cultivation,//pl_ms_autumn_harrow1,
			fmc_Cultivation,//pl_ms_autumn_harrow2,
			fmc_Cultivation,//pl_ms_stubble_harrow,
			fmc_Fertilizer,//pl_ms_ferti_p2,
			fmc_Fertilizer,//pl_ms_ferti_s2,
			fmc_Cultivation,//pl_ms_winter_plough,
			fmc_Cultivation,//pl_ms_winter_stubble_cultivator_heavy,	// 20810
			fmc_Fertilizer,//pl_ms_ferti_p3,
			fmc_Fertilizer,//pl_ms_ferti_s3,
			fmc_Cultivation,//pl_ms_spring_harrow,
			fmc_Fertilizer,//pl_ms_ferti_p4,
			fmc_Fertilizer,//pl_ms_ferti_s4,
			fmc_Fertilizer,//pl_ms_ferti_p5,
			fmc_Fertilizer,//pl_ms_ferti_s5,
			fmc_Cultivation,//pl_ms_heavy_cultivator,
			fmc_Cultivation,//pl_ms_preseeding_cultivator,
			fmc_Fertilizer,//pl_ms_spring_sow_with_ferti,
			fmc_Others,//pl_ms_spring_sow,	// 20821	
			fmc_Herbicide,//pl_ms_herbicide1,
			fmc_Herbicide,//pl_ms_herbicide2,
			fmc_Fungicide,//pl_ms_fungicide1,
			fmc_Insecticide,//pl_ms_insecticide1,
			fmc_Insecticide,//pl_ms_insecticide2,
			fmc_Herbicide,//pl_ms_biocide,
			fmc_Fertilizer,//pl_ms_ferti_p6,
			fmc_Fertilizer,//pl_ms_ferti_s6,
			fmc_Fertilizer,//pl_ms_ferti_p7,
			fmc_Fertilizer,//pl_ms_ferti_s7,	// 20831
			fmc_Harvest,//pl_ms_harvest,
			fmc_Others,//pl_ms_straw_chopping,
			fmc_Others,//pl_ms_hay_bailing,
			fmc_Fertilizer,//pl_ms_ferti_p8,
			fmc_Fertilizer,//pl_ms_ferti_s8,
			fmc_Fertilizer,//pl_ms_ferti_p9,
			fmc_Fertilizer//pl_ms_ferti_s9,	// 20838
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLMAIZESILAGE_H

