/**
\file
\brief
<B>UKPermanentGrass.h This file contains the headers for the PermanentGrass class</B> \n
*/
/**
\file
 by Chris J. Topping and Adam McVeigh \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKPermanentGrass.h
//


#ifndef UKPERMANENTGRASS_H
#define UKPERMANENTGRASS_H

#define UKPERMANENTGRASS_BASE 45300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define UK_PG_FERTI_DATE		a_field->m_user[1]
#define UK_PG_CUT_DATE		a_field->m_user[2]
#define UK_PG_WATER_DATE		a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing PermanentGrass, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	uk_pg_start = 1, // Compulsory, must always be 1 (one).
	uk_pg_sleep_all_day = UKPERMANENTGRASS_BASE,
	uk_pg_ferti_p1, // NPK I 90% March
	uk_pg_ferti_s1,
	uk_pg_cut_to_silage1, // Cutting 1 50% April-May (The rest graze)
	uk_pg_cut_to_silage2, // Cutting 2 100% 21 days after ferti_2
	uk_pg_cut_to_silage3, // Cutting 3 100% 21 days after ferti 4
	uk_pg_cut_to_silage4, // Cutting 4 100% August
	uk_pg_cut_to_silage5, // Cutting 5 40% 21 days after ferti 9
	uk_pg_cut_to_silage6, // Cutting 6 90% August-Oct 10 (if grazing)
	uk_pg_ferti_p2, // Slurry I 100% after Cutting 1
	uk_pg_ferti_s2,
	uk_pg_ferti_p3, // NPK II 90% After Slurry I
	uk_pg_ferti_s3,
	uk_pg_ferti_p4, // Slurry II 100% after Cutting 2
	uk_pg_ferti_s4,
	uk_pg_ferti_p5, // NPK III 90% before June 25
	uk_pg_ferti_s5,
	uk_pg_ferti_p6, // Slurry IV 100% after Cutting 3
	uk_pg_ferti_s6,
	uk_pg_ferti_p7, // NPK IV 90% before August 5
	uk_pg_ferti_s7,
	uk_pg_ferti_p8, // Slurry V 100% after Cutting 4
	uk_pg_ferti_s8,
	uk_pg_ferti_p9, // NPK V 90% before September 10
	uk_pg_ferti_s9,
	uk_pg_ferti_p10, // Slurry VI 100% 7 dats after Grazing (cattle out) ends
	uk_pg_ferti_s10,
	uk_pg_ferti_p11, // NPK VI 90% before Oct 30
	uk_pg_ferti_s11,
	uk_pg_watering,  // Water 20% April - August (as needed)
	uk_pg_cattle_out, // Grazing 50% (The rest cut) April-August
	uk_pg_cattle_is_out, // Grazing continues
	uk_pg_foobar,
} UKPermanentGrassToDo;


/**
\brief
UKPermanentGrass class
\n
*/
/**
See UKPermanentGrass.h::UKPermanentGrassToDo for a complete list of all possible events triggered codes by the PermanentGrass management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKPermanentGrass: public Crop
{
 public:
	 virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	 UKPermanentGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st March
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,3 );
		SetUpFarmCategoryInformation();
	 }
	 void SetUpFarmCategoryInformation() {
		 const int elements = 2 + (uk_pg_foobar - UKPERMANENTGRASS_BASE);
		 m_base_elements_no = UKPERMANENTGRASS_BASE - 2;
		 FarmManagementCategory catlist[elements] =
		 {
			  fmc_Others, // this needs to be at the zero line
			  fmc_Others,//uk_pg_start = 1, // Compulsory, must always be 1 (one).
			  fmc_Others,//uk_pg_sleep_all_day = UKPERMANENTGRASS_BASE,
			  fmc_Fertilizer,//uk_pg_ferti_p1, // NPK I 90% March
			  fmc_Fertilizer,//uk_pg_ferti_s1,
			  fmc_Cutting,//uk_pg_cut_to_silage1, // Cutting 1 50% April-May (The rest graze)
			  fmc_Cutting,//uk_pg_cut_to_silage2, // Cutting 2 100% 21 days after ferti_2
			  fmc_Cutting,//uk_pg_cut_to_silage3, // Cutting 3 100% 21 days after ferti 4
			  fmc_Cutting,//uk_pg_cut_to_silage4, // Cutting 4 100% August
			  fmc_Cutting,//uk_pg_cut_to_silage5, // Cutting 5 40% 21 days after ferti 9
			  fmc_Cutting,//uk_pg_cut_to_silage6, // Cutting 6 90% August-Oct 10 (if grazing)
			  fmc_Fertilizer,//uk_pg_ferti_p2, // Slurry I 100% after Cutting 1
			  fmc_Fertilizer,//uk_pg_ferti_s2,
			  fmc_Fertilizer,//uk_pg_ferti_p3, // NPK II 90% After Slurry I
			  fmc_Fertilizer,//uk_pg_ferti_s3,
			  fmc_Fertilizer,//uk_pg_ferti_p4, // Slurry II 100% after Cutting 2
			  fmc_Fertilizer,//uk_pg_ferti_s4,
			  fmc_Fertilizer,//uk_pg_ferti_p5, // NPK III 90% before June 25
			  fmc_Fertilizer,//uk_pg_ferti_s5,
			  fmc_Fertilizer,//uk_pg_ferti_p6, // Slurry IV 100% after Cutting 3
			  fmc_Fertilizer,//uk_pg_ferti_s6,
			  fmc_Fertilizer,//uk_pg_ferti_p7, // NPK IV 90% before August 5
			  fmc_Fertilizer,//uk_pg_ferti_s7,
			  fmc_Fertilizer,//uk_pg_ferti_p8, // Slurry V 100% after Cutting 4
			  fmc_Fertilizer,//uk_pg_ferti_s8,
			  fmc_Fertilizer,//uk_pg_ferti_p9, // NPK V 90% before September 10
			  fmc_Fertilizer,//uk_pg_ferti_s9,
			  fmc_Fertilizer,//uk_pg_ferti_p10, // Slurry VI 100% 7 dats after Grazing (cattle out) ends
			  fmc_Fertilizer,//uk_pg_ferti_s10,
			  fmc_Fertilizer,//uk_pg_ferti_p11, // NPK VI 90% before Oct 30
			  fmc_Fertilizer,//uk_pg_ferti_s11,
			  fmc_Watering,//uk_pg_watering,  // Water 20% April - August (as needed)
			  fmc_Grazing,//uk_pg_cattle_out, // Grazing 50% (The rest cut) April-August
			  fmc_Grazing,//uk_pg_cattle_is_out, // Grazing continues
		 };
		 copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	 }
};

#endif // UKPERMANENTGRASS_H

