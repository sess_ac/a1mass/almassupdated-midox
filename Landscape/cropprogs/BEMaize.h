/**
\file
\brief
<B>BEMaize.h This file contains the headers for the Maize class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BEMaize.h
//


#ifndef BEMAIZE_H
#define BEMAIZE_H

#define BEMAIZE_BASE 25200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define BE_M_START_FERTI	a_field->m_user[1]
#define BE_SB_FORCESPRING	a_field->m_user[2]

/** Below is the list of things that a farmer can do if he is growing mazie, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_m_start = 1, // Compulsory, must always be 1 (one).
	BE_m_sleep_all_day = BEMAIZE_BASE,
	BE_m_stubble_harrow1,
	BE_m_stubble_harrow2,
	BE_m_winter_plough1,
	BE_m_winter_plough2,
	BE_m_ferti_p1,
	BE_m_ferti_s1,
	BE_m_spring_plough1,
	BE_m_spring_plough2,
	BE_m_preseeding_cultivator,
	BE_m_spring_sow_with_ferti,
	BE_m_spring_sow,
	BE_m_ferti_p2,
	BE_m_ferti_s2,
	BE_m_herbicide1,
	BE_m_harvest,
	BE_m_straw_chopping,
} BEMaizeToDo;


/**
\brief
BEMaize class
\n
*/
/**
See BEMaize.h::BEMaizeToDo for a complete list of all possible events triggered codes by the mazie management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEMaize: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEMaize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10 );
   }
};

#endif // BEMAIZE_H

