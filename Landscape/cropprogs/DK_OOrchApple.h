/**
\file
\brief
<B>DK_OOrchApple.h This file contains the source for the DK_OOrchard_Perm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OOrchApple.h
//


#ifndef DK_OORCHAPPLE_H
#define DK_OORCHAPPLE_H

#define DK_OOAP_YEARS_AFTER_PLANT	a_field->m_user[0]
#define DK_OOAP_EST_YEAR a_field->m_user[1]

#define DK_OOAP_BASE 68400
/**

*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_ooap_start = 1, // Compulsory, must always be 1 (one).
	dk_ooap_sleep_all_day = DK_OOAP_BASE,
	dk_ooap_spring_plough,
	dk_ooap_stubble_harrow1,
	dk_ooap_stubble_harrow2,
	dk_ooap_manure1_s,
	dk_ooap_manure1_p,
	dk_ooap_water1,
	dk_ooap_subsoiler,
	dk_ooap_planting,
	dk_ooap_sow_grass,
	dk_ooap_manual_cutting1,
	dk_ooap_manual_cutting2,
	dk_ooap_manual_cutting3,
	dk_ooap_manual_cutting4,
	dk_ooap_manual_cutting5,
	dk_ooap_manual_cutting6,
	dk_ooap_manual_cutting7,
	dk_ooap_manual_cutting8,
	dk_ooap_manual_cutting9,
	dk_ooap_manual_cutting10,
	dk_ooap_manual_cutting11,
	dk_ooap_manual_cutting12,
	dk_ooap_manure2_s,
	dk_ooap_manure2_p,
	dk_ooap_row_cultivation1,
	dk_ooap_water2,
	dk_ooap_water3,
	dk_ooap_water4,
	dk_ooap_water5,
	dk_ooap_water6,
	dk_ooap_water7,
	dk_ooap_cutting1,
	dk_ooap_cutting2,
	dk_ooap_cutting3,
	dk_ooap_cutting4,
	dk_ooap_cutting5,
	dk_ooap_cutting6,
	dk_ooap_row_cultivation2,
	dk_ooap_copper_s,
	dk_ooap_copper_p,
	dk_ooap_fungicide1,
	dk_ooap_boron_s,
	dk_ooap_boron_p,
	dk_ooap_fungicide2,
	dk_ooap_insecticide,
	dk_ooap_remove_fruits,
	dk_ooap_fungicide3,
	dk_ooap_harvest,
	dk_ooap_fungicide4,
	dk_ooap_fungicide5,
	dk_ooap_foobar,
} DK_OOrchAppleToDo;


/**
\brief
DK_OOrchApple class
\n
*/
/**
See DK_OOrchApple.h::DK_OOrchAppleToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OOrchApple : public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OOrchApple(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,4 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_ooap_foobar - DK_OOAP_BASE);
	   m_base_elements_no = DK_OOAP_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_ooap_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_ooap_sleep_all_day = DK_OOAP_BASE,
			fmc_Cultivation, //dk_ooap_spring_plough,
			fmc_Cultivation, //dk_ooap_stubble_harrow1,
			fmc_Cultivation, //dk_ooap_stubble_harrow2,
			fmc_Fertilizer, //dk_ooap_manure1,
			fmc_Watering, //dk_ooap_water1,
			fmc_Cultivation, //dk_ooap_subsoiler,
			fmc_Others, //dk_ooap_planting,
			fmc_Others, //dk_ooap_sow_grass,
			fmc_Cutting, //dk_ooap_manual_cutting1,
			fmc_Cutting, //dk_ooap_manual_cutting2,
			fmc_Cutting, //dk_ooap_manual_cutting3,
			fmc_Cutting, //dk_ooap_manual_cutting4,
			fmc_Cutting, //dk_ooap_manual_cutting5,
			fmc_Cutting, //dk_ooap_manual_cutting6,
			fmc_Cutting, //dk_ooap_manual_cutting7,
			fmc_Cutting, //dk_ooap_manual_cutting8,
			fmc_Cutting, //dk_ooap_manual_cutting9,
			fmc_Cutting, //dk_ooap_manual_cutting10,
			fmc_Cutting, //dk_ooap_manual_cutting11,
			fmc_Cutting, //dk_ooap_manual_cutting12,
			fmc_Cutting, //dk_ooap_manure2,
			fmc_Cultivation, //dk_ooap_row_cultivation1,
			fmc_Watering, //dk_ooap_water2,
			fmc_Watering, //dk_ooap_water3,
			fmc_Watering, //dk_ooap_water4,
			fmc_Watering, //dk_ooap_water5,
			fmc_Watering, //dk_ooap_water6,
			fmc_Watering, //dk_ooap_water7,
			fmc_Cutting, //dk_ooap_cutting1,
			fmc_Cutting, //dk_ooap_cutting2,
			fmc_Cutting, //dk_ooap_cutting3,
			fmc_Cutting, //dk_ooap_cutting4,
			fmc_Cutting, //dk_ooap_cutting5,
			fmc_Cutting, //dk_ooap_cutting6,
			fmc_Cultivation, //dk_ooap_row_cultivation2,
			fmc_Fertilizer, //dk_ooap_coppper,
			fmc_Others, //dk_ooap_fungicide1,
			fmc_Fertilizer, //dk_ooap_boron,
			fmc_Others, //dk_ooap_fungicide2,
			fmc_Others, //dk_ooap_insecticide,
			fmc_Cutting, //dk_ooap_remove_fruits,
			fmc_Others, //dk_ooap_fungicide3,
			fmc_Harvest, //dk_ooap_harvest,
			fmc_Others, //dk_ooap_fungicide4,
			fmc_Others, //dk_ooap_fungicide5,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OOrchApple_H

