//
// DK_FodderBeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_FodderBeet_h
#define DK_FodderBeet_h


#define DK_FB_BASE 66000
#define DK_FB_FORCESPRING	a_field->m_user[1]

typedef enum {
  dk_fb_start = 1, // Compulsory, start event must always be 1 (one).
  dk_fb_harvest = DK_FB_BASE,
  dk_fb_autumn_plough,
  dk_fb_molluscicide1,
  dk_fb_harrow,
  dk_fb_sow,
  dk_fb_herbicide1,
  dk_fb_herbicide2,
  dk_fb_herbicide3,
  dk_fb_insecticide1,
  dk_fb_herbicide4,
  dk_fb_herbicide5,
  dk_fb_row_cultivation,
  dk_fb_insecticide2,
  dk_fb_fungicide1,
  dk_fb_fungicide2,
  dk_fb_foobar,
} DK_FodderBeetToDo;



class DK_FodderBeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_FodderBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(30,11); // 
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_fb_foobar - DK_FB_BASE);
	  m_base_elements_no = DK_FB_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_fb_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_fb_harvest = DK_FB_BASE,
			fmc_Cultivation,	//	  dk_fb_autumn_plough,
			fmc_Others,	//	  dk_fb_molluscicide1,
			fmc_Cultivation,	//	  dk_fb_harrow,
			fmc_Fertilizer,	//	  dk_fb_sow,
			fmc_Herbicide,	//	  dk_fb_herbicide1,
			fmc_Herbicide,	//	  dk_fb_herbicide2,
			fmc_Herbicide,	//	  dk_fb_herbicide3,
			fmc_Insecticide,	//	  dk_fb_insecticide1,
			fmc_Herbicide,	//	  dk_fb_herbicide4,
			fmc_Herbicide,	//	  dk_fb_herbicide5,
			fmc_Cultivation,	//	  dk_fb_row_cultivation,
			fmc_Insecticide,	//	  dk_fb_insecticide2,
			fmc_Fungicide,	//	  dk_fb_fungicide1,
			fmc_Fungicide,	//	  dk_fb_fungicide2,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_FodderBeet_h
