//
// DK_Legume_Whole.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_Legume_Whole_H
#define DK_Legume_Whole_H

#define DK_LW_BASE 60800

#define DK_LW_2_SOW m_field->m_user[0]
#define DK_LW_SOW_ROW m_field->m_user[1]
#define DK_LW_LAYOUT m_field->m_user[2]
#define DK_LW_FORCESPRING	a_field->m_user[3]
#define DK_LW_TILL_C m_field->m_user [4]
#define DK_LW_TILL_S m_field->m_user [5]

typedef enum {
  dk_lw_start = 1, // Compulsory, start event must always be 1 (one).
  dk_lw_harvest  = DK_LW_BASE,
  dk_lw_spring_plough,
  dk_lw_autumn_plough,
  dk_lw_spring_harrow,
  dk_lw_spring_sow,
  dk_lw_herbicide,
  dk_lw_spring_roll,
  dk_lw_manure_pig_s1,
  dk_lw_manure_pig_p1,
  dk_lw_spring_sow_lo,
  dk_lw_insecticide,
  dk_lw_swathing,
  dk_lw_straw_chopping,
  dk_lw_wait,
  dk_lw_foobar,
} DK_Legume_WholeToDo;



class DK_Legume_Whole: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_Legume_Whole(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(30,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_lw_foobar - DK_LW_BASE);
	  m_base_elements_no = DK_LW_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_lw_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_lw_harvest  = DK_LW_BASE,
			fmc_Cultivation,	//	  dk_lw_spring_plough,
			fmc_Cultivation,	//	  dk_lw_autumn_plough,
			fmc_Cultivation, // dk_lw_spring_harrow,
			fmc_Cultivation, 	//	  dk_lw_spring_sow,
			fmc_Herbicide,	//	  dk_lw_herbicide,
			fmc_Cultivation,	//	  dk_lw_spring_roll,
			fmc_Fertilizer,	//	  dk_lw_manure_pig_s1,
			fmc_Fertilizer,	//	  dk_lw_manure_pig_p1,
			fmc_Others,	//	  dk_lw_spring_sow_lo,
			fmc_Insecticide,	//	  dk_lw_insecticide,
			fmc_Cutting,	//	  dk_lw_swathing,
			fmc_Cutting, // dk_lw_straw_chopping,
			fmc_Others,	//	  dk_lw_wait,

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }

};

#endif // DK_Legume_Whole_H
