//
// OLegume.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved. 

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

// EZ: This code is based on DELegumes but assumes that no pesticides are applied

#ifndef DE_OLegume_H
#define DE_OLegume_H
/** \brief A flag used to indicate do catch crop
*/
#define DE_OLEGUME_DO_CATCHCROP	a_field->m_user[2]
#define DE_OLEGUME_BASE 37000

typedef enum {
  de_ol_start = 1, // Compulsory, start event must always be 1 (one).
  de_ol_sleep_all_day = DE_OLEGUME_BASE,
  de_ol_spring_harrow1,
  de_ol_spring_plough,
  de_ol_ferti_s,
  de_ol_ferti_p,
  de_ol_spring_harrow2,
  de_ol_spring_row_sow,
  de_ol_strigling,
  de_ol_rowcultivation1,
  de_ol_rowcultivation2, 
  de_ol_harvest,
    de_ol_foobar,
} DE_OLegumesToDo;



class DE_OLegume: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OLegume(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(15,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_ol_foobar - DE_OLEGUME_BASE);
	  m_base_elements_no = DE_OLEGUME_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_ol_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others,	//	  de_ol_sleep_all_day = DE_OLEGUME_BASE,
			fmc_Cultivation,	//	  de_ol_spring_harrow1,
			fmc_Cultivation,	//	  de_ol_spring_plough,
			fmc_Fertilizer,	//	  de_ol_ferti_s,
			fmc_Fertilizer,	//	  de_ol_ferti_p,
			fmc_Cultivation,	//	  de_ol_spring_harrow2,
			fmc_Others,	//	  de_ol_spring_row_sow,
			fmc_Cultivation,	//	  de_ol_strigling,
			fmc_Cultivation,	//	  de_ol_rowcultivation1,
			fmc_Cultivation,	//	  de_ol_rowcultivation2,
			fmc_Harvest,	//	  de_ol_harvest,

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DE_OLegume_H
