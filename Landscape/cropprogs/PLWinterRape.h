/**
\file
\brief
<B>PLWinterRape.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLWinterRape.h
//


#ifndef PLWinterRape_H
#define PLWinterRape_H

#define PLWinterRape_BASE 26300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_WR_FERTI_P1	a_field->m_user[1]
#define PL_WR_FERTI_S1	a_field->m_user[2]
#define PL_WR_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_WR_DECIDE_TO_GR a_field->m_user[4]

/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_wr_start = 1, // Compulsory, must always be 1 (one).
	pl_wr_sleep_all_day = PLWinterRape_BASE,
	pl_wr_ferti_p1, // 20101
	pl_wr_ferti_s1,
	pl_wr_stubble_plough,
	pl_wr_autumn_harrow1,
	pl_wr_autumn_harrow2,
	pl_wr_stubble_harrow,
	pl_wr_ferti_p2,
	pl_wr_ferti_s2,
	pl_wr_autumn_plough,
	pl_wr_autumn_roll,	//20110
	pl_wr_stubble_cultivator_heavy,
	pl_wr_ferti_p3,
	pl_wr_ferti_s3,
	pl_wr_preseeding_cultivator,
	pl_wr_autumn_harrow3,
	pl_wr_autumn_sow,
	pl_wr_herbicide1,
	pl_wr_herbicide2,
	pl_wr_fungicide1,
	pl_wr_insecticide1,	//20120
	pl_wr_insecticide2,
	pl_wr_insecticide3,
	pl_wr_molluscicide,
	pl_wr_ferti_p4,
	pl_wr_ferti_s4,
	pl_wr_ferti_p5,
	pl_wr_ferti_s5,
	pl_wr_ferti_p6,
	pl_wr_ferti_s6,
	pl_wr_ferti_p7,	//201030
	pl_wr_ferti_s7,
	pl_wr_ferti_p8,
	pl_wr_ferti_s8,
	pl_wr_herbicide3,
	pl_wr_fungicide2,
	pl_wr_fungicide3,
	pl_wr_fungicide4,
	pl_wr_insecticide4,
	pl_wr_insecticide5,
	pl_wr_insecticide6,	//20140
	pl_wr_herbicide4,
	pl_wr_harvest,
	pl_wr_straw_chopping,
	pl_wr_hay_bailing,
	pl_wr_ferti_p9,
	pl_wr_ferti_s9,
	pl_wr_ferti_p10,
	pl_wr_ferti_s10,	//20148
	pl_wr_foobar
} PLWinterRapeToDo;


/**
\brief
PLWinterRape class
\n
*/
/**
See PLWinterRape.h::PLWinterRapeToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLWinterRape: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLWinterRape(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th August
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 20,8 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_wr_foobar - PLWinterRape_BASE);
	   m_base_elements_no = PLWinterRape_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_wr_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_wr_sleep_all_day = PLWinterRape_BASE,
			fmc_Fertilizer,//pl_wr_ferti_p1, // 20101
			fmc_Fertilizer,//pl_wr_ferti_s1,
			fmc_Cultivation,//pl_wr_stubble_plough,
			fmc_Cultivation,//pl_wr_autumn_harrow1,
			fmc_Cultivation,//pl_wr_autumn_harrow2,
			fmc_Cultivation,//pl_wr_stubble_harrow,
			fmc_Fertilizer,//pl_wr_ferti_p2,
			fmc_Fertilizer,//pl_wr_ferti_s2,
			fmc_Cultivation,//pl_wr_autumn_plough,
			fmc_Others,//pl_wr_autumn_roll,	//20110
			fmc_Cultivation,//pl_wr_stubble_cultivator_heavy,
			fmc_Fertilizer,//pl_wr_ferti_p3,
			fmc_Fertilizer,//pl_wr_ferti_s3,
			fmc_Cultivation,//pl_wr_preseeding_cultivator,
			fmc_Cultivation,//pl_wr_autumn_harrow3,
			fmc_Others,//pl_wr_autumn_sow,
			fmc_Herbicide,//pl_wr_herbicide1,
			fmc_Herbicide,//pl_wr_herbicide2,
			fmc_Fungicide,//pl_wr_fungicide1,
			fmc_Insecticide,//pl_wr_insecticide1,	//20120
			fmc_Insecticide,//pl_wr_insecticide2,
			fmc_Insecticide,//pl_wr_insecticide3,
			fmc_Insecticide,//pl_wr_molluscicide,
			fmc_Fertilizer,//pl_wr_ferti_p4,
			fmc_Fertilizer,//pl_wr_ferti_s4,
			fmc_Fertilizer,//pl_wr_ferti_p5,
			fmc_Fertilizer,//pl_wr_ferti_s5,
			fmc_Fertilizer,//pl_wr_ferti_p6,
			fmc_Fertilizer,//pl_wr_ferti_s6,
			fmc_Fertilizer,//pl_wr_ferti_p7,	//201030
			fmc_Fertilizer,//pl_wr_ferti_s7,
			fmc_Fertilizer,//pl_wr_ferti_p8,
			fmc_Fertilizer,//pl_wr_ferti_s8,
			fmc_Herbicide,//pl_wr_herbicide3,
			fmc_Fungicide,//pl_wr_fungicide2,
			fmc_Fungicide,//pl_wr_fungicide3,
			fmc_Fungicide,//pl_wr_fungicide4,
			fmc_Insecticide,//pl_wr_insecticide4,
			fmc_Insecticide,//pl_wr_insecticide5,
			fmc_Insecticide,//pl_wr_insecticide6,	//20140
			fmc_Herbicide,//pl_wr_herbicide4,
			fmc_Harvest,//pl_wr_harvest,
			fmc_Others,//pl_wr_straw_chopping,
			fmc_Others,//pl_wr_hay_bailing,
			fmc_Fertilizer,//pl_wr_ferti_p9,
			fmc_Fertilizer,//pl_wr_ferti_s9,
			fmc_Fertilizer,//pl_wr_ferti_p10,
			fmc_Fertilizer//pl_wr_ferti_s10,	//20148
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLWinterRape_H

