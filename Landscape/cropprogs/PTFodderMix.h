/**
\file
\brief
<B>PTFodderMix.h This file contains the headers for the FodderMix class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of January 2018 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTFodderMix.h
//


#ifndef PTFODDERMIX_H
#define PTFODDERMIX_H

#define PTFODDERMIX_BASE 30400
/**
\brief A flag used to indicate cattle out dates
*/
#define PT_FL1_CATTLEOUT_DATE a_field->m_user[1]


/** Below is the list of things that a farmer can do if he is growing fodder lucerne in the first year, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_fm_start = 1, // Compulsory, must always be 1 (one).
	pt_fm_sleep_all_day = PTFODDERMIX_BASE,
	pt_fm_stubble_harrow1,
	pt_fm_autumn_plough,
	pt_fm_ferti_p1,//NPK
	pt_fm_ferti_s1,
	pt_fm_ferti_p2,//Calcium
	pt_fm_ferti_s2,
	pt_fm_stubble_harrow2,
	pt_fm_autumn_sow,
	pt_fm_cut_to_silage,
	pt_fm_harvest,
	pt_fm_hay_bailing,
	pt_fm_cattle_out,
	pt_fm_cattle_is_out,
	pt_fm_wait,
} PTFodderMixToDo;


/**
\brief
PTFodderMix class
\n
*/
/**
See PTFodderMix.h::PTFodderMixToDo for a complete list of all possible events triggered codes by the fodder lucerne1 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTFodderMix: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTFodderMix(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 31th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10 );//@AAS:Last possible date to do the first operation, i.e. stubble harrow
   }
};

#endif // PTFODDERMIX_H

