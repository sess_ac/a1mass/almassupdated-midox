//
// DummyCropPestTesting.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DummyCropPestTestingH
#define DummyCropPestTestingH

#define DCPT_BASE 200

#define DCPT_APRIL_PEST		a_field->m_user[1]
#define DCPT_MAY_PEST		a_field->m_user[2]
#define DCPT_JUNE_PEST		a_field->m_user[3]
#define DCPT_JULY_PEST		a_field->m_user[4]
#define DCPT_SEPT_PEST		a_field->m_user[5]
#define DCPT_OCT_PEST		a_field->m_user[6]

typedef enum {
  dcpt_start = 1, // Compulsory, start event must always be 1 (one).
  dcpt_March_pest1=DCPT_BASE,
  dcpt_April_pest1,
  dcpt_April_pest2,
  dcpt_May_pest1,
  dcpt_May_pest2,
  dcpt_June_pest1,
  dcpt_June_pest2,
  dcpt_July_pest1,
  dcpt_July_pest2,
  dcpt_August_pest1,
  dcpt_September_pest1,
  dcpt_September_pest2,
  dcpt_October_pest1,
  dcpt_October_pest2,
  dcpt_November_pest1,
  dcpt_foobar,
} DummyCropPestTestingToDo;



class DummyCropPestTesting : public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DummyCropPestTesting(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dcpt_foobar - DCPT_BASE);
	  m_base_elements_no = DCPT_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dcpt_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Herbicide,	//	  dcpt_March_pest1=DCPT_BASE,
			fmc_Herbicide,	//	  dcpt_April_pest1,
			fmc_Herbicide,	//	  dcpt_April_pest2,
			fmc_Herbicide,	//	  dcpt_May_pest1,
			fmc_Herbicide,	//	  dcpt_May_pest2,
			fmc_Herbicide,	//	  dcpt_June_pest1,
			fmc_Herbicide,	//	  dcpt_June_pest2,
			fmc_Herbicide,	//	  dcpt_July_pest1,
			fmc_Herbicide,	//	  dcpt_July_pest2,
			fmc_Herbicide,	//	  dcpt_August_pest1,
			fmc_Herbicide,	//	  dcpt_September_pest1,
			fmc_Herbicide,	//	  dcpt_September_pest2,
			fmc_Herbicide,	//	  dcpt_October_pest1,
			fmc_Herbicide,	//	  dcpt_October_pest2,
			fmc_Herbicide	//	  dcpt_November_pest1,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif
// DummyCropPestTesting.H













