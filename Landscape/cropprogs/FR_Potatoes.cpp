/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>FR_Potatoes.cpp This file contains the source for the FR_Potatoes class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of September 2021 \n
 \n
*/
//
// FR_Potatoes.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FR_Potatoes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_FR_Potatoes_SkScrapes("FR_CROP_P_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_potatoes_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_P_InsecticideDay;
extern CfgInt   cfg_P_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool FR_Potatoes::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	
	switch (a_ev->m_todo)
	{
	case fr_p_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		FR_P_PLOUGH = false;
		FR_P_SLURRY = false;

		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
		if (cfg_FR_Potatoes_SkScrapes.value()) {
			a_field->m_skylarkscrapes = true;
		}
		else {
			a_field->m_skylarkscrapes = false;
		}
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 3 start and stop dates for all events after harvest for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(5, 10)); // last possible day of harvest
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "FR_Potatoes::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "FR_Potatoes::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "FR_Potatoes::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes (start woth the first event on the after winter time) - Date here is a suggestion (not stated in crop scheme!)
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2), fr_p_herbicide1, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(10, 10);
		SimpleEvent(d1, fr_p_stubble_plough, false);
		break;

	}
	break;
	// This is the first real farm operation

	case fr_p_stubble_plough:
		if (a_ev->m_lock || a_farm->DoIt_prob(.80)) {
			if (!a_farm->StubblePlough(a_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_stubble_plough, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), fr_p_pk, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), fr_p_stubble_harrow, false);
		break;

	case fr_p_stubble_harrow:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(2, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_stubble_harrow, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), fr_p_pk, false);
		break;

	case fr_p_pk:
		if (a_ev->m_lock || a_farm->DoIt_prob(.40)) {
			if (!a_farm->FP_PK(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_pk, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1,2)+365, fr_p_herbicide1, false);
		break;

	case fr_p_herbicide1:
		if (a_ev->m_lock || a_farm->DoIt_prob(.40)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_herbicide1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 2), fr_p_fertilizer, false);
		break;

	case fr_p_fertilizer:
		if (a_ev->m_lock || a_farm->DoIt_prob(.30)) {
			if (!a_farm->FP_Manure(a_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_fertilizer, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fr_p_subsoiler, false);
		break;

	case fr_p_subsoiler:
		if (a_ev->m_lock || a_farm->DoIt_prob(.30)) {
			if (!a_farm->DeepPlough(a_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_subsoiler, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fr_p_spring_plough, false);
		break;

	case fr_p_spring_plough:
		if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_spring_plough, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 3), fr_p_sow, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), fr_p_fungicide1, false); // main thread
		break;

	case fr_p_sow:
		if (!a_farm->SpringSow(a_field, 0.0,
			g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_sow, true);
			break;
		}
		SimpleEvent(g_date->Date(), fr_p_n1, false);
		break;		

	case fr_p_n1:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_n1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fr_p_strigling_hill1, false);
		break;

	case fr_p_strigling_hill1:
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->StriglingHill(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_strigling_hill1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, fr_p_herbicide2, false);
			SimpleEvent(g_date->Date() + 30, fr_p_n2, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 30, fr_p_strigling_hill2, false);
		break;

	case fr_p_strigling_hill2:
		if (!a_farm->StriglingHill(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_strigling_hill1, true);
			break;
		}
		SimpleEvent(g_date->Date(), fr_p_n2, false);
		SimpleEvent(g_date->Date() + 1, fr_p_herbicide2, false);
		break;

	case fr_p_n2:
		if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_n2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), fr_p_insecticide1, false);
		break;

	case fr_p_insecticide1:
		if (a_ev->m_lock || a_farm->DoIt(80)) {
			if (!a_farm->InsecticideTreat(a_field, 0.0,
				g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_insecticide1, true);
				break;
			}
			SimpleEvent(g_date->Date()+14, fr_p_insecticide2, false);
			break;
		}
		break;

	case fr_p_insecticide2:
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->InsecticideTreat(a_field, 0.0,
				g_date->DayInYear(15, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_insecticide2, true);
				break;
			}
		}
		break; 

	case fr_p_herbicide2:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_herbicide2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), fr_p_herbicide3, false);
		break;

	case fr_p_herbicide3:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(16, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_herbicide3, true);
			break;
		}
		break;

	case fr_p_fungicide1:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_fungicide1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide2, false);
		break;

	case fr_p_fungicide2:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_fungicide2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide3, false);
		break;

	case fr_p_fungicide3:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_fungicide3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide4, false);
		break;

	case fr_p_fungicide4:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_fungicide4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide5, false);
		break;

	case fr_p_fungicide5:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_fungicide5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide6, false);
		break;

	case fr_p_fungicide6:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_fungicide6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide7, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(23, 6), fr_p_herbicide4, false);
		break;

	case fr_p_herbicide4:
		if (a_ev->m_lock || a_farm->DoIt(80)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(23, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_herbicide4, true);
				break;
			}
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(23, 6), fr_p_swathing, false);
		break;

	case fr_p_swathing:
		if (!a_farm->Swathing(a_field, 0.0, g_date->DayInYear(23, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_swathing, true);
			break;
		}
		break;

	case fr_p_fungicide7:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_fungicide7, true);
			break;
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide8, false);
		break;

	case fr_p_fungicide8:
		if (a_ev->m_lock || a_farm->DoIt(80)) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_fungicide8, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide9, false);
		break;

	case fr_p_fungicide9:
		if (a_ev->m_lock || a_farm->DoIt(80)) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_fungicide9, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide10, false);
		break;

	case fr_p_fungicide10:
		if (a_ev->m_lock || a_farm->DoIt(80)) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_fungicide10, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide11, false);
		break;

	case fr_p_fungicide11:
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_fungicide11, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 5, fr_p_fungicide12, false);
		break;

	case fr_p_fungicide12:
		if (a_ev->m_lock || a_farm->DoIt(25)) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fr_p_fungicide12, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 7), fr_p_harvest, false);
		break;

	case fr_p_harvest:
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fr_p_harvest, true);
			break;
		}
		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "FR_Potatoes::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}