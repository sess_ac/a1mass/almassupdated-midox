//
// WinterWheatStrigling.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WINTERWHEATStrigling_H
#define WINTERWHEATStrigling_H

#define WINTERWHEATStrigling_BASE 8300
#define WWStrigling_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define WWStrigling_AUTUMN_PLOUGH         m_field->m_user[1]

typedef enum {
  wws_start = 1, // Compulsory, must always be 1 (one).
  wws_sleep_all_day = WINTERWHEATStrigling_BASE,
  wws_ferti_p1,
  wws_ferti_s1,
  wws_ferti_s2,
  wws_autumn_plough,
  wws_autumn_harrow,
  wws_stubble_harrow1,
  wws_autumn_sow,
  wws_autumn_roll,
  wws_ferti_p2,
  wws_strigling0a,
  wws_spring_roll,
  wws_strigling0b,
  wws_GR,
  wws_fungicide,
  wws_fungicide2,
  wws_insecticide1,
  wws_insecticide2,
  wws_insecticide3,
  wws_strigling1,
  wws_strigling2,
  wws_water1,
  wws_water2,
  wws_ferti_p3,
  wws_ferti_p4,
  wws_ferti_p5,
  wws_ferti_s3,
  wws_ferti_s4,
  wws_ferti_s5,
  wws_harvest,
  wws_straw_chopping,
  wws_hay_turning,
  wws_hay_baling,
  wws_stubble_harrow2,
  wws_grubning,
  wws_foobar
} WinterWheatStriglingToDo;


class WinterWheatStrigling: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   WinterWheatStrigling(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
     m_first_date=g_date->DayInYear( 1,10 );
	 SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (wws_foobar - WINTERWHEATStrigling_BASE);
	   m_base_elements_no = WINTERWHEATStrigling_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
		   fmc_Others,	// zero element unused but must be here
		  fmc_Others,//wws_start = 1, // Compulsory, must always be 1 (one).
		  fmc_Others,//wws_sleep_all_day = WINTERWHEATStrigling_BASE,
		  fmc_Fertilizer,//wws_ferti_p1,
		  fmc_Fertilizer,//wws_ferti_s1,
		  fmc_Fertilizer,//wws_ferti_s2,
		  fmc_Cultivation,//wws_autumn_plough,
		  fmc_Cultivation,//wws_autumn_harrow,
		  fmc_Cultivation,//wws_stubble_harrow1,
		  fmc_Others,//wws_autumn_sow,
		  fmc_Others,//wws_autumn_roll,
		  fmc_Fertilizer,//wws_ferti_p2,
		  fmc_Cultivation,//wws_strigling0a,
		  fmc_Others,//wws_spring_roll,
		  fmc_Others,//wws_strigling0b,
		  fmc_Others,//wws_GR,
		  fmc_Fungicide,//wws_fungicide,
		  fmc_Fungicide,//wws_fungicide2,
		  fmc_Insecticide,//wws_insecticide1,
		  fmc_Insecticide,//wws_insecticide2,
		  fmc_Insecticide,//wws_insecticide3,
		  fmc_Cultivation,//wws_strigling1,
		  fmc_Cultivation,//wws_strigling2,
		  fmc_Watering,//wws_water1,
		  fmc_Watering,//wws_water2,
		  fmc_Fertilizer,//wws_ferti_p3,
		  fmc_Fertilizer,//wws_ferti_p4,
		  fmc_Fertilizer,//wws_ferti_p5,
		  fmc_Fertilizer,//wws_ferti_s3,
		  fmc_Fertilizer,//wws_ferti_s4,
		  fmc_Fertilizer,//wws_ferti_s5,
		  fmc_Harvest,//wws_harvest,
		  fmc_Others,//wws_straw_chopping,
		  fmc_Others,//wws_hay_turning,
		  fmc_Others,//wws_hay_baling,
		  fmc_Cultivation,//wws_stubble_harrow2,
		  fmc_Cultivation//wws_grubning,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // WINTERWHEATStrigling_H
