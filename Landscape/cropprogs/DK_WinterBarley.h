//
// DK_WinterBarley.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following dissbaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following dissbaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSBMUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSBMAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSBMUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSBMUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKWinterBarley_H
#define DKWinterBarley_H

#define DK_WB_BASE 66100

#define DK_WB_MN_S m_field->m_user[0]
#define DK_WB_MN_P m_field->m_user[1]

typedef enum {
	dk_wb_start = 1, // Compulsory, start event must always be 1 (one).
	dk_wb_harvest = DK_WB_BASE,
	dk_wb_autumn_plough,
	dk_wb_autumn_harrow,
	dk_wb_autumn_harrow_nt,
	dk_wb_autumn_roll,
	dk_wb_molluscicide,
	dk_wb_ferti_s1,
	dk_wb_ferti_p1,
	dk_wb_ferti_s2,
	dk_wb_ferti_p2,
	dk_wb_autumn_sow,
	dk_wb_herbicide1,
	dk_wb_ferti_s3,
	dk_wb_ferti_p3,
	dk_wb_ferti_s4,
	dk_wb_ferti_p4,
	dk_wb_ferti_s5,
	dk_wb_ferti_p5,
	dk_wb_ferti_s6,
	dk_wb_ferti_p6,
	dk_wb_ferti_s7,
	dk_wb_ferti_p7,
	dk_wb_herbicide2,
	dk_wb_herbicide3,
	dk_wb_fungicide1,
	dk_wb_fungicide2,
	dk_wb_gr1,
	dk_wb_gr2,
	dk_wb_wait,
	dk_wb_foobar,
} DK_WinterBarleyToDo;



class DK_WinterBarley : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_WinterBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_wb_foobar - DK_WB_BASE);
		m_base_elements_no = DK_WB_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_wb_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_wb_harvest = DK_WB_BASE,
			  fmc_Cultivation,	//	  dk_wb_autumn_plough,
			  fmc_Cultivation,	//	  dk_wb_autumn_harrow,
			  fmc_Cultivation,	//	  dk_wb_autumn_harrow_nt,
			  fmc_Others,	//	  dk_wb_autumn_roll,
			  fmc_Others, // dk_wb_molluscicide,
			  fmc_Fertilizer,	//	  dk_wb_ferti_s1,
			  fmc_Fertilizer,	//	  dk_wb_ferti_p1,
			  fmc_Fertilizer,	//	  dk_wb_ferti_s2,
			  fmc_Fertilizer,	//	  dk_wb_ferti_p2,
			  fmc_Cultivation,	//	  dk_wb_autumn_sow,
			  fmc_Herbicide,	//	  dk_wb_herbicide1,
			  fmc_Fertilizer,	//	  dk_wb_ferti_s3,
			  fmc_Fertilizer,	//	  dk_wb_ferti_p3,
			  fmc_Fertilizer,	//	  dk_wb_ferti_s4,
			  fmc_Fertilizer,	//	  dk_wb_ferti_p4,
			  fmc_Fertilizer,	//	  dk_wb_ferti_s5,
			  fmc_Fertilizer,	//	  dk_wb_ferti_p5,
			  fmc_Fertilizer,	//	  dk_wb_ferti_s6,
			  fmc_Fertilizer,	//	  dk_wb_ferti_p6,
			  fmc_Fertilizer,	//	  dk_wb_ferti_s7,
			  fmc_Fertilizer,	//	  dk_wb_ferti_p7,
			  fmc_Herbicide,	//	  dk_wb_herbicide2,
			  fmc_Herbicide,	//	  dk_wb_herbicide3,
			  fmc_Fungicide,	//	  dk_wb_fungicide1,
			  fmc_Fungicide,	//	  dk_wb_fungicide2,
			  fmc_Others, //		dk_wb_gr1,
		      fmc_Others, //		dk_wb_gr2,
			  fmc_Others, //		dk_wb_wait,

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};


#endif // DK_WinterBarley_H