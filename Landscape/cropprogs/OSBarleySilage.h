//
// OSBarleySilage.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OSBarleySilage_H
#define OSBarleySilage_H

#define OSBS_BASE 3800

typedef enum {
  osbs_start = 1, // Compulsory, start event must always be 1 (one).
  osbs_ferti_zero = OSBS_BASE,
  osbs_water1,
  osbs_water2,
  osbs_ferti_one,
  osbs_spring_plough,
  osbs_spring_harrow,
  osbs_spring_roll,
  osbs_spring_sow,
  osbs_spring_sow1,
  osbs_spring_sow2,
  osbs_cut_to_silage,
  osbs_foobar,
} OBSBSToDo;



class OSBarleySilage: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OSBarleySilage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (osbs_foobar - OSBS_BASE);
	  m_base_elements_no = OSBS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  osbs_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  osbs_ferti_zero = OSBS_BASE,
			fmc_Watering,	//	  osbs_water1,
			fmc_Watering,	//	  osbs_water2,
			fmc_Fertilizer,	//	  osbs_ferti_one,
			fmc_Cultivation,	//	  osbs_spring_plough,
			fmc_Cultivation,	//	  osbs_spring_harrow,
			fmc_Cultivation,	//	  osbs_spring_roll,
			fmc_Others,	//	  osbs_spring_sow,
			fmc_Others,	//	  osbs_spring_sow1,
			fmc_Others,	//	  osbs_spring_sow2,
			fmc_Cutting	//	  osbs_cut_to_silage,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OSBarleySilage_H
