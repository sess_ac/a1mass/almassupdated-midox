/**
\file
\brief
<B>NLCabbage.h This file contains the headers for the Cabbage class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLCabbage.h
//


#ifndef NLCABBAGE_H
#define NLCABBAGE_H

#define NLCABBAGE_BASE 20400
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_CAB_WINTER_PLOUGH	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_cab_start = 1, // Compulsory, must always be 1 (one).
	nl_cab_sleep_all_day = NLCABBAGE_BASE,
	nl_cab_winter_plough_clay,
	nl_cab_ferti_p1,
	nl_cab_ferti_s1,
	nl_cab_spring_plough_sandy,
	nl_cab_ferti_p2,
	nl_cab_ferti_s2,
	nl_cab_preseeding_cultivator,
	nl_cab_spring_planting,
	nl_cab_weeding1,
	nl_cab_herbicide1,
	nl_cab_weeding2,
	nl_cab_fungicide1,
	nl_cab_fungicide2,
	nl_cab_fungicide3,
	nl_cab_insecticide1,
	nl_cab_insecticide2,
	nl_cab_insecticide3,
	nl_cab_ferti_p3,
	nl_cab_ferti_s3,
	nl_cab_ferti_p4,
	nl_cab_ferti_s4,
	nl_cab_ferti_p5,
	nl_cab_ferti_s5,
	nl_cab_watering,
	nl_cab_harvest,
	nl_cab_foobar
} NLCabbageToDo;


/**
\brief
NLCabbage class
\n
*/
/**
See NLCabbage.h::NLCabbageToDo for a complete list of all possible events triggered codes by the cabbage management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLCabbage: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLCabbage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,12 );
		m_forcespringpossible = true;
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_cab_foobar - NLCABBAGE_BASE);
	   m_base_elements_no = NLCABBAGE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,//nl_cab_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_cab_sleep_all_day = NLCABBAGE_BASE,
			fmc_Cultivation,//nl_cab_winter_plough_clay,
			fmc_Fertilizer,//nl_cab_ferti_p1,
			fmc_Fertilizer,//nl_cab_ferti_s1,
			fmc_Cultivation,//nl_cab_spring_plough_sandy,
			fmc_Fertilizer,//nl_cab_ferti_p2,
			fmc_Fertilizer,//nl_cab_ferti_s2,
			fmc_Cultivation,//nl_cab_preseeding_cultivator,
			fmc_Others,//nl_cab_spring_planting,
			fmc_Cutting,//nl_cab_weeding1,
			fmc_Herbicide,//nl_cab_herbicide1,
			fmc_Cutting,//nl_cab_weeding2,
			fmc_Fungicide,//nl_cab_fungicide1,
			fmc_Fungicide,//nl_cab_fungicide2,
			fmc_Fungicide,//nl_cab_fungicide3,
			fmc_Insecticide,//nl_cab_insecticide1,
			fmc_Insecticide,//nl_cab_insecticide2,
			fmc_Insecticide,//nl_cab_insecticide3,
			fmc_Fertilizer,//nl_cab_ferti_p3,
			fmc_Fertilizer,//nl_cab_ferti_s3,
			fmc_Fertilizer,//nl_cab_ferti_p4,
			fmc_Fertilizer,//nl_cab_ferti_s4,
			fmc_Fertilizer,//nl_cab_ferti_p5,
			fmc_Fertilizer,//nl_cab_ferti_s5,
			fmc_Watering,//nl_cab_watering,
			fmc_Harvest//nl_cab_harvest,

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLCABBAGE_H

