/**
\file
\brief
<B>NLCatchCropPea.h This file contains the headers for the CatchPeaCrop class</B> \n
*/
/**
\file 
 by Chris J. Topping and Elzbieta Ziolkowska \n
 Version of June 22nd 2020 \n
 All rights reserved. \n
 \n
*/
//
// NLCatchCropPea.h
//


#ifndef NLCatchCropPea_H
#define NLCatchCropPea_H

#define NLCatchCropPea_BASE 20800

/** Below is the list of things that a farmer can do if he is growing NL_CatchCropPea, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_ccp_start = 1, // Compulsory, must always be 1 (one).
	nl_ccp_sleep_all_day = NLCatchCropPea_BASE,
	nl_ccp_stubble_cultivator,
	nl_ccp_ferti_p1,
	nl_ccp_ferti_s1,
	nl_ccp_preseeding_cultivator_with_sow,
	nl_ccp_ferti_p2_clay,
	nl_ccp_ferti_s2_clay,
	nl_ccp_winter_plough_clay,
	nl_ccp_ferti_p2_sandy,
	nl_ccp_ferti_s2_sandy,
	nl_ccp_foobar
} NLCatchCropPeaToDo;


/**
\brief
NLCatchCropPea class
\n
*/
/**
See NLCatchCropPea.h::NLCatchCropPeaToDo for a complete list of all possible events triggered codes by the CatchPeaCrop management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLCatchCropPea: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLCatchCropPea(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		/**
		* As a catch crop this differs a bit from the normal use of m_first_date. In this case we use the last possible date of the first action.
		*/
		m_first_date=g_date->DayInYear( 15,10 );
		SetCropClassification(tocc_Catch);
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_ccp_foobar - NLCatchCropPea_BASE);
	   m_base_elements_no = NLCatchCropPea_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//nl_ccp_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_ccp_sleep_all_day = NLCatchCropPea_BASE,
			fmc_Cultivation,//nl_ccp_stubble_cultivator,
			fmc_Fertilizer,//nl_ccp_ferti_p1,
			fmc_Fertilizer,//nl_ccp_ferti_s1,
			fmc_Cultivation,//nl_ccp_preseeding_cultivator_with_sow,
			fmc_Fertilizer,//nl_ccp_ferti_p2_clay,
			fmc_Fertilizer,//nl_ccp_ferti_s2_clay,
			fmc_Cultivation,//nl_ccp_winter_plough_clay,
			fmc_Fertilizer,//nl_ccp_ferti_p2_sandy,
			fmc_Fertilizer//nl_ccp_ferti_s2_sandy,

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLCatchCropPea_H

