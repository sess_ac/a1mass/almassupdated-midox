/**
\file
\brief
<B>DK_Cabbages.h This file contains the source for the DK_OCabbages class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of March 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_Cabbages.h
//


#ifndef DK_CABBAGES_H
#define DK_CABBAGES_H

#define DK_CA_BASE 60500
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DK_CA_WINTER_PLOUGH	m_field->m_user[1]
#define DK_CA_FORCESPRING	a_field->m_user[2]

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_ca_start = 1, // Compulsory, must always be 1 (one).
	dk_ca_harvest = DK_CA_BASE,
	dk_ca_molluscicide1,
	dk_ca_autumn_harrow_clay,
	dk_ca_sharrow1,
	dk_ca_sharrow2,
	dk_ca_sharrow3,
	dk_ca_sharrow4,
	dk_ca_herbicide1,
	dk_ca_plant,
	dk_ca_water,
	dk_ca_molluscicide2,
	dk_ca_herbicide2,
	dk_ca_strigling1,
	dk_ca_strigling2,
	dk_ca_strigling3,
	dk_ca_herbicide3,
	dk_ca_row_cultivation_clay,
	dk_ca_herbicide4,
	dk_ca_insecticide1,
	dk_ca_insecticide1_2a,
	dk_ca_insecticide1_2b,
	dk_ca_insecticide1_3b,
	dk_ca_manual_weeding,
	dk_ca_ferti2,
	dk_ca_fungicide1,
	dk_ca_fungicide1_2,
	dk_ca_fungicide1_3,
	dk_ca_wait,
	dk_ca_foobar,

} DK_CabbagesToDo;


/**
\brief
DK_Cabbages class
\n
*/
/**
See DK_Cabbages.h::DK_CabbagesToDo for a complete list of all possible events triggered codes by the cabbage management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_Cabbages: public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_Cabbages(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1, 12 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_ca_foobar - DK_CA_BASE);
	   m_base_elements_no = DK_CA_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_ca_start = 1, // Compulsory, must always be 1 (one).
			fmc_Harvest,	//	dk_ca_harvest = DK_CA_BASE,
			fmc_Insecticide,	//	dk_ca_molluscicide1,
			fmc_Cultivation,	//	dk_ca_autumn_harrow_clay,
			fmc_Cultivation,	//	dk_ca_sharrow1,
			fmc_Cultivation,	//	dk_ca_sharrow2,
			fmc_Cultivation,	//	dk_ca_sharrow3,
			fmc_Cultivation,	//	dk_ca_sharrow4,
			fmc_Herbicide,	//	dk_ca_herbicide1,
			fmc_Others,	//	dk_ca_plant,
			fmc_Watering,	//	dk_ca_water,
			fmc_Insecticide,	//	dk_ca_molluscicide2,
			fmc_Herbicide,	//	dk_ca_herbicide2,
			fmc_Cultivation,	//	dk_ca_strigling1,
			fmc_Cultivation,	//	dk_ca_strigling2,
			fmc_Cultivation,	//	dk_ca_strigling3,
			fmc_Herbicide,	//	dk_ca_herbicide3,
			fmc_Cultivation,	//	dk_ca_row_cultivation_clay,
			fmc_Herbicide,	//	dk_ca_herbicide4,
			fmc_Insecticide,	//	dk_ca_insecticide1,
			fmc_Insecticide,	//	dk_ca_insecticide1_2a,
			fmc_Insecticide,	//	dk_ca_insecticide1_2b,
			fmc_Insecticide,	//	dk_ca_insecticide1_3b,
			fmc_Cultivation,	//	dk_ca_manual_weeding,
			fmc_Fertilizer,	//	dk_ca_ferti2,
			fmc_Fungicide,	//	dk_ca_fungicide1,
			fmc_Fungicide,	//	dk_ca_fungicide1_2,
			fmc_Fungicide,	//	dk_ca_fungicide1_3,
			fmc_Others	//	dk_ca_wait,
				// no foobar entry			

	   };
	   // Iterate over the catlist elements and copy them to vector						
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};


#endif // DK_CABBAGES_H

