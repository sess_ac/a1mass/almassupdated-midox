//
// DK_GrazingPigs_Perm.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_GrazingPigs_Perm_H
#define DK_GrazingPigs_Perm_H

#define DK_GPP_BASE 67000
#define GPP_FIRST_PIGS_IN_DATE   m_field->m_user[0]
#define GPP_LAST_PIGS_IN_DATE    m_field->m_user[1]

typedef enum {
  dk_gpp_start = 1, // Compulsory, start event must always be 1 (one).
  dk_gpp_pigs_out = DK_GPP_BASE,
  dk_gpp_pigs_are_out,
  dk_gpp_pigs_are_out_forced,
  dk_gpp_foobar,
} DK_GrazingPigs_PermToDo;



class DK_GrazingPigs_Perm: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_GrazingPigs_Perm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L) 
  {
    m_first_date=g_date->DayInYear(1,11);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_gpp_foobar - DK_GPP_BASE);
	  m_base_elements_no = DK_GPP_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
					fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_gpp_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Grazing,	//	  dk_gpp_pigs_out = DK_GrazingPigs_Perm_BASE,
			fmc_Grazing,	//	  dk_gpp_pigs_are_out,
			fmc_Grazing	//	  dk_gpp_pigs_are_out_forced,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_GrazingPigs_Perm_H
