/**
\file
\brief
<B>DK_EnergyCrop_Perm.h This file contains the source for the DK_EnergyCrop_Perm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of May 2022 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_EnergyCrop_Perm.h
//


#ifndef DK_OENERGYCROP_H
#define DK_OENERGYCROP_H

#define DK_OEC_YEARS_AFTER_EST a_field->m_user[1]

#define DK_OEC_BASE 64300
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_oec_start = 1, // Compulsory, must always be 1 (one).
	dk_oec_sleep_all_day = DK_OEC_BASE,
	dk_oec_deep_plough_sand,
	dk_oec_roll1_sand,
	dk_oec_slurry1_s_sand,
	dk_oec_npk1_p_sand,
	dk_oec_deep_plough_clay,
	dk_oec_roll1_clay,
	dk_oec_slurry1_s_clay,
	dk_oec_npk1_p_clay,
	dk_oec_planting_sand,
	dk_oec_planting_clay,
	dk_oec_roll2,
	dk_oec_shallowharrow1,
	dk_oec_shallowharrow2,
	dk_oec_cut_trees,
	dk_oec_slurry2_s,
	dk_oec_npk2_p,
	dk_oec_shallowharrow3,
	dk_oec_shallowharrow4,
	dk_oec_harvest,
	dk_oec_slurry3_s,
	dk_oec_npk3_p,
	dk_oec_foobar,
} DK_OEnergyCrop_PermToDo;


/**
\brief
DK_OEnergyCrop_Perm class
\n
*/
/**
See DK_OEnergyCrop.h::DK_OEnergyCropToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OEnergyCrop_Perm: public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OEnergyCrop_Perm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 25,2 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_oec_foobar - DK_OEC_BASE);
	   m_base_elements_no = DK_OEC_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,	// dk_oec_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	// dk_oec_sleep_all_day = // DK_EC_BASE,
			fmc_Cultivation,	// dk_oec_deep_plough_sand,
			fmc_Cultivation,	//dk_oec_roll1_sand,
			fmc_Fertilizer,	// dk_oec_slurry1_s_sand,
			fmc_Fertilizer,	// dk_oec_npk1_p_sand,
			fmc_Cultivation,	// dk_oec_deep_plough_clay,
			fmc_Cultivation,	//dk_oec_roll1_clay,
			fmc_Fertilizer,	// dk_oec_slurry1_s_clay,
			fmc_Fertilizer,	// dk_oec_npk1_p_clay,
			fmc_Others,		//dk_oec_planting_sand,
			fmc_Others,		//dk_oec_planting_clay,
			fmc_Cultivation,	//dk_oec_roll2,
			fmc_Cultivation,	// dk_oec_shallow_harrow1,
			fmc_Cultivation,	// dk_oec_shallow_harrow2,
			fmc_Cutting,	// dk_oec_cut_trees, 
			fmc_Fertilizer,	// dk_oec_slurry2_s,
			fmc_Fertilizer,	// dk_oec_npk2_p,
			fmc_Cultivation,	// dk_oec_shallow_harrow3,
			fmc_Cultivation,	// dk_oec_shallow_harrow4,
			fmc_Harvest,	// dk_oec_harvest,
			fmc_Fertilizer,	// dk_oec_slurry3_s,
			fmc_Fertilizer,	// dk_oec_npk3_p,
				// no foobar entry

	   };
	   // Iterate over the catlist elements and copy them to vector
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};

#endif // DK_OEnergyCrop_H

