/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef CloverGrassGrazed1H
#define CloverGrassGrazed1H

#define CGG1_CUT_DATE          m_field->m_user[0]
#define CGG1_WATER_DATE        m_field->m_user[1]
#define CGG1_FERTI_DATE        m_field->m_user[2]
#define CGG1_FORCE_SECOND_CUT  m_field->m_user[3]
#define CGG1_HALT_SECOND_WATER m_field->m_user[4]
#define CGG1_BASE 1400

typedef enum {
  cgg1_start = 1, // Compulsory, start event must always be 1 (one).
  cgg1_ferti_zero = CGG1_BASE,
  cgg1_ferti_one,
  cgg1_ferti_two,
  cgg1_ferti_three,
  cgg1_ferti_four,
  cgg1_cut_to_silage,
  cgg1_cut_to_silage2,
  cgg1_cut_to_silage3,
  cgg1_water_zero,
  cgg1_water_one,
  cgg1_cattle_out,
  cgg1_cattle_is_out,
  cgg1_productapplic_one,
  cgg1_foobar
} CloverGrassGrazed1ToDo;
//} CloverGrassGrazed2ToDo;



class CloverGrassGrazed1: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  CloverGrassGrazed1(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(28,2);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (cgg1_foobar - CGG1_BASE);
	  m_base_elements_no = CGG1_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  cgg1_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  cgg1_ferti_zero = CGG1_BASE,
			fmc_Fertilizer,	//	  cgg1_ferti_one,
			fmc_Fertilizer,	//	  cgg1_ferti_two,
			fmc_Fertilizer,	//	  cgg1_ferti_three,
			fmc_Fertilizer,	//	  cgg1_ferti_four,
			fmc_Cutting,	//	  cgg1_cut_to_silage,
			fmc_Cutting,	//	  cgg1_cut_to_silage2,
			fmc_Cutting,	//	  cgg1_cut_to_silage3,
			fmc_Watering,	//	  cgg1_water_zero,
			fmc_Watering,	//	  cgg1_water_one,
			fmc_Grazing,	//	  cgg1_cattle_out,
			fmc_Grazing,	//	  cgg1_cattle_is_out,
			fmc_Herbicide	//	  cgg1_productapplic_one,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif
// CloverGrassGrazed1.H

