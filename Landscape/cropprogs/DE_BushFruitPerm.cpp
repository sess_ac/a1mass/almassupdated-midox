/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_BushFruitPerm.cpp This file contains the source for the DE_BushFruitPerm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2022 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DE_BushFruitPerm.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_BushFruitPerm.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..

extern CfgBool cfg_pest_bushfruit_on;
extern CfgFloat cfg_pest_product_1_amount;
extern Landscape* g_landscape_p;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DE_BushFruitPerm::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	int today = g_date->Date();
	TTypesOfVegetation l_tov = tov_DEBushFruitPerm;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_bfp_start:
	{
		// de_bfp_start just sets up all the starting conditions and reference dates that are needed to start a dk_bfp1
		DE_BFP_EARLY_HARVEST = false;

		m_last_date = g_date->DayInYear(30, 12); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(30, 12); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use - NO harvest here - this is herbicide2 instead
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(30, 12); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - water2

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_bfp_herbicide1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		// LKM: Here we queue up the first event 
		SimpleEvent(d1, de_bfp_herbicide1, false);
		break;
	}
	break;

	// LKM: This is the first real farm operation 
	case de_bfp_herbicide1:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_herbicide1, true);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		// fork of events
		SimpleEvent(d1, de_bfp_herbicide2, false); // herbi thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), de_bfp_fungicide1, false); // fungi/insecti thread - main thread
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), de_bfp_fertilizer1_s, false); 
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), de_bfp_fertilizer1_p, false);
		break;

	case de_bfp_herbicide2:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_herbicide2, true);
			break;
		}
		break;

	case de_bfp_fertilizer1_s: 
		if (m_farm->IsStockFarmer()) {
			if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
			{
				if (!a_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, de_bfp_fertilizer1_s, true);
					break;
				}
			}
		}
			break;

	case de_bfp_fertilizer1_p: 
			if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
			{
				if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, de_bfp_fertilizer1_p, true);
					break;
				}
			}
			break;

	case de_bfp_fungicide1:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide1, true);
			break;
		}
		SimpleEvent(g_date->Date(), de_bfp_insecticide1, false);
		break;

	case de_bfp_insecticide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide1, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(15, 4) - g_date->DayInYear();
				if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide1, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent(g_date->Date()+14, de_bfp_fungicide2, false);
		break;

	case de_bfp_fungicide2:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide2, true);
			break;
		}
		SimpleEvent(g_date->Date(), de_bfp_insecticide2, false);
		break;

	case de_bfp_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide2, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(30, 4) - g_date->DayInYear();
				if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide2, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), de_bfp_manual_weeding1, false);
		//SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), de_bfp_mow_margin1, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), de_bfp_fungicide3, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), de_bfp_cover_on, false); 
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), de_bfp_harvest, false); // main thread
		break;

	case de_bfp_manual_weeding1:
		if (!a_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_manual_weeding1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), de_bfp_manual_weeding2, false);
		break;

	case de_bfp_manual_weeding2:
		if (!a_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_manual_weeding2, true);
			break;
		}
		break; 

	case de_bfp_cover_on: // could be straw or fiber
		if (!m_farm->StrawCovering(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_cover_on, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), de_bfp_water1, false);
		break;

	//case de_bfp_mow_margin1: 
		//if (!m_farm->CutWeeds(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
		//	SimpleEvent(g_date->Date() + 1, de_bfp_mow_margin1, true);
		//	break;
		//}
		//SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), de_bfp_mow_margin2, false);
		//break;

	//case de_bfp_mow_margin2: 
		//if (!m_farm->CutWeeds(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
		//	SimpleEvent(g_date->Date() + 1, de_bfp_mow_margin2, true);
		//	break;
		//}
		//SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), de_bfp_manual_weeding2, false);
		//break;

	//case de_bfp_mow_margin3:
		//if (!m_farm->CutWeeds(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
		//	SimpleEvent(g_date->Date() + 1, de_bfp_mow_margin3, true);
		//	break;
		//}
		//break;

	case de_bfp_fungicide3:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide3, true);
			break;
		}
		SimpleEvent(g_date->Date(), de_bfp_insecticide3, false);
		break;

	case de_bfp_insecticide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide3, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(7, 5) - g_date->DayInYear();
				if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide3, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent(g_date->Date() + 7, de_bfp_fungicide4, false);
		break;

	case de_bfp_fungicide4:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide4, true);
			break;
		}
		SimpleEvent(g_date->Date(), de_bfp_insecticide4, false);
		break;

	case de_bfp_insecticide4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide4, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(15, 5) - g_date->DayInYear();
				if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide4, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent(g_date->Date()+7, de_bfp_fungicide5, false);
		break;


	case de_bfp_fungicide5:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(23, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide5, true);
			break;
		}
		SimpleEvent(g_date->Date(), de_bfp_insecticide5, false);
		break;

	case de_bfp_insecticide5:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide5, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(23, 5) - g_date->DayInYear();
				if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide5, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent(g_date->Date() + 7, de_bfp_fungicide6, false);
		break;

	case de_bfp_fungicide6:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide6, true);
			break;
		}
		SimpleEvent(g_date->Date(), de_bfp_insecticide6, false);
		break;

	case de_bfp_insecticide6:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide6, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(15, 5) - g_date->DayInYear();
				if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide6, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), de_bfp_fungicide7, false);
		break;

	case de_bfp_fungicide7:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide7, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), de_bfp_fungicide8, false);
		break;

	case de_bfp_fungicide8:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide8, true);
			break;
		}
		SimpleEvent(g_date->Date(), de_bfp_insecticide7, false);
		break;

	case de_bfp_insecticide7:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide7, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(15, 9) - g_date->DayInYear();
				if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide7, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent(g_date->Date() +14, de_bfp_fungicide9, false);
		break;

	case de_bfp_fungicide9:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_fungicide9, true);
			break;
		}
		SimpleEvent(g_date->Date(), de_bfp_insecticide8, false);
		break;

	case de_bfp_insecticide8:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide8, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(30, 9) - g_date->DayInYear();
				if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_bfp_insecticide8, true, m_farm, m_field);
					break;
				}
			}
		}
		break;

	case de_bfp_water1:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_water1, true);
			break;
		}
		SimpleEvent(g_date->Date()+14, de_bfp_water2, false); 
		break;

	case de_bfp_water2:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_water2, true);
			break;
		}
		break;
		
	case de_bfp_harvest:
		// continuous harvesting till 31 July, every 3rd day
		m_farm->FruitHarvest(m_field, 0.0, 0);
		if (today < g_date->OldDays() + g_date->DayInYear(31, 7)) {	
			SimpleEvent_(g_date->Date() + 3, de_bfp_harvest, true, m_farm, m_field);
			break;
		}
		//fork of events:
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), de_bfp_mowing_fruit, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), de_bfp_water3, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), de_bfp_herbicide3, false, m_farm, m_field); // main thread
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), de_bfp_fertilizer2_s, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), de_bfp_fertilizer2_p, false);
		break;

	case de_bfp_mowing_fruit:
		if (!a_farm->Swathing(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_mowing_fruit, true);
			break;
		}
		break;

	case de_bfp_water3:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_water3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, de_bfp_water4, false);
		break;

	case de_bfp_water4:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_water4, true);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
		}
		SimpleEvent(d1, de_bfp_water5, false);
		break;
		
	case de_bfp_water5:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_water5, true);
			break;
		}
		break;

	case de_bfp_fertilizer2_s: 
		if (m_farm->IsStockFarmer()) {
			if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
			{
				if (!a_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, de_bfp_fertilizer2_s, true);
					break;
				}
			}
		}
			break;

	case de_bfp_fertilizer2_p:
			if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
			{
				if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, de_bfp_fertilizer2_p, true);
					break;
				}
			}
			break;

	case de_bfp_herbicide3:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_herbicide3, true);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), de_bfp_herbicide4, false, m_farm, m_field); // main thread
		break;

	case de_bfp_herbicide4:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_herbicide4, true);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 11)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 11);
		}
		SimpleEvent_(d1, de_bfp_herbicide5, false, m_farm, m_field); // main thread
		break;

	case de_bfp_herbicide5:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_herbicide5, true);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 12)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 12);
		}
		SimpleEvent_(d1, de_bfp_herbicide6, false, m_farm, m_field); // main thread
		break;

	case de_bfp_herbicide6:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 12) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_bfp_herbicide6, true);
			break;
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop (DE_BushFruitPerm)
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_BushFruitPerm::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}