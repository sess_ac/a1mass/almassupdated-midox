//
// DK_OWinterRape.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OWinterRape.h"


bool DK_OWinterRape::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  TTypesOfVegetation l_tov = tov_DKOWinterRape;

  switch ( m_ev->m_todo ) {
  case dk_oosr_start:
    {
      a_field->ClearManagementActionSum();
      DK_OOSR_EARLY_SLURRY = false;
      DK_OOSR_ROW_SOW = false;
      m_last_date = g_date->DayInYear(10, 8); // Should match the last flexdate below
          //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
              // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(9, 8); // last possible day 
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(10, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 0;
      if (StartUpCrop(isSpring, flexdates, int(dk_oosr_ferti_s))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 7) + isSpring;
      // OK, let's go.
      SimpleEvent(d1, dk_oosr_autumn_harrow1, false);
  }
    break;

  case dk_oosr_autumn_harrow1:
      if (!m_farm->AutumnHarrow(m_field, 0.0,
          g_date->DayInYear(15, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_autumn_harrow1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_oosr_autumn_harrow2, false);
      break;

  case dk_oosr_autumn_harrow2:
      if (!m_farm->AutumnHarrow(m_field, 0.0,
          g_date->DayInYear(16, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_autumn_harrow2, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_s1, false);
      break;

  case dk_oosr_slurry_s1:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.75)) { // 75% do slurry now
          if (a_farm->IsStockFarmer()) {
              if (!m_farm->FA_Slurry(m_field, 0.0,
                  g_date->DayInYear(17, 8) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_s1, true);
                  break;
              }
              SimpleEvent(g_date->Date() + 2, dk_oosr_autumn_plough, false);
              break;
          }
          DK_OOSR_EARLY_SLURRY = true;
          SimpleEvent(g_date->Date() +1, dk_oosr_slurry_p1, false);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_oosr_autumn_plough, false);
      break;

  case dk_oosr_slurry_p1:
      if (!m_farm->FP_Slurry(m_field, 0.0,
          g_date->DayInYear(17, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_p1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 2, dk_oosr_autumn_plough, false);
      break;

  case dk_oosr_autumn_plough:
      if (!m_farm->AutumnPlough(m_field, 0.0,
          g_date->DayInYear(19, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_autumn_plough, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_oosr_cultivator_sow, false);
      break;

  case dk_oosr_cultivator_sow:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.80)) {
          if (!m_farm->PreseedingCultivatorSow(m_field, 0.0,
              g_date->DayInYear(20, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_oosr_cultivator_sow, true);
              break;
          }
          DK_OOSR_ROW_SOW = true;
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_oosr_slurry_s2, false); // ferti thread - main thread
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 9), dk_oosr_row_cultivation1, false); // row cultivation thread
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_oosr_sow, false);
      break;

  case dk_oosr_sow:
      if (!m_farm->AutumnSow(m_field, 0.0,
          g_date->DayInYear(20, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_sow, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_oosr_slurry_s2, false);
      break;

  case dk_oosr_row_cultivation1:
      if (!m_farm->RowCultivation(m_field, 0.0,
          g_date->DayInYear(10, 10) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_row_cultivation1, true);
          break;
      }
      break; // end of thread
    
  case dk_oosr_slurry_s2:
      if (a_farm->IsStockFarmer()) {
          if (DK_OOSR_EARLY_SLURRY == false) { // 25% do slurry now (not done before sowing)
              if (!m_farm->FA_Slurry(m_field, 0.0,
                  g_date->DayInYear(30, 9) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_s2, true);
                  break;
              }
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_oosr_ferti_s, false);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_oosr_slurry_p2, false);
      break;

  case dk_oosr_slurry_p2:
      if (DK_OOSR_EARLY_SLURRY == false) { // 25% do slurry now (not done before sowing)
          if (!m_farm->FP_Slurry(m_field, 0.0,
              g_date->DayInYear(29, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_p2, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_oosr_ferti_p, false);
      break;

  case dk_oosr_ferti_s:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) { // 50% can do this
          if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0,
              g_date->DayInYear(29, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_oosr_ferti_s, true);
              break;
          }
          SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_s3, false);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 3), dk_oosr_row_cultivation2, false);
      break;

  case dk_oosr_ferti_p:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) { // 50% can do this
          if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0,
              g_date->DayInYear(29, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_oosr_ferti_p, true);
              break;
          }
          SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_p3, false);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 3), dk_oosr_row_cultivation2, false);
      break;

  case dk_oosr_slurry_s3: // same 50%
      if (!m_farm->FA_Slurry(m_field, 0.0,
          g_date->DayInYear(30, 3) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_s3, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 3), dk_oosr_row_cultivation2, false);
      break;

  case dk_oosr_slurry_p3: // same 50%
      if (!m_farm->FP_Slurry(m_field, 0.0,
          g_date->DayInYear(30, 3) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_slurry_p3, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 3), dk_oosr_row_cultivation2, false);
      break;

  case dk_oosr_row_cultivation2:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.2)) { // 20% do this
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(31, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_oosr_row_cultivation2, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), dk_oosr_water, false);
      break;

  case dk_oosr_water:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(20, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_oosr_water, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 7), dk_oosr_swathing, false);
      break;

  case dk_oosr_swathing:
      if (!m_farm->Swathing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_swathing, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_oosr_harvest, false);
      break;

  case dk_oosr_harvest:
      if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_oosr_harvest, true);
          break;
      }
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "DK_OWinterRape::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


