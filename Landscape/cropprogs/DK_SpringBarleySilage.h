//
// DK_SpringBarley_Silage.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disspsaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disspsaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSPSUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSPSAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSPSUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSPSUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKSpringBarleySilage_H
#define DKSpringBarleySilage_H

#define DK_SPS_BASE 65700
#define DK_SPS_FORCESPRING	a_field->m_user[1]
#define DK_SPS_NO_TILL_C	a_field->m_user[2]
#define DK_SPS_NO_TILL_S	a_field->m_user[3]

typedef enum {
	dk_sps_start = 1, // Compulsory, start event must always be 1 (one).
	dk_sps_harvest = DK_SPS_BASE,
	dk_sps_autumn_plough1,
	dk_sps_slurry1,
	dk_sps_fertilizer,
	dk_sps_spring_plough,
	dk_sps_spring_sow,
	dk_sps_spring_sow_lo,
	dk_sps_water,
	dk_sps_herbicide,
	dk_sps_insecticide,
	dk_sps_fungicide,
	dk_sps_swathing,
	dk_sps_straw_chopping,
	dk_sps_foobar,
} DK_SpringBarleySilageToDo;



class DK_SpringBarleySilage : public Crop
{
public:
  bool  Do(Farm * a_farm, LE * a_field, FarmEvent * a_ev);
  DK_SpringBarleySilage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
	  m_first_date = g_date->DayInYear(1,12);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_sps_foobar - DK_SPS_BASE);
	  m_base_elements_no = DK_SPS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_sps_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_sps_harvest = DK_SPS_BASE,
			fmc_Cultivation,	//	  dk_sps_autumn_plough1,
			fmc_Fertilizer,	//	  dk_sps_slurry1,
			fmc_Fertilizer,	//	  dk_sps_fertilizer,
			fmc_Cultivation,	//	  dk_sps_spring_plough,
			fmc_Others,	//	  dk_sps_spring_sow,
			fmc_Others,	//	  dk_sps_spring_sow_lo,
			fmc_Watering,	//	  dk_sps_water,
			fmc_Herbicide,	//	  dk_sps_herbicide,
			fmc_Insecticide,	//	  dk_sps_insecticide,
			fmc_Fungicide,	//	  dk_sps_fungicide,
			fmc_Cutting,	//	  dk_sps_swathing,
			fmc_Others,	//	  dk_sps_straw_chopping,

			   // no foobar entry			

	  };
	  // Iterate over the catlist elements and copy them to vector						
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};


#endif // DK_SpringBarley_Silage_H
