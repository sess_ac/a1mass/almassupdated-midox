/**
\file
\brief
<B>IRGrassland_no_reseed.h This file contains the headers for the IRGrassland_no_reseed class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2022 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// IRGrassland_no_reseed.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef IRGRASSLAND_N_RS_H
#define IRGRASSLAND_N_RS_H

#define IR_GNRS_BASE 90300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define IR_GNRS_W_SIL        a_field->m_user[0]
#define IR_GNRS_W_SIL_1        a_field->m_user[1]
#define IR_GNRS_W_SIL_2       a_field->m_user[2]
#define IR_GNRS_EARLY        a_field->m_user[3]
#define IR_GNRS_MID       a_field->m_user[4]
#define IR_GNRS_LATE        a_field->m_user[5]
#define IR_GNRS_PEST_APP	a_field->m_user[6]
#define IR_GNRS_PEST_APP_1	a_field->m_user[7]
#define IR_GNRS_PEST_APP_2	a_field->m_user[8]


/** Below is the list of things that a farmer can do if he is growing the crop, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	ir_gnrs_start = 1, // Compulsory, must always be 1 (one).
	ir_gnrs_sleep_all_day = IR_GNRS_BASE,
	ir_gnrs_ferti_s1,
	ir_gnrs_ferti_p1,
	ir_gnrs_ferti_s2,
	ir_gnrs_ferti_p2,
	ir_gnrs_grazing1,
	ir_gnrs_cattle_out1,
	ir_gnrs_ferti_s3,
	ir_gnrs_ferti_p3,
	ir_gnrs_herbicide1,
	ir_gnrs_ferti_s4_s,
	ir_gnrs_ferti_p4_s,
	ir_gnrs_ferti_s4_c,
	ir_gnrs_ferti_p4_c,
	ir_gnrs_cutting1,
	ir_gnrs_grazing2,
	ir_gnrs_cattle_out2,
	ir_gnrs_ferti_s5,
	ir_gnrs_ferti_p5,
	ir_gnrs_herbicide2,
	ir_gnrs_ferti_s6_s,
	ir_gnrs_ferti_p6_s,
	ir_gnrs_ferti_s6_c,
	ir_gnrs_ferti_p6_c,
	ir_gnrs_cutting2,
	ir_gnrs_grazing3,
	ir_gnrs_cattle_out3,
	ir_gnrs_ferti_s7,
	ir_gnrs_ferti_p7,
	ir_gnrs_herbicide3,
	ir_gnrs_ferti_s8_c,
	ir_gnrs_ferti_p8_c,
	ir_gnrs_ferti_s8_s,
	ir_gnrs_ferti_p8_s,
	ir_gnrs_cutting3,
	ir_gnrs_grazing4,
	ir_gnrs_cattle_out4,
	ir_gnrs_foobar,
} IRGrassland_no_reseedToDo;


/**
\brief
IRGrassland_no_reseed class
\n
*/
/**
See IRGrassland_no_reseed.h::IRIRGrassland_no_reseedToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class IRGrassland_no_reseed : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	IRGrassland_no_reseed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(16, 8);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (ir_gnrs_foobar - IR_GNRS_BASE);
		m_base_elements_no = IR_GNRS_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	ir_gnrs_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	ir_gnrs_sleep_all_day = IR_GNRS_BASE,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s1,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p1,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s2,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p2,
			fmc_Grazing,	//	ir_gnrs_grazing1,
			fmc_Grazing,	//	ir_gnrs_cattle_out1,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s3,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p3,
			fmc_Herbicide,	//	ir_gnrs_herbicide1,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s4_s,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p4_s,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s4_c,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p4_c,
			fmc_Cutting,	//	ir_gnrs_cutting1,
			fmc_Grazing,	//	ir_gnrs_grazing2,
			fmc_Grazing,	//	ir_gnrs_cattle_out2,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s5,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p5,
			fmc_Herbicide,	//	ir_gnrs_herbicide2,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s6_s,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p6_s,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s6_c,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p6_c,
			fmc_Cutting,	//	ir_gnrs_cutting2,
			fmc_Grazing,	//	ir_gnrs_grazing3,
			fmc_Grazing,	//	ir_gnrs_cattle_out3,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s7,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p7,
			fmc_Herbicide,	//	ir_gnrs_herbicide3,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s8_c,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p8_c,
			fmc_Fertilizer,	//	ir_gnrs_ferti_s8_s,
			fmc_Fertilizer,	//	ir_gnrs_ferti_p8_s,
			fmc_Cutting,	//	ir_gnrs_cutting3,
			fmc_Grazing,	//	ir_gnrs_grazing4,
			fmc_Grazing,	//	ir_gnrs_cattle_out4,



				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // IRGRASSLAND_N_RS_H