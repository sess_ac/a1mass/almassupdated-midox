//
// DK_PotatoSeed.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_PotatoSeed_h
#define DK_PotatoSeed_h

#define DK_PS_BASE 65500

#define DK_PS_FORCESPRING	a_field->m_user[1]

typedef enum {
	dk_ps_start = 1, // Compulsory, start event must always be 1 (one).
	dk_ps_harvest = DK_PS_BASE,
	dk_ps_remove_straw,
	dk_ps_stoneburier,
	dk_ps_autumn_plough,
	dk_ps_deep_harrow,
	dk_ps_ferti_s1,
	dk_ps_ferti_p1,
	dk_ps_ferti_s2,
	dk_ps_ferti_p2,
	dk_ps_sow,
	dk_ps_water,
	dk_ps_strigling,
	dk_ps_harrow1,
	dk_ps_hill_up1,
	dk_ps_hill_up2,
	dk_ps_hill_up3,
	dk_ps_herbicide,
	dk_ps_herbicide_mw,
	dk_ps_herbicide1,
	dk_ps_herbicide2,
	dk_ps_herbicide3,
	dk_ps_herbicide4,
	dk_ps_herbicide5,
	dk_ps_herbicide6,
	dk_ps_ferti_s3,
	dk_ps_ferti_p3,
	dk_ps_fungicide1,
	dk_ps_fungicide2,
	dk_ps_fungicide3,
	dk_ps_fungicide4,
	dk_ps_fungicide5,
	dk_ps_fungicide6,
	dk_ps_fungicide7,
	dk_ps_fungicide8,
	dk_ps_fungicide9,
	dk_ps_fungicide10,
	dk_ps_insecticide1,
	dk_ps_insecticide2,
	dk_ps_insecticide3,
	dk_ps_harrow2,
	dk_ps_harrow3,
	dk_ps_herbicide7,
	dk_ps_herbicide8,
	dk_ps_foobar,
} DK_PotatoSeedToDo;



class DK_PotatoSeed : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_PotatoSeed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(28, 9); // 
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_ps_foobar - DK_PS_BASE);
		m_base_elements_no = DK_PS_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_ps_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_ps_harvest = DK_PS_BASE,
			  fmc_Others,	//	dk_ps_remove_straw,
			  fmc_Cultivation,	//dk_ps_stoneburier,
			  fmc_Cultivation,	//dk_ps_autumn_plough,
			  fmc_Cultivation,	//	dk_ps_deep_harrow,
			  fmc_Fertilizer,	//	dk_ps_ferti_s1,
			  fmc_Fertilizer,	//		dk_ps_ferti_p1,
			  fmc_Fertilizer,	//		dk_ps_ferti_s2,
			  fmc_Fertilizer,	//		dk_ps_ferti_p2,
			  fmc_Others,	//		dk_ps_sow,
			  fmc_Watering,	//		dk_ps_water,
			  fmc_Cultivation,	//		dk_ps_strigling,
			  fmc_Cultivation,	//		dk_ps_harrow1,
			  fmc_Cultivation,	//		dk_ps_hill_up1,
			  fmc_Cultivation,	//		dk_ps_hill_up2,
			  fmc_Cultivation,	//		dk_ps_hill_up3,
			  fmc_Herbicide,	//		dk_ps_herbicide,
			  fmc_Herbicide,	//		dk_ps_herbicide_mw,
			  fmc_Herbicide,	//		dk_ps_herbicide1,
			  fmc_Herbicide,	//		dk_ps_herbicide2,
			  fmc_Herbicide,	//	dk_ps_herbicide3,
			  fmc_Herbicide,	//	dk_ps_herbicide4,
			  fmc_Herbicide,	//	dk_ps_herbicide5,
			  fmc_Herbicide,	//	dk_ps_herbicide6,
			  fmc_Fertilizer,	//	dk_ps_ferti_s3,
			  fmc_Fertilizer,	//	dk_ps_ferti_p3,
			  fmc_Fungicide,	//	dk_ps_fungicide1,
			  fmc_Fungicide,	//	dk_ps_fungicide2,
			  fmc_Fungicide,	//	dk_ps_fungicide3,
			  fmc_Fungicide,	//	dk_ps_fungicide4,
			  fmc_Fungicide,	//	dk_ps_fungicide5,
			  fmc_Fungicide,	//	dk_ps_fungicide6,
			  fmc_Fungicide,	//	dk_ps_fungicide7,
			  fmc_Fungicide,	//	dk_ps_fungicide8,
				fmc_Fungicide,	//	dk_ps_fungicide9,
			  fmc_Fungicide,	//	dk_ps_fungicide10,
			  fmc_Insecticide,	//	dk_ps_insecticide1,
			  fmc_Insecticide,	//	dk_ps_insecticide2,
			  fmc_Insecticide,	//	dk_ps_insecticide3,
			  fmc_Cultivation,	//	dk_ps_harrow2,
			  fmc_Cultivation,	//	dk_ps_harrow3,
			  fmc_Herbicide,	//	dk_ps_herbicide7,
			  fmc_Herbicide,	//	dk_ps_herbicide8,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_PotatoSeed_h
