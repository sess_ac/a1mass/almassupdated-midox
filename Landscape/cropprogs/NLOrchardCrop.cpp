//
// NLOrchardCrop.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLOrchardCrop.h"

extern CfgBool cfg_pest_orchard_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_ORCH_InsecticideDay;
extern CfgInt   cfg_ORCH_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


bool NLOrchardCrop::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLOrchardCrop;
	int l_nextcropstartdate;
	/**********************************************To Here *************************************************************************/

  // Depending what event has occured jump to the correct bit of code
  switch ( m_ev->m_todo )
  {
  case nl_orch_start:
  {
	  NL_ORCH_INSECT = false;
	  NL_ORCH_ISECT_DAY = 0;
	  m_field->ClearManagementActionSum();

	  m_last_date = g_date->DayInYear(1, 9); // Should match the last flexdate below
	  //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
	  std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
	  // Set up the date management stuff
	  // Start and stop dates for all events after harvest
	  flexdates[0][1] = g_date->DayInYear(1, 9); // first possible day of finishing harvest - this is in effect day before the earliest date that a following crop can use

	  flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
	  flexdates[1][1] = g_date->DayInYear(1, 9);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping

	  // Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
	  int isSpring = 365;
	  if (StartUpCrop(isSpring, flexdates, int(nl_orch_cut1))) break;

	  // End single block date checking code. Please see next line comment as well.
	  // Reinit d1 to first possible starting date.
	  d1 = g_date->OldDays() + g_date->DayInYear(15, 4) + isSpring;
	  // OK, let's go.
	  SimpleEvent_(d1, nl_orch_insecticide1, false, m_farm, m_field);
	  SimpleEvent_(d1, nl_orch_cut1, false, m_farm, m_field);
	  break;
  }
  break;

  case nl_orch_cut1:
	  if (NL_ORCH_ISECT_DAY >= g_date->Date() - 2) { // Should by at least 3 days after insecticide
		  SimpleEvent_(g_date->Date() + 1, nl_orch_cut1, false, m_farm, m_field);
	  }
	  else
	  {
		  if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, nl_orch_cut1, true, m_farm, m_field);
			  break;
		  }
		  SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), nl_orch_cut2, false, m_farm, m_field);
		  break;
	  }
	  break;
  case nl_orch_cut2:
	  if (NL_ORCH_ISECT_DAY >= g_date->Date() - 2) { // Should by at least 3 days after insecticide
		  SimpleEvent_(g_date->Date() + 1, nl_orch_cut2, false, m_farm, m_field);
	  }
	  else
	  {
		  if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, nl_orch_cut2, true, m_farm, m_field);
			  break;
		  }
		  SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), nl_orch_cut3, false, m_farm, m_field);
		  break;
	  }
	  break;
  case nl_orch_cut3:
	  if (NL_ORCH_ISECT_DAY >= g_date->Date() - 2) { // Should by at least 3 days after insecticide
		  SimpleEvent_(g_date->Date() + 1, nl_orch_cut3, false, m_farm, m_field);
	  }
	  else
	  {
		  if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, nl_orch_cut3, true, m_farm, m_field);
			  break;
		  }
		  SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), nl_orch_cut4, false, m_farm, m_field);
		  break;
	  }
	  break;
  case nl_orch_cut4:
	  if (NL_ORCH_ISECT_DAY >= g_date->Date() - 2) { // Should by at least 3 days after insecticide
		  SimpleEvent_(g_date->Date() + 1, nl_orch_cut4, false, m_farm, m_field);
	  }
	  else
	  {
		  if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, nl_orch_cut4, true, m_farm, m_field);
			  break;
		  }
		  // End of thread
		  done = true;
		  break;
	  }
	  break;
  case nl_orch_insecticide1:
	  if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
	  {
		  // here we check wheter we are using ERA pesticide or not
		  if (!cfg_pest_orchard_on.value() ||
			  !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				  SimpleEvent_(g_date->Date() + 1, nl_orch_insecticide1, true, m_farm, m_field);
				  break;
			  }
		  }
		  else {
			  m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  NL_ORCH_ISECT_DAY = g_date->Date();
		  SimpleEvent_(g_date->Date() + 60, nl_orch_insecticide3, false, m_farm, m_field);
	  }
	  SimpleEvent_(g_date->Date() + 30, nl_orch_insecticide2, false, m_farm, m_field);
	  break;

  case nl_orch_insecticide2:
	  // here we check wheter we are using ERA pesticide or not
	  if (!cfg_pest_orchard_on.value() ||
		  !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
	  {
		  if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, nl_orch_insecticide2, true, m_farm, m_field);
			  break;
		  }
	  }
	  else {
		  m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
	  }
	  NL_ORCH_ISECT_DAY = g_date->Date();
	  SimpleEvent_(g_date->Date() + 30, nl_orch_insecticide3, false, m_farm, m_field);
	  break;
  case nl_orch_insecticide3:
	  if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
	  {
		  // here we check wheter we are using ERA pesticide or not
		  if (!cfg_pest_orchard_on.value() ||
			  !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
				  SimpleEvent_(g_date->Date() + 1, nl_orch_insecticide3, true, m_farm, m_field);
				  break;
			  }
		  }
		  else {
			  m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  NL_ORCH_INSECT = true;
		  NL_ORCH_ISECT_DAY = g_date->Date();
	  }
	  SimpleEvent_(g_date->Date() + 60, nl_orch_insecticide4, false, m_farm, m_field);
	  break;
  case nl_orch_insecticide4:
	  if (m_ev->m_lock || m_farm->DoIt_prob(0.67) && (NL_ORCH_INSECT==1))
	  {
		  // here we check wheter we are using ERA pesticide or not
		  if (!cfg_pest_orchard_on.value() ||
			  !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
				  SimpleEvent_(g_date->Date() + 1, nl_orch_insecticide4, true, m_farm, m_field);
				  break;
			  }
		  }
		  else {
			  m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  NL_ORCH_ISECT_DAY = g_date->Date();
	  }
	  // End of thread
	  break;
  default:
	  g_msg->Warn(WARN_BUG, "NLOrchardCrop::Do(): "
		  "Unknown event type! ", "");
	  exit(1);
	}
	return done;
}


