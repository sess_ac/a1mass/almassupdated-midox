/**
\file
\brief
<B>DK_SeedGrassRye_Spring.h This file contains the headers for the DK_SeedGrassRye_Spring class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of May 2022 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_SeedGrassRye_Spring.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_SEEDGRASSRYE_S_H
#define DK_SEEDGRASSRYE_S_H

#define DK_SGR_EVERY_2ND_YEAR a_field->m_user[1]
#define DK_SGR_FORCESPRING a_field->m_user[2]

#define DK_SGR_BASE 64600
/**
\
*/

/** Below is the list of things that a farmer can do if he is growing SetAside, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_sgr_start = 1, // Compulsory, must always be 1 (one).
	dk_sgr_autumn_plough = DK_SGR_BASE,
	dk_sgr_spring_plough,
	dk_sgr_sow,
	dk_sgr_herbicide1,
	dk_sgr_harvest,
	dk_sgr_remove_straw1,
	dk_sgr_ferti_s1,
	dk_sgr_ferti_p1,
	dk_sgr_straw_chopping1,
	dk_sgr_ferti_s2,
	dk_sgr_ferti_p2,
	dk_sgr_herbicide2,
	dk_sgr_fungicide1,
	dk_sgr_fungicide2,
	dk_sgr_growth_reg,
	dk_sgr_swathing,
	dk_sgr_seed_harvest,
	dk_sgr_straw_chopping2,
	dk_sgr_burn_straw,
	dk_sgr_ferti_s3,
	dk_sgr_ferti_p3,
	dk_sgr_herbicide3,
	dk_sgr_herbicide4,
	dk_sgr_foobar,
} DK_SeedGrassRye_SpringToDo;


/**
\brief
DK_SeedGrassRye_Spring class
\n
*/
/**
See DK_SeedGrassRye_Spring.h::DK_SeedGrassRye_SpringToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_SeedGrassRye_Spring : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_SeedGrassRye_Spring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(30, 11);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_sgr_foobar - DK_SGR_BASE);
		m_base_elements_no = DK_SGR_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_sgr_start = 1, // Compulsory, must always be 1 (one).
			fmc_Cultivation,	//	dk_sgr_plough = DK_SGR_BASE
			fmc_Others,	//	dk_sgr_sow 
			fmc_Herbicide,	//	dk_sgr_herbicide1,
			fmc_Harvest,	//	dk_sgr_harvest,
			fmc_Others, // dk_sgr_remove_straw1
			fmc_Fertilizer,	//	dk_sgr_ferti_s1,
			fmc_Fertilizer,	//	dk_sgr_ferti_p1,
			fmc_Cutting,	//	dk_sgr_straw_chopping1,
			fmc_Fertilizer,	//	dk_sgr_ferti_s2,
			fmc_Fertilizer,	//	dk_sgr_ferti_p2,
			fmc_Herbicide,	//	dk_sgr_herbicide2,
			fmc_Fungicide,	//	dk_sgr_fungicide1,
			fmc_Fungicide,	//	dk_sgr_fungicide2,
			fmc_Others, // dk_sgr_growth_reg,
			fmc_Cutting,	//	dk_sgr_swathing, 
			fmc_Harvest,	//	dk_sgr_seed_harvest,
			fmc_Cutting, // dk_sgr_straw_chopping2,
			fmc_Others, // dk_sgr_burn_straw
			fmc_Fertilizer,	//	dk_sgr_ferti_s3,
			fmc_Fertilizer,	//	dk_sgr_ferti_p3,
			fmc_Herbicide,	//	dk_sgr_herbicide3,
			fmc_Herbicide,	//	dk_sgr_herbicide4,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_SeedGrassRyee_Spring_H