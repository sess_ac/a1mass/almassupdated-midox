/**
\file
\brief
<B>UKMaize.h This file contains the headers for the Maize class</B> \n
*/
/**
\file
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKMaize.h
//

#ifndef UKMAIZE_H
#define UKMAIZE_H

#define UKMAIZE_BASE 45200
#define UKMAIZE_SOW_DATE m_field->m_user[0]
#define UKMAIZE_HERBI_ONE_DATE m_field->m_user[1]

typedef enum {
  uk_ma_start = 1, // Compulsory, start event must always be 1 (one).
  uk_ma_sleep_all_day = UKMAIZE_BASE,
  uk_ma_autumn_harrow,
  uk_ma_autumn_plough,
  uk_ma_ferti_s1, // Slurry
  uk_ma_ferti_p1,
  uk_ma_spring_plough,
  uk_ma_preseeding_cultivator,
  uk_ma_spring_sow, // SpringSow
  uk_ma_spring_sow_with_ferti, // SpringSowWithFerti
  uk_ma_spring_harrow,
  uk_ma_ferti_s2, // NPK if no ferti with sow
  uk_ma_ferti_p2,
  uk_ma_herbicide1,
  uk_ma_harvest,
  uk_ma_straw_chopping,
  uk_ma_foobar,
} UKMaizeToDo;



class UKMaize: public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	UKMaize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
		/**
		When we start it off, the first possible date for a farm operation is 1st September
		This information is used by other crops when they decide how much post processing of
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date=g_date->DayInYear(20,9);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (uk_ma_foobar - UKMAIZE_BASE);
		m_base_elements_no = UKMAIZE_BASE - 2;
		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others, // this needs to be at the zero line
			 fmc_Others,//uk_ma_start = 1, // Compulsory, start event must always be 1 (one).
			 fmc_Others,//uk_ma_sleep_all_day = UKMAIZE_BASE,
			 fmc_Cultivation,//uk_ma_autumn_harrow,
			 fmc_Cultivation,//uk_ma_autumn_plough,
			 fmc_Fertilizer,//uk_ma_ferti_s1, // Slurry
			 fmc_Fertilizer,//uk_ma_ferti_p1,
			 fmc_Cultivation,//uk_ma_spring_plough,
			 fmc_Cultivation,//uk_ma_preseeding_cultivator,
			 fmc_Others,//uk_ma_spring_sow, // SpringSow
			 fmc_Others, //uk_ma_spring_sow_with_ferti, // SpringSowWithFerti
			 fmc_Cultivation,//uk_ma_spring_harrow,
			 fmc_Fertilizer,//uk_ma_ferti_s2, // NPK if no ferti with sow
			 fmc_Fertilizer,//uk_ma_ferti_p2,
			 fmc_Herbicide,//uk_ma_herbicide1,
			 fmc_Harvest,//uk_ma_harvest,
			 fmc_Others,//uk_ma_straw_chopping,
		};
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // UKMAIZE_H
