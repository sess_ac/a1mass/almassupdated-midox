/**
\file
\brief
<B>PTOtherDryBeans.h This file contains the headers for the OtherDryBeans class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTOtherDryBeans.h
//


#ifndef PTOTHERDRYBEANS_H
#define PTOTHERDRYBEANS_H

#define PTOTHERDRYBEANS_BASE 30900
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing OtherDryBeans, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_odb_start = 1, // Compulsory, must always be 1 (one).
	pt_odb_sleep_all_day = PTOTHERDRYBEANS_BASE,
	pt_odb_stubble_harrow,
	pt_odb_autumn_plough,
	pt_odb_ferti_p1, // NPK
	pt_odb_ferti_s1,
	pt_odb_autumn_harrow,
	pt_odb_autumn_sow,
	pt_odb_harvest,
	//pt_odb_wait,

} PTOtherDryBeansToDo;


/**
\brief
PTOtherDryBeans class
\n
*/
/**
See PTOtherDryBeans.h::PTOtherDryBeansToDo for a complete list of all possible events triggered codes by the OtherDryBeans management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTOtherDryBeans: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTOtherDryBeans(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,11 );//@AAS:Last possible date to do the first operation, i.e. stuble harrow
   }
};

#endif // PTOTHERDRYBEANS_H

