/**
\file
\brief
<B>PTCorkOak.h This file contains the headers for the CorkOak class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTCorkOak.h
//


#ifndef PTCORKOAK_H
#define PTCORKOAK_H

#define PTCORKOAK_BASE 31100
/**
\brief A flag used to indicate ploughing status
*/
#define PT_CO_YEARS_AFTER_PLOUGH a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing CorkOak, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_co_start = 1, // Compulsory, must always be 1 (one).
	pt_co_sleep_all_day = PTCORKOAK_BASE,
	pt_co_spring_plough,
	pt_co_wait,
	
} PTCorkOakToDo;


/**
\brief
PTCorkOak class
\n
*/
/**
See PTCorkOak.h::PTCorkOakToDo for a complete list of all possible events triggered codes by the CorkOak management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTCorkOak: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTCorkOak(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,3 );//@AAS:Last possible date to do the first operation, i.e. plough or sow
   }
};

#endif // PTCORKOAK_H

