//
// DK_WinterWheat_CC.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following dissbaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following dissbaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSBMUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSBMAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSBMUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSBMUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKWinterWheat_CC_H
#define DKWinterWheat_CC_H

#define DK_WWCC_BASE 160800

#define DK_WWCC_MN_S m_field->m_user[0]
#define DK_WWCC_MN_P m_field->m_user[1]
#define DK_WWCC_TILL_1 m_field->m_user[2]
#define DK_WWCC_TILL_2 m_field->m_user[3]

typedef enum {
	dk_wwcc_start = 1, // Compulsory, start event must always be 1 (one).
	dk_wwcc_harvest = DK_WWCC_BASE,
	dk_wwcc_autumn_plough,
	dk_wwcc_autumn_harrow,
	dk_wwcc_autumn_harrow_nt,
	dk_wwcc_autumn_roll,
	dk_wwcc_molluscicide,
	dk_wwcc_ferti_s1,
	dk_wwcc_ferti_p1,
	dk_wwcc_ferti_s2,
	dk_wwcc_ferti_p2,
	dk_wwcc_autumn_sow,
	dk_wwcc_herbicide1,
	dk_wwcc_ferti_s3,
	dk_wwcc_ferti_p3,
	dk_wwcc_ferti_s4,
	dk_wwcc_ferti_p4,
	dk_wwcc_ferti_s5,
	dk_wwcc_ferti_p5,
	dk_wwcc_ferti_s6,
	dk_wwcc_ferti_p6,
	dk_wwcc_ferti_s7,
	dk_wwcc_ferti_p7,
	dk_wwcc_herbicide2,
	dk_wwcc_herbicide3,
	dk_wwcc_herbicide4,
	dk_wwcc_herbicide5,
	dk_wwcc_herbicide6,
	dk_wwcc_herbicide7,
	dk_wwcc_fungicide1,
	dk_wwcc_fungicide2,
	dk_wwcc_insecticide1,
	dk_wwcc_insecticide2,
	dk_wwcc_gr,
	dk_wwcc_wait,
	dk_wwcc_foobar,
} DK_WinterWheat_CCToDo;



class DK_WinterWheat_CC : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_WinterWheat_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_wwcc_foobar - DK_WWCC_BASE);
		m_base_elements_no = DK_WWCC_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_wwcc_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_wwcc_harvest = DK_wwcc_BASE,
			  fmc_Cultivation,	//	  dk_wwcc_autumn_plough,
			  fmc_Cultivation,	//	  dk_wwcc_autumn_harrow,
			  fmc_Cultivation,	//	  dk_wwcc_autumn_harrow_nt,
			  fmc_Others,	//	  dk_wwcc_autumn_roll,
			  fmc_Others, // dk_wwcc_molluscicide,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_s1,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_p1,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_s2,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_p2,
			  fmc_Cultivation,	//	  dk_wwcc_autumn_sow,
			  fmc_Herbicide,	//	  dk_wwcc_herbicide1,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_s3,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_p3,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_s4,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_p4,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_s5,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_p5,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_s6,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_p6,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_s7,
			  fmc_Fertilizer,	//	  dk_wwcc_ferti_p7,
			  fmc_Herbicide,	//	  dk_wwcc_herbicide2,
			  fmc_Herbicide,	//	  dk_wwcc_herbicide3,
			  fmc_Herbicide,	//	  dk_wwcc_herbicide4,
			  fmc_Herbicide,	//	  dk_wwcc_herbicide5,
			  fmc_Herbicide,	//	  dk_wwcc_herbicide6,
			  fmc_Herbicide,	//	  dk_wwcc_herbicide7,
			  fmc_Fungicide,	//	  dk_wwcc_fungicide1,
			  fmc_Fungicide,	//	  dk_wwcc_fungicide2,
			  fmc_Insecticide,	//	  dk_wwcc_insecticide1,
			  fmc_Insecticide,	//	  dk_wwcc_insecticide2,
			  fmc_Others, //		dk_wwcc_gr,
			  fmc_Others, //		dk_wwcc_wait,

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};


#endif // DK_WinterWheat_CC_H