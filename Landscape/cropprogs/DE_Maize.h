/**
\file
\brief
<B>DE_Maize.h This file contains the headers for the DE_Maize class</B> \n
*/
/**
\file 
 by Chris J. Topping and Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_Maize.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_MAIZE_H
#define DE_MAIZE_H

#define DE_MAIZE_BASE 35500
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DE_M_START_FERTI	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing DEMaize, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_m_start = 1, // Compulsory, must always be 1 (one).
	de_m_sleep_all_day = DE_MAIZE_BASE,
	de_m_stubble_harrow,
	de_m_winter_plough,
	de_m_ferti_s1,
	de_m_ferti_p1,
	de_m_spring_plough_sandy,
	de_m_preseeding_cultivator,
	de_m_spring_sow_with_ferti,
	de_m_insecticide,
	de_m_ferti_s2,
	de_m_ferti_p2,
	de_m_herbicide1,
	de_m_harvest,
	de_m_foobar,  // Obligatory, must be last
} DE_MaizeToDo;


/**
\brief
DE_Maize class
\n
*/
/**
See DE_Maize.h::DE_MaizeToDo for a complete list of all possible events triggered codes by the Maize management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_Maize: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_Maize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of 
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date=g_date->DayInYear( 31,10 );
		m_forcespringpossible = true;
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_m_foobar - DE_MAIZE_BASE);
	   m_base_elements_no = DE_MAIZE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			// ALL THE NECESSARY ENTRIES HERE
			fmc_Others,			//	de_m_start = 1, // Compulsory, must always be 1 (one)
			fmc_Others,			//	de_m_sleep_all_day
			fmc_Cultivation,	//	de_m_stubble_harrow,
			fmc_Cultivation,	//	de_m_winter_plough,
			fmc_Fertilizer,		//	de_m_ferti_s1,
			fmc_Fertilizer,		//	de_m_ferti_p1,
			fmc_Cultivation,	//	de_m_spring_plough,
			fmc_Cultivation,	//	de_m_preseeding_cultivator,
			fmc_Others,			//	de_m_spring_sow_with_ferti,
			fmc_Insecticide,	//	de_m_insecticide,
			fmc_Fertilizer,		//	de_m_ferti_s2,
			fmc_Fertilizer,		//	de_m_ferti_p2,
			fmc_Herbicide,		//	de_m_herbicide1,
			fmc_Harvest,		//	de_m_harvest,
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DE_MAIZE_H

