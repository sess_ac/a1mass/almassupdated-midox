//
// DK_Potato.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_Potato_h
#define DK_Potato_h

#define DK_PO_BASE 65300

#define DK_PO_FORCESPRING	a_field->m_user[1]

typedef enum {
	dk_po_start = 1, // Compulsory, start event must always be 1 (one).
	dk_po_harvest = DK_PO_BASE,
	dk_po_remove_straw,
	dk_po_stoneburier,
	dk_po_autumn_plough,
	dk_po_deep_harrow,
	dk_po_ferti_s1,
	dk_po_ferti_p1,
	dk_po_ferti_s2,
	dk_po_ferti_p2,
	dk_po_sow,
	dk_po_water,
	dk_po_strigling,
	dk_po_harrow1,
	dk_po_hill_up1,
	dk_po_hill_up2,
	dk_po_hill_up3,
	dk_po_herbicide,
	dk_po_herbicide_mw,
	dk_po_herbicide1,
	dk_po_herbicide2,
	dk_po_herbicide3,
	dk_po_herbicide4,
	dk_po_herbicide5,
	dk_po_herbicide6,
	dk_po_ferti_s3,
	dk_po_ferti_p3,
	dk_po_fungicide1,
	dk_po_fungicide2,
	dk_po_fungicide3,
	dk_po_fungicide4,
	dk_po_fungicide5,
	dk_po_fungicide6,
	dk_po_fungicide7,
	dk_po_fungicide8,
	dk_po_insecticide1,
	dk_po_harrow2,
	dk_po_harrow3,
	dk_po_herbicide7,
	dk_po_herbicide8,
	dk_po_foobar,
} DK_PotatoToDo;



class DK_Potato : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_Potato(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 12); // 
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_po_foobar - DK_PO_BASE);
		m_base_elements_no = DK_PO_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_po_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_po_harvest = DK_PO_BASE,
			  fmc_Others,	//	dk_po_remove_straw,
			  fmc_Cultivation,	//dk_po_stoneburier,
			  fmc_Cultivation,	//dk_po_autumn_plough,
			  fmc_Cultivation,	//	dk_po_deep_harrow,
			  fmc_Fertilizer,	//	dk_po_ferti_s1,
			  fmc_Fertilizer,	//		dk_po_ferti_p1,
			  fmc_Fertilizer,	//		dk_po_ferti_s2,
			  fmc_Fertilizer,	//		dk_po_ferti_p2,
			  fmc_Others,	//		dk_po_sow,
			  fmc_Watering,	//		dk_po_water,
			  fmc_Cultivation,	//		dk_po_strigling,
			  fmc_Cultivation,	//		dk_po_harrow1,
			  fmc_Cultivation,	//		dk_po_hill_up1,
			  fmc_Cultivation,	//		dk_po_hill_up2,
			  fmc_Cultivation,	//		dk_po_hill_up3,
			  fmc_Herbicide,	//		dk_po_herbicide,
			  fmc_Herbicide,	//		dk_po_herbicide_mw,
			  fmc_Herbicide,	//		dk_po_herbicide1,
			  fmc_Herbicide,	//		dk_po_herbicide2,
			  fmc_Herbicide,	//	dk_po_herbicide3,
			  fmc_Herbicide,	//	dk_po_herbicide4,
			  fmc_Herbicide,	//	dk_po_herbicide5,
			  fmc_Herbicide,	//	dk_po_herbicide6,
			  fmc_Fertilizer,	//	dk_po_ferti_s3,
			  fmc_Fertilizer,	//	dk_po_ferti_p3,
			  fmc_Fungicide,	//	dk_po_fungicide1,
			  fmc_Fungicide,	//	dk_po_fungicide2,
			  fmc_Fungicide,	//	dk_po_fungicide3,
			  fmc_Fungicide,	//	dk_po_fungicide4,
			  fmc_Fungicide,	//	dk_po_fungicide5,
			  fmc_Fungicide,	//	dk_po_fungicide6,
			  fmc_Fungicide,	//	dk_po_fungicide7,
			  fmc_Fungicide,	//	dk_po_fungicide8,
			  fmc_Insecticide,	//	dk_po_insecticide1,
			  fmc_Cultivation,	//	dk_po_harrow2,
			  fmc_Cultivation,	//	dk_po_harrow3,
			  fmc_Herbicide,	//	dk_po_herbicide7,
			  fmc_Herbicide,	//	dk_po_herbicide8,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_Potato_h

