/**
\file
\brief
<B>DK_OGrassGrazed_Perm.h This file contains the source for the DK_OGrassGrazed_Perm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of May 2022 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OGrassGrazed_Perm.h
//


#ifndef DK_OGRASSGRAZED_P_H
#define DK_OGRASSGRAZED_P_H

#define DK_OGGP_MANURE_S a_field->m_user[0]
#define DK_OGGP_MANURE_P a_field->m_user[1]
#define DK_OGGP_GRAZE a_field->m_user[1]

#define DK_OGGP_BASE 67900
/**

*/

/** Below is the list of things that a farmer can do if he is growing OGrassgrazed, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_oggp_start = 1, // Compulsory, must always be 1 (one).
	dk_oggp_sleep_all_day = DK_OGGP_BASE,
	dk_oggp_manure_s1,
	dk_oggp_manure_p1,
	dk_oggp_manure_s2,
	dk_oggp_manure_p2,
	dk_oggp_cutting1,
	dk_oggp_cutting2,
	dk_oggp_cutting3,
	dk_oggp_cutting4,
	dk_oggp_cutting5,
	dk_oggp_cutting6,
	dk_oggp_cutting_graze,
	dk_oggp_straw_chopping1,
	dk_oggp_straw_chopping2,
	dk_oggp_straw_chopping3,
	dk_oggp_straw_chopping4,
	dk_oggp_straw_chopping5,
	dk_oggp_straw_chopping6,
	dk_oggp_straw_chopping_graze,
	dk_oggp_grazing1,
	dk_oggp_grazing2,
	dk_oggp_cattle_out1,
	dk_oggp_cattle_out2,
	dk_oggp_water1,
	dk_oggp_water2,
	dk_oggp_wait,
	dk_oggp_foobar,
} DK_OGrassGrazed_PermToDo;


/**
\brief
DK_OGrassGrazed_Perm class
\n
*/
/**
See DK_OGrassGrazed_Perm.h::DK_OGrassGrazed_PermToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OGrassGrazed_Perm : public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OGrassGrazed_Perm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31, 8);
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_oggp_foobar - DK_OGGP_BASE);
	   m_base_elements_no = DK_OGGP_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_oggp_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_oggp_sleep_all_day = DK_oggp_BASE,
			fmc_Fertilizer, //dk_oggp_manure_s1,
			fmc_Fertilizer, //dk_oggp_manure_p1,
			fmc_Fertilizer, //dk_oggp_manure_s2,
			fmc_Fertilizer, //dk_oggp_manure_p2,
			fmc_Cutting, //dk_oggp_cutting1,
			fmc_Cutting, //dk_oggp_cutting2,
			fmc_Cutting, //dk_oggp_cutting3,
			fmc_Cutting, //dk_oggp_cutting4,
			fmc_Cutting, //dk_oggp_cutting5,
			fmc_Cutting, //dk_oggp_cutting6,
			fmc_Cutting, //dk_oggp_cutting_graze,
			fmc_Cutting, //dk_oggp_straw_chopping1,
			fmc_Cutting, //dk_oggp_straw_chopping2,
			fmc_Cutting, //dk_oggp_straw_chopping3,
			fmc_Cutting, //dk_oggp_straw_chopping4,
			fmc_Cutting, //dk_oggp_straw_chopping5,
			fmc_Cutting, //dk_oggp_straw_chopping6,
			fmc_Cutting, //dk_oggp_straw_chopping_graze,
			fmc_Grazing, //dk_oggp_grazing1,
			fmc_Grazing, //dk_oggp_grazing2,
			fmc_Grazing, //dk_oggp_cattle_out1,
			fmc_Grazing, //dk_oggp_cattle_out2,
			fmc_Watering, // dk_oggp_water1,
			fmc_Watering, // dk_oggp_water2,
			fmc_Others, //dk_oggp_wait
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OGrassGrazed_Perm_H

