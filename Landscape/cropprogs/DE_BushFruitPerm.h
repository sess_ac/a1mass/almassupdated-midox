/**
\file
\brief
<B>DE_BushFruitPerm.h This file contains the source for the DE_BushFruitPerm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2022 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DE_BushFruitPerm.h
//


#ifndef DE_BUSHFRUIT_P_H
#define DE_BUSHFRUIT_P_H

#define DE_BFP_EARLY_HARVEST a_field->m_user[1]

#define DE_BFP_BASE 39400
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_bfp_start = 1, // Compulsory, must always be 1 (one).
	de_bfp_sleep_all_day = DE_BFP_BASE,
	de_bfp_cover_on,
	de_bfp_fertilizer1_s,
	de_bfp_fertilizer1_p,
	de_bfp_mow_margin1,
	de_bfp_mow_margin2,
	de_bfp_mow_margin3,
	de_bfp_water1,
	de_bfp_herbicide1,
	de_bfp_manual_weeding1,
	de_bfp_manual_weeding2,
	de_bfp_insecticide1,	
	de_bfp_insecticide2,
	de_bfp_insecticide3,
	de_bfp_insecticide4,
	de_bfp_insecticide5,
	de_bfp_insecticide6,
	de_bfp_insecticide7,
	de_bfp_insecticide8,
	de_bfp_fungicide1,
	de_bfp_fungicide2,
	de_bfp_fungicide3,
	de_bfp_fungicide4,
	de_bfp_fungicide5,
	de_bfp_fungicide6,
	de_bfp_fungicide7,
	de_bfp_fungicide8,
	de_bfp_fungicide9,
	de_bfp_water2,
	de_bfp_harvest,
	de_bfp_mowing_fruit,
	de_bfp_herbicide2,
	de_bfp_fertilizer2_s,
	de_bfp_fertilizer2_p,
	de_bfp_water3,
	de_bfp_herbicide3,
	de_bfp_herbicide4,
	de_bfp_herbicide5,
	de_bfp_herbicide6,
	de_bfp_water4,
	de_bfp_water5,
	de_bfp_foobar,
} DE_BushFruitPermToDo;


/**
\brief
DE_BushFruitPerm class
\n
*/
/**
See DE_BushFruitPerm.h::DE_BushFruitPermToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_BushFruitPerm: public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_BushFruitPerm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_bfp_foobar - DE_BFP_BASE);
	   m_base_elements_no = DE_BFP_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	de_bfp_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	de_bfp_sleep_all_day = DK_BFP2_BASE,
			fmc_Others,	//	de_bfp_cover_on,
			fmc_Fertilizer,	//	de_bfp_fertilizer1_s,
			fmc_Fertilizer,	//	de_bfp_fertilizer1_p,
			fmc_Cutting, // de_bfp_mow_margin1,
			fmc_Cutting, // de_bfp_mow_margin2,
			fmc_Cutting, // de_bfp_mow_margin3,
			fmc_Watering,	//	de_bfp_water1,
			fmc_Herbicide,	//	de_bfp_herbicide1,
			fmc_Cultivation, // de_bfp_manual_weeding1,
			fmc_Cultivation, // de_bfp_manual_weeding2,
			fmc_Insecticide,	//	de_bfp_insecticide,
			fmc_Insecticide,	//	de_bfp_insecticide,
			fmc_Insecticide,	//	de_bfp_insecticide,
			fmc_Insecticide,	//	de_bfp_insecticide,
			fmc_Insecticide,	//	de_bfp_insecticide,
			fmc_Insecticide,	//	de_bfp_insecticide,
			fmc_Insecticide,	//	de_bfp_insecticide,
			fmc_Insecticide,	//	de_bfp_insecticide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Fungicide,	//	de_bfp_fungicide,
			fmc_Watering,	//	de_bfp_water2,
			fmc_Harvest,	//	de_bfp_harvest,
			fmc_Cutting, // de_bfp_mowing_fruit,
			fmc_Herbicide,	//	de_bfp_herbicide2,
			fmc_Fertilizer,	//	de_bfp_fertilizer2_s,
			fmc_Fertilizer,	//	de_bfp_fertilizer2_p,
			fmc_Watering,	//	de_bfp_water3,
			fmc_Herbicide,	//	de_bfp_herbicide3,
			fmc_Herbicide,	//	de_bfp_herbicide4,
		   fmc_Herbicide,	//	de_bfp_herbicide5,
		   fmc_Herbicide,	//	de_bfp_herbicide6,
		   fmc_Watering,	//	de_bfp_water4,
		   fmc_Watering,	//	de_bfp_water5,

				// no foobar entry			

	   };
	   // Iterate over the catlist elements and copy them to vector						
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};


#endif // DE_BushFruitPerm_H

