//
// DE_OOats.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DE_OOATS_H
#define DE_OOATS_H

#define DE_OOATS_BASE 37300

#define DE_OO_SOW_DATE          m_field->m_user[0]
#define DE_OO_DID_STRIGLING_ONE m_field->m_user[1]

typedef enum {
  de_oo_start = 1, // Compulsory, start event must always be 1 (one).
  de_oo_sleep_all_day = DE_OOATS_BASE,
  de_oo_autumn_plough,
  de_oo_fertmanure_stock,
  de_oo_spring_plough,
  de_oo_spring_harrow,
  de_oo_fertslurry_stock,
  de_oo_spring_sow,
  de_oo_spring_roll,
  de_oo_strigling_one,
  de_oo_strigling_two,
  de_oo_strigling_three,
  de_oo_harvest,
  de_oo_straw_chopping,
  de_oo_hay_bailing,
  de_oo_foobar,
// --FN--
} DEOOToDo;



class DE_OOats: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OOats(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(30,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_oo_foobar - DE_OOATS_BASE);
	  m_base_elements_no = DE_OOATS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_oo_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others, //   de_oo_sleep_all_day = DE_OOATS_BASE, 
			fmc_Cultivation,	//	  de_oo_autumn_plough = DE_OOATS_BASE,
			fmc_Fertilizer,	//	  de_oo_fertmanure_stock,
			fmc_Cultivation,	//	  de_oo_spring_plough,
			fmc_Cultivation,	//	  de_oo_spring_harrow,
			fmc_Fertilizer,	//	  de_oo_fertslurry_stock,
			fmc_Others,	//	  de_oo_spring_sow,
			fmc_Cultivation,	//	  de_oo_spring_roll,
			fmc_Cultivation,	//	  de_oo_strigling_one,
			fmc_Cultivation,	//	  de_oo_strigling_two,
			fmc_Cultivation,	//	  de_oo_strigling_three,
			fmc_Harvest,	//	  de_oo_harvest,
			fmc_Cutting,	//	  de_oo_straw_chopping,
			fmc_Others,	//	  de_oo_hay_bailing,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DE_OOats_h
