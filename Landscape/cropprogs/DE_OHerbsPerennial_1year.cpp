/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_OHerbsPerennial_1year.cpp This file contains the source for the DE_OHerbsPerennial_1year class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
*/
//
// DE_OHerbsPerennial_1year.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OHerbsPerennial_1year.h"


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DE_OHerbsPerennial_1year::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DEOHerbsPerennial_1year;

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_ohp1y_start:
	{
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(30, 11); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(30, 11); // last possible day of cutting2 - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;
		flexdates[1][1] = g_date->DayInYear(30, 11); // last possible day of grazing

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_ohp1y_spring_harrow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 10) + isSpring;
		// OK, let's go.
		if (m_ev->m_forcespring) {
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_ohp1y_ferti_s1, false, m_farm, m_field);
				break;
			}
			else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_ohp1y_ferti_p1, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->Date() + 1, de_ohp1y_winter_plough, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	// For now all the operations are done by 100% farmers
	case de_ohp1y_winter_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(5, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_winter_plough, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_ohp1y_ferti_s1, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_ohp1y_ferti_p1, false, m_farm, m_field);
		break;

	case de_ohp1y_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_ohp1y_spring_harrow, false, m_farm, m_field);
		break;

	case de_ohp1y_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_ohp1y_spring_harrow, false, m_farm, m_field);
		break;

	case de_ohp1y_spring_harrow:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_spring_harrow, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(25, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(25, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_ohp1y_ferti_s2, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(d1, de_ohp1y_ferti_p2, false, m_farm, m_field);
		break;

	case de_ohp1y_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FA_N(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_ohp1y_preseeding_cultivation, false, m_farm, m_field);
		break;

	case de_ohp1y_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FP_N(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_ohp1y_preseeding_cultivation, false, m_farm, m_field);
		break;

	case de_ohp1y_preseeding_cultivation:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(11, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_preseeding_cultivation, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_ohp1y_planting, false, m_farm, m_field);
		break;

	case de_ohp1y_planting:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ohp1y_planting, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, de_ohp1y_weeding, false, m_farm, m_field);
		break;

	case de_ohp1y_weeding:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->CutWeeds(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_weeding, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 5), de_ohp1y_ferti_s3, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 5), de_ohp1y_ferti_p3, false, m_farm, m_field);
		break;

	case de_ohp1y_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FA_N(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), de_ohp1y_harvest, false, m_farm, m_field);
		break;

	case de_ohp1y_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FP_N(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), de_ohp1y_harvest, false, m_farm, m_field);
		break;

	case de_ohp1y_harvest:
		if (!m_farm->GreenHarvest(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ohp1y_harvest, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 5, de_ohp1y_ferti_s4, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->Date() + 5, de_ohp1y_ferti_p4, false, m_farm, m_field);
		break;

	case de_ohp1y_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FA_N(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), de_ohp1y_harrow, false, m_farm, m_field);
		break;

	case de_ohp1y_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FP_N(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), de_ohp1y_harrow, false, m_farm, m_field);
		break;

	case de_ohp1y_harrow:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ohp1y_harrow, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "DE_OHerbsPerennial_1year::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}