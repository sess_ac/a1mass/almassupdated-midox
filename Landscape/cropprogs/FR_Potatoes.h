/**
\file
\brief
<B>FR_Potatoes.h This file contains the headers for the FR_Potatoes class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of September 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n


Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FR_POTATOES_H
#define FR_POTATOES_H

#define FR_P_PLOUGH	a_field->m_user[1]
#define FR_P_SLURRY	a_field->m_user[2]

#define FR_P_BASE 191200
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	fr_p_start = 1, // Compulsory, must always be 1 (one).
	fr_p_sleep_all_day = FR_P_BASE,
	fr_p_stubble_harrow,
	fr_p_stubble_plough,
	fr_p_pk,
	fr_p_herbicide1,
	fr_p_fertilizer,
	fr_p_subsoiler,
	fr_p_spring_plough,
	fr_p_sow,
	fr_p_n1,
	fr_p_strigling_hill1,
	fr_p_n2,
	fr_p_strigling_hill2,
	fr_p_herbicide2,
	fr_p_herbicide3,
	fr_p_fungicide1,
	fr_p_fungicide2,
	fr_p_fungicide3,
	fr_p_fungicide4,
	fr_p_fungicide5,
	fr_p_fungicide6,
	fr_p_fungicide7,
	fr_p_fungicide8,
	fr_p_fungicide9,
	fr_p_fungicide10,
	fr_p_fungicide11,
	fr_p_fungicide12,
	fr_p_insecticide1,
	fr_p_insecticide2,
	fr_p_herbicide4,
	fr_p_swathing,
	fr_p_harvest,
	fr_p_foobar,
} FR_PotatoesToDo;


/**
\brief
FR_Potatoes class
\n
*/
/**
See FR_Potatoes.h::FR_PotatoesToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class FR_Potatoes : public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   FR_Potatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,12 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (fr_p_foobar - FR_P_BASE);
	   m_base_elements_no = FR_P_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others, //fr_m_start = 1, // Compulsory, must always be 1 (one).
	fmc_Others, //fr_m_sleep_all_day = FR_P_BASE,
	fmc_Cultivation, //fr_p_stubble_harrow,
	fmc_Cultivation, //fr_p_stubble_plough,
	fmc_Fertilizer, //fr_p_pk,
	fmc_Herbicide, //fr_p_herbicide1,
	fmc_Fertilizer, //fr_p_fertilizer,
	fmc_Cultivation, //fr_p_subsoiler,
	fmc_Cultivation, //fr_p_spring_plough,
	fmc_Others, //fr_p_sow,
	fmc_Fertilizer, //fr_p_n1,
	fmc_Cultivation, //fr_p_strigling_hill1,
	fmc_Fertilizer, //fr_p_n2,
	fmc_Cultivation, //r_p_strigling_hill2,
	fmc_Herbicide, //fr_p_herbicide2,
	fmc_Herbicide, //fr_p_herbicide3,
	fmc_Fungicide, //fr_p_fungicide1,
	fmc_Fungicide, //fr_p_fungicide2,
	fmc_Fungicide, //fr_p_fungicide3,
	fmc_Fungicide, //fr_p_fungicide4,
	fmc_Fungicide, //fr_p_fungicide5,
	fmc_Fungicide, //fr_p_fungicide6,
	fmc_Fungicide, //fr_p_fungicide7,
	fmc_Fungicide, //fr_p_fungicide8,
	fmc_Fungicide, //fr_p_fungicide9,
	fmc_Fungicide, //fr_p_fungicide10,
	fmc_Fungicide, //fr_p_fungicide11,
	fmc_Fungicide, //fr_p_fungicide12,
	fmc_Insecticide, //fr_p_insecticide1,
	fmc_Insecticide, //fr_p_insecticide2,
	fmc_Herbicide, //fr_p_herbicide4,
	fmc_Cutting, //fr_p_swathing,
	fmc_Harvest, //fr_p_harvest,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // FR_POTATOES_H

