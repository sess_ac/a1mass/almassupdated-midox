//
// FI_OStarchPotato_South.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FI_OStarchPotato_South_h
#define FI_OStarchPotato_South_h

#define FI_OSPS_BASE 70600

#define FI_OSPS_DECIDE_TO_HERB		m_field->m_user[5]
#define FI_OSPS_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  fi_osps_start = 1, // Compulsory, start event must always be 1 (one).
  fi_osps_harvest = FI_OSPS_BASE,
  fi_osps_stubble_cultivator,
  fi_osps_autumn_plough,
  fi_osps_slurry,
  fi_osps_spring_plough,
  fi_osps_n_minerals1,
  fi_osps_n_minerals2,
  fi_osps_preseeding_cultivation,
  fi_osps_fertilizer,
  fi_osps_preseeding_plant,
  fi_osps_plant,
  fi_osps_harrow,
  fi_osps_foobar,
} FI_OStarchPotato_SouthToDo;



class FI_OStarchPotato_South: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FI_OStarchPotato_South(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(1,12); //
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (fi_osps_foobar - FI_OSPS_BASE);
	  m_base_elements_no = FI_OSPS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fi_osps_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  fi_osps_harvest = FI_OSPS_BASE,
			fmc_Cultivation,	//	  fi_osps_stubble_cultivator,
			fmc_Cultivation,	//	  fi_osps_autumn_plough,
			fmc_Fertilizer,	//	  fi_osps_slurry,
			fmc_Cultivation,	//	  fi_osps_spring_plough,
			fmc_Fertilizer,	//	  fi_osps_n_minerals1,
			fmc_Fertilizer,	//	  fi_osps_n_minerals2,
			fmc_Cultivation,	//	  fi_osps_preseeding_cultivation,
			fmc_Fertilizer,	//	  fi_osps_fertilizer,
			fmc_Cultivation,	//	  fi_osps_preseeding_plant,
			fmc_Others,	//	  fi_osps_plant,
			fmc_Cultivation	//	  fi_osps_harrow,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // FI_OStarchPotato_South_h
