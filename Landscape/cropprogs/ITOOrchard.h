/**
\file
\brief
<B>ITOOrchard.h This file contains the headers for the ITOOrchard class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of March 2022 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// ITOOrchard.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef ITOORCHARD_H
#define ITOORCHARD_H

#define IT_OO_EARLY a_field->m_user[1]
#define IT_OO_MID a_field->m_user[2]
#define IT_OO_LATE a_field->m_user[3]

#define IT_OO_BASE 50200
/**
\
*/

/** Below is the list of things that a farmer can do if he is growing ITOOrchard, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	it_oo_start = 1, // Compulsory, must always be 1 (one).
	it_oo_sleep_all_day = IT_OO_BASE,
	it_oo_winter_pruning,
	it_oo_ferti_s1,
	it_oo_ferti_p1,
	it_oo_irrigation_early,
	it_oo_irrigation_mid,
	it_oo_irrigation_late,
	it_oo_fungi_1,
	it_oo_fungi_2,
	it_oo_fungi_3,
	it_oo_fungi_4,
	it_oo_fungi_5,
	it_oo_fungi_6,
	it_oo_fungi_7,
	it_oo_fungi_8,
	it_oo_fungi_9,
	it_oo_fungi_10,
	it_oo_fungi_11,
	it_oo_fungi_12,
	it_oo_fungi_13,
	it_oo_fungi_14,
	it_oo_fungi_15,
	it_oo_fungi_16,
	it_oo_fungi_17,
	it_oo_fungi_18,
	it_oo_fungi_19,
	it_oo_fungi_20,
	it_oo_fungi_21,
	it_oo_fungi_22,
	it_oo_fungi_23,
	it_oo_fungi_24,
	it_oo_fungi_25,
	it_oo_fungi_26,
	it_oo_fungi_27,
	it_oo_fungi_28,
	it_oo_fungi_29,
	it_oo_fungi_30,
	it_oo_fungi_31,
	it_oo_fungi_32,
	it_oo_fungi_33,
	it_oo_fungi_34,
	it_oo_insecti_1,
	it_oo_insecti_2,
	it_oo_insecti_3,
	it_oo_insecti_4,
	it_oo_insecti_5,
	it_oo_insecti_6,
	it_oo_foliar_feed_s1,
	it_oo_foliar_feed_s2,
	it_oo_foliar_feed_s3,
	it_oo_foliar_feed_s4,
	it_oo_foliar_feed_s5,
	it_oo_foliar_feed_s6,
	it_oo_foliar_feed_s7,
	it_oo_foliar_feed_s8,
	it_oo_foliar_feed_s9,
	it_oo_foliar_feed_s10,
	it_oo_foliar_feed_p1,
	it_oo_foliar_feed_p2,
	it_oo_foliar_feed_p3,
	it_oo_foliar_feed_p4,
	it_oo_foliar_feed_p5,
	it_oo_foliar_feed_p6,
	it_oo_foliar_feed_p7,
	it_oo_foliar_feed_p8,
	it_oo_foliar_feed_p9,
	it_oo_foliar_feed_p10,
	it_oo_GR_russet_1,
	it_oo_GR_russet_2,
	it_oo_mechanical_thin_1,
	it_oo_mechanical_thin_2,
	it_oo_mechanical_thin_3,
	it_oo_GR_growth_inhib,
	it_oo_weed_mowing_1,
	it_oo_weed_mowing_2,
	it_oo_weed_mowing_3,
	it_oo_weed_mowing_4,
	it_oo_DI_continued_early,
	it_oo_DI_continued_mid,
	it_oo_DI_continued_late,
	it_oo_manual_thin,
	it_oo_harvest_early1,
	it_oo_harvest_early2,
	it_oo_harvest_mid1,
	it_oo_harvest_mid2,
	it_oo_harvest_late1,
	it_oo_harvest_late2,
	it_oo_summer_pruning,
	it_oo_foobar
} ITOOrchardToDo;


/**
\brief
ITOOrchardd class
\n
*/
/**
See ITOOrchard.h::ITOOrchardToDo for a complete list of all possible events triggered codes by the ITOOrchard management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class ITOOrchard : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	ITOOrchard(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 1);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (it_oo_foobar - IT_OO_BASE);
		m_base_elements_no = IT_OO_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	it_oo_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	it_oo_sleep_all_day = IT_O_BASE,
			fmc_Cutting,	//	it_oo_winter_pruning,
			fmc_Fertilizer,	//	it_oo_ferti_s1,
			fmc_Fertilizer,	//	it_oo_ferti_p1,
			fmc_Watering,	//	it_oo_irrigation,
			fmc_Watering,	//	it_oo_irrigation,
			fmc_Watering,	//	it_oo_irrigation,
			fmc_Fungicide,	//	it_oo_fungi_1,
			fmc_Fungicide,	//	it_oo_fungi_2,
			fmc_Fungicide,	//	it_oo_fungi_3,
			fmc_Fungicide,	//	it_oo_fungi_4,
			fmc_Fungicide,	//	it_oo_fungi_5,
			fmc_Fungicide,	//	it_oo_fungi_6,
			fmc_Fungicide,	//	it_oo_fungi_7,
			fmc_Fungicide,	//	it_oo_fungi_8,
			fmc_Fungicide,	//	it_oo_fungi_9,
			fmc_Fungicide,	//	it_oo_fungi_10,
			fmc_Fungicide,	//	it_oo_fungi_11,
			fmc_Fungicide,	//	it_oo_fungi_12,
			fmc_Fungicide,	//	it_oo_fungi_13,
			fmc_Fungicide,	//	it_oo_fungi_14,
			fmc_Fungicide,	//	it_oo_fungi_15,
			fmc_Fungicide,	//	it_oo_fungi_16,
			fmc_Fungicide,	//	it_oo_fungi_17,
			fmc_Fungicide,	//	it_oo_fungi_18,
			fmc_Fungicide,	//	it_oo_fungi_19,
			fmc_Fungicide,	//	it_oo_fungi_20,
			fmc_Fungicide,	//	it_oo_fungi_21,
			fmc_Fungicide,	//	it_oo_fungi_22,
			fmc_Fungicide,	//	it_oo_fungi_23,
			fmc_Fungicide,	//	it_oo_fungi_24,
			fmc_Fungicide,	//	it_oo_fungi_25,
			fmc_Fungicide,	//	it_oo_fungi_26,
			fmc_Fungicide,	//	it_oo_fungi_27,
			fmc_Fungicide,	//	it_oo_fungi_28,
			fmc_Fungicide,	//	it_oo_fungi_29,
			fmc_Fungicide,	//	it_oo_fungi_30,
			fmc_Fungicide,	//	it_oo_fungi_31,
			fmc_Fungicide,	//	it_oo_fungi_32,
			fmc_Fungicide,	//	it_oo_fungi_33,
			fmc_Fungicide,	//	it_oo_fungi_34,
			fmc_Insecticide,	//	it_oo_insecti_1,
			fmc_Insecticide,	//	it_oo_insecti_2,
			fmc_Insecticide,	//	it_oo_insecti_3,
			fmc_Insecticide,	//	it_oo_insecti_4,
			fmc_Insecticide,	//	it_oo_insecti_5,
			fmc_Insecticide,	//	it_oo_insecti_6,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s1,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s2,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s3,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s4,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s5,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s6,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s7,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s8,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s9,
			fmc_Fertilizer,	//	it_oo_foliar_feed_s10,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p1,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p2,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p3,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p4,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p5,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p6,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p7,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p8,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p9,
			fmc_Fertilizer,	//	it_oo_foliar_feed_p10,
			fmc_Others,	//	it_oo_GR_russet_1,
			fmc_Others,	//	it_oo_GR_russet_2,
			fmc_Cutting,	//	it_oo_mechanical_thin_1,
			fmc_Cutting,	//	it_oo_mechanical_thin_2,
			fmc_Cutting,	//	it_oo_mechanical_thin_3,
			fmc_Others, // it_oo_GR_growth_inhib,
			fmc_Cutting,	//	it_oo_weed_mowing_1,
			fmc_Cutting,	//	it_oo_weed_mowing_2,
			fmc_Cutting,	//	it_oo_weed_mowing_3,
			fmc_Cutting,	//	it_oo_weed_mowing_4,
			fmc_Watering,	//	it_oo_DI_continued_early,
			fmc_Watering,	//	it_oo_DI_continued_mid,
			fmc_Watering,	//	it_oo_DI_continued_late,
			fmc_Cutting,	//	it_oo_manual_thin, 
			fmc_Harvest,	//	it_oo_harvest_early1,
			fmc_Harvest,	//	it_oo_harvest_early2,
			fmc_Harvest,	//	it_oo_harvest_mid1,
			fmc_Harvest,	//	it_oo_harvest_mid2,
			fmc_Harvest,	//	it_oo_harvest_late1,
			fmc_Harvest,	//	it_oo_harvest_late2,
			fmc_Cutting,	//	it_oo_summer_pruning,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // ITOOrchard_H
