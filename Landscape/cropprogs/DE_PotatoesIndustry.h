/**
\file
\brief
<B>DE_PotatoesIndustry.h This file contains the headers for the DE_PotatoesIndustry class</B> \n
*/
/**
\file 
 by Chris J. Topping and Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_PotatoesIndustry.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

//EZ: This is based on potatoes for consumption but harvest was moved + 1 month and there are 3 instead of 1 insecticide applications and done by all farmers;
//other pesticides has not been changed

#ifndef DE_POTATOESINDUSTRY_H
#define DE_POTATOESINDUSTRY_H

#define DE_POTATOESINDUSTRY_BASE 38600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DE_POTIND_FERTI_P1		a_field->m_user[1]
#define DE_POTIND_FERTI_S1		a_field->m_user[2]
#define DE_POTIND_STUBBLE_PLOUGH	a_field->m_user[3]
#define DE_POTIND_DID_STRIG1    a_field->m_user[4]
#define DE_POTIND_HILL_DATE   a_field->m_user[7]
#define DE_POTIND_DID_HILL    a_field->m_user[8]
#define DE_POTIND_DID_DESS    a_field->m_user[9]


/** Below is the list of things that a farmer can do if he is growing DEPotatoesIndustry, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_potind_start = 1, // Compulsory, must always be 1 (one).
	de_potind_sleep_all_day = DE_POTATOESINDUSTRY_BASE,
	de_potind_stubble_harrow,
	de_potind_autumn_harrow1,
	de_potind_autumn_harrow2,
	de_potind_stubble_plough,
	de_potind_winter_plough,
	de_potind_spring_harrow,
	de_potind_ferti_s1,
	de_potind_ferti_p1,
	de_potind_ferti_s2,
	de_potind_ferti_p2,
	de_potind_ferti_p3,
	de_potind_ferti_s3,
	de_potind_spring_harrow2,
	de_potind_ferti_p4,
	de_potind_ferti_s4,
	de_potind_spring_plough,
	de_potind_bed_forming,
	de_potind_spring_planting,
	de_potind_hilling1,
	de_potind_hilling2,
	de_potind_hilling3,
	de_potind_herbicide1,
	de_potind_hilling4,
	de_potind_hilling5,
	de_potind_hilling6,
	de_potind_herbicide2,
	de_potind_ferti_p5,
	de_potind_ferti_s5,
	de_potind_ferti_p6,
	de_potind_ferti_s6,
	de_potind_fungicide1,
	de_potind_fungicide2,
	de_potind_fungicide3,
	de_potind_fungicide4,
	de_potind_insecticide1,
	de_potind_insecticide2,
	de_potind_insecticide3,
	de_potind_dessication1,
	de_potind_dessication2,
	de_potind_harvest,
	de_potind_ferti_p7,
	de_potind_ferti_s7,
	de_potind_foobar, // Obligatory, must be last
} DE_PotatoesIndustryToDo;


/**
\brief
DE_PotatoesIndustry class
\n
*/
/**
See DE_PotatoesIndustry.h::DE_PotatoesIndustryToDo for a complete list of all possible events triggered codes by the PotatoesIndustry management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_PotatoesIndustry: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_PotatoesIndustry(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of 
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date=g_date->DayInYear( 10,11 ); //EZ: This needs to be changed
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_potind_foobar - DE_POTATOESINDUSTRY_BASE);
	   m_base_elements_no = DE_POTATOESINDUSTRY_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			// ALL THE NECESSARY ENTRIES HERE
			fmc_Others,			//	  de_potind_start = 1, // Compulsory, must always be 1 (one)
			fmc_Others,			//	  de_potind_sleep_all_day
			fmc_Cultivation,	//	  de_potind_stubble_harrow,
			fmc_Cultivation,	//	  de_potind_autumn_harrow1,
			fmc_Cultivation,	//	  de_potind_autumn_harrow2,
			fmc_Cultivation,	//	  de_potind_stubble_plough,
			fmc_Cultivation,	//	  de_potind_winter_plough,
			fmc_Cultivation,	//	  de_potind_spring_harrow
			fmc_Fertilizer,		//	  de_potind_ferti_s1
			fmc_Fertilizer,		//	  de_potind_ferti_p1,
			fmc_Fertilizer,		//	  de_potind_ferti_s2,
			fmc_Fertilizer,		//	  de_potind_ferti_p2,
			fmc_Fertilizer,		//	  de_potind_ferti_p4,
			fmc_Fertilizer,		//	  de_potind_ferti_s4,
			fmc_Cultivation,	//	  de_potind_spring_harrow2,
			fmc_Fertilizer,		//	  de_potind_ferti_p5,
			fmc_Fertilizer,		//	  de_potind_ferti_s5,
			fmc_Cultivation,	//	  de_potind_spring_plough,
			fmc_Cultivation,	//	  de_potind_bed_forming,
			fmc_Others,	//	  de_potind_spring_planting,
			fmc_Cultivation,	//	  de_potind_hilling1,
			fmc_Cultivation,	//	  de_potind_hilling2,
			fmc_Cultivation,	//	  de_potind_hilling3,
			fmc_Herbicide,	//	  de_potind_herbicide1,
			fmc_Cultivation,	//	  de_potind_hilling4,
			fmc_Cultivation,	//	  de_potind_hilling5,
			fmc_Cultivation,	//	  de_potind_hilling6,
			fmc_Herbicide,		//	de_potind_herbicide2,
			fmc_Fertilizer,		//	  de_potind_ferti_p6,
			fmc_Fertilizer,		//	  de_potind_ferti_s6,
			fmc_Fertilizer,		//	  de_potind_ferti_p7,
			fmc_Fertilizer,		//	  de_potind_ferti_s7,
			fmc_Fungicide,		//	de_potind_fungicide1,
			fmc_Fungicide,		//	de_potind_fungicide2,
			fmc_Fungicide,		//	de_potind_fungicide3,
			fmc_Fungicide,		//	de_potind_fungicide4,
			fmc_Insecticide,		//	de_potind_insecticide1,
			fmc_Insecticide,		//	de_potind_insecticide2,
			fmc_Insecticide,		//	de_potind_insecticide3,
			fmc_Others,		//	de_potind_dessication1,
			fmc_Others,		//	de_potind_dessication2,
			fmc_Harvest,		//	de_potind_harvest,
			fmc_Fertilizer,		//	  de_potind_ferti_p8,
			fmc_Fertilizer,		//	  de_potind_ferti_s8,
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DE_POTATOESINDUSTRY_H