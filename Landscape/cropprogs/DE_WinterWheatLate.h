/**
\file
\brief
<B>DE_WinterWheatLate.h This file contains the headers for the DE_WinterWheatLate class</B> \n
*/
/**
\file 
 by Chris J. Topping  \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_WinterWheatLate.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_WINTERWHEATLATE_H
#define DE_WINTERWHEATLATE_H

#define DE_WINTERWHEATLATE_BASE 38300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DE_WWL_AUTUMN_PLOUGH	a_field->m_user[1]
#define DE_WWL_FERTI_P1	a_field->m_user[2]
#define DE_WWL_FERTI_S1	a_field->m_user[3]
#define DE_WWL_DECIDE_TO_GR a_field->m_user[4]

/** Below is the list of things that a farmer can do if he is growing DEWinterWheatLate, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_wwl_start = 1, // Compulsory, must always be 1 (one).
	de_wwl_sleep_all_day = DE_WINTERWHEATLATE_BASE,
	de_wwl_autumn_harrow_notill,
	de_wwl_autumn_plough,
	de_wwl_autumn_roll,
	de_wwl_autumn_sow,
	de_wwl_stubble_cultivator,
	de_wwl_ferti_s1,
	de_wwl_ferti_p1,
	de_wwl_ferti_s2,
	de_wwl_ferti_p2,
	de_wwl_ferti_s3,
	de_wwl_ferti_p3,
	de_wwl_ferti_s4,
	de_wwl_ferti_p4,
	de_wwl_ferti_p5,
	de_wwl_ferti_s5,
	de_wwl_herbicide1,
	de_wwl_herbicide2,
	de_wwl_insecticide1,
	de_wwl_insecticide2,
	de_wwl_insecticide3,
	de_wwl_fungicide1,
	de_wwl_fungicide2,
	de_wwl_fungicide3,
	de_wwl_growth_regulator1,
	de_wwl_growth_regulator2,
	de_wwl_harvest,
	de_wwl_straw_chopping,
	de_wwl_hay_bailing,
	de_wwl_foobar, // Obligatory, must be last
} DE_WinterWheatLateToDo;


/**
\brief
DE_WinterWheatLate class
\n
*/
/**
See DE_WinterWheatLate.h::DE_WinterWheatLateToDo for a complete list of all possible events triggered codes by the WinterWheatLate management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_WinterWheatLate: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_WinterWheatLate(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of 
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date=g_date->DayInYear( 25,10 ); 
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_wwl_foobar - DE_WINTERWHEATLATE_BASE);
	   m_base_elements_no = DE_WINTERWHEATLATE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			// ALL THE NECESSARY ENTRIES HERE
			fmc_Others,	//	  de_car_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	  de_car_sleep_all_day
			fmc_Cultivation,	//	de_wwl_autumn_harrow_notill,
			fmc_Cultivation,	//	de_wwl_stubble_cultivator,
			fmc_Cultivation,	//	de_wwl_autumn_plough,
			fmc_Cultivation,	//	de_wwl_autumn_roll,
			fmc_Cultivation,	//	de_wwl_autumn_sow,
			fmc_Fertilizer,		//	de_wwl_ferti_s1,
			fmc_Fertilizer,		//	de_wwl_ferti_p1,
			fmc_Fertilizer,		//	de_wwl_ferti_s2,
			fmc_Fertilizer,		//	de_wwl_ferti_p2,
			fmc_Fertilizer,		//	de_wwl_ferti_s3,
			fmc_Fertilizer,		//	de_wwl_ferti_p3,
			fmc_Fertilizer,		//	de_wwl_ferti_s4,
			fmc_Fertilizer,		//	de_wwl_ferti_p4,
			fmc_Fertilizer,		//	de_wwl_ferti_p5,
			fmc_Fertilizer,		//	de_wwl_ferti_s5,
			fmc_Herbicide,	//	  de_wwl_herbicide1,
			fmc_Herbicide,	//	  de_wwl_herbicide2,
			fmc_Insecticide,	//	  de_wwl_insecticide1,
			fmc_Insecticide,	//	  de_wwl_insecticide2,
			fmc_Insecticide,	//	  de_wwl_insecticide3,
			fmc_Fungicide,	//	  de_wwl_fungicide1,
			fmc_Fungicide,	//	  de_wwl_fungicide2,
			fmc_Fungicide,	//	  de_wwl_fungicide3,
			fmc_Others,	//	  de_wwl_growth_regulator1,
			fmc_Others,	//	  de_wwl_growth_regulator2,
			fmc_Harvest,	//	  de_wwl_harvest,
			fmc_Others,	//	  de_wwl_straw_chopping,
			fmc_Others,	//	  de_wwl_hay_bailing,
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DE_WINTERWHEATLATE_H

