//
// DK_OMixedVeg.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OMixedVeg.h"



bool DK_OMixedVeg::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
  TTypesOfVegetation l_tov = tov_DKOMixedVeg;

  switch ( m_ev->m_todo ) {
  case dk_omv_start:
    {
      a_field->ClearManagementActionSum();
      DK_OMV_LATE_SOW = false;
      m_last_date = g_date->DayInYear(15, 10); // Should match the last flexdate below
          //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
              // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(15, 10); // last possible day 
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(15, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 365;
      if (StartUpCrop(isSpring, flexdates, int(dk_omv_ferti_s1))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(25, 2) + isSpring;
      // OK, let's go.
      SimpleEvent(d1, dk_omv_ferti_s1, false);
  }
    break;

  case dk_omv_ferti_s1:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) { // suggests that 50% of the field will receive fertilizer now (sowing soon - 50% sown later)
          if (a_farm->IsStockFarmer()) {
              if (!m_farm->FA_Manure(m_field, 0.0,
                  g_date->DayInYear(15, 3) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_omv_ferti_s1, true);
                  break;
              }
              SimpleEvent(g_date->Date() + 1, dk_omv_spring_plough, false);
              break;
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 2), dk_omv_ferti_p1, false);
          break;
      }
      DK_OMV_LATE_SOW = true; // remember who/where sowing late
      SimpleEvent(g_date->Date() + 1, dk_omv_spring_plough, false);
      break;      

  case dk_omv_ferti_p1:
          if (!m_farm->FP_Manure(m_field, 0.0,
              g_date->DayInYear(15, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_ferti_p1, true);
              break;
          }
      SimpleEvent(g_date->Date() + 1, dk_omv_spring_plough, false);
      break;

  case dk_omv_spring_plough:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->SpringPlough(m_field, 0.0,
              g_date->DayInYear(20, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_spring_plough, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_omv_spring_harrow, false);
      break;

  case dk_omv_spring_harrow:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->SpringHarrow(m_field, 0.0,
              g_date->DayInYear(21, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_spring_harrow, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_omv_spring_sow1, false);
      break;

  case dk_omv_spring_sow1:
      if (DK_OMV_LATE_SOW == false) { // suggests 50% of the field is sown now
          if (!m_farm->SpringSow(m_field, 0.0,
              g_date->DayInYear(22, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_spring_sow1, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), dk_omv_row_cultivation1, false);
      break;

  case dk_omv_row_cultivation1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(20, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_row_cultivation1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 5, dk_omv_slurry_s1, false);
      break;
  case dk_omv_slurry_s1:
      if (DK_OMV_LATE_SOW == false) { 
          if (a_farm->IsStockFarmer()) {
              if (!m_farm->FA_Slurry(m_field, 0.0,
                  g_date->DayInYear(5, 5) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_omv_slurry_s1, true);
                  break;
              }
              SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_omv_water1, false);
              break;
          }
          else SimpleEvent(g_date->Date(), dk_omv_slurry_p1, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_omv_row_cultivation2, false);
      break;

  case dk_omv_slurry_p1:
      if (DK_OMV_LATE_SOW == false) { 
          if (!m_farm->FP_Slurry(m_field, 0.0,
              g_date->DayInYear(5, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_slurry_p1, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_omv_water1, false);
      break;

  case dk_omv_water1:
      if (DK_OMV_LATE_SOW == false) { 
          if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
              if (!m_farm->Water(m_field, 0.0,
                  g_date->DayInYear(5, 5) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_omv_water1, true);
                  break;
              }
          }
      }
      SimpleEvent(g_date->Date()+2, dk_omv_row_cultivation2, false);
      break;

  case dk_omv_row_cultivation2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(8, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_row_cultivation2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date()+ 6, dk_omv_water2, false);
      break;

  case dk_omv_water2:
      if (DK_OMV_LATE_SOW == false) { 
          if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
              if (!m_farm->Water(m_field, 0.0,
                  g_date->DayInYear(15, 5) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_omv_water2, true);
                  break;
              }
          }
          SimpleEvent(g_date->Date() + 6, dk_omv_water3, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_omv_slurry_s2, false);
      break;

  case dk_omv_water3:
      if (DK_OMV_LATE_SOW == false) { 
          if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
              if (!m_farm->Water(m_field, 0.0,
                  g_date->DayInYear(22, 5) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_omv_water3, true);
                  break;
              }
          }
      }
      SimpleEvent(g_date->Date() + 4, dk_omv_slurry_s2, false);
      break;

  case dk_omv_slurry_s2:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Slurry(m_field, 0.0,
              g_date->DayInYear(27, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_slurry_s2, true);
              break;
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_omv_water4, false);
          break;
      }
      else SimpleEvent(g_date->Date(), dk_omv_slurry_p2, false);
      break;

  case dk_omv_slurry_p2:
      if (!m_farm->FP_Slurry(m_field, 0.0,
          g_date->DayInYear(27, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_omv_slurry_p2, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_omv_water4, false);
      break;

  case dk_omv_water4:
      if (DK_OMV_LATE_SOW == true) { 
          if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
              if (!m_farm->Water(m_field, 0.0,
                  g_date->DayInYear(31, 5) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_omv_water4, true);
                  break;
              }
          }
      }
      SimpleEvent(g_date->Date() + 2, dk_omv_row_cultivation3, false);
      break;

  case dk_omv_row_cultivation3:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(3, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_row_cultivation3, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_omv_spring_sow2, false);
      break;

  case dk_omv_spring_sow2:
      if (DK_OMV_LATE_SOW == true) { 
          if (!m_farm->SpringSow(m_field, 0.0,
              g_date->DayInYear(5, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_spring_sow2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date()+2, dk_omv_water5, false);
      break;

  case dk_omv_water5:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(8, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_water5, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 2, dk_omv_row_cultivation4, false);
      break;

  case dk_omv_row_cultivation4:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(15, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_row_cultivation4, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 6), dk_omv_insecticide1, false);
      break;

  case dk_omv_insecticide1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->OrganicInsecticide(m_field, 0.0,
              g_date->DayInYear(30, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_insecticide1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 5, dk_omv_insecticide2, false);
      break;

  case dk_omv_insecticide2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->OrganicInsecticide(m_field, 0.0,
              g_date->DayInYear(6, 7) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_insecticide2, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_omv_insecticide3, false);
      break;

  case dk_omv_insecticide3:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->OrganicInsecticide(m_field, 0.0,
              g_date->DayInYear(6, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_insecticide3, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 5, dk_omv_insecticide4, false);
      break;

  case dk_omv_insecticide4:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->OrganicInsecticide(m_field, 0.0,
              g_date->DayInYear(12, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_insecticide4, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), dk_omv_harvest1, false);
      break;

  case dk_omv_harvest1:
      if (DK_OMV_LATE_SOW == false) { 
          if (!m_farm->Harvest(m_field, 0.0,
              g_date->DayInYear(31, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_harvest1, true);
              break;
          }
          done = true;
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_omv_insecticide5, false);
      break;

  case dk_omv_insecticide5:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->OrganicInsecticide(m_field, 0.0,
              g_date->DayInYear(31, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_insecticide5, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() +1, dk_omv_row_cultivation5, false);
      break;

  case dk_omv_row_cultivation5:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(5, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_row_cultivation5, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 9), dk_omv_insecticide6, false);
      break;

  case dk_omv_insecticide6:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->OrganicInsecticide(m_field, 0.0,
              g_date->DayInYear(15, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_omv_insecticide6, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 10), dk_omv_harvest2, false);
      break;

  case dk_omv_harvest2:
      if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_omv_harvest2, true);
          break;
      }
          done = true;
          break;

  default:
    g_msg->Warn( WARN_BUG, "DK_OMixedVeg::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
  return done;
}


