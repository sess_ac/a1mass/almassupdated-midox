/**
\file
\brief
<B>UKTempGrass.h This file contains the headers for the TemporalUKTempGrass class</B> \n
*/
/**
\file
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKTempGrass.h
//


#ifndef UKTEMPGRASS_H
#define UKTEMPGRASS_H

#define UKTEMPGRASS_BASE 45600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define UK_TG_CUT_DATE		a_field->m_user[1]
#define UK_TG_WATER_DATE		a_field->m_user[2]


/** Below is the list of things that a farmer can do if he is growing TemporalUKTempGrass, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	uk_tg_start = 1, // Compulsory, must always be 1 (one).
	uk_tg_sleep_all_day = UKTEMPGRASS_BASE,
	uk_tg_preseeding_cultivator,
	uk_tg_spring_sow,
	uk_tg_ferti_p2,
	uk_tg_ferti_s2,
	uk_tg_cut_to_silage1,
	uk_tg_cut_to_silage2,
	uk_tg_cut_to_silage3,
	uk_tg_cut_to_silage4,
	uk_tg_cut_to_silage5,
	uk_tg_cut_to_silage6,
	uk_tg_ferti_p3,
	uk_tg_ferti_s3,
	uk_tg_ferti_p4,
	uk_tg_ferti_s4,
	uk_tg_ferti_p5,
	uk_tg_ferti_s5,
	uk_tg_ferti_p6,
	uk_tg_ferti_s6,
	uk_tg_ferti_p7,
	uk_tg_ferti_s7,
	uk_tg_ferti_p8,
	uk_tg_ferti_s8,
	uk_tg_ferti_p9,
	uk_tg_ferti_s9,
	uk_tg_ferti_p10,
	uk_tg_ferti_s10,
	uk_tg_ferti_p11,
	uk_tg_ferti_s11,
	uk_tg_ferti_p12,
	uk_tg_ferti_s12,
	uk_tg_watering,
	uk_tg_cattle_out,
	uk_tg_cattle_is_out,
	uk_tg_foobar,
} UKTempGrassToDo;


/**
\brief
UKTempGrass class
\n
*/
/**
See UKTempGrass.h::UKTempGrassToDo for a complete list of all possible events triggered codes by the TemporalUKTempGrass management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKTempGrass : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	UKTempGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st March
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (uk_tg_foobar - UKTEMPGRASS_BASE);
		m_base_elements_no = UKTEMPGRASS_BASE - 2;
		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others, // this needs to be at the zero line
			 fmc_Others, //uk_tg_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Others, //uk_tg_sleep_all_day = UKTEMPGRASS_BASE,
			 fmc_Cultivation,//uk_tg_preseeding_cultivator,
			 fmc_Others, //uk_tg_spring_sow,
			 fmc_Fertilizer,//uk_tg_ferti_p2,
			 fmc_Fertilizer,//uk_tg_ferti_s2,
			 fmc_Cutting,//uk_tg_cut_to_silage1,
			 fmc_Cutting,//uk_tg_cut_to_silage2,
			 fmc_Cutting,//uk_tg_cut_to_silage3,
			 fmc_Cutting,//uk_tg_cut_to_silage4,
			 fmc_Cutting,//uk_tg_cut_to_silage5,
			 fmc_Cutting,//uk_tg_cut_to_silage6,
			 fmc_Fertilizer,//uk_tg_ferti_p3,
			 fmc_Fertilizer,//uk_tg_ferti_s3,
			 fmc_Fertilizer,//uk_tg_ferti_p4,
			 fmc_Fertilizer,//uk_tg_ferti_s4,
			 fmc_Fertilizer,//uk_tg_ferti_p5,
			 fmc_Fertilizer,//uk_tg_ferti_s5,
			 fmc_Fertilizer,//uk_tg_ferti_p6,
			 fmc_Fertilizer,//uk_tg_ferti_s6,
			 fmc_Fertilizer,//uk_tg_ferti_p7,
			 fmc_Fertilizer,//uk_tg_ferti_s7,
			 fmc_Fertilizer,//uk_tg_ferti_p8,
			 fmc_Fertilizer,//uk_tg_ferti_s8,
			 fmc_Fertilizer,//uk_tg_ferti_p9,
			 fmc_Fertilizer,//uk_tg_ferti_s9,
			 fmc_Fertilizer,//uk_tg_ferti_p10,
			 fmc_Fertilizer,//uk_tg_ferti_s10,
			 fmc_Fertilizer,//uk_tg_ferti_p11,
			 fmc_Fertilizer,//uk_tg_ferti_s11,
			 fmc_Fertilizer,//uk_tg_ferti_p12,
			 fmc_Fertilizer,//uk_tg_ferti_s12,
			 fmc_Watering,//uk_tg_watering,
			 fmc_Grazing,//uk_tg_cattle_out,
			 fmc_Grazing,//uk_tg_cattle_is_out,
		};
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // UKTEMPGRASS_H

