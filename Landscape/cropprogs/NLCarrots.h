/**
\file
\brief
<B>NLCarrots.h This file contains the headers for the Carrots class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLCarrots.h
//


#ifndef NLCARROTS_H
#define NLCARROTS_H

#define NLCARROTS_BASE 20600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_CA_WINTER_PLOUGH	a_field->m_user[1]
#define NL_CA_HERBI1	a_field->m_user[2]
#define NL_CA_HERBI2	a_field->m_user[3]
#define NL_CA_FUNGI1	a_field->m_user[4]
#define NL_CA_FUNGI2	a_field->m_user[5]

/** Below is the list of things that a farmer can do if he is growing carrots, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_ca_start = 1, // Compulsory, must always be 1 (one).
	nl_ca_sleep_all_day = NLCARROTS_BASE,
	nl_ca_spring_plough_sandy,
	nl_ca_winter_plough_clay,
	nl_ca_winter_deep_harrow_clay,
	nl_ca_ferti_p1,
	nl_ca_ferti_s1,
	nl_ca_preseeding_cultivator,
	nl_ca_bed_forming,
	nl_ca_spring_sow,
	nl_ca_ferti_p2,
	nl_ca_ferti_s2,
	nl_ca_herbicide1,
	nl_ca_herbicide2,
	nl_ca_herbicide3,
	nl_ca_fungicide1,
	nl_ca_fungicide2,
	nl_ca_fungicide3,
	nl_ca_harvest,
	nl_ca_foobar
} NLCarrotsToDo;


/**
\brief
NLCarrots class
\n
*/
/**
See NLCarrots.h::NLCarrotsToDo for a complete list of all possible events triggered codes by the carrots management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLCarrots: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLCarrots(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,12 );
		m_forcespringpossible = true;
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_ca_foobar - NLCARROTS_BASE);
	   m_base_elements_no = NLCARROTS_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,//nl_ca_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_ca_sleep_all_day = NLCARROTS_BASE,
			fmc_Cultivation,//nl_ca_spring_plough_sandy,
			fmc_Cultivation,//nl_ca_winter_plough_clay,
			fmc_Cultivation,//nl_ca_winter_deep_harrow_clay,
			fmc_Fertilizer,//nl_ca_ferti_p1,
			fmc_Fertilizer,//nl_ca_ferti_s1,
			fmc_Cultivation,//nl_ca_preseeding_cultivator,
			fmc_Others,//nl_ca_bed_forming,
			fmc_Others,//nl_ca_spring_sow,
			fmc_Fertilizer,//nl_ca_ferti_p2,
			fmc_Fertilizer,//nl_ca_ferti_s2,
			fmc_Herbicide,//nl_ca_herbicide1,
			fmc_Herbicide,//nl_ca_herbicide2,
			fmc_Herbicide,//nl_ca_herbicide3,
			fmc_Fungicide,//nl_ca_fungicide1,
			fmc_Fungicide,//nl_ca_fungicide2,
			fmc_Fungicide,//nl_ca_fungicide3,
			fmc_Harvest//nl_ca_harvest,
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLCARROTS_H

