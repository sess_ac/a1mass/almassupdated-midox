//
// WinterBarley.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WinterBarley_H
#define WinterBarley_H

#define WBarley_BASE 7600

#define WB_DID_SEVEN_ONE   m_field->m_user[0]
#define WB_DID_SEVEN_TWO   m_field->m_user[1]
#define WB_DID_SEVEN_THREE m_field->m_user[2]
#define WB_DID_SEVEN_FOUR  m_field->m_user[3]

#define WB_FUNGICIDE_DATE  m_field->m_user[4]
#define WB_HERBICIDE_DATE  m_field->m_user[5]

#define WB_DID_EIGHT_ONE   m_field->m_user[0]
#define WB_DID_EIGHT_TWO   m_field->m_user[1]
#define WB_DID_EIGHT_THREE m_field->m_user[2]
#define WB_DID_EIGHT_FOUR  m_field->m_user[3]
#define WB_DECIDE_TO_HERB m_field->m_user[4]
#define WB_DECIDE_TO_FI  m_field->m_user[5]


typedef enum {
  wb_start = 1, // Compulsory, start event must always be 1 (one).
  wb_fertsludge_plant_one = WBarley_BASE,
  wb_fertmanure_stock,
  wb_fertslurry_stock_one,
  wb_autumn_plough,
  wb_autumn_harrow,
  wb_autumn_sow,
  wb_herbicide_one,
  wb_fertmanganese_plant_one,
  wb_fertmanganese_plant_two,
  wb_fertnpk_plant_one,
  wb_fertnpk_plant_two,
  wb_fertslurry_stock_two,
  wb_fertnpk_stock_one,
  wb_fertnpk_stock_two,
  wb_herbicide_two,
  wb_fungicide_one,
  wb_switchboard,
  wb_fungicide_two,
  wb_growth_reg,
  wb_water,
  wb_insecticide,
  wb_harvest,
  wb_hay_turning,
  wb_hay_bailing,
  wb_stubble_harrowing,
  wb_foobar
// --FN--
} WBToDo;



class WinterBarley: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  WinterBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(15,9);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (wb_foobar - WBarley_BASE);
	  m_base_elements_no = WBarley_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//wb_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//wb_fertsludge_plant_one = WBarley_BASE,
		  fmc_Fertilizer,//wb_fertmanure_stock,
		  fmc_Fertilizer,//wb_fertslurry_stock_one,
		  fmc_Cultivation,//wb_autumn_plough,
		  fmc_Cultivation,//wb_autumn_harrow,
		  fmc_Others,//wb_autumn_sow,
		  fmc_Herbicide,//wb_herbicide_one,
		  fmc_Fertilizer,//wb_fertmanganese_plant_one,
		  fmc_Fertilizer,//wb_fertmanganese_plant_two,
		  fmc_Fertilizer,//wb_fertnpk_plant_one,
		  fmc_Fertilizer,//wb_fertnpk_plant_two,
		  fmc_Fertilizer,//wb_fertslurry_stock_two,
		  fmc_Fertilizer,//wb_fertnpk_stock_one,
		  fmc_Fertilizer,//wb_fertnpk_stock_two,
		  fmc_Herbicide,//wb_herbicide_two,
		  fmc_Fungicide,//wb_fungicide_one,
		  fmc_Others,//wb_switchboard,
		  fmc_Fungicide,//wb_fungicide_two,
		  fmc_Others,//wb_growth_reg,
		  fmc_Watering,//wb_water,
		  fmc_Insecticide,//wb_insecticide,
		  fmc_Harvest,//wb_harvest,
		  fmc_Others,//wb_hay_turning,
		  fmc_Others,//wb_hay_bailing,
		  fmc_Cultivation//wb_stubble_harrowing

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // WinterBarley_H
