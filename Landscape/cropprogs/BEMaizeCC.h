/**
\file
\brief
<B>BEMaizeCC.h This file contains the headers for the MaizeCC class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of August 2022 \n
 All rights reserved. \n
*/
//
// BEMaizeCC.h
//


#ifndef BEMAIZECC_H
#define BEMAIZECC_H

#define BEMAIZECC_BASE 25250
/**
\brief A flag used to indicate autumn ploughing status
*/
#define BE_MCC_START_FERTI	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing mazie, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_mcc_start = 1, // Compulsory, must always be 1 (one).
	BE_mcc_sleep_all_day = BEMAIZECC_BASE,
	BE_mcc_stubble_harrow1,
	BE_mcc_stubble_harrow2,
	BE_mcc_winter_plough1,
	BE_mcc_winter_plough2,
	BE_mcc_ferti_p1,
	BE_mcc_ferti_s1,
	BE_mcc_spring_plough1,
	BE_mcc_spring_plough2,
	BE_mcc_preseeding_cultivator,
	BE_mcc_spring_sow_with_ferti,
	BE_mcc_spring_sow,
	BE_mcc_ferti_p2,
	BE_mcc_ferti_s2,
	BE_mcc_herbicide1,
	BE_mcc_harvest,
	BE_mcc_straw_chopping,
	BE_mcc_wait
} BEMaizeCCToDo;


/**
\brief
BEMaize class
\n
*/
/**
See BEMaize.h::BEMaizeToDo for a complete list of all possible events triggered codes by the mazie management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEMaizeCC: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEMaizeCC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10 );
		m_forcespringpossible = true;
   }
};

#endif // BEMAIZE_H

