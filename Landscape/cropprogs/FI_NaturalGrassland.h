/**
\file
\brief
<B>FI_NaturalGrassland.h This file contains the headers for the NaturalGrassland class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// FI_NaturalGrassland.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FI_NATURALGRASSLAND_H
#define FI_NATURALGRASSLAND_H

#define FI_NG_YEARS_AFTER_SOW	a_field->m_user[1]

#define FI_NG_BASE 73500
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing spring barley Fodder, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	fi_ng_start = 1, // Compulsory, must always be 1 (one).
	fi_ng_sleep_all_day = FI_NG_BASE,
	fi_ng_sow,
	fi_ng_npk,
	fi_ng_slurry,
	fi_ng_cutting,
	fi_ng_grazing1,
	fi_ng_cattle_is_out1,
	fi_ng_cutting1,
	fi_ng_foobar,
} FI_NaturalGrasslandToDo;


/**
\brief
FI_NaturalGrassland class
\n
*/
/**
See FI_NaturalGrassland.h::FI_NaturalGrasslandToDo for a complete list of all possible events triggered codes by the GrasslandPasturePerennial management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class FI_NaturalGrassland : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	FI_NaturalGrassland(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 5);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (fi_ng_foobar - FI_NG_BASE);
		m_base_elements_no = FI_NG_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	fi_ng_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	fi_ng_sleep_all_day = FI_NG_BASE,
			fmc_Others,	//	fi_ng_sow,
			fmc_Fertilizer,	//	fi_ng_npk,
			fmc_Fertilizer,	//	fi_ng_slurry,
			fmc_Cutting,	//	fi_ng_cutting,
			fmc_Grazing,	//	fi_ng_grazing1,
			fmc_Grazing,	//	fi_ng_cattle_is_out1,
			fmc_Cutting	//	fi_ng_cutting1,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // FI_NATURALGRASSLAND_H