/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>FI_FeedingGround.cpp This file contains the source for the FI_FeedingGround class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
*/
//
// FI_FeedingGround.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FI_FeedingGround.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_FI_FeedingGround_SkScrapes("DK_CROP_FG_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_FeedingGround_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_FG_InsecticideDay;
extern CfgInt   cfg_FG_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool FI_FeedingGround::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case fi_fg_start:
	{
		FI_FG_YEARS_AFTER_SOW = 0; //Here FI_NG_YEARS_AFTER_SOW flag should get randomly for each field a value
									//THIS PART SHOULD BE SOMEWHERE ELSE OUTSIDE THE PLAN, SO THE STARTING CONDITIONS FOR FIELDS WITH PERMANENT GRASS (YEAR AFTER SOWING)
									// IS SET ONLY ONCE AT THE SIMULATION START!!!

		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
			if (cfg_FI_FeedingGround_SkScrapes.value()) {
				a_field->m_skylarkscrapes = true;
			}
			else {
				a_field->m_skylarkscrapes = false;
			}

		// Set up the date management stuff
	 // Could save the start day in case it is needed later
	// m_field->m_startday = m_ev->m_startday;
	//m_last_date = g_date->DayInYear(20, 8);
	// Start and stop dates for all events after harvest
		int noDates = 4;
		//m_field->SetMDates(0, 0, g_date->DayInYear(15, 8)); // last possible day of harvest
		// Determined by harvest date - used to see if at all possible
		//m_field->SetMDates(1, 0, g_date->DayInYear(17, 8)); // end day of straw chopping
		//m_field->SetMDates(0, 1, g_date->DayInYear(18, 8)); // end day of hay baling
		//m_field->SetMDates(1, 1, g_date->DayInYear(19, 8)); // end day of stubble harrow
		//a_field->SetMDates(1, 2, g_date->DayInYear(20, 8)); // end day of plough

		//a_field->SetMConstants(0, 1);
		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		  //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)

		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {
			//Checking the future...
			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(30, 12) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "FI_FeedingGround::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "FI_FeedingGround::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "FI_FeedingGround::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				//only for the first year
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), fi_fg_sow, false);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // OK, let's go.
		// Here we queue up the first event
		//Each field has assign randomly a FI_FG_YEARS_AFTER_SOW flag value from 1 to 5
	//if 1, 3 or 5 then first year after sowing and nothing is done (sleep all day) - mowing/grazing only every 2nd year
	//if 2 or 4 then grazing or cattle is out 
	//if 0 then it is a re-sowing year with fertilizer and cutting
		if ((FI_FG_YEARS_AFTER_SOW + g_date->GetYearNumber()) % 1 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, fi_fg_sow, false);
		}
		else if ((FI_FG_YEARS_AFTER_SOW + g_date->GetYearNumber()) % 1 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(28,2);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, fi_fg_sleepallday, false);
		}
		break;
	}
	break; // this thread is only done the first year
	case fi_fg_sow: // 100% do this
		if (!a_farm->SpringSow(a_field, 0.0,
			g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_fg_sow, true);
			break;
		}
	//Next event is grazing / cutting:
	SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), fi_fg_grazing1, false);
	break;
	case fi_fg_grazing1: // 90% cut or grazed - suggests 45% each
		if (a_ev->m_lock || a_farm->DoIt(45)) {
			if (!a_farm->CattleOut(a_field, 0.0,
				g_date->DayInYear(1, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_fg_grazing1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, fi_fg_cattle_is_out1, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), fi_fg_cutting1, false);
		break;
	case fi_fg_cutting1: //
		if (a_ev->m_lock || a_farm->DoIt(82)) { // 45% of the 55% left that did not do grazing
			if (!a_farm->CutToHay(a_field, 0.0,
				g_date->DayInYear(1, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_fg_cutting1, true);
				break;
			}
			done = true;
			break;
		}
	case fi_fg_cattle_is_out1:    // Keep the cattle out there
						   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(1, 11) - g_date->DayInYear(), g_date->DayInYear(1, 11))) {
			SimpleEvent(g_date->Date() + 1, fi_fg_cattle_is_out1, false);
			break;
		}
		done = true;
		break;
		//other years:
	case fi_fg_sleepallday: // 100% do this
		if (!a_farm->SleepAllDay(a_field, 0.0,
			g_date->DayInYear(1, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_fg_sleepallday, true);
			break;
		}
		//Here comes a fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), fi_fg_grazing2, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), fi_fg_sow_ny, false);
		break;
	
	case fi_fg_grazing2:
		if (a_ev->m_lock || a_farm->DoIt(45)) {
			if (!a_farm->CattleOut(a_field, 0.0,
				g_date->DayInYear(1, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_fg_grazing2, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, fi_fg_cattle_is_out2, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), fi_fg_cutting2, false);
		break;

	case fi_fg_sow_ny: // 100% do this
		if (!a_farm->SpringSow(a_field, 0.0,
			g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_fg_sow_ny, true);
			break;
		}
		break; // end of sowing thread

	case fi_fg_cattle_is_out2:    // Keep the cattle out there
							   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear(), g_date->DayInYear(1, 5))) {
			SimpleEvent(g_date->Date() + 1, fi_fg_cattle_is_out2, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), fi_fg_cutting3, false);
		break;
	case fi_fg_cutting2: //
		if (a_ev->m_lock || a_farm->DoIt(82)) { // 45% of the 55% left that did not do grazing
			if (!a_farm->CutToHay(a_field, 0.0,
				g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_fg_cutting2, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), fi_fg_cutting3, false);
		break;
	case fi_fg_cutting3:
		if (a_ev->m_lock || a_farm->DoIt(5)) {
			if (!a_farm->CutToHay(a_field, 0.0,
				g_date->DayInYear(30, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_fg_cutting3, true);
				break;
			}
			SimpleEvent(g_date->Date(), fi_fg_remove_straw, false);
			break;
		}
		done = true;
		break;
	case fi_fg_remove_straw:
		if (!a_farm->StrawRemoval(a_field, 0.0,
			g_date->DayInYear(30, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_fg_remove_straw, true);
			break;
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "FI_FeedingGround::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}
