//
// SpringBarleySingle.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SpringBarleyStriglingSingle.h"
#include "math.h"



extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgFloat cfg_strigling_prop;

CfgInt cfg_SingleStriglingTime( "STR_SINGLESTRIGLINGTIME", CFG_CUSTOM, 1 );



bool SpringBarleyStriglingSingle::Do( Farm * a_farm, LE * a_field, FarmEvent * a_ev ) {
  m_farm = a_farm;
  m_field = a_field;
  m_ev = a_ev;

  bool done = false;

  switch ( m_ev->m_todo ) {
    case sbsts_start: {
        a_field->ClearManagementActionSum();

        // Set up the date management stuff
        // Could save the start day in case it is needed later
        // m_field->m_startday = m_ev->m_startday;
        m_last_date = g_date->DayInYear( 30, 8 );
        // Start and stop dates for all events after harvest
        int noDates = 2;
        m_field->SetMDates( 0, 0, g_date->DayInYear( 20, 8 ) );
        // Determined by harvest date - used to see if at all possible
        m_field->SetMDates( 1, 0, g_date->DayInYear( 10, 8 ) );
        m_field->SetMDates( 0, 1, g_date->DayInYear( 10, 8 ) );
        m_field->SetMDates( 1, 1, g_date->DayInYear( 30, 8 ) );
        // Check the next crop for early start, unless it is a spring crop
        // in which case we ASSUME that no checking is necessary!!!!
        // So DO NOT implement a crop that runs over the year boundary
        if ( m_ev->m_startday > g_date->DayInYear( 1, 7 ) ) {
          if ( m_field->GetMDates( 0, 0 ) >= m_ev->m_startday ) {
            g_msg->Warn( WARN_BUG, "SpringBarley::Do(): ""Harvest too late for the next crop to start!!!", "" );
            exit( 1 );
          }
          // Now fix any late finishing problems
          for ( int i = 0; i < noDates; i++ ) {
            if ( m_field->GetMDates( 0, i ) >= m_ev->m_startday )
              m_field->SetMDates( 0, i, m_ev->m_startday - 1 );
            if ( m_field->GetMDates( 1, i ) >= m_ev->m_startday )
              m_field->SetMDates( 1, i, m_ev->m_startday - 1 );
          }
        }
        // Now no operations can be timed after the start of the next crop.

        int d1;
        if ( !m_ev->m_first_year ) {
          int today = g_date->Date();
          // Are we before July 1st?
          d1 = g_date->OldDays() + g_date->DayInYear( 1, 7 );
          if ( today < d1 ) {
            // Yes, too early. We assumme this is because the last crop was late
            g_msg->Warn( WARN_BUG, "SpringBarley::Do(): ""Crop start attempt between 1st Jan & 1st July", "" );
            exit( 1 );
          } else {
            d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
            if ( today > d1 ) {
              // Yes too late - should not happen - raise an error
              g_msg->Warn( WARN_BUG, "SpringBarley::Do(): ""Crop start attempt after last possible start date", "" );
              exit( 1 );
            }
          }
        } else {
          SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 4 ), sbsts_spring_plough, false );
        break;
        }
        // End single block date checking code. Please see next line
        // comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear( 1, 11 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }

        // OK, let's go.
        SimpleEvent( d1, sbsts_autumn_plough, false );
        SBSTS_SLURRY_DONE = false;
        SBSTS_MANURE_DONE = false;
        SBSTS_SLURRY_EXEC = false;
        SBSTS_MANURE_EXEC = false;
        SBSTS_DID_AUTUMN_PLOUGH = false;
      }
    break;

    case sbsts_autumn_plough:
      if ( m_ev->m_lock || m_farm->DoIt( 70 ) ) {
        if ( !m_farm->AutumnPlough( m_field, 0.0, g_date->DayInYear( 30, 11 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_autumn_plough, true );
        break;
        }
        SBSTS_DID_AUTUMN_PLOUGH = true;
      }
      // +365 for next year
      if ( m_farm->IsStockFarmer() ) // StockFarmer
      {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 3 ) + 365, sbsts_fertslurry_stock, false );
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 3 ) + 365, sbsts_fertmanure_stock_one, false );
      } else { // PlantFarmer
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 3 ) + 365, sbsts_spring_plough, false );
      }
    break;

      //*** The stock farmers thread
    case sbsts_fertslurry_stock:
      if ( m_ev->m_lock || m_farm->DoIt( 90 ) ) {
        if ( !m_farm->FA_Slurry( m_field, 0.0, g_date->DayInYear( 10, 4 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_fertslurry_stock, true );
        break;
        }
        SBSTS_SLURRY_EXEC = true;
      }
      SBSTS_SLURRY_DONE = true;
      if ( SBSTS_MANURE_DONE ) {
        // We are the last thread, so queue up spring plough.
        SimpleEvent( g_date->Date(), sbsts_spring_plough, false );
      }
    break;

    case sbsts_fertmanure_stock_one:
      if ( m_ev->m_lock || m_farm->DoIt( 67 ) ) {
        if ( !m_farm->FA_Manure( m_field, 0.0, g_date->DayInYear( 15, 4 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_fertmanure_stock_one, true );
        break;
        }
        SBSTS_MANURE_EXEC = true;
      }
      SBSTS_MANURE_DONE = true;
      if ( SBSTS_SLURRY_DONE ) {
        SimpleEvent( g_date->Date(), sbsts_spring_plough, false );
      }
    break;

    case sbsts_spring_plough:
      if ( !SBSTS_DID_AUTUMN_PLOUGH ) {
        if ( !m_farm->SpringPlough( m_field, 0.0, g_date->DayInYear( 10, 4 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_spring_plough, true );
        break;
        }
      }
      {
        int d1 = g_date->Date();
        if ( d1 < g_date->OldDays() + g_date->DayInYear( 20, 3 ) ) {
          d1 = g_date->OldDays() + g_date->DayInYear( 20, 3 );
        }
        SimpleEvent( d1, sbsts_spring_harrow, false );
      }
    break;

    case sbsts_spring_harrow:
      if ( !m_farm->SpringHarrow( m_field, 0.0, g_date->DayInYear( 10, 4 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, sbsts_spring_harrow, true );
      break;
      }
      if ( m_farm->IsStockFarmer() ) {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5, 4 ), sbsts_fertmanure_stock_two, false );
      } else {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5, 4 ), sbsts_fertmanure_plant, false );
      }
    break;

    case sbsts_fertmanure_stock_two:
      if ( m_ev->m_lock || m_farm->DoIt( 65 ) || ( !SBSTS_SLURRY_EXEC && !SBSTS_MANURE_EXEC ) ) {
        if ( !m_farm->FA_NPK( m_field, 0.0, g_date->DayInYear( 10, 4 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_fertmanure_stock_two, true );
        break;
        }
      }
      {
        int d1 = g_date->Date();
        if ( d1 < g_date->OldDays() + g_date->DayInYear( 25, 3 ) ) {
          d1 = g_date->OldDays() + g_date->DayInYear( 25, 3 );
        }
        SimpleEvent( d1, sbsts_spring_sow, false );
      }
    break;

    case sbsts_fertmanure_plant:
      if ( m_ev->m_lock || m_farm->DoIt( 75 ) ) {
        if ( !m_farm->FP_NPK( m_field, 0.0, g_date->DayInYear( 10, 4 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_fertmanure_plant, true );
        break;
        }
        // Did FP_NPK so go directly to spring sow.
        {
          int d1 = g_date->Date();
          if ( d1 < g_date->OldDays() + g_date->DayInYear( 25, 3 ) ) {
            d1 = g_date->OldDays() + g_date->DayInYear( 25, 3 );
          }
          SimpleEvent( d1, sbsts_spring_sow, false );
        }
      break;
      }
      SimpleEvent( g_date->Date(), sbsts_fertlnh3_plant, false );
    break;

    case sbsts_fertlnh3_plant:
      if ( !m_farm->FP_LiquidNH3( m_field, 0.0, g_date->DayInYear( 10, 4 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, sbsts_fertlnh3_plant, true );
      break;
      }
      SimpleEvent( g_date->Date(), sbsts_fertpk_plant, false );
    break;

    case sbsts_fertpk_plant:
      if ( !m_farm->FP_PK( m_field, 0.0, g_date->DayInYear( 10, 4 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, sbsts_fertpk_plant, true );
      break;
      }
      {
        int d1 = g_date->Date();
        if ( d1 < g_date->OldDays() + g_date->DayInYear( 25, 3 ) ) {
          d1 = g_date->OldDays() + g_date->DayInYear( 25, 3 );
        }
        SimpleEvent( d1, sbsts_spring_sow, false );
      }
    break;

    case sbsts_spring_sow:
      if ( !m_farm->SpringSow( m_field, 0.0, g_date->DayInYear( 10, 4 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, sbsts_spring_sow, true );
      break;
      }
      {
        int d1 = g_date->Date();
        if ( d1 < g_date->OldDays() + g_date->DayInYear( 5, 4 ) ) {
          d1 = g_date->OldDays() + g_date->DayInYear( 5, 4 );
        }
        SimpleEvent( d1, sbsts_spring_roll, false );
      }
    break;

    case sbsts_spring_roll:
      if ( m_ev->m_lock || m_farm->DoIt( 30 ) ) {
        if ( !m_farm->SpringRoll( m_field, 0.0, g_date->DayInYear( 20, 4 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_spring_roll, true );
        break;
        }
      }
      SBSTS_HERBI_DATE = 0;
      SBSTS_GR_DATE = 0;
      SBSTS_FUNGI_DATE = 0;
      SBSTS_WATER_DATE = 0;
      SBSTS_INSECT_DATE = 0;
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 5 ), sbsts_fungicide_one, false ); // Main.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 5 ), sbsts_GR, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ), sbsts_water_one, false );
      //
      if ( cfg_SingleStriglingTime.value() == 1 ) {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear(10,4), sbsts_strigling_one, true );
      } else if ( cfg_SingleStriglingTime.value() == 2 ) {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear(20,4), sbsts_strigling_one, true );
      } else if ( cfg_SingleStriglingTime.value() == 3 ) {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear(30,4), sbsts_strigling_one, true );
      } else {
        if ( cfg_SingleStriglingTime.value() == 4 )
//        SimpleEvent( g_date->OldDays() + g_date->DayInYear(10,5), sbsts_strigling_one, true );
        SimpleEvent( g_date->OldDays() + g_date->DayInYear(10,5), sbsts_strigling_one, true );
      }
    break;

    case sbsts_strigling_one:
      if ( !m_farm->Strigling( m_field, 0.0, 0 ) ) {
        SimpleEvent( g_date->Date() + 1, sbsts_strigling_one, true );
      break;
      }
      // end the thread
    break;


      // GReg thread
    case sbsts_GR:
      if ( g_date->Date() < SBSTS_HERBI_DATE + 1 || g_date->Date() < SBSTS_FUNGI_DATE + 1 ) {
        SimpleEvent( g_date->Date() + 1, sbsts_GR, m_ev->m_lock );
      break;
      }
      if ( m_ev->m_lock || m_farm->DoIt( ( int )floor( 0.5 + 5 * cfg_greg_app_prop.value() ) ) ) {
        if ( !m_farm->GrowthRegulator( m_field, 0.0, g_date->DayInYear( 25, 5 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_GR, true );
        break;
        }
        SBSTS_GR_DATE = g_date->Date();
      }
    break;

      // Fungicide thread  & MAIN THREAD
    case sbsts_fungicide_one:
      if ( g_date->Date() < SBSTS_HERBI_DATE + 1 || g_date->Date() < SBSTS_GR_DATE + 1 ) {
        SimpleEvent( g_date->Date() + 1, sbsts_fungicide_one, m_ev->m_lock );
      break;
      }
      if ( m_ev->m_lock || m_farm->DoIt( ( int )floor( 0.5 + 30 * cfg_fungi_app_prop1.value() ) ) ) {
        if ( !m_farm->FungicideTreat( m_field, 0.0, g_date->DayInYear( 25, 5 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_fungicide_one, true );
        break;
        }
        SBSTS_FUNGI_DATE = g_date->Date();
        {
          int d1 = g_date->Date() + 10;
          if ( d1 < g_date->OldDays() + g_date->DayInYear( 25, 5 ) ) {
            d1 = g_date->OldDays() + g_date->DayInYear( 25, 5 );
          }
          SimpleEvent( d1, sbsts_fungicide_two, false );
        }
      }
      {
        int d1 = g_date->Date();
        if ( d1 < g_date->OldDays() + g_date->DayInYear( 15, 5 ) ) {
          d1 = g_date->OldDays() + g_date->DayInYear( 15, 5 );
        }
        SimpleEvent( d1, sbsts_insecticide, false );
      }
    break;

    case sbsts_fungicide_two:
      if ( g_date->Date() < SBSTS_HERBI_DATE + 1 || g_date->Date() < SBSTS_GR_DATE + 1 ) {
        SimpleEvent( g_date->Date() + 1, sbsts_fungicide_two, m_ev->m_lock );
      break;
      }
      if ( m_ev->m_lock || m_farm->DoIt( ( int )floor( 0.5 + 33 * cfg_fungi_app_prop1.value() ) ) ) {
        if ( !m_farm->FungicideTreat( m_field, 0.0, g_date->DayInYear( 10, 6 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_fungicide_two, true );
        break;
        }
        SBSTS_FUNGI_DATE = g_date->Date();
      }
    break;

      // Water thread
    case sbsts_water_one:
      if ( g_date->Date() < SBSTS_HERBI_DATE + 1 || g_date->Date() < SBSTS_FUNGI_DATE + 1 ) {
        SimpleEvent( g_date->Date() + 1, sbsts_water_one, m_ev->m_lock );
      break;
      }
      if ( m_ev->m_lock || m_farm->DoIt( 20 ) ) {
        if ( !m_farm->Water( m_field, 0.0, g_date->DayInYear( 30, 5 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_water_one, true );
        break;
        }
        SBSTS_WATER_DATE = g_date->Date();
      }
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 6 ), sbsts_water_two, false );
    break;

    case sbsts_water_two:
      if ( g_date->Date() < SBSTS_INSECT_DATE + 1 ) {
        SimpleEvent( g_date->Date() + 1, sbsts_water_two, m_ev->m_lock );
      break;
      }
      if ( m_ev->m_lock || m_farm->DoIt( 10 ) ) {
        if ( !m_farm->Water( m_field, 0.0, g_date->DayInYear( 1, 7 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_water_two, true );
        break;
        }
        SBSTS_WATER_DATE = g_date->Date();
      }
    break;

      // Insecticide thread  & MAIN THREAD
    case sbsts_insecticide:
      if ( g_date->Date() < SBSTS_WATER_DATE + 1 ) {
        SimpleEvent( g_date->Date() + 1, sbsts_insecticide, m_ev->m_lock );
      break;
      }
      if ( m_ev->m_lock || m_farm->DoIt( ( int )floor( 0.5 + 35 * cfg_ins_app_prop1.value() ) ) ) {
        if ( !m_farm->InsecticideTreat( m_field, 0.0, g_date->DayInYear( 10, 6 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_insecticide, true );
        break;
        }
        SBSTS_INSECT_DATE = g_date->Date();
      }
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 8 ), sbsts_harvest, false );
    break;

    case sbsts_harvest:
      if ( !m_farm->Harvest( m_field, 0.0, g_date->DayInYear( 20, 8 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, sbsts_harvest, true );
      break;
      }
      SimpleEvent( g_date->Date(), sbsts_straw_chopping, false );
    break;

    case sbsts_straw_chopping:
      if ( m_ev->m_lock || m_farm->DoIt( 50 ) ) {
        // Force straw chopping to happen on the same day as harvest.
        if ( !m_farm->StrawChopping( m_field, 0.0, 0 ) ) {
          // Shouldn't happen.
          SimpleEvent( g_date->Date(), sbsts_straw_chopping, true );
        break;
        }
        // Did chop, so go directly to stubble harrowing.
        SimpleEvent( g_date->OldDays() + m_field->GetMDates( 0, 1 ), sbsts_stubble_harrow, false );
      break;
      }
      // Do hay baling first.
      SimpleEvent( g_date->Date(), sbsts_hay_baling, false );
    break;

    case sbsts_hay_baling:
      if ( !m_farm->HayBailing( m_field, 0.0, m_field->GetMDates( 1, 0 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, sbsts_hay_baling, true );
      break;
      }
      SimpleEvent( g_date->OldDays() + m_field->GetMDates( 0, 1 ), sbsts_stubble_harrow, false );
    break;

    case sbsts_stubble_harrow:
      if ( m_ev->m_lock || m_farm->DoIt( 60 ) ) {
        if ( !m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates( 1, 1 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, sbsts_stubble_harrow, true );
        break;
        }
      }
      // END MAIN THREAD
      done = true;
    break;

    default:
      g_msg->Warn( WARN_BUG, "SpringBarley::Do(): ""Unknown event type! ", "" );
      exit( 1 );
  }
  return done;
}


