/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_Carrots.cpp This file contains the source for the DE_Carrots class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Susanne Stein \n
 Version of August 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_Carrots.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_Carrots.h"


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional carrot.
*/
bool DE_Carrots::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{

	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DECarrots; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (m_ev->m_todo)
	{
	case de_car_start:
	{
		
		// de_car_start just sets up all the starting conditions and reference dates that are needed to start a DE_CAR
		DE_CAR_WINTER_PLOUGH = false;
		DE_CAR_HERBI1 = false;
		DE_CAR_FUNGI1 = false;
		DE_CAR_FUNGI2 = false;

		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(30, 9); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(30, 9); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(30, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_car_spring_sow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 10) + isSpring;
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_ev->m_forcespring) {
			if (m_field->GetSoilType() == 8) { // on sandy soils
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_car_spring_plough_sandy, false);
				break;
			}
			else {
				if (m_farm->IsStockFarmer()) //Stock Farmer
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_car_ferti_s1, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_car_ferti_p1, false, m_farm, m_field);
				break;
			}
		}
		else SimpleEvent_(d1, de_car_winter_plough_clay, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case de_car_winter_plough_clay: // not on sandy soils
		if (m_farm->DoIt_prob(0.80) && (m_field->GetSoilType() != 1 && m_field->GetSoilType() != 2 && m_field->GetSoilType() != 3 && m_field->GetSoilType() != 4))
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.95))
			{
				if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_car_winter_plough_clay, true, m_farm, m_field);
					break;
				}
				else
				{
					DE_CAR_WINTER_PLOUGH = true;
					if (m_farm->IsStockFarmer()) //Stock Farmer
					{
						SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_car_ferti_s1, false, m_farm, m_field);
					}
					else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_car_ferti_p1, false, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->Date() + 1, de_car_winter_deep_harrow_clay, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_car_spring_plough_sandy, false, m_farm, m_field);
		break;
	case de_car_winter_deep_harrow_clay:
		if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_car_winter_deep_harrow_clay, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_car_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_car_ferti_p1, false, m_farm, m_field);
		break;
	case de_car_spring_plough_sandy:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_car_spring_plough_sandy, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 7, de_car_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 7, de_car_ferti_p1, false, m_farm, m_field);
		break;
	case de_car_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.95))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_car_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_car_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_car_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.95))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_car_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_car_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_car_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 3, de_car_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 3, de_car_bed_forming, false, m_farm, m_field);
		break;
	case de_car_bed_forming:
		if (!m_farm->BedForming(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_car_bed_forming, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_car_spring_sow, false, m_farm, m_field);
		break;
	case de_car_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_car_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, de_car_herbicide1, false, m_farm, m_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_car_fungicide1, false, m_farm, m_field);	// Fungicide thread = MAIN THREAD
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), de_car_ferti_s2, false, m_farm, m_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), de_car_ferti_p2, false, m_farm, m_field);
		break;
	case de_car_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_car_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_car_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_car_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_car_herbicide1: // The first of the pesticide managements.
						   // Here comes the herbicide thread
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_car_herbicide1, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->Date() + 14, de_car_herbicide2, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_car_herbicide2, false, m_farm, m_field);
		break;
	case de_car_herbicide2:
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, de_car_herbicide2, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || (m_farm->DoIt_prob(0.8)))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_car_herbicide2, true, m_farm, m_field);
					break;
				}
				DE_CAR_HERBI1 = true;
			}
			SimpleEvent_(g_date->Date() + 14, de_car_herbicide3, false, m_farm, m_field);
			break;
		}
		break;
	case de_car_herbicide3:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.75) && DE_CAR_HERBI1 == 1)) // 60% of all farmers
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_car_herbicide3, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_car_fungicide1:
		// Here comes the fungicide thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_car_fungicide1, true, m_farm, m_field);
				break;
			}
			DE_CAR_FUNGI1 = true;
		}
		SimpleEvent_(g_date->Date() + 21, de_car_fungicide2, false, m_farm, m_field);
		break;
	case de_car_fungicide2:
		if (a_ev->m_lock || (a_farm->DoIt_prob(1.00) && DE_CAR_FUNGI1 == 1))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_car_fungicide2, true, m_farm, m_field);
				break;
			}
			DE_CAR_FUNGI2 = true;
		}
		SimpleEvent_(g_date->Date() + 21, de_car_fungicide3, false, m_farm, m_field);
		break;
	case de_car_fungicide3:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.75) && DE_CAR_FUNGI2 == 2)) // 75% of the previous 80% = 60% of all farmers
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_car_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 9), de_car_harvest, false, m_farm, m_field);
		break;
	case de_car_harvest:
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_car_harvest, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_Carrots::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}