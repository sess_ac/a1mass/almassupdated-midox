//
// DK_CerealLegume_Whole.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKCerealLegume_Whole_H
#define DKCerealLegume_Whole_H

#define DK_CLW_BASE 62000
#define DK_CLW_FORCESPRING	a_field->m_user[1]

typedef enum {
	dk_clw_start = 1, // Compulsory, start event must always be 1 (one).
	dk_clw_harvest = DK_CLW_BASE,
	dk_clw_autumn_plough1,
	dk_clw_slurry1,
	dk_clw_fertilizer,
	dk_clw_spring_plough,
	dk_clw_spring_sow,
	dk_clw_spring_sow_lo,
	dk_clw_water,
	dk_clw_herbicide,
	dk_clw_insecticide,
	dk_clw_fungicide,
	dk_clw_swathing,
	dk_clw_straw_chopping,
	dk_clw_foobar,
} DK_CerealLegume_WholeToDo;



class DK_CerealLegume_Whole : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_CerealLegume_Whole(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 11);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_clw_foobar - DK_CLW_BASE);
		m_base_elements_no = DK_CLW_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_clw_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_clw_harvest = DK_CLW_BASE,
			  fmc_Cultivation,	//	  dk_clw_autumn_plough1,
			  fmc_Fertilizer,	//	  dk_clw_slurry1,
			  fmc_Fertilizer,	//	  dk_clw_fertilizer,
			  fmc_Cultivation,	//	  dk_clw_spring_plough,
			  fmc_Others,	//	  dk_clw_spring_sow,
			  fmc_Others,	//	  dk_clw_spring_sow_lo,
			  fmc_Watering,	//	  dk_clw_water,
			  fmc_Herbicide,	//	  dk_clw_herbicide,
			  fmc_Insecticide,	//	  dk_clw_insecticide,
			  fmc_Fungicide,	//	  dk_clw_fungicide,
			  fmc_Cutting,	//	  dk_clw_swathing,
			  fmc_Others,	//	  dk_clw_straw_chopping,

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_CerealLegume_Whole_H
