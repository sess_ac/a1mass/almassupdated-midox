/**
\file
\brief
<B>DK_OOrchOther.h This file contains the source for the DK_OOrchard_Perm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OOrchOther.h
//


#ifndef DK_OORCHOTHER_H
#define DK_OORCHOTHER_H

#define DK_OOOT_YEARS_AFTER_PLANT	a_field->m_user[0]
#define DK_OOOT_EST_YEAR	a_field->m_user[1]

#define DK_OOOT_BASE 68700
/**

*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_ooot_start = 1, // Compulsory, must always be 1 (one).
	dk_ooot_sleep_all_day = DK_OOOT_BASE,
	dk_ooot_spring_plough,
	dk_ooot_stubble_harrow1,
	dk_ooot_stubble_harrow2,
	dk_ooot_manure1_s,
	dk_ooot_manure1_p,
	dk_ooot_water1,
	dk_ooot_subsoiler,
	dk_ooot_planting,
	dk_ooot_sow_grass,
	dk_ooot_manual_cutting1,
	dk_ooot_manual_cutting2,
	dk_ooot_manual_cutting3,
	dk_ooot_manual_cutting4,
	dk_ooot_manual_cutting5,
	dk_ooot_manual_cutting6,
	dk_ooot_manual_cutting7,
	dk_ooot_manual_cutting8,
	dk_ooot_manual_cutting9,
	dk_ooot_manual_cutting10,
	dk_ooot_manual_cutting11,
	dk_ooot_manual_cutting12,
	dk_ooot_manure2_s,
	dk_ooot_manure2_p,
	dk_ooot_row_cultivation1,
	dk_ooot_water2,
	dk_ooot_water3,
	dk_ooot_water4,
	dk_ooot_water5,
	dk_ooot_water6,
	dk_ooot_water7,
	dk_ooot_cutting1,
	dk_ooot_cutting2,
	dk_ooot_cutting3,
	dk_ooot_cutting4,
	dk_ooot_cutting5,
	dk_ooot_cutting6,
	dk_ooot_row_cultivation2,
	dk_ooot_copper_s,
	dk_ooot_copper_p,
	dk_ooot_fungicide1,
	dk_ooot_boron_s,
	dk_ooot_boron_p,
	dk_ooot_fungicide2,
	dk_ooot_insecticide,
	dk_ooot_remove_fruits,
	dk_ooot_fungicide3,
	dk_ooot_harvest,
	dk_ooot_fungicide4,
	dk_ooot_fungicide5,
	dk_ooot_foobar,
} DK_OOrchOtherToDo;


/**
\brief
DK_OOrchOther class
\n
*/
/**
See DK_OOrchOther.h::DK_OOrchOtherToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OOrchOther : public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OOrchOther(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,4 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_ooot_foobar - DK_OOOT_BASE);
	   m_base_elements_no = DK_OOOT_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_ooot_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_ooot_sleep_all_day = DK_OOOT_BASE,
			fmc_Cultivation, //dk_ooot_spring_plough,
			fmc_Cultivation, //dk_ooot_stubble_harrow1,
			fmc_Cultivation, //dk_ooot_stubble_harrow2,
			fmc_Fertilizer, //dk_ooot_manure1,
			fmc_Watering, //dk_ooot_water1,
			fmc_Cultivation, //dk_ooot_subsoiler,
			fmc_Others, //dk_ooot_planting,
			fmc_Others, //dk_ooot_sow_grass,
			fmc_Cutting, //dk_ooot_manual_cutting1,
			fmc_Cutting, //dk_ooot_manual_cutting2,
			fmc_Cutting, //dk_ooot_manual_cutting3,
			fmc_Cutting, //dk_ooot_manual_cutting4,
			fmc_Cutting, //dk_ooot_manual_cutting5,
			fmc_Cutting, //dk_ooot_manual_cutting6,
			fmc_Cutting, //dk_ooot_manual_cutting7,
			fmc_Cutting, //dk_ooot_manual_cutting8,
			fmc_Cutting, //dk_ooot_manual_cutting9,
			fmc_Cutting, //dk_ooot_manual_cutting10,
			fmc_Cutting, //dk_ooot_manual_cutting11,
			fmc_Cutting, //dk_ooot_manual_cutting12,
			fmc_Cutting, //dk_ooot_manure2,
			fmc_Cultivation, //dk_ooot_row_cultivation1,
			fmc_Watering, //dk_ooot_water2,
			fmc_Watering, //dk_ooot_water3,
			fmc_Watering, //dk_ooot_water4,
			fmc_Watering, //dk_ooot_water5,
			fmc_Watering, //dk_ooot_water6,
			fmc_Watering, //dk_ooot_water7,
			fmc_Cutting, //dk_ooot_cutting1,
			fmc_Cutting, //dk_ooot_cutting2,
			fmc_Cutting, //dk_ooot_cutting3,
			fmc_Cutting, //dk_ooot_cutting4,
			fmc_Cutting, //dk_ooot_cutting5,
			fmc_Cutting, //dk_ooot_cutting6,
			fmc_Cultivation, //dk_ooot_row_cultivation2,
			fmc_Fertilizer, //dk_ooot_coppper,
			fmc_Others, //dk_ooot_fungicide1,
			fmc_Fertilizer, //dk_ooot_boron,
			fmc_Others, //dk_ooot_fungicide2,
			fmc_Others, //dk_ooot_insecticide,
			fmc_Cutting, //dk_ooot_remove_fruits,
			fmc_Others, //dk_ooot_fungicide3,
			fmc_Harvest, //dk_ooot_harvest,
			fmc_Others, //dk_ooot_fungicide4,
			fmc_Others, //dk_ooot_fungicide5,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OOrchOther_H

