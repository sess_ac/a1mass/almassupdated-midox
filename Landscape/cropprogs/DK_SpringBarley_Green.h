//
// DK_SpringBarley_Green.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disspgaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disspgaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSPGUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSPGAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSPGUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSPGUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKSpringBarley_Green_H
#define DKSpringBarley_Green_H

#define DK_SPG_BASE 65600
#define DK_SPG_FORCESPRING	a_field->m_user[2]

typedef enum {
	dk_spg_start = 1, // Compulsory, start event must always be 1 (one).
	dk_spg_harvest = DK_SPG_BASE,
	dk_spg_autumn_plough1,
	dk_spg_slurry1,
	dk_spg_fertilizer,
	dk_spg_spring_plough,
	dk_spg_spring_sow,
	dk_spg_spring_sow_lo,
	dk_spg_water,
	dk_spg_herbicide,
	dk_spg_insecticide,
	dk_spg_fungicide,
	dk_spg_swathing,
	dk_spg_straw_chopping,
	dk_spg_wait,
	dk_spg_foobar,
} DK_SpringBarley_GreenToDo;



class DK_SpringBarley_Green : public Crop
{
public:
  bool  Do(Farm * a_farm, LE * a_field, FarmEvent * a_ev);
  DK_SpringBarley_Green(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
	  m_first_date = g_date->DayInYear(1, 11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_spg_foobar - DK_SPG_BASE);
	  m_base_elements_no = DK_SPG_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_spg_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_spg_harvest = DK_SPG_BASE,
			fmc_Cultivation,	//	  dk_spg_autumn_plough1,
			fmc_Fertilizer,	//	  dk_spg_slurry1,
			fmc_Fertilizer,	//	  dk_spg_fertilizer,
			fmc_Cultivation,	//	  dk_spg_spring_plough,
			fmc_Others,	//	  dk_spg_spring_sow,
			fmc_Others,	//	  dk_spg_spring_sow_lo,
			fmc_Watering,	//	  dk_spg_water,
			fmc_Herbicide,	//	  dk_spg_herbicide,
			fmc_Insecticide,	//	  dk_spg_insecticide,
			fmc_Fungicide,	//	  dk_spg_fungicide,
			fmc_Cutting,	//	  dk_spg_swathing,
			fmc_Others,	//	  dk_spg_straw_chopping,
			fmc_Others, // dk_spg_wait,

			   // no foobar entry			

	  };
	  // Iterate over the catlist elements and copy them to vector						
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};


#endif // DK_SpringBarley_Green_H
