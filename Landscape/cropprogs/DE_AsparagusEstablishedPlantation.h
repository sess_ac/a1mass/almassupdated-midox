/**
\file
\brief
<B>DE_AsparagusEstablishedPlantation.h This file contains the headers for the AsparagusEstablishedPlantation class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// AsparagusEstablishedPlantation.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_ASPARAGUSESTABLISHEDPLANTATION_H
#define DE_ASPARAGUSESTABLISHEDPLANTATION_H

#define DE_AEP_BASE 38900

/**
\brief A flag used to indicate autumn ploughing status
*/
#define CROP_HARVEST_DO	m_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing asparagus, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_aep_start = 1, // Compulsory, must always be 1 (one).
	de_aep_sleep_all_day = DE_AEP_BASE,
	de_aep_herbicide,
	de_aep_ferti_p1, // PK on spring
	de_aep_ferti_s1,
	de_aep_spring_harrow,
	de_aep_hilling,
	de_aep_harvesting,
	de_aep_ferti_p2, // N fertilizer I
	de_aep_ferti_s2,
	de_aep_ferti_p3, // N fertilizer II
	de_aep_ferti_s3,
	de_aep_watering_start,
	de_aep_watering,
	de_aep_fungicide1,
	de_aep_fungicide2,
	de_aep_fungicide3,
	de_aep_insecticide1,
	de_aep_insecticide2,
	de_aep_cutting_plants,
	de_aep_ferti_p4, //organic fertilizer autumn
	de_aep_ferti_s4,
	de_aep_harrowing_rows, //de-constructing rows before winter
	de_aep_foobar,
} DE_AsparagusEstablishedPlantationToDo;



class DE_AsparagusEstablishedPlantation : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_AsparagusEstablishedPlantation(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(10, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_aep_foobar - DE_AEP_BASE);
		m_base_elements_no = DE_AEP_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	de_aep_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	de_aep_sleep_all_day = DE_AEP_BASE,
			fmc_Herbicide,	//	de_aep_herbicide,
			fmc_Fertilizer,	//	de_aep_ferti_p2, // PK on spring
			fmc_Fertilizer,	//	de_aep_ferti_s2,
			fmc_Cultivation, //	de_aep_spring_harrow,
			fmc_Cultivation, //	de_aep_hilling,
			fmc_Harvest, //	de_aep_harvesting,
			fmc_Fertilizer,	//	de_aep_ferti_p3, // N fertilizer I
			fmc_Fertilizer,	//	de_aep_ferti_s3,
			fmc_Fertilizer,	//	de_aep_ferti_p4, // N fertilizer II
			fmc_Fertilizer,	//	de_aep_ferti_s4,
			fmc_Watering, //	de_aep_watering_start,
			fmc_Watering, //	de_aep_watering,
			fmc_Fungicide,	//	de_aep_fungicide1,
			fmc_Fungicide,	//	de_aep_fungicide2,
			fmc_Fungicide,	//	de_aep_fungicide3,
			fmc_Insecticide,	//	de_aep_insecticide1,
			fmc_Insecticide,	//	de_aep_insecticide2,
			fmc_Others,	//	de_aep_cutting_plants,
			fmc_Fertilizer,	//	de_aep_ferti_p1, //organic fertilizer autumn
			fmc_Fertilizer,	//	de_aep_ferti_s1,
			fmc_Cultivation, //	de_aep_harrowing_rows, //de-constructing rows before winter

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_ASPARAGUSESTABLISHEDPLANTATION_H

