//
// FI_StarchPotato.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FI_StarchPotato_h
#define FI_StarchPotato_h

#define FI_SP_BASE 70300

#define FI_SP_DECIDE_TO_HERB		m_field->m_user[5]
#define FI_SP_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  fi_sp_start = 1, // Compulsory, start event must always be 1 (one).
  fi_sp_harvest = FI_SP_BASE,
  fi_sp_stubble_cultivator,
  fi_sp_autumn_plough,
  fi_sp_slurry,
  fi_sp_spring_plough,
  fi_sp_n_minerals1,
  fi_sp_n_minerals2,
  fi_sbe_fertilizer1,
  fi_sbe_harrow,
  fi_sbe_herbicide1,
  fi_sbe_insecticide,
  fi_sbe_herbicide2,
  fi_sbe_herbicide3,
  fi_sbe_fertilizer2,
  fi_sbe_fungicide,
  fi_sbe_autumn_plough2,
} FI_SugarBeetToDo;



class FI_SugarBeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FI_SugarBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(15,11); // 
  }
};

#endif // FI_SugarBeet_h
