/**
\file
\brief
<B>DE_SpringRye.h This file contains the headers for the DE_SpringRye class</B> \n
*/
/**
\file 
 by Chris J. Topping and Elzbieta Ziolkowska \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_SpringRye.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include <Landscape/Farm.h>
#ifndef DE_SPRINGRYE_H
#define DE_SPRINGRYE_H

#define DE_SPRINGRYE_BASE 35100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DE_SR_FUNGII	a_field->m_user[1]


/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_sr_start = 1, // Compulsory, must always be 1 (one).
	de_sr_sleep_all_day = DE_SPRINGRYE_BASE,
	de_sr_stubble_harrow_sandy,
	de_sr_ferti_p1_sandy,
	de_sr_ferti_s1_sandy,
	de_sr_spring_plough_sandy,
	de_sr_stubble_harrow_clay,
	de_sr_winter_plough_clay,
	de_sr_ferti_p2,
	de_sr_ferti_s2,
	de_sr_ferti_p3,
	de_sr_ferti_s3,
	de_sr_preseeding_cultivator,
	de_sr_preseeding_cultivator_sow,
	de_sr_spring_sow,
	de_sr_harrow,
	de_sr_ferti_p4_clay,
	de_sr_ferti_s4_clay,
	de_sr_herbicide1,
	de_sr_fungicide1,
	de_sr_fungicide2,
	de_sr_insecticide1,
	de_sr_growth_regulator1,
	de_sr_harvest,
	de_sr_straw_chopping,
	de_sr_hay_bailing,
	de_sr_foobar, // Obligatory, must be last
} DE_SPRINGRYEToDo;


/**
\brief
DE_SpringRye class
\n
*/
/**
See DE_SpringRye.h::DE_SpringRyeToDo for a complete list of all possible events triggered codes by the Spring Rye management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_SpringRye : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_SpringRye(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date = g_date->DayInYear(31, 10);
		m_forcespringpossible = true;
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_sr_foobar - DE_SPRINGRYE_BASE);
		m_base_elements_no = DE_SPRINGRYE_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others,	// zero element unused but must be here
			 // ALL THE NECESSARY ENTRIES HERE
			 fmc_Others,			//	de_sr_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Others,			//	de_sr_sleep_all_day
			 fmc_Cultivation,	//	de_sr_stubble_harrow_sandy,
			 fmc_Fertilizer,		//	de_sr_ferti_p1_sandy,
			 fmc_Fertilizer,		//	de_sr_ferti_s1_sandy,
			 fmc_Cultivation,	//	de_sr_spring_plough_sandy,
			 fmc_Cultivation,	//	de_sr_stubble_harrow_clay,
			 fmc_Cultivation,	//	de_sr_winter_plough_clay,
			 fmc_Fertilizer,		//	de_sr_ferti_p2,
			 fmc_Fertilizer,		//	de_sr_ferti_s2,
			 fmc_Fertilizer,		//	de_sr_ferti_p3,
			 fmc_Fertilizer,		//	de_sr_ferti_s3,
			 fmc_Cultivation,	//	de_sr_preseeding_cultivator,
			 fmc_Cultivation,	//	de_sr_preseeding_cultivator_sow,
			 fmc_Others,	//	de_sr_spring_sow,
			 fmc_Cultivation,	//	de_sr_harrow,
			 fmc_Fertilizer,		//	de_sr_ferti_p4_clay,
			 fmc_Fertilizer,		//	de_sr_ferti_s4_clay,
			 fmc_Herbicide,		//	de_sr_herbicide1,
			 fmc_Fungicide,		//	de_sr_fungicide1,
			 fmc_Fungicide,		//	de_sr_fungicide2,
			 fmc_Insecticide,		//	de_sr_insecticide1,
			 fmc_Others,		//	de_sr_growth_regulator1,
			 fmc_Harvest,		//	de_sr_harvest,
			 fmc_Others,			//	de_sr_straw_chopping,
			 fmc_Others,			//	de_sr_hay_bailing,

			// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_SPRINGRYE_H

