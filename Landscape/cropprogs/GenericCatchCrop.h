/**
\file
\brief
<B>GenericCatchCrop.h This file contains the headers for the GenericCatchCrop class</B> \n
*/
/**
\file
by Chris J. Topping \n

Version of October 2020 \n
All rights reserved. \n
*/
//
// GenericCatchCrop.h
//


#ifndef GENERIC_CATCH_CROP_BASE_H
#define GENERIC_CATCH_CROP_BASE_H

#define GENERIC_CATCH_CROP_BASE 100000

#define GCCR_ENDDATE a_field->m_user[0]

/** Below is the list of things that a farmer can do if he is growing CatchPeaCrop, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	gccr_start = 1, // Compulsory, must always be 1 (one).
	gccr_sleep_all_day = GENERIC_CATCH_CROP_BASE,
	gccr_sow,
	gccr_harvest,
	gccr_foobar,
} GenericCatchCropToDo;


/**
\brief
GenericCatchCrop class
\n
*/
/**
* See GenericCatchCrop.h:: GenericCatchCropToDo for a complete list of all possible events triggered codes by the CatchPeaCrop management plan. 
* When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class GenericCatchCrop : public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   GenericCatchCrop(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		m_first_date=g_date->DayInYear();
		SetCropClassification(tocc_Catch);
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (gccr_foobar - GENERIC_CATCH_CROP_BASE);
	   m_base_elements_no = GENERIC_CATCH_CROP_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	gccr_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	gccr_sleep_all_day = GENERIC_CATCH_CROP_BASE,
			fmc_Others,	//	gccr_sow,
			fmc_Harvest	//	gccr_harvest,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif //GENERIC_CATCH_CROP_BASE_H

