//
// DK_OMaize.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OMaize.h"


bool DK_OMaize::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  int today = g_date->Date();
  TTypesOfVegetation l_tov = tov_DKOMaize;

  switch ( m_ev->m_todo ) {
  case dk_om_start:
  {
      a_field->ClearManagementActionSum();
      DK_OM_FORCESPRING = false;
      DK_OM_AUTUMN_PLOUGH = false;

      m_last_date = g_date->DayInYear(1, 10); // Should match the last flexdate below
          //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
              // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(30, 9); // last possible day of 1st harvest
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(1, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 0;
      if (StartUpCrop(isSpring, flexdates, int(dk_om_wait))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
      // OK, let's go.
      // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
      if (m_ev->m_forcespring) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_om_wait, false);
          DK_OM_FORCESPRING = true;
          break;
      }
      else SimpleEvent(d1, dk_om_autumn_plough, false);
  }
  break;

  case dk_om_autumn_plough:
      if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
      {
          if (m_ev->m_lock || m_farm->DoIt_prob(0.85)) { // % + date suggestions
              if (!m_farm->AutumnPlough(m_field, 0.0,
                  g_date->DayInYear(30, 11) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_om_autumn_plough, true);
                  break;
              }
              DK_OM_AUTUMN_PLOUGH = true; // we need to remember the farmers that do autumn plough
              SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_om_harrow1, false);
              break;            
          }
      }
      if (a_farm->IsStockFarmer()) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_om_slurry_s, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_om_slurry_p, false);
      break;

  case dk_om_wait:
      if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
      {
          if (m_ev->m_lock || m_farm->DoIt_prob(0.85)) { // % 
              if (!m_farm->SleepAllDay(m_field, 0.0,
                  g_date->DayInYear(2, 2) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_om_wait, true);
                  break;
              }
              DK_OM_AUTUMN_PLOUGH = true;
              SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2), dk_om_harrow1, false);
              break;
               // we need to remember the farmers that do autumn plough
          }
      }
      if (a_farm->IsStockFarmer()) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_om_slurry_s, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_om_slurry_p, false);
      break;
      
  case dk_om_harrow1:
      if (!m_farm->SpringHarrow(m_field, 0.0,
          g_date->DayInYear(2, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_harrow1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_om_harrow2, false);
      break;

  case dk_om_harrow2:
      if (!m_farm->SpringHarrow(m_field, 0.0,
          g_date->DayInYear(3, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_harrow2, true);
          break;
      }
      if (a_farm->IsStockFarmer()) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_om_slurry_s, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_om_slurry_p, false);
      break;

  case dk_om_slurry_s:
      if (!m_farm->FA_Slurry(m_field, 0.0,
          g_date->DayInYear(4, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_slurry_s, true);
          break;
      }
      if (DK_OM_AUTUMN_PLOUGH == true) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), dk_om_strigling1, false);
          break;
      }
      else
      SimpleEvent(g_date->Date() + 1, dk_om_spring_plough, false);
      break;

  case dk_om_slurry_p:
      if (!m_farm->FP_Slurry(m_field, 0.0,
          g_date->DayInYear(4, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_slurry_p, true);
          break;
      }
      if (DK_OM_AUTUMN_PLOUGH == true) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), dk_om_strigling1, false);
          break;
      }
      else
          SimpleEvent(g_date->Date() + 1, dk_om_spring_plough, false);
      break;

  case dk_om_spring_plough:
      if (!m_farm->SpringPlough(m_field, 0.0,
          g_date->DayInYear(5, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_spring_plough, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), dk_om_roll, false);
      break;

  case dk_om_roll:
      if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
      {
          if (!m_farm->SpringRoll(m_field, 0.0,
              g_date->DayInYear(15, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_om_roll, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 5, dk_om_strigling1, false);
      break;

  case dk_om_strigling1:
      if (!m_farm->Strigling(m_field, 0.0,
          g_date->DayInYear(20, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_strigling1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_om_strigling2, false);
      break;

  case dk_om_strigling2:
      if (!m_farm->Strigling(m_field, 0.0,
          g_date->DayInYear(21, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_strigling2, true);
          break;
      }
      SimpleEvent(g_date->Date() + 8, dk_om_ferti_sow, false);
      break;

  case dk_om_ferti_sow:
      if (!m_farm->SpringSowWithFerti(m_field, 0.0,
          g_date->DayInYear(31, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_ferti_sow, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_om_water, false);
      break;

  case dk_om_water:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(1, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_om_water, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_om_strigling3, false);
      break;

  case dk_om_strigling3:
      if (!m_farm->Strigling(m_field, 0.0,
          g_date->DayInYear(2, 6) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_strigling3, true);
          break;
      }
      SimpleEvent(g_date->Date() + 7, dk_om_harrow3, false);
      break;

  case dk_om_harrow3:
      if (!m_farm->SpringHarrow(m_field, 0.0,
          g_date->DayInYear(10, 6) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_harrow3, true);
          break;
      }
      SimpleEvent(g_date->Date() + 7, dk_om_harrow4, false);
      break;

  case dk_om_harrow4:
      if (!m_farm->SpringHarrow(m_field, 0.0,
          g_date->DayInYear(18, 6) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_harrow4, true);
          break;
      }
      SimpleEvent(g_date->Date() + 7, dk_om_row_cultivation1, false);
      break;

  case dk_om_row_cultivation1:
      if (!m_farm->RowCultivation(m_field, 0.0,
          g_date->DayInYear(4, 7) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_row_cultivation1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 14, dk_om_row_cultivation2, false);
      break;

  case dk_om_row_cultivation2:
      if (!m_farm->RowCultivation(m_field, 0.0,
          g_date->DayInYear(20, 7) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_row_cultivation2, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 8), dk_om_harvest, false);
      break;

  case dk_om_harvest:
      // continuous harvesting till 30/9, every 1-2 weeks (included as 10 days here)
      m_farm->FruitHarvest(m_field, 0.0, 0);
      if (today < g_date->OldDays() + g_date->DayInYear(20, 9)) {
          SimpleEvent_(g_date->Date() + 10, dk_om_harvest, true, m_farm, m_field);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_om_straw_chopping, false);
      break;

  case dk_om_straw_chopping:
      if (!m_farm->StrawChopping(m_field, 0.0, 
          m_field->GetMDates(1, 1) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_om_straw_chopping, true);
          break;
      }

    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "DK_OMaize::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
 if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
  return done;
}


