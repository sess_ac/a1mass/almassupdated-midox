/**
\file
\brief
<B>FR_WinterWheat.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of August 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// FR_WinterWheat.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FR_WINTERWHEAT_H
#define FR_WINTERWHEAT_H

#define FR_WW_EVERY_4_YEAR	a_field->m_user[1]

#define FR_WW_BASE 190000


/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  fr_ww_start = 1, // Compulsory, must always be 1 (one).
  fr_ww_sleep_all_day = FR_WW_BASE,
  fr_ww_fertilizer1,
  fr_ww_autumn_plough,
  fr_ww_stubble_cultivation,
  fr_ww_plough,
  fr_ww_stubble_harrow,
  fr_ww_preseed_cultivation,
  fr_ww_sow,
  fr_ww_autumn_harrow,
  fr_ww_herbicide1,
  fr_ww_spring_harrow,
  fr_ww_fertilizer2,
  fr_ww_ammonium_sulphate1,
  fr_ww_ammonium_sulphate2,
  fr_ww_ammonium_sulphate3,
  fr_ww_herbicide2,
  fr_ww_herbicide3,
  fr_ww_fungicide1,
  fr_ww_fungicide2,
  fr_ww_insecticide,
  fr_ww_growth_regulator,
  fr_ww_harvest,
  fr_ww_straw_chopping,
  fr_ww_hay_bailing,
  fr_ww_fertilizer1_y4,
  fr_ww_autumn_plough_y4,
  fr_ww_stubble_cultivation_y4,
  fr_ww_fertilizer2_y4,
  fr_ww_plough_y4,
  fr_ww_stubble_harrow_y4,
  fr_ww_wait,
  fr_ww_foobar,
} FR_WinterWheatToDo;


/**
\brief
FR_WinterWheat class
\n
*/
/**
See FR_WinterWheat.h::FR_WinterWheatToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class FR_WinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   FR_WinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 22,7 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (fr_ww_foobar - FR_WW_BASE);
	   m_base_elements_no = FR_WW_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
					fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fr_ww_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	  fr_ww_sleep_all_day = FR_WW_BASE,
			fmc_Fertilizer,	//	  fr_ww_fertilizer1,
			fmc_Cultivation,	//	  fr_ww_autumn_plough,
			fmc_Cultivation,	//	  fr_ww_stubble_cultivation,
			fmc_Cultivation,	//	  fr_ww_plough,
			fmc_Cultivation,	//	  fr_ww_stubble_cultivation,
			fmc_Cultivation,	//	  fr_ww_preseed_cultivation,
			fmc_Others,	//	  fr_ww_sow,
			fmc_Cultivation,	//	  fr_ww_autumn_harrow,
			fmc_Herbicide,	//	  fr_ww_herbicide1,
			fmc_Cultivation,	//	  fr_ww_spring_harrow,
			fmc_Fertilizer,	//	  fr_ww_fertilizer2,
			fmc_Fertilizer,	//	  fr_ww_ammonium_sulphate1,
			fmc_Fertilizer,	//	  fr_ww_ammonium_sulphate2,
			fmc_Fertilizer,	//	  fr_ww_ammonium_sulphate3,
			fmc_Herbicide,	//	  fr_ww_herbicide2,
			fmc_Herbicide,	//	  fr_ww_herbicide3,
			fmc_Fungicide,	//	  fr_ww_fungicide1,
			fmc_Fungicide,	//	  fr_ww_funigicide2
			fmc_Insecticide,	//	  fr_ww_insecticide,
			fmc_Others,	//	  fr_ww_growth_regulator,
			fmc_Harvest,	//	  fr_ww_harvest,
			fmc_Cutting,	//	  fr_ww_straw_chopping,
			fmc_Others,	//	  fr_ww_hay_bailing,
			fmc_Fertilizer,	//	  fr_ww_fertilizer1,
			fmc_Cultivation,	//	  fr_ww_autumn_plough,
			fmc_Cultivation,	//	  fr_ww_stubble_cultivation,
			fmc_Fertilizer,	//	  fr_ww_fertilizer2,
			fmc_Cultivation,	//	  fr_ww_plough,
			fmc_Cultivation,	//	  fr_ww_stubble_cultivation,
			fmc_Others, // fr_ww_wait

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // FR_WINTERWHEAT_H

