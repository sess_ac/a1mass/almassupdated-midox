//
// DK_FodderBeet.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OFodderBeet.h"


bool DK_OFodderBeet::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	int d1;

	bool done = false;
	TTypesOfVegetation l_tov = tov_DKOFodderBeets;

	switch (m_ev->m_todo)
	{
	case dk_ofb_start:
	{

		a_field->ClearManagementActionSum();

		m_field->SetVegPatchy(true); // Root crop so is open until tall
		DK_OFB_FORCESPRING = false;
		m_last_date = g_date->DayInYear(15, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)


		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(dk_ofb_molluscicide1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(20, 10);
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_ev->m_forcespring) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_ofb_molluscicide1, false);
			DK_OFB_FORCESPRING = true;
			break;
		}
		else SimpleEvent(d1, dk_ofb_autumn_plough, false);
		break;
	}
	break;
	
	case dk_ofb_autumn_plough:
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(20, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_autumn_plough, true);
			break;
		}
		// LKM: Queue up next event - winter harrow, do it after approx. 7 days after
		SimpleEvent(g_date->Date() + 7, dk_ofb_winter_harrow, false);
		break;
		// LKM: do it before the 30th of November - if not done, try again +1 day until the the 30th of November when we succeed - 100% of farmers do this
	case dk_ofb_winter_harrow:
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(27, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_winter_harrow, true);
			break;
		}
		// LKM: Queue up next event - Molluscicide1 after 1st of March next year
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_ofb_molluscicide1, false);
		break;
		// LKM: Do molluscicide1 before 25th of March - if not done, try again +1 day until the the 25th of March when we succeed - done if many slugs/snails (is done before any soil treatments that year) suggests 10% since it is not common
	case dk_ofb_molluscicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10))
		{
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ofb_molluscicide1, true);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date()+1, dk_ofb_ferti_s, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_ofb_ferti_p, false);
		break;
	case dk_ofb_ferti_s:
		if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear (24, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_ferti_s, true);
			break;
		}
		// LKM: Queue up the next event - spring plough (suggests 10% since it is not common) done after the 5th of March and before the 25th of April - if not done, try again +1 day until the 25th of April when we will succeed
		SimpleEvent(g_date->Date() + 1, dk_ofb_spring_plough, false);
		break;

	case dk_ofb_ferti_p:
		if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(24, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_ferti_p, true);
			break;
		}
		// LKM: Queue up the next event - spring plough (suggests 10% since it is not common) done after the 5th of March and before the 25th of April - if not done, try again +1 day until the 25th of April when we will succeed
		SimpleEvent(g_date->Date() + 1, dk_ofb_spring_plough, false);
		break;
	case dk_ofb_spring_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10))
		{
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear()))
			{
				SimpleEvent(g_date->Date() + 1, dk_ofb_spring_plough, true);
				break;
			}
		}
		
		// LKM: Queue up the next event - shallow harrow1 (making seedbed) done after the 28th of March and before the 28th of April - if not done, try again +1 day until the 28th of April when we will succeed
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(28, 3), dk_ofb_sharrow1, false);
		break;
	case dk_ofb_sharrow1:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_ofb_sharrow1, true);
			break;
		}
		// LKM: Queue up the next event - shallow harrow2 (making seedbed) done 7 days after, and before the 3rd of May - if not done, try again +1 day until the 3rd of May when we will succeed
		SimpleEvent(g_date->Date() + 7, dk_ofb_sharrow2, false);
		break;
	case dk_ofb_sharrow2:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(3, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_ofb_sharrow2, true);
			break;
		}
		// LKM: Queue up the next event - shallow harrow3 (making seedbed) done 7 days after, and before the 10th of May - if not done, try again +1 day until the 10th of May when we will succeed
		SimpleEvent(g_date->Date() + 7, dk_ofb_sharrow3, false);
		break;
	case dk_ofb_sharrow3:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_ofb_sharrow3, true);
			break;
		}
		// LKM: Queue up the next event - sow done the day after, and before 11th of May - if not done, try again +1 day until the 11th of May when we will succeed
		SimpleEvent(g_date->Date() + 1, dk_ofb_sow, false);
		break;
	case dk_ofb_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(11, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_ofb_sow, true);
			break;
		}
		// LKM: Queue up the next event - Molluscicide2 the day after sow 
		SimpleEvent(g_date->Date() + 1, dk_ofb_molluscicide2, false);
		break;
	case dk_ofb_molluscicide2:
		// LKM: Do molluscicide2 before 12th of May - if not done, try again +1 day until the 12th of May when we succeed- if many slugs/snails suggests 10% since it is not common
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10))
		{
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(12, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ofb_molluscicide2, true);
				break;
			}
		}
		//Here comes a fork of two - soil treatments and watering
		SimpleEvent(g_date->Date() + 15, dk_ofb_strigling, false); // soil treatment thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_ofb_water1, false); // watering thread
		break;
		// LKM: Here comes soil treatment thread (main thread)  - strigling done 15 days after
	case dk_ofb_strigling:
		//LKM: do strigling before 27th of May - if not done, try again +1 day until the 27th of May when we succeed
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(27, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_strigling, true);
			break;
		}
		// LKM: Queue up the next event - harrow done 15 days after
		SimpleEvent(g_date->Date() + 15, dk_ofb_harrow, false);
		break;
	case dk_ofb_harrow:
		//LKM: do harrow before 11th of June - if not done, try again +1 day until the 11th of June when we succeed
		if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(11, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_harrow, true);
			break;
		}
		// LKM: Queue up the next event - row cultivation1 done 15 days after
		SimpleEvent(g_date->Date() + 15, dk_ofb_row_cultivation1, false);
		break;
	case dk_ofb_row_cultivation1:
		//LKM: do row cultivaton before 26th of June - if not done, try again +1 day until the 26th of June when we succeed
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(26, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_row_cultivation1, true);
			break;
		}
		// LKM: Queue up the next event - row cultivation1 done 15 days after
		SimpleEvent(g_date->Date() + 15, dk_ofb_row_cultivation2, false);
		break;
		// Here comes water thread
	case dk_ofb_water1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 7) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ofb_water1, true);
				break;
			}
		}
			// LKM: Queue up the next event - water2 done after 1st of August
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ofb_water2, false);
			break;
			//LKM: do water2 before 30 of August - if not done, try again +1 day until the the 30 of July when we succeed
	case dk_ofb_water2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 8) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ofb_water2, true);
				break;
			}
		}
		break; 
		//End of thread
	case dk_ofb_row_cultivation2:
		//LKM: do row cultivaton before 11th of July - if not done, try again +1 day until the 11th of July when we succeed
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(11, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_row_cultivation2, true);
			break;
		}
		// LKM: Queue up the next event - row cultivation1 done 15 days after
		SimpleEvent(g_date->Date() + 15, dk_ofb_row_cultivation3, false);
		break;
	case dk_ofb_row_cultivation3:
		//LKM: do row cultivaton before 26th of July - if not done, try again +1 day until the the 26th of July when we succeed
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(26, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_row_cultivation3, true);
			break;
		}
		// LKM: Queue up the next event - manual weeding (remove wild beets) done after 15th of July
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 7), dk_ofb_manualweeding, false);
		break;
		//LKM: do manual weeding before 10th of August - if not done, try again +1 day until the 10th of August when we succeed (need af new function!)
	case dk_ofb_manualweeding:
		if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(10, 8) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_manualweeding, true);
			break;
		}
		// LKM: Queue up the next event - harvest done after the 15th of September
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 9), dk_ofb_harvest, false);
		break;
		// LKM: do harvest before 10th of December - if not done, try again + 1 day until the 10th of December when we succeed
	case dk_ofb_harvest:
		if (!m_farm->HarvestLong(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ofb_harvest, true);
			break;
		}
		m_field->SetVegPatchy(false);
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "DK_OFodderbeet::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}





	


