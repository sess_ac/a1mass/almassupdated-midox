//
// DK_WinterFodderGrass.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_WinterFodderGrass
#define DK_WinterFodderGrass_H

#define DK_WFG_BASE 65200

#define DK_WFG_CC m_field->m_user[0]
#define DK_WFG_CUT m_field->m_user[1]

typedef enum {
	dk_wfg_start = 1, // Compulsory, start event must always be 1 (one).
	dk_wfg_harvest = DK_WFG_BASE,
	dk_wfg_autumn_plough,
	dk_wfg_autumn_roll1,
	dk_wfg_autumn_harrow,
	dk_wfg_autumn_roll2,
	dk_wfg_manure_pig_s,
	dk_wfg_npk_s,
	dk_wfg_manure_pig_p,
	dk_wfg_npk_p,
	dk_wfg_sow,
	dk_wfg_herbicide1,
	dk_wfg_insecticide,
	dk_wfg_sow_spot,
	dk_wfg_cutting1,
	dk_wfg_cutting_graze,
	dk_wfg_cutting2,
	dk_wfg_cutting3,
	dk_wfg_cutting4,
	dk_wfg_grazing,
	dk_wfg_cattle_out,
	dk_wfg_herbicide2,
	dk_wfg_water,
	dk_wfg_swathing,
	dk_wfg_grass_collected,
	dk_wfg_wait,
	dk_wfg_foobar,
} DK_WinterFodderGrassToDo;



class DK_WinterFodderGrass : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_WinterFodderGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_wfg_foobar - DK_WFG_BASE);
		m_base_elements_no = DK_WFG_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_wfg_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_wfg_harvest  = DK_WFG_BASE,
			  fmc_Cultivation,	//	  dk_wfg_autumn_plough,
			  fmc_Cultivation,	//	  dk_wfg_autumn_roll1,
			  fmc_Cultivation,	//	  dk_wfg_autumn_harrow,
			  fmc_Cultivation,	//	  dk_wfg_autumn_roll2,
			  fmc_Fertilizer,	//	  dk_wfg_manure_pig_s,
			  fmc_Fertilizer,	//	  dk_wfg_npk_s,
			  fmc_Fertilizer,	//	  dk_wfg_manure_pig_p,
			  fmc_Fertilizer,	//	  dk_wfg_npk_p,
			  fmc_Others,	//	  dk_wfg_sow,
			  fmc_Herbicide,	//	  dk_wfg_herbicide1,
			  fmc_Insecticide, // dk_wfg_insecticide,
			  fmc_Others, // dk_wfg_sow_spot,
			  fmc_Cutting, // dk_wfg_cutting1,
			  fmc_Cutting, // dk_wfg_cutting_graze,
			  fmc_Cutting, // dk_wfg_cutting2,
			  fmc_Cutting, // dk_wfg_cutting3,
			  fmc_Cutting, // dk_wfg_cutting4,
			  fmc_Grazing, // dk_wfg_grazing,
			  fmc_Grazing, // dk_wfg_cattle_out,
			  fmc_Herbicide,	//	  dk_wfg_herbicide2,
			  fmc_Watering, // dk_wfg_water,
			  fmc_Cutting,	//	  dk_wfg_swathing,
			  fmc_Others, // dk_wfg_grass_collected.,
			  fmc_Others,	//	  dk_wfg_wait,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}

};

#endif // DK_WinterFodderGrass_H

