//
// WinterRape.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WINTERRAPE_H
#define WINTERRAPE_H

// <cropname>_BASE is the first event number to be dumped into the
// debugging log from this crop. *Must* be unique among all crops.
// I suggest steps of 100 between crops.

#define WINTERRAPE_BASE 7800
#define WR_DID_RC_CLEAN     m_field->m_user[0]
#define WR_DID_HERBI_ZERO   m_field->m_user[1]
#define WR_INSECT_DATE      m_field->m_user[2]
#define WR_FUNGI_DATE       m_field->m_user[3]
#define WR_SWARTH_DATE      m_field->m_user[4]
#define WR_HARVEST_DATE     m_field->m_user[5]

typedef enum {
  wr_start = 1, // Compulsory, start event must always be 1 (one).
  wr_ferti_zero = WINTERRAPE_BASE,
  wr_autumn_plough,
  wr_autumn_harrow,
  wr_autumn_sow,
  wr_rowcol_clean,
  wr_herbi_zero,
  wr_ferti_p1,
  wr_ferti_p2,
  wr_ferti_s1,
  wr_ferti_s2,
  wr_herbi_one,
  wr_fungi_one,
  wr_rowcol_one,
  wr_rowcol_one_b,
  wr_insect_one,
  wr_insect_one_b,
  wr_insect_one_c,
  wr_swarth,
  wr_harvest,
  wr_cuttostraw,
  wr_compress,
  wr_stub_harrow,
  wr_grubbing,
  wr_productapplic_one,
  wr_foobar
} WinterRapeToDo;



class WinterRape: public Crop
{
  // Private methods corresponding to the different management steps
  // (when needed).
  void HerbiZero( void );
  void RowcolOne( void );

public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  WinterRape(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date = g_date->DayInYear(24, 8);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (wr_foobar - WINTERRAPE_BASE);
	  m_base_elements_no = WINTERRAPE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//wr_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//wr_ferti_zero = WINTERRAPE_BASE,
		  fmc_Cultivation,//wr_autumn_plough,
		  fmc_Cultivation,//wr_autumn_harrow,
		  fmc_Others,//wr_autumn_sow,
		  fmc_Cultivation,//wr_rowcol_clean,
		  fmc_Herbicide,//wr_herbi_zero,
		  fmc_Fertilizer,//wr_ferti_p1,
		  fmc_Fertilizer,//wr_ferti_p2,
		  fmc_Fertilizer,//wr_ferti_s1,
		  fmc_Fertilizer,//wr_ferti_s2,
		  fmc_Herbicide,//wr_herbi_one,
		  fmc_Fungicide,//wr_fungi_one,
		  fmc_Cultivation,//wr_rowcol_one,
		  fmc_Cultivation,//wr_rowcol_one_b,
		  fmc_Insecticide,//wr_insect_one,
		  fmc_Insecticide,//wr_insect_one_b,
		  fmc_Insecticide,//wr_insect_one_c,
		  fmc_Cutting,//wr_swarth,
		  fmc_Harvest,//wr_harvest,
		  fmc_Others,//wr_cuttostraw,
		  fmc_Others,//wr_compress,
		  fmc_Cultivation,//wr_stub_harrow,
		  fmc_Cultivation,//wr_grubbing,
		  fmc_Others//wr_productapplic_one

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // WINTERRAPE_H
