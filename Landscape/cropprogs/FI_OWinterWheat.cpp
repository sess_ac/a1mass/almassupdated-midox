/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>FI_OWinterWheat.cpp This file contains the source for the FI_WinterWheat class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// FI_OWinterWheat.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FI_OWinterWheat.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_FI_OWinterWheat_SkScrapes("FI_CROP_OWW_SK_SCRAPES",CFG_CUSTOM, false);
extern CfgBool cfg_pest_winterwheat_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_WW_InsecticideDay;
extern CfgInt   cfg_WW_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool FI_OWinterWheat::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case fi_oww_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		FI_OWW_AUTUMN_PLOUGH = false;
		FI_OWW_DECIDE_TO_HERB = 1;
		FI_OWW_DECIDE_TO_FI = 1;

		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
		if (cfg_FI_OWinterWheat_SkScrapes.value()) {
			a_field->m_skylarkscrapes = true;
		}
		else {
			a_field->m_skylarkscrapes = false;
		}
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 3 start and stop dates for all events after harvest for this crop
		int noDates = 2;
		a_field->SetMDates(0, 0, g_date->DayInYear(25, 8)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(29, 8)); // last possible day of straw chopping
		a_field->SetMDates(0, 1, 0); // start day of hay bailing (not used as it depend on previous treatment)
		a_field->SetMDates(1, 1, g_date->DayInYear(29, 8)); // end day of hay bailing
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "FI_OWinterWheat::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "FI_OWinterWheat::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "FI_OWinterWheat::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes (start with the first event on the after winter time)
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 5), fi_oww_fertilizer2, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		// OK, let's go.
		// LKM: Here we queue up the first event - 
		//
		SimpleEvent(d1, fi_oww_slurry, false);
	}
	break;
	// This is the first real farm operation - LKM: done if there is time between 1-20th of August - if after 20th of August, go straight to fertilizer
	case fi_oww_slurry:
		if (a_ev->m_lock || a_farm->DoIt(20)) {
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_oww_slurry, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fi_oww_shallow_plough, false);
		break;
	case fi_oww_shallow_plough: //  shallow plough is coded as autumn plough - may need new function?
		d1 = g_date->Date();
		if (d1 > g_date->OldDays() + g_date->DayInYear(20, 8)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), fi_oww_fertilizer1, false);
			// 
			break;
		} // 5% do the shallow plough followed by harrowing
		else if (a_ev->m_lock || a_farm->DoIt(5)) {
			if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(19, 8) - g_date->DayInYear())) {
				// If we don't succeed on the first try, then try and try again (until 19/8 when we will succeed)
				SimpleEvent(g_date->Date() + 1, fi_oww_shallow_plough, true);
				break;
			}
			// Queue up the next event - in this case harrowing
			SimpleEvent(g_date->Date() + 1, fi_oww_harrow1, false);
			break;
		} // 20% do the stubble cultivator
		else if (a_ev->m_lock || a_farm->DoIt(20)) {
			SimpleEvent(g_date->Date(), fi_oww_stubble_cultivator1, false);
			break;
		}
		// otherwise do fertilizer
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), fi_oww_fertilizer1, false);
		break;
	case fi_oww_harrow1:
		if (!a_farm->AutumnHarrow(a_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			// If we don't succeed on the first try, then try and try again (until 20/8 when we will succeed)
			SimpleEvent(g_date->Date() + 1, fi_oww_harrow1, true);
			break;
		}
		// Queue up the next event - in this case harrowing again 1-2 days after
		SimpleEvent(g_date->Date() + 1, fi_oww_harrow2, false);
		break;
	case fi_oww_harrow2:
		if (!a_farm->AutumnHarrow(a_field, 0.0, g_date->DayInYear(22, 8) - g_date->DayInYear())) {
			// If we don't succeed on the first try, then try and try again (until 22/8 when we will succeed)
			SimpleEvent(g_date->Date() + 1, fi_oww_harrow2, true);
			break;
		}
		// Queue up the next event - in this case slurry
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), fi_oww_fertilizer1, false);
		break;
	case fi_oww_stubble_cultivator1:
		if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			// If we don't succeed on the first try, then try and try again (until 20/8 when we will succeed)
			SimpleEvent(g_date->Date() + 1, fi_oww_stubble_cultivator1, true);
			break;
		}
		// Queue up the next event - in this case stubble cultivator again a week after
		SimpleEvent(g_date->Date() + 7, fi_oww_stubble_cultivator2, false);
		break;
	case fi_oww_stubble_cultivator2:
		if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(28, 8) - g_date->DayInYear())) {
			// If we don't succeed on the first try, then try and try again (until 28/8 when we will succeed)
			SimpleEvent(g_date->Date() + 1, fi_oww_stubble_cultivator2, true);
			break;
		}
		// Queue up the next event - in this case fertilizer
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), fi_oww_fertilizer1, false);
		break;
	case fi_oww_fertilizer1:
		if (a_ev->m_lock || a_farm->DoIt(80)) {
			if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(11, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_oww_fertilizer1, true);
				break;
			}
		}
			// Queue up the next event - in this case stubble cultivator again
			SimpleEvent(g_date->Date(), fi_oww_stubble_cultivator3, false);
			break;
	case fi_oww_stubble_cultivator3:
		if (a_ev->m_lock || a_farm->DoIt_prob(.20)) { // 20% do stubble cultivator3
			if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_oww_stubble_cultivator3, true);
				break;
			}
			SimpleEvent(g_date->Date(), fi_oww_sow, false);
			break;
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.30 / .80)) {
			SimpleEvent(g_date->Date(), fi_oww_preseeding_cultivator, false);
			break;
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.30 / .50)) {
			SimpleEvent(g_date->Date(), fi_oww_cultivation_seeding, false);
			break;
		}
		else SimpleEvent(g_date->Date(), fi_oww_sow, false);
		break;

	case fi_oww_preseeding_cultivator:
		if (!a_farm->PreseedingCultivator(a_field, 0.0,
			g_date->DayInYear(13, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_oww_preseeding_cultivator, true);
			break;
		}
		// Queue up the next event - in this case sow
		SimpleEvent(g_date->Date(), fi_oww_sow, false);
		break;
	case fi_oww_cultivation_seeding:
		if (!a_farm->PreseedingCultivatorSow(a_field, 0.0,
			g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_oww_cultivation_seeding, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, fi_oww_harrow3, false);
		break;
	case fi_oww_sow:
		if (!a_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_oww_sow, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, fi_oww_harrow3, false);
		break;
	case fi_oww_harrow3: // Don't know the % here
		if (!a_farm->AutumnHarrow(a_field, 0.0,
			g_date->DayInYear(22, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_oww_harrow3, true);
			break;
		}
		//Winter time!
		//Queue up next event (next year) - in this case fertilizer2:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 5) + 365, fi_oww_fertilizer2, false);
		break;
case fi_oww_fertilizer2:
		if (a_ev->m_lock || a_farm->DoIt(60)) { //60% do a foliar fertilizer with microelements
			if (!a_farm->FP_NPKS(a_field, 0.0,
				g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_oww_fertilizer2, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 8), fi_oww_harvest, false); 
		break;
	case fi_oww_harvest:
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0,
			g_date->DayInYear(25, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_oww_harvest, true);
			break;
		}
		// 100% of farmers will do straw chopping
		SimpleEvent(g_date->Date() + 1, fi_oww_straw_chopping, false);
		break;
	case fi_oww_straw_chopping:
		if (a_field->GetMConstants(0) == 0) {
			if (!a_farm->StrawChopping(a_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "FI_OWinterWheat::Do(): failure in 'StrawChopping' execution", "");
				exit(1);
			}
		}
		SimpleEvent(g_date->Date(), fi_oww_hay_bailing, false);
		break;
	case fi_oww_hay_bailing:
		if (a_ev->m_lock || a_farm->DoIt(5)) { //5% do hay bailing
			if (a_field->GetMConstants(1) == 0) {
				if (!a_farm->HayBailing(a_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "FI_OWinterWheat::Do(): failure in 'HayBailing' execution", "");
					exit(1);
				}
			}
		}
		else {
			done = true;
		}
		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "FI_OWinterWheat::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
		return done;
}