/**
\file
\brief
<B>UKBeans.h This file contains the headers for the Beans class</B> \n
*/
/**
\file 
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKBeans.h
//


#ifndef UKBEANS_H
#define UKBEANS_H

#define UKBEANS_BASE 45000
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing beans, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	uk_bns_start = 1, // Compulsory, must always be 1 (one).
	uk_bns_sleep_all_day = UKBEANS_BASE,
	uk_bns_autumn_harrow, // Stubble cultivator / harrow 50% September
	uk_bns_autumn_plough, // Plough 50% September-October (50% who did not autumn harrow)
	uk_bns_preseeding_cultivator, // Preseeding cultivator 100% October-November (if autumn harrow)
	uk_bns_autumn_sowdd, // Sow 100% October-November (drill if autumn harrow)
	uk_bns_autumn_sowb, // Sow 100% October-November (broadcast if no autumn harrow)
	uk_bns_winter_plough, // Plough 100% October-Novemeber (if no autumn harrow. this ploughs in the big seed which were broadcast)
	uk_bns_ferti_p1, // PK 100% October-December
	uk_bns_ferti_s1,
	uk_bns_herbicide1, // Herbicide1 100% October-December (pre-em)
	uk_bns_herbicide2, // Herbicide2 10% February-April (post-em & last resort)
	uk_bns_fungicide1, // Fungicide1 100% April-May
	uk_bns_insecticide1, // Insecticide1 80% March (for Brucidae)
	uk_bns_insecticide2, // Insecticide2 80% May (for Aphidae)
	uk_bns_insecticide3, // Insecticide3 80% June (for Brucidae)
	uk_bns_harvest, // Harvest 100% September-October
	uk_bns_foobar,
} UKBeansToDo;

/**
\brief
UKBeans class
\n
*/
/**
See UKBeans.h::UKBeansToDo for a complete list of all possible events triggered codes by the beans management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKBeans: public Crop
{
 public:
	 virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	 UKBeans(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	 {
		// When we start it off, the first possible date for a farm operation is 1st September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10 );
		SetUpFarmCategoryInformation();
	 }
	 void SetUpFarmCategoryInformation() {
		 const int elements = 2 + (uk_bns_foobar - UKBEANS_BASE);
		 m_base_elements_no = UKBEANS_BASE - 2;
		 FarmManagementCategory catlist[elements] =
		 {
			  fmc_Others, // this needs to be at the zero line
			  fmc_Others,//uk_bns_start = 1, // Compulsory, must always be 1 (one).
			  fmc_Others,//uk_bns_sleep_all_day = UKBEANS_BASE,
			  fmc_Cultivation,//uk_bns_autumn_harrow, // Stubble cultivator / harrow 50% September
			  fmc_Cultivation,//uk_bns_autumn_plough, // Plough 50% September-October (50% who did not autumn harrow)
			  fmc_Cultivation,//uk_bns_preseeding_cultivator, // Preseeding cultivator 100% October-November (if autumn harrow)
			  fmc_Others,//uk_bns_autumn_sowdd, // Sow 100% October-November (drill if autumn harrow)
			  fmc_Others,//uk_bns_autumn_sowb, // Sow 100% October-November (broadcast if no autumn harrow)
			  fmc_Cultivation,//uk_bns_winter_plough, // Plough 100% October-Novemeber (if no autumn harrow. this ploughs in the big seed which were broadcast)
			  fmc_Fertilizer,//uk_bns_ferti_p1, // PK 100% October-December
			  fmc_Fertilizer,//uk_bns_ferti_s1,
			  fmc_Herbicide,//uk_bns_herbicide1, // Herbicide1 100% October-December (pre-em)
			  fmc_Herbicide,//uk_bns_herbicide2, // Herbicide2 10% February-April (post-em & last resort)
			  fmc_Fungicide,//uk_bns_fungicide1, // Fungicide1 100% April-May
			  fmc_Insecticide,//uk_bns_insecticide1, // Insecticide1 80% March (for Brucidae)
			  fmc_Insecticide,//uk_bns_insecticide2, // Insecticide2 80% May (for Aphidae)
			  fmc_Insecticide,//uk_bns_insecticide3, // Insecticide3 80% June (for Brucidae)
			  fmc_Harvest,//uk_bns_harvest, // Harvest 100% September-October
		 };
		 copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	 }
};

#endif // UKBEANS_H

