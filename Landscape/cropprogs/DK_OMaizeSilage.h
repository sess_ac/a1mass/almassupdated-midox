//
// DK_OMaize_Silage.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OMaizeSilage_H
#define DK_OMaizeSilage_H

#define DK_OMS_BASE 69200
#define DK_OMS_FORCESPRING	a_field->m_user[1]
#define DK_OMS_AUTUMN_PLOUGH	a_field->m_user[2]

typedef enum {
	dk_oms_start = 1, // Compulsory, start event must always be 1 (one).
	dk_oms_autumn_plough = DK_OMS_BASE,
	dk_oms_harrow1,
	dk_oms_harrow2,
	dk_oms_slurry_s,
	dk_oms_slurry_p,
	dk_oms_spring_plough,
	dk_oms_roll,
	dk_oms_strigling1,
	dk_oms_strigling2,
	dk_oms_ferti_sow,
	dk_oms_water,
	dk_oms_strigling3,
	dk_oms_harrow3,
	dk_oms_harrow4,
	dk_oms_row_cultivation1,
	dk_oms_row_cultivation2,
	dk_oms_harvest,
	dk_oms_straw_chopping,
	dk_oms_wait,
	dk_oms_foobar
} DK_OMaizeSilageToDo;



class DK_OMaizeSilage : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OMaizeSilage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1,12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_oms_foobar - DK_OMS_BASE);
		m_base_elements_no = DK_OMS_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_oms_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation, //dk_oms_autumn_plough = DK_OMS_BASE,
			fmc_Cultivation, //dk_oms_harrow1,
			fmc_Cultivation, //dk_oms_harrow2,
			fmc_Fertilizer, // dk_oms_slurry_s,
			fmc_Fertilizer, //  dk_oms_slurry_p,
			fmc_Cultivation, // dk_oms_spring_plough,
			fmc_Others, // dk_oms_roll,
			fmc_Cultivation, // dk_oms_strigling1,
			fmc_Cultivation, // dk_oms_strigling2,
			fmc_Cultivation, // dk_oms_ferti_sow,
			fmc_Watering, //dk_oms_water,
			fmc_Cultivation, //  dk_oms_strigling3,
			fmc_Cultivation, //  dk_oms_harrow3,
			fmc_Cultivation, //  dk_oms_harrow4,
			fmc_Cultivation, //  dk_oms_row_cultivation1,
			fmc_Cultivation, //  dk_oms_row_cultivation2,
			fmc_Harvest, // dk_oms_harvest,
			fmc_Cutting, // dk_oms_straw_chopping,
			fmc_Others, // dk_oms_wait,
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_OMaizeSilage_H
