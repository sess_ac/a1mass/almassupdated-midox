/**
\file
\brief
<B>FI_WinterRye.h This file contains the headers for the WinterRye class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// WinterRye.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FI_WINTERRYE_H
#define FI_WINTERRYE_H

#define FI_WR_BASE 71300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define FI_WR_AUTUMN_PLOUGH        a_field->m_user[1]
#define FI_WR_DECIDE_TO_HERB       a_field->m_user[2]
#define FI_WR_DECIDE_TO_FI         a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing winter rye, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  fi_wr_start = 1, // Compulsory, must always be 1 (one).
  fi_wr_sleep_all_day = FI_WR_BASE,
  fi_wr_plough,
  fi_wr_stubble_cultivator1,
  fi_wr_stubble_cultivator2,
  fi_wr_harrow1,
  fi_wr_harrow2,
  fi_wr_slurry,
  fi_wr_fertilizer1,
  fi_wr_herbicide1,
  fi_wr_stubble_cultivator3,
  fi_wr_preseeding_cultivator,
  fi_wr_cultivation_seeding,
  fi_wr_sow,
  fi_wr_harrow3,
  fi_wr_herbicide2,
  fi_wr_fungicide1,
  fi_wr_insecticide1,
  fi_wr_ammonium_sulphate,
  fi_wr_herbicide3,
  fi_wr_fertilizer2,
  fi_wr_fungicide2,
  fi_wr_insecticide2,
  fi_wr_growth_reg1,
  fi_wr_growth_reg2,
  fi_wr_harvest,
  fi_wr_straw_chopping,
  fi_wr_hay_bailing,
  fi_wr_foobar,
} FI_WinterRyeToDo;


/**
\brief
FI_WinterRye class
\n
*/
/**
See FI_WinterRye.h::FI_WinterRyeToDo for a complete list of all possible events triggered codes by the winter rye management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class FI_WinterRye: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   FI_WinterRye(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,9 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (fi_wr_foobar - FI_WR_BASE);
	   m_base_elements_no = FI_WR_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fi_wr_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	  fi_wr_sleep_all_day = FI_WR_BASE,
			fmc_Cultivation,	//	  fi_wr_shallow_plough,
			fmc_Cultivation,	//	  fi_wr_stubble_cultivator1,
			fmc_Cultivation,	//	  fi_wr_stubble_cultivator2,
			fmc_Cultivation,	//	  fi_wr_harrow1,
			fmc_Cultivation,	//	  fi_wr_harrow2,
			fmc_Fertilizer,	//	  fi_wr_slurry,
			fmc_Fertilizer,	//	  fi_wr_fertilizer1,
			fmc_Herbicide,	//	  fi_wr_herbicide1,
			fmc_Cultivation,	//	  fi_wr_stubble_cultivator3,
			fmc_Cultivation,	//	  fi_wr_preseeding_cultivator,
			fmc_Cultivation,	//	  fi_wr_cultivation_seeding,
			fmc_Others,	//	  fi_wr_sow,
			fmc_Cultivation,	//	  fi_wr_harrow3,
			fmc_Herbicide,	//	  fi_wr_herbicide2,
			fmc_Fungicide,	//	  fi_wr_fungicide1,
			fmc_Insecticide,	//	  fi_wr_insecticide1,
			fmc_Fertilizer,	//	  fi_wr_ammonium_sulphate,
			fmc_Herbicide,	//	  fi_wr_herbicide3,
			fmc_Fertilizer,	//	  fi_wr_fertilizer2,
			fmc_Fungicide,	//	  fi_wr_fungicide2,
			fmc_Insecticide,	//	  fi_wr_insecticide2,
			fmc_Others,	//	  fi_wr_growth_reg1,
			fmc_Others,	//	  fi_wr_growth_reg2,
			fmc_Harvest,	//	  fi_wr_harvest,
			fmc_Others,	//	  fi_wr_straw_chopping,
			fmc_Others	//	  fi_wr_hay_bailing,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // FI_WINTERRYE_H

