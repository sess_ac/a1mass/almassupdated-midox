/**
\file
\brief
<B>UKSpringBarley.h This file contains the headers for the SpringBarley class</B> \n
*/
/**
\file
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKSpringBarley.h
//


#ifndef UKSPRINGBARLEY_H
#define UKSPRINGBARLEY_H

#define UKSPRINGBARLEY_BASE 45500
/**
\brief A flag used to indicate autumn ploughing status
*/
#define UK_SB_FUNGII	a_field->m_user[1]


/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	uk_sb_start = 1, // Compulsory, must always be 1 (one).
	uk_sb_sleep_all_day = UKSPRINGBARLEY_BASE,
	uk_sb_autumn_harrow, // Stubble cultivator 60% September-October
	uk_sb_autumn_plough, // Autumn plough 50% October-December
	uk_sb_ferti_s1, // Slurry 40% March if no autumn plough
	uk_sb_ferti_p1,
	uk_sb_spring_plough, //Spring plough 20% March if Slurry
	uk_sb_ferti_s2, // N fertiliser 85% March
	uk_sb_ferti_p2,
	uk_sb_ferti_s3, // NPK application 50% before preseeding
	uk_sb_ferti_p3,
	uk_sb_preseeding_cultivator, // Preseeding cultivation 100% April
	uk_sb_spring_sow, // Spring sow 100% April
	uk_sb_harrow, // Harrow 20% pre-em April
	uk_sb_herbicide1, // Herbicide 1 90% April
	uk_sb_fungicide1, // Fungicide 1 80% June
	uk_sb_fungicide2, // Fungicide 2 40% 14-21 days after f1
	uk_sb_insecticide1, // Insecticide 1 30% June
	uk_sb_growth_regulator1, // Growth Regulator 1 40% May-June
	uk_sb_harvest, // Harvest 100% July-August
	uk_sb_straw_chopping, // Straw chopping 50% after harvest
	uk_sb_hay_bailing, // Hay bailing 50% after harvest
	uk_sb_foobar,
} UKSpringBarleyToDo;


/**
\brief
UKSpringBarley class
\n
*/
/**
See UKSpringBarley.h::UKSpringBarleyToDo for a complete list of all possible events triggered codes by the spring barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKSpringBarley: public Crop
{
 public:
	 virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	 UKSpringBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,10 );
		SetUpFarmCategoryInformation();
	 }
	 void SetUpFarmCategoryInformation() {
		 const int elements = 2 + (uk_sb_foobar - UKSPRINGBARLEY_BASE);
		 m_base_elements_no = UKSPRINGBARLEY_BASE - 2;
		 FarmManagementCategory catlist[elements] =
		 {
			  fmc_Others, // this needs to be at the zero line
			  fmc_Others,//uk_sb_start = 1, // Compulsory, must always be 1 (one).
			  fmc_Others,//uk_sb_sleep_all_day = UKSPRINGBARLEY_BASE,
			  fmc_Cultivation,//uk_sb_autumn_harrow, // Stubble cultivator 60% September-October
			  fmc_Cultivation,//uk_sb_autumn_plough, // Autumn plough 50% October-December
			  fmc_Fertilizer,//uk_sb_ferti_s1, // Slurry 40% March if no autumn plough
			  fmc_Fertilizer,//uk_sb_ferti_p1,
			  fmc_Cultivation,//uk_sb_spring_plough, //Spring plough 20% March if Slurry
			  fmc_Fertilizer,//uk_sb_ferti_s2, // N fertiliser 85% March
			  fmc_Fertilizer,//uk_sb_ferti_p2,
			  fmc_Fertilizer,//uk_sb_ferti_s3, // NPK application 50% before preseeding
			  fmc_Fertilizer,//uk_sb_ferti_p3,
			  fmc_Cultivation,//uk_sb_preseeding_cultivator, // Preseeding cultivation 100% April
			  fmc_Others,//uk_sb_spring_sow, // Spring sow 100% April
			  fmc_Cultivation,//uk_sb_harrow, // Harrow 20% pre-em April
			  fmc_Herbicide,//uk_sb_herbicide1, // Herbicide 1 90% April
			  fmc_Fungicide,//uk_sb_fungicide1, // Fungicide 1 80% June
			  fmc_Fungicide,//uk_sb_fungicide2, // Fungicide 2 40% 14-21 days after f1
			  fmc_Insecticide,//uk_sb_insecticide1, // Insecticide 1 30% June
			  fmc_Others,//uk_sb_growth_regulator1, // Growth Regulator 1 40% May-June
			  fmc_Harvest,//uk_sb_harvest, // Harvest 100% July-August
			  fmc_Others,//uk_sb_straw_chopping, // Straw chopping 50% after harvest
			  fmc_Others,//uk_sb_hay_bailing, // Hay bailing 50% after harvest
		 };
		 copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	 }
};

#endif // UKSPRINGBARLEY_H

