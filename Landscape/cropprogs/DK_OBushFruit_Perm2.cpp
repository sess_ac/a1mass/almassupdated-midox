/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OBushFruit_Perm2.cpp This file contains the source for the DK_OBushFruit_Perm2 on blackcurrant  </B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of January 2023 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OBushFruit_Perm2.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OBushFruit_Perm2.h"

// Some things that are defined externally - in this case these variables allow


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_OBushFruit_Perm2::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DKOBushFruit_Perm2;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_obfp2_start:
	{
		// dk_obfp2_start just sets up all the starting conditions and reference dates that are needed to start a dk_bfp1
		DK_OBFP2_AFTER_EST = false;

		m_last_date = g_date->DayInYear(31, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 12); // last possible day of cutting orch
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(365, flexdates, int(dk_obfp2_sleep_all_day))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		// Here we queue up the first event
		//Each field has assign randomly a DK_och_YEARS_AFTER_PLANT 

		if ((DK_OBFP2_AFTER_EST + g_date->GetYearNumber()) % 10 == 0) // establishment
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + 365;
			SimpleEvent(d1, dk_obfp2_sleep_all_day, false);
		}
		else if ((DK_OBFP2_AFTER_EST + g_date->GetYearNumber()) % 10 == 1) // 1st year after est - no harvest
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + 365;
			SimpleEvent(d1, dk_obfp2_molluscicide2, false);
		}
		else
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + 365; // harvest years
			SimpleEvent(d1, dk_obfp2_molluscicide4, false);
		}
		break;

	}
	break;


	// LKM: This is the first real farm operation - wait
	case dk_obfp2_sleep_all_day:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_sleep_all_day, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_obfp2_molluscicide1, false);
		break;

	case dk_obfp2_molluscicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.1)) { // suggests 10%
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(26, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_molluscicide1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_autumn_plough, false);
		break;

	case dk_obfp2_autumn_plough:
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(27, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_autumn_plough, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_autumn_harrow, false);
		break;

	case dk_obfp2_autumn_harrow:
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(28, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_autumn_harrow, true);
			break;
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s1, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p1, false);
		break;

	case dk_obfp2_ferti_s1: 
		if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(29, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_plant, false);
		break;

	case dk_obfp2_ferti_p1:
		if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(29, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_plant, false);
		break;

	case dk_obfp2_plant: // plant 1-year bushes
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_plant, true);
			break;
		}
		done = true; // end of est autumn
		break;

	case dk_obfp2_molluscicide2: // start of 1st year after est - no harvest
		if (m_ev->m_lock || m_farm->DoIt_prob(0.1)) { // suggests 10%
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_molluscicide2, true);
				break;
			}
		}
		//fork of events:
		if (a_farm->IsStockFarmer()) { // ferti thread
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s2, false);
			SimpleEvent(g_date->Date() + 14, dk_obfp2_row_cultivation1, false); // weeding thread 
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_molluscicide3, false); // mollusci thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_water1, false); // water thread - main
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p2, false);
		SimpleEvent(g_date->Date() + 14, dk_obfp2_row_cultivation1, false); // weeding thread 
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_molluscicide3, false); // mollusci thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_water1, false); // water thread - main
		break;

	case dk_obfp2_ferti_s2:  
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 30, dk_obfp2_ferti_s3, false);
		break;

	case dk_obfp2_ferti_p2: 
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 30, dk_obfp2_ferti_p3, false);
		break;

	case dk_obfp2_ferti_s3:  
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s3, true);
			break;
		}
		break; // end of thread

	case dk_obfp2_ferti_p3: 
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p3, true);
			break;
		}
		break; // end of thread

	case dk_obfp2_row_cultivation1:
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_manual_weeding1, false);
		break;

	case dk_obfp2_manual_weeding1:
		if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_manual_weeding1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, dk_obfp2_row_cultivation2, false);
		break;

	case dk_obfp2_row_cultivation2:
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_manual_weeding2, false);
		break;

	case dk_obfp2_manual_weeding2:
		if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_manual_weeding2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, dk_obfp2_row_cultivation3, false);
		break;

	case dk_obfp2_row_cultivation3:
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(16, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_manual_weeding3, false);
		break;

	case dk_obfp2_manual_weeding3:
		if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(17, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_manual_weeding3, true);
			break;
		}
		break; // end of thread

	case dk_obfp2_molluscicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.1)) { // suggests 10%
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_molluscicide3, true);
				break;
			}
		}
		break; // end of thread

	case dk_obfp2_water1: // 100% of sandy soils
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_water1, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_obfp2_water2, false);
			break;
		}
		else
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_water1_clay, false);
		break;

	case dk_obfp2_water1_clay: // suggests 50% of clay soils
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_water1_clay, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_obfp2_water2_clay, false);
		break;

	case dk_obfp2_water2: // 100% of sandy soils,
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_water2, true);
			break;
		}
		done = true; // end of 1st year after est
		break;

	case dk_obfp2_water2_clay: // 50% of clay
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_water2_clay, true);
				break;
			}
		}
		done = true; // end of 1st year after est
		break;

	case dk_obfp2_wait: // 50% of clay
		if (!m_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_wait, true);
			break;
		}
		done = true; // end of 1st year after est
		break;

	case dk_obfp2_molluscicide4: // start of harvest years
		if (m_ev->m_lock || m_farm->DoIt_prob(0.1)) { // suggests 10%
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_molluscicide4, true);
				break;
			}
		}
		//fork of events:
		if (a_farm->IsStockFarmer()) { // ferti thread
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s4, false);
			SimpleEvent(g_date->Date() + 14, dk_obfp2_cutting1, false); // weeding / cutting thread 
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_molluscicide5, false); // mollusci thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_water3, false); // water thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 7), dk_obfp2_harvest, false); // harvest - main thread
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p4, false);
		SimpleEvent(g_date->Date() + 14, dk_obfp2_cutting1, false); // weeding / cutting thread 
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_molluscicide5, false); // mollusci thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_water3, false); // water thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 7), dk_obfp2_harvest, false); // harvest - main thread
		break;

	case dk_obfp2_ferti_s4:
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s4, true);
			break;
		}
		break;

	case dk_obfp2_ferti_p4: 
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p4, true);
			break;
		}
		break; // end of thread

	case dk_obfp2_cutting1: // cutting grass between rows
		if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_cutting1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, dk_obfp2_cutting2, false);
		break;

	case dk_obfp2_cutting2:
		if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_cutting2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, dk_obfp2_cutting3, false);
		break;

	case dk_obfp2_cutting3:
		if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(16, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_cutting3, true);
			break;
		}
		break; // end of thread

	case dk_obfp2_molluscicide5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.1)) { // suggests 10%
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_molluscicide5, true);
				break;
			}
		}
		break; // end of thread

	case dk_obfp2_water3: // 100% of sandy soils
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_water3, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_obfp2_water4, false);
			break;
		}
		else
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_water3_clay, false);
		break;

	case dk_obfp2_water3_clay: // suggests 50% of clay soils
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_water3_clay, true);
				break;
			}
		}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_obfp2_water4_clay, false);
			break;

	case dk_obfp2_water4: // 100% of sandy soils
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_water4, true);
			break;
		}
		break;

	case dk_obfp2_water4_clay: // 50% of clay
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_water4_clay, true);
				break;
			}
		}
		break;

	case dk_obfp2_harvest:
		if (!m_farm->HarvestBF_Machine(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_harvest, true);
			break;
		}
		if (a_farm->IsStockFarmer()) { // ferti thread
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s5, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p5, false);
		break;
	case dk_obfp2_ferti_s5:  
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(2, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_s5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_pruning, false);
		break;

	case dk_obfp2_ferti_p5: 
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(2, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_ferti_p5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_pruning, false);
		break;

	case dk_obfp2_pruning:
		if (!m_farm->Pruning(m_field, 0.0, g_date->DayInYear(3, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_pruning, true);
			break;
		}
		done = true; // end of harvest year
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop 
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DK_OBushFruit_Perm2::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}