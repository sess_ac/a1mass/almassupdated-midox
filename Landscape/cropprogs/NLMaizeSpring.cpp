/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLMaizeSpring.cpp This file contains the source for the NLMaizeSpring class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLMaizeSpring.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLMaizeSpring.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_springwheat_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_MS_InsecticideDay;
extern CfgInt   cfg_MS_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional maize.
*/
bool NLMaizeSpring::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLMaizeSpring;
	int l_nextcropstartdate;
	/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case nl_ms_start:
	{
		NL_MS_START_FERTI = false;
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(10, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(5, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(10, 10);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(nl_ms_spring_sow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 4) + isSpring;
		// OK, let's go.
		if (m_field->GetSoilType() == 2 || m_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			SimpleEvent_(d1, nl_ms_spring_plough_sandy, false, m_farm, m_field);
		}
		else
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, nl_ms_preseeding_cultivator, false, m_farm, m_field);
		}
		break;
	}
	break;

	// This is the first real farm operation
	case nl_ms_spring_plough_sandy:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_spring_plough_sandy, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 10, nl_ms_preseeding_cultivator, false, m_farm, m_field);
		break;
	case nl_ms_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(4, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_ms_spring_sow_with_ferti, false, m_farm, m_field);
		break;
	case nl_ms_spring_sow_with_ferti:
		// 75% will do sow with fertilizer 
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->SpringSowWithFerti(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ms_spring_sow_with_ferti, true, m_farm, m_field);
				break;
			}
			else
			{
				// 75% of farmers will do this, but the other 25% won't so we need to remember whether we are in one or the other group
				NL_MS_START_FERTI = true;
				// Here is a fork leading to parallel events
				SimpleEvent_(g_date->Date() + 3, nl_ms_harrow, false, m_farm, m_field);
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_ms_herbicide1, false, m_farm, m_field); // Herbidide thread
				if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), nl_ms_ferti_s2, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), nl_ms_ferti_p2, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_ms_spring_sow, false, m_farm, m_field);
		break;
	case nl_ms_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to parallel events
		SimpleEvent_(g_date->Date() + 3, nl_ms_harrow, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_ms_herbicide1, false, m_farm, m_field); // Herbidide thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), nl_ms_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), nl_ms_ferti_p2, false, m_farm, m_field);
		break;
	case nl_ms_ferti_p2:
		// Here comes N thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60) && (NL_MS_START_FERTI == 0))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ms_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), nl_ms_harvest, false, m_farm, m_field);
		break;
	case nl_ms_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60) && (NL_MS_START_FERTI == 0))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ms_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), nl_ms_harvest, false, m_farm, m_field);
		break;
	case nl_ms_herbicide1:
		// Here comes the herbicide thread
		if (m_field->GetGreenBiomass() <= 0)
		{
			SimpleEvent_(g_date->Date() + 1, nl_ms_herbicide1, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_ms_herbicide1, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case nl_ms_harrow:
		// Here comes the MAIN thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.15))
		{
			if (m_field->GetGreenBiomass() <= 0)
			{
				if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_ms_harrow, true, m_farm, m_field);
					break;
				}
			}
			else { SimpleEvent_(g_date->Date() + 1, nl_ms_harrow, true, m_farm, m_field); }
		}
		// End of thread
		break;
	case nl_ms_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_ms_straw_chopping, false, m_farm, m_field);
		break;
	case nl_ms_straw_chopping:
		if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_straw_chopping, true, m_farm, m_field);
			break;
		}
		// So we are done,but this crop uses a catch crop
		l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
		m_field->SetVegPatchy(false); // reverse the patchy before the next crop
		m_farm->AddNewEvent(tov_NLCatchCropPea, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
		m_field->SetVegType(tov_NLCatchCropPea, tov_Undefined); //  Two vegetation curves are specified 
		// NB no "done = true" because this crop effectively continues into the catch crop.
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLMaizeSpring::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}