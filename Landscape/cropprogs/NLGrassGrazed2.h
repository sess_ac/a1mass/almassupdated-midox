/**
\file
\brief
<B>NLGrassGrazed2.h This file contains the headers for the TemporalGrassGrazed2 class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLGrassGrazed2.h
//


#ifndef NLGRASSGRAZED2_H
#define NLGRASSGRAZED2_H

#define NLGRASSGRAZED2_BASE 21100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_GG2_FERTI_DATE		a_field->m_user[1]
#define NL_GG2_CUT_DATE		a_field->m_user[2]
#define NL_GG2_WATER_DATE		a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing TemporalGrassGrazed2, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_gg2_start = 1, // Compulsory, must always be 1 (one).
	nl_gg2_sleep_all_day = NLGRASSGRAZED2_BASE,
	nl_gg2_ferti_p1,
	nl_gg2_ferti_s1,
	nl_gg2_cut_to_silage1,
	nl_gg2_cut_to_silage2,
	nl_gg2_cut_to_silage3,
	nl_gg2_cut_to_silage4,
	nl_gg2_cut_to_silage5,
	nl_gg2_cut_to_silage6,
	nl_gg2_ferti_p2,
	nl_gg2_ferti_s2,
	nl_gg2_ferti_p3,
	nl_gg2_ferti_s3,
	nl_gg2_ferti_p4,
	nl_gg2_ferti_s4,
	nl_gg2_ferti_p5,
	nl_gg2_ferti_s5,
	nl_gg2_ferti_p6,
	nl_gg2_ferti_s6,
	nl_gg2_ferti_p7,
	nl_gg2_ferti_s7,
	nl_gg2_ferti_p8,
	nl_gg2_ferti_s8,
	nl_gg2_ferti_p9,
	nl_gg2_ferti_s9,
	nl_gg2_ferti_p10,
	nl_gg2_ferti_s10,
	nl_gg2_ferti_p11,
	nl_gg2_ferti_s11,
	nl_gg2_watering,
	nl_gg2_cattle_out,
	nl_gg2_cattle_is_out,
	nl_gg2_foobar
} NLGrassGrazed2ToDo;


/**
\brief
NLGrassGrazed2 class
\n
*/
/**
See NLGrassGrazed2.h::NLGrassGrazed2ToDo for a complete list of all possible events triggered codes by the TemporalGrassGrazed2 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLGrassGrazed2: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLGrassGrazed2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_gg2_foobar- NLGRASSGRAZED2_BASE);
	   m_base_elements_no = NLGRASSGRAZED2_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
		   fmc_Others,	// zero element unused but must be here
			fmc_Others,//nl_gg2_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_gg2_sleep_all_day = NLGRASSGRAZED2_BASE,
			fmc_Fertilizer,//nl_gg2_ferti_p1,
			fmc_Fertilizer,//nl_gg2_ferti_s1,
			fmc_Cutting,//nl_gg2_cut_to_silage1,
			fmc_Cutting,//nl_gg2_cut_to_silage2,
			fmc_Cutting,//nl_gg2_cut_to_silage3,
			fmc_Cutting,//nl_gg2_cut_to_silage4,
			fmc_Cutting,//nl_gg2_cut_to_silage5,
			fmc_Cutting,//nl_gg2_cut_to_silage6,
			fmc_Fertilizer,//nl_gg2_ferti_p2,
			fmc_Fertilizer,//nl_gg2_ferti_s2,
			fmc_Fertilizer,//nl_gg2_ferti_p3,
			fmc_Fertilizer,//nl_gg2_ferti_s3,
			fmc_Fertilizer,//nl_gg2_ferti_p4,
			fmc_Fertilizer,//nl_gg2_ferti_s4,
			fmc_Fertilizer,//nl_gg2_ferti_p5,
			fmc_Fertilizer,//nl_gg2_ferti_s5,
			fmc_Fertilizer,//nl_gg2_ferti_p6,
			fmc_Fertilizer,//nl_gg2_ferti_s6,
			fmc_Fertilizer,//nl_gg2_ferti_p7,
			fmc_Fertilizer,//nl_gg2_ferti_s7,
			fmc_Fertilizer,//nl_gg2_ferti_p8,
			fmc_Fertilizer,//nl_gg2_ferti_s8,
			fmc_Fertilizer,//nl_gg2_ferti_p9,
			fmc_Fertilizer,//nl_gg2_ferti_s9,
			fmc_Fertilizer,//nl_gg2_ferti_p10,
			fmc_Fertilizer,//nl_gg2_ferti_s10,
			fmc_Fertilizer,//nl_gg2_ferti_p11,
			fmc_Fertilizer,//nl_gg2_ferti_s11,
			fmc_Watering,//nl_gg2_watering,
			fmc_Grazing,//nl_gg2_cattle_out,
			fmc_Grazing//nl_gg2_cattle_is_out,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLGRASSGRAZED2_H

