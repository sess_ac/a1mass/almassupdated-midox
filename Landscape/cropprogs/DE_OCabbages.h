/**
\file
\brief
<B>DE_OCabbages.h This file contains the source for the DE_OCabbages class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen and Susanne Stein \n
Version of March 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DE_OCabbages.h
//


#ifndef DE_OCABBAGES_H
#define DE_OCABBAGES_H

#define DE_OCA_BASE 36600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DE_OCAB_WINTER_PLOUGH	m_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_oca_start = 1, // Compulsory, must always be 1 (one).
	de_oca_harvest = DE_OCA_BASE,
	de_oca_autumn_plough_clay,
	de_oca_ferti_s1,
	de_oca_ferti_p1,
	de_oca_spring_harrow_sandy,
	de_oca_sharrow1,
	de_oca_sharrow2,
	de_oca_sharrow3,
	de_oca_sharrow4,
	de_oca_plant,
	de_oca_water,
	de_oca_strigling1,
	de_oca_strigling2,
	de_oca_strigling3,
	de_oca_row_cultivation_clay,
	de_oca_covering1,
	de_oca_remove_cover1,
	de_oca_manual_weeding,
	de_oca_wait,
	de_oca_foobar,
} DE_OCabbagesToDo;


/**
\brief
DE_OCabbages class
\n
*/
/**
See DE_OCabbages.h::DE_OCabbagesToDo for a complete list of all possible events triggered codes by the cabbage management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_OCabbages: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_OCabbages(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st of September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,12 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_oca_foobar - DE_OCA_BASE);
	   m_base_elements_no = DE_OCA_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	de_oca_start = 1, // Compulsory, must always be 1 (one).
			fmc_Harvest,	//	de_oca_harvest = DE_OCA_BASE,
			fmc_Cultivation,	//	de_oca_autumn_plough_clay,
			fmc_Fertilizer,	//		de_oca_ferti_s1,
			fmc_Fertilizer,	//			de_oca_ferti_p1,
			fmc_Cultivation,	//	de_oca_spring_harrow_sandy,
			fmc_Cultivation,	//	de_oca_sharrow1,
			fmc_Cultivation,	//	de_oca_sharrow2,
			fmc_Cultivation,	//	de_oca_sharrow3,
			fmc_Cultivation,	//	de_oca_sharrow4,
			fmc_Others,	//	de_oca_plant,
			fmc_Watering,	//	de_oca_water,
			fmc_Cultivation,	//	de_oca_strigling1,
			fmc_Cultivation,	//	de_oca_strigling2,
			fmc_Cultivation,	//	de_oca_strigling3,
			fmc_Cultivation,	//	de_oca_row_cultivation_clay,
			fmc_Others,	//	de_oca_covering1,
			fmc_Others,	//	de_oca_remove_cover1,
			fmc_Cultivation,	//	de_oca_manual_weeding,
			fmc_Others,	//	de_oca_wait,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};

#endif // de_OCABBAGES_H

