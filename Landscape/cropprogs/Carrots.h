//
// CarrotsEat.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef CARROTS_H
#define CARROTS_H

#define CARROTS_BASE 1300
#define CA_SLURRY_DATE m_field->m_user[0]
#define CA_DECIDE_TO_HERB m_field->m_user[1]
#define CA_DECIDE_TO_FI m_field->m_user[2]



typedef enum {
  ca_start = 1, // Compulsory, start event must always be 1 (one).
  ca_fa_slurry = CARROTS_BASE,
  ca_fp_slurry,
  ca_spring_plough,
  ca_spring_harrow_one,
  ca_spring_harrow_two,
  ca_spring_sow,
  ca_herbi_one,
  ca_herbi_two,
  ca_herbi_three,
  ca_herbi_four,
  ca_rowcul_one,
  ca_rowcul_two,
  ca_rowcul_three,
  ca_insect_one,
  ca_insect_two,
  ca_insect_three,
  ca_water_one,
  ca_water_two,
  ca_water_three,
  ca_fp_npk_one,
  ca_fp_npk_two,
  ca_fa_npk_one,
  ca_fa_npk_two,
  ca_harvest,
  ca_foobar,
} CarrotsToDo;

class Carrots: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  Carrots(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(25,4);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (ca_foobar - CARROTS_BASE);
	  m_base_elements_no = CARROTS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  ca_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  ca_fa_slurry = CARROTS_BASE,
			fmc_Fertilizer,	//	  ca_fp_slurry,
			fmc_Cultivation,	//	  ca_spring_plough,
			fmc_Cultivation,	//	  ca_spring_harrow_one,
			fmc_Cultivation,	//	  ca_spring_harrow_two,
			fmc_Cultivation,	//	  ca_spring_sow,
			fmc_Herbicide,	//	  ca_herbi_one,
			fmc_Herbicide,	//	  ca_herbi_two,
			fmc_Herbicide,	//	  ca_herbi_three,
			fmc_Herbicide,	//	  ca_herbi_four,
			fmc_Cultivation,	//	  ca_rowcul_one,
			fmc_Cultivation,	//	  ca_rowcul_two,
			fmc_Cultivation,	//	  ca_rowcul_three,
			fmc_Insecticide,	//	  ca_insect_one,
			fmc_Insecticide,	//	  ca_insect_two,
			fmc_Insecticide,	//	  ca_insect_three,
			fmc_Watering,	//	  ca_water_one,
			fmc_Watering,	//	  ca_water_two,
			fmc_Watering,	//	  ca_water_three,
			fmc_Fertilizer,	//	  ca_fp_npk_one,
			fmc_Fertilizer,	//	  ca_fp_npk_two,
			fmc_Fertilizer,	//	  ca_fa_npk_one,
			fmc_Fertilizer,	//	  ca_fa_npk_two,
			fmc_Harvest	//	  ca_harvest,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // CARROTS_H
