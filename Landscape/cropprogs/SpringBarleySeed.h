//
// SpringBarleySeed.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleySeed_H
#define SpringBarleySeed_H

#define SBARLEYSEED_BASE 6500

#define SBSE_FUNGI_DATE        m_field->m_user[0]
#define SBSE_WATER_DATE        m_field->m_user[1]

typedef enum {
  sbse_start = 1, // Compulsory, start event must always be 1 (one).
  sbse_autumn_plough = SBARLEYSEED_BASE,
  sbse_spring_plough,
  sbse_spring_harrow,
  sbse_fertnpk,
  sbse_fertlnh3,
  sbse_fertpk,
  sbse_spring_sow,
  sbse_spring_roll,
  sbse_herbicide_one,
  sbse_herbicide_two,
  sbse_fungicide_one,
  sbse_fungicide_two,
  sbse_GR,
  sbse_water_one,
  sbse_water_two,
  sbse_harvest,
  sbse_hay_bailing,
  sbse_burn_straw_stubble,
  sbse_foobar
} SBSEToDo;



class SpringBarleySeed: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleySeed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,11);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sbse_foobar - SBARLEYSEED_BASE);
	  m_base_elements_no = SBARLEYSEED_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sbse_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation,//sbse_autumn_plough = SBARLEYSEED_BASE,
		  fmc_Cultivation,//sbse_spring_plough,
		  fmc_Cultivation,//sbse_spring_harrow,
		  fmc_Fertilizer,//sbse_fertnpk,
		  fmc_Fertilizer,//sbse_fertlnh3,
		  fmc_Fertilizer,//sbse_fertpk,
		  fmc_Others,//sbse_spring_sow,
		  fmc_Others,//sbse_spring_roll,
		  fmc_Herbicide,//sbse_herbicide_one,
		  fmc_Herbicide,//sbse_herbicide_two,
		  fmc_Fungicide,//sbse_fungicide_one,
		  fmc_Fungicide,//sbse_fungicide_two,
		  fmc_Others,//sbse_GR,
		  fmc_Watering,//sbse_water_one,
		  fmc_Watering,//sbse_water_two,
		  fmc_Harvest,//sbse_harvest,
		  fmc_Others,//sbse_hay_bailing,
		  fmc_Others//sbse_burn_straw_stubble

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SpringBarleySeed_H
