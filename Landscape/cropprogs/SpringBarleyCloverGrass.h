//
// SpringBarleyCloverGrass.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#ifndef SpringBarleyCloverGrass_H
#define SpringBarleyCloverGrass_H

#define SBARLEYCG_BASE 6100
#define SBCG_ISAUTUMNPLOUGH    m_field->m_user[0]
#define SBCG_FERTI_DONE        m_field->m_user[1]
#define SBCG_SPRAY             m_field->m_user[2]

typedef enum {
  sbcg_start = 1, // Compulsory, start event must always be 1 (one).
  sbcg_ferti_s1 = SBARLEYCG_BASE,
  sbcg_ferti_s2,
  sbcg_ferti_s3,
  sbcg_harvest,
  sbcg_spring_plough,
  sbcg_autumn_plough,
  sbcg_spring_harrow,
  sbcg_spring_roll,
  sbcg_spring_sow,
  sbcg_hay_baling,
  sbcg_GR,
  sbcg_water1,
  sbcg_water2,
  sbcg_herbicide,
  sbcg_herbicide2,
  sbcg_insecticide1,
  sbcg_insecticide2,
  sbcg_insecticide3,
  sbcg_fungicide,
  sbcg_cattle_out,
  sbcg_cattle_is_out,
  sbcg_foobar
} SBCGToDo;



class SpringBarleyCloverGrass: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleyCloverGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(30,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sbcg_foobar - SBARLEYCG_BASE);
	  m_base_elements_no = SBARLEYCG_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sbcg_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//sbcg_ferti_s1 = SBARLEYCG_BASE,
		  fmc_Fertilizer,//sbcg_ferti_s2,
		  fmc_Fertilizer,//sbcg_ferti_s3,
		  fmc_Harvest,//sbcg_harvest,
		  fmc_Cultivation,//sbcg_spring_plough,
		  fmc_Cultivation,//sbcg_autumn_plough,
		  fmc_Cultivation,//sbcg_spring_harrow,
		  fmc_Others,//sbcg_spring_roll,
		  fmc_Others,//sbcg_spring_sow,
		  fmc_Others,//sbcg_hay_baling,
		  fmc_Others,//sbcg_GR,
		  fmc_Watering,//sbcg_water1,
		  fmc_Watering,//sbcg_water2,
		  fmc_Herbicide,//sbcg_herbicide,
		  fmc_Herbicide,//sbcg_herbicide2,
		  fmc_Insecticide,//sbcg_insecticide1,
		  fmc_Insecticide,//sbcg_insecticide2,
		  fmc_Insecticide,//sbcg_insecticide3,
		  fmc_Fungicide,//sbcg_fungicide,
		  fmc_Grazing,//sbcg_cattle_out,
		  fmc_Grazing//sbcg_cattle_is_out

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SpringBarleyCloverGrass_H
