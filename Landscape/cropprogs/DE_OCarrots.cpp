/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#define _CRT_SECURE_NO_DEPRECATE
//#include "ALMaSS_Setup.h"
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OCarrots.h"

bool DE_OCarrots::Do( Farm * a_farm, LE * a_field, FarmEvent * a_ev )
{
/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DEOCarrots; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	switch (m_ev->m_todo)
	{
	case de_ocar_start:
	{
		m_field->ClearManagementActionSum();

		if (g_rand_uni() <= 0.50) DE_OCAR_DO_SPR_HARVEST = true; else DE_OCAR_DO_SPR_HARVEST = false; // LKM: flags to mark that some farmers do harvest the year after in spring - date for finished management is different
		// DE_CAR_SLURRY_DATE = 0;
		// DE_CAR_DECIDE_TO_HERB = 1;
		// DE_CAR_DECIDE_TO_FI = 1;

		m_last_date = g_date->DayInYear(30, 11); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(30, 11); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(30, 11); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_ocar_stoneburier))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		SimpleEvent_(d1, de_ocar_stoneburier, false, m_farm, m_field);
		break;
	}
	break;

  case de_ocar_stoneburier:
      if (!m_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_ocar_stoneburier, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - manure + nutrients (K, S and Mg) done  before the 20th of April - if not done, try again + 1 day until the 20th of April  when we will succeed
      if (m_farm->IsStockFarmer()) //Stock Farmer					// Main thread
      {
            SimpleEvent_(g_date->Date() + 1, de_ocar_FAmanure, false, m_farm, m_field);
      }
      else SimpleEvent_(g_date->Date() + 1, de_ocar_FPmanure, true, m_farm, m_field);
      break;
  case de_ocar_FAmanure:
      if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_ocar_FAmanure, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - Deep harrow done before the 20th of April - if not done, try again +1 day until the 20th of April when we will succeed
      SimpleEvent_(g_date->Date(), de_ocar_deep_harrow, false, m_farm, m_field);
      break;
  case de_ocar_FPmanure:
      if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_ocar_FPmanure, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - Deep harrow done before the 20th of April - if not done, try again +1 day until the 20th of April when we will succeed
      SimpleEvent_(g_date->Date(), de_ocar_deep_harrow, false, m_farm, m_field);
      break;
  case de_ocar_deep_harrow:
      if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_ocar_deep_harrow, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - Bedformer done before the 20th of April - if not done, try again +1 day until the 20th of April when we will succeed
      SimpleEvent_(g_date->Date(), de_ocar_bedformer, false, m_farm, m_field);
      break;
  case de_ocar_bedformer:
      if (!m_farm->BedForming(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_ocar_bedformer, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - shallow harrow1 (making seedbed) done after the 5th of February and before the 5th of May - if not done, try again +1 day until the 5th of May when we will succeed
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 2), de_ocar_sharrow1, false, m_farm, m_field);
      break;
  case de_ocar_sharrow1:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(5, 4) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_sharrow1, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - shallow harrow2 (making seedbed) done 5 days after, and before the 10th of May - if not done, try again +1 day until the 10th of May when we will succeed
      SimpleEvent_(g_date->Date() + 5, de_ocar_sharrow2, false, m_farm, m_field);
      break;
  case de_ocar_sharrow2:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_sharrow2, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - shallow harrow3 (making seedbed) done 5 days after, and before the 15th of May - if not done, try again +1 day until the 15th of May when we will succeed
      SimpleEvent_(g_date->Date() + 5, de_ocar_sharrow3, false, m_farm, m_field);
      break;
  case de_ocar_sharrow3:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_sharrow3, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - shallow harrow4 (making seedbed) done 5 days after, and before the 20th of May - if not done, try again +1 day until the 20th of May when we will succeed
      SimpleEvent_(g_date->Date() + 5, de_ocar_sharrow4, false, m_farm, m_field);
      break;
  case de_ocar_sharrow4:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_sharrow4, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - sow done after the 1st of March and before the 30th of May - if not done, try again +1 day until the 30th of May when we will succeed
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_ocar_sow, false, m_farm, m_field);
      break;
  case de_ocar_sow:
      if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_sow, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - burn weeds1 done  before the 5th of May - if not done, try again + 1 day until the 5th of May  when we will succeed
      SimpleEvent_(g_date->Date() + 1, de_ocar_burn_weeds1, false, m_farm, m_field);
      break;
  case de_ocar_burn_weeds1:
      if (!m_farm->BurnStrawStubble(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_burn_weeds1, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - burn weeds2 done (+5 days after) before the 10th of May - if not done, try again + 1 day until the 10th of May  when we will succeed
      SimpleEvent_(g_date->Date() + 5, de_ocar_burn_weeds2, false, m_farm, m_field);
      break;
  case de_ocar_burn_weeds2:
      if (!m_farm->BurnStrawStubble(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_burn_weeds2, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - Row cultivation1 done  before the 15th of May - if not done, try again + 1 day until the 15th of May  when we will succeed
      SimpleEvent_(g_date->Date() + 1, de_ocar_row_cultivation1, false, m_farm, m_field);
      break;
  case de_ocar_row_cultivation1:
      if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_row_cultivation1, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - Row cultivation2 done (+8 days after) before the 20th of May - if not done, try again + 1 day until the 20th of May  when we will succeed
      SimpleEvent_(g_date->Date() + 8, de_ocar_row_cultivation2, false, m_farm, m_field);
      break;
  case de_ocar_row_cultivation2:
      if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_row_cultivation2, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - manual weeding (+5 days after) done before the 30th of May - if not done, try again + 1 day until the 30th of May  when we will succeed
      SimpleEvent_(g_date->Date() + 8, de_ocar_manual_weeding, false, m_farm, m_field);
      break;
  case de_ocar_manual_weeding:
      if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_manual_weeding, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - water2 done before the 5th of July - if not done, try again +1 day until the 5th of July when we will succeed
      SimpleEvent_(g_date->Date(), de_ocar_water1, false, m_farm, m_field);
      break;
  case de_ocar_water1:
      if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_water1, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - hilling up1 done before the 5th of July - if not done, try again +1 day until the 5th of July when we will succeed
      SimpleEvent_(g_date->Date() + 1, de_ocar_hilling_up1, false, m_farm, m_field);
      break;
  case de_ocar_hilling_up1:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_hilling_up1, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - add pfr1 done before the 10th of July - if not done, try again + 1 day until the 10th of July  when we will succeed
      SimpleEvent_(g_date->Date() + 1, de_ocar_boron1_s, false, m_farm, m_field);
      break;
  case de_ocar_boron1_s:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear()))
          {
              SimpleEvent_(g_date->Date() + 1, de_ocar_boron1_s, true, m_farm, m_field);
              break;
          }
          // LKM: Queue up the next event - water2 done after the 10th of June and before the 20th of July - if not done, try again +1 day until the 10th of July when we will succeed
          SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 6), de_ocar_water2, false, m_farm, m_field);
          break;
      }
      SimpleEvent_(g_date->Date() + 1, de_ocar_boron1_p, false, m_farm, m_field);
      break;

  case de_ocar_boron1_p:
      if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_boron1_p, true, m_farm, m_field);
          break;
      }
          // LKM: Queue up the next event - water2 done after the 10th of June and before the 20th of July - if not done, try again +1 day until the 10th of July when we will succeed
          SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 6), de_ocar_water2, false, m_farm, m_field);
          break;

  case de_ocar_water2:
      if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_water2, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - hilling up2 done before the 30th of July - if not done, try again +1 day until the 30th of July when we will succeed
      SimpleEvent_(g_date->Date() + 1, de_ocar_hilling_up2, false, m_farm, m_field);
      break;
  case de_ocar_hilling_up2:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_hilling_up2, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - boron2 done after the 1st of May, and before the 30th of August - if not done, try again +1 day until the 30th of August when we will succeed
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_ocar_boron2_s, false, m_farm, m_field);
      break;
  case de_ocar_boron2_s:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(29, 8) - g_date->DayInYear()))
          {
              SimpleEvent_(g_date->Date() + 1, de_ocar_boron2_s, true, m_farm, m_field);
              break;
          }
          // LKM: Queue up the next event - hilling up3 done before the 30th of August - if not done, try again +1 day until the 30th of July when we will succeed
          SimpleEvent_(g_date->Date(), de_ocar_hilling_up3, false, m_farm, m_field);
          break;
      }
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_ocar_boron2_p, false, m_farm, m_field);
      break;

  case de_ocar_boron2_p:
      if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(29, 8) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_boron2_p, true, m_farm, m_field);
          break;
      }
          // LKM: Queue up the next event - hilling up3 done before the 30th of August - if not done, try again +1 day until the 30th of July when we will succeed
          SimpleEvent_(g_date->Date(), de_ocar_hilling_up3, false, m_farm, m_field);
          break;

  case de_ocar_hilling_up3:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear()))
      {
          SimpleEvent_(g_date->Date() + 1, de_ocar_hilling_up3, true, m_farm, m_field);
          break;
      }
      // LKM: Queue up the next event - harvest carrots, done before the 30th of November - if not done, try again +1 day until the 30th of November when we will succeed
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 6), de_ocar_harvest, false, m_farm, m_field);
      break;
  case de_ocar_harvest:
      if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_ocar_harvest, true, m_farm, m_field);
          break;
          //if (!m_farm->Harvest(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear()))
          //{
          //	SimpleEvent_(g_date->Date() + 1, de_ca_harvest, true, m_farm, m_field);
          //	break;
          //}	
      }
      d1 = g_date->Date();
      if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
          SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_ocar_wait, false, m_farm, m_field);
          // Because we are ending harvest before 1.7 so we need to wait until the 1.7
          break;
      }
      else {
          done = true; // end of plan
      }
  case de_ocar_wait:
      done = true;
      break;
      // So we are done, and somewhere else the farmer will queue up the start event of the next crop
      // END of MAIN THREAD
      break;
  default:
      g_msg->Warn(WARN_BUG, "DE_OCarrots::Do(): "
          "Unknown event type! ", "");
      exit(1);
  }

  return done;
}


