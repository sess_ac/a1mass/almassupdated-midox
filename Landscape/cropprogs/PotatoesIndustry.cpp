//
// PotatoesIndustry.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PotatoesIndustry.h"

#include <stdio.h>

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool PotatoesIndustry::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case pi_start:
  {
    POT_SLURRY_DATE = 0;
    POT_HERBI_DATE  = 0;
    POT_STRIG_DATE  = 0;
    POT_HILL_DATE   = 0;
    POT_DID_TREAT   = false;
    POT_DID_HILL    = false;
    a_field->ClearManagementActionSum();

	m_field->SetVegPatchy(true); // root crop

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(24,10);
      // Start and stop dates for all events after harvest
      int noDates= 1;
      m_field->SetMDates(0,0,g_date->DayInYear(25,9));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(24,10));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

	  //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	  int d1;
	  if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "PotatoesIndustry::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
      }
      // Now no operations can be timed after the start of the next crop.

      if ( ! m_ev->m_first_year ) {
		int today=g_date->Date();
        // Are we before July 1st?
		d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
		if (today < d1) {
			// Yes, too early. We assumme this is because the last crop was late
			g_msg->Warn( WARN_BUG, "PotatoesIndustry::Do(): " "Crop start attempt between 1st Jan & 1st July", "" );
			exit( 1 );
		}
        else {
			d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
			if (today > d1) {
				// Yes too late - should not happen - raise an error
				g_msg->Warn( WARN_BUG, "PotatoesIndustry::Do(): " "Crop start attempt after last possible start date", "" );
				exit( 1 );
			}
        }
      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear(1,3), pi_spring_plough, false );
         break;
      }
	  }//if

      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
			d1 = g_date->Date();
      }
      // OK, let's go.

    if ( m_farm->DoIt( 75 )) {
      SimpleEvent( d1,pi_autumn_plough, false );
    } 
	else {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 3 ) + 365,
		   pi_spring_plough, false );
    }
  }
  break;

  case pi_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 1, 12 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_autumn_plough, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 3 ) + 365,
		 pi_spring_harrow, false );
    break;

  case pi_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
			       g_date->DayInYear( 15, 4 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_spring_plough, false );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 20, 3 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, pi_spring_harrow, false );
    }
    break;

  case pi_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 24, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_spring_harrow, false );
      break;
    }
    if ( m_farm->IsStockFarmer()) {
      // Do fa_slurry before sowing.
      int d1 = g_date->OldDays() + g_date->DayInYear( 27, 3 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, pi_fa_slurry, false );
    } else {
      int d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, pi_spring_sow, false );
    }
    break;

  case pi_fa_slurry:
    if ( m_ev->m_lock || m_farm->DoIt( 30 )) {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 1, 5 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, pi_fa_slurry, true );
	break;
      }
      POT_SLURRY_DATE = g_date->DayInYear();
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, pi_spring_sow, false );
    }
    break;

  case pi_spring_sow:
    {
      int time_to_waste;
      if ( POT_SLURRY_DATE ) {
		time_to_waste = POT_SLURRY_DATE + 3;
		if ( time_to_waste > g_date->DayInYear( 1, 5 )) {
		 time_to_waste = g_date->DayInYear( 1, 5 );
		}
      } 
	  else {
		time_to_waste = g_date->DayInYear( 1, 5 );
      }
      if (!m_farm->SpringSow( m_field, 0.0, time_to_waste - g_date->DayInYear())) {
		SimpleEvent( g_date->Date() + 1, pi_spring_sow, false );
		break;
      }
      // Did sowing, now add fa/fp_npk as well today.
      if ( m_farm->IsStockFarmer()) {
			m_farm->FA_NPK( m_field, 0.0, 0);
      } 
	  else {
			m_farm->FP_NPK( m_field, 0.0, 0);
      }
    }

    // Each of the three threads below need to set flags when they terminate
    // so that the last one can start the insecticide thread
    //
    // Start hilling up sub thread.
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ),
		 pi_hilling, false );

    if ( m_farm->DoIt( 80 )) { // was 80
      // Start herbicide treatment thread.
      int d1 = g_date->OldDays() + g_date->DayInYear( 10, 4 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, pi_herbi_one, false );
    } else {
      // Strigling thread.
      int d1 = g_date->OldDays() + g_date->DayInYear( 14, 4 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, pi_strigling_one, false );
    }
    break;

  case pi_strigling_one:
    if ( POT_HILL_DATE &&
	 POT_HILL_DATE >= g_date->Date() - 1 ) {
      // Too close to hilling, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, pi_strigling_one, false );
      break;
    }
    if (!m_farm->Strigling( m_field, 0.0,
         g_date->DayInYear( 25, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_strigling_one, false );
      break;
    }
    POT_STRIG_DATE = g_date->Date();
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 21, 4 );
      if ( g_date->Date() + 7 > d1 ) {
        d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, pi_strigling_two, false );
    }
    break;

  case pi_strigling_two:
    if ( POT_HILL_DATE &&
	 POT_HILL_DATE >= g_date->Date() - 1 ) {
      // Too close to hilling, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, pi_strigling_two, false );
      break;
    }
    if (!m_farm->Strigling( m_field, 0.0,
         g_date->DayInYear( 3, 6 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_strigling_two, false );
      break;
    }
    POT_STRIG_DATE = g_date->Date();
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 30, 4 );
      if ( g_date->Date() + 7 > d1 ) {
        d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, pi_strigling_three, false );
    }
    break;

  case pi_strigling_three:
    if ( POT_HILL_DATE &&
	 POT_HILL_DATE >= g_date->Date() - 1 ) {
      // Too close to hilling, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, pi_strigling_three, false );
      break;
    }
    if (!m_farm->Strigling( m_field, 0.0,
			    g_date->DayInYear( 12, 6 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_strigling_three, true );
      break;
    }
    POT_STRIG_DATE = g_date->Date();
    POT_DID_TREAT  = true;
    if ( POT_DID_HILL && POT_DID_TREAT) {
      // last surviving tread, start insecticide.
      int d1 = g_date->OldDays() + g_date->DayInYear( 10, 6 );
      if ( g_date->Date() + 3 > d1 ) {
	d1 = g_date->Date() + 3;
      }
      SimpleEvent( d1, pi_insecticide, false );
    }
    break;

  case pi_herbi_one:
    if (( m_ev->m_lock || m_farm->DoIt(  (int) (80*cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier())))) //modified probability
    {
      if ( POT_HILL_DATE &&
	 POT_HILL_DATE >= g_date->Date() - 1 ) {
      // Too close to hilling, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, pi_herbi_one, false );
        break;
      }
      if (!m_farm->HerbicideTreat( m_field, 0.0,
				 g_date->DayInYear( 12, 5 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_herbi_one, false );
      break;
    }
    POT_HERBI_DATE = g_date->Date();
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 20, 4 );
      if ( g_date->Date() + 10 > d1 ) {
        d1 = g_date->Date() + 10;
      }
    SimpleEvent( d1, pi_herbi_two, false );
    }
  }
  else {
    POT_DID_TREAT  = true; // Need to signal the finish of this thread
    if ( POT_DID_HILL && POT_DID_TREAT ) {
      // last surviving tread, start insecticide.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 6 ),
                   pi_insecticide, false );
    }
  }
  break;

  case pi_herbi_two:
    if (( m_ev->m_lock || m_farm->DoIt(  (int) (50*cfg_herbi_app_prop.value() * m_farm->Prob_multiplier() )))) //modified probability
    {
      if ( POT_HILL_DATE &&
	 POT_HILL_DATE >= g_date->Date() - 1 ) {
        // Too close to hilling, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, pi_herbi_two, false );
        break;
      }
      if (!m_farm->HerbicideTreat( m_field, 0.0,
				 g_date->DayInYear( 26, 5 ) -
				 g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, pi_herbi_two, false );
        break;
      }
      POT_HERBI_DATE = g_date->Date();
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 30, 4 );
        if ( g_date->Date() + 10 > d1 ) {
        	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, pi_herbi_three, false );
    }
  }
  else {
    POT_DID_TREAT  = true; // Need to signal the finish of this thread
    if ( POT_DID_HILL && POT_DID_TREAT ) {
      // last surviving tread, start insecticide.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 6 ),
                   pi_insecticide, false );
    }
  }
  break;

  case pi_herbi_three:
    if (( m_ev->m_lock || m_farm->DoIt(  (int) (53*cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier())))) //modified probability
    {
      if ( POT_HILL_DATE &&
	 POT_HILL_DATE >= g_date->Date() - 1 ) {
      // Too close to hilling, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, pi_herbi_three, false );
      break;
    }
    if (!m_farm->HerbicideTreat( m_field, 0.0,
				 g_date->DayInYear( 7, 6 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_herbi_three, false );
      break;
    }
    POT_HERBI_DATE = g_date->Date();
    POT_DID_TREAT  = true; // Need to signal the finish of this thread
    if ( POT_DID_HILL && POT_DID_TREAT ) {
      // last surviving tread, start insecticide.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 6 ),
		   pi_insecticide, false );
    }
  }
  else {
    POT_DID_TREAT  = true; // Need to signal the finish of this thread
    if ( POT_DID_HILL && POT_DID_TREAT ) {
      // last surviving tread, start insecticide.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 6 ),
                   pi_insecticide, false );
    }
  }
  break;

  case pi_hilling:
if ( (POT_HERBI_DATE &&
	  POT_HERBI_DATE >= g_date->Date() - 2)
	 ||
	 (POT_STRIG_DATE &&
	  POT_STRIG_DATE >= g_date->Date() - 7)) {
      // Too close to the others, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, pi_hilling, false );
      break;
    }
if (!m_farm->HillingUp( m_field, 0.0,
         g_date->DayInYear( 25, 6 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_hilling, false );
      break;
    }
    POT_HILL_DATE = g_date->Date();
    POT_DID_HILL = true;
    if ( POT_DID_TREAT )
    {
      // last surviving tread, start insecticide.
      int d1 = g_date->OldDays() + g_date->DayInYear( 10, 6 );
      if ( g_date->Date() + 3 > d1 ) {
	d1 = g_date->Date() + 3;
      }
      SimpleEvent( d1, pi_insecticide, false );
    }
    break;

  case pi_insecticide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (59*cfg_ins_app_prop1.value()  * m_farm->Prob_multiplier()))) { //modified probability
      if (!m_farm->InsecticideTreat( m_field, 0.0,
				     g_date->DayInYear( 26, 6 ) -
				     g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, pi_insecticide, true );
	break;
      }
    }
    POT_WATER_DATE = 0;
    POT_FUNGI_DATE = 0;
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 15, 6 );
      if ( g_date->Date() + 1 > d1 ) {
	d1 = g_date->Date() + 1;
      }
      SimpleEvent( d1, pi_water_one, false );
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 25, 6 );
      if ( g_date->Date() + 1 > d1 ) {
	d1 = g_date->Date() + 1;
      }
      SimpleEvent( d1, pi_fungi_one, false );
    }
    break;

  case pi_water_one:
    if ( POT_FUNGI_DATE &&
	 POT_FUNGI_DATE >= g_date->Date() + 2 ) {
      SimpleEvent( g_date->Date() + 1, pi_water_one, true );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 80 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 30, 6 ) -
			  g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, pi_water_one, true );
	break;
      }
      POT_WATER_DATE = g_date->Date();
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 7 ),
		   pi_water_two, false );
    }
    break;

  case pi_water_two:
    if ( POT_FUNGI_DATE &&
	 POT_FUNGI_DATE >= g_date->Date() + 2 ) {
      SimpleEvent( g_date->Date() + 1, pi_water_two, true );
      break;
    }
    if (!m_farm->Water( m_field, 0.0,
			g_date->DayInYear( 15, 7 ) -
			g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_water_two, true );
      break;
    }
    POT_WATER_DATE = g_date->Date();
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16, 7 ),
		 pi_water_three, false );
    break;

  case pi_water_three:
    if ( POT_FUNGI_DATE &&
	 POT_FUNGI_DATE >= g_date->Date() + 2 ) {
      SimpleEvent( g_date->Date() + 1, pi_water_three, true );
      break;
    }
    if (!m_farm->Water( m_field, 0.0,
			g_date->DayInYear( 30, 7 ) -
			g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_water_three, true );
      break;
    }
    POT_WATER_DATE = g_date->Date();
    break;

  case pi_fungi_one:
    if ( POT_WATER_DATE &&
	 POT_WATER_DATE >= g_date->Date() + 2 ) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_one, true );
      break;
    }
    if (!m_farm->FungicideTreat( m_field, 0.0,
				 g_date->DayInYear( 10, 7 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_one, true );
      break;
    }
    POT_FUNGI_DATE = g_date->Date();
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 5, 7 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, pi_fungi_two, false );
    }
    break;

  case pi_fungi_two:
    if ( POT_WATER_DATE &&
	 POT_WATER_DATE >= g_date->Date() + 2 ) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_two, true );
      break;
    }
    if (!m_farm->FungicideTreat( m_field, 0.0,
				 g_date->DayInYear( 20, 7 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_two, true );
      break;
    }
    POT_FUNGI_DATE = g_date->Date();
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 15, 7 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, pi_fungi_three, false );
    }
    break;

  case pi_fungi_three:
    if ( POT_WATER_DATE &&
	 POT_WATER_DATE >= g_date->Date() + 2 ) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_three, true );
      break;
    }
    if (!m_farm->FungicideTreat( m_field, 0.0,
				 g_date->DayInYear( 1, 8 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_three, true );
      break;
    }
    POT_FUNGI_DATE = g_date->Date();
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 25, 7 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, pi_fungi_four, false );
    }
    break;

  case pi_fungi_four:
    if ( POT_WATER_DATE &&
	 POT_WATER_DATE >= g_date->Date() + 2 ) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_four, true );
      break;
    }
    if (!m_farm->FungicideTreat( m_field, 0.0,
				 g_date->DayInYear( 11, 8 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_four, true );
      break;
    }
    POT_FUNGI_DATE = g_date->Date();
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 5, 8 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, pi_fungi_five, false );
    }
    break;

  case pi_fungi_five:
    if (!m_farm->FungicideTreat( m_field, 0.0,
				 g_date->DayInYear( 21, 8 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, pi_fungi_five, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5, 8 ),
		 pi_growth_reg, false );
    break;

  case pi_growth_reg:
    if ( m_ev->m_lock || m_farm->DoIt( 80 )) {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				    g_date->DayInYear( 1, 9 ) -
				    g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, pi_growth_reg, true );
	break;
      }
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 15, 9 );
      if ( g_date->Date() + 14 > d1 ) {
	d1 = g_date->Date() + 14;
      }
	  ChooseNextCrop (1);
      SimpleEvent( d1, pi_harvest, false );
    }
    break;

  case pi_harvest:
   if (m_field->GetMConstants(1)==0) {
			if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "PotatoesIndustry::Do(): failure in 'Harvest' execution", "" );
				exit( 1 );
			} 
	}
	else { 
		if (!m_farm->Harvest( m_field, 0.0,  m_field->GetMDates(0,1) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, pi_harvest, false );
			break;
		}
	}
	m_field->SetVegPatchy(false);
	done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "PotatoesEat::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


