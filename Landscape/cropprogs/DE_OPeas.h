//
// OPeas.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved. 

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DE_OPeas_H
#define DE_OPeas_H
/** \brief A flag used to indicate do catch crop
*/
#define DE_OPEAS_DO_CATCHCROP	a_field->m_user[2]
#define DE_OPEAS_BASE 38800

typedef enum {
  de_ope_start = 1, // Compulsory, start event must always be 1 (one).
  de_ope_sleep_all_day = DE_OPEAS_BASE,
  de_ope_spring_harrow1,
  de_ope_spring_plough,
  de_ope_ferti_s,
  de_ope_ferti_p,
  de_ope_spring_harrow2,
  de_ope_spring_row_sow,
  de_ope_strigling,
  de_ope_rowcultivation1,
  de_ope_rowcultivation2, 
  de_ope_harvest,
    de_ope_foobar,
} DE_OPeassToDo;



class DE_OPeas: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OPeas(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(15,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_ope_foobar - DE_OPEAS_BASE);
	  m_base_elements_no = DE_OPEAS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_ope_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others,	//	  de_ope_sleep_all_day = DE_OPEAS_BASE,
			fmc_Cultivation,	//	  de_ope_spring_harrow1,
			fmc_Cultivation,	//	  de_ope_spring_plough,
			fmc_Fertilizer,	//	  de_ope_ferti_s,
			fmc_Fertilizer,	//	  de_ope_ferti_p,
			fmc_Cultivation,	//	  de_ope_spring_harrow2,
			fmc_Others,	//	  de_ope_spring_row_sow,
			fmc_Cultivation,	//	  de_ope_strigling,
			fmc_Cultivation,	//	  de_ope_rowcultivation1,
			fmc_Cultivation,	//	  de_ope_rowcultivation2,
			fmc_Harvest,	//	  de_ope_harvest,

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DE_OPEAS_H
