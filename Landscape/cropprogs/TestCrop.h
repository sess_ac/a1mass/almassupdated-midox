/**
\file
\brief
<B>TestCrop.h This file contains the headers for the WinterWheat class</B> \n
*/

#ifndef TESTCROP_H
#define TESTCROP_H

#define TESTCROP_BASE 1
/**
\brief A flag used to indicate autumn ploughing status
*/
#define TC_WW_FERTI_P1	a_field->m_user[1]
#define TC_WW_FERTI_S1	a_field->m_user[2]
#define TC_WW_STUBBLE_PLOUGH	a_field->m_user[3]
#define TC_WW_DECIDE_TO_GR a_field->m_user[4]

/** Below is the list of things that a farmer can do if he is growing test crop, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	testcrop_ww_start = 1, // Compulsory, must always be 1 (one).
	testcrop_ww_sleep_all_day = TESTCROP_BASE,
	testcrop_ww_ferti_p1,
	testcrop_ww_ferti_s1,
	testcrop_ww_stubble_plough,
	testcrop_ww_autumn_harrow1,
	testcrop_ww_autumn_harrow2,
	testcrop_ww_stubble_harrow,
	testcrop_ww_ferti_p2,
	testcrop_ww_ferti_s2,
	testcrop_ww_autumn_plough,
	testcrop_ww_autumn_roll,
	testcrop_ww_stubble_cultivator_heavy,
	testcrop_ww_ferti_p3,
	testcrop_ww_ferti_s3,
	testcrop_ww_preseeding_cultivator,
	testcrop_ww_preseeding_cultivator_sow,
	testcrop_ww_autumn_sow,
	testcrop_ww_herbicide1,
	testcrop_ww_fungicide1,
	testcrop_ww_insecticide1,
	testcrop_ww_ferti_p4,
	testcrop_ww_ferti_s4,
	testcrop_ww_ferti_p5,
	testcrop_ww_ferti_s5,
	testcrop_ww_ferti_p6,
	testcrop_ww_ferti_s6,
	testcrop_ww_ferti_p7,
	testcrop_ww_ferti_s7,
	testcrop_ww_ferti_p8,
	testcrop_ww_ferti_s8,
	testcrop_ww_ferti_p9,
	testcrop_ww_ferti_s9,
	testcrop_ww_herbicide2, 
	testcrop_ww_fungicide2,
	testcrop_ww_fungicide3,
	testcrop_ww_fungicide4,
	testcrop_ww_insecticide2,
	testcrop_ww_insecticide3,
	testcrop_ww_growth_regulator1,
	testcrop_ww_growth_regulator2,
	testcrop_ww_harvest,
	testcrop_ww_straw_chopping,
	testcrop_ww_hay_bailing,
	testcrop_ww_ferti_p10,
	testcrop_ww_ferti_s10,
	testcrop_ww_ferti_p11,
	testcrop_ww_ferti_s11,
	testcrop_ww_foobar
} TestCropToDo;


class TestCrop: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   TestCrop(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 20,9 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (testcrop_ww_foobar - TESTCROP_BASE);
	   m_base_elements_no = TESTCROP_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
		   fmc_Others,	// zero element unused but must be here
			fmc_Others,//testcrop_ww_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//testcrop_ww_sleep_all_day = TESTCROP_BASE,
			fmc_Fertilizer,//testcrop_ww_ferti_p1,
			fmc_Fertilizer,//testcrop_ww_ferti_s1,
			fmc_Cultivation,//testcrop_ww_stubble_plough,
			fmc_Cultivation,//testcrop_ww_autumn_harrow1,
			fmc_Cultivation,//testcrop_ww_autumn_harrow2,
			fmc_Cultivation,//testcrop_ww_stubble_harrow,
			fmc_Fertilizer,//testcrop_ww_ferti_p2,
			fmc_Fertilizer,//testcrop_ww_ferti_s2,
			fmc_Cultivation,//testcrop_ww_autumn_plough,
			fmc_Others,//testcrop_ww_autumn_roll,
			fmc_Cultivation,//testcrop_ww_stubble_cultivator_heavy,
			fmc_Fertilizer,//testcrop_ww_ferti_p3,
			fmc_Fertilizer,//testcrop_ww_ferti_s3,
			fmc_Cultivation,//testcrop_ww_preseeding_cultivator,
			fmc_Cultivation,//testcrop_ww_preseeding_cultivator_sow,
			fmc_Others,//testcrop_ww_autumn_sow,
			fmc_Herbicide,//testcrop_ww_herbicide1,
			fmc_Fungicide,//testcrop_ww_fungicide1,
			fmc_Insecticide,//testcrop_ww_insecticide1,
			fmc_Fertilizer,//testcrop_ww_ferti_p4,
			fmc_Fertilizer,//testcrop_ww_ferti_s4,
			fmc_Fertilizer,//testcrop_ww_ferti_p5,
			fmc_Fertilizer,//testcrop_ww_ferti_s5,
			fmc_Fertilizer,//testcrop_ww_ferti_p6,
			fmc_Fertilizer,//testcrop_ww_ferti_s6,
			fmc_Fertilizer,//testcrop_ww_ferti_p7,
			fmc_Fertilizer,//testcrop_ww_ferti_s7,
			fmc_Fertilizer,//testcrop_ww_ferti_p8,
			fmc_Fertilizer,//testcrop_ww_ferti_s8,
			fmc_Fertilizer,//testcrop_ww_ferti_p9,
			fmc_Fertilizer,//testcrop_ww_ferti_s9,
			fmc_Herbicide,//testcrop_ww_herbicide2,
			fmc_Fungicide,//testcrop_ww_fungicide2,
			fmc_Fungicide,//testcrop_ww_fungicide3,
			fmc_Fungicide,//testcrop_ww_fungicide4,
			fmc_Insecticide,//testcrop_ww_insecticide2,
			fmc_Insecticide,//testcrop_ww_insecticide3,
			fmc_Others,//testcrop_ww_growth_regulator1,
			fmc_Others,//testcrop_ww_growth_regulator2,
			fmc_Harvest,//testcrop_ww_harvest,
			fmc_Others,//testcrop_ww_straw_chopping,
			fmc_Others,//testcrop_ww_hay_bailing,
			fmc_Fertilizer,//testcrop_ww_ferti_p10,
			fmc_Fertilizer,//testcrop_ww_ferti_s10,
			fmc_Fertilizer,//testcrop_ww_ferti_p11,
			fmc_Fertilizer//testcrop_ww_ferti_s11,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // TESTCROP_H

