//
// SpringBarleyPeaCloverGrassStrigling.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleyPeaCloverGrassStrigling_H
#define SpringBarleyPeaCloverGrassStrigling_H

#define SBARLEYPCGS_BASE 6300
#define SBPCGS_ISAUTUMNPLOUGH    m_field->m_user[0]
#define SBPCGS_FERTI_DONE        m_field->m_user[1]
#define SBPCGS_SPRAY             m_field->m_user[2]

typedef enum {
  sbpcgs_start = 1, // Compulsory, start event must always be 1 (one).
  sbpcgs_ferti_s1 = SBARLEYPCGS_BASE,
  sbpcgs_ferti_s2,
  sbpcgs_ferti_s3,
  sbpcgs_harvest,
  sbpcgs_spring_plough,
  sbpcgs_autumn_plough,
  sbpcgs_spring_harrow,
  sbpcgs_spring_roll,
  sbpcgs_spring_sow,
  sbpcgs_strigling_sow,
  sbpcgs_hay_baling,
  sbpcgs_GR,
  sbpcgs_water1,
  sbpcgs_water2,
  sbpcgs_insecticide,
  sbpcgs_fungicide,
  sbpcgs_cattle_out,
  sbpcgs_cattle_is_out,
  sbpcgs_foobar
} SBPCGSToDo;



class SpringBarleyPeaCloverGrassStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleyPeaCloverGrassStrigling(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(30,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sbpcgs_foobar - SBARLEYPCGS_BASE);
	  m_base_elements_no = SBARLEYPCGS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sbpcgs_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//sbpcgs_ferti_s1 = SBARLEYPCGS_BASE,
		  fmc_Fertilizer,//sbpcgs_ferti_s2,
		  fmc_Fertilizer,//sbpcgs_ferti_s3,
		  fmc_Harvest,//sbpcgs_harvest,
		  fmc_Cultivation,//sbpcgs_spring_plough,
		  fmc_Cultivation,//sbpcgs_autumn_plough,
		  fmc_Cultivation,//sbpcgs_spring_harrow,
		  fmc_Others,//sbpcgs_spring_roll,
		  fmc_Others,//sbpcgs_spring_sow,
		  fmc_Cultivation,//sbpcgs_strigling_sow,
		  fmc_Others,//sbpcgs_hay_baling,
		  fmc_Others,//sbpcgs_GR,
		  fmc_Watering,//sbpcgs_water1,
		  fmc_Watering,//sbpcgs_water2,
		  fmc_Insecticide,//sbpcgs_insecticide,
		  fmc_Fungicide,//sbpcgs_fungicide,
		  fmc_Grazing,//sbpcgs_cattle_out,
		  fmc_Grazing//sbpcgs_cattle_is_out

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SpringBarleyPeaCloverGrass_H
