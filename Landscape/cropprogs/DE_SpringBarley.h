/**
\file
\brief
<B>DE_SpringBarley.h This file contains the headers for the DE_SpringBarley class</B> \n
*/
/**
\file 
 by Chris J. Topping and Elzbieta Ziolkowska - modified by Luna Kondrup Marcussen \n
 Version of November 2022 \n
 All rights reserved. \n
 \n
*/
//
// DE_SpringBarley.
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_SPRINGBARLEY_H
#define DE_SPRINGBARLEY_H

#define DE_SPRINGBARLEY_BASE 39200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DE_SB_FUNGII	a_field->m_user[1]


/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_sb_start = 1, // Compulsory, must always be 1 (one).
	de_sb_sleep_all_day = DE_SPRINGBARLEY_BASE,
	de_sb_ferti_p1,
	de_sb_ferti_s1,
	de_sb_spring_plough,
	de_sb_ferti_p2,
	de_sb_ferti_s2,
	de_sb_preseeding_cultivator_sow,
	de_sb_herbicide,
	de_sb_fungicide,
	de_sb_insecticide,
	de_sb_growth_regulator1,
	de_sb_growth_regulator2,
	de_sb_harvest,
	de_sb_straw_chopping,
	de_sb_hay_bailing,
	de_sb_foobar, // Obligatory, must be last
} DE_SPRINGBARLEYToDo;


/**
\brief
DE_SpringBarley class
\n
*/
/**
See DE_SpringBarley.h::DE_SpringBarleyToDo for a complete list of all possible events triggered codes by the Spring Rye management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_SpringBarley : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_SpringBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date = g_date->DayInYear(19, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_sb_foobar - DE_SPRINGBARLEY_BASE);
		m_base_elements_no = DE_SPRINGBARLEY_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others,	// zero element unused but must be here
			 // ALL THE NECESSARY ENTRIES HERE
			 fmc_Others,			//	de_sb_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Others,			//	de_sb_sleep_all_day
			 fmc_Fertilizer,		//	de_sb_ferti_p1,
			 fmc_Fertilizer,		//	de_sb_ferti_s1,
			 fmc_Cultivation,	//	de_sb_spring_plough,
			 fmc_Fertilizer,		//	de_sb_ferti_p2,
			 fmc_Fertilizer,		//	de_sb_ferti_s2,
			 fmc_Cultivation,	//	de_sb_preseeding_cultivator_sow,
			 fmc_Herbicide,		//	de_sb_herbicide,
			 fmc_Fungicide,		//	de_sb_fungicide,
			 fmc_Insecticide,		//	de_sb_insecticide,
			 fmc_Others,		//	de_sb_growth_regulator1,
			 fmc_Others,		//	de_sb_growth_regulator2,
			 fmc_Harvest,		//	de_sb_harvest,
			 fmc_Others,			//	de_sb_straw_chopping,
			 fmc_Others,			//	de_sb_hay_bailing,

			// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_SPRINGBARLEY_H

