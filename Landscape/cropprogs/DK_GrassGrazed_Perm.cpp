/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_GrassGrazed_Perm.cpp This file contains the source for the DK_GrassGrazed_Perm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of May 2022 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_GrassGrazed_Perm.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_GrassGrazed_Perm.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_grassgrazed_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_GGP_InsecticideDay;
extern CfgInt   cfg_GGP_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_GrassGrazed_Perm::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKGrassGrazed_Perm;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_ggp_start:
	{
		DK_GGP_MANURE_S = false;
		DK_GGP_MANURE_P = false;
		DK_GGP_GRAZE = false;
		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(30, 8); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(4 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 6); // last possible day of cutting
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(16,6); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)  // straw chopping
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(15, 8); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) // cutting
		flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
		flexdates[3][1] = g_date->DayInYear(16, 8); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) // straw chopping
		flexdates[4][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 4 (start op 4)
		flexdates[4][1] = g_date->DayInYear(30, 8); // This date will be moved back as far as necessary and potentially to flexdates 4 (end op 4) // herbicide

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(0, flexdates, int(dk_ggp_sleep_all_day))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(2, 7);
		// OK, let's go.
		SimpleEvent(d1, dk_ggp_water1, false);

	}
	break;

	// LKM: This is the first real farm operation // suggests 50% of all do grazing, 50% do cutting - of the 50% who cuts, 25% (half) fertilize now, rest in spring next year)
	case dk_ggp_water1:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_water1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ggp_manure_s1, false);
		break;

	case dk_ggp_manure_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (a_farm->IsStockFarmer()) {
				if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
				{
					if (!a_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
						SimpleEvent(g_date->Date() + 1, dk_ggp_manure_s1, true);
						break;
					}
					else { //we want to remember who did this
						DK_GGP_MANURE_S = true;
					}
				}
				SimpleEvent(g_date->Date() + 7, dk_ggp_cutting1, false);
				break;
			}
			else SimpleEvent(g_date->Date() + 1, dk_ggp_manure_p1, false);
			break;
		}
		else if (m_ev->m_lock || m_farm->DoIt_prob(0.50/0.50)) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cutting_graze, false);
			break;
		}

	case dk_ggp_manure_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!a_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ggp_manure_p1, true);
				break;
			}
			else { //we want to remember who did this
				DK_GGP_MANURE_P = true;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_ggp_cutting1, false);
		break;

	case dk_ggp_cutting1:
		if (!a_farm->CutToHay(m_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cutting1, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ggp_straw_chopping1, false);
		break;

	case dk_ggp_straw_chopping1:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(21, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_straw_chopping1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 45, dk_ggp_cutting2, false);
		break;

	case dk_ggp_cutting_graze:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->CutToHay(m_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ggp_cutting_graze, true);
				break;
			}
			else {
				// we want to remember who did this (not cutting, but grazing)
				DK_GGP_GRAZE = true;
			}
		}
		SimpleEvent(g_date->Date(), dk_ggp_straw_chopping_graze, false);
		break;

	case dk_ggp_straw_chopping_graze:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(21, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_straw_chopping_graze, true);
			break;
		}
		SimpleEvent(g_date->Date() + 7, dk_ggp_grazing1, false);
		break;

	case dk_ggp_grazing1:
		if (!m_farm->CattleOut(m_field, 0.0,
			g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_grazing1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ggp_cattle_out1, false);
		break;

	case dk_ggp_cattle_out1:    // Keep the cattle out there
								 // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(20, 10) - g_date->DayInYear(), g_date->DayInYear(20, 10))) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cattle_out1, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 2) + 365, dk_ggp_sleep_all_day, false);
		break;

	case dk_ggp_cutting2:
		if (!a_farm->CutToHay(m_field, 0.0, g_date->DayInYear(20, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cutting2, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ggp_straw_chopping2, false);
		break;

	case dk_ggp_straw_chopping2:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(21, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_straw_chopping2, true);
			break;
		}

		d1 = g_date->OldDays() + g_date->DayInYear(5, 9);
		if (g_date->Date() <= d1) {
			SimpleEvent(g_date->Date() + 45, dk_ggp_cutting3, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 2) + 365, dk_ggp_sleep_all_day, false);
		break;

	case dk_ggp_cutting3:
		if (!a_farm->CutToHay(m_field, 0.0, g_date->DayInYear(20, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cutting3, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ggp_straw_chopping3, false);
		break;

	case dk_ggp_straw_chopping3:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(21, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_straw_chopping3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 2) + 365, dk_ggp_sleep_all_day, false);
		break;

	case dk_ggp_sleep_all_day:
		if (!a_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(1, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_sleep_all_day, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ggp_manure_s2, false); // ferti thread - main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ggp_water2, false);// water thread
		break;


	case dk_ggp_water2:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_water2, true);
			break;
		}
		break; // end of thread

	case dk_ggp_manure_s2:
		if (a_farm->IsStockFarmer()) {
			if (DK_GGP_GRAZE == false) {
				if (DK_GGP_MANURE_S == false) {
					if (!a_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
						SimpleEvent(g_date->Date() + 1, dk_ggp_manure_s2, true);
						break;
					}
				}
				SimpleEvent(g_date->Date() + 14, dk_ggp_cutting4, false);
				break;
			}
			else
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ggp_grazing2, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_ggp_manure_p2, false);
		break;

	case dk_ggp_manure_p2:
		if (DK_GGP_GRAZE == false) {
			if (DK_GGP_MANURE_P == false) {
				if (!a_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ggp_manure_p2, true);
					break;
				}
			}
			SimpleEvent(g_date->Date() + 14, dk_ggp_cutting4, false);
			break;
		}
		else
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), dk_ggp_grazing2, false);
		break;

	case dk_ggp_grazing2:
		if (!m_farm->CattleOut(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_grazing2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ggp_cattle_out2, false);
		break;

	case dk_ggp_cattle_out2:    // Keep the cattle out there
								 // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear(), g_date->DayInYear(1, 7))) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cattle_out2, false);
			break;
		}
		SimpleEvent(g_date->Date() + 14, dk_ggp_herbicide, false);
		break;

	case dk_ggp_herbicide:
		if (!m_farm->HerbicideTreat(m_field, 0.0, m_field->GetMDates(1, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_herbicide, true);
			break;
		} // end of management for grazed grass
		d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		if (g_date->Date() <= d1) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_ggp_wait, false);
			break;
		}
		else
		done = true;
		break;

	case dk_ggp_cutting4:
		if (!a_farm->CutToHay(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cutting4, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ggp_straw_chopping4, false);
		break;

	case dk_ggp_straw_chopping4:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(16, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_straw_chopping4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 45, dk_ggp_cutting5, false);
		break;

	case dk_ggp_cutting5:
		if (!a_farm->CutToHay(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cutting5, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ggp_straw_chopping5, false);
		break;

	case dk_ggp_straw_chopping5:
		if (!a_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_straw_chopping5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 45, dk_ggp_cutting6, false);
		break;

	case dk_ggp_cutting6:
		if (!a_farm->CutToHay(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_cutting6, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ggp_straw_chopping6, false);
		break;

	case dk_ggp_straw_chopping6:
		if (!a_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ggp_straw_chopping6, true);
			break;
		}
		d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		if (g_date->Date() <= d1) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_ggp_wait, false);
			break;
		}
		else
		// end of management for cut grass
		done = true;
		break;

	case dk_ggp_wait:
		// end of management 
		done = true;
		break;

		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
		default:
		g_msg->Warn(WARN_BUG, "DK_GrassGrazed_Perm::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}