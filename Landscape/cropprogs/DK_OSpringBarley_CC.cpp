//
// OSpringBarley_CC.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OSpringBarley_CC.h"
#include "math.h"

extern CfgBool cfg_SpringBarley_CC_SkScrapes;
extern CfgFloat cfg_DKCatchCropPct;

bool DK_OSpringBarley_CC::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{

  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1 = 0;

  bool done = false;
  int l_nextcropstartdate;
  TTypesOfVegetation l_tov = tov_DKOSpringBarley_CC;
  switch ( m_ev->m_todo )
  {
  case dk_osbmcc_start:
  {

      a_field->ClearManagementActionSum();
      m_last_date = g_date->DayInYear(21, 8); // Should match the last flexdate below
      //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(3 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
      // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(17, 8); // last possible day of in this case swathing - this is in effect day before the earliest date that a following crop can use
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(20, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) // harvest
      flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
      flexdates[2][1] = g_date->DayInYear(21, 8); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) // straw chopping
      flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
      flexdates[3][1] = g_date->DayInYear(21, 8); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) // hay baling

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 365;
      if (StartUpCrop(isSpring, flexdates, int(dk_osbmcc_spring_harrow1))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
      if (m_ev->m_forcespring) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_osbmcc_spring_harrow1, false);
          break;
      }
      else
          // OK, let's go.
          SimpleEvent(d1, dk_osbmcc_spring_harrow1, false);
      break;
  }
  break;

  case dk_osbmcc_spring_harrow1:
      if (m_ev->m_lock || m_farm->DoIt(100)) {
          if (!m_farm->SpringHarrow(m_field, 0.0,
              g_date->DayInYear(29, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbmcc_spring_harrow1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_osbmcc_spring_harrow2, false);
      break;

  case dk_osbmcc_spring_harrow2:
      if (m_ev->m_lock || m_farm->DoIt(50)) { //suggestion as done 1-2 times
          if (!m_farm->SpringHarrow(m_field, 0.0,
              g_date->DayInYear(30, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbmcc_spring_harrow2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date(), dk_osbmcc_slurry_s, false);
      break;
  case dk_osbmcc_slurry_s:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Slurry(m_field, 0.0,
              g_date->DayInYear(30, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbmcc_slurry_s, true);
              break;
          } 
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_osbmcc_spring_plough, false);
          break;
      }
      else SimpleEvent(g_date->Date(), dk_osbmcc_slurry_p, false);
      break;

  case dk_osbmcc_slurry_p:
      if (!m_farm->FP_Slurry(m_field, 0.0,
          g_date->DayInYear(30, 3) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbmcc_slurry_p, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_osbmcc_spring_plough, false);
      break;
     
  case dk_osbmcc_spring_plough:
      if (!m_farm->SpringPlough(m_field, 0.0, 
          g_date->DayInYear(29, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbmcc_spring_plough, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_osbmcc_spring_sow, false);
      break;
  case dk_osbmcc_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {  
      SimpleEvent( g_date->Date() + 1, dk_osbmcc_spring_sow, true );
      break;
    }
      SimpleEvent(g_date->Date() + 1, dk_osbmcc_shallow_harrow1, false);
      break;
  case dk_osbmcc_shallow_harrow1:
      if (m_ev->m_lock || m_farm->DoIt(80)) {
          if (!m_farm->ShallowHarrow(m_field, 0.0,
              g_date->DayInYear(10, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbmcc_shallow_harrow1, true);
              break;
          }
          SimpleEvent(g_date->Date() + 7, dk_osbmcc_shallow_harrow2, false);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_osbmcc_row_cultivation1, false);
      break;
  case dk_osbmcc_shallow_harrow2:
      if (!m_farm->ShallowHarrow(m_field, 0.0,
          g_date->DayInYear(20, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbmcc_shallow_harrow2, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_osbmcc_water, false);
      break;
  case dk_osbmcc_row_cultivation1:
      if (!m_farm->RowCultivation(m_field, 0.0,
          g_date->DayInYear(10, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbmcc_row_cultivation1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 7, dk_osbmcc_row_cultivation2, false);
      break;
  case dk_osbmcc_row_cultivation2:
      if (!m_farm->RowCultivation(m_field, 0.0,
          g_date->DayInYear(20, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbmcc_row_cultivation2, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_osbmcc_water, false);
      break;
   case dk_osbmcc_water:
       if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
           if (!m_farm->Water(m_field, 0.0,
               g_date->DayInYear(30, 7) - g_date->DayInYear())) {
               SimpleEvent(g_date->Date() + 1, dk_osbmcc_water, true);
               break;
           }
       }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(13, 8), dk_osbmcc_swathing, false);
      break;
   case dk_osbmcc_swathing:
       if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
           if (!m_farm->Swathing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
               SimpleEvent(g_date->Date() + 1, dk_osbmcc_swathing, true);
               break;
           }
       }
       SimpleEvent(g_date->Date()+2, dk_osbmcc_harvest, false);
       break;
  case dk_osbmcc_harvest:
      // We don't move harvest days
      if (!a_farm->Harvest(a_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbmcc_harvest, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_osbmcc_straw_chopping, false);
      break;
  case dk_osbmcc_straw_chopping:
      if (m_ev->m_lock || m_farm->DoIt(10)) //10% (suggestion) do straw chopping and leave on field
      {
          // Force straw chopping to happen on the same day as harvest.
          if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbmcc_straw_chopping, true);
              break;
          }
          if (m_ev->m_lock || m_farm->DoIt_prob(cfg_DKCatchCropPct.value())) {
              SimpleEvent(g_date->Date(), dk_osbmcc_wait, false);
              break;
          }
          else
              done = true;
          break;
      }
      // Do hay baling first.
      SimpleEvent(g_date->Date(), dk_osbmcc_hay_baling, false);
      break;

  case dk_osbmcc_hay_baling:
      if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbmcc_hay_baling, true);
          break;
      }
      if (m_ev->m_lock || m_farm->DoIt_prob(cfg_DKCatchCropPct.value())) {
          SimpleEvent(g_date->Date(), dk_osbmcc_wait, false);
          break;
      }
      else
          done = true;
      break;

  case dk_osbmcc_wait:
      //set to 100% from this code - in reality ~ 42% of all spring barley
      // So we are done,but this crop uses a catch crop
      l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
      m_field->BumpRunNum();
      m_field->SetVegPatchy(false); // reverse the patchy before the next crop
      m_farm->AddNewEvent(tov_DKOCatchCrop, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
      m_field->SetVegType(tov_DKOCatchCrop, tov_Undefined); //  Two vegetation curves are specified 
      // NB no "done = true" because this crop effectively continues into the catch crop.
      if (m_field->GetUnsprayedMarginPolyRef() != -1)
      {
          LE* um = m_OurLandscape->SupplyLEPointer(m_field->GetUnsprayedMarginPolyRef());
          um->SetVegType(tov_DKOCatchCrop, tov_Undefined);
      }
      break;
      // END OF MAIN THREAD

  default:
    g_msg->Warn( WARN_BUG, "DK_OSpringBarley_CC::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}


