//
// DK_WinterRape.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following dissbaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following dissbaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSBMUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSBMAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSBMUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSBMUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKWinterRape_H
#define DKWinterRape_H

#define DK_WOSR_BASE 66300

#define DK_WOSR_FERTI_S m_field->m_user[0]
#define DK_WOSR_FERTI_P m_field->m_user[1]
#define DK_WOSR_ROW_CULT m_field->m_user[2]
#define DK_WOSR_HERBI m_field->m_user[3]
#define DK_WOSR_FORCESPRING	a_field->m_user[4]

typedef enum {
	dk_wosr_start = 1, // Compulsory, start event must always be 1 (one).
	dk_wosr_harvest = DK_WOSR_BASE,
	dk_wosr_autumn_plough,
	dk_wosr_autumn_harrow,
	dk_wosr_autumn_roll,
	dk_wosr_molluscicide,
	dk_wosr_ferti_s1,
	dk_wosr_ferti_p1,
	dk_wosr_ferti_s2,
	dk_wosr_ferti_p2,
	dk_wosr_autumn_sow,
	dk_wosr_cultivator_sow,
	dk_wosr_herbicide1,
	dk_wosr_ferti_s3,
	dk_wosr_ferti_p3,
	dk_wosr_ferti_s4,
	dk_wosr_ferti_p4,
	dk_wosr_ferti_s5,
	dk_wosr_ferti_p5,
	dk_wosr_ferti_s6,
	dk_wosr_ferti_p6,
	dk_wosr_ferti_s7,
	dk_wosr_ferti_p7,
	dk_wosr_row_cultivation1,
	dk_wosr_row_cultivation2,
	dk_wosr_row_cultivation3,
	dk_wosr_herbicide2,
	dk_wosr_herbicide3,
	dk_wosr_herbicide4,
	dk_wosr_herbicide5,
	dk_wosr_herbicide6,
	dk_wosr_herbicide7,
	dk_wosr_herbicide8,
	dk_wosr_fungicide1,
	dk_wosr_fungicide2,
	dk_wosr_insecticide1,
	dk_wosr_insecticide2,
	dk_wosr_insecticide3,
	dk_wosr_gr,
	dk_wosr_swathing,
	dk_wosr_wait,
	dk_wosr_foobar,
} DK_WinterRapeToDo;



class DK_WinterRape : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_WinterRape(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(26, 8);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_wosr_foobar - DK_WOSR_BASE);
		m_base_elements_no = DK_WOSR_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_wosr_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_wosr_harvest = DK_wosr_BASE,
			  fmc_Cultivation,	//	  dk_wosr_autumn_plough,
			  fmc_Cultivation,	//	  dk_wosr_autumn_harrow,
			  fmc_Others,	//	  dk_wosr_autumn_roll,
			  fmc_Others, // dk_wosr_molluscicide,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_s1,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_p1,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_s2,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_p2,
			  fmc_Others,	//	  dk_wosr_autumn_sow,
			  fmc_Others,	//	  dk_wosr_cultivator_sow,
			  fmc_Herbicide,	//	  dk_wosr_herbicide1,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_s3,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_p3,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_s4,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_p4,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_s5,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_p5,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_s6,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_p6,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_s7,
			  fmc_Fertilizer,	//	  dk_wosr_ferti_p7,
			  fmc_Cultivation, // dk_wosr_row_cultivation1,
			  fmc_Cultivation, // dk_wosr_row_cultivation2,
			  fmc_Cultivation, // dk_wosr_row_cultivation3,
			  fmc_Herbicide,	//	  dk_wosr_herbicide2,
			  fmc_Herbicide,	//	  dk_wosr_herbicide3,
			  fmc_Herbicide,	//	  dk_wosr_herbicide4,
			  fmc_Herbicide,	//	  dk_wosr_herbicide5,
			  fmc_Herbicide,	//	  dk_wosr_herbicide6,
			  fmc_Herbicide,	//	  dk_wosr_herbicide7,
			   fmc_Herbicide,	//	  dk_wosr_herbicide8,
			  fmc_Fungicide,	//	  dk_wosr_fungicide1,
			  fmc_Fungicide,	//	  dk_wosr_fungicide2,
			  fmc_Insecticide,	//	  dk_wosr_insecticide1,
			  fmc_Insecticide,	//	  dk_wosr_insecticide2,
			   fmc_Insecticide,	//	  dk_wosr_insecticide3,
			  fmc_Others, //		dk_wosr_gr,
			  fmc_Cutting, //		dk_wosr_swathing
			  fmc_Others, //		dk_wosr_wait,

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};


#endif // DK_WinterRape_H