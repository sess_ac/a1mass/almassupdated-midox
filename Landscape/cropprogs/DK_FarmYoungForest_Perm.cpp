//
// DK_FarmYoungForest_Perm.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_FarmYoungForest_Perm.h"


bool DK_FarmYoungForest_Perm::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  TTypesOfVegetation l_tov = tov_DKFarmYoungForest_Perm;

  switch ( m_ev->m_todo ) {
  case dk_fyfp_start:
    {
      a_field->ClearManagementActionSum();

      m_last_date = g_date->DayInYear(1, 9); // Should match the last flexdate below
          //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
              // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(1, 9); // last possible day 
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(1, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 365;
      if (StartUpCrop(isSpring, flexdates, int(dk_fyfp_do_nothing_start))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 4) + isSpring;
      // OK, let's go.
      SimpleEvent(d1, dk_fyfp_do_nothing_start, false);
  }
    break;

  case dk_fyfp_do_nothing_start:
		SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 9 ),
     	           dk_fyfp_do_nothing_stop, false );
    break;

  case dk_fyfp_do_nothing_stop:
	 
	 ChooseNextCrop (1);

    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "DK_FarmYoungForest_Perm::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


