//
// DK_SpringFodderGrass.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_SpringFodderGrass_H
#define DK_SpringFodderGrass_H

#define DK_SFG_BASE 65100

#define DK_SFG_CC m_field->m_user[0]
#define DK_SFG_CUT m_field->m_user[1]

typedef enum {
  dk_sfg_start = 1, // Compulsory, start event must always be 1 (one).
  dk_sfg_harvest  = DK_SFG_BASE,
  dk_sfg_spring_plough,
  dk_sfg_spring_roll1,
  dk_sfg_spring_roll2,
  dk_sfg_spring_harrow,
  dk_sfg_spring_roll3,
  dk_sfg_manure_pig_s, 
  dk_sfg_npk_s,
  dk_sfg_manure_pig_p, 
  dk_sfg_npk_p,
  dk_sfg_spring_sow_cc,
  dk_sfg_spring_sow,
  dk_sfg_herbicide1,
  dk_sfg_cutting1,
  dk_sfg_cutting_graze,
  dk_sfg_cutting2,
  dk_sfg_cutting3,
  dk_sfg_cutting4,
  dk_sfg_grazing,
  dk_sfg_cattle_out,
  dk_sfg_herbicide2,
  dk_sfg_water,
  dk_sfg_insecticide,
  dk_sfg_swathing,
  dk_sfg_grass_collected,
  dk_sfg_wait,
  dk_sfg_foobar,
} DK_SpringFodderGrassToDo;



class DK_SpringFodderGrass : public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_SpringFodderGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,12);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_sfg_foobar - DK_SFG_BASE);
	  m_base_elements_no = DK_SFG_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_sfg_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_sfg_harvest  = DK_LW_BASE,
			fmc_Cultivation,	//	  dk_sfg_spring_plough,
			fmc_Cultivation,	//	  dk_sfg_spring_roll1,
			fmc_Cultivation,	//	  dk_sfg_spring_roll2,
			fmc_Cultivation,	//	  dk_sfg_spring_harrow,
			fmc_Cultivation,	//	  dk_sfg_spring_roll3,
			fmc_Fertilizer,	//	  dk_sfg_manure_pig_s,
			fmc_Fertilizer,	//	  dk_sfg_npk_s,
			fmc_Fertilizer,	//	  dk_sfg_manure_pig_p,
			fmc_Fertilizer,	//	  dk_sfg_npk_p,
			fmc_Others,	//	  dk_sfg_spring_sow_cc,
			fmc_Others,	//	  dk_sfg_spring_sow,
			fmc_Herbicide,	//	  dk_sfg_herbicide1,
			fmc_Cutting, // dk_sfg_cutting1,
			fmc_Cutting, // dk_sfg_cutting_graze,
			fmc_Cutting, // dk_sfg_cutting2,
			fmc_Cutting, // dk_sfg_cutting3,
			fmc_Cutting, // dk_sfg_cutting4,
			fmc_Grazing, // dk_sfg_grazing,
			fmc_Grazing, // dk_sfg_cattle_out,
			fmc_Herbicide,	//	  dk_sfg_herbicide2,
			fmc_Watering, // dk_sfg_water,
			fmc_Insecticide,	//	  dk_sfg_insecticide,
			fmc_Cutting,	//	  dk_sfg_swathing,
			fmc_Others, // dk_sfg_grass_collected.,
			fmc_Others,	//	  dk_sfg_wait,

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }

};

#endif // DK_SpringFodderGrass_H
