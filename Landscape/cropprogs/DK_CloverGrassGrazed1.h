/**
\file
\brief
<B>DK_CloverGrassGrazed1.h This file contains the headers for the DK_CloverGrassGrazed1 class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_CloverGrassGrazed1.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_CLOVERGRASSGRAZED1_H
#define DK_CLOVERGRASSGRAZED1_H


#define DK_C1_BASE 62200
#define DK_C1_CC m_field->m_user[0]
#define DK_C1_CUT m_field->m_user[1]

typedef enum {
	dk_c1_start = 1, // Compulsory, start event must always be 1 (one).
	dk_c1_harvest = DK_C1_BASE,
	dk_c1_spring_plough,
	dk_c1_sow_spot,
	dk_c1_herbicide1,
	dk_c1_cutting1,
	dk_c1_cutting_graze,
	dk_c1_cutting2,
	dk_c1_cutting3,
	dk_c1_cutting4,
	dk_c1_grazing,
	dk_c1_cattle_out,
	dk_c1_herbicide2,
	dk_c1_water,
	dk_c1_insecticide,
	dk_c1_swathing,
	dk_c1_grass_collected,
	dk_c1_wait,
	dk_c1_foobar,
} DK_CloverGrassGrazed1ToDo;



class DK_CloverGrassGrazed1 : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_CloverGrassGrazed1(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_c1_foobar - DK_C1_BASE);
		m_base_elements_no = DK_C1_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_c1_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_c1_harvest  = DK_C1_BASE,
			  fmc_Others,	//	  dk_c1_sow_spot,
			  fmc_Herbicide,	//	  dk_c1_herbicide1,
			  fmc_Cutting, // dk_c1_cutting1,
			  fmc_Cutting, // dk_c1_cutting_graze,
			  fmc_Cutting, // dk_c1_cutting2,
			  fmc_Cutting, // dk_c1_cutting3,
			  fmc_Cutting, // dk_c1_cutting4,
			  fmc_Grazing, // dk_c1_grazing,
			  fmc_Grazing, // dk_c1_cattle_out,
			  fmc_Herbicide,	//	  dk_c1_herbicide2,
			  fmc_Watering, // dk_c1_water,
			  fmc_Insecticide,	//	  dk_c1_insecticide,
			  fmc_Cutting,	//	  dk_c1_swathing,
			  fmc_Others, // dk_c1_grass_collected.,
			  fmc_Others,	//	  dk_c1_wait,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}

};


#endif // DK_CloverGrassGrazed1_H