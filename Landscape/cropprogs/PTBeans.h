/**
\file
\brief
<B>PTBeans.h This file contains the headers for the Beans class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTBeans.h
//


#ifndef PTBEANS_H
#define PTBEANS_H

#define PTBEANS_BASE 31900
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing Beans, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_bns_start = 1, // Compulsory, must always be 1 (one).
	pt_bns_sleep_all_day = PTBEANS_BASE,
	pt_bns_stubble_harrow,
	pt_bns_autumn_plough,
	pt_bns_ferti_p1, // NPK
	pt_bns_ferti_s1,
	pt_bns_autumn_harrow,
	pt_bns_autumn_sow,
	pt_bns_harvest,

} PTBeansToDo;


/**
\brief
PTBeans class
\n
*/
/**
See PTBeans.h::PTBeansToDo for a complete list of all possible events triggered codes by the Beans management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTBeans: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTBeans(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 30th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,11 );
   }
};

#endif // PTBEANS_H

