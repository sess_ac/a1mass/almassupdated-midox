/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PTVineyards.cpp This file contains the source for the PTVineyards class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of March 2021 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTVineyards.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PTVineyards.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_vineyard_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but subsequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool PTVineyards::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_PTVineyards; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/
	int add_year = 0;

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case pt_vine_start:
	{
		// pt_vine_start just sets up all the starting conditions and reference dates that are needed to start a pt_vine
		PT_VINE_PRUNING_DATE = 0;
		PT_VINE_PRUNING_DONE = false;
		// Set up the date management stuff

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(10, 10) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PTVineyards::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}

			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PTVineyards::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PTVineyards::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4), pt_vine_suckering, false, m_farm, m_field);//5.5//
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(22, 11);//AAS: First possible data to start the first event, in this case autumn pruning of branches
		// OK, let's go.
		// Here we queue up the first event - cultivation of stubble after previous crop
		SimpleEvent_(d1, pt_vine_autumn_pruning, false, m_farm, m_field); //AAS: First event, in this case pruning of branches
	}
	break;

	// This is the first real farm operation
	case pt_vine_autumn_pruning:
		// Here we need to make sure that Pruning it is not forced at the end of the period
		if (!m_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_autumn_pruning, true, m_farm, m_field);
			break;
		}
		/* The next line contains a function call that only works during the same day. E.g. 
		If you ask the same question tomorrow the answer will always be Sleep_All_Day, 
		unless of cours other things are done */
		if (m_field->GetLastTreatment() == pt_vine_autumn_pruning) {
		PT_VINE_PRUNING_DONE = true;
		}
		if (bool(PT_VINE_PRUNING_DONE) == true) {
		//  Fork of 5 events
		// Vine care treatments - MAIN TREAT
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, pt_vine_shredding, false, m_farm, m_field);
			// Mowing treat
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, pt_vine_mowing1, false, m_farm, m_field);
			// NPK treat
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, pt_vine_npk1, false, m_farm, m_field);
			// Herbicide treat
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(28, 2) + 365, pt_vine_herbicide1, false, m_farm, m_field);
			// Insecticide treat
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(28, 3) + 365, pt_vine_insecticide1, false, m_farm, m_field);
			// Fungicide treat
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(26, 3) + 365, pt_vine_fungicide1, false, m_farm, m_field);
			break;
		}
		else {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, pt_vine_spring_pruning, false, m_farm, m_field);
			break;
		}
	case pt_vine_spring_pruning:
		if (!m_farm->Pruning(m_field, 0.0, g_date->DayInYear(18, 02) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_spring_pruning, true, m_farm, m_field);
			break;
		}
		PT_VINE_PRUNING_DATE = g_date->Date();

		//  Fork of 5 events
		// Vine care treatments - MAIN TREAT
		d1 = PT_VINE_PRUNING_DATE + 10; // min 10 days after pruning - to be checked
		SimpleEvent_(d1, pt_vine_shredding, false, m_farm, m_field);
		// Mowing treat
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), pt_vine_mowing1, false, m_farm, m_field);
		// NPK treat
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2), pt_vine_npk1, false, m_farm, m_field);
		// Herbicide treat
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(28, 2), pt_vine_herbicide1, false, m_farm, m_field);
		// Insecticide treat
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(28, 3), pt_vine_insecticide1, false, m_farm, m_field);
		// Fungicide treat
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(26, 3), pt_vine_fungicide1, false, m_farm, m_field);
		break;
	case pt_vine_shredding:
		if (!m_farm->Shredding(m_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_shredding, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4), pt_vine_suckering, false, m_farm, m_field);
		break;
	case pt_vine_suckering:
		if (!m_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_suckering, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 14; // min 2 weeks after suckering - to be checked
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 5)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 5);
		}
		SimpleEvent_(d1, pt_vine_trimming, false, m_farm, m_field);
		break;
	case pt_vine_trimming:
		if (!m_farm->Pruning(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_trimming, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1; // min 1 day after trimming - to be checked
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
		}
		SimpleEvent_(d1, pt_vine_leafthinning, false, m_farm, m_field);
		break;
	case pt_vine_leafthinning:
		if (!m_farm->LeafThinning(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_leafthinning, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pt_vine_greenharvest, false, m_farm, m_field);
		break;
	case pt_vine_greenharvest:
		if (!m_farm->GreenHarvest(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_greenharvest, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 30; // min 1 month after green harvest - to be checked / related to develop stages
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 8)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 8);
		}
		SimpleEvent_(d1, pt_vine_harvest, false, m_farm, m_field);
		break;
	// Mowing treat
	case pt_vine_mowing1:
		if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_mowing1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pt_vine_mowing2, false, m_farm, m_field);
		break;
	case pt_vine_mowing2:
		if (!m_farm->CutOrch(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_mowing2, true, m_farm, m_field);
			break;
		}
		// end of treat
		break;
	// NPK treat
	case pt_vine_npk1:
		if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_npk1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pt_vine_npk2, false, m_farm, m_field);
		break;
	case pt_vine_npk2:
		if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_npk2, true, m_farm, m_field);
			break;
		}
		// end of treat
		break;
	// Herbicide treat - check if there is any specific break between events
	case pt_vine_herbicide1:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_herbicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), pt_vine_herbicide2, false, m_farm, m_field);
		break;
	case pt_vine_herbicide2:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_herbicide2, true, m_farm, m_field);
			break;
		}
		// end of treat
		break;
	// Insecticide treat - check if there is any specific break between events
	case pt_vine_insecticide1:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_vineyard_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pt_vine_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		d1 = g_date->Date() + 30; // min 1 month after herbicide1 - to be checked / related to develop stages
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 4);
		}
		SimpleEvent_(d1, pt_vine_insecticide2, false, m_farm, m_field);
		break;
	case pt_vine_insecticide2:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_vineyard_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pt_vine_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pt_vine_insecticide3, false, m_farm, m_field);
		break;
	case pt_vine_insecticide3:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_vineyard_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pt_vine_insecticide3, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		d1 = g_date->Date() + 14; // min 2 weeks after herbicide3 - to be checked / related to develop stages
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 7)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 7);
		}
		SimpleEvent_(d1, pt_vine_insecticide4, false, m_farm, m_field);
		break;
	case pt_vine_insecticide4:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_vineyard_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pt_vine_insecticide4, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		// end of treat
		break;
	// Fungicide treat - check if there is any specific break between events
	case pt_vine_fungicide1:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(12, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(d1 = g_date->OldDays() + g_date->DayInYear(20, 4), pt_vine_fungicide2, false, m_farm, m_field);
		break;
	case pt_vine_fungicide2:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_fungicide2, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(16, 5), pt_vine_fungicide3, false, m_farm, m_field);
		break;
	case pt_vine_fungicide3:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_fungicide3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), pt_vine_fungicide4, false, m_farm, m_field);
		break;
	case pt_vine_fungicide4:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_fungicide4, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(21, 6), pt_vine_fungicide5, false, m_farm, m_field);
		break;
	case pt_vine_fungicide5:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_fungicide5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), pt_vine_fungicide6, false, m_farm, m_field);
		break;
	case pt_vine_fungicide6:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_vine_fungicide6, true, m_farm, m_field);
			break;
		}
		// end of treat
		break;
	case pt_vine_harvest:
		if (!m_farm->FruitHarvest(m_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pt_vine_harvest, true, m_farm, m_field);
				break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;		
	default:
		g_msg->Warn(WARN_BUG, "PTVineyards::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}