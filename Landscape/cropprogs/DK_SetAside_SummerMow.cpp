/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_SetAside_SummerMow.cpp This file contains the source for the DK_SetAside_SummerMow class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of April 2022 \n
 \n
*/
//
// DK_SetAside_SummerMow.cpp
//
/*

Copyright (c) 2022, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_SetAside_SummerMow.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_SetAside_SM_SkScrapes("DK_CROP_SASM_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_SetAside_SM_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_SASM_InsecticideDay;
extern CfgInt   cfg_SASM_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DK_SetAside_SummerMow::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKSetAside_SummerMow;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_sasm_start:
	{
		DK_SASM_EVERY_2ND_YEAR = 0;
		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(30, 9); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(3 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 8); // last possible day of cutting in this case 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(16, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - strigling
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(16, 8); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - herbicide
		flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
		flexdates[3][1] = g_date->DayInYear(30, 8); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) - swathing

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(0, flexdates, int(dk_sasm_harrow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 12);
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if ((DK_SASM_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 2)
		{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 12);
				if (g_date->Date() >= d1) {
					d1 = g_date->Date();
				}
				SimpleEvent(d1, dk_sasm_wait1, false);
		}
		else if ((DK_SASM_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 1)
		{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 12);
				if (g_date->Date() >= d1) {
					d1 = g_date->Date();
				}
				SimpleEvent(d1, dk_sasm_wait1, false);
		}
		else if ((DK_SASM_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 2 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 12);
			if (g_date->Date() >= d1) {
				d1 = g_date->Date();
			}
			SimpleEvent(d1, dk_sasm_wait2, false);
		}
		break;

	}
	break;
	// LKM: This is the first real farm operation - soil cultivation only needed if establishment of flower og pollinator setaside (then cutting is not done in that year)- suggests 50%, otherwise straight to the cutting
	case dk_sasm_wait1:
		if (!a_farm->SleepAllDay(a_field, 0.0, g_date->DayInYear(31, 12) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sasm_wait1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, dk_sasm_harrow, false);
		break;

	case dk_sasm_wait2:
		if (!a_farm->SleepAllDay(a_field, 0.0, g_date->DayInYear(31, 12) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sasm_wait2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8) + 365, dk_sasm_cutting, false);
		break;
	
	case dk_sasm_harrow:
		if (a_ev->m_lock || a_farm->DoIt_prob(.50)) {
			if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sasm_harrow, true);
				break;

			}
			SimpleEvent(g_date->Date(), dk_sasm_roll, false);
			break;
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.50 / .50)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_sasm_cutting, false);
			break;
		}
		
	case dk_sasm_roll:
		if (!a_farm->SpringRoll(a_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sasm_roll, true);
			break;
		}
			SimpleEvent(g_date->Date(), dk_sasm_sow, false);
			break;

	case dk_sasm_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sasm_sow, true);
			break;
		}
		SimpleEvent(g_date->Date()+30, dk_sasm_strigling, false);
		break;

	case dk_sasm_cutting: // only allowed between 1 of aug to 25 of oct (when summer cutting) - changed end date to 15/8 to fit rots
		if (!a_farm->CutToHay(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sasm_cutting, true);
			break;
		}
		SimpleEvent(g_date->Date()+1, dk_sasm_strigling, false);
		break;

	case dk_sasm_strigling:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->Strigling(a_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sasm_strigling, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_sasm_herbicide, false);
		break;

	case dk_sasm_herbicide: // only allowed if risk of cross pollination - suggests 10%
		if (a_ev->m_lock || a_farm->DoIt(10)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sasm_herbicide, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+14, dk_sasm_swathing, false);
		break;

	case dk_sasm_swathing:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->Swathing(a_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sasm_swathing, true);
				break;
			}
		}
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_sasm_wait, false);
			// Because we are ending harvest before 1.7 so we need to wait until the 1.7
			break;
		}
		else {
			done = true; // end of plan
		}
	case dk_sasm_wait:
		// End of management
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		// End of management

	default:
		g_msg->Warn(WARN_BUG, "DK_SetAside_SummerMow::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}