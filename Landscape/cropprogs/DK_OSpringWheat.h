//
// DK_OSpringWheat.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OSpringWheat_H
#define DK_OSpringWheat_H

#define  DK_OSW_FORCESPRING   a_field->m_user[1]

#define DK_OSW_BASE 160000

typedef enum {
	dk_osw_start = 1, // Compulsory, start event must always be 1 (one).
	dk_osw_spring_harrow1 = DK_OSW_BASE,
	dk_osw_spring_harrow2,
	dk_osw_slurry_s,
	dk_osw_slurry_p,
	dk_osw_spring_plough,
	dk_osw_spring_sow,
	dk_osw_strigling1,
	dk_osw_strigling2,
	dk_osw_roll,
	dk_osw_weed_harrow,
	dk_osw_row_cultivation,
	dk_osw_water,
	dk_osw_swathing,
	dk_osw_harvest,
	dk_osw_hay_bailing,
	dk_osw_straw_chopping,
	dk_osw_foobar
} DK_OSpringWheatToDo;



class DK_OSpringWheat : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OSpringWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(28, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_osw_foobar - DK_OSW_BASE);
		m_base_elements_no = DK_OSW_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_osw_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation, //dk_osw_spring_harrow1 = DK_OSW_BASE,
			fmc_Cultivation, //dk_osw_spring_harrow2,
			fmc_Fertilizer, //dk_osw_slurry_s,
			fmc_Fertilizer, //dk_osw_slurry_p,
			fmc_Cultivation, //dk_osw_spring_plough,
			fmc_Others, //dk_osw_spring_sow,
			fmc_Cultivation, //dk_osw_strigling1,
			fmc_Cultivation, //  dk_osw_strigling2,
			fmc_Others, //  dk_osw_roll,
			fmc_Cultivation, // dk_osw_weed_harrow,
			fmc_Cultivation, //  dk_osw_row_cultivation,
			fmc_Watering, // dk_osw_water,
			fmc_Cutting, // dk_osw_swathing,
			fmc_Harvest, //  dk_osw_harvest,
			fmc_Others, //  dk_osw_hay_bailing,
			fmc_Cutting, //  dk_osw_straw_chopping,
						// No foobar entry
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_OSpringWheat_H
