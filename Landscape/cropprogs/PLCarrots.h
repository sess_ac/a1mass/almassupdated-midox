/**
\file
\brief
<B>PLCarrots.h This file contains the headers for the Carrots class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLCarrots.h
//


#ifndef PLCarrots_H
#define PLCarrots_H

#define PLCarrots_BASE 25300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_CA_STUBBLE_PLOUGH	a_field->m_user[1]
#define PL_CA_WEEDING			a_field->m_user[2]
#define PL_CA_HERBI				a_field->m_user[3]

/** Below is the list of things that a farmer can do if he is growing carrots, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_ca_start = 1, // Compulsory, must always be 1 (one).
	pl_ca_sleep_all_day = PLCarrots_BASE,
	pl_ca_stubble_plough,
	pl_ca_autumn_harrow1,
	pl_ca_autumn_harrow2,
	pl_ca_stubble_harrow,
	pl_ca_ferti_p1,
	pl_ca_ferti_s1,
	pl_ca_winter_plough,
	pl_ca_ferti_p2,
	pl_ca_ferti_s2,
	pl_ca_spring_harrow,
	pl_ca_preseeding_cultivator,
	pl_ca_bed_forming,
	pl_ca_spring_sow,
	pl_ca_strigling,
	pl_ca_herbicide1,
	pl_ca_herbicide2,
	pl_ca_herbicide3,
	pl_ca_fungicide1,
	pl_ca_fungicide2,
	pl_ca_ferti_p3,
	pl_ca_ferti_s3,	
	pl_ca_harvest,
	pl_ca_ferti_p4,
	pl_ca_ferti_s4,
	pl_ca_foobar,
} PLCarrotsToDo;


/**
\brief
PLCarrots class
\n
*/
/**
See PLCarrots.h::PLCarrotsToDo for a complete list of all possible events triggered codes by the carrots management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLCarrots: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLCarrots(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_ca_foobar - PLCarrots_BASE);
	   m_base_elements_no = PLCarrots_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_ca_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_ca_sleep_all_day = PLCarrots_BASE,
			fmc_Cultivation,//pl_ca_stubble_plough,
			fmc_Cultivation,//pl_ca_autumn_harrow1,
			fmc_Cultivation,//pl_ca_autumn_harrow2,
			fmc_Cultivation,//pl_ca_stubble_harrow,
			fmc_Fertilizer,//pl_ca_ferti_p1,
			fmc_Fertilizer,//pl_ca_ferti_s1,
			fmc_Cultivation,//pl_ca_winter_plough,
			fmc_Fertilizer,//pl_ca_ferti_p2,
			fmc_Fertilizer,//pl_ca_ferti_s2,
			fmc_Cultivation,//pl_ca_spring_harrow,
			fmc_Cultivation,//pl_ca_preseeding_cultivator,
			fmc_Others,//pl_ca_bed_forming,
			fmc_Others,//pl_ca_spring_sow,
			fmc_Others,//pl_ca_strigling,
			fmc_Herbicide,//pl_ca_herbicide1,
			fmc_Herbicide,//pl_ca_herbicide2,
			fmc_Herbicide,//pl_ca_herbicide3,
			fmc_Fungicide,//pl_ca_fungicide1,
			fmc_Fungicide,//pl_ca_fungicide2,
			fmc_Fertilizer,//pl_ca_ferti_p3,
			fmc_Fertilizer,//pl_ca_ferti_s3,
			fmc_Harvest,//pl_ca_harvest,
			fmc_Fertilizer,//pl_ca_ferti_p4,
			fmc_Fertilizer//pl_ca_ferti_s4,
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLCarrots_H

