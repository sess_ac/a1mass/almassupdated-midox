/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>WinterWheat.cpp This file contains the source for the WinterWheat class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of June 2003 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// WinterWheat.cpp
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WinterWheat.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_WinterWheat_SkScrapes("CROP_WW_SK_SCRAPES",CFG_CUSTOM, false);
extern CfgBool cfg_pest_winterwheat_on;
extern CfgBool cfg_seedcoating_winterwheat_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_WW_InsecticideDay;
extern CfgInt   cfg_WW_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;
extern CfgFloat cfg_seed_coating_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool WinterWheat::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	
					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case ww_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		WW_AUTUMN_PLOUGH = false;
		WW_DECIDE_TO_HERB = 1;
		WW_DECIDE_TO_FI = 1;
		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
		if (cfg_WinterWheat_SkScrapes.value()) {
			a_field->m_skylarkscrapes = true;
		}
		else {
			a_field->m_skylarkscrapes = false;
		}
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 5 start and stop dates for all events after harvest for this crop
		int noDates = 5;
		a_field->SetMDates(0, 0, g_date->DayInYear(20, 8));
		// Determined by harvest date - used to see if at all possible
		a_field->SetMDates(1, 0, g_date->DayInYear(20, 8));
		a_field->SetMDates(0, 1, 0); // HayTurning start
		a_field->SetMDates(1, 1, g_date->DayInYear(20, 8));
		a_field->SetMDates(0, 2, g_date->DayInYear(5, 8));
		a_field->SetMDates(1, 2, g_date->DayInYear(20, 8)); //was 25, 8
		a_field->SetMDates(0, 3, g_date->DayInYear(10, 8));
		a_field->SetMDates(1, 3, g_date->DayInYear(20, 8)); //was 15,9
		a_field->SetMDates(0, 4, g_date->DayInYear(15, 8));
		a_field->SetMDates(1, 4, g_date->DayInYear(20, 8)); //was 15, 10
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "WinterWheat::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "WinterWheat::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "WinterWheat::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), ww_spring_roll, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(21, 8);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have a
		// stock or arable farmer
		if (a_farm->IsStockFarmer()) { // StockFarmer
			SimpleEvent(d1, ww_ferti_s1, false);
		}
		else SimpleEvent(d1, ww_ferti_p1, false);

	}
	break;
	// This is the first real farm operation
	case ww_ferti_p1:
		// Unless we are forced to do this (probably because we tried once and failed due to bad weather)
		// then 10% of all threads that reach here will attempt to do ww_feri_p1
		if (a_ev->m_lock || a_farm->DoIt(10))
		{
			// Calls the FP_Slurry application with the number of days after 1,10 as a parameter
			// the FP_Slurry routine will attempt to apply slurry on a probabilistic basis up to 1,10 when
			// the operation will be forced
			// Many other constraints can be added to the FP_Slurry, e.g. temperature might have to be >0 degrees C
			// Other operations need low wind speed (e.g. spraying insecticide). These differ for each operation
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again (until 1/10 when we will suceed)
				SimpleEvent(g_date->Date() + 1, ww_ferti_p1, true);
				break;
			}
		}
		// Queue up the next event -in this case autumn ploughing
		SimpleEvent(g_date->Date(), ww_autumn_plough, false);
		break;
	case ww_ferti_s1:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ww_ferti_s1, true);
			break;
		}
		// This may cause two applications of fertilizer in one day...which is OK because one is Slurry and one Manure
		SimpleEvent(g_date->Date(), ww_ferti_s2, false);
		break;
	case ww_ferti_s2:
		if (a_ev->m_lock || a_farm->DoIt(10))
		{
			if (!a_farm->FA_Manure(a_field, 0.0,
				g_date->DayInYear(1, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_ferti_s2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), ww_autumn_plough, false);
		break;
	case ww_autumn_plough:
		/**
		* Inputs to a farm mangement event call.
		*
		* bool forcing a_eve->m_lock
		* bool probability (if forcing lock is false)
		* Farm management to try (function address)
		* Event type
		* Finish date
		* Flags to set on success
		* Flage value to set
		* Next event
		* Start date for next evemt
		*/
		// Almost all will autumn plough, but a few will get away with non-inversion
		if (a_ev->m_lock || a_farm->DoIt(95))
		{
			if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_autumn_plough, true);
				break;
			}
			else
			{
				// 95% of farmers will do this, but the other 5% won't so we need to remember whether
				// we are in one or the other group
				WW_AUTUMN_PLOUGH = true;
				// Queue up the next event
				SimpleEvent(g_date->Date() + 1, ww_autumn_harrow, false);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, ww_stubble_harrow1, false);
		break;
	case ww_autumn_harrow:
		if (!a_farm->AutumnHarrow(a_field, 0.0,
			g_date->DayInYear(10, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ww_autumn_harrow, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 9),
			ww_autumn_sow, false);
		break;
	case ww_stubble_harrow1:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ww_stubble_harrow1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 9),
			ww_autumn_sow, false);
		break;
	case ww_autumn_sow:
		if (cfg_seedcoating_winterwheat_on.value() && a_field->GetLandscape()->SupplyShouldSpray()) {
			if (!a_farm->AutumnSow(a_field, 0.0,
				g_date->DayInYear(20, 10) - g_date->DayInYear(),
				cfg_seed_coating_product_1_amount.value(), ppp_1)) {
				SimpleEvent(g_date->Date() + 1, ww_autumn_sow, true);
				break;
			}
		}
		else {
			if (!a_farm->AutumnSow(a_field, 0.0,
				g_date->DayInYear(20, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_autumn_sow, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, ww_autumn_roll, false);
		break;
	case ww_autumn_roll:
		// If they did plough, then 5% will also do an autumn rolling
		if ((a_ev->m_lock || a_farm->DoIt(5)) && (WW_AUTUMN_PLOUGH))
		{
			if (!a_farm->AutumnRoll(a_field, 0.0,
				g_date->DayInYear(27, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_autumn_roll, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 9),
			ww_ferti_p2, false);
		break;
		// Plant farmers start here.
	case ww_ferti_p2:
		if ((a_ev->m_lock || a_farm->DoIt(20)) && (!a_farm->IsStockFarmer()))
		{
			if (a_field->GetVegBiomass() > 0)
				//only apply this when there has been a bit of growth
			{
				if (!a_farm->FP_ManganeseSulphate(a_field, 0.0,
					g_date->DayInYear(30, 10) - g_date->DayInYear()))
				{
					SimpleEvent(g_date->Date() + 1, ww_ferti_p2, true);
					break;
				}
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 9),
			ww_herbicide1, false);
		break;
	case ww_herbicide1: // The first of the pesticide managements. NB these are externally scalable
		if (a_ev->m_lock || a_farm->DoIt((int)(80 * cfg_herbi_app_prop.value() * a_farm->Prob_multiplier()))) //modified probability
		{
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ww_herbicide1, true);
					break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4) + 365,
			ww_spring_roll, false);
		break;
	case ww_spring_roll:
		if (a_ev->m_lock || a_farm->DoIt(5))
		{
			if (!a_farm->SpringRoll(a_field, 0.0,
				g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_spring_roll, true);
				break;
			}
		}
		// Here we split the main thread in to depending on what fertilizer the farmer will use
		// these rejoin other later on.
		if (a_farm->IsStockFarmer()) // StockFarmer
		{
			SimpleEvent(g_date->Date() + 1, ww_ferti_s3, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), ww_ferti_s4, false);
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 3), ww_ferti_p3, false);
		// Here we see an example of setting up parallel threads
		// None of these are the main thread which will lead to the termination of the plan - they
		// just run along until they peter out e.g. ww_herbicide2
		// All paths through the plan need the next threads
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4),
			ww_herbicide2, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 4),
			ww_GR, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4),
			ww_fungicide, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(cfg_WW_InsecticideDay.value(), cfg_WW_InsecticideMonth.value()),
			ww_insecticide1, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4),
			ww_strigling1, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5),
			ww_water1, false);
		break;
	case ww_herbicide2:
		if (a_ev->m_lock || a_farm->DoIt((int)((int)(69 * cfg_herbi_app_prop.value()*WW_DECIDE_TO_HERB  * a_farm->Prob_multiplier())))) // was 40 //modified probability
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0,
				g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_herbicide2, true);
				break;
			}
		}
		// End of thread
		break;
	case ww_GR:
		if (a_ev->m_lock || a_farm->DoIt((int)(15 * cfg_greg_app_prop.value())))
		{
			if (!a_farm->GrowthRegulator(a_field, 0.0,
				g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_GR, true);
				break;
			}
		}
		// End of thread
		break;
	case ww_fungicide:
		if (a_ev->m_lock || a_farm->DoIt((int)(55 * cfg_fungi_app_prop1.value() * a_farm->Prob_multiplier()))) //modified probability
		{
				if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ww_fungicide, true);
					break;
				}
			
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 5), ww_fungicide2, false);
		break;
	case ww_fungicide2:
		if (a_ev->m_lock || a_farm->DoIt((int)(55 * cfg_fungi_app_prop2.value() * a_farm->Prob_multiplier()))) //modified probability
		{
				if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ww_fungicide2, true);
					break;
				}
			
		}
		// End of thread
		break;
	case ww_insecticide1:
		if (a_ev->m_lock || a_farm->DoIt((int)(50 * cfg_ins_app_prop1.value() * a_farm->Prob_multiplier()))) //changed from 16, 22.03.13 //modified probability
		{
				// Here we check wheter we are using ERA pesticde or not
				if (!cfg_pest_winterwheat_on.value() || !a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
                {
                    if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
                        SimpleEvent(g_date->Date() + 1, ww_insecticide1, true);
                        break;
                    }
                }
				else
				{
					a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(),ppp_1);
				}

			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), ww_insecticide2, false);
			break;
		}
		break; // End thread
	case ww_insecticide2:
		if (a_ev->m_lock || a_farm->DoIt((int)(50 * cfg_ins_app_prop2.value() *WW_DECIDE_TO_FI * a_farm->Prob_multiplier()))) //modified probability
		{
			if (!cfg_pest_winterwheat_on.value() || !a_field->GetLandscape()->SupplyShouldSpray())
			{
				if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear()))
				{
					SimpleEvent(g_date->Date() + 1, ww_insecticide2, true);
					break;
				}
			}
			else
			{
				a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if ((g_date->Date() + 7) < (g_date->OldDays() + g_date->DayInYear(15, 6)))
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), ww_insecticide3, false);
			else SimpleEvent(g_date->Date() + 7, ww_insecticide3, false);
			break;
		}
		break;
	case ww_insecticide3:
		if (a_ev->m_lock || a_farm->DoIt((int)(50 * cfg_ins_app_prop3.value() *WW_DECIDE_TO_FI * a_farm->Prob_multiplier()))) //modified probability
		{
			if (!cfg_pest_winterwheat_on.value() || !a_field->GetLandscape()->SupplyShouldSpray()) {
				if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ww_insecticide3, true);
					break;
				}
			}
			else {
				a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
	case ww_strigling1:
		if (a_ev->m_lock || a_farm->DoIt(5))
		{
			if (!a_farm->Strigling(a_field, 0.0,
				g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_strigling1, true);
				break;
			}
			else {
				if ((g_date->Date() + 7) < (g_date->OldDays() + g_date->DayInYear(15, 6)))
					SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 4),
						ww_strigling2, false);
				else SimpleEvent(g_date->Date() + 7, ww_strigling2, false);
			}
		}
		break;
	case ww_strigling2:
		if (!a_farm->Strigling(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ww_strigling2, true);
			break;
		}
		// End of thread
		break;
	case ww_water1:
		if (a_ev->m_lock || a_farm->DoIt(10)) // **CJT** Soil type 1-4 only
		{
			if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_water1, true);
				break;
			}
			else
				if ((g_date->Date() + 5) < (g_date->OldDays() + g_date->DayInYear(2, 5)))
					SimpleEvent(g_date->OldDays() + g_date->DayInYear(2, 5), ww_water2, false);
				else SimpleEvent(g_date->Date() + 5, ww_water2, false);
		}
		break;
	case ww_water2:
		if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ww_water2, true);
			break;
		}
		// End of thread
		break;
	case ww_ferti_p3:
		if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ww_ferti_p3, true);
			break;
		}

		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 4), ww_ferti_p4, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), ww_ferti_p5, false);
		break;
	case ww_ferti_p4:
		if (a_ev->m_lock || a_farm->DoIt(50))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_ferti_p4, true);
				break;
			}
		}
		// The Main thread
		// The Main thread - this will leads to harvest, whilst all the others stop before this point
		// This thread is only for arable farmers whereas you'll see below the other forks for stock farmers

		ChooseNextCrop(5);

		SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 8), ww_harvest, false);
		break;
	case ww_ferti_p5:
		if (a_ev->m_lock || a_farm->DoIt(20))
		{
			if (!a_farm->FP_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_ferti_p5, true);
				break;
			}
		}
		break;
	case ww_ferti_s3:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ww_ferti_s3, true);
			break;
		}
		// The Main thread - this will leads to harvest, whilst all the others stop before this point

		ChooseNextCrop(5);

		SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 8), ww_harvest, false);
		break;
	case ww_ferti_s4:
		if (a_ev->m_lock || a_farm->DoIt(75))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_ferti_s4, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(21, 4), ww_ferti_s5, false);
		}
		break;
	case ww_ferti_s5:
		if (a_ev->m_lock || a_farm->DoIt(40))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_ferti_s5, true);
				break;
			}
		}
		break;
	case ww_harvest:
		if (!a_farm->Harvest(a_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, ww_harvest, true);
			break;
		}
		SimpleEvent(g_date->Date(), ww_straw_chopping, false);
		break;
	case ww_straw_chopping:
		if (a_ev->m_lock || a_farm->DoIt(75))
		{
			if (a_field->GetMConstants(0) == 0) {
				if (!a_farm->StrawChopping(a_field, 0.0, -1)) { //raise an error
					g_msg->Warn(WARN_BUG, "WinterWheat::Do(): failure in 'StrawChopping' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->StrawChopping(a_field, 0.0, a_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ww_straw_chopping, true);
					break;
				}
				else {
					SimpleEvent(g_date->Date() + a_field->GetMConstants(0), ww_stubble_harrow2, false); //is this ok?
				}
			}
		}
		else {
			SimpleEvent(g_date->Date() + a_field->GetMConstants(0), ww_hay_turning, false);
		}
		break;
	case ww_hay_turning:
		if (a_ev->m_lock || a_farm->DoIt(5))
		{
			if (a_field->GetMConstants(1) == 0) {
				if (!a_farm->HayTurning(a_field, 0.0, -1)) { //raise an error
					g_msg->Warn(WARN_BUG, "WinterWheat::Do(): failure in 'HayTurning' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->HayTurning(a_field, 0.0, a_field->GetMDates(1, 1) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ww_hay_turning, true);
					break;
				}
			}
		}
		SimpleEvent(g_date->OldDays() + a_field->GetMDates(0, 2), ww_hay_baling, false);
		break;
	case ww_hay_baling:
		if (a_field->GetMConstants(2) == 0) {
			if (!a_farm->HayBailing(a_field, 0.0, -1)) { //raise an error
				g_msg->Warn(WARN_BUG, "WinterWheat::Do(): failure in 'HayBailing' execution", "");
				exit(1);
			}
		}
		else {
			if (!a_farm->HayBailing(a_field, 0.0, a_field->GetMDates(1, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ww_hay_baling, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + a_field->GetMDates(0, 3), ww_stubble_harrow2, false);
		break;
	case ww_stubble_harrow2:
		if (a_ev->m_lock || a_farm->DoIt(65))
		{
			if (a_field->GetMConstants(3) == 0) {
				if (!a_farm->StubbleHarrowing(a_field, 0.0, -1)) { //raise an error
					g_msg->Warn(WARN_BUG, "WinterWheat::Do(): failure in 'StubbleHarrowing' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->StubbleHarrowing(a_field, 0.0, a_field->GetMDates(1, 3) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ww_stubble_harrow2, true);
					break;
				}
			}
		}
		SimpleEvent(g_date->OldDays() + a_field->GetMDates(0, 4), ww_grubning, false);
		break;
	case ww_grubning:
		if (a_ev->m_lock || a_farm->DoIt(10)) {
			if (a_field->GetMConstants(4) == 0) {
				if (!a_farm->DeepPlough(a_field, 0.0, -1)) { //raise an error //IS THIS OK - PLOUGH?
					g_msg->Warn(WARN_BUG, "WinterWheat::Do(): failure in 'DeepPlough' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->DeepPlough(a_field, 0.0, a_field->GetMDates(1, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, ww_grubning, true);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "WinterWheat::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}

