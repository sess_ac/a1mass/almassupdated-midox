/**
\file
\brief
<B>DK_GrassGrazed_Perm.h This file contains the source for the DK_GrassGrazed_Perm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of May 2022 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_GrassGrazed_Perm.h
//


#ifndef DK_GRASSGRAZED_P_H
#define DK_GRASSGRAZED_P_H

#define DK_GGP_MANURE_S a_field->m_user[0]
#define DK_GGP_MANURE_P a_field->m_user[1]
#define DK_GGP_GRAZE a_field->m_user[1]

#define DK_GGP_BASE 64700
/**

*/

/** Below is the list of things that a farmer can do if he is growing grassgrazed, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_ggp_start = 1, // Compulsory, must always be 1 (one).
	dk_ggp_sleep_all_day = DK_GGP_BASE,
	dk_ggp_manure_s1,
	dk_ggp_manure_p1,
	dk_ggp_manure_s2,
	dk_ggp_manure_p2,
	dk_ggp_cutting1,
	dk_ggp_cutting2,
	dk_ggp_cutting3,
	dk_ggp_cutting4,
	dk_ggp_cutting5,
	dk_ggp_cutting6,
	dk_ggp_cutting_graze,
	dk_ggp_straw_chopping1,
	dk_ggp_straw_chopping2,
	dk_ggp_straw_chopping3,
	dk_ggp_straw_chopping4,
	dk_ggp_straw_chopping5,
	dk_ggp_straw_chopping6,
	dk_ggp_straw_chopping_graze,
	dk_ggp_grazing1,
	dk_ggp_grazing2,
	dk_ggp_cattle_out1,
	dk_ggp_cattle_out2,
	dk_ggp_water1,
	dk_ggp_water2,
	dk_ggp_herbicide,
	dk_ggp_wait,
	dk_ggp_foobar,
} DK_GrassGrazed_PermToDo;


/**
\brief
DK_GrassGrazed_Perm class
\n
*/
/**
See DK_GrassGrazed_Perm.h::DK_GrassGrazed_PermToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_GrassGrazed_Perm : public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_GrassGrazed_Perm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31, 8);
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_ggp_foobar - DK_GGP_BASE);
	   m_base_elements_no = DK_GGP_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_ggp_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_ggp_sleep_all_day = DK_GGP_BASE,
			fmc_Fertilizer, //dk_ggp_manure_s1,
			fmc_Fertilizer, //dk_ggp_manure_p1,
			fmc_Fertilizer, //dk_ggp_manure_s2,
			fmc_Fertilizer, //dk_ggp_manure_p2,
			fmc_Cutting, //dk_ggp_cutting1,
			fmc_Cutting, //dk_ggp_cutting2,
			fmc_Cutting, //dk_ggp_cutting3,
			fmc_Cutting, //dk_ggp_cutting4,
			fmc_Cutting, //dk_ggp_cutting5,
			fmc_Cutting, //dk_ggp_cutting6,
			fmc_Cutting, //dk_ggp_cutting_graze,
			fmc_Cutting, //dk_ggp_straw_chopping1,
			fmc_Cutting, //dk_ggp_straw_chopping2,
			fmc_Cutting, //dk_ggp_straw_chopping3,
			fmc_Cutting, //dk_ggp_straw_chopping4,
			fmc_Cutting, //dk_ggp_straw_chopping5,
			fmc_Cutting, //dk_ggp_straw_chopping6,
			fmc_Cutting, //dk_ggp_straw_chopping_graze,
			fmc_Grazing, //dk_ggp_grazing1,
			fmc_Grazing, //dk_ggp_grazing2,
			fmc_Grazing, //dk_ggp_cattle_out1,
			fmc_Grazing, //dk_ggp_cattle_out2,
			fmc_Watering, // dk_ggp_water1,
			fmc_Watering, // dk_ggp_water2,
			fmc_Herbicide, // dk_ggp_herbicide,
			fmc_Others, //dk_ggp_wait
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_GrassGrazed_Perm_H

