/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_Carrots.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_carrots_on;
extern CfgFloat cfg_pest_product_1_amount;


bool DK_Carrots::Do( Farm * a_farm, LE * a_field, FarmEvent * a_ev ) {
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
    TTypesOfVegetation l_tov = tov_DKCarrots;

  switch (m_ev->m_todo) {
  case dk_car_start:
  {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      DK_CAR_FORCESPRING = false;
      m_last_date = g_date->DayInYear(30, 11); // Should match the last flexdate below
      //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
      // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(30, 11); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)


      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 0;
      if (StartUpCrop(isSpring, flexdates, int(dk_car_stoneburier))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
      // OK, let's go.
      // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
      if (m_ev->m_forcespring) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_car_stoneburier, false);
          break;
          DK_CAR_FORCESPRING = true;
      }
      else SimpleEvent(d1, dk_car_herbicide1, false);
      break;
  }
  break;    
      // OK, Let's go - LKM: first treatment, herbicide1, do it before the 30th of Octotber - if not done, try again +1 day until the the 30th of October when we succeed - 100% of farmers do this
  case dk_car_herbicide1:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_car_herbicide1, true);
          break;
      }
      // LKM: Queue up next event - Stoneburier after 1st of February next year
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_car_stoneburier, false);
      break;
      // LKM: Do stoneburier before 15th of April - if not done, try again +1 day until the the 15th of April when we succeed 
  case dk_car_stoneburier:
      if (!m_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_car_stoneburier, true);
          break;
      }
      // LKM: Queue up the next event - Deep harrow done before the 20th of April - if not done, try again +1 day until the 20th of April when we will succeed
      SimpleEvent(g_date->Date(), dk_car_deep_harrow, false);
      break;
  case dk_car_deep_harrow:
      if (!m_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_car_deep_harrow, true);
          break;
      }
      // LKM: Queue up the next event - Bedformer done before the 20th of April - if not done, try again +1 day until the 20th of April when we will succeed
      SimpleEvent(g_date->Date(), dk_car_bedformer, false);
      break;
  case dk_car_bedformer:
      if (!m_farm->BedForming(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_car_bedformer, true);
          break;
      }
      // LKM: Queue up the next event - shallow harrow1 (making seedbed) done after the 5th of February and before the 5th of May - if not done, try again +1 day until the 5th of May when we will succeed
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 2) , dk_car_sharrow1, false);
      break;
  case dk_car_sharrow1:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_sharrow1, true);
          break;
      }
      // LKM: Queue up the next event - shallow harrow2 (making seedbed) done 5 days after, and before the 10th of May - if not done, try again +1 day until the 10th of May when we will succeed
      SimpleEvent(g_date->Date() + 5, dk_car_sharrow2, false);
      break;
  case dk_car_sharrow2:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_sharrow2, true);
          break;
      }
      // LKM: Queue up the next event - shallow harrow3 (making seedbed) done 5 days after, and before the 15th of May - if not done, try again +1 day until the 15th of May when we will succeed
      SimpleEvent(g_date->Date() + 5, dk_car_sharrow3, false);
      break;
  case dk_car_sharrow3:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_sharrow3, true);
          break;
      }
      // LKM: Queue up the next event - shallow harrow4 (making seedbed) done 5 days after, and before the 20th of May - if not done, try again +1 day until the 20th of May when we will succeed
      SimpleEvent(g_date->Date() + 5, dk_car_sharrow4, false);
      break;
  case dk_car_sharrow4:
      if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_sharrow4, true);
          break;
      }
      // LKM: Queue up the next event - water1 done before the 25th of May - if not done, try again +1 day until the 25th of May when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_water1, false);
      break;
  case dk_car_water1:
      if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_water1, true);
          break;
      }
      // LKM: Queue up the next event - fungicide1 done  before the 30th of May - if not done, try again +1 day until the 30th of May when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_fungicide1, false);
      break;
  case dk_car_fungicide1:
      if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_fungicide1, true);
          break;
      }
      // LKM: Queue up the next event - sow done  before the 30th of May - if not done, try again +1 day until the 30th of May when we will succeed
      SimpleEvent(g_date->Date(), dk_car_sow, false);
      break;
  case dk_car_sow:
      if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_sow, true);
          break;
      }
      // LKM: Queue up the next event - herbicide2 done  before the 5th of June - if not done, try again + 1 day until the 5th of June  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_herbicide2, false);
      break;
  case dk_car_herbicide2:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_herbicide2, true);
          break;
      }
      // LKM: Queue up the next event - herbicide3 done  before the 10th of June - if not done, try again + 1 day until the 10th of June  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_herbicide3, false);
      break;
  case dk_car_herbicide3:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_herbicide3, true);
          break;
      }
      // LKM: Queue up the next event - fertilizer1 done  before the 15th of June - if not done, try again + 1 day until the 15th of June  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_ferti_s1, false);
      break;
  case dk_car_ferti_s1:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear()))
          {
              SimpleEvent(g_date->Date() + 1, dk_car_ferti_s1, true);
              break;
          }// LKM: Queue up the next event - herbicide4 + row cultivation1 at the same time done before the 20th of June - if not done, try again + 1 day until the 20th of June  when we will succeed
           SimpleEvent(g_date->Date() + 1, dk_car_herbicide4, false);
           break;
      }
      else SimpleEvent(g_date->Date() + 1, dk_car_ferti_p1, false);
      break;

  case dk_car_ferti_p1:
      if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_ferti_p1, true);
          break;
      }// LKM: Queue up the next event - herbicide4 + row cultivation1 at the same time done before the 20th of June - if not done, try again + 1 day until the 20th of June  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_herbicide4, false);
      break;
      
  case dk_car_herbicide4:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_herbicide4, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_car_row_cultivation1, false);
      break;
  case dk_car_row_cultivation1:
      if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(21, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_row_cultivation1, true);
          break;
      }
      // LKM: Queue up the next event - herbicide5 + row cultivation2 (+8 days after) at the same time done before the 30th of June - if not done, try again + 1 day until the 30th of June  when we will succeed
      SimpleEvent(g_date->Date() + 8, dk_car_herbicide5, false);
      break;
  case dk_car_herbicide5:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(29, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_herbicide5, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_car_row_cultivation2, false);
      break;
  case dk_car_row_cultivation2:
      if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_row_cultivation2, true);
          break;
      }
      // LKM: Queue up the next event - water2 done before the 30th of June - if not done, try again +1 day until the 30th of June when we will succeed
      SimpleEvent(g_date->Date(), dk_car_water2, false);
      break;
  case dk_car_water2:
      if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_water2, true);
          break;
      }
      // LKM: Queue up the next event - hilling up1 done before the 5th of July - if not done, try again +1 day until the 5th of July when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_hilling_up1, false);
      break;
  case dk_car_hilling_up1:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_hilling_up1, true);
          break;
      }
      // LKM: Queue up the next event - herbicide6 done before the 10th of July - if not done, try again + 1 day until the 10th of July  when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_herbicide6, false);
      break;
  case dk_car_herbicide6:
      if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_herbicide6, true);
          break;
      }
      // LKM: Queue up the next event - add boron1 done before the 10th of July - if not done, try again + 1 day until the 10th of July  when we will succeed
      SimpleEvent(g_date->Date(), dk_car_boron_s1, false);
      break;
  case dk_car_boron_s1:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(11, 7) - g_date->DayInYear()))
          {
              SimpleEvent(g_date->Date() + 1, dk_car_boron_s1, true);
              break;
          }
          // LKM: Queue up the next event - water3 done before the 20th of July - if not done, try again +1 day until the 10th of July when we will succeed
          SimpleEvent(g_date->Date() + 1, dk_car_water3, false);
          break;
      }
      else
      SimpleEvent(g_date->Date(), dk_car_boron_p1, false);
      break;

  case dk_car_boron_p1:
      if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(11, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_boron_p1, true);
          break;
      }
      // LKM: Queue up the next event - water3 done before the 20th of July - if not done, try again +1 day until the 10th of July when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_water3, false);
      break;

  case dk_car_water3:
      if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_water3, true);
          break;
      }
      // LKM: Queue up the next event - hilling up2 done before the 30th of July - if not done, try again +1 day until the 30th of July when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_hilling_up2, false);
      break;
  case dk_car_hilling_up2:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_hilling_up2, true);
          break;
      }
      // LKM: Queue up the next event - boron2 done after the 1st of May, and before the 30th of August - if not done, try again +1 day until the 30th of August when we will succeed
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) , dk_car_boron_s2, false);
      break;
  case dk_car_boron_s2:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(28, 8) - g_date->DayInYear()))
          {
              SimpleEvent(g_date->Date() + 1, dk_car_boron_s2, true);
              break;
          }
          // LKM: Queue up the next event - hilling up3 done before the 30th of August - if not done, try again +1 day until the 30th of July when we will succeed
          SimpleEvent(g_date->Date(), dk_car_hilling_up3, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_car_boron_p2, false);
      break;

  case dk_car_boron_p2:
      if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(28, 8) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_boron_p2, true);
          break;
      }
      // LKM: Queue up the next event - hilling up3 done before the 30th of August - if not done, try again +1 day until the 30th of July when we will succeed
      SimpleEvent(g_date->Date(), dk_car_hilling_up3, false);
      break;

  case dk_car_hilling_up3:
      if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(29, 8) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_hilling_up3, true);
          break;
      }
      // LKM: Queue up the next event - fertilizer2 done after the 10th of June and before the 30th of August - if not done, try again +1 day until the 30th of August when we will succeed
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_car_ferti_s2, false);
      break;
  case dk_car_ferti_s2:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear()))
          {
              SimpleEvent(g_date->Date() + 1, dk_car_ferti_s2, true);
              break;
          }
          // LKM: Here is a fork leading to two parallel events
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_car_insecticide1, false); // Insecticide thread
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_car_fungicide2, false); // Fungicide thread
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_car_ferti_p2, false);
      break;

  case dk_car_ferti_p2:
      if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_ferti_p2, true);
          break;
      }
      // LKM: Here is a fork leading to two parallel events
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_car_insecticide1, false); // Insecticide thread
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_car_fungicide2, false); // Fungicide thread
      break;
      
  case dk_car_insecticide1:
          // here we check whether we are using ERA pesticide or not
      d1 = g_date->DayInYear(30, 9) - g_date->DayInYear();
      if (!cfg_pest_carrots_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
      {
          flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
      }
      else {
          flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
      }
      if (!flag) {
          SimpleEvent(g_date->Date() + 1, dk_car_insecticide1, true);
          break;
      }
      //LKM: Queue up the next event - insecticide2 (+ 7 days after) done before the 7th of October - if not done, try again +1 day until the 7th of October when we will succeed
      SimpleEvent(g_date->Date() + 7, dk_car_insecticide2, false);
      break;
  case dk_car_insecticide2:
         // here we check whether we are using ERA pesticide or not
      d1 = g_date->DayInYear(8, 10) - g_date->DayInYear();
      if (!cfg_pest_carrots_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
      {
          flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
      }
      else {
          flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
      }
      if (!flag) {
          SimpleEvent(g_date->Date() + 1, dk_car_insecticide2, true);
          break;
      }
      //End of thread
      break;

  case dk_car_fungicide2:
      // Here comes the fungicide thread - done before the 30th of September - if not done, try again +1 day until the 30th of September when we will succeed
      if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_fungicide2, true);
          break;
      }
      // LKM: Queue up the next event - fungicide3 (+ 14 days after) done before the 15th of October - if not done, try again +1 day until the 15th of October when we will succeed
      SimpleEvent(g_date->Date() + 14, dk_car_fungicide3, false);
      break;
  case dk_car_fungicide3:
      if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear()))
      {
          SimpleEvent(g_date->Date() + 1, dk_car_fungicide3, true);
          break;
      }
      //End of thread
      // LKM: Queue up the next event - harvest done before the 30th of November - if not done, try again +1 day until the 30th of November when we will succeed
      SimpleEvent(g_date->Date() + 1, dk_car_harvest, false);
      break;
  case dk_car_harvest:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_car_harvest, true);
              break;
          }
          d1 = g_date->Date();
          if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
              SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_car_wait, false);
              // Because we are ending harvest before 1.7 so we need to wait until the 1.7
              break;
          }
          else {
              done = true; // end of plan
          }
      }
  case dk_car_wait:
      done = true;
      break;
      default:
      g_msg->Warn(WARN_BUG, "DK_Carrots::Do(): ""Unknown event type! ", "");
      exit(1);
  }
  return done;
}


