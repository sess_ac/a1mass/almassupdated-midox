//
// OSeedGrass2.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OSEEDGRASS2_H
#define OSEEDGRASS2_H

#define OSEEDGRASS2_BASE 4000
#define OSG2_WATER_DATE     m_field->m_user[0]

typedef enum {
  osg2_start = 1, // Compulsory, start event must always be 1 (one).
  osg2_ferti_zero = OSEEDGRASS2_BASE,
  osg2_water_zero,
  osg2_water_zero_b,
  osg2_swarth,
  osg2_harvest,
  osg2_strawchopping,
  osg2_burnstrawstubble,
  osg2_compress,
  osg2_stubbleharrow,
  osg2_foobar,
} OSeedGrass2ToDo;



class OSeedGrass2: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OSeedGrass2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(14,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (osg2_foobar - OSEEDGRASS2_BASE);
	  m_base_elements_no = OSEEDGRASS2_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  osg2_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  osg2_ferti_zero = OSEEDGRASS2_BASE,
			fmc_Watering,	//	  osg2_water_zero,
			fmc_Watering,	//	  osg2_water_zero_b,
			fmc_Cutting,	//	  osg2_swarth,
			fmc_Harvest,	//	  osg2_harvest,
			fmc_Others,	//	  osg2_strawchopping,
			fmc_Others,	//	  osg2_burnstrawstubble,
			fmc_Others,	//	  osg2_compress,
			fmc_Cultivation	//	  osg2_stubbleharrow,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }

};

#endif // OSEEDGRASS1_H
