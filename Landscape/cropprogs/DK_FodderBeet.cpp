//
// DK_FodderBeet.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_FodderBeet.h"

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_beet_on;
extern CfgFloat cfg_pest_product_1_amount;


bool DK_FodderBeet::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DKFodderBeets;

	switch (m_ev->m_todo)
	{
	case dk_fb_start:
	{
		DK_FB_FORCESPRING = false;
		m_last_date = g_date->DayInYear(10, 12); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 8); // last possible day of fungicide in this case - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(15, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) // last fungi
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(10, 12); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) // harvest

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(dk_fb_molluscicide1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_ev->m_forcespring) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_fb_molluscicide1, false);
			DK_FB_FORCESPRING = true;
			break;
		}
		else SimpleEvent(d1, dk_fb_autumn_plough, false);
		break;
	}
	break;
	
	// OK, Let's go - LKM: first treatment, autumn plough, do it before the 30 of October - if not done, try again +1 day until the the 30 of October when we succeed - 100% of farmers do this
	case dk_fb_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.98))
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_fb_autumn_plough, true);
			break;
		}
		// LKM: Queue up next event - Molluscicide1 after 1st of March next year
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_fb_molluscicide1, false);
		break;
		// LKM: Do molluscicide1 before 25th of March - if not done, try again +1 day until the the 25th of March when we succeed - done if many slugs/snails (is done before any soil treatments that year) suggests 10% since it is not common
	case dk_fb_molluscicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.02))
		{
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_fb_molluscicide1, true);
				break;

			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 3), dk_fb_harrow, false);
		break;
	case dk_fb_harrow:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(5, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_fb_harrow, true);
				break;
			}
		}
		// LKM: Queue up the next event - spring sow with fertilizer done 5 days after the harrow
		SimpleEvent(g_date->Date() + 5, dk_fb_sow, false);
		break;	

	case dk_fb_sow:
		if (!m_farm->SpringSowWithFerti(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_fb_sow, true);
			break;
		}	

		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date()+1, dk_fb_herbicide1, false);  //herbi + row cult. thread
		SimpleEvent(g_date->Date ()+7, dk_fb_insecticide1, false); // insecti thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_fb_fungicide1, false); // fungi thread - main thread
		break;

	case dk_fb_herbicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_fb_herbicide1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), dk_fb_herbicide2, false);
		break;

	case dk_fb_herbicide2:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_fb_herbicide2, true);
			break;
		}
		SimpleEvent(g_date->Date()+5, dk_fb_herbicide3, false);
		break;
		
	case dk_fb_herbicide3:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_fb_herbicide3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_fb_herbicide4, false);
		break;
		
	case dk_fb_herbicide4:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_fb_herbicide4, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_fb_herbicide5, false);
		break;

	case dk_fb_herbicide5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_fb_herbicide5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+7, dk_fb_row_cultivation, false);
		break;

	case dk_fb_row_cultivation:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 6) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_fb_row_cultivation, true);
				break;
			}
		}
		break; // end of thread

		//LKM: do instecticide1 before 30 of May - if not done, try again +1 day until the the 30 of May when we succeed
	case dk_fb_insecticide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(.80)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(30, 5) - g_date->DayInYear();
			if (!cfg_pest_beet_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_fb_insecticide1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_fb_insecticide2, false);
		break;

	case dk_fb_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(.50)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(31, 7) - g_date->DayInYear();
			if (!cfg_pest_beet_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_fb_insecticide2, true);
				break;
			}
		}
		break; // end of thread
	
	case dk_fb_fungicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_fb_fungicide1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 14, dk_fb_fungicide2, false);
		break;

	case dk_fb_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.35))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_fb_fungicide2, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 9), dk_fb_harvest, false);
		break;
		// LKM: do harvest before 10th of December - if not done, try again + 1 day until the 10th of December when we succeed
	case dk_fb_harvest:
		if (!m_farm->HarvestLong(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_fb_harvest, true);
			break;
		}
		m_field->SetVegPatchy(false);
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "DK_Fodderbeet::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}





	


