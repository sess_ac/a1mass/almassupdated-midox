//
// FI_Potato_South.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FI_Potato_South.h"

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;


bool FI_Potato_South::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	//  int d1;

	bool done = false;

	switch (m_ev->m_todo)
	{
	case fi_ps_start:
	{
		FI_PS_DECIDE_TO_HERB = 1;
		FI_PS_DECIDE_TO_FI = 1;

		a_field->ClearManagementActionSum();

		m_field->SetVegPatchy(true); // Root crop so is open until tall
		// Set up the date management stuff
		// Could save the start day in case it is needed later
		// m_field->m_startday = m_ev->m_startday;
		m_last_date = g_date->DayInYear(1, 10);
		// Start and stop dates for all events after harvest
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(1, 10));
		// 0,0 determined by harvest date - used to see if at all possible
		m_field->SetMDates(1, 0, g_date->DayInYear(1, 10));
		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7))
			{
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "FI_Potato_South::Do(): "
						"Harvest too late for the next crop to start!!!", "");
					exit(1);
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0);
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			// CJT note:
			// Start single block date checking code to be cut-'n-pasted...

			if (!m_ev->m_first_year)
			{
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1)
				{
					// Yes, too early. We assumme this is because the last crop was late
					g_msg->Warn(WARN_BUG, "FI_hPotato_South::Do(): "
						"Crop start attempt between 1st Jan & 1st July", "");
					exit(1);
				}
				else
				{
					d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
					if (g_date->Date() > d1)
					{
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "FI_Potato_South::Do(): "
							"Crop start attempt after last possible start date", "");
						exit(1);
					}
				}
			}
			else
			{
				// If this is the first year of running then it is possible to start
				// on day 0, so need this to tell us what to do:
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), fi_ps_fertilizer, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line
		// comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// This is the first real farm operation
		SimpleEvent(d1, fi_ps_stubble_cultivator, false);
	}
	break;
		// OK, Let's go - LKM: first treatment, stubble cultivator, do it before the 10 of October - if not done, try again +1 day until the the 10 of October when we succeed - 2% of farmers do this
	case fi_ps_stubble_cultivator:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.02))
		{
			if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_stubble_cultivator, true);
				break;
			}
			//Here comes a fork of parallel events:
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, fi_ps_slurry, false); //Slurry thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, fi_ps_n_minerals1, false); //N minerals thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5) + 365, fi_ps_fertilizer, false); //Sowing thread - main thread
			break;
		}
		else if (m_ev->m_lock || m_farm->DoIt_prob(0.24 / 0.98)) { // autumn plough, done after 15th of October - before the 1st of December - 24% do this
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 10), fi_ps_autumn_plough, false);
			break;
		}
		else if (m_ev->m_lock || m_farm->DoIt_prob(0.74 / 0.74)) { 			//Here comes a fork of parallel events:
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, fi_ps_slurry, false); //Slurry thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, fi_ps_n_minerals1, false); //N minerals thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, fi_ps_spring_plough, false); //Spring plough thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5) + 365, fi_ps_fertilizer, false); //Sowing thread - main thread
			break;
		}
		
	case fi_ps_autumn_plough:
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_ps_autumn_plough, true);
			break;
		}
		//Here comes a fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, fi_ps_slurry, false); //Slurry thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, fi_ps_n_minerals1, false); //N minerals thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5) + 365, fi_ps_fertilizer, false); //Sowing thread - main thread
		break;
		// Slurry thread
	case fi_ps_slurry: //rarely applied on conventional farms (mostly organic) 5% do this, done before 25th of May
		if (m_ev->m_lock || m_farm->DoIt_prob(0.05)) {
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_slurry, true);
				break;
			}
		}
		break; //end of Slurry thread
		// N minerals thread
	case fi_ps_n_minerals1: //sometimes applied - 10% do this, done before 10th of May
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
			if (!m_farm->FP_NPKS(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_n_minerals1, true);
				break;
			}
		}
		break; //end of N minerals thread
		// Spring plough thread (the 74% from before)
	case fi_ps_spring_plough: // done before 25th of May
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_spring_plough, true);
				break;
			}
		break; //end of Spring plough thread	
		// Sowing thread - starts with fertilizer, 95% do this, before 30th of May
	case fi_ps_fertilizer:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.95)) {
			if (!m_farm->FP_NPKS(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_fertilizer, true);
				break;
			}
		}
		// LKM: Queue up next event preseeding cultivation right after - done before the 30th of May 
		SimpleEvent(g_date->Date (), fi_ps_preseeding_cultivation, false);
		break;
	case fi_ps_preseeding_cultivation:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.95)) {
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_preseeding_cultivation, true);
				break;
			}
			// LKM: Queue up next event planting right after - done before the 30th of May 
			SimpleEvent(g_date->Date(), fi_ps_plant, false);
			break;
		}
		else if (m_ev->m_lock || m_farm->DoIt_prob(0.05 / 0.05)) { // 5% plant with the preseeding cultivation
			// LKM: Queue up next event planting right after - done before the 30th of May 
			SimpleEvent(g_date->Date(), fi_ps_preseeding_plant, false);
			break;
		}
	case fi_ps_preseeding_plant:
		if (!m_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_ps_preseeding_plant, true);
			break;
		}
		// LKM: Queue up next event harrow, done 7 days after 
		SimpleEvent(g_date->Date() + 7, fi_ps_harrow, false);
		break;
	case fi_ps_plant:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_ps_plant, true);
			break;
		}
		// LKM: Queue up next event harrow, done 7 days after 
		SimpleEvent(g_date->Date() + 7, fi_ps_harrow, false);
		break;
	case fi_ps_harrow: // 98% do this, done before 7th of June
		if (m_ev->m_lock || m_farm->DoIt_prob(0.98)) {
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_harrow, true);
				break;
			}
		}
		// LKM: Here comes a fork with parallel events:
		SimpleEvent(g_date->Date() + 2, fi_ps_herbicide1, false); // herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), fi_ps_fungicide1, false); // Fungicide thread - main thread  - date not stated (suggestion)
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 6), fi_ps_n_minerals2, false); // N minerals2 thread 
		break;
		// herbicide1 thread - done before the 12th of June, 90% do this
	case fi_ps_herbicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(12, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_herbicide1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+10, fi_ps_herbicide2, false); 
		break;
		
	case fi_ps_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(26, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_herbicide2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+10, fi_ps_herbicide3, false);
		break;
	case fi_ps_herbicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_herbicide3, true);
				break;
			}
		}
		break; // End of herbicide thread
		// N minerals2 thread - done before the 15th of July, 20% do this
	case fi_ps_n_minerals2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
			if (!m_farm->FP_NPKS(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_n_minerals2, true);
				break;
			}
		}
		break; // End of N minerals2 thread
		// Fungicide thread - done before the 20th of June 
	case fi_ps_fungicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.98)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_fungicide1, true);
				break;
			}
		}
		// LKM: Queue up next event - fungicide2 done 7-14 days later - 98% do this
		SimpleEvent(g_date->Date() + 7, fi_ps_fungicide2, false);
		break;
	case fi_ps_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.98)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(4, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_fungicide2, true);
				break;
			}
		}
		// LKM: Queue up next event - fungicide3 done 7-14 days later - 98% do this
		SimpleEvent(g_date->Date() + 7, fi_ps_fungicide3, false);
		break;
	case fi_ps_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.98)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(18, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_fungicide3, true);
				break;
			}
		}
		// LKM: Queue up next event - fungicide4 done 7-14 days later - 60% do this
		SimpleEvent(g_date->Date() + 7, fi_ps_fungicide4, false);
		break;
	case fi_ps_fungicide4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_fungicide4, true);
				break;
			}
		}
		// LKM: Queue up next event - fungicide5 done 7 days later - 40% do this
		SimpleEvent(g_date->Date() + 7, fi_ps_fungicide5, false);
		break;
	case fi_ps_fungicide5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(8, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_fungicide5, true);
				break;
			}
		}
		// LKM: Queue up next event - fungicide6 done 7 days later - 10% do this
		SimpleEvent(g_date->Date() + 7, fi_ps_fungicide6, false);
		break;
	case fi_ps_fungicide6:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_fungicide6, true);
				break;
			}
		}
		// LKM: Queue up next event - fungicide7 done 7 days later - 2% do this
		SimpleEvent(g_date->Date() + 7, fi_ps_fungicide7, false);
		break;
	case fi_ps_fungicide7:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.02)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(22, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_fungicide7, true);
				break;
			}
		}
		// LKM: Queue up next event - insecticide (no dates) - 3% do this
		SimpleEvent(g_date->Date() + 1, fi_ps_insecticide, false);
		break;
	case fi_ps_insecticide: // no later than 31st of August (suggestion)
		if (m_ev->m_lock || m_farm->DoIt_prob(0.03)) {
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ps_insecticide, true);
				break;
			}
		}
		// LKM: Queue up the next event - harvest done after the 20th of August
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), fi_ps_harvest, false);
		break;
		// LKM: do harvest before 7th of October 
	case fi_ps_harvest:
		if (!m_farm->HarvestLong(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_ps_harvest, true);
			break;
		}
		m_field->SetVegPatchy(false);
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "FI_Potato_South::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}





	


