/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
/**
\file
\brief
<B>BEMaizeCC.cpp This file contains the headers for the MaizeCC class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of August 2022 \n
 All rights reserved. \n
*/


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/BEMaizeCC.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_M_InsecticideDay;
extern CfgInt   cfg_M_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional maize.
*/
bool BEMaizeCC::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKSpringBarley_CC;
	int l_nextcropstartdate;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case BE_mcc_start:
	{
		// BE_mcc_start just sets up all the starting conditions and reference dates that are needed to start a BE_maize
		BE_MCC_START_FERTI = false;
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 5 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(5, 10)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(10, 10)); // last possible day of straw chopping
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, uBEess it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "BEMaizeCC::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "BEMaizeCC::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "BEMaizeCC::Do(): ", "Crop start attempt after last possible start date");
						int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), BE_mcc_spring_sow, false, a_farm, a_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		if (a_field->GetSoilType() <= 5 || a_field->GetSoilType() == 7) { // on sandy soils (BE ZAND & LOSS)
			SimpleEvent_(d1, BE_mcc_stubble_harrow1, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, BE_mcc_stubble_harrow2, false, a_farm, a_field);
	}
	break;

	// This is the first real farm operation
	case BE_mcc_stubble_harrow1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.70))
		{
			if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_stubble_harrow1, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		SimpleEvent_(d1, BE_mcc_winter_plough1, false, a_farm, a_field);
		break;
	case BE_mcc_winter_plough1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0))
		{
			if (!a_farm->WinterPlough(a_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_winter_plough1, true, a_farm, a_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, BE_mcc_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, BE_mcc_ferti_p1, false, a_farm, a_field);
		break;
	case BE_mcc_stubble_harrow2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.20))
		{
			if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_stubble_harrow2, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 11)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 11);
		}
		SimpleEvent_(d1, BE_mcc_winter_plough2, false, a_farm, a_field);
		break;
	case BE_mcc_winter_plough2:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.00))
		{
			if (!a_farm->WinterPlough(a_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_winter_plough2, true, a_farm, a_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, BE_mcc_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, BE_mcc_ferti_p1, false, a_farm, a_field);
		break;
	case BE_mcc_ferti_p1:
		if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(23, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_mcc_ferti_p1, true, a_farm, a_field);
			break;
		}
		if (a_field->GetSoilType() <= 5 || a_field->GetSoilType() == 7) { // on sandy soils (BE ZAND & LOSS)
			SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_plough1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_plough2, false, a_farm, a_field);
		break;
	case BE_mcc_ferti_s1:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(23, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_mcc_ferti_s1, true, a_farm, a_field);
			break;
		}
		if (a_field->GetSoilType() <= 5 || a_field->GetSoilType() == 7) { // on sandy soils (BE ZAND & LOSS)
			SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_plough1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_plough2, false, a_farm, a_field);
		break;
	case BE_mcc_spring_plough1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80))
		{
			if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(24, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_plough1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, BE_mcc_preseeding_cultivator, false, a_farm, a_field);
		break;
	case BE_mcc_spring_plough2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0))
		{
			if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(24, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_plough2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, BE_mcc_preseeding_cultivator, false, a_farm, a_field);
		break;
	case BE_mcc_preseeding_cultivator:
		if (!a_farm->PreseedingCultivator(a_field, 0.0, g_date->DayInYear(4, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_mcc_preseeding_cultivator, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_sow_with_ferti, false, a_farm, a_field);
		break;
	case BE_mcc_spring_sow_with_ferti:
		// 95% will do sow with firtilizer 
		if (a_ev->m_lock || a_farm->DoIt_prob(0.95))
		{
			if (!a_farm->SpringSowWithFerti(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_sow_with_ferti, true, a_farm, a_field);
				break;
			}
			else
			{
				// 95% of farmers will do this, but the other 5% won't so we need to remember whether we are in one or the other group
				BE_MCC_START_FERTI = true;
				// Here is a fork leading to parallel events
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), BE_mcc_herbicide1, false, a_farm, a_field); // Herbidide thread
				if (a_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), BE_mcc_ferti_s2, false, a_farm, a_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), BE_mcc_ferti_p2, false, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_sow, false, a_farm, a_field);
		break;
	case BE_mcc_spring_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_mcc_spring_sow, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to parallel events
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), BE_mcc_herbicide1, false, a_farm, a_field); // Herbidide thread
		if (a_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), BE_mcc_ferti_s2, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), BE_mcc_ferti_p2, false, a_farm, a_field);
		break;
	case BE_mcc_ferti_p2:
		// Here comes N thread
		if (a_ev->m_lock || a_farm->DoIt_prob(1.00) && (BE_MCC_START_FERTI==0))
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_ferti_p2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), BE_mcc_harvest, false, a_farm, a_field);
		break;
	case BE_mcc_ferti_s2:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.00) && (BE_MCC_START_FERTI == 0))
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_ferti_s2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(30, 9), BE_mcc_harvest, false, a_farm, a_field);
		break;
	case BE_mcc_herbicide1:
		// Here comes the herbicide thread
		if (a_field->GetGreenBiomass() <= 0)
		{
			SimpleEvent_(g_date->Date() + 1, BE_mcc_herbicide1, false, a_farm, a_field);
		}
		else
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
			{
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, BE_mcc_herbicide1, true, a_farm, a_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case BE_mcc_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_mcc_harvest, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_mcc_straw_chopping, false, a_farm, a_field);
		break;
	case BE_mcc_straw_chopping:
		if (a_field->GetMConstants(0) == 0) {
			if (!a_farm->StrawChopping(a_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "BEMaizeCC::Do(): failure in 'StrawChopping' execution", "");
				exit(1);
			}
		}
		else {
			if (!a_farm->StrawChopping(a_field, 0.0, a_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_mcc_straw_chopping, true, a_farm, a_field);
				break;
			}
		}
		// So we are done,but this crop uses a catch crop
		l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
		m_field->BumpRunNum();
		m_field->SetVegPatchy(false); // reverse the patchy before the next crop
		m_farm->AddNewEvent(tov_BECatchPeaCrop, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
		m_field->SetVegType(tov_BECatchPeaCrop, tov_Undefined); //  Two vegetation curves are specified 
		// NB no "done = true" because this crop effectively continues into the catch crop.
		if (m_field->GetUnsprayedMarginPolyRef() != -1)
		{
			LE* um = m_OurLandscape->SupplyLEPointer(m_field->GetUnsprayedMarginPolyRef());
			um->SetVegType(tov_BECatchPeaCrop, tov_Undefined);
		}
		break;

	default:
		g_msg->Warn(WARN_BUG, "BEMaizeCC::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}