//
// DE_OSpringRye.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OSpringRye.h"
#include "math.h"

extern CfgFloat cfg_strigling_prop;

bool DE_OSpringRye::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEOSpringRye; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_osr_start:
	{
		m_field->ClearManagementActionSum();

		DE_OSR_SOW_DATE = 0;
		DE_OSR_DID_STRIGLING_ONE = false;

		m_last_date = g_date->DayInYear(20, 8); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(11, 8); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(20, 8);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping
		flexdates[2][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(20, 8);	 // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - hay bailing

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_osr_spring_harrow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.

		if (m_farm->IsStockFarmer())
		{
			SimpleEvent_(d1, de_osr_fertmanure_stock, false, m_farm, m_field);
		}
		else
		{
			SimpleEvent_(d1, de_osr_spring_plough, false, m_farm, m_field);
		}
		break;
	}
	break;

	case de_osr_fertmanure_stock:
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osr_fertmanure_stock, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_osr_spring_plough, false, m_farm, m_field);
		break;

	case de_osr_spring_plough:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osr_spring_plough, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date(), de_osr_spring_harrow, false, m_farm, m_field);
		break;

	case de_osr_spring_harrow:
		if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osr_spring_harrow, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date(), de_osr_spring_sow, false, m_farm, m_field);
		break;

	case de_osr_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(5, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osr_spring_sow, true, m_farm, m_field);
			break;
		}
		DE_OSR_SOW_DATE = g_date->Date();
		if (m_farm->IsStockFarmer()) {
			d1 = g_date->Date();
			if (d1 < g_date->OldDays() + g_date->DayInYear(5, 4)) {
				d1 = g_date->OldDays() + g_date->DayInYear(5, 4);
			}
		}
			SimpleEvent_(d1, de_osr_spring_roll, false, m_farm, m_field);
		break;

	case de_osr_spring_roll:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->SpringRoll(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osr_spring_roll, true, m_farm, m_field);
				break;
			}
		}
			d1 = g_date->OldDays() + g_date->DayInYear(15, 4);
			if (d1 < DE_OSR_SOW_DATE + 10) {
				d1 = DE_OSR_SOW_DATE + 10;
			}
			SimpleEvent_(d1, de_osr_strigling_one, false, m_farm, m_field);
		break;

	case de_osr_strigling_one:
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osr_strigling_one, true, m_farm, m_field);
			break;
		}
			d1 = g_date->Date() + 10;
			if (d1 < g_date->OldDays() + g_date->DayInYear(25, 4)) {
				d1 = g_date->OldDays() + g_date->DayInYear(25, 4);
			}
			SimpleEvent_(d1, de_osr_strigling_two, false, m_farm, m_field);
		break;

	case de_osr_strigling_two:
		if (m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt(60)))
		{
			if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osr_strigling_two, true, m_farm, m_field);
				break;
			}
		}
			d1 = g_date->Date() + 10;
			if (d1 < g_date->OldDays() + g_date->DayInYear(30, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(30, 5);
			}
			SimpleEvent_(d1, de_osr_strigling_three, false, m_farm, m_field);
		break;

	case de_osr_strigling_three:
		if (m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt(5)))
		{
			if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osr_strigling_three, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 8),
			de_osr_harvest, false, m_farm, m_field);
		break;

	case de_osr_harvest:
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osr_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date(), de_osr_straw_chopping, false, m_farm, m_field);
		break;

	case de_osr_straw_chopping:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osr_straw_chopping, true, m_farm, m_field);
				break;
			}
			done = true;
			break;
		}
		SimpleEvent_(g_date->Date(), de_osr_hay_bailing, false, m_farm, m_field);
		break;

	case de_osr_hay_bailing:
		if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osr_hay_bailing, true, m_farm, m_field);
			break;
		}

		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "DE_OSpringRye::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}

	return done;
}


