/**
\file
\brief
<B>BEGrassGrazed2.h This file contains the headers for the TemporalGrassGrazed2 class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BEGrassGrazed2.h
//


#ifndef BEGRASSGRAZED2_H
#define BEGRASSGRAZED2_H

#define BEGRASSGRAZED2_BASE 25900
/**
\brief A flag used to indicate autumn ploughing status
*/
#define BE_GG2_FERTI_DATE		a_field->m_user[1]
#define BE_GG2_CUT_DATE		a_field->m_user[2]
#define BE_GG2_WATER_DATE		a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing TemporalGrassGrazed2, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_gg2_start = 1, // Compulsory, must always be 1 (one).
	BE_gg2_sleep_all_day = BEGRASSGRAZED2_BASE,
	BE_gg2_spring_sow,
	BE_gg2_starting,
	BE_gg2_ferti_p1,
	BE_gg2_ferti_s1,
	BE_gg2_cut_to_silage1,
	BE_gg2_cut_to_silage2,
	BE_gg2_cut_to_silage3,
	BE_gg2_cut_to_silage4,
	BE_gg2_cut_to_silage5,
	BE_gg2_ferti_p2,
	BE_gg2_ferti_s2,
	BE_gg2_ferti_p3,
	BE_gg2_ferti_s3,
	BE_gg2_ferti_p4,
	BE_gg2_ferti_s4,
	BE_gg2_ferti_p5,
	BE_gg2_ferti_s5,
	BE_gg2_ferti_p6,
	BE_gg2_ferti_s6,
	BE_gg2_ferti_p7,
	BE_gg2_ferti_s7,
	BE_gg2_ferti_p8,
	BE_gg2_ferti_s8, 
	BE_gg2_cattle_out,
	BE_gg2_cattle_is_out,
	BE_gg2_winter_plough_clay,
} BEGrassGrazed2ToDo;


/**
\brief
BEGrassGrazed2 class
\n
*/
/**
See BEGrassGrazed2.h::BEGrassGrazed2ToDo for a complete list of all possible events triggered codes by the TemporalGrassGrazed2 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEGrassGrazed2: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEGrassGrazed2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,3 );
   }
};

#endif // BEGRASSGRAZED2_H

