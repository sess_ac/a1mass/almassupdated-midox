/**
\file
\brief
<B>DE_WinterRye.h This file contains the headers for the DE_WinterRye class</B> \n
*/
/**
\file 
 by Chris J. Topping and Luna Kondrup Marcussen, modified by Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_WinterRye.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_WINTERRYE_H
#define DE_WINTERRYE_H

#define DE_WINTERRYE_BASE 35600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DE_WRY_NOTILL	a_field->m_user[1]
#define DE_WRY_DECIDE_TO_GR	a_field->m_user[2]
/** Below is the list of things that a farmer can do if he is growing DEWinterRye, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_wry_start = 1, // Compulsory, must always be 1 (one).
	de_wry_sleep_all_day = DE_WINTERRYE_BASE,
	de_wry_autumn_plough,
	de_wry_autumn_roll,
	de_wry_autumn_harrow_notill,
	de_wry_autumn_sow,
	de_wry_ferti_s1, // P
	de_wry_ferti_p1, // P
	de_wry_ferti_s2,
	de_wry_ferti_p2,
	de_wry_ferti_s3,
	de_wry_ferti_p3,
	de_wry_herbicide1,
	de_wry_herbicide2,
	de_wry_insecticide1,
	de_wry_insecticide2,
	de_wry_fungicide1,
	de_wry_fungicide2,
	de_wry_growth_regulator1,
	de_wry_growth_regulator2,
	de_wry_harvest,
	de_wry_straw_chopping,
	de_wry_hay_bailing,
	de_wry_foobar, // Obligatory, must be last
} DE_WinterRyeToDo;


/**
\brief
DE_WinterRye class
\n
*/
/**
See DE_WinterRye.h::DE_WinterRyeToDo for a complete list of all possible events triggered codes by the WinterRye management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_WinterRye : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_WinterRye(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date = g_date->DayInYear(1, 10);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_wry_foobar - DE_WINTERRYE_BASE);
		m_base_elements_no = DE_WINTERRYE_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others,	// zero element unused but must be here
			 // ALL THE NECESSARY ENTRIES HERE
			 fmc_Others,		//	  de_wry_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Others,		//	  de_wry_sleep_all_day
			 fmc_Cultivation,	//	de_wry_autumn_plough,
			 fmc_Cultivation,	//	de_wry_autumn_roll,
			 fmc_Cultivation,	//	de_wry_autumn_harrow_notill,
			 fmc_Cultivation,	//	de_wry_autumn_sow,
			 fmc_Fertilizer,		//	de_wry_ferti_s1, // PK
			 fmc_Fertilizer,		//	de_wry_ferti_p1, // PK
			 fmc_Fertilizer,		//	de_wry_ferti_s2,
			 fmc_Fertilizer,		//	de_wry_ferti_p2,
			 fmc_Fertilizer,		//	de_wry_ferti_s3,
			 fmc_Fertilizer,		//	de_wry_ferti_p3,
			 fmc_Herbicide,	//	  de_wry_herbicide1,
			 fmc_Herbicide,	//	  de_wry_herbicide2,
			 fmc_Insecticide,	//	  de_wry_insecticide1,
			 fmc_Insecticide,	//	  de_wry_insecticide2,
			 fmc_Fungicide,	//	  de_wry_fungicide1,
			 fmc_Fungicide,	//	  de_wry_fungicide2,
			 fmc_Others,		//	  de_wry_growth_regulator1,
			 fmc_Others,		//	  de_wry_growth_regulator2,
			 fmc_Harvest,	//	  de_wry_harvest,
			 fmc_Others,		//	  de_wry_straw_chopping,
			 fmc_Others,		//	  de_wry_hay_bailing,
				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};


#endif // DE_WINTERRYE_H

