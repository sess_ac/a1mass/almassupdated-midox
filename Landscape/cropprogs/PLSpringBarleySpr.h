/**
\file
\brief
<B>PLSpringBarleySpr.h This file contains the headers for the SpringBarleySpr class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLSpringBarleySpr.h
//


#ifndef PLSPRINGBARLEYSPR_H
#define PLSPRINGBARLEYSPR_H

#define PLSPRINGBARLEYSPR_BASE 26000
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_SBS_SPRING_FERTI a_field->m_user[6]
#define PL_SBS_DECIDE_TO_GR a_field->m_user[6]

/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_sbs_start = 1, // Compulsory, must always be 1 (one).
	pl_sbs_sleep_all_day = PLSPRINGBARLEYSPR_BASE,
	pl_sbs_spring_plough,
	pl_sbs_spring_harrow,
	pl_sbs_ferti_p1,
	pl_sbs_ferti_s1,
	pl_sbs_heavy_cultivator,
	pl_sbs_preseeding_cultivator,
	pl_sbs_preseeding_cultivator_sow,	// 
	pl_sbs_spring_sow,	
	pl_sbs_herbicide1,
	pl_sbs_fungicide1,
	pl_sbs_fungicide2,
	pl_sbs_fungicide3,
	pl_sbs_insecticide1,
	pl_sbs_insecticide2,
	pl_sbs_ferti_p2,		
	pl_sbs_ferti_s2,	
	pl_sbs_ferti_p3,	// 
	pl_sbs_ferti_s3,
	pl_sbs_ferti_p4,
	pl_sbs_ferti_s4,
	pl_sbs_harvest,
	pl_sbs_straw_chopping,
	pl_sbs_hay_bailing,
	pl_sbs_ferti_p5,
	pl_sbs_ferti_s5,	
	pl_sbs_ferti_p6,	
	pl_sbs_ferti_s6,	// 
	pl_sbs_foobar
} PLSpringBarleySprToDo;


/**
\brief
PLSpringBarleySpr class
\n
*/
/**
See PLSpringBarleySpr.h::PLSpringBarleySprToDo for a complete list of all possible events triggered codes by the spring barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLSpringBarleySpr: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLSpringBarleySpr(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		m_first_date=g_date->DayInYear( 1,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_sbs_foobar - PLSPRINGBARLEYSPR_BASE);
	   m_base_elements_no = PLSPRINGBARLEYSPR_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_sbs_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_sbs_sleep_all_day = PLSPRINGBARLEYSPR_BASE,
			fmc_Cultivation,//pl_sbs_spring_plough,
			fmc_Cultivation,//pl_sbs_spring_harrow,
			fmc_Fertilizer,//pl_sbs_ferti_p1,
			fmc_Fertilizer,//pl_sbs_ferti_s1,
			fmc_Cultivation,//pl_sbs_heavy_cultivator,
			fmc_Cultivation,//pl_sbs_preseeding_cultivator,
			fmc_Cultivation,//pl_sbs_preseeding_cultivator_sow,	// 
			fmc_Others,//pl_sbs_spring_sow,
			fmc_Herbicide,//pl_sbs_herbicide1,
			fmc_Fungicide,//pl_sbs_fungicide1,
			fmc_Fungicide,//pl_sbs_fungicide2,
			fmc_Fungicide,//pl_sbs_fungicide3,
			fmc_Insecticide,//pl_sbs_insecticide1,
			fmc_Insecticide,//pl_sbs_insecticide2,
			fmc_Fertilizer,//pl_sbs_ferti_p2,
			fmc_Fertilizer,//pl_sbs_ferti_s2,
			fmc_Fertilizer,//pl_sbs_ferti_p3,	// 
			fmc_Fertilizer,//pl_sbs_ferti_s3,
			fmc_Fertilizer,//pl_sbs_ferti_p4,
			fmc_Fertilizer,//pl_sbs_ferti_s4,
			fmc_Harvest,//pl_sbs_harvest,
			fmc_Others,//pl_sbs_straw_chopping,
			fmc_Others,//pl_sbs_hay_bailing,
			fmc_Fertilizer,//pl_sbs_ferti_p5,
			fmc_Fertilizer,//pl_sbs_ferti_s5,
			fmc_Fertilizer,//pl_sbs_ferti_p6,
			fmc_Fertilizer//pl_sbs_ferti_s6,	// 
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLSPRINGBARLEYSPR_H

