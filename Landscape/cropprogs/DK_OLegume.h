//
// OLegume.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved. 

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_OLegume_H
#define DK_OLegume_H
/** \brief A flag used to indicate do catch crop
*/
#define DK_OL_DO_CATCHCROP	a_field->m_user[2]
#define DK_OL_BASE 60100

typedef enum {
  dk_ol_start = 1, // Compulsory, start event must always be 1 (one).
  dk_ol_harvest = DK_OL_BASE,
  dk_ol_spring_harrow1,
  dk_ol_spring_plough,
  dk_ol_ks_ferti,
  dk_ol_spring_harrow2,
  dk_ol_spring_row_sow,
  dk_ol_strigling,
  dk_ol_rowcultivation,
  dk_ol_water,
  dk_ol_autumn_harrow,
  dk_ol_autumn_plough,
  dk_ol_foobar,
} DK_OLegumesToDo;



class DK_OLegume: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OLegume(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_ol_foobar - DK_OL_BASE);
	  m_base_elements_no = DK_OL_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_ol_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_ol_harvest = DK_OL_BASE,
			fmc_Cultivation,	//	  dk_ol_spring_harrow1,
			fmc_Cultivation,	//	  dk_ol_spring_plough,
			fmc_Fertilizer,	//	  dk_ol_ks_ferti,
			fmc_Cultivation,	//	  dk_ol_spring_harrow2,
			fmc_Others,	//	  dk_ol_spring_row_sow,
			fmc_Cultivation,	//	  dk_ol_strigling,
			fmc_Cultivation,	//	  dk_ol_rowcultivation,
			fmc_Watering,	//	  dk_ol_water,
			fmc_Cultivation,	//	  dk_ol_autumn_harrow,
			fmc_Cultivation	//	  dk_ol_autumn_plough,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OLegume_H
