/**
\file
\brief
<B>IRWinterBarley.h This file contains the headers for the WinterBarley class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2022 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// IRWinterBarley.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef IRWINTERWHEAT_H
#define IRWINTERWHEAT_H

#define IR_WW_BASE 90600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define IR_WW_MT        a_field->m_user[1]
#define IR_WW_CP        a_field->m_user[2]


/** Below is the list of things that a farmer can do if he is growing the crop, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	ir_ww_start = 1, // Compulsory, must always be 1 (one).
	ir_ww_sleep_all_day = IR_WW_BASE,
	ir_ww_ferti_s1,
	ir_ww_ferti_p1,
	ir_ww_stubble_harrow_mt,
	ir_ww_stubble_harrow_cp,
	ir_ww_autumn_plough, 
	ir_ww_autumn_sow,
	ir_ww_autumn_harrow,
	ir_ww_stubble_cultivator,
	ir_ww_herbicide,
	ir_ww_fungicide1,
	ir_ww_fungicide2,
	ir_ww_fungicide3,
	ir_ww_stubble_cultivator_mt,
	ir_ww_cultivation_sow_mt,
	ir_ww_ferti_s2,
	ir_ww_ferti_p2,
	ir_ww_autumn_roll,
	ir_ww_ferti_s3,
	ir_ww_ferti_p3,
	ir_ww_ferti_s4,
	ir_ww_ferti_p4,
	ir_ww_ferti_s5,
	ir_ww_ferti_p5,
	ir_ww_molluscicide,
	ir_ww_insecticide1,
	ir_ww_insecticide2,
	ir_ww_harvest,
	ir_ww_hay_bailing,
	ir_ww_foobar,
} IRWinterWheatToDo;


/**
\brief
IRWinterWheat class
\n
*/
/**
See IRWinterWheat.h::IRWinterWheatToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class IRWinterWheat : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	IRWinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(31, 8);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (ir_ww_foobar - IR_WW_BASE);
		m_base_elements_no = IR_WW_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others,	// zero element unused but must be here	
			 fmc_Others,	//	  ir_ww_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Others,	//	  ir_ww_sleep_all_day = IR_WB_BASE,
			 fmc_Fertilizer, //	ir_ww_ferti_s1
			 fmc_Fertilizer, //	ir_ww_ferti_p1
			 fmc_Cultivation,	// ir_ww_stubble_harrow_mt,	
			 fmc_Cultivation,	//	ir_ww_stubble_harrow_cp,
			 fmc_Cultivation, //ir_ww_autumn_plough,
			 fmc_Others, //ir_wo_autumn_sow,
		     fmc_Cultivation, //ir_ww_autumn_harrow,
			 fmc_Cultivation, //ir_ww_stubble_cultivator,
			 fmc_Herbicide, //ir_ww_herbicide,
			 fmc_Fungicide, //ir_ww_fungicide1,
			 fmc_Fungicide, //ir_ww_fungicide2,
			 fmc_Fungicide, //ir_ww_fungicide3,
			 fmc_Cultivation, //ir_ww_stubble_cultivator_mt,
			 fmc_Cultivation, //ir_ww_cultivation_sow_mt,
			 fmc_Fertilizer, //ir_ww_ferti_s2,
			 fmc_Fertilizer, //ir_ww_ferti_p2,
			 fmc_Others, //ir_ww_spring_roll,
			 fmc_Fertilizer, //ir_ww_ferti_s3,
			 fmc_Fertilizer, //ir_ww_ferti_p3,
			 fmc_Fertilizer, // ir_ww_ferti_s4,
			 fmc_Fertilizer, //ir_ww_ferti_p4,
			 fmc_Fertilizer, // ir_ww_ferti_s5,
			 fmc_Fertilizer, //ir_ww_ferti_p5,
			 fmc_Insecticide, //ir_ww_molluscicide,
			 fmc_Insecticide, //ir_ww_insecticide1,
			 fmc_Insecticide, //ir_ww_insecticide2,
			 fmc_Harvest,	//	  ir_ww_harvest,
			 fmc_Others,	//	  ir_ww_hay_bailing,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // IRWINTERWHEAT_H