//
// FodderGrass.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FodderGrassH
#define FodderGrassH

#define FG_CUT_DATE          m_field->m_user[0]
#define FG_WATER_DATE        m_field->m_user[1]
#define FG_FERTI_DATE        m_field->m_user[2]
#define FG_FORCE_SECOND_CUT  m_field->m_user[3]
#define FG_HALT_SECOND_WATER m_field->m_user[4]
#define FG_BASE 1900

typedef enum {
  fg_start = 1, // Compulsory, start event must always be 1 (one).
  fg_ferti_zero=FG_BASE,
  fg_ferti_one,
  fg_ferti_two,
  fg_ferti_three,
  fg_ferti_four,
  fg_cut_to_silage,
  fg_cut_to_silage2,
  fg_cut_to_silage3,
  fg_water_zero,
  fg_water_one,
  fg_productapplic_one,
  fg_foobar,
} FodderGrassToDo;


class FodderGrass: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FodderGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(30,4);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (fg_foobar - FG_BASE);
	  m_base_elements_no = FG_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fg_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  fg_ferti_zero=FG_BASE,
			fmc_Fertilizer,	//	  fg_ferti_one,
			fmc_Fertilizer,	//	  fg_ferti_two,
			fmc_Fertilizer,	//	  fg_ferti_three,
			fmc_Fertilizer,	//	  fg_ferti_four,
			fmc_Cutting,	//	  fg_cut_to_silage,
			fmc_Cutting,	//	  fg_cut_to_silage2,
			fmc_Cutting,	//	  fg_cut_to_silage3,
			fmc_Watering,	//	  fg_water_zero,
			fmc_Watering,	//	  fg_water_one,
			fmc_Herbicide	//	  fg_productapplic_one,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif
// FodderGrass.h

