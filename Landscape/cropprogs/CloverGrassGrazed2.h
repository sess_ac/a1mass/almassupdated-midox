//
// CloverGrassGrazed2.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef CloverGrassGrazed2H
#define CloverGrassGrazed2H

#define CGG2_CUT_DATE     m_field->m_user[0]
#define CGG2_WATER_DATE   m_field->m_user[1]
#define CGG2_DO_STUBBLE   m_field->m_user[2]
#define CGG2_STOP_CATTLE  m_field->m_user[3]
#define CGG2_BASE 1500

typedef enum {
  cgg2_start = 1, // Compulsory, start event must always be 1 (one).
  cgg2_ferti_zero=CGG2_BASE,
  cgg2_ferti_one,
  cgg2_ferti_two,
  cgg2_cut_to_silage,
  cgg2_water_zero,
  cgg2_water_one,
  cgg2_cattle_out,
  cgg2_cattle_is_out,
  cgg2_stubble_harrow,
  cgg2_productapplic_one,
  cgg2_foobar,
} CloverGrassGrazed2ToDo;



class CloverGrassGrazed2: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  CloverGrassGrazed2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (cgg2_foobar - CGG2_BASE);
	  m_base_elements_no = CGG2_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  cgg2_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  cgg2_ferti_zero=CGG2_BASE,
			fmc_Fertilizer,	//	  cgg2_ferti_one,
			fmc_Fertilizer,	//	  cgg2_ferti_two,
			fmc_Cutting,	//	  cgg2_cut_to_silage,
			fmc_Watering,	//	  cgg2_water_zero,
			fmc_Watering,	//	  cgg2_water_one,
			fmc_Grazing,	//	  cgg2_cattle_out,
			fmc_Grazing,	//	  cgg2_cattle_is_out,
			fmc_Cultivation,	//	  cgg2_stubble_harrow,
			fmc_Herbicide	//	  cgg2_productapplic_one,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif
// CloverGrassGrazed2.H













