//
// DK_VegSeeds.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_VegSeeds_H
#define DK_VegSeeds_H

#define DK_VS_FORCESPRING a_field->m_user[1]
#define DK_VS_AUTUMNPLOUGH a_field->m_user[2]

#define DK_VS_BASE 160500


typedef enum {
  dk_vs_start = 1, // Compulsory, start event must always be 1 (one).
  dk_vs_autumn_plough = DK_VS_BASE,
  dk_vs_molluscicide,
  dk_vs_spring_plough, 
  dk_vs_ferti_s,
  dk_vs_ferti_p,
  dk_vs_sow_cultivation,
  dk_vs_herbicide1,
  dk_vs_herbicide2,
  dk_vs_herbicide3,
  dk_vs_herbicide4,
  dk_vs_herbicide5,
  dk_vs_manganese,
  dk_vs_row_cultivation1,
  dk_vs_row_cultivation2,
  dk_vs_row_cultivation3,
  dk_vs_fungicide1,
  dk_vs_fungicide2,
  dk_vs_insecticide,
  dk_vs_water,
  dk_vs_swathing,
  dk_vs_herbicide6,
  dk_vs_harvest,
  dk_vs_foobar
} DK_VegSeedsToDo;



class DK_VegSeeds: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_VegSeeds(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,12);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_vs_foobar - DK_VS_BASE);
	  m_base_elements_no = DK_VS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others, // zero element unused but must be here
		  fmc_Others, // dk_vs_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation, // dk_vs_autumn_plough = DK_VS_BASE,
		  fmc_Others, // dk_vs_molluscicide,
		  fmc_Cultivation, //  dk_vs_spring_plough,
		  fmc_Fertilizer, //  dk_vs_ferti_s,
		  fmc_Fertilizer, //  dk_vs_ferti_p,
		  fmc_Cultivation, //  dk_vs_sow_cultivation,
		  fmc_Herbicide, //  dk_vs_herbicide1,
		  fmc_Herbicide, //  dk_vs_herbicide2,
		  fmc_Herbicide, //  dk_vs_herbicide3,
		  fmc_Herbicide, //  dk_vs_herbicide4,
		  fmc_Herbicide, //  dk_vs_herbicide5,
		  fmc_Fertilizer, //  dk_vs_manganese,
		  fmc_Cultivation, // dk_vs_row_cultivation1,
		  fmc_Cultivation, //  dk_vs_row_cultivation2,
		  fmc_Cultivation, //  dk_vs_row_cultivation3,
		  fmc_Fungicide, // dk_vs_fungicide1,
		  fmc_Fungicide, //   dk_vs_fungicide2,
		  fmc_Insecticide, //   dk_vs_insecticide,
		  fmc_Watering, // dk_vs_water,
		  fmc_Cutting, //  dk_vs_swathing,
		  fmc_Herbicide, // dk_vs_herbicide6,
		  fmc_Harvest, //   dk_vs_harvest,
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};

#endif // DK_VegSeeds_H
