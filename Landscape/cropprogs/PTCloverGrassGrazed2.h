/**
\file
\brief
<B>PTCloverGrassGrazed2.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTCloverGrassGrazed2.h
//


#ifndef PTCLOVERGRASSGRAZED2_H
#define PTCLOVERGRASSGRAZED2_H

#define PTCLOVERGRASSGRAZED2_BASE 30700
/**
\brief A flag used to indicate cattle out dates
*/
#define PT_FL1_CATTLEOUT_DATE a_field->m_user[1]
#define PT_FL2_CATTLEOUT_DATE a_field->m_user[2]
#define PT_FL3_CATTLEOUT_DATE a_field->m_user[3]

/** Below is the list of things that a farmer can do if he is growing clover grass grazed in the second year, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_cgg2_start = 1, // Compulsory, must always be 1 (one).
	pt_cgg2_sleep_all_day = PTCLOVERGRASSGRAZED2_BASE,
	pt_cgg2_ferti_p1, // NPK
	pt_cgg2_ferti_s1,
	pt_cgg2_cattle_out1,
	pt_cgg2_cattle_is_out1,
	pt_cgg2_harvest1,
	pt_cgg2_harvest2,
	pt_cgg2_hay_bailing1,
	pt_cgg2_hay_bailing2,
	pt_cgg2_cattle_out2,
	pt_cgg2_cattle_is_out2,
	pt_cgg2_cattle_out3,
	pt_cgg2_cattle_is_out3,
	pt_cgg2_wait,

} PTCloverGrassGrazed2ToDo;


/**
\brief
PTCloverGrassGrazed2 class
\n
*/
/**
See PTCloverGrassGrazed2.h::PTCloverGrassGrazed2ToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTCloverGrassGrazed2: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTCloverGrassGrazed2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,10 );// AAS:Last possible date to do the first operation, i.e. fertilizer npk
   }
};

#endif // PTCLOVERGRASSGRAZED2_H

