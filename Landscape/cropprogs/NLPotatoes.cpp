/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLPotatoes.cpp This file contains the source for the NLPotatoes class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// NLPotatoes.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLPotatoes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_potatoes_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_POT_InsecticideDay;
extern CfgInt   cfg_POT_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional potatoes.
*/
bool NLPotatoes::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLPotatoes;
	int l_nextcropstartdate;
	/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case nl_pot_start:
	{
		NL_POT_HERBI = false;
		NL_POT_FUNGI1 = false;
		NL_POT_FUNGI2 = false;
		NL_POT_FUNGI3 = false;
		NL_POT_FUNGI4 = false;
		NL_POT_FUNGI5 = false;
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(30, 110); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(30, 10); // first possible day of finishing harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(30, 10);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(nl_pot_spring_planting))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9) + isSpring;
		// OK, let's go.
		if (m_ev->m_forcespring) {
			if (m_field->GetSoilType() == 2 || m_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, nl_pot_spring_plough_sandy, false, m_farm, m_field);
			}
			else {
				if (m_farm->IsStockFarmer()) //Stock Farmer
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, nl_pot_ferti_s2_clay, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, nl_pot_ferti_p2_clay, false, m_farm, m_field);
			}
			break;
		}
		else {
			// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
			SimpleEvent_(d1, nl_pot_stubble_harrow, false, m_farm, m_field);
		}
		break;
	}
	break;

	// This is the first real farm operation
	case nl_pot_stubble_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_stubble_harrow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		if (m_field->GetSoilType() == 2 || m_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, nl_pot_ferti_s1_sandy, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, nl_pot_winter_plough_clay, false, m_farm, m_field);
		break;

	case nl_pot_ferti_p1_sandy:
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p1_sandy, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_spring_plough_sandy, false, m_farm, m_field);
		break;
	case nl_pot_ferti_s1_sandy:
		if (m_farm->IsStockFarmer())
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s1_sandy, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, nl_pot_spring_plough_sandy, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p1_sandy, false, m_farm, m_field);
		break;
	case nl_pot_spring_plough_sandy:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_spring_plough_sandy, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 4);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_pot_ferti_s2_sandy, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, nl_pot_ferti_p2_sandy, false, m_farm, m_field);
		break;
	case nl_pot_ferti_p2_sandy:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p2_sandy, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, false, m_farm, m_field);
		break;
	case nl_pot_ferti_s2_sandy:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s2_sandy, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, false, m_farm, m_field);
		break;
	case nl_pot_winter_plough_clay:
		if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_winter_plough_clay, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, nl_pot_ferti_s2_clay, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, nl_pot_ferti_p2_clay, false, m_farm, m_field);
		break;
	case nl_pot_ferti_p2_clay:
		if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p2_clay, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, false, m_farm, m_field);
		break;
	case nl_pot_ferti_s2_clay:
		if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s2_clay, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, false, m_farm, m_field);
		break;
	case nl_pot_bed_forming:
		if (!m_farm->BedForming(m_field, 0.0, g_date->DayInYear(9, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_bed_forming, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_pot_spring_planting, false, m_farm, m_field);
		break;
	case nl_pot_spring_planting:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_spring_planting, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 14, nl_pot_hilling1, false, m_farm, m_field); // Hilling + herbicides = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), nl_pot_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), nl_pot_insecticide, false, m_farm, m_field);	// Insecticide thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_s3_clay, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_p3_clay, false, m_farm, m_field);
		if (m_farm->IsStockFarmer()) //Stock Farmer					// microelements thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_s4, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_p4, false, m_farm, m_field);
		break;
	case nl_pot_ferti_p3_clay:
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6)// on clay soils (NL KLEI & VEEN)
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.25))
			{
				if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p3_clay, true, m_farm, m_field);
					break;
				}
			}
			// End of thread
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_s3_sandy, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_p3_sandy, false, m_farm, m_field);
		break;
	case nl_pot_ferti_s3_clay:
		if (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6)// on clay soils (NL KLEI & VEEN)
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.25))
			{
				if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s3_clay, true, m_farm, m_field);
					break;
				}
			}
			// End of thread
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_s3_sandy, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), nl_pot_ferti_p3_sandy, false, m_farm, m_field);
		break;
	case nl_pot_ferti_p3_sandy:
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p3_sandy, true, m_farm, m_field);
			break;
		}
		// End of thread
		break;
	
	case nl_pot_ferti_s3_sandy:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s3_sandy, true, m_farm, m_field);
			break;
		}
		// End of thread
		break;

	case nl_pot_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_pot_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25))
		{ 
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_pot_hilling1:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_hilling1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 3, nl_pot_herbicide1, false, m_farm, m_field);
		break;
	case nl_pot_herbicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_herbicide1, true, m_farm, m_field);
				break;
			}
			NL_POT_HERBI = true;
		}
		SimpleEvent_(g_date->Date() + 14, nl_pot_herbicide2, false, m_farm, m_field);
		break;
	case nl_pot_herbicide2:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.625) && NL_POT_HERBI == 1)) // 50% of all farmers
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 9), nl_pot_dessication1, false, m_farm, m_field);
		break;
	case nl_pot_dessication1:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_dessication1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 5, nl_pot_dessication2, false, m_farm, m_field);
		break;

	case nl_pot_dessication2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_dessication2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, nl_pot_harvest, false, m_farm, m_field);
		break;
	case nl_pot_fungicide1:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide2, false, m_farm, m_field);
		break;
	case nl_pot_fungicide2:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide2, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide3, false, m_farm, m_field);
		break;
	case nl_pot_fungicide3:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide4, false, m_farm, m_field);
		break;
	case nl_pot_fungicide4:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide4, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide5, false, m_farm, m_field);
		break;
	case nl_pot_fungicide5:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide6, false, m_farm, m_field);
		break;
	case nl_pot_fungicide6:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide6, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide7, false, m_farm, m_field);
		break;
	case nl_pot_fungicide7:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide7, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide8, false, m_farm, m_field);
		break;
	case nl_pot_fungicide8:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide8, true, m_farm, m_field);
				break;
			}
			NL_POT_FUNGI1 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide9, false, m_farm, m_field);
		break;
	case nl_pot_fungicide9:
		if (m_ev->m_lock || (m_farm->DoIt_prob(1.00) && NL_POT_FUNGI1 == 1))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide9, true, m_farm, m_field);
				break;
			}
			NL_POT_FUNGI2 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide10, false, m_farm, m_field);
		break;
	case nl_pot_fungicide10:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.875) && NL_POT_FUNGI2 == 1)) // 70% of all farmers
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide10, true, m_farm, m_field);
				break;
			}
			NL_POT_FUNGI3 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide11, false, m_farm, m_field);
		break;
	case nl_pot_fungicide11:
		if (m_ev->m_lock || (m_farm->DoIt_prob(1.00) && NL_POT_FUNGI3 == 1))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide11, true, m_farm, m_field);
				break;
			}
			NL_POT_FUNGI4 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide12, false, m_farm, m_field);
		break;
	case nl_pot_fungicide12:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.857) && NL_POT_FUNGI4 == 1)) // 60% of all farmers
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide12, true, m_farm, m_field);
				break;
			}
			NL_POT_FUNGI5 = true;
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide13, false, m_farm, m_field);
		break;
	case nl_pot_fungicide13:
		if (m_ev->m_lock || (m_farm->DoIt_prob(1.00) && NL_POT_FUNGI5 == 1))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide13, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide14, false, m_farm, m_field);
		break;
	case nl_pot_fungicide14:
		if (m_ev->m_lock || (m_farm->DoIt_prob(1.00) && NL_POT_FUNGI5 == 1))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide14, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, nl_pot_fungicide15, false, m_farm, m_field);
		break;
	case nl_pot_fungicide15:
		if (m_ev->m_lock || (m_farm->DoIt_prob(1.00) && NL_POT_FUNGI5 == 1))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_pot_fungicide15, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_pot_insecticide:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_potatoes_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_pot_insecticide, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
	case nl_pot_harvest:
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_pot_harvest, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLPotatoes::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}