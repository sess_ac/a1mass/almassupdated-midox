//
// PermanentSetAside.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef PERMANENTSETASIDE_H
#define PERMANENTSETASIDE_H

#define PERMANENTSETASIDE_BASE 600

typedef enum {
  psa_start = 1, // Compulsory, start event must always be 1 (one).
  psa_do_nothing_start = PERMANENTSETASIDE_BASE,
  psa_do_nothing_stop,
  psa_cut_to_hay,
  psa_foobar
} PermanentSetAsideToDo;



class PermanentSetAside: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  PermanentSetAside(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,7);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (psa_foobar - PERMANENTSETASIDE_BASE);
	  m_base_elements_no = PERMANENTSETASIDE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others, // zero element unused but must be here
		  fmc_Others, //psa_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Others, //psa_do_nothing_start = PERMANENTSETASIDE_BASE,
		  fmc_Others, //psa_do_nothing_stop,
		  fmc_Cutting //psa_cut_to_hay,
		  // No foobar entry
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};

#endif // PERMANENTSETASIDE_H
