/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_Beet.cpp This file contains the source for the DE_Beet class</B> \n
*/
/**
\file
 by Chris J. Topping and Elzbieta Ziolkowska and modified by Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_Beet.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_SugarBeet.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_beet_on;
extern CfgFloat cfg_pest_product_1_amount;
extern Landscape* g_landscape_p;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional sugar beet.
*/
bool DE_SugarBeet::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool flag = false;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DESugarBeet; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_be_start:
	{
		DE_BE_FERTI_P1 = false;
		DE_BE_FERTI_S1 = false;
		DE_BE_WINTER_PLOUGH = false;
		DE_BE_SPRING_FERTI = 0;
		DE_BE_HERBI_DATE = 0;
		DE_BE_HERBI1 = false;
		DE_BE_HERBI2 = false;

		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(31, 11); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 11); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(31, 11); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_be_spring_sow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9) + isSpring;
		// OK, let's go.
		if (m_ev->m_forcespring) {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_be_spring_harrow, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(d1, de_be_autumn_harrow1, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case de_be_autumn_harrow1:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_be_autumn_harrow1, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 9);
		}
		SimpleEvent_(d1, de_be_autumn_harrow2, false, m_farm, m_field);
		break;
	case de_be_autumn_harrow2:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_autumn_harrow2, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, de_be_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, de_be_ferti_p1, false, m_farm, m_field);
		break;

	case de_be_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_p1, true, m_farm, m_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remember who already did it
				DE_BE_FERTI_S1 = true;
				d1 = g_date->Date() + 1;
				if (d1 < g_date->OldDays() + g_date->DayInYear(1, 10)) {
					d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
				}
				SimpleEvent_(d1, de_be_winter_plough, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_be_ferti_p2, false, m_farm, m_field);
		break;
	case de_be_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_s1, true, m_farm, m_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remember who already did it
				DE_BE_FERTI_S1 = true;
				d1 = g_date->Date() + 1;
				if (d1 < g_date->OldDays() + g_date->DayInYear(1, 10)) {
					d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
				}
				SimpleEvent_(d1, de_be_winter_plough, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_be_ferti_s2, false, m_farm, m_field);
		break;
	case de_be_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt(50) && (DE_BE_FERTI_S1 == 0))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
		}
		SimpleEvent_(d1, de_be_winter_plough, false, m_farm, m_field);
		break;
	case de_be_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt(50) && (DE_BE_FERTI_S1 == 0))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
		}
		SimpleEvent_(d1, de_be_winter_plough, false, m_farm, m_field);
		break;
	case de_be_winter_plough:
		// 20% will do winter plough, but 80% will get away with non-inversion cultivation
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_winter_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				DE_BE_WINTER_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_be_spring_harrow, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_be_winter_stubble_cultivator_heavy, false, m_farm, m_field);
		break;
	case de_be_winter_stubble_cultivator_heavy:
		// 80% will get away with non-inversion cultivation
		if (m_ev->m_lock || m_farm->DoIt(60)) // 60% of remaining 80% of farmers who didn't do winter plough
		{
			if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_winter_stubble_cultivator_heavy, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, de_be_spring_harrow, false, m_farm, m_field);
		break;

	case de_be_spring_harrow:
		if ((m_ev->m_lock) || m_farm->DoIt(50))
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_spring_harrow, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_be_ferti_s4, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_be_ferti_p4, false, m_farm, m_field);
		break;
	case de_be_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt(98))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_p4, true, m_farm, m_field);
				break;
			}
			DE_BE_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, de_be_ferti_p5, false, m_farm, m_field);
		break;
	case de_be_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt(98))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_s4, true, m_farm, m_field);
				break;
			}
			DE_BE_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, de_be_ferti_s5, false, m_farm, m_field);
		break;
	case de_be_ferti_p5:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_p5, true, m_farm, m_field);
				break;
			}
			DE_BE_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, de_be_heavy_cultivator, false, m_farm, m_field);
		break;
	case de_be_ferti_s5:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_s5, true, m_farm, m_field);
				break;
			}
			DE_BE_SPRING_FERTI = 1;
		}
		SimpleEvent_(g_date->Date() + 1, de_be_heavy_cultivator, false, m_farm, m_field);
		break;
	case de_be_heavy_cultivator:
		if (DE_BE_WINTER_PLOUGH == 0)
		{
			if (!m_farm->HeavyCultivatorAggregate(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_heavy_cultivator, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_be_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_be_preseeding_cultivator:
		// 90% will do preseeding cultivation, the rest will do it together with sow
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(19, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_preseeding_cultivator, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, de_be_spring_sow, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_be_preseeding_cultivator_sow, false, m_farm, m_field);
		break;
	case de_be_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_be_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, de_be_harrow_before_emergence, false, m_farm, m_field); // Harrowing before emergence, followed by thinning after emergence
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_be_watering1, false, m_farm, m_field);	// Watering thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), de_be_herbicide1, false, m_farm, m_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_be_fungicide, false, m_farm, m_field);	// Fungicide thread = MAIN THREAD
		SimpleEvent_(g_date->Date() + 14 + m_date_modifier, de_be_insecticide, false, m_farm, m_field);	// Insecticide thread
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), de_be_ferti_s6, false, m_farm, m_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), de_be_ferti_p6, false, m_farm, m_field);
		break;
	case de_be_preseeding_cultivator_sow:
		// 10% will do preseeding cultivation with sow
		if (!m_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_be_preseeding_cultivator_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, de_be_harrow_before_emergence, false, m_farm, m_field); // Harrowing before emergence, followed by thinning after emergence
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_be_watering1, false, m_farm, m_field);	// Watering thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), de_be_herbicide1, false, m_farm, m_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_be_fungicide, false, m_farm, m_field);	// Fungicide thread = MAIN THREAD
		SimpleEvent_(g_date->Date() + 14 + m_date_modifier, de_be_insecticide, false, m_farm, m_field);	// Insecticide thread
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), de_be_ferti_s6, false, m_farm, m_field); // Fertilizers thread; EZ: changed from 1.05 to match de_be_spring_sow
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), de_be_ferti_p6, false, m_farm, m_field);
		break;
	case de_be_harrow_before_emergence:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(5))
			{
				if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_be_harrow_before_emergence, true, m_farm, m_field);
					break;
				}
				SimpleEvent_(g_date->Date() + 5, de_be_thinning, false, m_farm, m_field);
				break;
			}
		}
		break;
	case de_be_thinning:
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, de_be_thinning, true, m_farm, m_field);
		}
		else
		{
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_thinning, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_be_watering1:
		if (m_ev->m_lock || m_farm->DoIt(3))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_be_watering1, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_be_watering1, true, m_farm, m_field);
					break;
				}
				d1 = g_date->Date() + 7;
				if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
					d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
				}
				SimpleEvent_(d1, de_be_watering2, false, m_farm, m_field);
				break;
			}
		}
		break;

	case de_be_watering2:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_be_watering2, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		}
		SimpleEvent_(d1, de_be_watering3, false, m_farm, m_field);
		break;
	case de_be_watering3:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_be_watering3, true, m_farm, m_field);
			break;
		}
		// End of thread
		break;
	case de_be_ferti_p6:
		// Here comes fertilizers thread
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, de_be_ferti_p6, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_p6, true, m_farm, m_field);
				break;
			}
			d1 = g_date->Date() + 1;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 5);
			}
			SimpleEvent_(d1, de_be_ferti_p7, false, m_farm, m_field);
			break;
		}
		break;
	case de_be_ferti_s6:
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, de_be_ferti_s6, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_s6, true, m_farm, m_field);
				break;
			}
			d1 = g_date->Date() + 1;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 5);
			}
			SimpleEvent_(d1, de_be_ferti_s7, false, m_farm, m_field);
			break;
		}
		break;
	case de_be_ferti_p7:
		if (m_ev->m_lock || m_farm->DoIt(10))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_p7, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_be_ferti_s7:
		if (m_ev->m_lock || m_farm->DoIt(10))
		{
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_s7, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_be_herbicide1: // The first of the pesticide managements.
		// Here comes the herbicide thread
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_be_herbicide1, true, m_farm, m_field);
			break;
		}
		DE_BE_HERBI1 = true;
		DE_BE_HERBI_DATE = g_date->Date();
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), de_be_herbicide2, false, m_farm, m_field);
		break;
	case de_be_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt(93))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_herbicide2, true, m_farm, m_field);
				break;
			}
			DE_BE_HERBI_DATE = g_date->Date();
			DE_BE_HERBI2 = true;
		}
		SimpleEvent_(g_date->Date() + 10, de_be_herbicide3, false, m_farm, m_field);
		break;
	case de_be_herbicide3:
		if (m_ev->m_lock || (m_farm->DoIt(81) && DE_BE_HERBI2)) // which gives 75% of all farmers (and 81% of those who did not apply herbicides before beet emergence)
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_herbicide3, true, m_farm, m_field);
				break;
			}
			DE_BE_HERBI_DATE = g_date->Date();
		}
		// End of thread
		break;
	case de_be_fungicide:
		// Here comes the fungicide thread
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_be_fungicide, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), de_be_harvest, false, m_farm, m_field);
		break;
	case de_be_insecticide:
		// Here comes the insecticide thread
		if (DE_BE_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, de_be_insecticide, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.05 * g_landscape_p->SupplyPestIncidenceFactor()))
			{
				// here we check wether we are using ERA pesticide or not
				d1 = g_date->DayInYear(25, 7) - g_date->DayInYear();
				if (!cfg_pest_beet_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_be_insecticide, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case de_be_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_be_harvest, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, de_be_ferti_s8, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, de_be_ferti_p8, false, m_farm, m_field);
		break;

	case de_be_ferti_p8:
		if (m_ev->m_lock || m_farm->DoIt(10))
		{
			if (!m_farm->FP_Calcium(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_p8, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	case de_be_ferti_s8:
		if (m_ev->m_lock || m_farm->DoIt(10))
		{
			if (!m_farm->FA_Calcium(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_be_ferti_s8, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_Beet::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}
