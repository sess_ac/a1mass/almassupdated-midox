//
// BroadBeans.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2015, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/BroadBeans.h"


extern CfgFloat cfg_strigling_prop;

bool BroadBeans::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev ) {
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;

	bool done = false;
	int d1 = 0;
	int d2 = 0;
	// For catch crops......
	TTypesOfVegetation l_tov = tov_BroadBeans; // The current type
	int l_nextcropstartdate; 
	// .....to here
	switch (m_ev->m_todo) {
		case bb_start:
			a_field->ClearManagementActionSum();

			m_field->SetVegPatchy( true ); // A crop with wide rows, so set patchy

			// Set up the date management stuff
			m_last_date = g_date->DayInYear( 28, 2 );
			// Start and stop dates for all events after harvest
			m_field->SetMDates( 0, 0, g_date->DayInYear( 28, 2 ) );

			// Broad beans does not fit into the normal crop management, so must precede a spring sown crop (SpringBarleySpr probably!)

			//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
			if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

				if ((m_ev->m_startday > g_date->DayInYear( 1, 7 ))   // this means spring start crop is OK
					|| (m_field->GetMDates( 0, 0 ) >= m_ev->m_startday)) {  // This checks that the spring start is not too early 
					char veg_type[ 20 ];
					sprintf( veg_type, "%d", m_ev->m_next_tov );
					g_msg->Warn( WARN_FILE, "BroadBeans::Do(): Harvest too late for the next crop to start!!! The next crop is: ", veg_type );
					exit( 1 );
				}
				// Not allowed to fix any late finishing problems!
				/*
				for (int i = 0; i < noDates; i++) {
				if (m_field->GetMDates( 0, i ) >= m_ev->m_startday) {
				m_field->SetMDates( 0, i, m_ev->m_startday - 1 ); //move the starting date
				}
				if (m_field->GetMDates( 1, i ) >= m_ev->m_startday) {
				m_field->SetMConstants( i, 0 );
				m_field->SetMDates( 1, i, m_ev->m_startday - 1 ); //move the finishing date
				}
				}
				*/

				if (!m_ev->m_first_year) {
					// Here we need to allow a start before 1/7 in case we run over the year - the only crop to do this (2802215)
					d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn( WARN_BUG, "BroadBeans::Do(): Crop start attempt after last possible start date", "" );
						exit( 1 );
					}
				}
				else {
					// If this is the first year of running then it is possible to start
					// on day 0, so need this to tell us what to do:
					SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 3 ),
						bb_spring_plough, false );
					break;
				}
			}//if
			// End single block date checking code. Please see next line
			// comment as well.
			// Reinit d1 to first possible starting date.
			d1 = g_date->OldDays() + g_date->DayInYear( 1, 3 );
			if (g_date->Date() > d1) {
				d1 = g_date->Date();
			}
			// OK, let's go.
			SimpleEvent( d1, bb_spring_plough, false );
			break;

		case bb_spring_plough:
			if (m_ev->m_lock || m_farm->DoIt( 100 )) {
				if (!m_farm->SpringPlough( m_field, 0.0,
					g_date->DayInYear( 1, 4 ) -
					g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, bb_spring_plough, true );
					break;
				}
			}
			d1 = g_date->Date() + 1;
			d2 = g_date->OldDays() + g_date->DayInYear( 5, 3 );
			if (d1 < d2) d1 = d2;
			SimpleEvent( d1, bb_spring_harrow, false );
			break;

		case bb_spring_harrow:
			if (!m_farm->SpringHarrow( m_field, 0.0, g_date->DayInYear( 5, 4 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, bb_spring_harrow, true );
				break;
			}
			d1 = g_date->Date();
			d2 = g_date->OldDays() + g_date->DayInYear( 25, 3 );
			if (d1 < d2) d1 = d2;
			SimpleEvent( d1, bb_spring_sow, false );
			break;

		case bb_spring_sow:
			if (!m_farm->SpringSow( m_field, 0.0, g_date->DayInYear( 15, 4 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, bb_spring_sow, true );
				break;
			}
			SimpleEvent( g_date->Date(), bb_spring_roll, false );
			break;

		case bb_spring_roll:
			if (!m_farm->SpringRoll( m_field, 0.0, g_date->DayInYear( 15, 4 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, bb_spring_roll, true );
				break;
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 4 ), bb_strigling, false );
			break;

		case bb_strigling:
			if (m_ev->m_lock || (cfg_strigling_prop.value()*m_farm->DoIt( 100 ))) {
				if (!m_farm->Strigling( m_field, 0.0, g_date->DayInYear( 30, 4 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, bb_strigling, true );
					break;
				}
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ), bb_rowcultivation, false );
			break;


		case bb_rowcultivation:
			if (!m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 30, 5 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, bb_rowcultivation, true );
				break;
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 6 ), bb_water_one, false );
			break;

		case bb_water_one:
			if (m_ev->m_lock || (m_ev->m_field->GetSoilTypeR() == 0)) { // Here we ask a soil question, if sandy water is needed
				if (!m_farm->Water( m_field, 0.0, g_date->DayInYear( 30, 6 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, bb_water_one, true );
					break;
				}
				// Did first water, so do the second one too.
				SimpleEvent( g_date->Date() + 14, bb_water_two, false );
				break;
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 8 ), bb_harvest, false );
			break;
		case bb_water_two:
			if (!m_farm->Water( m_field, 0.0, g_date->DayInYear( 21, 7 ) - g_date->DayInYear( ) )) {
				SimpleEvent( g_date->Date() + 1, bb_water_two, true );
				break;
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 8 ), bb_harvest, false );
			break;

		case bb_harvest:
			if (m_ev->m_lock || m_farm->DoIt( 50 )) {
				if (!m_farm->Harvest( m_field, 0.0, g_date->DayInYear( 30, 9 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, bb_harvest, true );
					break;
				}
			}
			//SimpleEvent( g_date->OldDays( ) + 365 + g_date->DayInYear( 28, 2 ), bb_wait, true );
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(), bb_wait, true);
			break;

		case bb_wait:
			// Testing code to be deleted
			l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
			m_field->SetVegPatchy(false); // reverse the patchy before the next crop
			m_farm->AddNewEvent(tov_GenericCatchCrop, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
			m_field->SetVegType(tov_GenericCatchCrop, tov_Undefined);
			if (m_field->GetUnsprayedMarginPolyRef() != -1)
			{
				LE* um = m_OurLandscape->SupplyLEPointer(m_field->GetUnsprayedMarginPolyRef());
				um->SetVegType(tov_GenericCatchCrop, tov_Undefined);
			}
			break;
			// NB no "done = true" because this crop effectively continues into the catch crop.
			// To here 
			/*
			// End Main Thread
			m_field->SetVegPatchy( false ); // reverse the patchy before the next crop
			done = true;
			*/
		default:
			g_msg->Warn( WARN_BUG, "BroadBeans::Do(): "	"Unknown event type! ", "" );
			exit( 1 );
	}

	return done;
}


