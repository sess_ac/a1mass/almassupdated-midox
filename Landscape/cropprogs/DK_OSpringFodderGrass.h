/**
\file
\brief
<B>DK_OSpringFodderGrass.h This file contains the headers for the DK_OSpringFodderGrass class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_OSpringFodderGrass.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OSpringFodderGrass_SP_H
#define DK_OSpringFodderGrass_SP_H

#define DK_OSFG_BASE 67800
/**
\THIS CODE IS FOR 1ST YEAR OF CLOVERGRASS SOWED AS SOLE CROP IN SPRING (if sowed as lay-out w- covercrop, it is incorporated in other crop managements - ex. cereal and legume)
*/

/** Below is the list of things that a farmer can do if he is growing OSpringFodderGrass, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_osfg_start = 1, // Compulsory, must always be 1 (one).
	dk_osfg_sleep_all_day = DK_OSFG_BASE,
	dk_osfg_harrow,
	dk_osfg_manure_s,
	dk_osfg_ferti_sk_s,
	dk_osfg_manure_p,
	dk_osfg_ferti_sk_p,
	dk_osfg_spring_plough,
	dk_osfg_sow,
	dk_osfg_water,
	dk_osfg_cutting1,
	dk_osfg_cutting2,
	dk_osfg_grazing,
	dk_osfg_cattle_is_out,
	dk_osfg_foobar,
} DK_OSpringFodderGrassToDo;


/**
\brief
DK_OSpringFodderGrass class
\n
*/
/**
See DK_OSpringFodderGrass.h::DK_OSpringFodderGrassToDo for a complete list of all possible events triggered codes by the DK_OSpringFodderGrass management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OSpringFodderGrass : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OSpringFodderGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(31, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_osfg_foobar - DK_OSFG_BASE);
		m_base_elements_no = DK_OSFG_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_osfg_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_osfg_sleep_all_day = DK_osfg_BASE,
			fmc_Cultivation,	//	dk_osfg_harrow,
			fmc_Fertilizer,	//	dk_osfg_manure,
			fmc_Fertilizer,	//	dk_osfg_ferti_sk,
			fmc_Fertilizer,	//	dk_osfg_manure,
			fmc_Fertilizer,	//	dk_osfg_ferti_sk,
			fmc_Cultivation,	//	dk_osfg_spring_plough,
			fmc_Others,	//	dk_osfg_sow,
			fmc_Watering,	//	dk_osfg_water,
			fmc_Cutting,	//	dk_osfg_cutting1,
			fmc_Cutting,	//	dk_osfg_cutting2,
			fmc_Grazing,	//	dk_osfg_grazing,
			fmc_Grazing	//	dk_osfg_cattle_is_out,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_OSpringFodderGrass_H