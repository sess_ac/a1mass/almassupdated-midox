//
// OSpringBarleyPigs.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OSpringBarleyPigs_H
#define OSpringBarleyPigs_H

#define OSBarleyPigs_BASE 4300
# define OSBP_SOW_DATE         m_field->m_user[0]
# define OSBP_STRIGLING_DATE   m_field->m_user[1]

typedef enum {
  osbp_start = 1, // Compulsory, start event must always be 1 (one).
  osbp_ferti = OSBarleyPigs_BASE,
  osbp_harvest,
  osbp_spring_plough,
  osbp_autumn_plough,
  osbp_spring_harrow,
  osbp_spring_roll,
  osbp_spring_sow1,
  osbp_strigling_sow,
  osbp_hay,
  osbp_strigling1,
  osbp_strigling2,
  osbp_strigling3,
  osbp_straw_chopping,
  osbp_foobar,
} OSBPigsToDo;



class OSpringBarleyPigs: public Crop
{
public:
	OSpringBarleyPigs(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (osbp_foobar - OSBarleyPigs_BASE);
		m_base_elements_no = OSBarleyPigs_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  osbp_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  osbp_ferti = OSBarleyPigs_BASE,
			fmc_Harvest,	//	  osbp_harvest,
			fmc_Cultivation,	//	  osbp_spring_plough,
			fmc_Cultivation,	//	  osbp_autumn_plough,
			fmc_Cultivation,	//	  osbp_spring_harrow,
			fmc_Cultivation,	//	  osbp_spring_roll,
			fmc_Others,	//	  osbp_spring_sow1,
			fmc_Cultivation,	//	  osbp_strigling_sow,
			fmc_Others,	//	  osbp_hay,
			fmc_Cultivation,	//	  osbp_strigling1,
			fmc_Cultivation,	//	  osbp_strigling2,
			fmc_Cultivation,	//	  osbp_strigling3,
			fmc_Others	//	  osbp_straw_chopping,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
	bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
};

#endif // OSpringBarleyPigs_H
