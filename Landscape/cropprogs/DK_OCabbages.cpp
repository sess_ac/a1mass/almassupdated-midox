/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OCabbages.cpp This file contains the source for the DK_OCabbages class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of March 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OCabbages.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OCabbages.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..

// check if below are needed



/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional cabbage.
*/
bool DK_OCabbages::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKOCabbages;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_oca_start:
	{
		// dk_ocab_start just sets up all the starting conditions and reference dates that are needed to start a dk_oca
		DK_OCAB_WINTER_PLOUGH = false;

		a_field->ClearManagementActionSum();

		DK_OCAB_FORCESPRING = false;
		m_last_date = g_date->DayInYear(30, 11); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(30, 11); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(30, 11); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(dk_oca_molluscicide1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_ev->m_forcespring) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 2) + 365, dk_oca_molluscicide1, false);
			DK_OCAB_FORCESPRING = true;
			break;
		}
		else SimpleEvent(d1, dk_oca_autumn_plough_clay, false);
		break;
	}
	break;

	// This is the first real farm operation - done before the 31st of October
	case dk_oca_autumn_plough_clay:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{

			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oca_autumn_plough_clay, true);
				break;
			}
		}
		//LKM: Queue up next event molluscicide 1 (suggests 50% of all do this - after the 20th of February next year)
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 2) + 365, dk_oca_molluscicide1, false);
		break;
		// LKM: do it before the 5th of May
	case dk_oca_molluscicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oca_molluscicide1, true);
				break;
			}
		}
		//LKM: Queue up next event - spring harrow if sandy soil, done before the 10th of May
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 2), dk_oca_spring_harrow_sandy, false);
		break;
	case dk_oca_spring_harrow_sandy:
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oca_spring_harrow_sandy, true);
				break;
			}
		}
	// LKM: Queue up the next event - shallow harrow1 (making seedbed) done after the 1st of March and before the 30th of April - if not done, try again +1 day until the 30th of April when we will succeed
	SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_oca_sharrow1, false);
	break;
	case dk_oca_sharrow1:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_sharrow1, true);
			break;
		}
		// LKM: Queue up the next event - shallow harrow2 (making seedbed) done 5 days after, and before the 10th of May - if not done, try again +1 day until the 10th of May when we will succeed
		SimpleEvent(g_date->Date() + 5, dk_oca_sharrow2, false);
		break;
	case dk_oca_sharrow2:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_sharrow2, true);
			break;
		}
		// LKM: Queue up the next event - shallow harrow3 (making seedbed) done 5 days after, and before the 20th of May - if not done, try again +1 day until the 20th of May when we will succeed
		SimpleEvent(g_date->Date() + 5, dk_oca_sharrow3, false);
		break;
	case dk_oca_sharrow3:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_sharrow3, true);
			break;
		}
		// LKM: Queue up the next event - shallow harrow4 (making seedbed) done 5 days after, and before the 30th of May - if not done, try again +1 day until the 30th of May when we will succeed
		SimpleEvent(g_date->Date() + 5, dk_oca_sharrow4, false);
		break;
	case dk_oca_sharrow4:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_sharrow4, true);
			break;
		}
		// LKM: Queue up the next event - planting done (together with manure) the day after, and before 31th of May - if not done, try again +1 day until the 31th of May when we will succeed
		SimpleEvent(g_date->Date() + 1, dk_oca_plant, false);
		break;
	case dk_oca_plant:
		if (!m_farm->SpringSowWithFerti(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_plant, true);
			break;
		}
		// LKM: Queue up the next event - watering done same day as planting, and before 31th of May - if not done, try again +1 day until the 31th of May when we will succeed
		SimpleEvent(g_date->Date(), dk_oca_water, false);
		break;
	case dk_oca_water:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear()))
			{
				SimpleEvent(g_date->Date() + 1, dk_oca_water, true);
				break;
			}
		}
		//LKM: Queue up next event molluscicide 2 (suggests 50% of all do this 
		SimpleEvent(g_date->Date() + 1, dk_oca_molluscicide2, false);
		break;
		// LKM: do it before the 1st of June
	case dk_oca_molluscicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oca_molluscicide2, true);
				break;
			}
		}
		// LKM: Queue up the next event - strigling1 (making seedbed) done after the +7 days after planting, and before the 8th of June - if not done, try again +1 day until the 8th of June when we will succeed
		SimpleEvent(g_date->Date() + 7, dk_oca_strigling1, false);
		break;
	case dk_oca_strigling1:
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_strigling1, true);
			break;
		}
		// LKM: Queue up the next event - strigling2 (making seedbed) done 8 days after, and before the 15th of June - if not done, try again +1 day until the 15th of June when we will succeed
		SimpleEvent(g_date->Date() + 8, dk_oca_strigling2, false);
		break;
	case dk_oca_strigling2:
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_strigling2, true);
			break;
		}
		// LKM: Queue up the next event - strigling3 (making seedbed) done 8 days after, and before the 23th of June - if not done, try again +1 day until the 23th of June when we will succeed
		SimpleEvent(g_date->Date() + 8, dk_oca_strigling3, false);
		break;
	case dk_oca_strigling3:
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(23, 6) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_strigling3, true);
			break;
		}
		// LKM: Queue up the next event - row cultivation (if clay soil) done before the 30th of June - if not done, try again +1 day until the 30th of June when we will succeed
		SimpleEvent(g_date->Date() + 1, dk_oca_row_cultivation_clay, false);
		break;
	case dk_oca_row_cultivation_clay:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{

			if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oca_row_cultivation_clay, true);
				break;
			}
		}
		// LKM: Queue up the next event - cover cabbages with net or fiber to prevent insects, done before the 25th of July - if not done, try again +1 day until the 25th of July when we will succeed
		SimpleEvent(g_date->Date() + 1, dk_oca_covering1, false);
		break;
	case dk_oca_covering1:
		if (!m_farm->StrawCovering(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_covering1, true);
			break;
		}
		// LKM: Queue up the next event - remove cover from cabbages, done before the 30th of July - if not done, try again +1 day until the 30th of July when we will succeed
		SimpleEvent(g_date->Date() + 20, dk_oca_remove_cover1, false);
		break;
	case dk_oca_remove_cover1:
		if (!m_farm->StrawRemoval(m_field, 0.0, g_date->DayInYear(14, 8) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_remove_cover1, true);
			break;
		}
		// LKM: Queue up the next event - manual weeding 
		SimpleEvent(g_date->Date() + 1, dk_oca_manual_weeding, false);
		break;
	case dk_oca_manual_weeding:
		if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_manual_weeding, true);
			break;
		}
		// LKM: Queue up the next event - cover cabbages with net or fiber to prevent insects, done before the 15th of August - if not done, try again +1 day until the 15th of August when we will succeed
		SimpleEvent(g_date->Date(), dk_oca_covering2, false);
		break;
	case dk_oca_covering2:
		if (!m_farm->StrawCovering(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_covering2, true);
			break;
		}
		// LKM: Queue up the next event - remove cover from cabbages, done before the 25th of November - if not done, try again +1 day until the 25th of November when we will succeed
		SimpleEvent(g_date->Date() + 20, dk_oca_remove_cover2, false);
		break;
	case dk_oca_remove_cover2:
		if (!m_farm->StrawRemoval(m_field, 0.0, g_date->DayInYear(25, 11) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_remove_cover2, true);
			break;
		}
		// LKM: Queue up the next event - harvest cabbages, done before the 30th of November - if not done, try again +1 day until the 30th of November when we will succeed
		SimpleEvent(g_date->Date()+1, dk_oca_harvest, false);
		break;
	case dk_oca_harvest:
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) 
		{
			SimpleEvent(g_date->Date() + 1, dk_oca_harvest, true);
			break;
		}
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_oca_wait, false);
			// Because we are ending harvest before 1.7 so we need to wait until the 1.7
			break;
		}
		else {
			done = true; // end of plan
		}
	case dk_oca_wait:
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DK_OCabbages::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}