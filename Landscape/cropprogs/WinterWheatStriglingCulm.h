//
// WinterWheatStriglingCulm.h
//
/*

Copyright (c) 2005, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WINTERWHEATStriglingCulm_H
#define WINTERWHEATStriglingCulm_H

#define WINTERWHEATStriglingCulm_BASE 8400
#define WWStriglingCulm_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define WWStriglingCulm_AUTUMN_PLOUGH         m_field->m_user[1]

typedef enum {
  wwsc_start = 1, // Compulsory, must always be 1 (one).
  wwsc_sleep_all_day = WINTERWHEATStriglingCulm_BASE,
  wwsc_ferti_p1,
  wwsc_ferti_s1,
  wwsc_ferti_s2,
  wwsc_autumn_plough,
  wwsc_autumn_harrow,
  wwsc_stubble_harrow1,
  wwsc_autumn_sow,
  wwsc_autumn_roll,
  wwsc_ferti_p2,
  wwsc_strigling0a,
  wwsc_spring_roll,
  wwsc_strigling0b,
  wwsc_GR,
  wwsc_fungicide,
  wwsc_fungicide2,
  wwsc_insecticide1,
  wwsc_insecticide2,
  wwsc_insecticide3,
  wwsc_strigling1,
  wwsc_strigling2,
  wwsc_water1,
  wwsc_water2,
  wwsc_ferti_p3,
  wwsc_ferti_p4,
  wwsc_ferti_p5,
  wwsc_ferti_s3,
  wwsc_ferti_s4,
  wwsc_ferti_s5,
  wwsc_harvest,
  wwsc_straw_chopping,
  wwsc_hay_turning,
  wwsc_hay_baling,
  wwsc_stubble_harrow2,
  wwsc_grubning,
  wwsc_foobar
} WinterWheatStriglingCulmToDo;


class WinterWheatStriglingCulm: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   WinterWheatStriglingCulm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
     m_first_date=g_date->DayInYear( 1,10 );
	 SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (wwsc_foobar - WINTERWHEATStriglingCulm_BASE);
	   m_base_elements_no = WINTERWHEATStriglingCulm_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
		   fmc_Others,	// zero element unused but must be here
		  fmc_Others,//wwsc_start = 1, // Compulsory, must always be 1 (one).
		  fmc_Others,//wwsc_sleep_all_day = WINTERWHEATStriglingCulm_BASE,
		  fmc_Fertilizer,//wwsc_ferti_p1,
		  fmc_Fertilizer,//wwsc_ferti_s1,
		  fmc_Fertilizer,//wwsc_ferti_s2,
		  fmc_Cultivation,//wwsc_autumn_plough,
		  fmc_Cultivation,//wwsc_autumn_harrow,
		  fmc_Cultivation,//wwsc_stubble_harrow1,
		  fmc_Others,//wwsc_autumn_sow,
		  fmc_Others,//wwsc_autumn_roll,
		  fmc_Fertilizer,//wwsc_ferti_p2,
		  fmc_Cultivation,//wwsc_strigling0a,
		  fmc_Others,//wwsc_spring_roll,
		  fmc_Cultivation,//wwsc_strigling0b,
		  fmc_Others,//wwsc_GR,
		  fmc_Fungicide,//wwsc_fungicide,
		  fmc_Fungicide,//wwsc_fungicide2,
		  fmc_Insecticide,//wwsc_insecticide1,
		  fmc_Insecticide,//wwsc_insecticide2,
		  fmc_Insecticide,//wwsc_insecticide3,
		  fmc_Cultivation,//wwsc_strigling1,
		  fmc_Cultivation,//wwsc_strigling2,
		  fmc_Watering,//wwsc_water1,
		  fmc_Watering,//wwsc_water2,
		  fmc_Fertilizer,//wwsc_ferti_p3,
		  fmc_Fertilizer,//wwsc_ferti_p4,
		  fmc_Fertilizer,//wwsc_ferti_p5,
		  fmc_Fertilizer,//wwsc_ferti_s3,
		  fmc_Fertilizer,//wwsc_ferti_s4,
		  fmc_Fertilizer,//wwsc_ferti_s5,
		  fmc_Harvest,//wwsc_harvest,
		  fmc_Others,//wwsc_straw_chopping,
		  fmc_Others,//wwsc_hay_turning,
		  fmc_Others,//wwsc_hay_baling,
		  fmc_Cultivation,//wwsc_stubble_harrow2,
		  fmc_Cultivation//wwsc_grubning,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // WINTERWHEATStriglingCulm_H
