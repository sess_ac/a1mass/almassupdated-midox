/**
\file
\brief
<B>UKBeet.h This file contains the headers for the Beet class</B> \n
*/
/**
\file 
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKBeet.h
//


#ifndef UKBEET_H
#define UKBEET_H

#define UKBEET_BASE 45100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define UK_BE_HERBI1	a_field->m_user[1]
#define UK_BE_FUNGI1	a_field->m_user[2]


/** Below is the list of things that a farmer can do if he is growing beet, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	uk_be_start = 1, // Compulsory, must always be 1 (one).
	uk_be_sleep_all_day = UKBEET_BASE,
	uk_be_autumn_harrow, // autumn harrow 57.5% September
	uk_be_winter_plough, // winter plough 50% October - December
	uk_be_spring_plough, // spring plough 50% March
	uk_be_ferti_p1, // NPK 60% March - April
	uk_be_ferti_s1,
	uk_be_preseeding_cultivator, // 100% March - April
	uk_be_spring_sow, // 100% March -  April
	uk_be_ferti_p2, // NI 100% 100% April - May
	uk_be_ferti_s2,
	uk_be_herbicide1, // pre-em 75% April - May
	uk_be_herbicide2, // 100% End May - June
	uk_be_herbicide3, // 100% June
	uk_be_herbicide4, // 80% July
	uk_be_herbicide5, // 50% September 
	uk_be_fungicide1, // 100% July
	uk_be_fungicide2, // 75% August
	uk_be_harvest, // 100% Septemeber - December
	uk_be_foobar,
} UKBeetToDo;


/**
\brief
UKBeet class
\n
*/
/**
See UKBeet.h::UKBeetToDo for a complete list of all possible events triggered codes by the beet management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKBeet : public Crop
{
 public:
	 virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	 UKBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 20,12 );
		SetUpFarmCategoryInformation();
	 }
	 void SetUpFarmCategoryInformation() {
		 const int elements = 2 + (uk_be_foobar - UKBEET_BASE);
		 m_base_elements_no = UKBEET_BASE - 2;
		 FarmManagementCategory catlist[elements] =
		 {
			  fmc_Others, // this needs to be at the zero line
			  fmc_Others,//uk_be_start = 1, // Compulsory, must always be 1 (one).
			  fmc_Others,//uk_be_sleep_all_day = UKBEET_BASE,
			  fmc_Cultivation,//uk_be_autumn_harrow, // autumn harrow 57.5% September
			  fmc_Cultivation,//uk_be_winter_plough, // winter plough 50% October - December
			  fmc_Cultivation,//uk_be_spring_plough, // spring plough 50% March
			  fmc_Fertilizer,//uk_be_ferti_p1, // NPK 60% March - April
			  fmc_Fertilizer,//uk_be_ferti_s1,
			  fmc_Cultivation,//uk_be_preseeding_cultivator, // 100% March - April
			  fmc_Others,//uk_be_spring_sow, // 100% March -  April
			  fmc_Fertilizer,//uk_be_ferti_p2, // NI 100% 100% April - May
			  fmc_Fertilizer,//uk_be_ferti_s2,
			  fmc_Herbicide,//uk_be_herbicide1, // pre-em 75% April - May
			  fmc_Herbicide,//uk_be_herbicide2, // 100% End May - June
			  fmc_Herbicide,//uk_be_herbicide3, // 100% June
			  fmc_Herbicide,//uk_be_herbicide4, // 80% July
			  fmc_Herbicide,//uk_be_herbicide5, // 50% September 
			  fmc_Fungicide,//uk_be_fungicide1, // 100% July
			  fmc_Fungicide,//uk_be_fungicide2, // 75% August
			  fmc_Harvest,//uk_be_harvest, // 100% Septemeber - December
		 };
		 copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	 }
};

#endif // UKBEET_H

