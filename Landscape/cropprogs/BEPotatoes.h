/**
\file
\brief
<B>BEPotatoes.h This file contains the headers for the Potatoes class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BEPotatoes.h
//


#ifndef BEPOTATOES_H
#define BEPOTATOES_H

#define BEPOTATOES_BASE 25300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define BE_POT_HERBI		a_field->m_user[1]
#define BE_POT_FUNGI1		a_field->m_user[2]
#define BE_POT_FUNGI2		a_field->m_user[3]
#define BE_POT_FUNGI3		a_field->m_user[4]
#define BE_POT_FUNGI4		a_field->m_user[5]
#define BE_POT_FUNGI5		a_field->m_user[6]

/** Below is the list of things that a farmer can do if he is growing potatoes, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_pot_start = 1, // Compulsory, must always be 1 (one).
	BE_pot_sleep_all_day = BEPOTATOES_BASE,
	BE_pot_stubble_harrow,
	BE_pot_winter_plough_sandy,
	BE_pot_winter_plough_clay,
	BE_pot_ferti_p2_clay,
	BE_pot_ferti_s2_clay,
	BE_pot_ferti_p1_sandy,
	BE_pot_ferti_s1_sandy,	
	BE_pot_spring_plough_sandy,
	BE_pot_ferti_p2_sandy,
	BE_pot_ferti_s2_sandy,
	BE_pot_bed_forming,
	BE_pot_spring_planting,
	BE_pot_hilling1,
	BE_pot_ferti_p3_clay,
	BE_pot_ferti_s3_clay,
	BE_pot_ferti_p3_sandy,
	BE_pot_ferti_s3_sandy,
	BE_pot_ferti_p4,
	BE_pot_ferti_s4,
	BE_pot_herbicide1,
	BE_pot_fungicide1,
	BE_pot_fungicide2,
	BE_pot_fungicide3,
	BE_pot_fungicide4,
	BE_pot_fungicide5,
	BE_pot_fungicide6,
	BE_pot_fungicide7,
	BE_pot_fungicide8,
	BE_pot_fungicide9,
	BE_pot_fungicide10,
	BE_pot_fungicide11,
	BE_pot_fungicide12,
	BE_pot_fungicide13,
	BE_pot_fungicide14,
	BE_pot_fungicide15,
	BE_pot_insecticide,
	BE_pot_dessication1,
	BE_pot_dessication2,
	BE_pot_harvest,
} BEPotatoesToDo;


/**
\brief
BEPotatoes class
\n
*/
/**
See BEPotatoes.h::BEPotatoesToDo for a complete list of all possible events triggered codes by the potatoes management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEPotatoes: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEPotatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
   }
};

#endif // BEPOTATOES_H

