/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University, modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_MaizeSilage.cpp This file contains the source for the DE_MaizeSilage class</B> \n
*/
/**
\file
 by Chris J. Topping and Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_MaizeSilage.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_MaizeSilage.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_maize_on;
extern CfgFloat cfg_pest_product_1_amount;
extern Landscape* g_landscape_p;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_MaizeSilage::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
		m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
		m_field = a_field;
		m_ev = a_ev;
		bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
		bool flag = false;
		int d1 = 0;
		TTypesOfVegetation l_tov = tov_DEMaizeSilage; // The current type - change to match the crop you have
	/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_ms_start:
	{
		DE_MS_START_FERTI = false;
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(10, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(5, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(10, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)


		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_ms_spring_sow_with_ferti))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(15, 9) + isSpring;
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_ev->m_forcespring) {
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_ms_ferti_s1, false);
				break;
			}
			else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_ms_ferti_p1, false);
			break;
		}
		else SimpleEvent_(d1, de_ms_stubble_harrow, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case de_ms_stubble_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ms_stubble_harrow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		SimpleEvent_(d1, de_ms_winter_plough, false, m_farm, m_field);
		break;
	case de_ms_winter_plough: 
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) 
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_winter_plough, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_ms_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_ms_ferti_p1, false, m_farm, m_field);
		break;
	case de_ms_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70)) {
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough_sandy, false, m_farm, m_field);
		break;
	case de_ms_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70)) {
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough_sandy, false, m_farm, m_field);
		break;
	case de_ms_spring_plough_sandy: // on sandy soils
		if (m_field->GetSoilType() == 1 || m_field->GetSoilType() == 2 || m_field->GetSoilType() == 3 || m_field->GetSoilType() == 4) {
			if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
			{
				if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_ms_spring_plough_sandy, true, m_farm, m_field);
					break;
				}
				
			}
			SimpleEvent_(g_date->Date() + 10, de_ms_spring_sow_with_ferti, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->Date(), de_ms_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_ms_preseeding_cultivator:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_preseeding_cultivator, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_ms_spring_sow_with_ferti, false, m_farm, m_field);
		break;
	case de_ms_spring_sow_with_ferti:
		// all sow with ferti - 60% with NP, 40% with organic ferti (not separated here) 
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->SpringSowWithFerti(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_spring_sow_with_ferti, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_ms_herbicide1, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5) + m_date_modifier, de_ms_insecticide, false, m_farm, m_field); // insecticide thread 
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_ms_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_ms_ferti_p2, false, m_farm, m_field);
		break;
	case de_ms_ferti_p2:
		// Here comes N thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), de_ms_harvest, false, m_farm, m_field);
		break;
	case de_ms_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_ms_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), de_ms_harvest, false, m_farm, m_field);
		break;
	case de_ms_herbicide1:
		// Here comes the herbicide thread
		if (m_field->GetGreenBiomass() <= 0)
		{
			SimpleEvent_(g_date->Date() + 1, de_ms_herbicide1, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.95))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_ms_herbicide1, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;

	case de_ms_insecticide:
		// here we check wheter we are using ERA pesticide or not
		d1 = g_date->DayInYear(30, 6) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_maize_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_ms_insecticide, true, m_farm, m_field);
				break;
			}
		}
		// end of thread
		break;

	case de_ms_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ms_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_ms_straw_chopping, false, m_farm, m_field);
		break;
	case de_ms_straw_chopping:
		if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_ms_straw_chopping, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_MaizeSilage::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}
