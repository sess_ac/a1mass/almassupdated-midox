//
// DK_SpringOats_CC.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following dissbaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following dissbaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSBMUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSBMAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSBMUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSBMUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKSpringOats_CC_H
#define DKSpringOats_CC_H

#define DK_SOCC_BASE 161000

#define DK_SOCC_FORCESPRING	a_field->m_user[1]

typedef enum {
	dk_socc_start = 1, // Compulsory, start event must always be 1 (one).
	dk_socc_harvest = DK_SOCC_BASE,
	dk_socc_autumn_plough,
	dk_socc_spring_plough,
	dk_socc_roll,
	dk_socc_ferti_s1,
	dk_socc_ferti_p1,
	dk_socc_ferti_s2,
	dk_socc_ferti_p2,
	dk_socc_spring_harrow,
	dk_socc_spring_sow,
	dk_socc_herbicide1,
	dk_socc_herbicide2,
	dk_socc_herbicide3,
	dk_socc_insecticide,
	dk_socc_fungicide,
	dk_socc_gr,
	dk_socc_foobar,
} DK_SpringOats_CCToDo;



class DK_SpringOats_CC : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_SpringOats_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 11);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_socc_foobar - DK_SOCC_BASE);
		m_base_elements_no = DK_SOCC_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_socc_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_socc_harvest = DK_socc_BASE,
			  fmc_Cultivation,	//	  dk_socc_autumn_plough,
			  fmc_Cultivation,	//	  dk_socc_spring_plough,
			  fmc_Others,	//	  dk_socc_roll,
			  fmc_Fertilizer,	//	  dk_socc_ferti_s1,
			  fmc_Fertilizer,	//	  dk_socc_ferti_p1,
			  fmc_Fertilizer,	//	  dk_socc_ferti_s2,
			  fmc_Fertilizer,	//	  dk_socc_ferti_p2,
			  fmc_Cultivation, // dk_socc_spring_harrow,
			  fmc_Cultivation,	//	  dk_socc_spring_sow,
			  fmc_Herbicide,	//	  dk_socc_herbicide1,
			  fmc_Herbicide,	//	  dk_socc_herbicide2,
			  fmc_Herbicide,	//	  dk_socc_herbicide3,
			  fmc_Insecticide,	//	  dk_socc_insecticide,
			  fmc_Fungicide,	//	  dk_socc_fungicide,
			  fmc_Others, //		dk_socc_gr,

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};


#endif // DK_SpringOats_CC_H