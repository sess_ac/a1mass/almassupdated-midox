/**
\file
\brief
<B>NLPotatoes.h This file contains the headers for the Potatoes class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLPotatoes.h
//


#ifndef NLPOTATOES_H
#define NLPOTATOES_H

#define NLPOTATOES_BASE 22000
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_POT_HERBI		a_field->m_user[1]
#define NL_POT_FUNGI1		a_field->m_user[2]
#define NL_POT_FUNGI2		a_field->m_user[3]
#define NL_POT_FUNGI3		a_field->m_user[4]
#define NL_POT_FUNGI4		a_field->m_user[5]
#define NL_POT_FUNGI5		a_field->m_user[6]

/** Below is the list of things that a farmer can do if he is growing potatoes, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_pot_start = 1, // Compulsory, must always be 1 (one).
	nl_pot_sleep_all_day = NLPOTATOES_BASE,
	nl_pot_stubble_harrow,
	nl_pot_winter_plough_clay,
	nl_pot_ferti_p2_clay,
	nl_pot_ferti_s2_clay,
	nl_pot_ferti_p1_sandy,
	nl_pot_ferti_s1_sandy,	
	nl_pot_spring_plough_sandy,
	nl_pot_ferti_p2_sandy,
	nl_pot_ferti_s2_sandy,
	nl_pot_bed_forming,
	nl_pot_spring_planting,
	nl_pot_hilling1,
	nl_pot_ferti_p3_clay,
	nl_pot_ferti_s3_clay,
	nl_pot_ferti_p3_sandy,
	nl_pot_ferti_s3_sandy,
	nl_pot_ferti_p4,
	nl_pot_ferti_s4,
	nl_pot_herbicide1,
	nl_pot_herbicide2,
	nl_pot_fungicide1,
	nl_pot_fungicide2,
	nl_pot_fungicide3,
	nl_pot_fungicide4,
	nl_pot_fungicide5,
	nl_pot_fungicide6,
	nl_pot_fungicide7,
	nl_pot_fungicide8,
	nl_pot_fungicide9,
	nl_pot_fungicide10,
	nl_pot_fungicide11,
	nl_pot_fungicide12,
	nl_pot_fungicide13,
	nl_pot_fungicide14,
	nl_pot_fungicide15,
	nl_pot_insecticide,
	nl_pot_dessication1,
	nl_pot_dessication2,
	nl_pot_harvest,
	nl_pot_foobar
} NLPotatoesToDo;


/**
\brief
NLPotatoes class
\n
*/
/**
See NLPotatoes.h::NLPotatoesToDo for a complete list of all possible events triggered codes by the potatoes management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLPotatoes: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLPotatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		m_forcespringpossible = true;
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_pot_foobar - NLPOTATOES_BASE);
	   m_base_elements_no = NLPOTATOES_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//nl_pot_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_pot_sleep_all_day = NLPOTATOES_BASE,
			fmc_Cultivation,//nl_pot_stubble_harrow,
			fmc_Cultivation,//nl_pot_winter_plough_clay,
			fmc_Fertilizer,//nl_pot_ferti_p2_clay,
			fmc_Fertilizer,//nl_pot_ferti_s2_clay,
			fmc_Fertilizer,//nl_pot_ferti_p1_sandy,
			fmc_Fertilizer,//nl_pot_ferti_s1_sandy,
			fmc_Cultivation,//nl_pot_spring_plough_sandy,
			fmc_Fertilizer,//nl_pot_ferti_p2_sandy,
			fmc_Fertilizer,//nl_pot_ferti_s2_sandy,
			fmc_Others,//nl_pot_bed_forming,
			fmc_Others,//nl_pot_spring_planting,
			fmc_Others,//nl_pot_hilling1,
			fmc_Fertilizer,//nl_pot_ferti_p3_clay,
			fmc_Fertilizer,//nl_pot_ferti_s3_clay,
			fmc_Fertilizer,//nl_pot_ferti_p3_sandy,
			fmc_Fertilizer,//nl_pot_ferti_s3_sandy,
			fmc_Fertilizer,//nl_pot_ferti_p4,
			fmc_Fertilizer,//nl_pot_ferti_s4,
			fmc_Herbicide,//nl_pot_herbicide1,
			fmc_Herbicide,//nl_pot_herbicide2,
			fmc_Fungicide,//nl_pot_fungicide1,
			fmc_Fungicide,//nl_pot_fungicide2,
			fmc_Fungicide,//nl_pot_fungicide3,
			fmc_Fungicide,//nl_pot_fungicide4,
			fmc_Fungicide,//nl_pot_fungicide5,
			fmc_Fungicide,//nl_pot_fungicide6,
			fmc_Fungicide,//nl_pot_fungicide7,
			fmc_Fungicide,//nl_pot_fungicide8,
			fmc_Fungicide,//nl_pot_fungicide9,
			fmc_Fungicide,//nl_pot_fungicide10,
			fmc_Fungicide,//nl_pot_fungicide11,
			fmc_Fungicide,//nl_pot_fungicide12,
			fmc_Fungicide,//nl_pot_fungicide13,
			fmc_Fungicide,//nl_pot_fungicide14,
			fmc_Fungicide,//nl_pot_fungicide15,
			fmc_Insecticide,//nl_pot_insecticide,
			fmc_Others,//nl_pot_dessication1,
			fmc_Others,//nl_pot_dessication2,
			fmc_Harvest//nl_pot_harvest,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLPOTATOES_H

