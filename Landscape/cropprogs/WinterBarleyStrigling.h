//
// WinterBarleyStrigling.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WinterBarleyStrigling_H
#define WinterBarleyStrigling_H

#define WBarleyStrigling_BASE 7700

#define WBS_DID_SEVEN_ONE   m_field->m_user[0]
#define WBS_DID_SEVEN_TWO   m_field->m_user[1]
#define WBS_DID_SEVEN_THREE m_field->m_user[2]
#define WBS_DID_SEVEN_FOUR  m_field->m_user[3]

#define WBS_FUNGICIDE_DATE  m_field->m_user[4]
#define WBS_HERBICIDE_DATE  m_field->m_user[5]

#define WBS_DID_EIGHT_ONE   m_field->m_user[0]
#define WBS_DID_EIGHT_TWO   m_field->m_user[1]
#define WBS_DID_EIGHT_THREE m_field->m_user[2]
#define WBS_DID_EIGHT_FOUR  m_field->m_user[3]

typedef enum {
  wbs_start = 1, // Compulsory, start event must always be 1 (one).
  wbs_fertsludge_plant_one = WBarleyStrigling_BASE,
  wbs_fertmanure_stock,
  wbs_fertslurry_stock_one,
  wbs_autumn_plough,
  wbs_autumn_harrow,
  wbs_autumn_sow,
  wbs_strigling_one,
  wbs_fertmanganese_plant_one,
  wbs_fertmanganese_plant_two,
  wbs_fertnpk_plant_one,
  wbs_fertnpk_plant_two,
  wbs_fertslurry_stock_two,
  wbs_fertnpk_stock_one,
  wbs_fertnpk_stock_two,
  wbs_strigling_two,
  wbs_fungicide_one,
  wbs_switchboard,
  wbs_fungicide_two,
  wbs_growth_reg,
  wbs_water,
  wbs_insecticide,
  wbs_harvest,
  wbs_hay_turning,
  wbs_hay_bailing,
  wbs_stubble_harrowing,
  wbs_foobar
// --FN--
} WBTSoDo;



class WinterBarleyStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  WinterBarleyStrigling(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(15,9);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (wbs_foobar - WBarleyStrigling_BASE);
	  m_base_elements_no = WBarleyStrigling_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//wbs_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//wbs_fertsludge_plant_one = WBarleyStrigling_BASE,
		  fmc_Fertilizer,//wbs_fertmanure_stock,
		  fmc_Fertilizer,//wbs_fertslurry_stock_one,
		  fmc_Cultivation,//wbs_autumn_plough,
		  fmc_Cultivation,//wbs_autumn_harrow,
		  fmc_Others,//wbs_autumn_sow,
		  fmc_Cultivation,//wbs_strigling_one,
		  fmc_Fertilizer,//wbs_fertmanganese_plant_one,
		  fmc_Fertilizer,//wbs_fertmanganese_plant_two,
		  fmc_Fertilizer,//wbs_fertnpk_plant_one,
		  fmc_Fertilizer,//wbs_fertnpk_plant_two,
		  fmc_Fertilizer,//wbs_fertslurry_stock_two,
		  fmc_Fertilizer,//wbs_fertnpk_stock_one,
		  fmc_Fertilizer,//wbs_fertnpk_stock_two,
		  fmc_Cultivation,//wbs_strigling_two,
		  fmc_Insecticide,//wbs_fungicide_one,
		  fmc_Others,//wbs_switchboard,
		  fmc_Insecticide,//wbs_fungicide_two,
		  fmc_Others,//wbs_growth_reg,
		  fmc_Watering,//wbs_water,
		  fmc_Insecticide,//wbs_insecticide,
		  fmc_Harvest,//wbs_harvest,
		  fmc_Others,//wbs_hay_turning,
		  fmc_Others,//wbs_hay_bailing,
		  fmc_Cultivation//wbs_stubble_harrowing

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // WinterBarley_H
