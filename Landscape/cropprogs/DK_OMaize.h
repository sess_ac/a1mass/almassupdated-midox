//
// DK_OMaize.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OMaize_H
#define DK_OMaize_H

#define DK_OM_BASE 69100
#define DK_OM_FORCESPRING	a_field->m_user[1]
#define DK_OM_AUTUMN_PLOUGH	a_field->m_user[2]

typedef enum {
  dk_om_start = 1, // Compulsory, start event must always be 1 (one).
  dk_om_autumn_plough = DK_OM_BASE,
  dk_om_harrow1,
  dk_om_harrow2,
  dk_om_slurry_s,
  dk_om_slurry_p,
  dk_om_spring_plough,
  dk_om_roll,
  dk_om_strigling1, 
  dk_om_strigling2,
  dk_om_ferti_sow,
  dk_om_water,
  dk_om_strigling3,
  dk_om_harrow3,
  dk_om_harrow4,
  dk_om_row_cultivation1,
  dk_om_row_cultivation2,
  dk_om_harvest,
  dk_om_straw_chopping,
  dk_om_wait,
  dk_om_foobar
} DK_OMaizeToDo;



class DK_OMaize : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OMaize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1,12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_om_foobar - DK_OM_BASE);
		m_base_elements_no = DK_OM_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_om_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation, //dk_om_autumn_plough = DK_OM_BASE,
			fmc_Cultivation, //dk_om_harrow1,
			fmc_Cultivation, //dk_om_harrow2,
			fmc_Fertilizer, // dk_om_slurry_s,
			fmc_Fertilizer, //  dk_om_slurry_p,
			fmc_Cultivation, // dk_om_spring_plough,
			fmc_Others, // dk_om_roll,
			fmc_Cultivation, // dk_om_strigling1,
			fmc_Cultivation, // dk_om_strigling2,
			fmc_Cultivation, // dk_om_ferti_sow,
			fmc_Watering, //dk_om_water,
			fmc_Cultivation, //  dk_om_strigling3,
			fmc_Cultivation, //  dk_om_harrow3,
			fmc_Cultivation, //  dk_om_harrow4,
			fmc_Cultivation, //  dk_om_row_cultivation1,
			fmc_Cultivation, //  dk_om_row_cultivation2,
			fmc_Harvest, // dk_om_harvest1,
			fmc_Cutting, // dk_om_straw_chopping,
			fmc_Others, // dk_om_wait,
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_OMaize_H
