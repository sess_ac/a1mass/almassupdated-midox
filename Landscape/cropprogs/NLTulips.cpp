/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CA LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLTulips.cpp This file contains the source for the NLTulips class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// NLTulips.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLTulips.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_tulips_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_TU_InsecticideDay;
extern CfgInt   cfg_TU_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional tulips.
*/
bool NLTulips::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLTulips;
	int l_nextcropstartdate;
	/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case nl_tu_start:
	{
		NL_TU_AUTUMN_PLOUGH = false;
		NL_TU_FERTI_DONE = false;
		NL_TU_STRAW_REMOVED = false;
		NL_TU_FUNGI_SPRAY_DATE = 0;
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(20, 7); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(20, 7); // first possible day of finishing harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(20, 7);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(nl_tu_fungicide1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9) + isSpring;
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_tu_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, nl_tu_ferti_p1, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case nl_tu_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 9);
		}
		SimpleEvent_(d1, nl_tu_autumn_plough, false, m_farm, m_field);
		break;
	case nl_tu_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 9);
		}
		SimpleEvent_(d1, nl_tu_autumn_plough, false, m_farm, m_field);
		break;
	case nl_tu_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_autumn_plough, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 10, nl_tu_bed_forming, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_tu_autumn_heavy_stubble_cultivator, false, m_farm, m_field);
		break;
	case nl_tu_autumn_heavy_stubble_cultivator:
		if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_autumn_heavy_stubble_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 10, nl_tu_bed_forming, false, m_farm, m_field);
		break;
	case nl_tu_bed_forming:
		if (!m_farm->BedForming(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_bed_forming, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide0, false, m_farm, m_field);
		break;
	case nl_tu_fungicide0:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide0, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_planting, false, m_farm, m_field);
		break;
	case nl_tu_planting:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_planting, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 1) + 365, nl_tu_herbicide1, false, m_farm, m_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, nl_tu_insecticide1, false, m_farm, m_field);	// Insecticide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_tu_fungicide1, false, m_farm, m_field);	// Fungicide thread = MAIN THREAD
		SimpleEvent_(g_date->Date() + 10, nl_tu_straw_covering, false, m_farm, m_field);	// Flower management
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4) + 365, nl_tu_irrigation, false, m_farm, m_field);	// Flower management
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 5, nl_tu_ferti_s2, false, m_farm, m_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->Date() + 5, nl_tu_ferti_p2, false, m_farm, m_field);
		break;
	case nl_tu_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		NL_TU_FERTI_DONE = true;
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 1) + 365, nl_tu_ferti_p3, false, m_farm, m_field);
		break;
	case nl_tu_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		NL_TU_FERTI_DONE = true;
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 1) + 365, nl_tu_ferti_s3, false, m_farm, m_field);
		break;
	case nl_tu_ferti_p3:
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_p3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), nl_tu_ferti_p4, false, m_farm, m_field);
		break;
	case nl_tu_ferti_s3:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_s3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), nl_tu_ferti_s4, false, m_farm, m_field);
		break;
	case nl_tu_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_tu_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_tu_straw_covering:
		if (NL_TU_FERTI_DONE == 0) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_straw_covering, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->StrawCovering(m_field, 0.0, g_date->DayInYear(20, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_straw_covering, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 1) + 365, nl_tu_straw_removal, false, m_farm, m_field);
			break;
		}
		break;
	case nl_tu_straw_removal:
		if (!m_farm->StrawRemoval(m_field, 0.0, g_date->DayInYear(20, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_straw_removal, true, m_farm, m_field);
			break;
		}
		NL_TU_STRAW_REMOVED = true;
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 4), nl_tu_flower_cutting, false, m_farm, m_field);
		break;
	case nl_tu_flower_cutting:
		if (!m_farm->FlowerCutting(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_flower_cutting, true, m_farm, m_field);
			break;
		}
		// End of thread
		break;
	case nl_tu_herbicide1: // The first of the pesticide managements.
						   // Here comes the herbicide thread
		if (NL_TU_STRAW_REMOVED == 0) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_herbicide1, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 2) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_herbicide1, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 7, nl_tu_herbicide2, false, m_farm, m_field);
			break;
		}
		break;
	case nl_tu_herbicide2:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_herbicide2, true, m_farm, m_field);
			break;
		}
		// End of thread
		break;
	case nl_tu_fungicide1:
		// Here comes the fungicide thread
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide1, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide1, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide1:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide2, false, m_farm, m_field);
		break;
	case nl_tu_fungicide2:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide2, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide2, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide2:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide3, false, m_farm, m_field);
		break;
	case nl_tu_fungicide3:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide3, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide3, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide3:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide3, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		SimpleEvent_(d1, nl_tu_fungicide4, false, m_farm, m_field);
		break;
	case nl_tu_fungicide4:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide4, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide4, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide4:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide4, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide5, false, m_farm, m_field);
		break;
	case nl_tu_fungicide5:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide5, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide5, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide5:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide5, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide6, false, m_farm, m_field);
		break;
	case nl_tu_fungicide6:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide6, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide6, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide6:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide6, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
		}
		SimpleEvent_(d1, nl_tu_fungicide7, false, m_farm, m_field);
		break;
	case nl_tu_fungicide7:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide7, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide7, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide7:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide7, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide8, false, m_farm, m_field);
		break;
	case nl_tu_fungicide8:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide8, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide8, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide8:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide8, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide9, false, m_farm, m_field);
		break;
	case nl_tu_fungicide9:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide9, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide9, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide9:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide9, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 3, nl_tu_fungicide10, false, m_farm, m_field);
		break;
	case nl_tu_fungicide10:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_fungicide10, true, m_farm, m_field);
			break;
		}
		NL_TU_FUNGI_SPRAY_DATE = g_date->Date();
		SimpleEvent_(g_date->Date(), nl_tu_added_insecticide10, false, m_farm, m_field);
		break;
	case nl_tu_added_insecticide10:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_tulips_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_added_insecticide10, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 6);
		}
		SimpleEvent_(d1, nl_tu_harvest, false, m_farm, m_field);		break;

	case nl_tu_insecticide1:
		if (NL_TU_FUNGI_SPRAY_DATE >= g_date->Date() - 2) { // Should by at least 3 days after fungicide + insecticide
			SimpleEvent_(g_date->Date() + 1, nl_tu_insecticide1, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
			{
				// here we check wheter we are using ERA pesticide or not
				if (!cfg_pest_tulips_on.value() ||
					!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
						SimpleEvent_(g_date->Date() + 1, nl_tu_insecticide1, true, m_farm, m_field);
						break;
					}
				}
				else {
					m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
				}
			}
			d1 = g_date->Date() + 7;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
			}
			SimpleEvent_(d1, nl_tu_insecticide2, false, m_farm, m_field);
			break;
		}
		break;
	case nl_tu_insecticide2:
		if (NL_TU_FUNGI_SPRAY_DATE >= g_date->Date() - 2) { // Should by at least 3 days after fungicide + insecticide
			SimpleEvent_(g_date->Date() + 1, nl_tu_insecticide2, false, m_farm, m_field);
		}
		else
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_tulips_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_tu_insecticide2, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
	case nl_tu_irrigation:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25))
		{
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_tu_irrigation, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_tu_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->BulbHarvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_harvest, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date();
		if (d1 <= g_date->OldDays() + g_date->DayInYear(1, 7)) {
			SimpleEvent_(d1, nl_tu_sleep_all_day, false, m_farm, m_field);
			break;
		}
		else {
			done = true;
			break;
		}
		break;
	case nl_tu_sleep_all_day:
		if (!m_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_tu_sleep_all_day, true, m_farm, m_field);
			break;
		}
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLTulips::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}