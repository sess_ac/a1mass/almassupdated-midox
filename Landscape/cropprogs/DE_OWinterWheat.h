//
// DE_OWinterWheat.h
//
/* 
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DE_OWINTERWHEAT_H
#define DE_OWINTERWHEAT_H

#define DE_OWINTERWHEAT_BASE 38200

typedef enum {
    de_oww_start = 1, // Compulsory, must always be 1 (one).
    de_oww_sleep_all_day = DE_OWINTERWHEAT_BASE,
    de_oww_ferti_s1,
    de_oww_ferti_s2,
    de_oww_ferti_s3,
    de_oww_ferti_p1,
    de_oww_ferti_p2,
    de_oww_autumn_plough,
    de_oww_autumn_harrow,
    de_oww_autumn_sow,
    de_oww_strigling1,
    de_oww_strigling2,
    de_oww_strigling3,
    de_oww_spring_roll1,
    de_oww_harvest,
    de_oww_hay_turning,
    de_oww_straw_chopping,
    de_oww_hay_bailing,
    de_oww_stubble_harrow1,
    de_oww_stubble_harrow2,
    de_oww_deep_plough,
    de_oww_foobar,
} DE_OWinterWheatToDo;


class DE_OWinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_OWinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
     m_first_date=g_date->DayInYear(30,9);
     SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
       const int elements = 2 + (de_oww_foobar - DE_OWINTERWHEAT_BASE);
       m_base_elements_no = DE_OWINTERWHEAT_BASE - 2;

       FarmManagementCategory catlist[elements] =
       {
            fmc_Others,	// zero element unused but must be here	
            fmc_Others,	//	    de_oww_start = 1, // Compulsory, must always be 1 (one).
            fmc_Others,	//	    de_oww_sleep_all_day = DE_OWINTERWHEAT_BASE,
            fmc_Fertilizer,	//	    de_oww_ferti_s1,
            fmc_Fertilizer,	//	    de_oww_ferti_s2,
            fmc_Fertilizer,	//	    de_oww_ferti_s3,
            fmc_Fertilizer,	//	    de_oww_ferti_p1,
            fmc_Fertilizer,	//	    de_oww_ferti_p2,
            fmc_Cultivation,	//	    de_oww_autumn_plough,
            fmc_Cultivation,	//	    de_oww_autumn_harrow,
            fmc_Others,	//	    de_oww_autumn_sow,
            fmc_Cultivation,	//	    de_oww_strigling1,
            fmc_Cultivation,	//	    de_oww_strigling2,
            fmc_Cultivation,	//	    de_oww_strigling3,
            fmc_Cultivation,	//	    de_oww_spring_roll1,
            fmc_Harvest,	//	    de_oww_harvest,
            fmc_Others,	//	    de_oww_hay_turning,
            fmc_Cutting,	//	    de_oww_straw_chopping,
            fmc_Others,	//	    de_oww_hay_bailing,
            fmc_Cultivation,	//	    de_oww_stubble_harrow1,
            fmc_Cultivation,	//	    de_oww_stubble_harrow2,
            fmc_Cultivation,	//	    de_oww_deep_plough,


               // no foobar entry	

       };
       // Iterate over the catlist elements and copy them to vector				
       copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DE_OWINTERWHEAT_H
