/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>BECatchPeaCrop.cpp This file contains the source for the BECatchPeaCrop class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// BECatchPeaCrop.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/BECatchPeaCrop.h"

extern CfgBool  g_farm_fixed_crop_enable;

/**
\brief
The one and oBEy method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional CatchPeaCrop.
*/
bool BECatchPeaCrop::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_BECatchPeaCrop; // The current type - change to match the crop you have
	/**********************************************To Here *************************************************************************/
	
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case BE_cpc_start:
	{
		a_field->ClearManagementActionSum();

		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		// OK, let's go.
		SimpleEvent_(d1, BE_cpc_stubble_cultivator, false, a_farm, a_field);
	}
	break;

	// This is the first real farm operation
	case BE_cpc_stubble_cultivator:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_cpc_stubble_cultivator, true, a_farm, a_field);
			break;
		}
		if (a_field->GetSoilType() <= 5 || a_field->GetSoilType() == 7) { // on sandy soils (BE ZAND & LOSS)
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 5, BE_cpc_ferti_s1_sand, false, a_farm, a_field);
			}
			else {
				SimpleEvent_(g_date->Date() + 5, BE_cpc_ferti_p1_sand, false, a_farm, a_field);
			}
			break;
		}
		else {
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 5, BE_cpc_ferti_s1_clay, false, a_farm, a_field);
			}
			else {
				SimpleEvent_(g_date->Date() + 5, BE_cpc_ferti_p1_clay, false, a_farm, a_field);
			}
			break;
		}
		break;
	case BE_cpc_ferti_p1_sand:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.15))
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_cpc_ferti_p1_sand, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_cpc_preseeding_cultivator_with_sow, false, a_farm, a_field);
		break;
	case BE_cpc_ferti_s1_sand:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.15))
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_cpc_ferti_s1_sand, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, BE_cpc_preseeding_cultivator_with_sow, false, a_farm, a_field);
		break;
	case BE_cpc_ferti_p1_clay:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_cpc_ferti_p1_clay, true, a_farm, a_field);
				break;
			}
		SimpleEvent_(g_date->Date() + 1, BE_cpc_preseeding_cultivator_with_sow, false, a_farm, a_field);
		break;
	case BE_cpc_ferti_s1_clay:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, BE_cpc_ferti_s1_clay, true, a_farm, a_field);
				break;
			}
		SimpleEvent_(g_date->Date() + 1, BE_cpc_preseeding_cultivator_with_sow, false, a_farm, a_field);
		break;
	case BE_cpc_preseeding_cultivator_with_sow:
		if (!a_farm->PreseedingCultivatorSow(a_field, 0.0, g_date->DayInYear(16, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_cpc_preseeding_cultivator_with_sow, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		}
		if (a_field->GetSoilType() <= 5 || a_field->GetSoilType() == 7) { // on sandy soils (BE ZAND & LOSS)
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, BE_cpc_sleep_all_day, false, a_farm, a_field);
		}
		else {
			SimpleEvent_(d1, BE_cpc_winter_plough_clay, false, a_farm, a_field);
		}
		break;
	case BE_cpc_winter_plough_clay:
		if (!a_farm->WinterPlough(a_field, 0.0, g_date->DayInYear(15, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_cpc_winter_plough_clay, true, a_farm, a_field);
			break;
		}
		// The plan is finished on clay soils (catch crop in incorporated into soil on autumn)
		// Calling sleep_all_day to move to the next year 
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, BE_cpc_sleep_all_day, false, a_farm, a_field);
		break;
	case BE_cpc_sleep_all_day:
		if (!a_farm->SleepAllDay(a_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, BE_cpc_sleep_all_day, true, a_farm, a_field);
			break;
		}
		done = true;
		break;


	default:
		g_msg->Warn(WARN_BUG, "BECatchPeaCrop::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}