/**
\file
\brief
<B>DK_OWinterFodderGrass.h This file contains the headers for the DK_OWinterFodderGrass class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_OWinterFodderGrass.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OWinterFodderGrass_H
#define DK_OWinterFodderGrass_H

#define DK_OWFG_BASE 67700
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing OWinterFodderGrass, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_owfg_start = 1, // Compulsory, must always be 1 (one).
	dk_owfg_sleep_all_day = DK_OWFG_BASE,
	dk_owfg_harrow,
	dk_owfg_autumn_plough,
	dk_owfg_sow,
	dk_owfg_water1,
	dk_owfg_water2,
	dk_owfg_manure1_s,
	dk_owfg_ferti_sk_s,
	dk_owfg_manure1_p,
	dk_owfg_ferti_sk_p,
	dk_owfg_cutting1,
	dk_owfg_grazing1,
	dk_owfg_cattle_is_out1,
	dk_owfg_grazing2,
	dk_owfg_cattle_is_out2,
	dk_owfg_manure2_s,
	dk_owfg_manure2_p,
	dk_owfg_cutting2,
	dk_owfg_cutting3,
	dk_owfg_cutting4,
	dk_owfg_cutting5,
	dk_owfg_foobar,

} DK_OWinterFodderGrassToDo;


/**
\brief
DK_OWinterFodderGrass class
\n
*/
/**
See OWinterFodderGrass.h::OWinterFodderGrassToDo for a complete list of all possible events triggered codes by the OWinterFodderGrass management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OWinterFodderGrass : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OWinterFodderGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(17, 8);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_owfg_foobar - DK_OWFG_BASE);
		m_base_elements_no = DK_OWFG_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_owfg_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_owfg_sleep_all_day = DK_owfg_BASE,
			fmc_Cultivation,	//	dk_owfg_harrow,
			fmc_Cultivation,	//	dk_owfg_autumn_plough,
			fmc_Others,	//	dk_owfg_sow,
			fmc_Watering,	//	dk_owfg_water1,
			fmc_Watering,	//	dk_owfg_water2,
			fmc_Fertilizer,	//	dk_owfg_manure1_s,
			fmc_Fertilizer,	//	dk_owfg_ferti_sk_s,
			fmc_Fertilizer,	//	dk_owfg_manure1_p,
			fmc_Fertilizer,	//	dk_owfg_ferti_sk_p,
			fmc_Cutting,	//	dk_owfg_cutting1,
			fmc_Grazing,	//	dk_owfg_grazing,
			fmc_Grazing,	//	dk_owfg_cattle_is_out,
			fmc_Grazing,	//	dk_owfg_grazing,
			fmc_Grazing,	//	dk_owfg_cattle_is_out,
			fmc_Fertilizer,	//	dk_owfg_manure2_s,
			fmc_Fertilizer,	//	dk_owfg_manure2_p,
			fmc_Cutting,	//	dk_owfg_cutting2,
			fmc_Cutting,	//	dk_owfg_cutting3,
			fmc_Cutting,	//	dk_owfg_cutting4,
			fmc_Cutting	//	dk_owfg_cutting4,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_OWinterFodderGrass_H