/**
\file
\brief
<B>DE_OOrchard.h This file contains the headers for the DE_OOrchard class</B> \n
*/
/**
\file 
 by Chris J. Topping and Elzbieta Ziolkowska - modified by Luna Kondrup Marcussen \n
 Version of November 2022 \n
 All rights reserved. \n
 \n
*/
//
// DE_OOrchard.
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

// EZ: This plan is the same as for conventional orchard but without any pesticides

#ifndef DE_OORCH_H
#define DE_OORCH_H

#define DE_OORCH_BASE 39600
/**
\brief A flag used to indicate autumn ploughing status
*/



/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_oorch_start = 1, // Compulsory, must always be 1 (one).
	de_oorch_harvest = DE_OORCH_BASE,
	de_oorch_ferti_p1,
	de_oorch_ferti_s1,
	de_oorch_ferti_p2,
	de_oorch_ferti_s2,
	de_oorch_ferti_p3,
	de_oorch_ferti_s3,
	de_oorch_ferti_p4,
	de_oorch_ferti_s4,
	de_oorch_ferti_p5,
	de_oorch_ferti_s5,
	de_oorch_ferti_p6,
	de_oorch_ferti_s6,
	de_oorch_ferti_p7,
	de_oorch_ferti_s7,
	de_oorch_ferti_p8,
	de_oorch_ferti_s8,
	de_oorch_ferti_p9,
	de_oorch_ferti_s9,
	de_oorch_fungicide1,
	de_oorch_fungicide2,
	de_oorch_fungicide3,
	de_oorch_fungicide4,
	de_oorch_fungicide5,
	de_oorch_fungicide6,
	de_oorch_fungicide7,
	de_oorch_fungicide8,
	de_oorch_fungicide9,
	de_oorch_fungicide10,
	de_oorch_fungicide11,
	de_oorch_fungicide12,
	de_oorch_fungicide13,
	de_oorch_fungicide14,
	de_oorch_fungicide15,
	de_oorch_fungicide16,
	de_oorch_fungicide17,
	de_oorch_fungicide18,
	de_oorch_fungicide19,
	de_oorch_fungicide20,
	de_oorch_fungicide21,
	de_oorch_fungicide22,
	de_oorch_pheromone,
	de_oorch_insecticide1,
	de_oorch_insecticide2,
	de_oorch_insecticide3,
	de_oorch_insecticide4,
	de_oorch_insecticide5,
	de_oorch_insecticide6,
	de_oorch_insecticide7,
	de_oorch_insecticide8,
	de_oorch_insecticide9,
	de_oorch_insecticide10,
	de_oorch_insecticide11,
	de_oorch_row_cultivation1,
	de_oorch_row_cultivation2,
	de_oorch_row_cultivation3,
	de_oorch_row_cultivation4,
	de_oorch_row_cultivation5,
	de_oorch_row_cultivation6,
	de_oorch_manual_weeding1,
	de_oorch_manual_weeding2,
	de_oorch_foobar, // Obligatory, must be last
} DE_OORCHToDo;


/**
\brief
DE_OOrchard class
\n
*/
/**
See DE_OOrchard.h::DE_OOrchardToDo for a complete list of all possible events triggered codes by the Spring Rye management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_OOrchard : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_OOrchard(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date = g_date->DayInYear(30, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_oorch_foobar - DE_OORCH_BASE);
		m_base_elements_no = DE_OORCH_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others,	// zero element unused but must be here
			 // ALL THE NECESSARY ENTRIES HERE
			 fmc_Others,			//	de_oorch_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Harvest,			//	de_oorch_harvest
			 fmc_Fertilizer,		//	de_oorch_ferti_p1,
			 fmc_Fertilizer,		//	de_oorch_ferti_s1,
			 fmc_Fertilizer,		//	de_oorch_ferti_p2,
			 fmc_Fertilizer,		//	de_oorch_ferti_s2,
			 fmc_Fertilizer,		//	de_oorch_ferti_p3,
			 fmc_Fertilizer,		//	de_oorch_ferti_s3,
			 fmc_Fertilizer,		//	de_oorch_ferti_p4,
			 fmc_Fertilizer,		//	de_oorch_ferti_s4,
			 fmc_Fertilizer,		//	de_oorch_ferti_p5,
			 fmc_Fertilizer,		//	de_oorch_ferti_s5,
			 fmc_Fertilizer,		//	de_oorch_ferti_p,
			 fmc_Fertilizer,		//	de_oorch_ferti_s,
			 fmc_Fertilizer,		//	de_oorch_ferti_p,
			 fmc_Fertilizer,		//	de_oorch_ferti_s,
			 fmc_Fertilizer,		//	de_oorch_ferti_p,
			 fmc_Fertilizer,		//	de_oorch_ferti_s,
			 fmc_Fertilizer,		//	de_oorch_ferti_p,
			 fmc_Fertilizer,		//	de_oorch_ferti_s,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Fungicide,			// de_oorch_fungicide,
			 fmc_Others,			// de_oorch_pheromone,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Insecticide,		// de_oorch_insecticide,
			 fmc_Cultivation,		// de_oorch_row_cultivation,
			 fmc_Cultivation,		// de_oorch_row_cultivation,
			 fmc_Cultivation,		// de_oorch_row_cultivation,
			 fmc_Cultivation,		// de_oorch_row_cultivation,
			 fmc_Cultivation,		// de_oorch_row_cultivation,
			 fmc_Cultivation,		// de_oorch_row_cultivation,
			 fmc_Cultivation,		// de_oorch_manual_weeding,
			 fmc_Cultivation,		// de_oorch_manual_weeding,

			// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_OORCH_H

