/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>UKPotatoes.cpp This file contains the source for the UKPotatoes class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
* * * Adapted for UK Potatoes, 2021, Adam McVeigh, Nottingham Trent University
*/
//
// UKPotatoes.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/UKPotatoes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_potatoes_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_POT_InsecticideDay;
extern CfgInt   cfg_POT_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional potatoes.
*/
bool UKPotatoes::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_UKPotatoes; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case uk_pot_start:
	{
		// uk_pot_start just sets up all the starting conditions and reference dates that are needed to start a uk_pot
		UK_POT_STUBBLE_HARROW = false;

		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(31, 10)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(31, 10));

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - ms_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "UKPotatoes::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "UKPotatoes::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "UKPotatoes::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), uk_pot_spring_plough, false, m_farm, m_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		SimpleEvent_(d1, uk_pot_stubble_harrow, false, m_farm, m_field);
	}
	break;
	// This is the first real farm operation

	// Stubble cultivation / Stubble Harrow 100% September - October
	case uk_pot_stubble_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(2, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_stubble_harrow, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), uk_pot_winter_plough, false, m_farm, m_field);
		break;
		// Winter plough 50% October - December.
	case uk_pot_winter_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_pot_winter_plough, true, m_farm, m_field);
				break;
			}
			if (m_farm->IsStockFarmer()) //Stock Farmer // setting the next step which is NPK fertiliser application
			{
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3)+365, uk_pot_ferti_s1, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3)+365, uk_pot_ferti_p1, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3) + 365, uk_pot_spring_plough, false, m_farm, m_field);
		break;
	case uk_pot_spring_plough:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_spring_plough, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer // setting the next step which is NPK fertiliser application
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), uk_pot_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), uk_pot_ferti_p1, false, m_farm, m_field);
		break;

		// Slurry Application 100% March-April.
		// Slurry s1 and p1 - leads to s2 or p2
	case uk_pot_ferti_s1:
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_s1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_s2, false, m_farm, m_field);
		break;
	case uk_pot_ferti_p1:
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_p1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_p2, false, m_farm, m_field);
		break;
		// NPK with sow 75% April
	case uk_pot_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_pot_bed_forming, false, m_farm, m_field);
		break;
	case uk_pot_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4), uk_pot_bed_forming, false, m_farm, m_field);
		break;
		// Bed forming / Preseeding cultivation 100% April-May
	case uk_pot_bed_forming:
		if (!m_farm->BedForming(m_field, 0.0, g_date->DayInYear(9, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_bed_forming, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_pot_spring_planting, false, m_farm, m_field);
		break;
		// Planting 100% April - May
	case uk_pot_spring_planting:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_spring_planting, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to five parallel events
		// Hilling, watering and Herbicides is MAIN THREAD. Fertiliser. Fungicide. Insecticide.
		SimpleEvent_(g_date->Date() + 14, uk_pot_hilling1, false, m_farm, m_field); // Hilling + herbicides = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), uk_pot_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), uk_pot_insecticide1, false, m_farm, m_field);	// Insecticide thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), uk_pot_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), uk_pot_ferti_p3, false, m_farm, m_field);
		break;

		// N and MG+S THREAD
		// Here comes N thread
		  // stock farmers thread first
		  // arable farmers thread second

		  //stock farmers N thread
		  // N 75% June
	case uk_pot_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(29, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_s4, false, m_farm, m_field);
		break;
		// MG+S 25% June 
	case uk_pot_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25))
		{
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;

		//arable farmers N thread
		// N 75% June
	case uk_pot_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(29, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_p4, false, m_farm, m_field);
		break;
		// MG+S 25% June
	case uk_pot_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_pot_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;

		// MAIN THREAD
		// HILLING I + herbicides I-II
			// Hilling 100% 2 weeks after planting
	case uk_pot_hilling1:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(24, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_hilling1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_pot_herbicide1, false, m_farm, m_field);
		break;
		// Herbicide1 80% after Hilling
	case uk_pot_herbicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_herbicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, uk_pot_herbicide2, false, m_farm, m_field);
		break;
		// Herbicide 2 50% 2-4 weeks after Herbicide 1
	case uk_pot_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), uk_pot_dessication1, false, m_farm, m_field);
		break;
		// End of thread

		// Fungicide thread I-XIV
				// Fungicide 1 100% June 1-7
	case uk_pot_fungicide1:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide2, false, m_farm, m_field);
		break;
		// Fungicide 2 100% June 8-14
	case uk_pot_fungicide2:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_fungicide2, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide3, false, m_farm, m_field);
		break;
		// Fungicide 3 100% June 15-21
	case uk_pot_fungicide3:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_fungicide3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide4, false, m_farm, m_field);
		break;
		// Fungicide 4 100% June 22-29
	case uk_pot_fungicide4:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(28, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_fungicide4, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide5, false, m_farm, m_field);
		break;
		// Fungicide 5 100% July 1-7
	case uk_pot_fungicide5:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_fungicide5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide6, false, m_farm, m_field);
		break;
		// Fungicide 6 100% July 8-14
	case uk_pot_fungicide6:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_fungicide6, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide7, false, m_farm, m_field);
		break;
		// Fungicide 7 100% July 15-21
	case uk_pot_fungicide7:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_fungicide7, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide8, false, m_farm, m_field);
		break;
		// Fungicide 8 80% July 22-28
	case uk_pot_fungicide8:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(28, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_fungicide8, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide9, false, m_farm, m_field);
		break;
		// Fungicide 9 80% August 1-7
	case uk_pot_fungicide9:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_fungicide9, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide10, false, m_farm, m_field);
		break;
		// Fungicide 10 70% August 8-14
	case uk_pot_fungicide10:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.7))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_fungicide10, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide11, false, m_farm, m_field);
		break;
		// Fungicide 11 70% August 15-21
	case uk_pot_fungicide11:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.7))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_fungicide11, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide12, false, m_farm, m_field);
		break;
		// Fungicide 12 60% August 22-29
	case uk_pot_fungicide12:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.6))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(28, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_fungicide12, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide13, false, m_farm, m_field);
		break;
		// Fungicide 13 60% September 1-7
	case uk_pot_fungicide13:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.6))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_fungicide13, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, uk_pot_fungicide14, false, m_farm, m_field);
		break;
		// Fungicide 14 60% September 8-14
	case uk_pot_fungicide14:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.6))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_pot_fungicide14, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;

		// Insecticide thread I
				// Insecticide 1 60% June
	case uk_pot_insecticide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.6))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_potatoes_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_pot_insecticide1, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
		// BACK TO MAIN THREAD
		// Dessication I-II
				// Dessication 1 100% 14 days before Harvest
	case uk_pot_dessication1:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_dessication1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 4, uk_pot_dessication2, false, m_farm, m_field);
		break;
		// Dessication 2 75% 4-6 days after Dessication 1
	case uk_pot_dessication2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(21, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_pot_dessication2, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date()+8, uk_pot_harvest, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date()+14, uk_pot_harvest, false, m_farm, m_field);
		break;

		// HARVEST
				// Harvest 100% October
	case uk_pot_harvest:
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_pot_harvest, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "UKPotatoes::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}