//
// DK_OPotatoIndustry.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OPotatoIndustry_H
#define DK_OPotatoIndustry_H

#define DK_OPOI_BASE 69500
#define DK_OPOI_FORCESPRING	a_field->m_user[1]
#define DK_OPOI_SANDY	a_field->m_user[2]

typedef enum {
	dk_opoi_start = 1, // Compulsory, start event must always be 1 (one).
	dk_opoi_autumn_remove_straw = DK_OPOI_BASE,
	dk_opoi_spring_remove_straw,
	dk_opoi_autumn_stoneburier,
	dk_opoi_autumn_plough,
	dk_opoi_spring_stoneburier,
	dk_opoi_spring_plough,
	dk_opoi_deep_harrow1,
	dk_opoi_ferti_s,
	dk_opoi_ferti_p,
	dk_opoi_sow,
	dk_opoi_calcium_sand_s,
	dk_opoi_calcium_sand_p,
	dk_opoi_water1_s,
	dk_opoi_water2_s,
	dk_opoi_water3_s,
	dk_opoi_water1_c,
	dk_opoi_water2_c,
	dk_opoi_water3_c,
	dk_opoi_row_cultivation1,
	dk_opoi_hilling_up1,
	dk_opoi_row_cultivation2,
	dk_opoi_hilling_up2,
	dk_opoi_harrow,
	dk_opoi_hilling_up3,
	dk_opoi_row_cultivation3,
	dk_opoi_hilling_up4,
	dk_opoi_top_off,
	dk_opoi_harvest,
	dk_opoi_deep_harrow2,
	dk_opoi_foobar
} DK_OPotatoIndustryToDo;



class DK_OPotatoIndustry : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OPotatoIndustry(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_opoi_foobar - DK_OPOI_BASE);
		m_base_elements_no = DK_OPOI_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_opoi_autumn_remove_straw = DK_OPOI_BASE,
			fmc_Others, // dk_opoi_spring_remove_straw,
			fmc_Cultivation, // dk_opoi_autumn_stoneburier,
			fmc_Cultivation, // dk_opoi_autumn_plough,
			fmc_Cultivation, // dk_opoi_spring_stoneburier,
			fmc_Cultivation, //  dk_opoi_spring_plough,
			fmc_Cultivation, //  dk_opoi_deep_harrow1,
			fmc_Fertilizer, //  dk_opoi_ferti_s,
			fmc_Fertilizer, //  dk_opoi_ferti_p,
			fmc_Others, //  dk_opoi_sow,
			fmc_Fertilizer, //  dk_opoi_calcium_sand_s,
			fmc_Fertilizer, //  dk_opoi_calcium_sand_p,
			fmc_Watering, //  dk_opoi_water1,
			fmc_Watering, //  dk_opoi_water2,
			fmc_Watering, //  dk_opoi_water3,
			fmc_Watering, //  dk_opoi_water1,
			fmc_Watering, //  dk_opoi_water2,
			fmc_Watering, //  dk_opoi_water3,
			fmc_Cultivation, //  dk_opoi_row_cultivation1,
			fmc_Cultivation, //  dk_opoi_hilling_up1,
			fmc_Cultivation, //  dk_opoi_row_cultivation2,
			fmc_Cultivation, //  dk_opoi_hilling_up2,
			fmc_Cultivation, //  dk_opoi_harrow,
			fmc_Cultivation, //  dk_opoi_hilling_up3,
			fmc_Cultivation, //  dk_opoi_row_cultivation3,
			fmc_Cultivation, //  dk_opoi_hilling_up4,
			fmc_Others, //  dk_opoi_top_off,
			fmc_Harvest, //  dk_opoi_harvest,
			fmc_Cultivation, //  dk_opoi_deep_harrow2,
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};
#endif // DK_OPotatoIndustry_H
