/**
\file
\brief
<B>PLFodderLucerne2.h This file contains the headers for the FodderLucerne2 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of January 2018 \n
All rights reserved. \n
\n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// PLFodderLucerne2.h
//


#ifndef PLFODDERLUCERNE2_H
#define PLFODDERLUCERNE2_H

#define PLFODDERLUCERNE2_BASE 25500

/** Below is the list of things that a farmer can do if he is growing fodder lucerne in the first year, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_fl2_start = 1, // Compulsory, must always be 1 (one).
	pl_fl2_sleep_all_day = PLFODDERLUCERNE2_BASE,
	pl_fl2_spring_harrow,
	pl_fl2_ferti_p0,
	pl_fl2_ferti_s0,
	pl_fl2_herbicide1,
	pl_fl2_cut_to_silage1A,
	pl_fl2_harrow1A,
	pl_fl2_ferti_p1A,
	pl_fl2_ferti_s1A,
	pl_fl2_cut_to_silage2A,
	pl_fl2_harrow2A,
	pl_fl2_cut_to_silage3A,
	pl_fl2_cut_to_silage1B,
	pl_fl2_harrow1B,
	pl_fl2_ferti_p1B,
	pl_fl2_ferti_s1B,
	pl_fl2_cut_to_silage2B,
	pl_fl2_harrow2B,
	pl_fl2_cut_to_silage3B,
	pl_fl2_harrow3B,
	pl_fl2_cut_to_silage4B,
	pl_fl2_foobar,
} PLFodderLucerne2ToDo;


/**
\brief
PLFodderLucerne2 class
\n
*/
/**
See PLFodderLucerne2.h::PLFodderLucerne2ToDo for a complete list of all possible events triggered codes by the fodder lucerne1 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLFodderLucerne2 : public Crop
{
public:
	virtual bool  Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev);
	PLFodderLucerne2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 31th March
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(31, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (pl_fl2_foobar - PLFODDERLUCERNE2_BASE);
		m_base_elements_no = PLFODDERLUCERNE2_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
				fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_fl2_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_fl2_sleep_all_day = PLFODDERLUCERNE2_BASE,
			fmc_Cultivation,//pl_fl2_spring_harrow,
			fmc_Fertilizer,//pl_fl2_ferti_p0,
			fmc_Fertilizer,//pl_fl2_ferti_s0,
			fmc_Herbicide,//pl_fl2_herbicide1,
			fmc_Cutting,//pl_fl2_cut_to_silage1A,
			fmc_Cultivation,//pl_fl2_harrow1A,
			fmc_Fertilizer,//pl_fl2_ferti_p1A,
			fmc_Fertilizer,//pl_fl2_ferti_s1A,
			fmc_Cutting,//pl_fl2_cut_to_silage2A,
			fmc_Cultivation,//pl_fl2_harrow2A,
			fmc_Cutting,//pl_fl2_cut_to_silage3A,
			fmc_Cutting,//pl_fl2_cut_to_silage1B,
			fmc_Cultivation,//pl_fl2_harrow1B,
			fmc_Fertilizer,//pl_fl2_ferti_p1B,
			fmc_Fertilizer,//pl_fl2_ferti_s1B,
			fmc_Cutting,//pl_fl2_cut_to_silage2B,
			fmc_Cultivation,//pl_fl2_harrow2B,
			fmc_Cutting,//pl_fl2_cut_to_silage3B,
			fmc_Cultivation,//pl_fl2_harrow3B,
			fmc_Cutting//pl_fl2_cut_to_silage4B,y

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // PLFODDERLUCERNE2_H

