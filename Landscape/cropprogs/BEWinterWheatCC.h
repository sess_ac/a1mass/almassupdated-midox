/**
\file
\brief
<B>BEWinterWheatCC.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BEWinterWheatCC.h
//


#ifndef BEWinterWheatCC_H
#define BEWinterWheatCC_H

#define BEWinterWheatCC_BASE 25400
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_wwcc_start = 1, // Compulsory, must always be 1 (one).
	BE_wwcc_sleep_all_day = BEWinterWheatCC_BASE,
	BE_wwcc_stubble_cultivator,
	BE_wwcc_herbicide1,
	BE_wwcc_mole_plough,
	BE_wwcc_autumn_plough,
	BE_wwcc_preseeding_cultivator_sow,
	BE_wwcc_herbicide2,
	BE_wwcc_herbicide3,
	BE_wwcc_ferti_p1, // slurry
	BE_wwcc_ferti_s1,
	BE_wwcc_ferti_p3, // NI
	BE_wwcc_ferti_s3,
	BE_wwcc_ferti_p4, // NII
	BE_wwcc_ferti_s4,
	BE_wwcc_ferti_p5, // NIII
	BE_wwcc_ferti_s5,
	BE_wwcc_herbicide4, 
	BE_wwcc_fungicide1,
	BE_wwcc_fungicide2,
	BE_wwcc_fungicide3,
	BE_wwcc_insecticide2,
	BE_wwcc_growth_regulator1,
	BE_wwcc_growth_regulator2,
	BE_wwcc_harvest,
	BE_wwcc_straw_chopping,
	BE_wwcc_hay_bailing,
} BEWinterWheatCC1ToDo;


/**
\brief
BEWinterWheatCC class
\n
*/
/**
See BEWinterWheatCC.h::BEWinterWheatCCToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEWinterWheatCC: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEWinterWheatCC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,10 );
   }
};

#endif // BEWinterWheatCC_H

