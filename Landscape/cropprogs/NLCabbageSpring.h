/**
\file
\brief
<B>NLCabbageSpring.h This file contains the headers for the CabbageSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLCabbageSpring.h
//


#ifndef NLCABBAGESPRING_H
#define NLCABBAGESPRING_H

#define NLCABBAGESPRING_BASE 20500
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_CABS_WINTER_PLOUGH	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_cabs_start = 1, // Compulsory, must always be 1 (one).
	nl_cabs_sleep_all_day = NLCABBAGESPRING_BASE,
	nl_cabs_spring_plough_sandy,
	nl_cabs_ferti_p1,
	nl_cabs_ferti_s1,
	nl_cabs_preseeding_cultivator,
	nl_cabs_spring_planting,
	nl_cabs_weeding1,
	nl_cabs_herbicide1,
	nl_cabs_weeding2,
	nl_cabs_fungicide1,
	nl_cabs_fungicide2,
	nl_cabs_fungicide3,
	nl_cabs_insecticide1,
	nl_cabs_insecticide2,
	nl_cabs_insecticide3,
	nl_cabs_ferti_p2,
	nl_cabs_ferti_s2,
	nl_cabs_ferti_p3,
	nl_cabs_ferti_s3,
	nl_cabs_ferti_p4,
	nl_cabs_ferti_s4,
	nl_cabs_watering,
	nl_cabs_harvest,
	nl_cabs_foobar
} NLCabbageSpringToDo;


/**
\brief
NLCabbageSpring class
\n
*/
/**
See NLCabbageSpring.h::NLCabbageSpringToDo for a complete list of all possible events triggered codes by the cabbage management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLCabbageSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLCabbageSpring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,4 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_cabs_foobar - NLCABBAGESPRING_BASE);
	   m_base_elements_no = NLCABBAGESPRING_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,//nl_cabs_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_cabs_sleep_all_day = NLCABBAGESPRING_BASE,
			fmc_Cultivation,//nl_cabs_spring_plough_sandy,
			fmc_Fertilizer,//nl_cabs_ferti_p1,
			fmc_Fertilizer,//nl_cabs_ferti_s1,
			fmc_Cultivation,//nl_cabs_preseeding_cultivator,
			fmc_Others,//nl_cabs_spring_planting,
			fmc_Cutting,//nl_cabs_weeding1,
			fmc_Herbicide,//nl_cabs_herbicide1,
			fmc_Cutting,//nl_cabs_weeding2,
			fmc_Fungicide,//nl_cabs_fungicide1,
			fmc_Fungicide,//nl_cabs_fungicide2,
			fmc_Fungicide,//nl_cabs_fungicide3,
			fmc_Insecticide,//nl_cabs_insecticide1,
			fmc_Insecticide,//nl_cabs_insecticide2,
			fmc_Insecticide,//nl_cabs_insecticide3,
			fmc_Fertilizer,//nl_cabs_ferti_p2,
			fmc_Fertilizer,//nl_cabs_ferti_s2,
			fmc_Fertilizer,//nl_cabs_ferti_p3,
			fmc_Fertilizer,//nl_cabs_ferti_s3,
			fmc_Fertilizer,//nl_cabs_ferti_p4,
			fmc_Fertilizer,//nl_cabs_ferti_s4,
			fmc_Watering,//nl_cabs_watering,
			fmc_Harvest//nl_cabs_harvest,
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLCABBAGESPRING_H

