/**
\file
\brief
<B>NLGrassGrazed1Spring.h This file contains the headers for the TemporalGrassGrazed1Spring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLGrassGrazed1Spring.h
//


#ifndef NLGRASSGRAZED1SPRING_H
#define NLGRASSGRAZED1SPRING_H

#define NLGRASSGRAZED1SPRING_BASE 21000
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_GG1S_CUT_DATE		a_field->m_user[1]
#define NL_GG1S_WATER_DATE		a_field->m_user[2]


/** Below is the list of things that a farmer can do if he is growing TemporalGrassGrazed1Spring, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_gg1s_start = 1, // Compulsory, must always be 1 (one).
	nl_gg1s_sleep_all_day = NLGRASSGRAZED1SPRING_BASE,
	nl_gg1s_preseeding_cultivator,
	nl_gg1s_spring_sow,
	nl_gg1s_ferti_p2,
	nl_gg1s_ferti_s2,
	nl_gg1s_cut_to_silage1,
	nl_gg1s_cut_to_silage2,
	nl_gg1s_cut_to_silage3,
	nl_gg1s_cut_to_silage4,
	nl_gg1s_cut_to_silage5,
	nl_gg1s_cut_to_silage6,
	nl_gg1s_ferti_p3,
	nl_gg1s_ferti_s3,
	nl_gg1s_ferti_p4,
	nl_gg1s_ferti_s4,
	nl_gg1s_ferti_p5,
	nl_gg1s_ferti_s5,
	nl_gg1s_ferti_p6,
	nl_gg1s_ferti_s6,
	nl_gg1s_ferti_p7,
	nl_gg1s_ferti_s7,
	nl_gg1s_ferti_p8,
	nl_gg1s_ferti_s8,
	nl_gg1s_ferti_p9,
	nl_gg1s_ferti_s9,
	nl_gg1s_ferti_p10,
	nl_gg1s_ferti_s10,
	nl_gg1s_ferti_p11,
	nl_gg1s_ferti_s11,
	nl_gg1s_ferti_p12,
	nl_gg1s_ferti_s12,
	nl_gg1s_watering,
	nl_gg1s_cattle_out,
	nl_gg1s_cattle_is_out,
	nl_gg1s_foobar
} NLGrassGrazed1SpringToDo;


/**
\brief
NLGrassGrazed1Spring class
\n
*/
/**
See NLGrassGrazed1Spring.h::NLGrassGrazed1SpringToDo for a complete list of all possible events triggered codes by the TemporalGrassGrazed1Spring management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLGrassGrazed1Spring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLGrassGrazed1Spring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 20,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_gg1s_foobar - NLGRASSGRAZED1SPRING_BASE);
	   m_base_elements_no = NLGRASSGRAZED1SPRING_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//nl_gg1s_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_gg1s_sleep_all_day = NLGRASSGRAZED1SPRING_BASE,
			fmc_Cultivation,//nl_gg1s_preseeding_cultivator,
			fmc_Others,//nl_gg1s_spring_sow,
			fmc_Fertilizer,//nl_gg1s_ferti_p2,
			fmc_Fertilizer,//nl_gg1s_ferti_s2,
			fmc_Cutting,//nl_gg1s_cut_to_silage1,
			fmc_Cutting,//nl_gg1s_cut_to_silage2,
			fmc_Cutting,//nl_gg1s_cut_to_silage3,
			fmc_Cutting,//nl_gg1s_cut_to_silage4,
			fmc_Cutting,//nl_gg1s_cut_to_silage5,
			fmc_Cutting,//nl_gg1s_cut_to_silage6,
			fmc_Fertilizer,//nl_gg1s_ferti_p3,
			fmc_Fertilizer,//nl_gg1s_ferti_s3,
			fmc_Fertilizer,//nl_gg1s_ferti_p4,
			fmc_Fertilizer,//nl_gg1s_ferti_s4,
			fmc_Fertilizer,//nl_gg1s_ferti_p5,
			fmc_Fertilizer,//nl_gg1s_ferti_s5,
			fmc_Fertilizer,//nl_gg1s_ferti_p6,
			fmc_Fertilizer,//nl_gg1s_ferti_s6,
			fmc_Fertilizer,//nl_gg1s_ferti_p7,
			fmc_Fertilizer,//nl_gg1s_ferti_s7,
			fmc_Fertilizer,//nl_gg1s_ferti_p8,
			fmc_Fertilizer,//nl_gg1s_ferti_s8,
			fmc_Fertilizer,//nl_gg1s_ferti_p9,
			fmc_Fertilizer,//nl_gg1s_ferti_s9,
			fmc_Fertilizer,//nl_gg1s_ferti_p10,
			fmc_Fertilizer,//nl_gg1s_ferti_s10,
			fmc_Fertilizer,//nl_gg1s_ferti_p11,
			fmc_Fertilizer,//nl_gg1s_ferti_s11,
			fmc_Fertilizer,//nl_gg1s_ferti_p12,
			fmc_Fertilizer,//nl_gg1s_ferti_s12,
			fmc_Watering,//nl_gg1s_watering,
			fmc_Grazing,//nl_gg1s_cattle_out,
			fmc_Grazing//nl_gg1s_cattle_is_out,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLGRASSGRAZED1SPRING_H

