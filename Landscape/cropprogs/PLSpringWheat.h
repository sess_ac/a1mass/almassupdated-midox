/**
\file
\brief
<B>PLSpringWheat.h This file contains the headers for the SpringWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLSpringWheat.h
//


#ifndef PLSPRINGWHEAT_H
#define PLSPRINGWHEAT_H

#define PLSPRINGWHEAT_BASE 26100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_SW_FERTI_P1	a_field->m_user[1]
#define PL_SW_FERTI_S1	a_field->m_user[2]
#define PL_SW_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_SW_FERTI_P4	a_field->m_user[4]
#define PL_SW_FERTI_S4	a_field->m_user[5]
#define PL_SW_SPRING_FERTI a_field->m_user[6]
#define PL_SW_DECIDE_TO_GR a_field->m_user[7]

/** Below is the list of things that a farmer can do if he is growing spring wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_sw_start = 1, // Compulsory, must always be 1 (one).
	pl_sw_sleep_all_day = PLSPRINGWHEAT_BASE,
	pl_sw_ferti_p1, // 20501
	pl_sw_ferti_s1,
	pl_sw_stubble_plough,
	pl_sw_autumn_harrow1,
	pl_sw_autumn_harrow2,
	pl_sw_stubble_harrow,
	pl_sw_ferti_p2,
	pl_sw_ferti_s2,
	pl_sw_ferti_p3,
	pl_sw_ferti_s3,	// 20510
	pl_sw_winter_plough,
	pl_sw_ferti_p4,
	pl_sw_ferti_s4,
	pl_sw_spring_harrow,
	pl_sw_ferti_p5,
	pl_sw_ferti_s5,
	pl_sw_ferti_p6,
	pl_sw_ferti_s6,
	pl_sw_heavy_cultivator,
	pl_sw_preseeding_cultivator,	// 20520
	pl_sw_preseeding_cultivator_sow,	
	pl_sw_spring_sow,	
	pl_sw_herbicide1,
	pl_sw_fungicide1,
	pl_sw_fungicide2,
	pl_sw_fungicide3,
	pl_sw_insecticide1,
	pl_sw_insecticide2,
	pl_sw_growth_regulator1,
	pl_sw_growth_regulator2,
	pl_sw_ferti_p7,
	pl_sw_ferti_s7,	
	pl_sw_ferti_p8,
	pl_sw_ferti_s8,
	pl_sw_ferti_p9,
	pl_sw_ferti_s9,
	pl_sw_harvest,
	pl_sw_straw_chopping,
	pl_sw_hay_bailing,
	pl_sw_ferti_p10,
	pl_sw_ferti_s10,
	pl_sw_ferti_p11,	
	pl_sw_ferti_s11,
	pl_sw_foobar
} PLSpringWheatToDo;


/**
\brief
PLSpringWheat class
\n
*/
/**
See PLSpringWheat.h::PLSpringWheatToDo for a complete list of all possible events triggered codes by the spring wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLSpringWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLSpringWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_sw_foobar - PLSPRINGWHEAT_BASE);
	   m_base_elements_no = PLSPRINGWHEAT_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_sw_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_sw_sleep_all_day = PLSPRINGWHEAT_BASE,
			fmc_Fertilizer,//pl_sw_ferti_p1, // 20501
			fmc_Fertilizer,//pl_sw_ferti_s1,
			fmc_Cultivation,//pl_sw_stubble_plough,
			fmc_Cultivation,//pl_sw_autumn_harrow1,
			fmc_Cultivation,//pl_sw_autumn_harrow2,
			fmc_Cultivation,//pl_sw_stubble_harrow,
			fmc_Fertilizer,//pl_sw_ferti_p2,
			fmc_Fertilizer,//pl_sw_ferti_s2,
			fmc_Fertilizer,//pl_sw_ferti_p3,
			fmc_Fertilizer,//pl_sw_ferti_s3,	// 20510
			fmc_Cultivation,//pl_sw_winter_plough,
			fmc_Fertilizer,//pl_sw_ferti_p4,
			fmc_Fertilizer,//pl_sw_ferti_s4,
			fmc_Cultivation,//pl_sw_spring_harrow,
			fmc_Fertilizer,//pl_sw_ferti_p5,
			fmc_Fertilizer,//pl_sw_ferti_s5,
			fmc_Fertilizer,//pl_sw_ferti_p6,
			fmc_Fertilizer,//pl_sw_ferti_s6,
			fmc_Cultivation,//pl_sw_heavy_cultivator,
			fmc_Cultivation,//pl_sw_preseeding_cultivator,	// 20520
			fmc_Cultivation,//pl_sw_preseeding_cultivator_sow,
			fmc_Others,//pl_sw_spring_sow,
			fmc_Herbicide,//pl_sw_herbicide1,
			fmc_Fungicide,//pl_sw_fungicide1,
			fmc_Fungicide,//pl_sw_fungicide2,
			fmc_Fungicide,//pl_sw_fungicide3,
			fmc_Insecticide,//pl_sw_insecticide1,
			fmc_Insecticide,//pl_sw_insecticide2,
			fmc_Others,//pl_sw_growth_regulator1,
			fmc_Others,//pl_sw_growth_regulator2,
			fmc_Fertilizer,//pl_sw_ferti_p7,
			fmc_Fertilizer,//pl_sw_ferti_s7,
			fmc_Fertilizer,//pl_sw_ferti_p8,
			fmc_Fertilizer,//pl_sw_ferti_s8,
			fmc_Fertilizer,//pl_sw_ferti_p9,
			fmc_Fertilizer,//pl_sw_ferti_s9,
			fmc_Harvest,//pl_sw_harvest,
			fmc_Others,//pl_sw_straw_chopping,
			fmc_Others,//pl_sw_hay_bailing,
			fmc_Fertilizer,//pl_sw_ferti_p10,
			fmc_Fertilizer,//pl_sw_ferti_s10,
			fmc_Fertilizer,//pl_sw_ferti_p11,
			fmc_Fertilizer//pl_sw_ferti_s11,

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLSPRINGWHEAT_H

