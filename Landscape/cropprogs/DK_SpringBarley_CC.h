//
// DK_SpringBarley_CC.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following dissbaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following dissbaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSBMUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSBMAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSBMUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSBMUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKSpringBarley_CC_H
#define DKSpringBarley_CC_H

#define DK_SBMCC_BASE 161100
#define DK_SBCC_AUTUMPLOUGH	a_field->m_user[1]
#define DK_SBCC_FORCESPRING	a_field->m_user[2]
#define DK_SBCC_TILL_C a_field->m_user[3]
#define DK_SBCC_TILL_S a_field->m_user[4]

typedef enum {
	dk_sbcc_start = 1, // Compulsory, start event must always be 1 (one).
	dk_sbcc_harvest = DK_SBMCC_BASE,
	dk_sbcc_autumn_plough,
	dk_sbcc_ferti_s1,
	dk_sbcc_ferti_p1,
	dk_sbcc_ferti_s2,
	dk_sbcc_ferti_p2,
	dk_sbcc_spring_plough,
	dk_sbcc_spring_harrow_nt,
	dk_sbcc_spring_sow,
	dk_sbcc_herbicide1,
	dk_sbcc_herbicide2,
	dk_sbcc_herbicide3,
	dk_sbcc_herbicide4,
	dk_sbcc_insecticide,
	dk_sbcc_water1,
	dk_sbcc_water2,
	dk_sbcc_fungicide1,
	dk_sbcc_fungicide2,
	dk_sbcc_gr1,
	dk_sbcc_gr2,
	dk_sbcc_straw_chopping,
	dk_sbcc_hay_bailing,
	dk_sbcc_stubble_harrow,
	dk_sbcc_plough,
	dk_sbcc_wait,
	dk_sbcc_foobar,
} DK_SpringBarley_CCToDo;



class DK_SpringBarley_CC : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_SpringBarley_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 11);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_sbcc_foobar - DK_SBMCC_BASE);
		m_base_elements_no = DK_SBMCC_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_sbcc_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_sbcc_harvest = DK_SBMCC_BASE,
			  fmc_Cultivation,	//	  dk_sbcc_autumn_plough,
			  fmc_Fertilizer,	//	  dk_sbcc_ferti_s1,
			  fmc_Fertilizer,	//	  dk_sbcc_ferti_p1,
			  fmc_Fertilizer,	//	  dk_sbcc_ferti_s2,
			  fmc_Fertilizer,	//	  dk_sbcc_ferti_p2,
			  fmc_Cultivation,	//	  dk_sbcc_spring_plough,
			  fmc_Cultivation, // dk_sbcc_spring_harrow_nt,
			  fmc_Cultivation,	//	  dk_sbcc_spring_sow,
			  fmc_Herbicide,	//	  dk_sbcc_herbicide1,
			  fmc_Herbicide,	//	  dk_sbcc_herbicide2,
			  fmc_Herbicide,	//	  dk_sbcc_herbicide3,
			  fmc_Herbicide,	//	  dk_sbcc_herbicide4,
			  fmc_Insecticide,	//	  dk_sbcc_insecticide,
			  fmc_Watering,	//	  dk_sbcc_water1,
			  fmc_Watering,	//	  dk_sbcc_water2,
			  fmc_Fungicide,	//	  dk_sbcc_fungicide1,
			  fmc_Fungicide,	//	  dk_sbcc_fungicide2,
			  fmc_Others, //		dk_sbcc_gr1,
			  fmc_Others, //		dk_sbcc_gr2,
			  fmc_Others,	//	  dk_sbcc_straw_chopping,
			  fmc_Others,	//	  dk_sbcc_hay_bailing,
			  fmc_Cultivation, // dk_sbcc_stubble_harrow,
			  fmc_Cultivation, // dk_sbcc_plough,
			  fmc_Others, // dk_sbcc_wait

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};


#endif // DK_SpringBarley_CC_H