//
// OFieldPeas.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OFieldPeas_H
#define OFieldPeas_H

#define OFPEAS_BASE 2900

typedef enum {
  ofp_start = 1, // Compulsory, start event must always be 1 (one).
  ofp_harvest= OFPEAS_BASE,
  ofp_spring_plough,
  ofp_spring_harrow,
  ofp_spring_roll,
  ofp_spring_sow,
  ofp_strigling1,
  ofp_strigling2,
  ofp_strigling3,
  ofp_strigling4,
  ofp_water1,
  ofp_water2,
  ofp_straw_chopping,
  ofp_foobar,
} OFieldPeasToDo;



class OFieldPeas: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OFieldPeas(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (ofp_foobar - OFPEAS_BASE);
	  m_base_elements_no = OFPEAS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  ofp_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  ofp_harvest= OFPEAS_BASE,
			fmc_Cultivation,	//	  ofp_spring_plough,
			fmc_Cultivation,	//	  ofp_spring_harrow,
			fmc_Cultivation,	//	  ofp_spring_roll,
			fmc_Others,	//	  ofp_spring_sow,
			fmc_Cultivation,	//	  ofp_strigling1,
			fmc_Cultivation,	//	  ofp_strigling2,
			fmc_Cultivation,	//	  ofp_strigling3,
			fmc_Cultivation,	//	  ofp_strigling4,
			fmc_Watering,	//	  ofp_water1,
			fmc_Watering,	//	  ofp_water2,
			fmc_Others	//	  ofp_straw_chopping,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OFieldPeas_H
