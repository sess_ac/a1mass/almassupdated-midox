//
// OLegume_Whole_CC.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Chris J. Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OLegume_Whole_CC.h"

extern CfgFloat cfg_strigling_prop;
extern CfgFloat cfg_DKCatchCropLegumePct;

bool DK_OLegume_Whole_CC::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    int d1;

    bool done = false;
    TTypesOfVegetation l_tov = tov_DKOLegume_Whole_CC;
    int l_nextcropstartdate;

    switch (m_ev->m_todo)
    {
    case dk_olwcc_start:
    {
        a_field->ClearManagementActionSum();
        m_field->SetVegPatchy(true); // LKM: A crop with wide rows, so set patchy
        m_last_date = g_date->DayInYear(25, 8); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(25, 8); // last possible day of plough in this case 
        // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[1][1] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - 


        // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
        int isSpring = 365;
        if (StartUpCrop(isSpring, flexdates, int(dk_olwcc_spring_harrow1))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
        // OK, let's go.
        // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
        SimpleEvent(d1, dk_olwcc_spring_harrow1, false);
        break;
    }
    break;
   
    // LKM: do spring harrow, do it before the 10th of April - if not done, try again +1 day until the 10th of April when we succeed - 100% of farmers do this
    case dk_olwcc_spring_harrow1:
        if (!m_farm->SpringHarrow(m_field, 0.0,
            g_date->DayInYear(10, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_harrow1, true);
            break;
        }
        // LKM: Queue up the next event - spring plough 
        SimpleEvent(g_date->Date() + 3, dk_olwcc_spring_harrow2, false);
        break;
    case dk_olwcc_spring_harrow2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) { // suggests 50% as it is common to harrow 1-2 times
            if (!m_farm->SpringHarrow(m_field, 0.0,
                g_date->DayInYear(14, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_harrow2, true);
                break;
            }
        }
        // LKM: Queue up the next event - spring plough 
        SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_plough, false);
        break;
        // LKM: do spring plough before the 15th of April - if not done, try again +1 day until the 15th of April when we succeed- 100% of farmers do this
    case dk_olwcc_spring_plough:
        if (!m_farm->SpringPlough(m_field, 0.0,
            g_date->DayInYear(15, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_plough, true);
            break;
        }
        // LKM: Queue up the next event - K and S are added (new todo is needed - Maybe work from FP_PK? ks_ferti in system needs to be valid) 
        if (a_farm->IsStockFarmer()) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_ks_ferti_s, false);
            break;
        }
        else SimpleEvent(g_date->Date() + 1, dk_olwcc_ks_ferti_p, false);
        break;
        // LKM: add K and S before the 20th of April - if not done, try again +1 day until the 20th of April when we succeed- 100% of farmers do this
    case dk_olwcc_ks_ferti_s:
        if (!m_farm->FA_SK(m_field, 0.0,
            g_date->DayInYear(20, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_ks_ferti_s, true);
            break;
        }
        // LKM: Queue up the next event - spring harrow just before sowing
        SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_harrow3, false);
        break;

    case dk_olwcc_ks_ferti_p:
        if (!m_farm->FP_SK(m_field, 0.0,
            g_date->DayInYear(20, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_ks_ferti_p, true);
            break;
        }
        // LKM: Queue up the next event - spring harrow just before sowing
        SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_harrow3, false);
        break;
        // LKM: spring harrow only done if difficult to sow because of heavy rain (assume 10% will do this) before the 25th of April - if not done, try again +1 day until the 25th of April when we will succeed
    case dk_olwcc_spring_harrow3:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10))
        {
            if (!m_farm->ShallowHarrow(m_field, 0.0,
                g_date->DayInYear(21, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_harrow3, true);
                break;
            }
        }
        // LKM: Queue up the next event - spring sow 
       // LKM: spring sow is split into 3 different ways - all done before the 30th of April (ans after 1st of April) - if not done, try again +1 day until the 30th of April when we will succeed
        if (m_farm->DoIt_prob(0.38)) // choose a normal spring sow 
        {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_olwcc_spring_sow, false);
        }
        else if (m_farm->DoIt_prob(0.62 / 0.62)) // LKM: choose row sow 
        {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_olwcc_spring_row_sow, false);
        }
        break;
    case dk_olwcc_spring_sow:
        if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_sow, true);
            break;
        }
        SimpleEvent(g_date->Date() + 3, dk_olwcc_strigling1, false);
        break;
        // LKM: strigling before the 5th of May - if not done, try again +3 days until the 5th of May when we succeed
    case dk_olwcc_strigling1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.1))
        {
            if (!m_farm->Strigling(m_field, 0.0,
                g_date->DayInYear(5, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olwcc_strigling1, true);
                break;
            } //LKM: strigling again 14 days after sow to clean field from weeds
            SimpleEvent(g_date->Date() + 14, dk_olwcc_strigling2, false);
            break;
        }
        //LKM: strigling again 14 days after sow to clean field from weeds
        SimpleEvent(g_date->Date() + 14, dk_olwcc_strigling2, false);
        break;
        // LKM: strigling before the 15th of June - if not done, try again +30 days until the 15th of June when we succeed
    case dk_olwcc_strigling2:
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(31, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_strigling2, true);
            break;
        }
        // LKM: Queue up the next event - in this case watering
        SimpleEvent(g_date->Date() + 1, dk_olwcc_water1, false);
        break;
        // LKM: water before the 20th of June - if not done, try again +1 days until the 20th of June when we succeed 
    case dk_olwcc_water1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
            if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olwcc_water1, true);
                break;
            }
        }
        // LKM: Queue up the next event - in this case cutting crop for drying before harvest not before 23rd of June
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(23, 6), dk_olwcc_swathing, false);
        break;

    case dk_olwcc_swathing:
        // LKM: cut for drying before the 23rd of August - if not done, try again +1 days until the 23rd of August when we succeed - 100% of farmers do this
        if (!m_farm->Swathing(m_field, 0.0, g_date->DayInYear(23, 8) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_swathing, true);
            break;
        }
        // LKM: Queue up the next event - harvest (not before the 25th of June - need new farmfunc)
        SimpleEvent(g_date->Date() + 2, dk_olwcc_harvest, false);
        break;
     
        // LKM: the rest  do row sow 
    case dk_olwcc_spring_row_sow:
        if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_spring_row_sow, true);
            break;
        }
        // LKM: Queue up the next event - harrow/strigling 3-5 days after sow 
        SimpleEvent(g_date->Date() + 3, dk_olwcc_strigling3, false);
        break;
    case dk_olwcc_strigling3:
        // LKM: strigling before the 5th of May - if not done, try again +3 days until the 5th of May when we succeed
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(5, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_strigling3, true);
            break;
        } //LKM: Queue up row cultivation 30-45 days after sow to clean field from weeds
        SimpleEvent(g_date->Date() + 14, dk_olwcc_rowcultivation, false);
        break;
    case dk_olwcc_rowcultivation:
        // LKM: row cultivation before the 15th of June - if not done, try again +30 days until the 15th of June when we succeed
        if (!m_farm->RowCultivation(m_field, 0.0,
            g_date->DayInYear(31, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_rowcultivation, true);
            break;
        }
        // LKM: Queue up the next event - in this case watering
        SimpleEvent(g_date->Date() + 1, dk_olwcc_water2, false);
        break;
    case dk_olwcc_water2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
            // LKM: water before the 25th of June - if not done, try again +1 days until the 25th of June when we succeed - 100% of farmers do this
            if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olwcc_water2, true);
                break;
            }
        }
        // LKM: Queue up the next event - harvest (not before the 25th of June - need new farmfunc)
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 6),
            dk_olwcc_harvest, false);
        break;
    case dk_olwcc_harvest:
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olwcc_harvest, true);
            break;
        }
        if (m_ev->m_lock || m_farm->DoIt_prob(cfg_DKCatchCropLegumePct.value())) { // 33%
            SimpleEvent(g_date->Date(), dk_olwcc_wait1, false);
            break;
        }
        else
            d1 = g_date->Date();
        if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_olwcc_wait2, false);
            // Because we are ending harvest before 1.7 so we need to wait until the 1.7
            break;
        }
        else {
            done = true; // end of plan
            break;
        }

    case dk_olwcc_wait1:
        //set to 100% from this code - in reality ~ 33% of all legumes
        // So we are done,but this crop uses a catch crop
        l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
        m_field->SetVegPatchy(false); // reverse the patchy before the next crop
        m_farm->AddNewEvent(tov_DKOCatchCrop, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
        m_field->SetVegType(tov_DKOCatchCrop, tov_Undefined); //  Two vegetation curves are specified 
        if (m_field->GetUnsprayedMarginPolyRef() != -1)
        {
            LE* um = m_OurLandscape->SupplyLEPointer(m_field->GetUnsprayedMarginPolyRef());
            um->SetVegType(tov_DKOCatchCrop, tov_Undefined);
        }
        // NB no "done = true" because this crop effectively continues into the catch crop.
        break;

    case dk_olwcc_wait2:
        done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "OLegume_Whole_CC::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

  return done;
}


