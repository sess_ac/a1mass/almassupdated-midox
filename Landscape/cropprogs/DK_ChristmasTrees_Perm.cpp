/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_ChristmasTrees_Perm.cpp This file contains the source for the DK_ChristmasTrees_Perm class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 \n
*/
//
// DK_ChristmasTrees_Perm.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_ChristmasTrees_Perm.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_ChristmasTrees_Perm_SkScrapes("DK_CROP_CTP_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_ChristmasTrees_Perm_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_CTP_InsecticideDay;
extern CfgInt   cfg_CTP_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_ChristmasTrees_Perm::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKChristmasTrees_Perm;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_ctp_start:
	{
		DK_CTP_YEAR = 0;
		a_field->ClearManagementActionSum();
		m_last_date = g_date->DayInYear(6, 10); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(5, 10); // last possible day of in this case row cultivation autumn (no harvest in this code) 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(6, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) harrow autumn

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(365, flexdates, int(dk_ctp_sleep_all_day))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		// OK, let's go. - EST1 followed by 10 years of growing threes -> EST2 followed by 10 years of growing trees
		if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 0) // establishment year 1
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_sleep_all_day, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 1)// first year after est
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_ferti_sand_s2, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 2 || (DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 3) // 2nd + 3rd after 
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_ferti_sand_s3_4, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 4) //4th after 
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_npk_s5, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 5) // 5th after
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_npk1_s6, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 6 || (DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 7) // 6th + 7th after
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_npk1_s7_8, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 8 || (DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 9) // 8th + 9th after
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_npk1_s9_10, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 10)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(10, 3) + 365;  // harvest year
			SimpleEvent(d1, dk_ctp_npk1_s11, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 11) // second establishment 
		{
			d1 = g_date->OldDays() + g_date->DayInYear(10, 3) + 365;
			SimpleEvent(d1, dk_ctp_crush_trees, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 12)// first year after est
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_ferti_sand_s2, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 13 || (DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 14) // 2nd + 3rd after 
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_ferti_sand_s3_4, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 15) //4th after 
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_npk_s5, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 16) // 5th after
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_npk1_s6, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 17 || (DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 18) // 6th + 7th after
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_npk1_s7_8, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 19 || (DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 20) // 8th + 9th after
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ctp_npk1_s9_10, false);
		}
		else if ((DK_CTP_YEAR + g_date->GetYearNumber()) % 22 == 21)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(10, 3) + 365;  // harvest year
			SimpleEvent(d1, dk_ctp_npk1_s11, false);
		}
		break;

	}
	break; 

	//EST 1
	case dk_ctp_sleep_all_day:
		if (!a_farm->SleepAllDay(a_field, 0.0,
			g_date->DayInYear(19, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_sleep_all_day, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 8), dk_ctp_plough1_autumn, false);
		break;

	case dk_ctp_plough1_autumn:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!a_farm->AutumnPlough(a_field, 0.0,
				g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_plough1_autumn, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp_plough2_autumn, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_depth_plough_autumn, false);
		break; // sandy soils

	case dk_ctp_depth_plough_autumn:
		if (!a_farm->DeepPlough(a_field, 0.0,
			g_date->DayInYear(10, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_depth_plough_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_stubble_harrow_autumn, false);
		break;

	case dk_ctp_plough2_autumn:
		if (!a_farm->AutumnPlough(a_field, 0.0,
			g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_plough2_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_cover_crop_autumn, false);
		break;

	case dk_ctp_stubble_harrow_autumn:
		if (!a_farm->StubbleHarrowing(a_field, 0.0,
			g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_stubble_harrow_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_cover_crop_autumn, false);
		break;

	case dk_ctp_sow_cover_crop_autumn:
		if (!a_farm->AutumnSow(a_field, 0.0,
			g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_sow_cover_crop_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_plant_trees_autumn, false);
		break;
		
	case dk_ctp_plant_trees_autumn:
		if (!a_farm->AutumnSow(a_field, 0.0,
			g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_plant_trees_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_row_cultivation_autumn, false);
		break;

	case dk_ctp_row_cultivation_autumn:
		if (!a_farm->RowCultivation(a_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_row_cultivation_autumn, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_harrow_autumn, false);
		break;

	case dk_ctp_harrow_autumn:
		if (!a_farm->AutumnHarrow(a_field, 0.0, g_date->DayInYear(6, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_harrow_autumn, true);
			break;
		}
		done = true; // end of EST1
		break;

		// EST2
	case  dk_ctp_crush_trees:
		if (!m_farm->Shredding(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_crush_trees, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_herbicide, false);
		break;

	case dk_ctp_herbicide: // some do this (suggest 50%) others sow inter crops
		if (m_ev->m_lock || m_farm->DoIt(50)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0,
				g_date->DayInYear(30, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_sow_catch_crop1, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_ctp_sow_inter_crops, false);
		break;

	case dk_ctp_sow_catch_crop1: // may need changing
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_plant_trees, false);
		break;

	case dk_ctp_sow_inter_crops: // may need changing
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_sow_inter_crops, true);
			break;
		}
		SimpleEvent(g_date->Date() + 50, dk_ctp_harrow, false);
		break;

	case dk_ctp_harrow:
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(29, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_harrow, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_plant_trees, false);
		break;

	case dk_ctp_plant_trees:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_plant_trees, true);
			break;
		}
		done = true; // end of EST2
		break;

		// 1st year after EST
	case dk_ctp_ferti_sand_s2:
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (a_farm->IsStockFarmer()) {
				if (!m_farm->FA_Manure(m_field, 0.0,
					g_date->DayInYear(30, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ctp_ferti_sand_s2, true);
					break;
				}
				SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_2, false);
				break;
			}
			else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp_ferti_sand_p2, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp_ferti_clay_s2, false);
		break; // clay soils

	case dk_ctp_ferti_sand_p2:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_ferti_sand_p2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_2, false);
		break;

	case dk_ctp_ferti_clay_s2:
		if (a_farm->IsStockFarmer()) {
			if (!m_farm->FA_Manure(m_field, 0.0,
				g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_ferti_clay_s2, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_2, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp_ferti_clay_p2, false);
		break;

	case dk_ctp_ferti_clay_p2:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_ferti_clay_p2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_2, false);
		break;

	case dk_ctp_manual_weeding_2:
		if (!m_farm->ManualWeeding(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_2, false);
		break;

	case dk_ctp_herbicide1_2:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide2_2, false);
		break;

	case dk_ctp_herbicide2_2:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide2_2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide3_2, false);
		break;

	case dk_ctp_herbicide3_2:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide3_2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_ctp_wait, false);
		break;

	case dk_ctp_wait:
		// end of 1st year after
		done = true;
		break;

		// year 2nd + 3rd year after
	case dk_ctp_ferti_sand_s3_4:
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (a_farm->IsStockFarmer())
			{
				if (!m_farm->FA_Manure(m_field, 0.0,
					g_date->DayInYear(30, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ctp_ferti_sand_s3_4, true);
					break;
				}
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp_npk_sand1_s3_4, false); // fertilizer thread main thread
				break;
			}
			else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp_ferti_sand_p3_4, false); // fertilizer thread main thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp_ferti_clay_s3_4, false);
		break;


	case dk_ctp_ferti_sand_p3_4:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_ferti_sand_p3_4, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp_npk_sand1_p3_4, false); // fertilizer thread main thread
		break;

	case dk_ctp_ferti_clay_s3_4:
		if (a_farm->IsStockFarmer())
		{
			if (!m_farm->FA_Manure(m_field, 0.0,
				g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_ferti_clay_s3_4, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk_clay_s3_4, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ctp_ferti_clay_p3_4, false);
		break;

	case dk_ctp_ferti_clay_p3_4:
		if (!m_farm->FP_Manure(m_field, 0.0,
			g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_ferti_clay_p3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_3_4, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_grazing_3_4, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_sow_catch_crop2, false);
		break;

	case dk_ctp_npk_clay_s3_4:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk_clay_s3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_3_4, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_grazing_3_4, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_sow_catch_crop2, false);
		break;

	case dk_ctp_npk_sand1_s3_4:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk_sand1_s3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_3_4, false); // weeding / herbicide thread
		SimpleEvent(g_date->Date() + 1, dk_ctp_grazing_3_4, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk_sand2_s3_4, false);
		break;

	case dk_ctp_npk_sand1_p3_4:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk_sand1_p3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_3_4, false); // weeding / herbicide thread
		SimpleEvent(g_date->Date() + 1, dk_ctp_grazing_3_4, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk_sand2_p3_4, false);
		break;

	case dk_ctp_manual_weeding_3_4:
		if (!m_farm->ManualWeeding(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_3_4, false);
		break;

	case dk_ctp_herbicide1_3_4:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide2_3_4, false);
		break;

	case dk_ctp_herbicide2_3_4:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide2_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide3_3_4, false);
		break;

	case dk_ctp_herbicide3_3_4:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide3_3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_3_4, false);
		break;

	case dk_ctp_manual_cutting_3_4: // no specific timing for this trimming event
		if (!m_farm->Pruning(m_field, 0.0,
			g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_3_4, true);
			break;
		}
		break;

		// end of weeding / herbicide thread

	case dk_ctp_grazing_3_4:
		if (m_ev->m_lock || m_farm->DoIt_prob(.10)) { // suggest it is not common
			if (!m_farm->PigsOut(m_field, 0.0,
				g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_grazing_3_4, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp_pig_is_out_3_4, false);
			break;
		}
		break;
	case dk_ctp_pig_is_out_3_4:    // Keep the pigs out there
								 // PigsAreOut() returns false if it is not time to stop grazing
		if (!m_farm->PigsAreOut(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_pig_is_out_3_4, false);
			break;
		}
		break; // end of grazing thread

	case dk_ctp_npk_sand2_s3_4:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk_sand2_s3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop2, false);
		break;

	case dk_ctp_npk_sand2_p3_4:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk_sand2_p3_4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop2, false);
		break;

		// year 5 (after establishment)
	case dk_ctp_npk_s5:
		if (a_farm->IsStockFarmer())
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_npk_s5, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_5, false); // weeding / herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_grazing_5, false); // grazing thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk_sand_s5, false); // fertilizer thread - main thread
			break;
		}// here comes a fork of parallel events:
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp_npk_p5, false);
		break;

	case dk_ctp_npk_p5:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk_p5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_5, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_grazing_5, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk_sand_p5, false); // fertilizer thread - main thread
		break;

	case dk_ctp_manual_weeding_5:
		if (!m_farm->ManualWeeding(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_5, false);
		break;

	case dk_ctp_herbicide1_5:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide2_5, false);
		break;

	case dk_ctp_herbicide2_5:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide2_5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide3_5, false);
		break;

	case dk_ctp_herbicide3_5:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide3_5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_5, false);
		break;

	case dk_ctp_manual_cutting_5: // no specific timing for this trimming event
		if (!m_farm->Pruning(m_field, 0.0,
			g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_5, true);
			break;
		}
		break;

	case dk_ctp_grazing_5:
		if (m_ev->m_lock || m_farm->DoIt_prob(.10)) { // suggest it is not common
			if (!m_farm->PigsOut(m_field, 0.0,
				g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_grazing_5, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp_pig_is_out_5, false);
			break;
		}
		break;
	case dk_ctp_pig_is_out_5:    // Keep the pigs out there
								 // PigsAreOut() returns false if it is not time to stop grazing
		if (!m_farm->PigsAreOut(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_pig_is_out_5, false);
			break;
		}
		break; // end of grazing thread

	case dk_ctp_npk_sand_s5:
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_npk_sand_s5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop2, false);
		break;

	case dk_ctp_npk_sand_p5:
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (!m_farm->FP_NPK(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_npk_sand_p5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop2, false);
		break;

	case dk_ctp_sow_catch_crop2: // changes may be needed
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(16, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 90, dk_ctp_herbicide4_3_4, false);
		break;

	case dk_ctp_herbicide4_3_4:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(16, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide4_3_4, true);
			break;
		}
		//end of year 2nd, 3rd and 4th year after
		done = true;
		break;

		// 5th year after
	case dk_ctp_npk1_s6:
		if (a_farm->IsStockFarmer())
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_npk1_s6, true);
				break;
			}
			// here comes a fork of parallel events:
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_6, false); // weeding / herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_grazing_6, false); // grazing thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk2_s6, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp_npk1_p6, false);
		break;

	case dk_ctp_npk1_p6:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk1_p6, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_6, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_grazing_6, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk2_p6, false);
		break;

	case dk_ctp_npk2_s6:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk2_s6, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_calcium_s6, false);
		break;

	case dk_ctp_npk2_p6:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk2_p6, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_calcium_p6, false);
		break;

	case dk_ctp_calcium_s6:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FA_Calcium(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_calcium_s6, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop3, false);
		break;

	case dk_ctp_calcium_p6:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FP_Calcium(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_calcium_p6, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop3, false);
		break;

	case dk_ctp_grazing_6:
		if (m_ev->m_lock || m_farm->DoIt_prob(.10)) { // suggest it is not common
			if (!m_farm->PigsOut(m_field, 0.0,
				g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_grazing_6, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp_pig_is_out_6, false);
			break;
		}
		break;
	case dk_ctp_pig_is_out_6:    // Keep the pigs out there
								 // PigsAreOut() returns false if it is not time to stop grazing
		if (!m_farm->PigsAreOut(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_pig_is_out_6, false);
			break;
		}
		break; // end of grazing thread

	case dk_ctp_manual_weeding_6:
		if (!m_farm->ManualWeeding(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_6, false);
		break;

	case dk_ctp_herbicide1_6:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide2_6, false);
		break;

	case dk_ctp_herbicide2_6:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide2_6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide3_6, false);
		break;

	case dk_ctp_herbicide3_6:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide3_6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_6, false);
		break;

	case dk_ctp_manual_cutting_6: // no specific timing for this trimming event
		if (!m_farm->Pruning(m_field, 0.0,
			g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_6, true);
			break;
		}
		break;

		// 6th + 7th year after 
	case dk_ctp_npk1_s7_8:
		if (a_farm->IsStockFarmer())
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_npk1_s7_8, true);
				break;
			}
			// here comes a fork of parallel events:
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_7_8, false); // weeding / herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_grazing_7_8, false); // grazing thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk2_s7_8, false); // fertilizer thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp_npk1_p7_8, false);
		break;

	case dk_ctp_npk1_p7_8:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk1_p7_8, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_7_8, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ctp_grazing_7_8, false); // grazing thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk2_p7_8, false); // fertilizer thread
		break;

	case dk_ctp_npk2_s7_8:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk2_s7_8, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_calcium_s7_8, false);
		break;

	case dk_ctp_npk2_p7_8:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk2_p7_8, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_calcium_p7_8, false);
		break;

	case dk_ctp_calcium_s7_8:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FA_Calcium(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_calcium_s7_8, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop3, false);
		break;

	case dk_ctp_calcium_p7_8:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FP_Calcium(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_calcium_p7_8, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop3, false);
		break;

	case dk_ctp_grazing_7_8:
		if (m_ev->m_lock || m_farm->DoIt_prob(.10)) { // suggest it is not common
			if (!m_farm->PigsOut(m_field, 0.0,
				g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_grazing_7_8, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ctp_pig_is_out_7_8, false);
			break;
		}
		break;
	case dk_ctp_pig_is_out_7_8:    // Keep the pigs out there
								 // PigsAreOut() returns false if it is not time to stop grazing
		if (!m_farm->PigsAreOut(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_pig_is_out_7_8, false);
			break;
		}
		break; // end of grazing thread

	case dk_ctp_manual_weeding_7_8:
		if (!m_farm->ManualWeeding(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_7_8, false);
		break;

	case dk_ctp_herbicide1_7_8:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide2_7_8, false);
		break;

	case dk_ctp_herbicide2_7_8:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide2_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide3_7_8, false);
		break;

	case dk_ctp_herbicide3_7_8:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide3_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_7_8, false);
		break;

	case dk_ctp_manual_cutting_7_8: // no specific timing for this trimming event
		if (!m_farm->Pruning(m_field, 0.0,
			g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_7_8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting2_7_8, false);
		break;

	case dk_ctp_manual_cutting2_7_8: // no specific timing for this trimming event (removing faulty trees)
		if (!m_farm->Pruning(m_field, 0.0,
			g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting2_7_8, true);
			break;
		}
		break;

		// 8th + 9th year after
	case dk_ctp_npk1_s9_10:
		if (a_farm->IsStockFarmer())
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_npk1_s9_10, true);
				break;
			}
			// here comes a fork of parallel events:
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_9_10, false); // weeding / herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk2_s9_10, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), dk_ctp_npk1_p9_10, false);
		break;

	case dk_ctp_npk1_p9_10:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk1_p9_10, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_9_10, false); // weeding / herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk2_p9_10, false);
		break;

	case dk_ctp_npk2_s9_10:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk2_s9_10, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_calcium_s9_10, false);
		break;

	case dk_ctp_npk2_p9_10:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk2_p9_10, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_calcium_p9_10, false);
		break;

	case dk_ctp_calcium_s9_10:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FA_Calcium(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_calcium_s9_10, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop3, false);
		break;

	case dk_ctp_calcium_p9_10:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FP_Calcium(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_calcium_p9_10, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop3, false);
		break;

	case dk_ctp_sow_catch_crop3:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(16, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 90, dk_ctp_herbicide4_9_10, false);
		break;

	case dk_ctp_herbicide4_9_10:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(16, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide4_9_10, true);
			break;
		}
		//end 5th-9th year after
		done = true;
		break;

	case dk_ctp_manual_weeding_9_10:
		if (!m_farm->ManualWeeding(m_field, 0.0,
			g_date->DayInYear(16, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_9_10, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_9_10, false);
		break;

	case dk_ctp_herbicide1_9_10:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1_9_10, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide2_9_10, false);
		break;

	case dk_ctp_herbicide2_9_10:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide2_9_10, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide3_9_10, false);
		break;

	case dk_ctp_herbicide3_9_10:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide3_9_10, true);
			break;
		}
		// end of weeding / herbicide thread
		break;

		// year 10+ - harvest year 
	case dk_ctp_npk1_s11:
		if (a_farm->IsStockFarmer())
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_npk1_s11, true);
				break;
			}
			// here comes a fork of parallel events:
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_11, false); // weeding 
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1, false); // herbi thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk2_s11, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_ctp_npk1_p11, false);
		break;

	case dk_ctp_npk1_p11:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk1_p11, true);
			break;
		}
		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_11, false); // weeding
		SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1, false); // herbi thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ctp_npk2_p11, false);
		break;

	case dk_ctp_npk2_s11:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk2_s11, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_calcium_s11, false);
		break;

	case dk_ctp_npk2_p11:
		if (!m_farm->FP_NPK(m_field, 0.0,
			g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_npk2_p11, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_calcium_p11, false);
		break;

	case dk_ctp_calcium_s11:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FA_Calcium(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_calcium_s11, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop_11, false);
		break;

	case dk_ctp_calcium_p11:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (!m_farm->FP_Calcium(m_field, 0.0,
				g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ctp_calcium_p11, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop_11, false);
		break;

	case dk_ctp_sow_catch_crop_11:
		if (!m_farm->AutumnSow(m_field, 0.0,
			g_date->DayInYear(17, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_sow_catch_crop_11, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_ctp_manual_cutting_11, false);
		break;

	case dk_ctp_manual_cutting_11:
		if (!m_farm->Pruning(m_field, 0.0, g_date->DayInYear(25, 12) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_cutting_11, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ctp_harvest, false);
		break;

	case dk_ctp_harvest:
		if (!m_farm->Harvest(m_field, 0.0, g_date->DayInYear(25, 12) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_harvest, true);
			break;
		}
		//end of harvest year - end of Christmas tree management!
		done = true;
		break;

	case dk_ctp_manual_weeding_11:
		if (!m_farm->ManualWeeding(m_field, 0.0,
			g_date->DayInYear(16, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_manual_weeding_11, true);
			break;
		}
		break;

	case dk_ctp_herbicide1:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(17, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide2, false);
		break;

	case dk_ctp_herbicide2:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ctp_herbicide3, false);
		break;

	case dk_ctp_herbicide3:
		if (!m_farm->HerbicideTreat(m_field, 0.0,
			g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ctp_herbicide3, true);
			break;
		}
		// end of weeding / herbicide thread
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		g_msg->Warn(WARN_BUG, "DK_ChristmasTrees_Perm::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}