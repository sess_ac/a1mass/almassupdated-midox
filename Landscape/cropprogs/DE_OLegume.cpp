//
// OLegume_Whole.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Chris J. Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OLegume.h"

extern CfgFloat cfg_strigling_prop;

bool DE_OLegume::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int d2 = 0;
	TTypesOfVegetation l_tov = tov_DEOLegume; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	switch (m_ev->m_todo)
	{
	case de_ol_start:
	{
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(5, 9); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(5, 9); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(5, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_ol_spring_harrow1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		SimpleEvent_(d1, de_ol_spring_harrow1, false, m_farm, m_field);
		break;
	}
	break;

    // LKM: do spring harrow, do it before the 1st of April - if not done, try again +1 day until the 10th of April when we succeed - 100% of farmers do this
    case de_ol_spring_harrow1:
        if (!m_farm->SpringHarrow(m_field, 0.0,
            g_date->DayInYear(15, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_spring_harrow1, true);
            break;
        }
        if (a_farm->IsStockFarmer())
        {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 2), de_ol_ferti_s, false);
            break;
        }
        else SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 2), de_ol_ferti_p, false);
        break;

    case de_ol_ferti_s:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
        {
            if (!m_farm->FA_PKS(m_field, 0.0,
                g_date->DayInYear(20, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, de_ol_ferti_s, true);
                break;
            }
        }
        // LKM: Queue up the next event - spring plough 
        SimpleEvent(g_date->Date() + 1, de_ol_spring_plough, false);
        break;

    case de_ol_ferti_p:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
        {
            if (!m_farm->FP_PKS(m_field, 0.0,
                g_date->DayInYear(20, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, de_ol_ferti_p, true);
                break;
            }
        }
        // LKM: Queue up the next event - spring plough 
        SimpleEvent(g_date->Date() + 1, de_ol_spring_plough, false);
        break;
        
        // LKM: do spring plough before the 15th of April - if not done, try again +1 day until the 15th of April when we succeed- 100% of farmers do this
    case de_ol_spring_plough:
        if (!m_farm->SpringPlough(m_field, 0.0,
            g_date->DayInYear(25, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_spring_plough, true);
            break;
        }
        // LKM: Queue up the next event - spring harrow just before sowing
        SimpleEvent(g_date->Date() + 1, de_ol_spring_harrow2, false);
        break;

        // LKM: spring harrow only done if difficult to sow because of heavy rain (assume 10% will do this) before the 25th of April - if not done, try again +1 day until the 25th of April when we will succeed
    case de_ol_spring_harrow2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10))
        {
            if (!m_farm->ShallowHarrow(m_field, 0.0,
                g_date->DayInYear(30, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, de_ol_spring_harrow2, true);
                break;
            }
        }
        // LKM: Queue up the next event - spring row sow done before the 30th of April (and after 20th of March) - if not done, try again +1 day until the 30th of April when we will succeed
                SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), de_ol_spring_row_sow, false);
                break;

    case de_ol_spring_row_sow:
        if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_spring_row_sow, true);
            break;
        }
        //Queue up harrow/strigling 3-5 days after sow
            SimpleEvent(g_date->Date() + 3, de_ol_strigling, false);
            break;

 // LKM: strigling before the 5th of May - if not done, try again +3 days until the 5th of May when we succeed
    case de_ol_strigling:
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(5, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_strigling, true);
            break;
        } //LKM: Queue up row cultivation 30-45 days after sow to clean field from weeds
        SimpleEvent(g_date->Date() + 30, de_ol_rowcultivation1, false);
        break;

    case de_ol_rowcultivation1:
        if (!m_farm->RowCultivation(m_field, 0.0,
            g_date->DayInYear(5, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, de_ol_rowcultivation1, true);
            break;
        }
        SimpleEvent(g_date->Date() + 30, de_ol_rowcultivation2, false);
        break;
    case de_ol_rowcultivation2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
        {
            if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, de_ol_rowcultivation2, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8),
            de_ol_harvest, false);
        break;
    case de_ol_harvest:
        // LKM: harvest before the 25th of August - if not done, try again +1 days until the 10th of September when we succeed - 100% of farmers do this
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_ol_harvest, true, m_farm, m_field);
            break;
        }
        // End Main Thread
        done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "OLegume::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

  return done;
}


