//
// OCarrots.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OCARROTS_H
#define OCARROTS_H

#define OCARROTS_BASE 2500
#define OCA_WATER_DATE m_field->m_user[0]

typedef enum {
  oca_start = 1, // Compulsory, start event must always be 1 (one).
  oca_fa_slurry = OCARROTS_BASE,
  oca_fp_slurry,
  oca_spring_plough,
  oca_spring_harrow_one,
  oca_spring_harrow_two,
  oca_spring_sow,
  oca_rowcul_one,
  oca_rowcul_two,
  oca_rowcul_three,
  oca_rowcul_four,
  oca_water_one,
  oca_water_two,
  oca_water_three,
  oca_harvest,
  oca_foobar,
} OCarrotsToDo;

class OCarrots: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OCarrots(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (oca_foobar - OCARROTS_BASE);
	  m_base_elements_no = OCARROTS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  oca_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  oca_fa_slurry = OCARROTS_BASE,
			fmc_Fertilizer,	//	  oca_fp_slurry,
			fmc_Cultivation,	//	  oca_spring_plough,
			fmc_Cultivation,	//	  oca_spring_harrow_one,
			fmc_Cultivation,	//	  oca_spring_harrow_two,
			fmc_Others,	//	  oca_spring_sow,
			fmc_Cultivation,	//	  oca_rowcul_one,
			fmc_Cultivation,	//	  oca_rowcul_two,
			fmc_Cultivation,	//	  oca_rowcul_three,
			fmc_Cultivation,	//	  oca_rowcul_four,
			fmc_Watering,	//	  oca_water_one,
			fmc_Watering,	//	  oca_water_two,
			fmc_Watering,	//	  oca_water_three,
			fmc_Harvest	//	  oca_harvest,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OCARROTS_H
