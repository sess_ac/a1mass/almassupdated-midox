/**
\file
\brief
<B>PTMaize.h This file contains the headers for the Maize class</B> \n
*/
/**
\file 
 by Chris J. Topping and Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// PTMaize.h
//


#ifndef PTMAIZE_H
#define PTMAIZE_H

#define PTMAIZE_BASE 32300

/** Below is the list of things that a farmer can do if he is growing PTMaize, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_ma_start = 1, // Compulsory, must always be 1 (one).
	pt_ma_sleep_all_day = PTMAIZE_BASE,
	pt_ma_event1,
	pt_ma_event_spring,
} PTMaizeToDo;


/**
\brief
PTMaize class
\n
*/
/**
See PTMaize.h::PTMaizeToDo for a complete list of all possible events triggered codes by the Maize management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTMaize: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTMaize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of 
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date=g_date->DayInYear( 15,10 ); //EZ: This needs to be changed
   }
};

#endif // PTMAIZE_H

