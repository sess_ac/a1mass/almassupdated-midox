/**
\file
\brief
<B>PLPotatoes.h This file contains the headers for the Potatoes class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLPotatoes.h
//


#ifndef PLPOTATOES_H
#define PLPOTATOES_H

#define PLPOTATOES_BASE 25800
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_POT_FERTI_P1		a_field->m_user[1]
#define PL_POT_FERTI_S1		a_field->m_user[2]
#define PL_POT_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_POT_DID_STRIG1    a_field->m_user[4]
#define PL_POT_DID_STRIG2    a_field->m_user[5]
#define PL_POT_DID_STRIG3    a_field->m_user[6]
#define PL_POT_HILL_DATE   a_field->m_user[7]
#define PL_POT_DID_HILL    a_field->m_user[8]
#define PL_POT_DID_DESS    a_field->m_user[9]

/** Below is the list of things that a farmer can do if he is growing potatoes, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_pot_start = 1, // Compulsory, must always be 1 (one).
	pl_pot_sleep_all_day = PLPOTATOES_BASE,
	pl_pot_ferti_s1,
	pl_pot_ferti_p1,
	pl_pot_stubble_plough,
	pl_pot_autumn_harrow1,
	pl_pot_autumn_harrow2,
	pl_pot_stubble_harrow,
	pl_pot_ferti_p2,
	pl_pot_ferti_s2,	//20910
	pl_pot_ferti_p3,
	pl_pot_ferti_s3,	
	pl_pot_winter_plough,
	pl_pot_spring_harrow,
	pl_pot_ferti_p4,
	pl_pot_ferti_s4,
	pl_pot_spring_harrow2,
	pl_pot_ferti_p5,
	pl_pot_ferti_s5,
	pl_pot_spring_plough,	//20920
	pl_pot_bed_forming,
	pl_pot_spring_planting,
	pl_pot_hilling1,
	pl_pot_hilling2,
	pl_pot_hilling3,
	pl_pot_herbicide1,
	pl_pot_hilling4,
	pl_pot_hilling5,
	pl_pot_hilling6,
	pl_pot_herbicide2,	//20930
	pl_pot_ferti_p6,
	pl_pot_ferti_s6,
	pl_pot_ferti_p7,
	pl_pot_ferti_s7,	
	pl_pot_fungicide1,
	pl_pot_fungicide2,
	pl_pot_fungicide3,
	pl_pot_fungicide4,
	pl_pot_insecticide,
	pl_pot_dessication1,	//20940
	pl_pot_dessication2,
	pl_pot_harvest,
	pl_pot_ferti_p8,
	pl_pot_ferti_s8,	
	pl_pot_foobar
} PLPotatoesToDo;


/**
\brief
PLPotatoes class
\n
*/
/**
See PLPotatoes.h::PLPotatoesToDo for a complete list of all possible events triggered codes by the potatoes management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLPotatoes: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLPotatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_pot_foobar - PLPOTATOES_BASE);
	   m_base_elements_no = PLPOTATOES_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_pot_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_pot_sleep_all_day = PLPOTATOES_BASE,
			fmc_Fertilizer,//pl_pot_ferti_s1,
			fmc_Fertilizer,//pl_pot_ferti_p1,
			fmc_Cultivation,//pl_pot_stubble_plough,
			fmc_Cultivation,//pl_pot_autumn_harrow1,
			fmc_Cultivation,//pl_pot_autumn_harrow2,
			fmc_Cultivation,//pl_pot_stubble_harrow,
			fmc_Fertilizer,//pl_pot_ferti_p2,
			fmc_Fertilizer,//pl_pot_ferti_s2,	//20910
			fmc_Fertilizer,//pl_pot_ferti_p3,
			fmc_Fertilizer,//pl_pot_ferti_s3,
			fmc_Cultivation,//pl_pot_winter_plough,
			fmc_Cultivation,//pl_pot_spring_harrow,
			fmc_Fertilizer,//pl_pot_ferti_p4,
			fmc_Fertilizer,//pl_pot_ferti_s4,
			fmc_Cultivation,//pl_pot_spring_harrow2,
			fmc_Fertilizer,//pl_pot_ferti_p5,
			fmc_Fertilizer,//pl_pot_ferti_s5,
			fmc_Cultivation,//pl_pot_spring_plough,	//20920
			fmc_Others,//pl_pot_bed_forming,
			fmc_Others,//pl_pot_spring_planting,
			fmc_Others,//pl_pot_hilling1,
			fmc_Others,//pl_pot_hilling2,
			fmc_Others,//pl_pot_hilling3,
			fmc_Herbicide,//pl_pot_herbicide1,
			fmc_Others,//pl_pot_hilling4,
			fmc_Others,//pl_pot_hilling5,
			fmc_Others,//pl_pot_hilling6,
			fmc_Herbicide,//pl_pot_herbicide2,	//20930
			fmc_Fertilizer,//pl_pot_ferti_p6,
			fmc_Fertilizer,//pl_pot_ferti_s6,
			fmc_Fertilizer,//pl_pot_ferti_p7,
			fmc_Fertilizer,//pl_pot_ferti_s7,
			fmc_Fungicide,//pl_pot_fungicide1,
			fmc_Fungicide,//pl_pot_fungicide2,
			fmc_Fungicide,//pl_pot_fungicide3,
			fmc_Fungicide,//pl_pot_fungicide4,
			fmc_Insecticide,//pl_pot_insecticide,
			fmc_Others,//pl_pot_dessication1,	//20940
			fmc_Others,//pl_pot_dessication2,
			fmc_Harvest,//pl_pot_harvest,
			fmc_Fertilizer,//pl_pot_ferti_p8,
			fmc_Fertilizer,//pl_pot_ferti_s8,

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLPOTATOES_H

