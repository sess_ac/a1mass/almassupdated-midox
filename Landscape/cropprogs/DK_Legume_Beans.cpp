//
// DK_Legume_Beans.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_Legume_Beans.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_beans_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_Legume_Beans::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
    TTypesOfVegetation l_tov = tov_DKLegume_Beans;

    switch (m_ev->m_todo)
    {
    case dk_legb_start:
    {
        a_field->ClearManagementActionSum();

        DK_LEGB_CLAY = false;
        m_field->SetVegPatchy(true); // LKM: A crop with wide rows, so set patchy
        DK_LEGB_FORCESPRING = false;
        m_last_date = g_date->DayInYear(30, 9); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(30, 9); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
        // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[1][1] = g_date->DayInYear(30, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

        // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
        int isSpring = 0;
        if (StartUpCrop(isSpring, flexdates, int(dk_legb_spring_harrow))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
        // OK, let's go.
        // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
        if (m_ev->m_forcespring) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_legb_spring_harrow, false);
            DK_LEGB_FORCESPRING = true;
            break;
        }
        else if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
        {
            SimpleEvent(d1, dk_legb_autumn_plough, false);
            break;
        }
        else  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_legb_spring_harrow, false);
        break;
    }
    break;

    case dk_legb_autumn_plough:
        if (m_ev->m_lock || m_farm->DoIt_prob(.85)) {
            if (!m_farm->AutumnPlough(m_field, 0.0,
                g_date->DayInYear(30, 11) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_autumn_plough, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_legb_spring_harrow, false);
        break;
    
    case dk_legb_spring_harrow:
        if (a_field->GetVegBiomass() > 0)
        {
            if (!m_farm->SpringHarrow(m_field, 0.0,
                g_date->DayInYear(15, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_spring_harrow, true);
                break;
            }
        }
        if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
        {
            if (m_farm->IsStockFarmer()) //Stock Farmer
            {
                SimpleEvent(g_date->Date() + 1, dk_legb_ferti_ks_s, false);
                break;
            }
            else  SimpleEvent(g_date->Date() + 1, dk_legb_ferti_ks_p, false);
            break;
        }
        else SimpleEvent(g_date->Date() + 1, dk_legb_spring_plough, false);
            break;

    case dk_legb_spring_plough:
        if (m_ev->m_lock || m_farm->DoIt_prob(.85)) {
            if (!m_farm->SpringPlough(m_field, 0.0,
                g_date->DayInYear(17, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_spring_plough, true);
                break;
            }
        }
        if (m_farm->IsStockFarmer()) //Stock Farmer
        {
            SimpleEvent(g_date->Date() + 1, dk_legb_ferti_ks_s, false);
            break;
        }
        else  SimpleEvent(g_date->Date() + 1, dk_legb_ferti_ks_p, false);
        break;

    case dk_legb_ferti_ks_s:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.80)) {
            if (!m_farm->FA_SK(m_field, 0.0,
                g_date->DayInYear(20, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_ferti_ks_s, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 1, dk_legb_sow, false);
        break;

    case dk_legb_ferti_ks_p:
        if (m_ev->m_lock || m_farm->DoIt_prob(.80)) {
            if (!m_farm->FP_SK(m_field, 0.0,
                g_date->DayInYear(20, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_ferti_ks_p, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 1, dk_legb_sow, false); 
        break;

    case dk_legb_sow:
        if (!m_farm->PreseedingCultivatorSow(m_field, 0.0,
            g_date->DayInYear(30, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_legb_sow, true);
            break;
        }
        SimpleEvent(g_date->Date(), dk_legb_roll, false);
        break;

    case dk_legb_roll:
        if (m_ev->m_lock || m_farm->DoIt_prob(.50)) {
            if (!m_farm->SpringRoll(m_field, 0.0,
                g_date->DayInYear(30, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_roll, true);
                break;
            }
        }
        // here comes a fork of different events:
        SimpleEvent(g_date->Date() + 1, dk_legb_herbicide1, false); // herbi thread 
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_legb_insecticide1, false); // insecti thread
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_legb_fungicide, false); // main thread
        break;

        //LKM: next stop herbicide1 
    case dk_legb_herbicide1: // when weeds have sprouted
        if (m_ev->m_lock || m_farm->DoIt_prob(0.80)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(2, 5) -  g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_herbicide1, true);
                break;
            }       
        }
        SimpleEvent(g_date->Date() + 7, dk_legb_herbicide2, false);
        break;

    case dk_legb_herbicide2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) { // random 10%
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(10, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_herbicide2, true);
                break;
            }
            SimpleEvent(g_date->Date() + 7, dk_legb_herbicide3, false);
            break;
        }
        break;
        
    case dk_legb_herbicide3:
        if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) { // the same 10%
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(20, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_herbicide3, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 7, dk_legb_herbicide4, false);
        break;

    case dk_legb_herbicide4:
        if (m_ev->m_lock || m_farm->DoIt_prob(.05)) { // 5% of all
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(30, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_herbicide4, true);
                break;
            }
        }
        break; // end of thread

    case dk_legb_insecticide1:
        if (m_ev->m_lock || m_farm->DoIt_prob(.30)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(30, 5) - g_date->DayInYear();
            if (!cfg_pest_beans_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent(g_date->Date() + 1, dk_legb_insecticide1, true);
                break;
            }
        }
        d1 = g_date->Date();
        if (d1 < g_date->OldDays() + g_date->DayInYear(30, 5)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_legb_insecticide2, false);
            break;
        }
        else  SimpleEvent(g_date->Date() + 14, dk_legb_insecticide2, false);
        break;

    case dk_legb_insecticide2:
        if (m_ev->m_lock || m_farm->DoIt_prob(.50)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(31, 7) - g_date->DayInYear();
            if (!cfg_pest_beans_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent(g_date->Date() + 1, dk_legb_insecticide2, true);
                break;
            }
        }
        break; // end of thread

    case dk_legb_fungicide:
        if (m_ev->m_lock || m_farm->DoIt_prob(.80)) {
            if (!m_farm->FungicideTreat(m_field, 0.0,
                g_date->DayInYear(31, 7) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_legb_fungicide, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_legb_harvest, false);
        break;

    case dk_legb_harvest:
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_legb_harvest, true);
            break;
        }

        m_field->SetVegPatchy(false);
        done = true;
        break;
        // End Main Thread
        done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "DK_Legume_Beans::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

    if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
    return done;
}





