//
// SpringBarleyStriglingSingle.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleyStriglingSingle_H
#define SpringBarleyStriglingSingle_H

#define SBSTS_BASE 7100
#define SBSTS_SLURRY_DONE       m_field->m_user[0]
#define SBSTS_MANURE_DONE       m_field->m_user[1]
#define SBSTS_SLURRY_EXEC       m_field->m_user[2]
#define SBSTS_MANURE_EXEC       m_field->m_user[3]
#define SBSTS_DID_AUTUMN_PLOUGH m_field->m_user[4]

#define SBSTS_HERBI_DATE        m_field->m_user[0]
#define SBSTS_GR_DATE           m_field->m_user[1]
#define SBSTS_FUNGI_DATE        m_field->m_user[2]
#define SBSTS_WATER_DATE        m_field->m_user[3]
#define SBSTS_INSECT_DATE       m_field->m_user[4]

typedef enum {
  sbsts_start = 1, // Compulsory, start event must always be 1 (one).
  sbsts_autumn_plough = SBSTS_BASE,
  sbsts_fertslurry_stock,
  sbsts_fertmanure_stock_one,
  sbsts_spring_plough,
  sbsts_spring_harrow,
  sbsts_fertmanure_plant,
  sbsts_fertlnh3_plant,
  sbsts_fertpk_plant,
  sbsts_fertmanure_stock_two,
  sbsts_fertnpk_stock,
  sbsts_spring_sow,
  sbsts_spring_roll,
  sbsts_strigling_one,
  sbsts_strigling_two,
  sbsts_strigling_three,
  sbsts_strigling_four,
  sbsts_GR,
  sbsts_fungicide_one,
  sbsts_insecticide,
  sbsts_fungicide_two,
  sbsts_water_one,
  sbsts_water_two,
  sbsts_harvest,
  sbsts_straw_chopping,
  sbsts_hay_baling,
  sbsts_stubble_harrow,
  sbsts_foobar
} SBSTSToDo;



class SpringBarleyStriglingSingle: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleyStriglingSingle(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sbsts_foobar - SBSTS_BASE);
	  m_base_elements_no = SBSTS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sbsts_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation,//sbsts_autumn_plough = SBSTS_BASE,
		  fmc_Fertilizer,//sbsts_fertslurry_stock,
		  fmc_Fertilizer,//sbsts_fertmanure_stock_one,
		  fmc_Cultivation,//sbsts_spring_plough,
		  fmc_Cultivation,//sbsts_spring_harrow,
		  fmc_Fertilizer,//sbsts_fertmanure_plant,
		  fmc_Fertilizer,//sbsts_fertlnh3_plant,
		  fmc_Fertilizer,//sbsts_fertpk_plant,
		  fmc_Fertilizer,//sbsts_fertmanure_stock_two,
		  fmc_Fertilizer,//sbsts_fertnpk_stock,
		  fmc_Others,//sbsts_spring_sow,
		  fmc_Others,//sbsts_spring_roll,
		  fmc_Cultivation,//sbsts_strigling_one,
		  fmc_Cultivation,//sbsts_strigling_two,
		  fmc_Cultivation,//sbsts_strigling_three,
		  fmc_Cultivation,//sbsts_strigling_four,
		  fmc_Others,//sbsts_GR,
		  fmc_Fungicide,//sbsts_fungicide_one,
		  fmc_Insecticide,//sbsts_insecticide,
		  fmc_Fungicide,//sbsts_fungicide_two,
		  fmc_Watering,//sbsts_water_one,
		  fmc_Watering,//sbsts_water_two,
		  fmc_Harvest,//sbsts_harvest,
		  fmc_Others,//sbsts_straw_chopping,
		  fmc_Others,//sbsts_hay_baling,
		  fmc_Cultivation//sbsts_stubble_harrow

		  //INSET YOUR FMCS HERE		

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SpringBarleyStriglingSingle_H
