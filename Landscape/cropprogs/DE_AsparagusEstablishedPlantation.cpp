/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_AsparagusEstablishedPlantation.cpp This file contains the source for the DE_AsparagusEstablishedPlantation class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
*/
//
// DE_AsparagusEstablishedPlantation.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_AsparagusEstablishedPlantation.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_asparagus_on;
// check if below are needed
extern CfgFloat cfg_pest_product_1_amount;

extern Landscape* g_landscape_p;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DE_AsparagusEstablishedPlantation::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int today = g_date->Date();
	TTypesOfVegetation l_tov = tov_DEAsparagusEstablishedPlantation;

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_aep_start:
	{
		m_field->ClearManagementActionSum();

		CROP_HARVEST_DO = 50;

		m_last_date = g_date->DayInYear(10, 12); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(10, 12); // last possible day of cutting2 - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;
		flexdates[1][1] = g_date->DayInYear(10, 12); // last possible day of grazing

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_aep_herbicide))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		SimpleEvent_(d1, de_aep_herbicide, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	// This how the asparagus is managed if it is already an established plantation
	// All events are 100% for now

	case de_aep_herbicide:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_herbicide, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(5, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(5, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_aep_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_aep_ferti_p1, false, m_farm, m_field);
		break;

	case de_aep_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_aep_spring_harrow, false, m_farm, m_field);
		break;

	case de_aep_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_aep_spring_harrow, false, m_farm, m_field);
		break;

	case de_aep_spring_harrow:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(16, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_spring_harrow, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_aep_hilling, false, m_farm, m_field);
		break;

	case de_aep_hilling:
		// hilling up should be before the emergence which is ~ 427 GDD, according to weather in Brandenburg in years 2011 - 2020 this GDD was earliest reached on 27.03, but in most cases ~ 10.04
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_aep_hilling, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 4), de_aep_harvesting, false, m_farm, m_field);
		break;

	case de_aep_harvesting: // ~55 events per year
		// continuous harvesting till 15 June, every day
		m_farm->HarvestShoots(m_field, 0.0, 0);
		if (today < g_date->OldDays() + g_date->DayInYear(15, 6)) {
			SimpleEvent_(g_date->Date() + 1, de_aep_harvesting, true, m_farm, m_field);
			break;
		}

		// fork of events
		// N fertilizer treat
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, de_aep_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, de_aep_ferti_p2, false, m_farm, m_field);
		// fungicide treat = MAIN
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), de_aep_fungicide1, false, m_farm, m_field);
		// insecticide treat
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7) + m_date_modifier, de_aep_insecticide1, false, m_farm, m_field);
		// watering treat
		SimpleEvent_(g_date->Date() + 5, de_aep_watering_start, false, m_farm, m_field);
		break;

	case de_aep_watering_start:	// Start irrigation
		if (!m_farm->IrrigationStart(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 7, de_aep_watering_start, true, m_farm, m_field);
			break;
		}
		//Continue the irrigation
		SimpleEvent_(g_date->Date() + 7, de_aep_watering, false, m_farm, m_field);
		break;

	case de_aep_watering:    // Performing irrigation once a week if no raining
		if (!m_farm->Irrigation(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear(), g_date->DayInYear(30, 8))) {
			SimpleEvent_(g_date->Date() + 7, de_aep_watering, false, m_farm, m_field);
			break;
		}
		// end of treat
		break;

	case de_aep_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FA_N(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, de_aep_ferti_s3, false, m_farm, m_field);
		break;

	case de_aep_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FP_N(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, de_aep_ferti_p3, false, m_farm, m_field);
		break;

	case de_aep_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FA_N(m_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		// end of treat
		break;

	case de_aep_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FP_N(m_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		// end of treat
		break;

	case de_aep_fungicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_fungicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, de_aep_fungicide2, false, m_farm, m_field);
		break;

	case de_aep_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, de_aep_fungicide3, false, m_farm, m_field);
		break;

	case de_aep_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 10), de_aep_cutting_plants, false, m_farm, m_field);
		break;

	case de_aep_insecticide1:
		// here we check wheter we are using ERA pesticide or not
		d1 = g_date->DayInYear(30, 7) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_asparagus_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_aep_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, de_aep_insecticide2, false, m_farm, m_field);
		break;

	case de_aep_insecticide2: // 70%
		// here we check wheter we are using ERA pesticide or not
		d1 = g_date->DayInYear(30, 8) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_asparagus_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_aep_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		// end of treat
		break;

	case de_aep_cutting_plants:
		if (!m_farm->HarvestShoots(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_aep_cutting_plants, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, de_aep_ferti_s4, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->Date() + 1, de_aep_ferti_p4, false, m_farm, m_field);
		break;

	case de_aep_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(5, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_aep_harrowing_rows, false, m_farm, m_field);
		break;

	case de_aep_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(5, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_aep_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_aep_harrowing_rows, false, m_farm, m_field);
		break;

	case de_aep_harrowing_rows:
		if (!m_farm->RowCultivation(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_aep_harrowing_rows, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;

	default:
		g_msg->Warn(WARN_BUG, "DE_AsparagusEstablishedPlantation::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}