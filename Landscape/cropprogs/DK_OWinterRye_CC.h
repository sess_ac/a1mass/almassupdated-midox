//
// DK_OWinterRye_CC.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OWinterRye_CC_H
#define DK_OWinterRye_CC_H

#define DK_OWRCC_BASE 161300

typedef enum {
	dk_owrcc_start = 1, // Compulsory, start event must always be 1 (one).
	dk_owrcc_autumn_plough = DK_OWRCC_BASE,
	dk_owrcc_autumn_sow,
	dk_owrcc_slurry_s1,
	dk_owrcc_slurry_p1,
	dk_owrcc_slurry_s2,
	dk_owrcc_slurry_p2,
	dk_owrcc_water,
	dk_owrcc_harvest,
	dk_owrcc_straw_chopping,
	dk_owrcc_hay_bailing,
	dk_owrcc_wait,
	dk_owrcc_foobar
} DK_OWinterRye_CCToDo;



class DK_OWinterRye_CC : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OWinterRye_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 10);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_owrcc_foobar - DK_OWRCC_BASE);
		m_base_elements_no = DK_OWRCC_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_owrcc_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation, // dk_owrcc_autumn_plough = DK_OWR_BASE,
			fmc_Others, //  dk_owrcc_autumn_sow,
			fmc_Fertilizer, //  dk_owrcc_slurry_s1,
			fmc_Fertilizer, //   dk_owrcc_slurry_p1,
			fmc_Fertilizer, //   dk_owrcc_slurry_s2,
			fmc_Fertilizer, //   dk_owrcc_slurry_p2,
			fmc_Watering, //   dk_owrcc_water,
			fmc_Harvest, //   dk_owrcc_harvest,
			fmc_Cutting, //   dk_owrcc_straw_chopping,
			fmc_Others, //   dk_owrcc_hay_bailing,
						// No foobar entry
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_OWinterRye_CC_H
