//
// DK_Maize.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_Maize.h"
#include "math.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_maize_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_Maize::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
    TTypesOfVegetation l_tov = tov_DKMaize; // 

    switch (m_ev->m_todo)
    {
    case dk_m_start:
    {
        a_field->ClearManagementActionSum();

        DK_M_RC_CC = false;
        DK_M_FORCESPRING = false;
        m_last_date = g_date->DayInYear(15, 10); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(14, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
        // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[1][1] = g_date->DayInYear(15, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

        // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
        int isSpring = 0;
        if (StartUpCrop(isSpring, flexdates, int(dk_m_cultivator_sow))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
        // OK, let's go.
        // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
        if (m_ev->m_forcespring) {
            if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
            {
                SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_m_spring_plough, false);
                break;
            }
            else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, dk_m_cultivator_sow, false);
            DK_M_FORCESPRING = true;
            break;
        }
        else SimpleEvent(d1, dk_m_autumn_plough, false);
    }
    break;

    // done if manye weeds, or waste plants from earlier catch crops 
    case dk_m_autumn_plough:
        if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
        {
            if (m_ev->m_lock || m_farm->DoIt_prob(0.85)) {
                if (!m_farm->AutumnPlough(m_field, 0.0,
                    g_date->DayInYear(30, 11) - g_date->DayInYear())) {
                    SimpleEvent(g_date->Date() + 1, dk_m_autumn_plough, true);
                    break;
                }
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_m_spring_plough, false);
        break;

    case dk_m_spring_plough: //+ bedformer in one
        if (m_ev->m_lock || m_farm->DoIt_prob(0.85)) {
            if (!m_farm->SpringPlough(m_field, 0.0,
                g_date->DayInYear(1, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_m_spring_plough, true);
                break;
            }
            SimpleEvent(g_date->Date() + 1, dk_m_spring_harrow1, false);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_m_cultivator_sow, false);
        break;

    case dk_m_spring_harrow1:
        if (!m_farm->SpringHarrow(m_field, 0.0,
            g_date->DayInYear(2, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_spring_harrow1, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_m_spring_harrow2, false);
        break;

    case dk_m_spring_harrow2:
        if (!m_farm->SpringHarrow(m_field, 0.0,
            g_date->DayInYear(3, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_spring_harrow2, true);
            break;
        }
        SimpleEvent(g_date->Date(), dk_m_sow, false);
        break;

    case dk_m_sow:
        if (!m_farm->SpringSow(m_field, 0.0,
            g_date->DayInYear(1, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_sow, true);
            break;
        }
        if (a_farm->IsStockFarmer()) {
            SimpleEvent(g_date->Date(), dk_m_ferti_s1, false);
            break;
        }
        else SimpleEvent(g_date->Date(), dk_m_ferti_p1, false);
        break;

    case dk_m_cultivator_sow:
        if (!m_farm->PreseedingCultivatorSow(m_field, 0.0,
            g_date->DayInYear(1, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_cultivator_sow, true);
            break;
        }
        if (a_farm->IsStockFarmer()) {
            SimpleEvent(g_date->Date(), dk_m_ferti_s1, false);
            break;
        }
        else SimpleEvent(g_date->Date(), dk_m_ferti_p1, false);
        break;

    case dk_m_ferti_s1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.90)) {
            if (!m_farm->FA_Slurry(m_field, 0.0,
                g_date->DayInYear(1, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_m_ferti_s1, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 7, dk_m_herbicide2, false);
        break;

    case dk_m_ferti_p1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.90)) {
            if (!m_farm->FP_Slurry(m_field, 0.0,
                g_date->DayInYear(1, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_m_ferti_p1, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 7, dk_m_herbicide2, false);
        break;

    case dk_m_herbicide2:
        if (!m_farm->HerbicideTreat(m_field, 0.0,
            g_date->DayInYear(8, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_herbicide2, true);
            break;
        }
        SimpleEvent(g_date->Date() + 7, dk_m_row_cultivation_cc, false); // main thread
        SimpleEvent(g_date->Date() + 10, dk_m_herbicide3, false); // herbi thread
        break;

    case dk_m_row_cultivation_cc: // 85% do catch crops - grasses - and thus cultivation row just before sowing
        if (m_ev->m_lock || m_farm->DoIt_prob(0.85)) {
            if (!m_farm->RowCultivation(m_field, 0.0,
                g_date->DayInYear(1, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_m_row_cultivation_cc, true);
                break;
            }
            DK_M_RC_CC = true; // we need to remember who did row cultivation due to catch crops
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_m_sow_cc, false);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_m_water, false);
        break;

    case dk_m_herbicide3:
        if (!m_farm->HerbicideTreat(m_field, 0.0,
            g_date->DayInYear(22, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_herbicide3, true);
            break;
        }
        SimpleEvent(g_date->Date() + 14, dk_m_row_cultivation_weeds, false);
        break;

    case dk_m_row_cultivation_weeds:
        if (DK_M_RC_CC == false)
        {
            if (!m_farm->RowCultivation(m_field, 0.0,
                g_date->DayInYear(7, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_m_row_cultivation_weeds, true);
                break;
            }
        }
        break; // end of thread

    case dk_m_sow_cc:
        if (!m_farm->SpringSow(m_field, 0.0,
            g_date->DayInYear(30, 6) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_sow_cc, true);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_m_water, false);
        break;

    case dk_m_water:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.35)) {
            if (!m_farm->Water(m_field, 0.0,
                g_date->DayInYear(31, 7) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_m_water, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_m_fungicide, false);
        break;


    case dk_m_fungicide:
        if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
        {
            if (!m_farm->FungicideTreat(m_field, 0.0,
                g_date->DayInYear(31, 7) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_m_fungicide, true);
                break;
            }
        }
        else if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
            if (!m_farm->FungicideTreat(m_field, 0.0,
                g_date->DayInYear(31, 7) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_m_fungicide, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(25,7), dk_m_harvest, false);
        break;

    case dk_m_harvest:
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_harvest, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_m_straw_chopping, false);
        break;

    case dk_m_straw_chopping:
        if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_m_straw_chopping, true);
            break;
        }
        done = true;
        break; 

    default:
        g_msg->Warn(WARN_BUG, "DK_Maize::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }
    return done;
}
