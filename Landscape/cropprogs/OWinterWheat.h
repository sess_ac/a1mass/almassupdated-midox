//
// OWinterWheat.h
//
/* 
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWINTERWHEAT_H
#define OWINTERWHEAT_H

#define OWINTERWHEAT_BASE 4900
#define OWW_PLOUGH_RUNS       m_field->m_user[0]
#define OWW_HARROW_RUNS       m_field->m_user[1]

typedef enum {
    oww_start = 1, // Compulsory, must always be 1 (one).
    oww_sleep_all_day = OWINTERWHEAT_BASE,
    oww_ferti_s1,
    oww_ferti_s2,
    oww_ferti_s3,
    oww_ferti_p1,
    oww_ferti_p2,
    oww_autumn_plough,
    oww_autumn_harrow,
    oww_autumn_sow,
    oww_strigling1,
    oww_strigling2,
    oww_strigling_sow,
    oww_spring_sow,
    oww_spring_roll1,
    oww_spring_roll2,
    oww_harvest,
    oww_hay_turning,
    oww_straw_chopping,
    oww_hay_baling,
    oww_stubble_harrow1,
    oww_stubble_harrow2,
    oww_deep_plough,
    oww_catch_all,
    oww_foobar,
} OWinterWheatToDo;


class OWinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   OWinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
     m_first_date=g_date->DayInYear(28,8);
     SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
       const int elements = 2 + (oww_foobar - OWINTERWHEAT_BASE);
       m_base_elements_no = OWINTERWHEAT_BASE - 2;

       FarmManagementCategory catlist[elements] =
       {
            fmc_Others,	// zero element unused but must be here	
            fmc_Others,	//	    oww_start = 1, // Compulsory, must always be 1 (one).
            fmc_Others,	//	    oww_sleep_all_day = OWINTERWHEAT_BASE,
            fmc_Fertilizer,	//	    oww_ferti_s1,
            fmc_Fertilizer,	//	    oww_ferti_s2,
            fmc_Fertilizer,	//	    oww_ferti_s3,
            fmc_Fertilizer,	//	    oww_ferti_p1,
            fmc_Fertilizer,	//	    oww_ferti_p2,
            fmc_Cultivation,	//	    oww_autumn_plough,
            fmc_Cultivation,	//	    oww_autumn_harrow,
            fmc_Others,	//	    oww_autumn_sow,
            fmc_Cultivation,	//	    oww_strigling1,
            fmc_Cultivation,	//	    oww_strigling2,
            fmc_Cultivation,	//	    oww_strigling_sow,
            fmc_Others,	//	    oww_spring_sow,
            fmc_Cultivation,	//	    oww_spring_roll1,
            fmc_Cultivation,	//	    oww_spring_roll2,
            fmc_Harvest,	//	    oww_harvest,
            fmc_Others,	//	    oww_hay_turning,
            fmc_Others,	//	    oww_straw_chopping,
            fmc_Others,	//	    oww_hay_baling,
            fmc_Cultivation,	//	    oww_stubble_harrow1,
            fmc_Cultivation,	//	    oww_stubble_harrow2,
            fmc_Cultivation,	//	    oww_deep_plough,
            fmc_Others	//	    oww_catch_all,


               // no foobar entry	

       };
       // Iterate over the catlist elements and copy them to vector				
       copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // OWINTERWHEATUndersown_H
