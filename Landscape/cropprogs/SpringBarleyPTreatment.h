//
// SpringBarleyPTreatment.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleyPTreatment_H
#define SpringBarleyPtreatment_H

#define SBARLEYPTREATMENT_BASE 6400
#define SBPT_SLURRY_DONE       m_field->m_user[0]
#define SBPT_MANURE_DONE       m_field->m_user[1]
#define SBPT_SLURRY_EXEC       m_field->m_user[2]
#define SBPT_MANURE_EXEC       m_field->m_user[3]
#define SBPT_DID_AUTUMN_PLOUGH m_field->m_user[4]

#define SBPT_HERBI_DATE        m_field->m_user[0]
#define SBPT_GR_DATE           m_field->m_user[1]
#define SBPT_FUNGI_DATE        m_field->m_user[2]
#define SBPT_WATER_DATE        m_field->m_user[3]
#define SBPT_INSECT_DATE       m_field->m_user[4]

typedef enum {
  sbpt_start = 1, // Compulsory, start event must always be 1 (one).
  sbpt_autumn_plough = SBARLEYPTREATMENT_BASE,
  sbpt_fertslurry_stock,
  sbpt_fertmanure_stock_one,
  sbpt_spring_plough,
  sbpt_spring_harrow,
  sbpt_fertmanure_plant,
  sbpt_fertlnh3_plant,
  sbpt_fertpk_plant,
  sbpt_fertmanure_stock_two,
  sbpt_fertnpk_stock,
  sbpt_spring_sow,
  sbpt_spring_roll,
  sbpt_herbicide_one,
  sbpt_herbicide_two,
  sbpt_GR,
  sbpt_fungicide_one,
  sbpt_insecticide1,
  sbpt_insecticide2,
  sbpt_insecticide3,
  sbpt_fungicide_two,
  sbpt_water_one,
  sbpt_water_two,
  sbpt_harvest,
  sbpt_straw_chopping,
  sbpt_hay_baling,
  sbpt_stubble_harrow,
  sbpt_foobar
} SBPTToDo;



class SpringBarleyPTreatment: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleyPTreatment(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(2,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sbpt_foobar - SBARLEYPTREATMENT_BASE);
	  m_base_elements_no = SBARLEYPTREATMENT_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sbpt_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation,//sbpt_autumn_plough = SBARLEYPTREATMENT_BASE,
		  fmc_Fertilizer,//sbpt_fertslurry_stock,
		  fmc_Fertilizer,//sbpt_fertmanure_stock_one,
		  fmc_Cultivation,//sbpt_spring_plough,
		  fmc_Cultivation,//sbpt_spring_harrow,
		  fmc_Fertilizer,//sbpt_fertmanure_plant,
		  fmc_Fertilizer,//sbpt_fertlnh3_plant,
		  fmc_Fertilizer,//sbpt_fertpk_plant,
		  fmc_Fertilizer,//sbpt_fertmanure_stock_two,
		  fmc_Fertilizer,//sbpt_fertnpk_stock,
		  fmc_Others,//sbpt_spring_sow,
		  fmc_Others,//sbpt_spring_roll,
		  fmc_Herbicide,//sbpt_herbicide_one,
		  fmc_Herbicide,//sbpt_herbicide_two,
		  fmc_Others,//sbpt_GR,
		  fmc_Fungicide,//sbpt_fungicide_one,
		  fmc_Insecticide,//sbpt_insecticide1,
		  fmc_Insecticide,//sbpt_insecticide2,
		  fmc_Insecticide,//sbpt_insecticide3,
		  fmc_Fungicide,//sbpt_fungicide_two,
		  fmc_Watering,//sbpt_water_one,
		  fmc_Watering,//sbpt_water_two,
		  fmc_Harvest,//sbpt_harvest,
		  fmc_Others,//sbpt_straw_chopping,
		  fmc_Others,//sbpt_hay_baling,
		  fmc_Cultivation//sbpt_stubble_harrow

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SpringBarley_H
