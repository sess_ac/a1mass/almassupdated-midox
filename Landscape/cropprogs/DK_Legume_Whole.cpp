//
// DK_Legume_Whole.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_Legume_Whole.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_peas_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_Legume_Whole::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false;  // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
    TTypesOfVegetation l_tov = tov_DKLegume_Whole; // The current type - change to match the crop you have

    switch (m_ev->m_todo)
    {
    case dk_lw_start:
    {
        a_field->ClearManagementActionSum();

        DK_LW_2_SOW = false;

        m_field->SetVegPatchy(true); // LKM: A crop with wide rows, so set patchy
        DK_LW_TILL_C = false;
        DK_LW_TILL_S = false;
        DK_LW_FORCESPRING = false;
        m_last_date = g_date->DayInYear(31, 8); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(29, 8); // last possible day of swathing - this is in effect day before the earliest date that a following crop can use
        // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[1][1] = g_date->DayInYear(31, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) // harvest
        flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[2][1] = g_date->DayInYear(31, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) // harvest

        // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
        int isSpring = 0;
        if (StartUpCrop(isSpring, flexdates, int(dk_lw_spring_sow))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
        // OK, let's go.
        // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
        if (m_ev->m_forcespring) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_lw_spring_sow, false);
            DK_LW_FORCESPRING = true;
            break;
        }
        else if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
        {   
            SimpleEvent(d1, dk_lw_autumn_plough, false);
            break;
        }
        else  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_lw_spring_plough, false);
        break;
    }
    break;

    case dk_lw_autumn_plough:
        if (m_ev->m_lock || m_farm->DoIt_prob(.85)) {
            if (!m_farm->AutumnPlough(m_field, 0.0,
                g_date->DayInYear(30, 11) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_lw_autumn_plough, true);
                break;
            }
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_lw_spring_sow, false);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_lw_spring_harrow, false);
        break;

    case dk_lw_spring_plough:
        if (m_ev->m_lock || m_farm->DoIt_prob(.85)) {
            if (!m_farm->SpringPlough(m_field, 0.0,
                g_date->DayInYear(17, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_lw_spring_plough, true);
                break;
            }
            SimpleEvent(g_date->Date() + 1, dk_lw_spring_sow, false);
            break;
        }
        SimpleEvent(g_date->Date(), dk_lw_spring_harrow, false);
        break;

    case dk_lw_spring_harrow:
        if (m_ev->m_lock || m_farm->DoIt_prob(.80)) {
            if (!m_farm->SpringHarrow(m_field, 0.0,
                g_date->DayInYear(17, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_lw_spring_harrow, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 1, dk_lw_spring_sow, false);
        break;

    case dk_lw_spring_sow: //~50% do combi sow (with lay-out of grass) 50% do a normal broad sow
        if (!m_farm->PreseedingCultivatorSow(m_field, 0.0,
            g_date->DayInYear(30, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_lw_spring_sow, true);
            break;
        }
        SimpleEvent(g_date->Date(), dk_lw_spring_roll, false);
        break;

    case dk_lw_spring_roll:
        if (m_ev->m_lock || m_farm->DoIt_prob(.50)) { // ones who do normal broad sow (sow grass afterwards)
            if (!m_farm->SpringRoll(m_field, 0.0,
                g_date->DayInYear(7, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_lw_spring_roll, true);
                break;
            }
            DK_LW_2_SOW = true; //we want to remember who do two separate sowings
            if (a_farm->IsStockFarmer()) {
                SimpleEvent(g_date->Date(), dk_lw_manure_pig_s1, false);
                break;
            }
            else SimpleEvent(g_date->Date(), dk_lw_manure_pig_p1, false);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_lw_insecticide, false); // main thread
        SimpleEvent(g_date->Date() + 7, dk_lw_herbicide, false);
        break;

    case dk_lw_manure_pig_s1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) { //10% of the 50% that sow grass afterwards - 5% in total
            if (!m_farm->FA_Manure(m_field, 0.0,
                g_date->DayInYear(7, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_lw_manure_pig_s1, true);
                break;
            }
        }
        SimpleEvent(g_date->Date(), dk_lw_spring_sow_lo, false);
        break;

    case dk_lw_manure_pig_p1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {//10% of the 50% that sow grass afterwards - 5% in total
            if (!m_farm->FP_Manure(m_field, 0.0,
                g_date->DayInYear(7, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_lw_manure_pig_p1, true);
                break;
            }
        }
        SimpleEvent(g_date->Date(), dk_lw_spring_sow_lo, false);
        break;

    case dk_lw_spring_sow_lo: 
        if (!m_farm->SpringSow(m_field, 0.0,
            g_date->DayInYear(7, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_lw_spring_sow_lo, true);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_lw_insecticide, false); // main thread
        SimpleEvent(g_date->Date() + 7, dk_lw_herbicide, false); // herbi thread        
        break;

    case dk_lw_herbicide: // when weeds have sprouted
        if (m_ev->m_lock || m_farm->DoIt_prob(0.95)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(15, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_lw_herbicide, true);
                break;
            }
        }
        break;

    case dk_lw_insecticide:
        if (m_ev->m_lock || m_farm->DoIt_prob(.30)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(30, 5) - g_date->DayInYear();
            if (!cfg_pest_peas_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent(g_date->Date() + 1, dk_lw_insecticide, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_lw_swathing, false);
        break;

    case dk_lw_swathing:
        if (!m_farm->Swathing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_lw_swathing, true);
            break;
        }
        SimpleEvent(g_date->Date()+1, dk_lw_harvest, false);
        break;

    case dk_lw_harvest:
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_lw_harvest, true);
            break;
        }
        SimpleEvent(g_date->Date(), dk_lw_straw_chopping, false);
        break;

    case dk_lw_straw_chopping:
        if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_lw_straw_chopping, true);
            break;
        }
        d1 = g_date->Date();
        if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_lw_wait, false);
            // Because we are ending harvest before 1.7 so we need to wait until the 1.7
            break;
        }
        else {
            m_field->SetVegPatchy(false);
            done = true;
            break;
        }
    case dk_lw_wait:
        m_field->SetVegPatchy(false);
        done = true;
        break;


    default:
        g_msg->Warn(WARN_BUG, "DK_Legume_Whole::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

    if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
    return done;
}
