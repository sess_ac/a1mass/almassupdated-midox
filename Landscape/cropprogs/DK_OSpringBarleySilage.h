//
// DK_OSpringBarleySilage.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OSpringBarleySilage_H
#define DK_OSpringBarleySilage_H

#define DK_OSBS_BASE 69800

#define DK_OSBS_SOLE_CROP a_field->m_user[1]
#define DK_OSBS_FORCESPRING a_field->m_user[2]


typedef enum {
  dk_osbs_start = 1, // Compulsory, start event must always be 1 (one).
  dk_osbs_spring_harrow1 = DK_OSBS_BASE,
  dk_osbs_spring_harrow2,
  dk_osbs_autumn_plough,
  dk_osbs_slurry_s,
  dk_osbs_slurry_p,
  dk_osbs_spring_plough,
  dk_osbs_spring_sow,
  dk_osbs_sow_lo,
  dk_osbs_harrow1,
  dk_osbs_harrow2,
  dk_osbs_roll,
  dk_osbs_weeding1,
  dk_osbs_weeding2,
  dk_osbs_water,
  dk_osbs_swathing,
  dk_osbs_harvest,
  dk_osbs_hay_bailing,
  dk_osbs_straw_chopping,
  dk_osbs_foobar
} DK_OSpringBarleySilageToDo;



class DK_OSpringBarleySilage: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OSpringBarleySilage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,12);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_osbs_foobar - DK_OSBS_BASE);
	  m_base_elements_no = DK_OSBS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others, // zero element unused but must be here
		  fmc_Cultivation, //  dk_osbs_spring_harrow1 = DK_OSBS_BASE,
		  fmc_Cultivation, //  dk_osbs_spring_harrow2,
		  fmc_Cultivation, // dk_osbs_autumn_plough
		  fmc_Fertilizer, // dk_osbs_slurry_s,
		  fmc_Fertilizer, //   dk_osbs_slurry_p,
		  fmc_Cultivation, // dk_osbs_spring_plough,
		  fmc_Others, //  dk_osbs_spring_sow,
		  fmc_Others, //  dk_osbs_sow_lo,
		  fmc_Cultivation, //  dk_osbs_harrow1,
		  fmc_Cultivation, //  dk_osbs_harrow2,
		  fmc_Others, //  dk_osbs_roll,
		  fmc_Cultivation, //  dk_osbs_weeding1,
		  fmc_Cultivation, //  dk_osbs_weeding2,
		  fmc_Watering, //  dk_osbs_water,
		  fmc_Cutting, //  dk_osbs_swathing,
		  fmc_Harvest, //  dk_osbs_harvest,
		  fmc_Others, //  dk_osbs_hay_bailing,
		  fmc_Cutting, //  dk_osbs_straw_chopping,
					  // No foobar entry
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};

#endif // DK_OSpringBarleySilage_H
