//
// DK_CerealLegume.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKCerealLegume_H
#define DKCerealLegume_H

#define DK_CL_BASE 62100
#define DK_CL_FORCESPRING	a_field->m_user[1]

typedef enum {
  dk_cl_start = 1, // Compulsory, start event must always be 1 (one).
  dk_cl_harvest = DK_CL_BASE,
  dk_cl_autumn_plough1,
  dk_cl_slurry1,
  dk_cl_fertilizer,
  dk_cl_spring_plough,
  dk_cl_spring_sow,
  dk_cl_spring_sow_lo,
  dk_cl_water,
  dk_cl_herbicide,
  dk_cl_insecticide,
  dk_cl_fungicide,
  dk_cl_swathing,
  dk_cl_straw_chopping,
  dk_cl_foobar,
} DK_CerealLegumeToDo;



class DK_CerealLegume : public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_CerealLegume(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_cl_foobar - DK_CL_BASE);
	  m_base_elements_no = DK_CL_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_cl_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_cl_harvest = DK_CL_BASE,
			fmc_Cultivation,	//	  dk_cl_autumn_plough1,
			fmc_Fertilizer,	//	  dk_cl_slurry1,
			fmc_Fertilizer,	//	  dk_cl_fertilizer,
			fmc_Cultivation,	//	  dk_cl_spring_plough,
			fmc_Others,	//	  dk_cl_spring_sow,
			fmc_Others,	//	  dk_cl_spring_sow_lo,
			fmc_Watering,	//	  dk_cl_water,
			fmc_Herbicide,	//	  dk_cl_herbicide,
			fmc_Insecticide,	//	  dk_cl_insecticide,
			fmc_Fungicide,	//	  dk_cl_fungicide,
			fmc_Cutting,	//	  dk_cl_swathing,
			fmc_Others,	//	  dk_cl_straw_chopping,

			   // no foobar entry			

	  };
	  // Iterate over the catlist elements and copy them to vector						
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};


#endif // DK_CerealLegume_H
