/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OOrchCherry.cpp This file contains the source for the DK_OOrchCherry class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OOrchCherry.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OOrchCherry.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_OOrchCherry::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DKOOrchCherry;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_ooch_start:
	{
		// dk_ooch_start just sets up all the starting conditions and reference dates that are needed to start a dk_och

		DK_OOCH_YEARS_AFTER_PLANT = 0;
		DK_OOCH_YEARS_HARVEST = false;
		DK_OOCH_EST_YEAR = false;

		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(31, 12); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 12); // last possible day of cutting orch
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(365, flexdates, int(dk_ooch_manual_cutting1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		// Here we queue up the first event
		//Each field has assign randomly a DK_ooap_YEARS_AFTER_PLANT 

		if ((DK_OOCH_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 10 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ooch_wait, false);
		}
		else if ((DK_OOCH_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 10 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ooch_manual_cutting1, false);
		}
		else if ((DK_OOCH_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 10 == 2)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + 365;
			SimpleEvent(d1, dk_ooch_manual_cutting1, false);
		}
		else 
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, dk_ooch_sleep_all_day, false);
		}
		break;

	}
	break;

	// LKM: This is the first real farm operation 
	case dk_ooch_wait:
		if (!m_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(31, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_wait, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ooch_autumn_plough, false);
		break;

	case dk_ooch_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_autumn_plough, true);
				break;
			}
			DK_OOCH_EST_YEAR = true; 
		}
		SimpleEvent(g_date->Date() + 1, dk_ooch_manure_s1, false);
		break;

	case dk_ooch_manure_s1:
		if (a_farm->IsStockFarmer()) {
			if (!a_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_manure_s1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ooch_water1, false);
		break;

	case dk_ooch_manure_p1:
		if (!a_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manure_p1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ooch_subsoiler, false);
		break;

	case dk_ooch_subsoiler:
		if (!a_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(2, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_subsoiler, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_water1, false);
		break;

	case dk_ooch_water1:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_water1, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ooch_planting, false);
		break;
	case dk_ooch_planting:
		if (!a_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_planting, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ooch_sow_grass, false); // sow thread
		SimpleEvent(g_date->Date(), dk_ooch_manual_cutting10, false); // cutting thread
		break;

	case dk_ooch_sow_grass:
		if (!a_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_sow_grass, true);
			break;
		}
		break; // end of thread

		// start of years after planting year:
	case dk_ooch_sleep_all_day:
		if (!a_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(31, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_sleep_all_day, true);
			break;
		}
		DK_OOCH_YEARS_HARVEST = true; // we need to remember who do this (they should harvest)
		SimpleEvent(g_date->Date(), dk_ooch_manual_cutting1, false);
		break;

	case dk_ooch_manual_cutting1:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(31, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting2, false); // thread for manual cutting (once a month)
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_ooch_manure_s2, false); // main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ooch_insecticide1, false); // insecticide thread
		break;
	case dk_ooch_manual_cutting2:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting3, false);
		break;
	case dk_ooch_manual_cutting3:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting4, false);
		break;
	case dk_ooch_manual_cutting4:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting5, false);
		break;
	case dk_ooch_manual_cutting5:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting6, false);
		break;
	case dk_ooch_manual_cutting6:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting7, false);
		break;
	case dk_ooch_manual_cutting7:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting7, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting8, false);
		break;
	case dk_ooch_manual_cutting8:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting9, false);
		break;
	case dk_ooch_manual_cutting9:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting9, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting10, false);
		break;
	case dk_ooch_manual_cutting10:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting10, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_manual_cutting11, false);
		break;
	case dk_ooch_manual_cutting11:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting11, true);
			break;
		}
		if (DK_OOCH_EST_YEAR == true)
		{
			done = true;
			break;
		}
		break;
		// end of cutting thread

	case dk_ooch_insecticide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_insecticide1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ooch_insecticide2, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ooch_insecticide3, false);
		break;

	case dk_ooch_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_insecticide2, true);
				break;
			}
		}
		break;

	case dk_ooch_insecticide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_insecticide3, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 14, dk_ooch_insecticide4, false);
		break;

	case dk_ooch_insecticide4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->OrganicInsecticide(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_insecticide4, true);
				break;
			}
		}
		break; // end of thread

	case dk_ooch_manure_s2:
		if (a_farm->IsStockFarmer()) {
			if (!a_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_manure_s2, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ooch_row_cultivation1, false); //row cultivation thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ooch_water2, false); // water thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_ooch_cutting1, false); // cutting grass thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ooch_harvest, false); // harvest thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ooch_manure_p2, false); 
		break;

	case dk_ooch_manure_p2:
		if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manure_s2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ooch_row_cultivation1, false); //row cultivation thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ooch_water2, false); // water thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_ooch_cutting1, false); // cutting grass thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ooch_harvest, false); // harvest - main thread
		break;

	case dk_ooch_row_cultivation1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) { // suggestion - not so often in conv. orchards
			if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_row_cultivation1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), dk_ooch_row_cultivation2, false);
		break;

	case dk_ooch_row_cultivation2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) { // suggestion - not so often in conv. orchards
			if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_row_cultivation2, true);
				break;
			}
		}
		break;
		// end of thread

		//Here comes water thread:
	case dk_ooch_water2:
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_water2, true);
				break;
			}
			SimpleEvent(g_date->Date() + 25, dk_ooch_water3, false);
			break;
		}
		break; // no watering if clay soils

	case dk_ooch_water3:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_water3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_water4, false);
		break;
	case dk_ooch_water4:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_water4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_water5, false);
		break;
	case dk_ooch_water5:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_water5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_water6, false);
		break;
	case dk_ooch_water6:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_water6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_water7, false);
		break;
	case dk_ooch_water7:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_water7, true);
			break;
		}
		break; 
		// end of thread
		// cutting grass thread:
	case dk_ooch_cutting1:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_cutting1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_cutting2, false);
		break;
	case dk_ooch_cutting2:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_cutting2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_cutting3, false);
		break;
	case dk_ooch_cutting3:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_cutting3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_cutting4, false);
		break;
	case dk_ooch_cutting4:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_cutting4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_cutting5, false);
		break;
	case dk_ooch_cutting5:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_cutting5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ooch_cutting6, false);
		break;
	case dk_ooch_cutting6:
		if (!a_farm->StrawChopping(m_field, 0.0, g_date->DayInYear(1, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_cutting6, true);
			break;
		}
		break;
		// end of thread

	case dk_ooch_harvest:
		if (DK_OOCH_YEARS_HARVEST == true) {
			if (!a_farm->FruitHarvest(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ooch_harvest, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 12), dk_ooch_manual_cutting12, false);
		break;
		
	case dk_ooch_manual_cutting12:
		if (!a_farm->CutOrch(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ooch_manual_cutting12, true);
			break;
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop (DK_OOrchCherry)
		// END of MAIN THREAD
		break;
		default:
		g_msg->Warn(WARN_BUG, "DK_OOrchCherry::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}