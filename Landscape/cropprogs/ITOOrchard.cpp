/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>ITOOrchard.cpp This file contains the source for the ITOOrchard class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of March 2022 \n
 \n
*/
//
// ITOOrchard.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/ITOOrchard.h"


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool ITOOrchard::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case it_oo_start:
	{
		IT_OO_EARLY = false;
		IT_OO_MID = false;
		IT_OO_LATE = false;
		// Check the next crop for early start, unless it is a spring crop
				// in which case we ASSUME that no checking is necessary!!!!
				// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.
		a_field->ClearManagementActionSum();

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {
			//Checking the future...
			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(1, 7) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "ITOOrchard::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "ITOOrchard::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "ITOOrchard::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				//only for the first year 
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 1), it_oo_winter_pruning, false);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // OK, let's go.
		// Here we queue up the first event
		d1 = g_date->OldDays() + m_first_date;
		if (!m_ev->m_first_year) d1 += 365; // Add 365 for spring crop (not 1st yr)
		if (g_date->Date() > d1) {
			d1 = g_date->Date();
		}
		// OK, let's go. - LKM: Queue first operation 
		SimpleEvent(d1, it_oo_winter_pruning, false);
	}
	break; 

	case it_oo_winter_pruning:
		if (!a_farm->Pruning(a_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_winter_pruning, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), it_oo_irrigation_early, false); // water thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(6, 3), it_oo_fungi_1, false); // fungicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 3), it_oo_insecti_1, false); // insecticide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), it_oo_mechanical_thin_1, false); // thinning thread - main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), it_oo_GR_growth_inhib, false); // growth regulator1 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(17, 4), it_oo_GR_russet_1, false);// growth regulator2 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), it_oo_weed_mowing_1, false); // mowing thread

		// fertilizer threads
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), it_oo_ferti_s1, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 3), it_oo_foliar_feed_s1, false);
			break;
		}
		else
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), it_oo_ferti_p1, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 3), it_oo_foliar_feed_p1, false);
			break;
		}
		break;

	case it_oo_irrigation_early:// 25% of farmers cultivate orchard w. early maturation
		if (a_ev->m_lock || a_farm->DoIt_prob(0.25)) {
			if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_irrigation_early, true);
				break;
			} 
			IT_OO_EARLY = true;
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), it_oo_DI_continued_early, false);
			break;
		}
		SimpleEvent(g_date->Date(), it_oo_irrigation_mid, false);
		break;

	case it_oo_irrigation_mid:// 50% of farmers cultivate orchard w. early maturation
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50/0.75)) {
			if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_irrigation_mid, true);
				break;
			}
			IT_OO_MID = true;
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), it_oo_DI_continued_mid, false);
			break;
		}
		SimpleEvent(g_date->Date(), it_oo_irrigation_late, false);
		break;

	case it_oo_irrigation_late:// 25% of farmers cultivate orchard w. early maturation
		if (a_ev->m_lock || a_farm->DoIt_prob(0.25 / 0.25)) {
			if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_irrigation_late, true);
				break;
			}
			IT_OO_LATE = true;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), it_oo_DI_continued_late, false);
		break;

	case it_oo_DI_continued_early:    // Keep watering every day until 5/8 
		if (!a_farm->Irrigation(a_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear(), g_date->DayInYear(5, 8))) {
			SimpleEvent(g_date->Date() + 1, it_oo_DI_continued_early, false);
			break;
		}
		break;

	case it_oo_DI_continued_mid:    // Keep watering every day until 15/9 
		if (!a_farm->Irrigation(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear(), g_date->DayInYear(15, 9))) {
			SimpleEvent(g_date->Date() + 1, it_oo_DI_continued_mid, false);
			break;
		}
		break;

	case it_oo_DI_continued_late:    // Keep watering every day until 1/10 
		if (!a_farm->Irrigation(a_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear(), g_date->DayInYear(1, 10))) {
			SimpleEvent(g_date->Date() + 1, it_oo_DI_continued_late, false);
			break;
		}
		break; // end of  water thread

	case it_oo_insecti_1:
		if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(18, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_insecti_1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(14, 3), it_oo_insecti_2, false);
		break;

	case it_oo_insecti_2:
		if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(24, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_insecti_2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(27, 3), it_oo_insecti_3, false);
		break;

	case it_oo_insecti_3:
		if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(6, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_insecti_3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(27, 4), it_oo_insecti_4, false);
		break;

	case it_oo_insecti_4: 
		if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_insecti_4, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(9, 6), it_oo_insecti_5, false);
		break;

	case it_oo_insecti_5:
		if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(19, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_insecti_5, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 6), it_oo_insecti_6, false);
		break;

	case it_oo_insecti_6:
		if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_insecti_6, true);
			break;
		}
		break; // end of insecti thread

	case it_oo_fungi_1:// only done if late maturation
		if (IT_OO_LATE == 1) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(16, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(7, 3), it_oo_fungi_2, false);
		break;

	case it_oo_fungi_2:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(17, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_2, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 3), it_oo_fungi_3, false);
		break;

	case it_oo_fungi_3:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_3, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(14, 3), it_oo_fungi_4, false);
		break;

	case it_oo_fungi_4:// only done if early or mid maturation
		if (IT_OO_LATE == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(24, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_4, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(28, 3), it_oo_fungi_5, false);
		break;

	case it_oo_fungi_5:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(7, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_fungi_5, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 3), it_oo_fungi_6, false);
		break;

	case it_oo_fungi_6:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_6, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(4, 4), it_oo_fungi_7, false);
		break;

	case it_oo_fungi_7:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(14, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_7, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(6, 4), it_oo_fungi_8, false);
		break;

	case it_oo_fungi_8:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(16, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_8, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 4), it_oo_fungi_9, false);
		break;

	case it_oo_fungi_9:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(18, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_9, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(11, 4), it_oo_fungi_10, false);
		break;

	case it_oo_fungi_10:// only done if early or mid maturation
		if (IT_OO_LATE == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(21, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_10, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(14, 4), it_oo_fungi_11, false);
		break;

	case it_oo_fungi_11:// only done if early maturation
		if (IT_OO_EARLY == 1) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(24, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_11, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(17, 4), it_oo_fungi_12, false);
		break;

	case it_oo_fungi_12:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(27, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_12, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(19, 4), it_oo_fungi_13, false);
		break;

	case it_oo_fungi_13:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_13, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), it_oo_fungi_14, false);
		break;

	case it_oo_fungi_14:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_14, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(24, 4), it_oo_fungi_15, false);
		break;

	case it_oo_fungi_15:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(4, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_15, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(27, 4), it_oo_fungi_16, false);
		break;

	case it_oo_fungi_16:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_16, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(3, 5), it_oo_fungi_17, false);
		break;

	case it_oo_fungi_17:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(13, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_17, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 5), it_oo_fungi_18, false);
		break;

	case it_oo_fungi_18:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_18, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 5), it_oo_fungi_19, false);
		break;

	case it_oo_fungi_19:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(22, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_fungi_19, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), it_oo_fungi_20, false);
		break;

	case it_oo_fungi_20:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_20, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(18, 5), it_oo_fungi_21, false);
		break;

	case it_oo_fungi_21:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(29, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_fungi_21, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(24, 5), it_oo_fungi_22, false);
		break;

	case it_oo_fungi_22:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(4, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_fungi_22, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 5), it_oo_fungi_23, false);
		break;

	case it_oo_fungi_23:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_23, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(2, 6), it_oo_fungi_24, false);
		break;

	case it_oo_fungi_24: // only done if mid maturation
		if (IT_OO_MID == 1) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(12, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_24, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(9, 6), it_oo_fungi_25, false);
		break;

	case it_oo_fungi_25:// only done if mid maturation
		if (IT_OO_MID == 1) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(19, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_25, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(13, 6), it_oo_fungi_26, false);
		break;

	case it_oo_fungi_26:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(23, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_26, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(26, 6), it_oo_fungi_27, false);
		break;

	case it_oo_fungi_27:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(6, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_fungi_27, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(7, 7), it_oo_fungi_28, false);
		break;

	case it_oo_fungi_28:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(17, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_fungi_28, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(21, 7), it_oo_fungi_29, false);
		break;

	case it_oo_fungi_29:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_29, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(24, 7), it_oo_fungi_30, false);
		break;

	case it_oo_fungi_30:// only done if mid maturation
		if (IT_OO_MID == 1) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(3, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_30, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 7), it_oo_fungi_31, false);
		break;

	case it_oo_fungi_31:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_31, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(28, 8), it_oo_fungi_32, false);
		break;

	case it_oo_fungi_32:// only done if late maturation
		if (IT_OO_LATE == 1) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(7, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_32, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(4, 9), it_oo_fungi_33, false);
		break;

	case it_oo_fungi_33:// only done if mid or late maturation
		if (IT_OO_EARLY == false) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(14, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_33, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(12, 9), it_oo_fungi_34, false);
		break;

	case it_oo_fungi_34:// only done if late maturation
		if (IT_OO_LATE == 1) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(22, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_fungi_34, true);
				break;
			}
		}
		break; // end of fungi thread

	case it_oo_GR_growth_inhib:
		if (!a_farm->GrowthRegulator(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_GR_growth_inhib, true);
			break;
		}
		break; // end of GR1 thread

	case it_oo_GR_russet_1:// only done if early or late maturation
		if (IT_OO_MID == false) {
			if (!a_farm->GrowthRegulator(a_field, 0.0, g_date->DayInYear(27, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_GR_russet_1, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(27, 4), it_oo_GR_russet_2, false);
			break;
		}
		break;

	case it_oo_GR_russet_2:
		if (!a_farm->GrowthRegulator(a_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_GR_russet_2, true);
			break;
		}
		break; // end of GR2 thread

	case it_oo_weed_mowing_1:
		if (!a_farm->CutOrch(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_weed_mowing_1, true);
			break;
		}
		SimpleEvent(g_date->Date()+30, it_oo_weed_mowing_2, false);
		break;

	case it_oo_weed_mowing_2:
		if (!a_farm->CutOrch(a_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_weed_mowing_2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 30, it_oo_weed_mowing_3, false);
		break;

	case it_oo_weed_mowing_3:
		if (!a_farm->CutOrch(a_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_weed_mowing_3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 30, it_oo_weed_mowing_4, false);
		break;

	case it_oo_weed_mowing_4:
		if (!a_farm->CutOrch(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_weed_mowing_4, true);
			break;
		}
		break; // end of weed mow thread

	case it_oo_ferti_s1:
		if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_ferti_s1, true);
			break;
		}
		break;
		// end of feti thread

	case it_oo_ferti_p1:
		if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_ferti_p1, true);
			break;
		}
		break;
		// end of ferti thread

	case it_oo_foliar_feed_s1:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s1, true);
			break;
		}
		SimpleEvent(g_date->Date()+14, it_oo_foliar_feed_s2, false);
		break;
	case it_oo_foliar_feed_s2:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(9, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_s3, false);
		break;
	case it_oo_foliar_feed_s3:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(24, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_s4, false);
		break;
	case it_oo_foliar_feed_s4:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(9, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_s5, false);
		break;
	case it_oo_foliar_feed_s5:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(24, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_s6, false);
		break;
	case it_oo_foliar_feed_s6:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(8, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_s7, false);
		break;
	case it_oo_foliar_feed_s7:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(23, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s7, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_s8, false);
		break;
	case it_oo_foliar_feed_s8:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(8, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_s9, false);
		break;
	case it_oo_foliar_feed_s9:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(23, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s9, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_s10, false);
		break;
	case it_oo_foliar_feed_s10:
		if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(7, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_s10, true);
			break;
		}
		break; // end of foliar feed stock thread

	case it_oo_foliar_feed_p1:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p2, false);
		break;
	case it_oo_foliar_feed_p2:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(9, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p3, false);
		break;
	case it_oo_foliar_feed_p3:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(24, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p4, false);
		break;
	case it_oo_foliar_feed_p4:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(9, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p5, false);
		break;
	case it_oo_foliar_feed_p5:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(24, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p6, false);
		break;
	case it_oo_foliar_feed_p6:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(8, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p7, false);
		break;
	case it_oo_foliar_feed_p7:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(23, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p7, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p8, false);
		break;
	case it_oo_foliar_feed_p8:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(8, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p9, false);
		break;
	case it_oo_foliar_feed_p9:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(23, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p9, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, it_oo_foliar_feed_p10, false);
		break;
	case it_oo_foliar_feed_p10:
		if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(7, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_foliar_feed_p10, true);
			break;
		}
		break; // end of foliar feed stock thread

	case it_oo_mechanical_thin_1:
		if (!a_farm->LeafThinning(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_mechanical_thin_1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), it_oo_mechanical_thin_2, false);
		break;

	case it_oo_mechanical_thin_2:
		if (!a_farm->LeafThinning(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_mechanical_thin_2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), it_oo_mechanical_thin_3, false);
		break;

	case it_oo_mechanical_thin_3:
		if (!a_farm->LeafThinning(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_mechanical_thin_3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), it_oo_manual_thin, false);
		break;

	case it_oo_manual_thin:
		if (!a_farm->Pruning(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_manual_thin, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 7), it_oo_summer_pruning, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), it_oo_harvest_early1, false);
		break;

	case it_oo_summer_pruning:
		if (!a_farm->Pruning(a_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_summer_pruning, true);
			break;
		}
		break; 

	case it_oo_harvest_early1:
		if (IT_OO_EARLY == 1) {
			if (!a_farm->FruitHarvest(a_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_harvest_early1, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(16, 8), it_oo_harvest_early2, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), it_oo_harvest_mid1, false);
		break;

	case it_oo_harvest_mid1:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.00) && IT_OO_MID == 1) {
			if (!a_farm->FruitHarvest(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, it_oo_harvest_mid1, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(16, 9), it_oo_harvest_mid2, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), it_oo_harvest_late1, false);
		break;

	case it_oo_harvest_late1:
		if (!a_farm->FruitHarvest(a_field, 0.0, g_date->DayInYear(8, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_harvest_late1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(9, 10), it_oo_harvest_late2, false);
		break;

	case it_oo_harvest_early2:
		if (!a_farm->FruitHarvest(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_harvest_early2, true);
			break;
		}
		SimpleEvent(g_date->Date(), it_oo_sleep_all_day, false);
		break;

	case it_oo_harvest_mid2:
		if (!a_farm->FruitHarvest(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_harvest_mid2, true);
			break;
		}
		SimpleEvent(g_date->Date(), it_oo_sleep_all_day, false);
		break;

	case it_oo_harvest_late2:
		if (!a_farm->FruitHarvest(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_harvest_late2, true);
			break;
		}
		SimpleEvent(g_date->Date(), it_oo_sleep_all_day, false);
		break;

	case it_oo_sleep_all_day:
		if (!a_farm->SleepAllDay(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, it_oo_sleep_all_day, true);
			break;
		}
		// End of management 
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "ITOOrchard::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}