/**
\file
\brief
<B>PTPotatoes.h This file contains the headers for the Potatoes class</B> \n
*/
/**
\file 
 by Chris J. Topping and Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// PTPotatoes.h
//


#ifndef PTPOTATOES_H
#define PTPOTATOES_H

#define PTPOTATOES_BASE 32500

/** Below is the list of things that a farmer can do if he is growing PTPotatoes, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_pot_start = 1, // Compulsory, must always be 1 (one).
	pt_pot_sleep_all_day = PTPOTATOES_BASE,
	pt_pot_event1,
	pt_pot_event_spring,
} PTPotatoesToDo;


/**
\brief
PTPotatoes class
\n
*/
/**
See PTPotatoes.h::PTPotatoesToDo for a complete list of all possible events triggered codes by the Potatoes management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTPotatoes: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTPotatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of 
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date=g_date->DayInYear( 15,10 ); //EZ: This needs to be changed
   }
};

#endif // PTPOTATOES_H

