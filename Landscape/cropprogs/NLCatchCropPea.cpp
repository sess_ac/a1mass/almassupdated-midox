/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLCatchCropPea.cpp This file contains the source for the NLCatchCropPea class</B> \n
*/
/**
\file
 by Chris J. Topping and Elzbieta Ziolkowska \n
 Version of June 22nd 2020 \n
 All rights reserved. \n
 \n
*/
//
// NLCatchCropPea.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLCatchCropPea.h"

extern CfgBool  g_farm_fixed_crop_enable;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional CatchPeaCrop.
*/
bool NLCatchCropPea::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLCatchCropPea; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case nl_ccp_start:
	{
		m_field->ClearManagementActionSum();

		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		// OK, let's go.
		SimpleEvent_(d1, nl_ccp_stubble_cultivator, false, m_farm, m_field);
	}
	break;

		// This is the first real farm operation
	case nl_ccp_stubble_cultivator:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_stubble_cultivator, true, m_farm, m_field);
			break;
		}
		// Here we queue up the nextt event - this differs depending on whether we have a
		// stock or arable farmer
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 5, nl_ccp_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 5, nl_ccp_ferti_p1, false, m_farm, m_field);
		break;

	case nl_ccp_ferti_p1:
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_p1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_ccp_preseeding_cultivator_with_sow, false, m_farm, m_field);
		break;
	case nl_ccp_ferti_s1:
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_s1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_ccp_preseeding_cultivator_with_sow, false, m_farm, m_field);
		break;
	case nl_ccp_preseeding_cultivator_with_sow:
		if (!m_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(16, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_preseeding_cultivator_with_sow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		}
		if (m_field->GetSoilType() == 2 || m_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			if (m_farm->IsStockFarmer()) {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, nl_ccp_ferti_s2_sandy, false, m_farm, m_field);
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, nl_ccp_ferti_p2_sandy, false, m_farm, m_field);
			}
		}
		else {
			if (m_farm->IsStockFarmer()) {
				SimpleEvent_(d1, nl_ccp_ferti_s2_clay, false, m_farm, m_field);
			}
			else {
				SimpleEvent_(d1, nl_ccp_ferti_p2_clay, false, m_farm, m_field);
			}
		}
		break;
	case nl_ccp_ferti_s2_clay:
			if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_s2_clay, true, m_farm, m_field);
				break;
			}
			d1 = g_date->Date() + 40;
			if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
				d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
			}
			SimpleEvent_(d1, nl_ccp_winter_plough_clay, false, m_farm, m_field);
			break;
	case nl_ccp_ferti_p2_clay:
		if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_p2_clay, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 40;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		SimpleEvent_(d1, nl_ccp_winter_plough_clay, false, m_farm, m_field);
		break;
	case nl_ccp_winter_plough_clay:
		if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(20, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_winter_plough_clay, true, m_farm, m_field);
			break;
		}
		// The plan is finished on clay soils (catch crop in incorporated into soil on autumn)
		// Calling sleep_all_day to move to the next year 
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, nl_ccp_sleep_all_day, false, m_farm, m_field);
		//SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, nl_ccp_ferti_s2_sandy, false, m_farm, m_field);
		break;
	case nl_ccp_sleep_all_day:
		if (!m_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_sleep_all_day, true, m_farm, m_field);
			break;
		}
		done = true;
		break;
	case nl_ccp_ferti_s2_sandy:
		if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_s2_sandy, true, m_farm, m_field);
			break;
		}
		// The plan is finished on sandy soils; spring plough (to incorporate catch crop into soil) will follow but it is called from the next crop 
		done = true;
		break;
	case nl_ccp_ferti_p2_sandy:
		if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ccp_ferti_p2_sandy, true, m_farm, m_field);
			break;
		}
		// The plan is finished on sandy soils; spring plough (to incorporate catch crop into soil) will follow but it is called from the next crop 
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLCatchCropPea::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}
