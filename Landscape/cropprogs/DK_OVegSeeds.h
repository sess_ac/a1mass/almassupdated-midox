//
// DK_OVegSeeds.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OVegSeeds_H
#define DK_OVegSeeds_H

#define DK_OVS_FORCESPRING a_field->m_user[1]

#define DK_OVS_BASE 160100

typedef enum {
	dk_ovs_start = 1, // Compulsory, start event must always be 1 (one).
	dk_ovs_autumn_plough = DK_OVS_BASE,
	dk_ovs_molluscicide,
	dk_ovs_harrow1,
	dk_ovs_harrow2,
	dk_ovs_ferti_s,
	dk_ovs_ferti_p,
	dk_ovs_spring_plough,
	dk_ovs_sow_cultivation,
	dk_ovs_strigling1,
	dk_ovs_strigling2,
	dk_ovs_manganese,
	dk_ovs_row_cultivation1,
	dk_ovs_row_cultivation2,
	dk_ovs_row_cultivation3,
	dk_ovs_water,
	dk_ovs_swathing,
	dk_ovs_harvest,
	dk_ovs_foobar
} DK_OVegSeedsToDo;



class DK_OVegSeeds : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OVegSeeds(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(30, 11);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_ovs_foobar - DK_OVS_BASE);
		m_base_elements_no = DK_OVS_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_ovs_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation, // dk_ovs_autumn_plough = DK_OVS_BASE,
			fmc_Others, // dk_ovs_molluscicide,
			fmc_Cultivation, //  dk_ovs_harrow1,
			fmc_Cultivation, //  dk_ovs_harrow2,
			fmc_Fertilizer, //  dk_ovs_ferti_s,
			fmc_Fertilizer, //  dk_ovs_ferti_p,
			fmc_Cultivation, //  dk_ovs_spring_plough,
			fmc_Cultivation, //  dk_ovs_sow_cultivation,
			fmc_Cultivation, //  dk_ovs_strigling1,
			fmc_Cultivation, //  dk_ovs_strigling2,
			fmc_Fertilizer, //  dk_ovs_manganese,
			fmc_Cultivation, // dk_ovs_row_cultivation1,
			fmc_Cultivation, //  dk_ovs_row_cultivation2,
			fmc_Cultivation, //  dk_ovs_row_cultivation3,
			fmc_Watering, // dk_ovs_water,
			fmc_Cutting, //  dk_ovs_swathing,
			fmc_Harvest, //   dk_ovs_harvest,
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_OVegSeeds_H