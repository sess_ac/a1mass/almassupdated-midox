/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>FI_OTurnipRape.cpp This file contains the source for the FI_OTurnipRape class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
*/
//
// FI_OSpringRape.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FI_OSpringRape.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_FI_OSpringRape_SkScrapes("DK_CROP_OSR_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_ospringrape_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_SW_InsecticideDay;
extern CfgInt   cfg_SW_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case organic spring rape.
*/
bool FI_OSpringRape::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case fi_osr_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		FI_OSR_AUTUMN_PLOUGH = false;
		FI_OSR_DECIDE_TO_HERB = 1;
		FI_OSR_DECIDE_TO_FI = 1;

		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
		if (cfg_FI_OSpringRape_SkScrapes.value()) {
			a_field->m_skylarkscrapes = true;
		}
		else {
			a_field->m_skylarkscrapes = false;
		}
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 3 start and stop dates for all events after harvest for this crop
		int noDates = 3;
		a_field->SetMDates(0, 0, g_date->DayInYear(10, 10)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(10, 10)); // last possible day of herbicide after harvest
		a_field->SetMDates(0, 1, 0); // start day of stubble cultivator
		a_field->SetMDates(1, 1, g_date->DayInYear(15, 10)); // end day of stubble cultivator
		a_field->SetMDates(0, 2, 0); // start day of autumn plough
		a_field->SetMDates(1, 2, g_date->DayInYear(30, 10)); // end day of autumn plough
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "FI_OSpringRape::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "FI_OSpringRape::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "FI_OSpringRape::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes (start woth the first event on the after winter time) - Date here is a suggestion (not stated in crop scheme!)
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), fi_osr_slurry, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
		// LKM: Here we queue up the first event - 
		//
		SimpleEvent(d1, fi_osr_stubble_cultivator1, false);
	}
	break;
	// This is the first real farm operation - LKM: done if there is time between 1st September-10th of October - 50% do this
	case fi_osr_stubble_cultivator1:
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
				// If we don't succeed on the first try, then try and try again (until 10/10 when we will succeed)
				SimpleEvent(g_date->Date() + 1, fi_osr_stubble_cultivator1, true);
				break;
			}
			// Queue up the next event - in this case spring plough (the next year) - date is a suggestion (no date stated in crop scheme!) - 5% do this
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4) + 365, fi_osr_slurry, false);
			break;
		}
		else //  autumn plough - the rest (50%) do this
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 9), fi_osr_autumn_plough, false);
		break;
	case fi_osr_autumn_plough:
		if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_osr_autumn_plough, true);
			break;
		}
		// Queue up the next event - in this case spring plough (the next year) - date is a suggestion - 8% do this
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, fi_osr_slurry, false);
		break;
	case fi_osr_slurry:
		if (a_ev->m_lock || a_farm->DoIt(8)) {
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_osr_slurry, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), fi_osr_spring_plough, false);
		break;
	case fi_osr_spring_plough:
		if (a_ev->m_lock || a_farm->DoIt(5)) {
			if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_osr_spring_plough, true);
				break;
			}
		}
		// Queue up the next event - in this case preseeding cultivation - 85% do this
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 4), fi_osr_preseeding_cultivation, false);
		break;
	case fi_osr_preseeding_cultivation:
		if (a_ev->m_lock || a_farm->DoIt(85)) {
			if (!a_farm->PreseedingCultivator(a_field, 0.0,
				g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_osr_preseeding_cultivation, true);
				break;
			}
			SimpleEvent(g_date->Date(), fi_osr_sow, false);
			break;
		} // the rest (15%) do preseeding cultivator with sow 
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 5), fi_osr_preseeding_sow, false);
		break;

	case fi_osr_sow: // sowing with fertilizer
		if (!a_farm->SpringSowWithFerti(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_osr_sow, true);
			break;
		} // Queue up the next event - in this case harrow before emergence (suggest 7 days)
		SimpleEvent(g_date->Date() + 7, fi_osr_harrow, false);
		break;
	case fi_osr_preseeding_sow:
		if (!a_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_osr_preseeding_sow, true);
			break;
		} // fertilizer when preseeding cultivator with sowing
		SimpleEvent(g_date->Date(), fi_osr_fertilizer, false);
		break;
	case fi_osr_fertilizer:
		if (!a_farm->FP_NPKS(a_field, 0.0,
			g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_osr_fertilizer, true);
			break;
		} // Queue up the next event - in this case harrow before emergence (suggest 7 days)
		SimpleEvent(g_date->Date() + 7, fi_osr_harrow, false);
		break;
	case fi_osr_harrow: // 100% do this (suggestion)
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->SpringHarrow(a_field, 0.0,
				g_date->DayInYear(7, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_osr_harrow, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 9), fi_osr_harvest, false);
		break;
	case fi_osr_harvest:
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0,
			g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_osr_harvest, true);
			break;
		}
		// 50% of farmers will do stubble cultivation
			SimpleEvent(g_date->Date(), fi_osr_stubble_cultivator2, false);
			break;
	case fi_osr_stubble_cultivator2:
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0,
				g_date->DayInYear(15, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_osr_stubble_cultivator2, true);
				break;
			}
			done = true;
		} // the rest (50%) will do plough
		else SimpleEvent(g_date->Date(), fi_osr_autumn_plough2, false);
		break;
	case fi_osr_autumn_plough2: // 50% do this
			if (!a_farm->AutumnPlough(a_field, 0.0,
				g_date->DayInYear(30, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_osr_autumn_plough2, true);
				break;
			}
		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "FI_OSpringRape::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}