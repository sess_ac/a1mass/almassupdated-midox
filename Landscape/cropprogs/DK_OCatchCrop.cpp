//
// DK_OCatchCrop.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OCatchCrop.h"

extern CfgBool  g_farm_fixed_crop_enable;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional OCatchPeaCrop.
*/
bool DK_OCatchCrop::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DKOCatchCrop; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case dk_occ_start:
	{
		a_field->ClearManagementActionSum();

		/**
		* Catch crops in ALMaSS cannot be part of the normal farm plan but are created as part of another crop management.
		* They are therefore dependend on the other crops to set up the starting conditions, hence AddNewEvent is called by the calling crop to create m_startday etc..
		*/
		/**
		* Date management control is done by providing pairs of dates as start and stop dates for a management. In this case only harvest can be moved
		* but normally there will be post harvest operations too. These dates are stored using Get and SetMDates
		*/
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(1, 1)); // This is the first day of sleep all day
		m_field->SetMDates(1, 0, g_date->DayInYear(1, 2)); // This is the last day of sleep all day
		m_field->SetMDates(0, 1, g_date->DayInYear(1, 2)); // This is the first day of spring plough
		m_field->SetMDates(1, 1, g_date->DayInYear(10, 3)); // This is the last day of spring plough
		// Now test for fixable late finishing problems
		if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
		{
			g_msg->Warn(WARN_BUG, "DK_OCatchCrop::Do(): Harvest too late for the next crop to start!!!", "");
			exit(l_tov);
		}
		// Now fix any late finishing problems - for the code below will work with more than one set of MDate pairs but is not entirely necessary for this generic crop (cannot move MDates[0,0])
		for (int i = 0; i < noDates; i++) {
			if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
				m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
			}
			if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
				m_field->SetMConstants(i, 0);
				m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
			}
		}
		// Now all the date management things are checked we can start with the main code
		d1 = g_date->OldDays() + g_date->DayInYear();
		// Because we don't know the sow date when coding we have to use a variable here, hence GCCR_ENDDATE
		DK_OCC_ENDDATE = g_date->DayInYear() + 7; // we allow 7 days for sowing
		// Here we queue up the first event - this differs depending on whether we have a
		// stock or arable farmer
		SimpleEvent_(d1, dk_occ_stubble_harrow, false, a_farm, a_field);
	}
	break;

	// This is the first real farm operation
	case dk_occ_stubble_harrow:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) { // suggests 50% do a harrow before sowing, the rest sow directly in the stubble
			if (!a_farm->StubbleHarrowing(a_field, 0.0, DK_OCC_ENDDATE - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_occ_stubble_harrow, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_occ_sow, false);
		break;
	case dk_occ_sow:
		if (!a_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(16, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_occ_sow, true);
			break;
		}
		SimpleEvent(g_date->Date() + 40, dk_occ_winter_plough, false);
		break;
	case dk_occ_winter_plough:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
				if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(31, 12) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_occ_winter_plough, true);
					break;
				}
				// The plan is finished on clay soils (catch crop in incorporated into soil on autumn)
				// Calling sleep_all_day to move to the next year d1 = g_date->OldDays() + 365 + m_field->GetMDates(0, 0);
				d1 = g_date->OldDays() + 365 + m_field->GetMDates(0, 0);
				SimpleEvent(d1, dk_occ_sleep_all_day, false);
				break;
		}
		else d1 = g_date->OldDays() + 365 + m_field->GetMDates(0, 1);
		SimpleEvent(d1, dk_occ_spring_plough, false);
		break;

	case dk_occ_sleep_all_day:
		if (!a_farm->SleepAllDay(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_occ_sleep_all_day, true);
			break;
		}
		done = true;
		break;

	case dk_occ_spring_plough:
			if (!a_farm->SpringPlough(a_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_occ_spring_plough, true);
				break;
			}
			done = true;
			break;

	default:
		g_msg->Warn(WARN_BUG, "DK_OCatchCrop::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}


