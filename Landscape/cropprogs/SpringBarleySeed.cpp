//
// SpringBarleySeed.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SpringBarleySeed.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool SpringBarleySeed::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1=0;
  int noDates=3;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case sbse_start:
    // Decide wether to do autumn_plough:
    SBSE_FUNGI_DATE = 0;
    SBSE_WATER_DATE = 0;
    a_field->ClearManagementActionSum();


      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_first_date=g_date->DayInYear(1,12);
      m_last_date=g_date->DayInYear(15,9);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(1,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,8));
      m_field->SetMDates(0,1,g_date->DayInYear(5,8));
      m_field->SetMDates(1,1,g_date->DayInYear(25,8));
      m_field->SetMDates(0,2,g_date->DayInYear(15,8));
      m_field->SetMDates(1,2,g_date->DayInYear(15,9));
      // Check the next crop for early start, unless it is a spring crop
      if ((m_field->GetMDates(0,0) >=m_ev->m_startday)
                  && (m_ev->m_startday>g_date->DayInYear(1,7)))
      {
        g_msg->Warn( WARN_BUG, "SpringBarleySeed::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++)
      {
        if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
        if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
      }
      // Now no operations can be timed after the start of the next crop.
      if ( ! m_ev->m_first_year )
      {
	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "SpringBarleySeed::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "SpringBarleySeed::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }

      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                                                    sbse_spring_plough, false );
         break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 1,11 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }

      // OK, let's go.

      if (m_farm->DoIt(50))
      {
        SimpleEvent( d1, sbse_autumn_plough, false );
      }
      else
      {
      SimpleEvent( g_date->OldDays() + 365 + g_date->DayInYear( 15, 3 ),
		   sbse_spring_plough, false );
      }
    break;

  case sbse_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 1,12 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 )+365,
		 sbse_spring_harrow, false );
    break;

  case sbse_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
			       g_date->DayInYear( 10, 4 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date()+1, sbse_spring_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), sbse_spring_harrow, false );
    break;

  case sbse_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
			       g_date->DayInYear( 10,4 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_spring_harrow, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 > g_date->OldDays() + g_date->DayInYear( 20,3 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 20,3 );
      if ( m_farm->DoIt( 80 ))
	SimpleEvent( d1, sbse_fertnpk, false );
      else
	SimpleEvent( d1, sbse_fertlnh3, false );
    }
    break;

  case sbse_fertnpk:
    if (!m_farm->FP_NPK( m_field, 0.0,
			g_date->DayInYear( 10, 4 ) -
			g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_fertnpk, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 25,3 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 25,3 );
      }
      SimpleEvent( d1, sbse_spring_sow, false );
    }
    break;

  case sbse_fertlnh3:
    if (!m_farm->FP_LiquidNH3( m_field, 0.0,
			       g_date->DayInYear( 10, 4 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_fertlnh3, true );
      break;
    }
    SimpleEvent( g_date->Date(), sbse_fertpk, false );
    break;


  case sbse_fertpk:
    if (!m_farm->FP_PK( m_field, 0.0,
			g_date->DayInYear( 10, 4 ) -
			g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_fertpk, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 25,3 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 25,3 );
      }
      SimpleEvent( d1, sbse_spring_sow, false );
    }
    break;

  case sbse_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
			    g_date->DayInYear( 15,4 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_spring_sow, true );
      break;
    }
    SimpleEvent( g_date->Date(), sbse_spring_roll, false );
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1,4 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 1,4 );
      }
      SimpleEvent( d1, sbse_herbicide_one, false );
    }
    break;

  case sbse_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 90 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
			       g_date->DayInYear( 20,4 ) -
			       g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbse_spring_roll, true );
        break;
      }
    }
    break;

  case sbse_herbicide_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (100*cfg_herbi_app_prop.value() ))) {
      if (!m_farm->HerbicideTreat( m_field, 0.0,
				 g_date->DayInYear( 30,4 ) -
				 g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbse_herbicide_one, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,5 ),
		 sbse_GR, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,5 ),
		 sbse_fungicide_one, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
		 sbse_water_one, false );
    break;

  case sbse_GR:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (25*cfg_greg_app_prop.value() )))
    {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				    g_date->DayInYear( 25,5 ) -
				    g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbse_GR, true );
        break;
      }
    }
    break;

  // Fungicide thread
  case sbse_fungicide_one:
    if ( g_date->Date() < SBSE_WATER_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sbse_fungicide_one, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (90*cfg_fungi_app_prop1.value() )))
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
				   g_date->DayInYear( 25,5 ) -
				   g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbse_fungicide_one, true );
        break;
      }
      SBSE_FUNGI_DATE = g_date->Date();
      {
        d1 = g_date->Date() + 10;
	if ( d1 < g_date->OldDays() + g_date->DayInYear( 15,5 )) {
	  d1 = g_date->OldDays() + g_date->DayInYear( 15,5 );
	}
        SimpleEvent( d1, sbse_fungicide_two, false );
      }
    }
    break;

  case sbse_fungicide_two:
    if ( g_date->Date() < SBSE_WATER_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sbse_fungicide_two, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (30*cfg_fungi_app_prop1.value() )))
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
				   g_date->DayInYear( 10,6 ) -
				   g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbse_fungicide_two, true );
        break;
      }
      SBSE_FUNGI_DATE = g_date->Date();
    }
    break;

  // Water thread
  case sbse_water_one:
    if ( g_date->Date() < SBSE_FUNGI_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sbse_water_one, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 30,5 ) -
			  g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbse_water_one, true );
        break;
      }
      SBSE_WATER_DATE = g_date->Date();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
		 sbse_water_two, false );
    break;

  case sbse_water_two:
    if ( g_date->Date() < SBSE_FUNGI_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sbse_water_two, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 1,7 ) -
			  g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbse_water_two, true );
        break;
      }
      SBSE_WATER_DATE = g_date->Date();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
		 sbse_harvest, false );
    break;

  case sbse_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
			  m_field->GetMDates(1,0) -
			  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_harvest, true );
      break;
    }
    {
      d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + m_field->GetMDates(0,1))
	d1 = g_date->OldDays() + m_field->GetMDates(0,1);
      SimpleEvent( d1, sbse_hay_bailing, false );
    }
    break;

  case sbse_hay_bailing:
    if (!m_farm->HayBailing( m_field, 0.0,
			     m_field->GetMDates(1,1) -
			     g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_hay_bailing, true );
      break;
    }
// The rest is removed until we figure out how to implement over the year
// boundries - this is not a nice crop  **CJT**
/*    if ( m_farm->DoIt( 20 ))
      SimpleEvent( g_date->Date(), sbse_burn_straw_stubble, false );
    else {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + m_field->GetMDates(0,2))
	d1 = g_date->OldDays() + m_field->GetMDates(0,2);
      SimpleEvent( d1, sbse_herbicide_two, false );
    }
    break;

  case sbse_herbicide_two:
   if (m_farm->DoIt(100*cfg_herbi_app_prop.value()))
   {
    if (!m_farm->HerbicideTreat( m_field, 0.0,
				 m_field->GetMDates(1,2) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_herbicide_two, true );
      break;
    }
    done = true;
   }
   */
   done = true;
   break;

  case sbse_burn_straw_stubble:
    if (!m_farm->BurnStrawStubble( m_field, 0.0,
				   m_field->GetMDates(1,2) -
				   g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbse_burn_straw_stubble, true );
      break;
    }
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "SpringBarleySeed::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}


