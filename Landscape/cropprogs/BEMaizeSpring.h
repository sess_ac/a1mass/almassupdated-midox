/**
\file
\brief
<B>BEMaizeSpring.h This file contains the headers for the MaizeSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BEMaizeSpring.h
//


#ifndef BEMAIZESPRING_H
#define BEMAIZESPRING_H

#define BEMAIZESPRING_BASE 26400
/**
\brief A flag used to indicate autumn ploughing status
*/
#define BE_MS_START_FERTI	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing mazie, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_ms_start = 1, // Compulsory, must always be 1 (one).
	BE_ms_sleep_all_day = BEMAIZESPRING_BASE,
	BE_ms_ferti_p1,
	BE_ms_ferti_s1,
	BE_ms_spring_plough_sandy,
	BE_ms_preseeding_cultivator,
	BE_ms_spring_sow_with_ferti,
	BE_ms_spring_sow,
	BE_ms_ferti_p2,
	BE_ms_ferti_s2,
	BE_ms_herbicide1,
	BE_ms_harvest,
	BE_ms_straw_chopping,
} BEMaizeSpringToDo;


/**
\brief
BEMaizeSpring class
\n
*/
/**
See BEMaizeSpring.h::BEMaizeSpringToDo for a complete list of all possible events triggered codes by the mazie management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEMaizeSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEMaizeSpring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,4 );
   }
};

#endif // BEMAIZESPRING_H

