/**
\file
\brief
<B>DK_OrchCherry.h This file contains the source for the DK_OrchCherry class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OrchCherry.h
//


#ifndef DK_ORCHCherry_H
#define DK_ORCHCherry_H

#define DK_OCH_YEARS_AFTER_PLANT	a_field->m_user[0]
#define DK_OCH_YEARS_HARVEST	a_field->m_user[1]
#define DK_OCH_EST_YEAR	a_field->m_user[2]

#define DK_OCH_BASE 68200
/**

*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_och_start = 1, // Compulsory, must always be 1 (one).
	dk_och_sleep_all_day = DK_OCH_BASE,
	dk_och_wait,
	dk_och_autumn_plough,
	dk_och_manure_s1,
	dk_och_manure_p1,
	dk_och_water1,
	dk_och_subsoiler,
	dk_och_planting,
	dk_och_sow_grass,
	dk_och_insecticide1,
	dk_och_insecticide2,
	dk_och_insecticide3,
	dk_och_herbicide1,
	dk_och_herbicide2,
	dk_och_manual_cutting1,
	dk_och_manual_cutting2,
	dk_och_manual_cutting3,
	dk_och_manual_cutting4,
	dk_och_manual_cutting5,
	dk_och_manual_cutting6,
	dk_och_manual_cutting7,
	dk_och_manual_cutting8,
	dk_och_manual_cutting9,
	dk_och_manual_cutting10,
	dk_och_manual_cutting11,
	dk_och_manual_cutting12,
	dk_och_manure_s2,
	dk_och_manure_p2,
	dk_och_row_cultivation1,
	dk_och_water2,
	dk_och_water3,
	dk_och_water4,
	dk_och_water5,
	dk_och_water6,
	dk_och_water7,
	dk_och_cutting1,
	dk_och_cutting2,
	dk_och_cutting3,
	dk_och_cutting4,
	dk_och_cutting5,
	dk_och_cutting6,
	dk_och_row_cultivation2,
	dk_och_fungicide1,
	dk_och_fungicide2,
	dk_och_insecticide4,
	dk_och_insecticide5,
	dk_och_insecticide6,
	dk_och_insecticide7,
	dk_och_insecticide8,
	dk_och_insecticide9,
	dk_och_insecticide10,
	dk_och_herbicide3,
	dk_och_herbicide4,
	dk_och_herbicide5,
	dk_och_fungicide3,
	dk_och_harvest,
	dk_och_fungicide4,
	dk_och_fungicide5,
	dk_och_fungicide6,
	dk_och_fungicide7,
	dk_och_fungicide8,
	dk_och_fungicide9,
	dk_och_fungicide10,
	dk_och_fungicide11,
	dk_och_fungicide12,
	dk_och_fungicide13,
	dk_och_fungicide14,
	dk_och_gr1,
	dk_och_gr2,
	dk_och_gr3,
	dk_och_gr4,
	dk_och_foobar,
} DK_OrchCherryToDo;


/**
\brief
DK_OrchCherry class
\n
*/
/**
See DK_OrchCherry.h::DK_OrchCherryToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OrchCherry : public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OrchCherry(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,1 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_och_foobar - DK_OCH_BASE);
	   m_base_elements_no = DK_OCH_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_och_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_och_sleep_all_day = DK_OCH_BASE,
			fmc_Others, // dk_och_wait,
			fmc_Cultivation, //dk_och_spring_plough,
			fmc_Fertilizer, //dk_och_manure_s1,
			fmc_Fertilizer, //dk_och_manure_p1,
			fmc_Watering, //dk_och_water1,
			fmc_Cultivation, //dk_och_subsoiler,
			fmc_Others, //dk_och_planting,
			fmc_Others, //dk_och_sow_grass,
			fmc_Insecticide, //dk_och_insecticide1,
			fmc_Insecticide, //dk_och_insecticide2,
			fmc_Insecticide, //dk_och_insecticide3,
			fmc_Herbicide, //dk_och_herbicide1
			fmc_Herbicide, //dk_och_herbicide2
			fmc_Cutting, //dk_och_manual_cutting1,
			fmc_Cutting, //dk_och_manual_cutting2,
			fmc_Cutting, //dk_och_manual_cutting3,
			fmc_Cutting, //dk_och_manual_cutting4,
			fmc_Cutting, //dk_och_manual_cutting5,
			fmc_Cutting, //dk_och_manual_cutting6,
			fmc_Cutting, //dk_och_manual_cutting7,
			fmc_Cutting, //dk_och_manual_cutting8,
			fmc_Cutting, //dk_och_manual_cutting9,
			fmc_Cutting, //dk_och_manual_cutting10,
			fmc_Cutting, //dk_och_manual_cutting11,
			fmc_Cutting, //dk_och_manual_cutting12,
			fmc_Cutting, //dk_och_manure_s2,
			fmc_Cutting, //dk_och_manure_p2,
			fmc_Cultivation, //dk_och_row_cultivation1,
			fmc_Watering, //dk_och_water2,
			fmc_Watering, //dk_och_water3,
			fmc_Watering, //dk_och_water4,
			fmc_Watering, //dk_och_water5,
			fmc_Watering, //dk_och_water6,
			fmc_Watering, //dk_och_water7,
			fmc_Cutting, //dk_och_cutting1,
			fmc_Cutting, //dk_och_cutting2,
			fmc_Cutting, //dk_och_cutting3,
			fmc_Cutting, //dk_och_cutting4,
			fmc_Cutting, //dk_och_cutting5,
			fmc_Cutting, //dk_och_cutting6,
			fmc_Cultivation, //dk_och_row_cultivation2,
			fmc_Fungicide, //dk_och_fungicide1,
			fmc_Fungicide, //dk_och_fungicide2,
			fmc_Insecticide, //dk_och_insecticide4,
			fmc_Insecticide, //dk_och_insecticide
			fmc_Insecticide, //dk_och_insecticide
			fmc_Insecticide, //dk_och_insecticide
			fmc_Insecticide, //dk_och_insecticide
			fmc_Insecticide, //dk_och_insecticide
			fmc_Insecticide, //dk_och_insecticide
			fmc_Herbicide, //dk_och_herbicide3
			fmc_Herbicide, //dk_och_herbicide4
			fmc_Herbicide, //dk_och_herbicide5
			fmc_Fungicide, //dk_och_fungicide3,
			fmc_Harvest, //dk_och_harvest,
			fmc_Fungicide, //dk_och_fungicide4,
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Fungicide, //dk_och_fungicide
			fmc_Others, // dk_och_gr1
			fmc_Others, // dk_och_gr2
			fmc_Others, // dk_och_gr3
			fmc_Others, // dk_och_gr4
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OrchCherry_H

