/**
\file
\brief
<B>PTWinterRye.h This file contains the headers for the PTWinterRye class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTWinterRye.h
//


#ifndef PTWINTERRYE_H
#define PTWINTERRYE_H

#define PTWINTERRYE_BASE 32000
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing Winter Rye, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_wr_start = 1, // Compulsory, must always be 1 (one).
	pt_wr_sleep_all_day = PTWINTERRYE_BASE,
	pt_wr_stubble_harrow,
	pt_wr_ferti_p1, // slurry
	pt_wr_ferti_s1,
	pt_wr_ferti_p2, // NPK
	pt_wr_ferti_s2,
	pt_wr_ferti_p3, // Calcium
	pt_wr_ferti_s3,
	pt_wr_ferti_p4, // NPK
	pt_wr_ferti_s4,
	pt_wr_autumn_harrow,
	pt_wr_autumn_sow,
	pt_wr_preseeding_cultivator_sow,
	pt_wr_harvest1,
	pt_wr_wait,
	pt_wr_harvest2,

} PTWinterRyeToDo;


/**
\brief
PTWinterRye class
\n
*/
/**
See PTWinterRye.h::PTWinterRyeToDo for a complete list of all possible events triggered codes by the Winter Rye management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTWinterRye: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTWinterRye(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,11 );
   }
};

#endif // PTWINTERRYE_H

