//
// DE_OMaize.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DE_OMAIZE_H
#define DE_OMAIZE_H

#define DE_OMAIZE_BASE 37100
#define DE_OMAIZE_SOW_DATE m_field->m_user[0]

typedef enum {
  de_om_start = 1, // Compulsory, start event must always be 1 (one).
  de_om_sleep_all_day = DE_OMAIZE_BASE,
  de_om_fa_manure,
  de_om_fp_manure,
  de_om_fa_slurry_one,
  de_om_fp_slurry_one,
  de_om_spring_plough,
  de_om_spring_harrow,
  de_om_spring_sow,
  de_om_strigling,
  de_om_row_one,
  de_om_fa_slurry_two,
  de_om_fp_slurry_two,
  de_om_row_two,
  de_om_row_three,
  de_om_water_one,
  de_om_water_two,
  de_om_harvest,
  de_om_stubble,
  de_om_foobar,
} DE_OMaizeToDo;



class DE_OMaize: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OMaize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
	  m_first_date = g_date->DayInYear(25, 4);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_om_foobar - DE_OMAIZE_BASE);
	  m_base_elements_no = DE_OMAIZE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_om_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others,	//	  de_om_sleep_all_day = DE_OMAIZE_BASE,
			fmc_Fertilizer,	//	  de_om_fa_manure_b,
			fmc_Fertilizer,	//	  de_om_fp_manure_b,
			fmc_Fertilizer,	//	  de_om_fa_slurry_one,
			fmc_Fertilizer,	//	  de_om_fp_slurry_one,
			fmc_Cultivation,	//	  de_om_spring_plough,
			fmc_Cultivation,	//	  de_om_spring_harrow,
			fmc_Others,	//	  de_om_spring_sow,
			fmc_Cultivation,	//	  de_om_strigling,
			fmc_Cultivation,	//	  de_om_row_one,
			fmc_Fertilizer,	//	  de_om_fa_slurry_two,
			fmc_Fertilizer,	//	  de_om_fp_slurry_two,
			fmc_Cultivation,	//	  de_om_row_two,
			fmc_Cultivation,	//	  de_om_row_three,
			fmc_Others,	//	  de_om_water_one,
			fmc_Others,	//	  de_om_water_two,
			fmc_Harvest,	//	  de_om_harvest,
			fmc_Cultivation,	//	  de_om_stubble,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DE_OMAIZE_H
