//
// DK_PotatoIndustry.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_PotatoIndustry.h"

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_potatoes_on;
extern CfgFloat cfg_pest_product_1_amount;


bool DK_PotatoIndustry::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DKPotatoIndustry;

	switch (m_ev->m_todo)
	{
	case dk_pi_start:
	{
		a_field->ClearManagementActionSum();
		m_field->SetVegPatchy(true); // Root crop so is open until tall
		DK_PI_FORCESPRING = false;
		m_last_date = g_date->DayInYear(24, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(3 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(25, 9); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use - 31/10
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(12, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - 1/11
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(13, 10); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - 2/11 
		flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
		flexdates[3][1] = g_date->DayInYear(14, 10); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) - 3/11

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(dk_pi_deep_harrow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_ev->m_forcespring) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_pi_deep_harrow, false);
			DK_PI_FORCESPRING = true;
			break;
		}
		else SimpleEvent(d1, dk_pi_remove_straw, false);
		break;
	}
	break;

	case dk_pi_remove_straw:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80)) {
			if (!m_farm->StrawRemoval(m_field, 0.0, g_date->DayInYear(28, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_remove_straw, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_stoneburier, false);
		break;

	case dk_pi_stoneburier:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40)) {
			if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(29, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_stoneburier, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_autumn_plough, false);
		break;

	case dk_pi_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40)) {
			if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_autumn_plough, true);
				break;
			}
			if (a_farm->IsStockFarmer()) {
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, dk_pi_ferti_s1, false);
				break;
			}
			else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, dk_pi_ferti_p1, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_pi_herbicide, false);
		break;

	case dk_pi_herbicide:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_herbicide, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_deep_harrow, false);
		break;

	case dk_pi_deep_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_deep_harrow, true);
			break;
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_pi_ferti_s1, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_pi_ferti_p1, false);
		break;

	case dk_pi_ferti_s1:
		if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(19, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_ferti_s1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_sow, false); // main thread
		SimpleEvent(g_date->Date() + 15, dk_pi_ferti_s2, false); // ferti thread
		break;

	case dk_pi_ferti_p1:
		if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(19, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_ferti_p1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_sow, false); // main thread
		SimpleEvent(g_date->Date() + 15, dk_pi_ferti_p2, false); // ferti thread
		break;

	case dk_pi_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_sow, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_strigling, false);
		break;
		
	case dk_pi_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_ferti_s2, true);
				break;
			}
		}
		break; // end of thread

	case dk_pi_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_ferti_p2, true);
				break;
			}
		}
		break; // end of thread

	case dk_pi_strigling:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.05)) {
			if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(27, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_strigling, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_pi_harrow1, false);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_herbicide1, false); // herbi thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_pi_insecticide1, false); // insecti1 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_pi_water, false); // water thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_pi_fungicide1, false); // main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_pi_insecticide3, false); // insecti3 thread
		break;

	case dk_pi_harrow1:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_harrow1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_herbicide_mw, false); //the 5% doing mechanical weeding (mw) - 75% of those will have one herbi treatment before sprouts
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_pi_hill_up1, false); // hill up thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_pi_insecticide1, false); // insecti1 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_pi_water, false); // water thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_pi_fungicide1, false); // main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_pi_insecticide3, false); // insecti3 thread
		break;

	case dk_pi_hill_up1:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_hill_up1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 30, dk_pi_hill_up2, false); // hill up thread
		break;

	case dk_pi_hill_up2:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_hill_up2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 30, dk_pi_hill_up3, false); // hill up thread
		break;

	case dk_pi_hill_up3:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_hill_up3, true);
			break;
		}
		break; // end of thread

	case dk_pi_herbicide_mw:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_herbicide_mw, true);
				break;
			}
		}
		break; // end of thread
	
	case dk_pi_herbicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.85)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_herbicide1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_pi_herbicide2, false);
		break;

	case dk_pi_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_herbicide2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_pi_herbicide3, false);
		break;

	case dk_pi_herbicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_herbicide3, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_pi_herbicide4, false);
		break;

	case dk_pi_herbicide4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_herbicide4, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_herbicide5, false);
		break;

	case dk_pi_herbicide5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_herbicide5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_herbicide6, false);
		break;

	case dk_pi_herbicide6:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_herbicide6, true);
				break;
			}
		}
		break; // end of thread

	case dk_pi_insecticide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(15, 6) - g_date->DayInYear();
			if (!cfg_pest_potatoes_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_pi_insecticide1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 30, dk_pi_insecticide2, false);
		break;

	case dk_pi_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(16, 7) - g_date->DayInYear();
			if (!cfg_pest_potatoes_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_pi_insecticide2, true);
				break;
			}
		}
		break; // end of thread

	case dk_pi_insecticide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(30, 6) - g_date->DayInYear();
			if (!cfg_pest_potatoes_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_pi_insecticide3, true);
				break;
			}
		}
		break; // end of thread

	case dk_pi_water:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_water, true);
				break;
			}
		}
		break; // end of thread

	case dk_pi_fungicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(29, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide1, true);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date(), dk_pi_ferti_s3, false);
			break;
		}
		else SimpleEvent(g_date->Date(), dk_pi_ferti_p3, false);
		break;

	case dk_pi_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.01)) {
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(29, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_ferti_s3, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide2, false);
		break;

	case dk_pi_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.01)) {
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(29, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_ferti_p3, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide2, false);
		break;

	case dk_pi_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(6, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide3, false);
		break;

	case dk_pi_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide3, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide4, false);
		break;

	case dk_pi_fungicide4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(22, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide4, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide5, false);
		break;

	case dk_pi_fungicide5:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide6, false);
		break;

	case dk_pi_fungicide6:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(6, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide6, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide7, false);
		break;

	case dk_pi_fungicide7:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide7, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide8, false);
		break;

	case dk_pi_fungicide8:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(22, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide8, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide9, false);
		break;

	case dk_pi_fungicide9:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide9, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide10, false);
		break;

	case dk_pi_fungicide10:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(6, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide10, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide11, false);
		break;

	case dk_pi_fungicide11:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide11, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide12, false);
		break;

	case dk_pi_fungicide12:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(22, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide12, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide13, false);
		break;

	case dk_pi_fungicide13:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide13, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_fungicide14, false);
		break;

	case dk_pi_fungicide14:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(6, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_fungicide14, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 7, dk_pi_harvest, false);
		break;

	case dk_pi_harvest:
		if (!m_farm->HarvestLong(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_harvest, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_harrow2, false);
		break;
		m_field->SetVegPatchy(false);
		done = true;
		break;

	case dk_pi_harrow2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
			if (!m_farm->AutumnHarrow(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_harrow2, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_pi_harrow3, false);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_herbicide7, false);
		break;

	case dk_pi_harrow3:
		if (!m_farm->AutumnHarrow(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_pi_harrow3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_pi_herbicide7, false);
		break;

	case dk_pi_herbicide7:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_pi_herbicide7, true);
				break;
			}
		}
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "DK_PotatoIndustry::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}





	


