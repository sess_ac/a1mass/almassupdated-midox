//
// DE_OSpringRye.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GosrDS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DEOSpringRye_H
#define DEOSpringRye_H

#define DE_OSPRINGRYE_BASE 37600

#define DE_OSR_SOW_DATE          m_field->m_user[0]
#define DE_OSR_DID_STRIGLING_ONE m_field->m_user[1]

typedef enum {
	de_osr_start = 1, // Compulsory, start event must always be 1 (one).
	de_osr_sleep_all_day = DE_OSPRINGRYE_BASE,
	de_osr_autumn_plough,
	de_osr_fertmanure_stock,
	de_osr_spring_plough,
	de_osr_spring_harrow,
	de_osr_fertslurry_stock,
	de_osr_spring_sow,
	de_osr_spring_roll,
	de_osr_strigling_one,
	de_osr_strigling_two,
	de_osr_strigling_three,
	de_osr_harvest,
	de_osr_straw_chopping,
	de_osr_hay_bailing,
	de_osr_foobar,
	// --FN--
} DEosrToDo;



class DE_OSpringRye : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_OSpringRye(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(30, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_osr_foobar - DE_OSPRINGRYE_BASE);
		m_base_elements_no = DE_OSPRINGRYE_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  de_osr_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Others, //   de_osr_sleep_all_day = DE_OSPRINGRYE_BASE, 
			  fmc_Cultivation,	//	  de_osr_autumn_plough,
			  fmc_Fertilizer,	//	  de_osr_fertmanure_stock,
			  fmc_Cultivation,	//	  de_osr_spring_plough,
			  fmc_Cultivation,	//	  de_osr_spring_harrow,
			  fmc_Fertilizer,	//	  de_osr_fertslurry_stock,
			  fmc_Others,	//	  de_osr_spring_sow,
			  fmc_Cultivation,	//	  de_osr_spring_roll,
			  fmc_Cultivation,	//	  de_osr_strigling_one,
			  fmc_Cultivation,	//	  de_osr_strigling_two,
			  fmc_Cultivation,	//	  de_osr_strigling_three,
			  fmc_Harvest,	//	  de_osr_harvest,
			  fmc_Cutting,	//	  de_osr_straw_chopping,
			  fmc_Others,	//	  de_osr_hay_bailing,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_OSpringRye_H
