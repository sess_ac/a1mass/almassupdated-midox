/**
\file
\brief
<B>UKPotatoes.h This file contains the headers for the Potatoes class</B> \n
*/
/**
\file
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKPotatoes.h
//


#ifndef UKPOTATOES_H
#define UKPOTATOES_H

#define UKPOTATOES_BASE 45400
/**
\brief A flag used to indicate autumn ploughing status
*/
#define UK_POT_STUBBLE_HARROW		a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing potatoes, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	uk_pot_start = 1, // Compulsory, must always be 1 (one).
	uk_pot_sleep_all_day = UKPOTATOES_BASE,
	uk_pot_stubble_harrow, // Stubble cultivation 100% September-October
	uk_pot_winter_plough, // Winter plough 50% October-December
	uk_pot_ferti_p1, // Slurry 100% March-April (if winter plough)
	uk_pot_ferti_s1,
	uk_pot_spring_plough, // Spring plough 50% March-April (if no winter plough)
	uk_pot_ferti_p2, // NPK with sow 75% April
	uk_pot_ferti_s2,
	uk_pot_bed_forming, // Bed forming / Preseeding cultivation 100% April-May
	uk_pot_spring_planting, // Planting 100% April-May
	uk_pot_ferti_p3, // N 75% June
	uk_pot_ferti_s3,
	uk_pot_ferti_p4, // MG+S 25% June
	uk_pot_ferti_s4,
	uk_pot_hilling1, // Hilling 100% 2 weeks after planting 
	uk_pot_herbicide1, // Herbicide1 80% after Hilling
	uk_pot_herbicide2, // Herbicide2 50% 2-4 weeks after Herbicide1
	uk_pot_fungicide1, // Fungicide1 100% June 1-7
	uk_pot_fungicide2, // Fungicide2 100% June 8-14
	uk_pot_fungicide3, // Fungicide3 100% June 15-21
	uk_pot_fungicide4, // Fungicide4 100% June 22-29
	uk_pot_fungicide5, // Fungicide5 100% July 1-7
	uk_pot_fungicide6, // Fungicide6 100% July 8-14
	uk_pot_fungicide7, // Fungicide7 100% July 15-21
	uk_pot_fungicide8, // Fungicide8 80% July 22-28
	uk_pot_fungicide9, // Fungicide9 80% August 1-7
	uk_pot_fungicide10, // Fungicide10 70% August 8-14
	uk_pot_fungicide11, // Fungicide11 70% August 15-21
	uk_pot_fungicide12, // Fungicide12 60% August 22-29
	uk_pot_fungicide13, // Fungicide13 60% September 1-7
	uk_pot_fungicide14, // Fungicide14 60% September 8-14
	uk_pot_insecticide1, // Insecticide1 60% June
	uk_pot_dessication1, // Dessication1 100% 14 days before Harvest
	uk_pot_dessication2, // Dessication2 75% 4-6 days after Dessication 1
	uk_pot_harvest, // Harvest 100% October
	uk_pot_foobar,
} UKPotatoesToDo;


/**
\brief
UKPotatoes class
\n
*/
/**
See UKPotatoes.h::UKPotatoesToDo for a complete list of all possible events triggered codes by the potatoes management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKPotatoes: public Crop
{
 public:
	 virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	 UKPotatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 2,11 );
		SetUpFarmCategoryInformation();
	 }
	 void SetUpFarmCategoryInformation() {
		 const int elements = 2 + (uk_pot_foobar - UKPOTATOES_BASE);
		 m_base_elements_no = UKPOTATOES_BASE - 2;
		 FarmManagementCategory catlist[elements] =
		 {
			  fmc_Others, // this needs to be at the zero line
			  fmc_Others,//uk_pot_start = 1, // Compulsory, must always be 1 (one).
			  fmc_Others,//uk_pot_sleep_all_day = UKPOTATOES_BASE,
			  fmc_Cultivation,//uk_pot_stubble_harrow, // Stubble cultivation 100% September-October
			  fmc_Cultivation,//uk_pot_winter_plough, // Winter plough 50% October-December
			  fmc_Fertilizer,//uk_pot_ferti_p1, // Slurry 100% March-April (if winter plough)
			  fmc_Fertilizer,//uk_pot_ferti_s1,
			  fmc_Cultivation,//uk_pot_spring_plough, // Spring plough 50% March-April (if no winter plough)
			  fmc_Fertilizer,//uk_pot_ferti_p2, // NPK with sow 75% April
			  fmc_Fertilizer,//uk_pot_ferti_s2,
			  fmc_Cultivation,//uk_pot_bed_forming, // Bed forming / Preseeding cultivation 100% April-May
			  fmc_Others,//uk_pot_spring_planting, // Planting 100% April-May
			  fmc_Fertilizer,//uk_pot_ferti_p3, // N 75% June
			  fmc_Fertilizer,//uk_pot_ferti_s3,
			  fmc_Fertilizer,//uk_pot_ferti_p4, // MG+S 25% June
			  fmc_Fertilizer,//uk_pot_ferti_s4,
			  fmc_Cultivation,//uk_pot_hilling1, // Hilling 100% 2 weeks after planting 
			  fmc_Herbicide,//uk_pot_herbicide1, // Herbicide1 80% after Hilling
			  fmc_Herbicide,//uk_pot_herbicide2, // Herbicide2 50% 2-4 weeks after Herbicide1
			  fmc_Fungicide,//uk_pot_fungicide1, // Fungicide1 100% June 1-7
			  fmc_Fungicide,//uk_pot_fungicide2, // Fungicide2 100% June 8-14
			  fmc_Fungicide,//uk_pot_fungicide3, // Fungicide3 100% June 15-21
			  fmc_Fungicide,//uk_pot_fungicide4, // Fungicide4 100% June 22-29
			  fmc_Fungicide,//uk_pot_fungicide5, // Fungicide5 100% July 1-7
			  fmc_Fungicide,//uk_pot_fungicide6, // Fungicide6 100% July 8-14
			  fmc_Fungicide,//uk_pot_fungicide7, // Fungicide7 100% July 15-21
			  fmc_Fungicide,//uk_pot_fungicide8, // Fungicide8 80% July 22-28
			  fmc_Fungicide,//uk_pot_fungicide9, // Fungicide9 80% August 1-7
			  fmc_Fungicide,//uk_pot_fungicide10, // Fungicide10 70% August 8-14
			  fmc_Fungicide,//uk_pot_fungicide11, // Fungicide11 70% August 15-21
			  fmc_Fungicide,//uk_pot_fungicide12, // Fungicide12 60% August 22-29
			  fmc_Fungicide,//uk_pot_fungicide13, // Fungicide13 60% September 1-7
			  fmc_Fungicide,//uk_pot_fungicide14, // Fungicide14 60% September 8-14
			  fmc_Insecticide,//uk_pot_insecticide1, // Insecticide1 60% June
			  fmc_Herbicide,//uk_pot_dessication1, // Dessication1 100% 14 days before Harvest
			  fmc_Herbicide,//uk_pot_dessication2, // Dessication2 75% 4-6 days after Dessication 1
			  fmc_Harvest,//uk_pot_harvest, // Harvest 100% October
		 };
		 copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	 }
};

#endif // UKPOTATOES_H

