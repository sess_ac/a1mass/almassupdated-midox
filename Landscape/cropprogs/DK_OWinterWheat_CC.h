/**
\file
\brief
<B>DK_WinterWheat_CC.h This file contains the headers for the WinterWheat_CC class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// WinterWheat_CC.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OWINTERWHEAT_CC_H
#define DK_OWINTERWHEAT_CC_H

#define DK_OWWCC_BASE 611200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DK_OWWCC_AUTUMN_PLOUGH        a_field->m_user[1]
#define DK_OWWCC_CC       a_field->m_user[2]
#define DK_OWWCC_DECIDE_TO_FI         a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  dk_owwcc_start = 1, // Compulsory, must always be 1 (one).
  dk_owwcc_harvest = DK_OWWCC_BASE,
  dk_owwcc_autumn_harrow,
  dk_owwcc_autumn_plough,
  dk_owwcc_autumn_sow,
  dk_owwcc_slurry1_s,
  dk_owwcc_slurry1_p,
  dk_owwcc_slurry2_s,
  dk_owwcc_slurry2_p,
  dk_owwcc_water1,
  dk_owwcc_water2,
  dk_owwcc_straw_chopping,
  dk_owwcc_hay_baling,
  dk_owwcc_wait,
  dk_owwcc_foobar,
} DK_OWinterWheat_CCToDo;


/**
\brief
DK_OWinterWheat_CC class
\n
*/
/**
See DK_WinterWheat_CC.h::DK_WinterWheat_CCToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OWinterWheat_CC: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OWinterWheat_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,10 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_owwcc_foobar - DK_OWWCC_BASE);
	   m_base_elements_no = DK_OWWCC_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_owwcc_start = 1, // Compulsory, must always be 1 (one).
			fmc_Harvest,	//	  dk_owwcc_harvest = DK_OWWCC_BASE,
			fmc_Cultivation,	//	  dk_owwcc_autumn_harrow,
			fmc_Cultivation,	//	  dk_owwcc_autumn_plough,
			fmc_Others,	//	  dk_owwcc_autumn_sow,
			fmc_Fertilizer,	//	  dk_owwcc_slurry1_s,
			fmc_Fertilizer,	//	  dk_owwcc_slurry1_p,
			fmc_Fertilizer,	//	  dk_owwcc_slurry2_s,
			fmc_Fertilizer,	//	  dk_owwcc_slurry2_p,
			fmc_Watering,	//	  dk_owwcc_water1,
			fmc_Watering,	//	  dk_owwcc_water2,
			fmc_Others,	//	  dk_owwcc_straw_chopping,
			fmc_Others,	//	  dk_owwcc_hay_baling,
			fmc_Others, // dk_owwcc_wait,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OWINTERWHEAT_CC_H

