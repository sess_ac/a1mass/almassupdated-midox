/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University, 
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_WinterRape.cpp This file contains the source for the DE_WinterRape class</B> \n
*/
/**
\file
 by Chris J. Topping, Elzbieta Ziolkowska \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_WinterRape.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_WinterRape.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_winterrape_on;
extern CfgFloat cfg_pest_product_1_amount;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_WinterRape::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEWinterRape; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_wr_start:
	{
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(15, 8); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(10, 8); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(15, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[2][1] = g_date->DayInYear(15, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 2)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_wr_herbicide3))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(10, 7) + isSpring;
		// OK, let's go.
		SimpleEvent_(d1, de_wr_autumn_harrow1, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case de_wr_autumn_harrow1:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wr_autumn_harrow1, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 8)) { 
			d1 = g_date->OldDays() + g_date->DayInYear(1, 8);	
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_wr_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_wr_ferti_p1, false, m_farm, m_field);
		break;
	case de_wr_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_wr_autumn_plough, false, m_farm, m_field);
		break;
	case de_wr_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_wr_autumn_plough, false, m_farm, m_field);
		break;
	case de_wr_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_autumn_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				// Queue up the next event and if it's late (after 15/8) it has to be rolling, else next fertilizer
				d1 = g_date->OldDays() + g_date->DayInYear(15, 8);
				if (g_date->Date() > d1)
				{
					SimpleEvent_(g_date->Date() + 1, de_wr_autumn_roll, true, m_farm, m_field);
					break;
				}
				else
				{
					if (m_farm->IsStockFarmer()) //Stock Farmer
					{
						SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 8), de_wr_ferti_s2, false, m_farm, m_field);
					}
					else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 8), de_wr_ferti_p2, false, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent_(g_date->Date(), de_wr_stubble_cultivator_heavy, false, m_farm, m_field); 
		break;
	case de_wr_autumn_roll:
		if (!m_farm->AutumnRoll(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wr_autumn_roll, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 8), de_wr_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 8), de_wr_ferti_p2, false, m_farm, m_field);
		break;
	case de_wr_stubble_cultivator_heavy:
		// the rest 60% who did not plough do heavy stubble cultivation
		if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wr_stubble_cultivator_heavy, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 8), de_wr_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 8), de_wr_ferti_p2, false, m_farm, m_field);
		break;
	case de_wr_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->FP_K(m_field, 0.0, g_date->DayInYear(3, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_wr_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_wr_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->FA_K(m_field, 0.0, g_date->DayInYear(3, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_wr_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_wr_preseeding_cultivator:
		// 90% will do preseeding cultivation, the rest will do only harrowing
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(4, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_preseeding_cultivator, true, m_farm, m_field);
				break;
			}
			d1 = g_date->Date() + 1;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 8)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 8);
			}
			SimpleEvent_(d1, de_wr_autumn_sow, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_wr_autumn_harrow2, false, m_farm, m_field);
		break;
	case de_wr_autumn_harrow2:
		// 10% will do only harrowing
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(4, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wr_autumn_harrow2, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 8)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 8);
		}
		SimpleEvent_(d1, de_wr_autumn_sow, false, m_farm, m_field);
		break;
	case de_wr_autumn_sow:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(5, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wr_autumn_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 1, de_wr_herbicide1, false, m_farm, m_field); // Herbidide thread = MAIN THREAD
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 9);
		}
		SimpleEvent_(d1, de_wr_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(d1 + m_date_modifier, de_wr_insecticide1, false, m_farm, m_field);	// Insecticide thread
		SimpleEvent_(d1, de_wr_molluscicide, false, m_farm, m_field);	// Snail thread; should be with biomass>0 condition
		if (m_farm->IsStockFarmer()) // N thread
		{
			SimpleEvent_(d1 = g_date->Date() + 10, de_wr_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(d1 = g_date->Date() + 10, de_wr_ferti_p3, false, m_farm, m_field);
		if (m_farm->IsStockFarmer()) // Microelements thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 9), de_wr_ferti_s5, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 9), de_wr_ferti_p5, false, m_farm, m_field);
		break;
	case de_wr_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_p3, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_wr_ferti_p3, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wr_ferti_p4, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wr_ferti_p4, false, m_farm, m_field);
		break;
	case de_wr_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_s3, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_wr_ferti_s3, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wr_ferti_s4, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wr_ferti_s4, false, m_farm, m_field);
		break;
	case de_wr_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wr_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wr_ferti_p5:
		// Here comes the microelements thread
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_p5, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wr_ferti_s5:
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_ferti_s5, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wr_herbicide1: // The first of the pesticide managements.
		// Here comes the herbicide thread
		if (m_ev->m_lock || m_farm->DoIt(95))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_herbicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, de_wr_herbicide2, false, m_farm, m_field);
		break;
	case de_wr_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt(30))
		{
			if (m_field->GetGreenBiomass() <= 0) {	// spraying should be after the rape emergence
				SimpleEvent_(g_date->Date() + 1, de_wr_herbicide2, true, m_farm, m_field);
				break;
			}
			else
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_wr_herbicide2, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3) + 365, de_wr_herbicide3, false, m_farm, m_field);
		break;
	case de_wr_herbicide3:
		if (m_ev->m_lock || m_farm->DoIt(99))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_herbicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 7), de_wr_harvest, false, m_farm, m_field); 
		break;
	case de_wr_fungicide1:
		// Here comes the fungicide thread
		if (m_ev->m_lock || m_farm->DoIt(99))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_fungicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_wr_fungicide2, false, m_farm, m_field);
		break;
	case de_wr_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt(99))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 30, de_wr_fungicide3, false, m_farm, m_field);
		break;
	case de_wr_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wr_insecticide1:
		// Here comes the insecticide thread
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_wr_insecticide1, true, m_farm, m_field);
			}
			else
			{
				// here we check whether we are using ERA pesticide or not
				d1 = g_date->DayInYear(5, 10) - g_date->DayInYear();
				if (!cfg_pest_winterrape_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_wr_insecticide1, true, m_farm, m_field);
					break;
				}
				SimpleEvent_(d1 = g_date->Date() + 14, de_wr_insecticide2, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(d1 = g_date->Date() + 14, de_wr_insecticide2, false, m_farm, m_field);
		break;
	case de_wr_insecticide2:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(5, 10) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt(10))
		{
			if (!cfg_pest_winterrape_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_wr_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, de_wr_insecticide3, false, m_farm, m_field);
		break;
	case de_wr_insecticide3:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(5, 10) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt(5))
		{
			if (!cfg_pest_winterrape_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_wr_insecticide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365 + m_date_modifier, de_wr_insecticide4, false, m_farm, m_field);
		break;
	case de_wr_insecticide4:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(30, 3) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (!cfg_pest_winterrape_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_wr_insecticide4, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4) + m_date_modifier) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4) + m_date_modifier;
		}
		SimpleEvent_(d1, de_wr_insecticide5, false, m_farm, m_field);
		break;
	case de_wr_insecticide5:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(30, 4) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!cfg_pest_winterrape_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_wr_insecticide5, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wr_molluscicide:
		if (m_ev->m_lock || m_farm->DoIt(30))
		{
			if (m_field->GetGreenBiomass() <= 0) {	// spraying should be after the rape emergence
				SimpleEvent_(g_date->Date() + 1, de_wr_molluscicide, true, m_farm, m_field);
				break;
			}
			else
			{
				if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_wr_molluscicide, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case de_wr_harvest:
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wr_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date(), de_wr_straw_chop, false, m_farm, m_field);
		break;

	case de_wr_straw_chop:
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wr_straw_chop, true, m_farm, m_field);
				break;
			}
			done = true;
			break;
		}
		d1 = g_date->Date() + 1;
		SimpleEvent_(d1, de_wr_haybailing, false, m_farm, m_field);
		break;

	case de_wr_haybailing:
		if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wr_haybailing, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_WinterRape::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}
