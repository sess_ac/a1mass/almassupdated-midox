//
// DK_Carrots.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_CARROTS_H
#define DK_CARROTS_H

#define DK_CAR_BASE 60600
#define DK_CAR_FORCESPRING	a_field->m_user[1]

typedef enum {
  dk_car_start = 1, // Compulsory, start event must always be 1 (one).
  dk_car_stoneburier = DK_CAR_BASE,
  dk_car_herbicide1, 
  dk_car_deep_harrow,
  dk_car_bedformer,
  dk_car_sharrow1,
  dk_car_sharrow2,
  dk_car_sharrow3,
  dk_car_sharrow4,
  dk_car_water1,
  dk_car_fungicide1,
  dk_car_sow,
  dk_car_herbicide2,
  dk_car_herbicide3,
  dk_car_ferti_s1,
  dk_car_ferti_p1,
  dk_car_herbicide4,
  dk_car_row_cultivation1,
  dk_car_herbicide5,
  dk_car_row_cultivation2,
  dk_car_water2,
  dk_car_hilling_up1,
  dk_car_herbicide6,
  dk_car_boron_s1,
  dk_car_boron_p1,
  dk_car_water3,
  dk_car_hilling_up2,
  dk_car_boron_s2,
  dk_car_boron_p2,
  dk_car_hilling_up3,
  dk_car_ferti_s2,
  dk_car_ferti_p2,
  dk_car_insecticide1,
  dk_car_insecticide2,
  dk_car_fungicide2,
  dk_car_fungicide3,
  dk_car_harvest,
  dk_car_wait,
  dk_car_foobar,
} DK_CarrotsToDo;

class DK_Carrots: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_Carrots(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L) {
    m_first_date=g_date->DayInYear(1,12);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_car_foobar - DK_CAR_BASE);
	  m_base_elements_no = DK_CAR_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		    fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	 dk_car_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation,	//	  dk_car_stoneburier = DK_CAR_BASE,
			fmc_Herbicide,	//	  dk_car_herbicide1, 
			fmc_Cultivation,	//	  dk_car_deep_harrow,
			fmc_Cultivation,	//	  dk_car_bedformer,
			fmc_Cultivation,	//	  dk_car_sharrow1,
			fmc_Cultivation,	//	  dk_car_sharrow2,
			fmc_Cultivation,	//	  dk_car_sharrow3,
			fmc_Cultivation,	//	  dk_car_sharrow4,
			fmc_Watering,	//	  dk_car_water1,
			fmc_Fungicide,	//	  dk_car_fungicide1,
			fmc_Others,	//	  dk_car_sow,
			fmc_Herbicide,	//	  dk_car_herbicide2,
			fmc_Herbicide,	//	  dk_car_herbicide3,
			fmc_Fertilizer,	//	  dk_car_ferti_s1,
			fmc_Fertilizer,	//	  dk_car_ferti_p1,
			fmc_Herbicide,	//	  dk_car_herbicide4,
			fmc_Cultivation,	//	  dk_car_row_cultivation1,
			fmc_Herbicide,	//	  dk_car_herbicide5,
			fmc_Cultivation,	//	  dk_car_row_cultivation2,
			fmc_Watering,	//	  dk_car_water2,
			fmc_Cultivation,	//	  dk_car_hilling_up1,
			fmc_Herbicide,	//	  dk_car_herbicide6,
			fmc_Fertilizer,	//	  dk_car_boron_s1,
			fmc_Fertilizer,	//	  dk_car_boron_p1,
			fmc_Watering,	//	  dk_car_water3,
			fmc_Cultivation,	//	  dk_car_hilling_up2,
			fmc_Fertilizer,	//	  dk_car_boron_s2,
			fmc_Fertilizer,	//	  dk_car_boron_p2,
			fmc_Cultivation,	//	  dk_car_hilling_up3,
			fmc_Fertilizer,	//	  dk_car_ferti_s2,
			fmc_Fertilizer,	//	  dk_car_ferti_p2,
			fmc_Insecticide,	//	  dk_car_insecticide1,
			fmc_Insecticide,	//	  dk_car_insecticide2,
			fmc_Fungicide,	//	  dk_car_fungicide2,
			fmc_Fungicide,	//	  dk_car_fungicide3,
			fmc_Harvest,	//	  dk_car_harvest,
			fmc_Others	//	  dk_car_wait,

			   // no foobar entry			

	  };
	  // Iterate over the catlist elements and copy them to vector						
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};


#endif // DK_CARROTS_H
