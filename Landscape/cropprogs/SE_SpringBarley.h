/**
\file
\brief
<B>SE_SpringBarley.h This file contains the headers for the SpringBarley class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of March 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// SE_SpringBarley.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SE_SPRINGBARLEY_H
#define SE_SPRINGBARLEY_H

#define SE_SB_BASE 80000
/**
\brief A flag used to indicate autumn ploughing status
*/


/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	se_sb_start = 1, // Compulsory, must always be 1 (one).
	se_sb_straw_removal = SE_SB_BASE,
	se_sb_herbicide1,
	se_sb_slurry1,
	se_sb_autumn_plough,
	se_sb_stubble_cultivator1,
	se_sb_stubble_cultivator2,
	se_sb_slurry2,
	se_sb_spring_plough,
	se_sb_spring_harrow1,
	se_sb_spring_sow,
	se_sb_spring_sow_w_ferti,
	se_sb_npk,
	se_sb_ns_manure,
	se_sb_spring_harrow2,
	se_sb_ns,
	se_sb_herbicide2,
	se_sb_fungicide,
	se_sb_insecticide,
	se_sb_harvest,
	se_sb_foobar,
} SE_SpringBarleyToDo;


/**
\brief
SE_SpringBarley class
\n
*/
/**
See SE_SpringBarley.h::SE_SpringBarleyToDo for a complete list of all possible events triggered codes by the SpringBarley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class SE_SpringBarley : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	SE_SpringBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(10, 10);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (se_sb_foobar - SE_SB_BASE);
		m_base_elements_no = SE_SB_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	se_sb_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	se_sb_straw_removal = SE_SB_BASE,
			fmc_Herbicide,	//	se_sb_herbicide1,
			fmc_Fertilizer,	//	se_sb_slurry1,
			fmc_Cultivation,	//	se_sb_autumn_plough,
			fmc_Cultivation,	//	se_sb_stubble_cultivator1,
			fmc_Cultivation,	//	se_sb_stubble_cultivator2,
			fmc_Fertilizer,	//	se_sb_slurry2,
			fmc_Cultivation,	//	se_sb_spring_plough,
			fmc_Cultivation,	//	se_sb_spring_harrow1,
			fmc_Others,	//	se_sb_spring_sow,
			fmc_Fertilizer,	//	se_sb_spring_sow_w_ferti,
			fmc_Fertilizer,	//	se_sb_npk,
			fmc_Fertilizer,	//	se_sb_ns_manure,
			fmc_Cultivation,	//	se_sb_spring_harrow2,
			fmc_Fertilizer,	//	se_sb_ns,
			fmc_Herbicide,	//	se_sb_herbicide2,
			fmc_Fungicide,	//	se_sb_fungicide,
			fmc_Insecticide,	//	se_sb_insecticide,
			fmc_Harvest	//	se_sb_harvest,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // SE_SPRINGBARLEY_H

