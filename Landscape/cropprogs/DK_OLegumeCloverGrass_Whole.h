//
// OLegumeCloverGrass_Whole.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved. 

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_OLegumeCloverGrass_Whole_H
#define DK_OLegumeCloverGrass_Whole_H
/** \brief A flag used to indicate do catch crop
*/
#define DK_OLCGW_BASE 64800

typedef enum {
  dk_olcgw_start = 1, // Compulsory, start event must always be 1 (one).
  dk_olcgw_harvest1 = DK_OLCGW_BASE,
  dk_olcgw_spring_harrow1,
  dk_olcgw_spring_harrow2,
  dk_olcgw_spring_plough,
  dk_olcgw_ks_ferti_s,
  dk_olcgw_ks_ferti_p,
  dk_olcgw_spring_harrow3,
  dk_olcgw_spring_sow,
  dk_olcgw_spring_roll,
  dk_olcgw_spring_sow_lo,
  dk_olcgw_strigling1,
  dk_olcgw_strigling2,
  dk_olcgw_water,
  dk_olcgw_spring_sow_mix,
  dk_olcgw_swathing,
  dk_olcgw_harvest,
  dk_olcgw_straw_chopping,
  dk_olcgw_hay_baling,
  dk_olcgw_cattle_out,
  dk_olcgw_cattle_is_out,
  dk_olcgw_wait,
  dk_olcgw_foobar,
} DK_OLegumeCloverGrass_WholeToDo;



class DK_OLegumeCloverGrass_Whole: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OLegumeCloverGrass_Whole(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(10,4);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_olcgw_foobar - DK_OLCGW_BASE);
	  m_base_elements_no = DK_OLCGW_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_olcgw_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_olcgw_harvest1 = DK_OLCGW_BASE,
			fmc_Cultivation,	//	  dk_olcgw_spring_harrow1,
			fmc_Cultivation,	//	  dk_olcgw_spring_harrow2,
			fmc_Cultivation,	//	  dk_olcgw_spring_plough,
			fmc_Fertilizer,	//	  dk_olcgw_ks_ferti,
			fmc_Fertilizer,	//	  dk_olcgw_ks_ferti,
			fmc_Cultivation,	//	  dk_olcgw_spring_harrow3,
			fmc_Others,	//	  dk_olcgw_spring_sow,
			fmc_Cultivation,	//	  dk_olcgw_spring_roll,
			fmc_Others,	//	  dk_olcgw_spring_sow_lo,
			fmc_Cultivation, // dk_olcgw_strigling1,
			fmc_Cultivation, // dk_olcgw_strigling2,
			fmc_Watering,	//	  dk_olcgw_water,
			fmc_Others,	//	  dk_olcgw_spring_sow_mix,
			fmc_Cutting,	//	  dk_olcgw_swathing,
			fmc_Harvest,	//	  dk_olcgw_harvest,
			fmc_Cutting,	//	  dk_olcgw_straw_chopping,
			fmc_Others,	//	  dk_olcgw_hay_baling,
			fmc_Grazing,	//	  dk_olcgw_cattle_out,
			fmc_Grazing,	//	  dk_olcgw_cattle_is_out,
			fmc_Others, // dk_olcgw_wait

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_OLegumeCloverGrass_Whole_H
