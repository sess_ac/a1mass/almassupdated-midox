//
// Maize.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef MAIZE_H
#define MAIZE_H

#define MAIZE_BASE 2000
#define MAIZE_SOW_DATE m_field->m_user[0]
#define MAIZE_HERBI_ONE_DATE m_field->m_user[1]

typedef enum {
  ma_start = 1, // Compulsory, start event must always be 1 (one).
  ma_fa_manure_a = MAIZE_BASE,
  ma_fa_manure_b,
  ma_autumn_plough,
  ma_fa_slurry_one,
  ma_spring_plough,
  ma_spring_harrow,
  ma_spring_sow,
  ma_fa_npk,
  ma_herbi_one,
  ma_row_one,
  ma_fa_slurry_two,
  ma_herbi_two,
  ma_row_two,
  ma_water_one,
  ma_water_two,
  ma_harvest,
  ma_stubble,
  ma_foobar,
} MaizeToDo;



class Maize: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  Maize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(15,10);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (ma_foobar - MAIZE_BASE);
	  m_base_elements_no = MAIZE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  ma_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  ma_fa_manure_a = MAIZE_BASE,
			fmc_Fertilizer,	//	  ma_fa_manure_b,
			fmc_Cultivation,	//	  ma_autumn_plough,
			fmc_Fertilizer,	//	  ma_fa_slurry_one,
			fmc_Cultivation,	//	  ma_spring_plough,
			fmc_Cultivation,	//	  ma_spring_harrow,
			fmc_Others,	//	  ma_spring_sow,
			fmc_Fertilizer,	//	  ma_fa_npk,
			fmc_Herbicide,	//	  ma_herbi_one,
			fmc_Cultivation,	//	  ma_row_one,
			fmc_Fertilizer,	//	  ma_fa_slurry_two,
			fmc_Herbicide,	//	  ma_herbi_two,
			fmc_Cultivation,	//	  ma_row_two,
			fmc_Watering,	//	  ma_water_one,
			fmc_Watering,	//	  ma_water_two,
			fmc_Harvest,	//	  ma_harvest,
			fmc_Cultivation	//	  ma_stubble,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // MAIZE_H
