//
// DE_OSugarbeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Susanne Stein, Julius-Kuehn-Institute
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DE_OSugarBeet_h
#define DE_OSugarBeet_h

#define DE_OSB_BASE 37700

#define DE_OSB_WINTER_PLOUGH		a_field->m_user[1]
#define DE_OSB_DECIDE_TO_HERB		m_field->m_user[2]
#define DE_OSB_DECIDE_TO_FI		 m_field->m_user[3]

typedef enum {
  de_osbe_start = 1, // Compulsory, start event must always be 1 (one).
  de_osbe_sleep_all_day = DE_OSB_BASE,
  de_osbe_ferti_p1, 
  de_osbe_ferti_s1,
  de_osbe_autumn_plough,
  de_osbe_ferti_s2,
  de_osbe_ferti_p2,
  de_osbe_spring_plough,
  de_osbe_sharrow1,
  de_osbe_sow,
  de_osbe_strigling,
  de_osbe_harrow,
  de_osbe_row_cultivation1,
  de_osbe_water1,
  de_osbe_water2,
  de_osbe_row_cultivation2,
  de_osbe_row_cultivation3,
  de_osbe_fertiFA_B,
  de_osbe_fertiFP_B,
  de_osbe_fertiFA_S,
  de_osbe_fertiFP_S,
  de_osbe_manualweeding1,
  de_osbe_manualweeding2,
  de_osbe_harvest,
  de_osbe_foobar,
} DE_OSugarBeetToDo;



class DE_OSugarBeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OSugarBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(20, 11); // 
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_osbe_foobar - DE_OSB_BASE);
	  m_base_elements_no = DE_OSB_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_osbe_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others, //		de_osbe_sleep_all_day = DE_OSB_BASE,
			fmc_Fertilizer,	//	  de_osbe_ferti_p1,
			fmc_Fertilizer,	//	  de_osbe_ferti_s1,
			fmc_Cultivation,	//	  de_osbe_autumn_plough,
			fmc_Fertilizer,	//	  de_osbe_ferti_s2,
			fmc_Fertilizer,	//	  de_osbe_ferti_p2,
			fmc_Cultivation,	//	  de_osbe_spring_plough,
			fmc_Cultivation,	//	  de_osbe_sharrow1,
			fmc_Others,	//	  de_osbe_sow,
			fmc_Cultivation,	//	  de_osbe_strigling,
			fmc_Cultivation,	//	  de_osbe_harrow,
			fmc_Cultivation,	//	  de_osbe_row_cultivation1,
			fmc_Watering,	//	  de_osbe_water1,
			fmc_Watering,	//	  de_osbe_water2,
			fmc_Cultivation,	//	  de_osbe_row_cultivation2,
			fmc_Cultivation,	//	  de_osbe_row_cultivation3,
			fmc_Fertilizer,	//	  de_osbe_fertiFA_B,
			fmc_Fertilizer,	//	  de_osbe_fertiFP_B,
			fmc_Fertilizer,	//	  de_osbe_fertiFA_S,
			fmc_Fertilizer,	//	  de_osbe_fertiFP_S,
			fmc_Cultivation,	//	  de_osbe_manualweeding1,
			fmc_Cultivation,	//	  de_osbe_manualweeding2,
			fmc_Harvest,	//	  de_osbe_harvest,

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DE_OSugarBeet_h
