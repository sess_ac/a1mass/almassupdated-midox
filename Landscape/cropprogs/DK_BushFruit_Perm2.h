/**
\file
\brief
<B>DK_BushFruit_Perm2.h This file contains the source for the DK_BushFruit_Perm2 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of January 2023 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_BushFruit_Perm2.h
//


#ifndef DK_BUSHFRUIT2_P_H // based on blackcurrant
#define DK_BUSHFRUIT2_P_H

#define DK_BFP2_AFTER_EST a_field->m_user[1]

#define DK_BFP2_BASE 62600

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_bfp2_start = 1, // Compulsory, must always be 1 (one).
	dk_bfp2_sleep_all_day = DK_BFP2_BASE,
	dk_bfp2_molluscicide1, //est autumn
	dk_bfp2_autumn_plough,
	dk_bfp2_autumn_harrow,
	dk_bfp2_herbicide1,
	dk_bfp2_ferti_s1,
	dk_bfp2_ferti_p1,
	dk_bfp2_plant,
	dk_bfp2_molluscicide2, // 1st year after est autumn
	dk_bfp2_ferti_s2,
	dk_bfp2_ferti_p2,
	dk_bfp2_insecticide1,
	dk_bfp2_fungicide1,
	dk_bfp2_herbicide2,
	dk_bfp2_fungicide2,
	dk_bfp2_ferti_s3,
	dk_bfp2_ferti_p3,
	dk_bfp2_row_cultivation1,
	dk_bfp2_manual_weeding1,
	dk_bfp2_row_cultivation2,
	dk_bfp2_manual_weeding2,
	dk_bfp2_row_cultivation3,
	dk_bfp2_manual_weeding3,
	dk_bfp2_herbicide3,
	dk_bfp2_molluscicide3,
	dk_bfp2_fungicide3,
	dk_bfp2_insecticide2,	
	dk_bfp2_fungicide4,
	dk_bfp2_fungicide5,
	dk_bfp2_fungicide6,
	dk_bfp2_water1,
	dk_bfp2_water1_clay,
	dk_bfp2_water2,
	dk_bfp2_water2_clay,
	dk_bfp2_molluscicide4, // harvest years (+10 years)
	dk_bfp2_ferti_s4,
	dk_bfp2_ferti_p4,
	dk_bfp2_insecticide3,
	dk_bfp2_fungicide7,
	dk_bfp2_herbicide4,
	dk_bfp2_fungicide8,
	dk_bfp2_cutting1,
	dk_bfp2_cutting2,
	dk_bfp2_cutting3,
	dk_bfp2_herbicide5,
	dk_bfp2_molluscicide5,
	dk_bfp2_fungicide9,
	dk_bfp2_insecticide4,
	dk_bfp2_fungicide10,
	dk_bfp2_fungicide11,
	dk_bfp2_fungicide12,
	dk_bfp2_water3,
	dk_bfp2_water3_clay,
	dk_bfp2_water4,
	dk_bfp2_water4_clay,
	dk_bfp2_harvest,
	dk_bfp2_ferti_s5,
	dk_bfp2_ferti_p5,
	dk_bfp2_pruning,
	dk_bfp2_insecticide5,
	dk_bfp2_wait,
	dk_bfp2_foobar,
} DK_BushFruit_Perm2ToDo;


/**
\brief
DK_BushFruit_Perm2 class
\n
*/
/**
See DK_BushFruit_Perm2.h::DK_BushFruit_Perm2ToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_BushFruit_Perm2: public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_BushFruit_Perm2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_bfp2_foobar - DK_BFP2_BASE);
	   m_base_elements_no = DK_BFP2_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_bfp2_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_bfp2_sleep_all_day = DK_BFP2_BASE,
			fmc_Others,	//	dk_bfp2_mollucicide1, //est autumn
			fmc_Cultivation,	//	dk_bfp2_autumn_plough,
			fmc_Cultivation,	//	dk_bfp2_autumn_harrow,
			fmc_Herbicide,	//	dk_bfp2_herbicide1,
			fmc_Fertilizer,	//	dk_bfp2_ferti_s1,
			fmc_Fertilizer,	//	dk_bfp2_ferti_p1,
			fmc_Others,	//	dk_bfp2_plant,
			fmc_Others,	//	dk_bfp2_molluscicide2, // 1st year after est autumn
			fmc_Fertilizer,	//	dk_bfp2_ferti_s2,
			fmc_Fertilizer,	//	dk_bfp2_ferti_p2,
			fmc_Insecticide,	//	dk_bfp2_insecticide1,
			fmc_Fungicide,	//	dk_bfp2_fungicide1,
			fmc_Herbicide,	//	dk_bfp2_herbicide2,
			fmc_Fungicide,	//	dk_bfp2_fungicide2,
			fmc_Fertilizer,	//	dk_bfp2_ferti_s3,
			fmc_Fertilizer,	//	dk_bfp2_ferti_p3,
			fmc_Cultivation,	//	dk_bfp2_row_cultivation1,
			fmc_Cultivation,	//	dk_bfp2_manual_weeding1,
			fmc_Cultivation,	//	dk_bfp2_row_cultivation2,
			fmc_Cultivation,	//	dk_bfp2_manual_weeding2,
			fmc_Cultivation,	//	dk_bfp2_row_cultivation3,
			fmc_Cultivation,	//	dk_bfp2_manual_weeding3,
			fmc_Herbicide,	//	dk_bfp2_herbicide3,
			fmc_Others,	//	dk_bfp2_mollusicide3,
			fmc_Fungicide,	//	dk_bfp2_fungicide3,
			fmc_Insecticide,	//	dk_bfp2_insecticide2,
			fmc_Fungicide,	//	dk_bfp2_fungicide4,
			fmc_Fungicide,	//	dk_bfp2_fungicide5,
			fmc_Fungicide,	//	dk_bfp2_fungicide6,
			fmc_Watering,	//	dk_bfp2_water1,
			fmc_Watering,	//	dk_bfp2_water1_clay,
			fmc_Watering,	//	dk_bfp2_water2,
			fmc_Watering,	//	dk_bfp2_water2_clay,
			fmc_Others,	//	dk_bfp2_molluscicide4, // harvest years (+10 years)
			fmc_Fertilizer,	//	dk_bfp2_ferti_s4,
			fmc_Fertilizer,	//	dk_bfp2_ferti_p4,
			fmc_Insecticide,	//	dk_bfp2_insecticide3,
			fmc_Fungicide,	//	dk_bfp2_fungicide7,
			fmc_Herbicide,	//	dk_bfp2_herbicide4,
			fmc_Fungicide,	//	dk_bfp2_fungicide8,
			fmc_Cutting,	//	dk_bfp2_cutting1,
			fmc_Cutting,	//	dk_bfp2_cutting2,
			fmc_Cutting,	//	dk_bfp2_cutting3,
			fmc_Herbicide,	//	dk_bfp2_herbicide5,
			fmc_Others,	//	dk_bfp2_mollusicide5,
			fmc_Fungicide,	//	dk_bfp2_fungicide9,
			fmc_Insecticide,	//	dk_bfp2_insecticide4,
			fmc_Fungicide,	//	dk_bfp2_fungicide10,
			fmc_Fungicide,	//	dk_bfp2_fungicide11,
			fmc_Fungicide,	//	dk_bfp2_fungicide13,
			fmc_Watering,	//	dk_bfp2_water3,
			fmc_Watering,	//	dk_bfp2_water3_clay,
			fmc_Watering,	//	dk_bfp2_water4,
			fmc_Watering,	//	dk_bfp2_water�4_clay,
			fmc_Harvest,	//	dk_bfp2_harvest,
			fmc_Fertilizer,	//	dk_bfp2_ferti_s5,
			fmc_Fertilizer,	//	dk_bfp2_ferti_p5,
			fmc_Cutting,	//	dk_bfp2_pruning,
			fmc_Insecticide,	//	dk_bfp2_insecticide5,
			fmc_Others, // dk_bfp2_wait,

				// no foobar entry			

	   };
	   // Iterate over the catlist elements and copy them to vector						
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};


#endif // DK_BushFruit_Perm2_H

