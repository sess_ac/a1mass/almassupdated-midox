/**
\file
\brief
<B>DE_OBushFruitPerm.h This file contains the source for the DE_OBushFruitPerm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2022 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DE_OBushFruitPerm.h
//

// EZ: This plan is the same as for conventional bush fruit but without any pesticides


#ifndef DE_OBUSHFRUIT_P_H
#define DE_OBUSHFRUIT_P_H

#define DE_OBFP_EARLY_HARVEST a_field->m_user[1]

#define DE_OBFP_BASE 39700
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_obfp_start = 1, // Compulsory, must always be 1 (one).
	de_obfp_sleep_all_day = DE_OBFP_BASE,
	de_obfp_cover_on,
	de_obfp_fertilizer1_s,
	de_obfp_fertilizer1_p,
	de_obfp_mow_margin1,
	de_obfp_mow_margin2,
	de_obfp_mow_margin3,
	de_obfp_water1,
	de_obfp_manual_weeding1,
	de_obfp_manual_weeding2,
	de_obfp_water2,
	de_obfp_harvest,
	de_obfp_mowing_fruit,
	de_obfp_fertilizer2_s,
	de_obfp_fertilizer2_p,
	de_obfp_water3,
	de_obfp_water4,
	de_obfp_water5,
	de_obfp_foobar,
} DE_OBushFruitPermToDo;


/**
\brief
DE_OBushFruitPerm class
\n
*/
/**
See DE_OBushFruitPerm.h::DE_OBushFruitPermToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_OBushFruitPerm: public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_OBushFruitPerm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_obfp_foobar - DE_OBFP_BASE);
	   m_base_elements_no = DE_OBFP_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	de_obfp_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	de_obfp_sleep_all_day = DK_BFP2_BASE,
			fmc_Others,	//	de_obfp_cover_on,
			fmc_Fertilizer,	//	de_obfp_fertilizer1_s,
			fmc_Fertilizer,	//	de_obfp_fertilizer1_p,
			fmc_Cutting, // de_obfp_mow_margin1,
			fmc_Cutting, // de_obfp_mow_margin2,
			fmc_Cutting, // de_obfp_mow_margin3,
			fmc_Watering,	//	de_obfp_water1,
			fmc_Cultivation, // de_obfp_manual_weeding1,
			fmc_Cultivation, // de_obfp_manual_weeding2,
			fmc_Watering,	//	de_obfp_water2,
			fmc_Harvest,	//	de_obfp_harvest,
			fmc_Cutting, // de_obfp_mowing_fruit,
			fmc_Fertilizer,	//	de_obfp_fertilizer2_s,
			fmc_Fertilizer,	//	de_obfp_fertilizer2_p,
			fmc_Watering,	//	de_obfp_water3,
		    fmc_Watering,	//	de_obfp_water4,
		    fmc_Watering,	//	de_obfp_water5,

				// no foobar entry			

	   };
	   // Iterate over the catlist elements and copy them to vector						
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};


#endif // DE_OBushFruitPerm_H

