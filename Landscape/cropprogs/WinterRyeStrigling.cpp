//
// WinterRyeStrigling.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WinterRyeStrigling.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgFloat cfg_strigling_prop;

bool WinterRyeStrigling::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  int d1=0;
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case wrys_start:
    {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(1,9);
      // Start and stop dates for all events after harvest
      int noDates=4;
      m_field->SetMDates(0,0,g_date->DayInYear(15,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(15,8));
      m_field->SetMDates(0,1,g_date->DayInYear(10,8)); // Subbleharrow start
      m_field->SetMDates(1,1,g_date->DayInYear(1,9)); // Stubbleharrow end
      m_field->SetMDates(0,2,g_date->DayInYear(3,8)); // alternative to 1st
      m_field->SetMDates(1,2,g_date->DayInYear(25,8));
      m_field->SetMDates(0,3,g_date->DayInYear(3,8)); // follows last one
      m_field->SetMDates(1,3,g_date->DayInYear(25,8));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary
    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "WinterRye::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      bool toggle=false;
      for (int i=0; i<noDates; i++) {
        if  (m_field->GetMDates(0,i)>=m_ev->m_startday) {
          toggle=true;
          m_field->SetMDates(0,i,m_ev->m_startday-1);
        }
        if  (m_field->GetMDates(1,i)>=m_ev->m_startday){
          toggle=true;
          m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      if (toggle) for (int i=0; i<10; i++) m_field->SetMConstants(i,0);
    }
      // Now no operations can be timed after the start of the next crop.

      // CJT note:
      // Start single block date checking code to be cut-'n-pasted...
      d1;
      if ( ! m_ev->m_first_year )
      {
	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "WinterRyeStrigling::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "WinterRyeStrigling::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        // Is the first year
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
             wrys_harvest, false );
        break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      // OK, let's go.

      if ( m_farm->IsStockFarmer())
      {
	d1 = g_date->OldDays() + g_date->DayInYear( 10,9 );
      }
      else
      {
	d1 = g_date->OldDays() + g_date->DayInYear( 15,9 );
      }
      if ( g_date->Date() > d1 )
      {
	d1 = g_date->Date();
      }

      if ( m_farm->IsStockFarmer()) {
	WRYS_DID_MANURE = false;
	WRYS_DID_SLUDGE = false;
	SimpleEvent( d1, wrys_fertmanure_stock, false );
	SimpleEvent( d1, wrys_fertsludge_stock, false );
      } else {
	SimpleEvent( d1, wrys_autumn_plough, false );
      }
    }
    break;

  case wrys_fertmanure_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 15 )) {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 10, 10 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_fertmanure_stock, true );
	break;
      }
    }
    WRYS_DID_MANURE = true;
    if ( WRYS_DID_SLUDGE ) {
      // We are the last thread.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,9 ),
		 wrys_autumn_plough, false );
    }
    break;

  case wrys_fertsludge_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) {
      if (!m_farm->FA_Sludge( m_field, 0.0,
			      g_date->DayInYear( 10, 10 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_fertsludge_stock, true );
	break;
      }
    }
    WRYS_DID_SLUDGE = true;
    if ( WRYS_DID_MANURE ) {
      // No, *we* are the last thread!
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,9 ),
		   wrys_autumn_plough, false );
    }
    break;

  case wrys_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 15, 10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrys_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), wrys_autumn_harrow, false );
    break;

  case wrys_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 15, 10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrys_autumn_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,10 ),
		 wrys_autumn_sow, false );
    break;

  case wrys_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
			    g_date->DayInYear( 1, 11 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrys_autumn_sow, true );
      break;
    }
    SimpleEvent( g_date->Date(), wrys_autumn_roll, false );
    break;

  case wrys_autumn_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
      if (!m_farm->AutumnRoll( m_field, 0.0,
			       g_date->DayInYear( 1, 11 ) -
			       g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_autumn_roll, true );
	break;
      }
    }

    if ( m_farm->IsStockFarmer()) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		   wrys_fertslurry_stock, false );
    } else {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 2,10 ),
		   wrys_fertmanganese_plant_one, false );
    }
    SimpleEvent( d1, wrys_strigling_one, false );
    break;

  case wrys_strigling_one:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 70 ))) {
      if (!m_farm->Strigling( m_field, 0.0,
                              g_date->DayInYear( 25, 10 ) -
                              g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrys_strigling_one, true );
        break;
      }
    }
    {
      d1 = g_date->Date() + 7;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 5,10 )) {
        d1 = g_date->OldDays() + g_date->DayInYear( 5,10 );
    }
      SimpleEvent( d1, wrys_strigling_two, false );
    }
    break;

  case wrys_strigling_two:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 15 ))) {
    if (!m_farm->Strigling( m_field, 0.0,
                          g_date->DayInYear( 2, 11 ) -
                          g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrys_strigling_two, true );
        break;
      }
    }
    // End of this strigling thread
    break;

  case wrys_fertmanganese_plant_one:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
      if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
					 g_date->DayInYear( 30, 10 ) -
					 g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_fertmanganese_plant_one, true );
	break;
      }
      // Did first application, then queue up second for next year.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		   wrys_fertmanganese_plant_two, false );
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,3 ) + 365,
		 wrys_fertnpk_plant, false );
    break;

  case wrys_fertmanganese_plant_two:
    if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
				       g_date->DayInYear( 5, 5 ) -
				       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrys_fertmanganese_plant_two, true );
      break;
    }
    // End of thread.
    break;

  case wrys_fertnpk_plant:
    if (!m_farm->FP_NPK( m_field, 0.0,
			 g_date->DayInYear( 30, 4 ) -
			 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrys_fertnpk_plant, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                 wrys_spring_roll, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                              wrys_strigling_three, false );
    break;

  case wrys_fertslurry_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 65 )) {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 30, 4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_fertslurry_stock, true );
	break;
      }
    }
    SimpleEvent( g_date->Date(), wrys_fert_ammonium_stock, false );
    break;

  case wrys_fert_ammonium_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
      if (!m_farm->FA_AmmoniumSulphate( m_field, 0.0,
			      g_date->DayInYear( 30, 4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_fert_ammonium_stock, true );
	break;
      }
    }
    SimpleEvent( g_date->Date(), wrys_spring_roll, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                                                 wrys_strigling_three, false );
    break;

  case wrys_strigling_three:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 60 ))) {
      if (!m_farm->Strigling( m_field, 0.0,
                            g_date->DayInYear( 15, 4 ) -
                            g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrys_strigling_three, true );
        break;
    }
  }
  // End of the last strigling thread
  break;

  case wrys_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if (!m_farm->SpringRoll( m_field, 0.0,
			       g_date->DayInYear( 25,4 ) -
			       g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_spring_roll, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,4 ),
                 wrys_growth_reg_one, false );
    break;

  case wrys_growth_reg_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (60*cfg_greg_app_prop.value() ))) {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				   g_date->DayInYear( 2,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_growth_reg_one, true );
	break;
      }
      // Did first application of growth regulator, so
      // queue up the second one too.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 11,5 ),
		   wrys_growth_reg_two, false );
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 25,4 );
      if ( d1 <= g_date->Date()) {
	d1 = g_date->Date() + 1;
      }
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
                   wrys_fungicide, false );
    }
    break;

  case wrys_growth_reg_two:
    if (!m_farm->GrowthRegulator( m_field, 0.0,
				  g_date->DayInYear( 25,5 ) -
				  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrys_growth_reg_two, true );
      break;
    }
    // End of thread.
    break;

  case wrys_fungicide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (50*cfg_fungi_app_prop1.value() ))) {
      if (!m_farm->FungicideTreat( m_field, 0.0,
				   g_date->DayInYear( 30,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_fungicide, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ),
		 wrys_insecticide, false );
    break;

  case wrys_insecticide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (10*cfg_ins_app_prop1.value() ))) {
      if (!m_farm->InsecticideTreat( m_field, 0.0,
				     g_date->DayInYear( 10,6 ) -
				     g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wrys_insecticide, true );
	break;
      }
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 1,6 );
      if ( d1 <= g_date->Date()) {
	d1 = g_date->Date() + 1;
      }
      SimpleEvent( d1, wrys_water, false );
    }
    break;

  case wrys_water:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 15,6 ) -
			  g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrys_water, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                  wrys_harvest, false );
    break;

  case wrys_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
			  g_date->DayInYear( 15,8 ) -
			  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrys_harvest, true );
      break;
    }
    SimpleEvent( g_date->Date(), wrys_straw_chopping, false );
    break;

  case wrys_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 60 )) {
      if (!m_farm->StrawChopping( m_field, 0.0,
				  m_field->GetMDates(1,0) -
				  g_date->DayInYear())) {
        SimpleEvent( g_date->Date()+1, wrys_straw_chopping, true );
        break;
      }
      // OK, did chop, so go directly to stubbles.
      SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
		   wrys_stubble_harrowing, false );
      break;
    }
    // No chopping, so do hay turning and bailing before stubbles.
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
		 wrys_hay_turning, false );
    break;

  case wrys_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 40 ))
    {
      if (!m_farm->HayTurning( m_field, 0.0,
			       m_field->GetMDates(1,2) -
			       g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrys_hay_turning, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,3),
                  wrys_hay_bailing, false );
    break;

  case wrys_hay_bailing:
    if (!m_farm->HayBailing( m_field, 0.0,
			     m_field->GetMDates(1,3) -
			     g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wrys_hay_bailing, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
                  wrys_stubble_harrowing, false );
    break;

  case wrys_stubble_harrowing:
    if ( m_ev->m_lock || m_farm->DoIt( 30 ))
    {
      if (!m_farm->StubbleHarrowing( m_field, 0.0,
				     m_field->GetMDates(1,1) -
				     g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wrys_stubble_harrowing, true );
        break;
      }
    }
    // End of main thread.
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "WinterRye::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


