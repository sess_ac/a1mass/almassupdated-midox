/**
\file
\brief
<B>DK_OrchPear.h This file contains the source for the DK_OrchPear class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OrchPear.h
//


#ifndef DK_ORCHPEAR_H
#define DK_ORCHPEAR_H

#define DK_OPE_YEARS_AFTER_PLANT	a_field->m_user[1]

#define DK_OPE_BASE 68100
/**

*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_ope_start = 1, // Compulsory, must always be 1 (one).
	dk_ope_sleep_all_day = DK_OPE_BASE,
	dk_ope_spring_plough,
	dk_ope_manure_s1,
	dk_ope_manure_p1,
	dk_ope_water1,
	dk_ope_subsoiler,
	dk_ope_planting,
	dk_ope_sow_grass,
	dk_ope_insecticide1,
	dk_ope_insecticide2,
	dk_ope_insecticide3,
	dk_ope_insecticide4,
	dk_ope_insecticide5,
	dk_ope_insecticide6,
	dk_ope_insecticide7,
	dk_ope_insecticide8,
	dk_ope_insecticide9,
	dk_ope_insecticide10,
	dk_ope_herbicide1,
	dk_ope_herbicide2,
	dk_ope_manual_cutting1,
	dk_ope_manual_cutting2,
	dk_ope_manual_cutting3,
	dk_ope_manual_cutting4,
	dk_ope_manual_cutting5,
	dk_ope_manual_cutting6,
	dk_ope_manual_cutting7,
	dk_ope_manual_cutting8,
	dk_ope_manual_cutting9,
	dk_ope_manual_cutting10,
	dk_ope_manual_cutting11,
	dk_ope_manual_cutting12,
	dk_ope_manure_s2,
	dk_ope_manure_p2,
	dk_ope_row_cultivation1,
	dk_ope_water2,
	dk_ope_water3,
	dk_ope_water4,
	dk_ope_water5,
	dk_ope_water6,
	dk_ope_water7,
	dk_ope_cutting1,
	dk_ope_cutting2,
	dk_ope_cutting3,
	dk_ope_cutting4,
	dk_ope_cutting5,
	dk_ope_cutting6,
	dk_ope_row_cultivation2,
	dk_ope_fungicide1,
	dk_ope_boron_s,
	dk_ope_boron_p,
	dk_ope_fungicide2,
	dk_ope_herbicide3,
	dk_ope_herbicide4,
	dk_ope_herbicide5,
	dk_ope_herbicide6,
	dk_ope_herbicide7,
	dk_ope_herbicide8,
	dk_ope_herbicide9,
	dk_ope_remove_fruits,
	dk_ope_fungicide3,
	dk_ope_harvest,
	dk_ope_fungicide4,
	dk_ope_fungicide5,
	dk_ope_fungicide6,
	dk_ope_fungicide7,
	dk_ope_fungicide8,
	dk_ope_fungicide9,
	dk_ope_fungicide10,
	dk_ope_fungicide11,
	dk_ope_fungicide12,
	dk_ope_fungicide13,
	dk_ope_fungicide14,
	dk_ope_gr1,
	dk_ope_gr2,
	dk_ope_gr3,
	dk_ope_gr4,

	dk_ope_foobar,
} DK_OrchPearToDo;


/**
\brief
DK_OrchPear class
\n
*/
/**
See DK_OrchPear.h::DK_OrchPearToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OrchPear : public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OrchPear(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,4 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_ope_foobar - DK_OPE_BASE);
	   m_base_elements_no = DK_OPE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_ope_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_ope_sleep_all_day = DK_OPE_BASE,
			fmc_Cultivation, //dk_ope_spring_plough,
			fmc_Fertilizer, //dk_ope_manure_s1,
			fmc_Fertilizer, //dk_ope_manure_p1,
			fmc_Watering, //dk_ope_water1,
			fmc_Cultivation, //dk_ope_subsoiler,
			fmc_Others, //dk_ope_planting,
			fmc_Others, //dk_ope_sow_grass,
			fmc_Insecticide, //dk_ope_insecticide1,
			fmc_Insecticide, //dk_ope_insecticide2,
			fmc_Insecticide, //dk_ope_insecticide3,
			fmc_Insecticide, //dk_ope_insecticide4,
			fmc_Insecticide, //dk_ope_insecticide5,
			fmc_Insecticide, //dk_ope_insecticide6,
			fmc_Insecticide, //dk_ope_insecticide7,
			fmc_Insecticide, //dk_ope_insecticide8,
			fmc_Insecticide, //dk_ope_insecticide9,
			fmc_Insecticide, //dk_ope_insecticide10,
			fmc_Herbicide, //dk_ope_herbicide1
			fmc_Herbicide, //dk_ope_herbicide2
			fmc_Cutting, //dk_ope_manual_cutting1,
			fmc_Cutting, //dk_ope_manual_cutting2,
			fmc_Cutting, //dk_ope_manual_cutting3,
			fmc_Cutting, //dk_ope_manual_cutting4,
			fmc_Cutting, //dk_ope_manual_cutting5,
			fmc_Cutting, //dk_ope_manual_cutting6,
			fmc_Cutting, //dk_ope_manual_cutting7,
			fmc_Cutting, //dk_ope_manual_cutting8,
			fmc_Cutting, //dk_ope_manual_cutting9,
			fmc_Cutting, //dk_ope_manual_cutting10,
			fmc_Cutting, //dk_ope_manual_cutting11,
			fmc_Cutting, //dk_ope_manual_cutting12,
			fmc_Cutting, //dk_ope_manure_s2,
			fmc_Cutting, //dk_ope_manure_p2,
			fmc_Cultivation, //dk_ope_row_cultivation1,
			fmc_Watering, //dk_ope_water2,
			fmc_Watering, //dk_ope_water3,
			fmc_Watering, //dk_ope_water4,
			fmc_Watering, //dk_ope_water5,
			fmc_Watering, //dk_ope_water6,
			fmc_Watering, //dk_ope_water7,
			fmc_Cutting, //dk_ope_cutting1,
			fmc_Cutting, //dk_ope_cutting2,
			fmc_Cutting, //dk_ope_cutting3,
			fmc_Cutting, //dk_ope_cutting4,
			fmc_Cutting, //dk_ope_cutting5,
			fmc_Cutting, //dk_ope_cutting6,
			fmc_Cultivation, //dk_ope_row_cultivation2,
			fmc_Fungicide, //dk_ope_fungicide1,
			fmc_Fertilizer, //dk_ope_boron_s,
			fmc_Fertilizer, //dk_ope_boron_p,
			fmc_Fungicide, //dk_ope_fungicide2,
			fmc_Herbicide, //dk_ope_herbicide3
			fmc_Herbicide, //dk_ope_herbicide4
			fmc_Herbicide, //dk_ope_herbicide5
			fmc_Herbicide, //dk_ope_herbicide6
			fmc_Herbicide, //dk_ope_herbicide7
			fmc_Herbicide, //dk_ope_herbicide8
			fmc_Herbicide, //dk_ope_herbicide9
			fmc_Cutting, //dk_ope_remove_fruits,
			fmc_Fungicide, //dk_ope_fungicide3,
			fmc_Harvest, //dk_ope_harvest,
			fmc_Fungicide, //dk_ope_fungicide4,
			fmc_Fungicide, //dk_ope_fungicide5,
			fmc_Fungicide, //dk_ope_fungicide6,
			fmc_Fungicide, //dk_ope_fungicide7,
			fmc_Fungicide, //dk_ope_fungicide8,
			fmc_Fungicide, //dk_ope_fungicide9,
			fmc_Fungicide, //dk_ope_fungicide10,
			fmc_Fungicide, //dk_ope_fungicide11,
			fmc_Fungicide, //dk_ope_fungicide12,
			fmc_Fungicide, //dk_ope_fungicide13,
			fmc_Fungicide, //dk_ope_fungicide14
			fmc_Others, // dk_ope_gr1
			fmc_Others, // dk_ope_gr2
			fmc_Others, // dk_ope_gr3
			fmc_Others, // dk_ope_gr4
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OrchPear_H

