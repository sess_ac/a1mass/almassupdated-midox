/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_Legumes.cpp This file contains the source for the DE_Legumes class</B> \n
*/
/**
\file
 by Chris J. Topping and Luna Kondrup Marcussen, modified by Susanne Stein \n
 Version of August 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_Legumes.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_Legumes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..


extern CfgBool cfg_pest_peas_on;
extern CfgFloat cfg_pest_product_1_amount;
extern Landscape* g_landscape_p;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional legumes.
*/
bool DE_Legumes::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DELegumes; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	switch (m_ev->m_todo)
	{
	case de_leg_start:
	{
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(31, 8); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 8); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(31, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_leg_spring_harrow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		SimpleEvent_(d1, de_leg_spring_harrow, false, m_farm, m_field);
		break;
	}
	break;
	case de_leg_spring_harrow:
        if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(10, 3) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_spring_harrow, true, m_farm, m_field);
            break;
        }
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, de_leg_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, de_leg_ferti_p1, false, m_farm, m_field);
		break;
	case de_leg_ferti_p1:
		if (!m_farm->FP_SK(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_ferti_p1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_leg_fungicide1, false, m_farm, m_field);
		break;
	case de_leg_ferti_s1:
		if (!m_farm->FA_SK(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_ferti_s1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_leg_fungicide1, false, m_farm, m_field);
		break;
	case de_leg_fungicide1:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(22, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_leg_spring_sow, false, m_farm, m_field);
		break;
    case de_leg_spring_sow:
        if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_leg_spring_sow, true, m_farm, m_field);
            break;
        }
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_leg_herbicide1, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 4) + m_date_modifier, de_leg_insecticide1, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_leg_fungicide2, false, m_farm, m_field);
		break;



	case de_leg_herbicide1:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_herbicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, de_leg_herbicide2, false, m_farm, m_field);
		break;
	case de_leg_herbicide2:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(18, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_herbicide2, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 7, de_leg_herbicide3, false, m_farm, m_field);
		break;
	case de_leg_herbicide3:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(26, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_herbicide3, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 5)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 5);
		}
		SimpleEvent_(d1, de_leg_herbicide4, false, m_farm, m_field);
		break;
	case de_leg_herbicide4:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_herbicide4, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), de_leg_herbicide5, false, m_farm, m_field);
		break;
	case de_leg_herbicide5:
		if (m_ev->m_lock || m_farm->DoIt(0.80)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_leg_herbicide5, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 8)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		}
		SimpleEvent_(d1, de_leg_harvest, false, m_farm, m_field);
		break;


	case de_leg_insecticide1:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(15, 5) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(0.3 * g_landscape_p->SupplyPestIncidenceFactor())) {
			if (!cfg_pest_peas_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_leg_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 6) + m_date_modifier, de_leg_insecticide2, false, m_farm, m_field);
		break;
	case de_leg_insecticide2:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(16, 7) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(0.2 * g_landscape_p->SupplyPestIncidenceFactor())) {
			if (!cfg_pest_peas_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_leg_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		// end of thread
		break;

	case de_leg_fungicide2:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_fungicide2, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 6), de_leg_fungicide3, false, m_farm, m_field); // Fungicide thread
		break;
	case de_leg_fungicide3:
		// Here comes the fungicide thread 
		if (m_ev->m_lock || m_farm->DoIt(0.30)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_leg_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, de_leg_fungicide4, false, m_farm, m_field);
		break;
	case de_leg_fungicide4:
		if (m_ev->m_lock || m_farm->DoIt(0.20)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(16, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_leg_fungicide4, true, m_farm, m_field);
				break;
			}
		}
		//End of thread
		break;


    case de_leg_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_leg_harvest, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
    default:
        g_msg->Warn(WARN_BUG, "DE_Legumes::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

    return done;
}