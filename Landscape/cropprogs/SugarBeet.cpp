//
// SugarBeet.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SugarBeet.h"

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;


bool SugarBeet::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	//  int d1;

	bool done = false;

	switch (m_ev->m_todo)
	{
	case sbe_start:
	{
					 SB_DECIDE_TO_HERB = 1;
					 SB_DECIDE_TO_FI = 1;
					 a_field->ClearManagementActionSum();

					 m_field->SetVegPatchy(true); // Root crop so is open until tall
					 // Set up the date management stuff
					 // Could save the start day in case it is needed later
					 // m_field->m_startday = m_ev->m_startday;
					 m_last_date = g_date->DayInYear(10, 11);
					 // Start and stop dates for all events after harvest
					 int noDates = 1;
					 m_field->SetMDates(0, 0, g_date->DayInYear(1, 10));
					 // 0,0 determined by harvest date - used to see if at all possible
					 m_field->SetMDates(1, 0, g_date->DayInYear(30, 12));
					 // Check the next crop for early start, unless it is a spring crop
					 // in which case we ASSUME that no checking is necessary!!!!
					 // So DO NOT implement a crop that runs over the year boundary

					 //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
					 int d1;
					 if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

						 if (m_ev->m_startday>g_date->DayInYear(1, 7))
						 {
							 if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
							 {
								 g_msg->Warn(WARN_BUG, "SugarBeet::Do(): "
									 "Harvest too late for the next crop to start!!!", "");
								 exit(1);
							 }
							 // Now fix any late finishing problems
							 for (int i = 0; i<noDates; i++) {
								 if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
									 m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
								 }
								 if (m_field->GetMDates(1, i) >= m_ev->m_startday){
									 m_field->SetMConstants(i, 0);
									 m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
								 }
							 }
						 }
						 // Now no operations can be timed after the start of the next crop.

						 // CJT note:
						 // Start single block date checking code to be cut-'n-pasted...

						 if (!m_ev->m_first_year)
						 {
							 // Are we before July 1st?
							 d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
							 if (g_date->Date() < d1)
							 {
								 // Yes, too early. We assumme this is because the last crop was late
								 g_msg->Warn(WARN_BUG, "SugarBeet::Do(): "
									 "Crop start attempt between 1st Jan & 1st July", "");
								 exit(1);
							 }
							 else
							 {
								 d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
								 if (g_date->Date() > d1)
								 {
									 // Yes too late - should not happen - raise an error
									 g_msg->Warn(WARN_BUG, "SugarBeet::Do(): "
										 "Crop start attempt after last possible start date", "");
									 exit(1);
								 }
							 }
						 }
						 else
						 {
							 // If this is the first year of running then it is possibel to start
							 // on day 0, so need this to tell us what to do:
							 SimpleEvent(g_date->OldDays() +g_date->DayInYear(10, 3), sbe_spring_plough, false);
							 break;
						 }
					 }//if

					 // End single block date checking code. Please see next line
					 // comment as well.
					 // Reinit d1 to first possible starting date.
					 d1 = g_date->OldDays() + g_date->DayInYear(10, 3) + 365; 
					 // OK, let's go.
					 SimpleEvent(d1, sbe_spring_plough, false);
	}
		break;

	case sbe_spring_plough:
		if (!m_farm->SpringPlough(m_field, 0.0,g_date->DayInYear(1, 4) -g_date->DayInYear())) 
		{
			SimpleEvent(g_date->Date() + 1, sbe_spring_plough, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, sbe_start_threads_one, false);
		break;

	case sbe_start_threads_one:
		// Today is the 16th of March, at the least.
		SB_DID_HARROW = false;
		SB_DID_NPKS_ONE = false;
		SB_DID_SLURRY = false;
		SB_SOW_DATE = 0;
		SimpleEvent(g_date->Date(), sbe_spring_harrow, false);
		SimpleEvent(g_date->Date() + 1, sbe_fertnpks_one, false);
		SimpleEvent(g_date->Date() + 1, sbe_fertslurry, false);
		break;

	case sbe_spring_harrow:
		if (!m_farm->SpringHarrow(m_field, 0.0,
			g_date->DayInYear(10, 4) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, sbe_spring_harrow, true);
			break;
		}
		SB_DID_HARROW = true;
		if (SB_DID_NPKS_ONE && SB_DID_SLURRY) {
			// We are the last surviving thread.
			SimpleEvent(g_date->Date() + 1, sbe_spring_sow, false);
		}
		break;

	case sbe_fertnpks_one:
		if (!m_farm->FA_NPK(m_field, 0.0,
			g_date->DayInYear(10, 4) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, sbe_fertnpks_one, true);
			break;
		}
		SB_DID_NPKS_ONE = true;
		if (SB_DID_HARROW && SB_DID_SLURRY) {
			// We are the last surviving thread.
			SimpleEvent(g_date->Date() + 1, sbe_spring_sow, false);
		}
		break;

	case sbe_fertslurry:
		if (!m_farm->FA_Slurry(m_field, 0.0,
			g_date->DayInYear(10, 4) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, sbe_fertslurry, true);
			break;
		}
		SB_DID_SLURRY = true;
		if (SB_DID_HARROW && SB_DID_NPKS_ONE) {
			// We are the last surviving thread.
			SimpleEvent(g_date->Date() + 1, sbe_spring_sow, false);
		}
		break;

	case sbe_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0,
			g_date->DayInYear(14, 4) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, sbe_spring_sow, true);
			break;
		}
		SB_SOW_DATE = g_date->Date();
		SimpleEvent(g_date->Date(), sbe_spring_roll, false);
		break;

	case sbe_spring_roll:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->SpringRoll(m_field, 0.0,
				g_date->DayInYear(15, 4) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, sbe_spring_roll, true);
				break;
			}
		}
		{
			int d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
			if (d1 < SB_SOW_DATE + 10) {
				d1 = SB_SOW_DATE;
			}
			SimpleEvent(d1, sbe_herbicide_one, false);
		}
		break;

	case sbe_herbicide_one:
		if (m_ev->m_lock || m_farm->DoIt((int)(100 * cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier()))) //modified probability
		{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(11, 5) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, sbe_herbicide_one, true);
					break;
				}
		}
		{
			int d1 = g_date->Date() + 7;
			if (d1 < g_date->OldDays() + g_date->DayInYear(2, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(2, 5);
			}
			SimpleEvent(d1, sbe_herbicide_two, false);
		}
		break;

	case sbe_herbicide_two:
		if (m_ev->m_lock || m_farm->DoIt((int)(80 * cfg_herbi_app_prop.value() * SB_DECIDE_TO_HERB  * m_farm->Prob_multiplier()))) //modified probability
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0,
				g_date->DayInYear(18, 5) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, sbe_herbicide_two, true);
				break;
			}
		}
		{
			int d1 = g_date->Date() + 7;
			if (d1 < g_date->OldDays() + g_date->DayInYear(10, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(10, 5);
			}
			SimpleEvent(g_date->DayInYear() + 14, sbe_herbicide_three, false);
			SimpleEvent(d1, sbe_row_cultivation_one, false);

		}
		break;

	case sbe_herbicide_three:
		if (m_ev->m_lock || m_farm->DoIt((int)(60 * cfg_herbi_app_prop.value() * SB_DECIDE_TO_HERB  * m_farm->Prob_multiplier()))) //modified probability
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0,
				g_date->DayInYear(18, 5) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, sbe_herbicide_two, true);
				break;
			}
		}
		break;

	case sbe_row_cultivation_one:
		if (m_ev->m_lock || m_farm->DoIt(10))
		{
			if (!m_farm->RowCultivation(m_field, 0.0,
				g_date->DayInYear(25, 5) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, sbe_row_cultivation_one, true);
				break;
			}
			// Did first row cultivation, queue up the second too.
			SB_DID_ROW_TWO = false;
			{
				int d1 = g_date->Date() + 7;
				if (d1 < g_date->OldDays() + g_date->DayInYear(17, 5)) {
					d1 = g_date->OldDays() + g_date->DayInYear(17, 5);
				}
				SimpleEvent(d1, sbe_row_cultivation_two, false);
			}
		}
		else {
			// Didn't do row cultivation on this field. Signal already done.
			SB_DID_ROW_TWO = true;
		}
		SB_DID_INSECT_ONE = false;
		SB_DID_NPKS_TWO = false;
		SB_DID_WATER_ONE = false;
		SB_TRULY_DID_WATER_ONE = false;
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 5),
			sbe_insecticide_one, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6),
			sbe_fertnpks_two, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7),
			sbe_water_one, false);
		break;

	case sbe_row_cultivation_two:
		if (!m_farm->RowCultivation(m_field, 0.0,
			g_date->DayInYear(15, 6) -
			g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, sbe_row_cultivation_two, true);
			break;
		}
		SB_DID_ROW_TWO = true;
		if (SB_DID_INSECT_ONE &&
			SB_DID_NPKS_TWO   &&
			SB_DID_WATER_ONE
			) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 6),
				sbe_insecticide_two, false);
		}
		break;

	case sbe_insecticide_one:
		if (m_ev->m_lock || m_farm->DoIt((int)(50 * cfg_ins_app_prop1.value()  * m_farm->Prob_multiplier()))) // was 90 //modified probability
		{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, sbe_insecticide_one, true);
					break;
				}
		}
		SB_DID_INSECT_ONE = true;
		if (SB_DID_ROW_TWO   &&
			SB_DID_NPKS_TWO  &&
			SB_DID_WATER_ONE
			) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 6),
				sbe_insecticide_two, false);
		}
		break;

	case sbe_fertnpks_two:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->FA_NPK(m_field, 0.0,
				g_date->DayInYear(15, 6) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, sbe_fertnpks_two, true);
				break;
			}
		}
		SB_DID_NPKS_TWO = true;
		if (SB_DID_ROW_TWO    &&
			SB_DID_INSECT_ONE &&
			SB_DID_WATER_ONE
			) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 6),
				sbe_insecticide_two, false);
		}
		break;

	case sbe_water_one:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->Water(m_field, 0.0,
				g_date->DayInYear(30, 7) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, sbe_water_one, true);
				break;
			}
			SB_TRULY_DID_WATER_ONE = true;
		}
		SB_DID_WATER_ONE = true;
		if (SB_DID_ROW_TWO    &&
			SB_DID_INSECT_ONE &&
			SB_DID_NPKS_TWO
			) {
			// We are the last surviving thread.
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 6),
				sbe_insecticide_two, false);
		}
		break;

	case sbe_insecticide_two:
		if (m_ev->m_lock || m_farm->DoIt((int)(30 * cfg_ins_app_prop1.value()*SB_DECIDE_TO_FI  * m_farm->Prob_multiplier()))) //modified probability
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0,
				g_date->DayInYear(25, 6) -
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, sbe_insecticide_two, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8),sbe_water_two, false);
		break;

	case sbe_water_two:
		if (SB_TRULY_DID_WATER_ONE)
		{
			if (!m_farm->Water(m_field, 0.0,g_date->DayInYear(30, 8) -g_date->DayInYear())) 
			{
				SimpleEvent(g_date->Date() + 1, sbe_water_two, true);
				break;
			}
		}
		ChooseNextCrop(1);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), sbe_harvest, false);
		break;

	case sbe_harvest:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->HarvestLong(m_field, 0.0, -1)) { //raise an error
				g_msg->Warn(WARN_BUG, "Sugarbeet::Do(): failure in 'Harvest' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->HarvestLong(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, sbe_harvest, true);
				break;
			}
		}
		m_field->SetVegPatchy(false);
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "Sugarbeet::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}

	return done;
}


