//
// OWinterBarley.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWinterBarley_H
#define OWinterBarley_H

#define OWBarley_BASE 4600
#define OWB_DID_SPRING_SOW  m_field->m_user[0]

typedef enum {
  owb_start = 1, // Compulsory, start event must always be 1 (one).
  owb_fertmanure_plant_one = OWBarley_BASE,
  owb_fertmanure_stock_one,
  owb_autumn_plough,
  owb_autumn_harrow,
  owb_autumn_sow,
  owb_strigling_one,
  owb_spring_sow,
  owb_fertslurry_plant,
  owb_fertslurry_stock,
  owb_strigling_two,
  owb_harvest,
  owb_straw_chopping,
  owb_hay_bailing,
  owb_stubble_harrowing,
  owb_foobar,
// --FN--
} OWBToDo;



class OWinterBarley: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OWinterBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(23,9);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (owb_foobar - OWBarley_BASE);
	  m_base_elements_no = OWBarley_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  owb_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  owb_fertmanure_plant_one = OWBarley_BASE,
			fmc_Fertilizer,	//	  owb_fertmanure_stock_one,
			fmc_Cultivation,	//	  owb_autumn_plough,
			fmc_Cultivation,	//	  owb_autumn_harrow,
			fmc_Others,	//	  owb_autumn_sow,
			fmc_Cultivation,	//	  owb_strigling_one,
			fmc_Others,	//	  owb_spring_sow,
			fmc_Fertilizer,	//	  owb_fertslurry_plant,
			fmc_Fertilizer,	//	  owb_fertslurry_stock,
			fmc_Cultivation,	//	  owb_strigling_two,
			fmc_Harvest,	//	  owb_harvest,
			fmc_Others,	//	  owb_straw_chopping,
			fmc_Others,	//	  owb_hay_bailing,
			fmc_Cultivation	//	  owb_stubble_harrowing,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OWinterBarley_H
