/**
\file
\brief
<B>PTVineyards.h This file contains the headers for the Vineyards class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of March 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTVineyards.h
//


#ifndef PTVINEYARDS_H
#define PTVINEYARDS_H

#define PTVINEYARDS_BASE 31700

/**
\brief A flag used to indicate autumn ploughing status
*/
#define PT_VINE_PRUNING_DATE		a_field->m_user[1]
#define PT_VINE_PRUNING_DONE		a_field->m_user[2]

/** Below is the list of things that a farmer can do if he is growing vineyards, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_vine_start = 1, // Compulsory, must always be 1 (one).
	pt_vine_sleep_all_day = PTVINEYARDS_BASE,
	// Vine care treatments
	pt_vine_autumn_pruning,
	pt_vine_spring_pruning,
	pt_vine_shredding,
	pt_vine_suckering,
	pt_vine_trimming,
	pt_vine_leafthinning,
	pt_vine_greenharvest,
	// Mowing
	pt_vine_mowing1,
	pt_vine_mowing2,
	// NPK
	pt_vine_npk1,
	pt_vine_npk2,
	// Herbicides
	pt_vine_herbicide1,
	pt_vine_herbicide2,
	// Insecticides
	pt_vine_insecticide1,
	pt_vine_insecticide2,
	pt_vine_insecticide3,
	pt_vine_insecticide4,
	// Fungicides
	pt_vine_fungicide1,
	pt_vine_fungicide2,
	pt_vine_fungicide3,
	pt_vine_fungicide4,
	pt_vine_fungicide5,
	pt_vine_fungicide6,
	// Harvest
	pt_vine_harvest,

} PTVineyardsToDo;


/**
\brief
PTVineyards class
\n
*/
/**
See PTVineyards.h::PTVineyardsToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTVineyards: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTVineyards(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 10th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,12 );// AAS:Last possible date to do the first operation, i.e. plough or sow
   }
};

#endif // PTVINEYARDS_H

