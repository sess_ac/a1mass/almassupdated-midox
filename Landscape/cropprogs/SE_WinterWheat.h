/**
\file
\brief
<B>SE_WinterWheat.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of March 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// SE_WinterWheat.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SE_WINTERWHEAT_H
#define SE_WINTERWHEAT_H

#define SE_WW_BASE 80200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define SE_WW_HERBICIDE        a_field->m_user[1]
#define SE_WW_DECIDE_TO_HERB       a_field->m_user[2]
#define SE_WW_DECIDE_TO_FI         a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  se_ww_start = 1, // Compulsory, must always be 1 (one).
  se_ww_straw_removal = SE_WW_BASE,
  se_ww_herbicide1,
  se_ww_autumn_plough,
  se_ww_stubble_cultivator1,
  se_ww_stubble_cultivator2,
  se_ww_slurry,
  se_ww_autumn_harrow,
  se_ww_autumn_sow,
  se_ww_sow_w_fertilizer,
  se_ww_herbicide2,
  se_ww_fungicide1,
  se_ww_insecticide1,
  se_ww_spring_harrow,
  se_ww_n_fertilizer1,
  se_ww_n_fertilizer2,
  se_ww_n_fertilizer3,
  se_ww_herbicide3,
  se_ww_fungicide2,
  se_ww_growth_regulator,
  se_ww_insecticide2,
  se_ww_harvest,
  se_ww_foobar,
} SE_WinterWheatToDo;

	
/**
\brief
SE_WinterWheat class
\n
*/
/**
See SE_WinterWheat.h::SE_WinterWheatToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class SE_WinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   SE_WinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,10 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (se_ww_foobar - SE_WW_BASE);
	   m_base_elements_no = SE_WW_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  se_ww_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	  se_ww_straw_removal = SE_WW_BASE,
			fmc_Herbicide,	//	  se_ww_herbicide1,
			fmc_Cultivation,	//	  se_ww_autumn_plough,
			fmc_Cultivation,	//	  se_ww_stubble_cultivator1,
			fmc_Cultivation,	//	  se_ww_stubble_cultivator2,
			fmc_Fertilizer,	//	  se_ww_slurry,
			fmc_Cultivation,	//	  se_ww_autumn_harrow,
			fmc_Others,	//	  se_ww_autumn_sow,
			fmc_Fertilizer,	//	  se_ww_sow_w_fertilizer,
			fmc_Herbicide,	//	  se_ww_herbicide2,
			fmc_Fungicide,	//	  se_ww_fungicide1,
			fmc_Insecticide,	//	  se_ww_insecticide1,
			fmc_Cultivation,	//	  se_ww_spring_harrow,
			fmc_Fertilizer,	//	  se_ww_n_fertilizer1,
			fmc_Fertilizer,	//	  se_ww_n_fertilizer2,
			fmc_Fertilizer,	//	  se_ww_n_fertilizer3,
			fmc_Herbicide,	//	  se_ww_herbicide3,
			fmc_Fungicide,	//	  se_ww_fungicide2,
			fmc_Others,	//	  se_ww_growth_regulator,
			fmc_Insecticide,	//	  se_ww_insecticide2,
			fmc_Harvest	//	  se_ww_harvest,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // SE_WINTERWHEAT_H

