//
// OPotatoes.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#ifndef OPOTATOESEAT_H
#define OPOTATOESEAT_H

#define OPOTATOESEAT_BASE 3600
#define OPOT_SLURRY_DATE   m_field->m_user[0]
#define OPOT_SOW_DATE      m_field->m_user[1]
#define OPOT_HILLING_THREE m_field->m_user[2]

typedef enum {
  ope_start = 1, // Compulsory, start event must always be 1 (one).
  ope_fa_slurry = OPOTATOESEAT_BASE,
  ope_fa_manure,
  ope_fp_slurry,
  ope_fp_manure,
  ope_spring_plough,
  ope_spring_harrow,
  ope_spring_sow,
  ope_flaming_one,
  ope_strigling_one,
  ope_hilling_one,
  ope_strigling_two,
  ope_hilling_two,
  ope_strigling_three,
  ope_hilling_three,
  ope_water_one,
  ope_water_two,
  ope_flaming_two,
  ope_harvest,
  ope_foobar,
} OPotatoesEatToDo;



class OPotatoes: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OPotatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (ope_foobar - OPOTATOESEAT_BASE);
	  m_base_elements_no = OPOTATOESEAT_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  ope_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  ope_fa_slurry = OPOTATOESEAT_BASE,
			fmc_Fertilizer,	//	  ope_fa_manure,
			fmc_Fertilizer,	//	  ope_fp_slurry,
			fmc_Fertilizer,	//	  ope_fp_manure,
			fmc_Cultivation,	//	  ope_spring_plough,
			fmc_Cultivation,	//	  ope_spring_harrow,
			fmc_Others,	//	  ope_spring_sow,
			fmc_Others,	//	  ope_flaming_one,
			fmc_Cultivation,	//	  ope_strigling_one,
			fmc_Cultivation,	//	  ope_hilling_one,
			fmc_Cultivation,	//	  ope_strigling_two,
			fmc_Cultivation,	//	  ope_hilling_two,
			fmc_Cultivation,	//	  ope_strigling_three,
			fmc_Cultivation,	//	  ope_hilling_three,
			fmc_Watering,	//	  ope_water_one,
			fmc_Watering,	//	  ope_water_two,
			fmc_Others,	//	  ope_flaming_two,
			fmc_Harvest	//	  ope_harvest,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }

};

#endif // OPOTATOESEAT_H
