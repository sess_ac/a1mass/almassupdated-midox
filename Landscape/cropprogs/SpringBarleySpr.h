//
// SpringBarley.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleySpr_H
#define SpringBarleySpr_H

#define SSPRBARLEY_BASE 6800
#define SBSPR_SLURRY_DONE       m_field->m_user[0]
#define SBSPR_MANURE_DONE       m_field->m_user[1]
#define SBSPR_SLURRY_EXEC       m_field->m_user[2]
#define SBSPR_MANURE_EXEC       m_field->m_user[3]
#define SBSPR_DID_AUTUMN_PLOUGH m_field->m_user[4]

#define SBSPR_HERBI_DATE        m_field->m_user[0]
#define SBSPR_GR_DATE           m_field->m_user[1]
#define SBSPR_FUNGI_DATE        m_field->m_user[2]
#define SBSPR_WATER_DATE        m_field->m_user[3]
#define SBSPR_INSECT_DATE       m_field->m_user[4]
#define SBSPR_DECIDE_TO_HERB	 m_field->m_user[5]
#define SBSPR_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  sbspr_start = 1, // Compulsory, start event must always be 1 (one).
  sbspr_spring_plough = SSPRBARLEY_BASE,
  sbspr_spring_harrow,
  sbspr_fertmanure_plant,
  sbspr_fertlnh3_plant,
  sbspr_fertpk_plant,
  sbspr_fertmanure_stock_two,
  sbspr_fertnpk_stock,
  sbspr_spring_sow,
  sbspr_spring_roll,
  sbspr_herbicide_one,
  sbspr_herbicide_two,
  sbspr_GR,
  sbspr_insecticide1,
  sbspr_insecticide2,
  sbspr_insecticide3,
  sbspr_fungicide_one,
  sbspr_fungicide_two,
  sbspr_water_one,
  sbspr_water_two,
  sbspr_harvest,
  sbspr_straw_chopping,
  sbspr_hay_baling,
  sbspr_stubble_harrow,
  sbspr_foobar
} SBSPRToDo;



class SpringBarleySpr: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleySpr(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,4);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sbspr_foobar - SSPRBARLEY_BASE);
	  m_base_elements_no = SSPRBARLEY_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sbspr_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation,//sbspr_spring_plough = SSPRBARLEY_BASE,
		  fmc_Cultivation,//sbspr_spring_harrow,
		  fmc_Fertilizer,//sbspr_fertmanure_plant,
		  fmc_Fertilizer,//sbspr_fertlnh3_plant,
		  fmc_Fertilizer,//sbspr_fertpk_plant,
		  fmc_Fertilizer,//sbspr_fertmanure_stock_two,
		  fmc_Fertilizer,//sbspr_fertnpk_stock,
		  fmc_Others,//sbspr_spring_sow,
		  fmc_Others,//sbspr_spring_roll,
		  fmc_Herbicide,//sbspr_herbicide_one,
		  fmc_Herbicide,//sbspr_herbicide_two,
		  fmc_Others,//sbspr_GR,
		  fmc_Insecticide,//sbspr_insecticide1,
		  fmc_Insecticide,//sbspr_insecticide2,
		  fmc_Insecticide,//sbspr_insecticide3,
		  fmc_Fungicide,//sbspr_fungicide_one,
		  fmc_Fungicide,//sbspr_fungicide_two,
		  fmc_Watering,//sbspr_water_one,
		  fmc_Watering,//sbspr_water_two,
		  fmc_Harvest,//sbspr_harvest,
		  fmc_Others,//sbspr_straw_chopping,
		  fmc_Others,//sbspr_hay_baling,
		  fmc_Cultivation//sbspr_stubble_harrow

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SpringBarleySpr_H
