/**
\file
\brief
<B>UKWinterWheat.h This file contains the headers for the UKWinterWheat class</B> \n
*/
/**
\file
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKWinterWheat.h
//
/*

Adapted for UK Winter Wheat, 2021, Adam McVeigh, Nottingham Trent University

*/

#ifndef UKWINTERWHEAT_H
#define UKWINTERWHEAT_H

#define UKWINTERWHEAT_BASE 45900
/**
\brief A flag used to indicate autumn ploughing status
*/
#define UKWW_AUTUMN_PLOUGH        a_field->m_user[1]
#define UKWW_DECIDE_TO_HERB       a_field->m_user[2]
#define UKWW_DECIDE_TO_FI         a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing uk winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  uk_ww_start = 1, // Compulsory, must always be 1 (one).
  uk_ww_sleep_all_day = UKWINTERWHEAT_BASE,
  uk_ww_stubble_cultivator1, // 70%
  uk_ww_herbicide1, // 50%
  uk_ww_autumn_plough, // 50%
  uk_ww_stubble_cultivator2, // 50%
  uk_ww_autumn_sow, // 100%
  uk_ww_herbicide2, // 75%
  uk_ww_herbicide3, // 100%
  uk_ww_ferti_p1, // slurry 70%
  uk_ww_ferti_s1,
  uk_ww_ferti_p2, // PK 80%
  uk_ww_ferti_s2,
  uk_ww_ferti_p3, // NI 100%
  uk_ww_ferti_s3,
  uk_ww_fungicide1, // 100%
  uk_ww_fungicide2, // 90%
  uk_ww_fungicide3, // 70%
  uk_ww_insecticide1, // 100%
  uk_ww_insecticide2, // 100%
  uk_ww_growth_regulator1, // 100%
  uk_ww_growth_regulator2, // 80%
  uk_ww_harvest, // 100%
  uk_ww_straw_chopping, // 50%
  uk_ww_hay_bailing, // 50%
  uk_ww_foobar,
} UKWinterWheatToDo;

/**
\brief
UKWinterWheat class
\n
*/
/**
See UKWinterWheat.h::UKWinterWheatToDo for a complete list of all possible events triggered codes by the uk winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKWinterWheat: public Crop
{
 public:
	 virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	 UKWinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear(5, 11);
		SetUpFarmCategoryInformation();
	 }
	 void SetUpFarmCategoryInformation() {
		 const int elements = 2 + (uk_ww_foobar - UKWINTERWHEAT_BASE);
		 m_base_elements_no = UKWINTERWHEAT_BASE - 2;
		 FarmManagementCategory catlist[elements] =
		 {
			  fmc_Others, // this needs to be at the zero line
			  fmc_Others,//uk_ww_start = 1, // Compulsory, must always be 1 (one).
			  fmc_Others,//uk_ww_sleep_all_day = UKWINTERWHEAT_BASE,
			  fmc_Cultivation,//uk_ww_stubble_cultivator1, // 70%
			  fmc_Herbicide,//uk_ww_herbicide1, // 50%
			  fmc_Cultivation,//uk_ww_autumn_plough, // 50%
			  fmc_Cultivation,//uk_ww_stubble_cultivator2, // 50%
			  fmc_Others,//uk_ww_autumn_sow, // 100%
			  fmc_Herbicide,//uk_ww_herbicide2, // 75%
			  fmc_Herbicide,//uk_ww_herbicide3, // 100%
			  fmc_Fertilizer,//uk_ww_ferti_p1, // slurry 70%
			  fmc_Fertilizer,//uk_ww_ferti_s1,
			  fmc_Fertilizer,//uk_ww_ferti_p2, // PK 80%
			  fmc_Fertilizer,//uk_ww_ferti_s2,
			  fmc_Fertilizer,//uk_ww_ferti_p3, // NI 100%
			  fmc_Fertilizer,//uk_ww_ferti_s3,
			  fmc_Fungicide,//uk_ww_fungicide1, // 100%
			  fmc_Fungicide,//uk_ww_fungicide2, // 90%
			  fmc_Fungicide,//uk_ww_fungicide3, // 70%
			  fmc_Insecticide,//uk_ww_insecticide1, // 100%
			  fmc_Insecticide,//uk_ww_insecticide2, // 100%
			  fmc_Others,//uk_ww_growth_regulator1, // 100%
			  fmc_Others,//uk_ww_growth_regulator2, // 80%
			  fmc_Harvest,//uk_ww_harvest, // 100%
			  fmc_Others,//uk_ww_straw_chopping, // 50%
			  fmc_Others,//uk_ww_hay_bailing, // 50%
		 };
		 copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	 }
};

#endif // UKWINTERWHEAT_H

