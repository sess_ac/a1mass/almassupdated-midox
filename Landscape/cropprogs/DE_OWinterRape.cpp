//
// DE_OWinterRape.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OWinterRape.h"

bool DE_OWinterRape::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEOWinterRape; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_owr_start:
	{
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(15, 8); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(1, 8); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(10, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[2][1] = g_date->DayInYear(15, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 2)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_owr_row_cultivation_three))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 8) + isSpring;
		// OK, let's go.
		if (m_farm->IsStockFarmer()) {
			SimpleEvent_(d1, de_owr_fa_manure, false, m_farm, m_field);
		}
		else 
			SimpleEvent_(d1, de_owr_fp_manure, false, m_farm, m_field);
		break;
	}
	break;

	case de_owr_fa_manure:
		if ( m_ev->m_lock || m_farm->DoIt( 75 )) {
			if (!m_farm->FA_Manure( m_field, 0.0, g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_owr_fa_manure, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_( g_date->Date(), de_owr_autumn_plough, false, m_farm, m_field);
		break;

	case de_owr_fp_manure:
		if ( m_ev->m_lock || m_farm->DoIt( 50 )) {
			if (!m_farm->FP_Manure( m_field, 0.0, g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_owr_fp_manure, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_( g_date->Date(), de_owr_autumn_plough, false, m_farm, m_field);
		break;

	case de_owr_autumn_plough:
		if (!m_farm->AutumnPlough( m_field, 0.0, g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_owr_autumn_plough, true, m_farm, m_field);
			break;
		}
		SimpleEvent_( g_date->Date() + 1, de_owr_autumn_harrow, false, m_farm, m_field);
		break;

	case de_owr_autumn_harrow:
		if (!m_farm->AutumnHarrow( m_field, 0.0, g_date->DayInYear( 27, 8 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_owr_autumn_harrow, true, m_farm, m_field);
			break;
		}
		SimpleEvent_( g_date->OldDays() + g_date->DayInYear( 10, 8 ), de_owr_autumn_sow, false, m_farm, m_field);
		break;

	case de_owr_autumn_sow:
		if (!m_farm->AutumnSow( m_field, 0.0, g_date->DayInYear( 10, 9 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_owr_autumn_sow, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, de_owr_row_cultivation_one, false, m_farm, m_field);
		break;

	case de_owr_row_cultivation_one:
		if (!m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 30, 9 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_owr_row_cultivation_one, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if ( d1 < g_date->OldDays() + g_date->DayInYear( 8, 10 ))
			d1 = g_date->OldDays() + g_date->DayInYear( 8, 10 );
		SimpleEvent_( d1, de_owr_row_cultivation_two, false, m_farm, m_field);
		break;
	
	case de_owr_row_cultivation_two:
		if (m_ev->m_lock || m_farm->DoIt(40)) {
			if (!m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 10, 10 ) - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_owr_row_cultivation_two, true, m_farm, m_field);
                break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer Sulphur treatment
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), de_owr_fertiFA_S, false, m_farm, m_field);	// Sulphur
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), de_owr_fertiFP_S, false, m_farm, m_field);	// Sulphur
		break;

	case de_owr_fertiFP_S:
		// Here comes the sulphur for arable farms
		if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
             SimpleEvent_(g_date->Date() + 1, de_owr_fertiFP_S, true, m_farm, m_field);
             break;
        }
		SimpleEvent_(g_date->Date(), de_owr_fertiFP_B, false, m_farm, m_field);
		break;

	case de_owr_fertiFA_S:
		// Here comes the sulphur thread for stock farms
		if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_owr_fertiFA_S, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date(), de_owr_fertiFA_B, false, m_farm, m_field);
		break;

	case de_owr_fertiFP_B:
		if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_owr_fertiFP_B, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_owr_row_cultivation_three, false, m_farm, m_field);
		break;

	case de_owr_fertiFA_B:
		if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_owr_fertiFA_B, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4) + 365, de_owr_row_cultivation_three, false, m_farm, m_field);
		break;

	case de_owr_row_cultivation_three:
		if (!m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_owr_row_cultivation_three, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 5;
		if ( d1 < g_date->OldDays() + g_date->DayInYear( 10, 7 ))
		d1 = g_date->OldDays() + g_date->DayInYear( 10, 7 );
		SimpleEvent_( d1, de_owr_harvest, false, m_farm, m_field);
		break;

	case de_owr_harvest:
		if ( !m_farm->Harvest( m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_owr_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_( g_date->Date(), de_owr_straw_chop, false, m_farm, m_field);
		break;

	case de_owr_straw_chop:
		if ( !m_farm->StrawChopping( m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_owr_straw_chop, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if ( d1 < g_date->OldDays() + g_date->DayInYear( 15, 7 ))
		d1 = g_date->OldDays() + g_date->DayInYear( 15, 7 );
		SimpleEvent_( d1, de_owr_stub_harrow, false, m_farm, m_field);
		break;

	case de_owr_stub_harrow:
		if ( m_ev->m_lock || m_farm->DoIt( 75 )) {
			if ( !m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_owr_stub_harrow, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		break;

	default:
		g_msg->Warn( WARN_BUG, "DE_OWinterRape::Do(): "
			"Unknown event type! ", "" );
		exit( 1 );
	}
	return done;
}

