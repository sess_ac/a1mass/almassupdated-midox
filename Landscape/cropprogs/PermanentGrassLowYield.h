//
// PermanentGrassLowYield.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef PermanentGrassLowYield_H
#define PermanentGrassLowYield_H

#define PGLY_BASE 5300
#define PGLY_CUT_DATE  m_field->m_user[0]

typedef enum {
  pgly_start = 1, // Compulsory, start event must always be 1 (one).
  pgly_cut_to_hay = PGLY_BASE,
  pgly_cattle_out1,
  pgly_cattle_out2,
  pgly_cattle_is_out,
  pgly_cut_weeds,
  pgly_herbicide,
  pgly_raking1,
  pgly_raking2,
  pgly_compress_straw,
  pgly_wait, 
  pgly_foobar
} PermanentGrassLowYieldToDo;



class PermanentGrassLowYield: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  PermanentGrassLowYield(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
        m_first_date=g_date->DayInYear(15,5);
        SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (pgly_foobar - PGLY_BASE);
	  m_base_elements_no = PGLY_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		fmc_Others, // zero element unused but must be here
		fmc_Cutting, // pgly_cut_to_hay = PGLY_BASE,
		fmc_Grazing, // pgly_cattle_out1,
		fmc_Grazing, // pgly_cattle_out2,
		fmc_Grazing, // pgly_cattle_is_out,
		fmc_Cutting, // pgly_cut_weeds,
		fmc_Herbicide, // pgly_herbicide,
		fmc_Others, // pgly_raking1,
		fmc_Others, // pgly_raking2,
		fmc_Others, // pgly_compress_straw,
		fmc_Others // pgly_wait,
		// No foobar entry
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }

};

#endif // PermanentGrassLowYield_H
