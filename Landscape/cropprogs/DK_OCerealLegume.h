//
// DK_OCerealLegume.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKOCerealLegume_H
#define DKOCerealLegume_H

#define DK_OCL_BASE 61500

#define DK_OCL_FORCESPRING	a_field->m_user[1]

typedef enum {
  dk_ocl_start = 1, // Compulsory, start event must always be 1 (one).
  dk_ocl_spring_harrow = DK_OCL_BASE,
  dk_ocl_manure1_s,
  dk_ocl_manure1_p,
  dk_ocl_spring_plough,
  dk_ocl_spring_sow,
  dk_ocl_spring_sow_lo,
  dk_ocl_strigling1, 
  dk_ocl_strigling2,
  dk_ocl_water,
  dk_ocl_water_lo,
  dk_ocl_swathing,
  dk_ocl_swathing_lo,
  dk_ocl_harvest,
  dk_ocl_harvest_lo,
  dk_ocl_manure2_s,
  dk_ocl_manure2_p,
  dk_ocl_autumn_plough,
  dk_ocl_cutting1,
  dk_ocl_cutting2,
  dk_ocl_grazing,
  dk_ocl_cattle_is_out,
  dk_ocl_foobar,
} DK_OCerealLegumeToDo;



class DK_OCerealLegume : public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OCerealLegume(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(31,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_ocl_foobar - DK_OCL_BASE);
	  m_base_elements_no = DK_OCL_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_ocl_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation,	//	  dk_ocl_spring_harrow = DK_OCL_BASE,
			fmc_Fertilizer,	//	  dk_ocl_manure1_s,
			fmc_Fertilizer,	//	  dk_ocl_manure1_p,
			fmc_Cultivation,	//	  dk_ocl_spring_plough,
			fmc_Others,	//	  dk_ocl_spring_sow,
			fmc_Others,	//	  dk_ocl_spring_sow_lo,
			fmc_Cultivation,	//	  dk_ocl_strigling1, 
			fmc_Cultivation,	//	  dk_ocl_strigling2,
			fmc_Watering,	//	  dk_ocl_water,
			fmc_Watering,	//	  dk_ocl_water_lo,
			fmc_Cutting,	//	  dk_ocl_swathing,
			fmc_Cutting,	//	  dk_ocl_swathing_lo,
			fmc_Harvest,	//	  dk_ocl_harvest,
			fmc_Harvest,	//	  dk_ocl_harvest_lo,
			fmc_Fertilizer,	//	  dk_ocl_manure2_s,
			fmc_Fertilizer,	//	  dk_ocl_manure2_p,
			fmc_Cultivation,	//	  dk_ocl_autumn_plough,
			fmc_Cutting,	//	  dk_ocl_cutting1,
			fmc_Cutting,	//	  dk_ocl_cutting2,
			fmc_Grazing,	//	  dk_ocl_grazing,
			fmc_Grazing	//	  dk_ocl_cattle_is_out,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_OCerealLegume_H
