/**
\file
\brief
<B>PLWinterRye.h This file contains the headers for the WinterRye class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLWinterRye.h
//


#ifndef PLWINTERRYE_H
#define PLWINTERRYE_H

#define PLWINTERRYE_BASE 26400
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_WRYE_FERTI_P1	a_field->m_user[1]
#define PL_WRYE_FERTI_S1	a_field->m_user[2]
#define PL_WRYE_STUBBLE_PLOUGH	a_field->m_user[3]

/** Below is the list of things that a farmer can do if he is growing winter rye, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_wrye_start = 1, // Compulsory, must always be 1 (one).
	pl_wrye_sleep_all_day = PLWINTERRYE_BASE,
	pl_wrye_ferti_p1, // 20401
	pl_wrye_ferti_s1,
	pl_wrye_stubble_plough,
	pl_wrye_autumn_harrow1,
	pl_wrye_autumn_harrow2,
	pl_wrye_stubble_harrow,
	pl_wrye_ferti_p2,
	pl_wrye_ferti_s2,
	pl_wrye_autumn_plough,
	pl_wrye_autumn_roll, // 20410
	pl_wrye_stubble_cultivator_heavy,
	pl_wrye_ferti_p3,
	pl_wrye_ferti_s3,
	pl_wrye_preseeding_cultivator,
	pl_wrye_preseeding_cultivator_sow,
	pl_wrye_autumn_sow, // 20416
	pl_wrye_herbicide1,
	pl_wrye_fungicide1,
	pl_wrye_ferti_p4,
	pl_wrye_ferti_s4, // 20420
	pl_wrye_ferti_p5,
	pl_wrye_ferti_s5,
	pl_wrye_ferti_p6,
	pl_wrye_ferti_s6,
	pl_wrye_ferti_p7,
	pl_wrye_ferti_s7,
	pl_wrye_ferti_p8,
	pl_wrye_ferti_s8,
	pl_wrye_ferti_p9,
	pl_wrye_ferti_s9, // 20430
	pl_wrye_herbicide2,
	pl_wrye_fungicide2,
	pl_wrye_growth_regulator1,
	pl_wrye_harvest,
	pl_wrye_straw_chopping,
	pl_wrye_hay_bailing,
	pl_wrye_ferti_p10,
	pl_wrye_ferti_s10,
	pl_wrye_ferti_p11,
	pl_wrye_ferti_s11, // 20440
	pl_wrye_foobar
} PLWinterRyeToDo;


/**
\brief
PLWinterRye class
\n
*/
/**
See PLWinterRye.h::PLWinterRyeToDo for a complete list of all possible events triggered codes by the winter rye management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLWinterRye: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLWinterRye(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,9 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_wrye_foobar - PLWINTERRYE_BASE);
	   m_base_elements_no = PLWINTERRYE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_wrye_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_wrye_sleep_all_day = PLWINTERRYE_BASE,
			fmc_Fertilizer,//pl_wrye_ferti_p1, // 20401
			fmc_Fertilizer,//pl_wrye_ferti_s1,
			fmc_Cultivation,//pl_wrye_stubble_plough,
			fmc_Cultivation,//pl_wrye_autumn_harrow1,
			fmc_Cultivation,//pl_wrye_autumn_harrow2,
			fmc_Cultivation,//pl_wrye_stubble_harrow,
			fmc_Fertilizer,//pl_wrye_ferti_p2,
			fmc_Fertilizer,//pl_wrye_ferti_s2,
			fmc_Cultivation,//pl_wrye_autumn_plough,
			fmc_Cultivation,//pl_wrye_autumn_roll, // 20410
			fmc_Cultivation,//pl_wrye_stubble_cultivator_heavy,
			fmc_Fertilizer,//pl_wrye_ferti_p3,
			fmc_Fertilizer,//pl_wrye_ferti_s3,
			fmc_Cultivation,//pl_wrye_preseeding_cultivator,
			fmc_Cultivation,//pl_wrye_preseeding_cultivator_sow,
			fmc_Others,//pl_wrye_autumn_sow, // 20416
			fmc_Herbicide,//pl_wrye_herbicide1,
			fmc_Fungicide,//pl_wrye_fungicide1,
			fmc_Fertilizer,//pl_wrye_ferti_p4,
			fmc_Fertilizer,//pl_wrye_ferti_s4, // 20420
			fmc_Fertilizer,//pl_wrye_ferti_p5,
			fmc_Fertilizer,//pl_wrye_ferti_s5,
			fmc_Fertilizer,//pl_wrye_ferti_p6,
			fmc_Fertilizer,//pl_wrye_ferti_s6,
			fmc_Fertilizer,//pl_wrye_ferti_p7,
			fmc_Fertilizer,//pl_wrye_ferti_s7,
			fmc_Fertilizer,//pl_wrye_ferti_p8,
			fmc_Fertilizer,//pl_wrye_ferti_s8,
			fmc_Fertilizer,//pl_wrye_ferti_p9,
			fmc_Fertilizer,//pl_wrye_ferti_s9, // 20430
			fmc_Fertilizer,//pl_wrye_herbicide2,
			fmc_Fertilizer,//pl_wrye_fungicide2,
			fmc_Others,//pl_wrye_growth_regulator1,
			fmc_Harvest,//pl_wrye_harvest,
			fmc_Others,//pl_wrye_straw_chopping,
			fmc_Others,//pl_wrye_hay_bailing,
			fmc_Fertilizer,//pl_wrye_ferti_p10,
			fmc_Fertilizer,//pl_wrye_ferti_s10,
			fmc_Fertilizer,//pl_wrye_ferti_p11,
			fmc_Fertilizer//pl_wrye_ferti_s11, // 20440
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLWINTERRYE_H

