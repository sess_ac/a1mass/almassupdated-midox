//
// WinterBarley.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WinterBarley.h"
#include <math.h>

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool WinterBarley::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  double ins_app_prop=cfg_ins_app_prop1.value();
  double herbi_app_prop=cfg_herbi_app_prop.value();
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case wb_start:
    {
      WB_DECIDE_TO_HERB=1;
	  WB_DECIDE_TO_FI=1;
      a_field->ClearManagementActionSum();

		// Set up the date management stuff
      m_last_date=g_date->DayInYear(10,8);
      // Start and stop dates for all events after harvest
      int noDates= 3;
      m_field->SetMDates(0,0,g_date->DayInYear(20,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(5,8));
      m_field->SetMDates(0,1,g_date->DayInYear(25,7));
      m_field->SetMDates(1,1,g_date->DayInYear(10,8));
      m_field->SetMDates(0,2,g_date->DayInYear(20,7));
      m_field->SetMDates(1,2,g_date->DayInYear(5,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

	  //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	  int d1;
	  if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "WinterBarley::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
      }
      // Now no operations can be timed after the start of the next crop.

      if ( ! m_ev->m_first_year )
      {
		int today=g_date->Date();
		// Are we before July 1st?
		d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
		if (today < d1){
		  // Yes, too early. We assumme this is because the last crop was late
			  g_msg->Warn( WARN_BUG, "WinterBarley::Do(): "
			 "Crop start attempt between 1st Jan & 1st July", "" );
			  exit( 1 );
		}
        else{
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1) {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "WinterBarley::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        // Is the first year so must start in spring like nothing was unusual
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 )
                                                     ,wb_harvest, false );
      }
	  }//if

      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      WB_DID_SEVEN_ONE   = false;
      WB_DID_SEVEN_TWO   = false;
      WB_DID_SEVEN_THREE = false;
      WB_DID_SEVEN_FOUR  = false;
      WB_FUNGICIDE_DATE  = 0;
      WB_HERBICIDE_DATE  = 0;

      if ( m_farm->IsStockFarmer()) {
	SimpleEvent( d1, wb_fertmanure_stock, false );
	d1 += 14;
	SimpleEvent( d1, wb_fertslurry_stock_one, false );
      } else {
	SimpleEvent( d1, wb_fertsludge_plant_one, false );
      }
    }
    break;

  case wb_fertmanure_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 5, 9 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wb_fertmanure_stock, true );
        break;
      }
    }
    break;

  case wb_fertslurry_stock_one:
    if ( m_ev->m_lock || m_farm->DoIt( 95 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 10, 9 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wb_fertslurry_stock_one, true );
        break;
      }
    }
    SimpleEvent( g_date->Date() + 1, wb_autumn_plough, false );
    break;

  case wb_fertsludge_plant_one:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FP_Sludge( m_field, 0.0,
			      g_date->DayInYear( 5, 9 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wb_fertsludge_plant_one, true );
        break;
      }
    }
    SimpleEvent( g_date->Date() + 1, wb_autumn_plough, false );
    break;

  case wb_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 5, 9 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wb_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), wb_autumn_harrow, false );
    break;

  case wb_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 5, 9 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wb_autumn_harrow, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1, 9 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 1, 9 );
      }
      SimpleEvent( d1, wb_autumn_sow, false );
    }
    break;

  case wb_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
			    g_date->DayInYear( 15,9 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wb_autumn_sow, true );
      break;
    }
    {
      int d1 = g_date->Date() + 14;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 15, 9 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 15, 9 );
      }
      SimpleEvent( d1, wb_herbicide_one, false );
    }
    break;

  case wb_herbicide_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) floor(100*herbi_app_prop * m_farm->Prob_multiplier()+0.5) )) //modified probability
    {
		  if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 30,9 ) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wb_herbicide_one, true );
			break;
		  }
    }
    if ( m_farm->IsStockFarmer()) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		   wb_fertslurry_stock_two, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ) + 365,
		   wb_fertnpk_stock_one, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 ) + 365,
		   wb_herbicide_two, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ) + 365,
		   wb_fungicide_one, false );
    } else {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16,9 ),
		   wb_fertmanganese_plant_one, false );
    }
    break;

  case wb_fertmanganese_plant_one:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
					 g_date->DayInYear( 30,10 ) -
					 g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_fertmanganese_plant_one, true );
	break;
      }
      // Did first, so do second manganese too.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		   wb_fertmanganese_plant_two, false );
    } else {
      // Event 'wb_fertmanganese_plant_two' was not queued up, so we will
      // signal its completion already here...
      WB_DID_SEVEN_ONE = true;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ) + 365,
		 wb_fertnpk_plant_one, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 ) + 365,
		 wb_herbicide_two, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ) + 365,
		 wb_fungicide_one, false );
    break;

  case wb_fertmanganese_plant_two:
    if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
				       g_date->DayInYear( 5,5 ) -
				       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wb_fertmanganese_plant_two, true );
      break;
    }
    WB_DID_SEVEN_ONE = true;
    if ( WB_DID_SEVEN_TWO &&
	 WB_DID_SEVEN_THREE &&
	 WB_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wb_switchboard, false );
    }
    break;

  case wb_fertnpk_plant_one:
    if (!m_farm->FP_NPK( m_field, 0.0,
			 g_date->DayInYear( 1,5 ) -
			 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wb_fertnpk_plant_one, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 2,5 ),
		   wb_fertnpk_plant_two, false );
    break;

  case wb_fertnpk_plant_two:
    if ( m_ev->m_lock || m_farm->DoIt( 15 )) {
      if (!m_farm->FP_NPK( m_field, 0.0,
			   g_date->DayInYear( 20,5 ) -
			   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_fertnpk_plant_two, true );
	break;
      }
    }
    WB_DID_SEVEN_TWO = true;
    if ( WB_DID_SEVEN_ONE &&
	 WB_DID_SEVEN_THREE &&
	 WB_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wb_switchboard, false );
    }
    break;

  case wb_fertslurry_stock_two:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 15,5 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_fertslurry_stock_two, true );
	break;
      }
    }
    WB_DID_SEVEN_ONE = true;
    if ( WB_DID_SEVEN_TWO &&
	 WB_DID_SEVEN_THREE &&
	 WB_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wb_switchboard, false );
    }
    break;

  case wb_fertnpk_stock_one:
    if ( m_ev->m_lock || m_farm->DoIt( 90 )) {
      if (!m_farm->FA_NPK( m_field, 0.0,
			   g_date->DayInYear( 30,4 ) -
			   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_fertnpk_stock_one, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
		   wb_fertnpk_stock_two, false );
    break;

  case wb_fertnpk_stock_two:
    if ( m_ev->m_lock || m_farm->DoIt( 14 )) {
      if (!m_farm->FA_NPK( m_field, 0.0,
			   g_date->DayInYear( 15,5 ) -
			   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_fertnpk_stock_two, true );
	break;
      }
    }
    WB_DID_SEVEN_TWO = true;
    if ( WB_DID_SEVEN_ONE &&
	 WB_DID_SEVEN_THREE &&
	 WB_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wb_switchboard, false );
    }
    break;

  case wb_herbicide_two:
    if ( m_ev->m_lock || m_farm->DoIt( (int) floor(35*herbi_app_prop*WB_DECIDE_TO_HERB * m_farm->Prob_multiplier()+0.5 ))) { //modified probability
      if (!m_farm->HerbicideTreat( m_field, 0.0,
				   g_date->DayInYear( 30,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_herbicide_two, true );
	break;
      }
      WB_HERBICIDE_DATE = g_date->Date();
    }
    WB_DID_SEVEN_THREE = true;
    if ( WB_DID_SEVEN_ONE &&
	 WB_DID_SEVEN_TWO &&
	 WB_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wb_switchboard, false );
    }
    break;

  case wb_fungicide_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (65*cfg_fungi_app_prop1.value()  * m_farm->Prob_multiplier()))) { //modified probability

		  if (!m_farm->FungicideTreat( m_field, 0.0, g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wb_fungicide_one, true );
			break;
		  }
		
      WB_FUNGICIDE_DATE = g_date->Date();
    }
    WB_DID_SEVEN_FOUR = true;
    if ( WB_DID_SEVEN_ONE &&
	 WB_DID_SEVEN_TWO &&
	 WB_DID_SEVEN_THREE ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wb_switchboard, false );
    }
    break;

  case wb_switchboard:
    // Welcome to the Winterbarley switchboard, where all threads
    // meet and have a jolly good time.

    // Reset all flags for the second round of parallel execution.
    // Flags double as date indicators when each action took place
    // (for checking proximity to watering).
    WB_DID_EIGHT_ONE   = false;
    WB_DID_EIGHT_TWO   = false;
    WB_DID_EIGHT_THREE = false;
    WB_DID_EIGHT_FOUR  = false;

    // Start all four threads.
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
		   wb_fungicide_two, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
		   wb_growth_reg, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
		   wb_water, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
		   wb_insecticide, false );
    break;

  case wb_fungicide_two:
    if ( g_date->Date() < WB_DID_EIGHT_THREE + 1 ||
	 g_date->Date() < WB_FUNGICIDE_DATE + 7 ) {
      // Try again tomorrow.
      SimpleEvent( g_date->Date() + 1, wb_fungicide_two, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (15*cfg_fungi_app_prop1.value() *WB_DECIDE_TO_HERB * m_farm->Prob_multiplier()))) { //modified probability
      if (!m_farm->FungicideTreat( m_field, 0.0,
				   g_date->DayInYear( 30,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_fungicide_two, true );
	break;
      }
    }
    WB_DID_EIGHT_ONE = g_date->Date();
    if ( WB_DID_EIGHT_TWO &&
	 WB_DID_EIGHT_THREE &&
	 WB_DID_EIGHT_FOUR ) {
	ChooseNextCrop (3);
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
		   wb_harvest, false );
    }
    break;

  case wb_growth_reg:
    if ( g_date->Date() < WB_DID_EIGHT_THREE + 1 ) {
      // Try again tomorrow.
      SimpleEvent( g_date->Date() + 1, wb_growth_reg, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (50*cfg_greg_app_prop.value() ))) {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				    g_date->DayInYear( 31,5 ) -
				    g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_growth_reg, true );
	break;
      }
    }
    WB_DID_EIGHT_TWO = g_date->Date();
    if ( WB_DID_EIGHT_ONE &&
	 WB_DID_EIGHT_THREE &&
	 WB_DID_EIGHT_FOUR ) {
		 ChooseNextCrop (3);
		SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ), wb_harvest, false );
    }
    break;

  case wb_water:
    if ( g_date->Date() < WB_HERBICIDE_DATE + 1 ||
	 g_date->Date() < WB_DID_EIGHT_ONE  + 1 ||
	 g_date->Date() < WB_DID_EIGHT_TWO  + 1 ||
	 g_date->Date() < WB_DID_EIGHT_FOUR + 1 ) {
      // Try again tomorrow.
      SimpleEvent( g_date->Date() + 1, wb_water, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 15,6 ) -
			  g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wb_water, true );
	break;
      }
    }
    WB_DID_EIGHT_THREE = g_date->Date();
    if ( WB_DID_EIGHT_ONE &&
	 WB_DID_EIGHT_TWO &&
	 WB_DID_EIGHT_FOUR ) {
		 ChooseNextCrop (3);
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
		   wb_harvest, false );
    }
    break;

  case wb_insecticide:
    if ( g_date->Date() < WB_DID_EIGHT_THREE + 1 ) {
      // Try again tomorrow.
      SimpleEvent( g_date->Date() + 1, wb_insecticide, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) floor(0.5+16*ins_app_prop  * m_farm->Prob_multiplier()))) { //modified probability

		  if (!m_farm->InsecticideTreat( m_field, 0.0, g_date->DayInYear( 1,6 ) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wb_insecticide, true );
			break;
		  
		}
    }
    WB_DID_EIGHT_FOUR = g_date->Date();
    if ( WB_DID_EIGHT_ONE &&
	 WB_DID_EIGHT_TWO &&
	 WB_DID_EIGHT_THREE ) {
		 ChooseNextCrop (3);
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
		   wb_harvest, false );
    }
    break;

  case wb_harvest:
    if (m_field->GetMConstants(0)==0) {
		if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "WinterBarley::Do(): failure in 'Harvest' execution", "" );
			exit( 1 );
		} 
	}
	else{  
	  if (!m_farm->Harvest( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
		  SimpleEvent( g_date->Date() + 1, wb_harvest, true );
		  break;
	  }
    }

    if ( m_farm->IsStockFarmer()) {
      if ( m_farm->DoIt( 15 )) {
	// Force straw chopping on the same day as harvest.
	m_farm->StrawChopping( m_field, 0.0, 0 );
	SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1), wb_stubble_harrowing, false );
	break;
      }
      // No chopping and harrowing, so try hay turning.
      SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2), wb_hay_turning, false );
      break;
    } else {
      // Plant farmer.
      if ( m_farm->DoIt( 75 )) {
	// Force straw chopping on the same day as harvest.
	m_farm->StrawChopping( m_field, 0.0, 0 );
	SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1), wb_stubble_harrowing, false );
	break;
      }
      SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2), wb_hay_turning, false );
    }
    break;

  case wb_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
		if (m_field->GetMConstants(2)==0) {
			if (!m_farm->HayTurning( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterBarley::Do(): failure in 'HayTurning' execution", "" );
				exit( 1 );
			} 
		}
		else {  
		  if (!m_farm->HayTurning( m_field, 0.0, m_field->GetMDates(1,2) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wb_hay_turning, true );
			break;
		  }
		}
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2), wb_hay_bailing, false );
    break;

  case wb_hay_bailing:
    if (m_field->GetMConstants(2)==0) {
			if (!m_farm->HayBailing( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterBarley::Do(): failure in 'HayBailing' execution", "" );
				exit( 1 );
			} 
	}
	else {    
		if (!m_farm->HayBailing( m_field, 0.0, m_field->GetMDates(1,2) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wb_hay_bailing, true );
			break;
		}
	}
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + m_field->GetMDates(0,1)) {
			d1 = g_date->OldDays() + m_field->GetMDates(0,1);
      }
      SimpleEvent( d1, wb_stubble_harrowing, false );
    }
    break;

  case wb_stubble_harrowing:
    if ( m_ev->m_lock || m_farm->DoIt( 40 )) {
		if (m_field->GetMConstants(2)==0) {
			if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "WinterBarley::Do(): failure in 'StubbleHarrowing' execution", "" );
				exit( 1 );
			} 
		}
		else { 
		  if (!m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,2) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, wb_stubble_harrowing, true );
			break;
		  }
		}
	}
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "WinterBarley::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


