/**
\file
\brief
<B>FI_SpringOats.h This file contains the headers for the WinterOats class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// SpringOats.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FI_SPRINGOATS_H
#define FI_SPRINGOATS_H

#define FI_SO_BASE 72300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define FI_SO_AUTUMN_PLOUGH        a_field->m_user[1]
#define FI_SO_DECIDE_TO_HERB       a_field->m_user[2]
#define FI_SO_DECIDE_TO_FI         a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing spring oats, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	fi_so_start = 1, // Compulsory, must always be 1 (one).
	fi_so_sleep_all_day = FI_SO_BASE,
	fi_so_stubble_cultivator1,
	fi_so_autumn_plough,
	fi_so_spring_plough,
	fi_so_fungicide1,
	fi_so_preseeding_cultivation,
	fi_so_preseeding_sow,
	fi_so_sow,
	fi_so_fertilizer,
	fi_so_harrow,
	fi_so_herbicide1,
	fi_so_insecticide,
	fi_so_growth_reg,
	fi_so_fungicide2,
	fi_so_harvest,
	fi_so_herbicide2,
	fi_so_straw_chopping,
	fi_so_hay_bailing,
	fi_so_stubble_cultivator2,
	fi_so_foobar,
} FI_SpringOatsToDo;


/**
\brief
FI_SpringOats class
\n
*/
/**
See FI_SpringOats.h::FI_SpringOatsToDo for a complete list of all possible events triggered codes by the spring oats management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class FI_SpringOats : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	FI_SpringOats(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(15, 10);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (fi_so_foobar - FI_SO_BASE);
		m_base_elements_no = FI_SO_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	fi_so_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	fi_so_sleep_all_day = FI_SO_BASE,
			fmc_Cultivation,	//	fi_so_stubble_cultivator1,
			fmc_Cultivation,	//	fi_so_autumn_plough,
			fmc_Cultivation,	//	fi_so_spring_plough,
			fmc_Fungicide,	//	fi_so_fungicide1,
			fmc_Cultivation,	//	fi_so_preseeding_cultivation,
			fmc_Cultivation,	//	fi_so_preseeding_sow,
			fmc_Others,	//	fi_so_sow,
			fmc_Fertilizer,	//	fi_so_fertilizer,
			fmc_Cultivation,	//	fi_so_harrow,
			fmc_Herbicide,	//	fi_so_herbicide1,
			fmc_Insecticide,	//	fi_so_insecticide,
			fmc_Others,	//	fi_so_growth_reg,
			fmc_Fungicide,	//	fi_so_fungicide2,
			fmc_Harvest,	//	fi_so_harvest,
			fmc_Herbicide,	//	fi_so_herbicide2,
			fmc_Others,	//	fi_so_straw_chopping,
			fmc_Others,	//	fi_so_hay_bailing,
			fmc_Cultivation	//	fi_so_stubble_cultivator2,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // FI_SPRINGOATS_H

