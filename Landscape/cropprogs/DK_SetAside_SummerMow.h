/**
\file
\brief
<B>DK_SetAside_SummerMow.h This file contains the headers for the DK_SetAside_SummerMow class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_SetAside_SummerMow.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_SETASIDE_SM_H
#define DK_SETASIDE_SM_H

#define DK_SASM_EVERY_2ND_YEAR a_field->m_user[0]
#define DK_SA_EST a_field->m_user[1]

#define DK_SASM_BASE 64100
/**
\
*/

/** Below is the list of things that a farmer can do if he is growing SetAside_SummerMow, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_sasm_start = 1, // Compulsory, must always be 1 (one).
	dk_sasm_harrow = DK_SASM_BASE,
	dk_sasm_wait,
	dk_sasm_wait1,
	dk_sasm_wait2,
	dk_sasm_roll,
	dk_sasm_sow,
	dk_sasm_cutting,
	dk_sasm_strigling,
	dk_sasm_swathing,
	dk_sasm_herbicide,
	dk_sasm_foobar,
} DK_SetAside_SummerMowToDo;


/**
\brief
DK_SetAside_SummerMow class
\n
*/
/**
See DK_SetAside_SummerMow.h::DK_SetAside_SummerMowToDo for a complete list of all possible events triggered codes by the DK_SetAside_SummerMow management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_SetAside_SummerMow : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_SetAside_SummerMow(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_sasm_foobar - DK_SASM_BASE);
		m_base_elements_no = DK_SASM_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_sasm_start = 1, // Compulsory, must always be 1 (one).
			fmc_Cultivation,	//	dk_sasm_harrow = DK_SASM_BASE
			fmc_Cultivation,	//	dk_sasm_roll 
			fmc_Others,	//	dk_sasm_wait,
			fmc_Others,	//	dk_sasm_wait1,
			fmc_Others,	//	dk_sasm_wait2,
			fmc_Others,	//	dk_sasm_sow,
			fmc_Cutting,	//	dk_sasm_cutting, 
			fmc_Cultivation,	//	dk_sasm_strigling,
			fmc_Cutting,	//	dk_sasm_swathing,
			fmc_Herbicide,	//	dk_sasm_herbicide,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_SetAside_SummerMow_H