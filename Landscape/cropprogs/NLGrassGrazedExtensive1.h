/**
\file
\brief
<B>NLGrassGrazedExtensive1.h This file contains the headers for the TemporalGrassGrazedExtensive1 class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLGrassGrazedExtensive1.h
//


#ifndef NLGRASSGRAZEDEXTENSIVE1_H
#define NLGRASSGRAZEDEXTENSIVE1_H

#define NLGRASSGRAZEDEXTENSIVE1_BASE 20000
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_GGE1_CUT_DATE		a_field->m_user[1]
#define NL_GGE1_WATER_DATE		a_field->m_user[2]


/** Below is the list of things that a farmer can do if he is growing TemporalGrassGrazedExtensive1, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_gge1_start = 1, // Compulsory, must always be 1 (one).
	nl_gge1_sleep_all_day = NLGRASSGRAZEDEXTENSIVE1_BASE,
	nl_gge1_winter_plough,
	nl_gge1_herbicide,
	nl_gge1_ferti_p1,
	nl_gge1_ferti_s1,
	nl_gge1_preseeding_cultivator,
	nl_gge1_spring_sow,
	nl_gge1_ferti_p2,
	nl_gge1_ferti_s2,
	nl_gge1_cut_to_silage1,
	nl_gge1_cut_to_silage2,
	nl_gge1_ferti_p3,
	nl_gge1_ferti_s3,
	nl_gge1_ferti_p4,
	nl_gge1_ferti_s4,
	nl_gge1_ferti_p5,
	nl_gge1_ferti_s5,
	nl_gge1_ferti_p6,
	nl_gge1_ferti_s6,
	nl_gge1_ferti_p7,
	nl_gge1_ferti_s7,
	nl_gge1_watering,
	nl_gge1_cattle_out,
	nl_gge1_cattle_is_out,
	nl_gge1_foobar
} NLGrassGrazedExtensive1ToDo;


/**
\brief
NLGrassGrazedExtensive1 class
\n
*/
/**
See NLGrassGrazedExtensive1.h::NLGrassGrazedExtensive1ToDo for a complete list of all possible events triggered codes by the TemporalGrassGrazedExtensive1 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLGrassGrazedExtensive1: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLGrassGrazedExtensive1(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,11 );
		m_forcespringpossible = true;
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_gge1_foobar - NLGRASSGRAZEDEXTENSIVE1_BASE);
	   m_base_elements_no = NLGRASSGRAZEDEXTENSIVE1_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
		    fmc_Others,//nl_gge1_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_gge1_sleep_all_day = NLGRASSGRAZEDEXTENSIVE1_BASE,
			fmc_Cultivation,//nl_gge1_winter_plough,
			fmc_Herbicide,//nl_gge1_herbicide,
			fmc_Fertilizer,//nl_gge1_ferti_p1,
			fmc_Fertilizer,//nl_gge1_ferti_s1,
			fmc_Cultivation,//nl_gge1_preseeding_cultivator,
			fmc_Others,//nl_gge1_spring_sow,
			fmc_Fertilizer,//nl_gge1_ferti_p2,
			fmc_Fertilizer,//nl_gge1_ferti_s2,
			fmc_Cutting,//nl_gge1_cut_to_silage1,
			fmc_Cutting,//nl_gge1_cut_to_silage2,
			fmc_Fertilizer,//nl_gge1_ferti_p3,
			fmc_Fertilizer,//nl_gge1_ferti_s3,
			fmc_Fertilizer,//nl_gge1_ferti_p4,
			fmc_Fertilizer,//nl_gge1_ferti_s4,
			fmc_Fertilizer,//nl_gge1_ferti_p5,
			fmc_Fertilizer,//nl_gge1_ferti_s5,
			fmc_Fertilizer,//nl_gge1_ferti_p6,
			fmc_Fertilizer,//nl_gge1_ferti_s6,
			fmc_Fertilizer,//nl_gge1_ferti_p7,
			fmc_Fertilizer,//nl_gge1_ferti_s7,
			fmc_Watering,//nl_gge1_watering,
			fmc_Grazing,//nl_gge1_cattle_out,
			fmc_Grazing//nl_gge1_cattle_is_out,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLGRASSGRAZEDEXTENSIVE1_H

