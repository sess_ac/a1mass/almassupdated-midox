/**
\file
\brief
<B>BECatchPeaCrop.h This file contains the headers for the CatchPeaCrop class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BECatchPeaCrop.h
//


#ifndef BECATCHPEACROP_H
#define BECATCHPEACROP_H

#define BECATCHPEACROP_BASE 26100

/** Below is the list of things that a farmer can do if he is growing CatchPeaCrop, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_cpc_start = 1, // Compulsory, must always be 1 (one).
	BE_cpc_sleep_all_day = BECATCHPEACROP_BASE,
	BE_cpc_stubble_cultivator,
	BE_cpc_ferti_p1_sand,
	BE_cpc_ferti_s1_sand, 
	BE_cpc_ferti_p1_clay,
	BE_cpc_ferti_s1_clay,
	BE_cpc_preseeding_cultivator_with_sow,
	BE_cpc_winter_plough_clay,
} BECatchPeaCropToDo;


/**
\brief
BECatchPeaCrop class
\n
*/
/**
See BECatchPeaCrop.h::BECatchPeaCropToDo for a complete list of all possible events triggered codes by the CatchPeaCrop management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BECatchPeaCrop: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BECatchPeaCrop(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,10 );
		SetCropClassification(tocc_Catch);
   }
};

#endif // BECATCHPEACROP_H

