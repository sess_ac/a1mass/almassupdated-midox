/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_SeedGrassRye_Spring.cpp This file contains the source for the DK_SeedGrassRye_Spring class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of May 2022 \n
 \n
*/
//
// DK_SeedGrassRye_Spring.cpp
//
/*

Copyright (c) 2022, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_SeedGrassRye_Spring.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_SeedGrassRye_Spring_SkScrapes("DK_CROP_SGR_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_SeedGrassRye_Spring_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_SGR_InsecticideDay;
extern CfgInt   cfg_SGR_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DK_SeedGrassRye_Spring::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKSeedGrassRye_Spring;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_sgr_start:
	{
		DK_SGR_EVERY_2ND_YEAR = 0;
		DK_SGR_FORCESPRING = false;
		a_field->ClearManagementActionSum();
		m_last_date = g_date->DayInYear(1, 9); // Should match the last flexdate below
	 //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(3 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 8); // last possible day of swathing in this case 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(31, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - harvest
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(31, 8); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - straw chopping
		flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
		flexdates[3][1] = g_date->DayInYear(1, 9); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) - burn straw

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(dk_sgr_spring_plough))) break;
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_ev->m_forcespring) {
			if ((DK_SGR_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 0)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
				if (g_date->Date() >= d1) d1 += 365;
				SimpleEvent(d1, dk_sgr_ferti_s2, false);
			}
			else if ((DK_SGR_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 1)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
				if (g_date->Date() >= d1) d1 += 365;
				SimpleEvent(d1, dk_sgr_ferti_s2, false);
			}
			else if ((DK_SGR_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 2)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
				if (g_date->Date() >= d1) d1 += 365;
				SimpleEvent(d1, dk_sgr_spring_plough, false);
			}
			else if ((DK_SGR_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 3)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
				if (g_date->Date() >= d1) d1 += 365;
				SimpleEvent(d1, dk_sgr_spring_plough, false);
			}
			
			DK_SGR_FORCESPRING = true;
			break;
		}
		else 

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date
		if ((DK_SGR_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, dk_sgr_ferti_s2, false);
		}
		else if ((DK_SGR_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, dk_sgr_ferti_s2, false);
		}
		else if ((DK_SGR_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 2)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
			SimpleEvent(d1, dk_sgr_autumn_plough, false);
		}
		else if ((DK_SGR_EVERY_2ND_YEAR + g_date->GetYearNumber()) % 3 == 3)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
			SimpleEvent(d1, dk_sgr_autumn_plough, false);
		}
		break;

	}
	break;
	// LKM: This is the first real farm operation
	case dk_sgr_autumn_plough:
		if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.95)) {
				if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_sgr_autumn_plough, true);
					break;
				}
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_sgr_spring_plough, false);
		break;

	case dk_sgr_spring_plough:
		if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sgr_spring_plough, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_sgr_sow, false);
		break;

	case dk_sgr_sow: // sow mix of seedgrass and cover crop - % doing cover crop is unknown atm
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sgr_sow, true);
			break;
		}
		SimpleEvent(g_date->Date() + 7, dk_sgr_herbicide1, false);
		break;

	case dk_sgr_herbicide1:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(22, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sgr_herbicide1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 7, dk_sgr_herbicide2, false);
		break;

	case dk_sgr_herbicide2:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sgr_herbicide2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_sgr_harvest, false);
		break;

	case dk_sgr_harvest: // harvest of cover crop
		if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
			if (!a_farm->Harvest(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_harvest, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_sgr_remove_straw1, false);
		break;

	case dk_sgr_remove_straw1:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
			if (!a_farm->StrawRemoval(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_remove_straw1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_sgr_ferti_s1, false);
		break;

	case dk_sgr_ferti_s1:
		if (a_farm->IsStockFarmer()) {
			if (a_ev->m_lock || a_farm->DoIt_prob(0.3)) {
				if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_sgr_ferti_s1, true);
					break;
				}
			}
			// End of management
			done = true;
			break;
		}
		else {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_sgr_ferti_p1, false);
			break;
		}

	case dk_sgr_ferti_p1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.3)) {
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_ferti_p1, true);
				break;
			}
		}
		// End of management
		done = true;
		break;

		// year 1+2 after establishment year:
	case dk_sgr_ferti_s2:
		if (a_farm->IsStockFarmer()) {
			if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
				if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_sgr_ferti_s2, true);
					break;
				}
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_sgr_herbicide3, false);
			break;
		}
		else {
			SimpleEvent(g_date->Date(), dk_sgr_ferti_p2, false);
			break;
		}

	case dk_sgr_ferti_p2:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_ferti_p2, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_sgr_herbicide3, false);
		break;

	case dk_sgr_herbicide3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.6)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_herbicide3, true);
				break;
			}
		}
		//here comes a fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_sgr_fungicide1, false); // fungi thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_sgr_growth_reg, false); // GR thread 
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_sgr_ferti_s3, false); // ferti thread  - main thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_sgr_ferti_p3, false); // ferti thread  - main thread
		break;

	case dk_sgr_fungicide1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.3)) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_fungicide1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+14, dk_sgr_fungicide2, false); 
		break;

	case dk_sgr_fungicide2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.5)) {
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_fungicide2, true);
				break;
			}
		}
		break; //end of thread

	case dk_sgr_growth_reg:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.8)) {
			if (!a_farm->GrowthRegulator(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_growth_reg, true);
				break;
			}
		}
		break; // end of thread

	case dk_sgr_ferti_s3:
		if (a_farm->IsStockFarmer()) {
			if (a_ev->m_lock || a_farm->DoIt_prob(0.90)) { 
				if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_sgr_ferti_s3, true);
					break;
				}

			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_sgr_herbicide4, false);
			break;
		}
		else {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_sgr_ferti_p3, false);
			break;
		}

	case dk_sgr_ferti_p3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.90)) {
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_ferti_p3, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_sgr_herbicide4, false);
		break;

	case dk_sgr_herbicide4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60)) { 
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_herbicide4, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_sgr_swathing, false);
		break;

	case dk_sgr_swathing:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.03)) {
			if (!a_farm->Swathing(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_swathing, true);
				break;
			}
			SimpleEvent(g_date->Date()+8, dk_sgr_seed_harvest, false);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_sgr_seed_harvest, false);
		break;
		

	case dk_sgr_seed_harvest:
		if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
			if (!a_farm->Harvest(a_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_seed_harvest, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_sgr_straw_chopping2, false);
		break;

	case dk_sgr_straw_chopping2: // often the ryegrass will stay as a catch crop (thus no straw chopping/burning for 90%)
		if (a_ev->m_lock || a_farm->DoIt_prob(0.1)) {
			if (!a_farm->StrawChopping(a_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_sgr_straw_chopping2, true);
				break;
			}
			SimpleEvent(g_date->Date(), dk_sgr_burn_straw, false); 
			break;
		}
		// End of management
		done = true;
		break;

	case dk_sgr_burn_straw: 
		if (!a_farm->BurnStrawStubble(a_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_sgr_burn_straw, true);
			break;
		}
		// End of management
		done = true;
		break;
	
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "DK_SeedGrassRye_Spring::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}