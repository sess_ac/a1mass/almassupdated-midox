/**
\file
\brief
<B>DE_GrasslandSilageAnnual.h This file contains the headers for the DE_GrasslandSilageAnnual class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Susanne Stein \n
 Version of August 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_GrasslandSilageAnnual.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_GRASSLANDSILAGEANNUAL_H
#define DE_GRASSLANDSILAGEANNUAL_H

#define DE_GSA_BASE 36500
/**
\brief Flags
*/
#define DE_GSA_STUBBLE_PLOUGH	a_field->m_user[1]
#define DE_GSA_WINTER_PLOUGH	a_field->m_user[2]
#define DE_GSA_SPRING_FERTI a_field->m_user[3]
#define DE_GSA_HERBICIDE1	a_field->m_user[4]
#define DE_GSA_HERBI_DATE	a_field->m_user[5]
#define DE_GSA_FORCESPRING	a_field->m_user[6]


/** Below is the list of things that a farmer can do if he is growing grassland silage annual, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  de_gsa_start = 1, // Compulsory, must always be 1 (one).
  de_gsa_sleep_all_day = DE_GSA_BASE,
  de_gsa_herbicide0,
  de_gsa_ferti_p1,
  de_gsa_ferti_s1,
  de_gsa_stubble_plough,
  de_gsa_autumn_harrow1,
  de_gsa_autumn_harrow2,
  de_gsa_stubble_harrow,
  de_gsa_ferti_p2,
  de_gsa_ferti_s2,
  de_gsa_winter_plough,
  de_gsa_winter_stubble_cultivator_heavy,
  de_gsa_spring_harrow,
  de_gsa_ferti_p3,
  de_gsa_ferti_s3,
  de_gsa_heavy_cultivator,
  de_gsa_herbicide1,
  de_gsa_preseeding_cultivator,
  de_gsa_spring_sow,
  de_gsa_herbicide2,
  de_gsa_cut_to_silage1,
  de_gsa_cut_to_silage2,
  de_gsa_foobar, // Obligatory, must be last
} DE_GrasslandSilageAnnualToDo;


/**
\brief
DE_GrasslandSilageAnnual class
\n
*/
/**
See DE_GrasslandSilageAnnual.h::DEGrasslandSilageAnnualToDo for a complete list of all possible events triggered codes by the grassland silage annual management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_GrasslandSilageAnnual: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_GrasslandSilageAnnual(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10 );
		m_forcespringpossible = true;
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_gsa_foobar - DE_GSA_BASE);
	   m_base_elements_no = DE_GSA_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			// ALL THE NECESSARY ENTRIES HERE
			fmc_Others,	//	  de_gsa_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	  de_gsa_sleep_all_day,
			fmc_Herbicide,	//	  de_gsa_herbicide0,
			fmc_Fertilizer,	//	  de_gsa_ferti_p1,
			fmc_Fertilizer,	//	  de_gsa_ferti_s1,
			fmc_Cultivation,	//	  de_gsa_stubble_plough,
			fmc_Cultivation,	//	  de_gsa_autumn_harrow1,
			fmc_Cultivation,	//	  de_gsa_autumn_harrow2,
			fmc_Cultivation,	//	  de_gsa_stubble_harrow,
			fmc_Fertilizer,	//	  de_gsa_ferti_p2,
			fmc_Fertilizer,	//	  de_gsa_ferti_s2,
			fmc_Cultivation,	//	  de_gsa_winter_plough,
			fmc_Cultivation,	//	  de_gsa_winter_stubble_cultivator_heavy,
			fmc_Cultivation,	//	  de_gsa_spring_harrow,
			fmc_Fertilizer,	//	  de_gsa_ferti_p3,
			fmc_Fertilizer,	//	  de_gsa_ferti_s3,
			fmc_Cultivation,	//	  de_gsa_heavy_cultivator,
			fmc_Herbicide,	//	  de_gsa_herbicide1,
			fmc_Cultivation,	//	  de_gsa_preseeding_cultivator,
			fmc_Cultivation,	//	  de_gsa_spring_sow,
			fmc_Herbicide,	//	  de_gsa_herbicide2,
			fmc_Cutting,	//	  de_gsa_cut_to_silage1,
			fmc_Cutting,	//	  de_gsa_cut_to_silage2,
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DE_GRASSLANDSILAGEANNUAL_H

