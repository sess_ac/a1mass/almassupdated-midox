//
// DK_SpringBarleyCloverGrass.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following dissbaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following dissbaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSBMAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSBMUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSBMUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKSpringBarleyCloverGrass_H
#define DKSpringBarleyCloverGrass_H

#define DK_SBCG_BASE 160600
#define DK_SBCG_FORCESPRING	a_field->m_user[1]
#define DK_SBCG_TILL_C a_field->m_user[2]
#define DK_SBCG_TILL_S a_field->m_user[3]

typedef enum {
	dk_sbcg_start = 1, // Compulsory, start event must always be 1 (one).
	dk_sbcg_harvest = DK_SBCG_BASE,
	dk_sbcg_autumn_plough,
	dk_sbcg_ferti_s1,
	dk_sbcg_ferti_p1,
	dk_sbcg_ferti_s2,
	dk_sbcg_ferti_p2,
	dk_sbcg_spring_plough,
	dk_sbcg_spring_harrow_nt,
	dk_sbcg_spring_sow,
	dk_sbcg_herbicide1,
	dk_sbcg_herbicide2,
	dk_sbcg_herbicide3,
	dk_sbcg_herbicide4,
	dk_sbcg_insecticide,
	dk_sbcg_water1,
	dk_sbcg_water2,
	dk_sbcg_fungicide1,
	dk_sbcg_fungicide2,
	dk_sbcg_gr1,
	dk_sbcg_gr2,
	dk_sbcg_straw_chopping,
	dk_sbcg_hay_bailing, 
	dk_sbcg_stubble_harrow, 
	dk_sbcg_plough,
	dk_sbcg_cattle_out, 
	dk_sbcg_cattle_is_out,
	dk_sbcg_foobar,
} DK_SpringBarleyCloverGrassToDo;



class DK_SpringBarleyCloverGrass : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_SpringBarleyCloverGrass(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 11);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_sbcg_foobar - DK_SBCG_BASE);
		m_base_elements_no = DK_SBCG_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_sbcg_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_sbcg_harvest = DK_SBCG_BASE,
			  fmc_Cultivation,	//	  dk_sbcg_autumn_plough,
			  fmc_Fertilizer,	//	  dk_sbcg_ferti_s1,
			  fmc_Fertilizer,	//	  dk_sbcg_ferti_p1,
			  fmc_Fertilizer,	//	  dk_sbcg_ferti_s2,
			  fmc_Fertilizer,	//	  dk_sbcg_ferti_p2,
			  fmc_Cultivation,	//	  dk_sbcg_spring_plough,
			  fmc_Cultivation, // dk_sbcg_spring_harrow_nt,
			  fmc_Cultivation,	//	  dk_sbcg_spring_sow,
			  fmc_Herbicide,	//	  dk_sbcg_herbicide1,
			  fmc_Herbicide,	//	  dk_sbcg_herbicide2,
		      fmc_Herbicide,	//	  dk_sbcg_herbicide3,
		      fmc_Herbicide,	//	  dk_sbcg_herbicide4,
			  fmc_Insecticide,	//	  dk_sbcg_insecticide,
			  fmc_Watering,	//	  dk_sbcg_water1,
			  fmc_Watering,	//	  dk_sbcg_water2,
			  fmc_Fungicide,	//	  dk_sbcg_fungicide1,
			  fmc_Fungicide,	//	  dk_sbcg_fungicide2,
			  fmc_Others, //		dk_sbcg_gr1,
			  fmc_Others, //		dk_sbcg_gr2,
			  fmc_Others,	//	  dk_sbcg_straw_chopping,
			  fmc_Others,	//	  dk_sbcg_hay_bailing,
			  fmc_Cultivation, // dk_sbcg_stubble_harrow,
			  fmc_Cultivation, // dk_sbcg_plough,
			  fmc_Grazing, // dk_sbcg_cattle_out,
			  fmc_Grazing, // dk_sbcg_cattle_is_out,

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};


#endif // DK_SpringBarleyCloverGrass_H