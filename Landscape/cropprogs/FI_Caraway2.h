/**
\file
\brief
<B>FI_Caraway2.h This file contains the headers for the Caraway2 class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// FI_Caraway2.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FI_CARAWAY2_H
#define FI_CARAWAY2_H

#define FI_CW2_BASE 74400

/** Below is the list of things that a farmer can do if he is growing FabaBean, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  fi_cw2_start = 1, // Compulsory, must always be 1 (one).
  fi_cw2_sleep_all_day = FI_CW2_BASE,
  fi_cw2_nk,
  fi_cw2_npk1,
  fi_cw2_herbicide1,
  fi_cw2_insecticide,
  fi_cw2_fungicide,  
  fi_cw2_harvest,
  fi_cw2_herbicide2,
  fi_cw2_npk2_for_next_year,
  fi_cw2_foobar,
} FI_Caraway2ToDo;


/**
\brief
FI_Caraway2 class
\n
*/
/**
See FI_Caraway2.h::FI_Caraway2ToDo for a complete list of all possible events triggered codes by the Caraway2 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class FI_Caraway2 : public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   FI_Caraway2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,5 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (fi_cw2_foobar - FI_CW2_BASE);
	   m_base_elements_no = FI_CW2_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fi_cw2_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	  fi_cw2_sleep_all_day = FI_CW2_BASE,
			fmc_Fertilizer,	//	  fi_cw2_nk,
			fmc_Fertilizer,	//	  fi_cw2_npk1,
			fmc_Herbicide,	//	  fi_cw2_herbicide1,
			fmc_Insecticide,	//	  fi_cw2_insecticide,
			fmc_Fungicide,	//	  fi_cw2_fungicide,  
			fmc_Harvest,	//	  fi_cw2_harvest,
			fmc_Herbicide,	//	  fi_cw2_herbicide2,
			fmc_Fertilizer	//	  fi_cw2_npk2_for_next_year,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // FI_CARAWAY2_H

