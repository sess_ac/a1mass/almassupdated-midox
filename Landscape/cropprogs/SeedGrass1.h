//
// SeedGrass1.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#ifndef SEEDGRASS1_H
#define SEEDGRASS1_H

#define SEEDGRASS1_BASE 5800
#define SG1_WATER_DATE     m_field->m_user[0]
#define SG1_FUNGI_DATE     m_field->m_user[1]
#define SG1_DECIDE_TO_HERB m_field->m_user[2]

typedef enum {
  sg1_start = 1, // Compulsory, start event must always be 1 (one).
  sg1_ferti_zero = SEEDGRASS1_BASE,
  sg1_herbi_zero,
  sg1_herbi_one,
  sg1_fungi_zero,
  sg1_water_zero,
  sg1_water_zero_b,
  sg1_swarth,
  sg1_harvest,
  sg1_strawchopping,
  sg1_compress,
  sg1_burn_straw,
  sg1_foobar,
} SeedGrass1ToDo;



class SeedGrass1: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SeedGrass1(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(14,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sg1_foobar - SEEDGRASS1_BASE);
	  m_base_elements_no = SEEDGRASS1_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  sg1_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  sg1_ferti_zero = SEEDGRASS1_BASE,
			fmc_Herbicide,	//	  sg1_herbi_zero,
			fmc_Herbicide,	//	  sg1_herbi_one,
			fmc_Fungicide,	//	  sg1_fungi_zero,
			fmc_Watering,	//	  sg1_water_zero,
			fmc_Watering,	//	  sg1_water_zero_b,
			fmc_Cutting,	//	  sg1_swarth,
			fmc_Harvest,	//	  sg1_harvest,
			fmc_Others,	//	  sg1_strawchopping,
			fmc_Others,	//	  sg1_compress,
			fmc_Others,	//	  sg1_burn_straw,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SEEDGRASS1_H
