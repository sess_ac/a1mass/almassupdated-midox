/**
\file
\brief
<B>DE_SugarBeet.h This file contains the headers for the Beet class</B> \n
*/
/**
\file 
 by Chris J. Topping, Elzbieta Ziolkowska and modified by Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
\n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_SugarBeet.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_SugarBeet_H
#define DE_SugarBeet_H

#define DE_SugarBeet_BASE 35800
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DE_BE_FERTI_P1	a_field->m_user[1]
#define DE_BE_FERTI_S1	a_field->m_user[2]
#define DE_BE_WINTER_PLOUGH		a_field->m_user[3]
#define DE_BE_SPRING_FERTI		a_field->m_user[4]
#define DE_BE_HERBI_DATE		a_field->m_user[5]
#define DE_BE_HERBI1			a_field->m_user[6]
#define DE_BE_HERBI2			a_field->m_user[7]
/** Below is the list of things that a farmer can do if he is growing DE_SugarBeet, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_be_start = 1, // Compulsory, must always be 1 (one).
	de_be_sleep_all_day = DE_SugarBeet_BASE,
	de_be_autumn_harrow1,
	de_be_autumn_harrow2,
	de_be_ferti_p1,
	de_be_ferti_s1,
	de_be_ferti_p2,
	de_be_ferti_s2,
	de_be_winter_plough,
	de_be_winter_stubble_cultivator_heavy,
	de_be_spring_harrow,
	de_be_ferti_p4,
	de_be_ferti_s4,
	de_be_ferti_p5,
	de_be_ferti_s5,
	de_be_heavy_cultivator,
	de_be_preseeding_cultivator,
	de_be_preseeding_cultivator_sow,
	de_be_spring_sow,
	de_be_harrow_before_emergence,
	de_be_thinning,
	de_be_watering1,
	de_be_watering2,
	de_be_watering3,
	de_be_herbicide1,
	de_be_herbicide2,
	de_be_herbicide3,
	de_be_fungicide,
	de_be_insecticide,
	de_be_ferti_p6,
	de_be_ferti_s6,
	de_be_ferti_p7,
	de_be_ferti_s7,
	de_be_harvest,
	de_be_ferti_p8,
	de_be_ferti_s8,
	de_be_foobar, // Obligatory, must be last
} DE_SugarBeetToDo;


/**
\brief
DE_SugarBeet class
\n
*/
/**
See DE_SugarBeet.h::DE_SugarBeetToDo for a complete list of all possible events triggered codes by the Beet management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_SugarBeet: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_SugarBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of 
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date=g_date->DayInYear( 1,12 ); //EZ: Changed to mach last possible day of first operation in the plan, i.e, application of PK; for testing 1.12 instead of 5.11
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_be_foobar - DE_SugarBeet_BASE);
	   m_base_elements_no = DE_SugarBeet_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			// ALL THE NECESSARY ENTRIES HERE
			fmc_Others,			//	de_be_start = 1, // Compulsory, must always be 1 (one)
			fmc_Others,			//	de_be_sleep_all_day
			fmc_Cultivation,	//	de_be_autumn_harrow1,
			fmc_Cultivation,	//	de_be_autumn_harrow2,
			fmc_Fertilizer,		//	de_be_ferti_p1,
			fmc_Fertilizer,		//	de_be_ferti_s1,
			fmc_Fertilizer,		//	de_be_ferti_p2,
			fmc_Fertilizer,		//	de_be_ferti_s2,
			fmc_Cultivation,	//	de_be_winter_plough,
			fmc_Cultivation,	//	de_be_winter_stubble_cultivator_heavy,
			fmc_Cultivation,	//	de_be_spring_harrow,
			fmc_Fertilizer,		//	de_be_ferti_p4,
			fmc_Fertilizer,		//	de_be_ferti_s4,
			fmc_Fertilizer,		//	de_be_ferti_p5,
			fmc_Fertilizer,		//	de_be_ferti_s5,
			fmc_Cultivation,	//	de_be_heavy_cultivator,
			fmc_Cultivation,	//	de_be_preseeding_cultivator,
			fmc_Cultivation,	//	de_be_preseeding_cultivator_sow,
			fmc_Others,			//	de_be_spring_sow,
			fmc_Cultivation,	//	de_be_harrow_before_emergence,
			fmc_Others,			//	de_be_thinning,
			fmc_Watering,			//	de_be_watering1,
			fmc_Watering,			//	de_be_watering2,
			fmc_Watering,			//	de_be_watering3,
			fmc_Herbicide,		//	de_be_herbicide1,
			fmc_Herbicide,		//	de_be_herbicide2,
			fmc_Herbicide,		//	de_be_herbicide3,
			fmc_Fungicide,		//	de_be_fungicide,
			fmc_Insecticide,		//	de_be_insecticide,
			fmc_Fertilizer,		//	de_be_ferti_p6,
			fmc_Fertilizer,		//	de_be_ferti_s6,
			fmc_Fertilizer,		//	de_be_ferti_p7,
			fmc_Fertilizer,		//	de_be_ferti_s7,
			fmc_Harvest,		//	de_be_harvest,
			fmc_Fertilizer,		//	de_be_ferti_p8,
			fmc_Fertilizer,		//	de_be_ferti_s8,
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DE_SugarBeet_H

