//
// MaizeSilage.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef MAIZESILAGE_H
#define MAIZESILAGE_H

#define MAIZESILAGE_BASE 2100
#define MAIZESILAGE_SOW_DATE m_field->m_user[0]
#define MAIZESILAGE_HERBI_ONE_DATE m_field->m_user[1]

typedef enum {
  ms_start = 1, // Compulsory, start event must always be 1 (one).
  ms_fa_manure_a = MAIZESILAGE_BASE,
  ms_fa_manure_b,
  ms_autumn_plough,
  ms_fa_slurry_one,
  ms_spring_plough,
  ms_spring_harrow,
  ms_spring_sow,
  ms_fa_npk,
  ms_herbi_one,
  ms_row_one,
  ms_fa_slurry_two,
  ms_herbi_two,
  ms_row_two,
  ms_water_one,
  ms_water_two,
  ms_harvest,
  ms_stubble,
  ms_foobar,
} MaizeSilageToDo;



class MaizeSilage: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  MaizeSilage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(15,10);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (ms_foobar - MAIZESILAGE_BASE);
	  m_base_elements_no = MAIZESILAGE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  ms_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  ms_fa_manure_a = MAIZESILAGE_BASE,
			fmc_Fertilizer,	//	  ms_fa_manure_b,
			fmc_Cultivation,	//	  ms_autumn_plough,
			fmc_Fertilizer,	//	  ms_fa_slurry_one,
			fmc_Cultivation,	//	  ms_spring_plough,
			fmc_Cultivation,	//	  ms_spring_harrow,
			fmc_Others,	//	  ms_spring_sow,
			fmc_Fertilizer,	//	  ms_fa_npk,
			fmc_Herbicide,	//	  ms_herbi_one,
			fmc_Cultivation,	//	  ms_row_one,
			fmc_Fertilizer,	//	  ms_fa_slurry_two,
			fmc_Herbicide,	//	  ms_herbi_two,
			fmc_Cultivation,	//	  ms_row_two,
			fmc_Watering,	//	  ms_water_one,
			fmc_Watering,	//	  ms_water_two,
			fmc_Harvest,	//	  ms_harvest,
			fmc_Cultivation	//	  ms_stubble,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // MAIZESILAGE_H
