//
//  OCloverGrassGrazed1.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OCloverGrassGrazed1.h"


using namespace std;

extern CfgBool cfg_organic_extensive;
extern CfgFloat cfg_silage_prop;

bool OCloverGrassGrazed1::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;
  int d1 = 0;
  int noDates= 2;

  switch ( m_ev->m_todo ) {
  case ocgg1_start:
  {
      a_field->ClearManagementActionSum();

	  // Set up the date management stuff
	  // Could save the start day in case it is needed later
	  // m_field->m_startday = m_ev->m_startday;
	  m_last_date = g_date->DayInYear(10, 10);
	  // Start and stop dates for all events after harvest
	  m_field->SetMDates(0, 0, g_date->DayInYear(7, 8));
	  // Determined by harvest date - used to see if at all possible
	  m_field->SetMDates(1, 0, g_date->DayInYear(1, 10));
	  m_field->SetMDates(0, 1, g_date->DayInYear(10, 10));
	  m_field->SetMDates(1, 1, g_date->DayInYear(10, 10));
	  // Check the next crop for early start, unless it is a spring crop
	  // in which case we ASSUME that no checking is necessary!!!!
	  // So DO NOT implement a crop that runs over the year boundary
	  if (m_ev->m_startday > g_date->DayInYear(1, 7))
	  {
		  if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
		  {
			  g_msg->Warn(WARN_BUG, "OCloverGrassGrazed1::Do(): "
				  "Harvest too late for the next crop to start!!!", "");
			  exit(1);
		  }
		  // Now fix any late finishing problems
		  for (int i = 0; i < noDates; i++)
		  {
			  if (m_field->GetMDates(0, i) >= m_ev->m_startday)
				  m_field->SetMDates(0, i, m_ev->m_startday - 1);
			  if (m_field->GetMDates(1, i) >= m_ev->m_startday)
				  m_field->SetMDates(1, i, m_ev->m_startday - 1);
		  }
	  }
	  // Now no operations can be timed after the start of the next crop.
	  // Added test for management plan testing.
	  if (!(m_ev->m_first_year)) {
		  // Are we before July 1st?
		  d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		  if (g_date->Date() < d1) {
			  // Yes, too early. We assumme this is because the last crop was late
			  g_msg->Warn(WARN_BUG, "OCloverGrassGrazed1::Do(): "
				  "Crop start attempt between 1st Jan & 1st July", "");
			  exit(1);
		  }
		  else {
			  d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
			  if (g_date->Date() > d1) {
				  // Yes too late - should not happen - raise an error
				  g_msg->Warn(WARN_BUG, "OCloverGrassGrazed1::Do(): "
					  "Crop start attempt after last possible start date",
					  "");
				  exit(1);
			  }
		  }
	  }
	  else {
		  // First (invisible) year or testing. Force the correct starting date.
		  d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		  // If testing and not the first year, then push all events
		  // into next year (start event is called in the autumn after
		  // the current year has finished).
		  if (!m_ev->m_first_year) d1 += 365;
	  }
	  m_field->SetLastSownVeg(m_field->GetVegType()); //Force last sown, needed for goose habitat classification
	  // OK, let's go.
	  OCGG1_CUT_DATE = 0;
	  SimpleEvent(d1, ocgg1_ferti_zero, false);
  }
  break;

  case ocgg1_ferti_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgg1_ferti_zero, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 5 ),
                 ocgg1_cut_to_silage, false );
    // Start a watering thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 6 ),
                 ocgg1_water_zero, false );
    break;

  case ocgg1_cut_to_silage:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (cfg_silage_prop.value()*100 )))
    {
      if (!m_farm->CutToSilage( m_field, 0.0,
           g_date->DayInYear( 15, 6 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgg1_cut_to_silage, true );
        break;
      }
      // Success so queue up the next event
      OCGG1_CUT_DATE=g_date->DayInYear();
      if ( OCGG1_CUT_DATE+14>g_date->DayInYear(25,5) )
      	SimpleEvent( g_date->OldDays() + OCGG1_CUT_DATE+14,
         		         ocgg1_cattle_out, false );
      else
      	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 5 ),
        		         ocgg1_cattle_out, false );
      break;
    }
    else
    {
      // Didn't want to cut so go direct to graze
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 5 ),
	                 ocgg1_cattle_out, true );

    }
    break;

  case ocgg1_water_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 30 ))
    {
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 15, 6 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgg1_water_zero, true );
        break;
      }
      // Success
     	if ( (g_date->DayInYear()+7)<g_date->DayInYear( 25,6 ) )
	    	SimpleEvent( g_date->OldDays() + (g_date->DayInYear()+7),
  	                ocgg1_water_one, false );
     	else
     		SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,6 ),
        		         ocgg1_water_one, false );
     	break;
   	}
   	// Didn't water so let the thread die
   	break;

  case ocgg1_water_one:
    if ( m_ev->m_lock || m_farm->DoIt( 67 ))    //**CJT** check this or 20
    {
    	// --FN--
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 25, 7 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgg1_water_one, true );
        break;
      }
    }
    break;
    // End of watering thread

  case ocgg1_cattle_out:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (cfg_organic_extensive.value()) {
        if (!m_farm->CattleOutLowGrazing( m_field, 0.0,
          m_field->GetMDates(1,0) - g_date->DayInYear())) {
          SimpleEvent( g_date->Date() + 1, ocgg1_cattle_out, true );
          break;
        }
      }
      else {
        if (!m_farm->CattleOut( m_field, 0.0,
           m_field->GetMDates(1,0) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ocgg1_cattle_out, true );
        break;
        }
      }
      // Success
      // Keep them out there
      SimpleEvent( g_date->Date() + 1, ocgg1_cattle_is_out, false );
      break;
    }
    // don't graze so carry on with fertilizer etc.
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ),
                 ocgg1_ferti_one, false );
    break;

  case ocgg1_cattle_is_out:    // Keep the cattle out there
    // CattleIsOut() returns false if it is not time to stop grazing
    if (cfg_organic_extensive.value()) {
      if (!m_farm->CattleIsOutLow( m_field, 0.0,
         m_field->GetMDates(0,1) - g_date->DayInYear(),m_field->GetMDates(0,1))) {
      SimpleEvent( g_date->Date() + 1, ocgg1_cattle_is_out, false );
      break;
      }
    }
    else {
      if (!m_farm->CattleIsOut( m_field, 0.0,
         m_field->GetMDates(0,1) - g_date->DayInYear(),m_field->GetMDates(0,1))) {
      SimpleEvent( g_date->Date() + 1, ocgg1_cattle_is_out, false );
      break;
      }
    }
    // if it is time to stop grazing then that years activities are over
    done = true;   // End of activities
    break;

  case ocgg1_ferti_one:
    if (!m_farm->FA_Slurry( m_field, 0.0,
         ( OCGG1_CUT_DATE+4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ocgg1_ferti_one, true );
      break;
    }
    OCGG1_SLURRY=g_date->DayInYear();
    if ( (g_date->DayInYear(15,6))<OCGG1_CUT_DATE+21 )
	    SimpleEvent( g_date->OldDays() + (OCGG1_CUT_DATE+21),
  	               ocgg1_cut_to_silage1, false );
    else
	    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,6 ),
  	               ocgg1_cut_to_silage1, false );
    break;

  case ocgg1_cut_to_silage1:
    if ( m_ev->m_lock || m_farm->DoIt((int) (cfg_silage_prop.value()* 60 )))
    {
      if (!m_farm->CutToSilage( m_field, 0.0,
           g_date->DayInYear( 7,7 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgg1_cut_to_silage, true );
        break;
      }
      // Success so queue up the next event
      OCGG1_CUT_DATE=g_date->DayInYear();
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 6 ),
                 ocgg1_ferti_two, false );
      break;
    }
    else
    {
      // Didn't want to cut so go direct to graze
      SimpleEvent( g_date->OldDays() +  OCGG1_SLURRY+14,
	                 ocgg1_cattle_out, true ); // Must do it
    }
    break;

  case ocgg1_ferti_two:
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           (OCGG1_CUT_DATE+4) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 2, ocgg1_ferti_two, true );
        break;
      }
      OCGG1_SLURRY=g_date->DayInYear();
    }
    {
    d1=OCGG1_SLURRY+21;
    long d2=g_date->DayInYear(1,7);
    if (d2>d1) d1=d2;
    SimpleEvent( g_date->OldDays() + d1,
  	               ocgg1_cut_to_silage2, false );
    }
    break;

  case ocgg1_cut_to_silage2:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (cfg_silage_prop.value()*60) ))
    {
      if (!m_farm->CutToSilage( m_field, 0.0,
           g_date->DayInYear( 30, 6 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgg1_cut_to_silage2, true );
        break;
      }
      // Success so queue up the next event
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5, 7 ),
                 ocgg1_ferti_three, false );
      break;
    }
    else
    {
      // Didn't want to cut so go direct to graze
      SimpleEvent( g_date->OldDays() + OCGG1_SLURRY+14,
	                 ocgg1_cattle_out, true ); // Must do it !
    }
    break;

  case ocgg1_ferti_three:
    if (!m_farm->FA_Slurry( m_field, 0.0,
			    m_field->GetMDates(0,0) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ocgg1_ferti_three, true );
      break;
    }
    done = true; // nothing else to do
    break;

  default:
    g_msg->Warn( WARN_BUG, "OCloverGrassGrazed1::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}
//---------------------------------------------------------------------------
