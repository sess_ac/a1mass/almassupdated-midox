/**
\file
\brief
<B>PLBeans.h This file contains the headers for the Beans class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLBeans.h
//


#ifndef PLBEANS_H
#define PLBEANS_H

#define PLBEANS_BASE 25000
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_BNS_FERTI_P1			a_field->m_user[1]
#define PL_BNS_FERTI_S1			a_field->m_user[2]
#define PL_BNS_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_BNS_HERBI			a_field->m_user[4]

/** Below is the list of things that a farmer can do if he is growing beans, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_bns_start = 1, // Compulsory, must always be 1 (one).
	pl_bns_sleep_all_day = PLBEANS_BASE,
	pl_bns_ferti_p1, // 21701
	pl_bns_ferti_s1,
	pl_bns_stubble_plough,
	pl_bns_autumn_harrow1,
	pl_bns_autumn_harrow2,
	pl_bns_stubble_harrow,
	pl_bns_ferti_p2,
	pl_bns_ferti_s2,
	pl_bns_ferti_p3,
	pl_bns_ferti_s3,	// 21710
	pl_bns_winter_plough,
	pl_bns_spring_harrow,
	pl_bns_ferti_p4,
	pl_bns_ferti_s4,
	pl_bns_heavy_cultivator,
	pl_bns_preseeding_cultivator,
	pl_bns_spring_sow,
	pl_bns_weeding1,
	pl_bns_weeding2,
	pl_bns_weeding3,
	pl_bns_herbicide1,
	pl_bns_herbicide2,
	pl_bns_fungicide1,
	pl_bns_fungicide2,
	pl_bns_insecticide,
	pl_bns_ferti_p5,		
	pl_bns_ferti_s5,	
	pl_bns_ferti_p6,
	pl_bns_ferti_s6,
	pl_bns_harvest,
	pl_bns_ferti_p7,
	pl_bns_ferti_s7,	
	pl_bns_foobar,
} PLBeansToDo;


/**
\brief
PLBeans class
\n
*/
/**
See PLBeans.h::PLBeansToDo for a complete list of all possible events triggered codes by the beans management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLBeans: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLBeans(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_bns_foobar - PLBEANS_BASE);
	   m_base_elements_no = PLBEANS_BASE - 2;
	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others, // this needs to be at the zero line
			fmc_Others,//pl_bns_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_bns_sleep_all_day = PLBEANS_BASE,
			fmc_Fertilizer,//pl_bns_ferti_p1, // 21701
			fmc_Fertilizer,//pl_bns_ferti_s1,
			fmc_Cultivation,//pl_bns_stubble_plough,
			fmc_Cultivation,//pl_bns_autumn_harrow1,
			fmc_Cultivation,//pl_bns_autumn_harrow2,
			fmc_Cultivation,//pl_bns_stubble_harrow,
			fmc_Fertilizer,//pl_bns_ferti_p2,
			fmc_Fertilizer,//pl_bns_ferti_s2,
			fmc_Fertilizer,//pl_bns_ferti_p3,
			fmc_Fertilizer,//pl_bns_ferti_s3,	// 21710
			fmc_Cultivation,//pl_bns_winter_plough,
			fmc_Cultivation,//pl_bns_spring_harrow,
			fmc_Fertilizer,//pl_bns_ferti_p4,
			fmc_Fertilizer,//pl_bns_ferti_s4,
			fmc_Cultivation,//pl_bns_heavy_cultivator,
			fmc_Cultivation,//pl_bns_preseeding_cultivator,
			fmc_Others,//pl_bns_spring_sow,
			fmc_Cultivation,//pl_bns_weeding1,
			fmc_Cultivation,//pl_bns_weeding2,
			fmc_Cultivation,//pl_bns_weeding3,
			fmc_Herbicide,//pl_bns_herbicide1,
			fmc_Herbicide,//pl_bns_herbicide2,
			fmc_Fungicide,//pl_bns_fungicide1,
			fmc_Fungicide,//pl_bns_fungicide2,
			fmc_Insecticide,//pl_bns_insecticide,
			fmc_Fertilizer,//pl_bns_ferti_p5,
			fmc_Fertilizer,//pl_bns_ferti_s5,
			fmc_Fertilizer,//pl_bns_ferti_p6,
			fmc_Fertilizer,//pl_bns_ferti_s6,
			fmc_Harvest,//pl_bns_harvest,
			fmc_Fertilizer,//pl_bns_ferti_p7,
			fmc_Fertilizer//pl_bns_ferti_s7,
	   };
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};

#endif // PLBEANS_H

