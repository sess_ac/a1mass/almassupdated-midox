//
// OSeedGrass1.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#ifndef OSEEDGRASS1_H
#define OSEEDGRASS1_H

#define OSEEDGRASS1_BASE 3900
#define SG1_WATER_DATE     m_field->m_user[0]

typedef enum {
  osg1_start = 1, // Compulsory, start event must always be 1 (one).
  osg1_ferti_zero = OSEEDGRASS1_BASE,
  osg1_water_zero,
  osg1_water_zero_b,
  osg1_swarth,
  osg1_harvest,
  osg1_strawchopping,
  osg1_compress,
  osg1_burn_straw,
  osg1_foobar,
} OSeedGrass1ToDo;



class OSeedGrass1: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OSeedGrass1(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(14,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (osg1_foobar - OSEEDGRASS1_BASE);
	  m_base_elements_no = OSEEDGRASS1_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  osg1_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  osg1_ferti_zero = OSEEDGRASS1_BASE,
			fmc_Watering,	//	  osg1_water_zero,
			fmc_Watering,	//	  osg1_water_zero_b,
			fmc_Cutting,	//	  osg1_swarth,
			fmc_Harvest,	//	  osg1_harvest,
			fmc_Others,	//	  osg1_strawchopping,
			fmc_Others,	//	  osg1_compress,
			fmc_Others	//	  osg1_burn_straw,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OSEEDGRASS1_H
