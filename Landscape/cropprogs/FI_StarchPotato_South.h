//
// FI_StarchPotato_South.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FI_StarchPotato_South_h
#define FI_StarchPotato_South_h

#define FI_SPS_BASE 70400

#define FI_SPS_DECIDE_TO_HERB		m_field->m_user[5]
#define FI_SPS_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  fi_sps_start = 1, // Compulsory, start event must always be 1 (one).
  fi_sps_harvest = FI_SPS_BASE,
  fi_sps_stubble_cultivator,
  fi_sps_autumn_plough,
  fi_sps_slurry,
  fi_sps_spring_plough,
  fi_sps_n_minerals1,
  fi_sps_n_minerals2,
  fi_sps_preseeding_cultivation,
  fi_sps_fertilizer,
  fi_sps_preseeding_plant,
  fi_sps_plant,
  fi_sps_harrow,
  fi_sps_herbicide1,
  fi_sps_herbicide2,
  fi_sps_herbicide3,
  fi_sps_fungicide1,
  fi_sps_fungicide2,
  fi_sps_fungicide3,
  fi_sps_fungicide4,
  fi_sps_fungicide5,
  fi_sps_fungicide6,
  fi_sps_fungicide7,
  fi_sps_insecticide,
  fi_sps_foobar,
} FI_StarchPotato_SouthToDo;



class FI_StarchPotato_South: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FI_StarchPotato_South(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(1,12); // 
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (fi_sps_foobar - FI_SPS_BASE);
	  m_base_elements_no = FI_SPS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fi_sps_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  fi_sps_harvest = FI_SPS_BASE,
			fmc_Cultivation,	//	  fi_sps_stubble_cultivator,
			fmc_Cultivation,	//	  fi_sps_autumn_plough,
			fmc_Fertilizer,	//	  fi_sps_slurry,
			fmc_Cultivation,	//	  fi_sps_spring_plough,
			fmc_Fertilizer,	//	  fi_sps_n_minerals1,
			fmc_Fertilizer,	//	  fi_sps_n_minerals2,
			fmc_Cultivation,	//	  fi_sps_preseeding_cultivation,
			fmc_Fertilizer,	//	  fi_sps_fertilizer,
			fmc_Cultivation,	//	  fi_sps_preseeding_plant,
			fmc_Others,	//	  fi_sps_plant,
			fmc_Cultivation,	//	  fi_sps_harrow,
			fmc_Herbicide,	//	  fi_sps_herbicide1,
			fmc_Herbicide,	//	  fi_sps_herbicide2,
			fmc_Herbicide,	//	  fi_sps_herbicide3,
			fmc_Fungicide,	//	  fi_sps_fungicide1,
			fmc_Fungicide,	//	  fi_sps_fungicide2,
			fmc_Fungicide,	//	  fi_sps_fungicide3,
			fmc_Fungicide,	//	  fi_sps_fungicide4,
			fmc_Fungicide,	//	  fi_sps_fungicide5,
			fmc_Fungicide,	//	  fi_sps_fungicide6,
			fmc_Fungicide,	//	  fi_sps_fungicide7,
			fmc_Insecticide	//	  fi_sps_insecticide,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // FI_StarchPotato_South_h
