//
// DK_MixedVeg.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_MixedVeg.h" // based on salat

extern CfgBool cfg_pest_veg_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_MixedVeg::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  bool flag = false;
  int d1;
  TTypesOfVegetation l_tov = tov_DKMixedVeg;

  switch ( m_ev->m_todo ) {
  case dk_mv_start:
    {
      a_field->ClearManagementActionSum();
      DK_MV_FORCESPRING = false;
      m_last_date = g_date->DayInYear(31, 10); // Should match the last flexdate below
          //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
              // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(31, 10); // last possible day 
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(31, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 365;
      if (StartUpCrop(isSpring, flexdates, int(dk_mv_molluscicide))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 2) + isSpring;
      if (m_ev->m_forcespring) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_mv_molluscicide, false);
          break;
          DK_MV_FORCESPRING = true;
      }
      else
      // OK, let's go.
      SimpleEvent(d1, dk_mv_molluscicide, false);
  }
    break;

  case dk_mv_molluscicide:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) { // if issues with snails
          if (!m_farm->Molluscicide(m_field, 0.0,
              g_date->DayInYear(15, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_molluscicide, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_mv_spring_plough, false);
      break;

  case dk_mv_spring_plough:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) { 
          if (!m_farm->SpringPlough(m_field, 0.0,
              g_date->DayInYear(20, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_spring_plough, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 2), dk_mv_strigling1, false);
      break;

  case dk_mv_strigling1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->Strigling(m_field, 0.0,
              g_date->DayInYear(27, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_strigling1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 5, dk_mv_strigling2, false);
      break;

  case dk_mv_strigling2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->Strigling(m_field, 0.0,
              g_date->DayInYear(4, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_strigling2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 5, dk_mv_strigling3, false);
      break;

  case dk_mv_strigling3:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->Strigling(m_field, 0.0,
              g_date->DayInYear(12, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_strigling3, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 5, dk_mv_strigling4, false);
      break;

  case dk_mv_strigling4:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->Strigling(m_field, 0.0,
              g_date->DayInYear(20, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_strigling4, true);
              break;
          }
      } // suggests 25% of the field (farmers) plant early salat, 25% weeks after, 25% plant summer, 25% plant autumn
      SimpleEvent(g_date->Date() + 1, dk_mv_herbicide1, false); // planting in week 12-16 
      break;

  case dk_mv_herbicide1:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
          if (!m_farm->HerbicideTreat(m_field, 0.0,
              g_date->DayInYear(24, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_herbicide1, true);
              break;
          }
          SimpleEvent(g_date->Date() + 1, dk_mv_plant1, false); // planting in week 12-16 
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 4), dk_mv_herbicide2, false); // planting in week 17-21 
      break;

  case dk_mv_herbicide2:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.25/0.75)) {
          if (!m_farm->HerbicideTreat(m_field, 0.0,
              g_date->DayInYear(24, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_herbicide2, true);
              break;
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_mv_plant2, false); // planting in week 17-21 
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 5), dk_mv_herbicide3, false); // planting in week 22-26
      break;

  case dk_mv_herbicide3:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.25 / 0.50)) {
          if (!m_farm->HerbicideTreat(m_field, 0.0,
              g_date->DayInYear(24, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_herbicide3, true);
              break;
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_mv_plant3, false); // planting in week 22-26
          break;
      }
      else if (m_ev->m_lock || m_farm->DoIt_prob(0.25 / 0.25)) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(30, 6), dk_mv_herbicide4, false); // planting in week 27-32 
          break;
      }

  case dk_mv_herbicide4:
      if (!m_farm->HerbicideTreat(m_field, 0.0,
          g_date->DayInYear(9, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_herbicide4, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_mv_plant4, false); // planting in week 27-32 
      break;

  case dk_mv_plant1:
      if (!m_farm->SpringSowWithFerti(m_field, 0.0,
          g_date->DayInYear(25, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_plant1, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_mv_cover_on, false);
      SimpleEvent(g_date->Date(), dk_mv_water1, false);
      SimpleEvent(g_date->Date() + 7, dk_mv_fungicide1, false);
      SimpleEvent(g_date->Date() + 14, dk_mv_ferti_s1, false);
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_mv_insecticide1, false);
      SimpleEvent(g_date->Date() + 77, dk_mv_harvest1, false); // main thread
      break;

  case dk_mv_plant2:
      if (!m_farm->SpringSowWithFerti(m_field, 0.0,
          g_date->DayInYear(25, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_plant2, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_mv_water1, false);
      SimpleEvent(g_date->Date() + 7, dk_mv_fungicide1, false);
      SimpleEvent(g_date->Date() + 14, dk_mv_ferti_s1, false);
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5), dk_mv_insecticide1, false);
      SimpleEvent(g_date->Date() + 77, dk_mv_harvest2, false); // main thread
      break;

  case dk_mv_plant3:
      if (!m_farm->SpringSowWithFerti(m_field, 0.0,
          g_date->DayInYear(25, 6) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_plant3, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_mv_water1, false);
      SimpleEvent(g_date->Date() + 7, dk_mv_fungicide1, false);
      SimpleEvent(g_date->Date() + 7, dk_mv_insecticide1, false);
      SimpleEvent(g_date->Date() + 14, dk_mv_ferti_s1, false);
      SimpleEvent(g_date->Date() + 42, dk_mv_harvest2, false); // main thread
      break;

  case dk_mv_plant4:
      if (!m_farm->SpringSowWithFerti(m_field, 0.0,
          g_date->DayInYear(10, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_plant4, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_mv_water1, false);
      SimpleEvent(g_date->Date() + 7, dk_mv_fungicide1, false);
      SimpleEvent(g_date->Date() + 7, dk_mv_insecticide1, false);
      SimpleEvent(g_date->Date() + 14, dk_mv_ferti_s1, false);
      SimpleEvent(g_date->Date() + 77, dk_mv_harvest3, false); // main thread
      break;

  case dk_mv_cover_on:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->FiberCovering(m_field, 0.0,
              g_date->DayInYear(22, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_cover_on, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 4), dk_mv_cover_off, false);
      break;

  case dk_mv_cover_off:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->FiberRemoval(m_field, 0.0,
              g_date->DayInYear(15, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_cover_off, true);
              break;
          }
      }
      break;

  case dk_mv_water1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(15, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_water1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 14, dk_mv_water2, false);
      break;

  case dk_mv_water2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(7, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_water2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 14, dk_mv_water3, false);
      break;

  case dk_mv_water3:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(30, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_water3, true);
              break;
          }
      }
      break; 

   case dk_mv_fungicide1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->FungicideTreat(m_field, 0.0,
              g_date->DayInYear(18, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_fungicide1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 7, dk_mv_fungicide2, false);
      break;

  case dk_mv_fungicide2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->FungicideTreat(m_field, 0.0,
              g_date->DayInYear(31, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_fungicide2, true);
              break;
          }
      }
      break; 
  
  case dk_mv_ferti_s1:
      if (a_farm->IsStockFarmer()){
          if (!m_farm->FA_NPKS(m_field, 0.0,
              g_date->DayInYear(25, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_ferti_s1, true);
              break;
          }
          SimpleEvent(g_date->Date() + 7, dk_mv_ferti_s2, false);
          break;
      }
      else
      SimpleEvent(g_date->Date(), dk_mv_ferti_p1, false);
      break;

  case dk_mv_ferti_p1:
      if (!m_farm->FP_NPKS(m_field, 0.0,
          g_date->DayInYear(25, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_ferti_p1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 7, dk_mv_ferti_p2, false);
      break;

  case dk_mv_ferti_s2:
      if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
      {
          if (!m_farm->FA_NPKS(m_field, 0.0,
              g_date->DayInYear(7, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_ferti_s2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() +7, dk_mv_row_cultivation1, false);
      break;

  case dk_mv_ferti_p2:
      if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
      {
          if (!m_farm->FP_NPKS(m_field, 0.0,
              g_date->DayInYear(7, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_ferti_p2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 7, dk_mv_row_cultivation1, false);
      break;

  case dk_mv_row_cultivation1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(15, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_row_cultivation1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 14, dk_mv_row_cultivation2, false);
      break;

  case dk_mv_row_cultivation2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(30, 9) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_mv_row_cultivation2, true);
              break;
          }
      }
      break;

  case dk_mv_insecticide1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          // here we check whether we are using ERA pesticide or not
          d1 = g_date->DayInYear(18, 8) - g_date->DayInYear();
          if (!cfg_pest_veg_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
          {
              flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
          }
          else {
              flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
          }
          if (!flag) {
              SimpleEvent(g_date->Date() + 1, dk_mv_insecticide1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 7, dk_mv_insecticide2, false);
      break;

  case dk_mv_insecticide2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          // here we check whether we are using ERA pesticide or not
          d1 = g_date->DayInYear(31, 8) - g_date->DayInYear();
          if (!cfg_pest_veg_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
          {
              flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
          }
          else {
              flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
          }
          if (!flag) {
              SimpleEvent(g_date->Date() + 1, dk_mv_insecticide2, true);
              break;
          }
      }
      break;

  case dk_mv_harvest1: // 25% of all
      if (!m_farm->Harvest(m_field, 0.0,
          g_date->DayInYear(15, 7) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_harvest1, true);
          break;
      }
      d1 = g_date->Date();
      if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_mv_wait, false);
          // Because we are ending harvest before 1.7 so we need to wait until the 1.7
          break;
      }
      else {
          done = true; // end of plan
          break;
      }

  case dk_mv_harvest2: // 50% of all
      if (!m_farm->Harvest(m_field, 0.0,
          g_date->DayInYear(15, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_harvest2, true);
          break;
      }
      d1 = g_date->Date();
      if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_mv_wait, false);
          // Because we are ending harvest before 1.7 so we need to wait until the 1.7
          break;
      }
      else {
          done = true; // end of plan
          break;
      }

  case dk_mv_harvest3: // 25% of all
      if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_mv_harvest3, true);
          break;
      }
      done = true;
      break;

  case dk_mv_wait:
      done = true;
      break;

  default:
    g_msg->Warn( WARN_BUG, "DK_MixedVeg::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
  return done;
}


