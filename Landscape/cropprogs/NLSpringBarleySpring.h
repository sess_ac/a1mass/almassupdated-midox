/**
\file
\brief
<B>NLSpringBarleySpring.h This file contains the headers for the SpringBarleySpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLSpringBarleySpring.h
//


#ifndef NLSPRINGBARLEYSPRING_H
#define NLSPRINGBARLEYSPRING_H

#define NLSPRINGBARLEYSPRING_BASE 22300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_SBS_FUNGII	a_field->m_user[1]


/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_sbs_start = 1, // Compulsory, must always be 1 (one).
	nl_sbs_sleep_all_day = NLSPRINGBARLEYSPRING_BASE,
	nl_sbs_spring_plough_sandy,
	nl_sbs_ferti_p2_sandy,
	nl_sbs_ferti_s2_sandy,
	nl_sbs_ferti_p2_clay,
	nl_sbs_ferti_s2_clay,
	nl_sbs_ferti_p3,
	nl_sbs_ferti_s3,
	nl_sbs_preseeding_cultivator,
	nl_sbs_preseeding_cultivator_sow,
	nl_sbs_spring_sow,
	nl_sbs_harrow,
	nl_sbs_ferti_p4_clay,
	nl_sbs_ferti_s4_clay,
	nl_sbs_herbicide1,
	nl_sbs_fungicide1,
	nl_sbs_fungicide2,
	nl_sbs_insecticide1,
	nl_sbs_growth_regulator1,
	nl_sbs_harvest,
	nl_sbs_straw_chopping,
	nl_sbs_hay_bailing,
	nl_sbs_foobar
} NLSpringBarleySpringToDo;


/**
\brief
NLSpringBarleySpring class
\n
*/
/**
See NLSpringBarleySpring.h::NLSpringBarleySpringToDo for a complete list of all possible events triggered codes by the spring barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLSpringBarleySpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLSpringBarleySpring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 25,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_sbs_foobar - NLSPRINGBARLEYSPRING_BASE);
	   m_base_elements_no = NLSPRINGBARLEYSPRING_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	nl_sbs_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	nl_sbs_sleep_all_day = NLSPRINGBARLEYSPRING_BASE,
			fmc_Cultivation,	//	nl_sbs_spring_plough_sandy,
			fmc_Fertilizer,	//	nl_sbs_ferti_p2_sandy,
			fmc_Fertilizer,	//	nl_sbs_ferti_s2_sandy,
			fmc_Fertilizer,	//	nl_sbs_ferti_p2_clay,
			fmc_Fertilizer,	//	nl_sbs_ferti_s2_clay,
			fmc_Fertilizer,	//	nl_sbs_ferti_p3,
			fmc_Fertilizer,	//	nl_sbs_ferti_s3,
			fmc_Cultivation,	//	nl_sbs_preseeding_cultivator,
			fmc_Cultivation,	//	nl_sbs_preseeding_cultivator_sow,
			fmc_Others,	//	nl_sbs_spring_sow,
			fmc_Cultivation,	//	nl_sbs_harrow,
			fmc_Fertilizer,	//	nl_sbs_ferti_p4_clay,
			fmc_Fertilizer,	//	nl_sbs_ferti_s4_clay,
			fmc_Herbicide,	//	nl_sbs_herbicide1,
			fmc_Fungicide,	//	nl_sbs_fungicide1,
			fmc_Fungicide,	//	nl_sbs_fungicide2,
			fmc_Insecticide,	//	nl_sbs_insecticide1,
			fmc_Others,	//	nl_sbs_growth_regulator1,
			fmc_Harvest,	//	nl_sbs_harvest,
			fmc_Others,	//	nl_sbs_straw_chopping,
			fmc_Others	//	nl_sbs_hay_bailing,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLSPRINGBARLEYSPRING_H

