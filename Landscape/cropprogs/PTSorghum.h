/**
\file
\brief
<B>PTSorghum.h This file contains the headers for the Sorghum class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTSorghum.h
//


#ifndef PTSORGHUM_H
#define PTSORGHUM_H

#define PTSORGHUM_BASE 30300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PT_S_START_FERTI	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing sorghum, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_s_start = 1, // Compulsory, must always be 1 (one).
	pt_s_sleep_all_day = PTSORGHUM_BASE,
	pt_s_stubble_harrow,
	pt_s_ferti_p1, // NPK
	pt_s_ferti_s1,
	pt_s_spring_plough,
	pt_s_spring_harrow,
	pt_s_spring_sow,
	pt_s_irrigation_start1,
	pt_s_irrigation1,
	pt_s_harvest1,
	pt_s_hay_bailing1,
	pt_s_ferti_p2, //NPK
	pt_s_ferti_s2,
	pt_s_irrigation_start2,
	pt_s_irrigation2,
	pt_s_harvest2,
	pt_s_hay_bailing2,
	//pt_s_wait,
} PTSorghumToDo;


/**
\brief
PTSorghum class
\n
*/
/**
See PTSorghum.h::PTSorghumToDo for a complete list of all possible events triggered codes by the mazie management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTSorghum: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTSorghum(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,4 );
   }
};

#endif // PTSORGHUM_H

