//
// FI_Sugarbeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FI_SugarBeet_h
#define FI_SugarBeet_h

#define FI_SB_BASE 70200

#define FI_SB_DECIDE_TO_HERB		m_field->m_user[5]
#define FI_SB_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  fi_sbe_start = 1, // Compulsory, start event must always be 1 (one).
  fi_sbe_harvest = FI_SB_BASE,
  fi_sbe_autumn_plough1,
  fi_sbe_slurry,
  fi_sbe_spring_plough,
  fi_sbe_preseeding_cultivation,
  fi_sbe_preseeding_sow,
  fi_sbe_sow,
  fi_sbe_fertilizer1,
  fi_sbe_harrow,
  fi_sbe_herbicide1,
  fi_sbe_insecticide,
  fi_sbe_herbicide2,
  fi_sbe_herbicide3,
  fi_sbe_fertilizer2,
  fi_sbe_fungicide,
  fi_sbe_autumn_plough2,
  fi_sbe_foobar,
} FI_SugarBeetToDo;



class FI_SugarBeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FI_SugarBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(15,11); //
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (fi_sbe_foobar - FI_SB_BASE);
	  m_base_elements_no = FI_SB_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fi_sbe_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  fi_sbe_harvest = FI_SB_BASE,
			fmc_Cultivation,	//	  fi_sbe_autumn_plough1,
			fmc_Fertilizer,	//	  fi_sbe_slurry,
			fmc_Cultivation,	//	  fi_sbe_spring_plough,
			fmc_Cultivation,	//	  fi_sbe_preseeding_cultivation,
			fmc_Cultivation,	//	  fi_sbe_preseeding_sow,
			fmc_Others,	//	  fi_sbe_sow,
			fmc_Fertilizer,	//	  fi_sbe_fertilizer1,
			fmc_Cultivation,	//	  fi_sbe_harrow,
			fmc_Herbicide,	//	  fi_sbe_herbicide1,
			fmc_Insecticide,	//	  fi_sbe_insecticide,
			fmc_Herbicide,	//	  fi_sbe_herbicide2,
			fmc_Herbicide,	//	  fi_sbe_herbicide3,
			fmc_Fertilizer,	//	  fi_sbe_fertilizer2,
			fmc_Fungicide,	//	  fi_sbe_fungicide,
			fmc_Cultivation	//	  fi_sbe_autumn_plough2,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // FI_SugarBeet_h
