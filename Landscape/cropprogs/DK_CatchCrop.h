//
// DK_CatchCrop.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_CatchCrop_H
#define DK_CatchCrop_H

#define DK_CC_BASE 68800
#define DK_CC_ENDDATE a_field->m_user[0]

typedef enum {
	dk_cc_start = 1, // Compulsory, start event must always be 1 (one).
	dk_cc_sleep_all_day = DK_CC_BASE,
	dk_cc_stubble_harrow,
	dk_cc_sow,
	dk_cc_winter_plough,
	dk_cc_spring_plough,
	dk_cc_herbicide1,
	dk_cc_herbicide2,
	dk_cc_foobar
} DK_CatchCropToDo;



class DK_CatchCrop : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_CatchCrop(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(15, 10);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_cc_foobar - DK_CC_BASE);
		m_base_elements_no = DK_CC_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_cc_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others, // dk_cc_sleep_all_day = DK_CC_BASE,
			fmc_Cultivation, // dk_cc_stubble_harrow
			fmc_Others, // dk_cc_sow
			fmc_Cultivation, // dk_cc_winter_plough
			fmc_Cultivation, // dk_cc_spring_plough
			fmc_Herbicide, // dk_cc_herbicide1
			fmc_Herbicide // dk_cc_herbicide2
						// No foobar entry
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_CatchCrop_H