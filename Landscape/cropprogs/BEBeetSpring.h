/**
\file
\brief
<B>BEBeetSpring.h This file contains the headers for the BeetSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of July 2018 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BEBeetSpring.h
//


#ifndef BEBEETSPRING_H
#define BEBEETSPRING_H

#define BEBEETSPRING_BASE 26200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define BE_BES_HERBI1	a_field->m_user[1]
#define BE_BES_FUNGI1	a_field->m_user[2]


/** Below is the list of things that a farmer can do if he is growing beet, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_bes_start = 1, // Compulsory, must always be 1 (one).
	BE_bes_sleep_all_day = BEBEETSPRING_BASE,
	BE_bes_spring_plough,
	BE_bes_ferti_p1,
	BE_bes_ferti_s1,
	BE_bes_preseeding_cultivator,
	BE_bes_spring_sow,
	BE_bes_ferti_p2,
	BE_bes_ferti_s2,
	BE_bes_herbicide1,
	BE_bes_herbicide2,
	BE_bes_herbicide3,
	BE_bes_herbicide4,
	BE_bes_fungicide1,
	BE_bes_fungicide2,
	BE_bes_fungicide3,
	BE_bes_harvest,
} BEBeetSpringToDo;


/**
\brief
BEBeetSpring class
\n
*/
/**
See BEBeetSpring.h::BEBeetSpringToDo for a complete list of all possible events triggered codes by the beet management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEBeetSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEBeetSpring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,3 );
   }
};

#endif // BEBEETSPRING_H

