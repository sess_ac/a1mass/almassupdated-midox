/**
\file
\brief
<B>NLCarrotsSpring.h This file contains the headers for the CarrotsSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLCarrotsSpring.h
//


#ifndef NLCARROTSSPRING_H
#define NLCARROTSSPRING_H

#define NLCARROTSSPRING_BASE 20700
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_CAS_WINTER_PLOUGH	a_field->m_user[1]
#define NL_CAS_HERBI1	a_field->m_user[2]
#define NL_CAS_HERBI2	a_field->m_user[3]
#define NL_CAS_FUNGI1	a_field->m_user[4]
#define NL_CAS_FUNGI2	a_field->m_user[5]

/** Below is the list of things that a farmer can do if he is growing carrots, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_cas_start = 1, // Compulsory, must always be 1 (one).
	nl_cas_sleep_all_day = NLCARROTSSPRING_BASE,
	nl_cas_spring_plough_sandy,
	nl_cas_ferti_p1,
	nl_cas_ferti_s1,
	nl_cas_preseeding_cultivator,
	nl_cas_bed_forming,
	nl_cas_spring_sow,
	nl_cas_ferti_p2,
	nl_cas_ferti_s2,
	nl_cas_herbicide1,
	nl_cas_herbicide2,
	nl_cas_herbicide3,
	nl_cas_fungicide1,
	nl_cas_fungicide2,
	nl_cas_fungicide3,
	nl_cas_harvest,
	nl_cas_foobar
} NLCarrotsSpringToDo;


/**
\brief
NLCarrotsSpring class
\n
*/
/**
See NLCarrotsSpring.h::NLCarrotsSpringToDo for a complete list of all possible events triggered codes by the carrots management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLCarrotsSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLCarrotsSpring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_cas_foobar - NLCARROTSSPRING_BASE);
	   m_base_elements_no = NLCARROTSSPRING_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//nl_cas_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_cas_sleep_all_day = NLCARROTSSPRING_BASE,
			fmc_Cultivation,//nl_cas_spring_plough_sandy,
			fmc_Fertilizer,//nl_cas_ferti_p1,
			fmc_Fertilizer,//nl_cas_ferti_s1,
			fmc_Cultivation,//nl_cas_preseeding_cultivator,
			fmc_Others,//nl_cas_bed_forming,
			fmc_Others,//nl_cas_spring_sow,
			fmc_Fertilizer,//nl_cas_ferti_p2,
			fmc_Fertilizer,//nl_cas_ferti_s2,
			fmc_Herbicide,//nl_cas_herbicide1,
			fmc_Herbicide,//nl_cas_herbicide2,
			fmc_Herbicide,//nl_cas_herbicide3,
			fmc_Fungicide,//nl_cas_fungicide1,
			fmc_Fungicide,//nl_cas_fungicide2,
			fmc_Fungicide,//nl_cas_fungicide3,
			fmc_Harvest//nl_cas_harvest,
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLCARROTSSPRING_H

