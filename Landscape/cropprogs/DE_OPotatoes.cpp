//
// DE_OPotatoes.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OPotatoes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_potatoes_on;
extern CfgFloat cfg_pest_product_1_amount;
extern Landscape* g_landscape_p;
extern CfgFloat cfg_strigling_prop;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_OPotatoes::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEOPotatoes; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_opot_start:
	{
		DE_OPOT_MANURE_DATE = 1;
		DE_OPOT_SOW_DATE = 1;
		DE_OPOT_HILLING_THREE = 1;

		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(1, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(1, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(1, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_opot_spring_plough))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		// Here we queue up the first event
		// stock or arable farmer
		if (m_farm->IsStockFarmer()) {
			SimpleEvent_(d1, de_opot_fa_manure, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_opot_fp_manure, false, m_farm, m_field);
		break;
	}
	break;

	case de_opot_fa_manure:
		if (!m_farm->FA_Manure( m_field, 0.0, g_date->DayInYear( 20, 3 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_opot_fa_manure, false, m_farm, m_field);
			break;
		}
		DE_OPOT_MANURE_DATE = g_date->DayInYear();
		SimpleEvent_( g_date->Date() + 1, de_opot_fa_slurry, false, m_farm, m_field);
		break;

	case de_opot_fa_slurry:
		if ( m_ev->m_lock || m_farm->DoIt( 40 )) {
			if (!m_farm->FA_Slurry( m_field, 0.0, DE_OPOT_MANURE_DATE - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_opot_fa_slurry, true, m_farm, m_field);
				break;
			}
		}	
		SimpleEvent_( g_date->Date(), de_opot_spring_plough, false, m_farm, m_field);
		break;

	 case de_opot_fp_manure:
		if (!m_farm->FP_Manure( m_field, 0.0, g_date->DayInYear( 20, 3 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_opot_fp_manure, false, m_farm, m_field);
			break;
		}
		DE_OPOT_MANURE_DATE = g_date->DayInYear();
		SimpleEvent_( g_date->Date() + 1, de_opot_fp_slurry, false, m_farm, m_field);
		break;

  case de_opot_fp_slurry:
      if (m_ev->m_lock || m_farm->DoIt(15)) {
          if (!m_farm->FP_Slurry(m_field, 0.0, DE_OPOT_MANURE_DATE -
              g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_fp_slurry, true, m_farm, m_field);
              break;
          }
      }
      SimpleEvent_(g_date->Date() + 1, de_opot_spring_plough, false, m_farm, m_field);
      break;

  case de_opot_spring_plough:
      if (!m_farm->SpringPlough(m_field, 0.0,
          DE_OPOT_MANURE_DATE -
          g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_opot_spring_plough, true, m_farm, m_field);
          break;
      }
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 3), de_opot_spring_harrow, false, m_farm, m_field);
      break;

  case de_opot_spring_harrow:
      if (!m_farm->SpringHarrow(m_field, 0.0,
          g_date->DayInYear(15, 4) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_opot_spring_harrow, true, m_farm, m_field);
          break;
      }
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), de_opot_spring_sow, false, m_farm, m_field);
      break;

  case de_opot_spring_sow:
      if (!m_farm->SpringSow(m_field, 0.0,
          g_date->DayInYear(15, 4) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_opot_spring_sow, true, m_farm, m_field);
          break;
      }
      SimpleEvent_(g_date->Date() + 7, de_opot_flaming_one, false, m_farm, m_field);
      break;

  case de_opot_flaming_one:
      if (m_ev->m_lock || m_farm->DoIt_prob(.10)) {
          if (!m_farm->Swathing(m_field, 0.0,
              g_date->DayInYear(10, 5) -
              g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_flaming_one, true, m_farm, m_field);
              break;
          }
          SimpleEvent_(g_date->Date() + 7, de_opot_strigling_one, false, m_farm, m_field);
          break;
      }
      else SimpleEvent_(g_date->Date() + 14, de_opot_strigling_one, false, m_farm, m_field);
      break;



  case de_opot_strigling_one:
      if (!m_farm->StriglingHill(m_field, 0.0,
          g_date->DayInYear(20, 5) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_opot_strigling_one, true, m_farm, m_field);
          break;
      }

      // here comes a fork of events:
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(19, 4), de_opot_water_one, false, m_farm, m_field); // main thread
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), de_opot_insecticide, false, m_farm, m_field); // Insecticide treat
      if (a_farm->IsStockFarmer()) //Stock Farmer
      {
          SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), de_opot_fertiFA_S, false, m_farm, m_field);	// Sulphur followed by Boron
      }
      else {
          SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), de_opot_fertiFP_S, false, m_farm, m_field);	// Sulphur followed by Boron
      }
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_opot_fungicide, false, m_farm, m_field); // Fungicide treat  
      break;

  case de_opot_water_one:
      if (m_ev->m_lock || m_farm->DoIt(80)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(30, 5) -
              g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_water_one, true, m_farm, m_field);
              break;
          }
      }
      SimpleEvent_(g_date->Date() + 10, de_opot_strigling_two, false, m_farm, m_field);
      break;

  case de_opot_strigling_two:
    if (!m_farm->StriglingHill( m_field, 0.0,
         g_date->DayInYear( 30, 5 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_strigling_two, true, m_farm, m_field);
      break;
    }
    SimpleEvent_(g_date->Date() + 7, de_opot_strigling_three, false, m_farm, m_field);
    break;

  case de_opot_strigling_three:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 75 ))) {
      if (!m_farm->StriglingHill( m_field, 0.0,
			      g_date->DayInYear( 12, 6 ) -
			      g_date->DayInYear())) {
	SimpleEvent_( g_date->Date() + 1, de_opot_strigling_three, true, m_farm, m_field);
	break;
      }
    }
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 7),
        de_opot_water_two, false, m_farm, m_field);
    break;

  case de_opot_insecticide:
      // here comes the insecticide thread
	  // here we check wheter we are using ERA pesticide or not
	  d1 = g_date->DayInYear(10, 6) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!cfg_pest_potatoes_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_opot_insecticide, true, m_farm, m_field);
				break;
			}
		}
		break;
		// End of thread


	case de_opot_fertiFP_S:
      // Here comes the microelements thread for arable farms
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_opot_fertiFP_S, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), de_opot_fertiFP_B, false, m_farm, m_field);
		break;

	case de_opot_fertiFP_B:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_opot_fertiFP_B, true, m_farm, m_field);
				break;
			}
		}
		break;
      // End of thread
	case de_opot_fertiFA_S:
		// Here comes the microelements thread for stock farms
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_opot_fertiFA_S, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), de_opot_fertiFA_B, false, m_farm, m_field);
		break;

	case de_opot_fertiFA_B:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_opot_fertiFA_B, true, m_farm, m_field);
				break;
			}
		}
		break;
		// End of thread

	case de_opot_fungicide:
		// Here comes the fungicide thread
		if (m_ev->m_lock || m_farm->DoIt(80)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_opot_fungicide, true, m_farm, m_field);
				break;
			}
		}
		break;
		// End of thread

	case de_opot_water_two:
		// MAIN THREAD
		if ( m_ev->m_lock || m_farm->DoIt( 70 )) {
			if (!m_farm->Water( m_field, 0.0, g_date->DayInYear( 30, 7 ) - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_opot_water_two, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_( g_date->OldDays() + g_date->DayInYear( 30, 7 ), de_opot_flaming_two, false, m_farm, m_field);
		break;

	case de_opot_flaming_two:
		if ( m_ev->m_lock || m_farm->DoIt( 10 ))
		{
			if (!m_farm->Swathing( m_field, 0.0, g_date->DayInYear( 15, 8 ) - g_date->DayInYear()))
			{
				SimpleEvent_( g_date->Date() + 1, de_opot_flaming_two, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 8), de_opot_harvest, false, m_farm, m_field);
		break;

  case de_opot_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         g_date->DayInYear( 1, 10 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_harvest, true, m_farm, m_field);
      break;
    }
    // So we are done, and somewhere else the farmer will queue up the start event of the next crop
    // END of MAIN THREAD
    done = true;
    break;

  default:
      g_msg->Warn(WARN_BUG, "DE_OPotatoes::Do(): "
          "Unknown event type! ", "");
      exit(1);
  }
  return done;
}