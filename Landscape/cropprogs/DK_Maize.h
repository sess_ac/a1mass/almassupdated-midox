//
// DK_Maize.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disspsaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disspsaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSPSUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSPSAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSPSUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSPSUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKMaize_H
#define DKMaize_H

#define DK_M_BASE 63800

#define DK_M_RC_CC m_field->m_user[0]
#define DK_M_FORCESPRING	a_field->m_user[1]

typedef enum {
	dk_m_start = 1, // Compulsory, start event must always be 1 (one).
	dk_m_harvest = DK_M_BASE,
	dk_m_autumn_plough,
	dk_m_spring_plough,
	dk_m_spring_harrow1,
	dk_m_spring_harrow2,
	dk_m_cultivator_sow,
	dk_m_sow,
	dk_m_sow_cc,
	dk_m_ferti_s1,
	dk_m_ferti_p1,
	dk_m_row_cultivation_weeds,
	dk_m_row_cultivation_cc,
	dk_m_water,
	dk_m_herbicide2,
	dk_m_herbicide3,
	dk_m_fungicide,
	dk_m_straw_chopping,
	dk_m_foobar,
} DK_MaizeToDo;



class DK_Maize : public Crop
{
public:
  bool  Do(Farm * a_farm, LE * a_field, FarmEvent * a_ev);
  DK_Maize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
	  m_first_date = g_date->DayInYear(30,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_m_foobar - DK_M_BASE);
	  m_base_elements_no = DK_M_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_m_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_m_harvest = DK_M_BASE,
			fmc_Cultivation,	//	  dk_m_autumn_plough,
			fmc_Cultivation,	//	  dk_m_spring_plough,
			fmc_Cultivation,	//	  dk_m_spring_harrow1,
			fmc_Cultivation,	//	  dk_m_spring_harrow2,
			fmc_Cultivation,	//	  dk_m_cultivator_sow,
			fmc_Others,	//	  dk_m_sow,
			fmc_Others,	//	  dk_m_sow_cc,
			fmc_Fertilizer,	//	  dk_m_ferti_s1,
			fmc_Fertilizer,	//	  dk_m_ferti_p1,
			fmc_Cultivation,	//	  dk_m_row_cultivation_weeds,
			fmc_Cultivation,	//	  dk_m_row_cultivation_cc,
			fmc_Watering, // dk_m_water,
			fmc_Herbicide, // dk_m_herbicide2,
			fmc_Herbicide, // dk_m_herbicide3,
			fmc_Fungicide, // dk_m_fungicide,
			fmc_Cutting, // dk_m_straw_chopping,

			   // no foobar entry			

	  };
	  // Iterate over the catlist elements and copy them to vector						
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};


#endif // DK_Maize_H
