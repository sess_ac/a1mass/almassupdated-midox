/**
\file
\brief
<B>FR_Grassland.h This file contains the headers for the FR_Grassland class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of September 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n


Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FR_GRASSLAND_H
#define FR_GRASSLAND_H

#define FR_G_SOW_YEAR a_field->m_user[1]
#define FR_G_CUT a_field->m_user[2]
#define FR_G_CUT_ONLY a_field->m_user[3]
#define FR_G_GRAZE_ONLY a_field->m_user[4]
#define FR_G_CUT_GRAZE a_field->m_user[5]


#define FR_G_BASE 190700
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	fr_g_start = 1, // Compulsory, must always be 1 (one).
	fr_g_sleep_all_day = FR_G_BASE,
	fr_g_slurry1,
	fr_g_winter_plough,
	fr_g_n_mineral1,
	fr_g_preseed_sow,
	fr_g_herbicide,
	fr_g_cutting1,
	fr_g_grazing,
	fr_g_cattle_is_out,
	fr_g_slurry2,
	fr_g_n_mineral2,
	fr_g_cutting2,
	fr_g_slurry3,
	fr_g_npk1,
	fr_g_cutting3,
	fr_g_slurry4,
	fr_g_npk2,
	fr_g_cutting4,
	fr_g_slurry5,
	fr_g_npk3,
	fr_g_cutting5,
	fr_g_sleep_cg,
	fr_g_sleep_c,
	fr_g_sleep_g,
	fr_g_cutting_ny,
	fr_g_slurry6,
	fr_g_water,
	fr_g_foobar,
} FR_Grassland_ToDo;


/**
\brief
FR_Grassland class
\n
*/
/**
See FR_Grassland.h::FR_GrasslandToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class FR_Grassland : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	FR_Grassland(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(30, 11);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (fr_g_foobar - FR_G_BASE);
		m_base_elements_no = FR_G_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
	 fmc_Others,	// zero element unused but must be here	
	 fmc_Others, //fr_g_start = 1, // Compulsory, must always be 1 (one).
	 fmc_Others, //fr_g_sleep_all_day = FR_G_BASE,
	 fmc_Fertilizer, //fr_g_slurry1,
	 fmc_Cultivation, // fr_g_winter_plough,
	 fmc_Fertilizer, //fr_g_n_mineral1,
	 fmc_Others, //fr_g_preseed_sow,
	 fmc_Herbicide, //fr_g_herbicide,
	 fmc_Cutting, //fr_g_cutting1,
	 fmc_Grazing, //fr_g_grazing,
	 fmc_Grazing, //fr_g_cattle_is_out
	 fmc_Fertilizer, //fr_g_slurry2,
	 fmc_Fertilizer, //fr_g_n_mineral2,
	 fmc_Cutting, //fr_g_cutting2,
	 fmc_Fertilizer, //fr_g_slurry3,
	 fmc_Fertilizer, //fr_g_npk1
	 fmc_Cutting, //fr_g_cutting3,
	 fmc_Fertilizer, //fr_g_slurry4,
	 fmc_Fertilizer, //fr_g_npk2,
	 fmc_Cutting, //	fr_g_cutting4,
	 fmc_Fertilizer, //fr_g_slurry5,
	 fmc_Fertilizer, //fr_g_npk3,
	 fmc_Cutting, //fr_g_cutting5,
	 fmc_Others, //fr_g_sleep_cg,
	 fmc_Others, //fr_g_sleep_c,
	 fmc_Others, //fr_g_sleep_g,
	 fmc_Cutting, //fr_g_cutting_ny,
	 fmc_Fertilizer, //fr_g_slurry6,
	 fmc_Watering, //fr_g_water,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};
#endif // FR_GRASSLAND_H

