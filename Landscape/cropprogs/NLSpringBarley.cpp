/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLSpringBarley.cpp This file contains the source for the NLSpringBarley class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// NLSpringBarley.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLSpringBarley.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_springbarley_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_SB_InsecticideDay;
extern CfgInt   cfg_SB_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley.
*/
bool NLSpringBarley::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLSpringBarley;
	int l_nextcropstartdate;
	/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case nl_sb_start:
	{
		NL_SB_FUNGII = false;
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(25, 8); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 8); // first possible day of finishing harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(20, 8);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping
		flexdates[2][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(25, 8);	 // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - hay bailing

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(nl_sb_spring_sow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9) + isSpring;
		// OK, let's go.
		if (m_ev->m_forcespring) {
			if (m_field->GetSoilType() == 2 || m_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
				if (m_farm->IsStockFarmer()) //Stock Farmer
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_sb_ferti_s1_sandy, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_sb_ferti_p1_sandy, false, m_farm, m_field);
			}
			else {
				if (m_farm->IsStockFarmer()) //Stock Farmer
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3) + 365, nl_sb_ferti_s2_clay, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3) + 365, nl_sb_ferti_p2_clay, false, m_farm, m_field);
			}
			break;
		}
		else {
			if (m_field->GetSoilType() == 2 || m_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
				SimpleEvent_(d1, nl_sb_stubble_harrow_sandy, false, m_farm, m_field);
			}
			else SimpleEvent_(d1, nl_sb_stubble_harrow_clay, false, m_farm, m_field);
			break;
		}
		break;
	}
	break;

	// This is the first real farm operation
	case nl_sb_stubble_harrow_sandy:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(31,10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_sb_stubble_harrow_sandy, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_sb_ferti_s1_sandy, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_sb_ferti_p1_sandy, false, m_farm, m_field);
		break;
	case nl_sb_ferti_p1_sandy:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_p1_sandy, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_sb_spring_plough_sandy, false, m_farm, m_field);
		break;
	case nl_sb_ferti_s1_sandy:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_s1_sandy, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_sb_spring_plough_sandy, false, m_farm, m_field);
		break;
	case nl_sb_spring_plough_sandy:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
		{
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_spring_plough_sandy, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 7, nl_sb_ferti_s2_sandy, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 7, nl_sb_ferti_p2_sandy, false, m_farm, m_field);
		break;
	case nl_sb_ferti_p2_sandy:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_p2_sandy, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_sb_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, nl_sb_ferti_p3, false, m_farm, m_field);
		break;
	case nl_sb_ferti_s2_sandy:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_s2_sandy, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_sb_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, nl_sb_ferti_p3, false, m_farm, m_field);
		break;
	case nl_sb_stubble_harrow_clay:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
		{
			if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_stubble_harrow_clay, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		SimpleEvent_(d1, nl_sb_winter_plough_clay, false, m_farm, m_field);
		break;
	case nl_sb_winter_plough_clay:
		if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_sb_winter_plough_clay, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3) + 365, nl_sb_ferti_s2_clay, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3) + 365, nl_sb_ferti_p2_clay, false, m_farm, m_field);
		break;
	case nl_sb_ferti_p2_clay:
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_p2_clay, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_sb_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, nl_sb_ferti_p3, false, m_farm, m_field);
		break;
	case nl_sb_ferti_s2_clay:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_s2_clay, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_sb_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, nl_sb_ferti_p3, false, m_farm, m_field);
		break;
	case nl_sb_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(9, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 3, nl_sb_preseeding_cultivator, false, m_farm, m_field);
		break;
	case nl_sb_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(9, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 3, nl_sb_preseeding_cultivator, false, m_farm, m_field);
		break;
	case nl_sb_preseeding_cultivator:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(9, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_preseeding_cultivator, true, m_farm, m_field);
				break;
			}
			else
			{
				SimpleEvent_(g_date->Date() + 1, nl_sb_spring_sow, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_sb_preseeding_cultivator_sow, false, m_farm, m_field);
		break;
	case nl_sb_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_sb_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, nl_sb_harrow, false, m_farm, m_field); // Harrowing
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), nl_sb_herbicide1, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_sb_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), nl_sb_insecticide1, false, m_farm, m_field);	// Insecticide thread = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_sb_growth_regulator1, false, m_farm, m_field);	// GR thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_sb_ferti_s4_clay, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_sb_ferti_p4_clay, false, m_farm, m_field);
		break;
	case nl_sb_preseeding_cultivator_sow:
		if (!m_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_sb_preseeding_cultivator_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, nl_sb_harrow, false, m_farm, m_field); // Harrowing
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), nl_sb_herbicide1, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_sb_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), nl_sb_insecticide1, false, m_farm, m_field);	// Insecticide thread = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_sb_growth_regulator1, false, m_farm, m_field);	// GR thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_sb_ferti_s4_clay, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_sb_ferti_p4_clay, false, m_farm, m_field);
		break;
	case nl_sb_harrow:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
			{
				if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_sb_harrow, true, m_farm, m_field);
					break;
				}
			}
		}
		// end of thread
		break;
	case nl_sb_ferti_p4_clay:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.50) && (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6)))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_p4_clay, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_sb_ferti_s4_clay:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.50) && (m_field->GetSoilType() != 2 && m_field->GetSoilType() != 6)))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_ferti_s4_clay, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_sb_herbicide1: // The first of the pesticide managements.
						   // Here comes the herbicide thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 5, nl_sb_herbicide1, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_sb_herbicide1, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case nl_sb_fungicide1:
		// Here comes the fungicide thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_fungicide1, true, m_farm, m_field);
				break;
			}
			NL_SB_FUNGII = true;
		}
		SimpleEvent_(g_date->Date() + 14, nl_sb_fungicide2, false, m_farm, m_field);
		break;
	case nl_sb_fungicide2:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.50) && NL_SB_FUNGII == 1)) // 40% of all farmers
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_sb_insecticide1:
		// Here comes the insecticide thread = MAIN THREAD
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_springbarley_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_sb_insecticide1, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 7), nl_sb_harvest, false, m_farm, m_field);
		break;
	case nl_sb_growth_regulator1:
		// Here comes the GR thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_growth_regulator1, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_sb_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_sb_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_sb_straw_chopping, false, m_farm, m_field);
		break;
	case nl_sb_straw_chopping:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_sb_straw_chopping, true, m_farm, m_field);
				break;
			}
			done = true;
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_sb_hay_bailing, false, m_farm, m_field);
		break;
	case nl_sb_hay_bailing:
		if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_sb_hay_bailing, true, m_farm, m_field);
			break;
		}
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "PLSpringBarley::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}