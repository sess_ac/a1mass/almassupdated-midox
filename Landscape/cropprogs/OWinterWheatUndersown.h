//
// OWinterWheatUndersown.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWINTERWHEATUndersown_H
#define OWINTERWHEATUndersown_H

#define OWINTERWHEATUndersown_BASE 5000
#define OWWU_PLOUGH_RUNS       m_field->m_user[0]
#define OWWU_HARROW_RUNS       m_field->m_user[1]

typedef enum {
    owwu_start = 1, // Compulsory, must always be 1 (one).
    owwu_sleep_all_day = OWINTERWHEATUndersown_BASE,
    owwu_ferti_s1,
    owwu_ferti_s2,
    owwu_ferti_s3,
    owwu_ferti_p1,
    owwu_ferti_p2,
    owwu_autumn_plough,
    owwu_autumn_harrow,
    owwu_autumn_sow,
    owwu_strigling1,
    owwu_strigling2,
    owwu_strigling_sow,
    owwu_spring_sow,
    owwu_spring_roll1,
    owwu_spring_roll2,
    owwu_harvest,
    owwu_hay_turning,
    owwu_straw_chopping,
    owwu_hay_baling,
    owwu_stubble_harrow1,
    owwu_stubble_harrow2,
    owwu_deep_plough,
    owwu_catch_all,
    owwu_foobar,
} OWinterWheatUndersownToDo;


class OWinterWheatUndersown: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   OWinterWheatUndersown(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
     m_first_date=g_date->DayInYear(28,8);
     SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
       const int elements = 2 + (owwu_foobar - OWINTERWHEATUndersown_BASE);
       m_base_elements_no = OWINTERWHEATUndersown_BASE - 2;

       FarmManagementCategory catlist[elements] =
       {
            fmc_Others,	// zero element unused but must be here	
            fmc_Others,	//	    owwu_start = 1, // Compulsory, must always be 1 (one).
            fmc_Others,	//	    owwu_sleep_all_day = OWINTERWHEATUndersown_BASE,
            fmc_Fertilizer,	//	    owwu_ferti_s1,
            fmc_Fertilizer,	//	    owwu_ferti_s2,
            fmc_Fertilizer,	//	    owwu_ferti_s3,
            fmc_Fertilizer,	//	    owwu_ferti_p1,
            fmc_Fertilizer,	//	    owwu_ferti_p2,
            fmc_Cultivation,	//	    owwu_autumn_plough,
            fmc_Cultivation,	//	    owwu_autumn_harrow,
            fmc_Others,	//	    owwu_autumn_sow,
            fmc_Cultivation,	//	    owwu_strigling1,
            fmc_Cultivation,	//	    owwu_strigling2,
            fmc_Cultivation,	//	    owwu_strigling_sow,
            fmc_Others,	//	    owwu_spring_sow,
            fmc_Cultivation,	//	    owwu_spring_roll1,
            fmc_Cultivation,	//	    owwu_spring_roll2,
            fmc_Harvest,	//	    owwu_harvest,
            fmc_Others,	//	    owwu_hay_turning,
            fmc_Others,	//	    owwu_straw_chopping,
            fmc_Others,	//	    owwu_hay_baling,
            fmc_Harvest,	//	    owwu_stubble_harrow1,
            fmc_Cultivation,	//	    owwu_stubble_harrow2,
            fmc_Cultivation,	//	    owwu_deep_plough,
            fmc_Others	//	    owwu_catch_all,


               // no foobar entry	

       };
       // Iterate over the catlist elements and copy them to vector				
       copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // OWINTERWHEATUndersown_H
