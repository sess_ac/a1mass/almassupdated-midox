/**
\file
\brief
<B>PTPermanentGrassGrazed.h This file contains the headers for the PermanentGrassGrazed class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTPermanentGrassGrazed.h
//


#ifndef PTPERMANENTGRASSLANDGRAZED_H
#define PTPERMANENTGRASSLANDGRAZED_H

#define PTPERMANENTGRASSLANDGRAZED_BASE 30000
/**
\brief A flag used to indicate cattle out dates
*/
#define PT_PGG_YEARS_AFTER_SOW a_field->m_user[1]
#define PT_FL1_CATTLEOUT_DATE a_field->m_user[2]
#define PT_FL2_CATTLEOUT_DATE a_field->m_user[3]
#define PT_FL3_CATTLEOUT_DATE a_field->m_user[4]
#define PT_FL4_CATTLEOUT_DATE a_field->m_user[5]
#define PT_FL5_CATTLEOUT_DATE a_field->m_user[6]

/** Below is the list of things that a farmer can do if he is growing PermanentGrassGrazed, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_pgg_start = 1, // Compulsory, must always be 1 (one).
	pt_pgg_sleep_all_day = PTPERMANENTGRASSLANDGRAZED_BASE,
	pt_pgg_cattle_out1,//Out means outside the barns. Out in the field to eat grass.
	pt_pgg_cattle_is_out1,//Time that the catle is in the field.
	pt_pgg_cattle_out2,
	pt_pgg_cattle_is_out2,
	pt_pgg_cattle_out3,
	pt_pgg_cattle_is_out3,
	pt_pgg_cattle_out4,
	pt_pgg_cattle_is_out4,
	pt_pgg_cattle_out5,
	pt_pgg_cattle_is_out5,
	pt_pgg_ferti_p1,//NPK in non-sowing year
	pt_pgg_ferti_s1,//NPK in non-sowing year	
	pt_pgg_ferti_p2,//Slurry in sowing year
	pt_pgg_ferti_s2,//Slurry in sowing year
	pt_pgg_ferti_s3,//NPK in sowing year
	pt_pgg_ferti_p3,//NPK in sowing year
	pt_pgg_ferti_p4,//Calcium
	pt_pgg_ferti_s4,//Calcium
	pt_pgg_ferti_p5,//NPK
	pt_pgg_ferti_s5,//NPK
	pt_pgg_plough,
	pt_pgg_sow,
	pt_pgg_sow_psc,//Sow with preseeding cultivator
	pt_pgg_wait,

} PTPermanentGrassGrazedToDo;


/**
\brief
PTPermanentGrassGrazed class
\n
*/
/**
See PTPermanentGrassGrazed.h::PTPermanentGrassGrazedToDo for a complete list of all possible events triggered codes by the PermanentGrassGrazed management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTPermanentGrassGrazed: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTPermanentGrassGrazed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 31th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,1 );//Last possible date to do the first operation, i.e. plough or sow
   }
};

#endif // PTPERMANENTGRASSLANDGRAZED_H

