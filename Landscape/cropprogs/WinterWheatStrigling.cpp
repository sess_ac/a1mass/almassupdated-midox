//
// winterwheatStrigling.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WinterWheatStrigling.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgFloat cfg_strigling_prop;

bool WinterWheatStrigling::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case wws_start:
    {
      WWStrigling_AUTUMN_PLOUGH         = false;
      WWStrigling_WAIT_FOR_PLOUGH       = false;
      a_field->ClearManagementActionSum();

      // Set up the date management stuff

      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;

      // Start and stop dates for all events after harvest
      int noDates=5;
      m_field->SetMDates(0,0,g_date->DayInYear(20,8));

      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,8));
      m_field->SetMDates(0,1,0); // Subbleharrow start
      m_field->SetMDates(1,1,g_date->DayInYear(20,8));
      m_field->SetMDates(0,2,g_date->DayInYear(5,8));
      m_field->SetMDates(1,2,g_date->DayInYear(25,8));
      m_field->SetMDates(0,3,g_date->DayInYear(10,8));
      m_field->SetMDates(1,3,g_date->DayInYear(15,9));
      m_field->SetMDates(0,4,g_date->DayInYear(15,8));
      m_field->SetMDates(1,4,g_date->DayInYear(15,10));
      // Can be up to 10 of these. If the shortening code is triggered
      // then these will be reduced in value to 0
      m_field->SetMConstants(0,1);

    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary
    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "WinterWheat::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      bool toggle=false;
      for (int i=0; i<noDates; i++) {
        if  (m_field->GetMDates(0,i)>=m_ev->m_startday) {
          toggle=true;
          m_field->SetMDates(0,i,m_ev->m_startday-1);
        }
        if  (m_field->GetMDates(1,i)>=m_ev->m_startday){
          toggle=true;
          m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      if (toggle) for (int i=0; i<10; i++) m_field->SetMConstants(i,0);
    }
      // Now no operations can be timed after the start of the next crop.

      // CJT note:
      // Start single block date checking code to be cut-'n-pasted...
      int d1;
      if ( ! m_ev->m_first_year ) {

	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1 ) {
	  // Yes, too early. We assumme this is because the last crop was late
	  printf ("Poly: %d\n", m_field->GetPoly());
          g_msg->Warn( WARN_BUG, "WinterWheatStrigling::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "WinterWheatStrigling::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        // Is the first year
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
             wws_spring_roll, false );
        break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 21,8 );
      // OK, let's go.
      if (m_farm->IsStockFarmer()) // StockFarmer
      {
        SimpleEvent( d1, wws_ferti_s1, false );
      }
      else SimpleEvent( d1, wws_ferti_p1, false );
    }
    break;

  case wws_ferti_p1:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FP_Slurry( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_ferti_p1, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),wws_autumn_plough, false );
    break;

  case wws_ferti_s1:
    if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wws_ferti_s1, true );
      break;
    }
    // This may cause two applications of fertilizer in one day...
    // --FN--
    SimpleEvent( g_date->Date(),wws_ferti_s2, false );
    // --FN--
    break;

  case wws_ferti_s2:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_ferti_s2, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),wws_autumn_plough, false );
    break;

  case wws_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 95 ))
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_autumn_plough, true );
        break;
      }
      else
      {
        WWStrigling_AUTUMN_PLOUGH=true;
        SimpleEvent( g_date->Date()+1,wws_autumn_harrow, false );
        break;
      }
    }
    SimpleEvent( g_date->Date()+1,wws_stubble_harrow1, false );
    break;

  case wws_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wws_autumn_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,9 ),
               wws_autumn_sow, false );
    break;

  case wws_stubble_harrow1:
    if (!m_farm->StubbleHarrowing( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wws_stubble_harrow1, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,9 ),
               wws_autumn_sow, false );
    break;

  case wws_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
           g_date->DayInYear( 20,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wws_autumn_sow, true );
      break;
    }
    // --FN-- Oooo!
    //SimpleEvent( g_date->OldDays() + g_date->Date( ),
    //               wws_autumn_roll, false );
		SimpleEvent( g_date->Date() + 1, wws_autumn_roll, false );
    break;

  case wws_autumn_roll:
    if (( m_ev->m_lock || m_farm->DoIt( 5 ))&& (WWStrigling_AUTUMN_PLOUGH))
    {
      if (!m_farm->AutumnRoll( m_field, 0.0,
           g_date->DayInYear( 27,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_autumn_roll, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,9 ),
                      wws_ferti_p2, false );
    break;

  case wws_ferti_p2:
    if (( m_ev->m_lock || m_farm->DoIt( 20 )) && (!m_farm->IsStockFarmer()))
    {
      if ( m_field->GetVegBiomass()>0)
      //only when there has been a bit of growth
      {
        if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
           g_date->DayInYear( 30,10 ) - g_date->DayInYear()))
        {
          SimpleEvent( g_date->Date() + 1, wws_ferti_p2, true );
          break;
        }
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,9 ),
                      wws_strigling0a, false );
    break;

  case wws_strigling0a:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 100*cfg_strigling_prop.value() )))
    {
      if (!m_farm->Strigling( m_field, 0.0,
             g_date->DayInYear( 5,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_strigling0a, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 )+365,
             wws_spring_roll, false );
    break;

  case wws_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
           g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_spring_roll, true );
        break;
      }
    }
    if (m_farm->IsStockFarmer()) // StockFarmer
    {
      SimpleEvent( g_date->Date() + 1, wws_ferti_s3, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,4 ),
                    wws_ferti_s4, false );
    }
    else
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
                   wws_ferti_p3, false );
    // All need the next threads
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( )+10,
             wws_strigling0b, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4 ),
             wws_GR, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,4 ),
             wws_fungicide, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
             wws_insecticide1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,4 ),
             wws_strigling1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
             wws_water1, false );
    break;

  case wws_strigling0b:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( (int) ( 100*cfg_strigling_prop.value() ))))
    {
      if (!m_farm->Strigling( m_field, 0.0,
             g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_strigling0a, true );
        break;
      }
    }
    // End of thread
    break;

  case wws_GR:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 15*cfg_greg_app_prop.value() )))
    {
    	// --FN--
      if (!m_farm->GrowthRegulator( m_field, 0.0,
             g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_GR, true );
        break;
      }
    }
    // End of thread
    break;

  case wws_fungicide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 70*cfg_fungi_app_prop1.value() )))
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
             g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_fungicide, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,5 ),
             wws_fungicide2, false );
    break;

  case wws_fungicide2:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 50*cfg_fungi_app_prop1.value() )))
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_fungicide2, true );
        break;
      }
    }
    // End of thread
    break;

  case wws_insecticide1:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 16*cfg_ins_app_prop1.value() )))
    {
      if (!m_farm->InsecticideTreat( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, wws_insecticide1, true );
        break;
      }
     	else
        {
          SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
                   wws_insecticide2, false );
          break;
        }
    }
    break;

  case wws_insecticide2:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 33*cfg_ins_app_prop1.value() )))
    {
      if (!m_farm->InsecticideTreat( m_field, 0.0,
             g_date->DayInYear( 10,6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_insecticide2, true );
        break;
      }
      else
      {
        if ((g_date->Date()+7)<( g_date->OldDays() + g_date->DayInYear( 15,6 )))
          SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,6 ),
             wws_insecticide3, false );
        else
        	SimpleEvent( g_date->Date()+7, wws_insecticide3, false );
        break;
      }
    }
    break;

  case wws_insecticide3:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 67*cfg_ins_app_prop1.value() )))
    {
      if (!m_farm->InsecticideTreat( m_field, 0.0,
             g_date->DayInYear( 30,6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_insecticide3, true );
        break;
      }
    }
    // End of thread
    break;

  case wws_strigling1:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->Strigling( m_field, 0.0,
             g_date->DayInYear( 25,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_strigling1, true );
        break;
      }
      else
      {
        if ((g_date->Date()+7)<( g_date->OldDays() + g_date->DayInYear( 15,6 )))
              SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4),
    	               wws_strigling2, false );
        else
        	SimpleEvent( g_date->Date()+7,wws_strigling2, false );
      }
    }
    break;

  case wws_strigling2:
      if (!m_farm->Strigling( m_field, 0.0,
             g_date->DayInYear( 5,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_strigling2, true );
        break;
    }
    // End of thread
    break;

  case wws_water1:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) // **CJT** Soil type 1-4 only
    {
      if (!m_farm->Water( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_water1, true );
        break;
      }
      else
        if ((g_date->Date()+5)<( g_date->OldDays() + g_date->DayInYear( 2,5 )))
            SimpleEvent( g_date->OldDays() + g_date->DayInYear( 2,5 ),
                         wws_water2, false );
        else
          SimpleEvent( g_date->Date()+5, wws_water2, false );
    }
    break;

  case wws_water2:
    if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 1,6 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wws_water2, true );
      break;
    }
    // End of thread
    break;

  case wws_ferti_p3:
    if (!m_farm->FP_NPK( m_field, 0.0,
         g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wws_ferti_p3, true );
      break;
    }

    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4 ),
                   wws_ferti_p4, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                   wws_ferti_p5, false );
    break;

  case wws_ferti_p4:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FP_NPK( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_ferti_p4, true );
        break;
      }
    }

    // The Main thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ),
                 wws_harvest, false );
    break;

   case wws_ferti_p5:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
             g_date->DayInYear( 5,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_ferti_p5, true );
        break;
      }
    }
    break;

  case wws_ferti_s3:
    if (!m_farm->FA_Slurry(m_field, 0.0,
         g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wws_ferti_s3, true );
      break;
    }
    // The Main thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ),
                 wws_harvest, false );
    break;

  case wws_ferti_s4:
    if ( m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_ferti_s4, true );
        break;
      }
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 21,4 ),
                   wws_ferti_s5, false );
    }
   break;

  case wws_ferti_s5:
    if ( m_ev->m_lock || m_farm->DoIt( 40 ))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 1,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_ferti_s5, true );
        break;
      }
    }
    break;

  case wws_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
           g_date->DayInYear( 20,8 ) - g_date->DayInYear()))
    {
        SimpleEvent( g_date->Date() + 1, wws_harvest, true );
        break;
    }
    SimpleEvent( g_date->Date(), wws_straw_chopping, false );
    break;

  case wws_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if (!m_farm->StrawChopping( m_field, 0.0,
           m_field->GetMDates(1,0) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_straw_chopping, true );
        break;
      }
      else
      {
          SimpleEvent( g_date->Date()+m_field->GetMConstants(0), wws_stubble_harrow2, false );
      }
    }
    else
    {
      SimpleEvent( g_date->Date()+m_field->GetMConstants(0), wws_hay_turning, false );
    }
   break;

  case wws_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->HayTurning( m_field, 0.0,
           m_field->GetMDates(1,1) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_hay_turning, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
                 wws_hay_baling, false );
    break;

  case wws_hay_baling:
    if (!m_farm->HayBailing( m_field, 0.0,
       m_field->GetMDates(1,2) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wws_hay_baling, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,3),
                   wws_stubble_harrow2, false );
    break;

  case wws_stubble_harrow2:
    if ( m_ev->m_lock || m_farm->DoIt( 65 ))
    {
      if (!m_farm->StubbleHarrowing( m_field, 0.0,
           m_field->GetMDates(1,3) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_stubble_harrow2, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,4),
                   wws_grubning, false );
    break;

  case wws_grubning:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->DeepPlough( m_field, 0.0,
           m_field->GetMDates(1,4) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wws_grubning, true );
        break;
      }
    }
    done=true;
    // END OF MAIN THREAD
    break;

  default:
    g_msg->Warn( WARN_BUG, "WinterWheat::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}

