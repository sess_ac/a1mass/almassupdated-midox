/**
\file
\brief
<B>DE_Peas.h This file contains the headers for the DE_Peas class</B> \n
*/
/**
\file 
 by Chris J. Topping and Luna Kondrup Marcussen, modified by Susanne Stein \n
 Version of August 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_Peas.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_PEAS_H
#define DE_PEAS_H

#define DE_PEAS_BASE 38700
/**
\brief Flags
*/

/** Below is the list of things that a farmer can do if he is growing peas, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  de_pe_start = 1, // Compulsory, must always be 1 (one).
  de_pe_sleep_all_day = DE_PEAS_BASE,
  de_pe_autumn,
  de_pe_spring_harrow,
  de_pe_ferti_p1,
  de_pe_ferti_s1,
  de_pe_fungicide1,
  de_pe_spring_sow,
  de_pe_herbicide1,
  de_pe_herbicide2,
  de_pe_herbicide3,
  de_pe_insecticide1,
  de_pe_insecticide2,
  de_pe_herbicide4,
  de_pe_fungicide2,
  de_pe_insecticide3,
  de_pe_fungicide3,
  de_pe_insecticide4,
  de_pe_fungicide4,
  de_pe_herbicide5,
  de_pe_harvest,
  de_pe_foobar,  // Obligatory, must be last
} DE_PeasToDo;


/**
\brief
DE_Peas class
\n
*/
/**
See DE_Peas.h::DEPeasToDo for a complete list of all possible events triggered codes by the legumes management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_Peas: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DE_Peas(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		//m_first_date=g_date->DayInYear( 15,3 );
		m_first_date = g_date->DayInYear(10, 11);
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (de_pe_foobar - DE_PEAS_BASE);
	   m_base_elements_no = DE_PEAS_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			// ALL THE NECESSARY ENTRIES HERE
			fmc_Others,	//	  de_pe_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	  de_pe_sleep_all_day
			fmc_Cultivation,	//	  de_pe_autumn,
			fmc_Cultivation,	//	  de_pe_spring_harrow,
			fmc_Fertilizer,	//	  de_pe_ferti_p1,
			fmc_Fertilizer,	//	  de_pe_ferti_s1,
			fmc_Fungicide,	//	  de_pe_fungicide1,
			fmc_Others,	//	  de_pe_spring_sow,
			fmc_Herbicide,	//	  de_pe_herbicide1,
			fmc_Herbicide,	//	  de_pe_herbicide2,
			fmc_Herbicide,	//	  de_pe_herbicide3,
			fmc_Insecticide,	//	  de_pe_insecticide1,
			fmc_Insecticide,	//	  de_pe_insecticide2,
			fmc_Herbicide,	//	  de_pe_herbicide4,
			fmc_Fungicide,	//	  de_pe_fungicide2,
			fmc_Insecticide,	//	  de_pe_insecticide3,
			fmc_Fungicide,	//	  de_pe_fungicide3,
			fmc_Insecticide,	//	  de_pe_insecticide4,
			fmc_Fungicide,	//	  de_pe_fungicide4,
			fmc_Herbicide,	//	  de_pe_herbicide5,
			fmc_Harvest,		//		de_pe_harvest
			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DE_PEAS_H

