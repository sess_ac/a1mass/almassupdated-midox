/**
\file
\brief
<B>NLPermanentGrassGrazedExtensive.h This file contains the headers for the PermanentGrassGrazedExtensive class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLPermanentGrassGrazedExtensive.h
//


#ifndef NLPERMANENTGRASSLANDGRAZEDEXTENSIVE_H
#define NLPERMANENTGRASSLANDGRAZEDEXTENSIVE_H

#define NLPERMANENTGRASSLANDGRAZEDEXTENSIVE_BASE 21900
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_PGGE_FERTI_DATE		a_field->m_user[1]
#define NL_PGGE_CUT_DATE		a_field->m_user[2]
#define NL_PGGE_WATER_DATE		a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing PermanentGrassGrazedExtensive, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_pgge_start = 1, // Compulsory, must always be 1 (one).
	nl_pgge_sleep_all_day = NLPERMANENTGRASSLANDGRAZEDEXTENSIVE_BASE,
	nl_pgge_ferti_p1,
	nl_pgge_ferti_s1,
	nl_pgge_cut_to_silage1,
	nl_pgge_cut_to_silage2,
	nl_pgge_ferti_p2,
	nl_pgge_ferti_s2,
	nl_pgge_ferti_p3,
	nl_pgge_ferti_s3,
	nl_pgge_ferti_p4,
	nl_pgge_ferti_s4,
	nl_pgge_ferti_p5,
	nl_pgge_ferti_s5,
	nl_pgge_ferti_p6,
	nl_pgge_ferti_s6,
	nl_pgge_watering,
	nl_pgge_cattle_out,
	nl_pgge_cattle_is_out,
	nl_pgge_foobar
} NLPermanentGrassGrazedExtensiveToDo;


/**
\brief
NLPermanentGrassGrazedExtensive class
\n
*/
/**
See NLPermanentGrassGrazedExtensive.h::NLPermanentGrassGrazedExtensiveToDo for a complete list of all possible events triggered codes by the PermanentGrassGrazedExtensive management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLPermanentGrassGrazedExtensive: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLPermanentGrassGrazedExtensive(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_pgge_foobar - NLPERMANENTGRASSLANDGRAZEDEXTENSIVE_BASE);
	   m_base_elements_no = NLPERMANENTGRASSLANDGRAZEDEXTENSIVE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	nl_pgge_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	nl_pgge_sleep_all_day = NLPERMANENTGRASSLANDGRAZEDEXTENSIVE_BASE,
			fmc_Fertilizer,	//	nl_pgge_ferti_p1,
			fmc_Fertilizer,	//	nl_pgge_ferti_s1,
			fmc_Cutting,	//	nl_pgge_cut_to_silage1,
			fmc_Cutting,	//	nl_pgge_cut_to_silage2,
			fmc_Fertilizer,	//	nl_pgge_ferti_p2,
			fmc_Fertilizer,	//	nl_pgge_ferti_s2,
			fmc_Fertilizer,	//	nl_pgge_ferti_p3,
			fmc_Fertilizer,	//	nl_pgge_ferti_s3,
			fmc_Fertilizer,	//	nl_pgge_ferti_p4,
			fmc_Fertilizer,	//	nl_pgge_ferti_s4,
			fmc_Fertilizer,	//	nl_pgge_ferti_p5,
			fmc_Fertilizer,	//	nl_pgge_ferti_s5,
			fmc_Fertilizer,	//	nl_pgge_ferti_p6,
			fmc_Fertilizer,	//	nl_pgge_ferti_s6,
			fmc_Watering,	//	nl_pgge_watering,
			fmc_Grazing,	//	nl_pgge_cattle_out,
			fmc_Grazing	//	nl_pgge_cattle_is_out,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLPERMANENTGRASSLANDGRAZEDEXTENSIVE_H

