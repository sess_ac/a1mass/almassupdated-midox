//
// DE_OMaize.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OMaize.h"


bool DE_OMaize::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DEOMaize; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_om_start:
	{
		DE_OMAIZE_SOW_DATE       = 0;
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(30, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(25, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(30, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(de_om_spring_plough))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(20, 3) + isSpring;
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_om_fa_manure, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_om_fp_manure, false, m_farm, m_field);
		break;
	}
	break;

	case de_om_fa_manure:
		if (!m_farm->FA_Manure( m_field, 0.0, g_date->DayInYear( 25, 4 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_fa_manure, true, m_farm, m_field);
		break;
		}
		d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
		if ( g_date->Date()+1 > d1 ) {
			d1 = g_date->Date()+1;
		}
		SimpleEvent_( d1, de_om_fa_slurry_one, false, m_farm, m_field);
		break;

	case de_om_fa_slurry_one:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FA_Slurry( m_field, 0.0, g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_om_fa_slurry_one, true, m_farm, m_field);
			break;
        }
    }
    SimpleEvent_( g_date->Date() + 1, de_om_spring_plough, false, m_farm, m_field);
    break;

	case de_om_fp_manure:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_om_fp_manure, true, m_farm, m_field);
				break;
			}
		}
        d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
        if (g_date->Date() + 1 > d1) {
			d1 = g_date->Date() + 1;
        }
		SimpleEvent_(d1, de_om_fp_slurry_one, false, m_farm, m_field);
        break;

	case de_om_fp_slurry_one:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_om_fp_slurry_one, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_om_spring_plough, false, m_farm, m_field);
		break;

	case de_om_spring_plough:
		if (!m_farm->SpringPlough( m_field, 0.0, g_date->DayInYear( 1, 5 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_spring_plough, true, m_farm, m_field);
			break;
		}
		SimpleEvent_( g_date->Date(), de_om_spring_harrow, false, m_farm, m_field);
		break;

	case de_om_spring_harrow:
		if (!m_farm->SpringHarrow( m_field, 0.0, g_date->DayInYear( 5, 5 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_spring_harrow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->OldDays() + g_date->DayInYear( 25, 4 );
		if ( g_date->Date() > d1 ) {
			d1 = g_date->Date();
		}
		SimpleEvent_( d1, de_om_spring_sow, false, m_farm, m_field);
		break;

	case de_om_spring_sow:
		if (!m_farm->SpringSow( m_field, 0.0, g_date->DayInYear( 15, 5 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_spring_sow, true, m_farm, m_field);
			break;
		}
		DE_OMAIZE_SOW_DATE = g_date->Date();
		SimpleEvent_( g_date->Date(), de_om_strigling, false, m_farm, m_field);
		break;

	case de_om_strigling:
		if (!m_farm->Strigling( m_field, 0.0, g_date->DayInYear( 20, 5 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_strigling, true, m_farm, m_field);
			break;
		}
		d1 = g_date->OldDays() + g_date->DayInYear( 2, 5 );
		if ( DE_OMAIZE_SOW_DATE + 7 > d1 ) {
			d1 = DE_OMAIZE_SOW_DATE + 7;
		}
		SimpleEvent_( d1, de_om_row_one, false, m_farm, m_field);
		break;

	case de_om_row_one:
		if (!m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 5, 5 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_row_one, true, m_farm, m_field);
			break;
		}
		d1 = g_date->OldDays() + g_date->DayInYear( 1, 5 );
		if ( DE_OMAIZE_SOW_DATE + 5 > d1 ) {
			d1 = DE_OMAIZE_SOW_DATE + 5;
		}
        if (m_farm->IsStockFarmer()) //Stock Farmer
        {
            SimpleEvent_(d1, de_om_fa_slurry_two, false, m_farm, m_field);
        }
        else SimpleEvent_(d1, de_om_fp_slurry_two, false, m_farm, m_field);
		break;

	case de_om_fa_slurry_two:
		if (!m_farm->FA_Slurry( m_field, 0.0, g_date->DayInYear( 25, 5 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_fa_slurry_two, true, m_farm, m_field);
			break;
		}
		d1 = g_date->OldDays() + g_date->DayInYear( 21, 5 );
		if ( g_date->Date() + 1 > d1 ) {
			d1 = g_date->Date() + 1;
		}
		SimpleEvent_( d1, de_om_row_two, false, m_farm, m_field);
		break;

	case de_om_fp_slurry_two:
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_om_fp_slurry_two, true, m_farm, m_field);
			break;
		}
		d1 = g_date->OldDays() + g_date->DayInYear(21, 5);
		if (g_date->Date() + 1 > d1) {
			d1 = g_date->Date() + 1;
		}
		SimpleEvent_(d1, de_om_row_two, false, m_farm, m_field);
		break;

	case de_om_row_two:
		if (!m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 20, 6 ) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_row_two, true, m_farm, m_field);
			break;
		}
		// LKM: Queue up the next event - row cultivation3 done 15 days after
		SimpleEvent_(g_date->Date() + 15, de_om_row_three, false, m_farm, m_field);
		break;

	case de_om_row_three:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			//LKM: do row cultivaton before 15th of July - if not done, try again +1 day until the the 15th of July when we succeed
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_om_row_three, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_( g_date->OldDays() + g_date->DayInYear( 15, 7 ), de_om_water_one, false, m_farm, m_field);
		break;

	case de_om_water_one:
		if ( m_ev->m_lock || m_farm->DoIt(10)) {
			if (!m_farm->Water( m_field, 0.0, g_date->DayInYear( 30, 7 ) - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_om_water_one, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->OldDays() + g_date->DayInYear( 31, 7 );
		if ( g_date->Date() + 7 > d1 ) {
			d1 = g_date->Date() + 7;
		}
		SimpleEvent_( d1, de_om_water_two, false, m_farm, m_field);
		break;

	case de_om_water_two:
		if (m_ev->m_lock || m_farm->DoIt(10)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_om_water_two, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_( g_date->OldDays() + g_date->DayInYear( 1, 9 ), de_om_harvest, false, m_farm, m_field);
		break;

	case de_om_harvest:
		if (!m_farm->Harvest( m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_( g_date->Date() + 1, de_om_harvest, true, m_farm, m_field);
			break;
		}
		d1 = g_date->OldDays() + m_field->GetMDates(0,1);
		if (g_date->Date() > d1) {
			d1 = g_date->Date();
		}
		SimpleEvent_( d1, de_om_stubble, false, m_farm, m_field);
		break;

	case de_om_stubble:
		if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
			if (!m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_( g_date->Date() + 1, de_om_stubble, true, m_farm, m_field);
				break;
			}
		}
		d1=g_date->DayInYear();
		done = true;
		break;

	default:
		g_msg->Warn( WARN_BUG, "DE_OMaize::Do(): "
			"Unknown event type! ", "" );
		exit( 1 );
	}
	return done;
}


