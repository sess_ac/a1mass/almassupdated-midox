//
// OSpringBarley.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OSpringBarley.h"

extern CfgFloat cfg_strigling_prop;

bool OSpringBarley::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case osb_start:
    {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(20,8);
      // Start and stop dates for all events after harvest
      int noDates=3;
      m_field->SetMDates(0,0,g_date->DayInYear(1,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(15,8));
      m_field->SetMDates(0,1,g_date->DayInYear(15,8));
      m_field->SetMDates(1,1,g_date->DayInYear(15,8));
      m_field->SetMDates(0,2,g_date->DayInYear(5,8));
      m_field->SetMDates(1,2,g_date->DayInYear(20,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OSpringBarley::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

	  int d1;
      if ( ! m_ev->m_first_year )
      {
		  int today=g_date->Date();
		  // Are we before July 1st?
		  d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
		  if (today < d1)
		  {
			  // Yes, too early. We assumme this is because the last crop was late
			  g_msg->Warn( WARN_BUG, "OSpringBarley::Do(): "
				  "Crop start attempt between 1st Jan & 1st July", "" );
			  exit( 1 );
		  }
		  else
		  {
			  d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
			  if (today > d1)
			  {
				  // Yes too late - should not happen - raise an error
				  g_msg->Warn( WARN_BUG, "OSpringBarley::Do(): "
					  "Crop start attempt after last possible start date", "" );
				  exit( 1 );
			  }
		  }
	  }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                                                     osb_spring_harrow, false );
         break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date;
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      SimpleEvent( d1, osb_autumn_plough, false );
      OSB_ISAUTUMNPLOUGH=false;
      OSB_SOW_DATE=0;
      OSB_STRIGLING_DATE=0;
    }
    break;

  case osb_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 15, 12 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_autumn_plough, true );
        break;
      }
      OSB_ISAUTUMNPLOUGH=true;
      if (m_farm->IsStockFarmer()) // StockFarmer
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,3 ) + 365,
                 osb_ferti_s2, false );
      else                         // PlantFarmer
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 4 ) + 365,
                 osb_ferti_p2, false );
      // both must start this second thread
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 3 ) + 365,
                 osb_spring_harrow, false );
      break;
    }
    if (m_farm->IsStockFarmer()) // StockFarmer
	    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 3 ) + 365,
  	               osb_ferti_s1, false );
    else                         // PlantFarmer
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 3 ) + 365,
      	           osb_ferti_p1, false );
    break;

  //*** The plant farmers thread
  case osb_ferti_p1:
    if ( m_ev->m_lock || m_farm->DoIt( 12 ))
    {
      if (!m_farm->FP_Manure( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_ferti_p1, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 3 ),
                 osb_spring_plough_p, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 30 ,4 ),
                 osb_ferti_p2, false );
    break;

  case osb_spring_plough_p:
    if (!m_farm->SpringPlough( m_field, 0.0,
         g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osb_spring_plough_p, true );
      break;
    }
		// --FN--
    SimpleEvent( g_date->Date(), osb_spring_harrow, false );
    break;

  case osb_ferti_p2:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FP_Slurry( m_field, 0.0,
           g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_ferti_p2, true );
        break;
      }
    }
    // end of thread
    break;

    // END of the special plant thread - this now joins at obs_spring_harrow

  //*** The stock farmers thread
  case osb_ferti_s1:
    if ( m_ev->m_lock || m_farm->DoIt( 62 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_ferti_s1, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),
                 osb_spring_plough_s, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,3 ),
                 osb_ferti_s2, false );
    break;

  case osb_spring_plough_s:
    if (!m_farm->SpringPlough( m_field, 0.0,
         g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osb_spring_plough_s, true );
      break;
    }
    SimpleEvent( g_date->Date(),
                 osb_spring_harrow, false );
    break;

  case osb_ferti_s2:
    if ( m_ev->m_lock || m_farm->DoIt( 80 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 20, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_ferti_s2, true );
        break;
      }
    }
    // end of thread
    break;
    // END of the special stock thread - this now joins at obs_spring_harrow

  case osb_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osb_spring_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 ),
                 osb_spring_sow1, false );
    break;

  case osb_spring_sow1:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osb_spring_sow1, true );
      break;
    }
    OSB_SOW_DATE=g_date->DayInYear();
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
                 osb_spring_sow2, false );
    break;

  case osb_spring_sow2:
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))
    {
      if (!m_farm->SpringSow( m_field, 0.0,
           g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_spring_sow2, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16,3 ),
                 osb_spring_roll, false );
    break;

  case osb_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 90 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
           g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_spring_roll, true );
        break;
      }
    }
    if (OSB_SOW_DATE+5 >g_date->DayInYear( 20,3 ))
	    SimpleEvent( g_date->OldDays() + OSB_SOW_DATE+5,
  	               osb_strigling1, false );
    else
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 ),
      	           osb_strigling1, false );
    break;

  case osb_strigling1:
    if ( m_ev->m_lock || (cfg_strigling_prop.value()*m_farm->DoIt( 90 )))
    {
      if (!m_farm->Strigling( m_field, 0.0,
           g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_strigling1, true );
        break;
      }
      else
      {
        OSB_STRIGLING_DATE=g_date->Date();
      	if ( g_date->Date()+5<g_date->DayInYear( 10,4 ))
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,4 ),
            osb_strigling2, false );
        else SimpleEvent( g_date->Date()+5, osb_strigling2, false );
      }
      break;
    }
    // didn't do strigling so straight to harvest
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                 osb_harvest, false );
    break;

  case osb_strigling2:
    if (!m_farm->Strigling( m_field, 0.0,
 	       g_date->DayInYear( 25,4 ) - g_date->DayInYear())) {
   	  SimpleEvent( g_date->Date() + 1, osb_strigling2, true );
     	break;
    }
    else
      {
        OSB_STRIGLING_DATE=g_date->Date();
      	if ( g_date->Date()+5<g_date->DayInYear( 21,4 ))
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 21,4 ),
          osb_strigling3, false );
        else SimpleEvent( g_date->Date()+5, osb_strigling3, false );
      }
    break;

  case osb_strigling3:
    if ( m_ev->m_lock || (cfg_strigling_prop.value()*m_farm->DoIt( 89 )))
    {
      if (!m_farm->Strigling( m_field, 0.0,
           g_date->DayInYear( 5,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_strigling3, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,0),
                 osb_harvest, false );
    break;

  case osb_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         m_field->GetMDates(1,0) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osb_harvest, true );
      break;
    }
    SimpleEvent( g_date->Date(), osb_straw_chopping, false );
    break;

  case osb_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->StrawChopping( m_field, 0.0,
           m_field->GetMDates(1,1) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osb_straw_chopping, true );
        break;
      }
      // END MAIN THREAD
      done=true;
			// --FN--
      break;
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
                 osb_hay, false );
    break;

  case osb_hay:
    if (!m_farm->HayBailing( m_field, 0.0,
         m_field->GetMDates(1,2) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osb_hay, true );
      break;
    }
    // END MAIN THREAD
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OSpringBarley::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


