/**
\file
\brief
<B>DE_OPermanentGrassGrazed.h This file contains the headers for the PermanentGrassGrazed class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PermanentGrassGrazed.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_OPERMANENTGRASSGRAZED_H
#define DE_OPERMANENTGRASSGRAZED_H

#define DE_OPGG_BASE 38400
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing spring barley Fodder, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  de_opgg_start = 1, // Compulsory, must always be 1 (one).
  de_opgg_sleep_all_day = DE_OPGG_BASE,
  de_opgg_grazing1,
  de_opgg_cattle_is_out1,
  de_opgg_cutting1,
  de_opgg_grazing2,
  de_opgg_cattle_is_out2,
  de_opgg_foobar,
} DE_OPermanentGrassGrazedToDo;


/**
\brief
DE_OPermanentGrassGrazed class
\n
*/
/**
See DE_OPermanentGrassGrazed.h::DE_OPermanentGrassGrazedToDo for a complete list of all possible events triggered codes by the PermanentGrassGrazed management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_OPermanentGrassGrazed : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_OPermanentGrassGrazed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(25, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_opgg_foobar - DE_OPGG_BASE);
		m_base_elements_no = DE_OPGG_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	de_opgg_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	de_opgg_sleep_all_day = DE_OPGG_BASE,
			fmc_Grazing,	//	de_opgg_grazing1,
			fmc_Grazing,	//	de_opgg_cattle_is_out1,
			fmc_Cutting,	//	de_opgg_cutting1,
			fmc_Grazing,	//	de_opgg_grazing2,
			fmc_Grazing,	//	de_opgg_cattle_is_out2,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_OPERMANENTGRASSGRAZED_H

