//
// DK_OCerealLegume_Whole.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKOCerealLegume_Whole_H
#define DKOCerealLegume_Whole_H

#define DK_OCLW_BASE 61600

typedef enum {
  dk_oclw_start = 1, // Compulsory, start event must always be 1 (one).
  dk_oclw_spring_harrow = DK_OCLW_BASE,
  dk_oclw_manure1_s,
  dk_oclw_manure1_p,
  dk_oclw_spring_plough,
  dk_oclw_spring_sow,
  dk_oclw_spring_sow_lo,
  dk_oclw_strigling1, 
  dk_oclw_strigling2,
  dk_oclw_water,
  dk_oclw_water_lo,
  dk_oclw_swathing,
  dk_oclw_swathing_lo,
  dk_oclw_harvest,
  dk_oclw_harvest_lo,
  dk_oclw_manure2_s,
  dk_oclw_manure2_p,
  dk_oclw_autumn_plough,
  dk_oclw_cutting1,
  dk_oclw_cutting2,
  dk_oclw_grazing,
  dk_oclw_cattle_is_out,
  dk_oclw_foobar, 
} DK_OCerealLegume_WholeToDo;



class DK_OCerealLegume_Whole : public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OCerealLegume_Whole(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(31,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_oclw_foobar - DK_OCLW_BASE);
	  m_base_elements_no = DK_OCLW_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_oclw_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation,	//	  dk_oclw_spring_harrow = DK_OCLW_BASE,
			fmc_Fertilizer,	//	  dk_oclw_manure1_s,
			fmc_Fertilizer,	//	  dk_oclw_manure1_p,
			fmc_Cultivation,	//	  dk_oclw_spring_plough,
			fmc_Others,	//	  dk_oclw_spring_sow,
			fmc_Others,	//	  dk_oclw_spring_sow_lo,
			fmc_Cultivation,	//	  dk_oclw_strigling1, 
			fmc_Cultivation,	//	  dk_oclw_strigling2,
			fmc_Watering,	//	  dk_oclw_water,
			fmc_Watering,	//	  dk_oclw_water_lo,
			fmc_Cutting,	//	  dk_oclw_swathing,
			fmc_Cutting,	//	  dk_oclw_swathing_lo,
			fmc_Harvest,	//	  dk_oclw_harvest,
			fmc_Harvest,	//	  dk_oclw_harvest_lo,
			fmc_Fertilizer,	//	  dk_oclw_manure2_s,
			fmc_Fertilizer,	//	  dk_oclw_manure2_p,
			fmc_Cultivation,	//	  dk_oclw_autumn_plough,
			fmc_Cutting,	//	  dk_oclw_cutting1,
			fmc_Cutting,	//	  dk_oclw_cutting2,
			fmc_Grazing,	//	  dk_oclw_grazing,
			fmc_Grazing	//	  dk_oclw_cattle_is_out,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_OCerealLegume_Whole_H
