/**
\file
\brief
<B>PTCloverGrassGrazed1.h This file contains the headers for the CloverGrassGrazed1 class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTCloverGrassGrazed1.h
//


#ifndef PTCLOVERGRASSGRAZED1_H
#define PTCLOVERGRASSGRAZED1_H

#define PTCLOVERGRASSGRAZED1_BASE 30600
/**
\brief A flag used to indicate cattle out dates
*/
#define PT_FL1_CATTLEOUT_DATE a_field->m_user[1]
#define PT_FL2_CATTLEOUT_DATE a_field->m_user[2]
#define PT_FL3_CATTLEOUT_DATE a_field->m_user[3]

/** Below is the list of things that a farmer can do if he is growing clover grass grazed in the first year, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_cgg1_start = 1, // Compulsory, must always be 1 (one).
	pt_cgg1_sleep_all_day = PTCLOVERGRASSGRAZED1_BASE,
	pt_cgg1_autumn_plough,
	pt_cgg1_ferti_p1, // slurry
	pt_cgg1_ferti_s1,
	pt_cgg1_ferti_p2, // NPK
	pt_cgg1_ferti_s2,
	pt_cgg1_ferti_p3, // Calcium
	pt_cgg1_ferti_s3,
	pt_cgg1_ferti_p4, // NPK
	pt_cgg1_ferti_s4,
	pt_cgg1_autumn_roll,
	pt_cgg1_autumn_sow,
	pt_cgg1_preseeding_cultivator_sow,
	pt_cgg1_harvest1,
	pt_cgg1_harvest2,
	pt_cgg1_hay_bailing1,
	pt_cgg1_hay_bailing2,
	pt_cgg1_cattle_out1,
	pt_cgg1_cattle_is_out1,
	pt_cgg1_cattle_out2,
	pt_cgg1_cattle_is_out2,
	pt_cgg1_cattle_out3,
	pt_cgg1_cattle_is_out3,
	pt_cgg1_wait,

} PTCloverGrassGrazed1ToDo;


/**
\brief
PTCloverGrassGrazed1 class
\n
*/
/**
See PTCloverGrassGrazed1.h::PTCloverGrassGrazed1ToDo for a complete list of all possible events triggered codes by the clover grass grazed management plan in the first year. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTCloverGrassGrazed1: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTCloverGrassGrazed1(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,10 );// AAS:Last possible date to do the first operation, i.e. plough or sow
   }
};

#endif // PTCLOVERGRASSGRAZED1_H

