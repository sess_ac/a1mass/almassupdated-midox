//
// NorwegianOats.h
//
/* 
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef NorwegianPotatoes_H
#define NorwegianPotatoes_H

#define NPOTATOES_BASE 29100
#define NPOT_SLURRY_DATE m_field->m_user[0]
#define NPOT_HERBI_DATE  m_field->m_user[1]
#define NPOT_STRIG_DATE  m_field->m_user[2]
#define NPOT_HILL_DATE   m_field->m_user[3]
#define NPOT_DID_TREAT   m_field->m_user[4]
#define NPOT_DID_HILL    m_field->m_user[5]
#define NPOT_WATER_DATE  m_field->m_user[6]
#define NPOT_FUNGI_DATE  m_field->m_user[7]

typedef enum {
	npe_start = 1, // Compulsory, start event must always be 1 (one).
	npe_autumn_plough = NPOTATOES_BASE,
	npe_spring_plough,
	npe_spring_harrow,
	npe_fa_slurry,
	npe_spring_sow,
	npe_fa_npk,
	npe_fp_npk,
	npe_herbi_one,
	npe_herbi_two,
	npe_herbi_three,
	npe_strigling_one,
	npe_strigling_two,
	npe_strigling_three,
	npe_hilling,
	npe_insecticide,
	npe_water_one,
	npe_water_two,
	npe_water_three,
	npe_fungi_one,
	npe_fungi_two,
	npe_fungi_three,
	npe_fungi_four,
	npe_fungi_five,
	npe_growth_reg,
	npe_harvest,
	npe_foobar
} NorwegianPotatoesToDo;



class NorwegianPotatoes : public Crop
{
public:
	bool  Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev);
	NorwegianPotatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 11);
		m_ddegstoharvest = 1400; // this is hard coded for each crop that uses this 
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (npe_foobar - NPOTATOES_BASE);
		m_base_elements_no = NPOTATOES_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
				fmc_Others,	// zero element unused but must be here
			fmc_Others,//npe_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation,//npe_autumn_plough = NPOTATOES_BASE,
			fmc_Cultivation,//npe_spring_plough,
			fmc_Cultivation,//npe_spring_harrow,
			fmc_Fertilizer,//npe_fa_slurry,
			fmc_Others,//npe_spring_sow,
			fmc_Fertilizer,//npe_fa_npk,
			fmc_Fertilizer,//npe_fp_npk,
			fmc_Herbicide,//npe_herbi_one,
			fmc_Herbicide,//npe_herbi_two,
			fmc_Herbicide,//npe_herbi_three,
			fmc_Others,//npe_strigling_one,
			fmc_Others,//npe_strigling_two,
			fmc_Others,//npe_strigling_three,
			fmc_Others,//npe_hilling,
			fmc_Insecticide,//npe_insecticide,
			fmc_Watering,//npe_water_one,
			fmc_Watering,//npe_water_two,
			fmc_Watering,//npe_water_three,
			fmc_Fungicide,//npe_fungi_one,
			fmc_Fungicide,//npe_fungi_two,
			fmc_Fungicide,//npe_fungi_three,
			fmc_Fungicide,//npe_fungi_four,
			fmc_Fungicide,//npe_fungi_five,
			fmc_Others,//npe_growth_reg,
			fmc_Harvest//npe_harvest

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // NorwegianPotatoes_H
