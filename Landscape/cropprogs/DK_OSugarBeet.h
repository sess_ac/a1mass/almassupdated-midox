//
// DK_OSugarbeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_OSugarBeet_h
#define DK_OSugarBeet_h

#define DK_OSB_BASE 60300
#define DK_OSBE_FORCESPRING	a_field->m_user[1]

typedef enum {
  dk_osbe_start = 1, // Compulsory, start event must always be 1 (one).
  dk_osbe_harvest = DK_OSB_BASE,
  dk_osbe_ferti_s,
  dk_osbe_ferti_p,
  dk_osbe_autumn_plough,
  dk_osbe_winter_harrow,
  dk_osbe_molluscicide1,
  dk_osbe_spring_plough,
  dk_osbe_sharrow1,
  dk_osbe_sharrow2,
  dk_osbe_sharrow3,
  dk_osbe_sow,
  dk_osbe_molluscicide2,
  dk_osbe_strigling,
  dk_osbe_harrow,
  dk_osbe_row_cultivation1,
  dk_osbe_water1,
  dk_osbe_water2,
  dk_osbe_row_cultivation2,
  dk_osbe_row_cultivation3,
  dk_osbe_manualweeding,
  dk_osbe_foobar,
} DK_OSugarBeetToDo;



class DK_OSugarBeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OSugarBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(20,11); // 
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_osbe_foobar - DK_OSB_BASE);
	  m_base_elements_no = DK_OSB_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_osbe_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_osbe_harvest = DK_OSB_BASE,
			fmc_Fertilizer, // dk_osbe_ferti_s
			fmc_Fertilizer, // dk_osbe_ferti_p
			fmc_Cultivation,	//	  dk_osbe_autumn_plough,
			fmc_Cultivation,	//	  dk_osbe_winter_harrow,
			fmc_Others,	//	  dk_osbe_molluscicide1,
			fmc_Cultivation,	//	  dk_osbe_spring_plough,
			fmc_Cultivation,	//	  dk_osbe_sharrow1,
			fmc_Cultivation,	//	  dk_osbe_sharrow2,
			fmc_Cultivation,	//	  dk_osbe_sharrow3,
			fmc_Others,	//	  dk_osbe_sow,
			fmc_Others,	//	  dk_osbe_molluscicide2,
			fmc_Cultivation,	//	  dk_osbe_strigling,
			fmc_Cultivation,	//	  dk_osbe_harrow,
			fmc_Cultivation,	//	  dk_osbe_row_cultivation1,
			fmc_Watering,	//	  dk_osbe_water1,
			fmc_Watering,	//	  dk_osbe_water2,
			fmc_Cultivation,	//	  dk_osbe_row_cultivation2,
			fmc_Cultivation,	//	  dk_osbe_row_cultivation3,
			fmc_Cultivation	//	  dk_osbe_manualweeding,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_OSugarBeet_h
