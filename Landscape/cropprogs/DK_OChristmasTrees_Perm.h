/**
\file
\brief
<B>DK_OChristmasTrees_Perm1.h This file contains the headers for the DK_OChristmasTrees_Perm class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of January 2023 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_OChristmasTrees_Perm.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OCHRISTMASTREES_PERM_H
#define DK_OCHRISTMASTREES_PERM_H

#define DK_OCTP_BASE 63400
/**
\brief A flag used to indicate year
*/
#define DK_OCTP_YEAR	a_field->m_user[1]
/** Below is the list of things that a farmer can do if he is growing DK_OChristmasTrees_Perm1_autumn, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_octp_start = 1, // Compulsory, must always be 1 (one).
	dk_octp_sleep_all_day = DK_OCTP_BASE,
	dk_octp_plough1_autumn, //establishment
	dk_octp_depth_plough_autumn,
	dk_octp_plough2_autumn,
	dk_octp_stubble_harrow_autumn,
	dk_octp_sow_cover_crop_autumn,
	dk_octp_plant_trees_autumn,
	dk_octp_row_cultivation_autumn,
	dk_octp_harrow_autumn,
	dk_octp_crush_trees, // 2nd establishment
	dk_octp_sow_inter_crops,
	dk_octp_harrow,
	dk_octp_sow_catch_crop1,
	dk_octp_plant_trees,
	dk_octp_ferti_clay_s2, //after establishment (y2-5)
	dk_octp_ferti_sand_s2,
	dk_octp_ferti_clay_p2,
	dk_octp_ferti_sand_p2,
	dk_octp_manual_weeding_2,
	dk_octp_manual_weeding2_2,
	dk_octp_ferti_clay_s3_4,
	dk_octp_ferti_sand_s3_4,
	dk_octp_npk_clay_s3_4,
	dk_octp_npk_sand1_s3_4,
	dk_octp_npk_sand2_s3_4,
	dk_octp_ferti_clay_p3_4,
	dk_octp_ferti_sand_p3_4,
	dk_octp_npk_clay_p3_4,
	dk_octp_npk_sand1_p3_4,
	dk_octp_npk_sand2_p3_4,
	dk_octp_manual_weeding_3_4,
	dk_octp_manual_weeding2_3_4,
	dk_octp_manual_cutting_3_4,
	dk_octp_grazing_3_4,
	dk_octp_pig_is_out_3_4,
	dk_octp_sow_catch_crop2,
	dk_octp_npk_s5,
	dk_octp_npk_sand_s5,
	dk_octp_npk_p5,
	dk_octp_npk_sand_p5,
	dk_octp_grazing_5,
	dk_octp_pig_is_out_5,
	dk_octp_manual_weeding_5,
	dk_octp_manual_weeding2_5,
	dk_octp_manual_cutting_5,
	dk_octp_npk1_s6, // after establishment (y6-10)
	dk_octp_npk2_s6,
	dk_octp_npk1_p6,
	dk_octp_npk2_p6,
	dk_octp_calcium_s6,
	dk_octp_calcium_p6,
	dk_octp_grazing_6,
	dk_octp_pig_is_out_6,
	dk_octp_manual_weeding_6,
	dk_octp_manual_weeding2_6,
	dk_octp_manual_cutting_6,
	dk_octp_npk1_s7_8,
	dk_octp_npk2_s7_8,
	dk_octp_calcium_s7_8,
	dk_octp_npk1_p7_8,
	dk_octp_npk2_p7_8,
	dk_octp_calcium_p7_8,
	dk_octp_grazing_7_8,
	dk_octp_pig_is_out_7_8,
	dk_octp_manual_weeding_7_8,
	dk_octp_manual_weeding2_7_8,
	dk_octp_manual_cutting_7_8,
	dk_octp_manual_cutting2_7_8,
	dk_octp_npk1_s9_10,
	dk_octp_npk2_s9_10,
	dk_octp_calcium_s9_10,
	dk_octp_npk1_p9_10,
	dk_octp_npk2_p9_10,
	dk_octp_calcium_p9_10,
	dk_octp_sow_catch_crop3,
	dk_octp_manual_weeding_9_10,
	dk_octp_manual_weeding2_9_10,
	dk_octp_npk1_s11, // after establishment (y11 - harvest year)
	dk_octp_npk2_s11,
	dk_octp_calcium_s11,
	dk_octp_npk1_p11,
	dk_octp_npk2_p11,
	dk_octp_calcium_p11,
	dk_octp_manual_weeding_11,
	dk_octp_manual_weeding2_11,
	dk_octp_sow_catch_crop_11,
	dk_octp_manual_cutting_11,
	dk_octp_harvest,
	dk_octp_wait,
	dk_octp_foobar,

} DK_OChristmasTrees_PermToDo;


/**
\brief
DK_OChristmasTrees_Perm class
\n
*/
/**
See DK_OChristmasTrees_Perm.h::DK_OChristmasTrees_PermToDo for a complete list of all possible events triggered codes by the DK_OChristmasTrees_Perm management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OChristmasTrees_Perm : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OChristmasTrees_Perm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_octp_foobar - DK_OCTP_BASE);
		m_base_elements_no = DK_OCTP_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
		fmc_Others,	// zero element unused but must be here	
		fmc_Others,	//	dk_octp_start = 1, // Compulsory, must always be 1 (one).
		fmc_Others,	//	dk_octp_sleep_all_day = DK_CTP1_BASE,
		fmc_Cultivation,	//	dk_octp_plough1_autumn,
		fmc_Cultivation,	//	dk_octp_depth_plough_autumn,
		fmc_Cultivation,	//	dk_octp_plough2_autumn,
		fmc_Cultivation,	//	dk_octp_stubble_harrow_autumn,
		fmc_Others,	//	dk_octp_sow_cover_crop_autumn,
		fmc_Others,	//	dk_octp_plant_trees_autumn,
		fmc_Cultivation,	//	dk_octp_row_cultivation_autumn,
		fmc_Cultivation,	//	dk_octp_harrow_autumn,
		fmc_Cutting, //dk_octpe_crush_trees,
		fmc_Others, //dk_octpe_sow_inter_crops,
		fmc_Cultivation, // dk_octpe_harrow,
		fmc_Others, // dk_octpe_sow_catch_crops,
		fmc_Others, // dk_octpe_plant_trees,
		fmc_Fertilizer, //dk_octp_ferti_clay_s2,
		fmc_Fertilizer, //dk_octp_ferti_sand_s2,
		fmc_Fertilizer, //dk_octp_ferti_clay_p2,
		fmc_Fertilizer, //dk_octp_ferti_sand_p2,
		fmc_Cultivation, //dk_octp_manual_weeding_2,
		fmc_Cultivation, //	dk_octp_manual_weeding
		fmc_Fertilizer, //dk_octp_ferti_clay_s3_4,
		fmc_Fertilizer, //dk_octp_ferti_sand_s3_4,
		fmc_Fertilizer, //dk_octp_npk_clay_s3_4,
		fmc_Fertilizer, //dk_octp_npk_sand1_s3_4,
		fmc_Fertilizer, //dk_octp_npk_sand2_s3_4,
		fmc_Fertilizer, //dk_octp_ferti_clay_p3_4,
		fmc_Fertilizer, //dk_octp_ferti_sand_p3_4,
		fmc_Fertilizer, //dk_octp_npk_clay_p3_4,
		fmc_Fertilizer, //dk_octp_npk_sand1_p3_4,
		fmc_Fertilizer, //dk_octp_npk_sand2_p3_4,
		fmc_Cultivation, //dk_octp_manual_weeding_3_4,
		fmc_Cultivation, //	dk_octp_manual_weeding
		fmc_Cutting, // dk_octp_manual_cutting_3_4
		fmc_Grazing, //dk_octp_grazing_3_4,
		fmc_Grazing, //dk_octp_pig_is_out_3_4,
		fmc_Others, //dk_octp_sow_catch_crop,
		fmc_Fertilizer, //dk_octp_npk_s5,
		fmc_Fertilizer, //dk_octp_npk_sand_s5,
		fmc_Fertilizer, //dk_octp_npk_p5,
		fmc_Fertilizer, //dk_octp_npk_sand_p5,
		fmc_Grazing, //dk_octp_grazing_5,
		fmc_Grazing, //dk_octp_pig_is_out_5,
		fmc_Cultivation, //dk_octp_manual_weeding_5,
		fmc_Cultivation, //	dk_octp_manual_weeding
		fmc_Cutting, //dk_octp_manual_cutting_5,
		fmc_Fertilizer, //dk_octp_npk1_s6,
		fmc_Fertilizer, //dk_octp_npk2_s6,
		fmc_Fertilizer, //dk_octp_calcium_s6,
		fmc_Fertilizer, //dk_octp_npk1_p6,
		fmc_Fertilizer, //dk_octp_npk2_p6,
		fmc_Fertilizer, //dk_octp_calcium_p6,
		fmc_Grazing, //dk_octp_grazing_6,
		fmc_Grazing, //dk_octp_pig_is_out_6,
		fmc_Cultivation, //dk_octp_manual_weeding_6,
		fmc_Cultivation, //	dk_octp_manual_weeding
		fmc_Cutting, //dk_octp_manual_cutting_6,
		fmc_Fertilizer, //dk_octp_npk1_s7_8,
		fmc_Fertilizer, //dk_octp_npk2_s7_8,
		fmc_Fertilizer, //dk_octp_calcium_s7_8,
		fmc_Fertilizer, //dk_octp_npk1_p7_8,
		fmc_Fertilizer, //dk_octp_npk2_p7_8,
		fmc_Fertilizer, //dk_octp_calcium_p7_8,
		fmc_Grazing, //dk_octp_grazing_7_8,
		fmc_Grazing, //dk_octp_pig_is_out_7_8,
		fmc_Cultivation, //dk_octp_manual_weeding_7_8,
		fmc_Cultivation, //	dk_octp_manual_weeding
		fmc_Cutting, //dk_octp_manual_cutting_7_8,
		fmc_Cutting, //dk_octp_manual_cutting2_7_8,
		fmc_Fertilizer, //dk_octp_npk1_s9_10,
		fmc_Fertilizer, //dk_octp_npk2_s9_10,
		fmc_Fertilizer, //dk_octp_calcium_s9_10,
		fmc_Fertilizer, //dk_octp_npk1_p9_10,
		fmc_Fertilizer, //dk_octp_npk2_p9_10,
		fmc_Fertilizer, //dk_octp_calcium_p9_10,
		fmc_Others, //	dk_octp_sow_catch_crop,
		fmc_Cultivation, //	dk_octp_manual_weeding_9_10,
		fmc_Cultivation, //	dk_octp_manual_weeding_9_10,
		fmc_Fertilizer, //dk_octp_npk1_s11,
		fmc_Fertilizer, //dk_octp_npk2_s11,
		fmc_Fertilizer, //dk_octp_calcium_s11,
		fmc_Fertilizer, //dk_octp_npk1_p11,
		fmc_Fertilizer, //dk_octp_npk2_p11,
		fmc_Fertilizer, //dk_octp_calcium_p11,
		fmc_Cultivation, //dk_octp_manual_weeding_11,
		fmc_Cultivation, //	dk_octp_manual_weeding
		fmc_Others, //dk_octp_sow_catch_crop_11,
		fmc_Cutting, //dk_octp_manual_cutting_11,
		fmc_Harvest, //dk_octp_harvest,
		fmc_Others, //dk_octp_wait


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_OChristmasTrees_Perm_H