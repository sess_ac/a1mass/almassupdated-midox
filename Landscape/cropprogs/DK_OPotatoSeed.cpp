//
// DK_OPotatoSeed.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OPotatoSeed.h"


bool DK_OPotatoSeed::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  TTypesOfVegetation l_tov = tov_DKOPotatoSeed;

  switch ( m_ev->m_todo ) {
  case dk_opos_start:
      {
          a_field->ClearManagementActionSum();
          DK_OPOS_FORCESPRING = false;
          DK_OPOS_SANDY = false;

          m_last_date = g_date->DayInYear(15, 9); // Should match the last flexdate below
              //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
          std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
          // Set up the date management stuff
                  // Start and stop dates for all events after harvest
          flexdates[0][1] = g_date->DayInYear(1, 9); // last possible day 
          // Now these are done in pairs, start & end for each operation. If its not used then -1
          flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
          flexdates[1][1] = g_date->DayInYear(15, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

          // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
          int isSpring = 0;
          if (StartUpCrop(isSpring, flexdates, int(dk_opos_spring_remove_straw))) break;

          // End single block date checking code. Please see next line comment as well.
          // Reinit d1 to first possible starting date.
          d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
          // OK, let's go.
          // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
          if (m_ev->m_forcespring) {
              SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_opos_spring_remove_straw, false);
              DK_OPOS_FORCESPRING = true;
              break;
          }
          else SimpleEvent(d1, dk_opos_autumn_remove_straw, false);
      }
      break;

  case dk_opos_autumn_remove_straw:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->StrawRemoval(m_field, 0.0,
              g_date->DayInYear(29, 11) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_autumn_remove_straw, true);
              break;
          }
      }
      SimpleEvent(g_date->Date(), dk_opos_autumn_stoneburier, false);
      break;

  case dk_opos_autumn_stoneburier: // if many stones or tussocks - suggests 10%
      if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
          if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0,
              g_date->DayInYear(30, 11) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_autumn_stoneburier, true);
              break;
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_opos_deep_harrow1, false);
          break;
      }
      SimpleEvent(g_date->Date(), dk_opos_autumn_plough, false);
      break;

  case dk_opos_autumn_plough:
      if (!m_farm->AutumnPlough(m_field, 0.0,
          g_date->DayInYear(30, 11) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_opos_autumn_plough, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_opos_deep_harrow1, false);
      break;

  case dk_opos_spring_remove_straw:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->StrawRemoval(m_field, 0.0,
              g_date->DayInYear(14, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_spring_remove_straw, true);
              break;
          }
      }
      SimpleEvent(g_date->Date(), dk_opos_spring_stoneburier, false);
      break;

  case dk_opos_spring_stoneburier: // if many stones or tussocks - suggests 10%
      if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
          if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0,
              g_date->DayInYear(15, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_spring_stoneburier, true);
              break;
          }
          SimpleEvent(g_date->Date() + 1, dk_opos_deep_harrow1, false);
          break;
      }
      SimpleEvent(g_date->Date(), dk_opos_spring_plough, false);
      break;

  case dk_opos_spring_plough:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->SpringPlough(m_field, 0.0,
              g_date->DayInYear(15, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_spring_plough, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_opos_deep_harrow1, false);
      break;

  case dk_opos_deep_harrow1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->StubbleHarrowing(m_field, 0.0,
              g_date->DayInYear(31, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_deep_harrow1, true);
              break;
          }
      }
      if (a_farm->IsStockFarmer()) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 3), dk_opos_ferti_s, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(31, 3), dk_opos_ferti_p, false);
      break;

  case dk_opos_ferti_s:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->FA_NPK(m_field, 0.0,
              g_date->DayInYear(18, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_ferti_s, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_opos_ferti_sow, false);
      break;

  case dk_opos_ferti_p:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->FP_NPK(m_field, 0.0,
              g_date->DayInYear(18, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_ferti_p, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_opos_ferti_sow, false);
      break;

  case dk_opos_ferti_sow:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->SpringSowWithFerti(m_field, 0.0,
              g_date->DayInYear(20, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_ferti_sow, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_opos_calcium_sand_s, false);
      SimpleEvent(g_date->Date() + 2, dk_opos_harrow, false);
      break;

  case dk_opos_calcium_sand_s:
      if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
      {
          if (a_farm->IsStockFarmer()) {
              if (!m_farm->FA_Calcium(m_field, 0.0,
                  g_date->DayInYear(5, 5) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_opos_calcium_sand_s, true);
                  break;
              }
              break;
          }
          else SimpleEvent(g_date->Date() + 1, dk_opos_calcium_sand_p, false);
          break;
      }
      break;
      // end of thread

  case dk_opos_calcium_sand_p:
      if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
      {
          if (!m_farm->FP_Calcium(m_field, 0.0,
              g_date->DayInYear(5, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_calcium_sand_p, true);
              break;
          }
          break;
      }
      break; // end of thread

  case dk_opos_harrow:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->SpringHarrow(m_field, 0.0,
              g_date->DayInYear(23, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_harrow, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_opos_hilling_up1, false);
      break;

  case dk_opos_hilling_up1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->HillingUp(m_field, 0.0,
              g_date->DayInYear(24, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_hilling_up1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 20, dk_opos_row_cultivation1, false); // main thread
      if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
      {
          SimpleEvent(g_date->Date() + 45, dk_opos_water1_s, false); // sand water thread
          break;
      }
      else SimpleEvent(g_date->Date() + 45, dk_opos_water1_c, false); // clay water thread
      break;

  case dk_opos_water1_s:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(15, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_water1_s, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 30, dk_opos_water2_s, false);
      break;

  case dk_opos_water2_s:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(15, 7) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_water2_s, true);
              break;
          }
      }
      break; // end of thread

  case dk_opos_water1_c:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) { // 50% of sandy soils, suggest 25% of clay soils
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(15, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_water1_c, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 30, dk_opos_water2_c, false);
      break;

  case dk_opos_water2_c:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) { 
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(15, 7) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_water2_c, true);
              break;
          }
      }
      break; // end of thread

  case dk_opos_row_cultivation1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(15, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_row_cultivation1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_opos_hilling_up2, false);
      break;

  case dk_opos_hilling_up2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->HillingUp(m_field, 0.0,
              g_date->DayInYear(16, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_hilling_up2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 20, dk_opos_row_cultivation2, false);
      break;

  case dk_opos_row_cultivation2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(6, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_row_cultivation2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_opos_hilling_up3, false);
      break;

  case dk_opos_hilling_up3:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->HillingUp(m_field, 0.0,
              g_date->DayInYear(7, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_hilling_up3, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 20, dk_opos_row_cultivation3, false);
      break;

  case dk_opos_row_cultivation3:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->RowCultivation(m_field, 0.0,
              g_date->DayInYear(28, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_row_cultivation3, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_opos_hilling_up4, false);
      break;

  case dk_opos_hilling_up4:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->HillingUp(m_field, 0.0,
              g_date->DayInYear(29, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_hilling_up4, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 7, dk_opos_top_off, false);
      break;

  case dk_opos_top_off:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.30)) { //% suggestion
          if (!m_farm->BurnTop(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_top_off, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 10, dk_opos_harvest, false);
      break;

  case dk_opos_harvest:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->HarvestLong(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_opos_harvest, true);
              break;
          }
      }
    //  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), dk_opos_deep_harrow2, false);
   //   break;

 // case dk_opos_deep_harrow2:
     // if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
     //     if (!m_farm->StubbleHarrowing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
      //        SimpleEvent(g_date->Date() + 1, dk_opos_deep_harrow2, true);
     //         break;
    //      }
     // }
      done = true;
      break;

  default:
      g_msg->Warn(WARN_BUG, "DK_OPotatoSeed::Do(): "
          "Unknown event type! ", "");
      exit(1);
  }
  if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
  return done;
}
 

