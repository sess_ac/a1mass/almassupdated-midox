/**
\file
\brief
<B>WinterWheat.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of June 2003 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// WinterWheat.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WINTERWHEAT_H
#define WINTERWHEAT_H

#define WINTERWHEAT_BASE 8200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define WW_AUTUMN_PLOUGH        a_field->m_user[1]
#define WW_DECIDE_TO_HERB       a_field->m_user[2]
#define WW_DECIDE_TO_FI         a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  ww_start = 1, // Compulsory, must always be 1 (one).
  ww_sleep_all_day = WINTERWHEAT_BASE,
  ww_ferti_p1,
  ww_ferti_s1,
  ww_ferti_s2,
  ww_autumn_plough,
  ww_autumn_harrow,
  ww_stubble_harrow1,
  ww_autumn_sow,
  ww_autumn_roll,
  ww_ferti_p2,
  ww_herbicide1,
  ww_spring_roll,
  ww_herbicide2,
  ww_GR,
  ww_fungicide,
  ww_fungicide2,
  ww_insecticide1,
  ww_insecticide2,
  ww_insecticide3,
  ww_strigling1,
  ww_strigling2,
  ww_water1,
  ww_water2,
  ww_ferti_p3,
  ww_ferti_p4,
  ww_ferti_p5,
  ww_ferti_s3,
  ww_ferti_s4,
  ww_ferti_s5,
  ww_harvest,
  ww_straw_chopping,
  ww_hay_turning,
  ww_hay_baling,
  ww_stubble_harrow2,
  ww_grubning,
  ww_foobar
} WinterWheatToDo;


/**
\brief
WinterWheat class
\n
*/
/**
See WinterWheat.h::WinterWheatToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class WinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   WinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,10 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (ww_foobar - WINTERWHEAT_BASE);
	   m_base_elements_no = WINTERWHEAT_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
		   fmc_Others,	// zero element unused but must be here
		  fmc_Others,//ww_start = 1, // Compulsory, must always be 1 (one).
		  fmc_Others,//ww_sleep_all_day = WINTERWHEAT_BASE,
		  fmc_Fertilizer,//ww_ferti_p1,
		  fmc_Fertilizer,//ww_ferti_s1,
		  fmc_Fertilizer,//ww_ferti_s2,
		  fmc_Cultivation,//ww_autumn_plough,
		  fmc_Cultivation,//ww_autumn_harrow,
		  fmc_Cultivation,//ww_stubble_harrow1,
		  fmc_Others,//ww_autumn_sow,
		  fmc_Others,//ww_autumn_roll,
		  fmc_Fertilizer,//ww_ferti_p2,
		  fmc_Herbicide,//ww_herbicide1,
		  fmc_Others,//ww_spring_roll,
		  fmc_Herbicide,//ww_herbicide2,
		  fmc_Others,//ww_GR,
		  fmc_Fungicide,//ww_fungicide,
		  fmc_Fungicide,//ww_fungicide2,
		  fmc_Insecticide,//ww_insecticide1,
		  fmc_Insecticide,//ww_insecticide2,
		  fmc_Insecticide,//ww_insecticide3,
		  fmc_Cultivation,//ww_strigling1,
		  fmc_Cultivation,//ww_strigling2,
		  fmc_Watering,//ww_water1,
		  fmc_Watering,//ww_water2,
		  fmc_Fertilizer,//ww_ferti_p3,
		  fmc_Fertilizer,//ww_ferti_p4,
		  fmc_Fertilizer,//ww_ferti_p5,
		  fmc_Fertilizer,//ww_ferti_s3,
		  fmc_Fertilizer,//ww_ferti_s4,
		  fmc_Fertilizer,//ww_ferti_s5,
		  fmc_Harvest,//ww_harvest,
		  fmc_Others,//ww_straw_chopping,
		  fmc_Others,//ww_hay_turning,
		  fmc_Others,//ww_hay_baling,
		  fmc_Cultivation,//ww_stubble_harrow2,
		  fmc_Cultivation//ww_grubning,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // WINTERWHEAT_H

