//
// DK_OPotato.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OPotato_H
#define DK_OPotato_H

#define DK_OPO_BASE 69400
#define DK_OPO_FORCESPRING	a_field->m_user[1]
#define DK_OPO_SANDY	a_field->m_user[2]

typedef enum {
  dk_opo_start = 1, // Compulsory, start event must always be 1 (one).
  dk_opo_autumn_remove_straw = DK_OPO_BASE,
  dk_opo_spring_remove_straw,
  dk_opo_autumn_stoneburier,
  dk_opo_autumn_plough,
  dk_opo_spring_stoneburier, 
  dk_opo_spring_plough,
  dk_opo_deep_harrow1,
  dk_opo_ferti_s,
  dk_opo_ferti_p,
  dk_opo_ferti_sow,
  dk_opo_calcium_sand_s,
  dk_opo_calcium_sand_p,
  dk_opo_water1_s,
  dk_opo_water2_s,
  dk_opo_water3_s,
  dk_opo_water1_c,
  dk_opo_water2_c,
  dk_opo_water3_c,
  dk_opo_row_cultivation1,
  dk_opo_hilling_up1,
  dk_opo_row_cultivation2,
  dk_opo_hilling_up2,
  dk_opo_harrow,
  dk_opo_hilling_up3,
  dk_opo_row_cultivation3,
  dk_opo_hilling_up4,
  dk_opo_top_off,
  dk_opo_harvest,
  dk_opo_deep_harrow2,
  dk_opo_foobar
} DK_OPotatoToDo;



class DK_OPotato : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OPotato(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_opo_foobar - DK_OPO_BASE);
		m_base_elements_no = DK_OPO_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_opo_autumn_remove_straw = DK_OPO_BASE,
			fmc_Others, // dk_opo_spring_remove_straw,
			fmc_Cultivation, // dk_opo_autumn_stoneburier,
			fmc_Cultivation, // dk_opo_autumn_plough,
			fmc_Cultivation, // dk_opo_spring_stoneburier,
			fmc_Cultivation, //  dk_opo_spring_plough,
			fmc_Cultivation, //  dk_opo_deep_harrow1,
			fmc_Fertilizer, //  dk_opo_ferti_s,
			fmc_Fertilizer, //  dk_opo_ferti_p,
			fmc_Fertilizer, //  dk_opo_ferti_sow,
			fmc_Fertilizer, //  dk_opo_calcium_sand_s,
			fmc_Fertilizer, //  dk_opo_calcium_sand_p,
			fmc_Watering, //  dk_opo_water1,
			fmc_Watering, //  dk_opo_water2,
			fmc_Watering, //  dk_opo_water3,
			fmc_Watering, //  dk_opo_water1,
			fmc_Watering, //  dk_opo_water2,
			fmc_Watering, //  dk_opo_water3,
			fmc_Cultivation, //  dk_opo_row_cultivation1,
			fmc_Cultivation, //  dk_opo_hilling_up1,
			fmc_Cultivation, //  dk_opo_row_cultivation2,
			fmc_Cultivation, //  dk_opo_hilling_up2,
			fmc_Cultivation, //  dk_opo_harrow,
			fmc_Cultivation, //  dk_opo_hilling_up3,
			fmc_Cultivation, //  dk_opo_row_cultivation3,
			fmc_Cultivation, //  dk_opo_hilling_up4,
			fmc_Others, //  dk_opo_top_off,
			fmc_Harvest, //  dk_opo_harvest,
			fmc_Cultivation, //  dk_opo_deep_harrow2,
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};



#endif // DK_OPotato_H
