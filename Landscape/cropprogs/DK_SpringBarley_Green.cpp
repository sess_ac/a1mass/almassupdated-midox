//
// DK_SpringBarley_Green.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_SpringBarley_Green.h"
#include "math.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_springbarley_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_SpringBarley_Green::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
    TTypesOfVegetation l_tov = tov_DKSpringBarley_Green; // based on barley w. peas

    switch (m_ev->m_todo)
    {
    case dk_spg_start:
    {
        a_field->ClearManagementActionSum();
        DK_SPG_FORCESPRING = false;
        m_last_date = g_date->DayInYear(11, 7); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(9, 7); // last possible day of swathing in this case - this is in effect day before the earliest date that a following crop can use
        // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[1][1] = g_date->DayInYear(10, 7); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)
        flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 2)
        flexdates[2][1] = g_date->DayInYear(11, 7); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 2)

        // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
        int isSpring = 0;
        if (StartUpCrop(isSpring, flexdates, int(dk_spg_slurry1))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
        // OK, let's go.
        // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
        if (m_ev->m_forcespring) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_spg_slurry1, false);
            DK_SPG_FORCESPRING = true;
            break;
        }
        else SimpleEvent(d1, dk_spg_autumn_plough1, false);
        break;
    }
    break;

    // done if manye weeds, or waste plants from earlier catch crops 
    case dk_spg_autumn_plough1:
        if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
        {
            if (m_ev->m_lock || m_farm->DoIt_prob(0.85)) {
                if (!m_farm->AutumnPlough(m_field, 0.0,
                    g_date->DayInYear(1, 11) - g_date->DayInYear())) {
                    SimpleEvent(g_date->Date() + 1, dk_spg_autumn_plough1, true);
                    break;
                }
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_spg_slurry1, false);
        break;

    case dk_spg_slurry1:
        if (a_farm->IsStockFarmer()) //Stock Farmer
        {
            if (!m_farm->FA_Slurry(m_field, 0.0,
                g_date->DayInYear(15, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_spg_slurry1, true);
                break;
            }
            SimpleEvent(g_date->Date() + 1, dk_spg_spring_plough, false);
            break;
        }
        else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_spg_fertilizer, false);
        break;
    case dk_spg_fertilizer:
        if (!m_farm->FP_NPKS(m_field, 0.0,
            g_date->DayInYear(15, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_spg_fertilizer, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_spg_spring_plough, false);
        break;
    case dk_spg_spring_plough:
        if (!m_farm->SpringPlough(m_field, 0.0,
            g_date->DayInYear(20, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_spg_spring_plough, true);
            break;
        }
        SimpleEvent(g_date->Date(), dk_spg_spring_sow, false);
        break;
    case dk_spg_spring_sow:
        if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
            if (!m_farm->SpringSow(m_field, 0.0,
                g_date->DayInYear(30, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_spg_spring_sow, true);
                break;
            }
        } // 50% sow combined (cereal, legume and lay-out), 50% sow twice (cereal+legume and layout individually)
        SimpleEvent(g_date->Date(), dk_spg_spring_sow_lo, false);
        break;

    case dk_spg_spring_sow_lo:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
            if (!m_farm->SpringSow(m_field, 0.0,
                g_date->DayInYear(1, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_spg_spring_sow_lo, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 14, dk_spg_herbicide, false);
        break;

    case dk_spg_herbicide:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.80)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(1, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_spg_herbicide, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_spg_fungicide, false);
        break;

    case dk_spg_fungicide:
        if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
            if (!m_farm->FungicideTreat(m_field, 0.0,
                g_date->DayInYear(31, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_spg_fungicide, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_spg_water, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_spg_insecticide, false);
        break;

    case dk_spg_water:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
            if (!m_farm->Water(m_field, 0.0,
                g_date->DayInYear(10, 7) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_spg_water, true);
                break;
            }
        }
        break; // end of thread

    case dk_spg_insecticide:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(9, 7) - g_date->DayInYear();
            if (!cfg_pest_springbarley_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent(g_date->Date() + 1, dk_spg_insecticide, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(13, 6), dk_spg_swathing, false);
        break;

    case dk_spg_swathing:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
            if (!m_farm->Swathing(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_spg_swathing, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 2, dk_spg_harvest, false);
        break;

    case dk_spg_harvest:
        if (!m_farm->Harvest(a_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_spg_harvest, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_spg_straw_chopping, false);
        break;

    case dk_spg_straw_chopping:
        if (!m_farm->StrawChopping(a_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_spg_straw_chopping, true);
            break;
        }
        d1 = g_date->Date();
        if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_spg_wait, false);
            // Because we are ending harvest before 1.7 so we need to wait until the 1.7
            break;
        }
        else {
            done = true; // end of plan
        }
    case dk_spg_wait:
        done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "DK_SpringBarley_Green::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }
    return done;
}

