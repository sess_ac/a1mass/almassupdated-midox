//
// NorwegianSpringBarley.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef NorwegianSpringBarley_H
#define NorwegianSpringBarley_H

#define NO_SBARLEY_BASE 29200
#define NO_SB_DID_AUTUMN_PLOUGH m_field->m_user[0]

typedef enum {
  no_sb_start = 1, // Compulsory, start event must always be 1 (one).
  no_sb_autumn_plough = NO_SBARLEY_BASE,
  no_sb_spring_plough,
  no_sb_spring_harrow,
  no_sb_spring_sow,
  no_sb_spring_roll,
  no_sb_harvest,
  no_sb_haybailing,
  no_sb_foobar
} NO_SBToDo;



class NorwegianSpringBarley : public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  NorwegianSpringBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(2,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (no_sb_foobar - NO_SBARLEY_BASE);
	  m_base_elements_no = NO_SBARLEY_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here
		  fmc_Others,//no_sb_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation,//no_sb_autumn_plough = NO_SBARLEY_BASE,
		  fmc_Cultivation,//no_sb_spring_plough,
		  fmc_Cultivation,//no_sb_spring_harrow,
		  fmc_Others,//no_sb_spring_sow,
		  fmc_Others,//no_sb_spring_roll,
		  fmc_Harvest,//no_sb_harvest,
		  fmc_Others//no_sb_haybailing,

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // NorwegianSpringBarley_H
