/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>IRSpringOats.cpp This file contains the source for the IRSpringOats class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2022 \n
 \n
*/
//
// FI_SpringOats.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/IRSpringOats.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_IR_SpringOats_SkScrapes("IR_CROP_SO_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_springoats_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_SO_InsecticideDay;
extern CfgInt   cfg_SO_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool IRSpringOats::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case ir_so_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		IR_SO_MT = false; // 10% do minimum tillage
		IR_SO_CP = false; // 90% do conventional plough

		a_field->ClearManagementActionSum();

		// Record whether skylark scrapes are present and adjust flag accordingly
		if (cfg_IR_SpringOats_SkScrapes.value()) {
			a_field->m_skylarkscrapes = true;
		}
		else {
			a_field->m_skylarkscrapes = false;
		}
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(15, 8)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(15, 8)); // las possible day of hay baling
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		int d1;
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "IRSpringOats::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "IRSpringOats::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "IRSpringOats::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes (start woth the first event on the after winter time) - Date here is a suggestion (not stated in crop scheme!)
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 2), ir_so_spring_roll, false);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(15, 8);
		// OK, let's go.
		// LKM: Here we queue up the first event - 10% do minimum tillage (mt) stubble harrow, 90% do conventional plough (cp), where half (45%) do stubble harrow as well
		//
		if (a_ev->m_lock || a_farm->DoIt_prob(.10)) {
			SimpleEvent(d1, ir_so_stubble_harrow_mt, false);
			break;
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.90 / .90)) {
			SimpleEvent(d1, ir_so_stubble_harrow_cp, false);
			break;
		}
		break;
	}
	break;
	// This is the first real farm operation 
	case ir_so_stubble_harrow_mt:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_stubble_harrow_mt, true);
			break;
		}
		IR_SO_MT = true; // we need to remember who do mt
		SimpleEvent(g_date->Date(), ir_so_sow_cover_crop, false);
		break;

	case ir_so_stubble_harrow_cp: // 27% (30% of conventional plough) do this
		if (a_ev->m_lock || a_farm->DoIt_prob(.30)) {
			if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_so_stubble_harrow_cp, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), ir_so_sow_cover_crop, false);
		break;

	case ir_so_sow_cover_crop: // 25% sow cover crop and stubble cultivation, 75% leave natural regeneration to grow
		if (a_ev->m_lock || a_farm->DoIt_prob(.25)) {
			if (!a_farm->AutumnSow(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_so_sow_cover_crop, true);
				break;
			}
			SimpleEvent(g_date->Date(), ir_so_stubble_cultivator, false);
			break;
		}
		if (IR_SO_MT == true) { // minimal tillage w. CC
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 1) + 365, ir_so_herbicide1_mt, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_stubble_cultivator_mt, false); // cultivation thread - main thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_molluscicide, false); // mollusc thread
			if (a_farm->IsStockFarmer()) {
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_ferti_s1, false); // ferti stock thread
				break;
			}
			else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_ferti_p1, false); // ferti plant thread
			break;
		}
		else // conventional plough w. CC
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 1) + 365, ir_so_herbicide1_cp, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 1) + 365, ir_so_spring_plough_cp, false); // cultivation thread - main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_molluscicide, false); // mollusc thread
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_ferti_s1, false); // ferti stock thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_ferti_p1, false); // ferti plant thread
		break;

	case ir_so_stubble_cultivator:
		if (!a_farm->StubblePlough(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_stubble_cultivator, true);
			break;
		}
		if (IR_SO_MT == true) { // minimal tillage w/o CC
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 1) + 365, ir_so_herbicide1_mt, false); // herbi thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_stubble_cultivator_mt, false); // cultivation thread - main thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_molluscicide, false); // mollusc thread
			if (a_farm->IsStockFarmer()) {
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_ferti_s1, false); // ferti stock thread
				break;
			}
			else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_ferti_p1, false); // ferti plant thread
			break;
		}
		else // conventional plough w/o CC
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 1) + 365, ir_so_herbicide1_cp, false); // herbi thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 1) + 365, ir_so_spring_plough_cp, false); // cultivation thread - main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_molluscicide, false); // mollusc thread
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_ferti_s1, false); // ferti stock thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, ir_so_ferti_p1, false); // ferti plant thread
		break;

	case ir_so_herbicide1_mt: // 100% or mt farmers
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(5, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_herbicide1_mt, true);
			break;
		}
		break; // end of thread

	case ir_so_herbicide1_cp: // 35% (of 90% - all of CP) do this, rest (65% of 90%) skips this step
		if (a_ev->m_lock || a_farm->DoIt_prob(.35)) {
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(5, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_so_herbicide1_cp, true);
				break;
			}
		}
		break; // end of thread

	case ir_so_molluscicide:
		if (a_ev->m_lock || a_farm->DoIt_prob(.05)) {
			if (!a_farm->Molluscicide(a_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_so_molluscicide, true);
				break;
			}
		}
		break; // end of thread

	case ir_so_ferti_s1: // 15% of all farmers do 
		if (a_ev->m_lock || a_farm->DoIt_prob(.15)) {
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_so_ferti_s1, true);
				break;
			}
		}
		break; // end of thread

	case ir_so_ferti_p1: // 15% of all farmers do 
		if (a_ev->m_lock || a_farm->DoIt_prob(.15)) {
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_so_ferti_p1, true);
				break;
			}
		}
		break; // end of thread

	//CP do spring plough, MT do stubble cultivator
	case ir_so_stubble_cultivator_mt:
		if (!a_farm->StubblePlough(a_field, 0.0,
			g_date->DayInYear(5, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_stubble_cultivator_mt, true);
			break;
		}
		SimpleEvent(g_date->Date() + 2, ir_so_cultivation_sow_mt, false);
		break;

	case ir_so_cultivation_sow_mt:
		if (!a_farm->PreseedingCultivatorSow(a_field, 0.0,
			g_date->DayInYear(8, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_cultivation_sow_mt, true);
			break;
		}
		SimpleEvent(g_date->Date() + 2, ir_so_spring_roll, false);
		break;

	case ir_so_spring_plough_cp:
		if (!a_farm->SpringPlough(a_field, 0.0,
			g_date->DayInYear(5, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_spring_plough_cp, true);
			break;
		}
		SimpleEvent(g_date->Date(), ir_so_spring_sow_cp, false);
		break;

	case ir_so_spring_sow_cp:
		if (!a_farm->SpringSow(a_field, 0.0,
			g_date->DayInYear(5, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_spring_sow_cp, true);
			break;
		}
		SimpleEvent(g_date->Date(), ir_so_spring_harrow_cp, false);
		break;

	case ir_so_spring_harrow_cp:
		if (!a_farm->SpringHarrow(a_field, 0.0,
			g_date->DayInYear(5, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_spring_harrow_cp, true);
			break;
		}
		SimpleEvent(g_date->Date() + 2, ir_so_spring_roll, false);
		break;
		//CP and MT comes together for spring roll
	case ir_so_spring_roll:
		if (!a_farm->SpringRoll(a_field, 0.0,
			g_date->DayInYear(8, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_spring_roll, true);
			break;
		}
		//Here comes a fork of parallel events
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), ir_so_herbicide2, false); //herbi thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), ir_so_insecticide1, false); //insecti thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), ir_so_fungicide1, false); //fungi thread, main thread
		if (a_farm->IsStockFarmer())
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 3), ir_so_ferti_s2, false); //stock ferti thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 3), ir_so_ferti_p2, false); //plant ferti thread
		break;

	case ir_so_ferti_s2:
		if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(5, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_ferti_s2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), ir_so_ferti_s3, false);
		break;

	case ir_so_ferti_p2:
		if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(5, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_ferti_p2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 4), ir_so_ferti_p3, false);
		break;

	case ir_so_ferti_s3:
		if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_ferti_s3, true);
			break;
		}
		break; // end of thread

	case ir_so_ferti_p3:
		if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_ferti_p3, true);
			break;
		}
		break; // end of thread



	case ir_so_herbicide2:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_herbicide2, true);
			break;
		}
		break; // end of thread

	case ir_so_insecticide1:
		if (a_ev->m_lock || a_farm->DoIt_prob(.70)) {
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_so_insecticide1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), ir_so_insecticide2, false);
		break;

	case ir_so_insecticide2: // 60% will do it every 5 years if pest levels are high - suggests 12% do it
		if (a_ev->m_lock || a_farm->DoIt_prob(.12)) {
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, ir_so_insecticide2, true);
				break;
			}
		}
		break; // end of thread


	case ir_so_fungicide1:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_fungicide1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), ir_so_fungicide2, false);
		break;

	case ir_so_fungicide2:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_fungicide2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), ir_so_harvest, false);
		break;

	case ir_so_harvest:
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_harvest, true);
			break;
		}
		SimpleEvent(g_date->Date(), ir_so_hay_bailing, false);
		break;

	case ir_so_hay_bailing:
		if (!a_farm->HayBailing(a_field, 0.0, a_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, ir_so_hay_bailing, true);
			break;
		}
		done = true;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "IRSpringOats::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}