//
// FodderBeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FodderBeet_h
#define FodderBeet_h

#define FBeet_BASE 1800

#define FB_DID_HARROW   m_field->m_user[0]
#define FB_DID_NPKS_ONE m_field->m_user[1]
#define FB_DID_SLURRY   m_field->m_user[2]
#define FB_SOW_DATE     m_field->m_user[3]

#define FB_DID_ROW_TWO         m_field->m_user[0]
#define FB_DID_INSECT_ONE      m_field->m_user[1]
#define FB_DID_NPKS_TWO        m_field->m_user[2]
#define FB_DID_WATER_ONE       m_field->m_user[3]
#define FB_TRULY_DID_WATER_ONE m_field->m_user[4]
#define FB_DECIDE_TO_HERB		m_field->m_user[5]
#define FB_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  fb_start = 1, // Compulsory, start event must always be 1 (one).
  fb_autumn_plough = FBeet_BASE,
  fb_fertmanure,
  fb_spring_plough,
  fb_start_threads_one,
  fb_spring_harrow,
  fb_fertnpks_one,
  fb_fertslurry,
  fb_spring_sow,
  fb_spring_roll,
  fb_herbicide_one,
  fb_herbicide_two,
  fb_herbicide_three,
  fb_row_cultivation_one,
  fb_row_cultivation_two,
  fb_insecticide_one,
  fb_fertnpks_two,
  fb_water_one,
  fb_insecticide_two,
  fb_water_two,
  fb_harvest,
  fb_foobar,
} FBToDo;



class FodderBeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FodderBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(11,10); // Was 1,10 - changed just for sake of getting year on year beet
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (fb_foobar - FBeet_BASE);
	  m_base_elements_no = FBeet_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fb_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation,	//	  fb_autumn_plough = FBeet_BASE,
			fmc_Fertilizer,	//	  fb_fertmanure,
			fmc_Cultivation,	//	  fb_spring_plough,
			fmc_Others,	//	  fb_start_threads_one,
			fmc_Cultivation,	//	  fb_spring_harrow,
			fmc_Fertilizer,	//	  fb_fertnpks_one,
			fmc_Fertilizer,	//	  fb_fertslurry,
			fmc_Others,	//	  fb_spring_sow,
			fmc_Cultivation,	//	  fb_spring_roll,
			fmc_Herbicide,	//	  fb_herbicide_one,
			fmc_Herbicide,	//	  fb_herbicide_two,
			fmc_Herbicide,	//	  fb_herbicide_three,
			fmc_Cultivation,	//	  fb_row_cultivation_one,
			fmc_Cultivation,	//	  fb_row_cultivation_two,
			fmc_Insecticide,	//	  fb_insecticide_one,
			fmc_Fertilizer,	//	  fb_fertnpks_two,
			fmc_Watering,	//	  fb_water_one,
			fmc_Insecticide,	//	  fb_insecticide_two,
			fmc_Watering,	//	  fb_water_two,
			fmc_Harvest	//	  fb_harvest,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // FodderBeet_h
