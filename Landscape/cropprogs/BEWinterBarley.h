/**
\file
\brief
<B>BEWinterbarley.h This file contains the headers for the Winterbarley class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BEWinterbarley.h
//


#ifndef BEWINTERBARLEY_H
#define BEWINTERBARLEY_H

#define BEWINTERBARLEY_BASE 25400
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing winter barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_wb_start = 1, // Compulsory, must always be 1 (one).
	BE_wb_sleep_all_day = BEWINTERBARLEY_BASE,
	BE_wb_stubble_cultivator,
	BE_wb_herbicide1,
	BE_wb_mole_plough,
	BE_wb_autumn_plough,
	BE_wb_preseeding_cultivator_sow,
	BE_wb_herbicide2,
	BE_wb_herbicide3,
	BE_wb_ferti_p1, // slurry
	BE_wb_ferti_s1,
	BE_wb_ferti_p3, // NI
	BE_wb_ferti_s3,
	BE_wb_ferti_p4, // NII
	BE_wb_ferti_s4,
	BE_wb_ferti_p5, // NIII
	BE_wb_ferti_s5,
	BE_wb_herbicide4, 
	BE_wb_fungicide1,
	BE_wb_fungicide2,
	BE_wb_insecticide2,
	BE_wb_growth_regulator1,
	BE_wb_growth_regulator2,
	BE_wb_harvest,
	BE_wb_straw_chopping,
	BE_wb_hay_bailing,
} BEWinterBarley1ToDo;


/**
\brief
BEWinterbarley class
\n
*/
/**
See BEWinterbarley.h::BEWinterbarleyToDo for a complete list of all possible events triggered codes by the winter barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEWinterBarley: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEWinterBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,10 );
   }
};

#endif // BEWINTERbarley_H

