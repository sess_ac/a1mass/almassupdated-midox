/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disspsaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disspsaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSPSUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSPSAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSPSUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSPSUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKMaizeSilage_H
#define DKMaizeSilage_H

#define DK_MS_BASE 63900

#define DK_MS_RC_CC m_field->m_user[0]
#define DK_MS_FORCESPRING	a_field->m_user[1]

typedef enum {
	dk_ms_start = 1, // Compulsory, start event must always be 1 (one).
	dk_ms_harvest = DK_MS_BASE,
	dk_ms_autumn_plough,
	dk_ms_herbicide1,
	dk_ms_spring_plough,
	dk_ms_spring_harrow1,
	dk_ms_spring_harrow2,
	dk_ms_cultivator_sow,
	dk_ms_sow,
	dk_ms_sow_cc,
	dk_ms_ferti_s1,
	dk_ms_ferti_p1,
	dk_ms_row_cultivation_weeds,
	dk_ms_row_cultivation_cc,
	dk_ms_water,
	dk_ms_herbicide2,
	dk_ms_herbicide3,
	dk_ms_fungicide,
	dk_ms_foobar,
} DK_MaizeSilageToDo;



class DK_MaizeSilage : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_MaizeSilage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(30, 11);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_ms_foobar - DK_MS_BASE);
		m_base_elements_no = DK_MS_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_ms_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_ms_harvest = DK_MS_BASE,
			  fmc_Cultivation,	//	  dk_ms_autumn_plough,
			  fmc_Herbicide,	//	  dk_ms_herbicide1,
			  fmc_Cultivation,	//	  dk_ms_spring_plough,
			  fmc_Cultivation,	//	  dk_ms_spring_harrow1,
			  fmc_Cultivation,	//	  dk_ms_spring_harrow2,
			  fmc_Cultivation,	//	  dk_ms_cultivator_sow,
			  fmc_Others,	//	  dk_ms_sow,
			  fmc_Others,	//	  dk_ms_sow_cc,
			  fmc_Fertilizer,	//	  dk_ms_ferti_s1,
			  fmc_Fertilizer,	//	  dk_ms_ferti_p1,
			  fmc_Cultivation,	//	  dk_ms_row_cultivation_weeds,
			  fmc_Cultivation,	//	  dk_ms_row_cultivation_cc,
			  fmc_Watering, // dk_ms_water,
			  fmc_Herbicide, // dk_ms_herbicide2,
			  fmc_Herbicide, // dk_ms_herbicide3,
			  fmc_Fungicide, // dk_ms_fungicide,

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};


#endif // DK_MaizeSilage_H

