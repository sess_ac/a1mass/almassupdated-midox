//
// DK_Sugarbeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_SugarBeet_h
#define DK_SugarBeet_h


#define DK_SB_BASE 60200

#define DK_SBE_FORCESPRING	a_field->m_user[1]

typedef enum {
  dk_sbe_start = 1, // Compulsory, start event must always be 1 (one).
  dk_sbe_harvest = DK_SB_BASE,
  dk_sbe_autumn_plough,
  dk_sbe_molluscicide1,
  dk_sbe_harrow,
  dk_sbe_sow,
  dk_sbe_herbicide1,
  dk_sbe_herbicide2,
  dk_sbe_herbicide3,
  dk_sbe_insecticide1,
  dk_sbe_herbicide4,
  dk_sbe_herbicide5,
  dk_sbe_row_cultivation,
  dk_sbe_insecticide2,
  dk_sbe_fungicide1,
  dk_sbe_fungicide2,
  dk_sbe_foobar,
} DK_SugarBeetToDo;



class DK_SugarBeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_SugarBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(30,11); // 
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_sbe_foobar - DK_SB_BASE);
	  m_base_elements_no = DK_SB_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_sbe_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_sbe_harvest = DK_SB_BASE,
			fmc_Cultivation,	//	  dk_sbe_autumn_plough,
			fmc_Others,	//	  dk_sbe_molluscicide1,
			fmc_Cultivation,	//	  dk_sbe_harrow,
			fmc_Fertilizer,	//	  dk_sbe_sow,
			fmc_Herbicide,	//	  dk_sbe_herbicide1,
			fmc_Herbicide,	//	  dk_sbe_herbicide2,
			fmc_Herbicide,	//	  dk_sbe_herbicide3,
			fmc_Insecticide,	//	  dk_sbe_insecticide1,
			fmc_Herbicide,	//	  dk_sbe_herbicide4,
			fmc_Herbicide,	//	  dk_sbe_herbicide5,
			fmc_Cultivation,	//	  dk_sbe_row_cultivation,
			fmc_Insecticide,	//	  dk_sbe_insecticide2,
			fmc_Fungicide,	//	  dk_sbe_fungicide1,
			fmc_Fungicide,	//	  dk_sbe_fungicide2,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_SugarBeet_h
