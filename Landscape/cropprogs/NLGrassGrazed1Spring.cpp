/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLGrassGrazed1Spring.cpp This file contains the source for the NLGrassGrazed1Spring class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// NLGrassGrazed1Spring.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLGrassGrazed1Spring.h"

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional TemporalGrassGrazed1Spring.
*/
bool NLGrassGrazed1Spring::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLGrassGrazed1Spring;
	int l_nextcropstartdate;
	/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case nl_gg1s_start:
	{
		NL_GG1S_CUT_DATE = 0;
		NL_GG1S_WATER_DATE = 0;
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(31, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(10, 10); // first possible day of finishing harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(31, 10);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(nl_gg1s_spring_sow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(28, 2) + isSpring;
		// OK, let's go.
		SimpleEvent_(d1, nl_gg1s_preseeding_cultivator, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case nl_gg1s_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_spring_sow, false, m_farm, m_field);
		break;
	case nl_gg1s_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_spring_sow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_gg1s_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, nl_gg1s_ferti_p2, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p2:
		if (m_farm->DoIt_prob(0.90)) {
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->DoIt_prob(0.50)) {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), nl_gg1s_cut_to_silage1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4), nl_gg1s_cattle_out, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_s2:
		if (m_farm->DoIt_prob(0.90)) {
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->DoIt_prob(0.50)) {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), nl_gg1s_cut_to_silage1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4), nl_gg1s_cattle_out, false, m_farm, m_field);
		break;
	case nl_gg1s_cut_to_silage1:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < NL_GG1S_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage1, true, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage1, true, m_farm, m_field);
				break;
			}
			NL_GG1S_CUT_DATE = g_date->DayInYear();
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s3, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p3, false, m_farm, m_field);
			// Start water thread
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_watering, false, m_farm, m_field);
			break;
		}
		break;

	case nl_gg1s_ferti_s3:
		if (!m_farm->FA_Slurry(m_field, 0.0, (NL_GG1S_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s4, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p3:
		if (!m_farm->FP_Slurry(m_field, 0.0, (NL_GG1S_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p4, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage2, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage2, false, m_farm, m_field);
		break;
	case nl_gg1s_cut_to_silage2:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < NL_GG1S_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage2, true, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage2, true, m_farm, m_field);
				break;
			}
			NL_GG1S_CUT_DATE = g_date->DayInYear();
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s5, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p5, false, m_farm, m_field);
			break;
		}
		break;
	case nl_gg1s_ferti_s5:
		if (!m_farm->FA_Slurry(m_field, 0.0, (NL_GG1S_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s6, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p5:
		if (!m_farm->FP_Slurry(m_field, 0.0, (NL_GG1S_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p6, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_s6:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s6, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage3, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p6:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p6, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage3, false, m_farm, m_field);
		break;
	case nl_gg1s_cut_to_silage3:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < NL_GG1S_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage3, true, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage3, true, m_farm, m_field);
				break;
			}
			NL_GG1S_CUT_DATE = g_date->DayInYear();
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s7, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p7, false, m_farm, m_field);
			break;
		}
		break;

	case nl_gg1s_ferti_s7:
		if (!m_farm->FA_Slurry(m_field, 0.0, (NL_GG1S_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s7, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s8, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p7:
		if (!m_farm->FP_Slurry(m_field, 0.0, (NL_GG1S_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p7, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p8, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_s8:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s8, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage4, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p8:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p8, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage4, false, m_farm, m_field);
		break;
	case nl_gg1s_cut_to_silage4:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < NL_GG1S_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage4, true, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage4, true, m_farm, m_field);
				break;
			}
			NL_GG1S_CUT_DATE = g_date->DayInYear();
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s9, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p9, false, m_farm, m_field);
			break;
		}
		break;
	case nl_gg1s_ferti_s9:
		if (!m_farm->FA_Slurry(m_field, 0.0, (NL_GG1S_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s9, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s10, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p9:
		if (!m_farm->FP_Slurry(m_field, 0.0, (NL_GG1S_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p9, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p10, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_s10:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(5, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s10, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage5, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p10:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(5, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p10, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage5, false, m_farm, m_field);
		break;
	case nl_gg1s_cut_to_silage5:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < NL_GG1S_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage5, true, m_farm, m_field);
			break;
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
			{
				if (!m_farm->CutToSilage(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage5, true, m_farm, m_field);
					break;
				}
				NL_GG1S_CUT_DATE = g_date->DayInYear();
			}
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 11), nl_gg1s_ferti_s12, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 11), nl_gg1s_ferti_p12, false, m_farm, m_field);
			break;
		}
		break;
	case nl_gg1s_ferti_s12:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s12, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		break;
	case nl_gg1s_ferti_p12:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p12, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		break;
	case nl_gg1s_cattle_out:
		if (!m_farm->CattleOut(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_cattle_out, true, m_farm, m_field);
			break;
		}
		// Keep them out there
		SimpleEvent_(g_date->Date(), nl_gg1s_cattle_is_out, false, m_farm, m_field);
		break;

	case nl_gg1s_cattle_is_out:    // Keep the cattle out there
								   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear(), g_date->DayInYear(31, 8))) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_cattle_is_out, false, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s11, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p11, false, m_farm, m_field);
		break;

	case nl_gg1s_ferti_s11:
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(5, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_s11, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage6, false, m_farm, m_field);
		break;
	case nl_gg1s_ferti_p11:
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(5, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_ferti_p11, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 21, nl_gg1s_cut_to_silage6, false, m_farm, m_field);
		break;
	case nl_gg1s_cut_to_silage6:
		if (!m_farm->CutToSilage(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_gg1s_cut_to_silage6, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 11), nl_gg1s_ferti_s12, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 11), nl_gg1s_ferti_p12, false, m_farm, m_field);
		break;
	case nl_gg1s_watering:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
		{
			if (g_date->Date() < NL_GG1S_CUT_DATE + 3) {
				// Too close to silage cutting, so try again tomorrow.
				SimpleEvent_(g_date->Date() + 1, nl_gg1s_watering, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_gg1s_watering, true, m_farm, m_field);
					break;
				}
				NL_GG1S_WATER_DATE = g_date->Date();
			}
		}
		// End of thread
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLGrassGrazed1Spring::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}