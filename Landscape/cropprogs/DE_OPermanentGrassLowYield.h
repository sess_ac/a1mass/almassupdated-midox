/**
\file
\brief
<B>DE_OPermanentGrassLowYield.h This file contains the headers for the OPermanentGrassLowYield class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// OPermanentGrassLowYield.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

//EZ: This is the same as conventional DEPermanentGrassLowYield

#ifndef DE_OPERMANENTGRASSLOWYIELD_H
#define DE_OPERMANENTGRASSLOWYIELD_H

#define DE_OPGLY_BASE 39800

/** Below is the list of things that a farmer can do if he is growing permanent grass low yield, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_opgly_start = 1, // Compulsory, must always be 1 (one).
	de_opgly_sleep_all_day = DE_OPGLY_BASE,
	de_opgly_strigling,
	de_opgly_cattle_out1,//Out means outside the barns. Out in the field to eat grass.
	de_opgly_cattle_is_out1,//Time that the catle is in the field.
	de_opgly_cattle_out2,
	de_opgly_cattle_is_out2,
	de_opgly_cutting1,
	de_opgly_wait,
	de_opgly_foobar,
} DE_OPermanentGrassLowYieldToDo;



class DE_OPermanentGrassLowYield : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_OPermanentGrassLowYield(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(28, 2);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_opgly_foobar - DE_OPGLY_BASE);
		m_base_elements_no = DE_OPGLY_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	de_opgly_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	de_opgly_sleep_all_day = DE_OPGLY_BASE,
			fmc_Cultivation, // de_opgly_strigling
			fmc_Grazing,	//	de_opgly_cattle_out1,//Out means outside the barns. Out in the field to eat grass.
			fmc_Grazing,	//	de_opgly_cattle_is_out1,//Time that the catle is in the field.
			fmc_Grazing,	//	de_opgly_cattle_out2,
			fmc_Grazing,	//	de_opgly_cattle_is_out2,
			fmc_Others,	//	de_opgly_cutting1,
			fmc_Others,	//	de_opgly_wait,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_OPERMAENTGRASSLOWYIELD_H

