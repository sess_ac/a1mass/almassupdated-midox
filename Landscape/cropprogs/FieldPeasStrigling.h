//
// FieldPeasStrigling.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FieldPeasStrigling_H
#define FieldPeasStrigling_H

#define FPEASS_BASE 5500

#define FPEASS_INSECT_DATE m_field->m_user[0]
#define FPEASS_FUNGI_DATE  m_field->m_user[1]
#define FPEASS_WATER_DATE  m_field->m_user[2]

typedef enum {
  fpst_start = 1, // Compulsory, start event must always be 1 (one).
  fpst_harvest= FPEASS_BASE,
  fpst_autumn_plough,
  fpst_spring_plough,
  fpst_spring_harrow,
  fpst_fertmanure_plant,
  fpst_fertmanure_stock,
  fpst_spring_sow,
  fpst_spring_roll,
  fpst_strigling1,
  fpst_strigling2,
  fpst_strigling3,
  fpst_strigling4,
  fpst_insecticide,
  fpst_fungicide,
  fpst_water_one,
  fpst_water_two,
  fpst_growth_reg,
  fpst_straw_chopping,
  fpst_foobar,
} FieldPeasSToDo;



class FieldPeasStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FieldPeasStrigling(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,10);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (fpst_foobar - FPEASS_BASE);
	  m_base_elements_no = FPEASS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fpst_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  fpst_harvest= FPEASS_BASE,
			fmc_Cultivation,	//	  fpst_autumn_plough,
			fmc_Cultivation,	//	  fpst_spring_plough,
			fmc_Cultivation,	//	  fpst_spring_harrow,
			fmc_Fertilizer,	//	  fpst_fertmanure_plant,
			fmc_Fertilizer,	//	  fpst_fertmanure_stock,
			fmc_Others,	//	  fpst_spring_sow,
			fmc_Cultivation,	//	  fpst_spring_roll,
			fmc_Cultivation,	//	  fpst_strigling1,
			fmc_Cultivation,	//	  fpst_strigling2,
			fmc_Cultivation,	//	  fpst_strigling3,
			fmc_Cultivation,	//	  fpst_strigling4,
			fmc_Insecticide,	//	  fpst_insecticide,
			fmc_Fungicide,	//	  fpst_fungicide,
			fmc_Watering,	//	  fpst_water_one,
			fmc_Watering,	//	  fpst_water_two,
			fmc_Others,	//	  fpst_growth_reg,
			fmc_Others	//	  fpst_straw_chopping,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }

};

#endif // FieldPeas_H
