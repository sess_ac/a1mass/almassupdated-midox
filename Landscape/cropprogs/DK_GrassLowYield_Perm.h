//
// DK_GrassLowYield_Perm.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_GrassLowYield_Perm_H
#define DK_GrassLowYield_Perm_H

#define DK_GLYP_BASE 67300
#define DK_GLYP_CUT_DATE  m_field->m_user[0]

typedef enum {
  dk_glyp_start = 1, // Compulsory, start event must always be 1 (one).
  dk_glyp_cut_to_hay = DK_GLYP_BASE,
  dk_glyp_cattle_out1,
  dk_glyp_cattle_out2,
  dk_glyp_cattle_is_out,
  dk_glyp_cut_weeds,
  dk_glyp_herbicide,
  dk_glyp_raking1,
  dk_glyp_raking2,
  dk_glyp_compress_straw,
  dk_glyp_wait, 
  dk_glyp_foobar
} DK_GrassLowYield_PermToDo;



class DK_GrassLowYield_Perm: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_GrassLowYield_Perm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
        m_first_date=g_date->DayInYear(1,12);
        SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_glyp_foobar - DK_GLYP_BASE);
	  m_base_elements_no = DK_GLYP_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		fmc_Others, // zero element unused but must be here
		fmc_Cutting, // dk_glyp_cut_to_hay = GLYP_BASE,
		fmc_Grazing, // dk_glyp_cattle_out1,
		fmc_Grazing, // dk_glyp_cattle_out2,
		fmc_Grazing, // dk_glyp_cattle_is_out,
		fmc_Cutting, // dk_glyp_cut_weeds,
		fmc_Herbicide, // dk_glyp_herbicide,
		fmc_Others, // dk_glyp_raking1,
		fmc_Others, // dk_glyp_raking2,
		fmc_Others, // dk_glyp_compress_straw,
		fmc_Others // dk_glyp_wait,
		// No foobar entry
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }

};

#endif // DK_GrassLowYield_Perm_H
