/**
\file
\brief
<B>DK_ChristmasTrees_Perm.h This file contains the headers for the DK_ChristmasTrees_Perm class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of January 2023 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_ChristmasTrees_Perm1.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_CHRISTMASTREES_PERM_H
#define DK_CHRISTMASTREES_PERM_H

#define DK_CTP_BASE 62900
/**
\brief A flag used to indicate year
*/
#define DK_CTP_YEAR	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing DK_ChristmasTrees_Perm1_autumn, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_ctp_start = 1, // Compulsory, must always be 1 (one).
	dk_ctp_sleep_all_day = DK_CTP_BASE,
	dk_ctp_plough1_autumn, //establishment
	dk_ctp_depth_plough_autumn,
	dk_ctp_plough2_autumn,
	dk_ctp_stubble_harrow_autumn,
	dk_ctp_sow_cover_crop_autumn,
	dk_ctp_plant_trees_autumn,
	dk_ctp_row_cultivation_autumn,
	dk_ctp_harrow_autumn,
	dk_ctp_crush_trees, // 2nd establishment
	dk_ctp_sow_inter_crops,
	dk_ctp_harrow,
	dk_ctp_herbicide,
	dk_ctp_sow_catch_crop1,
	dk_ctp_plant_trees,
	dk_ctp_ferti_clay_s2, //after establishment (y2-5)
	dk_ctp_ferti_sand_s2,
	dk_ctp_ferti_clay_p2,
	dk_ctp_ferti_sand_p2,
	dk_ctp_manual_weeding_2,
	dk_ctp_herbicide1_2,
	dk_ctp_herbicide2_2,
	dk_ctp_herbicide3_2,
	dk_ctp_ferti_clay_s3_4,
	dk_ctp_ferti_sand_s3_4,
	dk_ctp_npk_clay_s3_4,
	dk_ctp_npk_sand1_s3_4,
	dk_ctp_npk_sand2_s3_4,
	dk_ctp_ferti_clay_p3_4,
	dk_ctp_ferti_sand_p3_4,
	dk_ctp_npk_clay_p3_4,
	dk_ctp_npk_sand1_p3_4,
	dk_ctp_npk_sand2_p3_4,
	dk_ctp_manual_weeding_3_4,
	dk_ctp_herbicide1_3_4,
	dk_ctp_herbicide2_3_4,
	dk_ctp_herbicide3_3_4,
	dk_ctp_manual_cutting_3_4,
	dk_ctp_grazing_3_4,
	dk_ctp_pig_is_out_3_4,
	dk_ctp_sow_catch_crop2,
	dk_ctp_herbicide4_3_4,
	dk_ctp_npk_s5,
	dk_ctp_npk_sand_s5,
	dk_ctp_npk_p5,
	dk_ctp_npk_sand_p5,
	dk_ctp_grazing_5,
	dk_ctp_pig_is_out_5,
	dk_ctp_manual_weeding_5,
	dk_ctp_herbicide1_5,
	dk_ctp_herbicide2_5,
	dk_ctp_herbicide3_5,
	dk_ctp_manual_cutting_5,
	dk_ctp_npk1_s6, // after establishment (y6-10)
	dk_ctp_npk2_s6,
	dk_ctp_npk1_p6,
	dk_ctp_npk2_p6,
	dk_ctp_calcium_s6,
	dk_ctp_calcium_p6,
	dk_ctp_grazing_6,
	dk_ctp_pig_is_out_6,
	dk_ctp_manual_weeding_6,
	dk_ctp_herbicide1_6,
	dk_ctp_herbicide2_6,
	dk_ctp_herbicide3_6,
	dk_ctp_manual_cutting_6,
	dk_ctp_npk1_s7_8,
	dk_ctp_npk2_s7_8,
	dk_ctp_calcium_s7_8,
	dk_ctp_npk1_p7_8,
	dk_ctp_npk2_p7_8,
	dk_ctp_calcium_p7_8,
	dk_ctp_grazing_7_8,
	dk_ctp_pig_is_out_7_8,
	dk_ctp_manual_weeding_7_8,
	dk_ctp_herbicide1_7_8,
	dk_ctp_herbicide2_7_8,
	dk_ctp_herbicide3_7_8,
	dk_ctp_manual_cutting_7_8,
	dk_ctp_manual_cutting2_7_8,
	dk_ctp_npk1_s9_10,
	dk_ctp_npk2_s9_10,
	dk_ctp_calcium_s9_10,
	dk_ctp_npk1_p9_10,
	dk_ctp_npk2_p9_10,
	dk_ctp_calcium_p9_10,
	dk_ctp_sow_catch_crop3,
	dk_ctp_herbicide4_9_10,
	dk_ctp_manual_weeding_9_10,
	dk_ctp_herbicide1_9_10,
	dk_ctp_herbicide2_9_10,
	dk_ctp_herbicide3_9_10,
	dk_ctp_npk1_s11, // after establishment (y11 - harvest year)
	dk_ctp_npk2_s11,
	dk_ctp_calcium_s11,
	dk_ctp_npk1_p11,
	dk_ctp_npk2_p11,
	dk_ctp_calcium_p11,
	dk_ctp_manual_weeding_11,
	dk_ctp_herbicide1,
	dk_ctp_herbicide2,
	dk_ctp_herbicide3,
	dk_ctp_sow_catch_crop_11,
	dk_ctp_manual_cutting_11,
	dk_ctp_harvest,
	dk_ctp_wait,
	dk_ctp_foobar,

} DK_ChristmasTrees_PermToDo;


/**
\brief
DK_ChristmasTrees_Perm class
\n
*/
/**
See DK_ChristmasTrees_Perm.h::DK_ChristmasTrees_PermToDo for a complete list of all possible events triggered codes by the DK_ChristmasTrees_Perm management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_ChristmasTrees_Perm : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_ChristmasTrees_Perm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(10, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_ctp_foobar - DK_CTP_BASE);
		m_base_elements_no = DK_CTP_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
		fmc_Others,	// zero element unused but must be here	
		fmc_Others,	//	dk_ctp_start = 1, // Compulsory, must always be 1 (one).
		fmc_Others,	//	dk_ctp_sleep_all_day = DK_CTP_BASE,
		fmc_Cultivation,	//	dk_ctp_plough1_autumn,
		fmc_Cultivation,	//	dk_ctp_depth_plough_autumn,
		fmc_Cultivation,	//	dk_ctp_plough2_autumn,
		fmc_Cultivation,	//	dk_ctp_stubble_harrow_autumn,
		fmc_Others,	//	dk_ctp_sow_cover_crop_autumn,
		fmc_Others,	//	dk_ctp_plant_trees_autumn,
		fmc_Cultivation,	//	dk_ctp_row_cultivation_autumn,
		fmc_Cultivation,	//	dk_ctp_harrow_autumn,
		fmc_Cutting, //dk_ctp2e_crush_trees,
		fmc_Others, //dk_ctp2e_sow_inter_crops,
		fmc_Cultivation, // dk_ctp2e_harrow,
		fmc_Herbicide, //dk_ctp2e_herbicide,
		fmc_Others, // dk_ctp2e_sow_catch_crops,
		fmc_Others, // dk_ctp2e_plant_trees,
		fmc_Fertilizer, //dk_ctp2_ferti_clay_s2,
		fmc_Fertilizer, //dk_ctp2_ferti_sand_s2,
		fmc_Fertilizer, //dk_ctp2_ferti_clay_p2,
		fmc_Fertilizer, //dk_ctp2_ferti_sand_p2,
		fmc_Cultivation, //dk_ctp2_manual_weeding_2,
		fmc_Herbicide, //dk_ctp2_herbicide1_2,
		fmc_Herbicide, //dk_ctp2_herbicide2_2,
		fmc_Herbicide, //dk_ctp2_herbicide3_2,
		fmc_Fertilizer, //dk_ctp2_ferti_clay_s3_4,
		fmc_Fertilizer, //dk_ctp2_ferti_sand_s3_4,
		fmc_Fertilizer, //dk_ctp2_npk_clay_s3_4,
		fmc_Fertilizer, //dk_ctp2_npk_sand1_s3_4,
		fmc_Fertilizer, //dk_ctp2_npk_sand2_s3_4,
		fmc_Fertilizer, //dk_ctp2_ferti_clay_p3_4,
		fmc_Fertilizer, //dk_ctp2_ferti_sand_p3_4,
		fmc_Fertilizer, //dk_ctp2_npk_clay_p3_4,
		fmc_Fertilizer, //dk_ctp2_npk_sand1_p3_4,
		fmc_Fertilizer, //dk_ctp2_npk_sand2_p3_4,
		fmc_Cultivation, //dk_ctp2_manual_weeding_3_4,
		fmc_Herbicide, //dk_ctp2_herbicide1_3_4,
		fmc_Herbicide, //dk_ctp2_herbicide2_3_4,
		fmc_Herbicide, //dk_ctp2_herbicide3_3_4,
		fmc_Cutting, // dk_ctp2_manual_cutting_3_4
		fmc_Grazing, //dk_ctp2_grazing_3_4,
		fmc_Grazing, //dk_ctp2_pig_is_out_3_4,
		fmc_Others, //dk_ctp2_sow_catch_crop,
		fmc_Herbicide, //dk_ctp2_herbicide4,
		fmc_Fertilizer, //dk_ctp2_npk_s5,
		fmc_Fertilizer, //dk_ctp2_npk_sand_s5,
		fmc_Fertilizer, //dk_ctp2_npk_p5,
		fmc_Fertilizer, //dk_ctp2_npk_sand_p5,
		fmc_Grazing, //dk_ctp2_grazing_5,
		fmc_Grazing, //dk_ctp2_pig_is_out_5,
		fmc_Cultivation, //dk_ctp2_manual_weeding_5,
		fmc_Herbicide, //dk_ctp2_herbicide1_5,
		fmc_Herbicide, //dk_ctp2_herbicide2_5,
		fmc_Herbicide, //dk_ctp2_herbicide3_5,
		fmc_Cutting, //dk_ctp2_manual_cutting_5,
		fmc_Fertilizer, //dk_ctp3_npk1_s6,
		fmc_Fertilizer, //dk_ctp3_npk2_s6,
		fmc_Fertilizer, //dk_ctp3_calcium_s6,
		fmc_Fertilizer, //dk_ctp3_npk1_p6,
		fmc_Fertilizer, //dk_ctp3_npk2_p6,
		fmc_Fertilizer, //dk_ctp3_calcium_p6,
		fmc_Grazing, //dk_ctp3_grazing_6,
		fmc_Grazing, //dk_ctp3_pig_is_out_6,
		fmc_Cultivation, //dk_ctp3_manual_weeding_6,
		fmc_Herbicide, //dk_ctp3_herbicide1_6,
		fmc_Herbicide, //dk_ctp3_herbicide2_6,
		fmc_Herbicide, //dk_ctp3_herbicide3_6,
		fmc_Cutting, //dk_ctp3_manual_cutting_6,
		fmc_Fertilizer, //dk_ctp3_npk1_s7_8,
		fmc_Fertilizer, //dk_ctp3_npk2_s7_8,
		fmc_Fertilizer, //dk_ctp3_calcium_s7_8,
		fmc_Fertilizer, //dk_ctp3_npk1_p7_8,
		fmc_Fertilizer, //dk_ctp3_npk2_p7_8,
		fmc_Fertilizer, //dk_ctp3_calcium_p7_8,
		fmc_Grazing, //dk_ctp3_grazing_7_8,
		fmc_Grazing, //dk_ctp3_pig_is_out_7_8,
		fmc_Cultivation, //dk_ctp3_manual_weeding_7_8,
		fmc_Herbicide, //dk_ctp3_herbicide1_7_8,
		fmc_Herbicide, //dk_ctp3_herbicide2_7_8,
		fmc_Herbicide, //dk_ctp3_herbicide3_7_8,
		fmc_Cutting, //dk_ctp3_manual_cutting_7_8,
		fmc_Cutting, //dk_ctp3_manual_cutting2_7_8,
		fmc_Fertilizer, //dk_ctp3_npk1_s9_10,
		fmc_Fertilizer, //dk_ctp3_npk2_s9_10,
		fmc_Fertilizer, //dk_ctp3_calcium_s9_10,
		fmc_Fertilizer, //dk_ctp3_npk1_p9_10,
		fmc_Fertilizer, //dk_ctp3_npk2_p9_10,
		fmc_Fertilizer, //dk_ctp3_calcium_p9_10,
		fmc_Others, //	dk_ctp3_sow_catch_crop,
		fmc_Herbicide, //dk_ctp3_herbicide4,
		fmc_Cultivation, //	dk_ctp3_manual_weeding_9_10,
		fmc_Herbicide, //dk_ctp3_herbicide1_9_10,
		fmc_Herbicide, //dk_ctp3_herbicide2_9_10,
		fmc_Herbicide, //dk_ctp3_herbicide3_9_10,
		fmc_Fertilizer, //dk_ctp2_npk1_s11,
		fmc_Fertilizer, //dk_ctp2_npk2_s11,
		fmc_Fertilizer, //dk_ctp2_calcium_s11,
		fmc_Fertilizer, //dk_ctp2_npk1_p11,
		fmc_Fertilizer, //dk_ctp2_npk2_p11,
		fmc_Fertilizer, //dk_ctp2_calcium_p11,
		fmc_Cultivation, //dk_ctp2_manual_weeding_11,
		fmc_Herbicide, //dk_ctp4_herbicide1,
		fmc_Herbicide, //dk_ctp4_herbicide2,
		fmc_Herbicide, //dk_ctp4_herbicide3,
		fmc_Others, //dk_ctp2_sow_catch_crop_11,
		fmc_Cutting, //dk_ctp2_manual_cutting_11,
		fmc_Harvest, //dk_ctp2_harvest,
		fmc_Others, //dk_ctp_wait
				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_ChristmasTrees_Perm_H