//
// DE_OCarrots.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DE_OCARROTS_H
#define DE_OCARROTS_H

#define DE_OCAR_BASE 36700
#define DE_OCAR_SLURRY_DATE m_field->m_user[0]
#define DE_OCAR_DECIDE_TO_HERB m_field->m_user[1]
#define DE_OCAR_DECIDE_TO_FI m_field->m_user[2]
#define DE_OCAR_DO_SPR_HARVEST a_field->m_user[3]



typedef enum {
  de_ocar_start = 1, // Compulsory, start event must always be 1 (one).
  de_ocar_stoneburier = DE_OCAR_BASE,
  de_ocar_FAmanure, 
  de_ocar_FPmanure,
  de_ocar_deep_harrow,
  de_ocar_bedformer,
  de_ocar_sharrow1,
  de_ocar_sharrow2,
  de_ocar_sharrow3,
  de_ocar_sharrow4,
  de_ocar_sow,
  de_ocar_burn_weeds1,
  de_ocar_burn_weeds2,
  de_ocar_row_cultivation1,
  de_ocar_row_cultivation2,
  de_ocar_manual_weeding,
  de_ocar_water1,
  de_ocar_hilling_up1,
  de_ocar_boron1_s,
  de_ocar_boron1_p,
  de_ocar_water2,
  de_ocar_hilling_up2,
  de_ocar_boron2_s,
  de_ocar_boron2_p,
  de_ocar_hilling_up3,
  de_ocar_harvest,
  de_ocar_wait,
  de_ocar_foobar,
} DE_OCarrotsToDo;

class DE_OCarrots: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OCarrots(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L) {
    m_first_date=g_date->DayInYear(15,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_ocar_foobar - DE_OCAR_BASE);
	  m_base_elements_no = DE_OCAR_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_ocar_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation,	//	  de_ocar_stoneburier = DE_OCAR_BASE,
			fmc_Fertilizer,	//	  de_ocar_FAmanure, 
			fmc_Fertilizer,	//	  de_ocar_FPmanure,
			fmc_Cultivation,	//	  de_ocar_deep_harrow,
			fmc_Cultivation,	//	  de_ocar_bedformer,
			fmc_Cultivation,	//	  de_ocar_sharrow1,
			fmc_Cultivation,	//	  de_ocar_sharrow2,
			fmc_Cultivation,	//	  de_ocar_sharrow3,
			fmc_Cultivation,	//	  de_ocar_sharrow4,
			fmc_Others,	//	  de_ocar_sow,
			fmc_Others,	//	  de_ocar_burn_weeds1,
			fmc_Others,	//	  de_ocar_burn_weeds2,
			fmc_Cultivation,	//	  de_ocar_row_cultivation1,
			fmc_Cultivation,	//	  de_ocar_row_cultivation2,
			fmc_Cultivation,	//	  de_ocar_manual_weeding,
			fmc_Others,	//	  de_ocar_water1,
			fmc_Cultivation,	//	  de_ocar_hilling_up1,
			fmc_Fertilizer,	//	  de_ocar_boron1,
			fmc_Fertilizer,	//	  de_ocar_boron1,
			fmc_Others,	//	  de_ocar_water2,
			fmc_Cultivation,	//	  de_ocar_hilling_up2,
			fmc_Fertilizer,	//	  de_ocar_boron2,
			fmc_Fertilizer,	//	  de_ocar_boron2,
			fmc_Cultivation,	//	  de_ocar_hilling_up3,
			fmc_Harvest,	//	  de_ocar_harvest,
			fmc_Others,	//	  de_ocar_wait,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DE_OCARROTS_H
