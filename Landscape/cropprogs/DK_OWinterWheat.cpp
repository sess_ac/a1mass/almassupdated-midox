/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OWinterWheat.cpp This file contains the source for the DK_WinterWheat class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of May 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_OWinterWheat.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OWinterWheat.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_OWinterWheat_SkScrapes("DK_CROP_OWW_SK_SCRAPES",CFG_CUSTOM, false);
extern CfgFloat cfg_DKCatchCropPct;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DK_OWinterWheat::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	m_ev = a_ev;
	TTypesOfVegetation l_tov = tov_DKOWinterWheat;
	int l_nextcropstartdate;
					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_oww_start:
	{
		// This is just to hold a local variable in scope and prevent compiler errors
			// ww_start just sets up all the starting conditions and reference dates that are needed to start a ww
			// crop off
		DK_OWW_AUTUMN_PLOUGH = false;
 		DK_OWW_DECIDE_TO_FI = 1;

		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(30, 8); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(29, 8); // last possible day of harvest
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(30, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) // straw chopping
		flexdates[2][0] = -1;; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 2)
		flexdates[2][1] = g_date->DayInYear(30, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 2) // hay baling
		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(0, flexdates, int(dk_oww_water1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		// OK, let's go.
		SimpleEvent(d1, dk_oww_autumn_harrow, false);
	}
	break;

	// This is the first real farm operation - LKM: done if sandy soil (to prevent sand drift)
	case dk_oww_autumn_harrow:
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(11, 10) - g_date->DayInYear())) { // changed to fit rots from 1/10
				SimpleEvent(g_date->Date() + 1, dk_oww_autumn_harrow, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_oww_autumn_plough, false);
		break;
	case dk_oww_autumn_plough:
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_oww_autumn_plough, true);
			break;
		}
		// Queue up the next event - in this case autumn roll 
		SimpleEvent(g_date->Date() + 1, dk_oww_autumn_sow, false);
		break;
	case dk_oww_autumn_sow:
		if (!m_farm->AutumnSow(m_field, 0.0,
			g_date->DayInYear(30, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_oww_autumn_sow, true);
			break;
		}
		// Queue up the next event slurry 1 (next year)			
	    SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_oww_slurry1_s, false); 
		break;	
	case dk_oww_slurry1_s:
		if (a_farm->IsStockFarmer()) {
			if (!m_farm->FA_Slurry(m_field, 0.0,
				g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oww_slurry1_s, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_oww_slurry2_s, false);
			break;
		}
		else SimpleEvent(g_date->Date(), dk_oww_slurry1_p, false);
		break;

	case dk_oww_slurry1_p:
		if (!m_farm->FP_Slurry(m_field, 0.0,
			g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_oww_slurry1_p, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_oww_slurry2_p, false);
		break;
	case dk_oww_slurry2_s:
		if (!m_farm->FA_Slurry(m_field, 0.0,
			g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_oww_slurry2_s, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_oww_water1, false);
		break;
	case dk_oww_slurry2_p:
		if (!m_farm->FP_Slurry(m_field, 0.0,
			g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_oww_slurry2_p, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_oww_water1, false);
		break;
	case dk_oww_water1:
		if (m_ev->m_lock || m_farm->DoIt_prob(.50)) {
			if (!m_farm->Water(m_field, 0.0,
				g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oww_water1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_oww_water2, false);
		break;
	case dk_oww_water2:
		if (m_ev->m_lock || m_farm->DoIt_prob(.5)) {
			if (!m_farm->Water(m_field, 0.0,
				g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oww_water2, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_oww_harvest, false);
		break;
	case dk_oww_harvest:
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_oww_harvest, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_oww_straw_chopping, false);
		break;
	case dk_oww_straw_chopping:
		// 75% of farmers will leave straw on field and rest will do hay bailing
		if (m_ev->m_lock || m_farm->DoIt_prob(.75)) {
			if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_oww_straw_chopping, true);
				break;
			}
			done = true;
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_oww_hay_baling, false);
			break;
	case dk_oww_hay_baling:
		if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_oww_hay_baling, true);
			break;
		}
		done = true;
		break;


	default:
		g_msg->Warn(WARN_BUG, "DK_OWinterWheat::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}