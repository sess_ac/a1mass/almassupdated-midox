/**
\file
\brief
<B>NLBeetSpring.h This file contains the headers for the BeetSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of July 2018 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLBeetSpring.h
//


#ifndef NLBEETSPRING_H
#define NLBEETSPRING_H

#define NLBEETSPRING_BASE 20300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_BES_HERBI1	a_field->m_user[1]
#define NL_BES_FUNGI1	a_field->m_user[2]


/** Below is the list of things that a farmer can do if he is growing beet, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_bes_start = 1, // Compulsory, must always be 1 (one).
	nl_bes_sleep_all_day = NLBEETSPRING_BASE,
	nl_bes_spring_plough_sandy,
	nl_bes_ferti_p1,
	nl_bes_ferti_s1,
	nl_bes_preseeding_cultivator,
	nl_bes_spring_sow,
	nl_bes_ferti_p2,
	nl_bes_ferti_s2,
	nl_bes_herbicide1,
	nl_bes_herbicide2,
	nl_bes_herbicide3,
	nl_bes_herbicide4,
	nl_bes_fungicide1,
	nl_bes_fungicide2,
	nl_bes_fungicide3,
	nl_bes_harvest,
	nl_bes_foobar,
} NLBeetSpringToDo;


/**
\brief
NLBeetSpring class
\n
*/
/**
See NLBeetSpring.h::NLBeetSpringToDo for a complete list of all possible events triggered codes by the beet management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLBeetSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLBeetSpring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,3 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (nl_bes_foobar - NLBEETSPRING_BASE);
	   m_base_elements_no = NLBEETSPRING_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,//nl_bes_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//nl_bes_sleep_all_day = NLBEETSPRING_BASE,
			fmc_Cultivation,//nl_bes_spring_plough_sandy,
			fmc_Fertilizer,//nl_bes_ferti_p1,
			fmc_Fertilizer,//nl_bes_ferti_s1,
			fmc_Cultivation,//nl_bes_preseeding_cultivator,
			fmc_Others,//nl_bes_spring_sow,
			fmc_Fertilizer,//nl_bes_ferti_p2,
			fmc_Fertilizer,//nl_bes_ferti_s2,
			fmc_Herbicide,//nl_bes_herbicide1,
			fmc_Herbicide,//nl_bes_herbicide2,
			fmc_Herbicide,//nl_bes_herbicide3,
			fmc_Herbicide,//nl_bes_herbicide4,
			fmc_Fungicide,//nl_bes_fungicide1,
			fmc_Fungicide,//nl_bes_fungicide2,
			fmc_Fungicide,//nl_bes_fungicide3,
			fmc_Harvest//nl_bes_harvest, 
	  };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // NLBEETSPRING_H

