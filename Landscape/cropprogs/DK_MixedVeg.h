//
// DK_MixedVeg.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_MixedVeg_H
#define DK_MixedVeg_H


#define DK_MV_BASE 69000 // based on salat

#define DK_MV_FORCESPRING a_field->m_user[1]

typedef enum {
  dk_mv_start = 1, // Compulsory, start event must always be 1 (one).
  dk_mv_molluscicide = DK_MV_BASE,
  dk_mv_spring_plough,
  dk_mv_strigling1,
  dk_mv_strigling2,
  dk_mv_strigling3,
  dk_mv_strigling4,
  dk_mv_herbicide1,
  dk_mv_herbicide2,
  dk_mv_herbicide3,
  dk_mv_herbicide4,
  dk_mv_plant1,
  dk_mv_plant2,
  dk_mv_plant3,
  dk_mv_plant4,
  dk_mv_ferti_s1,
  dk_mv_ferti_p1,
  dk_mv_ferti_s2,
  dk_mv_ferti_p2,
  dk_mv_fungicide1,
  dk_mv_fungicide2,
  dk_mv_cover_on,
  dk_mv_cover_off,
  dk_mv_water1,
  dk_mv_water2,
  dk_mv_water3,
  dk_mv_row_cultivation1,
  dk_mv_row_cultivation2,
  dk_mv_insecticide1,
  dk_mv_insecticide2,
  dk_mv_harvest1,
  dk_mv_harvest2,
  dk_mv_harvest3,
  dk_mv_wait,
  dk_mv_foobar
} DK_MixedVegToDo;



class DK_MixedVeg: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_MixedVeg(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(20,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_mv_foobar - DK_MV_BASE);
	  m_base_elements_no = DK_MV_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others, // zero element unused but must be here
		  fmc_Others, // dk_mv_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Others, //  dk_mv_mollusicicde = DK_MV_BASE,
		  fmc_Cultivation, //   dk_mv_spring_plough,
		  fmc_Cultivation, //  dk_mv_strigling1,
		  fmc_Cultivation, //  dk_mv_strigling2,
		  fmc_Cultivation, //  dk_mv_strigling3,
		  fmc_Cultivation, //  dk_mv_strigling4,
		  fmc_Herbicide, //  dk_mv_herbicide1,
		  fmc_Herbicide, //  dk_mv_herbicide2,
		  fmc_Herbicide, //  dk_mv_herbicide3,
		  fmc_Herbicide, //  dk_mv_herbicide4,
		  fmc_Others, //   dk_mv_plant1,
		  fmc_Others, //   dk_mv_plant2,
		  fmc_Others, //   dk_mv_plant3,
		  fmc_Others, //   dk_mv_plant4,
		  fmc_Fertilizer, //   dk_mv_ferti_s1,
		  fmc_Fertilizer, //  dk_mv_ferti_p1,
		  fmc_Fertilizer, //  dk_mv_ferti_s2,
		  fmc_Fertilizer, //  dk_mv_ferti_p2,
		  fmc_Fungicide, //  dk_mv_fungicide1,
		  fmc_Fungicide, //   dk_mv_fungicide2,
		  fmc_Others, //  dk_mv_cover_on,
		  fmc_Others, //  dk_mv_cover_off,
		  fmc_Watering, //  dk_mv_water1,
		  fmc_Watering, //  dk_mv_water2,
		  fmc_Watering, //  dk_mv_water3,
		  fmc_Cultivation, //  dk_mv_row_cultivation1,
		  fmc_Cultivation, //  dk_mv_row_cultivation2,
		  fmc_Insecticide, //  dk_mv_insecticide1,
		  fmc_Insecticide, //  dk_mv_insecticide2,
		  fmc_Harvest, //  dk_mv_harvest1,
		  fmc_Harvest, //  dk_mv_harvest2,
		  fmc_Harvest, //  dk_mv_harvest3,
		  fmc_Others, // dk_mv_wait,
					  // No foobar entry
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};

#endif // DK_MixedVeg_H
