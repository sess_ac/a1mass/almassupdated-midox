/**
\file
\brief
<B>DK_OOrchCherry.h This file contains the source for the DK_OOrchCherry class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OOrchCherry.h
//


#ifndef DK_OOOrchCherry_H
#define DK_OOOrchCherry_H

#define DK_OOCH_YEARS_AFTER_PLANT	a_field->m_user[0]
#define DK_OOCH_YEARS_HARVEST	a_field->m_user[1]
#define DK_OOCH_EST_YEAR a_field->m_user[2]

#define DK_OOCH_BASE 68600
/**

*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_ooch_start = 1, // Compulsory, must always be 1 (one).
	dk_ooch_sleep_all_day = DK_OOCH_BASE,
	dk_ooch_wait,
	dk_ooch_autumn_plough,
	dk_ooch_manure_s1,
	dk_ooch_manure_p1,
	dk_ooch_water1,
	dk_ooch_subsoiler,
	dk_ooch_planting,
	dk_ooch_sow_grass,
	dk_ooch_insecticide1,
	dk_ooch_insecticide2,
	dk_ooch_insecticide3,
	dk_ooch_manual_cutting1,
	dk_ooch_manual_cutting2,
	dk_ooch_manual_cutting3,
	dk_ooch_manual_cutting4,
	dk_ooch_manual_cutting5,
	dk_ooch_manual_cutting6,
	dk_ooch_manual_cutting7,
	dk_ooch_manual_cutting8,
	dk_ooch_manual_cutting9,
	dk_ooch_manual_cutting10,
	dk_ooch_manual_cutting11,
	dk_ooch_manual_cutting12,
	dk_ooch_manure_s2,
	dk_ooch_manure_p2,
	dk_ooch_row_cultivation1,
	dk_ooch_water2,
	dk_ooch_water3,
	dk_ooch_water4,
	dk_ooch_water5,
	dk_ooch_water6,
	dk_ooch_water7,
	dk_ooch_cutting1,
	dk_ooch_cutting2,
	dk_ooch_cutting3,
	dk_ooch_cutting4,
	dk_ooch_cutting5,
	dk_ooch_cutting6,
	dk_ooch_row_cultivation2,
	dk_ooch_insecticide4,
	dk_ooch_harvest,
	dk_ooch_foobar,
} DK_OOrchCherryToDo;


/**
\brief
DK_OOrchCherry class
\n
*/
/**
See DK_OOrchCherry.h::DK_OOrchCherryToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OOrchCherry : public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OOrchCherry(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,1 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_ooch_foobar - DK_OOCH_BASE);
	   m_base_elements_no = DK_OOCH_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_ooch_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_ooch_sleep_all_day = DK_OOCH_BASE,
			fmc_Others, // dk_ooch_wait,
			fmc_Cultivation, //dk_ooch_spring_plough,
			fmc_Fertilizer, //dk_ooch_manure_s1,
			fmc_Fertilizer, //dk_ooch_manure_p1,
			fmc_Watering, //dk_ooch_water1,
			fmc_Cultivation, //dk_ooch_subsoiler,
			fmc_Others, //dk_ooch_planting,
			fmc_Others, //dk_ooch_sow_grass,
			fmc_Others, //dk_ooch_insecticide1,
			fmc_Others, //dk_ooch_insecticide2,
			fmc_Others, //dk_ooch_insecticide3,
			fmc_Cutting, //dk_ooch_manual_cutting1,
			fmc_Cutting, //dk_ooch_manual_cutting2,
			fmc_Cutting, //dk_ooch_manual_cutting3,
			fmc_Cutting, //dk_ooch_manual_cutting4,
			fmc_Cutting, //dk_ooch_manual_cutting5,
			fmc_Cutting, //dk_ooch_manual_cutting6,
			fmc_Cutting, //dk_ooch_manual_cutting7,
			fmc_Cutting, //dk_ooch_manual_cutting8,
			fmc_Cutting, //dk_ooch_manual_cutting9,
			fmc_Cutting, //dk_ooch_manual_cutting10,
			fmc_Cutting, //dk_ooch_manual_cutting11,
			fmc_Cutting, //dk_ooch_manual_cutting12,
			fmc_Cutting, //dk_ooch_manure_s2,
			fmc_Cutting, //dk_ooch_manure_p2,
			fmc_Cultivation, //dk_ooch_row_cultivation1,
			fmc_Watering, //dk_ooch_water2,
			fmc_Watering, //dk_ooch_water3,
			fmc_Watering, //dk_ooch_water4,
			fmc_Watering, //dk_ooch_water5,
			fmc_Watering, //dk_ooch_water6,
			fmc_Watering, //dk_ooch_water7,
			fmc_Cutting, //dk_ooch_cutting1,
			fmc_Cutting, //dk_ooch_cutting2,
			fmc_Cutting, //dk_ooch_cutting3,
			fmc_Cutting, //dk_ooch_cutting4,
			fmc_Cutting, //dk_ooch_cutting5,
			fmc_Cutting, //dk_ooch_cutting6,
			fmc_Cultivation, //dk_ooch_row_cultivation2,
			fmc_Others, //dk_ooch_insecticide4,
			fmc_Harvest, //dk_ooch_harvest,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OOrchCherry_H

