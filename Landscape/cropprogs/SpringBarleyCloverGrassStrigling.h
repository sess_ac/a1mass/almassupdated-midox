//
// SpringBarleyCloverGrassStrigling.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleyCloverGrassStrigling_H
#define SpringBarleyCloverGrassStrigling_H

#define SBARLEYCGS_BASE 6200
#define SBCGS_ISAUTUMNPLOUGH    m_field->m_user[0]
#define SBCGS_FERTI_DONE        m_field->m_user[1]
#define SBCGS_SPRAY             m_field->m_user[2]

typedef enum {
  sbcgs_start = 1, // Compulsory, start event must always be 1 (one).
  sbcgs_ferti_s1 = SBARLEYCGS_BASE,
  sbcgs_ferti_s2,
  sbcgs_ferti_s3,
  sbcgs_harvest,
  sbcgs_spring_plough,
  sbcgs_autumn_plough,
  sbcgs_spring_harrow,
  sbcgs_spring_roll,
  sbcgs_spring_sow,
  sbcgs_hay_baling,
  sbcgs_GR,
  sbcgs_water1,
  sbcgs_water2,
  sbcgs_strigling1,
  sbcgs_strigling_sow,
  sbcgs_insecticide,
  sbcgs_fungicide,
  sbcgs_cattle_out,
  sbcgs_cattle_is_out,
  sbcgs_foobar
} SBCGSToDo;



class SpringBarleyCloverGrassStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleyCloverGrassStrigling(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(30,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sbcgs_foobar - SBARLEYCGS_BASE);
	  m_base_elements_no = SBARLEYCGS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sbcgs_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//sbcgs_ferti_s1 = SBARLEYCGS_BASE,
		  fmc_Fertilizer,//sbcgs_ferti_s2,
		  fmc_Fertilizer,//sbcgs_ferti_s3,
		  fmc_Harvest,//sbcgs_harvest,
		  fmc_Cultivation,//sbcgs_spring_plough,
		  fmc_Cultivation,//sbcgs_autumn_plough,
		  fmc_Cultivation,//sbcgs_spring_harrow,
		  fmc_Others,//sbcgs_spring_roll,
		  fmc_Others,//sbcgs_spring_sow,
		  fmc_Others,//sbcgs_hay_baling,
		  fmc_Others,//sbcgs_GR,
		  fmc_Watering,//sbcgs_water1,
		  fmc_Watering,//sbcgs_water2,
		  fmc_Cultivation,//sbcgs_strigling1,
		  fmc_Cultivation,//sbcgs_strigling_sow,
		  fmc_Insecticide,//sbcgs_insecticide,
		  fmc_Fungicide,//sbcgs_fungicide,
		  fmc_Grazing,//sbcgs_cattle_out,
		  fmc_Grazing//sbcgs_cattle_is_out

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SpringBarleyCloverGrass_H
