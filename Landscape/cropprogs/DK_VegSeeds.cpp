//
// DK_VegSeeds.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_VegSeeds.h"

extern CfgBool cfg_pest_veg_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_VegSeeds::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
  TTypesOfVegetation l_tov = tov_DKVegSeeds; // made from guideline for spinach seeds

  switch (m_ev->m_todo) {
  case dk_vs_start:
  {
	  a_field->ClearManagementActionSum();

	  DK_VS_FORCESPRING = false;
	  DK_VS_AUTUMNPLOUGH = false;

	  m_last_date = g_date->DayInYear(31, 8); // Should match the last flexdate below
		  //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
	  std::vector<std::vector<int>> flexdates(1 + 2, std::vector<int>(2, 0));
	  // Set up the date management stuff
			  // Start and stop dates for all events after harvest
	  flexdates[0][1] = g_date->DayInYear(14, 8); // last possible day 
	  // Now these are done in pairs, start & end for each operation. If its not used then -1
	  flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
	  flexdates[1][1] = g_date->DayInYear(22, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 
	  flexdates[2][0] = -1;
	  flexdates[2][1] = g_date->DayInYear(31, 8);

// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
	  int isSpring = 0;
	  if (StartUpCrop(isSpring, flexdates, int(dk_vs_molluscicide))) break;

	  // End single block date checking code. Please see next line comment as well.
	  // Reinit d1 to first possible starting date.
	  d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
	  // OK, let's go.
	  // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
	  if (m_ev->m_forcespring) {
		  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_vs_molluscicide, false);
		  DK_VS_FORCESPRING = true;
		  break;
	  }
	  else SimpleEvent(d1, dk_vs_autumn_plough, false);
	  break;
  }
  break;

  case dk_vs_autumn_plough:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
		  if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_autumn_plough, true);
			  break;
		  }
	  }
	  DK_VS_AUTUMNPLOUGH = true; // remember who did autumn plough
	  // LKM: Queue up next event - Molluscicide1 after 1st of March next year
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_vs_molluscicide, false);
	  break;

  case dk_vs_molluscicide:
	  if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
		  if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_molluscicide, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 1, dk_vs_spring_plough, false);
	  break;

  case dk_vs_spring_plough: // only if no autumn plough (force spring)
	  if (DK_VS_AUTUMNPLOUGH == false) {
		  if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_spring_plough, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_vs_ferti_s, false);
	  break;

  case dk_vs_ferti_s: // if stock farmer
	  if (a_farm->IsStockFarmer()) {
		  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			  if (!m_farm->FA_NPKS(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, dk_vs_ferti_s, true);
				  break;
			  }
		  }
		  SimpleEvent(g_date->Date() + 1, dk_vs_sow_cultivation, false);
		  break;
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_vs_ferti_p, false);
	  break;

  case dk_vs_ferti_p: // if arable farmer
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->FP_NPKS(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_ferti_p, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 1, dk_vs_sow_cultivation, false);
	  break;

  case dk_vs_sow_cultivation:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_sow_cultivation, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 2, dk_vs_herbicide1, false); // herbi thread
	  SimpleEvent(g_date->Date() + 14, dk_vs_row_cultivation1, false); // row cultivation thread
	  SimpleEvent(g_date->Date() + 30, dk_vs_manganese, false); // Manganese thread
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_vs_insecticide, false); // insecti thread
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_vs_fungicide1, false); // fungi thread
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_vs_water, false); // main thread
	  break;

  case dk_vs_herbicide1:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_herbicide1, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 7, dk_vs_herbicide2, false);
	  break;

  case dk_vs_herbicide2:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_herbicide2, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 6, dk_vs_herbicide3, false);
	  break;

  case dk_vs_herbicide3:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(24, 5) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_herbicide3, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 6, dk_vs_herbicide4, false);
	  break;

  case dk_vs_herbicide4:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(2, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_herbicide4, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 6, dk_vs_herbicide5, false);
	  break;

  case dk_vs_herbicide5:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(11, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_herbicide5, true);
			  break;
		  }
	  }
	  break; // end of thread

  case dk_vs_row_cultivation1:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_row_cultivation1, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 14, dk_vs_row_cultivation2, false);
	  break;

  case dk_vs_row_cultivation2:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_row_cultivation2, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 14, dk_vs_row_cultivation3, false);
	  break;

  case dk_vs_row_cultivation3:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_row_cultivation3, true);
			  break;
		  }
	  }
	  break; // end of thread

  case dk_vs_manganese:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_manganese, true);
			  break;
		  }
	  }
	  break; // end of thread

  case dk_vs_insecticide:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
		  // here we check whether we are using ERA pesticide or not
		  d1 = g_date->DayInYear(31, 5) - g_date->DayInYear();
		  if (!cfg_pest_veg_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		  {
			  flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
		  }
		  else {
			  flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  if (!flag) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_insecticide, true);
			  break;
		  }
	  }
	  break; // end of thread

  case dk_vs_fungicide1:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_fungicide1, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 14, dk_vs_fungicide2, false);
	  break;

  case dk_vs_fungicide2:
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_fungicide2, true);
			  break;
		  }
	  }
	  break; // end of thread

  case dk_vs_water:
	  if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
	  {
		  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			  if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, dk_vs_water, true);
				  break;
			  }
		  }
	  }
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 7), dk_vs_swathing, false); 
	  break;

  case dk_vs_swathing:
	  if (m_ev->m_lock || m_farm->DoIt_prob(0.90)) {
		  if (!m_farm->Swathing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_swathing, true);
			  break;
		  }
	  }
	  SimpleEvent(g_date->Date() + 7, dk_vs_herbicide6, false);
	  SimpleEvent(g_date->Date() + 14, dk_vs_harvest, false); // main thread
	  break;

  case dk_vs_herbicide6: // if uneven maturing
	  if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
		  if (!m_farm->HerbicideTreat(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_herbicide6, true);
			  break;
		  }
	  }
	  break; //end of thread

  case dk_vs_harvest: 
	  if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
		  if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, dk_vs_harvest, true);
			  break;
		  }
	  }
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "DK_VegSeeds::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


