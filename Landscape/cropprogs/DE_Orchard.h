/**
\file
\brief
<B>DE_Orchard.h This file contains the headers for the DE_Orchard class</B> \n
*/
/**
\file 
 by Chris J. Topping and Elzbieta Ziolkowska - modified by Luna Kondrup Marcussen \n
 Version of November 2022 \n
 All rights reserved. \n
 \n
*/
//
// DE_Orchard.
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_ORCH_H
#define DE_ORCH_H

#define DE_ORCH_BASE 39300
/**
\brief A flag used to indicate autumn ploughing status
*/



/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_orch_start = 1, // Compulsory, must always be 1 (one).
	de_orch_harvest = DE_ORCH_BASE,
	de_orch_ferti_p1,
	de_orch_ferti_s1,
	de_orch_ferti_p2,
	de_orch_ferti_s2,
	de_orch_ferti_p3,
	de_orch_ferti_s3,
	de_orch_ferti_p4,
	de_orch_ferti_s4,
	de_orch_ferti_p5,
	de_orch_ferti_s5,
	de_orch_fungicide1,
	de_orch_fungicide2,
	de_orch_fungicide3,
	de_orch_fungicide4,
	de_orch_fungicide5,
	de_orch_fungicide6,
	de_orch_fungicide7,
	de_orch_fungicide8,
	de_orch_fungicide9,
	de_orch_fungicide10,
	de_orch_fungicide11,
	de_orch_fungicide12,
	de_orch_fungicide13,
	de_orch_fungicide14,
	de_orch_fungicide15,
	de_orch_fungicide16,
	de_orch_fungicide17,
	de_orch_fungicide18,
	de_orch_insecticide1,
	de_orch_insecticide2,
	de_orch_insecticide3,
	de_orch_insecticide4,
	de_orch_insecticide5,
	de_orch_insecticide6,
	de_orch_herbicide,
	de_orch_foobar, // Obligatory, must be last
} DE_ORCHToDo;


/**
\brief
DE_Orchard class
\n
*/
/**
See DE_Orchard.h::DE_OrchardToDo for a complete list of all possible events triggered codes by the Spring Rye management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DE_Orchard : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_Orchard(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		/**
		When we start it off, the first possible date for a farm operation is 15th September
		This information is used by other crops when they decide how much post processing of
		the management is allowed after harvest before the next crop starts.
		*/
		m_first_date = g_date->DayInYear(31, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_orch_foobar - DE_ORCH_BASE);
		m_base_elements_no = DE_ORCH_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others,	// zero element unused but must be here
			 // ALL THE NECESSARY ENTRIES HERE
			 fmc_Others,			//	de_orch_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Harvest,			//	de_orch_harvest
			 fmc_Fertilizer,		//	de_orch_ferti_p1,
			 fmc_Fertilizer,		//	de_orch_ferti_s1,
			 fmc_Fertilizer,		//	de_orch_ferti_p2,
			 fmc_Fertilizer,		//	de_orch_ferti_s2,
			 fmc_Fertilizer,		//	de_orch_ferti_p3,
			 fmc_Fertilizer,		//	de_orch_ferti_s3,
			 fmc_Fertilizer,		//	de_orch_ferti_p4,
			 fmc_Fertilizer,		//	de_orch_ferti_s4,
			 fmc_Fertilizer,		//	de_orch_ferti_p5,
			 fmc_Fertilizer,		//	de_orch_ferti_s5,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Fungicide,			//  de_orch_fungicide,
			 fmc_Insecticide,		//  de_orch_insecticide,
			 fmc_Insecticide,		//  de_orch_insecticide,
			 fmc_Insecticide,		//  de_orch_insecticide,
			 fmc_Insecticide,		//  de_orch_insecticide,
			 fmc_Insecticide,		//  de_orch_insecticide,
			 fmc_Insecticide,		//  de_orch_insecticide,
			 fmc_Herbicide,			//  de_orch_herbicide,

			// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_ORCH_H

