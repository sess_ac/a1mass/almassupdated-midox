//
// OLupines.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Chris J. Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OLupines.h"

extern CfgFloat cfg_strigling_prop;
extern CfgFloat cfg_DKCatchCropLegumePct;

bool DK_OLupines::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;

    bool done = false;
    int d1;
    TTypesOfVegetation l_tov = tov_DKOLupines;
    int l_nextcropstartdate;

    switch (m_ev->m_todo)
    {
    case dk_olu_start:
    {
        a_field->ClearManagementActionSum();
        m_field->SetVegPatchy(true); // LKM: A crop with wide rows, so set patchy
        m_last_date = g_date->DayInYear(15, 9); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(13, 9); // last possible day of plough in this case 
        // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[1][1] = g_date->DayInYear(15, 9); // last possible day of plough in this case 



        // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
        int isSpring = 365;
        if (StartUpCrop(365, flexdates, int(dk_olu_spring_harrow1))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
        // OK, let's go.
        // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
        SimpleEvent(d1, dk_olu_spring_harrow1, false);
        break;
    }
    break;

    // LKM: do spring harrow, do it before the 10th of April - if not done, try again +1 day until the 10th of April when we succeed - 100% of farmers do this
    case dk_olu_spring_harrow1:
        if (!m_farm->SpringHarrow(m_field, 0.0,
            g_date->DayInYear(10, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olu_spring_harrow1, true);
            break;
        }
        // LKM: Queue up the next event - spring plough 
        SimpleEvent(g_date->Date() + 1, dk_olu_spring_plough, false);
        break;
        // LKM: do spring plough before the 15th of April - if not done, try again +1 day until the 15th of April when we succeed- 100% of farmers do this
    case dk_olu_spring_plough:
        if (!m_farm->SpringPlough(m_field, 0.0,
            g_date->DayInYear(15, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olu_spring_plough, true);
            break;
        }
        if (a_farm->IsStockFarmer())
        {
            // LKM: Queue up the next event - K and S are added 
            SimpleEvent(g_date->Date() + 1, dk_olu_ks_ferti_s, false);
            break;
        } 
        else SimpleEvent(g_date->Date() + 1, dk_olu_ks_ferti_p, false);
        break;
       
        // LKM: add K and S before the 20th of April - if not done, try again +1 day until the 20th of April when we succeed- 100% of farmers do this
    case dk_olu_ks_ferti_s:
            if (!m_farm->FA_SK(m_field, 0.0,
                g_date->DayInYear(20, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olu_ks_ferti_s, true);
                break;
            }
            // LKM: Queue up the next event - spring harrow just before sowing
            SimpleEvent(g_date->Date() + 1, dk_olu_spring_harrow2, false);
            break;

    case dk_olu_ks_ferti_p:
        if (!m_farm->FP_SK(m_field, 0.0,
            g_date->DayInYear(20, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olu_ks_ferti_p, true);
            break;
        }
        // LKM: Queue up the next event - spring harrow just before sowing
        SimpleEvent(g_date->Date() + 1, dk_olu_spring_harrow2, false);
        break;
        // LKM: spring harrow only done if difficult to sow because of heavy rain (assume 10% will do this) before the 25th of April - if not done, try again +1 day until the 25th of April when we will succeed
    case dk_olu_spring_harrow2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10))
        {
            if (!m_farm->ShallowHarrow(m_field, 0.0,
                g_date->DayInYear(21, 4) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olu_spring_harrow2, true);
                break;
            }
        }

        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_olu_spring_sow, false);
        break;
    case dk_olu_spring_sow: // ~50% do a normal broad sow, 50% do row sow
        if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olu_spring_sow, true);
            break;
        }
        //Queue up harrow/strigling 3-5 days after sow
        SimpleEvent(g_date->Date() + 3, dk_olu_strigling1, false);
        break;
        // LKM: strigling before the 5th of May - if not done, try again +3 days until the 5th of May when we succeed
    case dk_olu_strigling1:
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(5, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olu_strigling1, true);
            break;
        }
        SimpleEvent(g_date->Date() + 3, dk_olu_strigling2, false);
        break;
    case dk_olu_strigling2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) { // suggestion because strigling happens 1-2 times
            if (!m_farm->Strigling(m_field, 0.0,
                g_date->DayInYear(9, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olu_strigling2, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 14, dk_olu_strigling3, false);
        break;

    case dk_olu_strigling3:
        if (!m_farm->Strigling(m_field, 0.0,
            g_date->DayInYear(24, 5) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olu_strigling3, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_olu_water, false);
        SimpleEvent(g_date->Date() + 7, dk_olu_strigling4, false);
        break;

    case dk_olu_strigling4:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) { // suggestion because strigling happens 1-2 times
            if (!m_farm->Strigling(m_field, 0.0,
                g_date->DayInYear(2, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olu_strigling4, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 14, dk_olu_rowcultivation1, false);
        break;

    case dk_olu_rowcultivation1:
        if (!m_farm->RowCultivation(m_field, 0.0,
            g_date->DayInYear(16, 6) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olu_rowcultivation1, true);
            break;
        }
        SimpleEvent(g_date->Date() + 7, dk_olu_rowcultivation2, false);
        break;

    case dk_olu_rowcultivation2: 
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) { // suggestion because row cultivation happens 1-2 times
            if (!m_farm->RowCultivation(m_field, 0.0,
                g_date->DayInYear(25, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olu_rowcultivation2, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(18, 8), dk_olu_swathing, false);
        break;

    case dk_olu_swathing:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) { // suggestion
            // LKM: +2 days before harvest
            if (!m_farm->Swathing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olu_swathing, true);
                break;
            }
        }
        // LKM: Queue up the next event - harvest 
        SimpleEvent(g_date->Date() + 2, dk_olu_harvest, false);
        break;

    case dk_olu_water:
        // LKM: water before the 25th of June - if not done, try again +1 days until the 25th of June when we succeed - not too common - suggests 10%
        if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
            if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_olu_water, true);
                break;
            }
        }
        break; // end of thread

    case dk_olu_harvest:
        // LKM: harvest before the 25th of August - if not done, try again +1 days until the 15th of September when we succeed - 100% of farmers do this
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_olu_harvest, true);
            break;
        } 
        m_field->SetVegPatchy(false);
        
            done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "OLupines::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

  return done;
}


