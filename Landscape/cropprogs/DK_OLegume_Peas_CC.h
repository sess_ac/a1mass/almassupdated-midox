//
// OLegume_Peas_CC.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved. 

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_OLegume_Peas_CC_H
#define DK_OLegume_Peas_CC_H
/** \brief A flag used to indicate do catch crop
*/
#define DK_OLPCC_BASE 161700

typedef enum {
  dk_olpcc_start = 1, // Compulsory, start event must always be 1 (one).
  dk_olpcc_harvest = DK_OLPCC_BASE,
  dk_olpcc_spring_harrow1,
  dk_olpcc_spring_harrow2,
  dk_olpcc_spring_plough,
  dk_olpcc_ks_ferti_s,
  dk_olpcc_ks_ferti_p,
  dk_olpcc_spring_harrow3,
  dk_olpcc_spring_row_sow,
  dk_olpcc_strigling1,
  dk_olpcc_rowcultivation1,
  dk_olpcc_strigling2,
  dk_olpcc_rowcultivation2,
  dk_olpcc_water,
  dk_olpcc_wait,
  dk_olpcc_foobar,
} DK_OLegume_Peas_CCToDo;



class DK_OLegume_Peas_CC: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OLegume_Peas_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(10,4);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_olpcc_foobar - DK_OLPCC_BASE);
	  m_base_elements_no = DK_OLPCC_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_olpcc_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_olpcc_harvest = DK_olpcc_BASE,
			fmc_Cultivation,	//	  dk_olpcc_spring_harrow1,
			fmc_Cultivation,	//	  dk_olpcc_spring_harrow2,
			fmc_Cultivation,	//	  dk_olpcc_spring_plough,
			fmc_Fertilizer,	//	  dk_olpcc_ks_ferti,
			fmc_Fertilizer,	//	  dk_olpcc_ks_ferti,
			fmc_Cultivation,	//	  dk_olpcc_spring_harrow3,
			fmc_Others,	//	  dk_olpcc_spring_row_sow,
			fmc_Cultivation,	//	  dk_olpcc_strigling,
			fmc_Cultivation,	//	  dk_olpcc_rowcultivation,
			fmc_Cultivation,	//	  dk_olpcc_strigling,
			fmc_Cultivation,	//	  dk_olpcc_rowcultivation,
			fmc_Watering,	//	  dk_olpcc_water,
			fmc_Others, // dk_olpcc_wait


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OLegume_Peas_CC_H
