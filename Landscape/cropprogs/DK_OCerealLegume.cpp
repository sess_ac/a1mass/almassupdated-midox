//
// DK_OCerealLegume.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OCerealLegume.h"
#include "math.h"


CfgBool cfg_DK_OCerealLegume_SkScrapes("DK_CROP_OCL_SK_SCRAPES", CFG_CUSTOM, false);

bool DK_OCerealLegume::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{

  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1 = 0;

  bool done = false;
  TTypesOfVegetation l_tov = tov_DKOCerealLegume;

  switch (m_ev->m_todo)
  {
  case dk_ocl_start:
  {
      a_field->ClearManagementActionSum();

      DK_OCL_FORCESPRING = false;

      m_last_date = g_date->DayInYear(10, 10); // Should match the last flexdate below
      //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(3 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
      // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(30, 9); // last possible day of plough in this case 
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(2, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - cutting
      flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
      flexdates[2][1] = g_date->DayInYear(2, 10); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - cattleout
      flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
      flexdates[3][1] = g_date->DayInYear(10, 10); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) - cattliseout

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 365;
      if (StartUpCrop(isSpring, flexdates, int(dk_ocl_spring_harrow))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;

      if (m_ev->m_forcespring) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, dk_ocl_spring_harrow, false);
          DK_OCL_FORCESPRING = true;
          break;
      }
      else
      // OK, let's go.
      // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
      SimpleEvent(d1, dk_ocl_spring_harrow, false);
      break;
  }
  break;
   
  // done if many weeds, or waste plants from earlier catch crops 
  case dk_ocl_spring_harrow:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->SpringHarrow(m_field, 0.0,
              g_date->DayInYear(31, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_spring_harrow, true);
              break;
          }
      }
      if (a_farm->IsStockFarmer()) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_manure1_s, false);
          break;
      }
      else SimpleEvent(g_date->Date() + 1, dk_ocl_manure1_p, false);
      break;

  case dk_ocl_manure1_s: // 75% do this (rest move to next event)
      if (m_ev->m_lock || m_farm->DoIt_prob(0.75)) {
          if (!m_farm->FA_Manure(m_field, 0.0,
              g_date->DayInYear(1, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_manure1_s, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_ocl_spring_plough, false);
      break;

  case dk_ocl_manure1_p: // 75% do this (rest move to next event)
      if (m_ev->m_lock || m_farm->DoIt_prob(0.75)) {
          if (!m_farm->FP_Manure(m_field, 0.0,
              g_date->DayInYear(1, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_manure1_p, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_ocl_spring_plough, false);
      break;

  case dk_ocl_spring_plough:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->SpringPlough(m_field, 0.0,
              g_date->DayInYear(20, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_spring_plough, true);
              break;
          }
      }
      SimpleEvent(g_date->Date(), dk_ocl_spring_sow, false);
      break;
  case dk_ocl_spring_sow:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
          if (!m_farm->SpringSow(m_field, 0.0,
              g_date->DayInYear(30, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_spring_sow, true);
              break;
          }
      } // 50% sow combined (cereal, legume and lay-out), 40% sow ceread + legume, then lay-out, 10% sow w/o lay-out = 60% sow once, 40% sow twice
      SimpleEvent(g_date->Date(), dk_ocl_spring_sow_lo, false);
      break;
  case dk_ocl_spring_sow_lo:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.40)) {
          if (!m_farm->SpringSow(m_field, 0.0,
              g_date->DayInYear(5, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_spring_sow_lo, true);
              break;
          }
            SimpleEvent(g_date->Date(), dk_ocl_water_lo, false); // no dates for this
            break;
      } // the 50% will go straight to water
      else if (m_ev->m_lock || m_farm->DoIt_prob(0.50 / 0.60)) {
          SimpleEvent(g_date->Date(), dk_ocl_water_lo, false); // no dates for this
          break;
      }
      // the 10% w/o lay-out will do strigling
      else if (m_ev->m_lock || m_farm->DoIt_prob(0.10 / 0.10)) {
          SimpleEvent(g_date->Date() + 3, dk_ocl_strigling1, false);
          break;
      } 
      
  case dk_ocl_strigling1:
      if (!m_farm->Strigling(m_field, 0.0,
          g_date->DayInYear(5, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_strigling1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 7, dk_ocl_strigling2, false);
      break;
  case dk_ocl_strigling2:
      if (!m_farm->Strigling(m_field, 0.0,
          g_date->DayInYear(15, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_strigling2, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_ocl_water, false); // no dates for this
      break;
  case dk_ocl_water_lo: // 50% do watering
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(15, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_water_lo, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), dk_ocl_harvest_lo, false);
      break;
  case dk_ocl_water: // 50% do watering
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(15, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_water, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), dk_ocl_harvest, false);
      break;
  
  case dk_ocl_harvest: // 100% do harvest
      if (!m_farm->Harvest(m_field, 0.0,
          g_date->DayInYear(1, 9) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_harvest, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ocl_autumn_plough, false);
      break;
  case dk_ocl_harvest_lo: // 100% do harvest
      if (!m_farm->Harvest(m_field, 0.0,
          g_date->DayInYear(1, 9) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_harvest_lo, true);
          break;
      }
      if (a_farm->IsStockFarmer()) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_manure2_s, false);
          break;
      }
      else SimpleEvent(g_date->Date() + 1, dk_ocl_manure2_p, false);
      break;

  case dk_ocl_manure2_s:
      if (!m_farm->FA_Manure(m_field, 0.0,
          g_date->DayInYear(2, 9) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_manure2_s, true);
          break;
      }
          SimpleEvent(g_date->Date(), dk_ocl_cutting1, false);
          break;

  case dk_ocl_manure2_p:
      if (!m_farm->FP_Manure(m_field, 0.0,
          g_date->DayInYear(2, 9) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_manure2_p, true);
          break;
      }
      SimpleEvent(g_date->Date(), dk_ocl_cutting1, false);
      break;

  case dk_ocl_autumn_plough:
      if (!m_farm->AutumnPlough(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_autumn_plough, true);
          break;
      }
      done = true;
      break;
  case dk_ocl_cutting1: 
      if (!m_farm->CutToHay(m_field, 0.0,
          g_date->DayInYear(2, 9) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_cutting1, true);
          break;
      }
      SimpleEvent(g_date->Date()+30, dk_ocl_cutting2, false);
      break;
  case dk_ocl_cutting2: //50% of the 90% with lay-out (45% in total) will do a second cut, 50% do grazing
      if (a_ev->m_lock || a_farm->DoIt_prob(0.50)) {
          if (!m_farm->CutToHay(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_ocl_cutting2, true);
              break;
          }
          // End of management w lay-out and cutting grass
          done = true;
          break;
      }
      else if (a_ev->m_lock || a_farm->DoIt_prob(0.50 / 0.50)) {
          SimpleEvent(g_date->Date() + 35, dk_ocl_grazing, false);
          break;
      }
  case dk_ocl_grazing: //50% of the 90% with lay-out (45% in total) will do a second cut, 50% do grazing
      if (!m_farm->CattleOut(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_grazing, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_ocl_cattle_is_out, false);
      break;
  case dk_ocl_cattle_is_out:    // Keep the cattle out there
                               // CattleIsOut() returns false if it is not time to stop grazing
      if (!m_farm->CattleIsOut(m_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear()
          , m_field->GetMDates(1, 3) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_ocl_cattle_is_out, false);
          break;
      }
        // End of management w lay-out and grazing
        done = true;
        break;

  default:
    g_msg->Warn( WARN_BUG, "DK_OCerealLegume::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
  return done;
}


