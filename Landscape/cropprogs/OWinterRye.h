//
// OWinterRye.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWinterRye_H
#define OWinterRye_H

#define OWRYE_BASE 4800

typedef enum {
  owry_start = 1, // Compulsory, start event must always be 1 (one).
  owry_fertmanure_plant = OWRYE_BASE,
  owry_fertmanure_stock,
  owry_fertslurry_plant,
  owry_fertslurry_stock,
  owry_autumn_plough,
  owry_autumn_harrow,
  owry_autumn_sow,
  owry_strigling_one,
  owry_strigling_two,
  owry_spring_sow,
  owry_strigling_three,
  owry_spring_roll,
  owry_harvest,
  owry_straw_chopping_plant,
  owry_straw_chopping_stock,
  owry_hay_turning,
  owry_hay_bailing,
  owry_foobar,
} OWinterRyeToDo;



class OWinterRye: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OWinterRye(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(23,9);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (owry_foobar - OWRYE_BASE);
	  m_base_elements_no = OWRYE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  owry_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  owry_fertmanure_plant = OWRYE_BASE,
			fmc_Fertilizer,	//	  owry_fertmanure_stock,
			fmc_Fertilizer,	//	  owry_fertslurry_plant,
			fmc_Fertilizer,	//	  owry_fertslurry_stock,
			fmc_Cultivation,	//	  owry_autumn_plough,
			fmc_Cultivation,	//	  owry_autumn_harrow,
			fmc_Others,	//	  owry_autumn_sow,
			fmc_Cultivation,	//	  owry_strigling_one,
			fmc_Cultivation,	//	  owry_strigling_two,
			fmc_Others,	//	  owry_spring_sow,
			fmc_Cultivation,	//	  owry_strigling_three,
			fmc_Cultivation,	//	  owry_spring_roll,
			fmc_Harvest,	//	  owry_harvest,
			fmc_Others,	//	  owry_straw_chopping_plant,
			fmc_Others,	//	  owry_straw_chopping_stock,
			fmc_Others,	//	  owry_hay_turning,
			fmc_Others	//	  owry_hay_bailing,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OWinterRye_H
