/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_GrasslandSilageAnnual.cpp This file contains the source for the DE_GrasslandSilageAnnual class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Susanne Stein \n
 Version of August 2021 \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DE_GrasslandSilageAnnual.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_GrasslandSilageAnnual.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional grassland silage annual.
*/
bool DE_GrasslandSilageAnnual::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DEGrasslandSilageAnnual; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

					   // Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (m_ev->m_todo)
	{
	case de_gsa_start:
	{
		DE_GSA_STUBBLE_PLOUGH = false;
		DE_GSA_WINTER_PLOUGH = false;
		DE_GSA_SPRING_FERTI = 0;
		DE_GSA_HERBICIDE1 = false;
		DE_GSA_HERBI_DATE = 0;

		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(10, 8); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(10, 8); // last possible day of cut to silage2 - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(10, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_gsa_spring_sow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(31, 10) + isSpring;
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		if (m_ev->m_forcespring) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_gsa_spring_harrow, false);
			break;
		}
		else SimpleEvent(d1, de_gsa_herbicide0, false);
		break;
	}
	break;

	// This is the first real farm operation
	case de_gsa_herbicide0:
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_herbicide0, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 14, de_gsa_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 14, de_gsa_ferti_p1, false, m_farm, m_field);
		break;
	case de_gsa_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again (until 20/8 when we will suceed)
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_plough, false, m_farm, m_field);
		break;
	case de_gsa_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_plough, false, m_farm, m_field);
		break;
	case de_gsa_stubble_plough:
		// 30% will do stubble plough, but rest will get away with non-inversion cultivation
		if (m_ev->m_lock || m_farm->DoIt(30))
		{
			if (!m_farm->StubblePlough(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				// 30% of farmers will do this, but the other 70% won't so we need to remember whether we are in one or the other group
				DE_GSA_STUBBLE_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->Date() + 1, de_gsa_autumn_harrow1, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_harrow, false, m_farm, m_field);
		break;
	case de_gsa_autumn_harrow1: 
		// all farmers who did stubble plough (30%)
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_gsa_autumn_harrow1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 2, de_gsa_autumn_harrow2, false, m_farm, m_field);
		break;
	case de_gsa_autumn_harrow2:
		// 70% of the 30% who did stubble plough = 21% of all farmers
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->Date() + 7 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_autumn_harrow2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 11)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 11);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_gsa_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_gsa_ferti_p2, false, m_farm, m_field);
		break;
	case de_gsa_stubble_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_gsa_stubble_harrow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 11)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 11);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_gsa_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_gsa_ferti_p2, false, m_farm, m_field);
		break;
	case de_gsa_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_winter_plough, false, m_farm, m_field);
		break;
	case de_gsa_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_winter_plough, false, m_farm, m_field);
		break;
	case de_gsa_winter_plough:
		// 60% will do winter plough, but rest will get away with non-inversion cultivation
		if (m_ev->m_lock || m_farm->DoIt(60))
		{
			if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_winter_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				// 60% of farmers will do this, but the other 40% won't so we need to remember whether we are in one or the other group
				DE_GSA_WINTER_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_gsa_spring_harrow, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_winter_stubble_cultivator_heavy, false, m_farm, m_field);
		break;
	case de_gsa_winter_stubble_cultivator_heavy:
		// the rest 40% who did not plough do heavy stubble cultivation
		if (!m_farm->StubbleCultivatorHeavy(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_gsa_winter_stubble_cultivator_heavy, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_gsa_spring_harrow, false, m_farm, m_field);
		break;
	case de_gsa_spring_harrow:
		if ((m_ev->m_lock) || m_farm->DoIt(90))
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_spring_harrow, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), de_gsa_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), de_gsa_ferti_p3, false, m_farm, m_field);
		break;
	case de_gsa_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_p3, true, m_farm, m_field);
				break;
			}
			DE_GSA_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_heavy_cultivator, false, m_farm, m_field);
		break;
	case de_gsa_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_ferti_s3, true, m_farm, m_field);
				break;
			}
			DE_GSA_SPRING_FERTI = 1;
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_heavy_cultivator, false, m_farm, m_field);
		break;
	case de_gsa_heavy_cultivator:
		if (DE_GSA_SPRING_FERTI == 1)
		{
			if (!m_farm->HeavyCultivatorAggregate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_heavy_cultivator, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
		}
		SimpleEvent_(d1, de_gsa_herbicide1, false, m_farm, m_field);
		break;
	case de_gsa_herbicide1:
		// Together 20% of farmers will do glyphosate spraying, but 15% before sow and the rest before emergence
		if (m_ev->m_lock || m_farm->DoIt(10))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_herbicide1, true, m_farm, m_field);
				break;
			}
			DE_GSA_HERBICIDE1 = true;
		}
		SimpleEvent_(g_date->Date() + 3, de_gsa_preseeding_cultivator, false, m_farm, m_field);
		break;
	case de_gsa_preseeding_cultivator:
		if (m_ev->m_lock || m_farm->DoIt(98))
		{
			if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(4, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_preseeding_cultivator, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_gsa_spring_sow, false, m_farm, m_field);
		break;
	case de_gsa_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_gsa_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, de_gsa_herbicide2, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), de_gsa_cut_to_silage1, false, m_farm, m_field); // Cutting thread
		break;
	case de_gsa_herbicide2:
		// Here comes the herbicide thread
		// Check biomass
		if (m_field->GetGreenBiomass() <= 0)
		{
			// The rest 10% of farmers do glyphosate spraying before emergence
			if ((m_ev->m_lock || (m_farm->DoIt(static_cast<int>((10.0 / 90.0) * 100))) && (DE_GSA_HERBICIDE1 == false)))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_gsa_herbicide2, true, m_farm, m_field);
					break;		
				}
				DE_GSA_HERBI_DATE = g_date->Date();
			}
		}
		// End of thread
		break;
	case de_gsa_cut_to_silage1:
		// Here comes cutting thread
		if (DE_GSA_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, de_gsa_cut_to_silage1, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_gsa_cut_to_silage1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 7), de_gsa_cut_to_silage2, false, m_farm, m_field);
		break;
	case de_gsa_cut_to_silage2:
		if (DE_GSA_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, de_gsa_cut_to_silage2, false, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt(73))
			{
				if (!m_farm->CutToSilage(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_gsa_cut_to_silage2, true, m_farm, m_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_GrasslandSilageAnnual::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}