//
// OWinterBarleyExt.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWinterBarleyExt_H
#define OWinterBarleyExt_H

#define OWBarleyExt_BASE 4500
#define OWBExt_DID_SPRING_SOW  m_field->m_user[0]

typedef enum {
  owbext_start = 1, // Compulsory, start event must always be 1 (one).
  owbext_fertmanure_plant_one = OWBarleyExt_BASE,
  owbext_fertmanure_stock_one,
  owbext_autumn_plough,
  owbext_autumn_harrow,
  owbext_autumn_sow,
  owbext_strigling_one,
  owbext_spring_sow,
  owbext_fertslurry_plant,
  owbext_fertslurry_stock,
  owbext_strigling_two,
  owbext_harvest,
  owbext_straw_chopping,
  owbext_hay_bailing,
  owbext_stubble_harrowing,
  owbext_foobar,
} OWBExtToDo;



class OWinterBarleyExt: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OWinterBarleyExt(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(23,9);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (owbext_foobar - OWBarleyExt_BASE);
	  m_base_elements_no = OWBarleyExt_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  owbext_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  owbext_fertmanure_plant_one = OWBarleyExt_BASE,
			fmc_Fertilizer,	//	  owbext_fertmanure_stock_one,
			fmc_Cultivation,	//	  owbext_autumn_plough,
			fmc_Cultivation,	//	  owbext_autumn_harrow,
			fmc_Others,	//	  owbext_autumn_sow,
			fmc_Cultivation,	//	  owbext_strigling_one,
			fmc_Others,	//	  owbext_spring_sow,
			fmc_Fertilizer,	//	  owbext_fertslurry_plant,
			fmc_Fertilizer,	//	  owbext_fertslurry_stock,
			fmc_Cultivation,	//	  owbext_strigling_two,
			fmc_Harvest,	//	  owbext_harvest,
			fmc_Others,	//	  owbext_straw_chopping,
			fmc_Others,	//	  owbext_hay_bailing,
			fmc_Cultivation	//	  owbext_stubble_harrowing,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OWinterBarley_H
