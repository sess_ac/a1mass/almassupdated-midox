/**
\file
\brief
<B>PLBeet.h This file contains the headers for the Beet class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLBeet.h
//


#ifndef PLBEET_H
#define PLBEET_H

#define PLBEET_BASE 25100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_BE_FERTI_P1	a_field->m_user[1]
#define PL_BE_FERTI_S1	a_field->m_user[2]
#define PL_BE_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_BE_WINTER_PLOUGH		a_field->m_user[4]
#define PL_BE_SPRING_FERTI		a_field->m_user[5]
#define PL_BE_WATER_DATE		a_field->m_user[6]
#define PL_BE_HERBI_DATE		a_field->m_user[7]
#define PL_BE_HERBI1			a_field->m_user[8]
#define PL_BE_HERBI3			a_field->m_user[9]


/** Below is the list of things that a farmer can do if he is growing beet, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_be_start = 1, // Compulsory, must always be 1 (one).
	pl_be_sleep_all_day = PLBEET_BASE,
	pl_be_ferti_p1,
	pl_be_ferti_s1,
	pl_be_stubble_plough,
	pl_be_autumn_harrow1,
	pl_be_autumn_harrow2,
	pl_be_stubble_harrow,
	pl_be_ferti_p2,
	pl_be_ferti_s2,
	pl_be_ferti_p3,
	pl_be_ferti_s3,
	pl_be_winter_plough,
	pl_be_winter_stubble_cultivator_heavy,
	pl_be_spring_harrow,
	pl_be_ferti_p4,
	pl_be_ferti_s4,
	pl_be_ferti_p5,
	pl_be_ferti_s5,
	pl_be_heavy_cultivator,
	pl_be_preseeding_cultivator,
	pl_be_preseeding_cultivator_sow,	
	pl_be_spring_sow,	
	pl_be_harrow_before_emergence,
	pl_be_thinning,
	pl_be_watering1,
	pl_be_watering2,
	pl_be_watering3,
	pl_be_herbicide1,
	pl_be_herbicide2,
	pl_be_herbicide3,
	pl_be_herbicide4,
	pl_be_fungicide1,
	pl_be_fungicide2,
	pl_be_insecticide,
	pl_be_ferti_p6,
	pl_be_ferti_s6,	
	pl_be_ferti_p7,
	pl_be_ferti_s7,
	pl_be_harvest,
	pl_be_ferti_p8,
	pl_be_ferti_s8,
	pl_be_foobar
} PLBeetToDo;


/**
\brief
PLBeet class
\n
*/
/**
See PLBeet.h::PLBeetToDo for a complete list of all possible events triggered codes by the beet management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLBeet: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_be_foobar - PLBEET_BASE);
	   m_base_elements_no = PLBEET_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_be_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_be_sleep_all_day = PLBEET_BASE,
			fmc_Fertilizer,//pl_be_ferti_p1,
			fmc_Fertilizer,//pl_be_ferti_s1,
			fmc_Cultivation,//pl_be_stubble_plough,
			fmc_Cultivation,//pl_be_autumn_harrow1,
			fmc_Cultivation,//pl_be_autumn_harrow2,
			fmc_Cultivation,//pl_be_stubble_harrow,
			fmc_Fertilizer,//pl_be_ferti_p2,
			fmc_Fertilizer,//pl_be_ferti_s2,
			fmc_Fertilizer,//pl_be_ferti_p3,
			fmc_Fertilizer,//pl_be_ferti_s3,
			fmc_Cultivation,//pl_be_winter_plough,
			fmc_Cultivation,//pl_be_winter_stubble_cultivator_heavy,
			fmc_Cultivation,//pl_be_spring_harrow,
			fmc_Fertilizer,//pl_be_ferti_p4,
			fmc_Fertilizer,//pl_be_ferti_s4,
			fmc_Fertilizer,//pl_be_ferti_p5,
			fmc_Fertilizer,//pl_be_ferti_s5,
			fmc_Cultivation,//pl_be_heavy_cultivator,
			fmc_Cultivation,//pl_be_preseeding_cultivator,
			fmc_Cultivation,//pl_be_preseeding_cultivator_sow,
			fmc_Others,//pl_be_spring_sow,
			fmc_Cultivation,//pl_be_harrow_before_emergence,
			fmc_Others,//pl_be_thinning,
			fmc_Watering,//pl_be_watering1,
			fmc_Watering,//pl_be_watering2,
			fmc_Watering,//pl_be_watering3,
			fmc_Herbicide,//pl_be_herbicide1,
			fmc_Herbicide,//pl_be_herbicide2,
			fmc_Herbicide,//pl_be_herbicide3,
			fmc_Herbicide,//pl_be_herbicide4,
			fmc_Fungicide,//pl_be_fungicide1,
			fmc_Fungicide,//pl_be_fungicide2,
			fmc_Insecticide,//pl_be_insecticide,
			fmc_Fertilizer,//pl_be_ferti_p6,
			fmc_Fertilizer,//pl_be_ferti_s6,
			fmc_Fertilizer,//pl_be_ferti_p7,
			fmc_Fertilizer,//pl_be_ferti_s7,
			fmc_Harvest,//pl_be_harvest,
			fmc_Fertilizer,//pl_be_ferti_p8,
			fmc_Fertilizer//pl_be_ferti_s8,

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLBEET_H

