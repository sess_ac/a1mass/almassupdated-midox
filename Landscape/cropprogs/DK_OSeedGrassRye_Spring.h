//
// DK_OSeedGrassRye_Spring.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OSeedGrassRye_Spring_H
#define DK_OSeedGrassRye_Spring_H


#define DK_OSGRS_EVERY_2ND_YEAR a_field->m_user[1]
#define DK_OSGRS_FORCESPRING a_field->m_user[2]
#define DK_OSGRS_BASE 69700
typedef enum {
  dk_osgrs_start = 1, // Compulsory, start event must always be 1 (one).
  dk_osgrs_autumn_plough = DK_OSGRS_BASE,
  dk_osgrs_spring_plough,
  dk_osgrs_sow,
  dk_osgrs_sow_lo,
  dk_osgrs_harrow1,
  dk_osgrs_harrow2,
  dk_osgrs_strigling1,
  dk_osgrs_strigling2,
  dk_osgrs_harvest_cc, 
  dk_osgrs_hay_bailing1,
  dk_osgrs_ferti_s1,
  dk_osgrs_ferti_p1,
  dk_osgrs_cutting1, 
  dk_osgrs_cattle_out1,
  dk_osgrs_cattle_is_out1,
  dk_osgrs_ferti_s2,
  dk_osgrs_ferti_p2,  
  dk_osgrs_weeding, 
  dk_osgrs_swathing,
  dk_osgrs_harvest,
  dk_osgrs_hay_bailing2,
  dk_osgrs_cutting2, 
  dk_osgrs_ferti_s3,
  dk_osgrs_ferti_p3,
  dk_osgrs_ferti_s4,
  dk_osgrs_ferti_p4,
  dk_osgrs_cutting3,
  dk_osgrs_cattle_out2,
  dk_osgrs_cattle_is_out2,
  dk_osgrs_ferti_s2_2y,
  dk_osgrs_ferti_p2_2y,
  dk_osgrs_ferti_s3_2y,
  dk_osgrs_ferti_p3_2y,
  dk_osgrs_weeding_2y,
  dk_osgrs_swathing_2y,
  dk_osgrs_harvest_2y,
  dk_osgrs_hay_bailing2_2y,
  dk_osgrs_foobar
} DK_OSeedGrassRye_SpringToDo;



class DK_OSeedGrassRye_Spring: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OSeedGrassRye_Spring(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,12);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_osgrs_foobar - DK_OSGRS_BASE);
	  m_base_elements_no = DK_OSGRS_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others, // zero element unused but must be here
		  fmc_Others, //  dk_osgrs_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation, //	dk_osgrs_autumn_plough = DK_OSGRS_BASE,
		  fmc_Cultivation, //dk_osgrs_spring_plough,
		  fmc_Others, //  dk_osgrs_sow,
		  fmc_Others, //  dk_osgrs_sow_lo,
		  fmc_Cultivation, //  dk_osgrs_harrow1,
		  fmc_Cultivation, //  dk_osgrs_harrow2,
		  fmc_Cultivation, //  dk_osgrs_strigling1,
		  fmc_Cultivation, //  dk_osgrs_strigling2,
		  fmc_Harvest, //  dk_osgrs_harvest_cc,
		  fmc_Others, // dk_osgrs_hay_bailing1,
		  fmc_Fertilizer, //  dk_osgrs_ferti_s1,
		  fmc_Fertilizer, //  dk_osgrs_ferti_p1,
		  fmc_Cutting, // dk_osgrs_cutting1,
		  fmc_Grazing, //  dk_osgrs_cattle_out1,
		  fmc_Grazing, //    dk_osgrs_cattle_is_out1,
		  fmc_Fertilizer, //  dk_osgrs_ferti_s2,
		  fmc_Fertilizer, //  dk_osgrs_ferti_p2,
		  fmc_Cultivation, //  dk_osgrs_weeding,
		  fmc_Cutting, // dk_osgrs_swathing,
		  fmc_Harvest, //  dk_osgrs_harvest,
		  fmc_Others, // dk_osgrs_hay_bailing2,
		  fmc_Cutting, //   dk_osgrs_cutting2,
		  fmc_Fertilizer, //  dk_osgrs_ferti_s3,
		  fmc_Fertilizer, //  dk_osgrs_ferti_p3,
		  fmc_Fertilizer, //  dk_osgrs_ferti_s4,
		  fmc_Fertilizer, //  dk_osgrs_ferti_p4,
		  fmc_Cutting, //  dk_osgrs_cutting3,
		  fmc_Grazing, //    dk_osgrs_cattle_out2,
		  fmc_Grazing, //   dk_osgrs_cattle_is_out2,
		  fmc_Fertilizer, //  		   dk_osgrs_ferti_s2_2y,
		  fmc_Fertilizer, //    dk_osgrs_ferti_p2_2y,
		  fmc_Fertilizer, //   dk_osgrs_ferti_s3_2y,
		  fmc_Fertilizer, //   dk_osgrs_ferti_p3_2y,
		  fmc_Cultivation, // dk_osgrs_weeding_2y,
		  fmc_Cutting, // dk_osgrs_swathing_2y,
		  fmc_Harvest, //  dk_osgrs_harvest_2y,
		  fmc_Others, // dk_osgrs_hay_bailing2_2y,
					  // No foobar entry
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};

#endif // DK_OSeedGrassRye_Spring_H
