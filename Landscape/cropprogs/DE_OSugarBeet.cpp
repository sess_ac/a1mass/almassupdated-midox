//
// DE_SugarBeet.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, University of Aarhus - modified by Susanne Stein, Julius-Kuehn-Institute
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OSugarBeet.h"


bool DE_OSugarBeet::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool flag = false;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DEOSugarBeet; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_osbe_start:
	{
		DE_OSB_DECIDE_TO_HERB = 1;
		DE_OSB_DECIDE_TO_FI = 1;
		DE_OSB_WINTER_PLOUGH = false;


		m_field->ClearManagementActionSum();

		m_field->SetVegPatchy(true); // Root 

		m_last_date = g_date->DayInYear(15, 11); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 11); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(15, 11); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_osbe_spring_plough))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(10, 10) + isSpring;
		// OK, let's go.
		if (m_ev->m_forcespring) {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, de_osbe_ferti_p2, false, m_farm, m_field);
			break;
		}
		else {
			if (m_farm->IsStockFarmer()) { // StockFarmer
				SimpleEvent_(d1, de_osbe_ferti_s1, false, m_farm, m_field);
			}
			else SimpleEvent_(d1, de_osbe_ferti_p1, false, m_farm, m_field);
			break;
		}
	}
	break;

	// This is the first real farm operation
	case de_osbe_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(.50))
		{
			if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(20, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_ferti_p1, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, de_osbe_autumn_plough, false, m_farm, m_field);
			break;
		}
		else if (m_ev->m_lock || m_farm->DoIt_prob(.40 / .50)) {
			// Queue up the next event -in this case autumn ploughing
			SimpleEvent_(g_date->Date() + 1, de_osbe_autumn_plough, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, de_osbe_ferti_p2, false, m_farm, m_field);
		break; 

	case de_osbe_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(20, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_ferti_s1, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, de_osbe_autumn_plough, false, m_farm, m_field);
			break;
		}
		else if (m_ev->m_lock || m_farm->DoIt_prob(.40 / .50)) {
			// Queue up the next event -in this case autumn ploughing
			SimpleEvent_(g_date->Date() + 1, de_osbe_autumn_plough, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, de_osbe_ferti_s2, false, m_farm, m_field);
		break;

	case de_osbe_autumn_plough:
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(21, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osbe_autumn_plough, true, m_farm, m_field);
			break;
		}
			DE_OSB_WINTER_PLOUGH = true;
		if (m_farm->IsStockFarmer()) { // StockFarmer
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, de_osbe_ferti_s2,  false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, de_osbe_ferti_p2,  false, m_farm, m_field);
		break;
	case de_osbe_ferti_s2:
		if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(25, 2) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_ferti_s2, true, m_farm, m_field);
				break;
		}
		// LKM: Queue up the next event - spring plough (suggests 10% since it is not common) done after the 5th of March and before the 25th of April - if not done, try again +1 day until the 25th of April when we will succeed
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3),
			de_osbe_spring_plough, false);
		break;
	case de_osbe_ferti_p2:
		if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(25, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osbe_ferti_p2, true, m_farm, m_field);
			break;
		}
		// LKM: Queue up the next event - spring plough (suggests 10% since it is not common) done after the 5th of March and before the 25th of April - if not done, try again +1 day until the 25th of April when we will succeed
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3),
			de_osbe_spring_plough, false);
		break;
	case de_osbe_spring_plough:
		// the 10% who did not autumn plough will do spring plough
		if (DE_OSB_WINTER_PLOUGH == false)
		{
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear()))
			{
				SimpleEvent_(g_date->Date() + 1, de_osbe_spring_plough, true, m_farm, m_field);
				break;
			}
		}
		// LKM: Queue up the next event - shallow harrow1 (making seedbed) done after the 28th of March and before the 28th of April - if not done, try again +1 day until the 28th of April when we will succeed
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(28, 3), de_osbe_sharrow1,  false, m_farm, m_field);
		break;
	case de_osbe_sharrow1:
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear()))
		{
			SimpleEvent_(g_date->Date() + 1, de_osbe_sharrow1, true, m_farm, m_field);
			break;
		}
		// LKM: Queue up the next event - sow done the day after, and before 1st of May - if not done, try again +1 day until the 1st of May when we will succeed
		SimpleEvent_(g_date->Date() + 1, de_osbe_sow,  false, m_farm, m_field);
		break;
	case de_osbe_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear()))
		{
			SimpleEvent_(g_date->Date() + 1, de_osbe_sow, true, m_farm, m_field);
			break;
		}
		//Here comes a fork of two 
		SimpleEvent_(g_date->Date() + 15, de_osbe_strigling,  false, m_farm, m_field); // soil treatment thread
		if (m_farm->IsStockFarmer()) {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), de_osbe_fertiFA_S, false, m_farm, m_field);
		}
		else {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), de_osbe_fertiFP_S, false, m_farm, m_field);
		}
		break;
		
		// LKM: Here comes soil treatment thread (main thread)  - strigling done 15 days after
	case de_osbe_strigling:
		//LKM: do strigling before 15th of May - if not done, try again +1 day until the 27th of May when we succeed
		if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osbe_strigling, true, m_farm, m_field);
			break;
		}
		// LKM: Queue up the next event - harrow done 15 days after
		SimpleEvent_(g_date->Date() + 15, de_osbe_harrow,  false, m_farm, m_field);
		break;
	case de_osbe_harrow:
		//LKM: do harrow before 30th May - if not done, try again +1 day until the 30th of May when we succeed
		if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osbe_harrow, true, m_farm, m_field);
			break;
		}
		// LKM: Queue up the next event - row cultivation1 done 15 days after
		SimpleEvent_(g_date->Date() + 15, de_osbe_row_cultivation1,  false, m_farm, m_field);
		break;
	case de_osbe_row_cultivation1:
		//LKM: do row cultivaton before 15th of June - if not done, try again +1 day until the 15th of June when we succeed
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osbe_row_cultivation1, true, m_farm, m_field);
			break;
		}
		// LKM: Queue up the next event - row cultivation2 done 15 days after
		SimpleEvent_(g_date->Date() + 15, de_osbe_row_cultivation2,  false, m_farm, m_field);
		break;
		
	case de_osbe_row_cultivation2:
		//LKM: do row cultivaton before 30th of June - if not done, try again +1 day until the 30th of June when we succeed
		if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osbe_row_cultivation2, true, m_farm, m_field);
			break;
		}
		// LKM: Queue up the next event - row cultivation3 done 15 days after
		SimpleEvent_(g_date->Date() + 15, de_osbe_row_cultivation3,  false, m_farm, m_field); // MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_osbe_water1, false, m_farm, m_field); // watering thread
		break;

		// Here comes water thread
	case de_osbe_water1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10))
		{
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 7) -
				g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_water1, true, m_farm, m_field);
				break;
			}
		}

		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 7)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 7);
		}
		SimpleEvent_(d1, de_osbe_water2, false, m_farm, m_field);
		break;
		//LKM: do water2 before 30 of August - if not done, try again +1 day until the the 30 of July when we succeed
	case de_osbe_water2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.3))
		{
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 8) -
				g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_water2, true, m_farm, m_field);
				break;
			}
		}
		break;
		//End of thread

	case de_osbe_row_cultivation3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			//LKM: do row cultivaton before 15th of July - if not done, try again +1 day until the the 15th of July when we succeed
			if (!m_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_row_cultivation3, true, m_farm, m_field);
				break;
			}
		}
		break; // end of thread
		
	case de_osbe_fertiFP_S:
		// Here comes the microelements thread
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_fertiFP_S, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_osbe_fertiFP_B, false, m_farm, m_field);
		break;

	case de_osbe_fertiFP_B:
		// Here comes the microelements thread
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_fertiFP_B, true, m_farm, m_field);
				break;
			}
		}
		// LKM: Queue up the next event - manual weeding (remove wild beets) done after 15th of July
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_osbe_manualweeding1, false, m_farm, m_field);
		break;

	case de_osbe_fertiFA_S:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_fertiFA_S, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_osbe_fertiFA_B, false, m_farm, m_field);
		break;

	case de_osbe_fertiFA_B:
		// Here comes the microelements thread
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_fertiFA_B, true, m_farm, m_field);
				break;
			}
		}
		// LKM: Queue up the next event - manual weeding (remove wild beets) done after 1st of July
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_osbe_manualweeding1,  false, m_farm, m_field);
		break;

		//LKM: do manual weeding before 1st of August - if not done, try again +1 day until the 1st of August when we succeed (need af new function!)
	case de_osbe_manualweeding1:
		if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(1, 8) -
			g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osbe_manualweeding1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 8), de_osbe_manualweeding2, false, m_farm, m_field);
		break;
	case de_osbe_manualweeding2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->ManualWeeding(m_field, 0.0, g_date->DayInYear(31, 8) -
				g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_osbe_manualweeding2, true, m_farm, m_field);
				break;
			}
		}
		// LKM: Queue up the next event - harvest done after the 15th of September
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), de_osbe_harvest,  false, m_farm, m_field);
		break;
		// LKM: do harvest before 15th of November - if not done, try again + 1 day until the 15th of November when we succeed
	case de_osbe_harvest:
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_osbe_harvest, true, m_farm, m_field);
			break;
		}
		m_field->SetVegPatchy( false);
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "DE_OSugarbeet::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}





	


