/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University, modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_Orchard.cpp This file contains the source for the DE_Orchard class</B> \n
*/
/**
\file
by Chris J. Topping and Elzbieta Ziolkowska \n
 modified by Luna Kondrup Marcussen \n
 Version of November 2022 \n
 All rights reserved. \n
 \n
*/
//
// DE_Orchard.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_Orchard.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_orchard_on;
extern CfgFloat cfg_pest_product_1_amount;
extern Landscape* g_landscape_p;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_Orchard::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEOrchard; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_orch_start:
	{
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(15, 9); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 9); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(15, 9);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_orch_ferti_s2))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 11) + isSpring;
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on sandy (2) or clay (1) soils
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent(d1, de_orch_ferti_s1, false);
			break;
		}
		else SimpleEvent(d1, de_orch_ferti_p1, false);
		break;
	}
	break;

	// This is the first real farm operation
	case de_orch_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30))
		{
			if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(31, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, de_orch_ferti_p2, false, m_farm, m_field);
		break;

	case de_orch_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30))
		{
			if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(31, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, de_orch_ferti_s2, false, m_farm, m_field);
		break;

	case de_orch_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_orch_ferti_p3, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 3), de_orch_fungicide1, false, m_farm, m_field); // main thread
		break;

	case de_orch_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_orch_ferti_s3, false, m_farm, m_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 3), de_orch_fungicide1, false, m_farm, m_field); // main thread
		break;

	case de_orch_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.3))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4), de_orch_ferti_p4, false, m_farm, m_field);
		break;

	case de_orch_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.3))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4), de_orch_ferti_s4, false, m_farm, m_field);
		break;

	case de_orch_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.6))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_orch_ferti_p5, false, m_farm, m_field);
		break;

	case de_orch_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.6))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_orch_ferti_s5, false, m_farm, m_field);
		break;

	case de_orch_ferti_p5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_p5, true, m_farm, m_field);
				break;
			}
		}
		break; 

	case de_orch_ferti_s5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_ferti_s5, true, m_farm, m_field);
				break;
			}
		}
		break;

	case de_orch_fungicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide2, false, m_farm, m_field);
		break;

	case de_orch_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(8, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 3, de_orch_fungicide3, false, m_farm, m_field);
		break;

	case de_orch_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(11, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 3, de_orch_fungicide4, false, m_farm, m_field);
		break;

	case de_orch_fungicide4:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_orch_insecticide1, false, m_farm, m_field);
		break;

	case de_orch_insecticide1:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(15, 4) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_orch_insecticide1, true, m_farm, m_field);
				break;
			}
		}

		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide5, false, m_farm, m_field);
		break;

	case de_orch_fungicide5:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(22, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide5, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide6, false, m_farm, m_field);
		break;

	case de_orch_fungicide6:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide6, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_herbicide, false, m_farm, m_field);
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide7, false, m_farm, m_field); // main thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_orch_insecticide2, false, m_farm, m_field);
		break;

	case de_orch_herbicide:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.6)) {
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_herbicide, true, m_farm, m_field);
				break;
			}
		}
		break;

	case de_orch_insecticide2:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(7, 5) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_orch_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, de_orch_insecticide3, false, m_farm, m_field);
		break;

	case de_orch_insecticide3:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(22, 5) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_orch_insecticide3, true, m_farm, m_field);
				break;
			}
		}
		break;

	case de_orch_fungicide7:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide7, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide8, false, m_farm, m_field); // main thread
		break;

	case de_orch_fungicide8:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide8, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide9, false, m_farm, m_field); // main thread
		break;

	case de_orch_fungicide9:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide9, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide10, false, m_farm, m_field); // main thread
		break;

	case de_orch_fungicide10:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide10, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date(), de_orch_insecticide4, false, m_farm, m_field);
		break;

	case de_orch_insecticide4:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(31, 5) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_orch_insecticide4, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide11, false, m_farm, m_field); // main thread
		SimpleEvent_(d1, de_orch_insecticide5, false, m_farm, m_field);
		break;

	case de_orch_insecticide5:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(15, 6) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_orch_insecticide5, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date()+14, de_orch_insecticide6, false, m_farm, m_field);
		break;


	case de_orch_insecticide6:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(30, 6) - g_date->DayInYear();
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_orch_insecticide6, true, m_farm, m_field);
				break;
			}
		}
		// of thread
		break;

	case de_orch_fungicide11:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide11, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide12, false, m_farm, m_field); // main thread
		break;

	case de_orch_fungicide12:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(14, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide12, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide13, false, m_farm, m_field); // main thread
		break;

	case de_orch_fungicide13:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) { 
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(21, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide13, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide14, false, m_farm, m_field); // main thread
		break;

	case de_orch_fungicide14:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) { 
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide14, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_fungicide15, false, m_farm, m_field); // main thread
		break;

	case de_orch_fungicide15:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(7, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide15, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), de_orch_fungicide16, false, m_farm, m_field); 
		break;

	case de_orch_fungicide16:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide16, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 8), de_orch_fungicide17, false, m_farm, m_field);
		break;

	case de_orch_fungicide17:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide17, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 9), de_orch_fungicide18, false, m_farm, m_field);
		break;

	case de_orch_fungicide18:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_orch_fungicide18, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_orch_harvest, false, m_farm, m_field);
		break;

	case de_orch_harvest:
		if (!a_farm->FruitHarvest(m_field, 0.0, g_date->DayInYear(1, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, de_orch_harvest, true);
			break;
		}
			done = true;
			break;
		
	default:
		g_msg->Warn(WARN_BUG, "DE_ORCHARD::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}
