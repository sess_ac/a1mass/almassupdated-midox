/**
\file
\brief
<B>PTTurnipGrazed.h This file contains the headers for the TurnipGrazed class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTTurnipGrazed.h
//


#ifndef PTTURNIPGRAZED_H
#define PTTURNIPGRAZED_H

#define PTTURNIPGRAZED_BASE 30500
/**
\brief A flag used to indicate cattle out dates
*/
#define PT_FL1_CATTLEOUT_DATE a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing turnip grazed, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_tg_start = 1, // Compulsory, must always be 1 (one).
	pt_tg_sleep_all_day = PTTURNIPGRAZED_BASE,
	pt_tg_stubble_harrow,
	pt_tg_autumn_plough,
	pt_tg_autumn_harrow,
	pt_tg_autumn_sow,
	pt_tg_cattle_out,
	pt_tg_cattle_is_out,

} PTTurnipGrazedToDo;


/**
\brief
PTTurnipGrazed class
\n
*/
/**
See PTTurnipGrazed.h::PTTurnipGrazedToDo for a complete list of all possible events triggered codes by the TurnipGrazed management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTTurnipGrazed: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTTurnipGrazed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,11 ); // change back to 10.11 when tested in rotation
   }
};

#endif // PTTURNIPGRAZED_H

