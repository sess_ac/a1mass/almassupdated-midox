/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>FI_FI_NaturalGrassland.cpp This file contains the source for the FI_NaturalGrassland1 class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 \n
*/
//
// FI_NaturalGrassland.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FI_NaturalGrassland.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_FI_NaturalGrassland_SkScrapes("DK_CROP_NG_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_FI_NaturalGrassland1_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_NG_InsecticideDay;
extern CfgInt   cfg_NG_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool FI_NaturalGrassland::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case fi_ng_start:
	{
		FI_NG_YEARS_AFTER_SOW = 0; //Here FI_NG_YEARS_AFTER_SOW flag should get randomly for each field a value from 1 to 5 ->
									//THIS PART SHOULD BE SOMEWHERE ELSE OUTSIDE THE PLAN, SO THE STARTING CONDITIONS FOR FIELDS WITH PERMANENT GRASS (YEAR AFTER SOWING)
									// IS SET ONLY ONCE AT THE SIMULATION START!!!
		a_field->ClearManagementActionSum();

// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {
			//Checking the future...
			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(30, 12) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "FINaturalGrassland::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "FINaturalGrassland::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "FINaturalGrassland::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				//only for the first year
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), fi_ng_sow, false);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // OK, let's go.
		// Here we queue up the first event
		//Each field has assign randomly a FI_NG_YEARS_AFTER_SOW flag value from 1 to 5

		if ((FI_NG_YEARS_AFTER_SOW + g_date->GetYearNumber()) % 2 == 2)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, fi_ng_sow, false);
		}
		else if ((FI_NG_YEARS_AFTER_SOW + g_date->GetYearNumber()) % 2 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, fi_ng_grazing1, false);
		}
		else if((FI_NG_YEARS_AFTER_SOW + g_date->GetYearNumber()) % 2 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent(d1, fi_ng_sleep_all_day, false);
		}
		break;
	}
	break; // this thread is only done the first year
	case fi_ng_sow:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				// If we don't succeed on the first try, then try and try again (until 10/10 when we will succeed)
				SimpleEvent(g_date->Date() + 1, fi_ng_sow, true);
				break;
			}
		}
		//here comes next event:
				SimpleEvent(g_date->Date(), fi_ng_slurry, false);
				break;
	case fi_ng_slurry: //suggest 50% does this and the rest choose NPK fertilizer (crop scheme only say: 100% do Slurry/NPK)
		if (a_ev->m_lock || a_farm->DoIt_prob(.50)) {
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ng_slurry, true);
				break;
			}
		}
		else if (a_ev->m_lock || a_farm->DoIt_prob(.50/.50)){
			SimpleEvent(g_date->Date(), fi_ng_npk, false); 
			break;
		}
	case fi_ng_npk: //suggest 50% does this and the rest choose NPK fertilizer (crop scheme only say: 100% do Slurry/NPK)
		if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_ng_npk, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), fi_ng_cutting, false);
		break;
	case fi_ng_cutting: // 100% do this
		if (!a_farm->CutToSilage(a_field, 0.0,
			g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_ng_cutting, true);
			break;
		}
		done = true;
		break;
	case fi_ng_sleep_all_day: // every second year nothing is done
		done = true;
		break;
	case fi_ng_grazing1: // 100% cut or grazed - suggests 50% each // every second year this thread
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->CattleOut(a_field, 0.0,
				g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, fi_ng_grazing1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, fi_ng_cattle_is_out1, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), fi_ng_cutting1, false);
		break;
	case fi_ng_cutting1: // 
		if (!a_farm->CutToSilage(a_field, 0.0,
			g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, fi_ng_cutting1, true);
			break;
		}
		done = true;
		break;
	case fi_ng_cattle_is_out1:    // Keep the cattle out there
						   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear(), g_date->DayInYear(30, 9))) {
			SimpleEvent(g_date->Date() + 1, fi_ng_cattle_is_out1, false);
			break;
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
	default:
		g_msg->Warn(WARN_BUG, "FI_NaturalGrassland::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}