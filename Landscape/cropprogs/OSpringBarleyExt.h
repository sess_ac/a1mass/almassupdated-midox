//
// OSpringBarleyExt.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OSpringBarleyExt_H
#define OSpringBarleyExt_H

#define OSBarleyExt_BASE 4200
#define OSBEXT_ISAUTUMNPLOUGH    m_field->m_user[0]
#define OSBEXT_SOW_DATE          m_field->m_user[1]
#define OSBEXT_STRIGLING_DATE    m_field->m_user[2]

typedef enum {
  osbext_start = 1, // Compulsory, start event must always be 1 (one).
  osbext_ferti_p1 = OSBarleyExt_BASE,
  osbext_ferti_p2,
  osbext_ferti_s1,
  osbext_ferti_s2,
  osbext_harvest,
  osbext_spring_plough_p,
  osbext_spring_plough_s,
  osbext_autumn_plough,
  osbext_spring_harrow,
  osbext_spring_roll,
  osbext_spring_sow1,
  osbext_spring_sow2,
  osbext_hay,
  osbext_strigling1,
  osbext_strigling2,
  osbext_strigling3,
  osbext_straw_chopping,
  osbext_foobar,
} OSBExtToDo;



class OSpringBarleyExt: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OSpringBarleyExt(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (osbext_foobar - OSBarleyExt_BASE);
	  m_base_elements_no = OSBarleyExt_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  osbext_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  osbext_ferti_p1 = OSBarleyExt_BASE,
			fmc_Fertilizer,	//	  osbext_ferti_p2,
			fmc_Fertilizer,	//	  osbext_ferti_s1,
			fmc_Fertilizer,	//	  osbext_ferti_s2,
			fmc_Harvest,	//	  osbext_harvest,
			fmc_Cultivation,	//	  osbext_spring_plough_p,
			fmc_Cultivation,	//	  osbext_spring_plough_s,
			fmc_Cultivation,	//	  osbext_autumn_plough,
			fmc_Cultivation,	//	  osbext_spring_harrow,
			fmc_Cultivation,	//	  osbext_spring_roll,
			fmc_Others,	//	  osbext_spring_sow1,
			fmc_Others,	//	  osbext_spring_sow2,
			fmc_Others,	//	  osbext_hay,
			fmc_Cultivation,	//	  osbext_strigling1,
			fmc_Cultivation,	//	  osbext_strigling2,
			fmc_Cultivation,	//	  osbext_strigling3,
			fmc_Others	//	  osbext_straw_chopping,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OSpringBarley_H
