//
// Triticale.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef Triticale_H
#define Triticale_H

#define TRITICALE_BASE 7400

#define TRI_OCCUP_DATE        m_field->m_user[0]
#define TRI_WATER_DATE        m_field->m_user[1]
#define TRI_DECIDE_TO_HERB       m_field->m_user[2]


typedef enum {
  tri_start = 1, // Compulsory, start event must always be 1 (one).
  tri_fa_manure = TRITICALE_BASE,
  tri_autumn_plough,
  tri_autumn_harrow,
  tri_autumn_sow,
  tri_autumn_roll,
  tri_herbi_one,
  tri_spring_roll,
  tri_fa_npk,
  tri_fa_slurry,
  tri_herbi_two,
  tri_strigling,
  tri_GR_one,
  tri_GR_two,
  tri_water,
  tri_fungicide,
  tri_insecticide,
  tri_harvest,
  tri_chopping,
  tri_hay_turning,
  tri_hay_bailing,
  tri_stubble_harrow,
  tri_foobar
} TriticaleToDo;



class Triticale: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  Triticale(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(5,10);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (tri_foobar - TRITICALE_BASE);
	  m_base_elements_no = TRITICALE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//tri_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//tri_fa_manure = TRITICALE_BASE,
		  fmc_Cultivation,//tri_autumn_plough,
		  fmc_Cultivation,//tri_autumn_harrow,
		  fmc_Others,//tri_autumn_sow,
		  fmc_Others,//tri_autumn_roll,
		  fmc_Herbicide,//tri_herbi_one,
		  fmc_Others,//tri_spring_roll,
		  fmc_Fertilizer,//tri_fa_npk,
		  fmc_Fertilizer,//tri_fa_slurry,
		  fmc_Herbicide,//tri_herbi_two,
		  fmc_Cultivation,//tri_strigling,
		  fmc_Others,//tri_GR_one,
		  fmc_Others,//tri_GR_two,
		  fmc_Watering,//tri_water,
		  fmc_Fungicide,//tri_fungicide,
		  fmc_Insecticide,//tri_insecticide,
		  fmc_Harvest,//tri_harvest,
		  fmc_Others,//tri_chopping,
		  fmc_Others,//tri_hay_turning,
		  fmc_Others,//tri_hay_bailing,
		  fmc_Cultivation//tri_stubble_harrow

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // Triticale_H
