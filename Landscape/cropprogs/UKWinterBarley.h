/**
\file
\brief
<B>UKWinterBarley.h This file contains the headers for the WinterBarley class</B> \n
*/
/**
\file
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKWinterBarley.h
//


#ifndef UKWINTERBARLEY_H
#define UKWINTERBARLEY_H

#define UKWINTERBARLEY_BASE 45700
/**
\brief A flag used to indicate autumn ploughing status
*/
#define UK_WB_FUNGII	a_field->m_user[1]


/** Below is the list of things that a farmer can do if he is growing winter barley, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	uk_wb_start = 1, // Compulsory, must always be 1 (one).
	uk_wb_sleep_all_day = UKWINTERBARLEY_BASE,
	uk_wb_autumn_harrow,
	uk_wb_winter_plough,
	uk_wb_herbicide1,
	uk_wb_preseeding_cultivator_sow,
	uk_wb_ferti_s1, // Slurry
	uk_wb_ferti_p1,
	uk_wb_ferti_s2, // NI
	uk_wb_ferti_p2,
	uk_wb_ferti_s3, // NII
	uk_wb_ferti_p3,
	uk_wb_herbicide2,
	uk_wb_herbicide3,
	uk_wb_growth_regulator1,
	uk_wb_growth_regulator2,
	uk_wb_fungicide1,
	uk_wb_fungicide2,
	uk_wb_insecticide1,
	uk_wb_harvest,
	uk_wb_straw_chopping,
	uk_wb_hay_bailing,
	uk_wb_foobar,
} UKWinterBarleyToDo;


/**
\brief
UKWinterBarley class
\n
*/
/**
See UKWinterBarley.h::UKWinterBarleyToDo for a complete list of all possible events triggered codes by the winter barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKWinterBarley : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	UKWinterBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 1/9
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(20, 9);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (uk_wb_foobar - UKWINTERBARLEY_BASE);
		m_base_elements_no = UKWINTERBARLEY_BASE - 2;
		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others, // this needs to be at the zero line
			 fmc_Others,//uk_wb_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Others,//uk_wb_sleep_all_day = UKWINTERBARLEY_BASE,
			 fmc_Cultivation,//uk_wb_autumn_harrow,
			 fmc_Cultivation,//uk_wb_winter_plough,
			 fmc_Herbicide,//uk_wb_herbicide1,
			 fmc_Cultivation,//uk_wb_preseeding_cultivator_sow,
			 fmc_Fertilizer,//uk_wb_ferti_s1, // Slurry
			 fmc_Fertilizer,//uk_wb_ferti_p1,
			 fmc_Fertilizer,//uk_wb_ferti_s2, // NI
			 fmc_Fertilizer,//uk_wb_ferti_p2,
			 fmc_Fertilizer,//uk_wb_ferti_s3, // NII
			 fmc_Fertilizer,//uk_wb_ferti_p3,
			 fmc_Herbicide,//uk_wb_herbicide2,
			 fmc_Herbicide,//uk_wb_herbicide3,
			 fmc_Others,//uk_wb_growth_regulator1,
			 fmc_Others,//uk_wb_growth_regulator2,
			 fmc_Fungicide,//uk_wb_fungicide1,
			 fmc_Fungicide,//uk_wb_fungicide2,
			 fmc_Insecticide,//uk_wb_insecticide1,
			 fmc_Harvest,//uk_wb_harvest,
			 fmc_Others,//uk_wb_straw_chopping,
			 fmc_Others,//uk_wb_hay_bailing,
		};
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // UKWINTERBARLEY_H

