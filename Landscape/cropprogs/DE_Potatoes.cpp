/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University, modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_Potatoes.cpp This file contains the source for the DE_Potatoes class</B> \n
*/
/**
\file
 by Chris J. Topping and Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_Potatoes.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_Potatoes.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_potatoes_on;
extern CfgFloat cfg_pest_product_1_amount;
extern Landscape* g_landscape_p;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_Potatoes::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEPotatoes; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_pot_start:
	{
		DE_POT_FERTI_P1 = false;
		DE_POT_FERTI_S1 = false;
		DE_POT_STUBBLE_PLOUGH = false;
		DE_POT_DID_STRIG1 = false;
		DE_POT_HILL_DATE = 0;
		DE_POT_DID_HILL = false;
		DE_POT_DID_DESS = false;

		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(20, 9); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 9); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(20, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(de_pot_spring_plough))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(20, 7) + isSpring;
		// OK, let's go.
		// Here we queue up the first event
		// stock or arable farmer
		if (m_ev->m_forcespring) {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_pot_spring_harrow, false, m_farm, m_field);
			break;
		}
		else {
			if (m_farm->IsStockFarmer()) { // StockFarmer
				SimpleEvent_(d1, de_pot_ferti_s1, false, m_farm, m_field);
			}
			else SimpleEvent_(d1, de_pot_ferti_p1, false, m_farm, m_field);
			break;
		}
	}
	break;

	// This is the first real farm operation
	case de_pot_ferti_p1:
		// In total 10% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (5%) do it now
		if (m_ev->m_lock || m_farm->DoIt(5))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again 
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p1, true, m_farm, m_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remember who already did it
				DE_POT_FERTI_P1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, de_pot_stubble_plough, false, m_farm, m_field);
		break;
	case de_pot_ferti_s1:
		// In total 40% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (40%) do it now
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s1, true, m_farm, m_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				DE_POT_FERTI_S1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, de_pot_stubble_plough, false, m_farm, m_field);
		break;
	case de_pot_stubble_plough:
		// 30% will do stubble plough, but rest will get away with non-inversion cultivation
		if (m_ev->m_lock || m_farm->DoIt(30))
		{
			if (!m_farm->StubblePlough(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_stubble_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				// 30% of farmers will do this, but the other 70% won't so we need to remember whether we are in one or the other group
				DE_POT_STUBBLE_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->Date() + 1, de_pot_autumn_harrow1, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_stubble_harrow, false, m_farm, m_field);
		break;
	case de_pot_autumn_harrow1:
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_autumn_harrow1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 2, de_pot_autumn_harrow2, false, m_farm, m_field);
		break;
	case de_pot_autumn_harrow2:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->Date() + 7 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_autumn_harrow2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_pot_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_pot_ferti_p2, false, m_farm, m_field);
		break;
	case de_pot_stubble_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_stubble_harrow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_pot_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_pot_ferti_p2, false, m_farm, m_field);
		break;
	case de_pot_ferti_p2:
		// In total 10% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (5%) do it now (if haven't done before)
		if ((m_ev->m_lock || m_farm->DoIt(static_cast<int>((5.0 / 95.0) * 100))) && (DE_POT_FERTI_P1 == false))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_winter_plough, false, m_farm, m_field);
		break;
	case de_pot_ferti_s2:
		// In total 80% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (40%) do it now (if haven't done before)
		if ((m_ev->m_lock || m_farm->DoIt(static_cast<int>((40.0 / 60.0) * 100))) && (DE_POT_FERTI_S1 == false))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_winter_plough, false, m_farm, m_field);
		break;
	case de_pot_winter_plough:
		if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_winter_plough, true, m_farm, m_field);
			break;
		}

		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_pot_spring_harrow, false, m_farm, m_field);
		break;
	case de_pot_spring_harrow:
		if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_spring_harrow, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 3, de_pot_spring_harrow2, false, m_farm, m_field);
		break;
	case de_pot_spring_harrow2:
		if ((m_ev->m_lock) || m_farm->DoIt(20))
		{
			if (!m_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_spring_harrow2, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_pot_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, de_pot_ferti_p3, false, m_farm, m_field);
		break;
	case de_pot_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_pot_ferti_p4, false, m_farm, m_field);
		break;
	case de_pot_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt(70))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_pot_ferti_s4, false, m_farm, m_field);
		break;
	case de_pot_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_spring_plough, false, m_farm, m_field);
		break;
	case de_pot_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt(80))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_spring_plough, false, m_farm, m_field);
		break;
	case de_pot_spring_plough:
		if (m_ev->m_lock || m_farm->DoIt(90))
		{
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_spring_plough, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
		}
		SimpleEvent_(d1, de_pot_bed_forming, false, m_farm, m_field);
		break;
	case de_pot_bed_forming:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_bed_forming, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date(), de_pot_spring_planting, false, m_farm, m_field);
		break;
	case de_pot_spring_planting:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_spring_planting, true, m_farm, m_field);
			break;
		}
		// Fork of events
		SimpleEvent_(g_date->Date() + 5, de_pot_hilling1, false, m_farm, m_field); // Strigling followed by hilling and herbicides
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 5)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 5);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, de_pot_ferti_s5, false, m_farm, m_field);	// N before emergence
		}
		else SimpleEvent_(d1, de_pot_ferti_p5, false, m_farm, m_field);
		SimpleEvent_(g_date->Date() + 25, de_pot_fungicide1, false, m_farm, m_field); // Fungicide treat = MAIN TREAT // EZ: min 3 weeks after planting to have already plants growing
		SimpleEvent_(g_date->Date() + 25 + m_date_modifier, de_pot_insecticide, false, m_farm, m_field); // Insecticide treat // EZ: min 3 weeks after planting to have already plants growing
		break;
	case de_pot_hilling1:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (!m_farm->StriglingHill(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_hilling1, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 5, de_pot_hilling2, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_hilling4, false, m_farm, m_field);
		break;
	case de_pot_hilling2:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || (m_farm->DoIt(70)))
			{
				if (!m_farm->StriglingHill(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_hilling2, true, m_farm, m_field);
					break;
				}
				DE_POT_DID_STRIG1 = true;
				// Queue up the third strigling_hill event after 5 days
				SimpleEvent_(g_date->Date() + 5, de_pot_hilling3, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_hilling4, false, m_farm, m_field);
		break;
	case de_pot_hilling3:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || (m_farm->DoIt(71) && DE_POT_DID_STRIG1)) // which gives 50% of all farmers
			{
				if (!m_farm->StriglingHill(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_hilling3, true, m_farm, m_field);
					break;
				}
				SimpleEvent_(g_date->Date() + 5, de_pot_herbicide1, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_hilling4, false, m_farm, m_field);
		break;
	case de_pot_herbicide1:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(93))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_herbicide1, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->Date() + 1, de_pot_hilling4, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, de_pot_hilling4, false, m_farm, m_field);
		break;
	case de_pot_hilling4:
		if (m_ev->m_lock || m_farm->DoIt(38))
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_pot_hilling4, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_hilling4, true, m_farm, m_field);
					break;
				}
				DE_POT_HILL_DATE = g_date->Date();
				SimpleEvent_(g_date->Date() + 5, de_pot_hilling5, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 3, de_pot_herbicide2, false, m_farm, m_field);
		break;
	case de_pot_hilling5:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_hilling5, true, m_farm, m_field);
			break;
		}
		DE_POT_HILL_DATE = g_date->Date();
		SimpleEvent_(g_date->Date() + 5, de_pot_hilling6, false, m_farm, m_field);
		break;
	case de_pot_hilling6:
		if (!m_farm->HillingUp(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_hilling6, true, m_farm, m_field);
			break;
		}
		DE_POT_HILL_DATE = g_date->Date();
		DE_POT_DID_HILL = true;

		SimpleEvent_(g_date->Date() + 5, de_pot_herbicide2, false, m_farm, m_field);
		break;
	case de_pot_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt(40))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_pot_ferti_s5:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(80))
			{
				if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s5, true, m_farm, m_field);
					break;
				}
			}
			d1 = g_date->Date() + 7;
			if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
				d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
			}
			SimpleEvent_(d1, de_pot_ferti_s6, false, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
		}
		SimpleEvent_(d1, de_pot_ferti_s6, false, m_farm, m_field);
		break;

	case de_pot_ferti_p5:
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt(80))
			{
				if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p5, true, m_farm, m_field);
					break;
				}
			}
			d1 = g_date->Date() + 7;
			if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
				d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
			}
			SimpleEvent_(d1, de_pot_ferti_p6, false, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
		}
		SimpleEvent_(d1, de_pot_ferti_p6, false, m_farm, m_field);
		break;
	case de_pot_ferti_s6:
		if (m_ev->m_lock || m_farm->DoIt(53))
		{
			if (DE_POT_HILL_DATE >= g_date->Date() - 2) { // Should by at least 3 days after hilling
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s6, false, m_farm, m_field);
			}
			else
			{
				if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s6, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case de_pot_ferti_p6:
		if (m_ev->m_lock || m_farm->DoIt(53))
		{
			if (DE_POT_HILL_DATE >= g_date->Date() - 2) { // Should by at least 3 days after hilling
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p6, false, m_farm, m_field);
			}
			else
			{
				if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p6, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case de_pot_fungicide1:
		// Here comes the fungicide thread = MAIN THREAD
		if (DE_POT_HILL_DATE >= g_date->Date() - 2) { // Should by at least 3 days after hilling
			SimpleEvent_(g_date->Date() + 1, de_pot_fungicide1, true, m_farm, m_field);
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt(78))
			{
				if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, de_pot_fungicide1, true, m_farm, m_field);
					break;
				}
			}
			SimpleEvent_(g_date->Date() + 10, de_pot_fungicide2, false, m_farm, m_field);
			break;
		}
		break;
	case de_pot_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt(63))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, de_pot_fungicide3, false, m_farm, m_field);
		break;
	case de_pot_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt(63))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, de_pot_fungicide4, false, m_farm, m_field);
		break;
	case de_pot_fungicide4:
		if (m_ev->m_lock || m_farm->DoIt(38))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_fungicide4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 8), de_pot_dessication1, false, m_farm, m_field);
		break;
	case de_pot_insecticide:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75 * g_landscape_p->SupplyPestIncidenceFactor()))
		{
			if (DE_POT_HILL_DATE >= g_date->Date() - 2) {
				SimpleEvent_(g_date->Date() + 1, de_pot_insecticide, false, m_farm, m_field);
			}
			else
			{
				// here we check whether we are using ERA pesticide or not
				d1 = g_date->DayInYear(20, 7) - g_date->DayInYear();
				if (!cfg_pest_potatoes_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_pot_insecticide, true, m_farm, m_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case de_pot_dessication1:
		// Here the MAIN thread continues
		// This is the desiccation thread with glyphosate or diquat
		if (m_ev->m_lock || m_farm->DoIt(35))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_dessication1, true, m_farm, m_field);
				break;
			}
			DE_POT_DID_DESS = true;
		}
		SimpleEvent_(g_date->Date() + 7, de_pot_dessication2, false, m_farm, m_field);
		break;
	case de_pot_dessication2:
		if (m_ev->m_lock || (m_farm->DoIt(17) && DE_POT_DID_DESS)) //which is 6% of all farmers
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(7, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_dessication2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_pot_harvest, false, m_farm, m_field);
		break;
	case de_pot_harvest:
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_pot_harvest, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s7, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p7, false, m_farm, m_field);
		break;
	case de_pot_ferti_p7:
		if (m_ev->m_lock || m_farm->DoIt(23))
		{
			if (!m_farm->FP_Calcium(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_p7, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	case de_pot_ferti_s7:
		if (m_ev->m_lock || m_farm->DoIt(23))
		{
			if (!m_farm->FA_Calcium(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_pot_ferti_s7, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_OPotatoes::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}
