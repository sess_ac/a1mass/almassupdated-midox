/**
\file
\brief
<B>DK_OCloverGrassGrazed2.h This file contains the headers for the DK_OCloverGrassGrazed2 class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_OCloverGrassGrazed2.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OCLOVERGRASSGRAZED2_H
#define DK_OCLOVERGRASSGRAZED2_H

#define DK_OC2_BASE 61900
/**
\THIS CODE IS FOR YEARS AFTER ESTABLISHMENT OF CLOVERGRASS (BOTH SOWED AS SOLE CROP OR W. COVER CROP IN SPRING/SUMMER) 
*/

/** Below is the list of things that a farmer can do if he is growing oclovergrassgrazed2, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_oc2_start = 1, // Compulsory, must always be 1 (one).
	dk_oc2_sleep_all_day = DK_OC2_BASE,
	dk_oc2_manure1_s,
	dk_oc2_manure1_p,
	dk_oc2_water,
	dk_oc2_cutting1,
	dk_oc2_grazing1,
	dk_oc2_cattle_is_out1,
	dk_oc2_grazing2,
	dk_oc2_cattle_is_out2,
	dk_oc2_manure2_s,
	dk_oc2_manure2_p,
	dk_oc2_cutting2,
	dk_oc2_manure3_s,
	dk_oc2_manure3_p,
	dk_oc2_cutting3,
	dk_oc2_cutting4,
	dk_oc2_cutting5,
	dk_oc2_cutting6,
	dk_oc2_foobar,
} DK_OCloverGrassGrazed2ToDo;


/**
\brief
DK_OCloverGrassGrazed2 class
\n
*/
/**
See DK_OCloverGrassGrazed2.h::DK_OCloverGrassGrazed2ToDo for a complete list of all possible events triggered codes by the DK_OCloverGrassGrazed2 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OCloverGrassGrazed2 : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OCloverGrassGrazed2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 4);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_oc2_foobar - DK_OC2_BASE);
		m_base_elements_no = DK_OC2_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_oc2_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_oc2_sleep_all_day = DK_OC2_BASE,
			fmc_Fertilizer,	//	dk_oc2_manure1_s,
			fmc_Fertilizer,	//	dk_oc2_manure1_p,
			fmc_Watering,	//	dk_oc2_water,
			fmc_Cutting,	//	dk_oc2_cutting1,
			fmc_Grazing,	//	dk_oc2_grazing,
			fmc_Grazing,	//	dk_oc2_cattle_is_out,
			fmc_Grazing,	//	dk_oc2_grazing,
			fmc_Grazing,	//	dk_oc2_cattle_is_out,
			fmc_Fertilizer,	//	dk_oc2_manure2_s,
			fmc_Fertilizer,	//	dk_oc2_manure2_p,
			fmc_Cutting,	//	dk_oc2_cutting2,
			fmc_Fertilizer,	//	dk_oc2_manure3_s,
			fmc_Fertilizer,	//	dk_oc2_manure3_p,
			fmc_Cutting,	//	dk_oc2_cutting3,
			fmc_Cutting,	//	dk_oc2_cutting4,
			fmc_Cutting,	//	dk_oc2_cutting5,
			fmc_Others, // dk_oc2_cutting6,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_OCloverGrassGrazed2_H