/**
\file
\brief
<B>SE_WinterRape_Seed.h This file contains the headers for the SE_WinterRape_Seed class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of March 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// SE_WinterRape_Seed.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SE_WINTERRAPE_SEEED_H
#define SE_WINTERRAPE_SEED_H

#define SE_WRS_BASE 80100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define SE_WRS_HERBICIDE        a_field->m_user[1]
#define SE_WRS_DECIDE_TO_HERB       a_field->m_user[2]
#define SE_WRS_DECIDE_TO_FI         a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing SE_WinterRape_Seed, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	se_wrs_start = 1, // Compulsory, must always be 1 (one).
	se_wrs_straw_removal = SE_WRS_BASE,
	se_wrs_herbicide1a,
	se_wrs_autumn_plough,
	se_wrs_stubble_cultivator1,
	se_wrs_stubble_cultivator2,
	se_wrs_herbicide1b,
	se_wrs_autumn_harrow,
	se_wrs_autumn_sow,
	se_wrs_insecticide1,
	se_wrs_herbicide2,
	se_wrs_herbicide3,
	se_wrs_insecticide2,
	se_wrs_growth_regulator,
	se_wrs_npk,
	se_wrs_ns1,
	se_wrs_ns2,
	se_wrs_herbicide4,
	se_wrs_insecticide3,
	se_wrs_herbicide5,
	se_wrs_harvest,
	se_wrs_foobar,
} SE_WinterRape_SeedToDo;


/**
\brief
SE_WinterRape_Seed class
\n
*/
/**
See SE_WinterRape_Seed.h::SE_WinterRape_SeedToDo for a complete list of all possible events triggered codes by the SE_WinterRape_Seed management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class SE_WinterRape_Seed : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	SE_WinterRape_Seed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (se_wrs_foobar - SE_WRS_BASE);
		m_base_elements_no = SE_WRS_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	se_wrs_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	se_wrs_straw_removal = SE_WRS_BASE,
			fmc_Herbicide,	//	se_wrs_herbicide1a,
			fmc_Cultivation,	//	se_wrs_autumn_plough,
			fmc_Cultivation,	//	se_wrs_stubble_cultivator1,
			fmc_Cultivation,	//	se_wrs_stubble_cultivator2,
			fmc_Herbicide,	//	se_wrs_herbicide1b,
			fmc_Cultivation,	//	se_wrs_autumn_harrow,
			fmc_Others,	//	se_wrs_autumn_sow,
			fmc_Insecticide,	//	se_wrs_insecticide1,
			fmc_Herbicide,	//	se_wrs_herbicide2,
			fmc_Herbicide,	//	se_wrs_herbicide3,
			fmc_Insecticide,	//	se_wrs_insecticide2,
			fmc_Others,	//	se_wrs_growth_regulator,
			fmc_Fertilizer,	//	se_wrs_npk,
			fmc_Fertilizer,	//	se_wrs_ns1,
			fmc_Fertilizer,	//	se_wrs_ns2,
			fmc_Herbicide,	//	se_wrs_herbicide4,
			fmc_Insecticide,	//	se_wrs_insecticide3,
			fmc_Herbicide,	//	se_wrs_herbicide5,
			fmc_Harvest	//	se_wrs_harvest,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // SE_WINTERRAPE_SEED_H