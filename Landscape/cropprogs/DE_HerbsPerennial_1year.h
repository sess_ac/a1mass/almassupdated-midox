/**
\file
\brief
<B>DE_HerbsPerennial_1year.h This file contains the headers for the HerbsPerennial_1year class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of June 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// HerbsPerennial_1year.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DE_HERBSPERENNIAL_1YEAR_H
#define DE_HERBSPERENNIAL_1YEAR_H

#define DE_HP1Y_BASE 39000

/** Below is the list of things that a farmer can do if he is growing perennial herbs, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	de_hp1y_start = 1, // Compulsory, must always be 1 (one).
	de_hp1y_sleep_all_day = DE_HP1Y_BASE,
	de_hp1y_winter_plough,
	de_hp1y_ferti_p1,//PK
	de_hp1y_ferti_s1,//PK
	de_hp1y_spring_harrow,
	de_hp1y_ferti_p2,//N I
	de_hp1y_ferti_s2,//N I
	de_hp1y_preseeding_cultivation,
	de_hp1y_planting,
	de_hp1y_weeding,
	de_hp1y_herbicide,
	de_hp1y_ferti_p3,//N II
	de_hp1y_ferti_s3,//N II
	de_hp1y_harvest,
	de_hp1y_ferti_p4,//N III
	de_hp1y_ferti_s4,//N III
	de_hp1y_harrow,
	de_hp1y_foobar,
} DE_HerbsPerennial_1yearToDo;



class DE_HerbsPerennial_1year : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DE_HerbsPerennial_1year(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(5, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (de_hp1y_foobar - DE_HP1Y_BASE);
		m_base_elements_no = DE_HP1Y_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	de_hp1y_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	de_hp1y_sleep_all_day = DE_HP_BASE,
			fmc_Cultivation, //	de_hp1y_winter_plough,
			fmc_Fertilizer,	//	de_hp1y_ferti_p1,//PK
			fmc_Fertilizer,	//	de_hp1y_ferti_s1,//PK
			fmc_Cultivation, //	de_hp1y_spring_harrow,
			fmc_Fertilizer,	//	de_hp1y_ferti_p2,//N I
			fmc_Fertilizer,	//	de_hp1y_ferti_s2,//N I
			fmc_Cultivation, //	de_hp1y_preeseding_cultivation,
			fmc_Others,	//	de_hp1y_planting,
			fmc_Others,	//	de_hp1y_weeding,
			fmc_Herbicide, //	de_hp1y_herbicide,
			fmc_Fertilizer,	//	de_hp1y_ferti_p3,//N II
			fmc_Fertilizer,	//	de_hp1y_ferti_s3,//N II
			fmc_Harvest, //	de_hp1y_harvest,
			fmc_Fertilizer,	//	de_hp1y_ferti_p4,//N III
			fmc_Fertilizer,	//	de_hp1y_ferti_s4,//N III
			fmc_Cultivation, //	de_hp1y_harrow,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DE_HERBSPERENNIAL_1YEAR_H

