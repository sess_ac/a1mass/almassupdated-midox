//
// DK_Legume_Peas.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_Legume_Peas_H
#define DK_Legume_Peas_H

#define DK_LEGP_BASE 60900

#define DK_LEGP_CLAY m_field->m_user[0]
#define DK_LEGP_FORCESPRING	a_field->m_user[1]

typedef enum {
    dk_legp_start = 1, // Compulsory, start event must always be 1 (one).
    dk_legp_harvest = DK_LEGP_BASE,
    dk_legp_autumn_plough,
    dk_legp_spring_plough,
    dk_legp_spring_harrow,
    dk_legp_ferti_ks_s,
    dk_legp_ferti_ks_p,
    dk_legp_sow,
    dk_legp_roll,
    dk_legp_herbicide1,
    dk_legp_herbicide2,
    dk_legp_insecticide1,
    dk_legp_insecticide2,
    dk_legp_foobar,
} DK_Legume_PeasToDo;



class DK_Legume_Peas : public Crop
{
public:
    bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
    DK_Legume_Peas(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
    {
        m_first_date = g_date->DayInYear(30, 11);
        SetUpFarmCategoryInformation();
    }
    void SetUpFarmCategoryInformation() {
        const int elements = 2 + (dk_legp_foobar - DK_LEGP_BASE);
        m_base_elements_no = DK_LEGP_BASE - 2;

        FarmManagementCategory catlist[elements] =
        {
            fmc_Others,	// zero element unused but must be here	
            fmc_Others,	//	    dk_legp_start = 1, // Compulsory, start event must always be 1 (one).
            fmc_Harvest,	//	    dk_legp_harvest = DK_LEGP_BASE,
            fmc_Cultivation,	//	    dk_legp_autumn_plough,
            fmc_Cultivation,	//	    dk_legp_spring_plough,
            fmc_Cultivation,	//	    dk_legp_spring_harrow,
            fmc_Fertilizer,	//	    dk_legp_ferti_ks_s,
            fmc_Fertilizer,	//	    dk_legp_ferti_ks_p,
            fmc_Cultivation,	//	    dk_legp_sow,
            fmc_Cultivation,	//	    dk_legp_roll,
            fmc_Herbicide,	//	    dk_legp_herbicide1,
            fmc_Herbicide,	//	    dk_legp_herbicide2,
            fmc_Insecticide,	//	    dk_legp_insecticide1,
            fmc_Insecticide,	//	    dk_legp_insecticide2,


                // no foobar entry	

        };
        // Iterate over the catlist elements and copy them to vector				
        copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

    }

};

#endif // DK_Legume_Peas_H
