/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLWinterWheat.cpp This file contains the source for the NLWinterWheat class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLWinterWheat.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLWinterWheat.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_winterwheat_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_WW_InsecticideDay;
extern CfgInt   cfg_WW_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;
extern CfgFloat cfg_NLCatchCropPct;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool NLWinterWheat::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLWinterWheat;
	int l_nextcropstartdate;
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case nl_ww_start:
	{
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(10, 9); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 8); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(5, 9);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping
		flexdates[2][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(10, 9);	 // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - hay bailing

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 0;
		if (StartUpCrop(isSpring, flexdates, int(nl_ww_herbicide3))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(15, 8) + isSpring;
		// OK, let's go.
		// Here we queue up the first event
		SimpleEvent_(d1, nl_ww_stubble_cultivator, false, m_farm, m_field);
		break;
	}
	break;
	

	// This is the first real farm operation
	case nl_ww_stubble_cultivator:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_stubble_cultivator, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 9);
		}
		SimpleEvent_(d1, nl_ww_herbicide1, false, m_farm, m_field);
		break;
	case nl_ww_herbicide1:
		// Some farmers will use total herbicide on a field, and some mole (deep) ploughing
		if (m_ev->m_lock || m_farm->DoIt_prob(0.20))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_herbicide1, true, m_farm, m_field);
				break;
			}
			else
			{
				SimpleEvent_(g_date->Date() + 14, nl_ww_autumn_plough, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_ww_mole_plough, false, m_farm, m_field);
		break;
	case nl_ww_mole_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.375)) // 30% from the rest 80% of farmers who don't do herbicide1
		{
			if (!m_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_mole_plough, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, nl_ww_autumn_plough, false, m_farm, m_field);
		break;
	case nl_ww_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_autumn_plough, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 10, nl_ww_preseeding_cultivator_sow, false, m_farm, m_field);
		break;
	case nl_ww_preseeding_cultivator_sow:
		// all farmers do preseeding cultivation together with sowing
		if (!m_farm->PreseedingCultivatorSow(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_preseeding_cultivator_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, nl_ww_herbicide2, false, m_farm, m_field); // Herbidide thread = Main thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 3) + 365, nl_ww_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 4) + 365, nl_ww_insecticide1, false, m_farm, m_field);	// Insecticide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, nl_ww_growth_regulator1, false, m_farm, m_field); // GR thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// slurry thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_ww_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_ww_ferti_p1, false, m_farm, m_field);
		if (m_farm->IsStockFarmer()) //Stock Farmer					// PK thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_ww_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_ww_ferti_p2, false, m_farm, m_field);
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_ww_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, nl_ww_ferti_p3, false, m_farm, m_field);
		break;
	case nl_ww_herbicide2: // The first of the pesticide managements.
		// Here comes the herbicide thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 3) + 365, nl_ww_herbicide3, false, m_farm, m_field);
		break;
	case nl_ww_herbicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_herbicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), nl_ww_desiccation, false, m_farm, m_field);
		break;
	case nl_ww_desiccation:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_desiccation, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, nl_ww_harvest, false, m_farm, m_field);
		break;
	case nl_ww_fungicide1:
		// Here comes the fungicide thread
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 21, nl_ww_fungicide2, false, m_farm, m_field);
		break;
	case nl_ww_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, nl_ww_fungicide3, false, m_farm, m_field);
		break;
	case nl_ww_fungicide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.78)) // 70% of all farmers spray 3 times
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_ww_insecticide1:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_winterwheat_on.value() ||
			!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		else {
			m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 21, nl_ww_insecticide2, false, m_farm, m_field);
		break;
	case nl_ww_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_winterwheat_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_ww_insecticide2, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
	case nl_ww_growth_regulator1:
		// Here comes the GR thread
		if (!m_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_growth_regulator1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, nl_ww_growth_regulator2, false, m_farm, m_field);
		break;
	case nl_ww_growth_regulator2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_growth_regulator2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_ww_ferti_p1:
		// Here comes the slurry thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_ww_ferti_s1:
		// Here comes the slurry thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_ww_ferti_p2:
		// Here comes the PK thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_ww_ferti_s2:
		// Here comes the PK thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_ww_ferti_p3:
		// Here comes the N thread
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_p3, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 21;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
		}
		SimpleEvent_(d1, nl_ww_ferti_p4, false, m_farm, m_field);
		break;
	case nl_ww_ferti_s3:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_s3, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 21;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
		}
		SimpleEvent_(d1, nl_ww_ferti_s4, false, m_farm, m_field);
		break;
	case nl_ww_ferti_p4:
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_p4, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 21;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 4);
		}
		SimpleEvent_(d1, nl_ww_ferti_p5, false, m_farm, m_field);
		break;
	case nl_ww_ferti_s4:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_s4, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 21;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 4);
		}
		SimpleEvent_(d1, nl_ww_ferti_s5, false, m_farm, m_field);
		break;
	case nl_ww_ferti_p5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_p5, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_ww_ferti_s5:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ww_ferti_s5, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_ww_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_harvest, true, m_farm, m_field);
			break;
		}
		// 75% of farmers will leave straw on field and rest will do hay bailing
		if (m_farm->DoIt_prob(0.75))
		{
			SimpleEvent_(g_date->Date() + 1, nl_ww_straw_chopping, false, m_farm, m_field);
		}
		else
		{
			SimpleEvent_(g_date->Date() + 1, nl_ww_hay_bailing, false, m_farm, m_field);
		}
		break;
	case nl_ww_straw_chopping:
		if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_straw_chopping, true, m_farm, m_field);
			break;
		}
		if (m_ev->m_lock || m_farm->DoIt_prob(cfg_NLCatchCropPct.value())) {
			 // So we are done,but this crop uses a catch crop
			l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
			m_field->BumpRunNum();
			m_field->SetVegPatchy(false); // reverse the patchy before the next crop
			m_farm->AddNewEvent(tov_NLCatchCropPea, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
			m_field->SetVegType(tov_NLCatchCropPea, tov_Undefined); //  Two vegetation curves are specified 
			if (m_field->GetUnsprayedMarginPolyRef() != -1)
			{
				LE* um = m_OurLandscape->SupplyLEPointer(m_field->GetUnsprayedMarginPolyRef());
				um->SetVegType(tov_NLCatchCropPea, tov_Undefined);
			}
			// NB no "done = true" because this crop effectively continues into the catch crop.
			break;
		}
		else
			done = true;
		break;
	case nl_ww_hay_bailing:
		if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ww_hay_bailing, true, m_farm, m_field);
			break;
		}
		if (m_ev->m_lock || m_farm->DoIt_prob(cfg_NLCatchCropPct.value())) {
			 // So we are done,but this crop uses a catch crop
			l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
			m_field->BumpRunNum();
			m_field->SetVegPatchy(false); // reverse the patchy before the next crop
			m_farm->AddNewEvent(tov_NLCatchCropPea, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
			m_field->SetVegType(tov_NLCatchCropPea, tov_Undefined); //  Two vegetation curves are specified 
			if (m_field->GetUnsprayedMarginPolyRef() != -1)
			{
				LE* um = m_OurLandscape->SupplyLEPointer(m_field->GetUnsprayedMarginPolyRef());
				um->SetVegType(tov_NLCatchCropPea, tov_Undefined);
			}
			// NB no "done = true" because this crop effectively continues into the catch crop.
			break;
		}
		else
			done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLWinterWheat::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}