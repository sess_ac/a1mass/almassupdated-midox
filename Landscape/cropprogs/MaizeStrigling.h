//
// MaizeStrigling.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef MAIZEStrigling_H
#define MAIZEStrigling_H

#define MAIZEStrigling_BASE 2200
#define MAIZEStrigling_SOW_DATE m_field->m_user[0]
#define MAIZEStrigling_HERBI_ONE_DATE m_field->m_user[1]

typedef enum {
  mas_start = 1, // Compulsory, start event must always be 1 (one).
  mas_fa_manure_a = MAIZEStrigling_BASE,
  mas_fa_manure_b,
  mas_autumn_plough,
  mas_fa_slurry_one,
  mas_spring_plough,
  mas_spring_harrow,
  mas_spring_sow,
  mas_fa_npk,
  mas_row_one,
  mas_fa_slurry_two,
  mas_row_two,
  mas_water_one,
  mas_water_two,
  mas_harvest,
  mas_stubble,
  mas_herbi_one,
  mas_herbi_two,
  mas_foobar,
} MaizeSToDo;



class MaizeStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  MaizeStrigling(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(15,10);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (mas_foobar - MAIZEStrigling_BASE);
	  m_base_elements_no = MAIZEStrigling_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  mas_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  mas_fa_manure_a = MAIZEStrigling_BASE,
			fmc_Fertilizer,	//	  mas_fa_manure_b,
			fmc_Cultivation,	//	  mas_autumn_plough,
			fmc_Fertilizer,	//	  mas_fa_slurry_one,
			fmc_Cultivation,	//	  mas_spring_plough,
			fmc_Cultivation,	//	  mas_spring_harrow,
			fmc_Others,	//	  mas_spring_sow,
			fmc_Fertilizer,	//	  mas_fa_npk,
			fmc_Cultivation,	//	  mas_row_one,
			fmc_Fertilizer,	//	  mas_fa_slurry_two,
			fmc_Cultivation,	//	  mas_row_two,
			fmc_Watering,	//	  mas_water_one,
			fmc_Watering,	//	  mas_water_two,
			fmc_Harvest,	//	  mas_harvest,
			fmc_Cultivation,	//	  mas_stubble,
			fmc_Herbicide,	//	  mas_herbi_one,
			fmc_Herbicide	//	  mas_herbi_two,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // MAIZE_H
