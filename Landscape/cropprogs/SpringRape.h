//
// springrape.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SPRINGRAPE_H
#define SPRINGRAPE_H

// <cropname>_BASE is the first event number to be dumped into the
// debugging log from this crop. *Must* be unique among all crops.
// I suggest steps of 100 between crops.

#define SPRINGRAPE_BASE 7200
#define SR_DID_RC_CLEAN     m_field->m_user[0]
#define SR_DID_HERBI_ZERO   m_field->m_user[1]
#define SR_INSECT_DATE      m_field->m_user[2]
#define SR_FUNGI_DATE       m_field->m_user[3]
#define SR_SWARTH_DATE      m_field->m_user[4]
#define SR_HARVEST_DATE     m_field->m_user[5]
//#define SR_DECIDE_TO_HERB  m_field->m_user[6]
//#define SR_DECIDE_TO_FI    m_field->m_user[7]



typedef enum {
  sr_start = 1, // Compulsory, start event must always be 1 (one).
  sr_ferti_zero = SPRINGRAPE_BASE,
  sr_spring_plough,
  sr_spring_harrow,
  sr_spring_sow,
  sr_rowcol_clean,
  sr_herbi_zero,
  sr_ferti_p1,
  sr_ferti_p2,
  sr_ferti_s1,
  sr_ferti_s2,
  sr_herbi_one,
  sr_fungi_one,
  sr_rowcol_one,
  sr_rowcol_one_b,
  sr_insect_one,
  sr_insect_one_b,
  sr_insect_one_c,
  sr_swarth,
  sr_harvest,
  sr_cuttostraw,
  sr_compress,
  sr_stub_harrow,
  sr_grubbing,
  sr_productapplic_one,
  sr_foobar
} SpringRapeToDo;



class SpringRape: public Crop
{
  // Private methods corresponding to the different management steps
  // (when needed).
  void HerbiZero( void );
  void RowcolOne( void );

public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   SpringRape(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
      m_first_date=g_date->DayInYear(1,3);
	  SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (sr_foobar - SPRINGRAPE_BASE);
	   m_base_elements_no = SPRINGRAPE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
		   fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sr_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//sr_ferti_zero = SPRINGRAPE_BASE,
		  fmc_Cultivation,//sr_spring_plough,
		  fmc_Cultivation,//sr_spring_harrow,
		  fmc_Others,//sr_spring_sow,
		  fmc_Cultivation,//sr_rowcol_clean,
		  fmc_Herbicide,//sr_herbi_zero,
		  fmc_Fertilizer,//sr_ferti_p1,
		  fmc_Fertilizer,//sr_ferti_p2,
		  fmc_Fertilizer,//sr_ferti_s1,
		  fmc_Fertilizer,//sr_ferti_s2,
		  fmc_Herbicide,//sr_herbi_one,
		  fmc_Fungicide,//sr_fungi_one,
		  fmc_Cultivation,//sr_rowcol_one,
		  fmc_Cultivation,//sr_rowcol_one_b,
		  fmc_Insecticide,//sr_insect_one,
		  fmc_Insecticide,//sr_insect_one_b,
		  fmc_Insecticide,//sr_insect_one_c,
		  fmc_Cutting,//sr_swarth,
		  fmc_Harvest,//sr_harvest,
		  fmc_Others,//sr_cuttostraw,
		  fmc_Others,//sr_compress,
		  fmc_Cultivation,//sr_stub_harrow,
		  fmc_Cultivation,//sr_grubbing,
		  fmc_Herbicide//sr_productapplic_one

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // WINTERRAPE_H
