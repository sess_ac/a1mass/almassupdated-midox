//
// DE_OWinterWheat.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OWinterWheat.h"

extern CfgFloat cfg_strigling_prop;

bool DE_OWinterWheat::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
    int d2 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEOWinterWheat; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
    switch (m_ev->m_todo)
    {
    case de_oww_start:
    {
        m_field->ClearManagementActionSum();

        m_last_date = g_date->DayInYear(15, 9); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(6 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(20, 8); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use
                // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1;
        flexdates[1][1] = g_date->DayInYear(22, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)
        flexdates[2][0] = g_date->DayInYear(23, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 2)
        flexdates[2][1] = g_date->DayInYear(23, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 2)
        flexdates[3][0] = g_date->DayInYear(25, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 3)
        flexdates[3][1] = g_date->DayInYear(30, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 3)
        flexdates[4][0] = -1;
        flexdates[4][1] = g_date->DayInYear(1, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 4)
        flexdates[5][0] = -1;
        flexdates[5][1] = g_date->DayInYear(5, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 5)
        flexdates[6][0] = -1;
        flexdates[6][1] = g_date->DayInYear(15, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 6)

        // Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
        int isSpring = 0;
        if (StartUpCrop(isSpring, flexdates, int(de_oww_spring_roll1))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(15, 8) + isSpring;
        // OK, let's go.
        if (m_farm->IsStockFarmer()) // StockFarmer
        {
            SimpleEvent_(d1, de_oww_ferti_s1, false, m_farm, m_field);
        }
        else SimpleEvent_(d1, de_oww_ferti_p1, false, m_farm, m_field);
        break;
    }
    break;

    case de_oww_ferti_p1:
        if (m_ev->m_lock || m_farm->DoIt(10))
        {
            if (!m_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_oww_ferti_p1, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->Date(), de_oww_autumn_plough, false, m_farm, m_field);
        break;

    case de_oww_ferti_s1:
        if (m_ev->m_lock || m_farm->DoIt(65))
        {
            if (!m_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_oww_ferti_s1, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->Date(), de_oww_autumn_plough, false, m_farm, m_field);
        break;

    case de_oww_autumn_plough:
        if (m_ev->m_lock || m_farm->DoIt(80))
        {
            if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_oww_autumn_plough, true, m_farm, m_field);
                break;
            }
            SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 9),
                de_oww_autumn_sow, false, m_farm, m_field);
            break;
        }
        else
            SimpleEvent_(g_date->Date(), de_oww_autumn_harrow, false, m_farm, m_field);
        break;

    case de_oww_autumn_harrow:
        if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_oww_autumn_harrow, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 9),
            de_oww_autumn_sow, false, m_farm, m_field);
        break;

    case de_oww_autumn_sow:
        if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_oww_autumn_sow, true, m_farm, m_field);
            break;
        }
        {
            long newdate1 = g_date->OldDays() + g_date->DayInYear(10, 9);
            long newdate2 = g_date->Date() + 10;
            if (newdate2 > newdate1)
                newdate1 = newdate2;
            SimpleEvent_(newdate1, de_oww_strigling1, false, m_farm, m_field);
        }
        break;

    case de_oww_strigling1:
        if (m_ev->m_lock || m_farm->DoIt(90))
        {
            if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_oww_strigling1, true, m_farm, m_field);
                break;
            }
        }
        // Next year
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 2) + 365, de_oww_spring_roll1, false, m_farm, m_field);
        break;

    case de_oww_spring_roll1:
        if (m_ev->m_lock || m_farm->DoIt(5))
        {
            if (!m_farm->SpringRoll(m_field, 0.0, g_date->DayInYear(5, 3) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_oww_spring_roll1, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 3), de_oww_strigling2, false, m_farm, m_field);
        break;
    case de_oww_strigling2:
        if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
            SimpleEvent_(g_date->Date() + 1, de_oww_strigling2, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 3), de_oww_strigling3, false, m_farm, m_field);
        break;

    case de_oww_strigling3:
        if (m_ev->m_lock || m_farm->DoIt(50))
        {
            if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_oww_strigling3, true, m_farm, m_field);
                break;
            }
        }
        if (m_farm->IsStockFarmer()) // StockFarmer
        {
            SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), de_oww_ferti_s2, false, m_farm, m_field);
            break;
        }
        else
            SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_oww_harvest, false, m_farm, m_field);
        break;

    case de_oww_ferti_s2:
        if (m_ev->m_lock || m_farm->DoIt(50))
        {
            if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
                SimpleEvent_(g_date->Date() + 1, de_oww_ferti_s2, true, m_farm, m_field);
                break;
            }
        }
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_oww_harvest, false, m_farm, m_field);
        break;

    case de_oww_harvest:
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) { // last date 20/8
            SimpleEvent_(g_date->Date() + 1, de_oww_harvest, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date(), de_oww_straw_chopping, false, m_farm, m_field);
        break;

    case de_oww_straw_chopping:
        if (m_ev->m_lock || m_farm->DoIt_prob(.60))
        {
            if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) // last date 22/8
            {
                SimpleEvent_(g_date->Date() + 1, de_oww_straw_chopping, true, m_farm, m_field);
                break;
            }
            SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), de_oww_deep_plough, false, m_farm, m_field);
            break;
        }
        else if (m_ev->m_lock || m_farm->DoIt_prob(.40 / .40)) {
            d1 = g_date->Date() + 3;
            if (d1 < m_field->GetMDates(0, 2)) d1 = m_field->GetMDates(0, 2); // start date 23/8
            SimpleEvent_(d1, de_oww_hay_turning, false, m_farm, m_field);
            break;
        }

    case de_oww_hay_turning:
        if (m_ev->m_lock || m_farm->DoIt_prob(.20 / .40))
        {
            if (!m_farm->HayTurning(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) { // end date 23/8
                SimpleEvent_(g_date->Date() + 1, de_oww_hay_turning, true, m_farm, m_field);
                break;
            }
        }
        d1 = g_date->OldDays() + g_date->DayInYear(7, 8);
        d2 = g_date->Date() + 3;
        if (d2 > d1) d1 = d2;
        if (d1 > m_field->GetMDates(0, 3)) d1 = m_field->GetMDates(0, 3); // start date 25/8
        SimpleEvent_(d1, de_oww_hay_bailing, false, m_farm, m_field);
        break;

    case de_oww_hay_bailing:
        if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) { // end date 30/8
            SimpleEvent_(g_date->Date() + 1, de_oww_hay_bailing, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date() + 1, de_oww_deep_plough, false, m_farm, m_field);
        break;

    case de_oww_deep_plough:
        if (m_ev->m_lock || m_farm->DoIt_prob(.60))
        {
            if (!m_farm->DeepPlough(m_field, 0.0,
                m_field->GetMDates(1, 4) - g_date->DayInYear())) // end date 1/9
            {
                SimpleEvent_(g_date->Date() + 1, de_oww_deep_plough, true, m_farm, m_field);
                break;
            }
            done = true;
            break;
        }
        else if (m_ev->m_lock || m_farm->DoIt_prob(.20 / .40)) {
            SimpleEvent_(g_date->Date() + 4, de_oww_stubble_harrow1, false, m_farm, m_field);
            break;
        }
        else  done = true;

    case de_oww_stubble_harrow1:
        if (!m_farm->StubbleHarrowing(m_field, 0.0, m_field->GetMDates(1, 5) - g_date->DayInYear())) { // last date 5/9
            SimpleEvent_(g_date->Date() + 1, de_oww_stubble_harrow1, true, m_farm, m_field);
            break;
        }
        SimpleEvent_(g_date->Date() + 10, de_oww_stubble_harrow2, false, m_farm, m_field); // main thread
        break;

    case de_oww_stubble_harrow2:
        if (!m_farm->StubbleHarrowing(m_field, 0.0, m_field->GetMDates(1, 6) - g_date->DayInYear())) // end date 15/9
        {
            SimpleEvent_(g_date->Date() + 1, de_oww_stubble_harrow2, true, m_farm, m_field);
            break;
        }
        done = true;
        // END OF MAIN THREAD
        break;

    default:
        g_msg->Warn(WARN_BUG, "DE_OWinterWheat::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }
  return done;
}

