//
// WinterRapeStrigling.h
//

#ifndef WINTERRAPESTRIGLING_H
#define WINTERRAPESTRIGLING_H

// <cropname>_BASE is the first event number to be dumped into the
// debugging log from this crop. *Must* be unique among all crops.
// I suggest steps of 100 between crops.

#define WINTERRAPESTRIGLING_BASE 7900
#define WR_DID_RC_CLEAN     m_field->m_user[0]
#define WR_DID_HERBI_ZERO   m_field->m_user[1]
#define WR_INSECT_DATE      m_field->m_user[2]
#define WR_FUNGI_DATE       m_field->m_user[3]
#define WR_SWARTH_DATE      m_field->m_user[4]

typedef enum {
  wrs_start = 1, // Compulsory, start event must always be 1 (one).
  wrs_ferti_zero = WINTERRAPESTRIGLING_BASE,
  wrs_autumn_plough,
  wrs_autumn_harrow,
  wrs_autumn_sow,
  wrs_strigling_one,
  wrs_strigling_two,
  wrs_strigling_three,
  wrs_strigling_threeb,
  wrs_ferti_p1,
  wrs_ferti_p2,
  wrs_ferti_s1,
  wrs_ferti_s2,
  wrs_fungi_one,
  wrs_insect_one,
  wrs_insect_one_b,
  wrs_insect_one_c,
  wrs_swarth,
  wrs_harvest,
  wrs_cuttostraw,
  wrs_compress,
  wrs_stub_harrow,
  wrs_grubbing,
  wrs_foobar
} WinterRapeStriglingToDo;



class WinterRapeStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   WinterRapeStrigling(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
      m_first_date=g_date->DayInYear(24,8);
	  SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (wrs_foobar - WINTERRAPESTRIGLING_BASE);
	   m_base_elements_no = WINTERRAPESTRIGLING_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
		   fmc_Others,	// zero element unused but must be here
		  fmc_Others,//wrs_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//wrs_ferti_zero = WINTERRAPESTRIGLING_BASE,
		  fmc_Cultivation,//wrs_autumn_plough,
		  fmc_Cultivation,//wrs_autumn_harrow,
		  fmc_Others,//wrs_autumn_sow,
		  fmc_Cultivation,//wrs_strigling_one,
		  fmc_Cultivation,//wrs_strigling_two,
		  fmc_Cultivation,//wrs_strigling_three,
		  fmc_Cultivation,//wrs_strigling_threeb,
		  fmc_Fertilizer,//wrs_ferti_p1,
		  fmc_Fertilizer,//wrs_ferti_p2,
		  fmc_Fertilizer,//wrs_ferti_s1,
		  fmc_Fertilizer,//wrs_ferti_s2,
		  fmc_Fungicide,//wrs_fungi_one,
		  fmc_Insecticide,//wrs_insect_one,
		  fmc_Insecticide,//wrs_insect_one_b,
		  fmc_Insecticide,//wrs_insect_one_c,
		  fmc_Cutting,//wrs_swarth,
		  fmc_Harvest,//wrs_harvest,
		  fmc_Others,//wrs_cuttostraw,
		  fmc_Others,//wrs_compress,
		  fmc_Cultivation,//wrs_stub_harrow,
		  fmc_Cultivation//wrs_grubbing

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // WINTERRAPE_H
