//
// WinterWheatStriglingSingle.h
//
/*

Copyright (c) 2005, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WINTERWHEATStriglingSingle_H
#define WINTERWHEATStriglingSingle_H

#define WINTERWHEATStriglingSingle_BASE 8500
#define WWStriglingSingle_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define WWStriglingSingle_AUTUMN_PLOUGH         m_field->m_user[1]

typedef enum {
  wwss_start = 1, // Compulsory, must always be 1 (one).
  wwss_sleep_all_day = WINTERWHEATStriglingSingle_BASE,
  wwss_ferti_p1,
  wwss_ferti_s1,
  wwss_ferti_s2,
  wwss_autumn_plough,
  wwss_autumn_harrow,
  wwss_stubble_harrow1,
  wwss_autumn_sow,
  wwss_autumn_roll,
  wwss_ferti_p2,
  wwss_strigling0a,
  wwss_spring_roll,
  wwss_strigling0b,
  wwss_GR,
  wwss_fungicide,
  wwss_fungicide2,
  wwss_insecticide1,
  wwss_insecticide2,
  wwss_insecticide3,
  wwss_strigling1,
  wwss_strigling2,
  wwss_water1,
  wwss_water2,
  wwss_ferti_p3,
  wwss_ferti_p4,
  wwss_ferti_p5,
  wwss_ferti_s3,
  wwss_ferti_s4,
  wwss_ferti_s5,
  wwss_harvest,
  wwss_straw_chopping,
  wwss_hay_turning,
  wwss_hay_baling,
  wwss_stubble_harrow2,
  wwss_grubning,
  wwss_foobar
} WinterWheatStriglingSingleToDo;


class WinterWheatStriglingSingle: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   WinterWheatStriglingSingle(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
     m_first_date=g_date->DayInYear( 1,10 );
	 SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (wwss_foobar - WINTERWHEATStriglingSingle_BASE);
	   m_base_elements_no = WINTERWHEATStriglingSingle_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
		   fmc_Others,	// zero element unused but must be here
		  fmc_Others,//wwss_start = 1, // Compulsory, must always be 1 (one).
		  fmc_Others,//wwss_sleep_all_day = WINTERWHEATStriglingSingle_BASE,
		  fmc_Fertilizer,//wwss_ferti_p1,
		  fmc_Fertilizer,//wwss_ferti_s1,
		  fmc_Fertilizer,//wwss_ferti_s2,
		  fmc_Cultivation,//wwss_autumn_plough,
		  fmc_Cultivation,//wwss_autumn_harrow,
		  fmc_Cultivation,//wwss_stubble_harrow1,
		  fmc_Others,//wwss_autumn_sow,
		  fmc_Others,//wwss_autumn_roll,
		  fmc_Fertilizer,//wwss_ferti_p2,
		  fmc_Cultivation,//wwss_strigling0a,
		  fmc_Others,//wwss_spring_roll,
		  fmc_Cultivation,//wwss_strigling0b,
		  fmc_Others,//wwss_GR,
		  fmc_Fungicide,//wwss_fungicide,
		  fmc_Fungicide,//wwss_fungicide2,
		  fmc_Insecticide,//wwss_insecticide1,
		  fmc_Insecticide,//wwss_insecticide2,
		  fmc_Insecticide,//wwss_insecticide3,
		  fmc_Cultivation,//wwss_strigling1,
		  fmc_Cultivation,//wwss_strigling2,
		  fmc_Harvest,//wwss_water1,
		  fmc_Harvest,//wwss_water2,
		  fmc_Fertilizer,//wwss_ferti_p3,
		  fmc_Fertilizer,//wwss_ferti_p4,
		  fmc_Fertilizer,//wwss_ferti_p5,
		  fmc_Fertilizer,//wwss_ferti_s3,
		  fmc_Fertilizer,//wwss_ferti_s4,
		  fmc_Fertilizer,//wwss_ferti_s5,
		  fmc_Harvest,//wwss_harvest,
		  fmc_Others,//wwss_straw_chopping,
		  fmc_Others,//wwss_hay_turning,
		  fmc_Others,//wwss_hay_baling,
		  fmc_Cultivation,//wwss_stubble_harrow2,
		  fmc_Cultivation//wwss_grubning,

			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // WINTERWHEATStriglingSingle_H
