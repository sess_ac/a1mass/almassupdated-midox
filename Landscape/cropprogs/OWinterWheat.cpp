//
// OWinterWheat.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OWinterWheat.h"

extern CfgFloat cfg_strigling_prop;

bool OWinterWheat::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case oww_start:
    {
      OWW_PLOUGH_RUNS     = false;
      OWW_HARROW_RUNS     = false;
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(15,9);
      // Start and stop dates for all events after harvest
      int noDates= 7;
      m_field->SetMDates(0,0,g_date->DayInYear(1,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,8));
      m_field->SetMDates(0,1,g_date->DayInYear(5,8));
      m_field->SetMDates(1,1,g_date->DayInYear(5,9));
      m_field->SetMDates(0,2,g_date->DayInYear(25,8));
      m_field->SetMDates(1,2,g_date->DayInYear(25,8));
      m_field->SetMDates(0,3,g_date->DayInYear(25,8));
      m_field->SetMDates(1,3,g_date->DayInYear(30,8));
      m_field->SetMDates(0,4,g_date->DayInYear(5,8));
      m_field->SetMDates(1,4,g_date->DayInYear(5,8)); // not needed
      m_field->SetMDates(0,5,g_date->DayInYear(15,9));
      m_field->SetMDates(1,5,g_date->DayInYear(15,9));
      m_field->SetMDates(0,6,g_date->DayInYear(1,8));
      m_field->SetMDates(1,6,g_date->DayInYear(30,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OWinterWheat::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

      int d1;
      if ( ! m_ev->m_first_year )
      {
	int today=g_date->Date();
        // Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (today < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "OWinterWheat::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "OWinterWheat::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear(28,8),
                         oww_harvest, false );
         break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }

      // OK, let's go.
      if (m_farm->IsStockFarmer()) // StockFarmer
      {
        SimpleEvent( d1, oww_ferti_s1, false );
      }
      else SimpleEvent( d1, oww_ferti_p1, false );
    }
    break;

  case oww_ferti_p1:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FP_Manure( m_field, 0.0,
           g_date->DayInYear( 30,9 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, oww_ferti_p1, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),oww_autumn_plough, false );
    break;

  case oww_ferti_s1:
    if ( m_ev->m_lock || m_farm->DoIt( 40 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 30,9 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, oww_ferti_s1, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),oww_autumn_plough, false );
    break;

  case oww_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 30,9 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date()+1,oww_autumn_harrow, false );
    break;

  case oww_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_autumn_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,9 ),
    	           oww_autumn_sow, false );
    break;

  case oww_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_autumn_sow, true );
      break;
    }
    {
      long newdate1 = g_date->OldDays() + g_date->DayInYear( 10,9 );
      long newdate2 = g_date->Date() + 10;
      if ( newdate2 > newdate1 )
      newdate1 = newdate2;
      SimpleEvent( newdate1, oww_strigling1, false );
    }
    break;

  case oww_strigling1:
    if (!m_farm->Strigling( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_strigling1, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 24,9 ),
                 oww_strigling2, false );
    break;

  case oww_strigling2:
  	// --FN--
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 70 )))
    // **CJT** Soil Type 2-4 later implementation
    {
      if (!m_farm->Strigling( m_field, 0.0,
           g_date->DayInYear( 24,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, oww_strigling2, true );
        break;
      }
    }
    // Next year
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 )+365,
                      oww_strigling_sow, false );
    break;

  case oww_strigling_sow:
    if (!m_farm->StriglingSow( m_field, 0.0,
           g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_strigling_sow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                   oww_spring_roll1, false );
    break;

  case oww_spring_roll1:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
             g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, oww_spring_roll1, true );
        break;
      }
    }
    if (m_farm->IsStockFarmer()) // StockFarmer
    {
      SimpleEvent( g_date->Date() + 1, oww_ferti_s2, false );
    }
    else
    	SimpleEvent( g_date->Date() + 1, oww_ferti_p2, false );
    break;

  case oww_ferti_p2:
    if (!m_farm->FP_Slurry( m_field, 0.0,
         g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_ferti_p2, true );
      break;
    }
    SimpleEvent( g_date->Date()+1,oww_spring_roll2, false );
    break;

  case oww_ferti_s2:
    if (!m_farm->FA_Slurry( m_field, 0.0,
         g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_ferti_s2, true );
      break;
    }
    SimpleEvent( g_date->Date()+1,oww_spring_roll2, false );
    break;

  case oww_spring_roll2:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
             g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, oww_spring_roll2, true );
        break;
      }
    }
    if (m_farm->IsStockFarmer()) // StockFarmer
    {
      SimpleEvent(  g_date->OldDays() + g_date->DayInYear( 1,5 ),
                    oww_ferti_s3, false );
    }
    else
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                   oww_harvest, false );
    break;

  case oww_ferti_s3:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 30,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, oww_ferti_s3, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                 oww_harvest, false );
    break;

  case oww_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         m_field->GetMDates(1,0) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_harvest, true );
      break;
    }
	SimpleEvent(g_date->Date(), oww_straw_chopping, false);
    break;

  case oww_straw_chopping:
   {
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))
    {
      if (!m_farm->StrawChopping( m_field, 0.0,
           m_field->GetMDates(1,0) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, oww_straw_chopping, true );
        break;
      }
      else
      {
        // Did Chopping so test for stubble harrow/deep_plough
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                      oww_deep_plough, false );
        SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
                       oww_stubble_harrow1, false );
        break;
      }
    }
    int d1=g_date->Date() + 3;
    if (d1>m_field->GetMDates(0,2)) d1=m_field->GetMDates(0,2);
    SimpleEvent( d1, oww_hay_turning, false );
   }
   break;

  case oww_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->HayTurning( m_field, 0.0,
           m_field->GetMDates(1,2) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, oww_hay_turning, true );
        break;
      }
    }
    {
      long d1 = g_date->OldDays() + g_date->DayInYear( 7,8 );
      long d2 = g_date->Date() + 3;
      if ( d2 > d1 ) d1 = d2;
      if (d1>m_field->GetMDates(0,3)) d1=m_field->GetMDates(0,3);
      SimpleEvent( d1, oww_hay_baling, false );
    }
    break;

  case oww_hay_baling:
    if (!m_farm->HayBailing( m_field, 0.0,
         m_field->GetMDates(1,3) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oww_hay_baling, true );
      break;
    }
    // These events will almost certainly predate
    // 'today' due to the ending date used above.
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,6),
                 oww_deep_plough, false );
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,4),
                 oww_stubble_harrow1, false );
    break;

  case oww_stubble_harrow1:
		OWW_HARROW_RUNS = true;
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->StubbleHarrowing( m_field, 0.0,
           m_field->GetMDates(1,1) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, oww_stubble_harrow1, true );
        break;
      }
      int d1=g_date->Date()+10;
      if (d1>m_field->GetMDates(0,5)) d1=m_field->GetMDates(0,5);
      SimpleEvent( d1,oww_stubble_harrow2, false );
      break;
    }
		OWW_HARROW_RUNS = false;
    SimpleEvent( g_date->Date(), oww_catch_all, false );
   	break;

  case oww_stubble_harrow2:
    if (!m_farm->StubbleHarrowing( m_field, 0.0,
         m_field->GetMDates(1,5) - g_date->DayInYear()))
    {
      SimpleEvent( g_date->Date() + 1, oww_stubble_harrow2, true );
      break;
    }
		OWW_HARROW_RUNS = false;
    SimpleEvent( g_date->Date(), oww_catch_all, false );
   	break;

  case oww_deep_plough:
 	OWW_PLOUGH_RUNS = true;
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->DeepPlough( m_field, 0.0,
           m_field->GetMDates(1,6) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, oww_deep_plough, true );
        break;
      }
      // if we deep plough then finish plan here
      done=true;
      // END OF MAIN THREAD
			break; //
    }
		OWW_PLOUGH_RUNS = false;
    SimpleEvent( g_date->Date(), oww_catch_all, false );
    break;

  case oww_catch_all:
		if ( !OWW_PLOUGH_RUNS || !OWW_HARROW_RUNS )
    	done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OWinterWheat::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}

