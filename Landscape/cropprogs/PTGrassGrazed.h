/**
\file
\brief
<B>PTGrassGrazed.h This file contains the headers for the TemporalGrassGrazed class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTGrassGrazed.h
//


#ifndef PTGRASSGRAZED_H
#define PTGRASSGRAZED_H

#define PTGRASSGRAZED_BASE 30200
/**
\brief A flag used to indicate cattle out dates
*/
#define PT_FL1_CATTLEOUT_DATE a_field->m_user[1]
#define PT_FL2_CATTLEOUT_DATE a_field->m_user[2]
#define PT_FL_CULT_SOW a_field->m_user[3]

/** Below is the list of things that a farmer can do if he is growing TemporalGrassGrazed, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_gg_start = 1, // Compulsory, must always be 1 (one).
	pt_gg_sleep_all_day = PTGRASSGRAZED_BASE,
	pt_gg_stubble_harrow,
	pt_gg_autumn_plough,
	pt_gg_autumn_harrow,
	pt_gg_ferti_p1,
	pt_gg_ferti_s1,
	pt_gg_autumn_sow,
	pt_gg_autumn_roll,
	pt_gg_preseeding_cultivator_sow,
	pt_gg_cattle_out1,
	pt_gg_cattle_is_out1,
	pt_gg_harvest,
	pt_gg_cattle_out2,
	pt_gg_cattle_is_out2,
	pt_gg_wait,

} PTGrassGrazedToDo;


/**
\brief
PTGrassGrazed class
\n
*/
/**
See PTGrassGrazed.h::PTGrassGrazedToDo for a complete list of all possible events triggered codes by the TemporalGrassGrazed management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTGrassGrazed: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTGrassGrazed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 20,10 );//@AAS:Last possible date to do the first operation, i.e. plough or sow
   }
};

#endif // PTGRASSGRAZED_H

