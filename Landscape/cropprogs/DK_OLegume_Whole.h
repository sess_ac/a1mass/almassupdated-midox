//
// OLegume_Whole.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved. 

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_OLegume_Whole_H
#define DK_OLegume_Whole_H
/** \brief A flag used to indicate do catch crop
*/
#define DK_OLW_BASE 60000

typedef enum {
  dk_olw_start = 1, // Compulsory, start event must always be 1 (one).
  dk_olw_harvest = DK_OLW_BASE,
  dk_olw_spring_harrow1,
  dk_olw_spring_harrow2,
  dk_olw_spring_plough,
  dk_olw_ks_ferti_s,
  dk_olw_ks_ferti_p,
  dk_olw_spring_harrow3,
  dk_olw_spring_sow,
  dk_olw_strigling1,
  dk_olw_strigling2,
  dk_olw_water1,
  dk_olw_swathing,
  dk_olw_water2,
  dk_olw_spring_row_sow,
  dk_olw_strigling3,
  dk_olw_rowcultivation,
  dk_olw_wait1,
  dk_olw_wait2,
  dk_olw_foobar,
} DK_OLegumes_WholeToDo;



class DK_OLegume_Whole: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OLegume_Whole(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(10,4);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_olw_foobar - DK_OLW_BASE);
	  m_base_elements_no = DK_OLW_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_olw_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_olw_harvest1 = DK_OLW_BASE,
			fmc_Cultivation,	//	  dk_olw_spring_harrow1,
			fmc_Cultivation,	//	  dk_olw_spring_harrow2,
			fmc_Cultivation,	//	  dk_olw_spring_plough,
			fmc_Fertilizer,	//	  dk_olw_ks_ferti,
			fmc_Fertilizer,	//	  dk_olw_ks_ferti,
			fmc_Cultivation,	//	  dk_olw_spring_harrow3,
			fmc_Others,	//	  dk_olw_spring_sow,
			fmc_Cultivation,	//	  dk_olw_strigling1,
			fmc_Cultivation,	//	  dk_olw_strigling2,
			fmc_Watering,	//	  dk_olw_water1,
			fmc_Cutting,	//	  dk_olw_swathing,
			fmc_Watering,	//	  dk_olw_water2,
			fmc_Others,	//	  dk_olw_spring_row_sow,
			fmc_Cultivation,	//	  dk_olw_strigling3,
			fmc_Cultivation,	//	  dk_olw_rowcultivation,
			fmc_Others,	//	  dk_olw_wait,
			fmc_Others,	//	  dk_olw_wait,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_OLegume_Whole_H
