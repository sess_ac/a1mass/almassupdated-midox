//
// DK_OFodderbeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_OFodderBeet_h
#define DK_OFodderBeet_h

#define DK_OFB_BASE 67600

#define DK_OFB_FORCESPRING	a_field->m_user[1]

typedef enum {
  dk_ofb_start = 1, // Compulsory, start event must always be 1 (one).
  dk_ofb_harvest = DK_OFB_BASE,
  dk_ofb_ferti_s,
  dk_ofb_ferti_p,
  dk_ofb_autumn_plough,
  dk_ofb_winter_harrow,
  dk_ofb_molluscicide1,
  dk_ofb_spring_plough,
  dk_ofb_sharrow1,
  dk_ofb_sharrow2,
  dk_ofb_sharrow3,
  dk_ofb_sow,
  dk_ofb_molluscicide2,
  dk_ofb_strigling,
  dk_ofb_harrow,
  dk_ofb_row_cultivation1,
  dk_ofb_water1,
  dk_ofb_water2,
  dk_ofb_row_cultivation2,
  dk_ofb_row_cultivation3,
  dk_ofb_manualweeding,
  dk_ofb_foobar,
} DK_OFodderBeetToDo;



class DK_OFodderBeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OFodderBeet(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(20,11); // 
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_ofb_foobar - DK_OFB_BASE);
	  m_base_elements_no = DK_OFB_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_ofb_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_ofb_harvest = DK_OSB_BASE,
			fmc_Fertilizer, // dk_ofb_ferti_s
			fmc_Fertilizer, // dk_ofb_ferti_p
			fmc_Cultivation,	//	  dk_ofb_autumn_plough,
			fmc_Cultivation,	//	  dk_ofb_winter_harrow,
			fmc_Others,	//	  dk_ofb_molluscicide1,
			fmc_Cultivation,	//	  dk_ofb_spring_plough,
			fmc_Cultivation,	//	  dk_ofb_sharrow1,
			fmc_Cultivation,	//	  dk_ofb_sharrow2,
			fmc_Cultivation,	//	  dk_ofb_sharrow3,
			fmc_Others,	//	  dk_ofb_sow,
			fmc_Others,	//	  dk_ofb_molluscicide2,
			fmc_Cultivation,	//	  dk_ofb_strigling,
			fmc_Cultivation,	//	  dk_ofb_harrow,
			fmc_Cultivation,	//	  dk_ofb_row_cultivation1,
			fmc_Watering,	//	  dk_ofb_water1,
			fmc_Watering,	//	  dk_ofb_water2,
			fmc_Cultivation,	//	  dk_ofb_row_cultivation2,
			fmc_Cultivation,	//	  dk_ofb_row_cultivation3,
			fmc_Cultivation	//	  dk_ofb_manualweeding,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_OFodderBeet_h
