//
// OWinterBarley.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OWinterBarley.h"

extern CfgFloat cfg_strigling_prop;

bool OWinterBarley::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case owb_start:
    {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(5,8);
      // Start and stop dates for all events after harvest
      int noDates= 2;
      m_field->SetMDates(0,0,g_date->DayInYear(20,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(1,8));
      m_field->SetMDates(0,1,g_date->DayInYear(25,7));
      m_field->SetMDates(1,1,g_date->DayInYear(5,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OWinterBarley::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

      int d1;
      if ( ! m_ev->m_first_year )
      {
	int today=g_date->Date();
        // Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (today < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "OWinterBarley::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "OWinterBarley::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear(20,7 ),
                   owb_harvest, false );
         break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.

      OWB_DID_SPRING_SOW = false;

      if ( m_farm->IsStockFarmer()) {
	SimpleEvent( d1, owb_fertmanure_stock_one, false );
      } else {
	SimpleEvent( d1, owb_fertmanure_plant_one, false );
      }
    }
    break;

  case owb_fertmanure_stock_one:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 30, 9 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, owb_fertmanure_stock_one, true );
        break;
      }
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 25, 8 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 25, 8 );
      }
      SimpleEvent( d1, owb_autumn_plough, false );
    }
    break;

  case owb_fertmanure_plant_one:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FP_Manure( m_field, 0.0,
			      g_date->DayInYear( 30, 9 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, owb_fertmanure_plant_one, true );
        break;
      }
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 25, 8 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 25, 8 );
      }
      SimpleEvent( d1, owb_autumn_plough, false );
    }
    break;

  case owb_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 25, 9 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owb_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), owb_autumn_harrow, false );
    break;

  case owb_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 25, 9 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owb_autumn_harrow, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 10, 9 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 10, 9 );
      }
      SimpleEvent( d1, owb_autumn_sow, false );
    }
    break;

  case owb_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
			    g_date->DayInYear( 25,9 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owb_autumn_sow, true );
      break;
    }
    {
      int d1 = g_date->Date() + 10;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 25, 9 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 25, 9 );
      }
      SimpleEvent( d1, owb_strigling_one, false );
    }
    break;

  case owb_strigling_one:
    if (!m_farm->Strigling( m_field, 0.0,
			    g_date->DayInYear( 10,10 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owb_strigling_one, true );
      break;
    }
    if ( m_farm->IsStockFarmer()) {
      // Skip directly to second fertilization with manure.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 4 ) + 365,
		   owb_fertslurry_stock, false );

      // Queue up second strigling too, plant farmers does this themselves
      // after the second (spring) sow.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ) + 365,
		   owb_strigling_two, false );

    } else {
      // Plant farmers (will perhaps) do the spring sow.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 3 ) + 365,
		   owb_spring_sow, false );
    }
    break;

  case owb_spring_sow:
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))
    {
      // We intend to sow, eventually, so stop second strigling
      // ahead of time.
      OWB_DID_SPRING_SOW = true;
      if (!m_farm->SpringSow( m_field, 0.0,
			      g_date->DayInYear( 15,4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owb_spring_sow, true );
	break;
      }
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1, 4 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      }
      SimpleEvent( d1, owb_fertslurry_plant, false );

      if ( d1 < g_date->OldDays() + g_date->DayInYear( 5, 4 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 5, 4 );
      }
      SimpleEvent( d1, owb_strigling_two, false );
    }
    break;

  case owb_strigling_two:
    if ( OWB_DID_SPRING_SOW ) {
      break;
    }
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 60 )))
    {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 25,4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owb_strigling_two, true );
	break;
      }
    }
    // End of thread.
    break;

  case owb_fertslurry_plant:
    if (!m_farm->FP_Slurry( m_field, 0.0,
			    g_date->DayInYear( 30,4 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owb_fertslurry_plant, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
                 owb_harvest, false );
    break;

  case owb_fertslurry_stock:
    if (!m_farm->FA_Slurry( m_field, 0.0,
			    g_date->DayInYear( 30,4 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owb_fertslurry_stock, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
                 owb_harvest, false );
    break;

  case owb_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
			  m_field->GetMDates(1,0) -
			  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owb_harvest, true );
      break;
    }
    SimpleEvent( g_date->Date(), owb_straw_chopping, false );
    break;

  case owb_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->StrawChopping( m_field, 0.0,
				  m_field->GetMDates(1,0) -
				  g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, owb_straw_chopping, true );
        break;
      }
      // Did chopping, so skip to stubble harrowing.
      SimpleEvent( g_date->Date(), owb_stubble_harrowing, false );
      break;
    }
    // No chop, thus try to bail hay.
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + m_field->GetMDates(0,1)) {
	d1 = g_date->OldDays() + m_field->GetMDates(0,1);
      }
      SimpleEvent( d1, owb_hay_bailing, false );
    }
    break;

  case owb_hay_bailing:
    if ( m_ev->m_lock || m_farm->DoIt( 80 )) {
      if (!m_farm->HayBailing( m_field, 0.0,
			       m_field->GetMDates(1,1) -
			       g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owb_hay_bailing, true );
	break;
      }
    }
    SimpleEvent( g_date->Date(), owb_stubble_harrowing, false );
    break;

  case owb_stubble_harrowing:
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) {
      if (!m_farm->StubbleHarrowing( m_field, 0.0,
				     m_field->GetMDates(1,1) -
				     g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owb_stubble_harrowing, true );
	break;
      }
    }
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OWinterBarley::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


