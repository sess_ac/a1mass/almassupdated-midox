/**
\file
\brief
<B>UKWinterRape.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file
 by Chris J. Topping and Adam McVeigh \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
*/
//
// UKWinterRape.h
//


#ifndef UKWinterRape_H
#define UKWinterRape_H

#define UKWINTERRAPE_BASE 45800
/**
\brief A flag used to indicate autumn ploughing status
*/
#define UK_WR_FERTI_P1	a_field->m_user[1]
#define UK_WR_FERTI_S1	a_field->m_user[2]
#define UK_WR_STUBBLE_PLOUGH	a_field->m_user[3]
#define UK_WR_DECIDE_TO_GR a_field->m_user[4]

/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	uk_wr_start = 1, // Compulsory, must always be 1 (one).
	uk_wr_sleep_all_day = UKWINTERRAPE_BASE,
	uk_wr_ferti_p1, // slurry 50%
	uk_wr_ferti_s1,
	uk_wr_stubble_plough, // autumn plough 50%
	uk_wr_autumn_harrow, // autumn harro 50%
	uk_wr_autumn_plough, // autumn plough 25%
	uk_wr_stubble_harrow, // stubble cultivate 25%
	uk_wr_ferti_p2, // NPK 90%
	uk_wr_ferti_s2,
	uk_wr_preseeding_cultivator, // 100%
	uk_wr_autumn_sow, // 100%
	uk_wr_herbicide1, // 90%
	uk_wr_herbicide2, // 50%
	uk_wr_insecticide1, // 90%
	uk_wr_insecticide2, // 50%
	uk_wr_insecticide3, // 80%
	uk_wr_insecticide4, // 50%
	uk_wr_fungicide1, // 70%
	uk_wr_fungicide2, // 50%
	uk_wr_ferti_p3, // NI 100% 
	uk_wr_ferti_s3,
	uk_wr_ferti_p4, // N II 50%
	uk_wr_ferti_s4,
	uk_wr_ferti_p5, // N III 100%
	uk_wr_ferti_s5,
	uk_wr_growth_regulator, // GR 75%
	uk_wr_harvest, // 100%
	uk_wr_straw_chopping, // 100%
	uk_wr_hay_bailing, // 100%
	uk_wr_foobar,
} UKWinterRapeToDo;

/**
\brief
UKWinterRape class
\n
*/
/**
See UKWinterRape.h::UKWinterRapeToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class UKWinterRape : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	UKWinterRape(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 1st September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 9);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (uk_wr_foobar - UKWINTERRAPE_BASE);
		m_base_elements_no = UKWINTERRAPE_BASE - 2;
		FarmManagementCategory catlist[elements] =
		{
			 fmc_Others, // this needs to be at the zero line
			 fmc_Others, //uk_wr_start = 1, // Compulsory, must always be 1 (one).
			 fmc_Others, //uk_wr_sleep_all_day = UKWINTERRAPE_BASE,
			 fmc_Fertilizer,//uk_wr_ferti_p1, // slurry 50%
			 fmc_Fertilizer,//uk_wr_ferti_s1,
			 fmc_Cultivation,//uk_wr_stubble_plough, // autumn plough 50%
			 fmc_Cultivation,//uk_wr_autumn_harrow, // autumn harro 50%
			 fmc_Cultivation,//uk_wr_autumn_plough, // autumn plough 25%
			 fmc_Cultivation,//uk_wr_stubble_harrow, // stubble cultivate 25%
			 fmc_Fertilizer,//uk_wr_ferti_p2, // NPK 90%
			 fmc_Fertilizer,//uk_wr_ferti_s2,
			 fmc_Cultivation,//uk_wr_preseeding_cultivator, // 100%
			 fmc_Others, //uk_wr_autumn_sow, // 100%
			 fmc_Herbicide,//uk_wr_herbicide1, // 90%
			 fmc_Herbicide,//uk_wr_herbicide2, // 50%
			 fmc_Insecticide,//uk_wr_insecticide1, // 90%
			 fmc_Insecticide,//uk_wr_insecticide2, // 50%
			 fmc_Insecticide,//uk_wr_insecticide3, // 80%
			 fmc_Insecticide,//uk_wr_insecticide4, // 50%
			 fmc_Fungicide,//uk_wr_fungicide1, // 70%
			 fmc_Fungicide,//uk_wr_fungicide2, // 50%
			 fmc_Fertilizer,//uk_wr_ferti_p3, // NI 100% 
			 fmc_Fertilizer,//uk_wr_ferti_s3,
			 fmc_Fertilizer,//uk_wr_ferti_p4, // N II 50%
			 fmc_Fertilizer,//uk_wr_ferti_s4,
			 fmc_Fertilizer,//uk_wr_ferti_p5, // N III 100%
			 fmc_Fertilizer,//uk_wr_ferti_s5,
			 fmc_Others, //uk_wr_growth_regulator, // GR 75%
			 fmc_Harvest,//uk_wr_harvest, // 100%
			 fmc_Others, //uk_wr_straw_chopping, // 100%
			 fmc_Others, //uk_wr_hay_bailing, // 100%
		};
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // UKWinterRape_H

