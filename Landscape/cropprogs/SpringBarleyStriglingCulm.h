//
// SpringBarleyStriglingCulm.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleyStriglingCulm_H
#define SpringBarleyStriglingCulm_H

#define SBSTC_BASE 7000
#define SBSTC_SLURRY_DONE       m_field->m_user[0]
#define SBSTC_MANURE_DONE       m_field->m_user[1]
#define SBSTC_SLURRY_EXEC       m_field->m_user[2]
#define SBSTC_MANURE_EXEC       m_field->m_user[3]
#define SBSTC_DID_AUTUMN_PLOUGH m_field->m_user[4]

#define SBSTC_HERBI_DATE        m_field->m_user[0]
#define SBSTC_GR_DATE           m_field->m_user[1]
#define SBSTC_FUNGI_DATE        m_field->m_user[2]
#define SBSTC_WATER_DATE        m_field->m_user[3]
#define SBSTC_INSECT_DATE       m_field->m_user[4]

typedef enum {
  sbstc_start = 1, // Compulsory, start event must always be 1 (one).
  sbstc_autumn_plough = SBSTC_BASE,
  sbstc_fertslurry_stock,
  sbstc_fertmanure_stock_one,
  sbstc_spring_plough,
  sbstc_spring_harrow,
  sbstc_fertmanure_plant,
  sbstc_fertlnh3_plant,
  sbstc_fertpk_plant,
  sbstc_fertmanure_stock_two,
  sbstc_fertnpk_stock,
  sbstc_spring_sow,
  sbstc_spring_roll,
  sbstc_strigling_one,
  sbstc_strigling_two,
  sbstc_strigling_three,
  sbstc_strigling_four,
  sbstc_GR,
  sbstc_fungicide_one,
  sbstc_insecticide,
  sbstc_fungicide_two,
  sbstc_water_one,
  sbstc_water_two,
  sbstc_harvest,
  sbstc_straw_chopping,
  sbstc_hay_baling,
  sbstc_stubble_harrow,
  sbstc_foobar
} SBSTCToDo;



class SpringBarleyStriglingCulm: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleyStriglingCulm(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(1,11);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (sbstc_foobar - SBSTC_BASE);
	  m_base_elements_no = SBSTC_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//sbstc_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation,//sbstc_autumn_plough = SBSTC_BASE,
		  fmc_Fertilizer,//sbstc_fertslurry_stock,
		  fmc_Fertilizer,//sbstc_fertmanure_stock_one,
		  fmc_Cultivation,//sbstc_spring_plough,
		  fmc_Cultivation,//sbstc_spring_harrow,
		  fmc_Fertilizer,//sbstc_fertmanure_plant,
		  fmc_Fertilizer,//sbstc_fertlnh3_plant,
		  fmc_Fertilizer,//sbstc_fertpk_plant,
		  fmc_Fertilizer,//sbstc_fertmanure_stock_two,
		  fmc_Fertilizer,//sbstc_fertnpk_stock,
		  fmc_Others,//sbstc_spring_sow,
		  fmc_Others,//sbstc_spring_roll,
		  fmc_Cultivation,//sbstc_strigling_one,
		  fmc_Cultivation,//sbstc_strigling_two,
		  fmc_Cultivation,//sbstc_strigling_three,
		  fmc_Cultivation,//sbstc_strigling_four,
		  fmc_Others,//sbstc_GR,
		  fmc_Fungicide,//sbstc_fungicide_one,
		  fmc_Insecticide,//sbstc_insecticide,
		  fmc_Fungicide,//sbstc_fungicide_two,
		  fmc_Watering,//sbstc_water_one,
		  fmc_Watering,//sbstc_water_two,
		  fmc_Harvest,//sbstc_harvest,
		  fmc_Others,//sbstc_straw_chopping,
		  fmc_Others,//sbstc_hay_baling,
		  fmc_Cultivation//sbstc_stubble_harrow

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // SpringBarleyStrigling_H
