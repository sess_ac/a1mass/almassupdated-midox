/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_EnergyCrop_Perm.cpp This file contains the source for the DK_EnergyCrop_Perm class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of April 2022 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_EnergyCrop.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_EnergyCrop_Perm.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_EC_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_EC_InsecticideDay;
extern CfgInt   cfg_EC_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_EnergyCrop_Perm::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKEnergyCrop_Perm;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_ec_start:
	{
		DK_EC_YEARS_AFTER_EST = false;
		// dk_ec_start just sets up all the starting conditions and reference dates that are needed to start a dk_bfp1

		a_field->ClearManagementActionSum();
		m_last_date = g_date->DayInYear(15, 10); // Should match the last flexdate below
		   //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(3 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(30, 9); // last possible day of harvest
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(15, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) herbi
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(15, 10); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) ferti
		flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
		flexdates[3][1] = g_date->DayInYear(15, 10); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) ferti

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(dk_ec_deep_plough_sand))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
	// Here we queue up the first event
		if ((DK_EC_YEARS_AFTER_EST + g_date->GetYearNumber()) % 1 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(25, 2) + isSpring;
			SimpleEvent(d1, dk_ec_deep_plough_sand, false);
		}
		else if ((DK_EC_YEARS_AFTER_EST + g_date->GetYearNumber()) % 1 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
			SimpleEvent(d1, dk_ec_cut_trees, false);
		}
		break;
	}
	break;

	// LKM: This is the first real farm operation - done if much seed weed or setaside field
	case dk_ec_deep_plough_sand:
		if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
		{
			if (a_field->GetVegBiomass() > 0)
			{
				if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
					if (!m_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(10, 3) - g_date->DayInYear())) {
						SimpleEvent(g_date->Date() + 1, dk_ec_deep_plough_sand, true);
						break;
					}
				}
			}
			SimpleEvent(g_date->Date(), dk_ec_roll1_sand, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 2), dk_ec_deep_plough_clay, false);
		break;

	case dk_ec_roll1_sand:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->SpringRoll(m_field, 0.0, g_date->DayInYear(12, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_roll1_sand, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ec_slurry1_s_sand, false);
		break;

	case dk_ec_slurry1_s_sand:
		if (m_farm->IsStockFarmer()) { //Stock Farmer
			if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
			{
				if (!a_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(14, 3) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ec_slurry1_s_sand, true);
					break;
				}
			}
			SimpleEvent(g_date->Date() + 1, dk_ec_planting_sand, false);
			break;
		}
		else
		SimpleEvent(g_date->Date(), dk_ec_npk1_p_sand, false);
		break;

	case dk_ec_npk1_p_sand:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(14, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_npk1_p_sand, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() +1, dk_ec_planting_sand, false);
		break;

	case dk_ec_planting_sand:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->SpringSow(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_planting_sand, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ec_roll2, false);
		break;

	case dk_ec_deep_plough_clay:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_deep_plough_clay, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_ec_roll1_clay, false);
		break;

	case dk_ec_roll1_clay:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->SpringRoll(m_field, 0.0, g_date->DayInYear(27, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_roll1_clay, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ec_slurry1_s_clay, false);
		break;

	case dk_ec_slurry1_s_clay:
		if (m_farm->IsStockFarmer()) { //Stock Farmer
			if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
			{
				if (!a_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ec_slurry1_s_clay, true);
					break;
				}
			}
			SimpleEvent(g_date->Date() + 1, dk_ec_planting_clay, false);
			break;
		}
		else
			SimpleEvent(g_date->Date(), dk_ec_npk1_p_clay, false);
		break;

	case dk_ec_npk1_p_clay:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_npk1_p_clay, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ec_planting_clay, false);
		break;

	case dk_ec_planting_clay:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->SpringSow(m_field, 0.0, g_date->DayInYear(31, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_planting_clay, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ec_roll2, false);
		break;

	case dk_ec_roll2:
		if (!a_farm->SpringRoll(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_roll2, true);
			break;
		}

		// if time then herbicide before May, otherwise after May (not when budding in ~May)
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(30, 4)) {
			SimpleEvent(g_date->Date()+1, dk_ec_herbicide1, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ec_herbicide1, false);
		break;

	case dk_ec_herbicide1:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_herbicide1, true);
			break;
		}

		SimpleEvent(g_date->Date()+1, dk_ec_shallowharrow1, false);
		break;
	case dk_ec_shallowharrow1:		
		if (!a_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_shallowharrow1, true);
			break;
		}
		SimpleEvent(g_date->Date()+1, dk_ec_shallowharrow2, false);
		break;
	case dk_ec_shallowharrow2: 
		if (!a_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(2, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_shallowharrow2, true);
			break;
		}

		// After first growth season
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3)+365, dk_ec_cut_trees, false);
		break;

	case dk_ec_cut_trees:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_cut_trees, true);
			break;
		}

		// here comes a fork of parallel events:
		SimpleEvent(g_date->Date()+1, dk_ec_herbicide2, false); // herbi thread
		SimpleEvent(g_date->Date() + 1, dk_ec_slurry2_s, false); // fertilizer thread
		SimpleEvent(g_date->Date() + 1, dk_ec_shallowharrow3, false); // cultivation thread - main thread
		break;

	case dk_ec_herbicide2:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_herbicide2, true);
			break;
		}
		// if time then herbicide before May, otherwise after May (not when budding in ~May)
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(30, 4)) {
			SimpleEvent(g_date->Date() + 1, dk_ec_herbicide3, false);
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ec_herbicide3, false);
		break;

	case dk_ec_herbicide3:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_herbicide3, true);
			break;
		}
		break; // end of thread

	case dk_ec_slurry2_s:
		if (m_farm->IsStockFarmer()) { //Stock Farmer
			if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
			{
				if (!a_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ec_slurry2_s, true);
					break;
				}
			}
			break; //end of thread
		}
		else
		SimpleEvent(g_date->Date(), dk_ec_npk2_p, false);
		break;

	case dk_ec_npk2_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_npk2_p, true);
				break;
			}
		}
		break; //end of thread

	case dk_ec_shallowharrow3:
		if (!a_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_shallowharrow3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ec_shallowharrow4, false);
		break;

	case dk_ec_shallowharrow4:
		if (!a_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(2, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_shallowharrow4, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ec_harvest, false);
		break;

	case dk_ec_harvest:
		if (!a_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_harvest, true);
			break;
		}

		//fork with parallel events:
		SimpleEvent(g_date->Date() + 1, dk_ec_herbicide4, false); // herbi thread
		SimpleEvent(g_date->Date() + 1, dk_ec_slurry3_s, false); // fertilizer thread - main thread
		break;

	case dk_ec_herbicide4:
		if (!a_farm->HerbicideTreat(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ec_herbicide4, true);
			break;
		}
		break; // end of thread

	case dk_ec_slurry3_s:
		if (m_farm->IsStockFarmer()) { //Stock Farmer
			if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
			{
				if (!a_farm->FA_Slurry(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ec_slurry3_s, true);
					break;
				}
			}
			done = true;
			break;
		}
		else
		SimpleEvent(g_date->Date(), dk_ec_npk3_p, false);
		break;

	case dk_ec_npk3_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NPK(m_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ec_npk3_p, true);
				break;
			}
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
		default:
		g_msg->Warn(WARN_BUG, "DK_EnergyCrop_Perm::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}