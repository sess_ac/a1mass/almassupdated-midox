//
// DKOBroadBeans_test.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2015, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DKOBroadBeans_test.cpp This file contains the source for the DKOBroadBeans_test class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Marie Riddervold \n
Version of May 2020 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DKOBroadBeans_test.cpp
//

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DKOBroadBeans_test.h"


extern CfgFloat cfg_strigling_prop;

bool DKOBroadBeans_test::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev ) {
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;

	bool done = false;
	int d1 = 0;
	int d2 = 0;

	switch (m_ev->m_todo) {
	case dkobbtest_start:
		m_field->SetVegPatchy(true); // A crop with wide rows, so set patchy

		// Set up the date management stuff
		// Could save the start day in case it is needed later
		// m_field->m_startday = m_ev->m_startday;
		m_last_date = g_date->DayInYear(10, 10);
		// Start and stop dates for all events after harvest
		m_field->SetMDates(0, 0, g_date->DayInYear(1, 9));

		// Broad beans does not fit into the normal crop management, so must precede a spring sown crop (SpringBarleySpr probably!)

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if ((m_ev->m_startday > g_date->DayInYear(1, 7))   // this means spring start crop is OK !!Does this have to be the same as the start day i line 69?!!
				|| (m_field->GetMDates(0, 0) >= m_ev->m_startday)) {  // This checks that the spring start is not too early 
				char veg_type[20]; //!!What does this line mean!!
				sprintf(veg_type, "%d", m_ev->m_next_tov);
				g_msg->Warn(WARN_FILE, "DKOBroadBeans_test::Do(): Harvest too late for the next crop to start!!! The next crop is: ", veg_type);
				exit(1);
			}
			// Not allowed to fix any late finishing problems!
			/*
			for (int i = 0; i < noDates; i++) {
			if (m_field->GetMDates( 0, i ) >= m_ev->m_startday) {
			m_field->SetMDates( 0, i, m_ev->m_startday - 1 ); //move the starting date
			}
			if (m_field->GetMDates( 1, i ) >= m_ev->m_startday) {
			m_field->SetMConstants( i, 0 );
			m_field->SetMDates( 1, i, m_ev->m_startday - 1 ); //move the finishing date
			}
			}
			*/

			if (!m_ev->m_first_year) {
				// Here we need to allow a start before 1/7 in case we run over the year - the only crop to do this (2802215)
				d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
				if (g_date->Date() > d1) {
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "DKOBroadBeans_test::Do(): Crop start attempt after last possible start date", "");
					exit(1);
				}
			}
			else {
				// If this is the first year of running then it is possible to start
				// on day 0, so need this to tell us what to do:
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(25, 2),
					dkobbtest_spring_harrow, false);
				break;
			}
		}//if
		// End single block date checking code. Please see next line
		// comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(25, 2); // The first date an event (management action) can take place
		if (g_date->Date() > d1) {
			d1 = g_date->Date();
		}
		// OK, let's go.
		SimpleEvent(d1, dkobbtest_spring_harrow, false);
		break;

	case dkobbtest_spring_harrow:
		if (m_ev->m_lock || m_farm->DoIt(100)) { // 100% of the farmers do this action !!does this DoIt have to be here in the first case!!
			if (!m_farm->SpringHarrow(m_field, 0.0,
				g_date->DayInYear(25, 3) - // !!Should this be the last day the event should take place!!
				g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again (until 25/3 when we will suceed)
				SimpleEvent(g_date->Date() + 1, dkobbtest_spring_harrow, true);
				break;
			}
		}
		d1 = g_date->Date() + 1; // !!Does this section set up the next even?!!
		d2 = g_date->OldDays() + g_date->DayInYear(1, 3); //If date 1 (d1) is less or equal to day 2 (d2) then the next event (spring plogh) can't take place (false)
		if (d1 < d2) d1 = d2;
		SimpleEvent(d1, dkobbtest_spring_plough, false);
		break;

	case dkobbtest_spring_plough:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dkobbtest_spring_plough, true);
			break; // If we don't suceed on the first try, then try and try again (until 1/4 when we will suceed)
		}
		d1 = g_date->Date();
		d2 = g_date->OldDays() + g_date->DayInYear(5, 3);
		if (d1 < d2) d1 = d2;
		SimpleEvent(d1, dkobbtest_ferti_k_s, false);
		break;

	case dkobbtest_ferti_k_s:
		if (!m_farm->SpringFertiKS(m_field, 0.0, g_date->DayInYear(5, 4) - g_date->DayInYear())) 
		{ // !!The SpringFertiKS is not valid, How do I make it valid?!!
			SimpleEvent(g_date->Date() + 1, dkobbtest_ferti_k_s, true);
			break;
		}
		if (a_farm->DoIt(50)) SimpleEvent(g_date->Date(), dkobbtest_broad_sow, false);
		else SimpleEvent(g_date->Date(), dkobbtest_row_sow, false);
		break;

	case dkobbtest_broad_sow:
		if (!m_farm->BroadSow(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) { //!! How do I add broad sow?!!
			SimpleEvent(g_date->Date() + 1, dkobbtest_broad_sow, true);
				break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 3), dkobbtest_strigling1, false);
		break;

	case dkobbtest_row_sow:
		if (!m_farm->RowSow(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) { //!! How do I add row sow?!!
			SimpleEvent(g_date->Date() + 1, dkobbtest_broad_sow, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 3), dkobbtest_strigling1, false);
		break;

	case dkobbtest_strigling1:
		if (m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt(100))) {
			if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dkobbtest_strigling1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dkobbtest_strigling2, false);
		break;

	case dkobbtest_strigling2:
		// 50% of the farmers will stringlig2, which is stirling one time
		if (a_ev->m_lock || a_farm->DoIt(100))
			if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dkobbtest_strigling2, true);
				break;
			}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 4), dkobbtest_watering, false);
		break;

	case dkobbtest_strigling3:
		// 50% of the farmers will go this, this is strigling 1-2 times
		if (a_ev->m_lock || a_farm->DoIt(50))
			if (!m_farm->Strigling(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) { //!! How do I write so they do it twice!!
				SimpleEvent(g_date->Date() + 1, dkobbtest_broad_sow, true);
				break;
			}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(5, 4), dkobbtest_watering, false);
		break;

	case dkobbtest_watering:
		// The farmers will water as many times as needed !! is there a way to have them water "when needed"!!
		if (a_ev->m_lock || a_farm->DoIt(100)) 
		{ 
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(25, 9) - g_date->DayInYear())) 
			{
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dkobbtest_harvest, false);
		break;


	case dkobbtest_harvest:
		if (!m_farm->Harvest(m_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			// If we don't suceed on the first try, then try and try again (until 1/10 when we will suceed)
			SimpleEvent(g_date->Date() + 1, dkobbtest_harvest, true);
			break;
		}
		d1 = g_date->Date();
		d2 = g_date->OldDays() + g_date->DayInYear(1, 9);
		if (d1 < d2) d1 = d2;
		SimpleEvent(d1, dkobbtest_autumn_harrow, false);
		break;

	case dkobbtest_autumn_harrow:
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dkobbtest_autumn_harrow, true);
			break;
		}
		SimpleEvent(g_date->Date(), dkobbtest_autumn_plough, false);
		break;

	case dkobbtest_autumn_plough:
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dkobbtest_autumn_plough, true);
			break;
		}
		SimpleEvent(g_date->Date(), dkobbtest_sow_catchcrop, false);
		break;

	case dkobbtest_sow_catchcrop:
		// 33.3% of the farmers will sow catch crop
		if (a_ev->m_lock || a_farm->DoIt(33))
			if (!m_farm->SowCatchCrop(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dkobbtest_broad_sow, true);
				break;
			}
			else
			{
				//Too remember who sowed ctach crop !!I don't know if this is correct!!
				DKOBBTEST_SOW_CATCHCROP = true;
			}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(24, 2), dkobbtest_wait, false);
		break;
	case dkobbtest_sow_wintercrop:
		// 66.6% of the farmers will sow winter crop
		if (a_ev->m_lock || a_farm->DoIt(66))
			if (!m_farm->SowWinterCrop(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dkobbtest_sow_wintercrop, true);
				break;
			}
			else
			{
				//Too remember who sowed wintercrop !!I don't know if this is correct!!
				DKOBBTEST_SOW_WINTERCROP = true;
			}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(24, 2), dkobbtest_wait, false);
		break;

	case dkobbtest_wait:
		// End Main Thread
		m_field->SetVegPatchy(false); // reverse the patchy before the next crop
		done = true;
		break;


	default:
		g_msg->Warn(WARN_BUG, "DKOBroadBeans::Do(): "	"Unknown event type! ", "");
		exit(1);
	}

	return done;
}


