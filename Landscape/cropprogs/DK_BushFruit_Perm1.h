/**
\file
\brief
<B>DK_BushFruit_Perm1.h This file contains the source for the DK_BushFruit_Perm1 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of July 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_BushFruit_Perm1.h
//


#ifndef DK_BUSHFRUIT1_P_H
#define DK_BUSHFRUIT1_P_H

#define DK_BFP1_BASE 62500
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DK_BFP_AFTER_EST	a_field->m_user[0]
#define DK_BFP_EARLY_HARVEST a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_bfp1_start = 1, // Compulsory, must always be 1 (one).
	dk_bfp1_sleep_all_day = DK_BFP1_BASE,
	dk_bfp1_molluscicide,
	dk_bfp1_fertilizer1_s,
	dk_bfp1_fertilizer1_p,
	dk_bfp1_spring_harrow1,
	dk_bfp1_spring_harrow2,
	dk_bfp1_spring_harrow3,
	dk_bfp1_herbicide1,
	dk_bfp1_shallow_harrow1,
	dk_bfp1_shallow_harrow2,
	dk_bfp1_insecticide1, 
	dk_bfp1_insecticide2,
	dk_bfp1_plant,
	dk_bfp1_water1,
	dk_bfp1_strigling1,
	dk_bfp1_strigling2,
	dk_bfp1_row_cultivation1,
	dk_bfp1_row_cultivation2,
	dk_bfp1_row_cultivation3,
	dk_bfp1_fertilizer2_s,
	dk_bfp1_fertilizer2_p,
	dk_bfp1_fungicide1,
	dk_bfp1_water2,
	dk_bfp1_herbicide2,
	dk_bfp_cover_on,
	dk_bfp_cover_off,
	dk_bfp_fertilizer1_s,
	dk_bfp_fertilizer1_p,
	dk_bfp_water1,
	dk_bfp_water1_cover,
	dk_bfp_row_cultivation1,
	dk_bfp_row_cultivation2,
	dk_bfp_row_cultivation3,
	dk_bfp_herbicide1,
	dk_bfp_insecticide1,
	dk_bfp_insecticide2,
	dk_bfp_insecticide3,
	dk_bfp_fungicide1,
	dk_bfp_fungicide2,
	dk_bfp_fungicide3,
	dk_bfp_fungicide4,
	dk_bfp_fungicide5,
	dk_bfp_fungicide6,
	dk_bfp_fungicide7,
	dk_bfp_fungicide8,
	dk_bfp_water2,
	dk_bfp_straw_cover,
	dk_bfp_harvest,
	dk_bfp_herbicide2,
	dk_bfp_fertilizer2_s,
	dk_bfp_fertilizer2_p,
	dk_bfp_water3,
	dk_bfp_herbicide3,
	dk_bfp1_foobar,
} DK_BushFruit_Perm1ToDo;


/**
\brief
DK_BushFruit_Perm1 class
\n
*/
/**
See DK_BushFruit_Perm1.h::DK_BushFruit_Perm1ToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_BushFruit_Perm1: public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_BushFruit_Perm1(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,12 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_bfp1_foobar - DK_BFP1_BASE);
	   m_base_elements_no = DK_BFP1_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,	// dk_bfp1_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	// dk_bfp1_sleep_all_day = // dk_BFP1_BASE,
			fmc_Insecticide,	// dk_bfp1_molluscicide,
			fmc_Fertilizer,	// dk_bfp1_fertilizer1_s,
			fmc_Fertilizer,	// dk_bfp1_fertilizer1_p,
			fmc_Cultivation,	// dk_bfp1_spring_harrow1,
			fmc_Cultivation,	// dk_bfp1_spring_harrow2,
			fmc_Cultivation,	// dk_bfp1_spring_harrow3,
			fmc_Herbicide,	// dk_bfp1_herbicide1,
			fmc_Cultivation,	// dk_bfp1_shallow_harrow1,
			fmc_Cultivation,	// dk_bfp1_shallow_harrow2,
			fmc_Insecticide,	// dk_bfp1_insecticide, 
			fmc_Insecticide,	// dk_bfp1_insecticide, 
			fmc_Others,	// dk_bfp1_plant,
			fmc_Watering,	// dk_bfp1_water1,
			fmc_Others,	// dk_bfp1_strigling1,
			fmc_Others,	// dk_bfp1_strigling2,
			fmc_Cultivation,	// dk_bfp1_row_cultivation1,
			fmc_Cultivation,	// dk_bfp1_row_cultivation2,
			fmc_Cultivation,	// dk_bfp1_row_cultivation3,
			fmc_Fertilizer,	// dk_bfp1_fertilizer2_s,
			fmc_Fertilizer,	// dk_bfp1_fertilizer2_p,
			fmc_Fungicide,	// dk_bfp1_fungicide1,
			fmc_Watering,	// dk_bfp1_water2,
			fmc_Herbicide,	// dk_bfp1_herbicide2,
			fmc_Others,	//	dk_bfp_cover_on,
			fmc_Others,	//	dk_bfp_cover_off,
			fmc_Fertilizer,	//	dk_bfp_fertilizer1_s,
			fmc_Fertilizer,	//	dk_bfp_fertilizer1_p,
			fmc_Watering,	//	dk_bfp_water1,
			fmc_Watering,	//	dk_bfp_water1_cover,
			fmc_Cultivation,	//	dk_bfp_row_cultivation1,
			fmc_Cultivation,	//	dk_bfp_row_cultivation2,
			fmc_Cultivation,	//	dk_bfp_row_cultivation3,
			fmc_Herbicide,	//	dk_bfp_herbicide1,
			fmc_Insecticide,	//	dk_bfp_insecticide,
			fmc_Insecticide,	//	dk_bfp_insecticide,
			fmc_Insecticide,	//	dk_bfp_insecticide,
			fmc_Fungicide,	//	dk_bfp_fungicide1,
			fmc_Fungicide,	//	dk_bfp_fungicide2,
			fmc_Fungicide,	//	dk_bfp_fungicide3,
			fmc_Fungicide,	//	dk_bfp_fungicide4,
			fmc_Fungicide,	//	dk_bfp_fungicide5,
			fmc_Fungicide,	//	dk_bfp_fungicide6,
			fmc_Fungicide,	//	dk_bfp_fungicide7,
			fmc_Fungicide,	//	dk_bfp_fungicide8,
			fmc_Watering,	//	dk_bfp_water2,
			fmc_Others,	//	dk_bfp_straw_cover,
			fmc_Harvest,	//	dk_bfp_harvest,
			fmc_Herbicide,	//	dk_bfp_herbicide2,
			fmc_Fertilizer,	//	dk_bfp_fertilizer2_s,
			fmc_Fertilizer,	//	dk_bfp_fertilizer2_p,
			fmc_Watering,	//	dk_bfp_water3,
			fmc_Herbicide	//	dk_bfp_herbicide3,
				// no foobar entry

	   };
	   // Iterate over the catlist elements and copy them to vector
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};

#endif // DK_BushFruit_Perm1_H

