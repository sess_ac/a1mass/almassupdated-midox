/**
\file
\brief
<B>PLMaize.h This file contains the headers for the Maize class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLMaize.h
//


#ifndef PLMAIZE_H
#define PLMAIZE_H

#define PLMAIZE_BASE 25600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_M_FERTI_P1	a_field->m_user[1]
#define PL_M_FERTI_S1	a_field->m_user[2]
#define PL_M_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_M_WINTER_PLOUGH	a_field->m_user[4]
#define PL_M_FERTI_P3	a_field->m_user[5]
#define PL_M_FERTI_S3	a_field->m_user[6]
#define PL_M_SPRING_FERTI a_field->m_user[7]
#define PL_M_START_FERTI	a_field->m_user[8]

/** Below is the list of things that a farmer can do if he is growing mazie, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_m_start = 1, // Compulsory, must always be 1 (one).
	pl_m_sleep_all_day = PLMAIZE_BASE,
	pl_m_ferti_p1, // 20701
	pl_m_ferti_s1,
	pl_m_stubble_plough,
	pl_m_autumn_harrow1,
	pl_m_autumn_harrow2,
	pl_m_stubble_harrow,
	pl_m_ferti_p2,
	pl_m_ferti_s2,
	pl_m_winter_plough,
	pl_m_winter_stubble_cultivator_heavy,	// 20710
	pl_m_ferti_p3,
	pl_m_ferti_s3,
	pl_m_spring_harrow,
	pl_m_ferti_p4,
	pl_m_ferti_s4,
	pl_m_ferti_p5,
	pl_m_ferti_s5,
	pl_m_heavy_cultivator,
	pl_m_preseeding_cultivator,
	pl_m_spring_sow_with_ferti,
	pl_m_spring_sow,	// 20720	
	pl_m_herbicide1,
	pl_m_herbicide2,
	pl_m_fungicide1,
	pl_m_insecticide1,
	pl_m_insecticide2,
	pl_m_biocide,
	pl_m_ferti_p6,
	pl_m_ferti_s6,	
	pl_m_ferti_p7,
	pl_m_ferti_s7,	// 20730
	pl_m_harvest,
	pl_m_straw_chopping,
	pl_m_hay_bailing,
	pl_m_ferti_p8,
	pl_m_ferti_s8,
	pl_m_ferti_p9,	
	pl_m_ferti_s9,	// 20737
	pl_m_foobar
} PLMaizeToDo;


/**
\brief
PLMaize class
\n
*/
/**
See PLMaize.h::PLMaizeToDo for a complete list of all possible events triggered codes by the mazie management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLMaize: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLMaize(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_m_foobar - PLMAIZE_BASE);
	   m_base_elements_no = PLMAIZE_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_m_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_m_sleep_all_day = PLMAIZE_BASE,
			fmc_Fertilizer,//pl_m_ferti_p1, // 20701
			fmc_Fertilizer,//pl_m_ferti_s1,
			fmc_Cultivation,//pl_m_stubble_plough,
			fmc_Cultivation,//pl_m_autumn_harrow1,
			fmc_Cultivation,//pl_m_autumn_harrow2,
			fmc_Cultivation,//pl_m_stubble_harrow,
			fmc_Fertilizer,//pl_m_ferti_p2,
			fmc_Fertilizer,//pl_m_ferti_s2,
			fmc_Cultivation,//pl_m_winter_plough,
			fmc_Cultivation,//pl_m_winter_stubble_cultivator_heavy,	// 20710
			fmc_Fertilizer,//pl_m_ferti_p3,
			fmc_Fertilizer,//pl_m_ferti_s3,
			fmc_Cultivation,//pl_m_spring_harrow,
			fmc_Fertilizer,//pl_m_ferti_p4,
			fmc_Fertilizer,//pl_m_ferti_s4,
			fmc_Fertilizer,//pl_m_ferti_p5,
			fmc_Fertilizer,//pl_m_ferti_s5,
			fmc_Cultivation,//pl_m_heavy_cultivator,
			fmc_Cultivation,//pl_m_preseeding_cultivator,
			fmc_Fertilizer,//pl_m_spring_sow_with_ferti,
			fmc_Others,//pl_m_spring_sow,	// 20720	
			fmc_Herbicide,//pl_m_herbicide1,
			fmc_Herbicide,//pl_m_herbicide2,
			fmc_Fungicide,//pl_m_fungicide1,
			fmc_Insecticide,//pl_m_insecticide1,
			fmc_Insecticide,//pl_m_insecticide2,
			fmc_Herbicide,//pl_m_biocide,
			fmc_Fertilizer,//pl_m_ferti_p6,
			fmc_Fertilizer,//pl_m_ferti_s6,
			fmc_Fertilizer,//pl_m_ferti_p7,
			fmc_Fertilizer,//pl_m_ferti_s7,	// 20730
			fmc_Harvest,//pl_m_harvest,
			fmc_Others,//pl_m_straw_chopping,
			fmc_Others,//pl_m_hay_bailing,
			fmc_Fertilizer,//pl_m_ferti_p8,
			fmc_Fertilizer,//pl_m_ferti_s8,
			fmc_Fertilizer,//pl_m_ferti_p9,
			fmc_Fertilizer//pl_m_ferti_s9,	// 20737

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLMAIZE_H

