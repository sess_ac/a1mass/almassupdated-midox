//
// OWinterRape.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWINTERRAPE_H
#define OWINTERRAPE_H

// <cropname>_BASE is the first event number to be dumped into the
// debugging log from this crop. *Must* be unique among all crops.
// I suggest steps of 100 between crops.

#define OWINTERRAPE_BASE 4700
#define OWR_DID_RC_CLEAN     m_field->m_user[0]
#define OWR_DID_HERBI_ZERO   m_field->m_user[1]
#define OWR_INSECT_DATE      m_field->m_user[2]
#define OWR_FUNGI_DATE       m_field->m_user[3]
#define OWR_SWARTH_DATE      m_field->m_user[4]

typedef enum {
  owr_start = 1, // Compulsory, start event must always be 1 (one).
  owr_fa_manure = OWINTERRAPE_BASE,
  owr_fp_manure,
  owr_autumn_plough,
  owr_autumn_harrow,
  owr_autumn_sow,
  owr_row_cultivation_one,
  owr_row_cultivation_two,
  owr_fa_slurry,
  owr_fp_slurry,
  owr_row_cultivation_three,
  owr_swarth,
  owr_harvest,
  owr_straw_chop,
  owr_stub_harrow,
  owr_foobar,
} OWinterRapeToDo;



class OWinterRape: public Crop
{
  // Private methods corresponding to the different management steps
  // (when needed).

public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OWinterRape(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(21,8);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (owr_foobar - OWINTERRAPE_BASE);
	  m_base_elements_no = OWINTERRAPE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  owr_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Fertilizer,	//	  owr_fa_manure = OWINTERRAPE_BASE,
			fmc_Fertilizer,	//	  owr_fp_manure,
			fmc_Cultivation,	//	  owr_autumn_plough,
			fmc_Cultivation,	//	  owr_autumn_harrow,
			fmc_Others,	//	  owr_autumn_sow,
			fmc_Cultivation,	//	  owr_row_cultivation_one,
			fmc_Cultivation,	//	  owr_row_cultivation_two,
			fmc_Fertilizer,	//	  owr_fa_slurry,
			fmc_Fertilizer,	//	  owr_fp_slurry,
			fmc_Cultivation,	//	  owr_row_cultivation_three,
			fmc_Cutting,	//	  owr_swarth,
			fmc_Harvest,	//	  owr_harvest,
			fmc_Others,	//	  owr_straw_chop,
			fmc_Cultivation	//	  owr_stub_harrow,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OWINTERRAPE_H
