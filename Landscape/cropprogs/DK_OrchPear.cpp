/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OrchPear.cpp This file contains the source for the DK_OrchPear class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of November 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OrchPear.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OrchPear.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_orchard_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_ope_InsecticideDay;
extern CfgInt   cfg_ope_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_OrchPear::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DKOrchPear;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_ope_start:
	{
		// dk_ope_start just sets up all the starting conditions and reference dates that are needed to start a dk_ope

		DK_OPE_YEARS_AFTER_PLANT = 0;

		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(31, 12); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 12); // last possible day of cutting orch
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(dk_ope_spring_plough))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		// Here we queue up the first event
		//Each field has assign randomly a DK_oap_YEARS_AFTER_PLANT 
		if (m_ev->m_forcespring) {
			if ((DK_OPE_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 1 == 1)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
				SimpleEvent(d1, dk_ope_spring_plough, false);
			}
			else if ((DK_OPE_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 1 == 0)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + isSpring;
				SimpleEvent(d1, dk_ope_sleep_all_day, false);
			}
			break;
		}
		else
		if ((DK_OPE_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 1 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
			SimpleEvent(d1, dk_ope_spring_plough, false);
		}
		else if ((DK_OPE_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 1 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 1) + isSpring;
			SimpleEvent(d1, dk_ope_sleep_all_day, false);
		}
		break;

	}
	break;

	// LKM: This is the first real farm operation 
	case dk_ope_spring_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_spring_plough, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_ope_manure_s1, false);
		break;

	case dk_ope_manure_s1:
		if (a_farm->IsStockFarmer()) {
			if (!a_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_manure_s1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, dk_ope_water1, false);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ope_manure_p1, false);
		break;

	case dk_ope_manure_p1:
		if (!a_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manure_p1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ope_water1, false);
		break;

	case dk_ope_water1:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_water1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_ope_subsoiler, false);
		break;

	case dk_ope_subsoiler:
		if (!a_farm->DeepPlough(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_subsoiler, true);
			break;
		}
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent(d1, dk_ope_planting, false);
		break;
	case dk_ope_planting:
		if (!a_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_planting, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ope_sow_grass, false); // sow thread
		SimpleEvent(g_date->Date(), dk_ope_manual_cutting5, false); // cutting thread
		d1 = g_date->Date();
		if (d1 > g_date->OldDays() + g_date->DayInYear(1, 5)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 5);
		}
		SimpleEvent(d1, dk_ope_herbicide1, false); // herbicide thread
		break;

	case dk_ope_sow_grass:
		if (!a_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_sow_grass, true);
			break;
		}
		break; // end of thread

	case dk_ope_herbicide1: // Starane
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_herbicide1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ope_herbicide2, false);
		break;

	case dk_ope_herbicide2:// Round Up
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_herbicide2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_ope_herbicide3, false);
		break;

	case dk_ope_herbicide3: // MCPA
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_herbicide3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 11), dk_ope_herbicide4, false);
		break;

	case dk_ope_herbicide4: // Round Up
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_herbicide4, true);
			break;
		}
		done = true;
		break; // end of establishment

		// start of years after planting year:
	case dk_ope_sleep_all_day:
		if (!a_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(31, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_sleep_all_day, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_ope_manual_cutting1, false);
		break;

	case dk_ope_manual_cutting1:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting2, false); // thread for manual cutting (once a month)
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_ope_manure_s2, false); // main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_ope_herbicide5, false); // herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_ope_fungicide1, false); // fungicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_insecticide1, false); // insecticide1 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_insecticide2, false); // insecticide2 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_insecticide3, false); // insecticide3 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_gr1, false); // growth regulator1 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_gr3, false); // growth regulator3 thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_ope_boron_s, false); // fertilizer thread
		break;
	case dk_ope_manual_cutting2:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(28, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting3, false);
		break;
	case dk_ope_manual_cutting3:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting4, false);
		break;
	case dk_ope_manual_cutting4:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting5, false);
		break;
	case dk_ope_manual_cutting5:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting6, false);
		break;
	case dk_ope_manual_cutting6:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting7, false);
		break;
	case dk_ope_manual_cutting7:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting7, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting8, false);
		break;
	case dk_ope_manual_cutting8:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting9, false);
		break;
	case dk_ope_manual_cutting9:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting9, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting10, false);
		break;
	case dk_ope_manual_cutting10:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting10, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_manual_cutting11, false);
		break;
	case dk_ope_manual_cutting11:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting11, true);
			break;
		}
		break;
		// end of cutting thread

	case dk_ope_herbicide5: // Round Up
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_herbicide5, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_ope_herbicide6, false);
		break;

	case dk_ope_herbicide6: // Starane
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_herbicide6, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ope_herbicide7, false);
		break;

	case dk_ope_herbicide7:// Round Up
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_herbicide7, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_ope_herbicide8, false);
		break;

	case dk_ope_herbicide8: // MCPA
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_herbicide8, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 11), dk_ope_herbicide9, false);
		break;

	case dk_ope_herbicide9: // Round Up
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_herbicide9, true);
			break;
		}
		break; // end of thread


		// fertilizer thread:
	case dk_ope_boron_s:
		if (a_farm->IsStockFarmer()) {
			if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
				if (!a_farm->FA_Boron(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_ope_boron_s, true);
					break;
				}
			}
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_ope_boron_p, false);
		break;

	case dk_ope_boron_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
			if (!a_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_boron_p, true);
				break;
			}
		}
		break;
		// end of fertilizer thread
		// pesticide thread:
	case dk_ope_fungicide1: //Syllit
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_fungicide1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+10, dk_ope_fungicide2, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_fungicide3, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_fungicide4, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_fungicide5, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_fungicide7, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_fungicide8, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_fungicide9, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ope_fungicide13, false);
		break;

	case dk_ope_fungicide2: // Syllit
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) { 
			if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_fungicide2, true);
				break;
			}
		}
		break;

	case dk_ope_fungicide3: // Candit
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_fungicide3, true);
				break;
			}
		}	
		break;

	case dk_ope_fungicide4: // Scala
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide4, true);
			break;
		}
		break;

	case dk_ope_fungicide5: // Kumulus S
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(22, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 7, dk_ope_fungicide6, false);
		break;

	case dk_ope_fungicide6: // Kumulus S
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide6, true);
			break;
		}
		break;

	case dk_ope_fungicide7: // Difcor
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide7, true);
			break;
		}
		break;

	case dk_ope_fungicide8: // Talius
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide8, true);
			break;
		}
		break;

	case dk_ope_fungicide9: // Delan WG
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide9, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ope_fungicide10, false);
		break;

	case dk_ope_fungicide10: // Delan WG
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide10, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ope_fungicide11, false);
		break;

	case dk_ope_fungicide11: // Delan WG
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide11, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_ope_fungicide12, false);
		break;

	case dk_ope_fungicide12: // Delan WG
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(12, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide12, true);
			break;
		}
		break;

	case dk_ope_fungicide13: // Kumulus S
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(23, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide13, true);
			break;
		}
		SimpleEvent(g_date->Date() + 7, dk_ope_fungicide14, false);
		break;

	case dk_ope_fungicide14: // Kumulus S
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_fungicide14, true);
			break;
		}
		break;
		// end of thread

	case dk_ope_insecticide1: // Teppeki/Mospilan
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(30, 4) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide1, true);
				break;
			}
		}
		break;

	case dk_ope_insecticide2: // Lamdex
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(30, 4) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide2, true);
				break;
			}
		}
		break;

	case dk_ope_insecticide3: // Steward
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(30, 4) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide3, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_insecticide4, false);
		break;

	case dk_ope_insecticide4: // Mospilan
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(31, 5) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide4, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ope_insecticide5, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_ope_insecticide8, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_ope_insecticide9, false);
		break;

	case dk_ope_insecticide5:// Madex Top
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(10, 8) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide5, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+10, dk_ope_insecticide6, false);
		break;

	case dk_ope_insecticide6:// Madex Top
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(20, 8) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide6, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 10, dk_ope_insecticide7, false);
		break;

	case dk_ope_insecticide7:// Madex Top
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(31, 8) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide7, true);
				break;
			}
		}
		break;

	case dk_ope_insecticide8:// Teppeki
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(31, 8) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide8, true);
				break;
			}
		}
		break;

	case dk_ope_insecticide9:// Signum
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(2, 9) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide9, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 10, dk_ope_insecticide10, false);
		break;

	case dk_ope_insecticide10:// Signum
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			// here we check whether we are using ERA pesticide or not
			d1 = g_date->DayInYear(12, 9) - g_date->DayInYear();
			if (!cfg_pest_orchard_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent(g_date->Date() + 1, dk_ope_insecticide10, true);
				break;
			}
		}
		break; // end of thread

			//GR threads
	case dk_ope_gr1: // Cerone
		if (!a_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_gr1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 7, dk_ope_gr2, false);
		break;

	case dk_ope_gr2: // Cerone
		if (!a_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_gr2, true);
			break;
		}
		break;

	case dk_ope_gr3: // Regalis plus
		if (!a_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_gr3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 21, dk_ope_gr4, false);
		break;

	case dk_ope_gr4: // Regalis Plus
		if (!a_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_gr4, true);
			break;
		}
		break;
		// end of GRs threads

	case dk_ope_manure_s2:
		if (a_farm->IsStockFarmer()) {
			if (!a_farm->FA_Manure(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_manure_s2, true);
				break;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_row_cultivation1, false); //row cultivation thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_water2, false); // water thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_ope_cutting1, false); // cutting grass thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), dk_ope_remove_fruits, false); // main thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_ope_manure_p2, false); 
		break;

	case dk_ope_manure_p2:
		if (!a_farm->FP_Manure(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manure_s2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_row_cultivation1, false); //row cultivation thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_ope_water2, false); // water thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_ope_cutting1, false); // cutting grass thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), dk_ope_remove_fruits, false); // main thread
		break;

	case dk_ope_row_cultivation1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) { // suggestion - not so often in conv. orchards
			if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_row_cultivation1, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), dk_ope_row_cultivation2, false);
		break;

	case dk_ope_row_cultivation2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) { // suggestion - not so often in conv. orchards
			if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_ope_row_cultivation2, true);
				break;
			}
		}
		break;
		// end of thread

		//Here comes water thread:
	case dk_ope_water2:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_water2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_water3, false);
		break;
	case dk_ope_water3:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_water3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_water4, false);
		break;
	case dk_ope_water4:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_water4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_water5, false);
		break;
	case dk_ope_water5:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_water5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_water6, false);
		break;
	case dk_ope_water6:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_water6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_water7, false);
		break;
	case dk_ope_water7:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_water7, true);
			break;
		}
		break; 
		// end of thread
		// cutting grass thread:

	case dk_ope_cutting1:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_cutting1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_cutting2, false);
		break;
	case dk_ope_cutting2:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_cutting2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_cutting3, false);
		break;
	case dk_ope_cutting3:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(1, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_cutting3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_cutting4, false);
		break;
	case dk_ope_cutting4:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_cutting4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_cutting5, false);
		break;
	case dk_ope_cutting5:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_cutting5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 25, dk_ope_cutting6, false);
		break;
	case dk_ope_cutting6:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(1, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_cutting6, true);
			break;
		}
		break;
		// end of thread

	case dk_ope_remove_fruits:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_remove_fruits, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_ope_harvest, false);
		break;
	case dk_ope_harvest:
		if (!a_farm->FruitHarvest(m_field, 0.0, g_date->DayInYear(1, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_harvest, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 12), dk_ope_manual_cutting12, false);
		break;
	case dk_ope_manual_cutting12:
		if (!a_farm->Pruning(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_ope_manual_cutting12, true);
			break;
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop (DK_OrchPear)
		// END of MAIN THREAD
		break;
		default:
		g_msg->Warn(WARN_BUG, "DK_OrchPear::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}