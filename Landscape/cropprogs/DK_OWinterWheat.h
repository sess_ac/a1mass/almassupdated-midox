/**
\file
\brief
<B>DK_WinterWheat.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// WinterWheat.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OWINTERWHEAT_H
#define DK_OWINTERWHEAT_H

#define DK_OWW_BASE 61200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define DK_OWW_AUTUMN_PLOUGH        a_field->m_user[1]
#define DK_OWW_CC       a_field->m_user[2]
#define DK_OWW_DECIDE_TO_FI         a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
  dk_oww_start = 1, // Compulsory, must always be 1 (one).
  dk_oww_harvest = DK_OWW_BASE,
  dk_oww_autumn_harrow,
  dk_oww_autumn_plough,
  dk_oww_autumn_sow,
  dk_oww_slurry1_s,
  dk_oww_slurry1_p,
  dk_oww_sow_catchcrops,
  dk_oww_slurry2_s,
  dk_oww_slurry2_p,
  dk_oww_water1,
  dk_oww_water2,
  dk_oww_straw_chopping,
  dk_oww_hay_baling,
  dk_oww_wait,
  dk_oww_foobar,
} DK_OWinterWheatToDo;


/**
\brief
DK_OWinterWheat class
\n
*/
/**
See DK_WinterWheat.h::DK_WinterWheatToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_OWinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_OWinterWheat(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,10 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_oww_foobar - DK_OWW_BASE);
	   m_base_elements_no = DK_OWW_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_oww_start = 1, // Compulsory, must always be 1 (one).
			fmc_Harvest,	//	  dk_oww_harvest = DK_OWW_BASE,
			fmc_Cultivation,	//	  dk_oww_autumn_harrow,
			fmc_Cultivation,	//	  dk_oww_autumn_plough,
			fmc_Others,	//	  dk_oww_autumn_sow,
			fmc_Fertilizer,	//	  dk_oww_slurry1_s,
			fmc_Fertilizer,	//	  dk_oww_slurry1_p,
			fmc_Others,	//	  dk_oww_sow_catchcrops,
			fmc_Fertilizer,	//	  dk_oww_slurry2_s,
			fmc_Fertilizer,	//	  dk_oww_slurry2_p,
			fmc_Watering,	//	  dk_oww_water1,
			fmc_Watering,	//	  dk_oww_water2,
			fmc_Others,	//	  dk_oww_straw_chopping,
			fmc_Others,	//	  dk_oww_hay_baling,
			fmc_Others, // dk_oww_wait,


			   // no foobar entry	

	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // DK_OWINTERWHEAT_H

