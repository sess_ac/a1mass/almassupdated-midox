/**
\file
\brief
<B>PTShrubPastures.h This file contains the headers for the ShrubPastures class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTShrubPastures.h
//


#ifndef PTSHRUBPASTURES_H
#define PTSHRUBPASTURES_H

#define PTSHRUBPASTURES_BASE 31000
/**
\brief A flag used to indicate cattle out dates
*/
#define PT_FL1_CATTLEOUT_DATE a_field->m_user[1]
#define PT_FL2_CATTLEOUT_DATE a_field->m_user[2]
#define PT_FL3_CATTLEOUT_DATE a_field->m_user[3]
#define PT_SP_YEARS_AFTER_PLOUGH a_field->m_user[4]

/** Below is the list of things that a farmer can do if he is growing ShrubPastures, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_sp_start = 1, // Compulsory, must always be 1 (one).
	pt_sp_sleep_all_day = PTSHRUBPASTURES_BASE,
	pt_sp_cattle_out1,
	pt_sp_cattle_is_out1,
	pt_sp_spring_plough,
	pt_sp_cattle_out2,
	pt_sp_cattle_is_out2,
	pt_sp_cattle_out3,
	pt_sp_cattle_is_out3,
	pt_sp_wait,

} PTShrubPasturesToDo;


/**
\brief
PTShrubPastures class
\n
*/
/**
See PTShrubPastures.h::PTShrubPasturesToDo for a complete list of all possible events triggered codes by the ShrubPastures management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTShrubPastures: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTShrubPastures(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 31th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,1 );//Last possible date to do the first operation, i.e. plough or sow
   }
};

#endif // PTSHRUBPASTURES_H

