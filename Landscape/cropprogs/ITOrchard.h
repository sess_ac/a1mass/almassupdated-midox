/**
\file
\brief
<B>ITOrchard.h This file contains the headers for the ITOrchard class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of March 2022 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// ITOrchard.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef ITORCHARD_H
#define ITORCHARD_H

#define IT_O_EARLY a_field->m_user[1]
#define IT_O_MID a_field->m_user[2]
#define IT_O_LATE a_field->m_user[3]

#define IT_O_BASE 50100
/**
\
*/

/** Below is the list of things that a farmer can do if he is growing ITOrchard, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	it_o_start = 1, // Compulsory, must always be 1 (one).
	it_o_sleep_all_day = IT_O_BASE,
	it_o_winter_pruning,
	it_o_ferti_s1,
	it_o_ferti_p1,
	it_o_irrigation_early,
	it_o_irrigation_mid,
	it_o_irrigation_late,
	it_o_fungi_1,
	it_o_fungi_2,
	it_o_fungi_3,
	it_o_fungi_4,
	it_o_fungi_5,
	it_o_fungi_6,
	it_o_fungi_7,
	it_o_fungi_8,
	it_o_fungi_9,
	it_o_fungi_10,
	it_o_fungi_11,
	it_o_fungi_12,
	it_o_fungi_13,
	it_o_fungi_14,
	it_o_fungi_15,
	it_o_fungi_16,
	it_o_fungi_17,
	it_o_fungi_18,
	it_o_fungi_19,
	it_o_fungi_20,
	it_o_fungi_21,
	it_o_fungi_22,
	it_o_fungi_23,
	it_o_fungi_24,
	it_o_fungi_25,
	it_o_insecti_1,
	it_o_insecti_2,
	it_o_insecti_3,
	it_o_insecti_4,
	it_o_insecti_5,
	it_o_insecti_6,
	it_o_foliar_feed_s1,
	it_o_foliar_feed_s2,
	it_o_foliar_feed_s3,
	it_o_foliar_feed_s4,
	it_o_foliar_feed_s5,
	it_o_foliar_feed_s6,
	it_o_foliar_feed_s7,
	it_o_foliar_feed_s8,
	it_o_foliar_feed_s9,
	it_o_foliar_feed_s10,
	it_o_foliar_feed_p1,
	it_o_foliar_feed_p2,
	it_o_foliar_feed_p3,
	it_o_foliar_feed_p4,
	it_o_foliar_feed_p5,
	it_o_foliar_feed_p6,
	it_o_foliar_feed_p7,
	it_o_foliar_feed_p8,
	it_o_foliar_feed_p9,
	it_o_foliar_feed_p10,
	it_o_GR_russet_1,
	it_o_GR_russet_2,
	it_o_mechanical_thin,
	it_o_GR_fruit_setting,
	it_o_GR_growth_inhib,
	it_o_GR_thinning_1,
	it_o_GR_thinning_2,
	it_o_GR_thinning_3,
	it_o_herbi_1,
	it_o_herbi_2,
	it_o_DI_continued_early,
	it_o_DI_continued_mid,
	it_o_DI_continued_late,
	it_o_manual_thin, 
	it_o_harvest_early1,
	it_o_harvest_early2,
	it_o_harvest_mid1,
	it_o_harvest_mid2,
	it_o_harvest_late1,
	it_o_harvest_late2,
	it_o_summer_pruning,
	it_o_GR_antidrop,
	it_o_foobar
} ITOrchardToDo;


/**
\brief
ITOrchardd class
\n
*/
/**
See ITOrchard.h::ITOrchardToDo for a complete list of all possible events triggered codes by the ITOrchard management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class ITOrchard : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	ITOrchard(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 1);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (it_o_foobar - IT_O_BASE);
		m_base_elements_no = IT_O_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
						fmc_Others,	// zero element unused but must be here	it_o_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	it_o_sleep_all_day = IT_O_BASE,
			fmc_Cutting,	//	it_o_winter_pruning,
			fmc_Fertilizer,	//	it_o_ferti_s1,
			fmc_Fertilizer,	//	it_o_ferti_p1,
			fmc_Watering,	//	it_o_irrigation,
			fmc_Watering,	//	it_o_irrigation,
			fmc_Watering,	//	it_o_irrigation,
			fmc_Fungicide,	//	it_o_fungi_1,
			fmc_Fungicide,	//	it_o_fungi_2,
			fmc_Fungicide,	//	it_o_fungi_3,
			fmc_Fungicide,	//	it_o_fungi_4,
			fmc_Fungicide,	//	it_o_fungi_5,
			fmc_Fungicide,	//	it_o_fungi_6,
			fmc_Fungicide,	//	it_o_fungi_7,
			fmc_Fungicide,	//	it_o_fungi_8,
			fmc_Fungicide,	//	it_o_fungi_9,
			fmc_Fungicide,	//	it_o_fungi_10,
			fmc_Fungicide,	//	it_o_fungi_11,
			fmc_Fungicide,	//	it_o_fungi_12,
			fmc_Fungicide,	//	it_o_fungi_13,
			fmc_Fungicide,	//	it_o_fungi_14,
			fmc_Fungicide,	//	it_o_fungi_15,
			fmc_Fungicide,	//	it_o_fungi_16,
			fmc_Fungicide,	//	it_o_fungi_17,
			fmc_Fungicide,	//	it_o_fungi_18,
			fmc_Fungicide,	//	it_o_fungi_19,
			fmc_Fungicide,	//	it_o_fungi_20,
			fmc_Fungicide,	//	it_o_fungi_21,
			fmc_Fungicide,	//	it_o_fungi_22,
			fmc_Fungicide,	//	it_o_fungi_23,
			fmc_Fungicide,	//	it_o_fungi_24,
			fmc_Fungicide,	//	it_o_fungi_25,
			fmc_Insecticide,	//	it_o_insecti_1,
			fmc_Insecticide,	//	it_o_insecti_2,
			fmc_Insecticide,	//	it_o_insecti_3,
			fmc_Insecticide,	//	it_o_insecti_4,
			fmc_Insecticide,	//	it_o_insecti_5,
			fmc_Insecticide,	//	it_o_insecti_6,
			fmc_Fertilizer,	//	it_o_foliar_feed_s1,
			fmc_Fertilizer,	//	it_o_foliar_feed_s2,
			fmc_Fertilizer,	//	it_o_foliar_feed_s3,
			fmc_Fertilizer,	//	it_o_foliar_feed_s4,
			fmc_Fertilizer,	//	it_o_foliar_feed_s5,
			fmc_Fertilizer,	//	it_o_foliar_feed_s6,
			fmc_Fertilizer,	//	it_o_foliar_feed_s7,
			fmc_Fertilizer,	//	it_o_foliar_feed_s8,
			fmc_Fertilizer,	//	it_o_foliar_feed_s9,
			fmc_Fertilizer,	//	it_o_foliar_feed_s10,
			fmc_Fertilizer,	//	it_o_foliar_feed_p1,
			fmc_Fertilizer,	//	it_o_foliar_feed_p2,
			fmc_Fertilizer,	//	it_o_foliar_feed_p3,
			fmc_Fertilizer,	//	it_o_foliar_feed_p4,
			fmc_Fertilizer,	//	it_o_foliar_feed_p5,
			fmc_Fertilizer,	//	it_o_foliar_feed_p6,
			fmc_Fertilizer,	//	it_o_foliar_feed_p7,
			fmc_Fertilizer,	//	it_o_foliar_feed_p8,
			fmc_Fertilizer,	//	it_o_foliar_feed_p9,
			fmc_Fertilizer,	//	it_o_foliar_feed_p10,
			fmc_Others,	//	it_o_GR_russet_1,
			fmc_Others,	//	it_o_GR_russet_2,
			fmc_Cutting,	//	it_o_mechanical_thin,
			fmc_Others,	//	it_o_GR_fruit_setting,
			fmc_Others, // it_o_GR_growth_inhib,
			fmc_Others,	//	it_o_GR_thinning_1,
			fmc_Others,	//	it_o_GR_thinning_2,
			fmc_Others,	//	it_o_GR_thinning_3,
			fmc_Herbicide,	//	it_o_herbi_1,
			fmc_Herbicide,	//	it_o_herbi_2,
			fmc_Watering,	//	it_o_DI_continued_early,
			fmc_Watering,	//	it_o_DI_continued_mid,
			fmc_Watering,	//	it_o_DI_continued_late,
			fmc_Cutting,	//	it_o_manual_thin, 
			fmc_Harvest,	//	it_o_harvest_early1,
			fmc_Harvest,	//	it_o_harvest_early2,
			fmc_Harvest,	//	it_o_harvest_mid1,
			fmc_Harvest,	//	it_o_harvest_mid2,
			fmc_Harvest,	//	it_o_harvest_late1,
			fmc_Harvest,	//	it_o_harvest_late2,
			fmc_Cutting,	//	it_o_summer_pruning,
			fmc_Others,	//	it_o_GR_antidrop,

				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // ITOrchard_H
