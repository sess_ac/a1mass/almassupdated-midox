//
// DK_OSpringOats_CC.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OSpringOats_CC.h"

extern CfgFloat cfg_DKCatchCropPct;
bool DK_OSpringOats_CC::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  TTypesOfVegetation l_tov = tov_DKOSpringOats_CC;
  int l_nextcropstartdate;

  switch ( m_ev->m_todo ) {
  case dk_osocc_start:
    {
      a_field->ClearManagementActionSum();

      DK_OSOCC_FORCESPRING = false;

      m_last_date = g_date->DayInYear(16, 8); // Should match the last flexdate below
          //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 3, std::vector<int>(2, 0));
      // Set up the date management stuff
              // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(14, 8); // last possible day of swathing
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(15, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) // harvest
      flexdates[2][0] = -1;
      flexdates[2][1] = g_date->DayInYear(16, 8); // hay bailing
      flexdates[3][0] = -1;
      flexdates[3][1] = g_date->DayInYear(16, 8); // straw chopping


      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 365;
      if (StartUpCrop(isSpring, flexdates, int(dk_osocc_spring_harrow1))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 2) + isSpring;
          // OK, let's go.
          SimpleEvent(d1, dk_osocc_spring_harrow1, false);
  }
    break;

  case dk_osocc_spring_harrow1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->SpringHarrow(m_field, 0.0,
              g_date->DayInYear(28, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osocc_spring_harrow1, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 2, dk_osocc_spring_harrow2, false);
      break;

  case dk_osocc_spring_harrow2:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) { // put to 50% as 100% harrow 1-2 times
          if (!m_farm->SpringHarrow(m_field, 0.0,
              g_date->DayInYear(30, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osocc_spring_harrow2, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_osocc_slurry_s, false);
      break;

  case dk_osocc_slurry_s:
      if (a_farm->IsStockFarmer()) {
          if (!m_farm->FA_Slurry(m_field, 0.0,
              g_date->DayInYear(31, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osocc_slurry_s, true);
              break;
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_osocc_spring_plough, false);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_osocc_slurry_p, false);
      break;

  case dk_osocc_slurry_p:
      if (!m_farm->FP_Slurry(m_field, 0.0,
          g_date->DayInYear(31, 3) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osocc_slurry_p, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_osocc_spring_plough, false);
      break;

  case dk_osocc_spring_plough:
      if (!m_farm->SpringPlough(m_field, 0.0,
          g_date->DayInYear(30, 4) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osocc_spring_plough, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_osocc_spring_sow, false);
      break;

  case dk_osocc_spring_sow:
      if (!m_farm->SpringSow(m_field, 0.0,
          g_date->DayInYear(1, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osocc_spring_sow, true);
          break;
      }
      SimpleEvent(g_date->Date() + 3, dk_osocc_strigling1, false);
      break;

  case dk_osocc_strigling1:
      if (!m_farm->Strigling(m_field, 0.0,
          g_date->DayInYear(4, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osocc_strigling1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 3, dk_osocc_strigling2, false);
      break;

  case dk_osocc_strigling2:
      if (!m_farm->Strigling(m_field, 0.0,
          g_date->DayInYear(7, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osocc_strigling2, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_osocc_roll, false);
      break;

  case dk_osocc_roll:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) {
          if (!m_farm->SpringRoll(m_field, 0.0,
              g_date->DayInYear(8, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osocc_roll, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 7, dk_osocc_weed_harrow, false);
      break;

  case dk_osocc_weed_harrow:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.8)) { // 80% do weed harrow, 20% row cultivation (as 20% row sow)
          if (!m_farm->SpringHarrow(m_field, 0.0,
              g_date->DayInYear(16, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osocc_weed_harrow, true);
              break;
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_osocc_water, false);
          break;
      }
      SimpleEvent(g_date->Date(), dk_osocc_row_cultivation, false);
      break;

  case dk_osocc_row_cultivation:// 80% do weed harrow, 20% row cultivation (as 20% row sow)
      if (!m_farm->RowCultivation(m_field, 0.0,
          g_date->DayInYear(16, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osocc_row_cultivation, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_osocc_water, false);
      break;

  case dk_osocc_water:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(31, 7) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osocc_water, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 8), dk_osocc_swathing, false);
      break;

  case dk_osocc_swathing:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.1)) { // est. 10%
          if (!m_farm->Swathing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osocc_swathing, true);
              break;
          }
          SimpleEvent(g_date->Date() + 1, dk_osocc_harvest, false);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 8), dk_osocc_harvest, false);
      break;

  case dk_osocc_harvest:
      if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osocc_harvest, true);
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_osocc_hay_bailing, false);
      break;

  case dk_osocc_hay_bailing:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.9)) { // est. 10%
          if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osocc_hay_bailing, true);
              break;
          }
          if (m_ev->m_lock || m_farm->DoIt_prob(cfg_DKCatchCropPct.value())) {
              SimpleEvent(g_date->Date(), dk_osocc_wait, false);
              break;
          }
          else
              done = true;
          break;
      }
      SimpleEvent(g_date->Date() + 1, dk_osocc_straw_chopping, false);
      break;

  case dk_osocc_straw_chopping:
      if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osocc_straw_chopping, true);
          break;
      }
      if (m_ev->m_lock || m_farm->DoIt_prob(cfg_DKCatchCropPct.value())) {
          SimpleEvent(g_date->Date(), dk_osocc_wait, false);
          break;
      }
      else
      done = true;
      break;

  case dk_osocc_wait:
      //set to 100% from this code - in reality ~ 42% of all spring barley
      // So we are done,but this crop uses a catch crop
      l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
      m_field->BumpRunNum();
      m_farm->AddNewEvent(tov_DKOCatchCrop, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
      m_field->SetVegType(tov_DKOCatchCrop, tov_Undefined); //  Two vegetation curves are specified 
      if (m_field->GetUnsprayedMarginPolyRef() != -1)
      {
          LE* um = m_OurLandscape->SupplyLEPointer(m_field->GetUnsprayedMarginPolyRef());
          um->SetVegType(tov_DKOCatchCrop, tov_Undefined);
      }
      // NB no "done = true" because this crop effectively continues into the catch crop.
      break;

  default:
      g_msg->Warn(WARN_BUG, "DK_OSpringOats_CC::Do(): "
          "Unknown event type! ", "");
      exit(1);
  }
  return done;
}


