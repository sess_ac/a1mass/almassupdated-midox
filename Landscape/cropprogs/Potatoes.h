//
// Potatoes.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef POTATOESEAT_H
#define POTATOESEAT_H

#define POTATOESEAT_BASE 5600
#define POT_SLURRY_DATE m_field->m_user[0]
#define POT_HERBI_DATE  m_field->m_user[1]
#define POT_STRIG_DATE  m_field->m_user[2]
#define POT_HILL_DATE   m_field->m_user[3]
#define POT_DID_TREAT   m_field->m_user[4]
#define POT_DID_HILL    m_field->m_user[5]
#define POT_WATER_DATE  m_field->m_user[6]
#define POT_FUNGI_DATE  m_field->m_user[7]

typedef enum {
  pe_start = 1, // Compulsory, start event must always be 1 (one).
  pe_autumn_plough = POTATOESEAT_BASE,
  pe_spring_plough,
  pe_spring_harrow,
  pe_fa_slurry,
  pe_spring_sow,
  pe_fa_npk,
  pe_fp_npk,
  pe_herbi_one,
  pe_herbi_two,
  pe_herbi_three,
  pe_strigling_one,
  pe_strigling_two,
  pe_strigling_three,
  pe_hilling,
  pe_insecticide,
  pe_water_one,
  pe_water_two,
  pe_water_three,
  pe_fungi_one,
  pe_fungi_two,
  pe_fungi_three,
  pe_fungi_four,
  pe_fungi_five,
  pe_growth_reg,
  pe_harvest,
  pe_foobar
} PotatoesEatToDo;



class Potatoes: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  Potatoes(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,11);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (pe_foobar - POTATOESEAT_BASE);
	  m_base_elements_no = POTATOESEAT_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here
		  fmc_Others,//pe_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cultivation,//pe_autumn_plough = POTATOESEAT_BASE,
		  fmc_Cultivation,//pe_spring_plough,
		  fmc_Cultivation,//pe_spring_harrow,
		  fmc_Fertilizer,//pe_fa_slurry,
		  fmc_Others,//pe_spring_sow,
		  fmc_Fertilizer,//pe_fa_npk,
		  fmc_Fertilizer,//pe_fp_npk,
		  fmc_Herbicide,//pe_herbi_one,
		  fmc_Herbicide,//pe_herbi_two,
		  fmc_Herbicide,//pe_herbi_three,
		  fmc_Cultivation,//pe_strigling_one,
		  fmc_Cultivation,//pe_strigling_two,
		  fmc_Cultivation,//pe_strigling_three,
		  fmc_Cultivation,//pe_hilling,
		  fmc_Insecticide,//pe_insecticide,
		  fmc_Watering,//pe_water_one,
		  fmc_Watering,//pe_water_two,
		  fmc_Watering,//pe_water_three,
		  fmc_Fungicide,//pe_fungi_one,
		  fmc_Fungicide,//pe_fungi_two,
		  fmc_Fungicide,//pe_fungi_three,
		  fmc_Fungicide,//pe_fungi_four,
		  fmc_Fungicide,//pe_fungi_five,
		  fmc_Others,//pe_growth_reg,
		  fmc_Harvest//pe_harvest

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // POTATOESEAT_H
