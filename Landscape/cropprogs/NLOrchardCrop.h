//
// NLOrchardCrop.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#ifndef NLORCHARDCROP_H
#define NLORCHARDCROP_H

#define NLORCHARDCROP_BASE 21700

/**
\brief A flag used to indicate insecticide use
*/
#define NL_ORCH_INSECT	a_field->m_user[1]
#define NL_ORCH_ISECT_DAY	a_field->m_user[2]

typedef enum {
  nl_orch_start = 1, // Compulsory, start event must always be 1 (one).
  nl_orch_sleep_all_day = NLORCHARDCROP_BASE,
  nl_orch_do_nothing_stop,
  nl_orch_insecticide1,
  nl_orch_insecticide2,
  nl_orch_insecticide3,
  nl_orch_insecticide4,
  nl_orch_cut1,
  nl_orch_cut2,
  nl_orch_cut3,
  nl_orch_cut4,
  nl_orch_foobar
} NLOrchardCropToDo;



class NLOrchardCrop: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  NLOrchardCrop(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,7);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (nl_orch_foobar - NLORCHARDCROP_BASE);
	  m_base_elements_no = NLORCHARDCROP_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//nl_orch_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Others,//nl_orch_sleep_all_day = NLORCHARDCROP_BASE,
		  fmc_Others,//nl_orch_do_nothing_stop,
		  fmc_Insecticide,//nl_orch_insecticide1,
		  fmc_Insecticide,//nl_orch_insecticide2,
		  fmc_Insecticide,//nl_orch_insecticide3,
		  fmc_Insecticide, //nl_orch_insecticide4,
		  fmc_Cutting,//nl_orch_cut1,
		  fmc_Cutting,//nl_orch_cut2,
		  fmc_Cutting,//nl_orch_cut3,
		  fmc_Cutting//nl_orch_cut4,

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // NLORCHARDCROP_H
