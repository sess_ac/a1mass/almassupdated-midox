/**
\file
\brief
<B>BEWinterBarleyCC.h This file contains the headers for the Winterbarley class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// BEWinterBarleyCC.h
//


#ifndef BEWinterBarleyCC_H
#define BEWinterBarleyCC_H

#define BEWinterBarleyCC_BASE 25400
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing winter barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	BE_wbcc_start = 1, // Compulsory, must always be 1 (one).
	BE_wbcc_sleep_all_day = BEWinterBarleyCC_BASE,
	BE_wbcc_stubble_cultivator,
	BE_wbcc_herbicide1,
	BE_wbcc_mole_plough,
	BE_wbcc_autumn_plough,
	BE_wbcc_preseeding_cultivator_sow,
	BE_wbcc_herbicide2,
	BE_wbcc_herbicide3,
	BE_wbcc_ferti_p1, // slurry
	BE_wbcc_ferti_s1,
	BE_wbcc_ferti_p3, // NI
	BE_wbcc_ferti_s3,
	BE_wbcc_ferti_p4, // NII
	BE_wbcc_ferti_s4,
	BE_wbcc_ferti_p5, // NIII
	BE_wbcc_ferti_s5,
	BE_wbcc_herbicide4, 
	BE_wbcc_fungicide1,
	BE_wbcc_fungicide2,
	BE_wbcc_insecticide2,
	BE_wbcc_growth_regulator1,
	BE_wbcc_growth_regulator2,
	BE_wbcc_harvest,
	BE_wbcc_straw_chopping,
	BE_wbcc_hay_bailing,
} BEWinterBarleyCC1ToDo;


/**
\brief
BEWinterBarleyCC class
\n
*/
/**
See BEWinterBarleyCC.h::BEWinterBarleyCCToDo for a complete list of all possible events triggered codes by the winter barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class BEWinterBarleyCC: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   BEWinterBarleyCC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,10 );
   }
};

#endif // BEWinterBarleyCC_H

