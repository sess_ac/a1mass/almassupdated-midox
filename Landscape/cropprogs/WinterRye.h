//
// WinterRye.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WinterRye_H
#define WinterRye_H

#define WRYE_BASE 8000
#define WRY_DID_MANURE          m_field->m_user[0]
#define WRY_DID_SLUDGE          m_field->m_user[1]
#define WRY_DECIDE_TO_HERB		m_field->m_user[2]

typedef enum {
  wry_start = 1, // Compulsory, start event must always be 1 (one).
  wry_fertmanure_stock = WRYE_BASE,
  wry_fertsludge_stock,
  wry_autumn_plough,
  wry_autumn_harrow,
  wry_autumn_sow,
  wry_autumn_roll,
  wry_fertmanganese_plant_one,
  wry_fertnpk_plant,
  wry_fertmanganese_plant_two,
  wry_fertslurry_stock,
  wry_fert_ammonium_stock,
  wry_spring_roll,
  wry_strigling,
  wry_growth_reg_one,
  wry_herbicide,
  wry_herbicide_two, //added 030513
  wry_growth_reg_two,
  wry_fungicide,
  wry_insecticide,
  wry_water,
  wry_harvest,
  wry_straw_chopping,
  wry_hay_turning,
  wry_hay_bailing,
  wry_stubble_harrowing,
  wry_foobar
} WinterRyeToDo;



class WinterRye: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  WinterRye(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(11,10);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (wry_foobar - WRYE_BASE);
	  m_base_elements_no = WRYE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others,	// zero element unused but must be here
		  fmc_Others,//wry_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer,//wry_fertmanure_stock = WRYE_BASE,
		  fmc_Fertilizer,//wry_fertsludge_stock,
		  fmc_Cultivation,//wry_autumn_plough,
		  fmc_Cultivation,//wry_autumn_harrow,
		  fmc_Others,//wry_autumn_sow,
		  fmc_Others,//wry_autumn_roll,
		  fmc_Fertilizer,//wry_fertmanganese_plant_one,
		  fmc_Fertilizer,//wry_fertnpk_plant,
		  fmc_Fertilizer,//wry_fertmanganese_plant_two,
		  fmc_Fertilizer,//wry_fertslurry_stock,
		  fmc_Fertilizer,//wry_fert_ammonium_stock,
		  fmc_Others,//wry_spring_roll,
		  fmc_Cultivation,//wry_strigling,
		  fmc_Others,//wry_growth_reg_one,
		  fmc_Herbicide,//wry_herbicide,
		  fmc_Herbicide,//wry_herbicide_two, //added 030513
		  fmc_Others,//wry_growth_reg_two,
		  fmc_Fungicide,//wry_fungicide,
		  fmc_Insecticide,//wry_insecticide,
		  fmc_Watering,//wry_water,
		  fmc_Harvest,//wry_harvest,
		  fmc_Others,//wry_straw_chopping,
		  fmc_Others,//wry_hay_turning,
		  fmc_Others,//wry_hay_bailing,
		  fmc_Cultivation//wry_stubble_harrowing

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // WinterRye_H
