//
// DK_SpringFodderGrass.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_SpringFodderGrass.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_grass_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_SpringFodderGrass::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false;  // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
    TTypesOfVegetation l_tov = tov_DKSpringFodderGrass; // The current type - change to match the crop you have

    switch (m_ev->m_todo)
    {
    case dk_sfg_start:
    {

        DK_SFG_CC = false;
        a_field->ClearManagementActionSum();

        m_last_date = g_date->DayInYear(30, 9); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(2 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(28, 9); // last possible day of swathing in this case 
        // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[1][1] = g_date->DayInYear(30, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - harvest
        flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
        flexdates[2][1] = g_date->DayInYear(30, 9); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - cattleout

        // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
        int isSpring = 365;
        if (StartUpCrop(isSpring, flexdates, int(dk_sfg_spring_plough))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(1, 2) + isSpring;
        // OK, let's go.
        // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
        if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
        {
            SimpleEvent(d1, dk_sfg_spring_roll1, false);
            break;
        }
        else SimpleEvent(d1, dk_sfg_spring_plough, false); // only done on sandy soils
        break;
    }
    break;

    case dk_sfg_spring_plough:
        if (m_ev->m_lock || m_farm->DoIt_prob(.85)) {
            if (!m_farm->SpringPlough(m_field, 0.0,
                g_date->DayInYear(15, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_spring_plough, true);
                break;
            }
        }
        SimpleEvent(g_date->Date(), dk_sfg_spring_roll1, false);
        break;

    case dk_sfg_spring_roll1:
        if (!m_farm->SpringRoll(m_field, 0.0,
            g_date->DayInYear(15, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_spring_roll1, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_sfg_spring_harrow, false);
        break;

    case dk_sfg_spring_harrow:
        if (!m_farm->ShallowHarrow(m_field, 0.0,
            g_date->DayInYear(16, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_spring_harrow, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_sfg_spring_roll2, false);
        break;

    case dk_sfg_spring_roll2:
        if (m_ev->m_lock || m_farm->DoIt_prob(.90)) {
            if (!m_farm->SpringRoll(m_field, 0.0,
                g_date->DayInYear(17, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_spring_roll2, true);
                break;
            }
        }
        if (a_farm->IsStockFarmer()) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_manure_pig_s, false);
            break;
        }
        else SimpleEvent(g_date->Date() + 1, dk_sfg_manure_pig_p, false);
        break;

    case dk_sfg_manure_pig_s:
        if (!m_farm->FA_Slurry(m_field, 0.0,
            g_date->DayInYear(18, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_manure_pig_s, true);
            break;
        }
        SimpleEvent(g_date->Date() + 7, dk_sfg_npk_s, false);
        break;

    case dk_sfg_npk_s:
        if (!m_farm->FA_NPK(m_field, 0.0,
            g_date->DayInYear(26, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_npk_s, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_sfg_spring_sow_cc, false);
        break;

    case dk_sfg_manure_pig_p:
        if (!m_farm->FP_Slurry(m_field, 0.0,
            g_date->DayInYear(18, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_manure_pig_p, true);
            break;
        }
        SimpleEvent(g_date->Date() + 7, dk_sfg_npk_p, false);
        break;

    case dk_sfg_npk_p:
        if (!m_farm->FP_NPK(m_field, 0.0,
            g_date->DayInYear(26, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_npk_p, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_sfg_spring_sow_cc, false);
        break;

    case dk_sfg_spring_sow_cc:
        if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) { 
            if (!m_farm->SpringSow(m_field, 0.0,
                g_date->DayInYear(27, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_spring_sow_cc, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 1, dk_sfg_spring_sow, false);
        break;

    case dk_sfg_spring_sow: // sow clover grass
        if (!m_farm->SpringSow(m_field, 0.0,
            g_date->DayInYear(28, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_spring_sow, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_sfg_spring_roll3, false);
        break;

    case dk_sfg_spring_roll3:
        if (!m_farm->SpringRoll(m_field, 0.0,
            g_date->DayInYear(29, 3) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_spring_roll3, true);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), dk_sfg_water, false); // water thread
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), dk_sfg_herbicide1, false); // main thread
        break;

    case dk_sfg_water:
        if (m_ev->m_lock || m_farm->DoIt_prob(.50)) { // 50% water
            if (!m_farm->Water(m_field, 0.0,
                g_date->DayInYear(31, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_water, true);
                break;
            }
        }
        break; // end of thread

    case dk_sfg_herbicide1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.8)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(15, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_herbicide1, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_sfg_cutting1, false);
        break;

    case dk_sfg_cutting1:
        if (m_ev->m_lock || m_farm->DoIt_prob(.90)) { // 90% cut to silage, 10% graze
            if (!m_farm->CutToSilage(m_field, 0.0,
                g_date->DayInYear(15, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_cutting1, true);
                break;
            }
            DK_SFG_CUT = true; //we need to remember who did cutting
            SimpleEvent(g_date->Date() + 35, dk_sfg_cutting2, false);
            break;
        }
        else if (m_ev->m_lock || m_farm->DoIt_prob(.10 / .10)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_sfg_cutting_graze, false);
            break;
        }

    case dk_sfg_cutting_graze:
        if (m_ev->m_lock || m_farm->DoIt_prob(.50)) { // 50% cut to silage of the 10% that will do grazing
            if (!m_farm->CutToSilage(m_field, 0.0,
                g_date->DayInYear(1, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_cutting_graze, true);
                break;
            }
            SimpleEvent(g_date->Date() + 14, dk_sfg_grazing, false);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_sfg_grazing, false);
        break;

    case dk_sfg_cutting2:
        if (DK_SFG_CUT == true) {
            if (!m_farm->CutToSilage(m_field, 0.0,
                g_date->DayInYear(20, 7) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_cutting2, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 35, dk_sfg_cutting3, false);
        break;

    case dk_sfg_cutting3:
        if (DK_SFG_CUT == true) {
            if (!m_farm->CutToSilage(m_field, 0.0,
                g_date->DayInYear(25, 8) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_cutting3, true);
                break;
            }
        }
        d1 = g_date->Date();
        if (d1 < g_date->OldDays() + g_date->DayInYear(25, 7)) {
            SimpleEvent(g_date->Date() + 35, dk_sfg_cutting4, false);
            break;
        }
        else SimpleEvent(g_date->Date() + 20, dk_sfg_insecticide, false);
        break;

    case dk_sfg_cutting4:
        if (DK_SFG_CUT == true) {
            if (!m_farm->CutToSilage(m_field, 0.0,
                g_date->DayInYear(31, 8) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_cutting4, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 20, dk_sfg_insecticide, false);
        break;

    case dk_sfg_grazing:
        if (!m_farm->CattleOut(m_field, 0.0,
            g_date->DayInYear(15, 6) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_grazing, true);
            break;
        }
        SimpleEvent(g_date->Date() + 1, dk_sfg_cattle_out, false);
        break;

    case dk_sfg_cattle_out:    // Keep the cattle out there
                             // CattleIsOut() returns false if it is not time to stop grazing
        if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear(), g_date->DayInYear(10, 8))) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_cattle_out, false);
            break;
        }
        SimpleEvent(g_date->Date() + 10, dk_sfg_herbicide2, false);
        break;

    case dk_sfg_herbicide2:
        if (!m_farm->HerbicideTreat(m_field, 0.0,
            g_date->DayInYear(30, 8) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_herbicide2, true);
            break;
        }
        SimpleEvent(g_date->Date() + 20, dk_sfg_insecticide, false);
        break;

    case dk_sfg_insecticide:
        d1 = g_date->Date();
        if (d1 > g_date->OldDays() + g_date->DayInYear(1, 9)) {
            if (m_ev->m_lock || m_farm->DoIt_prob(.02)) {
                // here we check whether we are using ERA pesticide or not
                d1 = g_date->DayInYear(28, 9) - g_date->DayInYear();
                if (!cfg_pest_grass_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
                {
                    flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
                }
                else {
                    flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
                }
                if (!flag) {
                    SimpleEvent(g_date->Date() + 1, dk_sfg_insecticide, true);
                    break;
                }
            }
            SimpleEvent(g_date->Date()+1, dk_sfg_swathing, false);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_sfg_swathing, false);
        break;

    case dk_sfg_swathing:
        if (m_ev->m_lock || m_farm->DoIt_prob(.10)) { // 10% do swathing
            if (!m_farm->Swathing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_sfg_swathing, true);
                break;
            }
        }
        SimpleEvent(g_date->Date()+2, dk_sfg_harvest, false);
        break;

    case dk_sfg_harvest:
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_harvest, true);
            break;
        }
        SimpleEvent(g_date->Date(), dk_sfg_grass_collected, false);
        break;

    case dk_sfg_grass_collected:
        if (!m_farm->StrawRemoval(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_sfg_grass_collected, true);
            break;
        }
        done = true;
        break;


    default:
        g_msg->Warn(WARN_BUG, "DK_SpringFodderGrass::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }

    return done;
}
