//
//  Fodder.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

//#define __EcoSol_01
#define _CRT_SECURE_NO_DEPRECATE
#include "../../BatchALMaSS/ALMaSS_Setup.h"
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FodderGrass.h"

//---------------------------------------------------------------------------

extern CfgFloat cfg_silage_prop;
extern CfgInt cfg_pest_productapplic_startdate;
extern CfgInt cfg_pest_productapplic_period;
extern CfgFloat cfg_pest_product_1_amount;

using namespace std;

bool FodderGrass::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  long d1 = 0;
  int noDates=2;
  bool done = false;

  switch ( m_ev->m_todo ) {
  case fg_start:

    FG_WATER_DATE        = 0;
    FG_CUT_DATE          = 0;
    FG_FERTI_DATE        = 0;
    FG_FORCE_SECOND_CUT  = 0;
    FG_HALT_SECOND_WATER = 0;
    a_field->ClearManagementActionSum();


    // Set up the date management stuff
    m_last_date=g_date->DayInYear(30,9);

    // Start and stop dates for all events after harvest
    m_field->SetMDates(0,0,g_date->DayInYear(27,7));

    // Determined by harvest date - used to see if at all possible
    m_field->SetMDates(1,0,g_date->DayInYear(27,7));
    m_field->SetMDates(0,1,g_date->DayInYear(28,7));
    m_field->SetMDates(1,1,g_date->DayInYear(10,10));

    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "FodderGrass::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
        if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
        	m_field->SetMDates(0,i,m_ev->m_startday-1);
        if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
         	m_field->SetMDates(1,i,m_ev->m_startday-1);
      }
    }

    // Now no operations can be timed after the start of the next crop.
    if ( ! m_ev->m_first_year ) {
      // Are we before July 1st?
      d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );

      if (g_date->Date() < d1) {
	// Yes, too early. We assumme this is because the last crop was late
	g_msg->Warn( WARN_BUG, "FodderGrass::Do(): "
		     "Crop start attempt between 1st Jan & 1st July", "" );
	exit( 1 );

      } else {

	d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
	if (g_date->Date() > d1) {
          // Yes too late - should not happen - raise an error
          g_msg->Warn( WARN_BUG, "FodderGrass::Do(): "
		       "Crop start attempt after last possible start date",
		       "" );
          exit( 1 );

        }
      }
    }
	}//if

    // End single block date checking code. Please see next line
    // comment as well.

    // Reinit d1 to first possible starting date.
    d1 = g_date->OldDays()+m_first_date;;
    if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
    SimpleEvent( d1, fg_ferti_zero, false );

    d1 = g_date->OldDays() + g_date->DayInYear( 15, 3 );
    if (g_date->Date() >= d1) d1 += 365;
    SimpleEvent( d1, fg_ferti_one, false );

#ifdef __EcoSol_01
    // THIS CODE IS ONLY NEEDED IF WE ARE TESTING A HERBICIDE WITH THE PESTICIDE ENGINE
	/* */
	if ( a_field->GetLandscape()->SupplyShouldSpray() ) {
		d1 = g_date->OldDays() + cfg_pest_productapplic_startdate.value();
		if (g_date->Date() >= d1) d1 += 365;
		SimpleEvent( d1, fg_productapplic_one, false );
	}
    /* */
#endif
	break;

  case fg_productapplic_one:
	if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->ProductApplication( m_field, 0.0,(cfg_pest_productapplic_startdate.value()+cfg_pest_productapplic_period.value()) - g_date->DayInYear(), cfg_pest_product_1_amount.value(), ppp_1)) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, fg_productapplic_one, true );
        break;
      }
    }
    break;

  case fg_ferti_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, fg_ferti_zero, true );
        break;
      }
      FG_FERTI_DATE = g_date->DayInYear();
    }

    // This used to be broken, but is now outdated, as fg_ferti_one
    // now is the main thread of execution. FN 15/5, 2003.
    // Note: The line should have read: g_date->OldDays() + ...
    //SimpleEvent( g_date->Date()+ g_date->DayInYear( 15,3 ),
    break;

  case fg_ferti_one:
    if (!m_farm->FA_NPK( m_field, 0.0,
         g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, fg_ferti_one, true );
      break;
    }
    FG_FERTI_DATE = g_date->DayInYear();

    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4 ),
                 fg_ferti_two, false );

    // Start a watering thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ),
                 fg_water_zero, false );
    break;

  case fg_ferti_two:
    if (!m_farm->FA_NPK( m_field, 0.0,
         g_date->DayInYear( 10, 5 ) - g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, fg_ferti_two, true );
      break;
    }
    FG_FERTI_DATE = g_date->DayInYear();

    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ),
                 fg_cut_to_silage, false );
    break;

  case fg_cut_to_silage:
  if ( m_ev->m_lock || m_farm->DoIt( 100 )) {
     // At least 7 days from last watering and 21 from
     // last application of fertilizer.
     if (g_date->DayInYear()<FG_FERTI_DATE+21 ||
	 g_date->DayInYear()<FG_WATER_DATE+7
	 ) {
       SimpleEvent( g_date->Date() + 1, fg_cut_to_silage, true );
       break;
     }
     if (!m_farm->CutToSilage( m_field, 0.0,
			       g_date->DayInYear( 10, 6 ) -
			       g_date->DayInYear())) {
       // We didn't do it today, try again tomorrow.
       SimpleEvent( g_date->Date() + 1, fg_cut_to_silage, true );
       break;
     }
     FG_CUT_DATE=g_date->DayInYear();

     // Decide whether we got for cattle or a second silage cut.
     // 75% go for the second cut.
	 if ( FG_FORCE_SECOND_CUT || m_farm->DoIt( 100 )) {
       // 75% do Slurry fertilization before second cutting, 25% NPK.
       if ( m_farm->DoIt( 75 )) {
	 SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 5 ),
		      fg_ferti_three, false );
       } else {
	 SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 5 ),
		      fg_ferti_four, false );
       }
       break;
     }
  }
  break; // Should never get here without setting up the next silage cut

  case fg_ferti_three:
    // The percentage has already been taken care of during
    // the decisions made under fg_cut_to_silage
    if (!m_farm->FA_Slurry( m_field, 0.0,
			    ( FG_CUT_DATE+4 ) - g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, fg_ferti_three, true );
      break;
    }

    {
      // Three weeks later min
      d1 = g_date->OldDays() + g_date->DayInYear( 16, 6 );
      if ( d1 < g_date->Date() + 21 )
	d1 = g_date->Date() + 21;
      SimpleEvent( d1, fg_cut_to_silage2, false );
    }
    break;

  case fg_ferti_four:
    // The percentage has already been taken care of during
    // the decisions made under fg_cut_to_silage
    if (!m_farm->FA_NPK( m_field, 0.0,
			 ( FG_CUT_DATE+4 ) - g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, fg_ferti_four, true );
      break;
    }

    {
      // Three weeks later min
      d1 = g_date->OldDays() + g_date->DayInYear( 16, 6 );
      if ( d1 < g_date->Date() + 21 )
	d1 = g_date->Date() + 21;
      SimpleEvent( d1, fg_cut_to_silage2, false );
    }
    break;

  case fg_cut_to_silage2:
    // The percentage has already been taken care of during
    // the decisions made under fg_cut_to_silage
    if (!m_farm->CutToSilage( m_field, 0.0,
			      g_date->DayInYear( 6,7 ) -
			      g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, fg_cut_to_silage2, true );
        break;
    }
    FG_CUT_DATE = g_date->DayInYear();

    {
      // Three weeks later min
      d1 = g_date->OldDays() + g_date->DayInYear( 8, 7 );
      if ( d1 < g_date->Date() + 21 )
	d1 = g_date->Date() + 21;

	  ChooseNextCrop (2);

	SimpleEvent( d1, fg_cut_to_silage3, false );
    }
    break;

  case fg_cut_to_silage3:
    // The percentage has already been taken care of during
    // the decisions made under fg_cut_to_silage2
    if (!m_farm->CutToSilage( m_field, 0.0,
			      m_field->GetMDates(0,0) -
			      g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, fg_cut_to_silage3, true );
      break;
    }
    FG_CUT_DATE = g_date->DayInYear();
    done=true;
    break;

  case fg_water_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 40 ))
    {
      if ( g_date->Date() < FG_CUT_DATE + 3 ) {
	// Too close to silage cutting, so try again tomorrow.
	SimpleEvent( g_date->Date() + 1, fg_water_zero, true );
      }

      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 1, 6 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, fg_water_zero, true );
        break;
      }
      // Success
      FG_WATER_DATE=g_date->DayInYear();

      // Determine whether we want to do the second watering.
      // If so, then force the second silage cutting as well.

      if ( m_farm->DoIt( 40 ) && !FG_HALT_SECOND_WATER ) {
	// Do second watering.
	FG_FORCE_SECOND_CUT = 1;
	d1 = g_date->OldDays() + g_date->DayInYear( 2,6 );
	if ( d1 < g_date->Date() + 7 )
	  d1 = g_date->Date() + 7;
	SimpleEvent( d1, fg_water_one, false );
      }
    }
    // Didn't water so let the thread die
    break;

  case fg_water_one:
    if ( g_date->Date() < FG_CUT_DATE + 3 ) {
      // Too close to silage cutting, so try again tomorrow.
      SimpleEvent( g_date->Date() + 1, fg_water_one, true );
    }

    if (!m_farm->Water( m_field, 0.0,
			g_date->DayInYear( 30, 6 ) -
			g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, fg_water_one, true );
      break;
    }
    FG_WATER_DATE=g_date->DayInYear();
    break;
    // End of watering thread

  default:
    g_msg->Warn( WARN_BUG, "FodderGrass::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}
//---------------------------------------------------------------------------
