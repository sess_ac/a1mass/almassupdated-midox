/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLBeetSpring.cpp This file contains the source for the NLBeetSpring class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLBeetSpring.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLBeetSpring.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_beet_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_BE_InsecticideDay;
extern CfgInt   cfg_BE_InsecticideMonth;
extern CfgFloat cfg_pest_product_3_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional beet.
*/
bool NLBeetSpring::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_NLBeetSpring;
	int l_nextcropstartdate;
	/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case nl_bes_start:
	{
		NL_BES_FUNGI1 = false;
		NL_BES_HERBI1 = false;
		m_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(15, 12); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(15, 12); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use

		flexdates[1][0] = -1;  // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(15, 12);  // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - straw chopping

		// Below if this is a spring crop use 365, otherwise set this to 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(nl_bes_spring_sow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		if (m_field->GetSoilType() == 2 || m_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			SimpleEvent_(d1, nl_bes_spring_plough_sandy, false, m_farm, m_field);
		}
		else
		{
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(d1, nl_bes_ferti_s1, false, m_farm, m_field);
			}
			else SimpleEvent_(d1, nl_bes_ferti_p1, false, m_farm, m_field);
		}
		break;
	}
	break;

	// This is the first real farm operation
	case nl_bes_spring_plough_sandy:
		if (!m_farm->SpringPlough(m_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_spring_plough_sandy, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 7, nl_bes_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 7, nl_bes_ferti_p1, false, m_farm, m_field);
		break;
	case nl_bes_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_bes_preseeding_cultivator, false, m_farm, m_field);
		break;
	case nl_bes_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_bes_preseeding_cultivator, false, m_farm, m_field);
		break;
	case nl_bes_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(14, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_bes_spring_sow, false, m_farm, m_field);
		break;
	case nl_bes_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_spring_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, nl_bes_herbicide1, false, m_farm, m_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), nl_bes_fungicide1, false, m_farm, m_field);	// Fungicide thread = MAIN THREAD
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), nl_bes_ferti_s2, false, m_farm, m_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), nl_bes_ferti_p2, false, m_farm, m_field);
		break;
	case nl_bes_ferti_p2:
		// Here comes fertilizers thread
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_p2, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_bes_ferti_s2:
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_s2, false, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_bes_herbicide1: // The first of the pesticide managements.
		// Here comes the herbicide thread
		// We assume that farmers do max 3 herbicide treatments
		if (m_field->GetGreenBiomass() <= 0)
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
			{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide1, true, m_farm, m_field);
					break;
				}
				NL_BES_HERBI1 = true;
			}
			SimpleEvent_(g_date->Date() + 14, nl_bes_herbicide2, false, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), nl_bes_herbicide2, false, m_farm, m_field);
		break;
	case nl_bes_herbicide2:
		if (m_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide2, false, m_farm, m_field);
		}
		else
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide2, true, m_farm, m_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 14, nl_bes_herbicide3, false, m_farm, m_field);
			break;
		}
		break;
	case nl_bes_herbicide3:
		if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, nl_bes_herbicide4, false, m_farm, m_field);
		break;
	case nl_bes_herbicide4:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.20) && (NL_BES_HERBI1 == false)))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide4, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_bes_fungicide1:
		// Here comes the fungicide thread
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 21, nl_bes_fungicide2, false, m_farm, m_field);
		break;
	case nl_bes_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(25, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_fungicide2, true, m_farm, m_field);
				break;
			}
			NL_BES_FUNGI1 = true;
		}
		SimpleEvent_(g_date->Date() + 21, nl_bes_fungicide3, false, m_farm, m_field);
		break;
	case nl_bes_fungicide3:
		if (m_ev->m_lock || (m_farm->DoIt_prob(0.667) && NL_BES_FUNGI1 == 1)) // 50% of all farmers
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_fungicide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 9), nl_bes_harvest, false, m_farm, m_field);
		break;
	case nl_bes_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_harvest, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLBeetSpring::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	if (done) m_ev->m_forcespring = true; // Here we need to force the next crop to spring operation, so set the ev->forcespring to true
	return done;
}