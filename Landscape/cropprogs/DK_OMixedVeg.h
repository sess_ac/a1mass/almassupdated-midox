//
// DK_OMixedVeg.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OMixedVeg_H
#define DK_OMixedVeg_H

#define  DK_OMV_LATE_SOW a_field->m_user[1]

#define DK_OMV_BASE 69300

typedef enum {
  dk_omv_start = 1, // Compulsory, start event must always be 1 (one).
  dk_omv_ferti_s1 = DK_OMV_BASE,
  dk_omv_ferti_p1,
  dk_omv_spring_plough, 
  dk_omv_spring_harrow,
  dk_omv_spring_sow1,
  dk_omv_row_cultivation1,
  dk_omv_slurry_s1,
  dk_omv_slurry_p1,
  dk_omv_water1,
  dk_omv_row_cultivation2,
  dk_omv_water2,
  dk_omv_water3,
  dk_omv_slurry_s2,
  dk_omv_slurry_p2,
  dk_omv_water4,
  dk_omv_row_cultivation3,
  dk_omv_spring_sow2,
  dk_omv_water5,
  dk_omv_row_cultivation4,
  dk_omv_insecticide1,
  dk_omv_insecticide2,
  dk_omv_insecticide3,
  dk_omv_insecticide4,
  dk_omv_harvest1,
  dk_omv_insecticide5,
  dk_omv_row_cultivation5,
  dk_omv_insecticide6,
  dk_omv_harvest2,
  dk_omv_foobar
} DK_OMixedVegToDo;



class DK_OMixedVeg: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OMixedVeg(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(20,3);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_omv_foobar - DK_OMV_BASE);
	  m_base_elements_no = DK_OMV_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others, // zero element unused but must be here
		  fmc_Others, // dk_omv_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Fertilizer, // dk_omv_ferti_s1 = DK_OMV_BASE,
		  fmc_Fertilizer, //   dk_omv_ferti_p1,
		  fmc_Cultivation, //   dk_omv_spring_plough,
		  fmc_Cultivation, //  dk_omv_spring_harrow,
		  fmc_Others, // dk_omv_spring_sow1,
		  fmc_Cultivation, //  dk_omv_row_cultivation1,
		  fmc_Fertilizer, //   dk_omv_slurry_s1,
		  fmc_Fertilizer, //   dk_omv_slurry_p1,
		  fmc_Watering, //  dk_omv_water1,
		  fmc_Cultivation, //  dk_omv_row_cultivation2,
		  fmc_Watering, //  dk_omv_water2,
		  fmc_Watering, // dk_omv_water3,
		  fmc_Fertilizer, //   dk_omv_slurry_s2,
		  fmc_Fertilizer, //   dk_omv_slurry_p2,
		  fmc_Watering, //  dk_omv_water4,
		  fmc_Cultivation, //  dk_omv_row_cultivation3,
		  fmc_Others, //  dk_omv_spring_sow2,
		  fmc_Watering, //  dk_omv_water5,
		  fmc_Cultivation, //  dk_omv_row_cultivation4,
		  fmc_Others, //  dk_omv_insecticide1,
		  fmc_Others, //   dk_omv_insecticide2,
		  fmc_Others,//   dk_omv_insecticide3,
		  fmc_Others, //   dk_omv_insecticide4,
		  fmc_Harvest, //   dk_omv_harvest1,
		  fmc_Others, //   dk_omv_insecticide5,
		  fmc_Cultivation, //  dk_omv_row_cultivation5,
		  fmc_Others, //   dk_omv_insecticide6,
		  fmc_Harvest, //   dk_omv_harvest2,
					  // No foobar entry
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};

#endif // DK_OMixedVeg_H
