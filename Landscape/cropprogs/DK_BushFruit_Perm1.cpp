/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_BushFruit_Perm1.cpp This file contains the source for the DK_BushFruit_Perm1 based on strawberries class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of July 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_BushFruit_Perm1.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_BushFruit_Perm1.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_bushfruit_on;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_BushFruit_Perm1::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	int today = g_date->Date();
	TTypesOfVegetation l_tov = tov_DKBushFruit_Perm1; 
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_bfp1_start:
		{
		DK_BFP_AFTER_EST = 0; //Here flags should get randomly for each field a value
		DK_BFP_EARLY_HARVEST = false;
			m_last_date = g_date->DayInYear(31, 10); // Should match the last flexdate below
			//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
			std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
			// Set up the date management stuff
					// Start and stop dates for all events after harvest
			flexdates[0][1] = g_date->DayInYear(30, 9); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use - NO harvest here - this is herbicide2 instead
			// Now these are done in pairs, start & end for each operation. If its not used then -1
			flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
			flexdates[1][1] = g_date->DayInYear(15, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - water2
			flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
			flexdates[1][1] = g_date->DayInYear(31, 10);
			// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
			int isSpring = 365;
			if (StartUpCrop(isSpring, flexdates, int(dk_bfp1_molluscicide))) break;

			// End single block date checking code. Please see next line comment as well.
			// Reinit d1 to first possible starting date.
			//Each field has assign randomly a DK_BFP_Yx flag value from 0 to 5

			if ((DK_BFP_AFTER_EST + g_date->GetYearNumber()) % 5 == 0)
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
				if (g_date->Date() >= d1) d1 += 365;

					SimpleEvent(d1, dk_bfp1_molluscicide, false);
					break;
			}
			else 
			{
				d1 = g_date->OldDays() + g_date->DayInYear(1, 1);
				if (g_date->Date() >= d1) d1 += 365;

				SimpleEvent(d1, dk_bfp_cover_on, false);
				break;
			}
		}
		break;

	// LKM: This is the first real farm operation - molluscicide if many snails (suggest 10%) - EST YEAR
	case dk_bfp1_molluscicide:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) {
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp1_molluscicide, true);
				break;
			}
		}
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 3))
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp1_fertilizer1_s, false);
			break;
		}
			else
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp1_fertilizer1_p, false);
			break;

	case dk_bfp1_fertilizer1_s:
			if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
			{
				if (!a_farm->FA_PK(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, dk_bfp1_fertilizer1_s, true);
					break;
				}
			}
			SimpleEvent(g_date->Date() + 1, dk_bfp1_spring_harrow1, false);
			break;

	case dk_bfp1_fertilizer1_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_PK(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp1_fertilizer1_p, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, dk_bfp1_spring_harrow1, false);
		break;

	case dk_bfp1_spring_harrow1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp1_spring_harrow1, true);
				break;
			}
		} // if time the harrow can continue, if not herbicide
		d1 = g_date->Date();
		if (d1 > g_date->OldDays() + g_date->DayInYear(23, 4)) {
			SimpleEvent(g_date->Date()+1, dk_bfp1_herbicide1, false);
			break;
		}
		else {
			SimpleEvent(g_date->Date() + 7, dk_bfp1_spring_harrow2, false);
			break;
		}
	case dk_bfp1_spring_harrow2:
		if (!a_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_spring_harrow2, true);
			break;
		}
		// if time the harrow can continue, if not herbicide
		d1 = g_date->Date();
		if (d1 > g_date->OldDays() + g_date->DayInYear(23, 4)) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_herbicide1, false);
			break;
		}
		else {
			SimpleEvent(g_date->Date() + 7, dk_bfp1_spring_harrow3, false);
			break;
		}
	case dk_bfp1_spring_harrow3:
		if (!a_farm->SpringHarrow(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_spring_harrow3, true);
			break;
		}		
		SimpleEvent(g_date->Date()+7, dk_bfp1_shallow_harrow1, false);
		break;
	case dk_bfp1_herbicide1:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_herbicide1, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_bfp1_shallow_harrow1, false);
		break;
	case dk_bfp1_shallow_harrow1:		
		if (!a_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_shallow_harrow1, true);
			break;
		}
		SimpleEvent(g_date->Date()+1, dk_bfp1_shallow_harrow2, false);
		break;
	case dk_bfp1_shallow_harrow2: 
		if (!a_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(8, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_shallow_harrow2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp1_insecticide1, false);
		break;
	case dk_bfp1_insecticide1:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(29, 6) - g_date->DayInYear();
		if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
		}
		else {
			flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
		}
		if (!flag) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_insecticide1, true);
			break;
		}
		SimpleEvent(g_date->Date()+1, dk_bfp1_plant, false);
		break;
	case dk_bfp1_plant:
		if (!a_farm->SpringSow(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_plant, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_bfp1_water1, false);
		break;
	case dk_bfp1_water1:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_water1, true);
			break;
		}

		//fork with parallel events:
		SimpleEvent(g_date->Date() + 10, dk_bfp1_strigling1, false); // weeding thread - main thread
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 14, dk_bfp1_fertilizer2_s, false); // fertilizer thread
			break;
		}
		else SimpleEvent(g_date->Date() + 14, dk_bfp1_fertilizer2_p, false); // fertilizer thread
		break;

	case dk_bfp1_fertilizer2_s:
		if (!a_farm->FA_N(m_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_fertilizer2_s, true);
			break;
		}
			break; // end of thread
		
	case dk_bfp1_fertilizer2_p:
		if (!a_farm->FP_N(m_field, 0.0, g_date->DayInYear(21, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_fertilizer2_p, true);
			break;
		}
		break;
		//end of fertilizer thread
	
	case dk_bfp1_strigling1:
		if (!a_farm->Strigling(m_field, 0.0, g_date->DayInYear(10, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_strigling1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp1_strigling2, false);
		break;
	case dk_bfp1_strigling2:
		if (!a_farm->Strigling(m_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_strigling2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp1_row_cultivation1, false);
		break;
	case dk_bfp1_row_cultivation1:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_row_cultivation1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp1_row_cultivation2, false);
		break;
	case dk_bfp1_row_cultivation2:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_row_cultivation2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp1_row_cultivation3, false);
		break;
	case dk_bfp1_row_cultivation3:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_row_cultivation3, true);
			break;
		}

		//fork of parallel events:
		SimpleEvent(g_date->Date(), dk_bfp1_herbicide2, false); // herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_bfp1_fungicide1, false); // fungicide thread
		break;

	case dk_bfp1_herbicide2:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_herbicide2, true);
			break;
		}
		break; // end of herbicide thread

	case dk_bfp1_fungicide1:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_fungicide1, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_bfp1_water2, false); 
		break;

	case dk_bfp1_water2:
		if (!a_farm->Water(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp1_water2, true);
			break;
		}
		d1 = g_date->Date();
		if (d1 > g_date->OldDays() + g_date->DayInYear(15, 10))
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 10), dk_bfp1_sleep_all_day, false);
			break;
		}
		else done = true;
		break;

	case dk_bfp1_sleep_all_day:
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop (DK_BushFruit_Perm2)
		// END of EST YEAR

		//BEGIN HARVEST YEARS
		// LKM: This is the first real farm operation - cover bushes with fiber (suggest 50%)
	case dk_bfp_cover_on:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->FiberCovering(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp_cover_on, true);
				break;
			}
			else
			{
				//We need to remember who did cover
				DK_BFP_EARLY_HARVEST = true;
			}
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp_water1_cover, false);
			break;
		}
		// fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp_water1, false); // water thread
		if (a_farm->IsStockFarmer())
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp_fertilizer1_s, false);
			break;
		}
		else
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp_fertilizer1_p, false);
		break;
	case dk_bfp_water1_cover:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_water1_cover, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_bfp_cover_off, false);
		break;
	case dk_bfp_cover_off:
		if (!m_farm->FiberRemoval(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_cover_off, true);
			break;
		}
		if (a_farm->IsStockFarmer())
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp_fertilizer1_s, false);
			break;
		}
		else
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_bfp_fertilizer1_p, false);
		break;
	case dk_bfp_water1:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_water1, true);
			break;
		}
		break; // end of water thread
	case dk_bfp_fertilizer1_s: // only for weak varieties, suggest 50%
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!a_farm->FA_N(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp_fertilizer1_s, true);
				break;
			}
		}
			//fork of parallel events:
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp_row_cultivation1, false); // weeding thread - main thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp_insecticide1, false); // insecticide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp_fungicide1, false); // fungicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp_fungicide3, false); // fungicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), dk_bfp_herbicide1, false); // herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_bfp_water2, false); // water thread
			break;

	case dk_bfp_fertilizer1_p: // only for weak varieties, suggest 50%
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!a_farm->FP_N(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp_fertilizer1_p, true);
				break;
			}
		}
		//fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp_row_cultivation1, false); // weeding thread - main thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp_insecticide1, false); // insecticide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp_fungicide1, false); // fungicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_bfp_fungicide3, false); // fungicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 4), dk_bfp_herbicide1, false); // herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_bfp_water2, false); // water thread
		break;
	case dk_bfp_insecticide1:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(30, 4) - g_date->DayInYear();
		if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
		}
		else {
			flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
		}
		if (!flag) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_insecticide1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, dk_bfp_insecticide2, false); // insecticide thread
		break;

	case dk_bfp_insecticide2:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(15, 5) - g_date->DayInYear();
		if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
		}
		else {
			flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
		}
		if (!flag) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_insecticide2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, dk_bfp_insecticide3, false); // insecticide thread
		break;

	case dk_bfp_insecticide3:
		// here we check whether we are using ERA pesticide or not
		d1 = g_date->DayInYear(31, 5) - g_date->DayInYear();
		if (!cfg_pest_bushfruit_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
		}
		else {
			flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
		}
		if (!flag) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_insecticide3, true);
			break;
		}
		break; // end of insecticide thread
	case dk_bfp_herbicide1:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_herbicide1, true);
			break;
		}
		break; // end of herbicide thread
	case dk_bfp_fungicide1:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fungicide1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 14, dk_bfp_fungicide2, false);
		break;
	case dk_bfp_fungicide2:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fungicide2, true);
			break;
		}
		break; // end of fungicide thread

	case dk_bfp_fungicide3:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fungicide3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp_fungicide4, false);
		break;
	case dk_bfp_fungicide4:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fungicide4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp_fungicide5, false);
		break;
	case dk_bfp_fungicide5:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fungicide5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp_fungicide6, false);
		break;
	case dk_bfp_fungicide6:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fungicide6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp_fungicide7, false);
		break;
	case dk_bfp_fungicide7:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fungicide7, true);
			break;
		}
		break;

	case dk_bfp_water2:
		if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_water2, true);
			break;
		}
		break; // end of water thread
	case dk_bfp_row_cultivation1:
		if (DK_BFP_EARLY_HARVEST == false) {
			if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_bfp_row_cultivation1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 10, dk_bfp_row_cultivation2, false);
			break;
		}
		else if (DK_BFP_EARLY_HARVEST == true) {
			SimpleEvent(g_date->Date(), dk_bfp_row_cultivation2, false);
			break;
		}
		break;

	case dk_bfp_row_cultivation2:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_row_cultivation2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_bfp_row_cultivation3, false);
		break;
	case dk_bfp_row_cultivation3:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_row_cultivation3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_bfp_straw_cover, false); // water thread
		break;
	case dk_bfp_straw_cover:
		if (!a_farm->StrawCovering(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_straw_cover, true);
			break;
		}

		if (DK_BFP_EARLY_HARVEST == true) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 6), dk_bfp_harvest, false);
			break;
		}
		else if (DK_BFP_EARLY_HARVEST == false) {
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 6), dk_bfp_harvest, false);
			break;
		}
		break;

	case dk_bfp_harvest:
	// continuous harvesting till 20 July, every third day
		m_farm->HarvestBushFruit(m_field, 0.0, 0);
		if (today < g_date->OldDays() + g_date->DayInYear(20, 7)) {
			SimpleEvent(g_date->Date() + 3, dk_bfp_harvest, true);
			break;
		}
		//fork of parallel events:
		SimpleEvent(g_date->Date(), dk_bfp_herbicide2, false); // herbicide thread
		if (a_farm->IsStockFarmer())
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 7), dk_bfp_fertilizer2_s, false); // fertilizer thread - main thread
			break;
		}
		else SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 7), dk_bfp_fertilizer2_p, false); // fertilizer thread - main thread
		break;
	case dk_bfp_herbicide2:
		if (!a_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(22, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_herbicide2, true);
			break;
		}
		break; // end of herbicide thread
	case dk_bfp_fertilizer2_s:
		if (!a_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fertilizer2_s, true);
			break;
		}
			//fork of parallel events:
			SimpleEvent(g_date->Date(), dk_bfp_herbicide3, false); // herbicide thread
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_bfp_fungicide8, false);
			break;

	case dk_bfp_fertilizer2_p:
		if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fertilizer2_p, true);
			break;
		}
		//fork of parallel events:
		SimpleEvent(g_date->Date(), dk_bfp_herbicide3, false); // herbicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_bfp_fungicide8, false);
		break;

	case dk_bfp_fungicide8:
		if (!a_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_fungicide8, true);
			break;
		}
		SimpleEvent(g_date->Date()+1, dk_bfp_water3, false);
		break; // end of fungicide thread

	case dk_bfp_herbicide3:
		if (!a_farm->HerbicideTreat(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_herbicide3, true);
			break;
		}
		break; // end of herbicide thread
	case dk_bfp_water3:
		if (!a_farm->Water(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_bfp_water3, true);
			break;
		}
		d1 = g_date->Date();
		if (d1 > g_date->OldDays() + g_date->DayInYear(15, 10))
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 10), dk_bfp1_sleep_all_day, false);
			break;
		}
		else done = true;
		break;

		default:
		g_msg->Warn(WARN_BUG, "DK_BushFruit_Perm1::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}