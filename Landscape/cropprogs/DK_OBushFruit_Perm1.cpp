/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OBushFruit_Perm1.cpp This file contains the source for the DK_OBushFruit_Perm1 - based on organic strawberries class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of July 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OBushFruit_Perm1.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OBushFruit_Perm1.h"

// Some things that are defined externally - in this case these variables allow




/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_OBushFruit_Perm1::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKOBushFruit_Perm1;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_obfp1_start:
	{
		// dk_obfp1_start just sets up all the starting conditions and reference dates that are needed to start a dk_obfp1

		DK_OBFP1_YEARS_AFTER_PLANT = 0;

		a_field->ClearManagementActionSum();
		m_last_date = g_date->DayInYear(31, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use - NO harvest here - this is hwater3 instead
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(31, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		if (StartUpCrop(365, flexdates, int(dk_obfp1_sleep_all_day))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		// OK, let's go.
		// LKM: Here we queue up the first event 
		//Each field has assign randomly a DK_OBFP1_YEARS_AFTER_PLANT 
		if ((DK_OBFP1_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 6 == 0)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + 365;
			SimpleEvent(d1, dk_obfp1_sleep_all_day, false);
		}
		else if ((DK_OBFP1_YEARS_AFTER_PLANT + g_date->GetYearNumber()) % 6 == 1)
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + 365;
			SimpleEvent(d1, dk_obfp1_water2, false);
		}
		else 
		{
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + 365;
			SimpleEvent(d1, dk_obfp_water1, false);
		}
		break;
	
	}
	break;

	// LKM: This is the first real farm operation
	case dk_obfp1_sleep_all_day:
		if (!m_farm->SleepAllDay(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_sleep_all_day, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_obfp1_molluscicide1, false);
		break;

	case dk_obfp1_molluscicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_molluscicide1, true);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer1_s, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer1_p, false);
		break;

	case dk_obfp1_fertilizer1_s:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer1_s, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_obfp1_autumn_harrow1, false);
		break;

	case dk_obfp1_fertilizer1_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer1_p, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_obfp1_autumn_harrow1, false);
		break;
		
	case dk_obfp1_autumn_harrow1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_autumn_harrow1, true);
				break;
			}
		} 
			SimpleEvent(g_date->Date() + 7, dk_obfp1_autumn_harrow2, false);
			break;
	case dk_obfp1_autumn_harrow2:
		if (!a_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(7, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_autumn_harrow2, true);
			break;
		}
			SimpleEvent(g_date->Date() + 7, dk_obfp1_autumn_harrow3, false);
			break;
	case dk_obfp1_autumn_harrow3:
		if (!a_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(14, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_autumn_harrow3, true);
			break;
		}		
		SimpleEvent(g_date->Date()+7, dk_obfp1_shallow_harrow1, false);
		break;
	case dk_obfp1_shallow_harrow1:		
		if (!a_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(21, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_shallow_harrow1, true);
			break;
		}
		SimpleEvent(g_date->Date()+1, dk_obfp1_shallow_harrow2, false);
		break;
	case dk_obfp1_shallow_harrow2: 
		if (!a_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(22, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_shallow_harrow2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), dk_obfp1_plant, false);
		break;
	case dk_obfp1_plant:
		if (!a_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_plant, true);
			break;
		}
		SimpleEvent(g_date->Date(), dk_obfp1_water1, false);
		break;
	case dk_obfp1_water1:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_water1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp1_row_cultivation1, false);
		break;
	case dk_obfp1_row_cultivation1:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(10, 12) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation1, true);
			break;
		}
		done = true;
		break; //est year done

		// first year after est - no harvest
	case dk_obfp1_water2:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_water2, true);
			break;
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer2_s, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer2_p, false);
		break;

	case dk_obfp1_fertilizer2_s:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer2_s, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_obfp1_row_cultivation2, false);
		break;

	case dk_obfp1_fertilizer2_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer2_p, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_obfp1_row_cultivation2, false);
		break;
	case dk_obfp1_row_cultivation2:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp1_row_cultivation3, false);
		break;
	case dk_obfp1_row_cultivation3:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation3, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp1_row_cultivation4, false);
		break;
	case dk_obfp1_row_cultivation4:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation4, true);
			break;
		}
		// fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp1_molluscicide2, false); // molluscicide thread
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_obfp1_water3, false); // water thread
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer3_s, false);
			break;
		}
		else SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer3_p, false);
		break;

	case dk_obfp1_molluscicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_molluscicide2, true);
				break;
			}
		}
		break; // end of molluscicide thread
	case dk_obfp1_water3:
		if (!a_farm->Water(m_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_water3, true);
			break;
		}
		break; // end of water thread
	case dk_obfp1_fertilizer3_s:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer3_s, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_obfp1_row_cultivation5, false);
		break;

	case dk_obfp1_fertilizer3_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp1_fertilizer3_p, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_obfp1_row_cultivation5, false);
		break;
	case dk_obfp1_row_cultivation5:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp1_row_cultivation6, false);
		break;
	case dk_obfp1_row_cultivation6:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(10, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation6, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp1_row_cultivation7, false);
		break;
	case dk_obfp1_row_cultivation7:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation7, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_obfp1_row_cultivation8, false);
		break;
	case dk_obfp1_row_cultivation8:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation8, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp1_row_cultivation9, false);
		break;
	case dk_obfp1_row_cultivation9:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation9, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp1_row_cultivation10, false);
		break;
	case dk_obfp1_row_cultivation10:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_row_cultivation10, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_obfp1_water4, false);
		break;
	case dk_obfp1_water4:
		if (!a_farm->Water(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp1_water4, true);
			break;
		}
		done = true;
		break; // first year after est done

		//4 years of harvest
	case dk_obfp_water1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp_water1, true);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_fertilizer1_s, false);
			break;
		}
		else
			SimpleEvent(g_date->Date() + 1, dk_obfp_fertilizer1_p, false);
		break;

	case dk_obfp_fertilizer1_s:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FA_NK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp_fertilizer1_s, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_obfp_row_cultivation1, false);
		break;

	case dk_obfp_fertilizer1_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp_fertilizer1_p, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_obfp_row_cultivation1, false);
		break;

	case dk_obfp_row_cultivation1:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_row_cultivation1, true);
			break;
		}

		//fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp_molluscicide, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp_water2, false);
		SimpleEvent(g_date->Date() + 10, dk_obfp_row_cultivation2, false); // main thread
		break;
	case dk_obfp_molluscicide:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp_molluscicide, true);
				break;
			}
		}
		break; // end of molluscicide thread
	case dk_obfp_water2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp_water2, true);
				break;
			}
		}
		break; // end of water thread
	case dk_obfp_row_cultivation2:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_row_cultivation2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp_row_cultivation3, false);
		break;
	case dk_obfp_row_cultivation3:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_row_cultivation3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_obfp_row_cultivation4, false);
		break;
	case dk_obfp_row_cultivation4:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_row_cultivation4, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp_row_cultivation5, false);
		break;
	case dk_obfp_row_cultivation5:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_row_cultivation5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp_row_cultivation6, false);
		break;
	case dk_obfp_row_cultivation6:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_row_cultivation6, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 7), dk_obfp_harvest, false);
		break;
	case dk_obfp_harvest:
		if (!a_farm->HarvestBF_Machine(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_harvest, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp_cut_bushes, false);
		break;
	case dk_obfp_cut_bushes:
		if (!a_farm->Pruning(m_field, 0.0, g_date->DayInYear(16, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_cut_bushes, true);
			break;
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_fertilizer2_s, false);
			break;
		}
		else
			SimpleEvent(g_date->Date() + 1, dk_obfp_fertilizer2_p, false);
		break;
	case dk_obfp_fertilizer2_s:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FA_NK(m_field, 0.0, g_date->DayInYear(17, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp_fertilizer2_s, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_obfp_water3, false);
		break;

	case dk_obfp_fertilizer2_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NK(m_field, 0.0, g_date->DayInYear(17, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp_fertilizer2_p, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_obfp_water3, false);
		break;
	case dk_obfp_water3:
		if (!a_farm->Water(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp_water3, true);
			break;
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop (DK_BushFruit_Perm2)
		// END of MAIN THREAD
		break;
		default:
		g_msg->Warn(WARN_BUG, "DK_OBushFruit_Perm1::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}