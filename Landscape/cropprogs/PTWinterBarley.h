/**
\file
\brief
<B>PTWinterBarley.h This file contains the headers for the PTWinterBarley class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTWinterBarley.h
//


#ifndef PTWINTERBARLEY_H
#define PTWINTERBARLEY_H

#define PTWINTERBARLEY_BASE 31800
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing Winter Barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_wb_start = 1, // Compulsory, must always be 1 (one).
	pt_wb_sleep_all_day = PTWINTERBARLEY_BASE,
	pt_wb_stubble_harrow,
	pt_wb_ferti_p1, // slurry
	pt_wb_ferti_s1,
	pt_wb_ferti_p2, // NPK
	pt_wb_ferti_s2,
	pt_wb_ferti_p3, // Calcium
	pt_wb_ferti_s3,
	pt_wb_ferti_p4, // NPK
	pt_wb_ferti_s4,
	pt_wb_autumn_harrow,
	pt_wb_autumn_sow,
	pt_wb_preseeding_cultivator_sow,
	pt_wb_harvest1,
	pt_wb_wait,
	pt_wb_harvest2,

} PTWinterBarleyToDo;


/**
\brief
PTWinterBarley class
\n
*/
/**
See PTWinterBarley.h::PTWinterBarleyToDo for a complete list of all possible events triggered codes by the Winter Barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PTWinterBarley: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PTWinterBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 15th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,11 );
   }
};

#endif // PTWINTERBARLEY_H

