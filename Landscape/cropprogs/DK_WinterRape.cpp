//
// DK_WinterRape.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_WinterRape.h"
#include "math.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_winterrape_on;
extern CfgFloat cfg_pest_product_1_amount;

bool DK_WinterRape::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;
    bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true, m_farm, m_field).
    bool flag = false;
    int d1 = 0;
    int noDates = 1;
    TTypesOfVegetation l_tov = tov_DKWinterRape;

    switch (m_ev->m_todo)
    {
    case dk_wosr_start:
    {
        a_field->ClearManagementActionSum();

        DK_WOSR_FERTI_S = false;
        DK_WOSR_FERTI_P = false;
        DK_WOSR_ROW_CULT = false;
        DK_WOSR_HERBI = false;

        m_last_date = g_date->DayInYear(19, 8); // Should match the last flexdate below
        //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
        std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
        // Set up the date management stuff
        // Start and stop dates for all events after harvest
        flexdates[0][1] = g_date->DayInYear(5, 8); // last possible day of swathing - this is in effect day before the earliest date that a following crop can use
        // Now these are done in pairs, start & end for each operation. If its not used then -1
        flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
        flexdates[1][1] = g_date->DayInYear(19, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1)


        // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
        if (StartUpCrop(0, flexdates, int(dk_wosr_wait))) break;

        // End single block date checking code. Please see next line comment as well.
        // Reinit d1 to first possible starting date.
        d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
        // OK, let's go.
        SimpleEvent(d1, dk_wosr_autumn_plough, false);
        break;
    }
    break;

    case dk_wosr_autumn_plough:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
            if (!m_farm->AutumnPlough(m_field, 0.0,
                g_date->DayInYear(20, 8) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_autumn_plough, true);
                break;
            }
            d1 = g_date->Date();
            if (d1 < g_date->OldDays() + g_date->DayInYear(10, 8)) {
                SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 8), dk_wosr_autumn_sow, false);
                break;
            }
            else
                SimpleEvent(g_date->Date() + 1, dk_wosr_autumn_sow, false);
            break;
        }
        else if (m_ev->m_lock || m_farm->DoIt_prob(0.20 / 0.50)) {
            SimpleEvent(g_date->Date(), dk_wosr_autumn_harrow, false);
            break;
        }
        else SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 8), dk_wosr_cultivator_sow, false);
        break;

    case dk_wosr_autumn_harrow:
        if (!m_farm->AutumnHarrow(m_field, 0.0,
            g_date->DayInYear(20, 8) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_autumn_harrow, true);
            break;
        }
        d1 = g_date->Date();
        if (d1 < g_date->OldDays() + g_date->DayInYear(10, 8)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 8), dk_wosr_autumn_sow, false);
            break;
        }
        else
            SimpleEvent(g_date->Date() + 1, dk_wosr_autumn_sow, false);
        break;

    case dk_wosr_cultivator_sow:
        if (!m_farm->PreseedingCultivatorSow(m_field, 0.0,
            g_date->DayInYear(25, 8) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_cultivator_sow, true);
            break;
        }
        if (a_farm->IsStockFarmer()) //Stock Farmer
        {
            SimpleEvent(g_date->Date(), dk_wosr_ferti_s1, false);
            SimpleEvent(g_date->Date() + 2, dk_wosr_herbicide1, false); // main thread
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_row_cultivation1, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_gr, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_insecticide1, false);
            break;
        }
        else SimpleEvent(g_date->Date(), dk_wosr_ferti_p1, false);
        SimpleEvent(g_date->Date() + 2, dk_wosr_herbicide1, false); // main thread
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_row_cultivation1, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_gr, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_insecticide1, false);
        break;

    case dk_wosr_autumn_sow:
        if (!m_farm->AutumnSow(m_field, 0.0,
            g_date->DayInYear(25, 8) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_autumn_sow, true);
            break;
        }
        if (a_farm->IsStockFarmer()) //Stock Farmer
        {
            SimpleEvent(g_date->Date(), dk_wosr_ferti_s1, false);
            SimpleEvent(g_date->Date() + 2, dk_wosr_herbicide1, false); // main thread
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_row_cultivation1, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_gr, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_insecticide1, false);
            break;
        }
        else SimpleEvent(g_date->Date(), dk_wosr_ferti_p1, false);
        SimpleEvent(g_date->Date() + 2, dk_wosr_herbicide1, false); // main thread
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_row_cultivation1, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_gr, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_insecticide1, false);
        break;

    case dk_wosr_ferti_s1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
            if (!m_farm->FA_Slurry(m_field, 0.0,
                g_date->DayInYear(25, 9) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_s1, true);
                break;
            }
        }
        else if (m_ev->m_lock || m_farm->DoIt_prob(0.25 / 0.75)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_ferti_s2, false);
            break;
        }
        else SimpleEvent(g_date->Date(), dk_wosr_ferti_s3, false);
        break;

    case dk_wosr_ferti_p1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.25)) {
            if (!m_farm->FP_Slurry(m_field, 0.0,
                g_date->DayInYear(25, 9) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_p1, true);
                break;
            }
        }
        else if (m_ev->m_lock || m_farm->DoIt_prob(0.25 / 0.75)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_wosr_ferti_p2, false);
            break;
        }
        else SimpleEvent(g_date->Date(), dk_wosr_ferti_p3, false);
        break;

    case dk_wosr_ferti_s2:
        if (!m_farm->FA_Slurry(m_field, 0.0,
            g_date->DayInYear(30, 9) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_s2, true);
            break;
        }
        break; // end of thread

    case dk_wosr_ferti_p2:
        if (!m_farm->FP_Slurry(m_field, 0.0,
            g_date->DayInYear(30, 9) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_p2, true);
            break;
        }
        break; // end of thread

    case dk_wosr_ferti_s3:
        if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0,
            g_date->DayInYear(30, 9) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_s3, true);
            break;
        }
        DK_WOSR_FERTI_S = true; // we need to remember who did NPK instead of slurry
        break; // end of thread

    case dk_wosr_ferti_p3:
        if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0,
            g_date->DayInYear(30, 9) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_p3, true);
            break;
        }
        DK_WOSR_FERTI_P = true; // we need to remember who did NPK instead of slurry
        break; // end of thread

    case dk_wosr_row_cultivation1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.01)) {
            if (!m_farm->RowCultivation(m_field, 0.0,
                g_date->DayInYear(15, 9) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_row_cultivation1, true);
                break;
            }
            SimpleEvent(g_date->Date() + 14, dk_wosr_row_cultivation2, false);
            break;
        }
        break; // end of thread

    case dk_wosr_row_cultivation2:
        if (!m_farm->RowCultivation(m_field, 0.0,
            g_date->DayInYear(5, 10) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_row_cultivation2, true);
            break;
        }
        DK_WOSR_ROW_CULT = true; // we need to remember who did row cultivation
        break; // end of thread

    case dk_wosr_gr:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
            if (!m_farm->GrowthRegulator(m_field, 0.0,
                g_date->DayInYear(30, 9) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_gr, true);
                break;
            }
        }
        break; // end of thread 

    case dk_wosr_insecticide1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.80)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(15, 10) - g_date->DayInYear();
            if (!cfg_pest_winterrape_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_insecticide1, true);
                break;
            }
        }
        break; // end of thread

    case dk_wosr_herbicide1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.35)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(28, 8) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide1, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 7, dk_wosr_herbicide2, false);
        break;

    case dk_wosr_herbicide2: // 1st split treatment
        if (m_ev->m_lock || m_farm->DoIt_prob(0.40)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(5, 9) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide2, true);
                break;
            }
            DK_WOSR_HERBI = true;
        }
        SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide3, false);
        break;

    case dk_wosr_herbicide3:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(13, 9) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide3, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide4, false);
        break;

    case dk_wosr_herbicide4: // 2nd of split treatment
        if (DK_WOSR_HERBI == true) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(14, 10) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide4, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 7, dk_wosr_herbicide5, false);
        break;

    case dk_wosr_herbicide5:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.40)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(31, 10) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide5, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 10), dk_wosr_herbicide6, false);
        break;

    case dk_wosr_herbicide6:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.90)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(30, 11) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide6, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_wosr_wait, false);
        break;

    case dk_wosr_wait:
        if (!m_farm->SleepAllDay(m_field, 0.0,
            g_date->DayInYear(28, 2) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_wait, true);
            break;
        }
        if (a_farm->IsStockFarmer()) //Stock Farmer
        {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_ferti_s4, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_ferti_s5, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_ferti_s7, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_row_cultivation3, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_herbicide7, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_wosr_insecticide2, false);
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_wosr_fungicide1, false);
            break;
        }
        else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_ferti_p4, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_ferti_p5, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_ferti_p7, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_row_cultivation3, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_wosr_herbicide7, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_wosr_insecticide2, false);
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_wosr_fungicide1, false);
        break;

    case dk_wosr_ferti_s4:
        if (DK_WOSR_FERTI_S == false) {
            if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) { // 50% of farmers applying slurry in autumn
                if (!m_farm->FA_Slurry(m_field, 0.0,
                    g_date->DayInYear(31, 3) - g_date->DayInYear())) {
                    SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_s4, true);
                    break;
                }
            }
        }
        break; // end of thread

    case dk_wosr_ferti_p4:
        if (DK_WOSR_FERTI_P == false) {
            if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) { // 50% of farmers applying slurry in autumn
                if (!m_farm->FP_Slurry(m_field, 0.0,
                    g_date->DayInYear(31, 3) - g_date->DayInYear())) {
                    SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_p4, true);
                    break;
                }
            }
        }
        break; // end of thread

    case dk_wosr_ferti_s5:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.90)) {
            if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0,
                g_date->DayInYear(10, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_s5, true);
                break;
            }
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), dk_wosr_ferti_s6, false);
            break;
        }
        break; // end of thread

    case dk_wosr_ferti_p5:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.90)) {
            if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0,
                g_date->DayInYear(10, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_p5, true);
                break;
            }
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 4), dk_wosr_ferti_p6, false);
            break;
        }
        break; // end of thread

    case dk_wosr_ferti_s6:
        if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0,
            g_date->DayInYear(30, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_s6, true);
            break;
        }
        break; // end of thread

    case dk_wosr_ferti_p6:
        if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0,
            g_date->DayInYear(30, 4) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_p6, true);
            break;
        }
        break; // end of thread

    case dk_wosr_ferti_s7:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.15)) {
            if (!m_farm->FA_Boron(m_field, 0.0,
                g_date->DayInYear(31, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_s7, true);
                break;
            }
        }
        break; // end of thread
        
    case dk_wosr_ferti_p7:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.15)) {
            if (!m_farm->FP_Boron(m_field, 0.0,
                g_date->DayInYear(31, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_ferti_p7, true);
                break;
            }
        }
        break; // end of thread

    case dk_wosr_row_cultivation3:
        if (DK_WOSR_ROW_CULT) {
            if (!m_farm->RowCultivation(m_field, 0.0,
                g_date->DayInYear(31, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_row_cultivation3, true);
                break;
            }
        }
        break; // end of thread

    case dk_wosr_herbicide7:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.05)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(31, 3) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide7, true);
                break;
            }
        }
        break; // end of thread

    case dk_wosr_insecticide2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(31, 5) - g_date->DayInYear();
            if (!cfg_pest_winterrape_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_insecticide2, true);
                break;
            }
        }
        d1 = g_date->Date();
        if (d1 < g_date->OldDays() + g_date->DayInYear(17, 4)) {
            SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_wosr_insecticide3, false);
            break;
        }
        else
        SimpleEvent(g_date->Date() + 14, dk_wosr_insecticide3, false);
        break;

    case dk_wosr_insecticide3:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
            // here we check whether we are using ERA pesticide or not
            d1 = g_date->DayInYear(30, 6) - g_date->DayInYear();
            if (!cfg_pest_winterrape_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
            {
                flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
            }
            else {
                flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
            }
            if (!flag) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_insecticide3, true);
                break;
            }
        }
        break; // end of thread

    case dk_wosr_fungicide1:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.80)) {
            if (!m_farm->FungicideTreat(m_field, 0.0,
                g_date->DayInYear(31, 5) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_fungicide1, true);
                break;
            }
        }
        SimpleEvent(g_date->Date() + 14, dk_wosr_fungicide2, false);
        break;

    case dk_wosr_fungicide2:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.30)) {
            if (!m_farm->FungicideTreat(m_field, 0.0,
                g_date->DayInYear(15, 6) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_fungicide2, true);
                break;
            }
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_wosr_herbicide8, false);
        break;

    case dk_wosr_herbicide8:
        if (m_ev->m_lock || m_farm->DoIt_prob(0.20)) {
            if (!m_farm->HerbicideTreat(m_field, 0.0,
                g_date->DayInYear(4, 8) - g_date->DayInYear())) {
                SimpleEvent(g_date->Date() + 1, dk_wosr_herbicide8, true);
                break;
            }
            SimpleEvent(g_date->Date() + 1, dk_wosr_swathing, false);
            break;
        }
        SimpleEvent(g_date->OldDays() + g_date->DayInYear(9, 7), dk_wosr_harvest, false);
        break;

    case dk_wosr_swathing:
        if (!m_farm->Swathing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_swathing, true);
            break;
        }
        SimpleEvent(g_date->Date() + 7, dk_wosr_harvest, false);
        break;

    case dk_wosr_harvest:
        if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
            SimpleEvent(g_date->Date() + 1, dk_wosr_harvest, true);
            break;
        }
        done = true;
        break;

    default:
        g_msg->Warn(WARN_BUG, "DK_WinterRape::Do(): "
            "Unknown event type! ", "");
        exit(1);
    }
    return done;
}