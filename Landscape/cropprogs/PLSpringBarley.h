/**
\file
\brief
<B>PLSpringBarley.h This file contains the headers for the SpringBarley class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLSpringBarley.h
//


#ifndef PLSPRINGBARLEY_H
#define PLSPRINGBARLEY_H

#define PLSPRINGBARLEY_BASE 25900
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_SB_FERTI_P1	a_field->m_user[1]
#define PL_SB_FERTI_S1	a_field->m_user[2]
#define PL_SB_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_SB_FERTI_P4	a_field->m_user[4]
#define PL_SB_FERTI_S4	a_field->m_user[5]
#define PL_SB_SPRING_FERTI a_field->m_user[6]
#define PL_SB_DECIDE_TO_GR a_field->m_user[7]

/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_sb_start = 1, // Compulsory, must always be 1 (one).
	pl_sb_sleep_all_day = PLSPRINGBARLEY_BASE,
	pl_sb_ferti_p1, // 20601
	pl_sb_ferti_s1,
	pl_sb_stubble_plough,
	pl_sb_autumn_harrow1,
	pl_sb_autumn_harrow2,
	pl_sb_stubble_harrow,
	pl_sb_ferti_p2,
	pl_sb_ferti_s2,
	pl_sb_ferti_p3,
	pl_sb_ferti_s3,	// 20610
	pl_sb_winter_plough,
	pl_sb_ferti_p4,
	pl_sb_ferti_s4,
	pl_sb_spring_harrow,
	pl_sb_ferti_p5,
	pl_sb_ferti_s5,
	pl_sb_heavy_cultivator,
	pl_sb_preseeding_cultivator,
	pl_sb_preseeding_cultivator_sow,	// 20620
	pl_sb_spring_sow,	
	pl_sb_herbicide1,
	pl_sb_fungicide1,
	pl_sb_fungicide2,
	pl_sb_fungicide3,
	pl_sb_insecticide1,
	pl_sb_insecticide2,
	pl_sb_ferti_p6,		
	pl_sb_ferti_s6,	
	pl_sb_ferti_p7,	// 20630
	pl_sb_ferti_s7,
	pl_sb_ferti_p8,
	pl_sb_ferti_s8,
	pl_sb_harvest,
	pl_sb_straw_chopping,
	pl_sb_hay_bailing,
	pl_sb_ferti_p9,
	pl_sb_ferti_s9,	
	pl_sb_ferti_p10,	
	pl_sb_ferti_s10,	// 20640
	pl_sb_foobar
} PLSpringBarleyToDo;


/**
\brief
PLSpringBarley class
\n
*/
/**
See PLSpringBarley.h::PLSpringBarleyToDo for a complete list of all possible events triggered codes by the spring barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLSpringBarley: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLSpringBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (pl_sb_foobar - PLSPRINGBARLEY_BASE);
	   m_base_elements_no = PLSPRINGBARLEY_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here
			fmc_Others,//pl_sb_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,//pl_sb_sleep_all_day = PLSPRINGBARLEY_BASE,
			fmc_Fertilizer,//pl_sb_ferti_p1, // 20601
			fmc_Fertilizer,//pl_sb_ferti_s1,
			fmc_Cultivation,//pl_sb_stubble_plough,
			fmc_Cultivation,//pl_sb_autumn_harrow1,
			fmc_Cultivation,//pl_sb_autumn_harrow2,
			fmc_Cultivation,//pl_sb_stubble_harrow,
			fmc_Fertilizer,//pl_sb_ferti_p2,
			fmc_Fertilizer,//pl_sb_ferti_s2,
			fmc_Fertilizer,//pl_sb_ferti_p3,
			fmc_Fertilizer,//pl_sb_ferti_s3,	// 20610
			fmc_Cultivation,//pl_sb_winter_plough,
			fmc_Fertilizer,//pl_sb_ferti_p4,
			fmc_Fertilizer,//pl_sb_ferti_s4,
			fmc_Cultivation,//pl_sb_spring_harrow,
			fmc_Fertilizer,//pl_sb_ferti_p5,
			fmc_Fertilizer,//pl_sb_ferti_s5,
			fmc_Cultivation,//pl_sb_heavy_cultivator,
			fmc_Cultivation,//pl_sb_preseeding_cultivator,
			fmc_Cultivation,//pl_sb_preseeding_cultivator_sow,	// 20620
			fmc_Others,//pl_sb_spring_sow,
			fmc_Herbicide,//pl_sb_herbicide1,
			fmc_Fungicide,//pl_sb_fungicide1,
			fmc_Fungicide,//pl_sb_fungicide2,
			fmc_Fungicide,//pl_sb_fungicide3,
			fmc_Insecticide,//pl_sb_insecticide1,
			fmc_Insecticide,//pl_sb_insecticide2,
			fmc_Fertilizer,//pl_sb_ferti_p6,
			fmc_Fertilizer,//pl_sb_ferti_s6,
			fmc_Fertilizer,//pl_sb_ferti_p7,	// 20630
			fmc_Fertilizer,//pl_sb_ferti_s7,
			fmc_Fertilizer,//pl_sb_ferti_p8,
			fmc_Fertilizer,//pl_sb_ferti_s8,
			fmc_Harvest,//pl_sb_harvest,
			fmc_Others,//pl_sb_straw_chopping,
			fmc_Others,//pl_sb_hay_bailing,
			fmc_Fertilizer,//pl_sb_ferti_p9,
			fmc_Fertilizer,//pl_sb_ferti_s9,
			fmc_Fertilizer,//pl_sb_ferti_p10,
			fmc_Fertilizer//pl_sb_ferti_s10,	// 20640
	   };
	   // Iterate over the catlist elements and copy them to vector				
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

   }
};

#endif // PLSPRINGBARLEY_H

