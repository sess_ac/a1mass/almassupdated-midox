//
// DK_OSpringBarley_CC.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKOSpringBarley_CC_H
#define DKOSpringBarley_CC_H

#define DK_OSBMCC_BASE 161500

typedef enum {
  dk_osbmcc_start = 1, // Compulsory, start event must always be 1 (one).
  dk_osbmcc_spring_harrow1 = DK_OSBMCC_BASE,
  dk_osbmcc_spring_harrow2,
  dk_osbmcc_slurry_s,
  dk_osbmcc_slurry_p,
  dk_osbmcc_spring_plough,
  dk_osbmcc_spring_sow,
  dk_osbmcc_shallow_harrow1,
  dk_osbmcc_shallow_harrow2,
  dk_osbmcc_row_cultivation1,
  dk_osbmcc_row_cultivation2,
  dk_osbmcc_water,
  dk_osbmcc_swathing, 
  dk_osbmcc_harvest,
  dk_osbmcc_straw_chopping,
  dk_osbmcc_hay_baling,
  dk_osbmcc_cut_to_hay, 
  dk_osbmcc_stubble_harrow,
  dk_osbmcc_plough,
  dk_osbmcc_wait,
  dk_osbmcc_foobar,
} DK_OSpringBarley_CCToDo;



class DK_OSpringBarley_CC: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OSpringBarley_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(29,3);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_osbmcc_foobar - DK_OSBMCC_BASE);
	  m_base_elements_no = DK_OSBMCC_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_osbmcc_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation,	//	  dk_osbmcc_spring_harrow = DK_OSBMCC_BASE,
			fmc_Cultivation, // dk_osbmcc_spring_harrow2,
			fmc_Fertilizer,	//	  dk_osbmcc_slurry,
			fmc_Fertilizer,	//	  dk_osbmcc_slurry,
			fmc_Cultivation,	//	  dk_osbmcc_spring_plough,
			fmc_Others,	//	  dk_osbmcc_spring_sow,
			fmc_Cultivation,	//	  dk_osbmcc_shallow_harrow1,
			fmc_Cultivation,	//	  dk_osbmcc_shallow_harrow2,
			fmc_Cultivation,	//	  dk_osbmcc_row_cultivation1,
			fmc_Cultivation,	//	  dk_osbmcc_row_cultivation2,
			fmc_Watering,	//	  dk_osbmcc_water,
			fmc_Cutting,	//	  dk_osbmcc_swathing, 
			fmc_Harvest,	//	  dk_osbmcc_harvest,
			fmc_Others,	//	  dk_osbmcc_straw_chopping,
			fmc_Others,	//	  dk_osbmcc_hay_baling,
			fmc_Cutting,	//	  dk_osbmcc_cut_to_hay, 
			fmc_Cultivation,	//	  dk_osbmcc_stubble_harrow,
			fmc_Cultivation,	//	  dk_osbmcc_plough,
			fmc_Others, // dk_osbmcc_wait,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DK_OSpringBarley_CC_H
