//
// DK_OWinterRape.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OWinterRape_H
#define DK_OWinterRape_H

#define DK_OOSR_EARLY_SLURRY   a_field->m_user[1]
#define DK_OOSR_ROW_SOW   a_field->m_user[2]

#define DK_OOSR_BASE 160300

typedef enum {
  dk_oosr_start = 1, // Compulsory, start event must always be 1 (one).
  dk_oosr_autumn_harrow1 = DK_OOSR_BASE,
  dk_oosr_autumn_harrow2,
  dk_oosr_slurry_s1,
  dk_oosr_slurry_p1,
  dk_oosr_autumn_plough,
  dk_oosr_cultivator_sow, // if row sown
  dk_oosr_sow,
  dk_oosr_row_cultivation1, 
  dk_oosr_slurry_s2,
  dk_oosr_slurry_p2,
  dk_oosr_ferti_s,
  dk_oosr_ferti_p,
  dk_oosr_slurry_s3,
  dk_oosr_slurry_p3,
  dk_oosr_row_cultivation2,
  dk_oosr_water,
  dk_oosr_swathing,
  dk_oosr_harvest,
  dk_oosr_foobar
} DK_OWinterRapeToDo;



class DK_OWinterRape : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OWinterRape(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(15, 8);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_oosr_foobar - DK_OOSR_BASE);
		m_base_elements_no = DK_OOSR_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_oosr_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation, // dk_oosr_autumn_harrow1 = DK_OOSR_BASE,
			fmc_Cultivation, // dk_oosr_autumn_harrow2,
			fmc_Fertilizer, // dk_oosr_slurry_s1,
			fmc_Fertilizer, // dk_oosr_slurry_p1,
			fmc_Cultivation, //  dk_oosr_autumn_plough,
			fmc_Cultivation, //  dk_oosr_cultivator_sow, // if row sown
			fmc_Others, //  dk_oosr_sow,
			fmc_Cultivation, //  dk_oosr_row_cultivation1,
			fmc_Fertilizer, //  dk_oosr_slurry_s2,
			fmc_Fertilizer, //  dk_oosr_slurry_p2,
			fmc_Fertilizer, //  dk_oosr_ferti_s,
			fmc_Fertilizer, //  dk_oosr_ferti_p,
			fmc_Fertilizer, //  dk_oosr_slurry_s3,
			fmc_Fertilizer, //  dk_oosr_slurry_p3,
			fmc_Cultivation, //  dk_oosr_row_cultivation2,
			fmc_Watering, //  dk_oosr_water,
			fmc_Cutting, //  dk_oosr_swathing,
			fmc_Harvest, //   dk_oosr_harvest,
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_OWinterRape_H
