/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
* * * Adapted for UK Beans, 2021, Adam McVeigh, Nottingham Trent University
*/
/**
\file
\brief
<B>UKMaize.cpp This file contains the source for the UKMaize class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
// UKMaize.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/UKBeans.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_springbarley_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_BNS_InsecticideDay;
extern CfgInt   cfg_BNS_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional beans.
*/
bool UKBeans::Do(Farm *m_farm, LE *m_field, FarmEvent *m_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = m_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = m_field;
	m_ev = m_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_UKBeans; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case uk_bns_start:
	{
		// uk_m_start just sets up all the starting conditions and reference dates that are needed to start a uk_maize
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// start and stop dates for all 'movable' events for this crop
		m_field->SetMDates(0, 0, g_date->DayInYear(30, 10)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(30, 10)); // last possible day of straw chopping
		// then these will be reduced in value to 0

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "UKBeans::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "UKBeans::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "UKBeans::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), uk_bns_fungicide1, false, m_farm, m_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		// OK, let's go.

		// Here we queue up the first event
		SimpleEvent_(d1, uk_bns_autumn_harrow, false, m_farm, m_field);

	}
	break;
	// This is the first real farm operation

	// Stubble cultivator / autumn harrow 50% early September. Do autumn harrow and then preseeding cultivation. Otherwise do autumn plough
	case uk_bns_autumn_harrow:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_bns_autumn_harrow, true, m_farm, m_field);
				break;
			}
			else
			{
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10) + 1, uk_bns_preseeding_cultivator, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 9), uk_bns_autumn_plough, false, m_farm, m_field);
		break;
		// Autumn Plough 100% (those who did not autumn harrow) September-October
	case uk_bns_autumn_plough:
		if (!m_farm->StubblePlough(m_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_autumn_plough, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), uk_bns_autumn_sowb, false, m_farm, m_field);
		break;

	// Preseeding cultivator 100% October-November (only those who did autumn harrow)
	case uk_bns_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(25, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), uk_bns_autumn_sowdd, false, m_farm, m_field);
		break;
		// Sow 100% October-November
		// Drilled if autumn harrow or Broadcast if no autumn harrow.
		// If broadcast than big seed is ploughed in.
		// Drilled Sow 100% October-November
	case uk_bns_autumn_sowdd:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(28, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_autumn_sowdd, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer // setting the next step which is PK fertiliser application
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(21, 10), uk_bns_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(21, 10), uk_bns_ferti_p1, false, m_farm, m_field);
		break;
		// Broadcast Sow 100% October-November
	case uk_bns_autumn_sowb:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(28, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_autumn_sowb, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_bns_winter_plough, false, m_farm, m_field);
		break;
		// Winter Plough 100% October-November (this ploughs in the broadcast seed)
	case uk_bns_winter_plough:
		if (!m_farm->WinterPlough(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_winter_plough, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer // setting the next step which is PK fertiliser application
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(21, 10), uk_bns_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(21, 10), uk_bns_ferti_p1, false, m_farm, m_field);
		break;
		// PK 100% October-December
		// Stock farmers
	case uk_bns_ferti_s1:
		if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(6, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_ferti_s1, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to three parallel events
		SimpleEvent_(g_date->Date() + 1, uk_bns_herbicide1, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, uk_bns_fungicide1, false, m_farm, m_field); // Fungicide thread = MAIN thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, uk_bns_insecticide1, false, m_farm, m_field);	// Insecticide thread
		break;
		// PK 100% October-December
		// Arable farmers
	case uk_bns_ferti_p1:
		if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(6, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_ferti_p1, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to three parallel events
		SimpleEvent_(g_date->Date() + 1, uk_bns_herbicide1, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, uk_bns_fungicide1, false, m_farm, m_field); // Fungicide thread = MAIN thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, uk_bns_insecticide1, false, m_farm, m_field);	// Insecticide thread
		break;
		//Herbicide thread
		// Herbicide1 100% October-December (pre-em)
	case uk_bns_herbicide1:
		//if (m_field->GetGreenBiomass() <= 0)
		//{
		//	SimpleEvent_(g_date->Date() + 1, uk_bns_herbicide1, false, m_farm, m_field);
		//}
		//else
		//{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(7, 12) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_bns_herbicide1, true, m_farm, m_field);
				break;
			}
		//}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 2) + 365, uk_bns_herbicide2, false, m_farm, m_field);
		break;
		// Herbicide2 10%
	case uk_bns_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.1))
		{
			//if (m_field->GetGreenBiomass() <= 0)
			//{
			//	SimpleEvent_(g_date->Date() + 1, uk_bns_herbicide2, false, m_farm, m_field);
			//}
			//else
			//{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_bns_herbicide2, true, m_farm, m_field);
					break;
				}
			//}
		}
		// End of thread
		break;
		// Fungicide thread - MAIN THREAD
		// Fungicide 1 100% April-May
	case uk_bns_fungicide1:
		if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_fungicide1, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 9), uk_bns_harvest, false, m_farm, m_field);
		break;
		// Insecticide thread
		// Insecticide 1 80% March (for Brucidae)
	case uk_bns_insecticide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_springbarley_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_bns_insecticide1, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), uk_bns_insecticide2, false, m_farm, m_field);
		break;
		// Insecticide2 80% May (for Aphidae)
	case uk_bns_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_springbarley_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_bns_insecticide2, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), uk_bns_insecticide3, false, m_farm, m_field);
		break;
		// Insecticide3 80% June (for Brucidae)
	case uk_bns_insecticide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_springbarley_on.value() ||
				!m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_bns_insecticide3, true, m_farm, m_field);
					break;
				}
			}
			else {
				m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
		// Back to main thread
		// Harvest 100% September-October
	case uk_bns_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_bns_harvest, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "UKBeans::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}