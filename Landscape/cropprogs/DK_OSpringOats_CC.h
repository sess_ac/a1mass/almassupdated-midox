//
// DK_OSpringOats_CC.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OSpringOats_CC_H
#define DK_OSpringOats_CC_H

#define  DK_OSOCC_FORCESPRING   a_field->m_user[1]


#define DK_OSOCC_BASE 161400

typedef enum {
	dk_osocc_start = 1, // Compulsory, start event must always be 1 (one).
	dk_osocc_spring_harrow1 = DK_OSOCC_BASE,
	dk_osocc_spring_harrow2,
	dk_osocc_slurry_s,
	dk_osocc_slurry_p,
	dk_osocc_spring_plough,
	dk_osocc_spring_sow,
	dk_osocc_strigling1,
	dk_osocc_strigling2,
	dk_osocc_roll,
	dk_osocc_weed_harrow,
	dk_osocc_row_cultivation,
	dk_osocc_water,
	dk_osocc_swathing,
	dk_osocc_harvest,
	dk_osocc_hay_bailing,
	dk_osocc_straw_chopping,
	dk_osocc_wait,
	dk_osocc_foobar
} DK_OSpringOats_CCToDo;



class DK_OSpringOats_CC : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OSpringOats_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(28, 3);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_osocc_foobar - DK_OSOCC_BASE);
		m_base_elements_no = DK_OSOCC_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Others, // dk_osocc_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation, //dk_osocc_spring_harrow1 = DK_OSO_BASE,
			fmc_Cultivation, //dk_osocc_spring_harrow2,
			fmc_Fertilizer, //dk_osocc_slurry_s,
			fmc_Fertilizer, //dk_osocc_slurry_p,
			fmc_Cultivation, //dk_osocc_spring_plough,
			fmc_Others, //dk_osocc_spring_sow,
			fmc_Cultivation, //dk_osocc_strigling1,
			fmc_Cultivation, //  dk_osocc_strigling2,
			fmc_Others, //  dk_osocc_roll,
			fmc_Cultivation, // dk_osocc_weed_harrow,
			fmc_Cultivation, //  dk_osocc_row_cultivation,
			fmc_Watering, // dk_osocc_water,
			fmc_Cutting, // dk_osocc_swathing,
			fmc_Harvest, //  dk_osocc_harvest,
			fmc_Others, //  dk_osocc_hay_bailing,
			fmc_Cutting, //  dk_osocc_straw_chopping,
			fmc_Others, // dk_osocc_wait,
						// No foobar entry
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_OSpringOats_CCH
