//
// DE_OMaizeSilage.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DE_OMAIZESILAGE_H
#define DE_OMAIZESILAGE_H

#define DE_OMAIZESILAGE_BASE 37200
#define DE_OMAIZESILAGE_SOW_DATE m_field->m_user[0]

typedef enum {
  de_oms_start = 1, // Compulsory, start event must always be 1 (one).
  de_oms_sleep_all_day = DE_OMAIZESILAGE_BASE,
  de_oms_fa_manure,
  de_oms_fp_manure,
  de_oms_fa_slurry_one,
  de_oms_fp_slurry_one,
  de_oms_spring_plough,
  de_oms_spring_harrow,
  de_oms_spring_sow,
  de_oms_strigling,
  de_oms_row_one,
  de_oms_fa_slurry_two,
  de_oms_fp_slurry_two,
  de_oms_row_two,
  de_oms_row_three,
  de_oms_water,
  de_oms_harvest,
  de_oms_stubble,
  de_oms_foobar,
} DE_OMaizeSilageToDo;



class DE_OMaizeSilage: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OMaizeSilage(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
	  m_first_date = g_date->DayInYear(25, 4);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_oms_foobar - DE_OMAIZESILAGE_BASE);
	  m_base_elements_no = DE_OMAIZESILAGE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_oms_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others,	//	  de_om_sleep_all_day = DE_OMAIZESILAGE_BASE,
			fmc_Fertilizer,	//	  de_oms_fa_manure_b,
			fmc_Fertilizer,	//	  de_oms_fp_manure_b,
			fmc_Fertilizer,	//	  de_oms_fa_slurry_one,
			fmc_Fertilizer,	//	  de_oms_fp_slurry_one,
			fmc_Cultivation,	//	  de_oms_spring_plough,
			fmc_Cultivation,	//	  de_oms_spring_harrow,
			fmc_Others,	//	  de_oms_spring_sow,
			fmc_Cultivation,	//	  de_oms_strigling,
			fmc_Cultivation,	//	  de_oms_row_one,
			fmc_Fertilizer,	//	  de_oms_fa_slurry_two,
			fmc_Fertilizer,	//	  de_oms_fp_slurry_two,
			fmc_Cultivation,	//	  de_oms_row_two,
			fmc_Cultivation,	//	  de_oms_row_three,
			fmc_Others,	//	  de_oms_water,
			fmc_Harvest,	//	  de_oms_harvest,
			fmc_Cultivation,	//	  de_oms_stubble,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DE_OMAIZESILAGE_H
