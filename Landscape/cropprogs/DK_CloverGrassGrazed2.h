/**
\file
\brief
<B>DK_CloverGrassGrazed2.h This file contains the headers for the DK_CloverGrassGrazed2 class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// DK_CloverGrassGrazed2.h
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_CLOVERGRASSGRAZED2_H
#define DK_CLOVERGRASSGRAZED2_H

#define DK_C2_BASE 62400

#define DK_C2_CC m_field->m_user[0]
#define DK_C2_CUT m_field->m_user[1]
/**
\THIS CODE IS FOR NEXT YEARS OF CLOVERGRASS SOWED AS SOLE CROP IN SUMMER(both for sown as sole crop or as lay-out w- covercrop - follow up after other crop managements w. clover grass lay out - ex. cereal and legume)
*/

/** Below is the list of things that a farmer can do if he is growing CloverGrassGrazed2, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_c2_start = 1, // Compulsory, must always be 1 (one).
	dk_c2_sleep_all_day = DK_C2_BASE,
	dk_c2_harvest,
	dk_c2_sow_spot,
	dk_c2_cutting1,
	dk_c2_cutting_graze,
	dk_c2_cutting2,
	dk_c2_cutting3,
	dk_c2_cutting4,
	dk_c2_grazing,
	dk_c2_cattle_out,
	dk_c2_herbicide2,
	dk_c2_water,
	dk_c2_insecticide,
	dk_c2_swathing,
	dk_c2_grass_collected,
	dk_c2_wait,
	dk_c2_foobar,

} DK_CloverGrassGrazed2ToDo;


/**
\brief
DK_CloverGrassGrazed2 class
\n
*/
/**
See CloverGrassGrazed2.h::OCloverGrassGrazed2ToDo for a complete list of all possible events triggered codes by the OCloverGrassGrazed2 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_CloverGrassGrazed2 : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_CloverGrassGrazed2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation 
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(1, 12);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_c2_foobar - DK_C2_BASE);
		m_base_elements_no = DK_C2_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_c2_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_c2_sleep_all_day = DK_C2_BASE,
			 fmc_Others, // dk_c2_sow_spot,
			  fmc_Cutting, // dk_c2_cutting1,
			  fmc_Cutting, // dk_c2_cutting_graze,
			  fmc_Cutting, // dk_c2_cutting2,
			  fmc_Cutting, // dk_c2_cutting3,
			  fmc_Cutting, // dk_c2_cutting4,
			  fmc_Grazing, // dk_c2_grazing,
			  fmc_Grazing, // dk_c2_cattle_out,
			  fmc_Herbicide,	//	  dk_c2_herbicide2,
			  fmc_Watering, // dk_c2_water,
			  fmc_Cutting,	//	  dk_c2_swathing,
			  fmc_Others, // dk_c2_grass_collected.,
			  fmc_Others,	//	  dk_c2_wait,


				// no foobar entry	

		};
		// Iterate over the catlist elements and copy them to vector				
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

	}
};

#endif // DK_CloverGrassGrazed2_H