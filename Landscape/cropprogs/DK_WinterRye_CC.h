//
// DK_WinterRye_CC.h
//
/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus, updated by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following dissbaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following dissbaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INSBMUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISSBMAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INSBMUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INSBMUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DKWinterRye_CC_H
#define DKWinterRye_CC_H

#define DK_WRCC_BASE 160900

#define DK_WRCC_MN_S m_field->m_user[0]
#define DK_WRCC_MN_P m_field->m_user[1]
#define DK_WRCC_TILL_1 m_field->m_user[2]
#define DK_WRCC_TILL_2 m_field->m_user[3]

typedef enum {
	dk_wrcc_start = 1, // Compulsory, start event must always be 1 (one).
	dk_wrcc_harvest = DK_WRCC_BASE,
	dk_wrcc_autumn_plough,
	dk_wrcc_stubble_harrow,
	dk_wrcc_autumn_roll,
	dk_wrcc_remove_straw,
	dk_wrcc_ferti_s1,
	dk_wrcc_ferti_p1,
	dk_wrcc_ferti_s2,
	dk_wrcc_ferti_p2,
	dk_wrcc_autumn_sow,
	dk_wrcc_herbicide1,
	dk_wrcc_ferti_s3,
	dk_wrcc_ferti_p3,
	dk_wrcc_ferti_s4,
	dk_wrcc_ferti_p4,
	dk_wrcc_ferti_s5,
	dk_wrcc_ferti_p5,
	dk_wrcc_ferti_s6,
	dk_wrcc_ferti_p6,
	dk_wrcc_ferti_s7,
	dk_wrcc_ferti_p7,
	dk_wrcc_herbicide2,
	dk_wrcc_herbicide3,
	dk_wrcc_herbicide4,
	dk_wrcc_herbicide5,
	dk_wrcc_fungicide,
	dk_wrcc_insecticide,
	dk_wrcc_gr1,
	dk_wrcc_gr2,
	dk_wrcc_wait,
	dk_wrcc_foobar,
} DK_WinterRye_CCToDo;



class DK_WinterRye_CC : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_WinterRye_CC(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(20, 10);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_wrcc_foobar - DK_WRCC_BASE);
		m_base_elements_no = DK_WRCC_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			  fmc_Others,	// zero element unused but must be here	
			  fmc_Others,	//	  dk_wrcc_start = 1, // Compulsory, start event must always be 1 (one).
			  fmc_Harvest,	//	  dk_wrcc_harvest = DK_wrcc_BASE,
			  fmc_Cultivation,	//	  dk_wrcc_autumn_plough,
			  fmc_Cultivation,	//	  dk_wrcc_stubble_harrow,
			  fmc_Others,	//	  dk_wrcc_autumn_roll,
			  fmc_Others, // dk_wrcc_remove_straw,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_s1,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_p1,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_s2,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_p2,
			  fmc_Cultivation,	//	  dk_wrcc_autumn_sow,
			  fmc_Herbicide,	//	  dk_wrcc_herbicide1,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_s3,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_p3,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_s4,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_p4,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_s5,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_p5,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_s6,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_p6,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_s7,
			  fmc_Fertilizer,	//	  dk_wrcc_ferti_p7,
			  fmc_Herbicide,	//	  dk_wrcc_herbicide2,
			  fmc_Herbicide,	//	  dk_wrcc_herbicide3,
			  fmc_Herbicide,	//	  dk_wrcc_herbicide4,
			  fmc_Herbicide,	//	  dk_wrcc_herbicide5,
			  fmc_Fungicide,	//	  dk_wrcc_fungicide,
			  fmc_Insecticide,	//	  dk_wrcc_insecticide,
			  fmc_Others, //		dk_wrcc_gr1,
		      fmc_Others, //		dk_wrcc_gr2,
			  fmc_Others, //		dk_wrcc_wait,

				 // no foobar entry			

		};
		// Iterate over the catlist elements and copy them to vector						
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};


#endif // DK_WinterRye_CC_H