//
// PermanentGrassGrazed.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef PermanentGrassGrazed_H
#define PermanentGrassGrazed_H

#define PGG_BASE 5200
#define PGG_CUT_DATE  m_field->m_user[0]
#define PGG_FERTI_DATE  m_field->m_user[1]

typedef enum {
  pgg_start = 1, // Compulsory, start event must always be 1 (one).
  pgg_cut_to_hay = PGG_BASE,
  pgg_cattle_out1,
  pgg_cattle_out2,
  pgg_cattle_is_out,
  pgg_cut_weeds,
  pgg_herbicide,
  pgg_ferti_s,
  pgg_ferti_p,
  pgg_raking1,
  pgg_raking2,
  pgg_compress_straw,
  pgg_wait,
  pgg_foobar
} PermanentGrassGrazedToDo;



class PermanentGrassGrazed: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  PermanentGrassGrazed(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
        m_first_date=g_date->DayInYear(15,4);
		SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (pgg_foobar - PGG_BASE);
	  m_base_elements_no = PGG_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
		  fmc_Others, // zero element unused but must be here
		  fmc_Others, //pgg_start = 1, // Compulsory, start event must always be 1 (one).
		  fmc_Cutting, //pgg_cut_to_hay = PGG_BASE,
		  fmc_Grazing, //pgg_cattle_out1,
		  fmc_Grazing, //pgg_cattle_out2,
		  fmc_Grazing, //pgg_cattle_is_out,
		  fmc_Cutting, //pgg_cut_weeds,
		  fmc_Herbicide, //pgg_herbicide,
		  fmc_Fertilizer, //pgg_ferti_s,
		  fmc_Fertilizer, //pgg_ferti_p,
		  fmc_Others, //pgg_raking1,
		  fmc_Others, //pgg_raking2,
		  fmc_Others, //pgg_compress_straw,
		  fmc_Others //pgg_wait,
		  // No foobar entry
	  };
	  // Iterate over the catlist elements and copy them to vector
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
  }
};

#endif // PermanentGrassGrazed_H
