/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University, modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OSpringFodderGrass.cpp This file contains the source for the DK_OSpringFodderGrass class</B> \n
*/
/**
\file
 by Chris J. Topping, modified by Luna Kondrup Marcussen \n
 Version of July 2021 \n
 \n
*/
//
// DK_OSpringFodderGrass.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OSpringFodderGrass.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
CfgBool cfg_DK_OSpringFodderGrass_SkScrapes("DK_CROP_osfg_SK_SCRAPES", CFG_CUSTOM, false);
extern CfgBool cfg_pest_DK_OSpringFodderGrass_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_osfg_InsecticideDay;
extern CfgInt   cfg_osfg_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional spring barley Fodder.
*/
bool DK_OSpringFodderGrass::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	m_field = a_field; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_farm = a_farm; // this is needed because of possible calls to other methods and currently we do not pass parameters.
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKOSpringFodderGrass; // The current type - change to match the crop you have
	// Depending what event has occured jump to the correct bit of code e.g. for ww_start jump to line 67 below
	switch (a_ev->m_todo)
	{
	case dk_osfg_start:
	{
		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(10, 10); // Should match the last flexdate below
	//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(4 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
		// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 8); // last possible day of water in this case 
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(30, 9); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) - cutting
		flexdates[2][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 2 (start op 2)
		flexdates[2][1] = g_date->DayInYear(10, 10); // This date will be moved back as far as necessary and potentially to flexdates 2 (end op 2) - cutting
		flexdates[3][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
		flexdates[3][1] = g_date->DayInYear(10, 10); // This date will be moved back as far as necessary and potentially to flexdates 3 (end op 3) - cattle out
		flexdates[4][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 3 (start op 3)
		flexdates[4][1] = g_date->DayInYear(10, 10); // This date will be moved back as far as necessary and potentially to flexdates 4 (end op 4) - cattle is out
	

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(dk_osfg_harrow))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		// Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
		SimpleEvent(d1, dk_osfg_harrow, false);
		break;
	}
	break;

	case dk_osfg_harrow:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_harrow, true);
				break;
			}
		}
		//here comes next events:
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date(), dk_osfg_manure_s, false);
			break;
		}
		else SimpleEvent(g_date->Date(), dk_osfg_manure_p, false);
		break;

	case dk_osfg_manure_s:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->FA_Manure(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_manure_s, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_osfg_ferti_sk_s, false);
		break;

	case dk_osfg_manure_p:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->FP_Manure(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_manure_p, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_osfg_ferti_sk_p, false);
		break;

	case dk_osfg_ferti_sk_s:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->FA_SK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_ferti_sk_s, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_osfg_spring_plough, false);
		break;

	case dk_osfg_ferti_sk_p:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->FP_SK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_ferti_sk_p, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_osfg_spring_plough, false);
		break;

	case dk_osfg_spring_plough:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_spring_plough, true);
				break;
			}
		}
		SimpleEvent(g_date->Date(), dk_osfg_sow, false);
		break;
	case dk_osfg_sow:
		if (a_ev->m_lock || a_farm->DoIt(100)) {
			if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_sow, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_osfg_water, false);
		break;
	case dk_osfg_water:
		if (a_ev->m_lock || a_farm->DoIt(50)) {
			if (!a_farm->Water(a_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_water, true);
				break;
			}
		}
		SimpleEvent(g_date->Date()+1, dk_osfg_cutting1, false);
		break;
	case dk_osfg_cutting1: // 
		if (!a_farm->CutToSilage(a_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_osfg_cutting1, true);
			break;
		}
		SimpleEvent(g_date->Date() + 35, dk_osfg_cutting2, false);
		break;
	case dk_osfg_cutting2: //50% of the 90% with lay-out (45% in total) will do a second cut, 50% do grazing
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50)) {
			if (!m_farm->CutToSilage(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_osfg_cutting2, true);
				break;
			}
			// End of management w lay-out and cutting grass
			done = true;
			break;
		}
		else SimpleEvent(g_date->Date() + 35, dk_osfg_grazing, false);
			break;
	case dk_osfg_grazing:
		if (!m_farm->CattleOut(m_field, 0.0, m_field->GetMDates(1, 3) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_osfg_grazing, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_osfg_cattle_is_out, false);
		break;
	case dk_osfg_cattle_is_out:    // Keep the cattle out there
								 // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, m_field->GetMDates(1, 4) - g_date->DayInYear(), m_field->GetMDates(1, 4))) {
			SimpleEvent(g_date->Date() + 1, dk_osfg_cattle_is_out, false);
			break;
		}
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop
		// END OF MAIN THREAD
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "DK_OSpringFodderGrass::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}