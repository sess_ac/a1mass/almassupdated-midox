//
// DK_OSpringBarleySilage.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OSpringBarleySilage.h"


bool DK_OSpringBarleySilage::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  TTypesOfVegetation l_tov = tov_DKOSpringBarleySilage; // The current type - change to match the crop you have

  switch ( m_ev->m_todo ) {
  case dk_osbs_start:
    {
      a_field->ClearManagementActionSum();
      DK_OSBS_SOLE_CROP = false;
      DK_OSBS_FORCESPRING = false;
      m_last_date = g_date->DayInYear(16, 8); // Should match the last flexdate below
          //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
      // Set up the date management stuff
              // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(16, 8); // last possible day 
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(16, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 0;
      if (StartUpCrop(isSpring, flexdates, int(dk_osbs_spring_harrow1))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(1, 10);
      // OK, let's go.
      // Here we queue up the first event which changes dependent on whether it is a forced spring sow or not
      if (m_ev->m_forcespring) {
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_osbs_spring_harrow1, false);
          DK_OSBS_FORCESPRING = true;
          break;
      }
      else SimpleEvent(d1, dk_osbs_autumn_plough, false);
      break;
  }
    break;
 
  case dk_osbs_autumn_plough:
      if (m_field->GetSoilType() != tos_Sand && m_field->GetSoilType() != tos_LoamySand && m_field->GetSoilType() != tos_SandyLoam && m_field->GetSoilType() != tos_SandyClayLoam) // on clay soils (NL KLEI & VEEN)
      {
          if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
              if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(1, 12) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_osbs_autumn_plough, true);
                  break;
              }
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2)+365, dk_osbs_spring_harrow1, false);
      break;
 
  case dk_osbs_spring_harrow1:
      if (m_field->GetSoilType() == tos_Sand || m_field->GetSoilType() == tos_LoamySand || m_field->GetSoilType() == tos_SandyLoam || m_field->GetSoilType() == tos_SandyClayLoam) // on sandy soils (NL ZAND & LOSS)
      {
          if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
              if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(25, 3) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_osbs_spring_harrow1, true);
                  break;
              }
          }
          SimpleEvent(g_date->Date()+1, dk_osbs_spring_harrow2, false);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2), dk_osbs_slurry_s, false);
      break;

  case dk_osbs_spring_harrow2:
      if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
          if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(27, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_spring_harrow2, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2), dk_osbs_slurry_s, false);
      break;

  case dk_osbs_slurry_s:
      if (a_farm->IsStockFarmer()) {
          if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
              if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_osbs_slurry_s, true);
                  break;
              }
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_osbs_spring_plough, false);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2), dk_osbs_slurry_p, false);
      break;

  case dk_osbs_slurry_p:
      if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) { 
          if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_slurry_p, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 3), dk_osbs_spring_plough, false);
      break;

  case dk_osbs_spring_plough:
      if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
          if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(28, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_spring_plough, true);
              break;
          }
      }
      SimpleEvent(g_date->Date()+1, dk_osbs_spring_sow, false);
      break;

  case dk_osbs_spring_sow: // 50% will combine sow (with lay-out/catch crop) 40% will split sow (with lay-out/catch crop), 10% will sow w/o lay-out/catch crop
      if (a_ev->m_lock || a_farm->DoIt_prob(1.0)) {
          if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_spring_sow, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_osbs_sow_lo, false); // sow lay-out/catch crop by itself (40%)
      break;

  case dk_osbs_sow_lo:
      if (a_ev->m_lock || a_farm->DoIt_prob(0.4)) {
          if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_sow_lo, true);
              break;
          }
          SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_osbs_water, false);
          break;
      }
      else SimpleEvent(g_date->Date() + 3, dk_osbs_harrow1, false); // 10% w/o lay-out/catch crop
      break;

  case dk_osbs_harrow1:
      if (a_ev->m_lock || a_farm->DoIt_prob(0.1/0.6)) { // 10% of the 60% that do one sow only
          if (!a_farm->ShallowHarrow(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_harrow1, true);
              break;
          }
          DK_OSBS_SOLE_CROP = true; // we need to remember the 10% who only sow spring barley
          SimpleEvent(g_date->Date() +1, dk_osbs_harrow2, false);
          break;
      }
      else SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_osbs_water, false);
      break;

  case dk_osbs_harrow2:
      if (!a_farm->ShallowHarrow(a_field, 0.0, g_date->DayInYear(6, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbs_harrow2, true);
          break;
      }
      SimpleEvent(g_date->Date()+1, dk_osbs_roll, false);
      break;

  case dk_osbs_roll:
      if (!a_farm->SpringRoll(a_field, 0.0, g_date->DayInYear(7, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbs_roll, true);
          break;
      }
      SimpleEvent(g_date->Date() + 14, dk_osbs_weeding1, false);
      break;

  case dk_osbs_weeding1:
      if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(22, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbs_weeding1, true);
          break;
      }
      SimpleEvent(g_date->Date() + 7, dk_osbs_weeding2, false);
      break;

  case dk_osbs_weeding2:
      if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbs_weeding2, true);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7), dk_osbs_water, false);
      break;

  case dk_osbs_water:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
          if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_water, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(8, 8), dk_osbs_swathing, false);
      break;

  case dk_osbs_swathing:
      if (a_ev->m_lock || a_farm->DoIt_prob(0.1)) { // only if uneven maturing
          if (!a_farm->Swathing(a_field, 0.0, g_date->DayInYear(14, 8) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_swathing, true);
              break;
          }
          SimpleEvent(g_date->Date()+1, dk_osbs_harvest, false);
          break;
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(10, 8), dk_osbs_harvest, false);
      break;

  case dk_osbs_harvest:
      if (!a_farm->Harvest(a_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbs_harvest, true);
          break;
      }
      SimpleEvent(g_date->Date()+1, dk_osbs_straw_chopping, false);
      break;

  case dk_osbs_straw_chopping:
      if (DK_OSBS_SOLE_CROP == true){
          if (!a_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_osbs_straw_chopping, true);
              break;
          }
          done = true;
          break;
      }
      else SimpleEvent(g_date->Date() + 1, dk_osbs_hay_bailing, false);
      break;

  case dk_osbs_hay_bailing:
      if (!a_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_osbs_hay_bailing, true);
          break;
      }

    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "DK_OSpringBarleySilage::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


