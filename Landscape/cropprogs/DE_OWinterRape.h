//
// DE_OWinterRape.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DE_OWINTERRAPE_H
#define DE_OWINTERRAPE_H

// <cropname>_BASE is the first event number to be dumped into the
// debugging log from this crop. *Must* be unique among all crops.
// I suggest steps of 100 between crops.

#define DE_OWINTERRAPE_BASE 38000

typedef enum {
  de_owr_start = 1, // Compulsory, start event must always be 1 (one).
  de_owr_fa_sleep_all_day = DE_OWINTERRAPE_BASE,
  de_owr_fa_manure,
  de_owr_fp_manure,
  de_owr_autumn_plough,
  de_owr_autumn_harrow,
  de_owr_autumn_sow,
  de_owr_row_cultivation_one,
  de_owr_row_cultivation_two,
  de_owr_fertiFP_S,
  de_owr_fertiFA_S,
  de_owr_fertiFP_B,
  de_owr_fertiFA_B,
  de_owr_row_cultivation_three,
  de_owr_harvest,
  de_owr_straw_chop,
  de_owr_stub_harrow,
  de_owr_foobar,
} DE_OWinterRapeToDo;



class DE_OWinterRape: public Crop
{
  // Private methods corresponding to the different management steps
  // (when needed).

public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DE_OWinterRape(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(25, 8);
	SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (de_owr_foobar - DE_OWINTERRAPE_BASE);
	  m_base_elements_no = DE_OWINTERRAPE_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  de_owr_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Others,	//	  de_owr_fa_sleep_all_day = DE_OWINTERRAPE_BASE,
			fmc_Fertilizer,	//	  de_owr_fa_manure,
			fmc_Fertilizer,	//	  de_owr_fp_manure,
			fmc_Cultivation,	//	  de_owr_autumn_plough,
			fmc_Cultivation,	//	  de_owr_autumn_harrow,
			fmc_Others,	//	  de_owr_autumn_sow,
			fmc_Cultivation,	//	  de_owr_row_cultivation_one,
			fmc_Cultivation,	//	  de_owr_row_cultivation_two,
			fmc_Fertilizer,	//	  de_owr_fertiFP_S,
			fmc_Fertilizer,	//	  de_owr_fertiFA_S,
			fmc_Fertilizer,	//	  de_owr_fertiFP_B,
			fmc_Fertilizer,	//	  de_owr_fertiFA_B,
			fmc_Cultivation,	//	  de_owr_row_cultivation_three,
			fmc_Harvest,	//	  de_owr_harvest,
			fmc_Cutting,	//	  de_owr_straw_chop,
			fmc_Cultivation,	//	  de_owr_stub_harrow,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // DE_OWINTERRAPE_H
