//
// DK_OWinterBarley.h
//
/*

Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef DK_OWinterBarley_H
#define DK_OWinterBarley_H

#define DK_OWB_BASE 160200

typedef enum {
  dk_owb_start = 1, // Compulsory, start event must always be 1 (one).
  dk_owb_autumn_harrow1 = DK_OWB_BASE,
  dk_owb_autumn_harrow2,
  dk_owb_autumn_plough,
  dk_owb_autumn_sow,
  dk_owb_slurry_s1,
  dk_owb_slurry_p1,
  dk_owb_slurry_s2,
  dk_owb_slurry_p2,
  dk_owb_water,
  dk_owb_harvest,
  dk_owb_hay_bailing,
  dk_owb_straw_chopping,
  dk_owb_foobar
} DK_OWinterBarleyToDo;



class DK_OWinterBarley : public Crop
{
public:
	bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	DK_OWinterBarley(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		m_first_date = g_date->DayInYear(28, 9);
		SetUpFarmCategoryInformation();
	}
	void SetUpFarmCategoryInformation() {
		const int elements = 2 + (dk_owb_foobar - DK_OWB_BASE);
		m_base_elements_no = DK_OWB_BASE - 2;

		FarmManagementCategory catlist[elements] =
		{
			fmc_Others, // zero element unused but must be here
			fmc_Cultivation, // dk_owb_autumn_harrow1 = 1, // Compulsory, start event must always be 1 (one).
			fmc_Cultivation, // dk_owb_autumn_harrow2 = DK_OWR_BASE,
			fmc_Cultivation, // dk_owb_autumn_plough,
			fmc_Others, // dk_owb_autumn_sow,
			fmc_Fertilizer, // dk_owb_slurry_s1,
			fmc_Fertilizer, //   dk_owb_slurry_p1,
			fmc_Fertilizer, //   dk_owb_slurry_s2,
			fmc_Fertilizer, //   dk_owb_slurry_p2,
			fmc_Watering, //   dk_owb_water,
			fmc_Harvest, //   dk_owb_harvest,
			fmc_Others, //  dk_owb_hay_bailing,
			fmc_Cutting, //   dk_owb_straw_chopping,
						// No foobar entry
		};
		// Iterate over the catlist elements and copy them to vector
		copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
	}
};

#endif // DK_OWinterBarley_H