/**
\file
\brief
<B>PTTriticale.h This file contains the headers for the Triticale class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PTTriticale.h
//


#ifndef PTTRITICALE_H
#define PTTRITICALE_H

#define PTTRITICALE_BASE 30800
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing triticale, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pt_tri_start = 1, // Compulsory, must always be 1 (one).
	pt_tri_sleep_all_day = PTTRITICALE_BASE,
	pt_tri_stubble_harrow,
	pt_tri_ferti_p1, // slurry
	pt_tri_ferti_s1,
	pt_tri_ferti_p2, // NPK
	pt_tri_ferti_s2,
	pt_tri_ferti_p3, // Calcium
	pt_tri_ferti_s3,
	pt_tri_ferti_p4, // NPK
	pt_tri_ferti_s4,
	pt_tri_autumn_harrow,
	pt_tri_autumn_sow,
	pt_tri_preseeding_cultivator_sow,
	pt_tri_harvest1,
	pt_tri_wait,
	pt_tri_harvest2,

} PTTriticaleToDo;


/**
\brief
PTTriticale class
\n
*/
/**
See PTTriticale.h::PTTriticaleToDo for a complete list of all possible events triggered codes by the triticale management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/

class PTTriticale : public Crop
{
public:
	virtual bool  Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev);
	PTTriticale(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
	{
		// When we start it off, the first possible date for a farm operation is 10th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(15, 11);// AAS:Last possible date to do the first operation, i.e. plough or sow
	}
};

#endif // PTTRITICALE_H

