/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.
 
Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>SetAside.cpp This file contains the source for the SetAside class</B> \n
*/
/**
\file
by Chris J. Topping \n
Version of June 2003 \n
\n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// SetAside.cpp
//
/*
 
Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)
 
All rights reserved.
 
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 
*/
//#include "stdafx.h"
 
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SetAside.h"
 
 
/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case rotational setaside.
*/
bool SetAside::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm      = a_farm;
  m_field     = a_field;
  m_ev        = a_ev;
  int d1      = 0;
  int noDates = 3;
  bool done   = false;
 
  switch ( m_ev->m_todo ) {
  case sa_start:
      a_field->ClearManagementActionSum();

    // Special for set-aside:
    m_field->SetVegPatchy( true );
    // Set up the date management stuff
    // Could save the start day in case it is needed later
    // m_field->m_startday = m_ev->m_startday;
    m_last_date=g_date->DayInYear(10,10);
    // Start and stop dates for all events after harvest
    m_field->SetMDates(0,0,g_date->DayInYear(30,6));
    // Determined by harvest date - used to see if at all possible
    m_field->SetMDates(1,0,g_date->DayInYear(25,8));
    m_field->SetMDates(0,1,g_date->DayInYear(1,9));
    m_field->SetMDates(1,1,g_date->DayInYear(15,9));
    m_field->SetMDates(0,2,g_date->DayInYear(1,9));
    m_field->SetMDates(1,2,g_date->DayInYear(10,10));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

    if (m_ev->m_startday>g_date->DayInYear(1,7)) {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday) {
        g_msg->Warn( WARN_BUG, "Setaside::Do(): "
                   "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
    }
 
    // Now no operations can be timed after the start of the next crop.
    if ( ! m_ev->m_first_year ) {
      // Are we before July 1st?
      d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
      if (g_date->Date() < d1) {
       // Yes, too early. We assumme this is because the last crop was late
       g_msg->Warn( WARN_BUG, "Setaside::Do(): "
                   "Crop start attempt between 1st Jan & 1st July", "" );
       exit( 1 );
      } else {
       d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
       if (g_date->Date() > d1) {
         // Yes too late - should not happen - raise an error
         g_msg->Warn( WARN_BUG, "SetAside::Do(): "
                     "Crop start attempt after last possible start date",
                     "" );
         exit( 1 );
       }
      }
    }
	}//if

    // New begin.
    d1 = g_date->OldDays() + m_first_date;
 
    if ( ! m_ev->m_first_year ) {
       d1 +=365 ;
    }
    if ( g_date->Date() > d1 ) {
      d1 = g_date->Date();
    }

    // ***CJT*** altered 16 August 2004 to be more similar to non-managed set-aside
    //      SimpleEvent( d1, sa_wait, false );
    //      break;
    
       SimpleEvent( d1, sa_cut_to_hay, false );
    break;
    
  case sa_cut_to_hay:
    if (!m_farm->CutToHay( m_field, 0.0, g_date->DayInYear( 30, 7 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sa_cut_to_hay, false );
      break;
    }
    if ( !m_farm->IsStockFarmer())
    {

		ChooseNextCrop (3);

      SimpleEvent( g_date->Date() + 1, sa_wait, false );
      break;
    }

    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
                  sa_cattle_out, false );
    break;
 
  case sa_cattle_out:
    if ( m_ev->m_lock || m_farm->DoIt( 15 )) {
		if (m_field->GetMConstants(1)==0) {
			if (!m_farm->CattleOut( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "Setaside::Do(): failure in 'CattleOut' execution", "" );
				exit( 1 );
			} 
		}
		else {  
			if (!m_farm->CattleOut( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, sa_cattle_out, true );
				break;
			}
		}
		SimpleEvent( g_date->Date() + m_field->GetMConstants(2), sa_cattle_is_out, false );
		break;
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1), sa_cut_to_silage, false );
    break;
 
  case sa_cattle_is_out:
    if (m_field->GetMConstants(2)==0) {
		if (!m_farm->CattleIsOut( m_field, 0.0, -1, m_field->GetMDates(1,2))) { //raise an error
			//added 28.08 - issue a warning only if we fail on the last day that this can be done, i.e. MDate
			if(g_date->Date() == m_field->GetMDates(1,2)){
				g_msg->Warn( WARN_BUG, "Setaside::Do(): failure in 'CattleIsOut' execution", "" );
				exit( 1 );
			}
		} 
	}
	else { 
		if (!m_farm->CattleIsOut( m_field, 0.0, m_field->GetMDates(1,2) - g_date->DayInYear(), m_field->GetMDates(1,2))) {
			SimpleEvent( g_date->Date() + 1, sa_cattle_is_out, false );
			break;
		}
	}
	ChooseNextCrop (3);

    SimpleEvent( g_date->Date() + 1, sa_wait, false );
    break;
 
  case sa_cut_to_silage:
    if ( m_ev->m_lock || m_farm->DoIt( 15 )) {
		if (m_field->GetMConstants(0)==0) {
			if (!m_farm->CutToSilage( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "Setaside::Do(): failure in 'CutToSilage' execution", "" );
				exit( 1 );
			} 
		}
		else {
		  if (!m_farm->CutToSilage( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, sa_cut_to_silage, true );
			break;
		  }
		}
	}

	ChooseNextCrop (3);

    SimpleEvent( g_date->Date() + 1, sa_wait, false );
    break;
 
  case sa_wait:
    // Cannot terminate too early otherwise will cause a rotation loop
 
    if ((g_date->DayInYear()-m_field->GetMDates(1,2))>=0)
    {
         //m_farm->Glyphosate(m_field,0.0,0);
      // Special for set-aside:
      m_field->SetVegPatchy( false );
      done = true;
    }
    else // queue this up again
    SimpleEvent( g_date->Date() + 1, sa_wait, true );
    break;
 
  default:
    g_msg->Warn( WARN_BUG, "SetAside::Do(): "
              "Unknown event type! ", "" );
    exit( 1 );
  }
 
  return done;
}





