//
// FI_OStarchPotato_North.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FI_OStarchPotato_North_h
#define FI_OStarchPotato_North_h

#define FI_OSPN_BASE 70500

#define FI_OSPN_DECIDE_TO_HERB		m_field->m_user[5]
#define FI_OSPN_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  fi_ospn_start = 1, // Compulsory, start event must always be 1 (one).
  fi_ospn_harvest = FI_OSPN_BASE,
  fi_ospn_stubble_cultivator,
  fi_ospn_autumn_plough,
  fi_ospn_slurry,
  fi_ospn_spring_plough,
  fi_ospn_n_minerals1,
  fi_ospn_n_minerals2,
  fi_ospn_preseeding_cultivation,
  fi_ospn_fertilizer,
  fi_ospn_preseeding_plant,
  fi_ospn_plant,
  fi_ospn_harrow,
  fi_ospn_foobar,
} FI_OStarchPotato_NorthToDo;



class FI_OStarchPotato_North: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FI_OStarchPotato_North(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
     m_first_date=g_date->DayInYear(1,12); // 
	 SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (fi_ospn_foobar - FI_OSPN_BASE);
	  m_base_elements_no = FI_OSPN_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  fi_ospn_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  fi_ospn_harvest = FI_OSPN_BASE,
			fmc_Cultivation,	//	  fi_ospn_stubble_cultivator,
			fmc_Cultivation,	//	  fi_ospn_autumn_plough,
			fmc_Fertilizer,	//	  fi_ospn_slurry,
			fmc_Cultivation,	//	  fi_ospn_spring_plough,
			fmc_Fertilizer,	//	  fi_ospn_n_minerals1,
			fmc_Fertilizer,	//	  fi_ospn_n_minerals2,
			fmc_Cultivation,	//	  fi_ospn_preseeding_cultivation,
			fmc_Fertilizer,	//	  fi_ospn_fertilizer,
			fmc_Cultivation,	//	  fi_ospn_preseeding_plant,
			fmc_Others,	//	  fi_ospn_plant,
			fmc_Cultivation	//	  fi_ospn_harrow,


			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // FI_OStarchPotato_North_h
