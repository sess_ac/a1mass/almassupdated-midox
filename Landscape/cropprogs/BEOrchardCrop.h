//
// BEOrchardCrop.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#ifndef BEORCHARDCROP_H
#define BEORCHARDCROP_H

#define BEORCHARDCROP_BASE 27000

/**
\brief A flag used to indicate insecticide use
*/
#define BE_ORCH_INSECT	a_field->m_user[1]
#define BE_ORCH_ISECT_DAY	a_field->m_user[2]

typedef enum {
  BE_orch_start = 1, // Compulsory, start event must always be 1 (one).
  BE_orch_sleep_all_day = BEORCHARDCROP_BASE,
  BE_orch_do_nothing_stop,
  BE_orch_insecticide1,
  BE_orch_insecticide2,
  BE_orch_insecticide3,
  BE_orch_insecticide4,
  BE_orch_cut1,
  BE_orch_cut2,
  BE_orch_cut3,
  BE_orch_cut4,
} BEOrchardCropToDo;



class BEOrchardCrop: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  BEOrchardCrop(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
    m_first_date=g_date->DayInYear(1,4);
  }
};

#endif // BEORCHARDCROP_H
