//
// DK_OWinterRye_CC.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2022, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OWinterRye_CC.h"
extern CfgFloat cfg_DKCatchCropPct;

bool DK_OWinterRye_CC::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  TTypesOfVegetation l_tov = tov_DKOWinterRye_CC;
  int l_nextcropstartdate;

  switch ( m_ev->m_todo ) {
  case dk_owrcc_start:
    {
      a_field->ClearManagementActionSum();

      m_last_date = g_date->DayInYear(11, 8); // Should match the last flexdate below
     //Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
      std::vector<std::vector<int>> flexdates(1 + 2, std::vector<int>(2, 0));
      // Set up the date management stuff
              // Start and stop dates for all events after harvest
      flexdates[0][1] = g_date->DayInYear(10, 8); // last possible day 
      // Now these are done in pairs, start & end for each operation. If its not used then -1
      flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
      flexdates[1][1] = g_date->DayInYear(11, 8); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) // straw chopping
      flexdates[2][0] = -1; // 
      flexdates[2][1] = g_date->DayInYear(11, 8); // hay bailing

      // Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
      int isSpring = 0;
      if (StartUpCrop(isSpring, flexdates, int(dk_owrcc_slurry_s1))) break;

      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(15, 9) + isSpring;
      // OK, let's go.
      SimpleEvent(d1, dk_owrcc_autumn_plough, false);
  }
    break;

  case dk_owrcc_autumn_plough:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->AutumnPlough(m_field, 0.0,
              g_date->DayInYear(1, 10) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_owrcc_autumn_plough, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_owrcc_autumn_sow, false);
      break;

  case dk_owrcc_autumn_sow:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->AutumnSow(m_field, 0.0,
              g_date->DayInYear(2, 10) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_owrcc_autumn_sow, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2) + 365, dk_owrcc_slurry_s1, false);
      break;

  case dk_owrcc_slurry_s1:
      if (a_farm->IsStockFarmer()) {
          if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
              if (!m_farm->FA_Slurry(m_field, 0.0,
                  g_date->DayInYear(31, 3) - g_date->DayInYear())) {
                  SimpleEvent(g_date->Date() + 1, dk_owrcc_slurry_s1, true);
                  break;
              }
              SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_owrcc_slurry_s2, false);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 2), dk_owrcc_slurry_p1, false);
      break;

  case dk_owrcc_slurry_p1:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->FP_Slurry(m_field, 0.0,
              g_date->DayInYear(31, 3) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_owrcc_slurry_p1, true);
              break;
          }

      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_owrcc_slurry_p2, false);
      break;

  case dk_owrcc_slurry_s2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->FA_Slurry(m_field, 0.0,
              g_date->DayInYear(30, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_owrcc_slurry_s2, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_owrcc_water, false);
      break;

  case dk_owrcc_slurry_p2:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->FP_Slurry(m_field, 0.0,
              g_date->DayInYear(30, 4) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_owrcc_slurry_p2, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_owrcc_water, false);
      break;

  case dk_owrcc_water:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.5)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(30, 6) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_owrcc_water, true);
              break;
          }
      }
      SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), dk_owrcc_harvest, false);
      break;

  case dk_owrcc_harvest:
      if (m_ev->m_lock || m_farm->DoIt_prob(1.0)) {
          if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_owrcc_harvest, true);
              break;
          }
      }
      SimpleEvent(g_date->Date() + 1, dk_owrcc_straw_chopping, false);
      break;

  case dk_owrcc_straw_chopping:
      if (m_ev->m_lock || m_farm->DoIt_prob(0.75)) { // 75% do straw chopping, 25% do hay bailing
          if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
              SimpleEvent(g_date->Date() + 1, dk_owrcc_straw_chopping, true);
              break;
          }
          if (m_ev->m_lock || m_farm->DoIt_prob(cfg_DKCatchCropPct.value())) {
              SimpleEvent(g_date->Date(), dk_owrcc_wait, false);
              break;
          }
          else
              done = true;
          break;
      }
      SimpleEvent(g_date->Date(), dk_owrcc_hay_bailing, false);
      break;

  case dk_owrcc_hay_bailing:
      if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 2) - g_date->DayInYear())) {
          SimpleEvent(g_date->Date() + 1, dk_owrcc_hay_bailing, true);
          break;
      }
      if (m_ev->m_lock || m_farm->DoIt_prob(cfg_DKCatchCropPct.value())) {
          SimpleEvent(g_date->Date(), dk_owrcc_wait, false);
          break;
      }
      else
          done = true;
      break;

  case dk_owrcc_wait:
      //set to 100% from this code - in reality ~ 42% of all spring barley
      // So we are done,but this crop uses a catch crop
      l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
      m_field->BumpRunNum();
      m_farm->AddNewEvent(tov_DKOCatchCrop, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false, false);
      m_field->SetVegType(tov_DKOCatchCrop, tov_Undefined); //  Two vegetation curves are specified 
      if (m_field->GetUnsprayedMarginPolyRef() != -1)
      {
          LE* um = m_OurLandscape->SupplyLEPointer(m_field->GetUnsprayedMarginPolyRef());
          um->SetVegType(tov_DKOCatchCrop, tov_Undefined);
      }
      // NB no "done = true" because this crop effectively continues into the catch crop.
      break;

  default:
    g_msg->Warn( WARN_BUG, "DK_OWinterRye_CC::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


