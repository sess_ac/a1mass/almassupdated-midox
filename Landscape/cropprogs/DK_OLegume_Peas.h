//
// OLegume_Peas.h
//
/* 
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, University of Aarhus - modified by Luna Kondrup Marcussen, University of Aarhus
All rights reserved. 

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DK_OLegume_Peas_H
#define DK_OLegume_Peas_H
/** \brief A flag used to indicate do catch crop
*/
#define DK_OLP_BASE 60100

typedef enum {
  dk_olp_start = 1, // Compulsory, start event must always be 1 (one).
  dk_olp_harvest = DK_OLP_BASE,
  dk_olp_spring_harrow1,
  dk_olp_spring_harrow2,
  dk_olp_spring_plough,
  dk_olp_ks_ferti_s,
  dk_olp_ks_ferti_p,
  dk_olp_spring_harrow3,
  dk_olp_spring_row_sow,
  dk_olp_strigling1,
  dk_olp_rowcultivation1,
  dk_olp_strigling2,
  dk_olp_rowcultivation2,
  dk_olp_water,
  dk_olp_wait,
  dk_olp_wait1,
  dk_olp_foobar,
} DK_OLegume_PeasToDo;



class DK_OLegume_Peas: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  DK_OLegume_Peas(TTypesOfVegetation a_tov, TTypesOfCrops a_toc, Landscape* a_L) : Crop(a_tov, a_toc, a_L)
  {
      m_first_date=g_date->DayInYear(10,4);
	  SetUpFarmCategoryInformation();
  }
  void SetUpFarmCategoryInformation() {
	  const int elements = 2 + (dk_olp_foobar - DK_OLP_BASE);
	  m_base_elements_no = DK_OLP_BASE - 2;

	  FarmManagementCategory catlist[elements] =
	  {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	  dk_olp_start = 1, // Compulsory, start event must always be 1 (one).
			fmc_Harvest,	//	  dk_olp_harvest = DK_olp_BASE,
			fmc_Cultivation,	//	  dk_olp_spring_harrow1,
			fmc_Cultivation,	//	  dk_olp_spring_harrow2,
			fmc_Cultivation,	//	  dk_olp_spring_plough,
			fmc_Fertilizer,	//	  dk_olp_ks_ferti,
			fmc_Fertilizer,	//	  dk_olp_ks_ferti,
			fmc_Cultivation,	//	  dk_olp_spring_harrow3,
			fmc_Others,	//	  dk_olp_spring_row_sow,
			fmc_Cultivation,	//	  dk_olp_strigling,
			fmc_Cultivation,	//	  dk_olp_rowcultivation,
			fmc_Cultivation,	//	  dk_olp_strigling,
			fmc_Cultivation,	//	  dk_olp_rowcultivation,
			fmc_Watering,	//	  dk_olp_water,
			fmc_Others, // dk_olp_wait
			fmc_Others, // dk_olp_wait

			  // no foobar entry	

	  };
	  // Iterate over the catlist elements and copy them to vector				
	  copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));

  }
};

#endif // OLegume_Peas_H
