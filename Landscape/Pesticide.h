//
// pesticide.h
//
/*
*******************************************************************************************************
Copyright (c) 2003, Christopher John Topping, EcoSol
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef PESTICIDE_H
#define PESTICIDE_H

// Side length of a square in the pesticide map is given by this parameter
// as a power of two. Ie. 3 will give a side length of 8 main map units
// as 2^3 equals 8, which must also be set below
//
// Unfortunately it would be a huge performance penalty to have to make
// this a runtime configuration variable, it has to be compiled in.
// Standard rough settings here:
/*
#define PEST_GRIDSIZE_POW2 0
#define PEST_GRIDSIZE      1
#define PEST_GRIDAREA      1  // i.e. PEST_GRIDSIZE^2

#define PEST_GRIDSIZE_POW2 1
#define PEST_GRIDSIZE      2
#define PEST_GRIDAREA      4  // i.e. PEST_GRIDSIZE^2

#define PEST_GRIDSIZE_POW2 2
#define PEST_GRIDSIZE      4
#define PEST_GRIDAREA      16  // i.e. PEST_GRIDSIZE^2
*/
#define PEST_GRIDSIZE_POW2 0
#define PEST_GRIDSIZE      1
#define PEST_GRIDAREA      1  // i.e. PEST_GRIDSIZE^2

// Consider undef for production code, no speed penalty if defined.
#define PEST_DEBUG

// Check for water bodies when asking for pesticide concentration
// at main map coords x,y? Speed vs. reality tradeoff. You
// probably want this defined.
//#define PEST_WATER_CHECK

extern class Pesticide *g_pest;

struct PesticideEvent
{
	/**
	* Records the amount sprayed per unit area of a polygon. Each cell in the polygon is assumed to receive this amount (effectively the field rate per unit area).
	*/
	LE*   m_sprayed_elem;
	double m_amount;
	PlantProtectionProducts m_ppp;

	PesticideEvent(LE* a_sprayed_elem, double a_amount, PlantProtectionProducts a_ppp) {
		m_sprayed_elem = a_sprayed_elem;
		m_amount = a_amount;
		m_ppp = a_ppp;
	}
};

class Pesticide
{
	/**
	* \brief Main class for pesticide engine.
	*/
// Method call stack and ordering:
//
// Pesticide()
//   CalcRainWashOffFactors()
//   DiffusionVectorInit()
//     DiffusionFunction()
//   DiffusionVectorTest()
//
// Tick()
//   MainMapDecay()
//     DecayMap()
//   DailyQueueProcess()
//     TwinMapClear()
//     TwinMapSpray()
//       TwinMapSprayPixel()
//       TwinMapSprayCorrectBorders()
//     TwinMapDiffusion()
//       DiffusionSprayPixel()
//   DailyQueueClear()
//   SeedCoatingMapDecay()
//   RecordPesticideLoad()
//   RecordPesticideMap()
//   TransferPesticide()
//     TransferToOne()
//     TransferToTwo()
//
// SupplyPesticide()
// #ifdef PEST_WATER_CHECK
//   ElementIsWater()
// #endif

protected:
	/** \brief 
	* Speed hack. Only actually run the daily decay routine on the
	* pesticide map if and only if we are sure that there *is*
	* some pesticide anywhere on the map for us to process.
	* 
	* Cleared daily by MainMapDecay().
	* 
	* Set to true when either adding pesticide to the map, or by the
	* MainMapDecay() method itself, when it finds remains of pesticide
	* anywhere on the pesticide map during its run.
	* One copy is needed for each active PPP
	*/
	bool m_something_to_decay[ppp_foobar];
	/** \brief Structure to keep track of whether there is any seed coating to decay or transfer */
	bool m_seedcoating_to_decay[ppp_foobar];

	/** \brief The number of active PPPs to track */
	int m_NoPPPs;

	/** Main map x-coordinate beyond which we have to use special proportional constants when adding new amounts of pesticide to the pesticide map to maintain a mass balance. */
	int   m_x_excess;
	/** Main map y-coordinate beyond which we have to use special proportional constants when adding new amounts of pesticide to the pesticide map to maintain a mass balance. */
	int   m_y_excess;
	/** \brief Constant related to the grid area (i.e. how many landscape map squares are in one grid square).*/
	double m_prop;
	/** \brief Corrected proportions for the edge squares along the x-axis. */
	double m_corr_x;
	/** \brief Corrected proportions for the edge squares along the y-axis. */
	double m_corr_y;
	/** \brief The daily wind direction */
	int m_wind;

#ifdef __DETAILED_PESTICIDE_FATE
	/** \brief Daily decay fraction, precalculated from the pesticide environmental half life parameter, for detailed pesticide fate, plant compartment. */
	vector<double> m_pest_daily_decay_frac_vegcanopy;
	/** \brief Daily decay fraction, precalculated from the pesticide environmental half life parameter, for detailed pesticide fate, soil compartment. */
	vector<double> m_pest_daily_decay_frac_soil;
#ifdef __FLOWER_PESTICIDE
	/** \brief Daily decay fraction, precalculated from the pesticide environmental half life parameter, for detailed flower model, inside the plant */
	vector<double> m_pest_daily_decay_frac_in_vegetation;
	/** \brief Daily decay fraction, precalculated from the pesticide environmental half life parameter, for detailed flower model, pollen */
	vector<double> m_pest_daily_decay_frac_pollen;
	/** \brief Daily decay fraction, precalculated from the pesticide environmental half life parameter, for detailed flower model, nectar */
	vector<double> m_pest_daily_decay_frac_nectar;
	/** \brief Daily decay fraction, precalculated from the pesticide environmental half life parameter, for seed coating */
	vector<double> m_pest_daily_decay_frac_seedcoating;
#endif
#else
	/** \brief Daily decay fraction, precalculated from the pesticide environmental half life parameter, for one pesticide compartment use */
	vector<double> m_pest_daily_decay_frac;
#endif


	/** \brief Local copy of pointer to the main map object. */
	RasterMap *m_land;

	/** \brief Local copy of pointer to the main landscape object.  */
	Landscape *m_map;

#ifdef __DETAILED_PESTICIDE_FATE
	/** \brief Map to track the pesticide concentration on vegetation surface*/
	vector<vector <double>> m_pest_map_vegcanopy;
	/** \brief Map to track the pesticide concentration in soil*/
	vector<vector <double>> m_pest_map_soil;
#ifdef __FLOWER_PESTICIDE
	/** \brief Map to track the pesticide concentration in nectar*/
  	vector<vector<double>> m_pest_map_nectar;
	/** \brief Map to track the pesticide concentration in pollen*/
	vector<vector<double>> m_pest_map_pollen;
	/** \brief Map to track the pesticide concentration in the vegetation*/
	vector<vector <double>> m_pest_map_in_vegetation;
	/** \brief Map to track the pesticide concentration in seed coatings, which will be transferred into vegetation and soil. */
	vector<vector<double>> m_pest_map_seed;
#endif
#else
	/** \brief Map to track the pesticide concentration*/
  	vector<vector <double>> m_pest_map_main;
#endif


	/** \brief A temporary map used when new pesticide is sprayed */
	vector<double> m_pest_map_twin;
	
	/** \brief If we are recording pesticide load we need this map */
	vector<vector<double>> m_pest_map_record;

	/** \brief The total size of one map in cellsize resolution */
	unsigned int m_pest_map_size;
	/** \brief Vector with indices used for parallel loop */
	vector<int> m_pest_map_indices;
	/** \brief The width of one map in cellsize resolution */
	unsigned int m_pest_map_width;
	/** \brief The height of one map in cellsize resolution */
	unsigned int m_pest_map_height;

	/** \brief a structure to hold pre-calculated pesticide rain washoff factor (Rw) */
	vector<vector <double>> m_RainWashoffFactor;
	/** \brief Daily rainfall saved here * 100 to use as an index to the Pesticide::m_RainWashoffFactor array - an optimisation to stop repeated calls to Landscape::SupplyRain */
	unsigned m_rainfallcategory;

	/** \brief  Pre-calculated diffusion vector <br>
	Used after spraying an element in the pesticide map to determine how much of the sprayed material spreads into the surroundings.*/
	vector<double> m_diffusion_vector;

	/** \brief List of landscape elements, which was sprayed on a given day. One for each PPP we track */
	vector<vector <PesticideEvent*>> m_daily_spray_queue;

	/** \brief List of landscape elements, which had seed coating on a given day. One for each PPP we track */
	vector<vector <PesticideEvent*>> m_daily_seedcoating_queue;

	/** \brief List of landscape elements, which had a granular application on a given day. One for each PPP we track */
	vector<vector <PesticideEvent*>> m_daily_granular_queue;

	/** \brief Resets the pesticide action queue */
	void DailyQueueClear(PlantProtectionProducts a_ppp);
	/** \brief Sprays the pesticides that are listed in the daily queue */
	void DailyQueueProcess(PlantProtectionProducts a_ppp);
	/** \brief Checks if cell has a water type */
	bool ElementIsWater( int a_x, int a_y );
	/** \brief Applies rain washoff and decays the pesticides in all maps by calling Pesticide::DecayMap */
	void MainMapDecay(PlantProtectionProducts a_ppp);
	/** \brief Decays the pesticide in the entire map in a parallel manner */
	void DecayMap(vector<vector<double>>& map, vector<double>& decay_frac, int a_ppp);
	/** \brief Clears the twin map */
	void TwinMapClear( int a_minx, int a_miny, int a_maxx, int a_maxy );
	/** \brief Sprays a_amount of the pesticide in each cell in the polygon in the twin map by calling Pesticide::TwinMapSprayPixel */
	void TwinMapSpray(  LE* a_element_spryaed, double a_amount, int a_minx, int a_miny, int a_maxx, int a_maxy );
	/** \brief Sprays pesticide in the cell */
	void TwinMapSprayPixel( int a_large_map_x,
							int a_large_map_y,
							double a_fractional_amount );
	/** \brief Corrects the border in case the pesticide map size doens't fit the landscape map size */
	void TwinMapSprayCorrectBorders( void );
	/** \brief Diffuses the pesticide in the twin map and adds it to the main map */
	void TwinMapDiffusion( int a_minx, int a_miny, int a_maxx, int a_maxy, double a_cover, PlantProtectionProducts a_ppp);
	/** \brief Initializes the diffusion vector that is used to drift the pesticde */
	void  DiffusionVectorInit( void );
	/** \brief Calculate the amount that drifts a_dist_meters away from the spraying point */
	double DiffusionFunction( double a_dist_meters, bool a_is_last );
	/** \brief Adds the diffused pesticide to the cell in the main map(s) */
	void  DiffusionSprayPixel( int a_x, int a_limit_x, int a_y, int a_limit_y, double a_amount, double a_cover, PlantProtectionProducts a_ppp );
	/** \brief Adds granular pesticide to polygon */
	void AddGranularPesticide(int minx, int miny, int maxx, int maxy, PlantProtectionProducts a_ppp);
#ifdef __DETAILED_PESTICIDE_FATE
	/** \brief Pre-calculates the constants required for rain wash off with increasing rainfall and stores this in #m_RainWashoffFactor */
	void CalcRainWashOffFactors(int a_ppp);
	/** \brief Transfers the pesticide between the different compartments */
	void TransferPesticide(void);
	/** \brief Transfers pesticide from one map to another with a given rate */
	void TransferToOne(vector<vector<double>>& map_from, const double& transfer_rate, vector<vector<double>>& map_to, int a_ppp, bool checkPollen, bool checkNectar);
	/** \brief Transfers pesticide from one map to two others with given rates */
	void TransferToTwo(vector<vector<double>>& map_from, const double& transfer_rate_1, vector<vector<double>>& map_to_1, const double& transfer_rate_2, vector<vector<double>>& map_to_2, int a_ppp);
	/** \brief Decays the seed coating pesticide */
	void SeedCoatingMapDecay(PlantProtectionProducts a_ppp);
	/** \brief Adds seed coating to polygon */
	void AddSeedCoating(int minx, int miny, int maxx, int maxy, PlantProtectionProducts a_ppp);
#endif
	
	/** \brief a file for recording pesticide loads */
	ofstream m_pesticideRecord;
#ifdef __DETAILED_PESTICIDE_FATE
	/** \brief File for recording soil pesticide map */
	ofstream m_pesticide_file_rec_soil;
	/** \brief File for recording plant surface pesticide map */
	ofstream m_pesticide_file_rec_vegcanopy;
#ifdef __FLOWER_PESTICIDE
	/** \brief File for recording in-plant pesticide map */
	ofstream m_pesticide_file_rec_in_vegetation;
	/** \brief File for recording pollen pesticide map */
	ofstream m_pesticide_file_rec_pollen;
	/** \brief File for recording nectar pesticide map */
	ofstream m_pesticide_file_rec_nectar;
	/** \brief File for recording seed coating pesticide map */
	ofstream m_pesticide_file_rec_seed;
#endif
#else
	ofstream m_pesticide_file_rec;
#endif


public:
	/** \brief Main pesticide method that is called every day */
	void Tick( void );
	/** \brief Adds new pesticide spray event to the daily queue */
	void DailyQueueAdd( LE* a_element_sprayed, double a_amount, PlantProtectionProducts a_ppp);
	/** \brief Adds new seed coating event to the daily queue */
	void DailyQueueAddSeedCoating(LE* a_element_sprayed, double a_amount, PlantProtectionProducts a_ppp);
	/** \brief Adds new granular application event to the daily queue */
	void DailyQueueAddGranular(LE* a_element_sprayed, double a_amount, PlantProtectionProducts a_ppp);
	/** \brief Checks if there are any pesticides to decay */
	bool GetAnythingToDecay(PlantProtectionProducts a_ppp){ return (m_something_to_decay[a_ppp]); }
	/** \brief Sets all plant pesticide maps (so all except soil) to 0 */
	void RemovePlantPesticide(int minx, int maxx, int miny, int maxy, int map_index);
	/** \brief Supplies the seed coating pesticide concentration in cell */
	double SupplyPesticideSeed(int a_x, int a_y, PlantProtectionProducts a_ppp);
	/** \brief Supplies the seed coating pesticide concentration in polygon. 
	* Note that the result is not precise, since pesticide concentration can vary within polygon. */
	double SupplyPesticideSeed(int a_polyref, PlantProtectionProducts a_ppp);
#ifdef __DETAILED_PESTICIDE_FATE
	/** \brief Supplies the soil pesticide concentration in cell */
	double SupplyPesticideS(int a_x, int a_y, PlantProtectionProducts a_ppp);
	/** \brief Supplies the soil pesticide concentration in polygon. 
	* Note that the result is not precise, since pesticide concentration can vary within polygon. */
	double SupplyPesticideS(int a_polyref, PlantProtectionProducts a_ppp);
	/** \brief Supplies the plant surface pesticide concentration in cell */
	double SupplyPesticideP(int a_x, int a_y, PlantProtectionProducts a_ppp);
	/** \brief Supplies the plant surface pesticide concentration in polygon. 
	* Note that the result is not precise, since pesticide concentration can vary within polygon. */
	double SupplyPesticideP(int a_polyref, PlantProtectionProducts a_ppp);
#ifdef __FLOWER_PESTICIDE
	/** \brief Supplies the nectar pesticide concentration in cell */
	double SupplyPesticideNectar(int a_x, int a_y, PlantProtectionProducts a_ppp);
	/** \brief Supplies the nectar pesticide concentration in polygon. 
	* Note that the result is not precise, since pesticide concentration can vary within polygon. */
	double SupplyPesticideNectar(int a_polyref, PlantProtectionProducts a_ppp);
	/** \brief Supplies the pollen pesticide concentration in cell */
	double SupplyPesticidePollen(int a_x, int a_y, PlantProtectionProducts a_ppp);
	/** \brief Supplies the pollen pesticide concentration in polygon. 
	* Note that the result is not precise, since pesticide concentration can vary within polygon. */
	double SupplyPesticidePollen(int a_polyref, PlantProtectionProducts a_ppp);
	/** \brief Supplies the plant surface pesticide concentration in cell */
	double SupplyPesticidePlantSurface(int a_x, int a_y, PlantProtectionProducts a_ppp);
	/** \brief Supplies the plant surface pesticide concentration in polygon. 
	* Note that the result is not precise, since pesticide concentration can vary within polygon. */
	double SupplyPesticidePlantSurface(int a_polyref, PlantProtectionProducts a_ppp);
	/** \brief Supplies the in-plant pesticide concentration in cell */
	double SupplyPesticideInPlant(int a_x, int a_y, PlantProtectionProducts a_ppp);
	/** \brief Supplies the in-plant pesticide concentration in polygon. 
	* Note that the result is not precise, since pesticide concentration can vary within polygon. */
	double SupplyPesticideInPlant(int a_polyref, PlantProtectionProducts a_ppp);
#endif
#else
	/** \brief Supplies the pesticide concentration in cell */
	double SupplyPesticide( int a_x, int a_y, PlantProtectionProducts a_ppp);
	/** \brief Supplies the pesticide concentration in polygon. 
	* Note that the result is not precise, since pesticide concentration can vary within polygon. */
	double SupplyPesticide(int a_polyref, PlantProtectionProducts a_ppp);
#endif

	/** \brief Constructer of pesticide engine */
	Pesticide(Landscape *a_map );
	/** \brief Destructor of pesticide engine */
	virtual ~Pesticide(void) {};
	/** \brief Records the pesticide load at landscape level */
	bool RecordPesticideLoad(int a_ppp);
	/** \brief Records the current pesticide concentration in map */
	bool RecordPesticideMap(int a_ppp, ofstream& outfile, vector<vector<double>>& map);
#ifdef PEST_DEBUG
	/** \brief For testing of the pesticide engine. */
	bool SavePPM(	double *a_map,
					int a_beginx, int a_width,
					int a_beginy, int a_height,
					char* a_filename );
	/** \brief For saving diffusion vector in file */
	void DiffusionVectorTest( void );
#endif

};


inline bool Pesticide::ElementIsWater( int a_x, int a_y )
{
	TTypesOfLandscapeElement l_eletype = m_map->SupplyElementType( a_x, a_y );

	if (l_eletype == tole_Freshwater ||
		l_eletype == tole_River      ||
		l_eletype == tole_Pond       ||
		l_eletype == tole_FishFarm   ||
		l_eletype == tole_Saltwater
		)
		return true;

	return false;
}

#ifdef __DETAILED_PESTICIDE_FATE

inline double Pesticide::SupplyPesticideS(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the soil surface pesticide grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_soil[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticideP(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the plant canopy pesticide grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	#ifdef __FLOWER_PESTICIDE
		int l_c = l_y * m_pest_map_width + l_x;
		return m_pest_map_vegcanopy[a_ppp][l_c] + m_pest_map_nectar[a_ppp][l_c] + m_pest_map_pollen[a_ppp][l_c] + m_pest_map_in_vegetation[a_ppp][l_c];
	#else
		return m_pest_map_vegcanopy[a_ppp][l_y * m_pest_map_width + l_x];
	#endif
}

inline double Pesticide::SupplyPesticideS(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the soil pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_soil[a_ppp][l_c];
}

inline double Pesticide::SupplyPesticideP(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the plant canopy pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	#ifdef __FLOWER_PESTICIDE
		return m_pest_map_vegcanopy[a_ppp][l_c]+m_pest_map_nectar[a_ppp][l_c]+m_pest_map_pollen[a_ppp][l_c]+m_pest_map_in_vegetation[a_ppp][l_c];
	#else
		return m_pest_map_vegcanopy[a_ppp][l_c];
	#endif
}
#ifdef __FLOWER_PESTICIDE
inline double Pesticide::SupplyPesticideNectar(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the nectar pesticide grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_nectar[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticidePollen(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the pollen pesticide grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_pollen[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticideInPlant(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the pesticide winthin plants grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_in_vegetation[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticidePlantSurface(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the pesticide on plants surface grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_vegcanopy[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticideNectar(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the nectar pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_nectar[a_ppp][l_c];
}

inline double Pesticide::SupplyPesticidePollen(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the pollen pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_pollen[a_ppp][l_c];
}

inline double Pesticide::SupplyPesticideInPlant(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the plant body pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_in_vegetation[a_ppp][l_c];
}

inline double Pesticide::SupplyPesticidePlantSurface(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the plant surface pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_vegcanopy[a_ppp][l_c];
}

inline double Pesticide::SupplyPesticideSeed(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the seed pesticide grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_seed[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticideSeed(int a_ele, PlantProtectionProducts a_ppp) {
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the seed pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_seed.at(a_ppp).at(l_c);
}
#endif

#else
inline double Pesticide::SupplyPesticide(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param a_x the x-coord in landscape x units
	@param a_y the y-coord in landscape x units
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the pesticide a_ppp grid cell containing a_x,a_y
	*/
#ifdef PEST_WATER_CHECK
	if (ElementIsWater(a_x, a_y))
		return 0.0;
#endif

	unsigned int l_x = a_x >> PEST_GRIDSIZE_POW2;
	unsigned int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_main[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticide(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@param a_ppp the index number of the pesticide asked for
	@return Returns the value of the pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_main[a_ppp][l_c];
}

#endif

inline void Pesticide::TwinMapSprayPixel(int a_large_map_x,
	int a_large_map_y,
	double a_fractional_amount)
{
	int l_x = a_large_map_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_large_map_y >> PEST_GRIDSIZE_POW2;

	m_pest_map_twin[l_y * m_pest_map_width + l_x] +=
		a_fractional_amount;
}

class PesticideOutput
{
public:
	PesticideOutput(int a_startyear, int a_noyears, int a_cellsize, Landscape* a_landscape, RasterMap* a_land);
	~PesticideOutput() { ; }
	virtual void Spray(LE* /* a_element_sprayed */, TTypesOfPesticideCategory /* a_type */) { ; }
protected:
	/** \brief insecticide map/table data */
	vector<double>* m_pmap_insecticides;
	/** \brief herbicide map/table data */
	vector<double>* m_pmap_fungicides;
	/** \brief fungicide map/table data */
	vector<double>* m_pmap_herbicides;
	/** \brief first simultion year to record */
	int m_startyear;
	/** \brief last year of data to record */
	int m_endyear;
	/** \brief the size of the cell for pesticide data in m */
	int m_cellsize;
	/** \brief based on cellsize the width of the map */
	int m_pmap_width;
	/** \brief  based on cellsize the height of the map */
	int m_pmap_height;
	/** \brief pointer to the landscape  */
	Landscape* m_OurLandscape;
	/** \brief pointer to the landscape map */
	RasterMap* m_Rastermap;
};


class PesticideTable : public PesticideOutput
{
public:
	PesticideTable(int a_startyear, int a_noyears, int a_cellsize, Landscape* a_landscape, RasterMap* a_land);
	~PesticideTable() {
		m_PTableI->close();
		m_PTableH->close();
		m_PTableF->close();
	}
	virtual void Spray(LE* a_element_sprayed, TTypesOfPesticideCategory a_type);
	virtual void PrintPTable();
	void PrintITable();
	void PrintHTable();
	void PrintFTable();
	void Clear() {
		fill(m_pmap_insecticides->begin(), m_pmap_insecticides->end(), 0.0);
		fill(m_pmap_herbicides->begin(), m_pmap_herbicides->end(), 0.0);
		fill(m_pmap_fungicides->begin(), m_pmap_fungicides->end(), 0.0);
	}

protected:
	ofstream* m_PTableI;
	ofstream* m_PTableH;
	ofstream* m_PTableF;

};


class PesticideMap : public PesticideOutput
{
	/**
	* PesticideMap is a class for handling pesticide mapping output. This can be used to sum up pesticide concentrations over time. It works currently for general pesticides
	* split into insecticides, herbicides, and fungicides, but also for the primary test pesticide if one is being used.
	*/
protected:
	/** \brief true if using test pesticide, false for general pesticides */
	bool m_typeofmap;
public:
	PesticideMap(int a_startyear, int a_noyears, int a_cellsize, Landscape* a_landscape, RasterMap* a_land, bool a_typeofmap);
	~PesticideMap();
	bool DumpPMap(vector<double>* a_map);
	bool DumpPMapI() { return DumpPMap(m_pmap_insecticides); }
	bool DumpPMapH() { return DumpPMap(m_pmap_herbicides); }
	bool DumpPMapF() { return DumpPMap(m_pmap_fungicides); }
	virtual void Spray(LE* a_element_sprayed, TTypesOfPesticideCategory a_type);
};

Pesticide* CreatePesticide(Landscape *l);

#endif // PESTICIDE_H
