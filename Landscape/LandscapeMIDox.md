Landscape MIDox                       {#Landscape_page}
=========
<h1> The ALMaSS Landscape and Associated Classes MIDox Documentation </h1>   
Topping, C. J.<sup>*,1</sup> & Ziółkowska, E.<sup>1,2</sup>  

1.  Social-Ecological Systems Simulation centre, Department of Ecoscience, Aarhus University, Denmark <br>
2.  Institute of Environmental Sciences, Jagiellonian University in Kraków, Poland

*Corresponding author: Chris J. Topping, cjt@ecos.au.dk

# Introduction
The Landscape Class is a large class with multiple functionalities, providing the interface to all environmental information required to run all ALMaSS agent-based and subpopulation models.<br>
The landscape class is initialised at the start of any ALMaSS simulation. This process sets up many of the major data structures and classes used by the simulation and as such is a critical part of the ALMaSS simulation system. The main sections will be further detailed below but include creating the internal representation of the map and associated classes, setting up the FarmManager class and all farm information, simulation of habitat patch development including vegetation modelling, miscellaneous functions associated with implementing behaviour in species models, modifying the input data or generating output for subsequent use. <br>
This documentation provides an entry point for navigating the C++ code and the Landscape and associated class functionality.

# Aims & Purposes
The purpose of the classes described in this documentation is to provide a dynamic and as far as possible realistic landscape simulation to be used on its own or as an environment in which to run other ALMaSS models. The Landscape should provide information on the topographical structure of the physical landscape, and details of the management carried out on these areas, together with the vegetation structure and other environmental descriptors.

# Model in brief
The landscape simulation is built around the Landscape class, see \ref _lsm1 "The Landscape Class" which contains an interface to all environmental and management information. The Landscape class itself has minimal functionality but is dependent on a number of associated classes for the detailed function. Key blocks of classes related to:<br>
- Farms and farm management, including crops see \ref _lsm2 "Farms and farm management, including crops"
- Habitat patches see \ref _lsm3 "LE & Associated Classes"
- The landscape map see \ref _lsm5 "The Raster Map"
- Pesticides etc. see \ref _lsm4 "Pesticide and related classes"
- Classes for the weather and date management see \ref _lsm6 "Weather and Calendar"

# Scheduling
Landscape::TurnTheWorld is called once per day by the main model program loop. TurnTheWorld is a wrapper for Landscape::Tick, which is responsible for calling all associated class scheduling methods. These methods are Calendar::Tick, Weather::Tick, LE::Tick, FarmManager::FarmManagement, Pesticide::Tick, and RodenticideManager::Tick and are called in the order specified here. Each of these tick methods carries out the required behaviour for its class for that day.<br>
Note that the map is assumed to be static during a simulation run, so that the information contained within a habitat patch may change, but the size, shape and location of that patch will be static during any simulation run.<br>
Although the landscape works on a daily time step, species models may work on faster times e.g. bees work on 10-minute time steps. Therefore, the once exception to the daily time step rule for landscape classes is the calendar, which may require running at smaller than daily steps.

# Implementation details
\anchor _lsm6
## Weather and Calendar
The Weather and Calendar classes have only minimal behaviour in themselves. They are primarily data suppliers.
### The Weather Class
The weather input file is specified by the configuration variable #l_map_weather_file and accessed through the configuration file with the key MAP_WEATHER_FILE (string). The default value is "weather.pre". See IO below for details. The main scheduling is managed through the Tick method, but otherwise the Weather class has no functionality except for supplying the data on weather requested by the simulation. <br>
#### Control and calculation methods
Weather::Tick is the daily updating method. Tick is responsible for calculating the following set of derived values:<br>
- Weather::m_snowtoday The depth of snow either from data if enabled or calculated by assuming precipitation and negative temperatures equates to snow.
- Weather::m_temptoday The average temperature today (degrees C)
- Weather::m_raintoday Precipitation today (mm)
- Weather::m_windtoday = The average windspeed today (m)
- Weather::m_soiltemptoday The temperature at 5cm soil depth
- Weather::m_mintempyesterday The minimum temperature yesterday, either calculated or from data if enabled
- Weather::m_maxtempyesterday The maximum temperature yesterday, either calculated or from data if enabled
- Weather::m_mintemptoday The minimum temperature today, either calculated or from data if enabled
- Weather::m_maxtemptoday The maximum temperature today, either calculated or from data if enabled
- Weather::m_radiationtoday The amount of sunshine hours today (if enabled)
- Weather::m_flyinghourstoday The number of bee flying hours today (if enabled)
- Weather::m_rainingtoday Whether there is at least 0.1mm precipitation today
- Weather::m_humiditytoday The humidity calculated from precipitation and temperature
- Weather::m_winddirtoday The wind direction. Currently hard-coded probabilities.
- Weather::m_insolation The insolation in MJ/m2 for a location in Denmark 1961-1990. Old function to be removed.

Weather::DeriveSnowCover Calculates the snow cover if snow cover is not available as data input.<br><br>
Weather::DeriveSoilTemp Calculates the soil temperature from air temperature if soil data is not available.<br><br>
Weather::readWeatherFile is the standard weather file input method. This reads the daily values for all input fields. Temperature, rainfall and windspeed are compulsory values, but snowcover, humidity, soil temperature, twilight soil temperature, minimum and maximum temperature, and bee flying hours are optional inputs read in dependent on user defined flags in configuration variables: #l_map_weather_file #l_weather_minmaxtemp #l_weather_radiation #l_weather_soiltemp #l_weather_snowcover #l_weather_relhumidity #l_weather_soiltemptwilight, and #l_weather_flyinghours <br><br>
Weather::readWeatherFileHourly In the case of models needing hourly weather then this method is used instead of the readWeatherFile method. Here the difference is that temperature, rainfall, windspeed and sunshine hours are all read in in 24 data points per day.

### The Calendar Class
The calendar class manages dates and associated features for the simulation. This manages all time keeping for activities working on a daily basis, and tracks the progress of the simulation through simulated time.
#### The Main Tick method
The Calendar::Tick method controls the passage of time in days and is responsible to updating the day, month, and year. The calendar assumes that every year has 365 days, therefore the 29th February in leap years is ignored. It sets the following attributes, which are accessed through a set of interface functions:
 - Calendar::m_date The counter for days since the start of the simulation. This is incremented when Tick is called.
 - Calendar::m_day_in_month The day number of the month. This is incremented when Tick is called. If this counter exceeds the days in the month then it is reset and Calendar::m_month is incremented.
 - Calendar::m_month The month number (0-11).  If this counter exceeds 11 then it is reset to zero and Calendar::m_year and Calendar::m_simulationyear is incremented, 365 is then added to Calendar::m_olddays.
 - Calendar::m_month The month number (0-11)
 - Calendar::m_year The calendar year. This will depend on the input data for the weather, but starts to count from the first weather date
 - Calendar::m_simulationyear The date in simulation years, assuming the first simulation year is zero
 - Calendar::m_olddays The number of days simulated before January 1st of the current simulation year
 - Calendar::m_janfirst A flag indicating today is the start of a new year
 - Calendar::m_day_in_year The Julian day in the year (0-364). This is incremented when Tick is called.
 - Calendar::m_todayslength The daylength (light hours)
 - Calendar::m_todayssunrise The sunrise time
 - Calendar::m_todayssunset The sunset time
 - Calendar::m_daylightproportion The proportion of time it is daylight for this day

#### Other Tick functions
These three methods allow for shorter than day time keeping. The returns value for the next three methods indicates whether the global Tick() should be called for the whole landscape model. Order matters here, so this cannot just be done internally within the Calendar class.<br>
If a model needs a minute time step then Calendar::TickMinute is used to increment Calendar::m_minutes, if m_minutes becomes 60 it is reset to 0 and Calendar::TickHour is called. Similarly if Calendar::m_hours becomes 24, it is reset to 0 and the main Tick function called. TickMinute10 is provided to replace TickMinute when 10-minute time steps are used.

#### Daylength Calculation
Calendar::CreateDaylength is responsible for calculation of the day length based on the landscape input providing the latitude and longitude. This uses the calculator provided in the Sunset library Copyright (GPL) 2004 Mike Chirico mchirico@comcast.net. This provides the SunSet class, from which CreateDayLength draws its main functions. CreateDayLength is called at the start of the simulation and creates the daylength, sunrise and sunset times for each day of the year based on the location and the angle of the sun to calculate the time sunrise/set occurs.  

#### Calendar Interface Functions
The interface functions are listed below preceded with their return type:<br>
int  Calendar::DayInYear( int a_day, int a_month ) returns the Julian day for a day & month combination  <br>
long  Calendar::Date returns m_date  <br>
int  Calendar::DayInYear returns m_day_in_year  <br>
long  Calendar::OldDays returns m_olddays <br>
long  Calendar::GlobalDate returns the simulation global date for the day, month and year supplied <br>
int   Calendar::DayLength returns m_todayslength <br>
int   Calendar::DayLength(int a_day_in_year) returns the daylength for the day specified in a_day_in_year <br>
int   Calendar::SunRiseTime returns m_todayssunrise <br>
int   Calendar::SunSetTime returns m_todayssunset <br>
int   Calendar::SunRiseTime(int a_day_in_year) returns the sunrise time for a day in the year <br>
int   Calendar::SunSetTime(int a_day_in_year) returns the sunset time for a day in the year <br>
int   Calendar::GetFirstYear returns m_firstyear <br>
int   Calendar::GetLastYear returns m_lastyear <br>
int   Calendar::GetYear returns m_year <br>
int   Calendar::GetYearNumber returns m_simulationyear <br>
int   Calendar::GetMonth returns m_month + 1  (the calendar month)<br>
int   Calendar::GetMinute returns m_minutes <br>
int   Calendar::GetHour returns m_hours <br>
int   Calendar::GetDayInMonth returns m_day_in_month <br>
double Calendar::GetDaylightProportion returns m_daylightproportion <br>
bool  Calendar::JanFirst returns m_janfirst  <br>
bool  Calendar::MarchFirst returns m_marchfirst <br>
void  Calendar::SetFirstYear( int a_year ) Sets m_firstyear as a_year  <br>
void  Calendar::SetLastYear( int a_year ) Sets m_lastyear a a_year<br> } <br>
bool  Calendar::ValidDate( int a_day, int a_month ) Checks whether a combination of day and month is a valid date <br>

\anchor _lsm5
## The Raster Map
### Raster Map class
The class RasterMap holds the data and functionality associated with the topography. RasterMap has only one job and that is hold the physical map information in an accessible raster format. The elements of the raster are references to LE instances (see below). Each LE instance represents a habitat patch.
RasterMap::Init reads a binary '.lsb' file into memory populating RasterMap::m_map with LE reference numbers in an array of integers RasterMap::m_width by RasterMap::m_height in size. It also reads an 12 character ID number RasterMap::m_id identifying the lsb map.<br>
#### Interface Functions
  RasterMap::MapWidth returns m_width<br>
  RasterMap::MapHeight returns m_height<br>
  RasterMap::GetID returns m_id<br>
  RasterMap::Get( int a_x, int a_y ) This is the main interface function, it returns the raster value (LE reference) at a_x, a_y coordinates <br>
  RasterMap::GetMagicP( int a_x, int a_y ) This returns a pointer to a location in the raster map given by a_x, a_y <br>
  RasterMap::Put( int a_x, int a_y, int a_elem ) This sets the raster value (LE reference a_elem) at a_x, a_y coordinates <br>
#### Other functions
There are three other functions associated with manipulations of the input map. RasterMap::ellReplacementNeighbour replaces a cell location with the most common cell value from the surrounding 8 cells. MissingCellReplace removes missing polygon. RasterMap::MissingCellReplace and RasterMap::MissingCellReplaceWrap also fill in missing cells from neigbours, but will preferentially select field cells. MissingCellReplaceWrap is used when cells are on the edge of the map.<br>

\anchor _lsm1
### The Landscape Class
The main class and linkage point for all other classes for the landscape simulation. This class contains a large number of interface functions, many of them are specialised for individual species e.g. Landscape::SupplyGooseGrazingForageH, however, it also contains the landscape simulation initialisation and main loops, as well as a number of manipulations of the landscape data. The main functionality is however provided by the associated classes, instances of which are created as attributes of this class. These key entities are:<br>
- FarmManager Landscape::m_FarmManager
- RasterMap Landscape::m_land
- RodenticideManager Landscape::m_RodenticideManager

The main data structure held by this class is related to holding the habitat patches information. The vector of integers Landscape::m_polymapping is a list of the mapping of the polygon numbers to their index in Landscape::m_elems which is the list of the LE instances representing the habitat patches in the landscape. In most optimised landscapes the m_elems index and polygon (LE) number are the same, but this is not compulsory.

#### Important methods
Landscape::Landscape The Landscape constructor. The landscape constructor sets up all the mapping, environment and management for the landscape simulation. <br>
Landscape::~Landscape The Landscape destructor<br>
Landscape::Tick The method that keeps landscape time, its called once per day to carry out all the daily activities<br>
Landscape::TurnTheWorld Identical to Tick, just a wrapper for backwards compatibility<br>
Landscape::ReadPolys1 Is used to take a look in the input file for polygons to figure out what the format is (since multiple formats are allowed). <br>
Landscape::ReadPolys2 Based on the format of the file (since multiple formats are allowed), this reads the polygon information from an input file. This method sets up the Landscape::m_polymapping lookup table, checks validity of polygon and associated farm information and creates the LE instances to populate the Landscape::m_elems array, and if needed creates the links between unsprayed margins and the owner tole type. <br>
Landscape::RunHiddenYear This is a short version of the landscape loop. It is used to run the first year from Jan 1st before any other models are run. This allows the landscape simulation to ‘burn-in’ and thus it should start the simulation properly at the beginning of the real model runs <br>
Landscape::TranslateEleTypes Returns element type translated from ALMaSS the reference number, e.g. 20 is tole_Field <br>
Landscape::TranslateVegTypes Returns vegetation type translated from the ALMaSS reference number, e.g. 801 is tov_PLWinterWheat <br>
Landscape::BackTranslateEleTypes Returns the ALMaSS reference number translated from the TTypesOfLandscapeElement type, e.g. tole_Field is 20 <br>
Landscape::BackTranslateVegTypesReturns the ALMaSS reference number translated from the TTypesOfVegetation type, e.g. tov_PLWinterWheat is 801 <br>


#### Interface Functions
These are numerous, listed below with a short description of their use:<br>
Landscape::SupplyPolyRef Get the in map polygon reference number from the x,y location<br>
Landscape::SupplyLEPointer Returns a pointer to the object referred to by the polygon number <br>
Landscape::SupplyUMRef Get the unsprayed margin reference number from the polygon at x,y <br>
Landscape::SupplyPolyLEptr Get the pointer to the LE object associated with the polygon at x,y location or by using the polygon reference<br>
Landscape::SupplyPolyRefIndex Get the index to the m_elems array for a polygon at location x,y <br>
Landscape::SupplyPolyRefCC Get the in map polygon reference number from the x,y location, and correcting for possible wrap around coordinates <br>
Landscape::SupplyPolyListPtr Returns the pointer to m_elemns <br>
Landscape::SupplySimAreaWidth Gets the landscape width<br>
Landscape::SupplySimAreaHeight Gets the landscape height<br>
Landscape::SupplySimAreaMaxExtent Returns which ever is larger, height or width of landscape<br>
Landscape::SupplySimAreaMinExtent Returns which ever is smaller, height or width of landscape<br>
Landscape::SupplyNumberOfPolygons Returns the number of polygons in the landscape<br>
Landscape::SupplyElementTypeFromVector  Gets the TTypesOfVegetation type of a polygon using the m_elems index<br>
Landscape::SupplyOwner_tole Gets the farm owner reference of a polygon, or -1 if not owned by a farmer<br>
Landscape::SupplyPolyRefVector Gets the polygon reference number from the index to m_elems
Landscape::SupplyVegPhase Returns the current vegetation growth phase for a polygon <br>
Landscape::SupplyPestIncidenceFactor Returns  Landscape::m_pestincidencefactor <br>
Landscape::SetPolymapping Sets a an entry in the polymapping to a specific value <br>
Landscape::GetPolymapping Returns the value of the m_polymapping array indexed by a_index <br>
Landscape::SupplyMaxPoly  Returns the size of the Landscape::m_elems vector. <br>
Landscape::GetAllVegPolys Creates a vector containing a list of all the polygons that do not have #tov_None as the vegetation type <br>
Landscape::HowManyPonds Returns the number of ponds in the landscape <br>
Landscape::SupplyRandomPondIndex Returns a random pond index <br>
Landscape::SupplyRandomPondRef Returns a random pond polyref <br>
Landscape::SupplyPondIndex Returns the index of a pond based on pondref or -1 if not found <br>
Landscape::SupplyFarmPtr Get a pointer to a farm owned by a farmer with a specific farm reference number <br>
Landscape::SupplyFarmManagerPtr Returns m_FarmManager, the pointer to the farm manager instance <br>
Landscape::SupplyRasterMap Returns m_land a pointer to the raster map instance <br>
Landscape::SupplyLargestPolyNumUsed Returns m_LargestPolyNumUsed <br>
Landscape::SupplyShouldSpray Returns m_toxShouldSpray, a flag indicating whether pesticide should be sprayed <br>
Landscape::SupplyCountryCode Returns m_countryCode e.g. “DK” for Denmark <br>
Landscape::SupplyTimezone Returns the number of hours relative to GMT based on the country code <br>
Landscape::SetCountryCode Sets m_countryCode <br>
Landscape::SupplyLandscapeName Gets the current landscape name <br>
Landscape::SetSpeciesFunctions This is the jumping off point for any landscape related species setup. It creates function pointers to these special functions <br>
Landscape::SupplyLatitude  Returns m_latitude <br>
Landscape::SupplyLongitude Returns m_longitude, the longitude of the landscape location <br>
Landscape::SetLatitude Sets the latitude of the landscape location <br>
Landscape::SetLongitude Sets the longitude of the landscape location <br>
Landscape::SupplyVegDigestibilityVector Gets the digestibility of the vegetation based on an index to the Landscape::m_elems array <br>
Landscape::SupplyVegDigestibility Gets the digestibility of the vegetation for a polygon given by a_polyref or based on the x,y coordinates given by a_x, a_y <br>
Landscape::SupplyVegHeightVector Returns the height of the vegetation using the index to Landscape::m_elems <br>
Landscape::SupplyVegHeight Returns the height of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyVegBiomassVector Returns the biomass of the vegetation using the index a_index to Landscape::m_elems <br>
Landscape::SupplyVegBiomass Returns the biomass of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyVegDensity Returns the density of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyWeedBiomass Returns the weed biomass of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyPollen Returns information on the pollen produced by the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyNectar Returns information on the nectar produced by the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplySugar Returns information on the sugar produced by the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyPollenQuantity Returns the pollen quantity produced by the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyNectarQuantity Returns the XXX of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyTotalNectar Returns the nectar quantity produced by  the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyTotalPollen Returns the sugar quantity produced by  of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyNecDD Returns the current day degree sum for nectar production of the vegetation using the polygon reference number a_polyref <br>
Landscape::SupplySugDD Returns the current day degree sum for sugar production of the vegetation using the polygon reference number a_polyref <br>
Landscape::SupplyPolDD Returns the current day degree sum for pollen production of the vegetation using the polygon reference number a_polyref <br>
Landscape::SupplyGreenBiomass Returns the green biomass of the vegetation using the polygon reference number a_polyref <br>
Landscape::SupplyGreenBiomassProp Returns the green biomass as a proportion of biomass of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyVegGrowthStage Returns the vegetation growth stage of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyDeadBiomass Returns the dead biomass of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyLAGreen Returns the green leaf area of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyLATotal Returns leaf area in total of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyVegCover Returns the vegetation cover proportion of the vegetation using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyVegCoverVector Returns the vegetation cover proportion of the vegetation using the polygon index to Landscape::m_elems <br>
Landscape::SupplyLastSownVeg Returns the last type of vegetation sown on a field using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyLastSownVegVector Returns the last type of vegetation sown on a field using the polygon index to Landscape::m_elems <br>
Landscape::SupplyInsects Returns the insect biomass on a polygon using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyOpenness Returns the openness measure for a polygon using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplySoilType Returns the soil type in ALMaSS types reference numbers. See # TTypesOfSoils <br>
Landscape::SupplyCentroid Returns the centroid of a polygon using the polygon reference number a_polyref <br>
Landscape::SupplyCentroidIndex Returns the centroid of a polygon using the polygon index in m_elems <br>
Landscape::SupplyCentroidX Returns the centroid x-coordinate of a polygon using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyCentroidY Returns the centroid y-coordinate of a polygon using the polygon reference number a_polyref or based on the x,y coordinates given by a_x, a_y  <br>
Landscape::SupplyLargeOpenFieldsNearXY Returns a pointer to a list of polygonrefs to large open fields within a range of location x,y <br>
Landscape::SupplyFarmIntensity Returns the farm intensity classification of the polygon using the polygon reference number a_polyref or coordinates a_x, a_y <br>
Landscape::SupplyFarmIntensityI Returns the farm intensity classification of the polygon using the polygon index in m_elems <br>
Landscape::SupplyElementType Returns the landscape type of the polygon using the polygon reference number a_polyref or coordinates a_x, a_y. Returns the value as a #TTypesOfLandscapeElement <br>
Landscape::SupplyElementTypeCC Returns the landscape type of the polygon using coordinates a_x, a_y, but allowing correction of coordinates for out of scope values. Returns the value as a #TTypesOfLandscapeElement <br>
Landscape::GetOwner_tole Returns the landscape type of the polygon owner (used in cases relating to e.g. unsprayed field margins) using coordinates a_x, a_y. Returns the value as a #TTypesOfLandscapeElement <br>
Landscape::SupplyCountryDesig Returns the designation of country or urban of the polygon using coordinates a_x, a_y <br>
Landscape::SupplyElementSubType Returns the landscape element sub-type of the polygon using the polygon reference number a_polyref or coordinates a_x, a_y <br>
Landscape::SupplyVegType Returns the vegetation type of the polygon using the polygon reference number a_polyref or coordinates a_x, a_y.  Returns the value as a # TTypesOfVegetation <br>
Landscape::SupplyVegTypeVector Returns the vegetation type of the polygon using the polygon index to m_elems.  Returns the value as a # TTypesOfVegetation <br>
Landscape::SupplyGrazingPressureVector Returns the grazing pressure of the polygon using the polygon index to m_elems.  
Landscape::SupplyGrazingPressure Returns the grazing pressure of the polygon using the polygon reference number a_polyref or coordinates a_x, a_y <br>
Landscape::SupplyPolyWithFlowers Returns m_poly_with_flowers, a list of polygons with flowering modelled <br>
Landscape::SupplyVegPatchy Returns whether the polygon referenced by a_polyref or a_x, a_y has a vegetation type designated as patchy <br>
Landscape::SupplyHasTramlines Returns whether the polygon referenced by a_polyref or a_x, a_y has a vegetation type with tramlines <br>
Landscape::SupplyJustSprayedVector Returns whether the polygon referenced by an index to Landscape::m_elems has been sprayed in the last timestep <br>
Landscape::SupplyJustSprayed Returns whether the polygon referenced by a_polyref or a_x, a_y has been sprayed in the last timestep <br>
Landscape::SupplyTreeAge Returns the tree age for the polygon referenced by a_polyref or a_x, a_y <br>
Landscape::SupplyVegAge Returns the vegetation age for the polygon referenced by a_polyref or a_x, a_y <br>
Landscape::SupplyFarmOwner Returns the farm owner pointer for the polygon referenced by a_polyref or a_x, a_y <br>
Landscape::SupplyFarmOwnerIndex Returns the farm owner reference number for the polygon referenced by a_polyref or a_x, a_y <br>
Landscape::SupplyJustMownVector Returns the whether the vegetation was mown for the polygon referenced the index to Landscape::m_elems<br>
Landscape::SupplyJustMown Returns the whether the vegetation was mown for the polygon referenced by a_polyref or a_x, a_y <br>
Landscape::SupplyFarmType Returns the farm type for the polygon referenced by a_polyref or a_x, a_y <br>
Landscape::SupplyNumberOfFarms Returns the number of farms in the current landscape <br>
Landscape::SupplyFarmRotFilename Returns filename for the rotation file (if any) for the polygon referenced by a_polyref or a_x, a_y <br>
Landscape::SupplyFarmArea Returns the farm area of a farm that owns a particular polygon<br>
Landscape::SupplyPolygonAreaVector Returns the polygon area of a particular polygon using an index to m_elems<br>
Landscape::SupplyPolygonArea(int a_polyref) Returns the polygon area of a particular polygon referenced by polygon number<br>
Landscape::SupplyGrainDist Returns m_grain_dist, a binary condition for good or bad years of spilled grain <br>
Landscape::SetGrainDist Sets m_grain_dist, a binary condition for good or bad years of spilled grain <br>
Landscape::SupplyInStubble Returns whether the polygon is cereal in stubble <br>
Landscape::SupplyRoadWidth Returns the width of roads <br>
Landscape::SupplyTrafficLoad Returns a measure of the traffic load at the time of calling <br>
Landscape::SupplyTreeHeight Returns the height of trees<br>
Landscape::SupplyLastTreatment Returns the last treatment recorded for the polygon <br>
Landscape::GrazeVegetation Removes grazing forage from a poly per m2 <br>
Landscape::GrazeVegetationTotal Removes grazing forage from a poly and divides this out per m2<br>
Landscape::Set_all_Att_UserDefinedBool Used to classify the user defined bool attribute and set it - this requires supplying the correct classification function. For example if the polygons should be classified as hare habitats a function needs to be passed that classifies TTypesOfLandscapeElement as true or false <br>
Landscape::Set_all_Att_UserDefinedInt Used to classify the user defined integer attribute and set it - this requires supplying the correct classification function. For example if the polygons should be classified as hare habitats a function needs to be passed that classifies TTypesOfLandscapeElement as true or false <br>
Landscape::SupplyValidX Returns an x-coordinate guaranteed to be within the polygon referenced <br>
Landscape::SupplyValidY Returns an y-coordinate guaranteed to be within the polygon referenced <br>
Landscape::Set_TOLE_Att Uses a point to an LE instance and sets all the attributes based on its tole type<br>
Landscape::Set_TOV_Att Uses a point to an LE instance and sets all the attributes based on its tov type<br>

This set of methods are all used to determine characteristics of a landscape polygon based on its vegetation type:<br>
Landscape::ClassificationVegGrass Returns whether a vegetation type is classified as Grass<br>
Landscape::ClassificationVegCereal Returns whether a vegetation type is classified as cereal<br>
Landscape::ClassificationVegMatureCereal Returns whether a vegetation type is classified as cereal grown to maturity<br>
Landscape::ClassificationVegGooseGrass Returns whether a vegetation type is classified as Goose Grass<br>
Landscape::ClassificationVegMaize Returns whether a vegetation type is classified as maize<br>
Landscape::ClassificationPermCrop Returns whether a vegetation type is classified as a permanent crop<br>

##### Pesticide and Related Interface Functions
Landscape::SupplySeedCoating Gets total seed coating for the centroid of a polygon for a particular pesticide given for PlantProtectionProducts a_ppp <br>
Landscape::SupplyPesticideDecay Returns true if there is any pesticide in the system at all at this polygon <br>
Landscape::SupplyPesticide Gets total pesticide for a location for pesticide for PlantProtectionProducts a_ppp <br>
Landscape::SupplyOverspray Returns the overspray flag for this location <br>
Landscape::SupplyPesticideP Returns the pesticide amount on plant material for this location for PlantProtectionProducts a_ppp <br>
Landscape::SupplyPesticideS Returns the pesticide amount in soil for this location for PlantProtectionProducts a_ppp <br>
Landscape::SupplyPesticidePlantSurface Returns the pesticide concentration on plant surfaces for this location for PlantProtectionProducts a_ppp <br>
Landscape::SupplyPesticideInPlant Returns the pesticide concentration in plant for this location for PlantProtectionProducts a_ppp <br>
Landscape::SupplyPesticideNectar Returns the pesticide concentration in nectar for this location for PlantProtectionProducts a_ppp <br>
Landscape::SupplyPesticidePollen Returns the pesticide concentration in nectar for this location for PlantProtectionProducts a_ppp <br>
Landscape::SupplyRodenticide Gets total rodenticide for a location<br>
Landscape::SupplyPesticideType Gets type of pesticide effect from a pesticide reference, see #TTypesOfPesticide <br>
Landscape::SupplyPesticideCell Gets the index to the cell used to store pesticide information from the coordinates x,y <br>
Landscape::SupplyPondPesticide Get the concentration of pesticide in a pond <br>

#### Weather related wrappers
This list of functions simply passes a request on to the associated Weather class function.
- Landscape::SupplyGlobalRadiation()<br>
- Landscape::SupplyGlobalRadiation(long a_date)<br>
- Landscape::SupplyRain(void)<br>
- Landscape::SupplyTemp(void)<br>
- Landscape::SupplyMinTemp(void)<br>
- Landscape::SupplyMaxTemp(void)<br>
- Landscape::SupplyMinTempYesterday(void)<br>
- Landscape::SupplyMaxTempYesterday(void)<br>
- Landscape::SupplyMinTempTomorrow(void)<br>
- Landscape::SupplyMaxTempTomorrow(void)<br>
- Landscape::SupplySoilTemp(void)<br>
- Landscape::SupplyHumidity(void)<br>
- Landscape::SupplyMeanTemp(long a_date, unsigned int a_period)<br>
- Landscape::SupplyWind(void)<br>
- Landscape::SupplyWindDirection(void)<br>
- Landscape::SupplySnowDepth(void)<br>
- Landscape::SupplySnowcover(void)<br>
- Landscape::SupplyDaylength(void)<br>
- Landscape::SupplyTempHour(int hour)<br>
- Landscape::SupplyWindHour(int hour)<br>
- Landscape::SupplyRainHour(int hour)<br>
- Landscape::SupplyRadiationHour(int hour)<br>
- Landscape::SupplyRain(long a_date)<br>
- Landscape::SupplyTemp(long a_date)<br>
- Landscape::SupplySoilTemp(long a_date)<br>
- Landscape::SupplyWind(long a_date)<br>
- Landscape::SupplyDayDegrees(int a_polyref)<br>
- Landscape::SupplyRainPeriod(long a_date, int a_period)<br>
- Landscape::SupplyWindPeriod(long a_date, int a_period)<br>
- Landscape::SupplyTempPeriod(long a_date, int a_period)<br>
- Landscape::SupplySnowcover(long a_date)<br>

#### Calendar related wrappers
These interface functions just pass the request to the associated calendar function
 - Landscape::SupplyDaylength(long a_date)
 - Landscape::SupplyDayInYear(void)
 - Landscape::SupplyHour(void)
 - Landscape::SupplyMinute(void)
 - Landscape::SupplyYear(void)
 - Landscape::SupplyYearNumber(void)
 - Landscape::SupplyMonth(void)
 - Landscape::SupplyMonthName(void)
 - Landscape::SupplyDayInMonth(void)
 - Landscape::SupplyDaylightProp(void)
 - Landscape::SupplyNightProp(void)



##### Species Related Interface Functions
Landscape::SupplySkScrapes Returns the presence of skylark scrapes in the vegetation using the polygon reference number a_polyref <br>
Landscape::GetGooseFields Gets the list of suitable goose foraging fields today <br>
Landscape::SupplyIsHumanDominated For the roe deer, determines the extent of human activity and returns true/false <br>
Landscape::SupplySoilTypeR Returns the soil type in rabbit warren reference numbers for a location given by a_x, a_y<br>
Landscape::SetBirdSeedForage Sets the grain forage resource as seen from a goose standpoint at a polygon <br>
Landscape::SetBirdMaizeForage Sets the maize forage resource as seen from a goose standpoint at a polygon <br>
Landscape::SupplyGooseGrazingForageH Returns the leaf forage resource as seen from a goose standpoint at a polygon based on the height only<br>
Landscape::SupplyGooseGrazingForageH Returns the leaf forage resource as seen from a goose standpoint at a polygon referenced by number based on height only <br>
Landscape::GetActualGooseGrazingForage Returns the leaf forage resource as seen from a goose standpoint at a polygon referenced by x,y location or a polygon ref<br>
Landscape::SupplyBirdSeedForage Returns the grain forage resource for a polygon or coordinate location<br>
Landscape::SupplyBirdMaizeForage Returns the maize forage resource for a polygon or coordinate location<br>
Landscape::RecordGooseNumbers This records the number of geese of a particular species on the polygon the day before. <br>
Landscape::RecordGooseSpNumbers This records the number of geese of each species on the polygon the day before<br>
Landscape::RecordGooseNumbersTimed This records the number of geese on the polygon the day before at a predefined time <br>
Landscape::RecordGooseSpNumbersTimed This records the number of geese of each species on the polygon the day before at a predefined time <br>
Landscape::RecordGooseRoostDist Records the distance to the closest roost of a goose species<br>
Landscape::GetGooseNumbers This returns the number of geese on the polygon or location the day before<br>
Landscape::GetQuarryNumbers This returns the number of geese which are legal quarry on the polygon the day before <br>
Landscape::SubtractPondLarvalFood Removes larval food from a pond and returns true if it was possible, otherwise false <br>
Landscape::SetMaleNewtPresent Sets a male newt as being present in a pond <br>
Landscape::SupplyMaleNewtPresent Determines if a male newt is present in a pond <br>
Landscape::SkylarkEvaluation Runs through all polygons and passes them back to a skylark territory evaluation function<br>
Landscape::GetHareFoodQuality Returns the hare food quality of polygons <br>

### Methods related to mapping and map manipulations
Landscape::NewElement Creates a new LE instance and where possible it will create the highest level in the class hierarchy e.g. tole_Portarea has no behaviour so can be created as a NonVegElement (LE->NonVegElement), removing the need to have a Portarea class <br>
Landscape::RemoveMissingValues A method for replacing missing values in the map with corrected ones <br>
Landscape::PolysValidate Checks for internal consistency in the polygon information, e.g. being sure that all polygons mentioned are actually in the map<br>
Landscape::PolysRemoveInvalid Checks whether all polygons are in the map, removing those that are not<br>
Landscape::ConsolidatePolys Used to replace polygons or combine them when there is no special management associated with them. This just reduces the number of polygon entries and saves time and space.<br>
Landscape::CountMapSquares Calculates the area for each polygon in the map and stores it in LE::m_squares_in_map
Landscape::PolysRenumber Renumbers the polygons from 0 to the number of LE instances -1. This saves space, and also means that the polygon number becomes the same as the index in Landscape::m_elems, allowing quicker lookup (reduces indirection by 1) <br>
Landscape::ForceArea Is a check on polygon areas. Each polygon should be present in the map with the correct area recorded, otherwise this method raises an error<br>
Landscape::ChangeMapMapping Maps the polygon numbers directly to the indices in m_elems<br>
Landscape::RebuildPolyMapping Called if there is any change in the polygons and loops through all polygons resetting the Landscape::m_polymapping value <br>
Landscape::BorderNeed Currently unused test for adding borders<br>
Landscape::BorderAdd Adds a border around a field of the specified type<br>
Landscape::BorderRemoval Removes field boundaries, hedgebanks and hedges if they are adjacent to a field <br>
Landscape::RemoveSmallPolygons Removes single cell small polygons from the map<br>
Landscape::BorderScan Scans around the edge of a field adding border cells until the specified width is achieved. Uses Landscape::BorderStep<br>
Landscape::BorderStep Steps around the edge of a polygon adding a new border<br>
Landscape::UnsprayedMarginAdd Adds an unsprayed margin of specified width around a field<br>
Landscape::UnsprayedMarginScan Used by Landscape::UnsprayedMarginAdd to add a unsprayed margin 1m border around a field<br>
Landscape::BorderTest Used to decide if a coordinate location should become a border <br>
Landscape::UMarginTest Used to decide if a coordinate location should become an unsprayed marginr <br>
Landscape::FindValidXY Finds a valid coordinate for the polygon referenced by the polygon number <br>
Landscape::StepOneValid Unused. Checks that a location has all valid polygon references (9 cells)<br>
Landscape::AddBeetleBanks The starting point for adding beetlebanks to a field (can be used to add ‘banks’ of any tole_type <br>
Landscape::BeetleBankPossible Determines whether a beetlebank can be added to the field depending on physical characteristics<br>
Landscape::BeetleBankAdd Adds a beetlebank to the field<br>
Landscape::FindFieldCenter Finds a location in the middle of a field to ‘seed a beetlebank’ <br>
Landscape::FindLongestAxis From a central location finds the longest axis of a field <br>
Landscape::AxisLoop Tests the length of a vector to find when we step outside a given polygon<br>
Landscape::AxisLoopLtd Same as Landscape::AxisLoop, but with a test for the edge of the landscape map <br>
Landscape::SetPolyMaxMinExtents Sets a bounding rectangle enclosing the polygon and validates that the polygone is in the map.
Landscape:: SupplyMagicMapP Returns a pointer to the map at position x, y. The value found through it is an internal number from the landscape simulator, that uniquely identifies a polygon, it is not the polygon number - to find this apply MagicMap2PolyRef <br>
Landscape:: MagicMapP2PolyRef Converts the internal number generated from SupplyMagicMapP to the polygon number<br>
Landscape::hb_Add The entry point for adding hedgebanks. Hedgebanks are created by replacing part of hedge polygons with the #tole_HedgeBank If the hedge is wide this will be with a border on each side, if narrow then by inserting hedgebank between trees<br>
Landscape::hb_AddNewHedgebanks(int a_orig_poly_num) <br>
Landscape::hb_StripingDist(void) <br>
Landscape::hb_GenerateHBPolys Creates the necessary new hedgebank polygons in Landscape::m_elems <br>
Landscape::hb_FindHedges Generate a list of polygon numbers for new hedgebank addition <br>
Landscape::hb_FindBoundingBox Finds a rectangle that encloses the hedge to be manipulated <br>
Landscape::hb_UpPolyNumbers Adds a big number to all polygon refs in the map to create space to safely manipulate the hedge pixels <br>
Landscape::hb_ClearPolygon Replaces all values in the map for a polygon with a temporary value ready for manipulation <br>
Landscape::hb_PaintBorder Identifies all border hedge pixels <br>
Landscape::hb_MapBorder Checks for out of bounds hedgebank coordinates<br>
Landscape::hb_HasOtherNeighbour Checks if a pixel has any non hedge neighbour cell <br>
Landscape::hb_PaintWhoHasNeighbourColor Pushes a value to a pixel cell that denotes that is has a neighbour that is not hedge <br>
Landscape::hb_HasNeighbourColor Tests for an already process neighbour cell <br>
Landscape::hb_MarkTopFromLocalMax Used to identify the distance a cell is from the centre of a hedge <br>
Landscape::hb_MarkTheBresenhamWay ‘Paints’ the core of a hedge with a negative number for use in deciding what becomes hedgebank later <br>
Landscape::hb_MaxUnpaintedNegNeighbour Determines the width from centre to edge of hedge <br>
Landscape::hb_ResetColorBits Remove high number colour bits from the map <br>
Landscape::hb_RestoreHedgeCore(int a_orig_poly_number) <br>
Landscape::hb_DownPolyNumbers Reverses the process carried out by Landscape::hb_UpPolyNumbers <br>
Landscape::DumpMapGraphics(const char* a_filename) <br>
Landscape::hb_dump_map  Debug to save the hb map <br>
Landscape::hb_dump_color Debug to save the hb intermediate map <br>

### Other Methods
These methods miscellaneous methods
Landscape::SupplyVersion Returns the Landscape version info <br>
Landscape::CorrectCoords Function to prevent wrap around errors with co-ordinates using x/y pair <br>
Landscape::CorrectCoordsPt Returns a APoint object with coordinates corrected for wrap around <br>
Landscape::CorrectCoordsPointNoWrap Function to prevent wrap around errors with co-ordinates using x/y pair, but by forcing points outside the map to the border, not wrapping around <br>
Landscape::CorrectWidth Prevents an x-coordinate from being outside the landscape using wrap around <br>
Landscape::CorrectHeight Prevents a y-coordinate from being outside the landscape using wrap around <br>
Landscape::CalculateCentroids Finds a location inside each landscape polygon as a roughly calculated centre point and stores this inside each LE instance <br>
Landscape::DumpCentroids Saves centroid information for each polygon found
Landscape::BuildingDesignationCalc Used to calculate whether a building as rural or town - for rodenticide use
Landscape::CentroidSpiralOut Function to move from midx & midy and outwards in concentric circles until a location that matches the polyref is found <br>
Landscape::CreatePondList Creates a list of pond polygon refs/indexes for easy look up <br>
Landscape::CheckForPesticideRecord Check if needed and record pesticide application <br>
Landscape::ResetGrainAndMaize Resets all grain and maize values for polygons<bZ>
Landscape::CalculateOpenness Causes openness to be calculated and stored for all polygons<br>
Landscape::CalculateFieldOpennessCentroid Provides a measure of the shortest distance in 360 degree, e-g- looking NE % SW before tall obstacles are encountered at both ends. Searches from centroid outwards <br>
Landscape::CalculateFieldOpennessAllCells Provides a measure of the shortest distance in 360 degree, e-g- looking NE % SW before tall obstacles are encountered at both ends. Checks all field 1m2<br>
Landscape::LineHighTest Provides a measure of the shortest distance in using a vector from a_cx,a_cy unitl tall obstacles are encountered in both +ve & -ve directions<br>
Landscape::GetAllVegPolys Returns a list of all the polygons that have LE types derived from VegElement <br>
Landscape::SimulationClosingActions Things to do when closing the simulation. Currently unused<br>
Landscape::IncTreatCounter Records that a farm treatment of the type specified has occurred <br>
Landscape::FillVegAreaData Runs through all polygons and records the area of each vegetation type<br>
Landscape::GetVegArea Returns the area covered by the specified vegetation <br>
Landscape::SupplyRodenticidePredatoryManager Returns the pointer to the RodenticidePredators_Population_Manager Landscape::m_RodenticidePreds <br>
Landscape::RodenticidePredatorsEvaluation Runs through all polygons and passes them back to a rodenticide predators evaluation function<br>
Landscape::SupplyThePopManagerList Get the pointer to the list of active population managers <br>
Landscape::SetThePopManagerList Set the pointer to the list of active population managers <br>
Landscape::DistanceToP Returns the distance in m between two points<br>
Landscape::DistanceToPSquared Returns the distance in meters squared between two points<br>
Landscape::SupplyLEReset Reset internal loop counter, used to intialise polygon data in the constructor <br>
Landscape::SupplyLENext Returns -1 at if the end of the list of m_elems is reached, polyref otherwise <br>
Landscape::SupplyLECountReturns number of all landscape elements<br>

This set of methods are all used to determine characteristics of a landscape polygon based on its element type:<br>
  - Landscape::ClassificationHigh Returns whether the polygon is classfied as high <br>
  - Landscape::ClassificationWater Returns whether the polygon is classfied as water <br>
  - Landscape::ClassificationFieldType Returns whether the polygon is classfied as a field <br>
  - Landscape::ClassificationUrbanNoVeg Returns whether the polygon is classfied as urban with no vegetation  <br>
  - Landscape::ClassificationForest Returns whether the polygon is classfied as woodland <br>
  - Landscape::ClassificationWoody Returns whether the polygon is classfied as woody <br>

This set of methods are all used to determine characteristics of a landscape polygon based on individual characteristics set using attributes:<br>
  -   Landscape::SupplyAttIsHigh Returns whether a polygon at coordinates a_x, a_y has the attribute High set <br>
  - 	Landscape::SupplyAttIsWater Returns whether a polygon at coordinates a_x, a_y has the attribute Water set <br>
  - 	Landscape::SupplyAttIsForest Returns whether a polygon at coordinates a_x, a_y has the attribute Forest set <br>
  - 	Landscape::SupplyAttIsWoody Returns whether a polygon at coordinates a_x, a_y has the attribute Woody set <br>
  - 	Landscape::SupplyAttIsUrbanNoVeg Returns whether a polygon at coordinates a_x, a_y, or by a_polyref has the attribute High set <br>
  - 	Landscape::SupplyAttIsVegPatchy Returns whether a polygon at coordinates a_x, a_y, or by a_polyref has the attribute Is Patchy set <br>
  - 	Landscape::SupplyAttIsVegCereal Returns whether a polygon at coordinates a_x, a_y, or by a_polyref has the attribute Is Cereal set <br>
  - 	Landscape::SupplyAttIsVegMatureCereal Returns whether a polygon at coordinates a_x, a_y, or by a_polyref has the attribute Is Mature Cereal set <br>
  - 	Landscape::SupplyAttIsVegGrass Returns whether a polygon a_polyref has the attribute Is Grass set <br>
  - 	Landscape::SupplyAttIsVegGooseGrass Returns whether a polygon a_polyref has the attribute Is Goose Grass set <br>
  - 	Landscape::SupplyAttIsVegMaize Returns whether a polygon a_polyref has the attribute Is Maize set <br>
  -   Landscape::SupplyAttUserDefinedBool Returns the user defined boolean attribute of a polygon using the polygon reference number a_polyref, or coordinates a_x, a_y <br>
  - 	Landscape::SupplyAttUserDefinedInt Returns the user defined integer attribute of a polygon using the polygon reference number a_polyref <br>
  - 	Landscape::SetAttUserDefinedBool Sets the user defined boolean attribute of a polygon using the polygon reference number a_polyref <br>
  - 	Landscape::SetAttUserDefinedInt Sets the user defined integer attribute of a polygon using the polygon reference number a_polyref <br>
These four functions are just for backwards code compatibility when attributes would now be used: 	Landscape::SupplyLEHigh	Landscape::IsFieldType 	Landscape::SupplyIsCereal2	Landscape::SupplyIsGrass2<br>


#### I/O
Landscape::PolysDump Called if the landscape is manipulated to save the new version of the polyref input file<br>
Landscape::DumpMap Called if the landscape is manipulated to save the new version of the .lsb input file (i.e. the topographical map) <br>
Landscape::Warn A wrapper for the g_msg Warn function. Used to pass program warnings for debug <br>
Landscape::WriteOpenness Stores openness for all polygons to a standard file<br>
Landscape::ReadOpenness Reads openness values from a standard input file for all polygons<br>
Landscape::VegDump Records vegetation characteristics for the x,y location <br>
Landscape::RecordEvent Optional method which can be turned on through a #define __RECORDFARMEVENTS to record details of farm events carried out.  <br>
Landscape::EventDump Records farm events carried out on the x,y locations <br>
Landscape::EventDumpPesticides Records pesticide application farm events carried out on the x,y location <br>
Landscape::DegreesDump brief Prints the sum of day degrees. See #FarmManager::daydegrees
Landscape::GISASCII_Output Saves the landscape map as a GIS readable ASCII file<br>
Landscape::GrainDump Records total amount of grain/seed/maize available as forage for each polygon<br>
Landscape::DumpVegAreaData Saves the information on vegetation types and areas <br>
Landscape::DumpTreatCounters When the simulation is closed this method saves the number of each farm management that has been carried out <br>
Landscape::EventtypeToString Returns the text representation of a farm treatment type <br>
Landscape::PolytypeToStringReturns the text representation of a TTypesOfLandscapeElement type <br>
Landscape::VegtypeToString Returns the text representation of a TTypesOfVegetation type <br>
Landscape::FarmtypeToString Returns the text name of a TTypesOfFarm type <br>
Landscape::SupplyLESignal Only used for hard code modifications, unused in release. Gets a signal mask that denotes logical I/O that is specified e.g. #LE_SIG_NO_INSECTICIDE to turn insecticide application off <br>
Landscape::SetLESignal Only used for hard code modifications, unused in release. Sets a signal mask that denotes logical I/O that is specified e.g. #LE_SIG_NO_INSECTICIDE to turn insecticide application off <br>
Landscape::DumpPublicSymbols Dumps all configuration values with a security level at or below a_level to a_dumpfile in alphabetical order <br>
Landscape::DumpAllSymbolsAndExitDumps all configuration values, including private ones, to a_dumpfile and then calls exit() <br>
Landscape::ReadSymbols Reads and parses a_cfgfile for configuration values to set up the main parameter input for ALMaSS <br>
Landscape::DumpMapInfoByArea Writes the areas of every vegetation type to an output file <br>

\anchors _lsm3
## LE & Associated Classes

\anchors _lsm2
## Farms and farm management, including crops

\anchors _lsm4
## Pesticide and related classes

# Interconnections

# I/O, Parameters and Scales
## Inputs
## Outputs
## State variables
## Scales

# Discussion of implementation

# References
