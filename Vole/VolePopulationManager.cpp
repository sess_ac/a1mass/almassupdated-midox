/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>VolePopulationManager.cpp This file contains the source for the vole population manager class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 28th Jan 2001 \n
 \n
 With additions as noted in: \n
 April 2006 \n
 November 2007 \n
 Doxygen formatted comments in May 2008 \n
*/
//---------------------------------------------------------------------------

#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../Vole/vole_all.h"
#include "../Vole/VolePopulationManager.h"
#include "../Vole/GeneticMaterial.h"
#include "../Vole/Predators.h"
#include "../Vole/Vole_toletov.h"

extern double MutationChance;


//---------------------------------------------------------------------------

/* External configuration variables related to outputs */
/** The first year for catastrophes */
extern CfgInt cfg_CatastropheEventStartYear;
/** The frequency of catastrophes */
extern CfgInt cfg_pm_eventfrequency;
/** The size of catastrophes */
extern CfgInt cfg_pm_eventsize;
/** Raw data for spatial output for calculating Ripley's spatial statistics */
extern CfgBool cfg_ReallyBigOutputMonthly_used;
extern CfgBool cfg_RipleysOutput_used;
/** Raw data for spatial output for calculating Ripley's spatial statistics with additional information about habitat and physiology */
extern CfgBool cfg_ReallyBigOutput_used;
/** Extra mortality for dispersing individuals*/
extern CfgFloat cfg_extradispmort;
extern double g_extradispmort;
/** risk of daily mortality*/
extern double g_DailyMortChance;
extern double g_DailyMortChanceMaleTerr;
extern double g_NoFemalesMove;
extern CfgInt cfg_VoleDDepConst;
extern int g_sigAgeDiff;
extern int MinReproAgeM;
extern int MinReproAgeF;
extern CfgInt cfg_MinReproAgeM;
extern CfgInt cfg_MinReproAgeF;
extern unsigned char g_MaxAllele;

//---------------------------------------------------------------------------

// End of breeding season
static CfgInt cfg_vole_reprofinishdate("VOLE_REPROFINISHDATE",CFG_CUSTOM, 243); // September 1st
// VoleSampleFile for the vole
static CfgBool cfg_VoleSampleDataUsed("VOLE_SAMPLE_FILE_USED", CFG_CUSTOM, false);
//First sampling day for vole samplefile
static CfgInt cfg_VoleSampleFileDayOne("VOLE_SAMPLE_FILE_DAY_ONE", CFG_CUSTOM,109);
//Second sampling day for vole samplefile
static CfgInt cfg_VoleSampleFileDayTwo("VOLE_SAMPLE_FILE_DAY_TWO", CFG_CUSTOM,287);
//Whether a vole will move if no females present
static CfgFloat cfg_VoleFemaleMove("VOLE_FEMALEMOVE", CFG_CUSTOM,0.0505);
// Radius whithin which the female picks a mate
static CfgInt cfg_MateRadius("FEMALE_MATE_RADIUS", CFG_CUSTOM, 250);
/** The unspecified background mortality rate */
static CfgFloat cfg_VoleBackgroundMort("VOLE_BCKMORT",CFG_CUSTOM,0.0025);
/** The probability of moving to less favourable habitats when walking */
static CfgFloat cfg_MoveToLessFavourable("VOLEMOVETOLESSFAVOURALBLE",CFG_CUSTOM, 0.005);
/** The age difference before an old male can oust a younger one */
static CfgInt cfg_sigAgeDiff("VOLE_SIGAGEDIFF",CFG_CUSTOM,30);
/** Toxicological variable for specific pesticide effects */
static CfgInt cfg_geneticproductfertilityeffect("VOLE_GENETICPRODUCTFERTILITYEFFECT",CFG_CUSTOM,50);
/** Toxicological variable for specific pesticide effects */
static CfgFloat cfg_geneticsterilitychance("VOLE_GENETICSTERILITYCHANCE",CFG_CUSTOM,0);
/** Toxicological variable for specific pesticide effects */
static CfgFloat cfg_f1sterilitychance("VOLE_FONESTERILITYCHANCE",CFG_CUSTOM,0.0);
/** Record genetic data or not */
static CfgBool cfg_genetic_output("VOLE_GENETIC_OUTPUT",CFG_CUSTOM,true);
/** The number of each sex to sample for genetics */
static CfgInt cfg_genetetic_output_sample_size("VOLE_GENETIC_SAMPLE_SIZE",CFG_CUSTOM,500);
// GeneticOutput variable for the first year the file should be produced
static CfgInt cfg_GeneticsResultOutputFirstYear("GENETICS_RESULT_OUTPUT_FIRST_YEAR",CFG_CUSTOM,100000);
// GeneticOutput variable for the start year in the first period
static CfgInt cfg_GeneticResultOutputSecondYear("GENETICS_RESULT_OUTPUT_SECOND_YEAR",CFG_CUSTOM,100000);
// GeneticOutput variable for the start year in the second period
CfgBool cfg_GeneticsResultOutputUsed( "GENETICS_RESULT_OUTPUT_USED", CFG_CUSTOM, false );
static CfgInt cfg_GeneticsResultOutputInterval_1("GENETICS_RESULT_OUTPUT_INTERVAL_ONE",CFG_CUSTOM,100000);
// GeneticOutput variable for the first interval the file should be produced
static CfgInt cfg_GeneticsResultOutputDay_1("GENETICS_RESULT_OUTPUT_DAY_ONE",CFG_CUSTOM,3);
// GeneticOutput variable for the second day of the year the file should be produced
static CfgInt cfg_GeneticsResultOutputDay_2("GENETICS_RESULT_OUTPUT_DAY_TWO",CFG_CUSTOM,6);
// GeneticOutput variable for the when the first interval should end

/** Flag turning resistance output on or off */
static CfgBool cfg_VoleUseResistanceOuput("VOLE_RESISTANCEOUTPUT_ON_OFF",CFG_CUSTOM, false );
/** The day in the year to output the resitance gene frequency */
static CfgInt cfg_VoleResistanceDay("VOLE_RESISTANCEOUTPUT_DAY",CFG_CUSTOM, 365 );
/** The starting frequency of '1' in chromosome 0 locus no 3 */
static CfgFloat cfg_ResistanceStartFrequency("VOLE_RESISTANCESTARTFREQ",CFG_CUSTOM,0.01);

static CfgBool cfg_UseVoleTraplines("VOLE_TRAPLINES_ON_OFF",CFG_CUSTOM, false );
static CfgStr cfg_VoleTraplinesfile("VOLE_TRAPLINESFILE",CFG_CUSTOM, "VoleTraplines.txt");
static CfgInt cfg_VoleTrapResolution("VOLE_TRAPRESOLUTION",CFG_CUSTOM, 2);

CfgBool cfg_RecordVoleMort("VOLE_RECORDMORT_ON",CFG_CUSTOM, false );
static CfgStr cfg_VoleRecordMortFile("VOLE_RECORDMORTFILE",CFG_CUSTOM, "VoleMortalityCauses.txt");

CfgBool cfg_SexRatiosOutput_used( "VOLE_SEXRATIOSOUTPUT_USED", CFG_CUSTOM, false );
static CfgStr cfg_SexRatiosOutput_filename("VOLE_SEXRATIOS_FILENAME",CFG_CUSTOM,"VoleSexRatios.txt");
static CfgInt cfg_SexRatiosOutput_interval( "VOLE_SEXRATIOSOUTPUT_INTERVAL", CFG_CUSTOM, 1 );
static CfgInt cfg_SexRatiosOutput_day( "VOLE_SEXRATIOSOUTPUT_DAY", CFG_CUSTOM, -1 ); // -1 means daily
static CfgInt cfg_SexRatiosOutputFirstYear("VOLE_SEXRATIOSOUTPUT_FIRSTYEAR",CFG_CUSTOM,1);
static CfgBool cfg_voleLandscapeGridOutputUsed("VOLE_LANDSCAPEGRIDOUTPUTUSED",CFG_CUSTOM, true);
static CfgInt cfg_voleLandscapeGridOutputDay("VOLE_LANDSCAPEGRIDOUTPUTDAY",CFG_CUSTOM, 10000);
static CfgBool cfg_volestartinoptimalonly("VOLE_STARTINOPTIMALONLY",CFG_CUSTOM, false);


/** This is specified as a global because it avoids costing time on look-ups. \n
 This is needed because of the extreme CPU intensive movement functions
*/
double MoveToLessFavourable;

/** How many male voles to start with */
static CfgInt  cfg_vole_starting_numberM( "VOLE_START_NO_M", CFG_CUSTOM,5000 );
/** How many female voles to start with */
static CfgInt  cfg_vole_starting_numberF( "VOLE_START_NO_F", CFG_CUSTOM,5000 );
/** The mean temperature over 14 days at which the grass is assummed to start to grow */
static CfgFloat  cfg_GrassStartGrowth( "VOLE_GRASSSTARTGROWTH",CFG_CUSTOM,3.552 );
/** The date before which repro is impossible */
static CfgFloat  cfg_GrassStartGrowthDay( "VOLE_GRASSSTARTGROWTHDAY",CFG_CUSTOM,80 );
/** Special output file toggle */
static CfgBool cfg_useagesexlocation("VOLE_USEAGESEXLOCATION",CFG_CUSTOM,false);

/** Litter sizes dependent upon month and year */
const float ReproTable_base [2][12] = { {0,0,0,0,4,4,4,4,3,2,3,0},// Born This year
                                 {0,0,4,4,5,5,5,5,4,4,0,0} // Born Last year
                               };  // data from Andrea (1981)

/** Mutation probability */
static CfgFloat  cfg_MutationChance("GENETICS_MUTATION_CHANCE", CFG_CUSTOM, 0.001);
/** The maximum allele value used */
static CfgInt cfg_MaxAllele("GENETICS_MAXALLELE",CFG_CUSTOM,255);

extern int g_MaleReproductFinish;
extern CfgInt cfg_MinFemaleTerritorySize;
extern CfgInt cfg_MinMaleTerritorySize;
extern CfgBool cfg_ResistanceDominant;

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//                      Vole_Population_Manager
//---------------------------------------------------------------------------


/**
Class for managing lists of all voles in the simulation and responsible for generating output.
Main functionality is the the base class Population_Manager
*/
Vole_Population_Manager::Vole_Population_Manager(Landscape* p_L)
	: Population_Manager(p_L, 2)
{
	// two lists are needed so need to remove 8 of the ten default arrays
	m_ListNames[0] = "Male";
	m_ListNames[1] = "Female";
	m_ListNameLength = 2;
	IDNumber = 0;
	thisYear = 0;
	YearsTotal = 0;
	m_VoleMap = new IDMap<TAnimal*>(p_L);
	AFreq = new AlleleFreq();
	m_GrowthStartDate = 9000; // delays this until we know the real one
	Init();
}
//---------------------------------------------------------------------------

/**
Vole_Population_Manager destructor \n
Only needed to close any potentially open output files
*/
Vole_Population_Manager::~Vole_Population_Manager()
{
  // Save the impacted file
  //errno_t errNum;
	// Output files for the vole
	//fclose( m_GeneticsFile );
	//fclose( m_AlleleFreqsFile );
	//fclose( m_EasyPopRes );
	if (cfg_SexRatiosOutput_used.value()) CloseSexRatiosProbe();
	delete m_VoleMap;
	delete AFreq;
	// Vole traplines tidy-up
	if (cfg_UseVoleTraplines.value()) {
		delete m_Traplines;
	}
	if (cfg_RecordVoleMort.value()) {
		m_VoleRecordMort->OPrint();
		delete m_VoleRecordMort;
	}
	if (cfg_useagesexlocation.value())
	{
		(*m_VoleAgeSexLocationFile).close();
	}
	if (cfg_VoleUseResistanceOuput.value()) CloseResistanceOutput();
}
//---------------------------------------------------------------------------

void Vole_Population_Manager::Init()
{
  /** autom. called by constructor. \n
  Sets up output files and creates initial population

*/
	// Need to resize out vole habitat qualities vector to the correct size for the number of polygons
	// NB IF the landscape were to change the number of polygons due to morphing (not implemented currently), then
	// this code is broken.
	unsigned int p = m_TheLandscape->SupplyNumberOfPolygons();
	VoleHabitatBaseQualities.resize(p);
	//// Initialise the vole grazing data
	//m_TheLandscape->ResetAllVoleGrazing();
	// Set up output files
	if (cfg_voleLandscapeGridOutputUsed.value())
	{
		ofstream ofile("VoleLandscapeGridData.txt",ios::out);
		ofile.close();
	}
	if ( cfg_RipleysOutput_used.value() ) {
		OpenTheRipleysOutputProbe("");
	}
	if ( cfg_ReallyBigOutput_used.value() ) {
		OpenTheReallyBigProbe();
	} else ReallyBigOutputPrb=0;
	if (cfg_SexRatiosOutput_used.value()) OpenSexRatiosProbe();
	FILE* GFile = fopen("VoleGeneticsOutput.txt", "w" );
	if ( !GFile ) {
		g_msg->Warn( WARN_FILE, "Population_Manager::Init(): ""Unable to open VoleGeneticsOutput.txt","");
		exit( 1 );
	}
	if (cfg_useagesexlocation.value())
	{
		m_VoleAgeSexLocationFile = new ofstream("VoleAgeSexLocation.txt", ios::out);
	}
	// Pass some parameters to member variables (speed optimisation)
	g_MaleReproductFinish = cfg_vole_reprofinishdate.value();
	g_extradispmort = cfg_extradispmort.value();
	g_DailyMortChance = cfg_VoleBackgroundMort.value();
	g_NoFemalesMove = cfg_VoleFemaleMove.value();
	//m_BaseVoleDensity = 1/(cfg_MinFemaleTerritorySize.value() * 4.0) * cfg_VoleDDepConst.value();
	g_sigAgeDiff  = cfg_sigAgeDiff.value();
	MinReproAgeM=cfg_MinReproAgeM.value();
	MinReproAgeF=cfg_MinReproAgeF.value();
#ifdef __VOLEPESTICIDEON
  /** Four memeber variables Used in pesticide simulations */
  m_impacted = 0;
  m_notimpacted = 0;
  m_geneticimpacted = 0;
  m_LittersLost = 0;
  m_LittersProduced = 0;
  FILE* impf;
  impf = fopen("Impacted.res","w");
  if (!impf) {
	  m_TheLandscape->Warn("Vole_Population_Manager Destructor","Could Not Open Impacted.Res File");
	  exit(0);
  }
  fclose(impf);
#endif
  /*
   Create probabilities for ReproTable

   ReproTable is two years each with 12 numbers representing the minimum litter size in the 0,1 position and in 2,3 positions then it is the number out of 100 that have one more than the first number\n
   Currently this is set to no variation and litter sizes follow Andrea (1981).
  */
  for (int i = 0; i < 12; i++)
  {
     ReproTable[0][i] = (int)floor(ReproTable_base[0][i]);
     ReproTable[2][i] = (int)floor(0.5+(ReproTable_base[0][i]*100)-(ReproTable[0][i]*100));
     ReproTable[1][i] = (int)floor(ReproTable_base[1][i]);
     ReproTable[3][i] = (int)floor(0.5+(ReproTable_base[1][i]*100)-(ReproTable[1][i]*100));
  }
  m_SimulationName ="Field Vole";
  // Create males and females
  struct_Vole_Adult* av;
  av = new struct_Vole_Adult;
  av->VPM = this;
  av->L = m_TheLandscape;
  MutationChance = (cfg_MutationChance.value());
  g_MaxAllele = (unsigned char) cfg_MaxAllele.value();

  GeneticMaterial AGene;
  for (int i=0; i<cfg_vole_starting_numberM.value(); i++)  //Initial creation of males
  {
	  AGene.Initiation(AFreq);
	  if (m_TheLandscape->SupplyPesticideType() !=-1) // No big time penalty here, so we use this rather than the #ifdef
	  {
		  // If any pesticide is being used then we assume genetics is only used to control this - so make sure there is no
		  // pesticide influenced genetetic code set (ie put a zero in 0 allele, of both chromosomes
		  if (m_TheLandscape->SupplyPesticideType() ==5)
		  {
			  if (g_rand_uni()< cfg_ResistanceStartFrequency.value()) AGene.SetAllele(3,1,0); else AGene.SetAllele(3,0,0);
			  if (g_rand_uni()< cfg_ResistanceStartFrequency.value()) AGene.SetAllele(3,1,1); else AGene.SetAllele(3,0,1);
		  }
		  AGene.SetAllele(0,0,0);
		  AGene.SetAllele(0,0,1);
	  }
     av->Genes=AGene;
      bool found = false;
	  while (!found)
	  {
		  av->x = random(SimW);
		  av->y = random(SimH);
		  found = SuitableStartingLocation(av->x, av->y);
	  }
      CreateObjects_Init(vob_Male,NULL,av,1);
  }

  for (int i=0; i<cfg_vole_starting_numberF.value(); i++) //initial creation of females
  {
	  AGene.Initiation(AFreq);
	  if (m_TheLandscape->SupplyPesticideType() !=-1) // No big time penalty here, so we use this rather than the #ifdef
	  {
		  // If any pesticide is being used then we assume genetics is only used to control this - so make sure there is no
		  // pesticide influenced genetetic code set (ie put a zero in 0 allele, of both chromosomes
		  if (m_TheLandscape->SupplyPesticideType() ==5)
		  {
			  if (g_rand_uni()< cfg_ResistanceStartFrequency.value()) AGene.SetAllele(3,1,0); else AGene.SetAllele(3,0,0);
			  if (g_rand_uni()< cfg_ResistanceStartFrequency.value()) AGene.SetAllele(3,1,1); else AGene.SetAllele(3,0,1);
		  }
		  AGene.SetAllele(0,0,0);
		  AGene.SetAllele(0,0,1);
	  }
      av->Genes=AGene; // Their genes are set based on the Initiation (AlFreq) function
      bool found = false;
	  while (!found)
	  {
		  av->x = random(SimW);
		  av->y = random(SimH);
		  found = SuitableStartingLocation(av->x, av->y);
	  }
      CreateObjects_Init(vob_Female,NULL,av,1);
  }
  delete av;


  // Load List of Animal Classes
  m_ListNames[0] = "Juvenile Male";
  m_ListNames[1] = "Juvenile Female";
  m_ListNames[2] = "Male";
  m_ListNames[3] = "Female";
  m_ListNameLength = 4;
  m_population_type = TOP_Vole;

  // Set up before step action sorts
  // sort w.r.t. x
  BeforeStepActions[0] = 0; // SortXIndex
  BeforeStepActions[1] = 0; // SortXIndex

  // Load State Names
  StateNames[tovs_InitialState] = "Initial State";
  //Males
  StateNames[tovs_JuvenileExploration] = "Juvenile Exploration";
  StateNames[tovs_MMaturation] = "Maturation";
  StateNames[tovs_MEvaluateExplore] = "Evaluate & Explore";
  StateNames[tovs_Infanticide] = "Infanticide";
  StateNames[tovs_MDying] = "Dying";
  //Females
  StateNames[tovs_FEvaluateExplore] = "Evaluate & Explore";
  StateNames[tovs_ReproBehaviour] = "Repro. Behaviour";
  StateNames[tovs_Lactating] = "Lactating";
  StateNames[tovs_GiveBirth] = "Give Birth";
  StateNames[tovs_FMaturation] = "Maturation";
  StateNames[tovs_Mating] = "Mating";
  StateNames[tovs_UpdateGestation] = "Update Gestation";
  StateNames[tovs_SpecialExplore] = "Special Explore";
  StateNames[tovs_FDying] = "Dying";

  MoveToLessFavourable=cfg_MoveToLessFavourable.value();
  // OPEN THE TEST FILES _ THIS CODE CAN BE COMMENTED OUT IF THEY ARE NOT NEEDED - TODO TIDY THIS UP
  TestFile = fopen("TestFile.Txt", "w" );
  if (!TestFile) {
	  m_TheLandscape->Warn("Population_Manager::Population_Manager -", "Could Not Open TestFile.txt");
	  exit(0);
  }
  TestFile2 = fopen("TestFile2.Txt", "w" );
  m_StepSize = 1440; // Default - it is up to the individual animal models to
  if (!TestFile2) {
	  m_TheLandscape->Warn("Population_Manager::Population_Manager -", "Could Not Open TestFile2.txt");
	  exit(0);
  }
#ifdef __VOLEPESTICIDEON
  /** use for pesticide simulations only */
  m_geneticproductfertilityeffect = cfg_geneticproductfertilityeffect.value();
  /** use for pesticide simulations only */
  m_geneticsterilitychance = cfg_geneticsterilitychance.value();
  /** use for pesticide simulations only */
  m_f1sterilitychance = cfg_f1sterilitychance.value();
// Other output files
#endif
  char Nme[ 511 ];
    //strcpy( Nme, "GeneticsOutput.txt" );
   // m_GeneticsFile = fopen( Nme, "w" );
    //strcpy( Nme, "AlleleFreqs.txt" );
    //m_AlleleFreqsFile = fopen( Nme, "w" );
    //strcpy( Nme, "forEASYPOP.txt" );
    //m_EasyPopRes = fopen( Nme, "w" );

	// Vole traplines set-up
	if (cfg_UseVoleTraplines.value()) {
		m_Traplines = new TrapLineMap( SimW, SimH, cfg_VoleTrapResolution.value(), cfg_VoleTraplinesfile.value() );
	}
	// Vole record mortality set-up
	if (cfg_RecordVoleMort.value()) {
		// The two ints in the constructor call refer to the number of int data and double data used respectively
		// The output will print ints first then doubles
		m_VoleRecordMort = new VoleSummaryOutput(cfg_VoleRecordMortFile.value(), m_TheLandscape, 14, 0);
		// In the string output allows us to provide titles:
		m_VoleRecordMort->OPrint("Year\tDay\tMStarve\tFStarve\tMBck\tFBck\tMFarm\tFFarm\tMDisp\tFDisp\tMPred\tFPred\tMLife\tFLife\tMPest\tFPest\t");
		m_VoleRecordMort->OPrintEndl();
	}
	if (cfg_VoleUseResistanceOuput.value()) OpenResistanceOutput();
 }
//---------------------------------------------------------------------------

bool Vole_Population_Manager::SuitableStartingLocation(int x,  int y)
{
/**
* Two verisons of this exist, the first only lets voles start in good places, the second also allows fields and orchards.
*/
	if (cfg_volestartinoptimalonly.value())
	{
		return vole_tole_init_optimal(m_TheLandscape, x, y);
	}
	else
	{
		return vole_tole_init_friendly(m_TheLandscape, x, y);
	}
}
//---------------------------------------------------------------------------

/** use for pesticide simulations only to determine the number of individual males currently impacted by pesticide
    Added April 2006
*/
void Vole_Population_Manager::ImpactedProbe( ) {
	FILE* MyFile;
	MyFile=fopen("VoleImpactedProbe.txt","a");
	if (MyFile == NULL){
	m_TheLandscape->Warn("Vole_Population_Manager::ImpactedProbe","Could Not Open VoleImpactedProbe.txt File");
	exit(0);
	}
	// This just needs to trawl through the males and to count how many are affected by
	// a pesticide.  This function needs to be redefined for every new pesticide case
	// Current implementation is for Trine Dalkvists 'vinclozolin' work
    int dno=0;
    int gno=0;
	Vole_Male* vm;
    unsigned size=(unsigned)GetLiveArraySize(vob_Male);
	for (unsigned j=0; j<size; j++) {
		vm=dynamic_cast<Vole_Male*>(SupplyAnimalPtr(vob_Male, j));
		if (vm->GetGeneticFlag()!=0) gno++;
		if (vm->GetDirectFlag()!=0) dno++;
	}
	int tno=size;
	fprintf(MyFile,"%d\t%d\t%d\n",tno,dno,gno);
	fclose(MyFile);

}
//-------------------------------------------------------------------------------------------

void Vole_Population_Manager::DoFirst()
{
/**
This method is called before the BeginStep of the main program loop by the base class. \n
It controls when the grass starts to grow and therefore triggers start of reproduction but
the primary role is as a safe location where outputs can be generated. At the time this is
called there no chance that the voles are engaged in any kind of activity.
*/
/**
* First job is to calculate a basic quality for each polygon. This is more efficient than getting the voles to do the
* calculation each time they need to know.
*/


	//for (int type = 0; type<vob_foobar; type++)
	//{
	//	int s = (int) TheArray[type].size();
	//	for (int i = 0; i<s; i++) m_TheLandscape->IncVoleGrazing(TheArray[type][i]->Supply_m_Location_x(), TheArray[type][i]->Supply_m_Location_y());
	//}
	//m_TheLandscape->CalcAllVoleGrazingDensity();
	unsigned sz = m_TheLandscape->SupplyNumberOfPolygons();
	for (unsigned p = 0; p<sz; p++)
	{
		double qual = AssessHabitat(p);
		//double volegrazing = m_TheLandscape->SupplyVoleGrazingDensityVector(p);
		//if (volegrazing<m_BaseVoleDensity) volegrazing = m_BaseVoleDensity;
		//VoleHabitatBaseQualities[p] = qual / (volegrazing/m_BaseVoleDensity);
		VoleHabitatBaseQualities[p] = qual;
	}


	int year = m_TheLandscape->SupplyYearNumber();
	if (year > -1)
	{
		g_DailyMortChance = cfg_VoleBackgroundMort.value();
		g_extradispmort = cfg_extradispmort.value();
	}
	else
	{
		g_DailyMortChance = 0;
		g_extradispmort = 0;
	}

  int today=m_TheLandscape->SupplyDayInYear();
  if (today==0) {
	  m_GrowthStartDate=9000;
  }
  if ((today>cfg_GrassStartGrowthDay.value())&& (m_GrowthStartDate==9000))
  {
    if (m_TheLandscape->SupplyMeanTemp(m_TheLandscape->SupplyGlobalDate()-7,7)
                                                 > cfg_GrassStartGrowth.value())
    {
      m_GrowthStartDate = today;
      printf("The start day is: %d\n",today);
    }
  }
	if (GetLiveArraySize(vob_Female) > 0)
	{
		if ((today>SupplyGrowthStartDate())&&(today<=g_MaleReproductFinish))
		{
			dynamic_cast<Vole_Female*>(SupplyAnimalPtr(vob_Female, 0))->SetBreedingSeason( true );
			g_DailyMortChanceMaleTerr = g_DailyMortChance/2.0;
		}
		else
		{
			dynamic_cast<Vole_Female*>(SupplyAnimalPtr(vob_Female, 0))->SetBreedingSeason( false );
			// Below could be used to alter mortality outside breeding season - currently unused (did not help POM fit)
			g_DailyMortChanceMaleTerr = g_DailyMortChance/2.0;
		}
	};


  //** OUTPUT BELOW HERE **
  if (cfg_SexRatiosOutput_used.value()) {
	  if (year>=cfg_SexRatiosOutputFirstYear.value()) {
		  if ( year % cfg_SexRatiosOutput_interval.value() == 0 ) {
			  if (( cfg_SexRatiosOutput_day.value() == today )||( cfg_SexRatiosOutput_day.value() == -1 )) {
				  TheSexRatiosProbe();
			  }
		  }
	  }
  }
  if (cfg_voleLandscapeGridOutputDay.value() == m_TheLandscape->SupplyDayInYear()) LandscapeQuadrantOutputProbe(m_TheLandscape->SupplyGlobalDate()-365);
  if (cfg_voleLandscapeGridOutputDay.value()+181 == m_TheLandscape->SupplyDayInYear()) LandscapeQuadrantOutputProbe(m_TheLandscape->SupplyGlobalDate()-365);
  NoYoungKilledToday=0;
  NoYoungKilledToday4=0;
  NoYoungKilledToday8=0;
  NoYoungKilledToday9=0;
  //AgeYoungKilledToday=0;
  YoungProducedToday=0; // Zero the number of young
  JuvsProducedToday=0; // Zero the number of juveniles
  if (today==0)
  {
	  // Annual Vole Mortality Record Operations
	  if (cfg_RecordVoleMort.value() ) m_VoleRecordMort->OPrint();
	  if (cfg_RecordVoleMort.value() ) m_VoleRecordMort->ResetData();
#ifdef __VOLEPESTICIDEON
	  // Save the impacted file
	  FILE* impf = fopen("Impacted.res","a");
	  if (!impf) {
		  m_TheLandscape->Warn("Vole_Population_Manager Destructor","Could Not Open Impacted.Res File");
		  exit(0);
	  }
	  fprintf(impf,"%d\t%d\t%d\t%d\t%d\n",m_notimpacted, m_impacted, m_geneticimpacted, m_LittersProduced, m_LittersLost);
	  fclose(impf);
	  m_impacted=0;
	  m_notimpacted=0;
	  m_geneticimpacted=0;
	  m_LittersLost=0;
	  m_LittersProduced = 0;
#endif
  }
  if ( RecordGeneticsToday(today, year, cfg_GeneticsResultOutputFirstYear.value(), cfg_GeneticsResultOutputInterval_1.value())) {
		// Open genetic output file for append
		int no_samples = cfg_genetetic_output_sample_size.value();
		fstream gfout;
		gfout.open("VoleGeneticsOutput.txt",ios::app);
		if (!gfout) {
			g_msg->Warn( "Population_Manager::SpeciesSpecificReporting(int a_species, int a_time) ","Cannot open file VoleGeneticsOutput.txt for append" );
			exit( 0 );
		}
		// Select voles and print their genetics and location to the file
		// Simplest is to randomise the list of males and females, then take the first 500 of each
		Shuffle(vob_Female);
		Shuffle(vob_Male);
		//
		int x = 0; int y = 0;
		int s = (int)GetLiveArraySize(vob_Female);
		if ( s > no_samples ) s = no_samples;
		for (int i = 0; i<s; i++) {
			Vole_Female* VF = dynamic_cast<Vole_Female*>(SupplyAnimalPtr(vob_Female, i));
			gfout << year << '\t';
			x = VF->Supply_m_Location_x();
			y = VF->Supply_m_Location_y();
			gfout << x << '\t' << y << '\t' << "F";
			for (int a=0; a<16; a++) {
				int allele1 = 1 + VF->SupplyAllele(a,0);
				int allele2 = 1 + VF->SupplyAllele(a,1);
				gfout << '\t' << allele1 << '\t' << allele2;
			}
			gfout << endl;
		}
		s = (int)GetLiveArraySize(vob_Male);
		if ( s > no_samples ) s = no_samples;
		for (int i = 0; i<s; i++) {
			Vole_Male* VM = dynamic_cast<Vole_Male*>(SupplyAnimalPtr(vob_Male, i));
			gfout << year << '\t';
			x = VM->Supply_m_Location_x();
			y = VM->Supply_m_Location_y();
			gfout << x << '\t' << y << '\t' << "M";
			for (int a=0; a<16; a++) {
				int allele1 = 1 + VM->SupplyAllele(a,0);
				int allele2 = 1 + VM->SupplyAllele(a,1);
				gfout << '\t' << allele1 << '\t' << allele2;
			}
			gfout << endl;
		}
		gfout.close();
	}

 if (cfg_VoleUseResistanceOuput.value())
 {
	 if(cfg_VoleResistanceDay.value() == today) ResistanceOutput();
 }


	bool terr = false;
  if ( cfg_UseVoleTraplines.value() ) {
	  int day = m_TheLandscape->SupplyGlobalDate();
	  // Loop through all voles and see if any of them are in trap locations
		int s = (int)GetLiveArraySize(vob_JuvenileMale);
		for (int i = 0; i<s; i++) {
			Vole_JuvenileMale* VJM = dynamic_cast<Vole_JuvenileMale*>(SupplyAnimalPtr(vob_JuvenileMale, i));
			if ( VJM->SupplyInTrap()) {
				InTrapPosition tp = VJM->SupplyTrapPosition();
				m_Traplines->Output(tp, day, vob_JuvenileMale, false , VJM->SupplyAge(),VJM->SupplyXBorn(),VJM->SupplyYBorn(), VJM->SupplyIDNo() );
				VJM->SetFree();
			}
		}

		s = (int)GetLiveArraySize(vob_JuvenileFemale);
		for (int i = 0; i<s; i++) {
			Vole_JuvenileFemale* VJF = dynamic_cast<Vole_JuvenileFemale*>(SupplyAnimalPtr(vob_JuvenileFemale, i));
			if ( VJF->SupplyInTrap()) {
				InTrapPosition tp = VJF->SupplyTrapPosition();
				m_Traplines->Output(tp, day, vob_JuvenileFemale, false , VJF->SupplyAge(),VJF->SupplyXBorn(),VJF->SupplyYBorn(), VJF->SupplyIDNo() );
				VJF->SetFree();
			}
		}

		s = (int)GetLiveArraySize(vob_Female);
		for (int i = 0; i<s; i++) {
			Vole_Female* VF = dynamic_cast<Vole_Female*>(SupplyAnimalPtr(vob_Female, i));
			if ( VF->SupplyInTrap()) {
				InTrapPosition tp = VF->SupplyTrapPosition();
				terr = VF->SupplyTerritorial();
				m_Traplines->Output(tp, day, vob_Female, terr, VF->SupplyAge(),VF->SupplyXBorn(),VF->SupplyYBorn(), VF->SupplyIDNo() );
				VF->SetFree();
			}
		}

		s = (int)GetLiveArraySize(vob_Male);
		for (int i = 0; i<s; i++) {
			Vole_Male* VM = dynamic_cast<Vole_Male*>(SupplyAnimalPtr(vob_Male, i));
			if ( VM->SupplyInTrap()) {
				InTrapPosition tp = VM->SupplyTrapPosition();
				terr = VM->SupplyTerritorial();
				m_Traplines->Output(tp, day, vob_Male, terr, VM->SupplyAge(),VM->SupplyXBorn(),VM->SupplyYBorn(), VM->SupplyIDNo() );
				VM->SetFree();
			}
		}
  }
  if (cfg_useagesexlocation.value())
  {
	  if (g_date->GetDayInMonth()==1) TheAgeSexLocationProbe();
  }
}
//---------------------------------------------------------------------------

/**
\brief Assess the quality of habitat at p_Polyref.
*/
double Vole_Population_Manager::AssessHabitat(int p_Polyref)
{
	/**
	* Each unit of area (1m2) is assigned a score based on the polygon type. These scores fall currently into
	* a limited number of catagories, with 4, 3, 2.5, 2, 1, & 0 being the only scores currently used. 2.5 is only used by young forest
	* which is a hybrid between 2 & 3 represented by grassland and forest respectively. 4 denotes the best possible conditions.<br>
	* Once the score has been obtained it is modified by the digestability of the vegetation in terms of proportion of young green matter.
	* This modifies the value by multiplication with 0.7-1.0
	*/
	
	//double digestability = ( m_TheLandscape->SupplyVegDigestibilityVector(p_Polyref) + 0.2 );
	//double digestability = ( m_TheLandscape->SupplyVegDigestibilityVector(p_Polyref)+0.1) * (1.0/0.9);
	double digestability = 1.0;
    double score = vole_toletov_asses_habitat_score(m_TheLandscape , p_Polyref);
	return (score*digestability);
}
//---------------------------------------------------------------------------

bool Vole_Population_Manager::RecordGeneticsToday(int p_today, int p_year, int p_start_year, int p_interval) {
	if (p_today != 1) return false;
	if (p_year<p_start_year) return false;
	p_year -= p_start_year;
	if (p_year % p_interval != 0) return false;
	return true;
}

bool Vole_Population_Manager::SupplyOlderFemales(unsigned p_x, unsigned p_y,
                              unsigned p_Age, unsigned p_range)
{
  /** Returns false if there is an older female within the area p_x,p_y +/- range
  */
  // Before checking the map remove ourselves so we don't count
  TAnimal* caller=(TAnimal*)m_VoleMap->GetMapValue(p_x,p_y);
  m_VoleMap->ClearMapValue(p_x,p_y);
  // This is reset when the result is known
  int x=p_x - p_range;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y - p_range;
  if (y<0) y+=SimH;
  int range_x=p_range*2;
  int range_y=p_range*2;
  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Base* Ap;
  // A Loop
  for (int i=0; i<Afinx; i++)
  {
    for (int j=Asty; j<Afiny; j++)
    {
      Ap=(Vole_Base*) (m_VoleMap->GetMapValue(i,j)) ;
      if (Ap)
      {
        if (!Ap->SupplySex()) 
        {
          if (Ap->SupplyAge()>=p_Age)
          {
            m_VoleMap->SetMapValue(p_x,p_y,caller);
            return false;
          }
        }
      }
    }
    // C Loop
    for (int j=y; j<Dfiny; j++)
    {
      Ap=(Vole_Base*) (m_VoleMap->GetMapValue(i,j)) ;
      if (Ap)
      {
        if (!Ap->SupplySex()) 
        {
          if (Ap->SupplyAge()>=p_Age)
          {
            m_VoleMap->SetMapValue(p_x,p_y,caller);
            return false;
          }
        }
      }
    }
  }
  // D Loop
  for (int i=x; i<Dfinx; i++)
  {
    for (int j=y; j<Dfiny; j++)
    {
      Ap=(Vole_Base*) (m_VoleMap->GetMapValue(i,j)) ;
      if (Ap)
      {
        if (!Ap->SupplySex()) // female
        {
          if (Ap->SupplyAge()>=p_Age)
          {
            m_VoleMap->SetMapValue(p_x,p_y,caller);
            return false;
          }
        }
      }
    }
    // B Loop
    for (int j=0; j<Afiny; j++)
    {
      Ap=(Vole_Base*) (m_VoleMap->GetMapValue(i,j)) ;
      if (Ap)
      {
        if (!Ap->SupplySex()) // female
        {
          if (Ap->SupplyAge()>=p_Age)
          {
            m_VoleMap->SetMapValue(p_x,p_y,caller);
            return false;
          }
        }
      }
    }
  }
  // End of search algorithm
   m_VoleMap->SetMapValue(p_x,p_y,caller);
  return true;
}
//---------------------------------------------------------------------------

/**
  Counts vole postions on the map from p_x-p_range,p-y-p_range to p_x+p_size, p_y+p_range
*/
int Vole_Population_Manager::SupplyHowManyVoles(unsigned p_x, unsigned p_y, unsigned p_range)
{
  int x=p_x - p_range;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y - p_range;
  if (y<0) y+=SimH;
  int range_x=p_range*2;
  int range_y=p_range*2;
  int Voles=0;
  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  // A Loop
  for (int i=0; i<Afinx; i++)
  {
    for (int j=Asty; j<Afiny; j++)
    {
      if(m_VoleMap->GetMapValue(i,j)!=NULL) Voles++;
    }
    // C Loop
    for (int j=y; j<Dfiny; j++)
    {
		if (m_VoleMap->GetMapValue(i, j) != NULL) Voles++;
    }
  }
  // D Loop
  for (int i=x; i<Dfinx; i++)
  {
    for (int j=y; j<Dfiny; j++)
    {
		if (m_VoleMap->GetMapValue(i, j) != NULL) Voles++;
    }
    // B Loop
    for (int j=0; j<Afiny; j++)
    {
		if (m_VoleMap->GetMapValue(i, j) != NULL) Voles++;
    }
  }
  // End of search algorithm
  return Voles;
}
//---------------------------------------------------------------------------

void Vole_Population_Manager::ReproductionProbe()
{
   fprintf(YoungsFile,"%i   %i\n",YoungProducedToday,JuvsProducedToday);
}
//---------------------------------------------------------------------------

/** Prevents voles from mating with an individual on the other side of af barrier*/
bool Vole_Population_Manager::BarrierSearch(int F_x, int F_y, int M_x, int M_y)
{
	int poly2 = 200;

	if(F_x == M_x){
		if (F_y == M_y) return true; // if they are on the same spot
		if(F_y < M_y){ // if they have the same x coordinate and different y coordinates and the female has the lowest y
			for (int j = F_y; j < M_y; j++){
				int poly1 = m_TheLandscape->SupplyPolyRef(F_x, j);
				if (poly2 != poly1)
				{
					poly2 = poly1;
					if ( vole_tole_assess_barrier(m_TheLandscape, poly1)  == false )
						return false; 
				}
			}
				return true;
		}
		else { // if (F_y > M_y) if they have the same x coordinate and different y coordinates and the male has the lowest y
			for (int j = M_y; j < F_y; j++){
				int poly1 = m_TheLandscape->SupplyPolyRef(M_x, j);
				if (poly1 != poly2)
				{
					poly2 = poly1;
					if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
						return false;
				}
			}
				return true;
		}
	}
	if (F_y == M_y){
		if (F_x < M_x) { // if they have the same y coordinate and different x coordinates and the female's x coordinate is the lowest
			for (int i = F_x; i < M_x; i++){
			int poly1 = m_TheLandscape->SupplyPolyRef(i, F_y);
				if (poly1 != poly2)
				{
					poly2 = poly1;
					if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
						return false;
				}
			}
			return true;
		}
		else { // (F_x > M_x) if they have the same y coordinate and different x coordinates and the male's x coordinate is the lowest
			for (int i = M_x; i < F_x; i++){
				int poly1 = m_TheLandscape->SupplyPolyRef(i, M_y);
				if (poly1 != poly2)
				{
					poly2 = poly1;
					if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
						return false;
				}
			}
			return true;
		}
	}
	if (F_x < M_x){ // if the x coordinates differ and the females is the lowest
		if (F_y < M_y){ // if the y coordinates differ and the female has the lowest y coordinate
			unsigned diff_x = M_x - F_x;
			unsigned diff_y = M_y - F_y;
			if (diff_x <= diff_y){ // if the area between them is enlongated (y) or diagonal
				int j = F_y;

				for (int i = F_x; i < M_x; i++){	// the diagonal part
						int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
						j++;
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				if (diff_x != diff_y){ // if not diagonal but enlongated and the x and y coordinates differ with the female having the lowest values
					int j_extent = F_y + diff_x;
					for (int j_y = j_extent; j_y < M_y; j_y++){ // the enlongated part
						int poly1 = m_TheLandscape->SupplyPolyRef(M_x, j_y);
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
					}
				}
					return true;
			}
			if (diff_x > diff_y) { // if not diagonal but widened and the x and y coordinates differ with the female having the lowest values
				int i = F_x;
				for (int j = F_y; j < M_y; j++){ // the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					i++;
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				int i_extent = diff_y + F_x;
				for (int i_x = i_extent; i_x < M_x; i_x++){ // the widened part
					int poly1 = m_TheLandscape->SupplyPolyRef(i_x, M_y);
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				return true;
			}
		}
		if (F_y > M_y) { // and if (F_x < M_x)
			int diff_x = M_x - F_x;
			int diff_y = F_y - M_y;
			if (diff_x <= diff_y){ // if the area between them is enlongated (y) or diagonal
				int j = F_y;
				for (int i = F_x; i < M_x; i++){	// the diagonal part
						int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
						j--;
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				if (diff_x != diff_y){ // if not diagonal but enlongated and the x and y coordinates differ with the female having the lowest x value and highest y
					int j_extent = F_y - diff_x;
					for (int j_y = j_extent; j_y > M_y; j_y--){ // the enlongated part
						int poly1 = m_TheLandscape->SupplyPolyRef(M_x, j_y);
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
					}
				}
					return true;
			}
			else { // if (diff_x > diff_y) if not diagonal but widened and the x and y coordinates differ with the female having the lowest x and highest y
				int i = F_x;
				for (int j = F_y; j > M_y; j--){ // the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					i++;
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				int i_extent = diff_y + F_x;
				for (int i_x = i_extent; i_x < M_x; i_x++){ // the widened part
					int poly1 = m_TheLandscape->SupplyPolyRef(i_x, M_y);
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				return true;
			}
		}
	}
	if (F_x > M_x){
		if (F_y < M_y){
			unsigned diff_x = F_x - M_x;
			unsigned diff_y = M_y - F_y;
			if (diff_x <= diff_y){ // if the area between them is enlongated (y) or diagonal
				int j = M_y;
				for (int i = M_x; i < F_x; i++){	// the diagonal part
						int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
						j--;
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}

				if (diff_x != diff_y){ // if not diagonal but enlongated and the x and y coordinates differ with the female having the lowest values
					int j_extent = M_y - diff_x;
					for (int j_y = j_extent; j_y > F_y; j_y--){ // the enlongated part
						int poly1 = m_TheLandscape->SupplyPolyRef(F_x, j_y);
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
					}
				}
					return true;
			}
			else { // if (diff_x > diff_y) if not diagonal but widened and the x and y coordinates differ with the female having the lowest values
				int i = M_x;
				for (int j = M_y; j > F_y; j--){
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					i++;
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				int i_extent = diff_y + M_x;
				for (int i_x = i_extent; i_x < F_x; i_x++){ // the widened part
					int poly1 = m_TheLandscape->SupplyPolyRef(i_x, F_y);
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				return true;
			}
		}
		if (F_y > M_y) {// if (F_x > M_x)
			int diff_x = F_x - M_x;
			int diff_y = F_y - M_y;
			if (diff_x <= diff_y){ // if the area between them is enlongated (y) or diagonal
				int j = M_y;
				for (int i = M_x; i < F_x; i++){	// the diagonal part
						int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
						j++;
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				if (diff_x != diff_y){ // if not diagonal but enlongated and the x and y coordinates differ with the female having the lowest x value and highest y
					int j_extent = M_y + diff_x;
					for (int j_y = j_extent; j_y < F_y; j_y++){ // the enlongated part
						int poly1 = m_TheLandscape->SupplyPolyRef(F_x, j_y);
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
					}
				}
					return true;
			}
			else { // if (diff_x > diff_y) if not diagonal but widened and the x and y coordinates differ with the female having the lowest x and highest y
				int i = M_x;
				for (int j = M_y; j < F_y; j++){ // the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					i++;
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				int i_extent = diff_y + M_x;
				for (int i_x = i_extent; i_x < F_x; i_x++){ // the widened part
					int poly1 = m_TheLandscape->SupplyPolyRef(i_x, F_y);
						if (poly1 != poly2)
						{
							poly2 = poly1;
							if (vole_tole_assess_barrier(m_TheLandscape, poly1) == false)
								return false;
						}
				}
				return true; //returns true if both the diagonal and widened part dont hit return false
			}
		}
	}
	return true;
}

//-----------------------------------------------------------------------------

/**
Looks for males within p_Steps of p_x,p_y and returns the number of them. Pointers to these are saved in MList
*/
int Vole_Population_Manager::ListClosestMales(int p_x, int p_y,int p_steps)
{
  // First clear the MaleVoleList
  MList.clear();

  // as usual there are 4 possibilities of overlap
  // First convert centre co-rdinates to square co-ordinates

  int x=p_x - p_steps;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y - p_steps;
  if (y<0) y+=SimH;
  int range_x=p_steps*2;
  int range_y=p_steps*2;

  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Base* Ap;
  Vole_Male* AMale;
  int NoFound=0;

    // A Loop
    for (int i=0; i<Afinx; i++)
    {
      for (int j=Asty; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
            AMale=(Vole_Male*) Ap;
            if (AMale->SupplyTerritorial())
            {
              MList.push_back(AMale);
              NoFound++;
            }
          }
        }
      }
      // C Loop
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
            AMale=(Vole_Male*) (Ap) ;
            if (AMale->SupplyTerritorial())
            {
              MList.push_back(AMale);
              NoFound++;
            }
          }
        }
      }
    }
    // D Loop
    for (int i=x; i<Dfinx; i++)
    {
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
            AMale=(Vole_Male*) (Ap) ;
            if (AMale->SupplyTerritorial())
            {
              MList.push_back(AMale);
              NoFound++;
            }
          }
        }
      }
      // B Loop
      for (int j=0; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
            AMale=(Vole_Male*) (Ap) ;
            if (AMale->SupplyTerritorial())
            {
              MList.push_back(AMale);
              NoFound++;
            }
          }
        }
      }
    }
  return NoFound;
}
//---------------------------------------------------------------------------

/** Lists all females within p_steps of p_x & p_y and returns the number in the list */
int Vole_Population_Manager::ListClosestFemales(int p_x, int p_y,int p_steps)
{
  // First clear the FemaleVoleList
  FList.clear();
  // looks for a female within p_Steps of p_x,p_y
  // as usual there are 4 possibilities of overlap
  // First convert centre co-ordinates to square co-ordinates

  int x=p_x - p_steps;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y - p_steps;
  if (y<0) y+=SimH;
  int range_x=p_steps*2;
  int range_y=p_steps*2;

  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Female* AFemale;
  Vole_Base* Ap;
  int NoFound=0;

    // A Loop
    for (int i=0; i<Afinx; i++)
    {
      for (int j=Asty; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            AFemale=(Vole_Female*) (Ap) ;
            if (AFemale->SupplyTerritorial())
            {
              FList.push_back(AFemale);
              NoFound++;
            }
          }
        }
      }
      // C Loop
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            AFemale=(Vole_Female*) (Ap) ;
            if (AFemale->SupplyTerritorial())
            {
              FList.push_back(AFemale);
              NoFound++;
            }
          }
        }
      }
    }
    // D Loop
    for (int i=x; i<Dfinx; i++)
    {
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            AFemale=(Vole_Female*) (Ap) ;
            if (AFemale->SupplyTerritorial())
            {
              FList.push_back(AFemale);
              NoFound++;
            }
          }
        }
      }
      // B Loop
      for (int j=0; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            AFemale=(Vole_Female*) (Ap) ;
            if (AFemale->SupplyTerritorial())
            {
              FList.push_back(AFemale);
              NoFound++;
            }
          }
        }
      }
    }
  return NoFound;
}
//---------------------------------------------------------------------------

Vole_Female* Vole_Population_Manager::FindClosestFemale(int p_x, int p_y,int p_steps)
{
  /** looks for the closest female within p_Steps of p_x,p_y */
  // as usual there are 4 possibilities of overlap
  // First convert centre co-rdinates to square co-ordinates

  int x=p_x - p_steps;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y - p_steps;
  if (y<0) y+=SimH;
  int range_x=p_steps*2;
  int range_y=p_steps*2;

  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Female* AFemale;
  Vole_Female* Found=NULL;
  int dist, disty;
  int FoundDist=SimW; // too big
  Vole_Base* Ap;

    // A Loop
    for (int i=0; i<Afinx; i++)
    {
      for (int j=Asty; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            AFemale=(Vole_Female*) (Ap) ;
            if (AFemale->SupplyTerritorial())
            {
              dist=abs(p_x-i); // remove the signed bit
              disty=abs(p_y-j);
              dist+=disty;
              if (dist<FoundDist)
              {
                Found=AFemale;
                FoundDist=dist;
              }
            }
          }
        }
      }
      // C Loop
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            AFemale=(Vole_Female*) (Ap) ;
            if (AFemale->SupplyTerritorial())
            {
              dist=abs(p_x-i); // remove the signed bit
              disty=abs(p_y-j);
              dist+=disty;
              if (dist<FoundDist)
              {
                Found=AFemale;
                FoundDist=dist;
              }
            }
          }
        }
      }
    }
    // D Loop
    for (int i=x; i<Dfinx; i++)
    {
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            AFemale=(Vole_Female*) (Ap) ;
            if (AFemale->SupplyTerritorial())
            {
              dist=abs(p_x-i); // remove the signed bit
              disty=abs(p_y-j);
              dist+=disty;
              if (dist<FoundDist)
              {
                Found=AFemale;
                FoundDist=dist;
              }
            }
          }
        }
      }
      // B Loop
      for (int j=0; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            AFemale=(Vole_Female*) (Ap) ;
            if (AFemale->SupplyTerritorial())
            {
              dist=abs(p_x-i); // remove the signed bit
              disty=abs(p_y-j);
              dist+=disty;
              if (dist<FoundDist)
              {
                Found=AFemale;
                FoundDist=dist;
              }
            }
          }
        }
      }
    }
  return Found;
}
//---------------------------------------------------------------------------
Vole_Male* Vole_Population_Manager::FindOutsideRadiusMale(int p_x, int p_y) //**TD**
{
  /** Returns a pointer to a random male - he does not even have to have a territory */
  int x= (p_x - cfg_MateRadius.value()); // 'left most' x
  if (x<0) x+=SimW;  // ensure we start within the landscape!
  int y= (p_y - cfg_MateRadius.value()); // 'top' y
  if (y<0) y+=SimH;
  int range_x= (cfg_MateRadius.value()*2); // width of the search area
  int range_y= (cfg_MateRadius.value()*2); // hight of the search area

  // create the extent variables - if out of bounds
  int xextent0 = x+range_x; // 'right most' x
  int yextent0 = y+range_y; // 'lowest'(bottom) y
  int xextent1 = (x+range_x)-SimW; // incase we are out of bounds
  int yextent1 = (y+range_y)-SimH;

  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  //int Asty=0;

  // Dfinx, Dfiny are the stop x and y values for 'the mate picking area'
  // Afinx, Afiny are the 'overlapping area' if 'the mate picking area' goes out off bounds
  // Set default at 0 and only changes value if out off bounds

  // Now create the loop values;
  // Type A (no overlap with the eastern bound)
  if (xextent0<=SimW)
  {
    //Afinx are not needed
    Dfinx=xextent0; // because no overlap with the right most bound
    // do we overlap the bottom (lowest y)?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top because off bounds
    }
    else Dfiny=yextent0; // if no overlap
  }
  else
  {
    // Type A & C eastern bound & overlap bottom
    if (yextent0>SimH)
    {
      // relies on the default start for Afiny, Afinx, Dfinx, Dfiny
      Afinx=xextent1; // Afinx gets a value other than 0 = overlapping value
      Afiny=yextent1; //
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap west bound only
      //Afiny, Dfinx, Dfiny set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }

  Vole_Male* Found=NULL;
  TAnimal* Ap;
  int size=(int)GetLiveArraySize(vob_Male);

  do
  {
    int test =random(size);
    Ap= SupplyAnimalPtr(vob_Male, test); // Males only
    Found=(Vole_Male*)(Ap); // Male pointer to TAnimal[0][test]
	int territory = Found->SupplyTerritorial();
	if (Found->GetCurrentStateNo() == -1) Found=NULL; // If end of timestep reached skip check
	{
		int i = Found->Supply_m_Location_x();
		int j = Found->Supply_m_Location_y();

		if (Afinx == 0) // no right overlap
		{
			if (Afiny == 0) // no bottom overlap
			{
				if (((x <= i) && (i <= Dfinx)) && ((y <= j) && (j <= Dfiny)))
				{
					Found=NULL;
				}
			}

			if (Afiny != 0)// overlap bottom
			{
				if (((x <= i) && (i <= Dfinx)) && ((( 0<= j) && (j <=Afiny)) || ((y <= j) && (j <= Dfiny))))
				{
					Found=NULL;
				}
			}
		}
		if (Afinx !=0) // overlap right
		{
			if (Afiny == 0)
			{
				if (((y <= j) && (j <= Dfiny)) && (((0 <= i) && (i <= Afinx)) || ((x <= i) && (i <= Dfinx))))
				{
					Found=NULL;
				}
			}

			if (Afiny != 0) //overlap bottom
			{
				if ((( 0 <= i) && (i <= Afinx)) && (((0 <= j) && (j <= Afiny)) || ((y <= j) &&(j <= Dfiny))))
				{
					Found= NULL;
				}
				if (((x <= i) && (i <= Dfinx)) && (((0 <= j) && (j <= Afiny)) || ((y <= j) &&(j <= Dfiny))))
				{
					Found= NULL;
				}
			}
		}
	}
	if (territory == false) Found=NULL;
	} while ((Found==NULL) && (size>0));
	return Found;
  }


//--------------------------------------------------------------------------------------

Vole_Male* Vole_Population_Manager::FindWithinRadiusMale(int p_x, int p_y) // ***TD***
{
  /** looks for males within cfg_MateRadius of p_x,p_y and returns a list of all males*/

  vector<Vole_Male*> *vbl;
  vbl=new vector<Vole_Male*>;

  // as usual there are 4 possibilities of overlap
  // First convert centre co-rdinates to square co-ordinates
  // To create start x and y values for 'the mate picking area'
  int x= (p_x - cfg_MateRadius.value()); // 'left most' x
  if (x<0) x+=SimW;  // ensure we start within the landscape!
  int y= (p_y - cfg_MateRadius.value()); // 'top' y
  if (y<0) y+=SimH;
  int range_x= (cfg_MateRadius.value()*2); // width of the search area
  int range_y= (cfg_MateRadius.value()*2); // hight of the search area

  // create the extent variables - if out of bounds
  int xextent0 = x+range_x; // 'right most' x
  int yextent0 = y+range_y; // 'lowest'(bottom) y
  int xextent1 = (x+range_x)-SimW; // incase we are out of bounds (positive if we are)
  int yextent1 = (y+range_y)-SimH;

  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  //int Asty=0;

  // Dfinx, Dfiny are the stop x and y values for 'the mate picking area'
  // Afinx, Afiny are the 'overlapping area' if 'the mate picking area' goes out off bounds
  // Set default at 0 and only changes value if out off bounds

  // Now create the loop values;
  // Type A (no overlap with the eastern bound)
  if (xextent0<=SimW)
  {
    //Afinx are not needed
    Dfinx=xextent0; // because no overlap with the right most bound
    // do we overlap the bottom (lowest y)?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top because off bounds
    }
    else Dfiny=yextent0; // if no overlap bottom
  }
  else
  {
    // Type A & C eastern bound & overlap bottom
    if (yextent0>SimH)
    {
      // relies on the default start for Afiny, Afinx, Dfinx, Dfiny
      Afinx=xextent1; // Afinx gets a value other than 0 = overlapping value
      Afiny=yextent1; //
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap west bound only
      //Afiny, Dfinx, Dfiny set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Male* AMale;
  Vole_Male* Found=NULL;
  Vole_Base* Ap;

    // A Loop
    for (int i=0; i<Afinx; i++) // Afinx and y will be 0 if no overlap
    {
      for (int j=0; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j)); // Tjeck to see if any voles are at the location
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
			  AMale=(Vole_Male*) (Ap) ;
			  if (AMale->SupplyTerritorial())
			  {
				  vbl->push_back(AMale);
			  }
          }
        }
      }
      // C Loop
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j)); // Tjeck to see if any voles are at the location
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
			  AMale=(Vole_Male*) (Ap) ;
			  if (AMale->SupplyTerritorial())
			  {
				  vbl->push_back(AMale);
			  }
          }
        }
      }
    }
    // D Loop
    for (int i=x; i<Dfinx; i++)
    {
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j)); // Tjeck to see if any voles are at the location
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
			  AMale=(Vole_Male*) (Ap) ;
			  if (AMale->SupplyTerritorial())
			  {
				  vbl->push_back(AMale);
			  }
          }
        }
      }
      // B Loop
      for (int j=0; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j)); // Tjeck to see if any voles are at the location
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
			  AMale=(Vole_Male*) (Ap) ;
			  if (AMale->SupplyTerritorial())
			  {
				  vbl->push_back(AMale);
			  }
          }
        }
      }
    }

	int l = (int) vbl->size();
	if (l == 0)
	{
		return Found=NULL;
	}
	else {

	int i=(random(l));
	Found = vbl->at(i);
	return  Found;
	}
}
//--------------------------------------------------------------------------------------

Vole_Male* Vole_Population_Manager::FindClosestMale(int p_x, int p_y,int p_steps)
{
  /** looks for the closest male within p_Steps of p_x,p_y */
  // as usual there are 4 possibilities of overlap
  // First convert centre co-rdinates to square co-ordinates
  int x=p_x - p_steps;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y - p_steps;
  if (y<0) y+=SimH;
  int range_x=p_steps*2;
  int range_y=p_steps*2;

  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Male* AMale;
  Vole_Male* Found=NULL;
  int dist, disty;
  int FoundDist=SimW; // too big
  Vole_Base* Ap;
    // A Loop
    for (int i=0; i<Afinx; i++)
    {
      for (int j=Asty; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
			  {
				  AMale=(Vole_Male*) (Ap) ;
				  if (AMale->SupplyTerritorial())
				  {
					  dist=abs(p_x-i); // remove the signed bit
					  disty=abs(p_y-j);
					  dist+=disty;

					  if (dist<FoundDist)
					  {
						  bool Barrier = BarrierSearch(p_x, p_y, i, j);
						  if (Barrier == true){
							  Found=AMale;
							  FoundDist=dist;
						  }
					  }
				  }
			  }
		  }
		}
	  }
      // C Loop
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
			  {
				  AMale=(Vole_Male*) (Ap) ;
				  if (AMale->SupplyTerritorial())
				  {
					  dist=abs(p_x-i); // remove the signed bit
					  disty=abs(p_y-j);
					  dist+=disty;

					  if (dist<FoundDist)
					  {
						  bool Barrier = BarrierSearch(p_x, p_y, i, j);
						  if (Barrier == true){
							  Found=AMale;
							  FoundDist=dist;
						  }
					  }
				  }
			  }
		  }
		}
	  }
	}
	// D Loop
    for (int i=x; i<Dfinx; i++)
    {
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
			  {
				  AMale=(Vole_Male*) (Ap) ;
				  if (AMale->SupplyTerritorial())
				  {
					  dist=abs(p_x-i); // remove the signed bit
					  disty=abs(p_y-j);
					  dist+=disty;
					  if (dist<FoundDist)
					  {
						  bool Barrier = BarrierSearch(p_x, p_y, i, j);
						  if (Barrier == true){
							  Found=AMale;
							  FoundDist=dist;
						  }
					  }
				  }
			  }
		  }
		}
	  }
	  // B Loop
	  for (int j=0; j<Afiny; j++)
	  {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
		  if (Ap)
		  {
			  if (Ap->SupplySex()) // male
			  {
				  AMale=(Vole_Male*) (Ap) ;
				  {

					  if (AMale->SupplyTerritorial())
					  {
						  dist=abs(p_x-i); // remove the signed bit
						  disty=abs(p_y-j);
						  dist+=disty;
						  if (dist<FoundDist)
						  {
							  bool Barrier = BarrierSearch(p_x, p_y, i, j);
							  if (Barrier == true){
								  Found=AMale;
								  FoundDist=dist;
							  }
						  }
					  }
				  }
			  }
		  }
	  }
	}
	return Found;
}
//---------------------------------------------------------------------------

Vole_Male* Vole_Population_Manager::FindRandomMale()
{
  /** Returns a pointer to a random male - he does not even have to have a territory */
	Vole_Male* Found=NULL;
	TAnimal* Ap;
	int size = (int)GetLiveArraySize(vob_Male);
	if (size<1) return NULL;
	do
	{
		int test =random(size);
		Ap= SupplyAnimalPtr(vob_Male, test);
		Found=(Vole_Male*)(Ap);
		if (Found->GetCurrentStateNo() == -1) Found=NULL; //if not alive
	} while ((Found==NULL) && (size>0));
	return Found;
}
//---------------------------------------------------------------------------
int Vole_Population_Manager::SupplyCountFemales(unsigned p_x, unsigned p_y, unsigned p_TerrRange)
{
  /** returns -1 if a male has p_x,p_y in his territory and is older than p_Age else returns the number of females present
  */

  // Before checking the map remove ourselves so we don't count
  //PointerInt c=VoleMap->GetMapValue(p_x,p_y);
  //TAnimal* caller=(TAnimal*) c;
	TAnimal* caller = m_VoleMap->GetMapValue(p_x, p_y);
	m_VoleMap->ClearMapValue(p_x, p_y);
  int x=p_x - p_TerrRange;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y - p_TerrRange;
  if (y<0) y+=SimH;
  int range_x=p_TerrRange*2;
  int range_y=p_TerrRange*2;
  int Females=0;

  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Base* Ap;

    // A Loop
    for (int i=0; i<Afinx; i++)
    {
      for (int j=Asty; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            if (Ap->SupplyTerritorial())
            {
              Females++;
            }
          }
        }
      }
      // C Loop
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {

            if (Ap->SupplyTerritorial())
            {
              Females++;
            }
          }
        }
      }
    }
    // D Loop
    for (int i=x; i<Dfinx; i++)
    {
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            if (Ap->SupplyTerritorial())
            {
              Females++;
            }
          }
        }
      }
      // B Loop
      for (int j=0; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (!Ap->SupplySex()) // female
          {
            if (Ap->SupplyTerritorial())
            {
              Females++;
            }
          }
        }
      }
    }
   // End of search algorithm
	m_VoleMap->SetMapValue(p_x, p_y, caller);
  return Females; // returns the number of adult females
}
//---------------------------------------------------------------------------


int Vole_Population_Manager::SupplyInOlderTerr(unsigned p_x, unsigned p_y,
                                               unsigned p_Age, unsigned p_Range)
{
  /** returns -1 if a male has p_x,p_y in his territory and is older than p_Age
  */

  // Before checking the map remove ourselves so we don't count
  //PointerInt c=VoleMap->GetMapValue(p_x,p_y);
  //TAnimal* caller=(TAnimal*) c;
  int state = 0;
  TAnimal* caller = m_VoleMap->GetMapValue(p_x, p_y);
  m_VoleMap->ClearMapValue(p_x, p_y);
  int x=p_x - p_Range;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y - p_Range;
  if (y<0) y+=SimH;
  int range_x=p_Range*2;
  int range_y=p_Range*2;

  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Male* AMale;
  Vole_Base* Ap;

    // A Loop
    for (int i=0; i<Afinx; i++)
    {
      for (int j=Asty; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
            AMale=(Vole_Male*) (Ap) ;
            if (AMale->SupplyTerritorial())
            {
             if (AMale->SupplyAge()>=p_Age)
             {
				 if ((AMale->SupplyAge() - g_sigAgeDiff > p_Age) || (g_rand_uni()<0.5))
				 {
					 m_VoleMap->SetMapValue(p_x, p_y, caller);
					 state = -1;
					 return state;   // No Good
				 }
             }
            }
		  }
		  else state++;
        }
      }
      // C Loop
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
            AMale=(Vole_Male*) (Ap) ;
            if (AMale->SupplyTerritorial())
            {
              if (AMale->SupplyAge()>=p_Age)
			  {
				  if ((AMale->SupplyAge() - g_sigAgeDiff > p_Age) || (g_rand_uni()<0.5))
				  {
					  m_VoleMap->SetMapValue(p_x, p_y, caller);
					  state = -1;
					  return state;   // No Good
				  }
			  }
			}
          }
		  else state++;
        }
      }
    }
    // D Loop
    for (int i=x; i<Dfinx; i++)
    {
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
            AMale=(Vole_Male*) (Ap) ;
            if (AMale->SupplyTerritorial())
            {
              if (AMale->SupplyAge()>=p_Age)
			  {
				  if ((AMale->SupplyAge() - g_sigAgeDiff > p_Age) || (g_rand_uni()<0.5))
				  {
					  m_VoleMap->SetMapValue(p_x, p_y, caller);
					  state = -1;
					  return state;   // No Good
				  }
			  }
			}
          }
		  else state++;
        }
      }
      // B Loop
      for (int j=0; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          if (Ap->SupplySex()) // male
          {
            AMale=(Vole_Male*) (Ap) ;
            if (AMale->SupplyTerritorial())
            {
              if (AMale->SupplyAge()>=p_Age)
			  {
				  if ((AMale->SupplyAge() - g_sigAgeDiff > p_Age) || (g_rand_uni()<0.5))
				  {
					  m_VoleMap->SetMapValue(p_x, p_y, caller);
					  state = -1;
					  return state;   // No Good
				  }
			  }
			}
          }
		  else state++;
        }
      }
    }
   // End of search algorithm
	m_VoleMap->SetMapValue(p_x, p_y, caller);
  return state; // No Males it return the number of adult females
}
//---------------------------------------------------------------------------


bool Vole_Population_Manager::InSquare(int p_x, int p_y,int p_sqx,int p_sqy,
                                                                    int p_range)
{
	/** Determines if p_x,p_y is the in the square denoted by p_sqx,p_sqy + p_range */

    int x_extent = p_sqx+p_range;
    int y_extent = p_sqy+p_range;
    if (x_extent >= SimW)
    {
      if (y_extent >= SimH)  // overlaps TR corner of sim area
                             // Must test four rectangles
      /*
        1) p_sqx to <SimW, p_sq_y to SimW (TR)
        2) 0 to x_extent-SimW p_sq_y to SimH (TL)
        3) 0 to x_extent-SimW, 0 to y_extent-SimH (BL)
        4) p_sq_x to <SimW, 0 to y_extent-SimH (BR)
      */
      {
        // 1 Top right square (limited by SimAreaHeight & SimAreaWidth
        if ((p_x >=p_sqx) && (p_y >=p_sqy)) return true;
        // 2 Top Left Square (limited by 0,SimAreaHeight)
        if ((p_x <x_extent-SimW)&& (p_y>p_sqy)) return true;
        // 3 Bottom Left square (limited by 0,0)
        if ((p_x <x_extent-SimW)&&(p_y<y_extent-SimH)) return true;
        // Bottom Right square (limited by SimAreaWidth,0)
        if ((p_x >=p_sqx)&& (p_y<y_extent-SimH)) return true;

      }
      else // Overlaps the west edge of the sim area
      {
        if ((p_y >=p_sqy) && (p_y<y_extent))
        {  // y is in square
          if (p_x >=p_sqx) return true;
          else if (p_x <x_extent-SimW) return true;
        }
      }
    }
    else
    {
      if (y_extent >= SimH) // overlaps top of simulation area
      {
        if ((p_x >=p_sqx) && (p_x<x_extent))
        {
          // x is OK
          if (p_y >=p_sqy) return true;
          else if (p_y<y_extent-SimH) return true;
        }
      }
      else // square does not overlap end of simulation area
      {
        if ((p_x >=p_sqx) && (p_x<x_extent) &&
                         (p_y >=p_sqy) && (p_y<y_extent)) return true;
      }
    }
    return false; // not in square
}
//---------------------------------------------------------------------------


vector<Vole_Base*>* Vole_Population_Manager::SupplyVoleList(unsigned p_x,
                                                unsigned p_y, unsigned p_Range)
{
  /** returns a list of all voles in p_x,p_y, p_range square */

  vector<Vole_Base*> *vbl;
  vbl=new vector<Vole_Base*>;
  // This is reset when the result is known
  int x=p_x;
  if (x<0) x+=SimW;  // ensure we start in the simulation area!
  int y=p_y;
  if (y<0) y+=SimH;
  int range_x=p_Range;
  int range_y=p_Range;
  // create the extent variables
  int xextent0 = x+range_x;
  int yextent0 = y+range_y;
  int xextent1 = (x+range_x)-SimW;
  int yextent1 = (y+range_y)-SimH;
  // Create the looping variables needed
  // Create the looping variables needed
  int Dfinx;
  int Dfiny;
  int Afinx=0;  //unless the finx values for A are changed this stop
  int Afiny=0;  //the loop from executing
  int Asty=0;
  // int Astx,Dstx,Dtsy are always default so variables not used from them
  // NB Astx, Asty and Dstx are always 0, 0 & x respectively
  // Dsty is always y, Afiny is always yextent1 if it is used
  // Now create the loop values;
  if (xextent0<=SimW)   // No overlap with the eastern side
  {
    // Dstx, Dsty, Asty set by defaults
    //Astx & Afinx are not needed
    Dfinx=xextent0;
    // do we overlap the bottom?
    // Type B & D (overlap bottom, no overlap)
    if (yextent0>SimH)
    {
      // Type B (overlap bottom only)
      Dfiny=SimH;  // stop at the end
      Afiny=yextent1; // the overlap with the top
    }
    else Dfiny=yextent0;
  }
  else
  {
    // Type A & C overlap bottom & eastern edgdes
    if (yextent0>SimH)
    {
      // relies on the default start for Asty, Astx, Dstx, Dsty
      Afinx=xextent1;
      Afiny=yextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=SimH;
    }
    else
    {
      // Type C overlap left edge only
      // Astx & Afiny are not needed here
      //Astx, Dstx, Dsty set by default
      Afinx=xextent1;
      Dfinx=SimW;  // Stop at the end
      Dfiny=yextent0;
    }
  }
  Vole_Base* Ap;

    // A Loop
    for (int i=0; i<Afinx; i++)
    {
      for (int j=Asty; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          vbl->push_back(Ap);
        }
      }
      // C Loop
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          vbl->push_back(Ap);
        }
      }
    }
    // D Loop
    for (int i=x; i<Dfinx; i++)
    {
      for (int j=y; j<Dfiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          vbl->push_back(Ap);
        }
      }
      // B Loop
      for (int j=0; j<Afiny; j++)
      {
		  Ap = (Vole_Base*)(m_VoleMap->GetMapValue(i, j));
        if (Ap)
        {
          vbl->push_back(Ap);
        }
      }
    }
  return vbl;
}
//---------------------------------------------------------------------------


void Vole_Population_Manager::SendMessage(TTypeOfVoleMessage p_message_type,
       unsigned p_x, unsigned p_y, unsigned p_range, bool p_sex, double /*p_Weight*/ /*, unsigned  p_IDNo */)

{
  /** Passes a message to recipients. In this case the only one we use is infanticide sent to all females within an area */
  if (p_message_type==tovm_Infanticide)
  {
      if (p_sex==false) // female
      {
        for (unsigned i=0; i<GetLiveArraySize(vob_Female); i++)
        {
          Vole_Female *AFemale;
          AFemale=(Vole_Female *) SupplyAnimalPtr(vob_Female, i);
          {
             // is it in the square defined by p_x,p_y & p_range
             unsigned x=AFemale->SupplyX();
             unsigned y=AFemale->SupplyY();
			 // Need to know if female is within p_range of p_x, p_y

			 unsigned dx = abs((int)(p_x-x));
			 unsigned dy = abs((int)(p_y-y));
			 if (dx>SimWH) dx = SimW-dx;
			 if (dy>SimHH) dy = SimH-dy;
			 if ((dx<=p_range) && (dy<=p_range))
			 {
//				 if (p_Weight>AFemale->SupplyWeight())
//				 {
					 AFemale->OnInfanticideAttempt();
					 //InfanticideOutput(p_ActualAge,p_IDNo);
//				 }
			 }
		  }
		}
	  }
	  else
	  {
		  m_TheLandscape->Warn("Vole_Population_Manager::SendMessage Error","Wrong sex specified for infanticide");
	  }
  }
  else
  {
	  m_TheLandscape->Warn("Vole_Population_Manager::SendMessage Error","Unknown message");
  }

}

//---------------------------------------------------------------------------

/**
Creates 'number' of vole objects of the type ob_type using 'as' for the base data
*/
void Vole_Population_Manager::CreateObjects(VoleObject ob_type, TAnimal* /* pvo */, struct_Vole_Adult * as, int number)
{
	Vole_JuvenileMale*  new_JMale;
	Vole_JuvenileFemale*  new_JFemale;
	Vole_Male*  new_Male;
	Vole_Female*  new_Female;

	for (int i = 0; i < number; i++)
	{
		if (ob_type == vob_JuvenileMale)
		{
			if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				dynamic_cast<Vole_JuvenileMale*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(as);
				IncLiveArraySize(ob_type);
			}
			else {
				new_JMale = new Vole_JuvenileMale(as);
                PushIndividual(ob_type, new_JMale);
				m_VoleMap->SetMapValue(as->x, as->y, (TAnimal*)new_JMale);
				IncLiveArraySize(ob_type);
			}

#ifdef __VOLEPESTICIDEON
			if (as->m_dflag)
			{  // Chromo 1. direct effect
				new_JMale->SetDirectFlag();
				new_JMale->SetGeneticFlag();
				if (g_rand_uni() >= m_f1sterilitychance) new_JMale->SetFertile(true);
				else new_JMale->SetFertile(false); //
				new_JMale->SetPesticideInfluenced2(true);
			}
			else
			{
				if (as->m_gflag)
				{  // Chromo 0, genetic effect
					new_JMale->UnsetDirectFlag();
					new_JMale->SetGeneticFlag();
					if (g_rand_uni() >= m_geneticsterilitychance) new_JMale->SetFertile(true); //100 means all are sterile, 0 means none are
					else new_JMale->SetFertile(false);
					new_JMale->SetPesticideInfluenced2(true);
				}
				else
				{
					new_JMale->UnsetGeneticFlag();
					new_JMale->UnsetDirectFlag();
					new_JMale->SetFertile(true);
				}
			}
#endif
		}
		if (ob_type == vob_JuvenileFemale)
		{
			if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				dynamic_cast<Vole_JuvenileFemale*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(as);
				IncLiveArraySize(ob_type);
			}
			else {
				new_JFemale = new Vole_JuvenileFemale(as);
                PushIndividual(ob_type, new_JFemale);
				m_VoleMap->SetMapValue(as->x, as->y, (TAnimal*)new_JFemale);
				IncLiveArraySize(ob_type);
			}

#ifdef __VOLEPESTICIDEON
			new_JFemale->SetMaturityDelay(as->misc_use);
#endif
		}
		if (ob_type == vob_Male)
		{
			if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				dynamic_cast<Vole_Male*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(as);
				IncLiveArraySize(ob_type);
			}
			else {
				new_Male = new Vole_Male(as);
                PushIndividual(ob_type, new_Male);
				m_VoleMap->SetMapValue(as->x, as->y, (TAnimal*)new_Male);
				IncLiveArraySize(ob_type);
			}

#ifdef __VOLEPESTICIDEON
			if (as->m_dflag)
			{  // Chromo 1. direct effect
				new_Male->SetDirectFlag();
				new_Male->SetGeneticFlag();
				if (g_rand_uni() >= m_f1sterilitychance) new_Male->SetFertile(true);
				else new_Male->SetFertile(false); //
				new_Male->SetPesticideInfluenced2(true);
			}
			else
			{
				if (as->m_gflag)
				{  // Chromo 0, genetic effect
					new_Male->UnsetDirectFlag();
					new_Male->SetGeneticFlag();
					if (g_rand_uni() >= m_geneticsterilitychance) new_Male->SetFertile(true); //100 means all are sterile, 0 means none are
					else new_Male->SetFertile(false);
					new_Male->SetPesticideInfluenced2(true);
				}
				else
				{
					new_Male->UnsetGeneticFlag();
					new_Male->UnsetDirectFlag();
					new_Male->SetFertile(true);
				}
			}
#endif
		}
		if (ob_type == vob_Female)
		{
			if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				dynamic_cast<Vole_Female*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(as);
				IncLiveArraySize(ob_type);
			}
			else {
				new_Female = new Vole_Female(as);
                PushIndividual(ob_type, new_Female);
				m_VoleMap->SetMapValue(as->x, as->y, (TAnimal*)new_Female);
				IncLiveArraySize(ob_type);
			}
		}
	}
}
//---------------------------------------------------------------------------

/**
Creates 'number' of vole objects of the type ob_type using 'as' for the base data for use at the beginning of a simulation. \n
A number of the attributes are set at defaults or randomised
*/
void Vole_Population_Manager::CreateObjects_Init(VoleObject ob_type,
           TAnimal* /* pvo */ ,struct_Vole_Adult * data,int number)
{
   struct_Vole_Adult *as=NULL;
   int Year = m_TheLandscape->SupplyYearNumber();

   for (int i=0; i<number; i++)
   {
    if (ob_type == vob_Male)
    {
       Vole_Male*  new_Male;
       as=dynamic_cast<struct_Vole_Adult *>(data);
	   as->FatherId = 10000000;
	   as->MotherId = 10000000;
	   as->FatherStateAtBirth = 2;
       new_Male = new Vole_Male(as);

       new_Male->SetWeight(40.0);
       new_Male->Setm_Mature();
	   new_Male->Set_Age(random(500));
	   new_Male->Set_BirthYear(Year);
	   new_Male->SetFertile(true);
       PushIndividual(ob_type, new_Male);
   }
    if (ob_type == vob_Female)
    {
       Vole_Female*  new_Female;
       as=dynamic_cast<struct_Vole_Adult *>(data);
	   as->FatherId = 10000000;
	   as->MotherId = 10000000;
	   as->FatherStateAtBirth = 2;
       new_Female = new Vole_Female(as);
       new_Female->SetWeight(40.0);
       new_Female->Setm_Mature();
       new_Female->Set_Age(1); // (random(500))
	   new_Female->Set_BirthYear(Year);
       PushIndividual(ob_type, new_Female);
    }
	IncLiveArraySize(ob_type);
   }
}
//---------------------------------------------------------------------------

void Vole_Population_Manager::Catastrophe( void ) {
	/** This version simply alters populations after 1st January - it is very dangerous to
	add individuals in many of the models so beware!!!! */

	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=1) return;
	// First do we have the right year?
	int year = m_TheLandscape->SupplyYearNumber()- cfg_CatastropheEventStartYear.value();
	;
	if (year%cfg_pm_eventfrequency.value()!=0) return;

	Vole_Male* MV = NULL; // assignment to get rid of warning
	Vole_Female* FV = NULL;
	Vole_Base* VB = NULL;
	// Now if the % decrease is higher or lower than 100 we need to do different things
	int esize=cfg_pm_eventsize.value();
	if (esize<100) {
		unsigned size2 = (unsigned)GetLiveArraySize(vob_Male);
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) > esize) {
                        MV = dynamic_cast < Vole_Male * > ( SupplyAnimalPtr(vob_Male, j));
					    MV->CurrentVState=tovs_MDying; // Kill it
				}
				}
			size2 = (unsigned)GetLiveArraySize(vob_Female);
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) > esize) {
                        FV = dynamic_cast < Vole_Female * > ( SupplyAnimalPtr(vob_Female, j) );
					    FV->CurrentVState=tovs_FDying; // Kill it
				}
				}
		}
	else if (esize>100) {
		// This is a tricky thing to do because we need to duplicate voles, but dare not mess
		// mate pointers etc up.
		// This also requires a copy method in the target vole
		// esize also needs translating  120 = 20%, 200 = 100%
		if (esize<200) {
			esize-=100;
			for (int i= (int)vob_JuvenileMale; i<= (int)vob_Female; i++) {
			unsigned size2;
			size2 = (unsigned)GetLiveArraySize(i);
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) < esize) {
                        VB = dynamic_cast < Vole_Base * > ( SupplyAnimalPtr(i, j));
					    VB->CopyMyself((VoleObject)i); // Duplicate it
					}
				}
				}
		} else {
			esize-=100;
			esize/=100; // this will throw away fractional parts so will get 1, 2, 3  from 200, 350 400
			for (int i= (int)vob_JuvenileMale; i<= (int)vob_Female; i++) {
				unsigned size2;
				size2 = (unsigned)GetLiveArraySize(i);
				for ( unsigned j = 0; j < size2; j++ ) {
					for ( int e=0; e<esize; e++) {
                        VB = dynamic_cast < Vole_Base * > ( SupplyAnimalPtr(i, j));
					    VB->CopyMyself((VoleObject)i); // Duplicate it
						}
				}
			}
		}
		}
	else return; // No change so do nothing
}
//-----------------------------------------------------------------------------

void Vole_Population_Manager::LandscapeQuadrantOutputProbe(int a_day)
{
	/** Output file facility added in January 2013 */
    TAnimal* VB;
	unsigned int totalYM, totalYF, totalAM, totalAF;
	int gridcount[256];
	/**
	* This output splits the landscape up into 16x16 cells which gives landscapes of 625x625m grids for 10x10 km. <br>
	* All voles are counted in the landspape and assigned to a grid. The sum of all individials in the grid is then output to the output file separated by tabs and prefixed by the global simulation day. <br>
	* NB this only works for square landscapes.
	*/
	int width = m_TheLandscape->SupplySimAreaWidth();
  	for (int i=0; i<256; i++) gridcount[i] = 0;
	double sqwidth = width/16.0;
	totalYM= (unsigned int)GetLiveArraySize(vob_JuvenileMale);
  	for (unsigned j=0; j<totalYM; j++)      //juvenile males
  	{

        int x{0}, y{0};
        SupplyLocXY(vob_JuvenileMale, j , x, y);
	  	int gx= (int) floor(x/sqwidth);
	  	int gy= (int) floor(y/sqwidth);
	  	gridcount[gx+gy*16]++;
  	}
	totalYF= (unsigned int) GetLiveArraySize(vob_JuvenileFemale);
  	for (unsigned j=0; j<totalYF; j++)      //juvenile females
  	{
        int x{0}, y{0};
        SupplyLocXY(vob_JuvenileFemale, j , x, y);

	  	int gx= (int) floor(x/sqwidth);
	  	int gy= (int) floor(y/sqwidth);
	  	gridcount[gx+gy*16]++;
  	}
	totalAM= (unsigned int) GetLiveArraySize(vob_Male);
  	for (unsigned j=0; j<totalAM; j++)      //adult males
  	{

        int x{0}, y{0};
        SupplyLocXY(vob_Male, j , x, y);
	  	int gx= (int) floor(x/sqwidth);
	  	int gy= (int) floor(y/sqwidth);
	  	gridcount[gx+gy*16]++;
  	}
	totalAF= (unsigned int) GetLiveArraySize(vob_Female);
  	for (unsigned j=0; j<totalAF; j++)      //adult females
  	{

        int x{0}, y{0};
        SupplyLocXY(vob_Female, j , x, y);
	  	int gx= (int) floor(x/sqwidth);
	  	int gy= (int) floor(y/sqwidth);
	  	gridcount[gx+gy*16]++;
  	}
	// Do the output
	/* Open the output file and append */
	ofstream ofile("VoleLandscapeGridData.txt",ios::app);
	ofile << a_day << '\t' << totalYM << '\t' << totalYF << '\t' << totalAM << '\t' << totalAF<< '\t';
	for (int i=0; i<255; i++)
	{
		ofile << gridcount[i] << '\t';
	}
	ofile << gridcount[255] << endl;
	ofile.close();

}


void Vole_Population_Manager::TheAOROutputProbe(  ) {
	m_AOR_Probe->DoProbe(vob_Female);
}
//-----------------------------------------------------------------------------

void Vole_Population_Manager::TheRipleysOutputProbe( FILE* a_prb ) {
  Vole_Female* FS;
  unsigned int totalF= (unsigned int) GetLiveArraySize(vob_Female);
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {

      SupplyLocXY(vob_Female, j, x, y);

	  fprintf(a_prb,"%d\t%d\n", x,y);
  }
  fflush(a_prb);
}
//-----------------------------------------------------------------------------
void Vole_Population_Manager::TheAgeSexLocationProbe()
{
  Vole_Base* VB;
  int year = m_TheLandscape->SupplyYearNumber();
  int day = m_TheLandscape->SupplyDayInYear();
  int x,y;
  char sex[4] = {'m','f','M','F'};

  /**
  The code is separated into two loops and calls duplicated. This is because there may be sex-specific calls placed
  here which would be imcompatible with a single data input/output method.\n
  This is called the Really Big Probe because 10yrs of daily output on 10x10km landscape can easily produce a >500MB text file.
  Use carefully. \n
  */
  // Check all males
  for (int vob=0; vob<=(int)vob_Female; vob++)
  {
	  unsigned int total = (unsigned int) GetLiveArraySize(vob);
	  for (unsigned j=0; j<total; j++)
	  {

          Population_Manager::SupplyLocXY(vob, j , x, y);

		  int Age=VB->SupplyAge();
		 (* m_VoleAgeSexLocationFile) << year << '\t' << day << '\t' << sex[vob] << '\t' << Age << '\t' << x << '\t' << y << endl;
	  }
  }
}
//-----------------------------------------------------------------------------

/**
An output facility. This method can be re-written to provide any data necessary on a daily or annual basis
e.g. for pattern oriented modelling purposes. Outputs can be altered and added to the print statement as necessary.
*/
void Vole_Population_Manager::TheReallyBigOutputProbe() {

  Vole_JuvenileFemale* JFV;
  Vole_JuvenileMale* JMV;
  Vole_Female* FV;
  Vole_Male* MV;
  int year = m_TheLandscape->SupplyYearNumber();
  int day = m_TheLandscape->SupplyDayInYear();
  int x,y;
  /**
  The code is separated into two loops and calls duplicated. This is because there may be sex-specific calls placed
  here which would be imcompatible with a single data input/output method.\n
  This is called the Really Big Probe because 10yrs of daily output on 10x10km landscape can easily produce a >500MB text file.
  Use carefully. \n
  */
  // Check all males
  unsigned int totalM = (unsigned int) GetLiveArraySize(vob_Male);
  for (unsigned j=0; j<totalM; j++)
  {
	  MV=dynamic_cast<Vole_Male*>(SupplyAnimalPtr(vob_Male, j));
      SupplyLocXY(vob_Male, j, x, y);


	  int poly=m_TheLandscape->SupplyPolyRef(x,y);
	  int ele=m_TheLandscape->SupplyElementType(poly);
	  int vegt=m_TheLandscape->SupplyVegType(poly);
	  int Age=MV->SupplyAge();
	  int Ter=(int)MV->SupplyTerritorial();
	  fprintf(ReallyBigOutputPrb,"%d\t%d\t%d\t%d\t adM\t%d\t%d\t%d\t%d\t%d\t%d\n", year,day,x,y,0,poly,ele,vegt,Age,Ter);
  }
  totalM = (unsigned int) GetLiveArraySize(vob_JuvenileMale);
  for (unsigned j=0; j<totalM; j++)
  {
	  JMV=dynamic_cast<Vole_JuvenileMale*>(SupplyAnimalPtr(vob_JuvenileMale, j));
      SupplyLocXY(vob_JuvenileMale, j, x, y);

	  int poly=m_TheLandscape->SupplyPolyRef(x,y);
	  int ele=m_TheLandscape->SupplyElementType(poly);
	  int vegt=m_TheLandscape->SupplyVegType(poly);
	  int Age=JMV->SupplyAge();
	  int Ter=(int)JMV->SupplyTerritorial();
	  fprintf(ReallyBigOutputPrb,"%d\t%d\t%d\t%d\t juvM\t%d\t%d\t%d\t%d\t%d\t%d\n", year,day,x,y,0,poly,ele,vegt,Age,Ter);
  }
  // Do the same for females
  unsigned int totalF = (unsigned int) GetLiveArraySize(vob_Female);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
	  FV=dynamic_cast<Vole_Female*>(SupplyAnimalPtr(vob_Female, j));
      SupplyLocXY(vob_Female, j, x, y);
	  int poly=m_TheLandscape->SupplyPolyRef(x,y);
	  int ele=m_TheLandscape->SupplyElementType(poly);
	  int vegt=m_TheLandscape->SupplyVegType(poly);
	  int Age=FV->SupplyAge();
	  int Ter=(int)FV->SupplyTerritorial();
	  fprintf(ReallyBigOutputPrb,"%d\t%d\t%d\t%d\t adF\t%d\t%d\t%d\t%d\t%d\t%d\n", year,day,x,y,0,poly,ele,vegt,Age,Ter);
  }
  totalF = (unsigned int) GetLiveArraySize(vob_JuvenileFemale);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
	  JFV=dynamic_cast<Vole_JuvenileFemale*>(SupplyAnimalPtr(vob_JuvenileFemale, j));
      SupplyLocXY(vob_JuvenileFemale, j, x, y);
	  x=JFV->Supply_m_Location_x();
	  y=JFV->Supply_m_Location_y();
	  int poly=m_TheLandscape->SupplyPolyRef(x,y);
	  int ele=m_TheLandscape->SupplyElementType(poly);
	  int vegt=m_TheLandscape->SupplyVegType(poly);
	  int Age=JFV->SupplyAge();
	  int Ter=(int)JFV->SupplyTerritorial();
	  fprintf(ReallyBigOutputPrb,"%d\t%d\t%d\t%d\t juvF\t%d\t%d\t%d\t%d\t%d\t%d\n", year,day,x,y,0,poly,ele,vegt,Age,Ter);
  }
  fflush(ReallyBigOutputPrb);
}
//-----------------------------------------------------------------------------

/**
Open the sex ratio probe
*/
bool Vole_Population_Manager::OpenSexRatiosProbe() {
  SexRatiosPrb = fopen(cfg_SexRatiosOutput_filename.value(), "w" );
  if ( !SexRatiosPrb ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenSexRatiosProbe(): ""Unable to open probe file",
         cfg_SexRatiosOutput_filename.value() );
    exit( 1 );
  }
  fprintf(SexRatiosPrb,"Year\tDay\tSubMales\tAdMalesThisYear\tAdMalesLatYear\tSubFemales\tAdFemalesThisYear\tAdFemalesLatYear\tJuvMales\tJuvFemales\tTotalMales\tTotalFemales\n");
  return true;
}
//-----------------------------------------------------------------------------

/**
Close the sex ratio probe
*/
void Vole_Population_Manager::CloseSexRatiosProbe() {
  if ( SexRatiosPrb )
    fclose( SexRatiosPrb );
  SexRatiosPrb=NULL;
}
//-----------------------------------------------------------------------------

/**
This does the same as the ReallyBigProbe, but digests the data to produce a daily output instead of
individual vole data. This only works for the whole population not a subset.
*/
void Vole_Population_Manager::TheSexRatiosProbe()
{
	Vole_Base* VB;
	int year = m_TheLandscape->SupplyYearNumber();
	int day = m_TheLandscape->SupplyDayInYear();
	fprintf(SexRatiosPrb, "%d\t%d\t", year, day);
	// Create our counters
	int Ad1 = { 0 };
	int Ad2 = { 0 };
	int Sub = { 0 };
	for (int v = (int)vob_Male; v <= (int)vob_Female; v++) {
		unsigned int total = (unsigned int)GetLiveArraySize(v);
		for (unsigned j = 0; j < total; j++)
		{
			VB = dynamic_cast<Vole_Base*>(SupplyAnimalPtr(v, j));
			//TTypesOfLandscapeElement tole = VB->SupplyElemType();
			//if ((tole == tole_Orchard) || (tole == tole_NaturalGrassDry) )
			{
				bool mature = (int)VB->SupplyMature();
				bool born_lastyear = VB->SupplyBornLastYear();
				if (!mature) Sub++; else if (born_lastyear) Ad2++; else Ad1++;
			}
		}
		fprintf(SexRatiosPrb, "%d\t%d\t%d\t", Sub, Ad1, Ad2);
		Sub = 0; Ad1 = 0; Ad2 = 0;
	}
	fprintf(SexRatiosPrb, "%d\t%d\t%d\t%d\n", (int)GetLiveArraySize(vob_JuvenileMale), (int)GetLiveArraySize(vob_JuvenileFemale), (int)GetLiveArraySize(vob_Male), GetLiveArraySize(vob_Female));
}
//-----------------------------------------------------------------------------
TrapLineMap::TrapLineMap( unsigned int a_width, unsigned int a_height, unsigned int a_resolution, const char* a_file ) : BinaryMapBase(a_width, a_height, a_resolution, 2 )
{
	Init(a_file);
}

TrapLineMap::~TrapLineMap()
{
	fclose(m_ofile);
}

/**
* Clears the map then reads the list of co-ordinates from the text file, saves these in m_TrapList.
* At the same time this creates the trap map by putting 1s at each co-ordinate.
*/
void TrapLineMap::Init( const char* a_inifile )
{
	ClearMap();
	FILE * inpfile=fopen(a_inifile, "r" );
	if (!inpfile) {
		g_msg->Warn( WARN_FILE, " TrapLineMap::Init Unable to open file ", a_inifile );
		exit( 1 );
	}
	APoint pt;
	unsigned input, x, y;
	fscanf( inpfile, "%d\n", & input );
	m_noTraps = input;
	for (unsigned i=0; i< m_noTraps; i++)
	{
		fscanf( inpfile, "%d\t%d\n", &x, &y );
		pt.m_x = x;
		pt.m_y = y;
		m_TrapCoords.push_back(pt);
		SetValue( x, y, 1 );
	}
	fclose(inpfile);
	// Open the output file
	m_ofile = fopen("VoleTrapCounts.txt","w");
	if (!m_ofile) {
		g_msg->Warn( WARN_FILE, " TrapLineMap::Init Unable to open output file ", "VoleTrapCounts.txt" );
		exit( 1 );
	}
	fprintf(m_ofile, "Day\tx\ty\tElement\tVegType\tSex\tTerritorial\tAge\tBornX\tBornY\tID no\n");
}

void TrapLineMap::Output(InTrapPosition a_tp, int a_day, int a_sex, bool a_terr, int a_age, int a_bx, int a_by, int a_ID )
{
	char terr;
	if (a_terr) terr='Y'; else terr = 'N';
	fprintf(m_ofile, "%d\t%d\t%d\t%d\t%d\t%d\t%c\t%d\t%d\t%d\t%d\n", a_day, a_tp.m_x, a_tp.m_y, a_tp.m_EleType, a_tp.m_VegType, a_sex, terr, a_age, a_bx, a_by, a_ID);
	fflush(m_ofile);
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

VoleSummaryOutput::VoleSummaryOutput(const char* a_filename, Landscape* a_land, int a_numdataINT, int a_numdataDOUBLE)
{
	m_landscape = a_land;
	OpenOutput( a_filename );
	m_ndInt = a_numdataINT;
	m_ndDouble = a_numdataDOUBLE;
	ResetData();
}
VoleSummaryOutput::~VoleSummaryOutput()
{
	CloseOutput();
}
void VoleSummaryOutput::OPrint()
{
	(*m_File) << m_landscape->SupplyYearNumber() << '\t' << m_landscape->SupplyDayInYear();
	for (int i= 0; i< m_ndInt; i++) (*m_File) << '\t' << m_dataI[i];
	for (int i= 0; i< m_ndDouble; i++) (*m_File) << '\t' << m_dataD[i];
	(*m_File) << endl;
}
void VoleSummaryOutput::OPrint(int a_value)
{
	(*m_File) << a_value << '\t';
}
void VoleSummaryOutput::OPrint(double a_value)
{
	(*m_File) << a_value << '\t';
}
void VoleSummaryOutput::OPrint(const char* a_value)
{
	(*m_File) << a_value << '\t';
}
void VoleSummaryOutput::OPrintEndl()
{
	(*m_File) << endl;
}
void VoleSummaryOutput::OpenOutput(const char* a_filename)
{
	m_File = new ofstream(a_filename,ios::out);
}
void VoleSummaryOutput::CloseOutput()
{
	m_File->close();
	delete m_File;
}
void VoleSummaryOutput::ResetData()
{
	for (int i= 0; i< m_ndInt; i++) m_dataI[i] = 0;
	for (int i= 0; i< m_ndDouble; i++) m_dataD[i] = 0.0;
}
void VoleSummaryOutput::ChangeData(int a_data, int a_value)
{
	m_dataI[a_data]+=a_value;
}
void VoleSummaryOutput::ChangeData(int a_data, double a_value)
{
	m_dataD[a_data]+=a_value;
}
//-----------------------------------------------------------------------------

/** Used to output vole genetics at user-defined dates*/
void Vole_Population_Manager::GeneticsOutputFile(unsigned listindex)
{
	FILE* vfile= fopen ("GeneticsData.txt","a");
	Vole_Base* MV;
	if (vfile == NULL) {
		m_TheLandscape->Warn("Vole_Population_Manager::GeneticsOutputFile","Could Not Open GeneticsData.txt File");
		  exit(0);
	}
	int Y=m_TheLandscape->SupplyYearNumber();
	int M=m_TheLandscape->SupplyMonth();
	int D=m_TheLandscape->SupplyDayInYear();
	unsigned size= (unsigned)GetLiveArraySize(listindex);
	unsigned TotalSize = (unsigned)GetLiveArraySize(vob_Male)+ GetLiveArraySize(vob_Female);
	int w = m_TheLandscape->SupplySimAreaWidth();
	int h = m_TheLandscape->SupplySimAreaWidth();
	fprintf(vfile,"\n");
	fprintf(vfile,"%d\t %d\t %d\t %d\t %d\t %d\n", 0,w ,0, h, size, TotalSize);
	fprintf(vfile,"\n");
	fprintf(vfile,"%s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\n", "Year", "Mo", "Day", "ID", "Sex", "Age", "Birth Yr", "Terr", "Ma", "TotY", "x", "y", "Poly", "Ele", "Vege", "GA", "DA", "Bx", "By", "Bpol", "Bele", "Bveg");
	for (unsigned j=0; j<size; j++)
	{
		MV=dynamic_cast<Vole_Base*>(SupplyAnimalPtr(vob_Male, j));
        int x{0}, y{0};
        SupplyLocXY(vob_Male, j, x, y);

		int poly=m_TheLandscape->SupplyPolyRef(x,y);
		int ele=m_TheLandscape->SupplyElementType(poly);
		int vegt=m_TheLandscape->SupplyVegType(poly);
		int GA=MV->GetGeneticFlag();
		int DA=MV->GetDirectFlag();

		unsigned Age=MV->SupplyAge();
		int BY=MV->SupplyBirthYear();
		int Mature=MV->SupplyMature();
		int Terr=MV->SupplyTerritorial();
		int TotYoung = 0;

		int BornX=MV->SupplyXBorn();
		int BornY=MV->SupplyYBorn();
		int BornPoly=MV->SupplyPolyRefBorn();
		int BornEle=MV->SupplyElemBorn();
		int BornVegt=MV->SupplyVegBorn();
		int sex=(int)MV->SupplySex();
		int ID=MV->SupplyIDNo();

		 fprintf(vfile,"%d\t",Y); //Year
		 fprintf(vfile,"%d\t",M); // month letter
		 fprintf(vfile,"%d\t",D);

		 fprintf(vfile,"%d\t",ID);
		 fprintf(vfile,"%d\t",sex); // sex
		 fprintf(vfile,"%u\t",Age);
		 fprintf(vfile,"%d\t",BY); //BirthYear
		 fprintf(vfile,"%d\t",Terr);
		 fprintf(vfile,"%d\t",Mature);
		 fprintf(vfile,"%d\t",TotYoung);
		 fprintf(vfile,"%d\t",x); // coordinate
		 fprintf(vfile,"%d\t",y); // coordinate
		 fprintf(vfile,"%d\t",poly); // polygon
		 fprintf(vfile,"%d\t",ele); // element type
		 fprintf(vfile,"%d\t",vegt); // vegetation type
		 fprintf(vfile,"%d\t",GA); // Genetic affected
		 fprintf(vfile,"%d\t",DA); // Direct affected
		 fprintf(vfile,"%d\t",BornX); //X-coordinat for birth location
		 fprintf(vfile,"%d\t",BornY); //Y-coordinat for birth location
		 fprintf(vfile,"%d\t",BornPoly);
		 fprintf(vfile,"%d\t",BornEle);
		 fprintf(vfile,"%d\t",BornVegt);

		 for(int i=0; i< 16; i++)
		 {
			 for (int jj=0; jj<2; jj++)
			 {
				 uint32 allele = MV->SupplyMyAllele(i, jj);
				 fprintf(vfile,"%u\t",allele);
			 }
		 }
		 fprintf(vfile,"\n");
		 ID++;
	}
	fclose(vfile);
}
//-----------------------------------------------------------------------------

/**
Output recording the genetic information for each individual in the population of listindex type
*/
void Vole_Population_Manager::GeneticsResultsOutput(FILE* ofile,unsigned listindex)
{
  char month [13] = {'0','J','F','M','A','m','j','u','a','S','O','N','D'};
  int ID=0;
  int Y=m_TheLandscape->SupplyYear();
  int M=m_TheLandscape->SupplyMonth();
  unsigned size = (unsigned)GetLiveArraySize(listindex);
  if (size>0)
  {
   if (listindex==0)
   {
    Vole_Male* MV;
    for (unsigned j=0; j<size; j++)
    {
        MV=dynamic_cast<Vole_Male*>(SupplyAnimalPtr(vob_Male, j));
        int x{0}, y{0};
        SupplyLocXY(vob_Male, j, x, y);

          // Outputs genetic results
          // Year \t Month \t sex \t individual code \t XX\tXX\tXX\tXX\tXX\tXX\tXX\tXXn .... 32 loci x 2 alleles
          fprintf(ofile,"%d\t",ID);
          fprintf(ofile,"M");
          fprintf(ofile,"%c",month[M]);
          fprintf(ofile,"%d\t",Y);
          fprintf(ofile,"%d\t",x);
          fprintf(ofile,"%d\t",y);
          for (int g=0; g<32; g++)
          {
             int allele=1+MV->SupplyAllele(g,0);
             fprintf(ofile,"%d\t",allele);
             allele=1+MV->SupplyAllele(g,1);
             fprintf(ofile,"%d\t",allele);
          }
          fprintf(ofile,"\n");
          ID++;
        }
  }
  else
  {
    Vole_Female* FV;
    for (unsigned j=0; j<size; j++)
    {
      FV=dynamic_cast<Vole_Female*>(SupplyAnimalPtr(vob_Female, j));
          int x{0}, y{0};
          SupplyLocXY(vob_Female, j, x, y);

          // Outputs genetic results
          // Year \t Month \t sex \t individual code \t XX\tXX\tXX\tXX\tXX\tXX\tXX\tXXn .... 32 loci x 2 alleles
          fprintf(ofile,"%d\t",ID);
          fprintf(ofile,"F");
          fprintf(ofile,"%c",month[M]);
          fprintf(ofile,"%d\t",Y);
          fprintf(ofile,"%d\t",x);
          fprintf(ofile,"%d\t",y);
          for (int g=0; g<32; g++)
          {
             int allele=1+FV->SupplyAllele(g,0);
             fprintf(ofile,"%d\t",allele);
             allele=1+FV->SupplyAllele(g,1);
             fprintf(ofile,"%d\t",allele);
          }
          fprintf(ofile,"\n");
          ID++;
        }
      }
  }
}
//-----------------------------------------------------------------------------

void Vole_Population_Manager::OpenResistanceOutput()
{
	m_VoleResistanceOutputFile = new ofstream("VoleResistanceOutput.txt", ios::out);
	if (!m_VoleResistanceOutputFile->is_open())
	{
		m_TheLandscape->Warn("Vole_Population_Manager::GeneticsOutputFile","Could Not Open VoleResistanceOutput.txt File");
		exit(0);
	}
	(* m_VoleResistanceOutputFile) << "year" << '\t' << "day" << '\t' << "Resistgene" << '\t' << "Neutralgene" << '\t'  << "Frequency" << '\t'  << "Population Size" <<endl;
}
//-----------------------------------------------------------------------------

void Vole_Population_Manager::CloseResistanceOutput()
{
	if (!m_VoleResistanceOutputFile->is_open()) m_VoleResistanceOutputFile->close();
	delete m_VoleResistanceOutputFile;
}
//-----------------------------------------------------------------------------

void Vole_Population_Manager::ResistanceOutput()
{
    uint32 allele1, allele2;
	int Rgene = 0;
	int Ngene = 0;
	Vole_Base*  new_VB;
	int Y=m_TheLandscape->SupplyYearNumber();
	int D=m_TheLandscape->SupplyDayInYear();
	for (int listindex=0; listindex<vob_foobar; listindex++)
	{
		unsigned size= (unsigned)GetLiveArraySize(listindex);
		for (unsigned j=0; j<size; j++)
		{
			new_VB = dynamic_cast<Vole_Base *>(SupplyAnimalPtr(listindex, j));
			allele1 = new_VB->SupplyMyAllele(3, 0);
			allele2 = new_VB->SupplyMyAllele(3, 1);
			if ( cfg_ResistanceDominant.value())
			{
				if ((allele1 == 1) || (allele2 == 1))  Rgene++; else Ngene++;
			}
			else
			{
				if ((allele1 == 1) && (allele2 == 1))  Rgene++; else Ngene++;
			}
		}
	}
	double freq = (double)Rgene/(double) (Rgene+Ngene);
	(* m_VoleResistanceOutputFile) << Y << '\t' << D << '\t' << Rgene << '\t' << Ngene << '\t' << freq << '\t' << Rgene+Ngene << endl;
}
//---------------------------------------------------------------------------


