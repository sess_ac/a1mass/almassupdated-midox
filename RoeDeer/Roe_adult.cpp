//
// roe_adult.cpp
//


#include <iostream>
#include <fstream>
#pragma hdrstop

#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"


#include "Roe_all.h"
#include "Roe_constants.h"

extern int FastModulus( int a_val, int a_modulus );

/**
\brief
Function to compare to APoint m_Location_x
*/
class CompareAPointX {
public:
    bool operator() (APoint A1, APoint A2) const {
        return (A1.m_x < A2.m_x);
    }
};

//---------------------------------------------------------------------------
//                              Roe_Adult_Base
//---------------------------------------------------------------------------
void Roe_Adult_Base::BeginStep (void)
{
 if (m_CurrentStateNo==-1) return;
}
//---------------------------------------------------------------------------
void Roe_Adult_Base::Step (void)
{
 if (m_StepDone || (m_CurrentStateNo==-1)) return;
}
//---------------------------------------------------------------------------
void Roe_Adult_Base::EndStep (void)
{
 if (m_CurrentStateNo==-1) return;
}
//---------------------------------------------------------------------------

//--------------------------------------------------------------------------
Roe_Adult_Base::Roe_Adult_Base(int x, int y, double size, int age, Landscape* L,
    RoeDeer_Population_Manager* RPM) : Roe_Base(x, y, L, RPM)
{
    m_DispCount = 0;
}
//---------------------------------------------------------------------------
void Roe_Adult_Base::ReInit(int x, int y, double size, int age, Landscape* L, RoeDeer_Population_Manager* RPM)
{
    Roe_Base::ReInit(x, y, L, RPM);
    m_DispCount = 0;
}
//---------------------------------------------------------------------------
Roe_Adult_Base::~Roe_Adult_Base ()
{
}

//----------------------------------------------------------------------------
void Roe_Adult_Base::CalculateArea()
{
    /**  Calculate the area of the convex hull containing all the locations
        in m_FixList and write the result to file
        1: sort according to increasing x and yvalues
        2: find Left, Right
        3: for each point decide which are above and below the lr-line
        4: find h that gives the triangle with max area
        5: eliminate all points within the triangle
        6: calculate area of lrh and add to total area
        7: repeat for lower hull
        8: repeat recursively for the 4 (max) new subsets adding the area of
           each triangle to total
        9: write total area to file with a reference to deer
        Makes use of 3 methods; Sort(),Divide1(), Divide2()  */
    //first sort the array
    sort(m_FixedLocationRecord.begin(), m_FixedLocationRecord.end(), CompareAPointX());
    //now divide the points above and below the line, m_UpperList and m_LowerList
    Divide1(0, int(m_FixedLocationRecord.size()) - 1);   //initial division of m_FixList, Divide1 calls Divide2
    m_HomeRange = (double)m_totalarea / 1000000;  //sqkm

}
//-------------------------------------------------------------------------------

void Roe_Adult_Base::Divide1(int p_a, int p_b)
{   //TODO need to check home range results with fixfile!!

  //create 2 new subarrays, upper starts with min and ends with max
    long u_area = 0;
    long l_area = 0;
    int u_c = 0;
    int l_c = 0;
    int u_N = 0;
    int l_N = 0;
    vector<APoint> UpperList, LowerList;

    for (int i = 0; i < p_b; i++) //for all points on list
    {  //Calculates twice the area, if area >= 0 then a point belongs to upper,
       //else to lower
        long area = m_FixedLocationRecord[i].m_x * (m_FixedLocationRecord[p_a].m_y - m_FixedLocationRecord[p_b].m_y) +
            m_FixedLocationRecord[p_a].m_x * (m_FixedLocationRecord[p_b].m_y - m_FixedLocationRecord[i].m_y) +
            m_FixedLocationRecord[p_b].m_x * (m_FixedLocationRecord[i].m_y - m_FixedLocationRecord[p_a].m_y);

        if (area > 0) //belongs to upper
        {
            //add to upper
            UpperList.push_back(m_FixedLocationRecord[i]);
            u_N++;
            if (area > u_area) //so far this is the largest triangle
            {
                u_area = area;
                u_c = i; //the 3rd corner in largest triangle
            }
        }
        else if (area < 0)
        {
            //add to lower
            LowerList.push_back(m_FixedLocationRecord[i]);
            l_N++;
            if (area * -1 > l_area)
            {
                l_area = area * -1;  //positive area
                l_c = i;
            }
        }
    }
    //add areas to total
    m_totalarea += (u_area + l_area) / 2;
    //now call Divide2 to do the calculations on the subarrays
    Divide2(0, u_N, u_c, true, &UpperList, &LowerList);  //upper list
    Divide2(0, l_N, l_c, false, &UpperList, &LowerList);  //lower list
}
//---------------------------------------------------------------------------
void Roe_Adult_Base::Divide2(int p_a, int p_b, int p_c, bool p_upper, vector<APoint>* p_upperlist, vector<APoint>* p_lowerlist)
{   //divide the 2 subarrays further, recursive function
    int u_N = -1;
    int l_N = -1;
    long l_area = 0;
    long r_area = 0;
    int l_c = 0;
    int r_c = 0;
    int l_index = -1;
    int r_index = -1;
    int l_list[72], r_list[72];
    if (p_upper == true) //do upper array
    {
        for (int i = p_a; i < p_b; i++) //for all points on list
        {
            //Calculates twice the area with respect to aci and cbi, if area >= 0
            //then a point belongs to upper, else to lower
            long area1 = (*p_upperlist)[i].m_x * ((*p_upperlist)[p_a].m_y - (*p_upperlist)[p_c].m_y) +
                (*p_upperlist)[p_a].m_x * ((*p_upperlist)[p_c].m_y - (*p_upperlist)[i].m_y) +
                (*p_upperlist)[p_c].m_x * ((*p_upperlist)[i].m_y - (*p_upperlist)[p_c].m_y);
            if (area1 > 0)
            {
                l_index++;
                l_list[l_index] = i;
                u_N++;
                if (area1 > l_area)
                {
                    l_area = area1;
                    l_c = i;
                }
            }
            else if (area1 < 0)
            {
                long area2 = (*p_upperlist)[i].m_x * ((*p_upperlist)[p_c].m_y - (*p_upperlist)[p_b].m_y) +
                    (*p_upperlist)[p_c].m_x * ((*p_upperlist)[p_b].m_y - (*p_upperlist)[i].m_y) +
                    (*p_upperlist)[p_b].m_x * ((*p_upperlist)[i].m_y - (*p_upperlist)[p_c].m_y);

                if (area2 > 0)
                {
                    r_index++;
                    r_list[r_index] = i;
                    u_N++;
                    if (area2 > r_area) //so far this is the largest triangle
                    {
                        r_area = area2;
                        r_c = i; //the 3rd corner in largest triangle
                    }
                } //ignore lower part of the set
            }
        }
    }

    else //do lower subarray
    {
        for (int i = p_a; i < p_b; i++) //for all points on list
        {
            //Calculates twice the area with respect to aci and cbi, if area >= 0
            //then a point belongs to upper, else to lower
            long area1 = (*p_lowerlist)[i].m_x * ((*p_lowerlist)[p_a].m_y - (*p_lowerlist)[p_c].m_y) +
                (*p_lowerlist)[p_a].m_x * ((*p_lowerlist)[p_c].m_y - (*p_lowerlist)[i].m_y) +
                (*p_lowerlist)[p_c].m_x * ((*p_lowerlist)[i].m_y - (*p_lowerlist)[p_a].m_y);
            if (area1 > 0)
            {
                l_index++;
                l_list[l_index] = i;
                l_N++;
                if (area1 > l_area)
                {
                    l_area = area1;
                    l_c = i;
                }
            }
            else
            {
                long area2 = (*p_lowerlist)[i].m_x * ((*p_lowerlist)[p_c].m_y - (*p_lowerlist)[p_b].m_y) +
                    (*p_lowerlist)[p_c].m_x * ((*p_lowerlist)[p_b].m_y - (*p_lowerlist)[i].m_y) +
                    (*p_lowerlist)[p_b].m_x * ((*p_lowerlist)[i].m_y - (*p_lowerlist)[p_c].m_y);

                if (area2 > 0)
                {
                    r_index++;
                    r_list[r_index] = i;
                    if (area2 > r_area) //so far this is the largest triangle
                    {
                        r_area = area2;
                        r_c = i; //the 3rd corner in largest triangle
                    }
                }
            }
        }
    }
    //add area to total
    m_totalarea += (l_area + r_area) / 2;
    //r_index and l_index now indicates whether there are any locations left
    if (l_index > 0)
    {
        if (r_index > 0) //call for both left and right
        {
            //points left on both sides, make a local store of a, b and c
            int a = l_list[0];
            int b = l_list[l_index];
            int c = l_c;
            Divide2(r_list[0], r_list[r_index], r_c, p_upper, p_upperlist, p_lowerlist);
            Divide2(a, b, c, p_upper, p_upperlist, p_lowerlist);
        }
        else  //only call for left
        {
            Divide2(l_list[0], l_list[l_index], l_c, p_upper, p_upperlist, p_lowerlist);
        }
    }
}
//---------------------------------------------------------------------------

