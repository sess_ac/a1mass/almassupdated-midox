//
// roe_pop_manager.cpp
//
#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include <algorithm>
#include "Roe_all.h"
#include "Roe_constants.h"
#include "Roe_pop_manager.h"

// Use the define below to turn on home range calculations
//#define  __ROE_USINGFIXES

extern CfgBool cfg_ReallyBigOutputMonthly_used;
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;
extern CfgBool cfg_ReallyBigOutput_used;



RoeDeerThreatProbability::RoeDeerThreatProbability(Landscape* a_land, int a_gridsize, double a_baseprobability)
{
    /**
    * The probability of events is scaled to an underlying chance, and then by the proportion of human related activity. \n
    * The first step is to pass through the landscape and count all 'dangerous' m2 per cell, then convert to a proportion.
    */
    // Calculate the cell numbers
    int SimW = a_land->SupplySimAreaWidth();
    int SimH = a_land->SupplySimAreaHeight();
    m_max_x = SimW / a_gridsize;
    if (SimW % a_gridsize != 0) m_max_x++;  // Add one if there is an odd bit at the end
    m_max_y = SimH / a_gridsize;
    m_cellnumber = m_max_x * m_max_y;
    m_cellsize = a_gridsize;
    if (SimH % m_cellsize != 0) m_max_y++;  // Add one if there is an odd bit at the end
    // Save the space to fill in probabilities
    m_ThreatProbability.resize(m_max_x * m_max_y);
    // Plough through the landscape and assign the threat potential
    for (int x = 0; x < SimW; x++)
        for (int y = 0; y < SimH; y++)
        {
            TTypesOfLandscapeElement tole = a_land->SupplyElementType(x, y);
            int cell = (x / m_cellsize) + ((y / m_cellsize) * m_max_x);
            if (a_land->SupplyIsHumanDominated(tole)) m_ThreatProbability[cell] += 1.0;
        }
    double cellarea = double(m_cellsize * m_cellsize);
    for (int c = 0; c < m_cellnumber; c++) m_ThreatProbability[c] = (m_ThreatProbability[c]/cellarea) * a_baseprobability;
}

RoeDeer_Population_Manager::RoeDeer_Population_Manager(Landscape* L)//changed from L to p_L 13/7/2012 ljk
   : Population_Manager(L , 3)
{
  m_TheGrid = new Roe_Grid(L);
  m_ThreatEventsCalc = new RoeDeerThreatProbability(L, 1000, ThreatBaseProbability);
  Turnover=0;  //total number of individuals created during a simulation
  StepCounter=0;
  m_AverageMaleSize = 0;
  m_FemaleDispThreshold = 0;
  m_MinWeight_Males = 0;
  m_MinWeight_Females = 0;
  m_CriticalWeight_Males = 0;
  m_CriticalWeight_Females = 0;
  m_MaturedThisYear = 0;
  m_BornThisYear = 0;
  m_DiedAdultsThisYear = 0;
  m_DiedYoungThisYear = 0;
  m_lastgrouprefnum = 0;
  m_population_type = TOP_RoeDeer;

  // empty all the group pointers
  m_GroupList.clear();

  // Get SimW & SimH (landscape size)
  SimW= L->SupplySimAreaWidth();
  SimH= L->SupplySimAreaHeight();
  m_MoveMap=new MovementMap(L, 8); // 8 for roe deer //**ljk inserted on 25/7-2012
  Init();
}
//---------------------------------------------------------------------------
RoeDeer_Population_Manager::~RoeDeer_Population_Manager()
{
  delete m_TheGrid;
  delete m_ThreatEventsCalc;
}
//---------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::Init - Initialises the run ( and is thus only accessed once at beginning),
by checking for output probes, preprocess landscape to get the quality of grid cells in m_TheGrid, 
adds female and male objects for year 0 and loads state names. Calls functions PreProcessLandscape() 
and SupplyElementType().
*/
void RoeDeer_Population_Manager::Init(void)
{
    m_CriticalWeight_Males = CriticalWeightMales;
    m_MinWeight_Males = MinWeightMales;
    m_CriticalWeight_Females = CriticalWeightMales;
    m_MinWeight_Females = MinWeightMales;

    m_SimulationName = "RoeDeer";

    //ljk added 22/8-2012
    if (cfg_RipleysOutput_used.value()) {
        OpenTheRipleysOutputProbe("");
    }
    if (cfg_ReallyBigOutput_used.value()) {
        OpenTheReallyBigProbe();
    }
    else ReallyBigOutputPrb = 0;

    ofstream Ofile("RoeDeerAgeStructure.txt", ios::out);
    Ofile << "Year" << '\t' << "Age0" << '\t' << "Age1" << '\t' << "Age2" << '\t' << "Age3" << '\t' << "Age4" << '\t' << "Age5" << '\t' << "Age6" << '\t' << "Age7" << '\t' << "Age8" << '\t' << "Age9" << '\t' << "Age10" << '\t' << "Age11" << '\t' << "Age12" << '\t' << "Age13" << '\t' << "Age14" << '\t' << "Age15" << '\t' << "Age16" << '\t' << "Age17" << '\t' << "Age18" << '\t' << "Age19" << '\t' << "Age20" << '\t' << "Born" << '\t' << "Matured" << '\t' << "DeadYoung" << '\t' << "DeadAdult" << '\n';;
    Ofile.close();
    ofstream Ofile2("RoeDeerSizeDistribution.txt", ios::out);
    Ofile2 << "Year";
    for (int i = 0; i < 2; i++)
    {
        char s;
        if (i == 0) s = 'M'; else s = 'F';
        for (int j = 0; j < 30; j++)
        {
            Ofile2 << '\t' << s << j+10;
        }
        Ofile2 << '\t' << "Error";
    }
    Ofile2 << '\n';
    Ofile2.close();

    ofstream Ofile3("RoeEnergyBudgets.txt", ios::out);
    Ofile3 << "Year" << '\t' << "Day" << '\t' << "LifeStage" << '\t' << "Disperse" << '\t' << "InHeatAttend" << '\t' << "GiveBirth" << '\t' << "Care" << '\t' << "Run" << '\t' << "Recover" << '\t' << "SeekCover" << '\t' << "Evade" << '\t' << "Rumi" << '\t' << "Feed" << '\t' << "Mate" << '\t' << "Fight" << '\n';
 
    m_gridextenty = int(SimH * InvCell_Size);
    m_gridextentx = int(SimW * InvCell_Size);

    //First thing to do is to preprocess landscape to get the quality of grid
    //cells in m_TheGrid

    PreProcessLandscape();

    int number = 0; //number of each sex created
    // Create the right number of males and females
        number = (int)(0.00001 * SimW * SimH);

    Deer_struct* ads;
    ads = new Deer_struct;
    ads->Pop = this;
    ads->L = m_TheLandscape;
    ads->mum = NULL;
    ads->sex = true;
    ads->group = -1;  //in constructor
    ads->ID = -1; //don't know yet
    ads->size = -1;  //in constructor
    TTypesOfLandscapeElement El;
    for (int i = 0; i < number; i++)
    {
        ads->x = random(m_TheLandscape->SupplySimAreaWidth());
        ads->y = random(m_TheLandscape->SupplySimAreaHeight());
        ads->age = 575 + (random(14) * 365);  //born on average 1st of June. 1-15 yrs old just for starting.
          //make sure they are dropped in either forest or field
        El = m_TheLandscape->SupplyElementType(ads->x, ads->y);
        while ((El != tole_ConiferousForest) && (El != tole_DeciduousForest) &&
            (El != tole_MixedForest) && (El != tole_YoungForest) && (El != tole_Field))
        {
            ads->x = random(m_TheLandscape->SupplySimAreaWidth());
            ads->y = random(m_TheLandscape->SupplySimAreaHeight());
            El = m_TheLandscape->SupplyElementType(ads->x, ads->y);
        }
        CreateObjects(1, NULL, NULL, ads, 1);
    }
    ads->sex = false;    //females
    for (int i = 0; i < number; i++)
    {
        ads->x = random(m_TheLandscape->SupplySimAreaWidth());
        ads->y = random(m_TheLandscape->SupplySimAreaHeight());
        ads->age = 575 + (random(14) * 365);  //born on average 1st of June. 1-15 yrs old just for starting.
        //make sure they are dropped in either forest or field
        El = m_TheLandscape->SupplyElementType(ads->x, ads->y);
        while ((El != tole_ConiferousForest) && (El != tole_DeciduousForest) &&
            (El != tole_MixedForest) && (El != tole_YoungForest) && (El != tole_Field))
        {
            ads->x = random(m_TheLandscape->SupplySimAreaWidth());
            ads->y = random(m_TheLandscape->SupplySimAreaHeight());
            El = m_TheLandscape->SupplyElementType(ads->x, ads->y);
        }
        CreateObjects(2, NULL, NULL, ads, 1);
    }
    delete ads;

    // Set up the list names (i.e. what are our animals called in this simulation
    m_ListNames[0] = "Fawn";
    m_ListNames[1] = "Male Roe Deer";
    m_ListNames[2] = "Female Roe Deer";
    m_ListNameLength = 3;

    //load state names
    StateNames[rds_Initialise] = "Initial State";
    StateNames[rds_FOnMature] = "Female OnMature";
    StateNames[rds_MOnMature] = "Male OnMature";
    StateNames[rds_FUpdateGestation] = "Female Update Gestation";
    StateNames[rds_FUpdateEnergy] = "Female Update Energy";
    StateNames[rds_MUpdateEnergy] = "Male Update Energy";
    StateNames[rds_FAUpdateEnergy] = "Fawn Update Energy";
    StateNames[rds_FEstablishRange] = "Female Establish Range";
    StateNames[rds_MEstablishRange] = "Male Establish Range";
    StateNames[rds_MEstablishTerritory] = "Male Establish Territory";
    StateNames[rds_FDisperse] = "Female Disperse";
    StateNames[rds_MDisperse] = "Male Disperse";
    StateNames[rds_FOnNewDay] = "Female OnNewDay";
    StateNames[rds_MOnNewDay] = "Male OnNewDay";
    StateNames[rds_FAOnNewDay] = "Fawn OnNewDay";
    StateNames[rds_FFormGroup] = "Female Form Group";
    StateNames[rds_FInHeat] = "Female In Heat";
    StateNames[rds_FGiveBirth] = "Female Give Birth";
    StateNames[rds_FCareForYoung] = "Female Care For Young";
    StateNames[rds_FRun] = "Female Run";
    StateNames[rds_MRun] = "Male Run";
    StateNames[rds_FARunToMother] = "Fawn Run To Mother";
    StateNames[rds_FRecover] = "Female Recover";
    StateNames[rds_MRecover] = "Male Recover";
    StateNames[rds_FARecover] = "Fawn Recover";
    StateNames[rds_FEvade] = "Female Evade";
    StateNames[rds_MEvade] = "Male Evade";
    StateNames[rds_MEvade] = "Male Evade";
    StateNames[rds_FAHide] = "Fawn Hide";
    StateNames[rds_MAttendFemale] = "Male Attend Female";
    StateNames[rds_FRuminate] = "Female Ruminate";
    StateNames[rds_MRuminate] = "Male Ruminate";
    StateNames[rds_FARuminate] = "Fawn Ruminate";
    StateNames[rds_FFeed] = "Female Feed";
    StateNames[rds_MFeed] = "Male Feed";
    StateNames[rds_FAFeed] = "Fawn Feed";
    StateNames[rds_FASuckle] = "Fawn Suckle";
    StateNames[rds_FMate] = "Female Mate";
    StateNames[rds_MMate] = "Male Mate";
    StateNames[rds_FDie] = "Female Die";
    StateNames[rds_MDie] = "Male Die";
    StateNames[rds_FADie] = "Fawn Die";
    StateNames[rds_FDeathState] = "Female Death State";
    StateNames[rds_MDeathState] = "Male Death State";
    StateNames[rds_FADeathState] = "Fawn Death State";
    StateNames[rds_FAInit] = "Fawn Initial State";

    //determine whether we should shuffle or sort or do nothing
    BeforeStepActions[0] = 0; //'0' is shuffle, '1' is sortx, '2' is sorty, '3' is nothing
    BeforeStepActions[1] = 0;
    BeforeStepActions[2] = 0;
    BeforeStepActions[3] = 0;
    BeforeStepActions[4] = 0;
}

//---------------------------------------------------------------------------
/**
PreProcessLandscape starts at top-left corner (0,0) and runs through the
    whole landscape to create a grid of qualities. For each gridcell of a
    predefined size (=const int Cell_Size, minimum is 20x20) it evaluates the
    quality, using RangeQuality() and saves it in the array m_grid[][].
    This map of qualities is constant throughout the simulation.
*/
void RoeDeer_Population_Manager::PreProcessLandscape()
{

    int a_x = Cell_HalfSize;
    int a_y = Cell_HalfSize;    //centre of first cell
    int row = SimH / Cell_Size;
    int column = SimW / Cell_Size;
    int value;

    for (int i = 0; i < column; i++)   //x-values, this many columns
    {
        for (int j = 0; j < row; j++)  //y-values, this many rows
        {
            value = (int)RangeQuality(a_x, a_y, Cell_HalfSize);
            //add this value
            m_TheGrid->AddGridValue(i, j, value);
            a_y += Cell_Size; //move right to next column
        }
        a_x += Cell_Size;  //move down to next row
        a_y = Cell_HalfSize; //reset a_x to first cell in new row

    }

    /*   //Lene: kommenter denne sektion ud naar du korer dine scenarier. Kun til at
       //teste fordelingen af kvaliteter
    //*************************************
       int rand_x=-1;
       int rand_y=-1;
       int total=0;
       for(int i=0;i<NoRandomAreas;i++)
       {
         rand_x= random(SimW);
         rand_y= random(SimH);
         total = ScanGrid(rand_x,rand_y,true);
         WriteRandHR("RandomHR.txt", total);
         total=0;
       }
    //**************************************
     */
}

//----------------------------------------------------------------------------
/**
 RoeDeer_Population_Manager::ScanGrid - adds up quality values stored in m_grid to see if a home
    range can be established here. Starts with a minimun area and extends it
    outwards until sufficient or maximum is reached. It returns the range size if the area is OK
	and zero if not. Uses functions Supply_GridCoord(), Supply_GridValue() and AddToArea().
*/
int RoeDeer_Population_Manager::ScanGrid(int p_x,int p_y,bool avq)
{
  /*
    This function adds up quality values stored in m_grid to see if a home
    range can be established here. Starts with a minimun area and extends it
    outwards until sufficient or maximum is reached. It returns the range size
    if the area is OK and zero if not.
  */
  //p_x and p_y are landscape coordinates, so find out what the grid coordinates
  //are here
  int g_x = m_TheGrid->Supply_GridCoord(p_x);
  int g_y = m_TheGrid->Supply_GridCoord(p_y);
  //what is the range measured in grid cells?
  int less = int (MinRange * InvCell_Size);
  //then find the starting coordinates in minimum area
  int start_x = g_x - less;
  int start_y = g_y - less;
  int end_x = g_x + less;
  int end_y = g_y + less;
  int quality = 0;
  int range = MinRange;
  //First sum up minimum area = 2*MinRange x 2*Minrange
  if ((start_x < less)||(start_y < less) || (end_x >= m_gridextentx - less) || (end_y >= m_gridextenty - less))  //cound be overlap
  {
	  start_x += m_gridextentx;
	  start_y += m_gridextenty;
	  end_x += m_gridextentx;
	  end_y += m_gridextenty;

    for (int i=start_x; i< end_x; i++)  //each column
    {
      for (int j=start_y; j<end_y; j++) //each row
      {
		  quality += m_TheGrid->Supply_GridValue(i%m_gridextentx, j%m_gridextenty);
      }
    }
    //minimum area is evaluated. If this is for average range quality return
    //quality here
    if(avq) return quality;
    else
    {
      while((quality <= MinRangeQuality)&&(range < MaxRange))
      {  //extend area outwards by adding layers of cells until quality is high enough
        start_x --;
        start_y --;
        end_x ++;
        end_y ++;
        range += Cell_Size;

		quality += (AddToAreaCC(start_x%m_gridextentx, start_y%m_gridextenty, int(range*InvCell_Size)));
      }
    }
  }
  else  //no overlap
  {
    for (int i=start_x; i<end_x; i++)  //each column
    {
      for (int j=start_y; j<end_y; j++) //each row
      {
         quality += m_TheGrid->Supply_GridValue(i,j);
      }
    }
    //minimum area is evaluated. If this is for average range quality return
    //quality here
    if(avq) return quality;
    else
    {
      while((quality <= MinRangeQuality)&&(range < MaxRange))
      {  //extend area outwards by adding layers of cells until quality is high enough
        start_x --;
        start_y --;
        end_x ++;
        end_y ++;
        range += Cell_Size;

        quality += (AddToArea(start_x, start_y, int(range*InvCell_Size)));
      }
    }
  }
  if(quality >= MinRangeQuality)  //can set up range here
  {
    return range;
  }
  else return 0;
}
//----------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::AddToArea - Coordinates not corrected for wrap-around.
  Function sums up the quality in a layer of grid cells of size p_range x p_range
  Calls function Supply_GridValue(). 

*/
int RoeDeer_Population_Manager::AddToArea(int g_x, int g_y, int p_range)
{
  /*Coordinates not corrected . Starting in grid coordinates g_x,g_y this
  function summs up the quality in a layer of grid cells of size p_range x p_range*/

  int quali = 0;
  int length = 2*p_range; //size of the square

  for (int i=0; i< length;i++) //top and bottom row
  {
    quali += m_TheGrid->Supply_GridValue(g_x,g_y);
    quali += m_TheGrid->Supply_GridValue (g_x,g_y+length);
    g_x ++; //move right to next cell
  }
  //Don't want to count corner cells twice so increment g_y before function call
  //and subtract 2 from length
  for (int i=0; i< length-2;i++) //left and right column
  {
    g_y ++;  //move down to next cell
    quali += m_TheGrid->Supply_GridValue(g_x-length,g_y);
    quali += m_TheGrid->Supply_GridValue (g_x,g_y);
  }
  return quali;
}
//----------------------------------------------------------------------------

/**
RoeDeer_Population_Manager::AddToAreaCC - Coordinates corrected for wrap-around landscape.
Function sums up the quality in a layer of grid cells of size p_range x p_range.
 Calls function Supply_GridValue(). 
*/
int RoeDeer_Population_Manager::AddToAreaCC(int g_x, int g_y, int p_range)
{
  /* Coordinates corrected. Starting in grid coordinates g_x,g_y this function
  summs up the quality in a layer of grid cells of size p_range x p_range*/

  int quali = 0;
  int length = 2 * p_range; //size of the square

  g_x += m_gridextentx;
  g_y += m_gridextenty;
  for (int i=0; i< length;i++) //top and bottom row
  {
	  quali += m_TheGrid->Supply_GridValue(g_x%m_gridextentx, g_y%m_gridextenty);
	  quali += m_TheGrid->Supply_GridValue(g_x%m_gridextentx, (g_y + length) % m_gridextenty);
    g_x ++; //move right to next cell
  }
  //Don't want to count corner cells twice so increment g_y before function call
  //and subtract 2 from length
  for (int i=0; i< length-2;i++) //left and right column
  {
    g_y ++;  //move down to next cell
	quali += m_TheGrid->Supply_GridValue((g_x - length) % m_gridextentx, g_y%m_gridextenty);
	quali += m_TheGrid->Supply_GridValue(g_x%m_gridextentx, g_y%m_gridextenty);
  }
  return quali;
}
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------

/**
DoFirst - for every time step, this function calls PreProcessLandscape,
Clears fightlists for all males, creates several output files, such as deer location, deer home range,
population age structure etc. Also, the average size of the male and female population is calculated 
to get dispersal thresholds and sums up the amount of floaters in the population
*/
void RoeDeer_Population_Manager::DoFirst()
{
    int today = m_TheLandscape->SupplyDayInYear();
    int year = m_TheLandscape->SupplyYearNumber();
    int month = m_TheLandscape->SupplyMonth();
    int dawn = g_date->SunRiseTime();
    int dusk = g_date->SunSetTime();
    if ((StepCounter * 10 >= dawn) && (StepCounter * 10 <= dusk)) m_isdaylight = true; else m_isdaylight = false;

   //StepCounter is incremented every timestep for as long as the simulation runs
    if (StepCounter % 144 == 0)    //new day
    {  
        cout << "Year is " << year << " Day is " << today << '\n';
        if (today == 0)   //once a year
        {
            if (StepCounter != 0)  //once a year, but not the first year
            {
                PreProcessLandscape(); //update the grid with qualities!!!
                //tell all males that fightlists have been cleared
                for (unsigned j = 0; j < GetLiveArraySize(int(trd_Roe_Male)); j++)
                {
                    static_cast<Roe_Male*>(SupplyAnimalPtr(trd_Roe_Male, j))->ClearFightlist();
                }
                //write deadfile and clear list of dead animals
                //WriteDeadFile("Mortality.txt",year);
                //write age structure file
                WriteSizeFile("RoeDeerSizeDistribution.txt", year);
                WriteAgeStructFile("RoeDeerAgeStructure.txt", year);
                m_MaturedThisYear = 0;
                m_BornThisYear = 0;
                m_DiedAdultsThisYear = 0;
                m_DiedYoungThisYear = 0;
                m_DeadListFemales.clear();
                m_DeadListMales.clear();
            }
        }
#ifdef __DEBUG_CJT1
        if (today == 364) {
            g_RoeMortRec.outputroemort(m_TheLandscape->SupplyYearNumber(), m_TheLandscape->SupplyDayInYear());
        }
#endif
        if ((today % 30 == 0) && (today != 0))  //every 30 days
        {
            //calculate average size of the male and female population to get dispersal thresholds
            double malesize = 0;
            double femalesize = 0;
            int male_float = 0;
            int female_float = 0;
            for (unsigned i = trd_Roe_Male; i < trd_Roe_Foobar; i++)  //don't include fawns
            {
                for (unsigned j = 0; j < GetLiveArraySize(i); j++)
                {
                    Roe_Base* deer;
                    deer = static_cast<Roe_Base*> (SupplyAnimalPtr(i,j));
                    if (i == trd_Roe_Male)
                    {
                        malesize += deer->m_Size; //males
                        //Sum up how many floaters in population
                        if (deer->m_HaveRange == false)male_float++;
                    }
                    else
                    {
                        femalesize += deer->m_Size; //females
                        //Sum up how many floaters in population
                        if (deer->m_HaveRange == false) female_float++;
                    }
                }
            }
            //write this number to file for debug
            //WriteFloatFile("Float.txt",male_float,female_float);

            //calculate dispersal threshold and min weights
            if (GetLiveArraySize(int(trd_Roe_Male)) > 0)   //to avoid division with zero if no males
            {
                m_AverageMaleSize = int(malesize / GetLiveArraySize(int(trd_Roe_Male)));
                //m_MinWeight_Males = int(m_AverageMaleSize * 0.75);
                //m_CriticalWeight_Males = int(m_AverageMaleSize * 0.80);
            }
            if (GetLiveArraySize(int(trd_Roe_Female)) > 0)
            {
                m_FemaleDispThreshold = int(femalesize / GetLiveArraySize(int(trd_Roe_Female)));
                //m_MinWeight_Females = int(m_FemaleDispThreshold * 0.75);
                //m_CriticalWeight_Females = int(m_FemaleDispThreshold * 0.80);
            }
        }
    }

    //from year 10 (i.e. 2000) onwards, every 40 hrs, store the positions of all animals

#ifdef __ROE_USINGFIXES
    Roe_Adult_Base* deer;
    if ((year >= 2000) && (StepCounter % 144 == 0))
    {
        for (unsigned i = trd_Roe_Male; i < trd_Roe_Foobar; i++)  //adults only
        {
            for (unsigned j = 0; j < TheArray[i].size(); j++)  //for every animal of each type
            {
                deer = dynamic_cast<Roe_Adult_Base*> (TheArray[i][j]);
                deer->AddFix();
                //every month, calculate MCP home range and write it to file
                if (g_date->GetDayInMonth() == 1)
                {
                    deer->CalculateArea(); //first and last position to be sorted
                    deer->ClearFixes();
                    WriteToFixFile(deer);
                    WriteToHRFile("HRFile.txt",month, year);
                }
            }
        }
    }
#endif
    StepCounter++;
}
//-----------------------------------------------------------------------------
void RoeDeer_Population_Manager::UpdateRanges()
{
  Roe_Base* deer;

  for (unsigned listindex=1; listindex<trd_Roe_Foobar;listindex++)
  {
    for (unsigned j=0; j< GetLiveArraySize(listindex); j++)
    {
      deer =dynamic_cast< Roe_Base * > (SupplyAnimalPtr(listindex, j));
      RoeDeerInfo info = deer->SupplyInfo();
      if(deer->m_HaveRange)
      {
         deer->m_SearchRange = ScanGrid(info.m_x,info.m_y,false);
      }
    }
  }
}   
//-----------------------------------------------------------------------------
//void RoeDeer_Population_Manager::LOG(int day, int year) //debug log
//{ //writes to individual specific files
//  Roe_Male* M_ptr;
//  Roe_Female* F_ptr;
//  Roe_Fawn* FA_ptr;
//#ifndef __UNIX__
//   std::string filename("");//**ljk May 18 2012**
//  char* f;
//#else
//  char f[20];
//#endif
//
//  for (unsigned listindex=0; listindex<TheArray.size();listindex++)
//  {
//    FILE *PFile;
//    for (unsigned j=0; j<TheArray[listindex].size(); j++)
//    {
//      switch(listindex)
//      {
//        case 0: //fawn
//          FA_ptr=dynamic_cast< Roe_Fawn * > (TheArray[0][j]);
//#ifdef __UNIX__
//          sprintf( f, "Fawn%d.txt", FA_ptr->m_ID);
//#else
//		   filename += "Fawn" + boost::lexical_cast<std::string>(FA_ptr->m_ID)+".txt";//**ljk May 18 2012**
//		   f=(char *)filename.c_str();
//#endif
//	  if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::LOG::Unable to open file for appending data!\n",
//                       0);
//                exit( 1 );
//	  }
//
//          fprintf(PFile," %i,%i, ",day,year);  //day in year
//          fprintf(PFile," %i,%i,%i,%i\n", FA_ptr->m_ID,
//                               FA_ptr->m_Size,FA_ptr->m_Age,FA_ptr->m_MyGroup);
//          fclose(PFile);
//          break;
//        case 1:  //male
//         M_ptr=dynamic_cast< Roe_Male * > (TheArray[1][j]);
//#ifdef __UNIX__
//         sprintf( f, "Male%d.txt", FA_ptr->m_ID);
//#else
//         filename += "Male" + boost::lexical_cast<std::string>(M_ptr->m_ID)+".txt";//**ljk May 18 2012**
//		 f=(char *)filename.c_str();
//#endif
//	 if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::LOG::Unable to open file for appending data!\n",
//                       0);	 }
//         fprintf(PFile," %i,%i, ",day,year);  //day in year
//         fprintf(PFile," %i,%i,%i \n",M_ptr->m_ID,M_ptr->m_Size,M_ptr->m_Age);
//         fclose(PFile);
//         break;
//        case 2: //female
//           F_ptr=dynamic_cast< Roe_Female * > (TheArray[2][j]);
//#ifdef __UNIX__
//           sprintf( f, "Female%d.txt", FA_ptr->m_ID);
//#else
//         
//           filename += "Female" + boost::lexical_cast<std::string>(F_ptr->m_ID)+".txt";//**ljk May 18 2012**
//		   f=(char *)filename.c_str();
//#endif
//	   if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::LOG::Unable to open file for appending data!\n",
//                       0);	   }
//           fprintf(PFile," %i,%i, ",day,year);  //day in year
//           fprintf(PFile," %i,%i,%i,%i,%i,%i \n",F_ptr->m_ID,
//             F_ptr->m_Size,F_ptr->m_Age,F_ptr->m_MyGroup,F_ptr->SupplyIsPregnant(),
//             F_ptr->SupplyYoung());
//           fclose(PFile);
//           break;
//      }
//    }
//  }
//}
//----------------------------------------------------------------------------

void RoeDeer_Population_Manager::WriteAgeStructFile(string name, int year)
{
    //every year sum up the number of animals in each age group and write to file
    ofstream PFile(name.c_str(), ios::app);

    if (!(PFile.is_open())) {
        //throw an error
        m_TheLandscape->Warn("RoeDeerPopulationManager::WriteAgeStructFile::Unable to open file!\n", 0);
    }
    Roe_Base* deer;
    int age_array[21] = { 0 };
    for (unsigned listindex = 0; listindex < SupplyListIndexSize(); listindex++)
    {
        int sz = GetLiveArraySize(listindex);
        for (int j = 0; j < sz; j++)
        {
            //convert to Roe_Base
            deer = static_cast<Roe_Base*> (SupplyAnimalPtr(listindex, j));
            int age = deer->m_Age / 365;
            if (age > 20) age = 20;
            age_array[age]++; //add one to this age
        }
    }
    PFile << year;
    for (unsigned j = 0; j < 21; j++)
    {
        PFile << '\t' << age_array[j];
    }
    PFile << '\t' << m_BornThisYear << '\t' << m_MaturedThisYear << '\t' << m_DiedYoungThisYear << '\t' << m_DiedAdultsThisYear;
    PFile << '\n'; //new line
    PFile.close();
}
////------------------------------------------------------------------------------

void RoeDeer_Population_Manager::WriteSizeFile(string name, int year)
{
    //every year sum up the number of animals in each age group and write to file
    ofstream PFile(name.c_str(), ios::app);

    if (!(PFile.is_open())) {
        //throw an error
        m_TheLandscape->Warn("RoeDeerPopulationManager::WriteSizeFile::Unable to open file!\n", 0);
    }
    Roe_Base* deer;
    int size_array[2][31] = { 0 };
    for (unsigned listindex = trd_Roe_Male; listindex <= trd_Roe_Female; listindex++)
    {
        int sz = GetLiveArraySize(listindex);
        for (int j = 0; j < sz; j++)
        {
            //convert to Roe_Base
            deer = static_cast<Roe_Base*> (SupplyAnimalPtr(listindex, j));
            int size = int(floor(deer->m_Size / 1000.0 + 0.5)-10.0);
            if (size > 29) size = 30; // To stop possible overruns - this size means an error
            if (size < 0) size = 30; // To stop possible overruns - this size means an error
            size_array[listindex-1][size]++; //add one to this size class
        }
    }
    PFile << year;
    for (int i=0; i<2; i++) 
        for (unsigned j = 0; j < 31; j++)
    {
        PFile << '\t' << size_array[i][j];
    }
    PFile << '\n'; //new line
    PFile.close();
}
////------------------------------------------------------------------------------

void RoeDeer_Population_Manager::WriteDeadFile(string name, int p_year)
{
    const char* f = name.c_str();

    int year = m_TheLandscape->SupplyYearNumber();
    ofstream PFile(name.c_str(), ios::app);
    if (!(PFile.is_open())) {
        //throw an error
        m_TheLandscape->Warn("RoeDeerPopulationManager::WriteSizeFile::Unable to open file!\n", 0);
    }


    int sz1 = int(m_DeadListMales.size());
    int sz2 = int(m_DeadListFemales.size());
    int size=sz2;
    if (sz1 > sz2) size = sz1;
    for (int i = 0; i < size; i++)
    {
        int dead1, dead2;
        if (i >= sz1) dead1 = -1; else dead1 = m_DeadListMales[i];
        if (i >= sz2) dead2 = -1; else dead2 = m_DeadListFemales[i];
        PFile << year << '\t' << dead1 << '\t' << dead2 << '\n';
    }
    PFile.close();
    m_DeadListFemales.clear();
    m_DeadListMales.clear();
}
//-----------------------------------------------------------------------------

void RoeDeer_Population_Manager::WriteToHRFile(string name, int month, int year)
{
    FILE* PFile;


    const char* f = name.c_str();


    if (!(PFile = fopen(f, "a+"))) {
        //throw an error
        m_TheLandscape->Warn("RoeDeerPopulationManager::WriteToHRFile::Unable to open file!\n", 0);
    }
    Roe_Base* deer;

    for (unsigned listindex = trd_Roe_Female; listindex <= trd_Roe_Male; listindex++)
    {
        fprintf(PFile, "%s :\n", m_ListNames[listindex]);
        for (unsigned j = 0; j < SupplyListSize(listindex); j++)
        {
            //convert to Roe_Base
            deer = dynamic_cast<Roe_Base*> (SupplyAnimalPtr(listindex, j));
            fprintf(PFile, "%i %i %i %f \n", year, month, deer->m_ID, deer->GetHomeRange());
        }
    }
    fclose(PFile);
}
//---------------------------------------------------------------------------

//void RoeDeer_Population_Manager::WriteFloatFile(string name, int male_f,int female_f)
//{
// FILE *PFile;
//const char *f = name.c_str();
//
// if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//     m_TheLandscape->Warn("RoeDeerPopulationManager::WriteFloatFile::Unable to open file!\n",
//                       0);
// }
// int day = m_TheLandscape->SupplyDayInYear();
// int year = m_TheLandscape->SupplyYear();
//
// fprintf(PFile,"%i %i %i %i \n",year,day,male_f,female_f);
// fclose(PFile);
//}
////---------------------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteReproFile(string name, int id, int young)
//{
// FILE *PFile;
//const char *f = name.c_str();
// int year = m_TheLandscape->SupplyYear();
// if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//     m_TheLandscape->Warn("RoeDeerPopulationManager::WriteReproFile::Unable to open file!\n",
//                       0);
// }
// fprintf(PFile,"%i %i %i  \n",year, id, young);
// fclose(PFile);
//}
////---------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteRandHR(string name, int total)
//{
// FILE *PFile;
//const char *f = name.c_str();
//
// if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//     m_TheLandscape->Warn("RoeDeerPopulationManager::WriteRandHR::Unable to open file!\n",
//                       0);
// }
//
// fprintf(PFile,"%i \n",total);
// fclose(PFile);
//}
//---------------------------------------------------------------------------

void RoeDeer_Population_Manager::WriteIDFile(string name, int p_ID, bool p_sex)
{
 FILE *PFile;
const char *f = name.c_str();
 int Sex;
 if (!(PFile = fopen(f,"a+"))) {
                //throw an error
     m_TheLandscape->Warn("RoeDeerPopulationManager::WriteIDFile::Unable to open file!\n", 0);
 }
 if(p_sex) Sex=1;
 else Sex=0;
 fprintf(PFile,"%i %i \n",p_ID,Sex);
 fclose(PFile);
}
//---------------------------------------------------------------------------

void RoeDeer_Population_Manager::WriteToFixFile(Roe_Adult_Base* deer)
{
    FILE* PFile;
    int year = m_TheLandscape->SupplyYear();
    int day = m_TheLandscape->SupplyDayInYear();
#ifndef __UNIX__
    std::string filename = "Fix" + boost::lexical_cast<std::string>(year) + ".txt";
    char* f = (char*)filename.c_str();

#else
    char* f = name;
#endif
    if (!(PFile = fopen(f, "a+"))) {
        //throw an error
        m_TheLandscape->Warn("RoeDeerPopulationManager::WriteToFixFile::Unable to open file!\n",
            0);
    }
    int sz = deer->GetFixListSize();
    for (int k = 0; k < sz; k++)   //for each location
    {
        APoint pt = deer->GetFixNo(k);
        fprintf(PFile, "%i %i %i %i %d %d \n", year, day, deer->m_ID, k, pt.m_x, pt.m_y);

    }
    fclose(PFile);
}
//---------------------------------------------------------------------------

void RoeDeer_Population_Manager::WriteToEnergyBudgetFile(int a_day, int a_year)
{
    /**
    * Writes a daily mean energy budget for each lifestage
    */
    //every year sum up the number of animals in each age group and write to file
    ofstream PFile("RoeEnergyBudgets.txt", ios::app);

    if (!(PFile.is_open())) {
        //throw an error
        m_TheLandscape->Warn("RoeDeerPopulationManager::WriteSizeFile: Unable to open file! RoeEnergyBudgets.txt\n", 0);
    }
    Roe_Base* deer;
    double energyuse[12] = { 0,0,0,0,0,0,0,0,0,0,0,0 };
    for (unsigned listindex = trd_Roe_Fawn; listindex <= trd_Roe_Female; listindex++)
    {
        int sz = GetLiveArraySize(listindex);
        for (int j = 0; j < sz; j++)
        {
            //convert to Roe_Base
            deer = static_cast<Roe_Base*> (SupplyAnimalPtr(listindex, j));
            int* use;
            use = deer->GetEnergyUse();
            for (int i = 0; i < 12; i++)
            {
                energyuse[i] += *(use +i);
            }
        }
        if (sz < 1) sz = 1; // Stop division by zero
        for (int i = 0; i < 12; i++) energyuse[i] /= (double)sz;
        PFile << a_year << '\t' << a_day << '\t' << listindex;
        for (unsigned j = 0; j < 12; j++)
        {
            PFile << '\t' << energyuse[j];
            energyuse[j] = 0;
        }
        PFile << '\n'; //new line
    }
     PFile.close();
}
//---------------------------------------------------------------------------

/**
RoeDeer_Population_Manager::SupplyTMales - searches a specific range, given coordinates and search 
   range and returns list of male objects that are territory holders within the range.
  
*/
ListOfMales* RoeDeer_Population_Manager::SupplyTMales(int p_x,int p_y,int SearchRange)
   {
      ListOfMales* LoTM;
      // Create a new list
      LoTM=new ListOfMales;
      Roe_Male * male_ptr;
      // For each male on the global list
      for (unsigned j=0; j< GetLiveArraySize(int(trd_Roe_Male)); j++)  // '1' == males
      {
        // convert from TAnimal* to Roe_Male*
        male_ptr=dynamic_cast< Roe_Male * > (SupplyAnimalPtr(trd_Roe_Male, j));
        {
          // Check if it is in the square and has a territory
          int x=male_ptr->Supply_m_Location_x();
          int y=male_ptr->Supply_m_Location_y();
          if (male_ptr->GetStatus()== rdstatus_territorial)
          {
			  if ((InSquare(x, y, p_x, p_y, SearchRange)) && (male_ptr->GetCurrentStateNo() != -1))
            {
              // It is so add it to the list of males
              LoTM->push_back(male_ptr);
            }
          }
        }
      }
      // return the list
      return LoTM;
   }
//---------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::SupplyMaleRC - searches a specific range, given coordinates and search 
   range and returns list of male objects within the range.
   */
//should return array of all Males/TMales/Females in area
   ListOfMales* RoeDeer_Population_Manager::SupplyMales (int p_x,int p_y, int SearchRange)
   {
      ListOfMales* LoM;
      // Create a new list
      LoM=new ListOfMales;
      Roe_Male * male_ptr;
      // For each male on the global list
      for (unsigned j=0; j< GetLiveArraySize(int(trd_Roe_Male)); j++)  // '1' == males
      {
        // convert from TAnimal* to Roe_Male*
        male_ptr=dynamic_cast< Roe_Male * > (SupplyAnimalPtr(trd_Roe_Male, j));
        // Check if it is in the square and has a territory
        int x=male_ptr->Supply_m_Location_x();
        int y=male_ptr->Supply_m_Location_y();
		if ((InSquare(x, y, p_x, p_y, SearchRange)) && (male_ptr->GetCurrentStateNo() != -1))
        {
          // It is so add it to the list of males
          LoM->push_back(male_ptr);
        }
      }
      // return the list
      return LoM;
      // NB Calling function has the responsibility of deleting the list after use!
   }
//------------------------------------------------------------------------------
   /**
   RoeDeer_Population_Manager::SupplyMaleRC - searches a specific range, given coordinates and search 
   range and returns list of male objects with established ranges within the range.

   */
 ListOfMales* RoeDeer_Population_Manager::
                                SupplyMaleRC (int p_x,int p_y, int SearchRange)
   {
      ListOfMales* LoM;
      // Create a new list
      LoM=new ListOfMales;
      Roe_Male * male_ptr;
      // For each male on the global list
      for (unsigned j=0; j< GetLiveArraySize(int(trd_Roe_Male)); j++)  // '1' == males
      {
        // convert from TAnimal* to Roe_Male*
        male_ptr=dynamic_cast< Roe_Male * > (SupplyAnimalPtr(trd_Roe_Male, j));
        // Check if its range centre is in the square
        RoeDeerInfo range=male_ptr->SupplyInfo();
        int x=range.m_Range.m_x;  
        int y=range.m_Range.m_y;
		if ((InSquare(x, y, p_x, p_y, SearchRange)) && (male_ptr->GetCurrentStateNo() != -1))
        {
          // It is so add it to the list of males
          LoM->push_back(male_ptr);
        }
      }
      // return the list
      return LoM;
      // NB Calling function has the responsibility of deleting the list after use!
   }
//-----------------------------------------------------------------------------
 /**
   RoeDeer_Population_Manager::SupplyFemales - searches a specific range, given coordinates and search
   range and returns list of female objects within the range.
   */
 ListOfFemales* RoeDeer_Population_Manager::SupplyFemales(int p_x, int p_y, int SearchRange)
 {
     ListOfFemales* LoF;
     LoF = new ListOfFemales;
     Roe_Female* female_ptr;
     // For each female on the global list
     for (unsigned j = 0; j < GetLiveArraySize(int(trd_Roe_Female)); j++)
     {
         // convert from TAnimal* to Roe_Female*
         female_ptr = static_cast<Roe_Female*> (SupplyAnimalPtr(trd_Roe_Female, j));
         // Check if it is in the square
         int x = female_ptr->Supply_m_Location_x();
         int y = female_ptr->Supply_m_Location_y();
         if ((InSquare(x, y, p_x, p_y, SearchRange)) && (female_ptr->GetCurrentStateNo() != -1))
         {
             // It is so add it to the list of females
             LoF->push_back(female_ptr);
         }
     }
     return LoF;
 }

 //-----------------------------------------------------------------------------
 /**
   RoeDeer_Population_Manager::SupplyFemaleRC - searches a specific range, given coordinates and search
   range and returns list of female objects that have established ranges within the range.
   */
 ListOfFemales* RoeDeer_Population_Manager::SupplyFemaleRC(APoint a_Loc, int SearchRange)
 {   //returns the females already established with a range within the square in
     //question. Uses m_OldRange
     ListOfFemales* LoF;
     LoF = new ListOfFemales;
     Roe_Female* female_ptr;
     // For each female on the global list
     for (unsigned j = 0; j < GetLiveArraySize(int(trd_Roe_Female)); j++)  // '2' == females
     {
         // convert from TAnimal* to Roe_Female*
         female_ptr = static_cast<Roe_Female*> (SupplyAnimalPtr(trd_Roe_Female, j));
         // Check if its range centre is in the square
         RoeDeerInfo range = female_ptr->SupplyInfo();
         int x = range.m_OldRange.m_x;
         int y = range.m_OldRange.m_y;
         if ((InSquare(x, y, a_Loc.m_x, a_Loc.m_y, SearchRange)) && (female_ptr->GetCurrentStateNo() != -1))
         {
             // It is so add it to the list of females
             LoF->push_back(female_ptr);
         }
     }
     return LoF;
 }
 //----------------------------------------------------------------------------
 
bool RoeDeer_Population_Manager::SupplyIsFemale(int p_x, int p_y, int SearchRange)
 {
     /**
       RoeDeer_Population_Manager::SupplyNearestFemale - Searches first to find a female within SearchRange, otherwise finds the location of the nearest female.
       Returns either -1 (female in range) or an 4 point direction (of 8) to the nearest female (N = 0, NE = 1, SE = 3 etc.).
    */
     Roe_Female* female_ptr;
     // For each female on the global list
     for (unsigned j = 0; j < GetLiveArraySize(int(trd_Roe_Female)); j++)
     {
         // convert from TAnimal* to Roe_Female*
         female_ptr = static_cast<Roe_Female*> (SupplyAnimalPtr(trd_Roe_Female, j));
         // Check if its range centre is in the square
         RoeDeerInfo range = female_ptr->SupplyInfo();
         int x = range.m_Range.m_x;
         int y = range.m_Range.m_y;
         if ((InSquare(x, y, p_x, p_y, SearchRange)) && (female_ptr->GetCurrentStateNo() != -1)) return true; // All good female near
      }
     return false; // there are no females 
 }
//----------------------------------------------------------------------------
void RoeDeer_Population_Manager::CreateObjects(int ob_type, TAnimal *pvo,void* null , Deer_struct * data, int number)
{
  Roe_Fawn *New_Fawn;
  Roe_Male *New_Male;
  Roe_Female *New_Female;
  Roe_Female* mum;
  for (int i=0; i<number; i++)
  {
    Turnover++;

    if (ob_type == trd_Roe_Fawn)
    {
        m_BornThisYear++;
        if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(int(ob_type))) {
            // We need to reuse an object
            New_Fawn = dynamic_cast<Roe_Fawn*>(SupplyAnimalPtr(int(ob_type), GetLiveArraySize(int(ob_type))));
            New_Fawn->ReInit(data);
        }
        else {
            New_Fawn = new Roe_Fawn(data);
            PushIndividual(ob_type, New_Fawn);
        }
        mum = dynamic_cast <Roe_Female*> (pvo);
        //Mum handles the adding to lists
        New_Fawn->Mum = mum;
        New_Fawn->m_ID = Turnover;  //fawns always need a new ID
        New_Fawn->Mum->AddYoungToList(New_Fawn);
        //WriteIDFile("Sex.txt", New_Fawn->m_ID, New_Fawn->m_Sex);
    }
    if (ob_type == trd_Roe_Male)
    {
       // Male
        m_MaturedThisYear++;
        if (unsigned(SupplyListSize(int(ob_type))) > GetLiveArraySize(int(ob_type))) {
            // We need to reuse an object
            New_Male = dynamic_cast<Roe_Male*>(SupplyAnimalPtr(int(ob_type), GetLiveArraySize(int(ob_type))));
            New_Male->ReInit(data);
        }
        else {
            New_Male = new Roe_Male(data);
            PushIndividual(ob_type, New_Male);
        }
       if (data->ID!=-1) //maturing fawn
       {
          Turnover--;
          New_Male->m_ID=data->ID;
       }
       else New_Male->m_ID = Turnover; //new individual
    }
    if (ob_type == trd_Roe_Female)
    {
       // Female
        m_MaturedThisYear++;
        if (unsigned(SupplyListSize(int(ob_type))) > GetLiveArraySize(int(ob_type))) {
            // We need to reuse an object
            New_Female = dynamic_cast<Roe_Female*>(SupplyAnimalPtr(int(ob_type), GetLiveArraySize(int(ob_type))));
            New_Female->ReInit(data);
        }
        else
        {
            New_Female = new Roe_Female(data);
            PushIndividual(ob_type, New_Female);
        }
       if (data->ID!=-1) //maturing fawn
       {
          Turnover--;
          New_Female->m_ID=data->ID;
          //debug
       }
       else New_Female->m_ID = Turnover;
    }
    IncLiveArraySize(int(ob_type));
  }
}
//---------------------------------------------------------------------------

/**
RoeDeer_Population_Manager::CreateNewGroup - creates a new group and adds the calling 
deer to that group. 

*/
int RoeDeer_Population_Manager::CreateNewGroup(Roe_Base* p_deer)
{
    // Creates a new group and adds the calling deer to that group
    // The group reference number is generated incrementally
    RoeDeerGroup grp(m_lastgrouprefnum);
    m_GroupList.push_back(grp);
    // add the first group member
    AddToGroup(p_deer, m_lastgrouprefnum++);
    return m_lastgrouprefnum-1;
}
//---------------------------------------------------------------------------

APoint RoeDeer_Population_Manager::GetGroupCentre( int a_TheGroup)
{
    return m_GroupList[GetGroupIndex(a_TheGroup)].GetGroupCentre();
}
//---------------------------------------------------------------------------

void RoeDeer_Population_Manager::UpdateGroup(int a_TheGroup)
{
    m_GroupList[GetGroupIndex(a_TheGroup)].Update();
}
//---------------------------------------------------------------------------

void RoeDeer_Population_Manager::AddToGroup(Roe_Base* p_deer, int a_TheGroup)
{
    /**
    * Adds a new member to the group.
    * First needs to locate the group that is referenced by the group number a_TheGroup, then add the member
    */
    m_GroupList[GetGroupIndex(a_TheGroup)].AddDeer(p_deer);
}
//---------------------------------------------------------------------------
void RoeDeer_Population_Manager::RemoveFromGroup(Roe_Base* p_deer, int a_TheGroup)
{
    /**
    * RoeDeer_Population_Manager::RemoveFromGroup - removes deer object from a group by erasing deer from
    * list - if this causes group to have zero members, the entire group list will be deleted.
    * Removes a new member to the group.
    * First needs to locate the group that is referenced by the group number a_TheGroup, then removes the member
    * If the deer referenced is not in the group an error is raised via g_msg
    */
    if (!m_GroupList[GetGroupIndex(a_TheGroup)].RemoveDeer(p_deer))
    {
        // Empty list so erase it from the group list
        vector<RoeDeerGroup>::iterator nth = m_GroupList.begin();
        m_GroupList.erase(nth + GetGroupIndex(a_TheGroup));
    }
}
//----------------------------------------------------------------------------

void RoeDeer_Population_Manager::DissolveGroup(int a_TheGroup)
{
    // Empty list so erase it from the group list
    int index = GetGroupIndex(a_TheGroup);
    m_GroupList[index].EndGroup();
    vector<RoeDeerGroup>::iterator nth = m_GroupList.begin();
    m_GroupList.erase(nth + index);
}
//----------------------------------------------------------------------------

void RoeDeer_Population_Manager::GroupUpdates()
{
    // Force an updating of all current female groups to sync all members
    for (int nth = 0; nth < m_GroupList.size(); ++nth)
    {
        m_GroupList[nth].Update();
    }
}
//----------------------------------------------------------------------------

bool RoeDeer_Population_Manager::InSquare(int p_x, int p_y, int p_sqx, int p_sqy, int p_range)
{
    // are p_x & p_y in the square p_sqx,p_sqy,p_range?
    int x_extent = p_sqx + p_range;
    int y_extent = p_sqy + p_range;
    if (x_extent >= SimW)
    {
        if (y_extent >= SimH)  // overlaps TR corner of sim area
        {
            // Top right square (limited by SimAreaHeight & SimAreaWidth
            if ((p_x >= p_sqx) && (p_y >= p_sqy)) return true;
            // Top Left Square (limited by 0,SimAreaHeight)
            if ((p_y >= p_sqy) && (p_y < y_extent - SimH)) return true;
            // Bottom Left square (limited by 0,0)
            if ((p_x >= x_extent - SimW) && (p_y < y_extent - SimH)) return true;
            // Bottom Right square (limited by SimAreaWidth,0)
            if ((p_x >= p_sqx) && (p_y < y_extent - SimH)) return true;

        }
        else // Overlaps the western edge of the sim area
        {
            if ((p_x >= p_sqx) && (p_y >= p_sqy) && (p_y < y_extent)) return true;
            if ((p_x < x_extent - SimW) && (p_y >= p_sqy) && (p_y < y_extent)) return true;
        }
    }
    else
    {
        if (y_extent >= SimH) // overlaps top of simulation area
        {
            if ((p_x >= p_sqx) && (p_x < x_extent) && (p_y >= p_sqy)) return true;
            if ((p_x >= p_sqx) && (p_x < x_extent) && (p_y < y_extent - SimH)) return true;
        }
        else // territory does not overlap end of simulation area
        {
            if ((p_x >= p_sqx) && (p_x < x_extent) && (p_y >= p_sqy) && (p_y < y_extent)) return true;
        }
    }
    return false; // not in square
}
//------------------------------------------------------------------------------

/**
An output designed to be used as an input to calculating Ripley's k in R.\n
*/
void RoeDeer_Population_Manager::TheRipleysOutputProbe( FILE* a_prb ) { //ljk added 22/8-2012
  Roe_Female* FS;
  unsigned totalF=(unsigned)GetLiveArraySize(int(trd_Roe_Female));
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
	  FS=dynamic_cast<Roe_Female*>(SupplyAnimalPtr(2, j));
	  x=FS->Supply_m_Location_x();
	  y=FS->Supply_m_Location_y();
	  fprintf(a_prb,"%d\t%d\n", x,y);
  }
  fflush(a_prb);
}
//-------------
void RoeDeer_Population_Manager::TheAOROutputProbe() {
	m_AOR_Probe->DoProbe(2);
}


void RoeDeerGroup::AddDeer(Roe_Base* a_deer)
{
    m_DeerList.push_back(a_deer);
    if (m_oldest == NULL) m_oldest = a_deer;
    else
    {
        // Is the new deer older? 
        // If so update oldest
        if (a_deer->GetAge() > m_oldest->GetAge())
        {
            m_oldest = a_deer;
        }
    }
}

void RoeDeerGroup::ResetOldest()
{
    int age = 0;
    for (ListOfDeer::iterator nth = m_DeerList.begin(); nth != m_DeerList.end(); ++nth)
    {
        int age2 = (*nth)->GetAge();
        if (age2 > age) {
            m_oldest = (*nth);
            age = age2;
        }
    }
}

bool RoeDeerGroup::RemoveDeer(Roe_Base* a_deer)
{
    bool found = false;
    for (ListOfDeer::iterator nth = m_DeerList.begin(); nth != m_DeerList.end(); ++nth)
    {
        if ((*nth) == a_deer)
        {
            m_DeerList.erase(nth);
            found = true;
            break;
        }
        if (!found) {
            // Not in group - error
            g_msg->Warn("RoeDeerGroup::RemoveDeer Deer not found in group number ", m_reference);
            exit(0);
        }
    }
    if (m_DeerList.size() < 1) return false; // Signal group dissolution
    if (a_deer == m_oldest) ResetOldest();
    return true; // All OK
}

inline APoint RoeDeerGroup::GetGroupCentre() { return m_oldest->SupplyPoint(); }

void RoeDeerGroup::Update()
{
    APoint pt = m_oldest->SupplyPoint();
    for (ListOfDeer::iterator nth = m_DeerList.begin(); nth != m_DeerList.end(); ++nth)
    {
        (*nth)->On_UpdateGroup(pt);
    }
}

void RoeDeerGroup::EndGroup()
{
    for (ListOfDeer::iterator nth = m_DeerList.begin(); nth != m_DeerList.end(); ++nth)
    {
        (*nth)->On_EndGroup();
    }
}