//
// Roe_constants.cpp
//
#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "Roe_constants.h"
#include "Roe_all.h"



extern const int Vector_x[8];
extern const int Vector_y[8];
//grid constants
const int Cell_Size = 20;  //cells are 20x20 m
const double InvCell_Size = 0.05;
const int Cell_HalfSize = 10;
const int MinRange = 200;
const int MaxRange = 400;
const int MaxDistToMum = 200;
const int MinDispDistance = 300;
const double ThreatBaseProbability = 1.0 / (2.0*144.0); // Once per 2 days for human dominated - fitting function

//cannot set up range if present density is more than this
const unsigned MaleDensityThreshold = 1;

const unsigned FemaleDensityThreshold = 1;

//average range quality based on this many areas
const int NoRandomAreas = 1000;

const double DivideRandomAreas = 0.001; //= 1 / NoRandomAreas
const int MinRangeQuality = 3200;  //all-year minimum 3900
const int AverageRecoveryTime=8;  //timesteps, data from CRO
const int MaxRecoveryTime=12;      //data from CRO
const int CoverThreshold1 = 1;   //> 1 = medium (= open-summer/forest-winter)
const int CoverThreshold2 = 2;   //> 2 = good (= forest-summer)

//g dw/minute Drozdz 1979. Values here are multiplied by 10 to get time step values
//Intake -10%
/*const double IntakeRate[12] = {
  7.9,10.3,10.8,15.3,22.5,25.2,25.2,18.0,17.1,15.3,13.5,9.9
};  */

//Intake +10%
/*const double IntakeRate[12] = {
  9.6,12.7,13.2,18.7,27.5,30.8,30.8,22.0,20.9,18.7,16.5,12.1
};
*/
//Intake Drozdz 1979
const double IntakeRate[12] = {
  8.7,8.9,11.7,13.2,31.8,33.0,24.7,23.8,22.4,18.8,11.9,9.5
}; 


//will not feed if gain for 10 minutes feeding is less than this
const int MinNutri =  8000;

//TRAFFIC, width in meter and traffic load in no. of vehicles per hour.
const int MaxRoadWidth = 12;   //max road width in area
const int MaxTraffic = 513; //In this area
const double RoeRoadMortalityScaler = 0.0625; // Used to scale road mortality chance
// ENERGETICS
// Resting met. rate in cal/timestep/kg.
// RMR[0]=non-pregnant, RMR[1]=pre-partum 240-300,
// RMR[2] post-pertum 1-15, RMR[3]=postp 16-30,
// RMR[4]=postp 31-60, RMR[5]=postp 61-90
// RMR[6]=postp 91-120
const double FemaleRMR[7]={358.35,414.93,452.65,437.56,414.93,396.07,377.21};
const double MaleRMR = 358.35; //same as non-pregnant female
const double FawnRMR = 358.35;  //same as non-pregnant female  // **CJT** Previously 526.2 where did this come from?

// For fawns lower critical bw follows age, not month in year.
// [0]:0-9 days, [1]:10-60 days,[2]: 2-4 months,
// [3]:5-10 months, [4]: 11-12 months
const double MinWeightFawn[5]={1.500,3.000,6.000,7.000,8.000};
// Critcal male weight determined from bag data as 5% smallest deer after cleaning obvious errors. MinWeight is 2 kg higher
const int MinWeightMales = 16;
const int CriticalWeightMales = 14;
const double prop_femalefawns = 0.50;  //sexratio at birth, of 100 fawns

//No of feeding bouts per 24 hrs
//in updateE
const int Male_FeedBouts[12]={
  9,8,8,13,14,13,17,14,11,13,9,9
};

const int Female_FeedBouts[12]={
  8,9,9,12,14,13,12,13,11,11,8,8
};

//COST; additional costs for different types of activity. Corresponds to the
//m_MinInState-array. In cal/timestep/kg.
//Based on values for 1:rest,2:standing/foraging,
//3:walking and 4:running. Activities that are assumed to be costly, but not
//taking up real time in the model (e.g. giving birth) is given a value of 7000
//Value "1" for resting, will be multiplied by RMR
// see enum roeEnergyUse
const int CostFemaleAct[12]={
  467,467,7000,500,2530,467,467,467,1,467,7000,7000
};
const int CostMaleAct[12]={
    467,2000,0,0,2530,467,467,467,1,467,7000,7000
};
const int CostFawnAct[12]={
    0,0,0,684,2530,1,0,1,1,684,0,0
         //feed & suckle = 1.3 * RMR (same proportional increase as in adults)    
};

//Ana. and catab. gives the cost and gain of storing vs. metabolising tissue
//In cal/kg tissue (i.e. fat). Data from farmed deer.
const double Anabolic = (double) 13186;
const double Anabolic_Inv = (double) (1.0/Anabolic);
// Compileren forudberegner denne v�rdi.
const double Catabolic = (double) 9400;
const double Catabolic_Inv = (double) (1.0/Catabolic);

//BACKGROUND MORTALITY, adults
//DailyMortRate (DMR)=1-(sqrt(survivalrate,365))
//(e.g. FDMR[0]= 1-(365sgrt of 0.77)
//In a year DMR is proportional to annual mortality rates from Gaillard et al.
//1993. All values are multiplied by 100.000 to get int values!!
//DMR[0]=1-2 yrs, DMR[1]=2-7 yrs, DMR[2]=7-10 yrs, DMR[3]=>10 yrs. From
//capture-recapture data
//Floaters are given an increased annual mortality
const int FloatMort = 0; // 180;
const double FemaleDMR[4]={
  24,3,35,220
};
const double MaleDMR[4]={
  27,14,28,220
};

//MORTALITY fawns
//FDMR[0]=0-6 months, FDMR[1]=6-12 months. Includes all kinds of mortality
//must be revised when predation is specifically included in model
const double FawnDMR[2]={ 68,16 };

//NURSING, female+fawn
//Minhrcare=minimum care per hour in timesteps.
//Duration_care=duration of each care
//period. Depends on fawn age in days:[0]=0-9,[1]=10-60
const int Minhrcare[2]={2,1};
const int Flush_dist[2]={1,7}; //fawn flush dist when approached
const int MalePrime = 2555; //age in days (=7 yrs) above which senescence is
                            //assumed to start (age is a disadvantage in ranks)    
const int GestationPeriod = 300;  //days

 /** \brief The fight mortality probability with win or loss */
const double FightMortalityWinLoss[2] = {0.0005, 0.001};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

