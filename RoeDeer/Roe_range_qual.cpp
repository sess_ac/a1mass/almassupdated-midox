//
// roe_range_qual.cpp
//

#include <assert.h>
#include <cstdio>
#include "../Landscape/ls.h"
#include <iostream>
#include <fstream>
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PositionMap.h"


#include "Roe_all.h"
#include "Roe_constants.h"

using namespace std;

// Functions and variables defined as static in the global scope are
// invisible outside their code module, ie. these are *not* globals!
// The linker will not be able to see these names, so we can use
// whatever names we want that are not already defined globally. Made
// only for readability of the code below.

// Notice well that the reverse is also true, ie. the innards of these
// static functions cannot see any global symbols, even though
// TheLandscape is a globally defined and declared pointer.
// The actual class definition of Landscape is visible, of course, as
// we have included the appropriate header file above.

// This is yet another reason why it is a good idea not to have the
// whole project in a single .cpp file. By using smaller, logically
// isolated, code modules we can use small static functions to clean
// up any inappropriately long methods without cluttering the class
// definition with function names that are only ever used once or
// twice in the inner workings of a method.

// We can save a multiplication when calculating a polygon's area if we 
// use two arrays instead of one two-dimensional one.
//
//

#define HASH_SIZE 2048  /** \todo Replace this C style approach with C++ to prevent potential bugs with large numbers of polygons. */
#define HASH_MASK (HASH_SIZE-1)

static int Polygonrefs[ HASH_SIZE ];
static int Polygonarea[ HASH_SIZE ];
static int OldPolyRef;
static int OldPolyIndex;

static inline double PolyArray2QualSum( void );
static inline double ForestSum(void);
static        void  ScanForPolys( int xmin, int ymin, int xmax, int ymax );
static        void  ScanForPolysModulus( int xmin, int ymin, int xmax, int ymax,int SimW, int SimH );
static inline bool  Scanner( int* P_Start );

static Landscape* LocalL;
static int        NoOfPolyrefs;



//----------------------------------------------------------------------------
/**
RangeQuality, calculates the quality of a grid cell of a
    predefined size (=const int Cell_Size, minimum is 20x20) using
	PolyArray2QualSum(). From this we get the quality of a polygon.
    The function then returns the total quality divided by area of square

*/
double RoeDeer_Population_Manager::RangeQuality(int p_x, int p_y,int p_range)
{

	NoOfPolyrefs = 0;
	OldPolyRef   =-1;
	OldPolyIndex =-1;

	int Start_x = p_x - p_range;
	int End_x = p_x + p_range;
	int Start_y = p_y - p_range;
	int End_y = p_y + p_range;

	LocalL = m_TheLandscape;

	// Clear the polygon reference array.
	for ( int i=0; i<HASH_SIZE; i++ )
		Polygonrefs[ i ] = -1;

	//First: get a list of polygons
	if ((Start_x >=0)&&(Start_y >=0) &&
		(End_x < SimW)&&(End_y < SimH)) {
		//no overlap
		ScanForPolys( Start_x, Start_y, End_x, End_y );
	}
	else {
		//Overlap
		ScanForPolysModulus( Start_x, Start_y,End_x, End_y, SimW, SimH );
	}

	// Convert the Polygonrefs[] array from map magic numbers to polygon
	// reference numbers.
	for ( int i=0; i<HASH_SIZE; i++ ) {
		if ( Polygonrefs [i] != -1 ) {
			Polygonrefs [i] = m_TheLandscape->MagicMapP2PolyRef( Polygonrefs [i] );
		}
	}

	//then get the element type of each polygon and multiply by area of polygon
	double quality = PolyArray2QualSum();

	//return total quality divided by area of square
	return  quality/((2*p_range)*(2*p_range));
}
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::PercentForest - assesses the proportion of forest in given range,
Calls functions ScanForPolys(), ScanForPolysModulus(), MagicMapP2PolyRef(), ForestSum().
*/
double RoeDeer_Population_Manager::PercentForest(APoint a_Loc, int p_range)
{

	NoOfPolyrefs = 0;
	OldPolyRef   =-1;
	OldPolyIndex =-1;

	int Start_x = a_Loc.m_x - p_range;
	int End_x = a_Loc.m_x + p_range;
	int Start_y = a_Loc.m_y - p_range;
	int End_y = a_Loc.m_y + p_range;

	LocalL = m_TheLandscape;

	// Clear the polygon reference array.
	for ( int i=0; i<HASH_SIZE; i++ )
		Polygonrefs[ i ] = -1;

	//First: get a list of polygons
	if ((Start_x >=0)&&(Start_y >=0) &&
		(End_x < SimW)&&(End_y < SimH)) {
		//no overlap
		ScanForPolys( Start_x, Start_y, End_x, End_y );
	}
	else {
		//Overlap
		ScanForPolysModulus( Start_x, Start_y,
			End_x, End_y,
			SimW, SimH );
	}

	// Convert the Polygonrefs[] array from map magic numbers to polygon
	// reference numbers.
	for ( int i=0; i<HASH_SIZE; i++ ) {
		if ( Polygonrefs [i] != -1 ) {
			Polygonrefs [i] = m_TheLandscape->MagicMapP2PolyRef( Polygonrefs [i] );
		}
	}

	//then get the element type of each polygon and multiply by area of polygon
	double forest = ForestSum();
	//return total forest divided by area of square
	return  (forest/(4*p_range*p_range))*100;
}
//-----------------------------------------------------------------------------

// This is where all the work is done.
//
// Speed is the most important here, so we optimise where we can.
//
//
// Deuden ved vi (jeg), at et kald til SupplyPolyRef(i,j) er
// suboptimalt, idet der g�r b�de en multiplikation, en addition og et
// opslag igennem C standardbibliotekets vector<> rutiner til denne
// ene ting. Ikke s�rligt underholdende, n�r vi laver dem i
// hundredevis af millioner om �ret (simulationstid)!
//
// Hovsa, landskabsmodellen har forresten et sjovt lille s�t metoder,
// som kan hj�lpe her...

// Det er under kvajebajerstraf forbudt at skrive noget ind i
// landskabets kort via den magiske pointer, dvs. udtryk af typen
// *magicP = foo;
// er lig �lregning.
// Skriv derfor hellere ( baz == *magicP ) end ( *magicP == baz ).
// Overvej.

// Desuden bruger vi et lille kneb, som kaldes en hashfunktion, til at
// s�ge efter et givet polygonnummer i vores Polygonrefs[] tabel.
// Denne metode sikrer, at vi med stor sandsynlighed kan finde
// nummeret i eet fors�g! Bagdelen er, at der kan v�re 'huller' i
// tabellen efter at vi har scannet hele arealet for polygoner.
// Disse huller symboliseres med v�rdien -1, som hele tabellen
// initialiseres med f�rend vi g�r i gang med scanningen.


static void   ScanForPolys( int xmin, int ymin, int xmax, int ymax )
{
	/**
	ScanForPolys - provides a list of polygons within the range specified by the coordinates.
	Calls SupplyMagicMapP().\n
	* First we need to check if we have already found a polygone number in the array Polygonrefs[].
	* If we have we can re-use this result, and we will normally see a polygon again because the cells are adjacent.
	* So we most often don't need to run through the whole Polygonrefs[] to find it.
	*/
	for (int y = ymin; y < ymax; y++) {

    int *P_Start = LocalL->SupplyMagicMapP( xmin, y );

    // Luk lige �jnene et �jeblik, Jane.
    int *P_End   = P_Start + (xmax - xmin);
    // S� m� du godt kigge igen. Husk, at man aldrig m� lave aritmetik
    // p� pointere! ;-)

    for ( ; P_Start<P_End; P_Start++ ) 
	{
		if ( OldPolyRef == *P_Start ) 
		{
			// We already have this result so re-use!
			Polygonarea [OldPolyIndex]++;

		} 
		else 
		{
			// No way out, search through the list.
			if ( !Scanner( P_Start )) {
				// Not in the list so add it.
				int k = *P_Start & HASH_MASK;
				while ( Polygonrefs[ k  ] != -1 )
					k = (k+1) & HASH_MASK;

				Polygonrefs [k] = *P_Start;
				OldPolyRef      = *P_Start;
				OldPolyIndex    = k;
				Polygonarea [k] = 1;
				NoOfPolyrefs++;
			}
		}
	}
	}
}



static inline bool  Scanner( int* P_Start )
{
	// Tuning done, CJT 22/07/2014
	int target = *P_Start;
	int k = target & HASH_MASK;

	if ( Polygonrefs[ k ] != target ) 
	{
		// Hash function missed in first attempt, scan for the value.
		// Now we have trouble, we may need to run through the whole table because we have no idea where the value we are looking for is hiding.
		// Let's make the assumption that it is more likely to be near this one than not. First try searching upwards, then downwards from our location.
		int j;
		for ( j=k+1; j<HASH_SIZE; j++ ) 
		{
			if ( Polygonrefs[ j ] == target ) 
			{
				// goto's are really bad style, but can be a bit useful sometimes :)
				goto foundit;
			}
		}
		for ( j=k-1; j>=0; j-- ) 
		{
			if ( Polygonrefs[ j ] == target ) 
			{
				goto foundit;
			}
		}
		return false;
	foundit:
		k = j;
	}
	OldPolyIndex = k;
	OldPolyRef   = target;
	Polygonarea [k]++;
	return true;
}


//----------------------------------------------------------------------------
/**
ScanForPolysModulus - provides a list of polygons within range specified by coordinates - and corrects
coordinates for wrap around.
Calls ScanForPolys().
*/

static void  ScanForPolysModulus( int Start_x, int Start_y,
				  int End_x,   int End_y,
				  int SimW, int SimH )
{
  // Vi checker for de forskellige muligheder for overlap, og kalder
  // ScanForPolys() med allerede korrigerede koordinater (op til 4
  // gange i v�rste tilf�lde). Dette sparer modulus funktionen.

  // Her g�r vi ud fra, at 2*p_range aldrig er st�rre end kortets
  // h�jde eller bredde.

  if ( Start_x < 0 ) {
    if ( Start_y < 0 ) {
      // Overlap i negativ y, og i negativ x.
      ScanForPolys( Start_x + SimW, Start_y + SimH,
		    SimW, SimH );
      ScanForPolys( 0, Start_y + SimH,
		    End_x, SimH );
      ScanForPolys( 0, 0,
		    End_x, End_y );
      ScanForPolys( Start_x + SimW, 0,
		    SimW, End_y );

    } else if ( End_y >= SimH ) {
      // Overlap i positiv y, og i negativ x.
      ScanForPolys( Start_x + SimW, Start_y,
		    SimW, SimH );
      ScanForPolys( 0, Start_y,
		    End_x, SimH );
      ScanForPolys( 0, 0,
		    End_x, End_y - SimH );
      ScanForPolys( Start_x + SimW, 0,
		    SimW, End_y - SimH );

    } else {
      // Overlap i negativ x, ikke i y.
      ScanForPolys( 0, Start_y,
		    End_x, End_y );
      ScanForPolys( Start_x + SimW, Start_y,
		    SimW, End_y );

    }
  } else if ( End_x >= SimW ) {
    if ( Start_y < 0 ) {
      // Overlap i negativ y, og i positiv x.
      ScanForPolys( Start_x, Start_y + SimH,
		    SimW, SimH );
      ScanForPolys( 0, Start_y + SimH,
		    End_x - SimW, SimH );
      ScanForPolys( 0, 0,
		    End_x - SimW, End_y );
      ScanForPolys( Start_x, 0,
		    SimW, End_y );

    } else if ( End_y >= SimH ) {
      // Overlap i positiv y, og i positiv x.
      ScanForPolys( Start_x, Start_y,
		    SimW, SimH );
      ScanForPolys( 0, Start_y ,
		    End_x - SimW, SimH );
      ScanForPolys( 0, 0,
		    End_x - SimW, End_y - SimH );
      ScanForPolys( Start_x, 0,
		    SimW, End_y - SimH );

    } else {
      // Overlap i positiv x, ikke i y.
      ScanForPolys( Start_x, Start_y,
		    SimW, End_y );
      ScanForPolys( 0, Start_y,
		    End_x - SimW, End_y );

    }
  } else {
    // Intet overlap i x.
    if ( Start_y < 0 ) {
      // Overlap i negativ y, ikke i x.
      ScanForPolys( Start_x, Start_y + SimH,
		    End_x, 0 );
      ScanForPolys( Start_x, 0,
		    End_x, End_y );

    } else {
      // Overlap i positiv y, ikke i x.
      ScanForPolys( Start_x, Start_y,
		    End_x, SimH );
      ScanForPolys( Start_x, 0,
		    End_x, End_y - SimH );

    }
  }
}


//-----------------------------------------------------------------------------
/**
PolyArray2QualSum - polygons are given scores dependent on cover types. The score for fields depend
on the specific crop type. The scores are then multiplied by area of the polygon
Calls SupplyElementType(), SupplyVegType(), SupplyTreeHeight().
*/

static inline double PolyArray2QualSum(void)
{
	double quality = 0.0;

	for (int h = 0; h < HASH_SIZE; h++) {
		if (Polygonrefs[h] == -1)
			continue;

		TTypesOfLandscapeElement El =
			LocalL->SupplyElementType(Polygonrefs[h]);

		// good cover polygons very important (high score).
		// Permanent grassy habitats
		// important for feeding (intermediate score), score for fields
		// depends on
		// crop type, water, coast and the like of no importance (score=0),
		// urban is bad (negative score)

		switch (El)
		{
		case tole_Building:
		case tole_Garden:
		case tole_UrbanNoVeg:
		case tole_Churchyard:
		case tole_Carpark:
		case tole_SandDune:
		case tole_BuiltUpWithParkland:
		case tole_UrbanPark:
		case tole_PlantNursery:
		case tole_UrbanVeg:
			quality -= 4 * Polygonarea[h];
			break;

		case tole_ActivePit:
		case tole_LargeRoad:
		case tole_SmallRoad:
			quality -= 2 * Polygonarea[h];
			break;

		case tole_StoneWall:	//neutral
		case tole_Fence:		//neutral
		case tole_Saltwater:	//neutral
		case tole_Freshwater:	//neutral
		case tole_Pond:
		case tole_River:		//neutral
		case tole_Coast:		//neutral
		case tole_Railway:		//neutral
		case tole_Track:		//neutral
		case tole_BareRock:		//neutral
		case tole_Stream:		//neutral
		case tole_Saltmarsh:
		case tole_WindTurbine:
		case tole_Pylon:
			break;

		case tole_RoadsideVerge:
		case tole_RoadsideSlope:
		case tole_PermPasture:
		case tole_PermPastureTussocky:
		case tole_PermPastureLowYield:
		case tole_ConiferousForest:
		case tole_Marsh:
		case tole_Heath:
		case tole_AmenityGrass:
		case tole_HeritageSite:
		case tole_RiversidePlants:
		case tole_NaturalGrassDry:
		case tole_NaturalGrassWet:
		case tole_PermanentSetaside:
		case tole_HedgeBank:
		case tole_FieldBoundary:
		case tole_Orchard:
		case tole_Wasteland:
		case tole_UnknownGrass:
			quality += 2 * Polygonarea[h];
			break;

		case tole_Scrub:
		case tole_PitDisused:
		case tole_RiversideTrees:
		case tole_Vildtager:
		quality += 2.5f* Polygonarea[h];
			break;
		case tole_Field:
		{
						   TTypesOfVegetation veg =
							   LocalL->SupplyVegType(Polygonrefs[h]);
						   switch (veg)
						   {
						   case tov_SpringBarley:  //grassy, non-grazed
						   case tov_SpringBarleySpr:  //grassy, non-grazed
						   case tov_SpringWheat:
						   case tov_Oats:
						   case tov_Triticale:
						   case tov_SeedGrass2:
						   case tov_OSpringBarley:
						   case tov_OOats:
						   case tov_OTriticale:
						   case tov_SetAside:
						   case tov_OSetAside:
						   case tov_PermanentSetAside:
						   case tov_SeedGrass1:
						   case tov_SpringBarleySilage:
						   case tov_OSeedGrass1:
						   case tov_OSeedGrass2:
						   case tov_OCloverGrassSilage1:
						   case tov_NaturalGrass:
						   case tov_Heath:
							   quality += 2.2f*Polygonarea[h];
							   break;


						   case tov_PotatoesIndustry:    //neutral
						   case tov_Potatoes:
						   case tov_SpringRape:
						   case tov_WinterRape:
						   case tov_OWinterRape:
						   case tov_OCarrots:
						   case tov_OPotatoes:
						   case tov_OGrazingPigs:
						   case tov_OSpringBarleyPigs:
						   case tov_NoGrowth:
						   case tov_OFirstYearDanger:
						   case tov_None:
							   break;

						   case tov_OSBarleySilage:
						   case tov_OBarleyPeaCloverGrass:    //undersown+winter crops
						   case tov_OSpringBarleyGrass:
						   case tov_OSpringBarleyClover:
						   case tov_SpringBarleyCloverGrass:
						   case tov_OWinterBarley:
						   case tov_OWinterWheatUndersown:
						   case tov_OWinterWheat:
						   case tov_OWinterRye:
						   case tov_SpringBarleyGrass:
						   case tov_WinterBarley:
						   case tov_WinterWheat:
						   case tov_WinterWheatShort:
						   case tov_WinterRye:
							   quality += 2.8* Polygonarea[h];
							   break;
						   case tov_BroadBeans:
						   case tov_SugarBeet:
						   case tov_FodderBeet:
						   case tov_OFodderBeet:
						   case tov_FieldPeas:
						   case tov_OFieldPeasSilage:
						   case tov_OFieldPeas:
						   case tov_Maize:
						   case tov_MaizeSilage:
						   case tov_MaizeStrigling:
						   case tov_OMaizeSilage:
							   quality += 3 * Polygonarea[h];
							   break;

						   case tov_CloverGrassGrazed1:   //grazed
						   case tov_CloverGrassGrazed2:
						   case tov_OCloverGrassGrazed1:
						   case tov_OCloverGrassGrazed2:
						   case tov_PermanentGrassGrazed:
						   case tov_OPermanentGrassGrazed:
						   case tov_OrchardCrop:
							   quality += Polygonarea[h];
							   break;

						   case tov_Undefined:
						   default:
							   LocalL->Warn("Roe_Range_Quality::PolyArray2QualSum():No matching veg case!", LocalL->VegtypeToString(veg));
							   exit(1);
							   break;
						   }
		}
			break;
		case tole_DeciduousForest:
		case tole_MixedForest:
		case tole_Copse:
		case tole_IndividualTree:
		case tole_WoodlandMargin:
		case tole_WoodyEnergyCrop:
		{
									int height = LocalL->SupplyTreeHeight(Polygonrefs[h]);
									if (height <= 5) quality += 4 * Polygonarea[h];
									else quality += 2.5f* Polygonarea[h];
		}
			break;
		case tole_Hedges:
			quality += 2.5f* Polygonarea[h];
			break;
		case tole_YoungForest:  //new class 55
			quality += 3 * Polygonarea[h];
			break;

		case tole_Foobar:
		default:
			LocalL->Warn("Roe_Range_Quality::PolyArray2QualSum():No matching elem case!", LocalL->PolytypeToString(El));
			exit(1);
		}

	}

	return quality;
}




//----------------------------------------------------------------------------
/**
ForestSum -calculates quality values for forested polygons by multiplying by area of polygon.
Calls SupplyElementType().
*/
static inline double ForestSum(void)
{
	// CJT modified 22/07/2014 to speed up code
	double quality = 0.0;
	for (int h = 0; h < HASH_SIZE; h++)
	{
		if (Polygonrefs[h] == -1)    continue;
		TTypesOfLandscapeElement El = LocalL->SupplyElementType(Polygonrefs[h]);
		//Forest and hedges scores 1 * area, everything else scores 0
		switch (El)
		{
		case tole_RiversideTrees:
		case tole_DeciduousForest:
		case tole_ConiferousForest:
		case tole_MixedForest:
		case tole_Hedges:
		case tole_HedgeBank:
		case tole_YoungForest:  //new class 55
		case tole_Copse:
		case tole_IndividualTree:
		case tole_WoodyEnergyCrop:
		case tole_WoodlandMargin:
			quality += Polygonarea[h];
			break;
		}
	}
	return quality;
}
//----------------------------------------------------------------------------

