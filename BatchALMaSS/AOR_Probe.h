#pragma once
// The default function that is used by the probe class
// returns true for any coordinate:
bool always_true(int x, int y, Landscape *);
typedef vector < int > ListOfCells;
typedef vector < bool > ListOfCellBools;
class AOR_Probe_Base{
protected:
    ofstream m_ProbeFile;
    int m_gridcountwidth[4];
    int m_gridcountheight[4];
    int m_gridcountsize[4];
    int m_totalcells[4];
    int m_exclusivecells[4];

    int m_gridwidthexp[4]; // the 'real' width of the cells
    int m_gridheightexp[4]; // the 'real' height of the cells
    ListOfCells m_gridcount[4];
    ListOfCellBools m_gridexclusive[4];
    bool (*m_filter_fun)(int, int, Landscape *);
    Landscape* m_TheLandscape;
    Population_Manager_Base* m_owner;
    bool m_IsExclusive{false};
public:
    AOR_Probe_Base(Population_Manager_Base* a_owner,Landscape* a_TheLandscape, string a_filename, bool (*filter_fun)(int, int, Landscape *)=always_true, bool IsExclusive=false);
    virtual ~AOR_Probe_Base() {
        CloseFile();
    }

    virtual void CloseFile() {
        m_ProbeFile.close();
    }
    void WriteData();
    virtual void DoProbe(int a_lifestage);
};
class AOR_Probe: public AOR_Probe_Base
{

public:
	AOR_Probe(Population_Manager* a_owner,Landscape* a_TheLandscape, string a_filename, bool (*filter_fun)(int, int, Landscape *)=always_true, bool IsExclusive=false);
	virtual void DoProbe(int a_lifestage);
};

class AOR_Probe_Goose : public AOR_Probe
{
protected:
public:
	AOR_Probe_Goose(Population_Manager* a_owner, Landscape* a_TheLandscape, string a_filename, bool (*filter_fun)(int, int, Landscape *)=always_true, bool IsExclusive=false);
	void DoProbe(int a_lifestage) override;
};
class AOR_Probe_Beetle : public AOR_Probe
{
protected:
public:
    AOR_Probe_Beetle(Population_Manager* a_owner, Landscape* a_TheLandscape, string a_filename, bool (*filter_fun)(int, int, Landscape *)=always_true, bool IsExclusive=false);
    void DoProbe(int a_lifestage) override;
};

