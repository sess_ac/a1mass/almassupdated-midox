/**
 * \file almass_random.cpp
 * \author Luminita C. Totu
 * \version 1.0
 *
 * \bried Implementing almass specific random number generation and probability distribution classes, based on/wrapping Boost-library functionality 
 *
 * \section LICENSE
 *
 * \section DESCRIPTION
 * This file contains the definition of the rng global object, which holds the basic randomness source (the seeded pseudo random generator).
 * The file also contains the definition of functions declared in almass_random-h header file
 */

#include <fstream>
#include <chrono>

/* Boost Seed and Generators */
#include <boost/random/seed_seq.hpp>
// #include <boost/random/mersenne_twister.hpp>
#include <boost/random/lagged_fibonacci.hpp>

/* Boost Distributions */
#include <boost/random/normal_distribution.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/beta_distribution.hpp>
#include <boost/random/gamma_distribution.hpp>
#include <boost/random/cauchy_distribution.hpp>
#include <boost/random/exponential_distribution.hpp>

/* Boost Math helper function */
#include <boost/math/special_functions/binomial.hpp> // for the calculation of the custom beta-binomial probabilities
#include <boost/math/special_functions/gamma.hpp> // for the calculation of the custom gamma probabilities

/* For string splitting */
#include<boost/algorithm/string.hpp>   
#include <boost/algorithm/string/split.hpp>

#include "ALMaSS_Random.h"
#include "../Landscape/MapErrorMsg.h"
#include "../Landscape/Configurator.h"
#include <random>



extern MapErrorMsg* g_msg;
extern CfgBool cfg_fixed_random_sequence;
extern CfgInt cfg_fixed_random_seed;


//Define (a single time) the random seed 
boost::random::seed_seq seed_seq{
	// static_cast<std::uintmax_t>(std::random_device{ }()), // We would need to link with boost libraries to use the OS random_device() 
	static_cast<std::uintmax_t>(std::chrono::steady_clock::now().time_since_epoch().count()) };

boost::random::lagged_fibonacci19937 g_generator(seed_seq);
// As a temporary solution we will also initialise the std library random engine
// This is all because we need it for random shuffle. Boosts random_shuffle cannot accept begin and end parameters of the range
// which makes it impossible to use in Beetle_Population_Manager::Shuffle( unsigned Type ) when we want to shuffle the size of the livearray
std::default_random_engine g_std_rand_engine(seed_seq);

static boost::random::uniform_real_distribution<double> g_uni_dist(0, 1);
static boost::random::exponential_distribution<double> g_exp_dist(5);

/**
 * frequently used function to return a double between [0,1) with uniform probability
 */
double g_rand_uni() {
	return g_uni_dist(g_generator);
}

/**
 * fixed seed for the random generator
 */
void g_generator_fixed(unsigned int s)
{
	g_generator.seed(s);
    g_std_rand_engine.seed(s);
}

/**
 *  Local utility function for generating the weights-vector for the beta binomial distribution from input parameters
 *
 *	\param[in]  N the maximum value of the random variate
 *	\param[in]  alpha, beta  parameters of the underlying beta distribution, together determine the shape/profile of the distribution
 *	\param[in]  no_zero extra parameter to cancel out the probability for the zero value (started as quick hack for beetle feature)
 *	\param[out] weights for the probability of each outcome [0,...,N]
 */

std::vector<double> beta_binomial_probabilities(int N, double alpha, double beta, bool no_zero) {

	std::vector<double> probabilities(N + 1);
	double Cnk, B1, B2;

	for (int k = 0; k <= N; k = k + 1)
	{
		Cnk = boost::math::binomial_coefficient<double>(N, k);
		B1 = boost::math::beta<double, double, int>(k + alpha, -k + beta + N, 1);
		B2 = boost::math::beta<double, double, int>(alpha, beta, 1);
		probabilities[k] = Cnk * B1 / B2;
	}

	if (no_zero == true) {
		probabilities[0] = 0;
	}

	return probabilities;
}

/* type definition to be used in the probability_distribution class implementation only */
typedef boost::random::discrete_distribution<int, double> DISCRETE_DIST;
typedef boost::random::uniform_int_distribution<int> UNIINT_DIST;
typedef boost::random::normal_distribution<double> NORMAL_DIST;
typedef boost::random::uniform_real_distribution<double> UNIREAL_DIST;
typedef boost::random::beta_distribution<double> BETA_DIST;
typedef boost::random::gamma_distribution<double> GAMMA_DIST;
typedef boost::random::cauchy_distribution<double> CAUCHY_DIST;
typedef boost::random::exponential_distribution<double> EXPONENTIAL_DIST;

probability_distribution::probability_distribution(std::string prob_type, std::string prob_params) {

	// take the argument string and split it into numerical values ( make this a function )
	std::vector<std::string> split_args;
	boost::algorithm::split(split_args, prob_params, boost::is_any_of(" , \t"), boost::token_compress_on);
	int no_args = split_args.size();
	std::vector<double> number_args(no_args);
	std::transform(split_args.begin(), split_args.end(), number_args.begin(), [](const std::string& val) {
		double result = 0;
		try {
			result = stod(val);
		}
		catch (const std::invalid_argument & ia) {
			g_msg->Warn(WARN_MSG, "Error parsing the arguments of a probability_distribution object into numerical values", ia.what());
			exit(1);
		}
		catch (const std::out_of_range & oor) {
			g_msg->Warn(WARN_MSG, "Error parsing the arguments of a probability_distribution object into numerical values", oor.what());
			exit(1);
		}
		return result;
		});

	// for each supported probability distribution type, perform initialization
	if (boost::iequals(prob_type, DISCRETE_DIST_S))
	{
		m_probDistrib = new DISCRETE_DIST(number_args);
		m_type = DISCRETE_DIST_T;
		return;
	}
	if (boost::iequals(prob_type, BETABINOMIAL_DIST_S))
	{
		if (no_args != 4) {
			g_msg->Warn(WARN_MSG, "Number of arguments for a Beta Binomial distribution needs to be 4. It is now: ", no_args);
			exit(1);
		}
		m_probDistrib = new DISCRETE_DIST(beta_binomial_probabilities(int(number_args[0]), number_args[1], number_args[2], bool(number_args[3]))); // max value, alpha, beta, prob(outcome=0) = 0 boolean option
		m_type = DISCRETE_DIST_T;
		return;
	}
	else if (boost::iequals(prob_type, NORMAL_DIST_S))
	{
		if (no_args != 2) {
			g_msg->Warn(WARN_MSG, "Number of arguments for a Normal distribution needs to be 2. It is now: ", no_args);
			exit(1);
		}
		if (number_args[1] < 0) {
			g_msg->Warn(WARN_MSG, "Std. deviation (sigma) parameter of normal distribution is negative:", number_args[1]);
			exit(1);
		}
		m_probDistrib = new NORMAL_DIST(number_args[0], number_args[1]); //  mean, sigma>0
		m_type = NORMAL_DIST_T;
		return;
	}
	else if (boost::iequals(prob_type, UNIREAL_DIST_S))
	{
		if (no_args != 2) {
			g_msg->Warn(WARN_MSG, "Number of arguments for a uniform  real distribution needs to be 2. It is now: ", no_args);
			exit(1);
		}
		if (number_args[0] > number_args[1]) {
			g_msg->Warn(WARN_MSG, "Min param > Max param of uniform real distribution :", std::to_string(number_args[0])+" "+std::to_string(number_args[1]));
			exit(1);			
		}
		m_probDistrib = new UNIREAL_DIST(number_args[0], number_args[1]); // [min, max)
		m_type = UNIREAL_DIST_T;
		return;
	}
	else if (boost::iequals(prob_type, UNIINT_DIST_S))
	{
		if (no_args != 2) {
			g_msg->Warn(WARN_MSG, "Number of arguments for a uniform int distribution needs to be 2. It is now: ", no_args);
			exit(1);
		}
		if (number_args[0] > number_args[1]) {
			g_msg->Warn(WARN_MSG, "Min param > Max param of uniform integer distribution :", std::to_string(number_args[0]) + " " + std::to_string(number_args[1]));
			exit(1);
		}
		m_probDistrib = new UNIINT_DIST(int(number_args[0]), int(number_args[1])); // [min, max]
		m_type = UNIINT_DIST_T;
		return;
	}
	else if (boost::iequals(prob_type, BETA_DIST_S))
	{
		if (no_args != 2) {
			g_msg->Warn(WARN_MSG, "Number of arguments for a beta distribution needs to be 2. It is now: ", no_args);
			exit(1);
		}
		if ((number_args[0]<=0) || (number_args[1]<=0)) {
			g_msg->Warn(WARN_MSG, "The parameters of the beta distribution need to be strictly positive values. They are now: ", std::to_string(number_args[0]) + " " + std::to_string(number_args[1]));
			exit(1);
		}
		m_probDistrib = new BETA_DIST(double(number_args[0]), double(number_args[1])); // alpha and beta
		m_type = BETA_DIST_T;
	}
	else if (boost::iequals(prob_type, GAMMA_DIST_S))
	{
		if (no_args != 2) {
			g_msg->Warn(WARN_MSG, "Number of arguments for a gamma distribution needs to be 2. It is now: ", no_args);
			exit(1);
		}
		if ((number_args[0] <= 0) || (number_args[1] <= 0)) {
			g_msg->Warn(WARN_MSG, "The parameters of the gamma distribution need to be strictly positive values. They are now: ", std::to_string(number_args[0]) + " " + std::to_string(number_args[1]));
			exit(1);
		}
		m_probDistrib = new GAMMA_DIST(number_args[0], number_args[1]); // alpha and beta
		m_type = GAMMA_DIST_T;
		return;
	}
	else if (boost::iequals(prob_type, CAUCHY_DIST_S))
	{
		if (no_args != 2) {
			g_msg->Warn(WARN_MSG, "Number of arguments for a cauchy distribution needs to be 2. It is now: ", no_args);
			exit(1);
		}
		if ((number_args[1] <= 0)) {
			g_msg->Warn(WARN_MSG, "The scale parameters of the cauchy distribution need to be a strictly positive value. They are now: ", std::to_string(number_args[1]));
			exit(1);
		}
		m_probDistrib = new CAUCHY_DIST(number_args[0], number_args[1]); // location and scale
		m_type = CAUCHY_DIST_T;
		return;
	}
	else if (boost::iequals(prob_type, EXPONENTIAL_DIST_S))
	{
		if (no_args != 1) {
			g_msg->Warn(WARN_MSG, "Number of arguments for a exponential distribution needs to be 1. It is now: ", no_args);
			exit(1);
		}
		if ((number_args[1] <= 0)) {
			g_msg->Warn(WARN_MSG, "The scale parameters of the exponential distribution need to be a strictly positive value. They are now: ", std::to_string(number_args[1]));
			exit(1);
		}
		m_probDistrib = new EXPONENTIAL_DIST(number_args[0]); // location and scale
		m_type = EXPONENTIAL_DIST_T;
		return;
	}
	else
	{
		g_msg->Warn(WARN_UNDEF, "Unknown distribution type string value: ", prob_type);
		exit(1);
	}

}

double probability_distribution::get()
{
	
	switch (m_type)
	{
	case DISCRETE_DIST_T:
	{
		int x = (*static_cast<DISCRETE_DIST*>(m_probDistrib))(g_generator);
		return double(x);
	}
	case NORMAL_DIST_T:
	{
		double x = (*static_cast<NORMAL_DIST*>(m_probDistrib))(g_generator);
		return x;
	}
	case UNIREAL_DIST_T:
	{
		double x = (*static_cast<UNIREAL_DIST*>(m_probDistrib))(g_generator);
		return x;
	}
	case UNIINT_DIST_T:
	{
		int x = (*static_cast<UNIINT_DIST*>(m_probDistrib))(g_generator);
		return double(x);
	}
	case BETA_DIST_T:
	{
		double x = (*static_cast<BETA_DIST*>(m_probDistrib))(g_generator);
		return x;
	}
	case GAMMA_DIST_T:
	{
		double x = (*static_cast<GAMMA_DIST*>(m_probDistrib))(g_generator);
		return x;
	}
	case CAUCHY_DIST_T:
	{
		double x = (*static_cast<CAUCHY_DIST*>(m_probDistrib))(g_generator);
		return x;
	}

	case EXPONENTIAL_DIST_T:
	{
		double x = (*static_cast<EXPONENTIAL_DIST*>(m_probDistrib))(g_generator);
		return x;
	}
	default:
		g_msg->Warn(WARN_BUG, "Unknown distribution type number: ", std::to_string(m_type));
		return 0; // should never happen, all supported types are included here
	}
}

int probability_distribution::geti()
{
	switch (m_type)
	{
	case DISCRETE_DIST_T:
	{
		int x = (*static_cast<DISCRETE_DIST*>(m_probDistrib))(g_generator);
		return x;
	}
	case UNIINT_DIST_T:
	{
		int x = (*static_cast<UNIINT_DIST*>(m_probDistrib))(g_generator);
		return x;
	}
	default:
		return 0; // can happen, not all types are integer 
	}
}

probability_distribution::~probability_distribution()
{
	switch (m_type)
	{
	case DISCRETE_DIST_T:
	{
		delete(static_cast<DISCRETE_DIST*>(m_probDistrib));
		return;
	}
	case NORMAL_DIST_T:
	{
		delete(static_cast<NORMAL_DIST*>(m_probDistrib));
		return;
	}
	case UNIREAL_DIST_T:
	{
		delete(static_cast<UNIREAL_DIST*>(m_probDistrib));
		return;
	}
	case UNIINT_DIST_T:
	{
		delete(static_cast<UNIINT_DIST*>(m_probDistrib));
		return;
	}
	case BETA_DIST_T:
	{
		delete(static_cast<BETA_DIST*>(m_probDistrib));
		return;
	}
	case GAMMA_DIST_T:
	{
		delete(static_cast<GAMMA_DIST*>(m_probDistrib));
		return;
	}
	case CAUCHY_DIST_T:
	{
		delete(static_cast<CAUCHY_DIST*>(m_probDistrib));
		return;
	}
	case EXPONENTIAL_DIST_T:
	{
		delete(static_cast<EXPONENTIAL_DIST*>(m_probDistrib));
		return;
	}
	default:
		g_msg->Warn(WARN_BUG, "Unknown distribution type number: ", std::to_string(m_type));
		return; // should never happen, all supported types are included here
	}	
};


static boost::random::uniform_int_distribution<int> g_uni_dist2(0, 9999);
static boost::random::uniform_int_distribution<int> g_uni_dist3(0, 999);

/** 
 * g_rand_uni2() Return a random integer number from {0,1,...,9999} with uniform probability
 * Function implemented for old code compatibility. Ideally not to be used in new code.
 */
int g_rand_uni2() {
	return g_uni_dist2(g_generator);
}

/**
 * g_rand_uni2() Return a random integer number from {0,1,...,999} with uniform probability
 * Function implemented for old code compatibility. Ideally not to be used in new code.
 */
int g_rand_uni3() {
	return g_uni_dist3(g_generator);
}

/**
 * random(a_range)  Return a random integer number from {0,1,...,a_range-1} with uniform probability
 * Function implemented for old code compatibility. Ideally not to be used in new code.
 */
int random(int a_range) {

    //old version 1: int result = (int)(((double) rand() / RAND_MAX. ) * a_range);
	//old version 2: return (int)(g_rand_uni() * a_range);
	//old version 3: int result = rand()%a_range;

	//Note: (int) rounds always down

	// Tested. This is at least as fast as any of the older versions
	/** CJT Temporary replaced with old version because of crash when range is zero *
	//boost::random::uniform_int_distribution<int> dis(0, a_range - 1); /* -1 for backward compatibility */
	//return dis(g_generator);
	return (int)(g_rand_uni() * a_range);
}

/**
*  init_random_seed() randomizes the random generator based on the configuration
*  (either fixed seed or random seed)
*/
void init_random_seed()
{
	int seed;

	// Randomize the random generator.
	if (cfg_fixed_random_sequence.value())
	{
		seed = cfg_fixed_random_seed.value();
		srand(seed); // Use of rand() discouraged. use internal almass_random.h functionality
		g_generator_fixed(seed);

		/* Logging */
		std::cout << "Setting a fixed seed for srand and boost: " << seed << "\n";
		g_msg->WarnAddInfo(WARN_MSG, "Setting a fixed seed for srand and boost: ", seed);
	}
	else
	{
		seed = (int)time(NULL);
		//std::ofstream RecordSeedFile;
		//RecordSeedFile.open("RecordedSeed.txt");
		//RecordSeedFile << seed;
		//RecordSeedFile.close();
		srand(seed); // Use of rand() discouraged. use internal almass_random.h functionality 

		/* Logging */
		//std::cout << "Setting a timestamp-based seed for srand: " << seed << "\n";
		//g_msg->WarnAddInfo(WARN_MSG, "Setting a timestamp-based seed for srand: ", seed);
		/* Recover the boost seed */
		std::string boost_seed_seq_str;
		if (seed_seq.size() > 10)
			boost_seed_seq_str = "More than 10 elements, too long to display";
		else
		{
			std::uintmax_t iter[10] = { 0 };
			boost_seed_seq_str = "(" + std::to_string(seed_seq.size()) + " element/s) ";
			seed_seq.param(iter);
			for (int i = 0; i < seed_seq.size(); i++)
				boost_seed_seq_str = boost_seed_seq_str + std::to_string(iter[i]) + " ";
		}
		std::cout << "Setting a timestamp-based seed sequence for boost: " << boost_seed_seq_str << "\n";
		//g_msg->WarnAddInfo(WARN_MSG, "Setting a timestamp-based seed sequence for boost: ", boost_seed_seq_str);
	}

}
