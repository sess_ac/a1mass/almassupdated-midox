/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PopulationManager.cpp This is the code file for the population manager and associated classes</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 23rd July 2003 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 January 2008 \n
 Doxygen formatted comments in May 2008 \n
*/
//---------------------------------------------------------------------------
//
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string.h>
#include <list>
#include <numeric>


#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../People/Hunters/Hunters_all.h"
#include "../GooseManagement/Goose_Population_Manager.h"
#ifdef __ALMASS_VISUAL
#include "wx/wx.h"
#include "wx/spinctrl.h"
#include "../GUI/ALMaSS_GUI.h"
#endif
#include "AOR_Probe.h"


using namespace std;

//------------------------------------------------------------------------------


#define _CRTDBG_MAP_ALLOC

static CfgStr cfg_RipleysOutput_filename( "G_RIPLEYSOUTPUT_FILENAME", CFG_CUSTOM, "RipleysOutput.txt" );
static CfgStr cfg_ReallyBigOutput_filename( "G_REALLYBIGOUTPUT_FILENAME", CFG_CUSTOM, "ReallyBigOutput.txt" );
CfgBool cfg_RipleysOutputMonthly_used( "G_RIPLEYSOUTPUTMONTHLY_USED", CFG_CUSTOM, false );
CfgBool cfg_RipleysOutput_used("G_RIPLEYSOUTPUT_USED", CFG_CUSTOM, false);
CfgBool cfg_AOROutput_used("G_AOROUTPUT_USED", CFG_CUSTOM, false);
CfgBool cfg_ReallyBigOutput_used( "G_REALLYBIGOUTPUT_USED", CFG_CUSTOM, false );
CfgBool cfg_fixed_random_sequence("G_FIXEDRANDOMSEQUENCE",CFG_CUSTOM,false);
CfgInt cfg_AOROutput_interval("G_AORSOUTPUT_INTERVAL", CFG_CUSTOM, 1);
CfgInt cfg_AOROutput_day("G_AOROUTPUT_DAY", CFG_CUSTOM, 60);
CfgInt cfg_AOROutputFirstYear("G_AOROUTPUT_FIRSTYEAR", CFG_CUSTOM, 1);
CfgInt cfg_RipleysOutput_interval("G_RIPLEYSOUTPUT_INTERVAL", CFG_CUSTOM, 1);
CfgInt cfg_RipleysOutput_day("G_RIPLEYSOUTPUT_DAY", CFG_CUSTOM, 60);
CfgInt cfg_RipleysOutputFirstYear("G_RIPLEYSOUTPUT_FIRSTYEAR", CFG_CUSTOM, 1);
CfgInt cfg_ReallyBigOutput_interval( "G_REALLYBIGOUTPUT_INTERVAL", CFG_CUSTOM, 1 );
CfgInt cfg_ReallyBigOutput_day1( "G_REALLYBIGOUTPUT_DAY_ONE", CFG_CUSTOM, 1 );
CfgInt cfg_ReallyBigOutput_day2( "G_REALLYBIGOUTPUT_DAY_TWO", CFG_CUSTOM, 91 );
CfgInt cfg_ReallyBigOutput_day3( "G_REALLYBIGOUTPUT_DAY_THREE", CFG_CUSTOM, 182 );
CfgInt cfg_ReallyBigOutput_day4( "G_REALLYBIGOUTPUT_DAY_FOUR", CFG_CUSTOM, 274 );
CfgInt cfg_ReallyBigOutputFirstYear("G_REALLYBIGOUTPUT_FIRSTYEAR",CFG_CUSTOM,1);
CfgInt cfg_fixed_random_seed("G_FIXEDRANDOMSEED", CFG_CUSTOM, 0);
static CfgBool cfg_VoleCatastrophe_on("VOLE_CATASTROPHE_ON", CFG_CUSTOM, false);
static CfgInt cfg_VoleCatastrophe_interval("VOLE_CATASTROPHE_I", CFG_CUSTOM, 365 * 5);
static CfgInt cfg_VoleCatastrophe_mortality("VOLE_CATASTROPHE_M", CFG_CUSTOM, 90);

// Catastrophe config variables
CfgInt cfg_CatastropheEventStartYear("PM_CATASTROPHEEVENTSTARTYEAR",CFG_CUSTOM,999999); // this means unless this is altered it events will not happen
CfgInt cfg_pm_eventfrequency("PM_EVENTFREQUENCY", CFG_CUSTOM, 999999); // every X years
CfgInt cfg_pm_eventday("PM_EVENTDAY", CFG_CUSTOM, March); // default 1st March
CfgInt cfg_pm_eventsize( "PM_EVENTSIZE", CFG_CUSTOM, 100 ); // The percentage change in population size, 100=no change
static CfgInt cfg_DayInMonth( "PRB_DAYINMONTH", CFG_CUSTOM, 1 );


extern std::default_random_engine g_std_rand_engine;
char g_str[255];

TTypesOfPopulation g_Species;


//---------------------------------------------------------------------------
//                        Population Manager
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void probe_data::CloseFile() {
  if ( m_MyFile != NULL ) {
	  if (m_MyFile->is_open()) m_MyFile->close();
	  delete m_MyFile;
  }
};

/**
\brief
Function to compare to TAnimal's m_Location_x
*/
class CompareX {
public:
    bool operator() ( const std::shared_ptr<TAnimal> &A1, const std::shared_ptr<TAnimal> &A2 ) const {
        return (A1->Supply_m_Location_x() < A2->Supply_m_Location_x());
    }
};

class CompareY {
public:
    bool operator() ( const std::shared_ptr<TAnimal> &A1, const std::shared_ptr<TAnimal> &A2 ) const {
        return (A1->Supply_m_Location_y() < A2->Supply_m_Location_y());
    }
};
/**
\brief
Function to compare to TAnimal's m_CurrentStateNo to anything but -1
*/
class CompareStateAlive {
public:
    bool operator() (const std::shared_ptr<TAnimal> &A1) const {
        return (A1->GetCurrentStateNo() != -1);
    }
};
/**
\brief
Function to compare to TAnimal's Current behavioural state
*/
/**
NB WhatState must be reimplemented by all descendents of TAnimal that use this functionality (not all do)
*/
class CompareState {
public:
  bool operator() ( const std::shared_ptr<TAnimal> &A1, const std::shared_ptr<TAnimal> &A2 ) const {
    return (A1->WhatState() < A2->WhatState());
  }
};


/**
\brief
Function to compare to TAnimal's m_CurrentStateNo
*/
class CompareStateR {
public:
	bool operator() (const std::shared_ptr<TAnimal> &A1, const std::shared_ptr<TAnimal> &A2) const {
		return (A1->GetCurrentStateNo() > A2->GetCurrentStateNo());
	}
};

/**
\brief
Function to compare to TAnimal's m_CurrentStateNo to -1
*/
class CompareStateDead {
public:
	bool operator() (TAnimal * A1) const {
		return (A1->GetCurrentStateNo() == -1);
	}
};






//---------------------------------------------------------------------------
/**
Constructor for the Population_Manager_Base class
*/
Population_Manager_Base::Population_Manager_Base(Landscape* L) {
    // Keep a pointer to the landscape this population is in
    m_TheLandscape = L;

    // Set the simulation bounds
    SimH = m_TheLandscape->SupplySimAreaHeight();
    SimW = m_TheLandscape->SupplySimAreaWidth();
    SimHH = m_TheLandscape->SupplySimAreaHeight() / 2;
    SimWH = m_TheLandscape->SupplySimAreaWidth() / 2;
}
//---------------------------------------------------------------------------
/**
Constructor for the Population_Manager_Base class
*/
Population_Manager_Base::~Population_Manager_Base() {
    ;
}

//-----------------------------------------------------------------------------
/**
open the AOR Grid probe
*/
void Population_Manager_Base::OpenTheAOROutputProbe(string a_AORFilename) {
    m_AORProbeFileName= a_AORFilename;
    m_AOR_Probe = new AOR_Probe_Base(this, m_TheLandscape, a_AORFilename);
}
//---------------------------------------------------------------------------
/**
Constructor for the Population_Manager class
*/
Population_Manager::Population_Manager( Landscape * L, int N ) :Population_Manager_Base(L) {
	


    // create N empty arrays

	TheArray= std::make_unique<vector < smartTListOfAnimals >>(N);

    BeforeStepActions.resize(N);
    for (int i = 0; i < N; i++) {
		// Set default BeforeStepActions
		BeforeStepActions[ i ] = 0;
		m_ListNames[ i ] = "Unknown";
		m_LiveArraySize.push_back( 0 );
	}
	// modify this if they need to in descendent classes
	StateNamesLength = 0; // initialise this variable.
#ifdef __LAMBDA_RECORD
	ofstream fout("LambdaGridOuput.txt", ios_base::out);  // open for writing
	fout.close();
	LamdaClear();
#endif
  if ( cfg_RipleysOutputMonthly_used.value() ) {
	OpenTheMonthlyRipleysOutputProbe();
  }

	// Ensure the GUI pointer is NULL in case we are not in GUI mode
#ifdef __ALMASS_VISUAL
	m_MainForm = NULL;
#endif
	m_SeasonNumber = 0;  // Ensure that we start in season number 0. Season number is incremented at day in year 183
}
//---------------------------------------------------------------------------

/**
Destructor for the Population_Manager class
*/
Population_Manager::~Population_Manager( void ) {
  // clean-up // no need to delete members of the array

  if ( cfg_RipleysOutput_used.value() ) {
    CloseTheRipleysOutputProbe();
  }
 if ( cfg_RipleysOutputMonthly_used.value() ) {
	CloseTheMonthlyRipleysOutputProbe();
  }
  if ( cfg_ReallyBigOutput_used.value() ) {
    CloseTheReallyBigOutputProbe();
  }
  if (cfg_AOROutput_used.value()) delete m_AOR_Probe;
}

void Population_Manager::SetNoProbesAndSpeciesSpecificFunctions(int a_pn)
{
    // This function relies on the fact that g_Species has been set already
   m_NoProbes = a_pn;
   m_TheLandscape->SetSpeciesFunctions(g_Species);
}
void Population_Manager::OpenTheAOROutputProbe(string a_AORFilename) {
    m_AORProbeFileName= a_AORFilename;
    m_AOR_Probe = new AOR_Probe(this, m_TheLandscape, a_AORFilename);
}
//-----------------------------------------------------------------------------

/**
Debug function used to log whatever is needed - this is just a place to write whatever is needed at the time - so contents vary
*/
void Population_Manager::LOG( const char* fname ) {
  FILE * PFile = fopen(fname, "w" );
  if (PFile) {
	  m_TheLandscape->Warn("PopulationManager::LOG - Could not open file ",fname);
	  exit(0);
  }
  AnimalPosition AP;
  for ( unsigned listindex = 0; listindex < TheArray->size(); listindex++ ) {
#ifndef __BCB__
    fprintf( PFile, "%s :\n", m_ListNames[ listindex ] );
#else
    fprintf( PFile, "%s :\n", m_ListNames[ listindex ].c_str() );
#endif
    for ( unsigned j = 0; j < (unsigned) m_LiveArraySize[listindex]; j++ ) {
      AP = TheArray->at(listindex).at(j)->SupplyPosition();
      fprintf( PFile, "%i %i %i\n", j, AP.m_x, AP.m_y );
    }
  }
  fclose( PFile );
}

//-----------------------------------------------------------------------------

/**
Can be used in descendent classes
*/
void Population_Manager::DoFirst() {
}

//---------------------------------------------------------------------------

/**
Can be used in descendent classes
*/
void Population_Manager::DoBefore() {
}

//---------------------------------------------------------------------------

/**
This is the main scheduling method for the population manager. \n
Note the structure of Shuffle_or_Sort(), DoFirst(), BeginStep, DoBefore(), Step looping until all are finished, DoAfter(), EndStep, DoLast(). \n
*/
void Population_Manager::Run( int NoTSteps ) {
  /**
  Can do multiple time-steps here inside one landscape time-step (a day). This is used in the roe deer model to provide 10 minute behavioural time-steps.
  */
  for ( int TSteps = 0; TSteps < NoTSteps; TSteps++ )
  {
	  unsigned size2;
	  auto size1 =  (unsigned) TheArray->size();
	  /**
	  * It is necessary to remove any dead animals before the timestep starts. It is possible that animals are killed after their population manager Run method has been executed.
	  * This is the case with geese and hunters. Checking death first prevents this becomming a problem.
	  */
	  for ( int listindex = 0; listindex < size1; listindex++ )
	  {
		  // Must check each object in the list for m_CurrentStateNo==-1
		  m_LiveArraySize[listindex] = PartitionLiveDead(listindex);
	  }
#ifdef __ALMASS_VISUAL
	  if (m_MainForm!=NULL)
	  {
		  int n = 0;
		  for ( unsigned listindex = 0; listindex < size1; listindex++ ) n += (int) m_LiveArraySize[ listindex ];
		  /** \todo consider making control of display locations a config variable */
		  //if (n>0) DisplayLocations();
	  }
#endif
	  // begin step actions ...
	  // set all stepdone to false.... is this really necessary??
	  for ( int listindex = 0; listindex < size1; listindex++ )
	  {
		  size2 = (unsigned) GetLiveArraySize(listindex);
		  for ( unsigned j = 0; j < size2; j++ )
		  {
              SupplyAnimalSmartPtr(listindex, j)->SetStepDone( false );
		  }
	  }

	  for ( unsigned listindex = 0; listindex < size1; listindex++ ) {
		  // Call the Shuffle/Sort procedures
		  Shuffle_or_Sort( listindex );
	  }
	  // Need to check if Ripleys Statistic needs to be saved
	  if (cfg_RipleysOutput_used.value()) {
		  int Year = m_TheLandscape->SupplyYearNumber();
		  if (Year >= cfg_RipleysOutputFirstYear.value()) {
			  if (Year % cfg_RipleysOutput_interval.value() == 0) {
				  int day = m_TheLandscape->SupplyDayInYear();
				  if (cfg_RipleysOutput_day.value() == day) {
					  // Do the Ripley Probe
					  TheRipleysOutputProbe(RipleysOutputPrb);
				  }
			  }
		  }
	  }
	  // Need to check if AOR output needs to be saved
	  if (cfg_AOROutput_used.value()) {
		  int Year = m_TheLandscape->SupplyYearNumber();
		  if (Year >= cfg_AOROutputFirstYear.value()) {
			  if (Year % cfg_AOROutput_interval.value() == 0) {
				  int day = m_TheLandscape->SupplyDayInYear();
				  if (cfg_AOROutput_day.value() == day) {
					  // Do the AOR Probe
					  TheAOROutputProbe();
				  }
			  }
		  }
	  }
	  // Need to check if Monthly Ripleys Statistic needs to be saved
    if ( cfg_RipleysOutputMonthly_used.value() ) {
		if (m_TheLandscape->SupplyDayInMonth()==1) {
			int Year = m_TheLandscape->SupplyYearNumber();
			if (Year>=cfg_RipleysOutputFirstYear.value()) {
				if ( Year % cfg_RipleysOutput_interval.value() == 0 ) {
					int month = m_TheLandscape->SupplyMonth();
					// Do the Ripley Probe
					switch (month) {
						case 1: TheRipleysOutputProbe( RipleysOutputPrb1 );
							break;
						case 2: TheRipleysOutputProbe( RipleysOutputPrb2 );
							break;
						case 3: TheRipleysOutputProbe( RipleysOutputPrb3 );
							break;
						case 4: TheRipleysOutputProbe( RipleysOutputPrb4 );
							break;
						case 5: TheRipleysOutputProbe( RipleysOutputPrb5 );
							break;
						case 6: TheRipleysOutputProbe( RipleysOutputPrb6 );
							break;
						case 7: TheRipleysOutputProbe( RipleysOutputPrb7 );
							break;
						case 8: TheRipleysOutputProbe( RipleysOutputPrb8 );
							break;
						case 9: TheRipleysOutputProbe( RipleysOutputPrb9 );
							break;
						case 10: TheRipleysOutputProbe( RipleysOutputPrb10 );
							break;
						case 11: TheRipleysOutputProbe( RipleysOutputPrb11 );
							break;
						case 12: TheRipleysOutputProbe( RipleysOutputPrb12 );
							break;
                        default:
                            g_msg->Warn("Population_Manager::Run ","Wrong month");
                            exit(-1);
					}
				}
			}
		}
	}
    // Need to check if Really Big Probe needs to be saved
    if ( cfg_ReallyBigOutput_used.value() ) {
      int Year = m_TheLandscape->SupplyYearNumber();
	  if (Year>=cfg_ReallyBigOutputFirstYear.value()) {
		  if ( Year % cfg_ReallyBigOutput_interval.value() == 0 ) {
			int day = m_TheLandscape->SupplyDayInYear();
			if (( cfg_ReallyBigOutput_day1.value() == day )|| ( cfg_ReallyBigOutput_day2.value() == day )|| ( cfg_ReallyBigOutput_day3.value() == day )|| ( cfg_ReallyBigOutput_day4.value() == day )||( cfg_ReallyBigOutput_day1.value() == -1 ))
			{
			  // Do the Ripley Probe
			  TheReallyBigOutputProbe();
			}
		  }
		}
	}
	int yr=m_TheLandscape->SupplyYearNumber();
    RunStepMethods();
  } // End of time step loop
}

void Population_Manager::RunStepMethods() {
    unsigned size2;
    auto size1 =  (unsigned) SupplyListIndexSize();
    DoFirst();
    // call the begin-step-method of all objects
    for ( int listindex = 0; listindex < size1; listindex++ ) {
        size2 = (unsigned)GetLiveArraySize( listindex );
        for (unsigned j = 0; j < size2; j++)
            SupplyAnimalSmartPtr(listindex, j)->BeginStep();
    }
    DoBefore();
    // call the step-method of all objects
    do {
        for ( int listindex = 0; listindex < size1; listindex++ ) {
            size2 = (unsigned)GetLiveArraySize( listindex );
            for (unsigned j = 0; j < size2; j++) {
                SupplyAnimalSmartPtr(listindex, j)->Step();
            }
        } // for listindex
    } while ( !StepFinished() );
    DoAfter();
    // call the end-step-method of all objects
    for ( int listindex = 0; listindex < size1; listindex++ ) {
        size2 = (unsigned)GetLiveArraySize( listindex );
        for (unsigned j = 0; j < size2; j++) {
            SupplyAnimalSmartPtr(listindex, j)->EndStep();
        }
    }
    // ----------------
    // end of this step actions
    // For each animal list

    DoLast();
}
//---------------------------------------------------------------------------

/** Returns true if and only if all objects have finished the current step */
bool Population_Manager::StepFinished( void ) {
  for ( unsigned listindex = 0; listindex < TheArray->size(); listindex++ ) {
	  for (unsigned j = 0; j < (unsigned)GetLiveArraySize( listindex ); j++) {
      if ( TheArray->at(listindex) .at(j)->GetStepDone() == false ) {
        return false;
      }
    }
  }
  return true;
}
//---------------------------------------------------------------------------

/**
Can be used in descendent classes
*/
void Population_Manager::DoAfter() {
  //TODO: Add your source code here
}

//---------------------------------------------------------------------------
/**
Collects some data to describe the number of animals in each state at the end of the day
*/
void Population_Manager::DoLast() {

/*
#ifdef __UNIX__
  for ( unsigned i = 0; i < 100; i++ ) {
    StateList.n[ i ] = 0;
  }
#else
  // Zero results
  for ( unsigned listindex = 0; listindex < m_ListNameLength; listindex++ ) {
    for ( unsigned i = 0; i < 100; i++ ) {
      Counts[ listindex ] [ i ] = 0;
    }
  }
#endif
  // How many animals in each state?
  for ( unsigned listindex = 0; listindex < TheArray.size(); listindex++ ) {
    unsigned size = (unsigned) TheArray[ listindex ].size();
    for ( unsigned j = 0; j < size; j++ ) {
#ifdef __UNIX__
      StateList.n[ TheArray[ listindex ] [ j ]->WhatState() ] ++;
#else
      Counts[ listindex ] [ TheArray[ listindex ] [ j ]->WhatState() ] ++;
#endif
    }
  }
*/
}
//---------------------------------------------------------------------------

void Population_Manager::DisplayLocations()
{
/**
* Used to update the graphics when control is not returned to the ALMaSS_GUI between timesteps.
*/
#ifdef __ALMASS_VISUAL
	m_MainForm->UpdateGraphics();
#endif
}
//---------------------------------------------------------------------------

/**
Default probe file input
*/

int Population_Manager_Base::ProbeFileInput( char * p_Filename, int p_ProbeNo ) {
  CfgInt cfg_ProbeMaxAreas("PROBE_MAX_AREAS", CFG_CUSTOM, 10, 1, 16);
  CfgInt cfg_ProbeTargetTypesNo("PROBE_TARGET_TYPES_NO", CFG_CUSTOM, 10, 1, 16);
  FILE * PFile;
  int data = 0;
  int data2 = 0;
  char S[ 255 ];
  PFile = fopen(p_Filename, "r" );
  if ( !PFile ) {
    m_TheLandscape->Warn( "Population Manager - cannot open Probe File ", p_Filename );
    exit( 0 );
  }
  fgets( S, 255, PFile ); // dummy line
  fgets( S, 255, PFile ); // dummy line
  fscanf( PFile, "%d\n", & data ); // Reporting interval
  TheProbe[ p_ProbeNo ]->m_ReportInterval = data;
  fgets( S, 255, PFile ); // dummy line
  fscanf( PFile, "%d\n", & data ); // Write to file
  if ( data == 0 ) TheProbe[ p_ProbeNo ]->m_FileRecord = false; else
    TheProbe[ p_ProbeNo ]->m_FileRecord = true;
  fgets( S, 255, PFile ); // dummy line
  for ( int i = 0; i < cfg_ProbeTargetTypesNo.value(); i++ ) {
    fscanf( PFile, "%d", & data );
    if ( data > 0 ) TheProbe[ p_ProbeNo ]->m_TargetTypes[ i ] = true; else
      TheProbe[ p_ProbeNo ]->m_TargetTypes[ i ] = false;
  }

  fgets( S, 255, PFile ); // dummy line
  fgets( S, 255, PFile ); // dummy line
  fscanf( PFile, "%d", & data );
  TheProbe[ p_ProbeNo ]->m_NoAreas = data;
  fgets( S, 255, PFile ); // dummy line
  fgets( S, 255, PFile ); // dummy line
  fscanf( PFile, "%d", & data2 ); // No References areas
  fgets( S, 255, PFile ); // dummy line
  fgets( S, 255, PFile ); // dummy line
  fscanf( PFile, "%d", & data ); // Type reference for probe
  if ( data == 1 ) TheProbe[ p_ProbeNo ]->m_NoEleTypes = data2; else
    TheProbe[ p_ProbeNo ]->m_NoEleTypes = 0;
  if ( data == 2 ) TheProbe[ p_ProbeNo ]->m_NoVegTypes = data2; else
    TheProbe[ p_ProbeNo ]->m_NoVegTypes = 0;
  if ( data == 3 ) TheProbe[ p_ProbeNo ]->m_NoFarms = data2; else
    TheProbe[ p_ProbeNo ]->m_NoFarms = 0;
  fgets( S, 255, PFile ); // dummy line
  fgets( S, 255, PFile ); // dummy line
  // Now read in the areas data
  for ( int i = 0; i < cfg_ProbeMaxAreas.value(); i++ ) {
    fscanf( PFile, "%d", & data );
    TheProbe[ p_ProbeNo ]->m_Rect[ i ].m_x1 = data;
    fscanf( PFile, "%d", & data );
    TheProbe[ p_ProbeNo ]->m_Rect[ i ].m_y1 = data;
    fscanf( PFile, "%d", & data );
    TheProbe[ p_ProbeNo ]->m_Rect[ i ].m_x2 = data;
    fscanf( PFile, "%d", & data );
    TheProbe[ p_ProbeNo ]->m_Rect[ i ].m_y2 = data;
  }
  fgets( S, 255, PFile ); // dummy line
  fgets( S, 255, PFile ); // dummy line
  if ( TheProbe[ p_ProbeNo ]->m_NoVegTypes > 0 ) {
    for ( int i = 0; i < 25; i++ ) {
      fscanf( PFile, "%d", & data );
      if ( data != 999 )
        TheProbe[ p_ProbeNo ]->m_RefVeg[ i ] = m_TheLandscape->TranslateVegTypes( data );
    }
  } else if ( TheProbe[ p_ProbeNo ]->m_NoFarms > 0 ) {
    for ( int i = 0; i < 25; i++ ) {
      fscanf( PFile, "%d", & data );
      if ( data != 999 )
        TheProbe[ p_ProbeNo ]->m_RefFarms[ i ] = data;
    }
  } else {
    for ( int i = 0; i < 25; i++ ) {
      fscanf( PFile, "%d", & data );
      if ( data != 999 ) TheProbe[ p_ProbeNo ]->m_RefEle[ i ] = m_TheLandscape->TranslateEleTypes( data );
    }
  }
    if ((TheProbe[ p_ProbeNo ]->m_NoAreas == 1)&&
            (TheProbe[ p_ProbeNo ]->m_NoVegTypes == 0)&&
            (TheProbe[ p_ProbeNo ]->m_NoEleTypes == 0)&&
            (TheProbe[ p_ProbeNo ]->m_NoFarms == 0)&&
            (TheProbe[ p_ProbeNo ]->m_Rect[ 0 ].m_x1==-1)&&
            (TheProbe[ p_ProbeNo ]->m_Rect[ 0 ].m_y1==-1)&&
            (TheProbe[ p_ProbeNo ]->m_Rect[ 0 ].m_x2==-1)&&
            (TheProbe[ p_ProbeNo ]->m_Rect[ 0 ].m_y2==-1)){
        // If there are no defined farms, no veg types and no landscape animals, and
        // if there is only one area [-1,-1,-1,-1] then it is a full landscape probe and
        // we can treat it in a special (faster) way
        TheProbe[ p_ProbeNo ]->m_FullLandscapeProbe = true;
    }
    else{
        TheProbe[ p_ProbeNo ]->m_FullLandscapeProbe = false;
    }
  fclose( PFile );
  return data2; // number of data references
}

//-----------------------------------------------------------------------------

/**
Special probe
*/
void Population_Manager::LamdaDumpOutput() {
   ofstream fout("LambdaGridOuput.txt", ios_base::app);  // open for writing
   for (int i=0; i<257; i++ ) {
	   for (int j=0; j<257; j++) {
		   fout << lamdagrid[0][i][j] << "\t" << lamdagrid[1][i][j] << endl;
	   }
   }
   //fout << "NEXT" << endl;
   fout.close();
}
//-----------------------------------------------------------------------------

/**
Special pesticide related probe. Overidden in descendent classes
*/
void Population_Manager::ImpactedProbe( ) {

}
//-----------------------------------------------------------------------------

/**
Default data probe. Rarely used in actuality but always available
*/
long Population_Manager::Probe( int ListIndex, probe_data * p_TheProbe ) {
	// Counts through the list and goes through each area to see if the animal
	// is standing there and if the farm, veg or element conditions are met
	AnimalPosition Sp;
	long NumberSk = 0;
	// Four possibilites
	// either NoVegTypes or NoElementTypes or NoFarmTypes is >0 or all==0
	if ( p_TheProbe->m_NoFarms != 0 ) {
		for (unsigned j = 0; j < (unsigned)GetLiveArraySize( ListIndex ); j++) {
			Sp = TheArray->at(ListIndex).at(j)->SupplyPosition();
			unsigned Farm = TheArray->at(ListIndex).at(j)->SupplyFarmOwnerRef();
			for ( unsigned i = 0; i < p_TheProbe->m_NoAreas; i++ ) {
				if ( ( Sp.m_x >= p_TheProbe->m_Rect[ i ].m_x1 ) && ( Sp.m_y >= p_TheProbe->m_Rect[ i ].m_y1 )
						&& ( Sp.m_x <= p_TheProbe->m_Rect[ i ].m_x2 ) && ( Sp.m_y <= p_TheProbe->m_Rect[ i ].m_y2 ) )
				for ( unsigned k = 0; k < p_TheProbe->m_NoFarms; k++ ) {
					if ( p_TheProbe->m_RefFarms[ k ] == Farm )
					NumberSk++; // it is in the square so increment number
				}

			}
    }
	} else if ( p_TheProbe->m_NoEleTypes != 0 ) {
		for (unsigned j = 0; j < (unsigned)GetLiveArraySize( ListIndex ); j++) {
      Sp = TheArray->at(ListIndex).at (j)->SupplyPosition();
      for ( unsigned i = 0; i < p_TheProbe->m_NoAreas; i++ ) {
        if ( ( Sp.m_x >= p_TheProbe->m_Rect[ i ].m_x1 ) && ( Sp.m_y >= p_TheProbe->m_Rect[ i ].m_y1 )
             && ( Sp.m_x <= p_TheProbe->m_Rect[ i ].m_x2 ) && ( Sp.m_y <= p_TheProbe->m_Rect[ i ].m_y2 ) )
               for ( unsigned k = 0; k < p_TheProbe->m_NoEleTypes; k++ ) {
                 if ( p_TheProbe->m_RefEle[ k ] == Sp.m_EleType )
                   NumberSk++; // it is in the square so increment number
               }
      }
    }
	} else {
    if ( p_TheProbe->m_NoVegTypes != 0 ) {
		for (unsigned j = 0; j < (unsigned)GetLiveArraySize( ListIndex ); j++) {
        Sp = TheArray->at(ListIndex).at(j)->SupplyPosition();

        for ( unsigned i = 0; i < p_TheProbe->m_NoAreas; i++ ) {
          if ( ( Sp.m_x >= p_TheProbe->m_Rect[ i ].m_x1 ) && ( Sp.m_y >= p_TheProbe->m_Rect[ i ].m_y1 )
               && ( Sp.m_x <= p_TheProbe->m_Rect[ i ].m_x2 ) && ( Sp.m_y <= p_TheProbe->m_Rect[ i ].m_y2 ) ) {
                 for ( unsigned k = 0; k < p_TheProbe->m_NoVegTypes; k++ ) {
                   if ( p_TheProbe->m_RefVeg[ k ] == Sp.m_VegType )
                     NumberSk++; // it is in the square so increment number
                 }
          }
        }
      }
    } else // both must be zero
    {
		long sz = (long)GetLiveArraySize( ListIndex );

		// It is worth checking whether we have a total dump of all individuals
		// if so don't bother with asking each where it is
		if (p_TheProbe->m_NoAreas==1) {
			if (((p_TheProbe->m_Rect[0].m_x1==0) && (p_TheProbe->m_Rect[0].m_y1==0) && (p_TheProbe->m_Rect[0].m_x2==(unsigned)SimW) &&
				(p_TheProbe->m_Rect[0].m_y2==(unsigned)SimH))||
                    (p_TheProbe->m_FullLandscapeProbe)) {
					return  sz;
			}
		}
		// Asking for a subset - need to test them all
		for ( unsigned j = 0; j < sz; j++ ) {
			Sp = TheArray->at(ListIndex).at(j)->SupplyPosition();
			for ( unsigned i = 0; i < p_TheProbe->m_NoAreas; i++ ) {
				if ( ( Sp.m_x >= p_TheProbe->m_Rect[ i ].m_x1 ) && ( Sp.m_y >= p_TheProbe->m_Rect[ i ].m_y1 )
					   && ( Sp.m_x <= p_TheProbe->m_Rect[ i ].m_x2 ) && ( Sp.m_y <= p_TheProbe->m_Rect[ i ].m_y2 ) )
						NumberSk++; // it is in the square so increment number
			}
		}
	}
	}
	return NumberSk;
}
//-----------------------------------------------------------------------------

/**
open the Ripley probe
*/
bool Population_Manager::OpenTheRipleysOutputProbe(string a_NWordFilename) {
	RipleysOutputPrb = fopen(cfg_RipleysOutput_filename.value(), "w");
	if (!RipleysOutputPrb) {
		g_msg->Warn(WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
			cfg_RipleysOutput_filename.value());
		exit(1);
	}
	return true;
}


//-----------------------------------------------------------------------------
/**
open 12 ripley output probles, one for each month
*/
bool Population_Manager::OpenTheMonthlyRipleysOutputProbe() {
  RipleysOutputPrb1 = fopen("RipleyOutput_Jan.txt", "w" );
  if ( !RipleysOutputPrb1 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb2 = fopen("RipleyOutput_Feb.txt", "w" );
  if ( !RipleysOutputPrb2 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb3 = fopen("RipleyOutput_Mar.txt", "w" );
  if ( !RipleysOutputPrb3 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb4 = fopen("RipleyOutput_Apr.txt", "w" );
  if ( !RipleysOutputPrb4 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb5 = fopen("RipleyOutput_May.txt", "w" );
  if ( !RipleysOutputPrb5 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb6 = fopen("RipleyOutput_Jun.txt", "w" );
  if ( !RipleysOutputPrb6 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb7 = fopen("RipleyOutput_Jul.txt", "w" );
  if ( !RipleysOutputPrb7 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb8 = fopen("RipleyOutput_Aug.txt", "w" );
  if ( !RipleysOutputPrb8 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb9 = fopen("RipleyOutput_Sep.txt", "w" );
  if ( !RipleysOutputPrb9 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb10 = fopen("RipleyOutput_Oct.txt", "w" );
  if ( !RipleysOutputPrb10 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb11 = fopen("RipleyOutput_Nov.txt", "w" );
  if ( !RipleysOutputPrb11 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  RipleysOutputPrb12 = fopen("RipleyOutput_Dec.txt", "w" );
  if ( !RipleysOutputPrb12 ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_RipleysOutput_filename.value() );
    exit( 1 );
  }
  return true;
}

//-----------------------------------------------------------------------------

/**
open the probe
*/
bool Population_Manager::OpenTheReallyBigProbe() {
  ReallyBigOutputPrb = fopen(cfg_ReallyBigOutput_filename.value(), "w" );
  if ( !ReallyBigOutputPrb ) {
    g_msg->Warn( WARN_FILE, "Population_Manager::OpenTheRipleysOutputProbe(): ""Unable to open probe file",
         cfg_ReallyBigOutput_filename.value() );
    exit( 1 );
  }
  return true;
}

//-----------------------------------------------------------------------------

/**
close the probe
*/
void Population_Manager::CloseTheRipleysOutputProbe() {
  if ( cfg_RipleysOutputMonthly_used.value() )
    fclose( RipleysOutputPrb );
  RipleysOutputPrb=NULL;
}

//-----------------------------------------------------------------------------

/**
close the monthly probes
*/
void Population_Manager::CloseTheMonthlyRipleysOutputProbe() {
  fclose( RipleysOutputPrb1 );
  fclose( RipleysOutputPrb2 );
  fclose( RipleysOutputPrb3 );
  fclose( RipleysOutputPrb4 );
  fclose( RipleysOutputPrb5 );
  fclose( RipleysOutputPrb6 );
  fclose( RipleysOutputPrb7 );
  fclose( RipleysOutputPrb8 );
  fclose( RipleysOutputPrb9 );
  fclose( RipleysOutputPrb10 );
  fclose( RipleysOutputPrb11 );
  fclose( RipleysOutputPrb12 );
}

//-----------------------------------------------------------------------------
/**
close the probe
*/
void Population_Manager::CloseTheReallyBigOutputProbe() {
  if ( ReallyBigOutputPrb != 0 )
    fclose( ReallyBigOutputPrb );
  ReallyBigOutputPrb=0;
}

//-----------------------------------------------------------------------------

/** This method must be overridden in descendent classes */
void Population_Manager::TheReallyBigOutputProbe() {
}
//-----------------------------------------------------------------------------

/**  This method must be overridden in descendent classes */
void Population_Manager::TheRipleysOutputProbe(FILE* /* a_Prob */) {
}

//---------------------------------------------------------------------------

/**  This method must be overridden in descendent classes */
void Population_Manager::TheAOROutputProbe() {
}

//---------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//                    TheArray handling functions
//----------------------
/**
Gets a random individual that is not a_me
*/
TAnimal* Population_Manager::FindIndividual(unsigned Type, TAnimal* a_me) {
    int i = int (GetLiveArraySize(Type) * g_rand_uni());
    if (TheArray->at(Type).at(i).get() != a_me) return TheArray->at(Type).at(i).get();
    else return nullptr;
}

/**
Finds the closest individual to an x,y point that is not a_me
*/
TAnimal * Population_Manager::FindClosest( int x, int y, unsigned Type, TAnimal* a_me ) {
  int distance = 100000000;
  TAnimal * TA = nullptr;
  int dx, dy, d;
  for (unsigned j = 0; j < (unsigned)GetLiveArraySize( Type ); j++) {
    dx = TheArray->at(Type).at(j) ->Supply_m_Location_x();
    dy = TheArray->at(Type).at(j)->Supply_m_Location_y();
    dx = ( dx - x );
    dx *= dx;
    dy = ( dy - y );

    dy *= dy;
    d = dx + dy;
    if ( d < distance ) {
        if (TheArray->at(Type).at(j).get() != a_me)
        {
            if (TheArray->at(Type).at(j)->GetCurrentStateNo() != -1)
            {
                TA = TheArray->at(Type).at(j).get();
                distance = d;
            }
        }
    }
  }
  return TA;
}

//------------------------------------------------------------------------------
/**
Sort TheArray w.r.t. the m_Location_x attribute
*/
void Population_Manager::EmptyTheArray()
{
    for ( unsigned i = 0; i < TheArray->size(); i++ )
	{
        // if objects in the list need to be deleted:

        // empty the array
        TheArray->at(i).clear();
    }
}

/**
Sort TheArray w.r.t. the m_Location_x attribute
*/
void Population_Manager::SortX( unsigned Type ) {
    auto nth = (*TheArray)[Type].begin() + m_LiveArraySize[Type];
    sort( (TheArray)->at(Type).begin(), nth, CompareX() );
}

//-----------------------------------------------------------------------------

/**
Sort TheArray w.r.t. the m_Location_y attribute
*/
void Population_Manager::SortY( unsigned Type ) {
    auto nth = (TheArray)->at(Type).begin() + m_LiveArraySize[Type];
    sort((TheArray)->at(Type).begin(), nth, CompareY());
}


//-----------------------------------------------------------------------------

/**
Sort TheArray w.r.t. the current state attribute
*/
void Population_Manager::SortState( unsigned Type ) {
  auto nth = (TheArray)->at(Type).begin() + m_LiveArraySize[Type];
  sort( (TheArray)->at(Type).begin(), nth, CompareState() );
}

//-----------------------------------------------------------------------------

/**
Sort TheArray w.r.t. the current state attribute in reverse order
*/
void Population_Manager::SortStateR(unsigned Type) {
    auto nth = (TheArray)->at(Type).begin() + m_LiveArraySize[Type];
    sort( (TheArray)->at(Type).begin(), nth,CompareStateR());

}

/**
Sort TheArray w.r.t. the current state attribute in reverse order
*/
unsigned Population_Manager::PartitionLiveDead(unsigned Type) {
    auto it = partition((*TheArray)[Type].begin(), (*TheArray)[Type].begin() + m_LiveArraySize[Type], CompareStateAlive());
    // it now points at the first dead element
    // the number of live ones before it is it-(*TheArray_new)[Type].begin(). This is the new vector size for live animals.
    return unsigned(it - (*TheArray)[Type].begin());
}

//-----------------------------------------------------------------------------

/**
Sort TheArray w.r.t. the m_Location_x attribute, and make an indexing array
*/
void Population_Manager::SortXIndex( unsigned Type ) {
  SortX(Type);
  unsigned s = (unsigned)GetLiveArraySize( Type );
  // Now make the index array to X;
  int counter = 0;
  int x;
  // for each individual
  for ( unsigned i = 0; i < s; i++ ) {
    // Get Next co-ordiated
    x = TheArray->at(Type).at(i)->Supply_m_Location_x();
    // fill gaps up to x
    while ( counter < x ) IndexArrayX[ Type ] [ counter++ ] = -1;
    if ( x == counter ) {
      IndexArrayX[ Type ] [ counter++ ] = i;
    }
  }
  // Fill up the rest of the array with -1;
  for ( int c = counter; c < 10000; c++ ) {
    IndexArrayX[ Type ] [ c ] = -1;
  }
}

//-----------------------------------------------------------------------------

/**
	Run once through the list swapping randomly chosen elements
*/
void Population_Manager::Shuffle( unsigned Type ) {
    std::shuffle((*TheArray)[Type].begin(), (*TheArray)[Type].begin() + m_LiveArraySize[Type], g_std_rand_engine);
}

void Population_Manager::Debug_Shuffle( unsigned Type ) {
    auto s = (int) GetLiveArraySize( (int)Type );
    //std::random_shuffle((*TheArray_new)[Type].begin(), (*TheArray_new)[Type].begin() + m_LiveArraySize[Type]);

    for ( unsigned i = 0; i < s; i++ ) {
        std::shared_ptr<TAnimal> temp;
        unsigned a = random( s );
        unsigned b = random( s );
        temp = std::move((*TheArray)[ Type ] [ a ]);
        (*TheArray)[ Type ] [ a ] = std::move((*TheArray)[ Type ] [ b ]);
        (*TheArray)[ Type ] [ b ] = std::move(temp);
    }

}


//-----------------------------------------------------------------------------

/**
This method is used to determine whether the array of animals should be shuffled or sorted. \n
To do nothing ensure that the BeforeStepActions[] is set appropriately // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = sortXIndex, 4 = do nothing
*/
void Population_Manager::Shuffle_or_Sort( unsigned Type ) {
  switch ( BeforeStepActions[ Type ] ) {
    case 0:
      Shuffle( Type );
      break;
    case 1:
      SortX( Type );
      break;
    case 2:
      SortY( Type );
      break;
    case 3:
      SortXIndex( Type );
      break;
	case 4: // Do nothing
	  break;
	case 5: 
	  if (g_rand_uni() < double (1./500)) Shuffle( Type );
	  break;
    default:
        m_TheLandscape->Warn( "Population_Manager::Shuffle_or_Sort", "- BeforeStepAction Unknown");
      exit( 1 );
  }
}

//-----------------------------------------------------------------------------
/**
Is it the first day of the month?
*/
bool Population_Manager::BeginningOfMonth() {
  if ( m_TheLandscape->SupplyDayInMonth() == cfg_DayInMonth.value() ) return true;
  return false;
}

//------------------------------------------------------------------------------

void Population_Manager::Catastrophe() {
    /**
	This method MUST be overidden in descendent classes if this functionality is
	does not match with the animals requirements
	*/
	return;
}
//---------------------------------------------------------------------------

/**
This method handles species specific outputs. This is one place to do it. More commonly this is done in descendent classes
*/
char* Population_Manager::SpeciesSpecificReporting(int a_species, int a_time) {
  strcpy(g_str,"");
  if (a_species == -1)
  {
      return g_str;
  }
// Vole Model
  else if ( a_species == 1 ) {
/*
// The next lines are obselete code from genetic simulations
float MeanNoAlleles;
    float MeanHO;
    float MeanHE;
    unsigned size;

	if ( m_time % ( 365 * 24 * 60 ) == 0 ) // End of year
    {

      // empty the GenStruc data
      for ( int ProbeNo = 0; ProbeNo < ( int )m_NoProbes; ProbeNo++ ) {
        fprintf( m_GeneticsFile, "%d ", m_time );
        int TotalSize = 0;
        m_AManager->AlFreq->Flush();
        for ( unsigned listindex = 0; listindex <= 1; listindex++ ) {
          size = 0;
          m_AManager->TheGeneticProbe( listindex, ProbeNo, size );
          TotalSize += size;
        }
        if ( TotalSize > 0 ) {
          m_AManager->AlFreq->CalcAF();
          m_AManager->AlFreq->CalcNoAlleles();
          m_AManager->AlFreq->CalcHE();
          m_AManager->AlFreq->CalcHO( TotalSize );
        }
        // Get the means
        // For N alleles
        int NA = 0;
        float NHO = 0;
        float NHE = 0;
        for ( int l = 0; l < 16; l++ ) {
          NA += m_AManager->AlFreq->SupplyNoAlleles( l );
        }
        MeanNoAlleles = ( float )NA / 16.0;
        for ( int l = 0; l < 16; l++ ) {
          NHO += m_AManager->AlFreq->SupplyHO( l );
        }
        MeanHO = NHO / 16.0;
        for ( int l = 0; l < 16; l++ ) {
          NHE += m_AManager->AlFreq->SupplyHE( l );
        }
        MeanHE = NHE / 16.0;

        // Do some output
        // Output is to two files, the genetics file and the AllelerequencyFile
        fprintf( m_GeneticsFile, "%2.2f ", MeanNoAlleles );
        fprintf( m_GeneticsFile, "%1.3f ", MeanHO );
        fprintf( m_GeneticsFile, "%1.3f ", MeanHE );
        for ( int l = 0; l < 16; l++ ) {
          fprintf( m_GeneticsFile, "%04i ", m_AManager->AlFreq->SupplyNoAlleles( l ) );
        }
        for ( int l = 0; l < 16; l++ ) {
          fprintf( m_GeneticsFile, "%1.3f ", m_AManager->AlFreq->SupplyHO( l ) );
        }
        for ( int l = 0; l < 16; l++ ) {
          fprintf( m_GeneticsFile, "%1.3f ", m_AManager->AlFreq->SupplyHE( l ) );
        }
        fprintf( m_GeneticsFile, "\n" );
        fprintf( m_AlleleFreqsFile, "%d ", m_time );
        for ( int l = 0; l < 16; l++ ) {
          for ( int al = 0; al < 4; al++ ) {
            fprintf( m_AlleleFreqsFile, "%1.3f ", m_AManager->AlFreq->SupplyAF( l, al ) );
          }
        }
        fprintf( m_AlleleFreqsFile, "\n" );
      }
      // make sure progress reaches the disk
      fflush( m_GeneticsFile );
      fflush( m_AlleleFreqsFile );
    }

    if ( cfg_UseEasyPop.value()) {
      int targettime=a_time % ( 365 * 100 );
      if ( (  targettime == 59 ) ||(targettime == 181)||(targettime ==304)||(targettime ==59+365)||(targettime ==181+365)||(targettime ==304+365)) {
          for ( unsigned listindex = 0; listindex <= 1; listindex++ ) {
            GeneticsResultsOutput( m_EasyPopRes, listindex);
        }
        fflush( m_EasyPopRes );
      }
    }
*/
    /* Three times a year reporting
	if ( m_time % 365 == 60 ) ProbeReportNow( m_time );
    if ( m_time % 365 == 152 ) ProbeReportNow( m_time );
    if ( m_time % 365 == 273 ) ProbeReportNow( m_time ); */
    ProbeReport( a_time );
#ifdef __SpecificPesticideEffectsVinclozolinLike__
	ImpactProbeReport( a_time );
#endif
#ifdef __WithinOrchardPesticideSim__
	ImpactProbeReport( a_time );
#endif

  }
  // Skylark Model
  else if ( a_species == 0 ) {
    int No;
	int a_day = a_time % 365;
    if ( a_time % 365 == 364 )
	{
      //Write the Breeding Attempts Probe
      int BreedingFemales, YoungOfTheYear, TotalPop, TotalFemales, TotalMales, BreedingAttempts;
      No = TheBreedingSuccessProbe( BreedingFemales, YoungOfTheYear, TotalPop, TotalFemales, TotalMales, BreedingAttempts );
      float bs = 0;
      if ( BreedingFemales > 0 ) {
        bs = No / ( float )BreedingFemales; //bs = successful breeding attempt per attempting to breed female
      }
      BreedingSuccessProbeOutput( bs, BreedingFemales, YoungOfTheYear, TotalPop,
           TotalFemales, TotalMales, a_time, BreedingAttempts );
    }
    if ( a_day == 152 ) {
      // Need to fill in the landscape from 1st June, it will change before
      // the count of fledgelings is needed
      m_TheLandscape->FillVegAreaData();
    }
    if ( a_day == 197 ) {
      for ( int ProbeNo = 0; ProbeNo < m_NoProbes; ProbeNo++ ) {
        No = TheFledgelingProbe();
        // Do some output
        FledgelingProbeOutput( No, a_time );
      }
    }
#ifdef __SKPOM
    // This is clunky but for purposes of validation we want probes throughout the year
	// First get the breeding pairs data.
	BreedingPairsOutput(a_day);
#else
	ProbeReport( a_time );
#endif
  }
  else return ProbeReport( a_time );
  return g_str;
}
//-----------------------------------------------------------------------------

unsigned int Population_Manager::FarmAnimalCensus(unsigned int a_farm, unsigned int a_typeofanimal)
{
	unsigned int No = 0;
	unsigned sz = (unsigned)GetLiveArraySize( a_typeofanimal );
	for ( unsigned j = 0; j < sz; j++ ) 
	{
		if (a_farm == TheArray->at(a_typeofanimal).at(j)->SupplyFarmOwnerRef()) No++;
	}
	return No;
}
//-----------------------------------------------------------------------------
	
char* Population_Manager::ProbeReport( int Time ) {

  int No;
  char str[100]; // 100 out to be enough!!
  strcpy(g_str,"");
  for ( int ProbeNo = 0; ProbeNo < m_NoProbes; ProbeNo++ ) {
    No = 0;
    // See if we need to record/update this one
    // if time/months/years ==0 or every time
    if ( ( TheProbe[ ProbeNo ]->m_ReportInterval == 3 )
         || ( ( TheProbe[ ProbeNo ]->m_ReportInterval == 2 ) && ( BeginningOfMonth() ) )
         || ( ( TheProbe[ ProbeNo ]->m_ReportInterval == 1 ) && ( Time % 365 == 0 ) ) ) {
           // Goes through each area and sends a value to OutputForm
           unsigned Index = SupplyListIndexSize();
           for ( unsigned listindex = 0; listindex < Index; listindex++ ) {
             if ( TheProbe[ ProbeNo ]->m_TargetTypes[ listindex ] )
               No += (int) Probe( listindex, TheProbe[ ProbeNo ] );
           }
           TheProbe[ ProbeNo ]->FileOutput( No, Time, ProbeNo );
			sprintf(str," %d ", No );
			strcat(g_str,str);
    }
  }
  return g_str;
}
//-----------------------------------------------------------------------------

char* Population_Manager::ProbeReportTimed( int Time) {
	int No;
	char str[100]; // 100 ought to be enough!!
	strcpy(g_str,"");
	for ( int ProbeNo = 0; ProbeNo < m_NoProbes; ProbeNo++ ) {
	No = 0;
    unsigned Index = SupplyListIndexSize();
    for ( unsigned listindex = 0; listindex < Index; listindex++ ) {
		if ( TheProbe[ ProbeNo ]->m_TargetTypes[ listindex ] ) No += (int) Probe( listindex, TheProbe[ ProbeNo ] );
	}
	TheProbe[ ProbeNo ]->FileOutput( No, Time, ProbeNo );
	sprintf(str," %d ", No );
	strcat(g_str,str);
  }
  return g_str;
}
//-----------------------------------------------------------------------------

/**
Special probe
*/
void Population_Manager::ImpactProbeReport( int a_Time ) {

  for ( int ProbeNo = 0; ProbeNo < 1; ProbeNo++ ) {
    // See if we need to record/update this one
    // if time/months/years ==0 or every time
    if ( ( TheProbe[ ProbeNo ]->m_ReportInterval == 3 )
         || ( ( TheProbe[ ProbeNo ]->m_ReportInterval == 2 ) && ( BeginningOfMonth() ) )
		 || ( ( TheProbe[ ProbeNo ]->m_ReportInterval == 1 ) && ( a_Time % 365 == 0 ) ) ) {
             ImpactedProbe();
    }
  }
}

//-----------------------------------------------------------------------------

//-----------------End of TheArray handling functions

//-----------------------------------------------------------------------------
//------------------------------------------------------------------------------


/**
Provides the location of an animal in terms of x,y,elementtype and vegetation type
*/
AnimalPosition TAnimal::SupplyPosition() {
  AnimalPosition SkP;
  SkP.m_x = m_Location_x;
  SkP.m_y = m_Location_y;
  SkP.m_EleType = m_OurLandscape->SupplyElementType( m_Location_x, m_Location_y );
  SkP.m_VegType = m_OurLandscape->SupplyVegType( m_Location_x, m_Location_y );
  return SkP;
}

//-----------------------------------------------------------------------------

/**
Provides the farmer reference for the location of a TAnimal
*/
unsigned TAnimal::SupplyFarmOwnerRef() {
  return m_OurLandscape->SupplyFarmOwner( m_Location_x, m_Location_y );
}

//-----------------------------------------------------------------------------
/**
TAnimal Constructor
*/
TAnimal::TAnimal( int x, int y, Landscape * L ) {
  m_OurLandscape = L;
  m_Location_x = x;
  m_Location_y = y;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

/**
TALMaSSObject Constructor
*/
TALMaSSObject::TALMaSSObject() {
#ifdef __CJTDebug_5
  AmAlive = 0xDEADC0DE;
#endif
  m_StepDone = false;
  m_CurrentStateNo = 0;
}

//-----------------------------------------------------------------------------


/**
TALMaSSObject Destructor
*/
TALMaSSObject::~TALMaSSObject() {
#ifdef __CJTDebug_5
  AmAlive = 0;
#endif
}

//-----------------------------------------------------------------------------

/**
Checks to see if there has been a management event at the TAnimals' x,y location. If so calls an event handler to handle the management event.
*/
void TAnimal::CheckManagement( void ) {
  FarmToDo event;
  int i = 0;
  while ( ( event = ( FarmToDo )m_OurLandscape->SupplyLastTreatment( m_Location_x, m_Location_y, & i ) ) != sleep_all_day ) {
    if ( OnFarmEvent( event ) ) break;
  }
}

//-----------------------------------------------------------------------------

/**
Checks to see if there has been a management event at the x,y location. If so calls an event handler to handle the management event.
*/
void TAnimal::CheckManagementXY( int x, int y ) {
  FarmToDo event;
  int i = 0;
  while ( ( event = ( FarmToDo )m_OurLandscape->SupplyLastTreatment( x, y, & i ) ) != sleep_all_day ) {
    OnFarmEvent( event );
  }
}

//-----------------------------------------------------------------------------

void TALMaSSObject::OnArrayBoundsError() {
  exit( 1 );
}

//-----------------------------------------------------------------------------

#ifdef __CJTDebug_5
void TALMaSSObject::DEADCODEError() {
  exit( 0 );
}

#endif
//-----------------------------------------------------------------------------
//           probe_data
//-----------------------------------------------------------------------------

/**
Basic output function of the default probe data file. \n
This just counts numbers in specified areas
*/
void probe_data::FileOutput( int No, int time, int ProbeNo ) {
  if ( m_FileRecord ) {

    if ( ProbeNo == 0 ) {
      // First probe so write the time and a new line
      (*m_MyFile) << endl;
      (*m_MyFile) << time << '\t' << No;
    } else
      (*m_MyFile) << '\t' << No ;
  }
  (*m_MyFile).flush();
}

//-----------------------------------------------------------------------------

void probe_data::FileAppendOutput( int No, int time ) {
  m_MyFile->open(m_MyFileName,ios::app);
  if ( !m_MyFile->is_open() ) {
    g_msg->Warn( (MapErrorState)0,"Cannot open file for append: ", m_MyFileName );
    exit( 0 );
  }
  if ( m_FileRecord ) {
   (*m_MyFile)<< time << '\t' << No << endl;
  }
  m_MyFile->close();
}

//-----------------------------------------------------------------------------

/**
Constructor for probe_data
*/
probe_data::probe_data() {
  m_Time = 0;
  m_FileRecord = false;
  m_MyFile = NULL;
}

//-----------------------------------------------------------------------------

/**
Opens the default probe data output file
*/
ofstream * probe_data::OpenFile( string Nme ) {
  m_MyFile = new ofstream( Nme );
  if ( !m_MyFile->is_open() ) {
    g_msg->Warn( (MapErrorState)0,"probe_data::OpenFile - Cannot open file: ", Nme );
    exit( 0 );
  }
  return m_MyFile;
}
//-----------------------------------------------------------------------------

/**
Sets the filename for the default probe data output
*/
void probe_data::SetFile( ofstream * F ) {
  m_MyFile = F;
}

//-----------------------------------------------------------------------------

/**
Destructor for probe_data
*/
probe_data::~probe_data() {
}
//-----------------------------------------------------------------------------


bool Population_Manager::CheckXY(int l, int i)
{
	if (TheArray->at(l).at(i)->Supply_m_Location_x() > 10000) return true;
	if (TheArray->at(l).at(i)->Supply_m_Location_y() > 10000) return true;
	if (TheArray->at(l).at(i)->Supply_m_Location_x() < 0) return true;
	if (TheArray->at(l).at(i)->Supply_m_Location_y() < 0) return true;
	return false;
}
//-----------------------------------------------------------------------------
int PopulationManagerList::SupplyActivePopulationsCount(){
    return std::accumulate(m_populationarray.begin(), m_populationarray.end(), 0);
}

