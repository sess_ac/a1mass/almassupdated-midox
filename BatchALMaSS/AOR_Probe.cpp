#include <iostream>
#include <fstream>
#include <string.h>
#include "../Landscape/ls.h"
#include "PopulationManager.h"
#include "../GooseManagement/Goose_Base.h"
#include "AOR_Probe.h"
#include "../Beetles/Beetle_BaseClasses.h"
#include <numeric>
using namespace std;

AOR_Probe_Base::AOR_Probe_Base(Population_Manager_Base* a_owner, Landscape* a_TheLandscape, string a_filename, bool (*filter_fun)(int, int, Landscape *), bool IsExclusive)
{
    m_owner = a_owner;
    m_IsExclusive = IsExclusive;
    m_TheLandscape = a_TheLandscape;
    if (a_filename == "") a_filename = "NWordOutputPrb.txt"; // this is for backwards compatability - this output used to be called NWord
    m_ProbeFile.open(a_filename, ios::out);
    if (!m_ProbeFile) {
        g_msg->Warn(WARN_FILE, "Population_Manager::AOR_Probe: ""Unable to open NWord probe file", "");
        exit(1);
    }
    m_ProbeFile << "Year" << '\t' << "Day" << '\t' << "Total_no" << '\t' << "Cells50" << '\t' << "Occupied50" << '\t' << "Cells100" << '\t' << "Occupied100" << '\t' << "Cells200" << '\t' << "Occupied200" << '\t' << "Cells400" << '\t' << "Occupied400" << endl;

    m_gridcountsize[0] = 50;
    m_gridcountsize[1] = 100;
    m_gridcountsize[2] = 200;
    m_gridcountsize[3] = 400;
    for (int g = 0; g < 4; g++) {
        m_gridcountwidth[g] = m_TheLandscape->SupplySimAreaWidth() / m_gridcountsize[g];
        m_gridcountheight[g] = m_TheLandscape->SupplySimAreaHeight() / m_gridcountsize[g];
        m_gridwidthexp[g] =   m_gridcountwidth[g] * m_gridcountsize[g];
        m_gridheightexp[g] =   m_gridcountheight[g] * m_gridcountsize[g];
        m_totalcells[g] = m_gridcountwidth[g] * m_gridcountheight[g];
        m_gridcount[g].resize(m_totalcells[g]);
        m_gridexclusive[g].resize(m_totalcells[g]);
        m_exclusivecells[g]=m_totalcells[g];
        for (int e=0; e<m_totalcells[g]; e++){
            m_gridexclusive[g].at(e) = true;
        }
    }

    m_filter_fun=filter_fun;
    //CfgBool cfg_AOR_ExclusiveCells("EXCLUSIVEAOR", CFG_CUSTOM, false);
    if (m_IsExclusive){
        for (int grid = 0; grid < 4; grid++) {
            for (int xx = 0; xx < m_gridwidthexp[grid]; xx++) {
                for (int yy = 0; yy < m_gridheightexp[grid]; yy++) {
                    int gx = xx / m_gridcountsize[grid];
                    int gy = yy / m_gridcountsize[grid];
                    int pos = gx + gy*m_gridcountwidth[grid];
                    m_gridexclusive[grid].at(pos)=m_filter_fun(xx,yy, m_TheLandscape) &
                            m_gridexclusive[grid].at(pos);
                }
            }

            m_exclusivecells[grid]=std::accumulate(m_gridexclusive[grid].begin(), m_gridexclusive[grid].end(), 0);
        }
    }

}
void AOR_Probe_Base::WriteData()
{
    /** A common output stub for the AOR output probe. Specialist counting for each species
    occurs as part of the AOR grid probe, then this method deals with the final output. */
    int Counted[4];
    int OccupiedCells[4];
    for (int gsz = 0; gsz < 4; gsz++) {
        Counted[gsz] = 0;
        OccupiedCells[gsz] = 0;
        for (int i = 0; i < m_gridcountwidth[gsz]; i++) {
            for (int j = 0; j < m_gridcountheight[gsz]; j++) {
                int res = m_gridcount[gsz][i + j*m_gridcountwidth[gsz]];
                Counted[gsz] += res;
                if (res > 0) OccupiedCells[gsz]++;
            }
        }
    }

    m_ProbeFile << m_TheLandscape->SupplyYearNumber() << '\t' << (int)m_TheLandscape->SupplyDayInYear() << '\t' << Counted[0] << '\t';
    for (int c = 0; c < 3; c++) {
        m_ProbeFile << m_exclusivecells[c] << '\t' << OccupiedCells[c] << '\t';
    }
    m_ProbeFile << m_exclusivecells[3] << '\t' << OccupiedCells[3] << endl;
}
void AOR_Probe_Base::DoProbe(int a_lifestage){
    for (int grid = 0; grid < 4; grid++) {
        for (int i = 0; i < m_totalcells[grid]; i++) m_gridcount[grid][i] = 0;
    }
    WriteData();
}
AOR_Probe::AOR_Probe(Population_Manager* a_owner, Landscape* a_TheLandscape, string a_filename, bool (*a_filter_fun)(int, int, Landscape *), bool IsExclusive)
: AOR_Probe_Base( a_owner, a_TheLandscape, a_filename, a_filter_fun,IsExclusive)
{
;
}




void AOR_Probe::DoProbe(int a_lifestage) {
	/** Counts all a_lifestage animals in each grid of each size */
	unsigned int total = (unsigned)m_owner->GetLiveArraySize(a_lifestage);
	// Empty old data
	for (int grid = 0; grid < 4; grid++) {
		for (int i = 0; i < m_totalcells[grid]; i++) m_gridcount[grid][i] = 0;
	}
	// For each animal get the location and place it in each of the (4) grids
	for (unsigned j = 0; j < total; j++)      //adult females
	{
		APoint pt = dynamic_cast<Population_Manager*>(m_owner)->SupplyAnimalPtr(a_lifestage, j)->SupplyPoint();
        int x = pt.m_x;
        int y = pt.m_y;
		for (int grid = 0; grid < 4; grid++) {
            if((x<m_gridwidthexp[grid]&&y<m_gridheightexp[grid])&&
                    (m_filter_fun(x,y, m_TheLandscape))){
                int gx = x / m_gridcountsize[grid];
                int gy = y / m_gridcountsize[grid];
                int pos = gx + gy*m_gridcountwidth[grid];
                if (pos<m_totalcells[grid] // todo: just a safety net, use std::array with limit check for m_gridcount[grid] instead
                && m_gridexclusive[grid].at(pos)){
                    m_gridcount[grid][pos]++;
                }
            }


		}
	}
	WriteData();
}
//-----------------------------------------------------------------------------

AOR_Probe_Goose::AOR_Probe_Goose(Population_Manager * a_owner, Landscape * a_TheLandscape, string a_filename, bool (*a_filter_fun)(int, int, Landscape *), bool IsExclusive)
: AOR_Probe( a_owner, a_TheLandscape, a_filename, a_filter_fun, IsExclusive)
{
	;
}

void AOR_Probe_Goose::DoProbe(int a_goosespecies)
{
	Goose_Base* aGoose;
	for (int grid = 0; grid < 4; grid++) {
		// The line below may be the fastest way to fill a vector of ints with '0', but the one following is the safe way
		// memset(&m_gridcount[grid][0], 0, m_gridcount[grid].size() * sizeof m_gridcount[grid][0]);
		fill(m_gridcount[grid].begin(), m_gridcount[grid].end(), 0);
	}
	for (int groups = 0; groups < 2; groups++)
	{
		int gooseindex = a_goosespecies * 2 + groups;
		unsigned int total = (unsigned)m_owner->GetLiveArraySize(gooseindex);
		for (int grid = 0; grid < 4; grid++) 
		{
			for (unsigned j = 0; j < total; j++)
			{
				aGoose = dynamic_cast<Goose_Base*>(dynamic_cast<Population_Manager*>(m_owner)->SupplyAnimalPtr(gooseindex, j));
				if (aGoose->GetCurrentStateNo() != -1) {
					APoint pt = aGoose->SupplyPoint();
                    int x = pt.m_x;
                    int y = pt.m_y;
                    if(x<m_gridwidthexp[grid]&&y<m_gridheightexp[grid] &&
                    (m_filter_fun(x,y, m_TheLandscape))) {
                        int gx = x / m_gridcountsize[grid];
                        int gy = y / m_gridcountsize[grid];
                        int pos = gx + gy * m_gridcountwidth[grid];
                        if (pos < m_totalcells[grid]// todo: just a safety net, use std::array with limit check for m_gridcount[grid] instead
                            && m_gridexclusive[grid].at(pos)) {
                            m_gridcount[grid][gx + gy * m_gridcountwidth[grid]] += aGoose->GetGroupsize();
                        }
                    }
				}
			}
		}
	}
	WriteData();
}

//-----------------------------------------------------------------------------

AOR_Probe_Beetle::AOR_Probe_Beetle(Population_Manager * a_owner, Landscape * a_TheLandscape, string a_filename, bool (*a_filter_fun)(int, int, Landscape *), bool IsExclusive)
: AOR_Probe( a_owner, a_TheLandscape, a_filename, a_filter_fun,IsExclusive)
{
    ;
}

void AOR_Probe_Beetle::DoProbe(int a_lifestage) {
    /** Counts all a_lifestage animals in each grid of each size */
    /** If this is adults we count, we should use the AdultPosMap */

    if (static_cast<Beetle_Population_Manager*>(m_owner)->beetleconstantslist->p_cfg_OldProbeCount->value()){
        /** Old AOR style, in case we want to ask each individual and not rely on
         * map and SupplyTilePopulation(x, y)
        * */

        AOR_Probe::DoProbe(a_lifestage);
    }
    else{
        // Empty old data
        for (int grid = 0; grid < 4; grid++) {
            for (int i = 0; i < m_totalcells[grid]; i++) m_gridcount[grid][i] = 0;
        }
        // For each animal get the location and place it in each of the (4) grids
        for (int grid = 0; grid < 4; grid++) {
            for (int xx=0; xx<m_gridwidthexp[grid]; xx++){
                for (int yy=0; yy<m_gridheightexp[grid]; yy++){
                    unsigned tile_population = static_cast<Beetle_Population_Manager*>(m_owner)->SupplyTilePopulation(xx,yy, a_lifestage);
                    if ((tile_population>0)&&(m_filter_fun(xx,yy, m_TheLandscape))){

                        int gx = xx / m_gridcountsize[grid];
                        int gy = yy / m_gridcountsize[grid];
                        int pos = gx + gy*m_gridcountwidth[grid];
                        if(pos<m_totalcells[grid] // todo: just a safety net, use std::array with limit check for m_gridcount[grid] instead
                           && m_gridexclusive[grid].at(pos)){
                            m_gridcount[grid][gx + gy*m_gridcountwidth[grid]]+=tile_population;
                        }

                    }
                }
            }
        }
        WriteData();
    }

}

bool always_true(int x, int y, Landscape * L){
    return true;
}