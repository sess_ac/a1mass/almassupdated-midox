/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*//**
\file
\brief
<B>PopulationManager.h This is the header file for the population manager and associated classes</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 23rd July 2003 \n
 \n
 With additions as noted in: \n
 January 2008 \n
 Doxygen formatted comments in May 2008 \n
*/
//---------------------------------------------------------------------------

#ifndef PopulationManagerH
  #define PopulationManagerH

// Forwards
class TAnimal;
class ALMaSSGUI;
class AlleleFreq;
class AOR_Probe_Base;

// Start defines
typedef char * AnsiString;
typedef vector < TAnimal * > TListOfAnimals;
typedef vector < std::shared_ptr<TAnimal> > smartTListOfAnimals;
// END defines


//------------------------------------------------------------------------------
/**
* \brief An enum to hold all the possible types of population handled by a Population_Manager class
*/
enum TTypesOfPopulation :int
{
    TOP_NoSpecies = -1,
	TOP_Skylark = 0,
	TOP_Vole,
	TOP_Erigone,
	TOP_Bembidion,
	TOP_Hare,
	TOP_Partridge,
	TOP_Goose,
	TOP_RoeDeer,
	TOP_Rabbit,
	TOP_Newt,
	TOP_Osmia,
	TOP_ApisRAM,
	TOP_Haplodrassus,
	TOP_OliveMoth,
	TOP_OliveFly,
	TOP_Lacewing,
	TOP_Aphid,
    TOP_Ladybird, //17
    TOP_Poecilus,
	TOP_Theoretical_1, // For developing simple theoretical models
	TOP_Bombus,
	// Above this line are used for menu items and have to be in this order and present
	TOP_Predators,
	TOP_Hunters,
    TOP_OsmiaParasitoids,
    TOP_Beetle,
    TOP_AphidsD,  // debugging version of aphids
	TOP_Disturbers,
	TOP_foobar
};
//------------------------------------------------------------------------------

enum to_BeforeStepActions :unsigned
{
	toBSA_Shuffle = 0,
	toBSA_SortX,
	toBSA_SortY,
	toBSA_Nothing,
	toBSA_ShuffleEvery500,
	toBSA_foobar
};
/**
\brief
A struct of 100 ints
*/
struct IntArray100 {
public:
  int n[ 100 ];
};

//------------------------------------------------------------------------------

/**
\brief
A struct defining two x,y coordinate sets of positive co-ords only
*/
struct rectangle {
public:
  unsigned m_x1;
  unsigned m_y1;
  unsigned m_x2;
  unsigned m_y2;
};
//------------------------------------------------------------------------------

/**
\brief
The base class of all ALMaSS objects requiring Step code
*/
class TALMaSSObject {
protected:
	/** \brief The basic state number for all objects - '-1' indicates death */
	int m_CurrentStateNo;
	/** \brief Indicates whether the iterative step code is done for this timestep */
	bool m_StepDone;
public:
	/** \brief Returns the current state number */
	int GetCurrentStateNo() const {
		return m_CurrentStateNo;
	}
	/** \brief Sets the current state number */
	void SetCurrentStateNo(int a_num) {
		m_CurrentStateNo = a_num;
	}
	/** \brief Returns the step done indicator flag */
	bool GetStepDone() const {
		return m_StepDone;
	}
	/** \brief Sets the step done indicator flag */
	void SetStepDone(bool a_bool) {
		m_StepDone = a_bool;
	}
	/** \brief BeingStep  behaviour - must be implemented in descendent classes */
	virtual void BeginStep() {}
	/** \brief Step  behaviour - must be implemented in descendent classes */
	virtual void Step() {}
	/** \brief EndStep  behaviour - must be implemented in descendent classes */
	virtual void EndStep() {}
	/** \brief Used to re-use an object - must be implemented in descendent classes */
	virtual void ReinitialiseObject() {
		m_StepDone = false;
		m_CurrentStateNo = 0;
	}
	/** \brief The constructor for TALMaSSObject */
	TALMaSSObject();
	/** \brief The destructor for TALMaSSObject */
	virtual ~TALMaSSObject();
	/** \brief Used for debugging only, tests basic object properties */
	void OnArrayBoundsError();
#ifdef __CJTDebug_5
	int AmAlive;
	int IsAlive() {
		return AmAlive;
	}
	void DEADCODEError();
#endif
};

//------------------------------------------------------------------------------

/**
\brief
A class defining an animals position
*/
class AnimalPosition
// used to communicate the position of an animal to inquiring objects
{
public:
  unsigned m_x;
  unsigned m_y;
  TTypesOfLandscapeElement m_EleType;
  TTypesOfVegetation m_VegType;
};
//------------------------------------------------------------------------------

/**
\brief
Part of the basic ALMaSS system (obselete)
*/
/**
Communicates the range centre, age and size of animals to other objects
*/
class RoeDeerInfo : public AnimalPosition
{
public:
  double m_Size;
  int m_Age;
  APoint m_Range;
  APoint m_OldRange;
};
//------------------------------------------------------------------------------

/**
\brief
The base class for all ALMaSS animal classes.
*/
/**
Includes all the functionality required to be handled by classes derived from Population_Manager, hence a number of empty methods that MUST be reimplemented in descendent classes e.g. CopyMyself()
*/
class TAnimal : public TALMaSSObject {
public:
    TAnimal( int x, int y, Landscape * L );
    unsigned SupplyFarmOwnerRef();
    AnimalPosition SupplyPosition();
    APoint SupplyPoint() { APoint p( m_Location_x, m_Location_y); return p; }
  int SupplyPolygonRef() {
	return m_OurLandscape->SupplyPolyRef( m_Location_x, m_Location_y );
  }
  TTypesOfLandscapeElement SupplyPolygonType() {
	  return m_OurLandscape->GetOwner_tole(m_Location_x, m_Location_y);
  }
  int Supply_m_Location_x() const {
    return m_Location_x;
  }
  int Supply_m_Location_y() const {
    return m_Location_y;
  }
  virtual void KillThis()
  {
    m_CurrentStateNo = -1;
    m_StepDone = true;
  };
  virtual void CopyMyself() {
  };
    void SetX( int a_x ) {
        m_Location_x = a_x;
    }
    void SetY( int a_y ) {

        m_Location_y = a_y;

    }

    /** \brief Used to re-use an object - must be implemented in descendent classes */
    virtual void ReinitialiseObject(int x, int y, Landscape * L) {
        m_OurLandscape = L;
        m_Location_x = x;
        m_Location_y = y;
        TALMaSSObject::ReinitialiseObject();
    }
    virtual int WhatState() {
        return 0;
    }
    virtual void Dying() {
        KillThis();
    }
    void CheckManagement( );
    void CheckManagementXY( int x, int y );
    virtual bool OnFarmEvent( FarmToDo /* event */ ) {
        return false;
    }
protected:
  int m_Location_x;
  int m_Location_y;
  Landscape * m_OurLandscape;
  /** \brief Corrects wrap around co-ordinate problems */

  void CorrectWrapRound() {
	/**
	Does the standard wrap around testing of positions. Uses the addition and modulus operators to avoid testing for negative or > landscape extent.
	This would be an alternative that should be tested for speed at some point.
	*/
	m_Location_x = (m_Location_x + m_OurLandscape->SupplySimAreaWidth()) % m_OurLandscape->SupplySimAreaWidth();
	m_Location_y = (m_Location_y + m_OurLandscape->SupplySimAreaHeight()) % m_OurLandscape->SupplySimAreaHeight();
  }


};

//------------------------------------------------------------------------------

class ReturnLessThanX
{
public:
	bool operator()(TAnimal* A1, int x) const
	{
		return (A1->Supply_m_Location_x() < x);
	}
};
//---------------------------------------------------------------------------

class ReturnMoreThanX
{
public:
	bool operator()(int x, TAnimal* A1) const
	{
		return (A1->Supply_m_Location_x() > x);
	}
};
//---------------------------------------------------------------------------

/**
\brief
Data structure to hold & output probe data probe data is designed to be used to return the number of objects in a given area or areas in specific element or vegetation types or farms
 */
class probe_data
{
protected:
  ofstream * m_MyFile;
  int m_Time;
  char m_MyFileName[ 255 ];
public:
  bool m_FileRecord;
  bool m_FullLandscapeProbe; // true if the probe is a full landscape probe, then no need to check the position and we can just ask PM for numbers: most of the time is like that
  unsigned m_ReportInterval; // from 1-10
  unsigned m_NoAreas; // from 1-10
  rectangle m_Rect[ 16 ]; // can have up to thirty two areas
  unsigned m_NoEleTypes;
  unsigned m_NoVegTypes;
  unsigned m_NoFarms;
  TTypesOfVegetation m_RefVeg[ 25 ]; // up to 25 reference types
  TTypesOfLandscapeElement m_RefEle[ 25 ]; // up to 25 reference types
  unsigned m_RefFarms[ 25 ]; // up to 25 reference types
  // Species Specific Code below:
  bool m_TargetTypes[ 16 ]; // eggs,nestlings,fledgelings,males,females etc.
  void FileOutput( int No, int time, int ProbeNo );
  void FileAppendOutput( int No, int time );
  probe_data();
  void SetFile( ofstream * F );
  ofstream * OpenFile( string Nme );

  bool OpenForAppendToFile() {
	  m_MyFile = new ofstream(m_MyFileName, ios::app);
	  if (!(*m_MyFile).is_open())
	  {
		  g_msg->Warn( WARN_FILE, "PopulationManager::AppendToFile() Unable to open file for append: ", m_MyFileName );
		  exit(1);
	  }
	  return true;
  }
  void CloseFile();
  ~probe_data();
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class SimpleStatistics
{
	/**
	* This class is designed to provide the facility to create simple stats from data that comes in incrementally.
	* It can provide the mean, variance of the data set at any point in time
	*/
protected:
	double m_K;
	double m_n;
	double m_Sum;
	double m_SumX;
	double m_SumX2;
public:
	/** \brief SimpleStatistics constructor */
	SimpleStatistics()
	{
		ClearData();
	}
	/** \brief Add a value */
	void add_variable(double x){
		// This uses the computing shifted data equation. 
		if (m_n == 0) m_K = x;
		m_n++;
		m_Sum += x;
		m_SumX += x - m_K;
		m_SumX2 += (x - m_K) * (x - m_K);
	}
	/** \brief Remove a value */
	void remove_variable(double x){
		m_n--;
		m_Sum -= x;
		m_SumX -= (x - m_K);
		m_SumX2 -= (x - m_K) * (x - m_K);
	}
	/** \brief Returns the number of values */
	double get_N(){
		return m_n;
	}
	/** \brief Returns the mean */
	double get_Total(){
		return m_Sum;
	}
	/** \brief Returns the mean */
	double get_meanvalue(){
		if (m_n == 0) return -1;
		return m_K + m_SumX / m_n;
	}
	/** \brief Returns the population variance */
	double get_varianceP(){
		if (m_n < 2)
		{
			return -1; // Ilegal n value, but don't want to exit
		}
		return (m_SumX2 - (m_SumX*m_SumX) / m_n) / (m_n);
	}
	/** \brief Returns the sample variance */
	double get_varianceS(){
		if (m_n < 2)
		{
			return -1; // Ilegal n value, but don't want to exit
		}
		return (m_SumX2 - (m_SumX*m_SumX) / m_n) / (m_n - 1);
	}
	/** \brief Returns the sample standard deviation */
	double get_SD(){
		if (m_n < 2)
		{
			return -1; // Ilegal n value, but don't want to exit
		}
		return (sqrt(get_varianceS()));
	}
	/** \brief Returns the sample standard error */
	double get_SE(){
		if (m_n < 2)
		{
			return -1; // Ilegal n value, but don't want to exit
		}
		return (sqrt(get_varianceS()/get_N()));
	}
	/** \brief Clears the data */
	void ClearData()
	{
		m_K = 0;
		m_n = 0;
		m_Sum = 0;
		m_SumX = 0;
		m_SumX2 = 0;
	}
};
//------------------------------------------------------------------------------
/**
\brief
Base class for all population managers
*/
/**
The core of the handling of animal populations. All time-step code and most input/output is handled by this class and its descendents. This class effectively implements a state machine to facilitate simulation of animal behaviours and handle potential issues with concurrency. The PopulationManager class is never instantiated but must be used by deriving a descendent class.
*/
class Population_Manager_Base {
public:
	// Methods
	/** \brief The Population_Manager_Base constructor */
	Population_Manager_Base(Landscape* L);
	/** \brief The Population_Manager_Base destructor */
	virtual ~Population_Manager_Base(void);
	/** \brief Must be re-implemented in descendent classes. Gets the number of 'live' objects */
	virtual unsigned GetLiveArraySize(int a_listindex) {
		return 0;
	}
	/** \brief Must be re-implemented in descendent classes */
	virtual unsigned GetPopulationSize(int a_listindex) {
		return 0;
	}
	/** \brief Must be re-implemented in descendent classes */
	virtual void Catastrophe(int /* a_mort */) {
	}
	/** \brief Returns landscape width in m */
	int SupplySimW() {
		return SimW;
	}
	/** \brief Returns landscape height in m */
	int SupplySimH() {
		return SimH;
	}
	/** \brief Returns half the landscape width in m */
	int SupplySimWH() {
		return SimWH;
	}
	/** \brief Returns half the landscape height in m */
	int SupplySimHH() {
		return SimHH;
	}
	/** \brief A place holder for the run function for all population managers, this must be reimplemented in descendent classes */
	virtual void Run(int NoTSteps) { for (int i = 0; i < NoTSteps; i++); };
	/** \brief A stub to build  specfic probes and functions */
	virtual void SetNoProbesAndSpeciesSpecificFunctions(int /*a_pn*/) { ; };
	/** \brief A stub to build  specfic probes and functions */
	virtual int ProbeFileInput(char* p_Filename, int p_ProbeNo);
	/** \brief A stub to build  specfic reporting */
	virtual char* SpeciesSpecificReporting(int /*a_species*/, int /*a_time*/) { return ""; }
	/** \brief Holds a list of pointers to standard output probes */
	probe_data* TheProbe[100] = { nullptr }; // lct: Array of pointers
	int SupplyListNameLength() {
		return m_ListNameLength;
	}
	/** \brief Get a list name from the list */
	const char* SupplyListName(int i) {
		return m_ListNames[i];
	}
	/** \brief A stub for identifying an individual at a location */
	virtual void SupplyLocXY(unsigned /*listindex */, unsigned /*j*/, int& /*x*/, int& /*y*/) {	; }
	/** \brief A stub for identifying an individual at a location */
	virtual TAnimal* FindClosest(int /*x*/, int /*y*/, unsigned /*Type*/) {
		return nullptr;
	}
    std::string SupplySimulationName(){return m_SimulationName;};
    virtual void OpenTheAOROutputProbe(string a_AORFilename);

protected:
	/** \brief stores the simulation height */
	int SimH;
	/** \brief stores the simulation width */
	int SimW;
	/** \brief stores the simulation height halved */
	unsigned SimHH;
	/** \brief stores the simulation width halved */
	unsigned SimWH;
	/** \brief stores the simulation name */
	string m_SimulationName;
	/** \brief holds an internal pointer to the landscape */
	Landscape* m_TheLandscape;
	/** \brief the number of life-stages simulated in the population manager */
	unsigned m_ListNameLength;
	/** \brief A list of life-stage names */
	const char* m_ListNames[32];
    /** \brief name of the AOR Probe file */
    std::string m_AORProbeFileName{"AOR_Probe.txt"};
    /** \brief name of the Probe file */
    std::string m_ProbeFileName{"Probe.res"};
    AOR_Probe_Base* m_AOR_Probe;

};
//------------------------------------------------------------------------------

/**
\brief
Base class for all population managers for agent based models
*/
/**
The core of the handling of animal populations. All time-step code and most input/output is handled by this class and its descendents. This class effectively implements a state machine to facilitate simulation of animal behaviours and handle potential issues with concurrency. The PopulationManager class is never instantiated but must be used by deriving a descendent class.
*/
class Population_Manager : public Population_Manager_Base {
public:
	// Methods
	Population_Manager(Landscape * L, int N = 12);
	virtual ~Population_Manager(void);

	
	/** \brief Sets up probe and species specifics */
	virtual void SetNoProbesAndSpeciesSpecificFunctions(int a_pn);
	/** \brief Gets the number of 'live' objects for a list index in the TheArray */
	virtual unsigned GetLiveArraySize(int a_listindex) {
		return m_LiveArraySize[a_listindex];
	}
    /** \brief Gets the number of species objects
     * by default it is the same as LiveArray, however it could be different.
     * This function should be used/overriden when we are interested in the number of the objects for presentation
     * and not in inner maintenance of the PopulationManager and or TheArray
     * */
    virtual unsigned GetPopulationSize(int a_listindex) {
        return GetLiveArraySize(a_listindex);
    }
	/** \brief Increments the number of 'live' objects for a list index in the TheArray */
	void IncLiveArraySize(int a_listindex) {
		m_LiveArraySize[a_listindex]++;
	}
	virtual void Catastrophe(int /* a_mort */) {
	}
	unsigned int FarmAnimalCensus(unsigned int a_farm, unsigned int a_typeofanimal);
	char* SpeciesSpecificReporting(int a_species, int a_time);
	char* ProbeReport(int a_time);
	char* ProbeReportTimed(int a_time);
	void ImpactProbeReport(int a_Time);
	bool BeginningOfMonth();
	void LOG(const char* fname);
	int SupplyStepSize() {
		return m_StepSize;
	}
	virtual void Run(int NoTSteps);
	virtual long Probe(int ListIndex, probe_data * p_TheProbe);
	virtual void ImpactedProbe();
	/** \brief Returns the pointer indexed by a_index and a_animal. Note NO RANGE CHECK */
	virtual TAnimal* SupplyAnimalPtr(unsigned int a_index, unsigned int a_animal) {
		return TheArray->at(a_index).at(a_animal).get();
	}
    virtual std::shared_ptr<TAnimal> SupplyAnimalSmartPtr(unsigned int a_index, unsigned int a_animal) {
        return TheArray->at(a_index).at(a_animal);
    }
    virtual unsigned SupplyListIndexSize() {
		return (unsigned)TheArray->size();
	}
	unsigned SupplyListSize(unsigned listindex) {
		return (unsigned)TheArray->at(listindex).size();
	}
	/** \brief Debug method to test for out of bounds coordinates */
	bool CheckXY(int l, int i);

    void PushIndividual(unsigned listindex, TAnimal* individual){
        TheArray->at(listindex).push_back(std::shared_ptr<TAnimal>(individual));
    }
    void PushIndividual(unsigned listindex, shared_ptr<TAnimal> individual){
        TheArray->at(listindex).push_back(std::move(individual));
    }
	//---------------------------------------------------------------------------
	bool IsLast(unsigned listindex) {
		if (TheArray->at(listindex).size() > 1) return false; else
			return true;
	}
	/**
	IntArray100 * SupplyStateList() {
	  return & StateList;
	}
	*/
	int SupplyState(unsigned listindex, unsigned j) {
		return TheArray->at(listindex).at(j)->WhatState();
	}
	virtual void SupplyLocXY(unsigned listindex, unsigned j, int & x, int & y) {
		x = TheArray->at(listindex).at(j)->Supply_m_Location_x();
		y = TheArray->at(listindex).at(j)->Supply_m_Location_y();
	}
	const char* SupplyStateNames(int i) {
		return StateNames[i];
	}
	unsigned SupplyStateNamesLength() {
		return StateNamesLength;
	}
	virtual void DisplayLocations();
	// Attributes
#ifdef __ALMASS_VISUAL
	ALMaSSGUI * m_MainForm; // Need to use this when drawing to main form
#endif
	int IndexArrayX[5][10000];
	bool ProbesSet;
	/** Gets a random live individual returned as TAnimal* */
	TAnimal* FindIndividual(unsigned Type, TAnimal* a_me);
	// used to show when probes are needed to be set
	virtual TAnimal * FindClosest(int x, int y, unsigned Type, TAnimal* a_me);
	// Output arrays
#ifndef __UNIX__
// unsigned Counts[ 10 ] [ 100 ];
#endif
protected:
	// Attributes
	  // Holds the number of live animals repsented in each element of vector of vectors TheArray
	vector<unsigned> m_LiveArraySize;
	int m_NoProbes = 0;

	//FILE* m_AlleleFreqsFile;
	//FILE* m_EasyPopRes;
	//FILE* m_FledgelingFile;
	const char* StateNames[100];
	int m_StepSize;
    std::unique_ptr<vector < smartTListOfAnimals >> TheArray{nullptr};
	unsigned StateNamesLength;
	FILE * TestFile;
	FILE * TestFile2;
	/** \brief Holds the season list of possible before step actions.*/
	vector<unsigned> BeforeStepActions; ;
	/** \brief Holds the season number. Used when running goose and hunter sims.*/
	int m_SeasonNumber;
	// Methods
	virtual bool StepFinished();
	virtual void DoFirst();
	virtual void DoBefore();
	virtual void DoAfter();
	virtual void DoLast();
	/** \brief Removes all objects from the TheArray by deleting them and clearing TheArray */
	void EmptyTheArray();
    void RemoveFromList(int listindex, int element){
        TheArray->at(listindex).erase(TheArray->at(listindex).begin()+element);
    }
    virtual void SortX(unsigned Type);

	void SortXIndex(unsigned Type);
    virtual void SortY(unsigned Type);

	void SortState(unsigned Type);
	void SortStateR(unsigned Type);
    virtual unsigned PartitionLiveDead(unsigned Type);
    virtual void Shuffle_or_Sort(unsigned Type);
    virtual void Shuffle(unsigned Type);
	virtual void Catastrophe();
public:
	// Grid related functions
	bool OpenTheRipleysOutputProbe(string a_NWordFilename);
	//virtual void OpenTheAOROutputProbe(string a_AORFilename);
	bool OpenTheMonthlyRipleysOutputProbe();
	bool OpenTheReallyBigProbe();
	virtual void TheAOROutputProbe();
	virtual void TheRipleysOutputProbe(FILE* a_prb);
	virtual void TheReallyBigOutputProbe();
	void CloseTheMonthlyRipleysOutputProbe();
	virtual void CloseTheRipleysOutputProbe();
	virtual void CloseTheReallyBigOutputProbe();
	TTypesOfPopulation GetPopulationType() { return m_population_type; }
	/** \brief Get the season number */
	int GetSeasonNumber() { return m_SeasonNumber; }
protected:
	TTypesOfPopulation m_population_type;
	ofstream* AOROutputPrb;
	FILE * RipleysOutputPrb;
	FILE * RipleysOutputPrb1;
	FILE * RipleysOutputPrb2;
	FILE * RipleysOutputPrb3;
	FILE * RipleysOutputPrb4;
	FILE * RipleysOutputPrb5;
	FILE * RipleysOutputPrb6;
	FILE * RipleysOutputPrb7;
	FILE * RipleysOutputPrb8;
	FILE * RipleysOutputPrb9;
	FILE * RipleysOutputPrb10;
	FILE * RipleysOutputPrb11;
	FILE * RipleysOutputPrb12;
	FILE * ReallyBigOutputPrb;

	long int lamdagrid[2][257][257]; // THIS ONLY WORKS UP TO 10x10 KM !!!!
public:
	void LamdaDeath(int x, int y) {
		// inlined for speed
		lamdagrid[1][x / __lgridsize][y / __lgridsize]++;
	}
	void LamdaBirth(int x, int y) {
		lamdagrid[0][x / __lgridsize][y / __lgridsize]++;
	}
	void LamdaBirth(int x, int y, int z) {
		lamdagrid[0][x / __lgridsize][y / __lgridsize] += z;
	}
	void LamdaClear() {
		for (int i = 0; i < 257; i++) {
			for (int j = 0; j < 257; j++) {
				lamdagrid[0][i][j] = 0;
				lamdagrid[1][i][j] = 0;
			}
		}
	}
	void LamdaDumpOutput();
	// end grid stuff
public: // Special ones for compatability to descended managers
	virtual int SupplyPegPosx(int) {
		return 0;
	}
	virtual int SupplyPegPosy(int) {
		return 0;
	}
	virtual int SupplyCovPosx(int) {
		return 0;
	}
	virtual int SupplyCovPosy(int) {
		return 0;
	}
	virtual bool OpenTheFledgelingProbe() {
		return false;
	}
	virtual bool OpenTheBreedingPairsProbe() {
		return false;
	}
	virtual bool OpenTheBreedingSuccessProbe() {
		return false;
	}
	virtual void BreedingPairsOutput(int) {
	}
	virtual int TheBreedingFemalesProbe(int) {
		return 0;
	}
	virtual int TheFledgelingProbe() {
		return 0;
	}
	virtual void BreedingSuccessProbeOutput(double, int, int, int, int, int, int, int) {
	}
	virtual int TheBreedingSuccessProbe(int &, int &, int &, int &, int &, int &) {
		return 0;
	}
	virtual void FledgelingProbeOutput(int, int) {
	}
	virtual void TheGeneticProbe(unsigned, int, unsigned &) {
	}
	virtual void GeneticsResultsOutput(FILE *, unsigned) {
	}

    void OpenTheAOROutputProbe(string a_AORFilename);

    void Debug_Shuffle(unsigned int Type);

    void RunStepMethods();
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



/**
\brief A class for running simulations without animals
*/
class None_Population_Manager : public Population_Manager 
{
public:
	None_Population_Manager(Landscape* L) : Population_Manager(L, 0)
	{
		m_ListNameLength = 0; // This is for life stages
		TheProbe[0] = NULL;
	}
};

//------------------------------------------------------------------------------



/**
\brief
A small class to hold the pointers to active population managers
*/
class PopulationManagerList
{
public:
  PopulationManagerList() { for (int i=0; i< (int) TOP_foobar; i++) m_populationlist[i] = nullptr; }
  void SetPopulation(std::shared_ptr<Population_Manager_Base> p_pm, int a_pt) { m_populationlist[a_pt] = p_pm; if (p_pm !=
              nullptr) m_populationarray.at(a_pt)=true; else m_populationarray.at(a_pt)=false;}
  /*
   * The following function returnes raw pointer-- it should never be used
   * */
  Population_Manager_Base* GetPopulation(int a_pt) { return m_populationlist[a_pt].get(); }
  /*
   * In the bright future only the next function will be used (or the prototype of the former one will be changed)
   * */
  std::shared_ptr<Population_Manager_Base> GetPopulation_smart(int a_pt) { return m_populationlist[a_pt]; }
  /**\brief returns the number of active populations O(1) */
  int SupplyActivePopulationsCount();

protected:
    std::shared_ptr<Population_Manager_Base> m_populationlist[(int) TOP_foobar];
    std::array<bool, TOP_foobar> m_populationarray{false};
};
//------------------------------------------------------------------------------


#endif


