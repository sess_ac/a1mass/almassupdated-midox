/*
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** 
* \file Newt_Population_Manager.cpp
* \brief <B>The main source code for the newt population manager classes</B>
*
Version of  1 January 2016 \n
By Chris J. Topping \n \n
*/
//---------------------------------------------------------------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../Newt/Newt.h"
#include "../Newt/Newt_Population_Manager.h"

//---------------------------------------------------------------------------------------------------------------------------------
// Configuration variables
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_ReallyBigOutput_used;
extern CfgBool cfg_AOROutput_used;
/** \brief Newt model input parameter - The sum of day degrees above NEWT_EGG_DEVELTHRESHOLD before an egg hatches.	Default value is 900 fitted to T.carnifex data from D'Amesn et al (2007),
but adjusted to get the correct development rate. */
static CfgFloat cfg_NewtEggDevelTotal( "NEWT_EGG_DEVELTOTAL", CFG_CUSTOM, 900 );
/** \brief Newt model input parameter - The temperature slope for egg development change with temperature.	Default value is -0.534 fitted to T.carnifex data from D'Amesn et al (2007).   */
static CfgFloat cfg_NewtEggDevelDDParameter( "NEWT_EGG_DEVELDDPARAMETER", CFG_CUSTOM, -0.534 );
/** \brief Newt model input parameter - the length needed to be achieved before a juvenile matures, from Baker (1998) */
static CfgFloat cfg_NewtJuvenileDevelSize( "NEWT_JUVENILE_DEVELSIZE", CFG_CUSTOM, 42.0 + 21.9 );
/** \brief Newt model input parameter - If temperature is above the NEWT_DORMANCYTEMP the newt grows by this many mm per day and free-living stages can move */
static CfgFloat cfg_NewtJuvenileDailyLengthGain("NEWT_JUVENILEDAILYLENGTHGAIN", CFG_CUSTOM, 0.047827);
/** \brief Newt model input parameter - This is the per mm decrease in survival in non-aquatic rainfall (from Griffiths et al, 2010).  */
static CfgFloat cfg_NewtNARMortalityFactor( "NEWT_NAR_MORTALITYFACTOR", CFG_CUSTOM, 0.0016 );
/** \brief Newt model input parameter - This is the per degree decrease in survival in winter minimum temp (from Griffiths et al, 2010).  */
static CfgFloat cfg_NewtWTMortalityFactor( "NEWT_WT_MORTALITYFACTOR", CFG_CUSTOM, 0.10 );
/** \brief Newt model input parameter - Chance of mortality per road crossing (0.45 for typical road, 0.9 for busy road Hels&Buchwald 2001). This is divided by road width for small road 0.9/18, assuming it is a large busy road.*/
static CfgFloat cfg_NewtRoadMortalityProb( "NEWT_ROADMORTALITYPROB", CFG_CUSTOM, 0.9/18.0 );
/** \brief Newt model input parameter - This is the number of m moved per day */
static CfgInt cfg_NewtWalkSpeed( "ENWT_WALKSPEED", CFG_CUSTOM, 20 );
/** \brief Newt model input parameter - First possible breeding season date Griffiths et al (2010) & Langton et al (2001)*/
static CfgInt cfg_breedingseasonstart( "NEWT_BREEDINGSEASONSTART", CFG_CUSTOM, March );
/** \brief Newt model input parameter - Last possible breeding season date Griffiths et al (2010) & Langton et al (2001) */
static CfgInt cfg_breedingseasonend( "NEWT_BREEDINGSEASONEND", CFG_CUSTOM, June );
/** \brief Newt model input parameter - The maximum lifespan in days for an adult (including egg and larval stages), set at 14 yrs (Francillon-Vieillot et al, 1990) */
static CfgInt cfg_NewtAdultLifespan("NEWT_ADULTLIFESPAN", CFG_CUSTOM, 365 * 14);
/** \brief Newt model input parameter - The temperature at which freeliving stages are dormant (do not move more than within their current 1m cell). Summed over previous 5 days. Default value based on Langton et al (2001)*/
static CfgFloat cfg_newtdormancytemp("NEWT_DORMANCYTEMP", CFG_CUSTOM, 22.5);
/** \brief Newt model input parameter - Total number of eggs produced per breeding season	Default value is 200 based on Arntzen & Teunis 1993, but halved because Macgregor, H. C. and H. Horner (1980) suggests 50% are non-viable. */
static CfgInt cfg_newteggproductionvolume( "NEWT_EGGPRODUCTIONVOLUME", CFG_CUSTOM, 100 );
/** \brief Newt model input parameter - The number of eggs laid per day when breeding. Estimated based on Langton et al (2001) */
static CfgInt cfg_newtdailyeggproductionvolume( "NEWT_EGGDAILYPRODUCTIONVOLUME", CFG_CUSTOM, 5 );
/** \brief Newt model input parameter - the upper size at which larvae will undergo metamorphosis */
static CfgFloat cfg_NewtLarvaDevelThresholdUpperSz( "NEWT_LARVA_DEVELTHRESHOLDUPPERSZ", CFG_CUSTOM, 42.3 );
/** \brief Newt model input parameter - the daily increment assuming a linear growth model to the juvenile start length. Is 42mm - start length of 1mm divided by 12 development weeks */
static CfgFloat cfg_NewtLarvaDailyGrowthIncrement( "NEWT_LARVA_DAILYGROWTHINCREMENT", CFG_CUSTOM, (42.3 - 1.0) / (12 * 7.0) );
/** \brief Newt model input parameter - the development time at which the larva will undergo metamorphosis if its size is between cfg_NewtLarvaDevelThresholdUpperSz and cfg_NewtLarvaDevelThresholdLowerSz thresholds */
static CfgFloat cfg_NewtLarvaDevelThresholdTime( "NEWT_LARVA_DEVELTHRESHOLDTIME", CFG_CUSTOM, 12 * 7 );
/** \brief Newt model input parameter - The preciptitation needed for newt movement (over last two days) */
static CfgFloat cfg_newtdormancyhumidity( "NEWT_DORMANCYHUMIDITY", CFG_CUSTOM, 0.003125 );
/** \brief Newt model input parameter - The daily probability of dispersal in good habitat if weather is suitable (temp and precipication). Needed to be set to keep 64% of newts within 20 m of the pond (Jehle & Arntzen, 2000) and 95% within 50m (Jehle, 2000) */
static CfgFloat cfg_newtgoodhabitatdispersalprob("NEWT_GOODHABITATDISPPROB", CFG_CUSTOM, 0.25, 0.0, 1.0);
/** \brief Newt model input parameter - The daily probability of dispersal in poor habitat if weather is suitable (temp and precipication). habitat preference (%LOCS/%AREA 0.5 for open 9.5 for hedgerow) (Jehle & Arntzen, 2000) */
static CfgFloat cfg_newtpoorhabitatdispersalprob("NEWT_POORHABITATDISPPROB", CFG_CUSTOM, 1.0);
/** \brief Newt model input parameter - Daily egg mortality probability, fitted to 75% mortality by 4 weeks */
static CfgFloat cfg_NewtEggMortalityChance("NEWT_EGG_MORTALITYCHANCE", CFG_CUSTOM, 0.048);
/** \brief Newt model input parameter - Daily larval mortality probability, excluding density dependent mortality. Fitted to 5% survival from egg to juvenile stage. */
static CfgFloat cfg_NewtLarvaMortalityChance("NEWT_LARVAL_MORTALITYCHANCE", CFG_CUSTOM, 0.015);
/** \brief Newt model input parameter - This multiplied by its age is the larval food consumption g of food per day */
static CfgFloat cfg_LarvalFoodProportion("NEWT_LARVALFOODPROPCONST", CFG_CUSTOM, 0.01);

//Pesticide Testing
/** \brief Pesticide input parameter - the mg of pesticicde assumed to trigger a response in embryonic newts */
static CfgFloat	cfg_NewtEggPPPToxTrigger( "NEWT_EGG_PPP_TOX_TRIGGER", CFG_CUSTOM, 9999.999 );
/** \brief Pesticide input parameter - the mg of pesticicde assumed to trigger a response in larval newts */
static CfgFloat	cfg_NewtLarvaPPPToxTrigger( "NEWT_LARVA_PPP_TOX_TRIGGER", CFG_CUSTOM, 9999.999 );
/** \brief Pesticide input parameter - the mg of pesticicde assumed to trigger a response in juvenile newts */
static CfgFloat	cfg_NewtJuvenilePPPToxTrigger_EnvConc("NEWT_JUVENILE_PPP_TOX_TRIGGER_ENVCONC", CFG_CUSTOM, 9999.999);
/** \brief Pesticide input parameter - the mg of pesticicde assumed to trigger a response in juvenile newts for overspray */
static CfgFloat	cfg_NewtJuvenilePPPToxTrigger_Overspray("NEWT_JUVENILE_PPP_TOX_TRIGGER_OVERSPRAY", CFG_CUSTOM, 9999.999);
/** \brief Pesticide input parameter - the mg of pesticicde assumed to trigger a response in adult newts */
static CfgFloat	cfg_NewtAdultPPPToxTrigger( "NEWT_ADULT_PPP_TOX_TRIGGER", CFG_CUSTOM, 9999.999 );
/** \brief Pesticide input parameter - One minus the proportion of internal PPP eliminated each day in embryonic newts */
static CfgFloat	cfg_NewtEggPPPToxEliminationRate( "NEWT_EGG_PPP_TOX_ELIMINATIONRATE", CFG_CUSTOM, 0.0 );
/** \brief Pesticide input parameter - One minus the proportion of internal PPP eliminated each day in larval newts */
static CfgFloat	cfg_NewtLarvaPPPToxEliminationRate( "NEWT_LARVA_PPP_TOX_ELIMINATIONRATE", CFG_CUSTOM, 0.0 );
/** \brief Pesticide input parameter - One minus the proportion of internal PPP eliminated each day in juvenile newts */
static CfgFloat	cfg_NewtJuvenilePPPToxEliminationRate( "NEWT_JUVENILE_PPP_TOX_ELIMINATIONRATE", CFG_CUSTOM, 0.0 );
/** \brief Pesticide input parameter - One minus the proportion of internal PPP eliminated each day in adult newts */
static CfgFloat	cfg_NewtAdultPPPToxEliminationRate( "NEWT_ADULT_PPP_TOX_ELIMINATIONRATE", CFG_CUSTOM, 0.0 );
/** \brief Pesticide input parameter - daily probability of mortality if above a trigger dose */
static CfgFloat	cfg_NewtEggPPPEffectProbability( "NEWT_EGG_PPP_TOX_EFFECTPROBABILITY", CFG_CUSTOM, 0.0 );
/** \brief Pesticide input parameter  - daily probability of mortality if above a trigger dose */
static CfgFloat	cfg_NewtLarvaPPPEffectProbability( "NEWT_LARVA_PPP_TOX_EFFECTPROBABILITY", CFG_CUSTOM, 0.0 );
/** \brief Pesticide input parameter  - daily probability of mortality if above a trigger dose */
static CfgFloat	cfg_NewtJuvenilePPPEffectProbability_EnvConc("NEWT_JUV_PPP_TOX_EFFECTPROBABILITY_ENVCONC", CFG_CUSTOM, 0.0);
/** \brief Pesticide input parameter  - daily probability of mortality if above a trigger dose */
static CfgFloat	cfg_NewtJuvenilePPPEffectProbability_Overspray("NEWT_JUV_PPP_TOX_EFFECTPROBABILITY_OVERSPRAY", CFG_CUSTOM, 0.0);
/** \brief Pesticide input parameter  - daily probability of mortality if above a trigger dose */
static CfgFloat	cfg_NewtAdultPPPEffectProbability( "NEWT_ADULT_PPP_TOX_EFFECTPROBABILITY", CFG_CUSTOM, 0.0 );


// Other parameters
/** \brief Newt model input parameter - This is the step size the newt can take in the model (can be set >1 to speed model up) */
static CfgInt cfg_NewtWalkStepsize( "NEWT_WALKSSTEPSIZE", CFG_CUSTOM, 1 );
/** \brief Newt model input parameter - the lower size at which larvae CANNOT undergo metamorphosis */
static CfgFloat cfg_NewtLarvaDevelThresholdLowerSz( "NEWT_LARVA_DEVELTHRESHOLDLOWERSZ", CFG_CUSTOM, 42.3 );
/** \brief The starting numbers for male and female newts */
static CfgInt cfg_newt_startnumber( "NEWT_STARTNUMBER", CFG_CUSTOM, 1000 );
/** \brief Denotes whether pesticide testing mode is active for eggs - needs to be set to true to evaluate PPP impacts */
static CfgBool cfg_Newt_Test_Pesticide_Egg( "NEWT_TEST_PESTICIDE_EGG", CFG_CUSTOM, false );
/** \brief Denotes whether pesticide testing mode is active for larvae - needs to be set to true to evaluate PPP impacts */
static CfgBool cfg_Newt_Test_Pesticide_Larva( "NEWT_TEST_PESTICIDE_LARVA", CFG_CUSTOM, false );
/** \brief Denotes whether pesticide testing mode is active for adults - needs to be set to true to evaluate PPP impacts */
static CfgBool cfg_Newt_Test_Pesticide_Terrestrial( "NEWT_TEST_PESTICIDE_TERRESTRIAL", CFG_CUSTOM, false );

//---------------------------------------------------------------------------------------------------------------------------------
// Assign default static member values (these will be changed later).
bool Newt_Base::m_test_pesticide_egg = cfg_Newt_Test_Pesticide_Egg.value();
bool Newt_Base::m_test_pesticide_larva = cfg_Newt_Test_Pesticide_Larva.value();
bool Newt_Base::m_test_pesticide_terrestrial = cfg_Newt_Test_Pesticide_Terrestrial.value();
double Newt_Egg::m_EggPPPThreshold = cfg_NewtEggPPPToxTrigger.value();
double Newt_Egg::m_EggPPPElimRate = cfg_NewtEggPPPToxEliminationRate.value();
double Newt_Egg::m_EggPPPEffectProbability = cfg_NewtEggPPPEffectProbability.value();
double Newt_Larva::m_LarvaPPPThreshold = cfg_NewtLarvaPPPToxTrigger.value();
double Newt_Larva::m_LarvaPPPElimRate = cfg_NewtLarvaPPPToxEliminationRate.value();
double Newt_Larva::m_LarvaPPPEffectProbability = cfg_NewtLarvaPPPEffectProbability.value();
double Newt_Juvenile::m_JuvenilePPPThreshold_Min = 0;
double Newt_Juvenile::m_JuvenilePPPThreshold_EnvConc = cfg_NewtJuvenilePPPToxTrigger_EnvConc.value();
double Newt_Juvenile::m_JuvenilePPPThreshold_Overspray = cfg_NewtJuvenilePPPToxTrigger_Overspray.value();
double Newt_Juvenile::m_JuvenilePPPElimRate = cfg_NewtJuvenilePPPToxEliminationRate.value();
double Newt_Juvenile::m_JuvenilePPPEffectProbability_EnvConc = cfg_NewtJuvenilePPPEffectProbability_EnvConc.value();
double Newt_Juvenile::m_JuvenilePPPEffectProbability_Overspray = cfg_NewtJuvenilePPPEffectProbability_Overspray.value();
double Newt_Adult::m_AdultPPPThreshold = cfg_NewtAdultPPPToxTrigger.value();
double Newt_Adult::m_AdultPPPElimRate = cfg_NewtAdultPPPToxEliminationRate.value();
double Newt_Adult::m_AdultPPPEffectProbability = cfg_NewtAdultPPPEffectProbability.value();


double Newt_Base::m_EggDevelopmentDDTotal = cfg_NewtEggDevelTotal.value();
double Newt_Base::m_EggDevelopmentDDParameter = cfg_NewtEggDevelDDParameter.value();
double Newt_Larva::m_LarvaDevelopmentUpperSz = cfg_NewtLarvaDevelThresholdUpperSz.value();
double Newt_Larva::m_LarvaDevelopmentLowerSz = cfg_NewtLarvaDevelThresholdLowerSz.value();
double Newt_Larva::m_LarvaDevelopmentTime = cfg_NewtLarvaDevelThresholdTime.value();
double Newt_Larva::m_NewtLarvaDailyGrowthIncrement = cfg_NewtLarvaDailyGrowthIncrement.value();
double Newt_Base::m_JuvenileDevelopmentSize = cfg_NewtJuvenileDevelSize.value();
double  Newt_Base::m_EggMortalityChance = cfg_NewtEggMortalityChance.value();
double  Newt_Larva::m_LarvaMortalityChance = cfg_NewtLarvaMortalityChance.value();
double  Newt_Base::m_JuvenileMortalityChance = 0.0;
double  Newt_Base::m_AdultMortalityChance = 0.0;
double Newt_Juvenile::m_JuvenileDailyWeightGain = cfg_NewtJuvenileDailyLengthGain.value();
int Newt_Juvenile::m_SimW = 0;
int Newt_Juvenile::m_SimH = 0;
double Newt_Juvenile::m_roadmortalityprob = cfg_NewtRoadMortalityProb.value();
int Newt_Juvenile::m_newtwalkspeed = cfg_NewtWalkSpeed.value();
int Newt_Juvenile::m_newtwalkstepsize = cfg_NewtWalkStepsize.value();
int Newt_Adult::m_newtadultwalkspeed = cfg_NewtWalkSpeed.value()/10; // 1/10th of the juvenile following Karlsson et al (2007)
double Newt_Juvenile::m_goodhabitatdispersalprob = cfg_newtgoodhabitatdispersalprob.value();
double Newt_Juvenile::m_poorhabitatdispersalprob = cfg_newtpoorhabitatdispersalprob.value();
unsigned Newt_Adult::m_AdultLifespan = cfg_NewtAdultLifespan.value();
double Newt_Juvenile::m_NewtDormancyTemperature = cfg_newtdormancytemp.value();
double Newt_Juvenile::m_NewtDormancyHumidity = cfg_newtdormancyhumidity.value();
unsigned Newt_Female::m_eggproductionvolume = cfg_newteggproductionvolume.value();
unsigned Newt_Female::m_eggdailyproductionvolume = cfg_newtdailyeggproductionvolume.value();
double Newt_Larva::m_LarvalFoodProportion = cfg_LarvalFoodProportion.value();
//---------------------------------------------------------------------------------------------------------------------------------

Newt_Population_Manager::~Newt_Population_Manager(void) {
	// Should all be done by the Population_Manager destructor
}
//---------------------------------------------------------------------------------------------------------------------------------

Newt_Population_Manager::Newt_Population_Manager(Landscape* L) : Population_Manager(L, 5) {
	// Load List of Animal Classes
	m_ListNames[0] = "Newt_Egg";
	m_ListNames[1] = "Newt_Larva";
	m_ListNames[2] = "Newt_Juvenile";
	m_ListNames[3] = "Newt_Male";
	m_ListNames[4] = "Newt_Female";
	m_ListNameLength = 5;
	m_SimulationName = "Newt";
	// Assign the correct values to static variables
	Newt_Base::m_test_pesticide_egg = cfg_Newt_Test_Pesticide_Egg.value();
	Newt_Base::m_test_pesticide_larva = cfg_Newt_Test_Pesticide_Larva.value();
	Newt_Base::m_test_pesticide_terrestrial = cfg_Newt_Test_Pesticide_Terrestrial.value();
	Newt_Egg::m_EggPPPThreshold = cfg_NewtEggPPPToxTrigger.value();
	Newt_Egg::m_EggPPPElimRate = cfg_NewtEggPPPToxEliminationRate.value();
	Newt_Egg::m_EggPPPEffectProbability = cfg_NewtEggPPPEffectProbability.value();
	Newt_Larva::m_LarvaPPPThreshold = cfg_NewtLarvaPPPToxTrigger.value();
	Newt_Larva::m_LarvaPPPElimRate = cfg_NewtLarvaPPPToxEliminationRate.value();
	Newt_Larva::m_LarvaPPPEffectProbability = cfg_NewtLarvaPPPEffectProbability.value();
	Newt_Juvenile::m_JuvenilePPPThreshold_EnvConc = cfg_NewtJuvenilePPPToxTrigger_EnvConc.value();
	Newt_Juvenile::m_JuvenilePPPThreshold_Overspray = cfg_NewtJuvenilePPPToxTrigger_Overspray.value();
	if (Newt_Juvenile::m_JuvenilePPPThreshold_Overspray<Newt_Juvenile::m_JuvenilePPPThreshold_EnvConc) Newt_Juvenile::m_JuvenilePPPThreshold_Min = Newt_Juvenile::m_JuvenilePPPThreshold_Overspray;
	    else Newt_Juvenile::m_JuvenilePPPThreshold_Min = Newt_Juvenile::m_JuvenilePPPThreshold_EnvConc;
	Newt_Juvenile::m_JuvenilePPPElimRate = cfg_NewtJuvenilePPPToxEliminationRate.value();
	Newt_Juvenile::m_JuvenilePPPEffectProbability_EnvConc = cfg_NewtJuvenilePPPEffectProbability_EnvConc.value();
	Newt_Juvenile::m_JuvenilePPPEffectProbability_Overspray = cfg_NewtJuvenilePPPEffectProbability_Overspray.value();
	Newt_Adult::m_AdultPPPThreshold = cfg_NewtAdultPPPToxTrigger.value();
	Newt_Adult::m_AdultPPPElimRate = cfg_NewtAdultPPPToxEliminationRate.value();
	Newt_Adult::m_AdultPPPEffectProbability = cfg_NewtAdultPPPEffectProbability.value();
	Newt_Base::m_EggDevelopmentDDTotal = cfg_NewtEggDevelTotal.value();
	Newt_Base::m_EggDevelopmentDDParameter = cfg_NewtEggDevelDDParameter.value();
	Newt_Base::m_JuvenileDevelopmentSize = cfg_NewtJuvenileDevelSize.value();
	Newt_Base::m_EggMortalityChance = cfg_NewtEggMortalityChance.value();
	Newt_Larva::m_LarvaDevelopmentUpperSz = cfg_NewtLarvaDevelThresholdUpperSz.value();
	Newt_Larva::m_LarvaDevelopmentLowerSz = cfg_NewtLarvaDevelThresholdLowerSz.value();
	Newt_Larva::m_LarvaDevelopmentTime = cfg_NewtLarvaDevelThresholdTime.value();
	Newt_Larva::m_LarvaMortalityChance = cfg_NewtLarvaMortalityChance.value();
	Newt_Larva::m_LarvalFoodProportion = cfg_LarvalFoodProportion.value();
	Newt_Larva::m_NewtLarvaDailyGrowthIncrement = cfg_NewtLarvaDailyGrowthIncrement.value();
	Newt_Juvenile::m_JuvenileDailyWeightGain = cfg_NewtJuvenileDailyLengthGain.value();
	Newt_Juvenile::m_SimW = m_TheLandscape->SupplySimAreaWidth();
	Newt_Juvenile::m_SimH = m_TheLandscape->SupplySimAreaHeight();
	Newt_Juvenile::m_roadmortalityprob = cfg_NewtRoadMortalityProb.value();
	Newt_Juvenile::m_newtwalkspeed = cfg_NewtWalkSpeed.value();
	Newt_Juvenile::m_newtwalkstepsize = cfg_NewtWalkStepsize.value();
	Newt_Adult::m_newtadultwalkspeed = cfg_NewtWalkSpeed.value() / 10; // 1/ 10th of the juvenile following Karlsson et al( 2007 )
	Newt_Juvenile::m_goodhabitatdispersalprob = cfg_newtgoodhabitatdispersalprob.value();
	Newt_Juvenile::m_poorhabitatdispersalprob = cfg_newtpoorhabitatdispersalprob.value();
	Newt_Juvenile::m_NewtDormancyTemperature = cfg_newtdormancytemp.value();
	Newt_Juvenile::m_NewtDormancyHumidity = cfg_newtdormancyhumidity.value();
	Newt_Female::m_eggproductionvolume = cfg_newteggproductionvolume.value();
	Newt_Female::m_eggdailyproductionvolume = cfg_newtdailyeggproductionvolume.value();
	// Create any precalculated data arrays
	for (int i = 1; i < 30; i++) {
		double a = cfg_NewtEggDevelTotal.value() * pow(i, Newt_Base::m_EggDevelopmentDDParameter);
		double b = a / double(i);
		m_NewtEgg_DDTempRate[i] = (cfg_NewtEggDevelTotal.value() / b) / double(i);
	}
	m_NewtEgg_DDTempRate[0] = 0.0;
	m_NewtEgg_DDTempRate[30] = 0.0;
	// Create some animals
	struct_Newt* sp;
	sp = new struct_Newt;
	sp->NPM = this;
	sp->L = m_TheLandscape;
	sp->age = 365;
	sp->reproinhib = false;
	for (int i = 0; i < cfg_newt_startnumber.value(); i++) {
		int pref = m_TheLandscape->SupplyRandomPondRef();
		sp->x = m_TheLandscape->SupplyCentroidX(pref);
		sp->y = m_TheLandscape->SupplyCentroidY(pref);
		if (m_TheLandscape->SupplyElementType(sp->x, sp->y) != tole_Pond) {
			g_msg->Warn("Centroid not in Pond.Pond ref : ", pref);
			exit(0);
		}
		sp->pondrefs.resize(1);
		sp->pondrefs[0] = m_TheLandscape->SupplyPondIndex(pref);
		// test if this is a pond, if so create a male and female
		CreateObjects(tton_Male, NULL, sp, 1);
		CreateObjects(tton_Female, NULL, sp, 1);
	}
	// Now we must the set the in pond flag for all newly created newts (because we put them in a pond)
	int sz = int( GetLiveArraySize( tton_Male ) );
	for (int i = 0; i < sz; i++) {
		dynamic_cast<Newt_Male*>(SupplyAnimalPtr(tton_Male,i))->SetInPond();
	}
	sz = int( GetLiveArraySize( tton_Female ) );
	for (int i = 0; i < sz; i++) {
		dynamic_cast<Newt_Female*>(SupplyAnimalPtr(tton_Female,i))->SetInPond();
	}
	// Set any variable values that are needed on start-up
	m_BreedingSeasonFlag = 1;
	// Set up any output needed
#ifdef __RECORDNEWTMETAMORPHOSIS
	InitOutputMetamorphosisStats();
#endif
	if (cfg_RipleysOutput_used.value()) {
		OpenTheRipleysOutputProbe("");
	}
	if (cfg_ReallyBigOutput_used.value()) {
		OpenTheReallyBigProbe();
	}
	else ReallyBigOutputPrb = 0;
}

//---------------------------------------------------------------------------------------------------------------------------------
void Newt_Population_Manager::SetFreeLivingMortChance() {
	/**
	* The model for daily newt mortality in free-living stages and out of water is based on Griffiths et al (2010).
	* It assumes survival is inversely releated to rainfall Jun-Feb and minimum winter mean (Dec-Feb).
	* This method calculates the daily probability of survival assuming 270 days of season and 0.0016 reduction for each 1mm rain above 200mm
	* and 0.1 reduction for each 1 degree above -2 during the 90 days of Winter assumed by Griffiths et al (2010).
	* The two survival curves are added and divided by 2 (equal weight and central tendency).
	* Because we use seasonal data we only need to calculate this at the beginning of the season.
	* Initial tests show that extending these relationships outside the range of the original data leads to extreme mortalities and therefore
	* input data is constrained to lie within the maximum bounds of the original data used for the relationships.
	* This daily rate of mortality is applied each day so the overall survival rate per year is transformed to daily probability needed to achieve this.
	*/
	double survivalchance1 = 0;
	double survivalchance2 = 0;
	int today = m_TheLandscape->SupplyDayInYear();
	if (today == June) {
		// New season, so calculate the mortalities
		double mortrainfall = m_TheLandscape->SupplyRainPeriod(g_date->OldDays() + June + 270, 270);
		if (mortrainfall < 200) mortrainfall = 200;
		if (mortrainfall > 650) mortrainfall = 650;
		double morttemp = m_TheLandscape->SupplyTempPeriod(g_date->OldDays() + December + 90, 90) / 90.0;
		if (morttemp < -2.0) morttemp = -2.0;
		if (morttemp > 4.0) morttemp = 4.0;
		survivalchance1 = pow((1.0 - ((mortrainfall - 200)*cfg_NewtNARMortalityFactor.value())), 1.0 / 365.0);
		survivalchance2 = pow((1.0 - ((morttemp + 2.0)*cfg_NewtWTMortalityFactor.value())), 1.0 / 365.0);
		// Griffiths et al have an average survival of 0.5, which equates to a mortality of 0.001897 per day.
		// However, tests indicate that this is just too low for overall population persistance.
		double mortchance = 1.0 - (((survivalchance1 + survivalchance2) / 2.0) );
		// Add some annual stochasiticity
		mortchance = mortchance + (mortchance*g_rand_uni()*2.0) - (mortchance*1.0); // +/-10%	
		Newt_Base::m_JuvenileMortalityChance = mortchance + 0.001;
		Newt_Base::m_AdultMortalityChance = mortchance;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------
void Newt_Population_Manager::CreateObjects(int ob_type, TAnimal *, struct_Newt * data, int number) {
	Newt_Egg*  new_Newt_Egg;
	Newt_Larva*  new_Newt_Larva;
	Newt_Juvenile*  new_Newt_Juvenile;
	Newt_Male* new_Newt_Male;
	Newt_Female*  new_Newt_Female;
#ifdef __RECORDNEWTMETAMORPHOSIS
	if (ob_type == tton_Egg) RecordEggProduction(number);
#endif
	for (int i = 0; i < number; i++) {
		switch (ob_type) {
		case tton_Egg:
			if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				dynamic_cast<Newt_Egg*>(SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->pondrefs, data->L, data->NPM, false);
				IncLiveArraySize(ob_type);
			}
			else {
				new_Newt_Egg = new Newt_Egg(data->x, data->y, data->pondrefs, data->L, data->NPM, false);
                PushIndividual(ob_type,new_Newt_Egg);
				IncLiveArraySize(ob_type);
			}
			break;
		case tton_Larva:
			if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				new_Newt_Larva = dynamic_cast<Newt_Larva*>(SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Newt_Larva->ReInit(data->x, data->y, data->pondrefs, data->L, data->NPM, data->age, data->reproinhib);
				new_Newt_Larva->SetAge(data->age);
			}
			else {
				new_Newt_Larva = new Newt_Larva(data->x, data->y, data->pondrefs, data->L, data->NPM, data->age, data->reproinhib);
                PushIndividual(ob_type,new_Newt_Larva);
				new_Newt_Larva->SetAge(data->age);
			}
			IncLiveArraySize(ob_type);
			break;
		case tton_Juvenile:
			if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				new_Newt_Juvenile = dynamic_cast<Newt_Juvenile*>(SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Newt_Juvenile->ReInit(data->x, data->y, data->pondrefs, data->L, data->NPM, data->age, data->reproinhib);
				new_Newt_Juvenile->SetAge(data->age);
			}
			else {
				new_Newt_Juvenile = new Newt_Juvenile(data->x, data->y, data->pondrefs, data->L, data->NPM, data->age, data->reproinhib);
				new_Newt_Juvenile->SetAge(data->age);
                PushIndividual(ob_type,new_Newt_Juvenile);
			}
			IncLiveArraySize(ob_type);
			break;
		case tton_Male:
			if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				new_Newt_Male = dynamic_cast<Newt_Male*>(SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Newt_Male->ReInit(data->x, data->y, data->pondrefs, data->L, data->NPM, data->age, data->reproinhib);
				new_Newt_Male->SetAge(data->age);
				new_Newt_Male->SetWeight(data->weight);
			}
			else {
				new_Newt_Male = new Newt_Male(data->x, data->y, data->pondrefs, data->L, data->NPM, data->age, data->reproinhib);
				new_Newt_Male->SetAge(data->age);
				new_Newt_Male->SetWeight(data->weight);
                PushIndividual(ob_type,new_Newt_Male);
			}
			IncLiveArraySize(ob_type);
			break;
		case tton_Female:
			if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				new_Newt_Female = dynamic_cast<Newt_Female*>(SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Newt_Female->ReInit(data->x, data->y, data->pondrefs, data->L, data->NPM, data->age, data->reproinhib);
				new_Newt_Female->SetAge(data->age);
				new_Newt_Female->SetWeight(data->weight);
			}
			else {
				new_Newt_Female = new Newt_Female(data->x, data->y, data->pondrefs, data->L, data->NPM, data->age, data->reproinhib);
				new_Newt_Female->SetAge(data->age);
				new_Newt_Female->SetWeight(data->weight);
                PushIndividual(ob_type,new_Newt_Female);
			}
			IncLiveArraySize(ob_type);
			break;
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Population_Manager::DoFirst() {
	int today = m_TheLandscape->SupplyDayInYear();
	if (today == cfg_breedingseasonstart.value()) m_BreedingSeasonFlag = 0;
	else if (today >= cfg_breedingseasonend.value()) m_BreedingSeasonFlag++;
	if (today == 1) {
		int sz = int( GetLiveArraySize( tton_Female ) );
		for (int i = 0; i < sz; i++) {
			dynamic_cast<Newt_Female*>(SupplyAnimalPtr(tton_Female,i))->ResetEggProduction();
		}
	}
	SetFreeLivingMortChance();
#ifdef __RECORDNEWTMETAMORPHOSIS
	if (today == 364) {
		OutputMetamorphosisStats();
	}
#endif
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Population_Manager::TheAOROutputProbe() {
	m_AOR_Probe->DoProbe(tton_Female);
}
//-----------------------------------------------------------------------------

void Newt_Population_Manager::RecordAdultProduction(int a_adult) {
	// A Newt has become juvenile, we need to record this for the statistics
	m_NewtAdultProdStats.add_variable(a_adult);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Population_Manager::RecordEggProduction(int a_eggs) {
	// A Newt has become juvenile, we need to record this for the statistics
	m_NewtEggProdStats.add_variable(a_eggs);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Population_Manager::RecordMetamorphosis(int a_age) {
	// A Newt has become juvenile, we need to record this for the statistics
	m_NewtMetamorphosisStats.add_variable(a_age);
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Population_Manager::InitOutputMetamorphosisStats() {
	fstream ofile("NewtMetamorphosisDevelStats.txt", ios::out);
	ofile << "Year" << '\t' << "N" << '\t' << "Mean" << '\t' << "Variance" << '\t' << "EggProduction" << '\t' << "JuvenileProduction" << '\t' << "AdultProduction" << '\t' << "Juveniles" << '\t' << "Adults" << endl;
	ofile.close();
}
//---------------------------------------------------------------------------------------------------------------------------------

void Newt_Population_Manager::OutputMetamorphosisStats() {
	fstream ofile("NewtMetamorphosisDevelStats.txt", ios::app | ios::out);
	ofile << m_TheLandscape->SupplyYearNumber() << '\t' << m_NewtMetamorphosisStats.get_N() << '\t' << m_NewtMetamorphosisStats.get_meanvalue() << '\t' << m_NewtMetamorphosisStats.get_varianceP()
		<< '\t' << m_NewtEggProdStats.get_Total() << '\t' << m_NewtMetamorphosisStats.get_N() << '\t' << m_NewtAdultProdStats.get_N() << '\t' << GetLiveArraySize( tton_Juvenile ) << '\t' << GetLiveArraySize( tton_Female ) + GetLiveArraySize( tton_Male ) << endl;
	m_NewtMetamorphosisStats.ClearData();
	m_NewtEggProdStats.ClearData();
	m_NewtAdultProdStats.ClearData();
	ofile.close();
}
//---------------------------------------------------------------------------------------------------------------------------------
