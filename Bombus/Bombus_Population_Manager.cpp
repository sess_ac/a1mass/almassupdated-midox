/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Bombus_Population_Manager.cpp
Version of  10 September 2021 \n
By Jordan Chetcuti \n \n
*/
//---------------------------------------------------------------------------
#include <string.h>
#include <fstream>
#include <utility>
#include<vector>
#pragma warning( push )
#pragma warning( disable : 4100)
#pragma warning( disable : 4127)
#pragma warning( disable : 4244)
#pragma warning( disable : 4267)
#pragma warning( pop )
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Bombus/Bombus.h"
#include "../Bombus/Bombus_Population_Manager.h"
//---------------------------------------------------------------------------------------

/** \brief Input file for Bombus Colony density per LE */
static CfgStr cfg_BombusColonyByLE_Datafile("BOMBUS_COLONYBYLEDATAFILE", CFG_CUSTOM, "BombusColonysByHabitat.txt");
/** \brief OutputFile file for Bombus individuals */
CfgStr cfg_BombusOutput("BOMBUS_OUTPUT", CFG_CUSTOM, "Bombus_Output.txt");
/** \brief Run number to added to output file*/
CfgInt cfg_BombusRunNo("BOMBUS_RUNNO", CFG_CUSTOM, 0);
/** \brief The number of starting gynes when the simulation starts*/
CfgInt cfg_BombusNoStartingGyne("BOMBUS_NOSTARTINGGYNE", CFG_CUSTOM, 1);
/** The age of the first gynes. Will be organic for future generations, but here, a figured a third of the year for the first? */
CfgInt cfg_BombusStartGyneAge("BOMBUS_STARTGYNEAGE", CFG_CUSTOM, 144000);
/** \brief These are the mean lean mass weight in grams from \cite<Samuelson2018>. Used to create a normal distribution of mass for starting gynes*/
CfgFloat cfg_BombusStartGyneMass("BOMBUS_STARTGYNEMASS", CFG_CUSTOM, 1325.57);
/** \brief These are the standard deviation lean mass weight in grams from \cite<Samuelson2018>. Used to create a normal distribution of mass for starting gynes*/
CfgFloat cfg_BombusStartGyneMassSD("BOMBUS_STARTGYNEMASSSD", CFG_CUSTOM, 20.76);
/** \brief These are the mean  body fat weight in grams from \cite<Samuelson2018>. Used to create a normal distribution of mass for starting gynes*/
CfgFloat cfg_BombusStartGyneBodyFat("BOMBUS_STARTGYNEBODYFAT", CFG_CUSTOM, 1056.47);
/** \brief These are the standard deviation body fat weight in grams from \cite<Samuelson2018>. Used to create a normal distribution of mass for starting gynes*/
CfgFloat cfg_BombusStartGyneBodyFatSD("BOMBUS_STARTGYNEBODYFATSD", CFG_CUSTOM, 132.56);
/** \brief Mean Concentration of pheromones added into wax by queen. Used to create a normal distribution of mass for starting gynes*/
CfgFloat cfg_BombuswaxConcInter("BOMBUS_WAXCONCINTER", CFG_CUSTOM, 0.14);
/** \brief Standard deviation concentration of pheromones added into wax by queen. Used to create a normal distribution of mass for starting gynes*/
CfgFloat cfg_BombuswaxConcInterSD("BOMBUS_WAXCONCINTERSD", CFG_CUSTOM, 0.01);

/** \brief Whether the starting number of days the gyne has already hibernated is fixed or not*/
CfgBool cfg_BombusDaysHibernatedFix("BOMBUS_DAYSHIBERNATEDFIX", CFG_CUSTOM, false);
/** \brief If cfg_BombusDaysHibernated == true the fixed number of days the starting gynes has already hibernated is fixed or not*/
CfgInt cfg_BombusDaysHibernatedFixedVal("BOMBUS_DAYSHIBERNATEDFIXEDVAL", CFG_CUSTOM, 100);
/** \brief If cfg_BombusDaysHibernated == false alpha to define a gamma distributionof days the starting gynes has already hibernated*/
CfgFloat cfg_BombusDaysHibernatedAlpha("BOMBUS_DAYSHIBERNATEDALPHA", CFG_CUSTOM, 5.0);
/** \brief If cfg_BombusDaysHibernated == false beta to define a gamma distributionof days the starting gynes has already hibernated*/
CfgFloat cfg_BombusDaysHibernatedBeta("BOMBUS_DAYSHIBERNATEDBETA", CFG_CUSTOM, 25.0);

/** \brief Mean number of starting workers for a lab colony. Defines normal distribution.*/
CfgFloat cfg_BombusNoStartingColWorkers("BOMBUS_NOSTARTINGCOLWORKERS", CFG_CUSTOM, 2.0);
/** \brief SD number of starting workers for a lab colony. Defines normal distribution.*/
CfgFloat cfg_BombusNoStartingColWorkersSD("BOMBUS_NOSTARTINGCOLWORKERSSD", CFG_CUSTOM, 0.0);

/** \brief Coeficient to modify the effect of pheromone in wax based on number of days hibernated*/
CfgFloat cfg_BombuswaxConcCoef("BOMBUS_WAXCONCCOEF", CFG_CUSTOM, -0.0004);

/**\brief The size of the starting gynes ovaries as a proportion of size.*/
CfgFloat cfg_BombusStartGynePropOvaries("BOMBUS_STARTGYNEPROPOVARIES", CFG_CUSTOM, 0.04);

/**\brief Mean amount an interaction with the queen reduces the degree minutes needed by a female larvae.*/
CfgFloat cfg_BombusminsQueenCanReduce("BOMBUS_MINSQUEENCANREDUCE", CFG_CUSTOM, 8.0);
/**\brief SD amount an interaction with the queen reduces the degree minutes needed by a female larvae.*/
CfgFloat cfg_BombusminsQueenCanReduceSD("BOMBUS_MINSQUEENCANREDUCESD", CFG_CUSTOM, 0.0);
/**\brief Number of foraging bands around colonies.*/
static CfgInt cfg_BombusForageBandNumber("BOMBUS_FORAGEBANDNUMBER", CFG_CUSTOM, 5);
/** \brief We have assumed that given the choice of mate, gynes and males would avoid mating with nestmates. But this may vary between species. Reducing this number
 * with make nestmate mating more common. 
 */
CfgInt cfg_BombusInbreedCounter("BOMBUS_INBREEDCOUNTER", CFG_CUSTOM, 1000);

/** \brief Link to the Landscape to supply information and methods without settng the landscape to a particular one*/
extern Landscape* g_landscape_p;

//---------------------------------------------------------------------------
//                         BOMBUS CONSTANTS
//---------------------------------------------------------------------------


// Assign default static member values (these will be changed later).
double Bombus_Colony::m_TempToday = -9999.0;
double Bombus_Worker::m_TempToday = -9999;
string Bombus_Recording::m_RunNo = "0";
vector<string> Bombus_Recording::m_VectString{"RunNo", "Year", "Day"};
int BombusPollenNectarLists::m_LandWidthDiv2 = 0;
int BombusPollenNectarLists::m_LandHeightDiv2 = 0;
int BombusPollenNectarLists::m_LandWidth = 0;
int BombusPollenNectarLists::m_LandHeight = 0;
vector<int> BombusPollenNectarLists::m_ForageBands = {0};
vector<double> BombusPollenNectarLists::m_ForageBandArea = {0.0};
Bombus_Colony_Manager* Bombus_Colony::m_OurColonyManager = nullptr;

//---------------------------------------------------------------------------
/**
 * \brief The Bombus population manager destructor. 
 */
Bombus_Population_Manager::~Bombus_Population_Manager()
{
	/** brief Bombus_Population_Manager needs to release the memory used for the nectar and pollen list.*/
	delete m_PollenNectarLists;
	/** \brief Bombus_Population_Manager needs to release the memory used recording the individual bees.	*/
	delete RecordingIndividuals;
	// Should all be done by the Population_Manager destructor
}

/** Initialise output file on disc and set headings and run number*/
Bombus_Recording::Bombus_Recording(const string& a_FileName, vector<string> a_headings, string a_RunNo)
{
	m_RunNo = std::move(a_RunNo);
	m_BombusRecordingFile = new ofstream(a_FileName);
	if (!(*m_BombusRecordingFile).is_open())
	{
		g_msg->Warn(WARN_FILE, "Bombus_Recording::Bombus_Recording() Unable to open file for append: ", a_FileName);
		exit(1);
	}
	RecordValues(std::move(a_headings));
}

/** Destructor for Bombus_Recording flushes final values into document, closes document and then frees the memory. */
Bombus_Recording::~Bombus_Recording()
{
	m_BombusRecordingFile->flush();
	m_BombusRecordingFile->close();
	delete m_BombusRecordingFile;
}

/** Add the string of recorded values to the document on their own line.*/
void Bombus_Recording::RecordValues(vector<string> a_values)
{
	ostream_iterator<string> output_iterator((*m_BombusRecordingFile), "\t");
	std::copy(m_VectString.begin(), m_VectString.end(), output_iterator);
	std::copy(a_values.begin(), a_values.end(), output_iterator);
	(*m_BombusRecordingFile) << '\n';
}

/** Sets the date to be used for the day in the recording file.*/
void Bombus_Recording::SetDate(int a_year, int a_day)
{
	m_VectString = {m_RunNo, to_string(a_year), to_string(a_day)};
}

//---------------------------------------------------------------------------
Bombus_Population_Manager::Bombus_Population_Manager(Landscape* L) : Population_Manager(L, 9)
{
	/** The population manager loads a list of pollen and nectar polygons.*/
	m_PollenNectarLists = new BombusPollenNectarLists(L);

	/** Loads the list of Animal Classes, the nine \a Bombus lifestages. This includes three juviniles; eggs, larvae and pupae, four adults; workers,
	 *gynes, queens and males, and two non-living classes, the colony and the clusters.  */
	m_ListNames[0] = "Colony";
	m_ListNames[1] = "Cluster";
	m_ListNames[2] = "Gynes";
	m_ListNames[3] = "Queen";
	m_ListNames[4] = "Eggs";
	m_ListNames[5] = "Larva";
	m_ListNames[6] = "Pupa";
	m_ListNames[7] = "Worker";
	m_ListNames[8] = "Male";
	m_ListNameLength = 9;
	m_population_type = TOP_Bombus;

	/** Loads the name of the simulation set to Bombus Simulation.*/
	m_SimulationName = "Bombus Simulation";

	/** Reads in file to assign a number of possible colonies in a patch*/
	m_OurColonyManager.InitDensityBombusColonies();
	/** The updates the probability of founding a colony in each patch based on number of colonies that can go in patch and how many there are.*/
	m_OurColonyManager.UpdateDensityBombusColonies();


	/**
	* A few objects, such as Nectar, Bombus_Colony are temporarily created here just to set the specific parameter values.
	* It is preferred that this is done here rather than the constructure for parameters since these values do not cange (static variables ) and only need to be set once.
	*/

	Nectar bonectar;
	bonectar.SetStatic();

	Pollen bopollen;
	bopollen.SetStatic();

	Food bofood;
	bofood.SetStatic();

	Stomach bostomach;
	bostomach.SetStatic();


	Bombus_Colony bocolony(0, 0, m_TheLandscape, this, 0, toColonyLoc_Foo);
	bocolony.SetLabTemp();
	bocolony.SetStatic();

	//int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, int a_NumberYoung, Bombus_Colony* a_Colony = 0
	Bombus_Cluster bocluster(0, 0, m_TheLandscape, this, 0, &bocolony);
	bocluster.SetStatic();

	Mass bomass;
	bomass.SetStatic();

	Bombus_Base bobase(0, 0, m_TheLandscape, this, &bocolony);
	bobase.SetStatic();

	Bombus_Egg boegg(0, 0, m_TheLandscape, this, 0, 0, 0, 0, &bocolony, nullptr);
	boegg.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Colony, a_Cluster
	Bombus_Larva bolarva(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, &bocolony, nullptr);
	bolarva.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_Colony, a_Cluster
	Bombus_Pupa bopupa(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 0, 0, &bocolony, nullptr, false, 0.0);
	bopupa.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_OvaryProp, a_Colony, a_id
	Bombus_Worker boWorker(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 200.0, 0.0, 0, &bocolony, 0);
	boWorker.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_OvaryProp, a_Colony, a_id, a_SpermID
	Bombus_Gyne boGyne(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 0, 600.0, 0, 0, &bocolony, 0, 0);
	boGyne.SetStatic();

	//a_x, a_y, p_L, p_BPM, a_MotherID, a_FatherID, a_age, a_Mass, a_BodyFat, a_OvaryProp, a_Colony, a_id, a_SpermID
	Bombus_Queen boQueen(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 0, 800.0, 0, 0, &bocolony, 0, 0);
	boQueen.SetStatic();


	Bombus_Male boMale(0, 0, m_TheLandscape, this, 0, 0, 0, 0, 0, 400.0, 0, &bocolony, 0);
	boMale.SetStatic();


	/**
	 * The first gynes and any starting workers can have values that would normally emerge if born into the simulation set based on a distribution. 
	 * This is similar to a Bayesian prior. These distributions are set prior to the creation of the first animals in the simulation.
	 */


	auto StartGyneMass = static_cast<double>(cfg_BombusStartGyneMass.value());
	auto StartGyneMassSD = static_cast<double>(cfg_BombusStartGyneMassSD.value());

	string Variables = to_string(StartGyneMass) + " " + to_string(StartGyneMassSD);
	probability_distribution StartGyneMassDistribution{"NORMAL", Variables};


	auto StartGyneBodyFat = static_cast<double>(cfg_BombusStartGyneBodyFat.value());
	auto StartGyneBodyFatSD = static_cast<double>(cfg_BombusStartGyneBodyFatSD.value());
	string Variables2 = to_string(StartGyneBodyFat) + " " + to_string(StartGyneBodyFatSD);
	probability_distribution StartGyneBodyFatDistribution{"NORMAL", Variables2};


	auto waxConcInter = static_cast<double>(cfg_BombuswaxConcInter.value());
	auto waxConcInterSD = static_cast<double>(cfg_BombuswaxConcInterSD.value());
	string Variables3 = to_string(waxConcInter) + " " + to_string(waxConcInterSD);
	probability_distribution waxConcInterDistribution{"NORMAL", Variables3};

	auto DaysHibernatedAlpha = static_cast<double>(cfg_BombusDaysHibernatedAlpha.value());
	auto DaysHibernatedBeta = static_cast<double>(cfg_BombusDaysHibernatedBeta.value());
	string Variables4 = to_string(DaysHibernatedAlpha) + " " + to_string(DaysHibernatedBeta);
	probability_distribution DaysHibernatedDistribution{"GAMMA", Variables4};


	auto minsQueenCanReduceMean = static_cast<double>(cfg_BombusminsQueenCanReduce.value());
	auto minsQueenCanReduceSD = static_cast<double>(cfg_BombusminsQueenCanReduceSD.value());
	string Variables5 = to_string(minsQueenCanReduceMean) + " " + to_string(minsQueenCanReduceSD);
	probability_distribution minsQueenCanReduceDistribution{"NORMAL", Variables5};

	auto StartGynePropOvaries = static_cast<double>(cfg_BombusStartGynePropOvaries.value());


	// Create some animals

	struct_Bombus* sp;
	sp = new struct_Bombus;

	sp->BPM = this;
	sp->L = m_TheLandscape;


	auto StartingColWorkers = static_cast<double>(cfg_BombusNoStartingColWorkers.value());
	auto StartingColWorkersSD = static_cast<double>(cfg_BombusNoStartingColWorkersSD.value());

	string Variables6 = to_string(StartingColWorkers) + " " + to_string(StartingColWorkersSD);
	probability_distribution StartingColWorkersDistribution{"NORMAL", Variables6};


	CfgInt cfg_BombusSexGenes("BOMBUS_SEX_GENES", CFG_CUSTOM, 24);

	/** A number of starting gynes are added to the simulation.*/
	int NoStartingGyne = cfg_BombusNoStartingGyne.value();
	for (int i = 0; i < NoStartingGyne; i++) // This will need to be an input variable (config)
	{
		/** They are created withing the simulation in hibernation apprpriate locations. */
		sp->x = random(SimW);
		sp->y = random(SimH);

		while (!HibernationHabitat(sp->x, sp->y))
		{
			sp->x = random(SimW);
			sp->y = random(SimH);
		}


		/** Each of the new gynes have: a random ID for the mother and father. */
		sp->MotherID = nextID();
		sp->FatherID = nextID();
		/** age of the starting gynes, */
		sp->age = cfg_BombusStartGyneAge.value();
		/** \brief and mass and body fat weight in grams from the distributions. */
		sp->Mass = StartGyneMassDistribution.get();
		sp->BodyFat = StartGyneBodyFatDistribution.get();
		/** These starting gynes are not callow. */
		sp->Callow = false;

		/** They are already hibernating and have hibernated for a number of days, either a fixed number or from a distribution. */
		if (cfg_BombusDaysHibernatedFix.value())
		{
			sp->DaysHibernated = cfg_BombusDaysHibernatedFixedVal.value() * 144;
		}
		else
		{
			sp->DaysHibernated = static_cast<int>(DaysHibernatedDistribution.get()) * 144;
		}
		sp->Hibernating = true;

		/** A concentration of wax pheremone is determined for each gyne based on a distribution and number of days hibernated. */
		double ConcValue = cfg_BombuswaxConcCoef.value() * sp->DaysHibernated + waxConcInterDistribution.get();
		sp->waxConcInter = ConcValue;
		/** Alternativly to the wax pheremone controling larval development, the queen may reduce developement time by contact, in which case an amount a queen reduces
		 * the development time per interaction is added. 
		 */
		sp->minsQueenCanReduce = minsQueenCanReduceDistribution.get();

		/** Each has a proportion of ovaries, although this may be zero. */
		sp->PropOvaries = StartGynePropOvaries;
		/** They are not a member of a colony.*/
		sp->Colony = nullptr;
		/** They have an individual ID assigned to themselves and to the male whos sperm they are carrying. */
		sp->m_id = nextID();
		sp->SpermID = nextID();
		/** In addition to the ID of the individual and mate, the gynes have two sex alleles and one for their mate that they pass on to offspring.
		 * The gyne cannot be inbred themselves (two matching copies of their sex allele or they would have emerged as a diploid male. 
		 */
		sp->SexLocus_1 = random(cfg_BombusSexGenes.value()) + 1;
		int tempRandomSexLocus = random(cfg_BombusSexGenes.value()) + 1;
		while (sp->SexLocus_1 == tempRandomSexLocus)
		{
			tempRandomSexLocus = random(cfg_BombusSexGenes.value()) + 1;
		}
		sp->SexLocus_2 = tempRandomSexLocus;
		sp->MatesSexLocus = random(cfg_BombusSexGenes.value()) + 1;

		/** In addition to the starting gyne, the simulation can also be started with a number of workers in the colony once the gyne creates one. */
		sp->StartingColWorkers = static_cast<int>(round(StartingColWorkersDistribution.get()));
		/** All starting gynes and workers are labeled as a starting individual, so that there starting values can be destinguished from the the emergeny
		 * values of future generations. 
		 */
		sp->StartingIndiv = true;

		if (NoStartingGyne == 1)
		{
			cout << "Single colony run: \n";
			cout << "Colony x: " << sp->x << "\n";
			cout << "Colony y: " << sp->y << "\n";
			cout << "Gynes sex locus 1: " << sp->SexLocus_1 << "\n";
			cout << "Gynes sex locus 2: " << sp->SexLocus_2 << "\n";
			cout << "Mates sex locus: " << sp->MatesSexLocus << "\n";
		}
		CreateObjects(to_Gyne, nullptr, sp, 1);
	}
	delete sp;

	/** To allow each individual adult to report its characteristics, the population manager creates an output text files that is kept open until the end of the simulation.*/
	RecordingIndividuals = new Bombus_Recording(cfg_BombusOutput.value(), {
		                                            "Colony",
		                                            "ID",
		                                            "MothersID",
		                                            "FathersID",
		                                            "Identity",
		                                            "Age",
		                                            "StageAge",
		                                            "Mass",
		                                            "Starting",
		                                            "BackgroundTemp",
		                                            "ColonyTemp"
	                                            }, to_string(cfg_BombusRunNo.value()));
}

//---------------------------------------------------------------------------
void Bombus_Population_Manager::CreateObjects(int ob_type, TAnimal* /*pvo*/, struct_Bombus* data, int number)
{
	/** CreateObjects allows for the creation of any living bumble bee withing the simulation (not the colony or clusters which have their own creators). */
	Bombus_Egg* new_Bombus_Egg;
	Bombus_Larva* new_Bombus_Larva;
	Bombus_Pupa* new_Bombus_Pupa;
	Bombus_Worker* new_Bombus_Worker;
	Bombus_Gyne* new_Bombus_Gyne;
	Bombus_Queen* new_Bombus_Queen;
	Bombus_Male* new_Bombus_Male;
	for (int i = 0; i < number; i++)
	{
		switch (ob_type)
		{
		case to_Egg:
			//int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID, unsigned long a_FatherID
			if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Egg = dynamic_cast<Bombus_Egg*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
				new_Bombus_Egg->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                       data->SexLocus_1, data->SexLocus_2, data->Colony, data->Cluster);
				new_Bombus_Egg->SetMotherID(data->MotherID);
				new_Bombus_Egg->SetFatherID(data->FatherID);
				new_Bombus_Egg->SetColony(data->Colony);
				new_Bombus_Egg->SetCluster(data->Cluster);
				new_Bombus_Egg->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Egg->SetSexLocus_2(data->SexLocus_2);

				data->Cluster->AddEgg(new_Bombus_Egg);
			}
			else
			{
				new_Bombus_Egg = new Bombus_Egg(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                                data->SexLocus_1, data->SexLocus_2, data->Colony, data->Cluster);
				PushIndividual(ob_type, new_Bombus_Egg);
				new_Bombus_Egg->SetMotherID(data->MotherID);
				new_Bombus_Egg->SetFatherID(data->FatherID);
				new_Bombus_Egg->SetColony(data->Colony);
				new_Bombus_Egg->SetCluster(data->Cluster);
				new_Bombus_Egg->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Egg->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddEgg(new_Bombus_Egg);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Larva:
			//int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID, unsigned long a_FatherID, double a_age
			if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Larva = dynamic_cast<Bombus_Larva*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
				new_Bombus_Larva->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                         data->SexLocus_1, data->SexLocus_2, data->age, data->Colony, data->Cluster);
				new_Bombus_Larva->SetMotherID(data->MotherID);
				new_Bombus_Larva->SetFatherID(data->FatherID);
				new_Bombus_Larva->SetAge(data->age);
				new_Bombus_Larva->SetColony(data->Colony);
				new_Bombus_Larva->SetCluster(data->Cluster);
				new_Bombus_Larva->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Larva->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddLarva(new_Bombus_Larva);
			}
			else
			{
				new_Bombus_Larva = new Bombus_Larva(data->x, data->y, data->L, data->BPM, data->MotherID,
				                                    data->FatherID, data->SexLocus_1, data->SexLocus_2, data->age,
				                                    data->Colony, data->Cluster);
				PushIndividual(ob_type, new_Bombus_Larva);
				new_Bombus_Larva->SetMotherID(data->MotherID);
				new_Bombus_Larva->SetFatherID(data->FatherID);
				new_Bombus_Larva->SetAge(data->age);
				new_Bombus_Larva->SetColony(data->Colony);
				new_Bombus_Larva->SetCluster(data->Cluster);
				new_Bombus_Larva->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Larva->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddLarva(new_Bombus_Larva);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Pupa:
			//int a_x, int a_y, Landscape* p_L, Bombus_Population_Manager* p_BPM, unsigned long a_MotherID, unsigned long a_FatherID, double a_age, double a_Mass, double a_BodyFat
			if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Pupa = dynamic_cast<Bombus_Pupa*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
				new_Bombus_Pupa->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                        data->SexLocus_1, data->SexLocus_2, data->age, data->Mass, data->BodyFat,
				                        data->Colony, data->Cluster, data->IsGyne, data->PupaDM);
				new_Bombus_Pupa->SetMotherID(data->MotherID);
				new_Bombus_Pupa->SetFatherID(data->FatherID);
				new_Bombus_Pupa->SetAge(data->age);
				new_Bombus_Pupa->SetMass(data->Mass);
				new_Bombus_Pupa->SetColony(data->Colony);
				new_Bombus_Pupa->SetCluster(data->Cluster);
				new_Bombus_Pupa->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Pupa->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddPupa(new_Bombus_Pupa);
			}
			else
			{
				new_Bombus_Pupa = new Bombus_Pupa(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                                  data->SexLocus_1, data->SexLocus_2, data->age, data->Mass,
				                                  data->BodyFat, data->Colony, data->Cluster, data->IsGyne,
				                                  data->PupaDM);
				PushIndividual(ob_type, new_Bombus_Pupa);
				new_Bombus_Pupa->SetMotherID(data->MotherID);
				new_Bombus_Pupa->SetFatherID(data->FatherID);
				new_Bombus_Pupa->SetAge(data->age);
				new_Bombus_Pupa->SetMass(data->Mass);
				new_Bombus_Pupa->SetColony(data->Colony);
				new_Bombus_Pupa->SetCluster(data->Cluster);
				new_Bombus_Pupa->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Pupa->SetSexLocus_2(data->SexLocus_2);
				data->Cluster->AddPupa(new_Bombus_Pupa);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Worker:
			if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Worker = dynamic_cast<Bombus_Worker*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
				new_Bombus_Worker->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                          data->SexLocus_1, data->SexLocus_2, data->age, data->Mass, data->BodyFat,
				                          data->PropOvaries, data->Colony, data->m_id);
				new_Bombus_Worker->SetMotherID(data->MotherID);
				new_Bombus_Worker->SetFatherID(data->FatherID);
				new_Bombus_Worker->SetAge(data->age);
				new_Bombus_Worker->SetMass(data->Mass);
				new_Bombus_Worker->SetColony(data->Colony);
				new_Bombus_Worker->SetMyID(data->m_id);
				new_Bombus_Worker->SetAgeDegrees(data->AgeDM);
				new_Bombus_Worker->SetCallow(data->Callow);
				new_Bombus_Worker->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Worker->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Worker->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Worker->AddToWarmAccount();
			}
			else
			{
				new_Bombus_Worker = new Bombus_Worker(data->x, data->y, data->L, data->BPM, data->MotherID,
				                                      data->FatherID, data->SexLocus_1, data->SexLocus_2, data->age,
				                                      data->Mass, data->BodyFat, data->PropOvaries, data->Colony,
				                                      data->m_id);
				PushIndividual(ob_type, new_Bombus_Worker);
				new_Bombus_Worker->SetMotherID(data->MotherID);
				new_Bombus_Worker->SetFatherID(data->FatherID);
				new_Bombus_Worker->SetAge(data->age);
				new_Bombus_Worker->SetMass(data->Mass);
				new_Bombus_Worker->SetColony(data->Colony);
				new_Bombus_Worker->SetMyID(data->m_id);
				new_Bombus_Worker->SetAgeDegrees(data->AgeDM);
				new_Bombus_Worker->SetCallow(data->Callow);
				new_Bombus_Worker->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Worker->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Worker->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Worker->AddToWarmAccount();
			}

			if (data->Callow == false)
			{
				data->Colony->m_ActiveColonyWorkers.push_back(new_Bombus_Worker);
			}


			IncLiveArraySize(ob_type);
			break;
		case to_Gyne:
			if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Gyne = dynamic_cast<Bombus_Gyne*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
				new_Bombus_Gyne->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                        data->SexLocus_1, data->SexLocus_2, data->MatesSexLocus, data->age, data->Mass,
				                        data->BodyFat, data->PropOvaries, data->Colony, data->m_id, data->SpermID);
				new_Bombus_Gyne->SetMotherID(data->MotherID);
				new_Bombus_Gyne->SetFatherID(data->FatherID);
				new_Bombus_Gyne->SetAge(data->age);
				new_Bombus_Gyne->SetMass(data->Mass);
				new_Bombus_Gyne->SetBodyFat(data->BodyFat);
				new_Bombus_Gyne->SetColony(data->Colony);
				new_Bombus_Gyne->SetMyID(data->m_id);
				new_Bombus_Gyne->SetSpermID(data->SpermID);
				new_Bombus_Gyne->SetStepsHibernated(data->DaysHibernated);
				new_Bombus_Gyne->SetChemicalConc(data->waxConcInter);
				new_Bombus_Gyne->SetminsQueenCanReduce(data->minsQueenCanReduce);
				new_Bombus_Gyne->SetCallow(data->Callow);
				new_Bombus_Gyne->SetStartingColWorkers(data->StartingColWorkers);
				new_Bombus_Gyne->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Gyne->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Gyne->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Gyne->SetMateSexAllele(data->MatesSexLocus);
				new_Bombus_Gyne->SetHibernating(data->Hibernating);
			}
			else
			{
				new_Bombus_Gyne = new Bombus_Gyne(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                                  data->SexLocus_1, data->SexLocus_2, data->MatesSexLocus, data->age,
				                                  data->Mass, data->BodyFat, data->PropOvaries, data->Colony,
				                                  data->m_id, data->SpermID);
				PushIndividual(ob_type, new_Bombus_Gyne);
				new_Bombus_Gyne->SetMotherID(data->MotherID);
				new_Bombus_Gyne->SetFatherID(data->FatherID);
				new_Bombus_Gyne->SetAge(data->age);
				new_Bombus_Gyne->SetMass(data->Mass);
				new_Bombus_Gyne->SetBodyFat(data->BodyFat);
				new_Bombus_Gyne->SetColony(data->Colony);
				new_Bombus_Gyne->SetMyID(data->m_id);
				new_Bombus_Gyne->SetSpermID(data->SpermID);
				new_Bombus_Gyne->SetStepsHibernated(data->DaysHibernated);
				new_Bombus_Gyne->SetChemicalConc(data->waxConcInter);
				new_Bombus_Gyne->SetminsQueenCanReduce(data->minsQueenCanReduce);
				new_Bombus_Gyne->SetCallow(data->Callow);
				new_Bombus_Gyne->SetStartingColWorkers(data->StartingColWorkers);
				new_Bombus_Gyne->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Gyne->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Gyne->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Gyne->SetMateSexAllele(data->MatesSexLocus);
				new_Bombus_Gyne->SetHibernating(data->Hibernating);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Queen:
			if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Queen = dynamic_cast<Bombus_Queen*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
				new_Bombus_Queen->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                         data->SexLocus_1, data->SexLocus_2, data->MatesSexLocus, data->age, data->Mass,
				                         data->BodyFat, data->PropOvaries, data->Colony, data->m_id, data->SpermID);
				new_Bombus_Queen->SetMotherID(data->MotherID);
				new_Bombus_Queen->SetFatherID(data->FatherID);
				new_Bombus_Queen->SetAge(data->age);
				new_Bombus_Queen->SetMass(data->Mass);
				new_Bombus_Queen->SetBodyFat(data->BodyFat);
				new_Bombus_Queen->SetColony(data->Colony);
				new_Bombus_Queen->SetMyID(data->m_id);
				new_Bombus_Queen->SetSpermID(data->SpermID);
				new_Bombus_Queen->SetStepsHibernated(data->DaysHibernated);
				new_Bombus_Queen->SetChemicalConc(data->waxConcInter);
				new_Bombus_Queen->SetminsQueenCanReduce(data->minsQueenCanReduce);
				new_Bombus_Queen->SetCallow(false);
				new_Bombus_Queen->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Queen->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Queen->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Queen->SetMateSexAllele(data->MatesSexLocus);
				new_Bombus_Queen->AddToWarmAccount();
				data->Colony->SetColoniesQueen(new_Bombus_Queen);
			}
			else
			{
				new_Bombus_Queen = new Bombus_Queen(data->x, data->y, data->L, data->BPM, data->MotherID,
				                                    data->FatherID, data->SexLocus_1, data->SexLocus_2,
				                                    data->MatesSexLocus, data->age, data->Mass, data->BodyFat,
				                                    data->PropOvaries, data->Colony, data->m_id, data->SpermID);
				PushIndividual(ob_type, new_Bombus_Queen);
				new_Bombus_Queen->SetMotherID(data->MotherID);
				new_Bombus_Queen->SetFatherID(data->FatherID);
				new_Bombus_Queen->SetAge(data->age);
				new_Bombus_Queen->SetMass(data->Mass);
				new_Bombus_Queen->SetBodyFat(data->BodyFat);
				new_Bombus_Queen->SetColony(data->Colony);
				new_Bombus_Queen->SetMyID(data->m_id);
				new_Bombus_Queen->SetSpermID(data->SpermID);
				new_Bombus_Queen->SetStepsHibernated(data->DaysHibernated);
				new_Bombus_Queen->SetChemicalConc(data->waxConcInter);
				new_Bombus_Queen->SetminsQueenCanReduce(data->minsQueenCanReduce);
				new_Bombus_Queen->SetCallow(false);
				new_Bombus_Queen->SetStartingIndiv(data->StartingIndiv);
				new_Bombus_Queen->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Queen->SetSexLocus_2(data->SexLocus_2);
				new_Bombus_Queen->SetMateSexAllele(data->MatesSexLocus);
				new_Bombus_Queen->AddToWarmAccount();
				data->Colony->SetColoniesQueen(new_Bombus_Queen);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_Male:
			if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
			{
				// We need to reuse an object
				new_Bombus_Male = dynamic_cast<Bombus_Male*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
				new_Bombus_Male->ReInit(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                        data->SexLocus_1, data->SexLocus_2, data->age, data->Mass, data->BodyFat,
				                        data->Colony, data->m_id);
				new_Bombus_Male->SetMotherID(data->MotherID);
				new_Bombus_Male->SetFatherID(data->FatherID);
				new_Bombus_Male->SetAge(data->age);
				new_Bombus_Male->SetColony(data->Colony);
				new_Bombus_Male->SetMyID(data->m_id);
				new_Bombus_Male->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Male->SetSexLocus_2(data->SexLocus_2);
			}
			else
			{
				new_Bombus_Male = new Bombus_Male(data->x, data->y, data->L, data->BPM, data->MotherID, data->FatherID,
				                                  data->SexLocus_1, data->SexLocus_2, data->age, data->Mass,
				                                  data->BodyFat, data->Colony, data->m_id);
				PushIndividual(ob_type, new_Bombus_Male);
				new_Bombus_Male->SetMotherID(data->MotherID);
				new_Bombus_Male->SetFatherID(data->FatherID);
				new_Bombus_Male->SetAge(data->age);
				new_Bombus_Male->SetColony(data->Colony);
				new_Bombus_Male->SetMyID(data->m_id);
				new_Bombus_Male->SetSexLocus_1(data->SexLocus_1);
				new_Bombus_Male->SetSexLocus_2(data->SexLocus_2);
			}
			IncLiveArraySize(ob_type);
			break;
		default:
			m_TheLandscape->Warn("Bombus_Population_Manager::CreateObjects() unknown object type - ",
			                     to_string(ob_type));
			exit(1); // NOLINT(concurrency-mt-unsafe)
		}
	}
}

//---------------------------------------------------------------------------
Bombus_Colony* Bombus_Population_Manager::CreateColony(int ob_type, struct_Bombus* data, int number)
{
	/** CreateColony creates a \a Bombus colony created by a gyne at her location. The colony is identified by the gyne/queens ID, but this can be changes by an invading gyne.*/
	Bombus_Colony* new_Bombus_Colony;
	if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
	{
		// We need to reuse an object
		new_Bombus_Colony = dynamic_cast<Bombus_Colony*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
		new_Bombus_Colony->ReInit(data->x, data->y, data->L, data->BPM, data->m_id, data->ColonyLocation);
		new_Bombus_Colony->SetColonyID(data->m_id);
	}
	else
	{
		new_Bombus_Colony = new Bombus_Colony(data->x, data->y, data->L, data->BPM, data->m_id, data->ColonyLocation);
		PushIndividual(ob_type, new_Bombus_Colony);
		new_Bombus_Colony->SetColonyID(data->m_id);
	}
	IncLiveArraySize(ob_type);

	if (new_Bombus_Colony->AccessForaging)
	{
		/** Once the colony is created it needs to register itself with the polygon pollen and nectar data structures */
		m_PollenNectarLists->AddColony(new_Bombus_Colony);
	}


	return new_Bombus_Colony;
}

//--------------------------------------------------------------------------------------------------------------------------------
Bombus_Cluster* Bombus_Population_Manager::CreateCluster(int ob_type, struct_Bombus* data, int NumberYoung)
{
	/** A queen or worker laying an egg also creates a cluster with a number of eggs in it. */
	Bombus_Cluster* new_Bombus_Cluster;
	if (static_cast<unsigned>(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type))
	{
		// We need to reuse an object
		new_Bombus_Cluster = dynamic_cast<Bombus_Cluster*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
		new_Bombus_Cluster->ReInit(data->x, data->y, data->L, data->BPM, NumberYoung, data->Colony);
	}
	else
	{
		new_Bombus_Cluster = new Bombus_Cluster(data->x, data->y, data->L, data->BPM, NumberYoung, data->Colony);
		PushIndividual(ob_type, new_Bombus_Cluster);
	}
	IncLiveArraySize(ob_type);
	return new_Bombus_Cluster;
}

//--------------------------------------------------------------------------------------------------------------------------------
unsigned long Bombus_Population_Manager::nextID()
{
	/** This increments the latest ID of all adults or parents of adults in the simulation by one and supplies that ID.*/
	return currentID++;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Population_Manager::CalculateTemp()
{
	/** The \a Bombus simulation runs on a ten munite time scale. Temperature in the simulation is very important, but at a landscape scale the temperature
	 * is only given as minimum, mean and maximum for the day. This function calculates the temperature in a ten minute time step by assuming that the temperature fluctatates
	 * between minimum and maximum based on a cosine wave with the minimum at 2am and the maximum at 2pm. 
	 */
	double TimeHours = static_cast<double>(Time) / 60;
	double min;
	double max;
	/** As the temperature is given for a calendar day, based on the time of day the minimum or maximum temperauture could be from yesterday or tommorow when making these calculations.  */
	if (TimeHours < 14)
	{
		min = m_TheLandscape->SupplyMinTemp();
	}
	else
	{
		min = m_TheLandscape->SupplyMinTempTomorrow();
	}


	if (TimeHours < 2)
	{
		max = m_TheLandscape->SupplyMaxTempYesterday();
	}
	else
	{
		max = m_TheLandscape->SupplyMaxTemp();
	}

	double CosineWave = 1 - cos((TimeHours * 15 - 30) * PI / 180);


	m_10minTemp = (max - min) * CosineWave / 2 + min;
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Population_Manager::DoFirst()
{
	/**
	* The Bombus_Population_Manager::DoFirst calculates the time in minutes and runs the ten minute temperature calculation (Bombus_Population_Manager::CalculateTemp).
	*/
	Time = g_date->GetHour() * 60 + g_date->GetMinute();

	CalculateTemp();
	/** It also determins if it is daylight or not and sets this as a flag IsDaylight. This is also where gynes and males are mated in the landscape \a Bombus model. */
	if (Time >= g_date->SunRiseTime() && Time < g_date->SunSetTime())
	{
		IsDaylight = true;

		/** If there are virgin gynes or searching males, then one of each is selected and mated. */
		if (!m_Virgin_Gynes.empty() && !m_Searching_Males.empty())
		{
			int Counter = 0;

			int randomIndexG = random(static_cast<int>(m_Virgin_Gynes.size()));
			int randomIndexM = random(static_cast<int>(m_Searching_Males.size()));
			/** Gynes and males seem to be able to recognise each other and avoid mating with each other, although this is not the case for all species \cite<Foster1992>.
			 * \a B. \a terrestris wll nest with individuals from the same colony, but as they can recognise each other, we assume at a landscape level they would
			 * avoid doing so if possible as the gynes seek out mates based on pheromone trails\cite<Bogo2018>. We have added a counter, so that in landscapes with no
			 * other option they will, but this can be set to zero to turn off inbreeding avoidence. 
			 */
			while (m_Searching_Males[randomIndexM]->ReturnBirthColony() == m_Virgin_Gynes[randomIndexG]->
				ReturnBirthColony() && Counter < cfg_BombusInbreedCounter.value())
			{
				randomIndexM = random(static_cast<int>(m_Searching_Males.size()));
				Counter++;
			}

			if (m_Searching_Males[randomIndexM]->ReturnSexLocus2() == 0)
			{
				m_Virgin_Gynes[randomIndexG]->SetSpermID(m_Searching_Males[randomIndexM]->ReturnMyID());
				m_Virgin_Gynes[randomIndexG]->SetMateSexAllele(m_Searching_Males[randomIndexM]->ReturnSexLocus1());
				m_Virgin_Gynes[randomIndexG]->st_Mate();
			}
			else
			{
				m_Virgin_Gynes[randomIndexG]->KillThis();
			}
			m_Searching_Males[randomIndexM]->st_Mate();
		}
	}
	else
	{
		IsDaylight = false;
	}

	/** Once a day, certain static variables need updating for all individuals. */
	if (Time == 0)
	{
		/** The static varaible for temperature for all Bombus colonies (speed optimization) are updated.*/

		Bombus_Colony BoC(0, 0, m_TheLandscape, this, 0, toColonyLoc_Foo);
		BoC.SetLabTemp();
		/** The date and year is also set so it can be used by all when recording individual characteristics (Bombus_Recording). */
		RecordingIndividuals->SetDate(m_TheLandscape->SupplyYearNumber(), m_TheLandscape->SupplyDayInYear());
		/** When bumble bees have access to the landscape to forage, the list of pollen and nectar resources need updated daily. */
		if (BoC.AccessForaging)
		{
			m_PollenNectarLists->DoUpdate();
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Population_Manager::DoAfter()
{
}

//--------------------------------------------------------------------------------------------------------------------------------
void Bombus_Population_Manager::DoLast()
{
}


//--------------------------------------------------------------------------------------------------------------------------------
//                                 class BombusPollenNectarLists and associated classes
//--------------------------------------------------------------------------------------------------------------------------------

/** \brief Monthly pollen thresholds, 12 quantity then 12 quality. Units mg/m2  and unitless */
static CfgArray_Double cfg_BombusPollenThresholds("BOMBUS_POLLEN_THRESHOLDS", CFG_CUSTOM, 24, vector<double>{
	                                                  1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
	                                                  1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
                                                  });
//--------------------------------------------------------------------------------------------------------------------------------
/** \brief Monthly nectar thresholds, 12 quantity then 12 quality. Units mg/m2  and vol/vol */
static CfgArray_Double cfg_BombusNectarThresholds("BOMBUS_NECTAR_THRESHOLDS", CFG_CUSTOM, 24, vector<double>{
	                                                  1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
	                                                  1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
                                                  });


//--------------------------------------------------------------------------------------------------------------------------------
BombusPollenNectarLists::BombusPollenNectarLists(Landscape* a_land)
{
	/** 
	* The constructor for class BombusPollenNectarLists. Its main job is to create the list of possible pollen and nectar sources.
	* It also reads the pollen and nectar thresholds for each month into an internal list.
	*/
	m_land = a_land; // store a local ptr to the landscape
	m_LandWidth = m_land->SupplySimAreaWidth();
	m_LandHeight = m_land->SupplySimAreaHeight();
	m_LandWidthDiv2 = m_LandWidth / 2;
	m_LandHeightDiv2 = m_LandHeight / 2;
	// Get all the VegElements to our list
	list<LE*> plist = m_land->GetAllVegPolys();
	BombusPolygonNestList bpnl(nullptr);
	for (auto it = plist.begin(); it != plist.end(); ++it)
	{
		bpnl.m_polygon = (*it);
		m_PolyNestLists.push_back(bpnl);
	}
	// Read the pollen and nectar thresholds
	PollenNectarThresholds pnt;
	for (int m = 0; m < 12; m++)
	{
		pnt.m_pollenTquan = cfg_BombusPollenThresholds.value(m);
		pnt.m_pollenTqual = cfg_BombusPollenThresholds.value(m + 12);
		pnt.m_nectarTquan = cfg_BombusNectarThresholds.value(m);
		pnt.m_nectarTqual = cfg_BombusNectarThresholds.value(m + 12);
		m_PN_thresholds.push_back(pnt);
	}
	m_ForageBandNumber = cfg_BombusForageBandNumber.value();
	static CfgArray_Int cfg_BombusForageBands("BOMBUS_FORAGEBANDS", CFG_CUSTOM, m_ForageBandNumber,
	                                          vector<int>{500, 1000, 1500, 2000, 2500});
	m_ForageBands = cfg_BombusForageBands.value();
	double lastCircleArea = 0.0;
	for (int m_ForageBand : m_ForageBands)
	{
		double CurrentCircleArea = PI * m_ForageBand * m_ForageBand;


		m_ForageBandArea.push_back(1 / (CurrentCircleArea - lastCircleArea));
		lastCircleArea = CurrentCircleArea;
	}
	if (cfg_BombusForageBandNumber.value() != static_cast<int>(m_ForageBands.size()))
	{
		g_msg->Warn("BOMBUS_FORAGEBANDNUMBER does not match BOMBUS_FORAGEBANDS: ", cfg_BombusForageBandNumber.value());
		exit(1);
	}
	if (cfg_BombusForageBandNumber.value() > 10)
	{
		g_msg->Warn("In Bombus.h I have hardset the maximum number of bands as 10: ",
		            cfg_BombusForageBandNumber.value());
		exit(1);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void BombusPollenNectarLists::DoUpdate()
{
	/**
	* This trawls the list of pollen sources and alter the status as pollen nectar producing.
	* It also triggers a message to any associated colony regarding the change in status for that polygon.
	* Called once per day.
	*/
	int month = g_date->GetMonth() - 1; // returns 1-12 but we need 0-11
	for (auto it = m_PolyNestLists.begin(); it != m_PolyNestLists.end(); ++it)
	{
		/**
		* Checks each polygon to see if it reaches the current minimum pollen or nectar threshold
		*/


		bool r_qual_new = false; // New flag to be determined for resource
		//PollenNectarData pollen = (*it).m_polygon->GetPollen();
		PollenNectarData nectar = (*it).m_polygon->GetNectar();

		// Test and assign the new flags
		// if (((pollen.m_quantity >= m_PN_thresholds[month].m_pollenTquan) && (pollen.m_quality >= m_PN_thresholds[month].m_pollenTqual)) ||
		// ((nectar.m_quantity >= m_PN_thresholds[month].m_nectarTquan) && (nectar.GetNectarSugarConc() >= m_PN_thresholds[month].m_nectarTqual))) r_qual_new = true;
		/** As Bombus seems to need less pollen than nectar, we have assumed that a nectar source is more important, and pollen is then being taken from the same sources.
		 * Workers return with pollen only rarely. \cite<Goulson2002> they only return 5% of the time only with pollen, 42% with both and 53% with nectar alone. This is
		 * likely strongly context dependent. 
		 */
		if ((nectar.m_quantity >= m_PN_thresholds[month].m_nectarTquan) && (nectar.GetNectarSugarConc() >=
			m_PN_thresholds[month].m_nectarTqual))
		{
			r_qual_new = true;
		}

		// Now figure out if anything changed
		if ((*it).m_resources != r_qual_new) // Something is different
		{
			(*it).m_resources = r_qual_new;
			for (auto itt = (*it).m_NestList.begin(); itt != (*it).m_NestList.end(); ++itt)
			{
				// Calls the colony change status which will either remove this polygon from the list or adds it depending on the flags
				(*itt).m_nestptr->ChangeStatus((*it).m_resources, (*it).m_polygon, (*itt).m_distanceclass);
			}
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
void BombusPollenNectarLists::AddColony(Bombus_Colony* a_new_Bombus_Colony)
{
	/** When a new colony is created, it must be added to the list of nests and have the resource polygons added to its own list. */
	APoint c_loc = a_new_Bombus_Colony->SupplyPoint();
	int sz = static_cast<int>(m_PolyNestLists.size());
	for (int it = 0; it != sz; ++it)
	{
		APoint ap = m_PolyNestLists[it].m_polygon->GetCentroid();
		// Calculate the distance between ap and the location of the colony
		int dx = abs(static_cast<int>(c_loc.m_x - ap.m_x));
		int dy = abs(static_cast<int>(c_loc.m_y - ap.m_y));
		if (dx > m_LandWidthDiv2) dx = m_LandWidth - dx;
		if (dy > m_LandHeightDiv2) dy = m_LandHeight - dy;
		// Speeding up calculations here because sqrt(dx squared + dy squared) is not needed since we know the thresholds for distance bands
		double dist2 = (dx * dx) + (dy * dy);
		// Hard coding here for now **Jordan** you can make it flexible later.
		int distancecategory = -1; // the highest
		for (int i = 0; i < m_ForageBands.size(); i++)
		{
			if ((dist2) < m_ForageBands[i] * m_ForageBands[i])
			{
				distancecategory = i;
				break;
			}
		}


		if (distancecategory > -1)
		{
			BombusNestDistancePair pair(a_new_Bombus_Colony, distancecategory);
			// Add the data to this list
			m_PolyNestLists[it].AddNest(pair);
			// Tell the colony that it has a potential forage source
			a_new_Bombus_Colony->AddPolygon(pair.m_distanceclass, m_PolyNestLists[it].m_polygon, it);
		}
		//This should add any existing resource polygons with resources
		int month = g_date->GetMonth() - 1; // returns 1-12 but we need 0-11
		bool r_qual_new = false; // New flag to be determined for resource
		//PollenNectarData pollen = m_PolyNestLists[it].m_polygon->GetPollen();
		PollenNectarData nectar = m_PolyNestLists[it].m_polygon->GetNectar();
		// Test and assign the new flags
		// if (((pollen.m_quantity >= m_PN_thresholds[month].m_pollenTquan) && (pollen.m_quality >= m_PN_thresholds[month].m_pollenTqual)) ||
		// ((nectar.m_quantity >= m_PN_thresholds[month].m_nectarTquan) && (nectar.GetNectarSugarConc() >= m_PN_thresholds[month].m_nectarTqual))) r_qual_new = true;
		if ((nectar.m_quantity >= m_PN_thresholds[month].m_nectarTquan) && (nectar.GetNectarSugarConc() >=
			m_PN_thresholds[month].m_nectarTqual))
		{
			r_qual_new = true;
		}

		// Now figure out if anything changed
		if (r_qual_new && distancecategory > -1) // Something is different
		{
			m_PolyNestLists[it].m_resources = r_qual_new;
			a_new_Bombus_Colony->ChangeStatus(m_PolyNestLists[it].m_resources, m_PolyNestLists[it].m_polygon,
			                                  distancecategory);
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
/** \brief Removes a colony from the data structure. */
void BombusPollenNectarLists::RemoveColony(Bombus_Colony* a_Bombus_Colony, int a_index)
{
	m_PolyNestLists[a_index].RemoveNest(a_Bombus_Colony);
}

//--------------------------------------------------------------------------------------------------------------------
//    Classes associated with colony density - copied from Osmia model 2022-10-11
//--------------------------------------------------------------------------------------------------------------------
void Bombus_Colony_Manager::InitDensityBombusColonies()
{
	/** Reads in an input file and provides a max Colony number to each instance of LE* in the m_elems vector */
	array<TTypesOfLandscapeElement, tole_Foobar> tole_ref{};
	array<double, tole_Foobar> maxBombusColonies{};
	array<double, tole_Foobar> minBombusColonies{};
	fstream ifile(cfg_BombusColonyByLE_Datafile.value(), ios::in);
	if (!ifile.is_open())
	{
		g_msg->Warn("Cannot open file: ", cfg_BombusColonyByLE_Datafile.value());
		exit(1);
	}
	// Read the file tole type by tole type - here we can't rely on the order but need the tole number
	int length;
	ifile >> length;
	if (length != tole_Foobar)
	{
		g_msg->Warn("Inconsistent file length with tole_Foobar: ", static_cast<int>(tole_Foobar));
		exit(1);
	}
	// read the file
	for (int i = 0; i < length; i++)
	{
		int toleref;
		ifile >> toleref >> minBombusColonies[i] >> maxBombusColonies[i];
		tole_ref[i] = g_landscape_p->TranslateEleTypes(toleref);
		if (minBombusColonies[i] > 0) m_PossibleColonyType[tole_ref[i]] = true;
		else m_PossibleColonyType[tole_ref[i]] = false;
	}
	ifile.close();
	unsigned nopolys = g_landscape_p->SupplyNumberOfPolygons();
	BombusPolygonEntry ope(0, 0);
	m_PolyList.resize(nopolys);
	for (int e = 0; e < nopolys; e++)
	{
		ope.SetAreaAttribute(static_cast<int>(floor(0.5 + g_landscape_p->SupplyPolygonAreaVector(e))));
		ope.SetIndexAttribute(e);
		m_PolyList[e] = ope;
		TTypesOfLandscapeElement eletype = g_landscape_p->SupplyElementTypeFromVector(e);
		// first find the eletype
		int found = -1;
		for (int j = 0; j < length; j++)
		{
			if (tole_ref[j] == eletype)
			{
				found = j;
				break;
			}
		}
		if (found == -1)
		{
			g_msg->Warn("Inconsistent file data, missing tole type ref: ", eletype);
			exit(1);
		}
		// We have the ref type, so now calculate the number of Colonies and set it
		m_PolyList[e].SetMaxBombusColonies(
			minBombusColonies[found] + static_cast<double>(g_rand_uni() * (maxBombusColonies[found] - minBombusColonies[
				found])));
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
Bombus_Colony* Bombus_Colony_Manager::GetRandomColony(int a_polyindex)
{
	/** When gynes are finding patches which are full of existing nests, eventually she will attempt to steal one. This code allows a random colony to be chosen.
	   This code may also allow other cases where a random colony needs to be chosen from a location. */
	return m_PolyList[a_polyindex].ChooseRandomColony();
}

//--------------------------------------------------------------------------------------------------------------------
//    End classes associated with colony density - copied from Osmia model 2022-10-11
//--------------------------------------------------------------------------------------------------------------------

bool Bombus_Population_Manager::HibernationHabitat(int a_x, int a_y)
{
	/** Hibernation habitat has been hardwired, but could be implimented in a similar way to the colony density. It is unclear if hibernating habitat limits populations.*/
	int pindex = m_TheLandscape->SupplyPolyRefIndex(a_x, a_y);
	double VegCover;
	//switch (m_TheLandscape->SupplyElementType(pindex))
	switch (m_TheLandscape->GetOwner_tole(a_x, a_y))
	{
	// NOLINT(clang-diagnostic-switch)
	/** Currently the gynes can hibernate in the below habitats.*/
	case tole_Hedges:
	case tole_Scrub:
	case tole_RiversideTrees:
	case tole_DeciduousForest:
	case tole_MixedForest:
	case tole_ConiferousForest:
	case tole_YoungForest:
	case tole_HedgeBank:
	case tole_BeetleBank:
	case tole_Orchard:
	case tole_OrchardBand:
	case tole_Copse:
	case tole_IndividualTree:
	case tole_WoodlandMargin:
	case tole_RoadsideSlope:

		VegCover = m_TheLandscape->SupplyVegCover(pindex);
		if (g_rand_uni() >= VegCover)
		{
			return true;
		}
		break;
	/** Currently the gynes cannot hibernate in the below habitats. These can be moved up.*/
	case tole_RoadsideVerge: // NOLINT(clang-diagnostic-implicit-fallthrough)
	case tole_Railway:
	case tole_FieldBoundary:
	case tole_Marsh:
	case tole_Field:
	case tole_PermPastureLowYield:
	case tole_PermPastureTussocky:
	case tole_PermanentSetaside:
	case tole_PermPasture:
	case tole_NaturalGrassDry:
	case tole_RiversidePlants:
	case tole_PitDisused:
	case tole_StoneWall:
	case tole_Fence:
	case tole_Garden:
	case tole_Track:
	case tole_SmallRoad:
	case tole_LargeRoad:
	case tole_Building:
	case tole_ActivePit:
	case tole_Freshwater:
	case tole_River:
	case tole_Saltwater:
	case tole_Coast:
	case tole_Heath:
	case tole_UnsprayedFieldMargin:
	case tole_MownGrass:
	case tole_BareRock:
	case tole_AmenityGrass:
	case tole_Parkland:
	case tole_UrbanNoVeg:
	case tole_UrbanPark:
	case tole_BuiltUpWithParkland:
	case tole_SandDune:
	case tole_MetalledPath:
	case tole_Carpark:
	case tole_Churchyard:
	case tole_NaturalGrassWet:
	case tole_Saltmarsh:
	case tole_Stream:
	case tole_HeritageSite:
	case tole_UnknownGrass:
	case tole_Wasteland:
	case tole_WoodyEnergyCrop:
	case tole_PlantNursery:
	case tole_Pylon:
	case tole_WindTurbine:
	case tole_Vildtager:
	case tole_PermPastureTussockyWet:
	case tole_Pond:
	case tole_FishFarm:
	case tole_UrbanVeg:
	case tole_RiverBed:
	case tole_DrainageDitch:
	case tole_Canal:
	case tole_RefuseSite:
	case tole_WaterBufferZone:
	case tole_Airport:
	case tole_Portarea:
	case tole_Saltpans:
	case tole_Pipeline:
	case tole_SwampForest:
	case tole_MontadoCorkOak:
	case tole_MontadoHolmOak:
	case tole_MontadoMixed:
	case tole_AgroForestrySystem:
	case tole_CorkOakForest:
	case tole_HolmOakForest:
	case tole_OtherOakForest:
	case tole_ChestnutForest:
	case tole_EucalyptusForest:
	case tole_InvasiveForest:
	case tole_MaritimePineForest:
	case tole_StonePineForest:
	case tole_Vineyard:
	case tole_OliveGrove:
	case tole_RiceField:
	case tole_SolarPanel:
	case tole_ForestAisle:
	case tole_OOrchard:
	case tole_BushFruit:
	case tole_OBushFruit:
	case tole_ChristmasTrees:
	case tole_OChristmasTrees:
	case tole_EnergyCrop:
	case tole_OEnergyCrop:
	case tole_FarmForest:
	case tole_OFarmForest:
	case tole_PermPasturePigs:
	case tole_OPermPasturePigs:
	case tole_OPermPasture:
	case tole_OPermPastureLowYield:
	case tole_FarmYoungForest:
	case tole_OFarmYoungForest:
	case tole_AlmondPlantation:
	case tole_WalnutPlantation:
	case tole_FarmBufferZone:
	case tole_NaturalFarmGrass:
	case tole_GreenFallow:
	case tole_FarmFeedingGround:
	case tole_FlowersPerm:
	case tole_AsparagusPerm:
	case tole_MushroomPerm:
	case tole_OtherPermCrop:
	case tole_OAsparagusPerm:
	case tole_Missing:
	case tole_Chameleon:
	case tole_Foobar:
	default: ; // NOLINT(clang-diagnostic-covered-switch-default)
	}
	return false;
}
