Bombus MIDox {#Bombus_page}
=========
<h1> MIDox for an ALMaSS <i>Bombus</i> complex agent-based model </h1>
Chetcuti, J.<sup>*,1,2</sup> Groom, G.<sup>1</sup> and Topping, C.J.<sup>1</sup>

1.	Department of Ecoscience, Aarhus University, C. F. Møllers Allé 8, 8000 Aarhus C
2.	Department of Botany, School of Natural Sciences, Trinity College Dublin, Dublin, Ireland

*Corresponding author: Jordan Chetcuti, Jordan.chetcuti@ecos.au.dk


## Introduction

Vital deliverers of pollinator services, bumble bees (*Bombus* spp.) are experiencing pressure on their populations due to landscape configuration and application of pesticides. Bees are not the target of pesticide application and are beneficial to agricultural yields (\ref _r1 "Cecen et al., 2007", \ref _r2 "Cecen et al., 2008"). Therefore, landscape-scale risk assessment is necessary to test landscape management scenarios to maintain viable bee populations while allowing sustainable crop yields. The document that follows will follow the Model Implementation Documentation with Doxygen (MIDox) format, the next step in designing, documenting and calibrating complicated models after the “Formal Model” format document (\ref _r3 "Topping et al., 2022"). Our MIDox is for both the laboratory and landscape *Bombus* synthetic agent-based models specified in the (LAB *Bombus* FORMAL MODEL 2023) and (Landscape *Bombus* formal model 2023). This model was created within the ALMaSS (Animal \Landscape and Man Simulation System) framework (\ref _r4 "Topping et al., 2003") and will focus on eusocial bumble bees generally, but *Bombus terrestris* in the first instance.
## Aims & Purposes
Development of the *Bombus* model was split into two phases that persist as two modes in the model. These two modes are, a simulation of bumble bees in the laboratory and of free foraging and nesting bees and colonies. This was done because life within bumble bee colonies is complex with multiple timings and interactions and much of the knowledge that we have about bumble bees comes from laboratory studies. We also recognised that the first step in simulating other species than B. terrestris will be in trying to match other species kept in constant laboratory conditions, therefore the laboratory version of the simulation has longevity of use and is worthwhile in its own right. The landscape simulation takes the laboratory model, and bookends it with gynes (potential future queens) with the solitary life stage of bumble bees as well as allowing the queen and workers to forage to provision the colony.

The simulation will be used to better understand bumble bees generally and to make predictions of the impact of different landscape and agricultural managements in space and time on bumble bees. *Bombus terrestris* is a common species that is likely more robust to landscape pressures than other *Bombus* species but is used as it is the species for which we have the most data. Other species within the genus may be able to better represent the effect of pressures on bumble bees. We can achieve this within the model by changing variables to make a pseudo species that is more sensitive, and eventually re-calibration for other *Bombus* species once more data is available.
## Model in brief
ALMaSS is implemented in C++ using object orientation. The Bombus model is built into the existing ALMaSS implementation alongside multiple other species models (\ref _r4 "Topping et al., 2003"). The Bombus model implements a Bombus population manager (Bombus_Population_Manager) which inherits from Population_Manager to which each life stage is added within a vector. Bombus life stages are of type TAnimal of type TALMaSSObject. For economy of memory usage, TALMaSSObject are recycled once created, reusing dead life stages when they are available instead of creating new objects in memory.

In the laboratory mode the laboratory colony starts with a colony kept at known daily temperatures. The colony is also fed nectar and pollen and the individuals do not have access to the outside to forage or disperse. The laboratory model only runs for a year or less.

The landscape model includes the solitary life of the *Bombus* gynes (potential future queens). Each gyne hibernates, forages and searches for a colony site. Once the gyne finds suitable habitat it then founds a colony, moves on if the site is full or steals a colony. Once the gyne has become a queen in their new colony the model runs very much like the laboratory model, but with the queen and workers foraging and then males and gynes leaving the colony. Males and gynes mate, and the gynes forage, search for a hibernation site and return to hibernation. The landscape model then repeats this cycle for multiple years. In the landscape, the bees interact with the ALMaSS landscape models of weather data, crop rotations and management, vegetation growth models and other landscape elements. Additionally, the bumble bee models access dynamically changing pollen and nectar resources.

##	Scheduling
Each individual has a TAnimal::BeginStep, TAnimal::Step and TAnimal::EndStep. TAnimal::BeginStep and TAnimal::EndStep are only called once per step, while Step can be called repeatedly until the step is specified as being over (when TAnimal::EndStep is then called). The Step is where the relevant states from Table 1 are translated into a corresponding action.

The ALMaSS landscape is setup first and then this is passed to the Bombus_Population_Manager. Static variables for each of the life stages are set. A number of gynes (Bombus_Gyne) are created within the landscape at random locations within suitable hibernation habitat. This is done even in the laboratory mode, even though it is not needed. The starting gynes are the first individuals added to the list of living and dead individuals (Population_Manager::TheArray). Dead individuals are recycled within the model to save the cost of creating a new individual. All new individuals are added to or recycle from the list. The first gynes are the only ones randomly placed into the landscape. These gynes are given information for parents and a mate that never existed in the model. Gynes are given two different copies of the sex allele. If they had been given two of the same, they would have been male and died in the summer before the model started.

Having been created, the first gynes are not in the colony and pass a logical branch that would take precedence if they were in the colony. If the model is being run in laboratory mode (Bombus_Base::ColonyLocation == \ref TTypeOfBombusState -> toColonyLoc_BoxLab) the individual immediately returns to the state of founding a colony (\ref TTypeOfBombusState -> toBombusS_FoundColony).

In landscape mode the model starts with gynes having not finished hibernating. They will each awake from hibernation if they have both hibernated long enough and the temperature is warm enough. Otherwise the gynes will continue to the hibernate state (\ref TTypeOfBombusState -> toBombusS_Hibernate) which directs to Bombus_Gyne::st_Hibernate which increments the duration hibernated and ends the Step. Upon awaking from hibernation, if it is daylight in the model, each gyne forages (TTypeOfBombusState -> toBombusS_Forage) until they gain fat (#cfg_Bombus_PropMaxFatSearch) and a defined proportion of ovary development (#cfg_Bombus_PropMaxOvariesSearch) (each set as a parameter). Once this is reached they search for a colony location (\ref TTypeOfBombusState -> toBombusS_SearchColony). If it is not daylight, they sleep. In reality they would have to look for shelter somewhere to sleep, but we are assuming they find shelter very near wherever they are.

Having found a colony site, a gyne creates (Bombus_Gyne::st_FoundColony) a colony (Bombus_Colony) and a queen (Bombus_Queen) object with all of its properties and then dies (transitioning from gyne to queen). In laboratory colony mode, the gyne can also create a few starting colony workers before it dies if this is defined in the configuration file (#cfg_BombusNoStartingColWorkers). In landscape mode the gyne can also try and steal a colony (Bombus_Gyne::st_FoundColony).

The colony and the queen now work in tandem. Colony actions (reactions) take place in the Bombus_Colony::BeginStep. The Colony calls Bombus_Colony::st_Dying if there are no adults in the colony or with a probability of the colony dying. A random death of colony attempts to account for physical nest destruction in the field. This could be animals destroying the nest for example. In the laboratory a random death is unlikely (other than the death of the adults), so the colony can also be killed on a fixed day to represent the end of laboratory experiments. Within Bombus_Colony::BeginStep the temperature of the colony is calculated Bombus_Colony::CalculateColonyTemp. The female larval degree minutes needed for development is calculated once a day by the colony. This is done as an efficiency step rather than having every individual larva make the calculation. Nectar and pollen can be added for day, as this is vital in the laboratory, but could also take place in experimental colonies placed in the field. In the Bombus_Colony::EndStep, the colony increments age.

The queen calls Bombus_Queen::BeginStep taking care of basic bodily functions and then Bombus_Queen::Step. The first state in Bombus_Queen::Step directs to Bombus_Queen::st_Decide. Within Bombus_Queen::st_Decide the queen first takes care of her own survival, sleeping, eating (Bombus_Worker::MyProbabilityPrioritisingFood) and foraging (Bombus_Queen::DoINeedToForage). Foraging only takes place during daylight (Bombus_Population_Manager::IsDaylight) and until there are brood to care for, with sleep at night-time. Once there are broods to care for the queen sleeps based on need (Bombus_Worker::MyProbabilitySleeping and Bombus_Base::m_SleepNeeded), and again if there are no brood care tasks she needs to perform (Bombus_Base::m_SleepWanted). Once these vital actions are accounted for the queen can lay and care for broods.

Before laying the first eggs the queen first builds a nectar pot Bombus_Queen::BuildFirstNectarPot until this is complete (Bombus_Colony::FirstNectarCellComplete). If she can lay eggs (Bombus_Queen::CanLayEggs) she returns \ref TTypeOfBombusState -> toBombusS_LayEgg, which then via Step, call (Bombus_Queen::st_LayEgg. Bombus_Queen::CanLayEggs controls laying behviour; the queen does not lay eggs again to broods one or two until the larvae of those broods pupate (Bombus_Larva::Pupate) and Bombus_Colony::LarvaInColony and Bombus_Colony::EggsInColony return false. After brood two, eggs are laid if the queen is not significantly damaged (Bombus_Queen::m_damaged > Bombus_Queen::m_QueenNoLayDamageThreshold) and there are eggs left in a day to lay (#m_NumberEggsPerCell). When there are clusters (Bombus_Cluster) in the colony (Bombus_Colony::m_ColonyClusters) the queen will return (Bombus_Queen::toBombusS_Inspect) and from Bombus_Queen::Step call (Bombus_Queen::st_Inspect). If the queen has no actions to perform, the queen sleeps (Bombus_Worker::st_Sleep).

###	Inspect
Inspect (Bombus_Worker::st_Inspect) is where the queen, gynes and workers examine clusters of juveniles. A threshold of energy usage can be set in the configuration file for performing this task, and without sufficient energy a female can refuse the task. The energy requirement can be set to zero. The adult female chooses a cluster at random and takes ownership of the cluster. Different things are checked for in the eggs and larvae. Eggs are checked (Bombus_Worker::CheckClusterEggs) within the approximate first day for which adult laid them. This period is defined by a proportion of the total number of degree minutes needed to develop. The queen will always eat any eggs which are not her own (Bombus_Worker::EatAllEggs). Workers will only eat the eggs that are not their own after the competition point is reached and if they are acting aggressively. Aggressiveness is defined as when the ovaries are developed (Mass::ReportPropOvaries() >= Mass::MaxPropOvaries). Larvae are inspected to determine if they need feeding because they are signalling that they are hungry. At the same time the number of interactions with queens and workers can be recorded (Bombus_Worker::IncrementLarvaeInteractions) and the number of larvae that will develop into gynes can be recorded. The hungry larvae are then fed (Bombus_Worker::st_Feed). During inspect (Bombus_Queen::st_Inspect) the temperature of clusters is inspected and if below the optimum temperature for a cluster the probability of incubating the cluster is calculated based on how much below the temperature the cluster is (Bombus_Base::ProbV1). This means it is more likely that an individual will incubate (\ref TTypeOfBombusState -> toBombusS_Incubate -> Bombus_Worker::st_Incubate) a cluster if the cluster is at a lesser temperature below the optimum cluster temperature. During inspect, if an individual can add wax to a cluster (Bombus_Worker::CanIAddWax) they check if the individuals within the cluster have grown and need more wax added (\ref TTypeOfBombusState -> toBombusS_AddWax -> Bombus_Worker::st_AddWax). Clusters that contain only pupae (Bombus_Cluster::m_ClusterPupa) can still be incubated and killed, but there are no other actions which can be performed on the pupae.
###	Juveniles
Eggs (Bombus_Egg) and pupae (Bombus_Pupa) develop within Step and have no BegingStep. This is where mortality is modified (Bombus_Egg::MortalityStressors) and then checked leading to eggs and larvae dying probabilistically. If mortality, or the colony is dead, or there are no workers or queen then the individual dies. If an individual is over a number of degree minutes threshold the individual transitions to the next life stage (\ref TTypeOfBombusState -> toBombusS_NextStage directs to Bombus_Egg::st_Hatch and Bombus_Pupa::st_Emerge); otherwise they accumulate degree minutes of degrees on the minimum developmental threshold. They also increment minutes and timesteps.

The larvae (Bombus_Larva) are more complex in their behaviour and have a Bombus_Larva::BeginStep, Bombus_Larva::Step and Bombus_Larva::EndStep. The Bombus_Larva::BeginStep only checks if a female larva is young enough and with a high enough proportion of target degree minutes needed to develop into a gyne, at which point Bombus_Larva::m_IAmGyne is irreverably set to true. Step is only used when a larva dies. Step time is used by adult females to feed and incubate larvae within the cluster. The action of larva then take place in the EndStep with the resources they have been given.
Within Bombus_Larva::EndStep the temperature over the minimum development temperature is calculated once for use throughout the end step. Larvae can consume Food that they have been given (Bombus_Larva::m_HeldFood), but can also eat Food in the cluster if it is provided by the laying adult (Bombus_Cluster::PointToPollenLump). This switching is accomplished through pointing to the food source (Bombus_Base::PointToFoodSource) if there is food in the cluster and the individual is not holding food.

Larvae consume an amount of food (Bombus_Larva::st_Consume) and then develop (Bombus_Larva::st_Develop). The development stage is similar to that of eggs and pupae. First however Bombus_Larva::m_mortality is calculated based on the size of the larvae to allow for increased mortality of smaller larvae. The Bombus_Egg::MortalityStressors is calculated (the same as other juveniles) and then checked in the same way as for other juveniles. Additionally a hard limit can be set for larvae to kill larvae that have excessively slowed their development due to limited food. Partly this is a processing issue as the colonies can accumulate large numbers of larvae late in the colony when worker numbers are getting lower but egg laying is still happening due to paused development.

Within develop (Bombus_Larva::st_Develop) the degree minutes achived are checked, and if over the threshold the larvae pupates (Bombus_Larva::Pupate) ending the larval stage (Bombus_Larva::st_Dying). This pupation and death happen immediately without waiting for the next Bombus_Larva::BeginStep and Bombus_Larva::Step as the adults would otherwise have the opportunity to feed the larvae again and the cluster would contain larvae and pupae representing the same individuals. If the larvae have not pupated, then they grow (Bombus_Base::Grow). This growth includes the digestion of food by the stomach (Stomach::Digest), the accumulation of mass (Mass::DigestedFoodEffect) or starvation (Mass::Starvation). Finally within development (Bombus_Larva::st_Develop) as long as the larva is developing (Mass::AskIfDeveloping) and the temperature of the cluster (Bombus_Cluster::GetTemp) is over the minimum juvenile development temperature (Bombus_Base::MinJuvDevTemp), then the larvae accumulates degree minutes. The larva increments its age (Bombus_Larva::m_Age), the age of this life stage (Bombus_Larva::m_StageAge) and number of timesteps regardless of if it accumulates degree minutes.
###	Adults
Adults perform tasks in the Step, but take care of bodily functions in the Bombus_Base::BeginStep and Bombus_Base::EndStep. Within Bombus_Base::BeginStep adults calculate their body temperature (Bombus_Base::CalculateMyBodyTemp), mortality probability in the step (Bombus_Base::MortalityStressors), checks if it is going to die based on the probability, in which case it dies immediately without continuing. If alive and in a colony they add themselves to a the account of warm bodies in the colony and adds their temperature. Finally, once a day, not a step, the additional sleep needed and wanted is added to the account of sleep needed and wanted for the adult. Also, once a day the vital statistics of the individual are written to file (Bombus_Base::RecordVitals).
The Step starts with \ref TTypeOfBombusState -> toBombusS_Decide which directs to calling Bombus_Base::st_Decide. Within Bombus_Base::st_Decide decision making differs based on logical tests which lead directly to calling other functions or setting states to be passed in a subsequent iteration through Step. The Step can also be set as done (TALMaSSObject::m_StepDone).

All adults have a development phase once a step (Bombus_Base::st_Develop) called in the end step (Bombus_Base::EndStep) and increment their age in step count and day count. Workers, Gynes and the Queen use the same Bombus_Worker::EndStep in the Worker base class (Bombus_Worker) and additionally disconnect a mutual link between an adult female and a cluster under set circumstances. Individuals also reset how many times they have been dominated in the step. This is only relevant to the queen and workers.
###	Emergence
The queen looks after juvenile life stages alone until the first workers emerge from the pupal stage (Bombus_Pupa::st_Emerge) and pass a time they spend as a callow individual which is decided by the indivdual worker bumble bee within Bombus_Worker::st_Decide based on if the individual is below an amount of degree days spent as a callow. Males and gynes also control how long they are callow in exactly the same way as a worker within the Decide (e.g. Bombus_Male::st_Decide), but the callow phase constrains the different adult types differently. Workers, gynes and males can only feed themselves, sleep or end the step as callows. Once the callow phase is over males and gynes then have the possibility to leave the colony. Gynes join the workers in inspecting juvenile clusters of juveniles but without the ability to eat eggs or deposit wax.
###	Eusocial colony
Once the first workers start acting like adults, they assist the queen in taking care of the young and the colony. In the laboratory mode this only involves inspecting clusters, incubating clusters and feeding larvae. As a callow moves from being callow to adult (Bombus_Base::m_Callow from true to false) adults are added to a vector of active workers (Bombus_Colony::m_ActiveColonyWorkers) that can be directed by the colony to spend time to keep the colony cool through fanning (Bombus_Base::st_Fan) and to a colony account of warm bodies (Bombus_Base::CalculateMyBodyTemp and Bombus_Base::AddToWarmAccount) used to calculate warming of the colony. The warming account of the individual is updated during the Bombus_Base::BeginStep. Within the program the colony controls allocation of adults fanning and calculation of colony temperature (Bombus_Colony::CalculateColonyTemp) we have only done this for runtime efficiency, rather than having every adult doing the same calculations of temperature and whether or not they want to fan. This also allows the thermal dynamics of the colony to act on the physics that would be present in a real colony.

Workers and the queen interact with each other and other adults within the colony as a means for each to establish their place in the pecking order of the colony. That is, whether they are more or less dominant than other individuals, which has a direct influence on their development and behaviour. During a step in Bombus_Worker::st_Decide, Bombus_Worker::Dominate() is called a number of times specified by the configuration file (#cfg_BombusWorkerEncounteredInStep).

In the landscape mode, adults can decide to forage (\ref TTypeOfBombusState -> toBombusS_Forage) or leave (\ref TTypeOfBombusState -> toBombusS_Leave) the colony. Foraging is initiated for workers and the queen based on food in the colony. After having consumed food, the adult checks if it needs to go foraging (Bombus_Worker::DoINeedToForage). The queen and workers switches back and forth between foraging or not based on nectar in the colony. As they have just consumed prior to checking, if what they have consumed compared to what they want to consume outweighs the nectar in the colony, then they switch to foraging. They also decide if they should collect pollen if there is a minimum amount in the colony. This is more than zero pollen for a worker and an amount equal to provisioning the current brood cluster before egg laying for the queen (Bombus_Queen::PollenLumpToAdd). If a worker has become a forager a number of times defined by a value in the configuration file (#cfg_BombusForagingPermenance) the worker switches permanently to being a forager and no longer performs within colony tasks.

If a male or gyne experiences a shortage of food, they simply leave the colony before they would have done otherwise. Without a shortage of food they leave based on a probability calculated based on fat reserves for gynes and a probability for males (Bombus_Male::m_ProbMaleLeaving) (less long lived and needing fewer resources to be successful than gynes). Those foraging or leaving the colony only do so in daylight (Bombus_Population_Manager::IsDaylight) and only with access to the outside world (Bombus_Base::AccessForaging) which is denied in the laboratory mode.
###	Foraging
Workers, gynes and the queen can forage. Gynes only do forage in the model once they have left the colony, and they consume what they forage (Bombus_Gyne::st_Forage). Gynes use a correlated random walk to move across the landscape (Bombus_Gyne::st_RandomMove) foraging as they move. If they need food and there is none in their current location, they reverse their heading once (only possible when Bombus_Gyne::HeadingSwitched is false) until they consume food or return to the polygon that previously held food.

Workers and the queen use data held collectively within the model (BombusPollenNectarLists::BombusPolygonNestList) and are allocated a polygon containing nectar (Bombus_Colony::SupplyPolygon) based on the probability of encountering a patch within each of the distance bands around the colony. This method is an economisation based on computational time, as there may be a great many workers in the landscape, and if each were to perform a random walk looking for a patch of habitat, the model would run very slowly.
Foraging for the colony by the workers and the queen entail, leaving to forage (Bombus_Worker::st_Forage), time spent foraging (Bombus_Worker::st_ForagePause), returning and storing resources (Bombus_Worker::st_StoreResources) and then deciding whether to forage again (Bombus_Worker::st_ForageDecide).
###	Switching point
The model has been designed based on the premise that workers get larger as the colony ages (\ref _r5 "Shpigler et al., 2013"). We have assumed that the natural conclusion of this progression is that a tipping point is reached, and female larvae become gynes instead of workers. This is one way the colony then begins to initiate the switch to the production of sexual within the colony. The other means of switching is caused by diploid individuals having two copies of the same sex allele and therefore developing into diploid males interrupting the production of workers in the continual growth phase of worker production (\ref _r6 "Di Pietro et al., 2022").

The females within the colony gradually getting bigger within the colony may be controlled by multiple different methods. At the simplest the increase in size is controlled by the number of workers in the colony (Bombus_Colony::NoAdults) or indirectly either through the dilution of some signal left by the queen in the wax of the colony by workers, or dilution of the number of times the queen interacts with each cluster within the colony through increasing number of clusters and worker numbers. Within the model, which of these methods is used is controlled by configuration variable (#cfg_BombusLarvaDevControl). If workers number (\ref TTypeOfBombusState -> toLarvaDev_WorkerNo) or wax signal (\ref TTypeOfBombusState -> toLarvaDev_WaxSignal) is selected the degree minutes needed for a female larvae to develop is calculated in the Bombus_Colony::BeginStep, based on NoAdults or the amount of chemical signal (ReportChemConc). Otherwise, in the last option (\ref TTypeOfBombusState -> toLarvaDev_Queen), the larvae record how many times a worker or the queen interact (IncrementLarvaeInteractions) with them, and then calculate how much this will reduce their degree minutes needed to develop from the maximum within the Bombus_Larva::EndStep.

In the Bombus_Larva::BeginStep, if after a few days, determined by a number of accumulated degree minutes (Bombus_Larva::m_AgeDegrees) the amount of degree minutes (Bombus_Larva::m_DM) that will be needed to develop is some proportion (Bombus_Larva::PropDMmaxIsGyne) of the maximum degree minutes possible (Bombus_Larva::LarvalDMmax), then the larvae will be flagged as becoming a gyne (Bombus_Larva::m_IAmGyne) (if it is female).

The other means of the colony switching to reproductive individuals is controlled by diploid male production. Diploid males are created through a queen laying an egg with two identical copies of the sex allele (Bombus_Base::m_sexLocus_1 == Bombus_Base::m_sexLocus_2), one from the queen and the other from the male who’s sperm she has stored from mating (Bombus_Worker::GetMatesSexLocus). A colony that would have diploid males, should produce ~50% workers and diploid males from the beginning, but males do not appear until after the first brood (\ref _r6 "Di Pietro et al., 2022"). Therefore, in the model we have made it so that if the queen spots a diploid egg when she checks the eggs (Bombus_Queen::CheckClusterEggs), she eats it (Bombus_Worker::EatAnEgg). Eating an egg results in the return of the resources used to make the egg to the queen and removal of the egg from the colony (Bombus_Egg::Eaten), without leaving a dead egg as happens if the egg dies (Bombus_Egg::st_Dying).

Once a gyne larvae is spotted by the queen or workers (Bombus_Worker::st_Inspect), or they encounters an adult male or gyne (Bombus_Worker::Dominate), they recognise that the colony has switched to producing reproductive adults (Bombus_Base::m_CompetitionPossible). For the queen this is the point she switches to the laying of haploid male eggs and does not lay anymore diploid eggs. With no more diploid eggs laid, only those remaining can develop into workers and gynes, and then there will be no more of either produced. Workers registering that the colony has switched, is the beginning of the process that will lead to the competition point. The workers count the gyne larvae they encounter each step, and once they do not encounter anymore, if their ovaries have grown to the required size, they rebel, and the competition phase of the colony starts (Bombus_Base::m_CompetitionPossible).
###	Competition point
Workers that have developed ovaries (Mass::ReportPropOvaries and Bombus_Base::MaxPropOvaries) can lay a small number of haploid eggs every day. Workers with developed ovaries are also more aggressive towards each other and the queen. In the model this means that they inflict damage (Bombus_Base::Damaged) onto each other within the dominate behaviour (Bombus_Worker::Dominate) and that they will eat young eggs that are not their own (Bombus_Worker::EatAllEggs) when they inspect a cluster of eggs (Bombus_Worker::CheckClusterEggs). The queen also eats eggs that are not her own. If high enough, the damage (Bombus_Queen::m_QueenNoLayDamageThreshold and Bombus_Worker::m_WorkerNoLayDamageThreshold) can result in the individual going into a senescence phase (increasing mortality) and to stop laying eggs (Bombus_Worker::CanLayEggs). With no more workers being produced, and aggression within the colony causing increased mortality and an end of egg laying, the colony begins to die.
###	Colony end
In the laboratory mode colonies are often killed on a set day (#cfg_BombusColonyDeathdays). In the landscape mode the gynes and males leave and the workers and queen eventually die. Combined, the senescence of individuals, increasing number of dead in the colony, and age based mortality will result in higher mortality (Bombus_Base::MortalityStressors) and therefore bring around the end of the colony. Once there are no adults in the colony, the colony dies.

Gynes have three mechanisms that cause them to leave. They leave if there is not enough food in the colony for them to eat (similar to the trigger for foraging in workers and the queen), if there are no workers or a queen, or once they have enough fat stored (Bombus_Gyne::st_Decide). Males leave if there are no queen or workers or based on a probability of leaving (Bombus_Male::m_ProbMaleLeaving). Males are less long lived and store much less fat and therefore we have assumed that they would not have the same mechanism of needing fat before leaving. When the gynes and males leave they are added to a list of searching gynes (Bombus_Population_Manager::m_Virgin_Gynes) and males (Bombus_Population_Manager::m_Searching_Males) at the population level of the model. Diploid males are not added to the list and are killed. Males are assigned a random number of times they can mate up to a maximum (Bombus_Male::st_Leave) (\ref _r7 "Gosterit and Gurel, 2016") and removed from the colony. Spatially we do not consider males in this version of the simulation. Gynes move out from their natal colony.
###	Post colony
Outside the colony, males and gynes are mated, with a random male and gyne selected to mate (Bombus_Population_Manager::DoFirst) from the list of gynes and males that can mate (Bombus_Population_Manager::m_Virgin_Gynes and Bombus_Population_Manager::m_Searching_Males). If the pair are from the same colony, the male is rechosen based on pheromones and nest recognition (\ref _r8 "Foster, 1992", \ref _r9 "Bogo et al., 2018"). The male is redrawn a number of times (#cfg_BombusInbreedCounter) and then they mate regardless. The number of times redrawn can be set to zero to disable inbreeding avoidance. Once the male has mated the number of times it has been assigned, the male dies. The gynes forage until they have found a hibernation habitat (Bombus_Population_Manager::HibernationHabitat) and reached a proportion of their maximum fat reserves (#cfg_Bombus_FatPropEnterHib), with this gyne behaviour controlled in (Bombus_Gyne::st_Decide). Once the gynes enter hibernation, they remain in hibernation until they have hibernated for a defined minimum period (Bombus_Gyne::MinHibernationDuration) and then wake up when a hibernation body temperature is reached (HibernationWakeUpTemp).

The hibernation body temperature is calculated based on an assumption of insulation of the soil (#cfg_Bombus_HibernationInsulation) and the temperature in the 10 minute time step (Bombus_Population_Manager::Get10minTemperature) which takes place in the Bombus_Population_Manager::DoFirst (Bombus_Population_Manager::CalculateTemp). The calculation of body temperature while hibernating takes place in the base class, but only takes place for the gynes when they are hibernating (Bombus_Base::CalculateMyBodyTemp).
The landscape mode simulation repeats the above cycle for the number of years specified.

## Implementation
Each individual has a Bombus_Base::BeginStep, Bombus_Base::Step and Bombus_Base::EndStep. Bombus_Base::BeginStep and Bombus_Base::EndStep are only called once per step, while Step can be called repeatedly until the step is specified as being over (when Bombus_Base::EndStep is then called). The Bombus_Base::Step is where the relevant states from Table 1 are translated into a corresponding action.
Bumble bees utilise haplodiploidy sex determination (\ref _r10 "Duchateau et al., 1994"). With two different copies the individual is female, with two the same or only the mothers the individual is male (\ref _r11 "Duchateau and Mariën, 1995"). The two potential copies will be represented as an integer from the mother (Bombus_Base::m_sexLocus_1) and - when diploid - the fathers sex allele (Bombus_Base::m_sexLocus_2).
The model has seven bumble bee life stages:
1. Eggs (Bombus_Egg)
2. Larvae (Bombus_Larva)
3. Pupae (Bombus_Pupa)
4. Workers (Bombus_Worker)
5. Gynes (Bombus_Gyne)
6. Males (Bombus_Male)
7. Queens (Bombus_Queen)

As well as two ways of grouping bees:
1. Clusters (Bombus_Cluster)
2. Colony (Bombus_Colony)

Bees (1-7) will be based on a common Bombus_Base. Bombus_Cluster and Bombus_Colony do not, but directly from TAnimal.
Each life stage has states and can transition or be transitioned between states. These states are shown in \ref _t1 "Table 1".


\anchor _t1 Table 1. Behabioural states for different Bombus lifestages (\ref TTypeOfBombusState).

| \ref TTypeOfBombusState | Description | User |
| ------------------------ | ------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| toBombusS_InitialState | Sets the first state. | All |
| toBombusS_Die | | All |
| toBombusS_Warm | Incubating adults set and then alter the temperature of the cluster | Clusters |
| toBombusS_Cool | Passive adults return to colony background temperature | Clusters |
| toBombusS_Develop | | Eggs, Larvae and Pupae |
| toBombusS_NextStage | | Eggs and Pupae |
| toBombusS_Decide | | Workers, Gynes, Males and Queens |
| toBombusS_Consume | | Workers, Gynes, Males and Queens |
| toBombusS_Sleep | | Workers, Gynes, Males and Queens |
| toBombusS_Fan | | Workers, Gynes, Males and Queens |
| toBombusS_Inspect | | Workers, Gynes and Queens |
| toBombusS_Feed | | Workers, Gynes and Queens |
| toBombusS_Forage | | Workers, Gynes Queens |
| toBombusS_Incubate | | Workers, Gynes Queens |
| toBombusS_ForagePause | | Workers and Queens |
| toBombusS_ForageDecide | | Workers and Queens |
| toBombusS_StoreResources | | Workers and Queens |
| toBombusS_LayEgg | | Workers and Queens |
| toBombusS_AddWax | | Workers and Queens |
| toBombusS_Leave | | Gynes and males |
| toBombusS_SearchColony | | Gynes |
| toBombusS_FoundColony | | Gynes |
| toBombusS_StealColony | | Gynes |
| toBombusS_Hibernate | | Gynes |

Callow adults will be included within their adult life stage. Callow adults have just emerged from their pupa and are soft and must wait until they harden before acting like adults. During this time within the model they will eat, sleep or do nothing.

### Metabolism and thermodynamics
Metabolism is a size dependent relationship, that follows a power law (\ref _r12 "Peters, 1983"). This is costly if done by every individual bee, therefore this relationship is precalculated and stored during the setup of the mass class once at start up (Mass::SetStatic) using the function (PreCalculateEnergyPowCurve). Despite the energy used by the metabolism of the individual the temperature will tend towards that of the background temperature (Bombus_Base::CalculateMyBodyTemp). Adults then use additional energy above the metabolic energy to increase their temperature (Bombus_Base::CalculateMyBodyTemp). Where there is not enough energy available to the individual stored in their energy reserves (Mass) the temperature will be lower.
### Development time
Development time for most stages (Bombus_Egg, Bombus_Pupa, that need timing are controlled by a fixed amount of degree minutes that need to be accumulated above a minimum development temperature. Female larvae are the exception and the amount of degree minutes are determined in one of three ways set as a model mode setting as one of the enumerated options in \ref TTypeOfLarvaDevControl. If either \ref TTypeOfLarvaDevControl -> toLarvaDev_WorkerNo or \ref TTypeOfLarvaDevControl -> toLarvaDev_WaxSignal are chosen, then once a day the degree minutes needed for a female larvae are calculated in Bombus_Colony::BeginStep. If \ref TTypeOfLarvaDevControl -> toLarvaDev_QueenStep is chosen, the degree minutes are calculated in Bombus_Larva::EndStep. In this version of the model, only \ref TTypeOfLarvaDevControl -> toLarvaDev_WorkerNo is being used and tested.
### Growth rate
The main growth of bees is as a larva. Adults can only add body fat and the females develop ovaries. The rate that a larva grow at is controlled by their current mass, temperature and food consumed. The first two of these are used to calculate potential growth (Bombus_Larva::CalculatePotentialGrowth). The next mass (Bombus_Base::SetNextMass) is set for the larva in the Bombus_Larva::EndStep. The larva then attempts to consume the energy needed to grow to the potential size (Bombus_Larva::st_Consume). The larva then grows (Bombus_Base::Grow) based on the food it consumed within Bombus_Larva::st_Develop. If the food consumed is less than that which is needed to reach the next mass, the larvae grows less and then the next potential mass is less. If the larva consumes less than the energy needed to maintain its current mass it shrinks, and this is not fatal unless the mass reaches half the maximum mass it has previously grown to. This lost mass must be regained, before the larvae continues to grow. Degree minutes are only accumulated if the larva grows from the last size (determined in Mass::DigestedFoodEffect).
### End of the colony
The colony can die at any time based on the death of the adults within the colony or in landscape mode alternatively the random destruction of the colony. In the laboratory mode the colonies can be killed on a set day (#cfg_BombusColonyDeathdays). This dying takes place in the Bombus_Colony::BeginStep. The decline of the colony in the landscape mode is a balance between the colony as a whole (the queen and workers) living long enough to produce reproductive and the queen living on as a male producing solitary bee. This is controlled by the Bombus_Base::MortalityStressors of the queen and workers and the gynes and males leaving the colony.
### Resources
The model contains Nectar, Pollen and Food classes, that are objects that represent the volume, mass, energy and nutrition of nectar, pollen and a mixture of nectar and pollen as a form of bee bread as it may be fed to larvae.Mass
The Mass is a class that contains the lean mass, fat mass and ovaries as well as the history of the individual (maximum mass). It allows the individual to request energy and to take from the correct source in the correct order (Mass::TakeEnergyFromAny) and allow storage through digestion of the food (Mass::DigestedFoodEffect)
### Stomach
The Stomach is a class that acts as a container for food that can be passed from adults to larvae or to the colony stores (Stomach::Regurgitate), but also limits the throughput of food (Stomach::Digest). Both are limited by the size of the individual and in the case of larva grow as the larva grows. The digestion rate is limited by both volume throughput and sugar throughput (Stomach::Digest). With higher sugar the bee can be satiated.
### Mortality
A background level of mortality is set for each life stage (m_mortality). This is then multiplied to give a mortality for a particular day (m_mortalityToday) for the living bee life stages (not clusters and the colony), based stressors. For adults (Bombus_Base::MortalityStressors) these are their age and when in a colony number of corpses as a proxy for colony dirtiness. Outside the colony. When outside the colony mortality is also multiplied.

Both adults and juveniles also have mortality modified by temperature, with adult mortality increased above or below the optimum colony temperature (Bombus_Base::ModTodayMortByTemp) and juveniles the optimum cluster temperature (Bombus_Egg::MortalityStressors). Temperature has a different effect on gynes when they are hibernating with mortality multiplied by the absolute value of the difference in temperature from the optimal hibernation temperature (Bombus_Gyne::ModTodayMortByTemp).

\f$M_d=M_d\cdot \frac{|(T-T_o)|\cdot (1-m_o)}{T_w-T_O}+m_o\f$

Where To is the optimum hibernation temperature, Tw is the temperature gynes awake from hibernation, and mo is the proportion by which mortality is less when hibernating.
### Landscape resources
When the model is run in landscape mode, the bees need to forage. For gynes that do not belong to a colony this is relatively simple. When gynes forage (Bombus_Gyne::st_Forage) they move randomly (Bombus_Gyne::st_RandomMove), as a correlated random walk, using a Cauchy distribution to turn and a gamma distribution for the distances of a step. Within this walk, the bee can reverse course if they have not found food, in an attempt to perform something more similar to a foray loop ()\ref _r13 "McIntire et al., 2013"). At every step the gyne attempts to extract Nectar from their current location (Bombus_Worker::TakeNectarFromPoly) and if successful marks the location as a good source of food which determines if a gyne would return in the general direction of the patch if it does not find food elsewhere.

Workers and the queen living in a colony forage in a different manner. We implemented a simple solution for finding a patch of resources. We used this simple approach to make the model more efficient as the number of individuals in each colony can be many hundreds of times the gynes in the solitary phase of the model. Each colony has a list of polygons in distance bands around the colony. This list is sorted by whether the nectar in these polygons have nectar over a threshold. The nectar in the landscape is not of type Nectar, as that is an ALMaSS wide model used by multiple models.
As well as the colonies having lists of polygons, the population manager also has lists that link to each colony (BombusPollenNectarLists) that allows for a central update of polygons with resources (BombusPollenNectarLists::DoUpdate). As a colony is founded it is added to the list BombusPollenNectarLists::AddColony giving the distance band the colony is in. When a colony dies it must be removed BombusPollenNectarLists::RemoveColony.

The lists held by colonies are divided into polygons with resources and those without (Bombus_Colony::ChangeStatus). Colony foragers can then request a polygon with resources (Bombus_Colony::SupplyPolygon) within (Bombus_Worker::st_Forage and Bombus_Queen::st_Forage). These polygons are supplied probabilistically based on the area of resource polygons in a distance band, so that if the area is low, the bee has a high chance of passing to the next band. If all bands are passed through, the bee is assigned a random polygon in the furthest band to simulate the bee searching and failing to find resources. The polygon is provisionally stored as a good polygon. The time needed to travel to and from the band is also stored for use in the next foraging step.
The bee then is moved to the location in the landscape they have been assigned and attempts to extract nectar (Bombus_Worker::TakeNectarFromPoly) and pollen if needed (Bombus_Worker::TakePollenFromPoly). If the forager has failed to get the nectar and pollen it wants at that location it does no remember the polygon as a good foraging location. Bombus_Worker::st_Forage and Bombus_Queen::st_Forage do not end the Step. Step is called again, but this time Bombus_Worker::st_ForagePause is called.

Bombus_Worker::st_ForagePause moves the model on through successive 10-minute-timesteps to occupy the time it would take the forager to travel, forage and return to the colony. After all of the time needed has passed within the model, the bee returns to the colony and deposits the resources it is carrying (Bombus_Worker::st_StoreResources). Foraging workers continue to forage during daylight, controlled by Bombus_Worker::st_ForageDecide. The queen only continues to forage at need during the day (Bombus_Queen::st_ForageDecide).
### Colony density
Bombus populations are limited by the availability of habitat for colonies to be built in. Within the model this is controlled by two classes BombusPolygonEntry, and Bombus_Colony_Manager. BombusPolygonEntry contains a list of colonies either possible or within a polygon. Bombus_Colony_Manager loads information from an input into all of the polygons. The input contains a minimum and maximum number of colonies per unit area of habitat type, and then any polygon of that type is assigned a uniform random number of colonies for its area between the minimum and maximum. It also allows a polygon to be queried as to whether a colony can be added to it (Bombus_Colony_Manager::IsBombusColonyPossible, via Bombus_Population_Manager::IsBombusColonyPossible).
## Interconnections
The Bombus model connects to various established elements of the ALMaSS code through the Landscape. Landscape gives access to all the polygons (LE) within the landscape. These polygons give access to \ref TTypesOfLandscapeElement, Calendar, PollenNectarData and Landscape::SupplyPesticide models.

## I/O, Variables & Scales
### Inputs
In landscape mode, ALMaSS models need a BatchALMaSS.ini, TI_inifile.ini, cfg file by default named TIALMaSSConfig.cfg, maps of the area simulated, weather, farm rotation files for each farm type. In laboratory mode, weather, maps and rotations would not be needed. The model also can be set to have a number of probe files, one for each life stage (nine for the Bombus model). The two .ini files specify which probe files are used, the duration of the model run and in the case of the BatchALMaSS.ini the species model that is being run (Bombus is species 20).

The landscape mode bumble bee model uses the ALMaSS nectar and pollen model and needs two files that define how nectar and pollen change for the LE (l_map_nectarpolen) and vegetation (l_map_tov_nectarpolen). To control the number of colonies per patch of habitat, the Bombus model needs a file defining the minimum and maximum number of colonies per square meter for each habitat type (#cfg_BombusColonyByLE_Datafile).
TIALMaSSConfig.cfg contains all parameters for ALMaSS, although the model will run with default values if not specified. The parameters specific to the Bombus model are given in the next section.

The model parameters are given in \ref _t2 "Table 2" linking through doxygen to the code. Clicking on each gives the parameter name in the code as it appears in cfg input file and the default value.


\anchor _t2 Table 2. Parameters specific to the bumble bee model that can be changed via the input cfg file.

| **Parameters** | **Description** |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------- |
| #cfg_BombusOutput | Name of Bombus specific output file |
| #cfg_BombusColonyLocation | By specifying colony location the model can be run in laboratory mode or landscape mode. |
| #cfg_BombusDaysOfTemp | If running in laboratory mode, specify how many days of temperature are given in #cfg_BombusDayTemps |
| cfg_BombusDayTemps (see Bombus_Colony::m_MultipleDailyColonyTemps) | If running in laboratory mode, temperature each day (last used for every other day). |
| #cfg_BombusAccessForaging | Specifies if adults have access to the landscape to forage and disperse. |
| #cfg_BombusFedNectarPerDay | µl of nectar fed to a colony per day. |
| #cfg_BombusFedSugarProp | Sugar concentration of fed nectar % volume. |
| #cfg_BombusFedPollenPerDay | mg of pollen fed to colony per day. |
| #cfg_BombusNectarThresholds | Threshold for inclusion of a polygon as a foraging location. |
| **Fed pollen values** | |
| #cfg_BombusPropProtein | |
| #cfg_BombusPropSugar | |
| #cfg_BombusPropFat | |
| **Starting adults** | |
| #cfg_BombusNoStartingGyne | How many starting gynes there are at the beginning of the model. |
| #cfg_BombusNoStartingColWorkers | Mean number of workers in each laboratory colony. |
| #cfg_BombusNoStartingColWorkersSD | SD number of workers in each laboratory colony for use in normal distribution. |
| **Constants** | |
| #cfg_BombusEnergyPermgSugar | |
| #cfg_BombusEnergyPermgProtein | |
| #cfg_BombusEnergyPermgFat | |
| #cfg_BombusSugarDensitymgperul | |
| #cfg_BombusProteinDensitymgperul | |
| #cfg_BombusFatDenstymgperul | |
| #cfg_BombusAshDensitymgperml | |
| #cfg_BombusEnergyDensityFlesh | |
| #cfg_BombusWaxEnergyDensity | |
| #cfg_BombusAdultBodyDensity | |
| #cfg_BombusAdultHeatCapacityJperg | |
| **Fed pollen values** | |
| #cfg_BombusPropProtein | |
| #cfg_BombusPropSugar | |
| #cfg_BombusPropFat | |
| **Starting conditions** | |
| #cfg_BombusColonyDeathdays | |
| #cfg_BombusBroodStage | |
| **Starting conditions of gynes/queens** | |
| #cfg_BombusNoStartingGyne | |
| #cfg_BombusStartGyneAge | |
| #cfg_BombusStartGyneBodyFat | |
| #cfg_BombusStartGyneBodyFatSD | |
| #cfg_BombusStartGyneMass | |
| #cfg_BombusStartGyneMassSD | |
| #cfg_BombusStartGynePropOvaries | |
| #cfg_BombusDaysHibernatedFix | |
| #cfg_BombusDaysHibernatedFixedVal | |
| #cfg_BombusDaysHibernatedAlpha | |
| #cfg_BombusDaysHibernatedBeta | |
| **Signal queen lays down in wax when she produces wax.** | |
| #cfg_BombuswaxConcInter | |
| #cfg_BombuswaxConcInterSD | |
| #cfg_BombuswaxConcCoef | |
| **Starting conditions of initial colony workers** | |
| #cfg_BombusNoStartingColWorkers | |
| #cfg_BombusNoStartingColWorkersSD | |
| #cfg_BombusStartWorkerMassAlpha | |
| #cfg_BombusStartWorkerMassBeta | |
| **Starting condition of colony** | |
| #cfg_BombusColonyLocation | |
| **Food fed to lab colonies** | |
| #cfg_BombusAccessForaging | |
| #cfg_BombusFedNectarPerDay | |
| #cfg_BombusFedSugarProp | |
| #cfg_BombusFedPollenPerDay | |
| **Static mortality probability in step** | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| #Decreasing the slope and incresaing the intercept will make mortality higher when small, and decrease | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| cfg_BombusLarvaStageHardLimit (see Bombus_Larva::m_LarvaStageHardLimit) | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_BombusWorkerDeadEffected | |
| #cfg_BombusGyneDeadEffected | |
| #cfg_BombusQueenDeadEffected | |
| #cfg_BombusMaleDeadEffected | |
| **The mortality of the adults could be increased with age. I think this is likely important for the queen at least** | |
| #cfg_BombusAgeMortalityMultiQueen | |
| #cfg_BombusAgeMortalityMultiGyne | |
| #cfg_BombusAgeMortalityMultiWorker | |
| #cfg_BombusAgeMortalityMultiMales | |
| #cfg_BombusSenescenceMultiplier | |
| **Coefficents to increase mortality above normal exponentially.** | |
| #cfg_BombusEggBelowTempCoef | |
| #cfg_BombusEggAboveTempCoef | |
| #cfg_BombusLarvaBelowTempCoef | |
| #cfg_BombusLarvaAboveTempCoef | |
| #cfg_BombusPupaBelowTempCoef | |
| #cfg_BombusPupaAboveTempCoef | |
| #cfg_BombusWorkerBelowTempCoef | |
| #cfg_BombusWorkerAboveTempCoef | |
| #cfg_BombusGyneBelowTempCoef | |
| #cfg_BombusGyneAboveTempCoef | |
| #cfg_BombusQueenBelowTempCoef | |
| #cfg_BombusQueenAboveTempCoef | |
| #cfg_BombusMaleBelowTempCoef | |
| #cfg_BombusMaleAboveTempCoef | |
| **Adult variables** | |
| #cfg_BombusMaxSleep | |
| #cfg_BombusMinSleep | |
| #cfg_BombusBodyTempAdults | |
| #cfg_BombusEnergyDensityOvaries | |
| #cfg_Bombus_OtherWorkerTaskProb | There are other tasks that workers can perform within the colony other than those specified in \ref TTypeOfBombusState. Increasing this probability means more time is spent on other colony specific tasks. |
| **Egg laying variables** | |
| #cfg_BombusMinColonyTempToLay | |
| #cfg_BombusInitialEggTemp | |
| #cfg_BombusEggMax | |
| #cfg_BombusEggMin | |
| #cfg_BombusEnergyDensityEgg | |
| **Pollen priming differs between species and between stages.** | |
| #cfg_BombusB1EggCellPollen | |
| #cfg_BombusB2EggCellPollen | |
| #cfg_BombusB3EggCellPollen | |
| #cfg_BombusWorkersEggCellPollen | |
| **The cluster has starting dimensions used in temp calculations** | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_BombusSexGenes | |
| **Cluster wax variables** | |
| #cfg_BombusMaxAgeWorkerMakeWax | |
| #cfg_BombusMaxAgeQueenMakeWax | |
| **Temperatures** | |
| #cfg_BombusOptimumColTemp | |
| #cfg_BombusCallowMinTemp | |
| #cfg_BombusMinJuvDevTemp | |
| #cfg_BombusOptimumDevTemp | |
| **Rate number of adults increases temp of colony** | |
| #cfg_BombusAdultColWarmCoef | |
| **Weight that acts as insulation for the colony.** | |
| #cfg_BombusInsulation | |
| **Variables influencing how k is calculated for cluster** | |
| #cfg_BombusClusterHeatCapacityJperg | |
| #cfg_BombusAlphaOfAir | |
| #cfg_BombusFanningEnergyPerMin | |
| cfg_BombusFanningExp_a (see Bombus_Colony::SetStatic)| |
| cfg_BombusFanningExp_b (see Bombus_Colony::SetStatic)| |
| **Developement of juviniles or adults** | |
| #cfg_BombusAdultsHomeotherms | |
| #cfg_BombusJuvinilessHomeotherms | |
| #cfg_BombusMaxCocoonMass | |
| #cfg_BombusDivisionEggAtDangerOfEaten | |
| #cfg_BombusPropEggProtein | |
| #cfg_BombusVolumeLarvalFeeding | |
| #cfg_BombusMassWaxFirstNectarPot | |
| **Degree minutes** | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_BombusLarvalMaleDM | |
| #cfg_BombusDayInDM | |
| #cfg_BombusLarvaGyneDMcutoff | |
| #cfg_BombusPropDetectGyneLarva | |
| #cfg_BombusPropDetectGyneAdults | |
| #cfg_BombusLarvalDMmax | |
| #cfg_BombusLarvalDMmin | |
| #cfg_BombusPupaDMDivisionOfLarval | |
| **Female larva vary in developement time between the above minimum and maximum. Three possibilities have been considered for how this may happen** | |
| #cfg_BombusLarvaDevControl | |
| **For \ref TTypeOfLarvaDevControl -> toLarvaDev_QueenStep** | |
| #cfg_BombusminsQueenCanReduce | |
| #cfg_BombusminsQueenCanReduceSD | |
| **For \ref TTypeOfLarvaDevControl -> toLarvaDev_WorkerNo** | |
| #cfg_BombusLarvalB | |
| #cfg_BombusLarvalC | |
| #cfg_BombusLarvalQ | |
| #cfg_BombusLarvalv | |
| **For \ref TTypeOfLarvaDevControl -> toLarvaDev_WaxSignal** | |
| #cfg_BombusLarvalA | |
| #cfg_BombusLarvalW | |
| #cfg_BombusLarvalBWax | |
| #cfg_BombusLarvalvWax | |
| **For \ref TTypeOfLarvaDevControl -> toLarvaDev_WorkerNo, toLarvaDev_WaxSignal the function determining DM will never reach full, this defines how close before the individual is a gyne** | |
| #cfg_BombusPropDMmaxIsGyne | |
| **Larva grow from min, upto a value less than or equal to a maximum at a rate.** | |
| #cfg_BombusMinMass | This is the starting mass of a larva in mg.|
| #cfg_BombusMaxMass | This is the maximum mass a larva can grow to in mg. |
| #cfg_BombusMaxMassMale | Without specifying this maximum mass individually the males end up with some individuals gyne sized. |
| #cfg_BombusGrowthG | This is the rate that a larva grows. |
| #cfg_BombusGrowthGMale | Males appear to need grow slower than workers to achieve the correct mass in a longer development time.|
| **Inspecting and feeding juviniles** | |
| #cfg_BombusFeedLargestFirst | |
| #cfg_BombusPropBroodCare | |
| **Deviation of probability of incubating a cluster when moving away from optimum temp.** | |
| #cfg_BombusRootIncubatingProb | |
| #cfg_BombusIncubatingDenominator | |
| **Energy used simply inspecting a cluster of juviniles.** | |
| #cfg_BombusStepEnergyInspection | |
| **Individual food absorbtion ration.** | |
| **With low quality nectar this is the rate a worker consumes nectar** | |
| #cfg_BombusDigestionRate | |
| **With higher quality nectar with more sugar the adult consumes less.** | |
| #cfg_BombusSugarDigestionRate | |
| **The lowest of these two is taken to stop the adult consuming too much.** | |
| #cfg_BombusConcentrationAdultAddToNectar | |
| #cfg_BombusMassPollenConcInNectar | |
| #cfg_BombusStomachCapacityPermgBodyWeight | |
| **Ovary development and dominance** | |
| **Workers if not sufficiently dominated by a queen or more agressive workers can develop ovaries and compete with the queen to lay eggs.** | |
| #cfg_BombusWorkerEncounteredInStep | |
| #cfg_BombusDominationThreshold | |
| #cfg_BombusOvaryShrinkage | |
| #cfg_BombusMaxPropOvaries | |
| #cfg_BombusMinAgeWokersCompete | |
| #cfg_BombusQueenNoLayDamageThreshold | |
| #cfg_BombusWorkerNoLayDamageThreshold | |
| **Free flying bumble bees parameters** | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_BombusMinHibernationDuration | |
| #cfg_BombusHibernationWakeUpTemp | |
| **Proportion of remaining possible fat that can be added in a step.** | |
| #cfg_BombusPerRemainingFatWorker | |
| #cfg_BombusPerRemainingFatGyne | |
| #cfg_BombusPerRemainingFatQueen | |
| #cfg_BombusPerRemainingFatMale | |
| **A simple multiplier to the mortality of the day for going outside.** | |
| #cfg_BombusOutsideMortMultiWorker | |
| #cfg_BombusOutsideMortMultiGyne | |
| #cfg_BombusOutsideMortMultiQueen | |
| #cfg_BombusOutsideMortMultiMale | |
| **Workers can switch backwards and forwards between tasks, but the more they do the task, the more likely they will stick with it.** | |
| #cfg_BombusForagingPermenance | |
| **This should possibly be zero, or should set the warming energy. Also probably shouldn't include a proportion of maintenance energy.** | |
| #cfg_BombusStepFlightEnergy | |
| #cfg_BombusPollenLoadMg | |
| **Presumably as gynes store more fat their probability of leaving the colony should increase.** **Below is the rate and the midpoint** | |
| #cfg_BombusGyneLeaveFatRate | |
| #cfg_BombusGyneLeaveFatMid | |
| #Gyne hibernation | |
| #cfg_BombusOptimalHibernationTemp | |
| #cfg_BombusOptimalHibernationPropMort | |
| **Nest stealing** | |
| #cfg_BombusStealTollerance | |
| **Foraging bands, the number and the distances** | |
| #cfg_BombusForageBandNumber | |
| #cfg_BombusForageBands | |
| #cfg_Bombus_AdultTempBelowMax | |
| #cfg_Bombus_HibernationInsulation | |
| **Influences adult body temperture. A threshold above which they reduce their body temperature and by how much.** | |
| #cfg_Bombus_AdultTempToMaxThreshold | |
| #cfg_Bombus_AdultTempBelowMax | |
| #Correlated random walk turning distribution | |
| #cfg_Bombus_WalkCauchyLoc | |
| #cfg_Bombus_WalkCauchyScale | |
| **Correlated random walk travel distance distribution** | |
| #cfg_Bombus_WalkGammaAlpha | |
| #cfg_Bombus_WalkGammaBeta | |
| **How much fat abd ovary developement the gyne stores before searching for and founding a colony** | |
| #cfg_Bombus_PropMaxFatSearch | |
| #cfg_Bombus_PropMaxOvariesSearch | |
| **Fat level needed for hibernation.** | |
| #cfg_Bombus_FatPropEnterHib | |
| **Inbreeding avoidence number. Zero would cause inbreeding to be randomly possible.** | |
| #cfg_BombusInbreedCounter | |

###	Outputs
The Bombus model has two main output files. A file which gives daily counts of each lifestage in the model. A Bombus specific output file that contains data on the colonies and adults in the model every day (reported by the adults) ( see Bombus_Base::RecordVitals ). This is the identity of the colony and individual, parents, age, time as an adult, mass, background temperature and colony temperature. The specific outputs for each file can be seen in \ref _t3 "Table 3".

\anchor _t3
| **Output value** | **Description** |
| ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| *Default almass, default: Probe.res* | The outputs of this table are the day and then follow the order of \ref TTypeOfBombusColonyLifeStages and \ref TTypeOfBombusLifeStages. |
| Col 1 | The day in the model. |
| Col 2 | Number of colonies. |
| Col 3 | Number of clusters of juviniles. |
| Col 4 | Number of gynes . |
| Col 5 | Number of queens. |
| Col 6 | Number of eggs. |
| Col 7 | Number of larvae. |
| Col 8 | Number of pupae. |
| Col 9 | Number of workers. |
| Col 10 | Number of males. |
| *Bombus specific, default: Individuals.txt* | This file contains reporting from each adult, but can also have larva report. |
| RunNo | If specified as a parameter #cfg_BombusRunNo this number is recorded here. |
| Year | The year of the model (1, 2, …, n). |
| Day | Day in the model, (1, 2, …, 2000, … n). |
| Colony | The ID of the colony, that of the current queen. |
| ID | The ID of the individual. |
| MothersID | The ID of the individuals mother. |
| FathersID | The ID of the individuals father. |
| Identity | What they are (worker, queen, …). |
| Age | Age in days of the individual since being laid as an egg. |
| StageAge | Age of their current life stage. |
| Mass | Total mass of the individual (lean, fat and ovaries combined). |
| Starting | If this individual is one we specified at the beginning of the model. |
| BackgroundTemp | The background temperature of the individual. That of the laboratory or landscape. |
| ColonyTemp | Temperature inside the colony. |

<br>

### State variables

The state variables are those which change throughout the model can be see in \ref _t4 "Table 4".

\anchor _t4 Table 4. Most important state variables that change throughout .

| Variable name | Units | Description |
| ----------------------------- | -------------------------- | -------------------------------------- |
| Bombus_Colony::m_AgeColony | days | |
| Bombus_Colony::ColonyTimeStep | Steps | |
| Bombus_Cluster::m_Age | Minutes | |
| Bombus_Base::m_Age | Minutes | |
| Bombus_Base::m_StageAge | Minutes | |
| Bombus_Base::m_StageAgeDay | Days | |
| Bombus_Base::m_AgeDegrees | Accumulated degree minutes | |
| Bombus_Base::timestep | Steps | |
| | | |
| Mass::m_LeanMass | mg | The mass before fat storage or ovaries |

<br>

###	Scales

The Bombus model runs at a ten-minute time step. This is the scale of the bees and colony. All landscape functions take place on a daily time scale. The landscape is a one-meter resolution and is commonly run on landscapes of 10 by 10 km.
##	Discussion of implementation

Bumble bee colonies are sensitive. In the laboratory it is not uncommon for a queen to die or a colony to fail to produce reproductive adults. The model is no exception; for example, changing ability to have lower body temperature or not can cause colonies to not warm or grow. The balance of factors within the colony are vital, in terms of temperature, resources, and the relevant life cycle stages, and this may explain why so many real laboratory colonies fail to produce gynes (or negligibly) and only few males.
We found that without nest recognition and killing of the diploid males in the landscape mode of the model, too few gynes were successful at raising healthy colonies. We initially planned to have a single growth rate and maximum mass for workers, gynes and males, assuming that with the variation in degree minutes continually for females and a set value for males, that the correct mass for each type of lifestage would emerge. However this is not the case. With the growth values the same, the males would become too large in the time they take to develop. Therefore their growth rate must be lower. 

<br>

## Calibration
The calibration output of the model is available in a [separate document](Doxygen_Pics/Bombus_calibration.pdf).

##	References
\anchor _r9 BOGO, G., DE MANINCOR, N., FISOGNI, A., GALLONI, M., ZAVATTA, L. & BORTOLOTTI, L. 2018. No evidence for an inbreeding avoidance system in the bumble bee Bombus terrestris. *Apidologie*, 49, 473-483.

\anchor _r1 CECEN, S., GOSTERIT, A. & GUREL, F. 2007. Pollination effects of the bumble bee and honey bee on white clover (Trifolium repens L.) seed production. *Journal of Apicultural Research*, 46, 69-72.

\anchor _r2 CECEN, S., GUREL, F. & KARACA, A. 2008. Impact of honeybee and bumblebee pollination on alfalfa seed yield. *Acta Agriculturae Scandinavica Section B-Soil and Slant Science*, 58, 77-81.

\anchor _r6 DI PIETRO, V., FERREIRA, H. M., VAN OYSTAEYEN, A., WÄCKERS, F., WENSELEERS, T. & OLIVEIRA, R. C. 2022. Distinct Colony Types Caused by Diploid Male Production in the Buff-Tailed Bumblebee Bombus terrestris. *Frontiers in Ecology and Evolution*, 10.

\anchor _r10 DUCHATEAU, M. J., HOSHIBA, H. & VELTHUIS, H. H. W. 1994. Diploid males in the bumble bee Bombus terrestris Sex determination, sex alleles and viability. *Entomologia Experimentalis et Applicata*, 71, 263-269.

\anchor _r11 Duchateau, M.J., Mariën, J. Sexual biology of haploid and diploid males in the bumble beeBombus terrestris . Ins. Soc 42, 255–266 (1995). https://doi.org/10.1007/BF01240420

\anchor _r8 FOSTER, R. L. 1992. Nestmate Recognition as an Inbreeding Avoidance Mechanism in Bumble Bees (Hymenoptera: Apidae). *Journal of the Kansas Entomological Society*, 65, 3, 238-243.

\anchor _r7 GOSTERIT, A. & GUREL, F. 2016. Male remating and its influences on queen colony foundation success in the bumblebee, Bombus terrestris. *Apidologie*, 47, 828-834.

\anchor _r13 McIntire, E.J.B., Rompré, G. & Severns, P.M. Biased correlated random walk and foray loop: which movement hypothesis drives a butterfly metapopulation?. Oecologia 172, 293–305 (2013). https://doi.org/10.1007/s00442-012-2475-9

\anchor _r12 PETERS, R. H. 1983. The ecological implications of body size, Cambridge [Cambridgeshire]; New York.

\anchor _r5 SHPIGLER, H., TAMARKIN, M., GRUBER, Y., POLEG, M., SIEGEL, A. J. & BLOCH, G. 2013. Social influences on body size and developmental time in the bumblebee Bombus terrestris. *Behavioral Ecology and Sociobiology*, 67, 1601-1612.

\anchor _r4 TOPPING, C. J., HANSEN, T. S., JENSEN, T. S., JEPSEN, J. U., NIKOLAJSEN, F. & ODDERSKÆR, P. 2003. ALMaSS, an agent-based model for animals in temperate European landscapes. *Ecological Modelling*, 167, 65-82.

\anchor _r3 TOPPING, C. J., KONDRUP MARCUSSEN, L., THOMSEN, P. & CHETCUTI, J. 2022. The Formal Model article format: justifying modelling intent and a critical review of data foundations through publication. *\Food and Ecological Systems Modelling Journal*, 3.
