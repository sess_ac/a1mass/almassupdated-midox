/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Bombus_Population_Manager.h
Version of  14 December 2022 \n
By Jordan Chetcuti \n \n
*/
//---------------------------------------------------------------------------
#ifndef Bombus_Population_ManagerH
#define Bombus_Population_ManagerH
//---------------------------------------------------------------------------
class Bombus;

//--------------------------------------------------------------------------------------------------------------------
//    Classes associated with foraging
//--------------------------------------------------------------------------------------------------------------------

/** \brief A data class to hold each pair of pollen and nectar thresholds */
class PollenNectarThresholds
{
public:
	double m_pollenTqual = 0.0;
	double m_nectarTqual = 0.0;
	double m_pollenTquan = 0.0;
	double m_nectarTquan = 0.0;
};

/** \brief A data class to hold each pair of nest and distance data */
class BombusNestDistancePair
{
public:
	/** \brief Holds the value for the distance class from the colony */
	int m_distanceclass = 0;
	/** \brief  The pointer to the colony */
	Bombus_Colony* m_nestptr = nullptr;
	/** \brief BombusNestDistancePair constructor */
	BombusNestDistancePair(Bombus_Colony* a_nestptr = nullptr, int a_distanceclass = 0)
	{
		m_distanceclass = a_distanceclass;
		m_nestptr = a_nestptr;
	}
};

/** \brief A data class to hold the lists of nests associated with a polygon and their respective polygon as a ptr */
class BombusPolygonNestList
{
public:
	/** \brief The list of polygons and associated colonies */
	vector<BombusNestDistancePair> m_NestList;
	/** \brief The pointer to the polygon */
	LE* m_polygon;
	/** \brief Flag for pollen production above threshold */
	bool m_pollen;
	/** \brief Flag for nectar production above threshold */
	bool m_nectar;
	/** \brief Flag for nectar or pollen production above threshold */
	bool m_resources;

	/** \brief Constructor for BombusPolygonNestList */
	BombusPolygonNestList(LE* a_poly)
	{
		m_polygon = a_poly;
		m_NestList.resize(0);
		m_pollen = false;
		m_nectar = false;
		m_resources = false;
	}

	/* \brief Adds a nest to the list */
	void AddNest(BombusNestDistancePair a_datapair) { m_NestList.push_back(a_datapair); }
	/* \brief Removes a nest to the list */
	void RemoveNest(BombusNestDistancePair a_datapair)
	{
		// Note does not raise an error if not found
		for (auto it = m_NestList.begin(); it != m_NestList.end(); ++it)
		{
			if ((*it).m_nestptr == a_datapair.m_nestptr)
			{
				m_NestList.erase(it);
				break;
			}
		}
	}
};

/** \brief A class to manage the nectar and pollen producing polygon lists and links to each colony */
class BombusPollenNectarLists
{
	/**
	* This is a data structure with some data manipulation capacity to manage all the potential polygons that have nectar and pollen
	* and to link the ones that are currently producing with the colonies that can forage from them. For each polyon the distance class
	* to the nest is saved as is whether it passes a seasonally variable threshold for pollen and nectar availability per m2.
	* The size of the polygon is also used by the nest to calculate the chance of finding it, but this is not part of this class.
	*
	* The structure is a list of polygons that theoretically can produce pollen/nectar (ie all VegElements). For each one there is an associated
	* list of bee nests and distance classes.
	*
	* When a nest is created it is added to the relevant polygon lists and when destroyed the nest is removed from the lists.
	* Each day all polygons that change status either to above minimum production or from it will signal their respective colonies that the status is changed
	* allowing the nest to adjust its current useful lists of polygons. This method does mean double accounting of lists (ie in this class and in the nest)
	* but minimises the number of times the entire list of VegElements needs to be managed.
	*/
public:
	// Methods
	/** \brief The BombusPollenNectarLists constructor */
	BombusPollenNectarLists(Landscape* a_land);
	/** \brief This will trawl the list of pollen sources and alter the status as pollen nectar producing */
	void DoUpdate();
	/** \brief Adds a new colony to the data structure, figuring out the distance classificiation for each polygon */
	void AddColony(Bombus_Colony* a_new_Bombus_Colony);
	/** \brief Removes a colony from the data structure using the index to the polygon to prevent a sweep of all polygons */
	void RemoveColony(Bombus_Colony* a_Bombus_Colony, int a_index);

	//Public attributes
	/** \brief the number of foraging bands around each colony*/
	int m_ForageBandNumber;
	/** \brief Area in each foraging band*/
	static vector<double> m_ForageBandArea;

protected:
	// Attributes
	/** \brief Holds a list of pollen and nectar thresholds, one for each month */
	vector<PollenNectarThresholds> m_PN_thresholds;
	/** \brief This is the main data structure, a list of LE* and their associated nest lists */
	vector<BombusPolygonNestList> m_PolyNestLists;
	/** \brief A handy pointer to the Landscape */
	Landscape* m_land;
	/** \brief A record of half the size of the landscape for speed calculations */
	static int m_LandWidthDiv2;
	/** \brief A record of half the size of the landscape for speed calculations */
	static int m_LandHeightDiv2;
	/** \brief A record of the size of the landscape for speed calculations */
	static int m_LandWidth;
	/** \brief A record of the size of the landscape for speed calculations */
	static int m_LandHeight;

	static vector<int> m_ForageBands;
};

//--------------------------------------------------------------------------------------------------------------------
//    End classes associated with foraging
//--------------------------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------------------------
//    Classes associated with colony density - copied from Osmia model 2022-10-11 then adapted
//--------------------------------------------------------------------------------------------------------------------

/** \brief Is the list of Colonies held by a polygon and associated density controls*/
class BombusPolygonEntry
{
protected:
	vector<Bombus_Colony*> m_ColonyList;
	/** \brief to record the chance of Bombus creating a colony */
	double m_BombusColonyProb;
	/** \brief to record the number of possible Bombus Colonies */
	int m_MaxBombusColonies;
	/** \brief to record the number of actual Bombus Colonies (only used for speed - it is the same as the size of the vector m_ColonyList */
	int m_CurrentBombusColonies;
	/** \brief The polygon area */
	int m_Area;
	/** \brief The polygon reference used by the Landscape and Bombus_Colony_Manager */
	unsigned int m_Polyindex;

public:
	/** \brief The constructor for BombusPolygonEntry */
	BombusPolygonEntry()
	{
		m_ColonyList.clear();
		m_CurrentBombusColonies = 0;
		m_MaxBombusColonies = 0;
		m_BombusColonyProb = 0;
		m_Polyindex = -1;
		m_Area = -1;
	}

	BombusPolygonEntry(int a_index, int a_area)
	{
		m_ColonyList.clear();
		m_CurrentBombusColonies = 0;
		m_MaxBombusColonies = 0;
		m_BombusColonyProb = 0;
		m_Polyindex = a_index;
		m_Area = a_area;
	}

	/** \brief The destructor for BombusPolygonEntry */
	~BombusPolygonEntry()
	{
		m_ColonyList.clear();
	}

	/** \brief Test to see if a Colony is found */
	bool IsBombusColonyPossible()
	{
		if ((m_CurrentBombusColonies < m_MaxBombusColonies) && (g_rand_uni() < m_BombusColonyProb)) return true;
		return false;
	}

	/** \brief Test release an Bombus Colony that is no longer used */
	void ReleaseBombusColony(Bombus_Colony* a_Colony)
	{
		for (auto i = m_ColonyList.begin(); i < m_ColonyList.end(); ++i)
		{
			if (*(i) == a_Colony)
			{
				m_ColonyList.erase(i);
				m_CurrentBombusColonies--;
				return;
			}
		}
	}

	/** \brief Recalculate the Colony finding probability */
	void UpdateBombusColonying()
	{
		if (m_MaxBombusColonies <= 0) m_BombusColonyProb = 0.0;
		else
		{
			m_BombusColonyProb = 1.0 - (static_cast<double>(m_CurrentBombusColonies) / static_cast<double>(
				m_MaxBombusColonies));
		}
	}

	/** \brief Add an occupied Colony */
	void IncBombusColony(Bombus_Colony* a_Colony)
	{
		m_ColonyList.push_back(a_Colony);
		m_CurrentBombusColonies++;
	}

	/** \brief Sets the max number of Bombus Colonies for this LE */
	void SetMaxBombusColonies(double a_noColonies)
	{
		double maxColonies = a_noColonies * m_Area;
		if (maxColonies > 2147000000) maxColonies = 2147000000;
		m_MaxBombusColonies = static_cast<int>(maxColonies);
		if (m_MaxBombusColonies < 1) m_MaxBombusColonies = 0;
	}

	/** \brief Sets the area  attrribute */
	void SetAreaAttribute(int a_area) { m_Area = a_area; }
	/** \brief Sets the area  attrribute */
	void SetIndexAttribute(unsigned int a_index) { m_Polyindex = a_index; }

	/** \brief Supply a random colony from those already in a patch for a gyne to try and steal.*/
	Bombus_Colony* ChooseRandomColony()
	{
		return m_ColonyList[random(static_cast<int>(m_ColonyList.size()))];
	}

	/** \brief Return the number of colonies in a patch.*/
	int ReturnNumberColonies() { return m_CurrentBombusColonies; }
};

/**
 * \brief Class to manage the number of colonies possible within a patch of habitat.
 */
class Bombus_Colony_Manager
{
public:
	/** \brief Bombus Colony manager constructor */
	Bombus_Colony_Manager()
	{
	}

	/** \brief Bombus Colony manager denstructor */
	~Bombus_Colony_Manager()
	{
		m_PolyList.clear();
	}

	/** \brief Read in the Bombus Colony density files and allocate to each LE object */
	void InitDensityBombusColonies();
	/** \brief Tell all LE objects to update their Bombus Colony status */
	void UpdateDensityBombusColonies()
	{
		/**
		Loops through all landscape element objects and updates their number of colonies
		*/
		for (unsigned int s = 0; s < m_PolyList.size(); s++)
		{
			m_PolyList[s].UpdateBombusColonying();
		}
	}

	/** \brief Find out whether an Bombus Colony can be made here */
	bool IsBombusColonyPossible(int a_polyindex)
	{
		return m_PolyList[a_polyindex].IsBombusColonyPossible();
	}

	/** \brief Returns the number of colonies in a polygon index \p a_polyindex*/
	int ReturnNumberColonies(int a_polyindex) { return m_PolyList[a_polyindex].ReturnNumberColonies(); }


	/** \brief Add the Bombus Colony here  */
	void AddColony(Bombus_Colony* a_Colony, int a_polyindex)
	{
		m_PolyList[a_polyindex].IncBombusColony(a_Colony);
		m_PolyList[a_polyindex].UpdateBombusColonying();
	}


	/** \brief Remove the Bombus Colony here allowing for new colonies  */
	void RemoveBombusColony(Bombus_Colony* a_Colony, int a_polyindex)
	{
		m_PolyList[a_polyindex].ReleaseBombusColony(a_Colony);
		m_PolyList[a_polyindex].UpdateBombusColonying();
	}

	/** \brief Is a Colony possible in this polytype */
	bool GetColonyPossible(TTypesOfLandscapeElement index) { return m_PossibleColonyType[static_cast<int>(index)]; }

	Bombus_Colony* GetRandomColony(int a_polyindex);

protected:
	/** \brief Vector of the BombusPolygonEntry */
	vector<BombusPolygonEntry> m_PolyList;
	/** \brief Holds a set of flags indicating whether a Bombus Colony is possible (true) or not (false) */
	bool m_PossibleColonyType[tole_Foobar] = {false};
};


//--------------------------------------------------------------------------------------------------------------------
//    End classes associated with colony density - copied from Osmia model 2022-10-11
//--------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------

/**
\brief
Used for creation of a new Bombus object
*/
class struct_Bombus
{
public:
	/** \brief x-coord */
	int x = 0;
	/** \brief y-coord */
	int y = 0;
	/** \brief Landscape pointer */
	Landscape* L = nullptr;
	/** \brief Bombus_Population_Manager pointer */
	Bombus_Population_Manager* BPM = nullptr;
	/** \brief age */
	int age = 0;
	/** \brief colony or adults id */
	unsigned long m_id = 0;
	/** \brief ID of the mother */
	unsigned long MotherID = 0;
	/** \brief ID of the father */
	unsigned long FatherID = 0;
	/** \brief The mass of the whole body in mg. */
	double Mass = 0;
	/** \brief The mass of the body fat in mg. */
	double BodyFat = 0;
	/** \brief The proportion of the individuals mass that is ovaries. */
	double PropOvaries = 0;
	/** \brief a pointer to a colony */
	Bombus_Colony* Colony = nullptr;
	/** \brief The gyne gets sperm from males and stores it overwinter (this later gets passed as FatherID to the next generation) */
	unsigned long SpermID = 0;
	/** \brief The juviniles are part of a cluster */
	Bombus_Cluster* Cluster = nullptr;
	/** \brief The location of the colony created, lab, field box, above ground, on surface or underground. */
	TTypeOfColonyLocation ColonyLocation = toColonyLoc_Foo;
	/** \brief Degree minutres needed to emerge as an adult, set my the larval amount needed divided by PupaDMDivisionOfLarval */
	double PupaDM = 0.0;
	/** \brief Whether an individual is a gyne*/
	bool IsGyne = false;
	/** \brief How long an egg takes to develop in degree minutes*/
	double AgeDM = 0.0;
	/** \brief concentration of some queen pheremone that can set in a particular set up of the model @see TTypeOfLarvaDevControl*/
	double waxConcInter = 0.0;
	/** \brief the number of days a gyne has hibernated, passed on to her queen self*/
	int DaysHibernated = 0;
	/** \brief concentration of some amount the queen can reduce the degree minutes needed by a larva to develop in a particular set up of the model @see TTypeOfLarvaDevControl*/
	double minsQueenCanReduce = 0.0;
	/** \brief whether an adult is callow. Adults born in the model start callow. */
	bool Callow = true;
	/** \brief Number of workers in the starting lab colony*/
	int StartingColWorkers = 0;
	/** \brief Flad of whether an individual is one of the individuals that started the simulation used for output analysis*/
	bool StartingIndiv = false;
	/** \brief Individuals get given either one or two sex allele. Haploid males only one, diploid two, but they are the same one, all other two different ones.*/
	int SexLocus_1 = 0;
	int SexLocus_2 = 0;
	/** \brief The gyne needs to pass on the information of the sperms sex allele to her queen self. */
	int MatesSexLocus = 0;
	/** \brief Whether an individual is currently hibernating, used for starting gynes.*/
	bool Hibernating = false;
};

//------------------------------------------------------------------------------
/**
 * \brief Creates a text file with defined headings that can then have data saved to. 
 */
class Bombus_Recording
{
protected:
	/** \brief
	Pointer to an output file for individual \a <Bombus> data
	*/
	ofstream* m_BombusRecordingFile;
	/** \brief A run number that can be set in the config file for ease of combining multiple run data*/
	static string m_RunNo;
	/** \brief the headings of the output file*/
	static vector<string> m_VectString;

public:
	/** \brief Bombus_Recording constructor*/
	Bombus_Recording(const string& a_FileName, vector<string> a_headings, string a_RunNo);

	/** \brief Bombus_Recording destructor*/
	~Bombus_Recording();

	/** \brief Record variables from an individual bee*/
	void RecordValues(vector<string> a_values);

	/** \brief Set the year and day to be added to the record, only needs done once a day*/
	void SetDate(int a_year, int a_day);
};

//------------------------------------------------------------------------------
/**
\brief
The class to handle all Bombus population related matters
*/
class Bombus_Population_Manager final : public Population_Manager
{
protected:
	// Attributes
	/**
	\brief To give each adult Bombus an ID, the population manager counts up from one.
	*/
	unsigned long currentID = 1;
	/** \brief The temperature in a ten minute time step derived in Bombus_Population_Manager::CalculateTemp*/
	double m_10minTemp = 0.0;
	/** \brief The time of day in minutes*/
	int Time = 0;

public:
	// Methods
	/** \brief Bombus_Population_Manager Constructor */
	Bombus_Population_Manager(Landscape* L);

	/** \brief Bombus_Population_Manager Destructor */
	~Bombus_Population_Manager() override;
	/** \brief Method for creating a new individual Bombus */
	void CreateObjects(int ob_type, TAnimal* pvo, struct_Bombus* data, int number);
	/** \brief Like the more general create object, but specific for colony creation*/
	Bombus_Colony* CreateColony(int ob_type, struct_Bombus* data, int number);

	/** \brief Checks whether a nest is possible here */
	bool IsBombusColonyPossible(int a_polyindex) { return m_OurColonyManager.IsBombusColonyPossible(a_polyindex); }

	int ReturnNumberColonies(int a_polyindex) { return m_OurColonyManager.ReturnNumberColonies(a_polyindex); }
	/** \brief Like the more general create object, but specific for cluster creation*/
	Bombus_Cluster* CreateCluster(int ob_type, struct_Bombus* data, int NumberYoung);


	/** \brief Whether it is daylight or not.*/
	bool IsDaylight{};

	/** \brief Provides the next ID to the next new adult*/
	unsigned long nextID();
	/** \brief return the tempertaure in a ten minute time step derived in Bombus_Population_Manager::CalculateTemp */
	double Get10minTemperature() { return m_10minTemp; }

	/** \brief Bombus_Recordings of "Day", "Colony", "ID", "MothersID", "FathersID", "Identity", "Age","StageAge",
	 * "Mass", "Starting", "BackgroundTemp", "ColonyTemp"
	 * */
	Bombus_Recording* RecordingIndividuals;

	/** \brief Called on the death of a colony to remove this from the main data in m_PollenNectarLists */
	void RemoveColony(Bombus_Colony* a_Bombus_Colony, int a_index)
	{
		/**
		* Only need to calculate pollen and nectar in the landscape when the individuals are out and foraging. 
		*/
		if (a_Bombus_Colony->AccessForaging)
		{
			m_PollenNectarLists->RemoveColony(a_Bombus_Colony, a_index);
		}
	}

	/** \brief Vector of the gynes that have left their maturnal colonies and are searching for a mate. */
	vector<Bombus_Gyne*> m_Virgin_Gynes;
	/** \brief Vector of the males that have left their maturnal colonies and are searching for a mate. */
	vector<Bombus_Male*> m_Searching_Males;

	/** \brief Supply a random colony within a patch. Used by invading gynes */
	Bombus_Colony* RandomColony(int a_polyindex) { return m_OurColonyManager.GetRandomColony(a_polyindex); }

	/** \brief Return pointer the the Bombus_Colony_Manager */
	Bombus_Colony_Manager* PointToColonyManager() { return &m_OurColonyManager; }

	/** \brief Return if location is hibernation habitat*/
	bool HibernationHabitat(int a_x, int a_y);

	/** \brief Return the number of foraging bands*/
	int ReturnForagingBands() { return m_PollenNectarLists->m_ForageBandNumber; }
	/** \brief Return the size of a partcular band (index 0 -> n)*/
	double ReturnForagingBandArea(int a_BandIndex) { return m_PollenNectarLists->m_ForageBandArea[a_BandIndex]; }

protected:
	// Attributes
	/** \brief A data structure to hold the current pollen/nectar producing polygons */
	BombusPollenNectarLists* m_PollenNectarLists;
	/** \brief This provides the interface to the Osmia_Nests linked to the polgons 
	* - it duplicates some functionality of the Landscape but is held here to prevent bloating of landscape code/footprint for other models
	*/
	Bombus_Colony_Manager m_OurColonyManager;


	// Methods
	/** \brief Scale min and max daily temperture to that of the 10min time step. */
	void CalculateTemp();

	/** \brief  Things to do before anything else at the start of a timestep  */
	void DoFirst() override;

	/** \brief Things to do before the EndStep */
	void DoAfter() override;

	/** \brief Things to do after the EndStep */
	void DoLast() override;
};
#endif
