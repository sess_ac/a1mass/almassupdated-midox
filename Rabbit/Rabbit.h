/*
*******************************************************************************************************

Copyright (c) 2015, Christopher John Topping, Faarupvej 54, DK-8410 R�nde
ADAMA Makhteshim Ltd., PO Box 60, Beer-Sheva 84100, Israel

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.

********************************************************************************************************

*/
/** \file Rabbit.h 
\brief <B>The header file for all rabbit classes, and associated classes</B>
*/
/**  \file Rabbit.h
Version of  November 2015 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef RabbitH
#define RabbitH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Rabbit_Population_Manager;
class Rabbit_Female;
class Rabbit_Warren;
class Rabbit_Adult;
class Landscape;

//------------------------------------------------------------------------------
/**
Used for the population manager's list of Rabbit
*/
//typedef vector<Rabbit*> TListOfRabbit;
//---------------------------------------------------------------------------

/**
An enumeration to make working with rabbit objects easier. 
This lists all the rabbit object types
*/
enum RabbitObjectTypes
{
	rob_Young = 0,
		rob_Juvenile,
		rob_Male,
		rob_Female,
		rob_Warren,
		rob_foobar
};

/**
Rabbits like other ALMaSS animals work using a state/transition concept. 
These are the Rabbit behavioural states, these need to be altered, but some are here just to show how they should look.
*/
enum TTypeOfRabbitState
{
      toRabbits_InitialState=0,
      toRabbits_Develop,
      toRabbits_Mature,
      toRabbits_Explore,
	  toRabbits_Gestation,
	  toRabbits_Lactating,
	  toRabbits_GiveBirth,
      toRabbits_Weaning,
	  toRabbits_Foraging,
	  toRabbits_EvaluateTerritory,
	  toRabbits_UpdateBreedingStatus,
	  toRabbits_Remove,
      toRabbits_Die,
	  toRabbits_WarrenBeing
} ;

/** \brief A classification of rabbit habitat types. All landscape elements will be classified into one of the categories below. */
enum TTypesOfRabbitHabitat
{
	torh_Forage = 0,
	torh_TemporaryForage,
	torh_Cover,
	torh_Other,
	torh_foobar
};

/** \brief A classification of rabbit habitat types. All landscape elements will be classified into one of the categories below. */
enum TTypesOfRabbitSocialStatus {
	rabbit_socialstatus_zero = 0,
	rabbit_socialstatus_subordinate,
	rabbit_socialstatus_subdominant,
	rabbit_socialstatus_dominant,
};

/** \brief The basic information needed for an LE present in a warren area. */
struct RabbitWarrenLEInfo
{
	LE* m_ele;
	int m_area;
	int m_ref;
	double m_pesticide_conc;
	double m_forage;
	int m_foragearea;
	TTypesOfRabbitHabitat m_torh;
};

/** \brief The information needed to hold a single rabbit memory. */
struct RabbitMemoryLocation
{
	int m_x;
	int m_y;
	int m_quality;
	int m_decay;
};

/** \brief An entry in the local warren network - a helper list of accessibly local warren locations */
struct LocalWarrenNewtorkEntry
{
	Rabbit_Warren* m_aWarren;
	int m_dist;
    double m_visitprob;
};

/** \brief
* A class to describe the data held in a rabbit memory. Warren locations and quality.
*/
class RabbitMemory
{
public:
	/** \brief Rabbit Memory constructor */
	RabbitMemory( void );
	/** \brief A list of memory locations */
	vector <RabbitMemoryLocation> m_MyMemory;
	/** \brief daily update of the memory */
	void Update( void );
	/** \brief Add a memory location */
	void AddMemory( RabbitMemoryLocation a_mem);
	/** \brief Get the best current location  */
	RabbitMemoryLocation GetBestLocation( void );
};

//************************Rabbit_Base***********************************************************************
class Rabbit_Base : public TAnimal {
	// ATTRIBUTES
public:
	/** \brief The rabbits type */
	RabbitObjectTypes m_RabbitType;
	/** \brief The extra dispersal mortality per m travelled */
	static double m_dispersalmortperm;
	/**	\brief	State variable used to hold the daily degredation rate of the pesticide in the body	*/
	static double m_pesticidedegradationrate;
protected:
	/** \brief The rabbit's age */
	int m_Age;
	/** \brief Variable to record current behavioural state */
	TTypeOfRabbitState m_CurrentRState;
	/** \brief This is a time saving pointer to the correct population manager object */
	Rabbit_Population_Manager*  m_OurPopulationManager;
	/** An individual daily mortality probability */
	double m_MyMortChance;
	/** \brief Flag to record burrow status */
	bool m_haveBurrow;
	/** \brief True if currently mated */
	//bool m_haveMate;
	/** \brief Pointer to the current warren if any */
	Rabbit_Warren* m_myWarren;
	/** \brief Pointer to mum */
	Rabbit_Female* m_Mum;
	/** \brief Flag for been fed today */
	bool m_FedToday;
	/** \brief The weight in g */
	double m_weight;
	/** \brief A physiological age parameter, this is the growth age based on an optimal curve (if optimal conditions it will be the same as m_age) */
	int m_weightAge;
	/** \brief Flag to denote digging behaviour. This keeps the rabbit in a warren without burrows whilst it tries to make one. */
	int m_digging;
	/** \brief The x,y location at birth */
	APoint m_born_location;
	/** \brief	State variable used to hold the current body-burden of pesticide */
	double m_pesticide_burden;
	/**	\brief	Flag to indicate pesticide effects (e.g. can be used for endocrine distruptors with delayed effects until birth).	*/
	bool m_pesticideInfluenced1;

	// METHODS
public:
	/** DEBUG method to get the current state */
	TTypeOfRabbitState GetCurrentRState() {
		return m_CurrentRState;
	}
	/** \brief Set age method */
	void SetAge( int a_age ) {
		m_Age = a_age;
	}
	/** \brief Get rabbit type */
	RabbitObjectTypes GetRabbitType( void ) {
		return m_RabbitType;
	}
	/** \brief Get age method */
	int GetAge( void ) {
		return m_Age;
	}
	/** \brief Set age method */
	void SetweightAge( int a_age ) {
		m_weightAge = a_age;
	}
	/** \brief Get age method */
	int GetweightAge( void ) {
		return m_weightAge;
	}
	/** \brief Get warren pointer */
	Rabbit_Warren* GetWarren( void ) {
		return m_myWarren;
	}
	/** \brief Set the warren pointer */
	void SetWarren( Rabbit_Warren* a_warren ) {
		m_myWarren = a_warren;
	}
	/** \brief Get burrow status */
	bool GetHasBurrow( void ) {
		return m_haveBurrow;
	}
	/** \brief Set/unset burrow status */
	void SetHasBurrow( bool a_status ) {
		m_haveBurrow = a_status;
	}
	/** \brief Set number of days to dig */
	void SetDigging( int a_days ) {
		m_digging = a_days;
	}
	/** \brief Get number of days to dig */
	int GetDigging() {
		return m_digging;
	}
	/** \brief Get location of birth */
	APoint GetBornLocation() {
		return m_born_location;
	}
	/** \brief Rabbit constructor */
	Rabbit_Base(int p_x, int p_y, int p_x2, int p_y2, Landscape* p_L, Rabbit_Population_Manager* p_NPM, Rabbit_Warren* a_warren);
	/** \brief Rabbit destructor */
	virtual ~Rabbit_Base( void );
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void BeginStep( void );
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step( void ) {
		;
	}
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void EndStep( void ) {
		;
	}
	/** \brief Signals food arrived today */
	void OnFed( void ) {
		m_FedToday = true;
	}
	/** \brief Signals death of mum */
	void OnMumDead( void ) {
		m_Mum = NULL;
	}
	/** \brief Signals mum has a new litter to look after */
	void OnEvicted( void ) {
		m_Mum = NULL; m_CurrentRState = toRabbits_Weaning;
	}
	/** \brief Get mate pointer */
	virtual Rabbit_Adult* GetMate( void ) {
		return NULL;
	};
protected:
	/** \brief Exploration method */
	virtual void Explore( void ) {
		;
	}
	/** \brief Default dying state */
	virtual void st_Dying( void );
	/** \brief A simple probability based test */
	bool MortalityTest( double a_prop );
	/** \brief Walks to a location from current location */
	bool WalkTo( int a_x, int a_y );
	/**	\brief	Handles internal effects of pesticide exposure. If any effects are needed this method must be re-implemented by descendent classes.	*/
	virtual void InternalPesticideHandlingAndResponse() {
		;
	}
	/**	\brief	Handles internal effects of endocrine distrupter pesticide exposure.	*/
	virtual void GeneralEndocrineDisruptor( double /* a_pesticide_dose */ ) {
		;
	}
	/**	\brief	Handles internal effects of organophosphate pesticide exposure.	*/
	virtual void GeneralOrganoPhosphate( double /* a_pesticide_dose */ );
};


//************************Rabbit_Young***********************************************************************
/** \brief
* The rabbit young class. All special young behaviour is described here.
*/
class Rabbit_Young : public Rabbit_Base
{
public:
	/** \brief Rabbit_Young constructor */
	Rabbit_Young(int p_x, int p_y, int p_x2, int p_y2, Rabbit_Female* a_mum, Landscape* p_L, Rabbit_Population_Manager* p_NPM, Rabbit_Warren* a_warren);
	/** \brief Rabbit_Young destructor */
	virtual ~Rabbit_Young( void );
	/** \brief Development state for young */
	TTypeOfRabbitState st_Develop( void );
	/** \brief Young maturation */
	TTypeOfRabbitState st_BecomeJuvenile( void );
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in.  */
	virtual void BeingStep(void);
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** \brief The EndStep is the last 'part' of the timestep that an animal can behave in.*/
	virtual void EndStep(void);
	/** \brief Tests for maturation to the next stage */
	virtual bool ShouldMature(void);
protected:
	/** \brief Young dying state */
	virtual void st_Dying( void );
};

//************************Rabbit_Juvenile***********************************************************************
/** \brief
* The rabbit juvenile class. All special juvenile behaviour is described here.
*/
class Rabbit_Juvenile : public Rabbit_Young
{
public:
	/** \brief Rabbit_Young constructor */
	Rabbit_Juvenile(int p_x, int p_y, int p_x2, int p_y2, Rabbit_Female* p_M, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_age, int a_weightage, Rabbit_Warren* a_warren);
	/** \brief Rabbit_Young destructor */
	virtual ~Rabbit_Juvenile( void );
	/** \brief Development state for young */
	TTypeOfRabbitState st_Develop();
	/** \brief Juvenile maturation */
	TTypeOfRabbitState st_BecomeAdult( void );
	/** \brief Juvenile local exploration */
	TTypeOfRabbitState st_Explore( void );
	/** \brief Juvenile forage behaviour */
	TTypeOfRabbitState st_Forage( void );
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** \brief The EndStep is the last 'part' of the timestep that an animal can behave in.*/
	virtual void EndStep(void);
	/** \brief Tests for maturation to the next stage */
	virtual bool ShouldMature(void);
protected:
	/** \brief Young dying state */
	virtual void st_Dying( void );
	/**	\brief	Handles internal effects of pesticide exposure - reimplemented from base class */
	virtual void InternalPesticideHandlingAndResponse();
};
//************************Rabbit_Adult***********************************************************************
/** \brief
* The rabbit adult class. All generic adult behaviour is described here.
*/
class Rabbit_Adult : public Rabbit_Base
{
public:	
// Methods
	/** \brief Rabbit_Adult constructor */
	Rabbit_Adult( int p_x, int p_y, int p_x2, int p_y2, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_age, int a_weightage, Rabbit_Warren* a_warren );
	/** \brief Rabbit_Adult destructor */
	virtual ~Rabbit_Adult();
	/** \brief Return the dominance status */
	TTypesOfRabbitSocialStatus GetSocialStatus( void ) {
		return m_socialstatus;
	}
	/** \brief Sets the dominance status */
	void SetSocialStatus( TTypesOfRabbitSocialStatus a_status ) {
		m_socialstatus = a_status;
	}
	/** \brief Set/unset mate status */
    void SetMate( Rabbit_Adult* a_mate );
	/** \brief Get mate pointer */
    virtual Rabbit_Adult* GetMate( void ) { return m_myMate; };
	/** \brief Action when a mate has finished digging a burrow */
	void OnMateFinishedDigging( Rabbit_Adult* a_mate );
	/** \brief Set/unset mate status */
	void OnMateDead( Rabbit_Adult* a_mate );
protected:
// Attributes
	/** \brief Flag to record dominance status (0-4)*/
	TTypesOfRabbitSocialStatus m_socialstatus;
    /** \brief The rabbit's alloted lifespan */
    int m_lifespan;
	/** \brief Pointer to the mate if any */
	Rabbit_Adult* m_myMate;
 
// Methods
	/** \brief Default dying state */
	virtual void st_Dying( void );
	/** \brief The EndStep is the last 'part' of the timestep that an animal can behave in.*/
	virtual void EndStep(void);
};

//************************Rabbit_Male***********************************************************************
/** \brief
* The rabbit male class. All special male behaviour is described here.
*/
class Rabbit_Male : public Rabbit_Adult
{
public:
	/** \brief Rabbit_Male constructor */
	Rabbit_Male(int p_x, int p_y, int p_x2, int p_y2, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_age, int a_weightage, Rabbit_Warren* a_warren);
	/** \brief Rabbit_Male destructor */
	virtual ~Rabbit_Male( void );
	/** \brief The male rabbit step code */
	virtual void Step( void );
protected:
	/** \brief Male Evaluate Territory Step */
	virtual TTypeOfRabbitState st_EvaluateTerritory( void );
	/** \brief Adult male forage behaviour */
	virtual TTypeOfRabbitState st_Forage(void);
	/**	\brief	Handles internal effects of pesticide exposure - reimplemented from base class */
	virtual void InternalPesticideHandlingAndResponse();
};
//************************Rabbit_Female***********************************************************************
/** \brief
* The rabbit female class. All special female behaviour is described here.
*/
class Rabbit_Female : public Rabbit_Adult
{
public:
// METHODS
	/** \brief Rabbit_Female constructor */
	Rabbit_Female(int p_x, int p_y, int p_x2, int p_y2, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_age, int a_weightage, Rabbit_Warren* a_warren);
	/** \brief Rabbit_Female destructor */
	virtual ~Rabbit_Female( void );
	/** \brief The female rabbit step code */
	virtual void Step( void );
	/** \brief Add a young */
	void AddYoung(Rabbit_Young* a_young) 
	{ 
		m_myLitter[m_myLitterSize++] = a_young; 
	}
	/** \brief Message on death of a young */
	void OnYoungDeath(Rabbit_Young* a_young);
	/** \brief Message on weaning of a young */
	void Weaned(Rabbit_Young* a_young);
	void SetMinKits(double a_num) { m_MinKitsNo = a_num; }
	void SetMaxKits(double a_num) { m_MaxKitsNo = a_num; }
	int GetTotalOffspring() { return m_MyOffspring; }
	int GetTotalLitters() { return m_MyTotalLitters; }
	int GetLittersThisYear() {
		return static_cast<int>(m_AnnualLitters.size());
	}

protected:
	/** \brief Female Evaluate Territory Step */
	virtual TTypeOfRabbitState st_EvaluateTerritory( void );
	/** \brief Female reproductive update - handles oestrous, gestation, lactation and birth */
	virtual TTypeOfRabbitState st_UpdateBreedingStatus( void );
	/** \brief Female give birth state */
	virtual TTypeOfRabbitState st_GiveBirth( void );
	/** \brief Female lactating state */
	virtual TTypeOfRabbitState st_Lactating( void );
	/** \brief Female gestating state */
	virtual TTypeOfRabbitState st_Gestating( void );
	/** \brief Adult female forage behaviour */
	virtual TTypeOfRabbitState st_Forage(void);
	/** \brief Female dying state */
	virtual void st_Dying( void );
	/** \brief Calculates the litter size at birth */
	int CalcLitterSize( void );
// ATTRIBUTES
	/** \brief Flag to indicate pregnancy */
	bool m_pregnant;
	/** \brief Flag to indicate lactating */
	bool m_lactating;
	/** \brief Counter to record the number of days gestating */
	int m_gestationcounter;
	/** \brief  Holds the current young (for lactation) */
	Rabbit_Young* m_myLitter[20];
	/** \brief  Holds the current litter size */
	int m_myLitterSize;
	/** \brief The minimum number of kits*/
	static double m_MinKitsNo;
	/** \brief The maximum number of kits*/
	static double m_MaxKitsNo;
	/** \brief The total number of kits born to her*/
	int m_MyOffspring;
	/** \brief The number of litters produced */
	int m_MyTotalLitters;
	/** \brief The number of litters produced */
	vector<int> m_AnnualLitters;
	/**	\brief	Handles internal effects of pesticide exposure - reimplemented from base class */
	virtual void InternalPesticideHandlingAndResponse();
	/**	\brief	Handles internal effects of endocrine distrupter pesticide exposure.	*/
	virtual void GeneralEndocrineDisruptor( double /* a_pesticide_dose */ );
};

//************************Rabbit_Warren***********************************************************************
/** \brief
* A class to describe the rabbits warren system
*/
class Rabbit_Warren : public TAnimal
{
/**
* From http://informedfarmers.com/rabbit-reproduction/ <br>
* The size of a warren depends on the soil type. Typically, warrens can be around two metres deep, although warrens in sandy soils are more likely to be smaller and have fewer 
* underground interconnections than warrens in hard soils.<br>
* This is because it is easier for rabbits to start a new warren in sandy soil but in harder, clay-type soils it is easier to extend an existing warren. 
* On average, one warren will have 3 to 15 entrances, with each active entrance likely to house two adult rabbits.
*
*
* Warren carrying capacity is assumed to be maximum if the 90% of whole area is under permanent forage. Max CC is 15 pairs. We assume that the number of pais is linearly related
* to the area above a minimum required for 3 pairs.
*
* The warren is a square area of the landscape which is of a standard size and must contain a certain proportion of permanent forage habitat (input parameter RABBIT_WARRENMINPCTFORAGE ).
* The warren manages its own list of resident rabbits and is responsible for ensuring that the number of rabbits does not rise above its carrying capacity which is determined by the 
* total forage available. Maximum possible carrying capacity is 15 burrows, minimum for a warren to exist is 3.<br>
* Daily actual carrying capacity is determined by the number of burrows possible and the number that are actually present. On creation there is no burrows present and a rabbit must 
* first occupy the warren then dig the burrow before breeding can start. Subsequently the warren will grow as rabbits dig further burrows if the warren is continually occupied. 
* After 2 years it will be assumed to have reached the maximum number of burrows possible.
*/
public:
// ATTRIBUTES
	/** \brief The maximum vegetation height assumed for forage potential */
	static double m_maxForageHeight;
	/** \brief The minimum vegetation digestability allowed for foraging */
	static double m_minForageDigestibility;
	/** \brief The number of females that bred this year */
	int m_breedingfemales;
	/** \brief The number of 1 year old females */
	int m_1yrOldFemales;
	/** \brief The number of litters produced this year */
	int m_littersthisyear;
	/** \brief The number of females that did not breed this year but are older than 1 year */
	int m_nonbreedingfemales;
// METHODS
	/** \brief Rabbit warren constructor */
	Rabbit_Warren(int p_x, int p_y, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_size, int a_soil);
	/** \brief Rabbit warren destructor */
	virtual ~Rabbit_Warren();
	/** \brief Warren step code */
	virtual void Step();
	/** \brief Warren begin step code */
	virtual void BeginStep() {
		CalcCarryingCapacityRatio1();
	}
	/** \brief Get m_TL_x */
	int Get_TL_x() { return m_TL_x; }
	/** \brief Get m_TL_x */
	int Get_TL_y() { return m_TL_y; }
	/** \brief Get the TL coords as a point */
	APoint GetPointTL() { APoint TL; TL.m_x=m_TL_x; TL.m_y=m_TL_y; return TL; }
	/** \brief Gets the number of occupied burrows */
	int GetActiveBurrows() { return m_NoOccupiedBurrows; }
	/** \brief Gets the total warren population of rabbits */
	int GetPopulationSize() { return (int)m_InhabitantsList.size(); }
	/** \brief Checks whether all possible burrows are filled with rabbits*/
	bool GetCarryingCapacityFilled() { return (m_CarryingCapacity >= m_NoOccupiedBurrows); }
	/** \brief Checks whether all possible burrows are filled with rabbits. This is updated daily by Rabbit_Warren::UpdateForageInformation */
	double GetCarryingCapacityRatio() { return m_CarryingCapacityRatio; }
	/** \brief Returns the carrying capacity in burrows */
	int GetCarryingCapacity() { return m_CarryingCapacity; }
	/** \brief Returns the warrens current disease constant */
	double GetDiseaseConstant(void) { return m_diseaseconstant; }
	/** \brief Stores data about production of rabbits throughout year*/
	void RabbitProductionRecord(RabbitObjectTypes YoungType, int kits) { m_ThisYearsProduction[YoungType] += kits; }
	/** \brief Get data about production of rabbits throughout year*/
	int GetRabbitProductionRecord(RabbitObjectTypes YoungType) { return m_ThisYearsProduction[YoungType]; }
	/** \brief Reset specific data about production of rabbits throughout year*/
	void ResetRabbitProductionRecord(RabbitObjectTypes YoungType) { m_ThisYearsProduction[YoungType] = 0; }
	/** \brief Reset data about production of rabbits throughout year*/
	void ResetAllRabbitProductionRecord(void) { for (int r = (int) rob_Young; r < (int) rob_foobar; r++)  m_ThisYearsProduction[r] = 0; }
	/** \brief Adds a warren to the local network list. */
    void AddNetworkConnection( LocalWarrenNewtorkEntry a_LWNE ) { m_LocalWarrenNetwork.push_back( a_LWNE ); }
	/** \brief Chooses a warren to evaluate based on distance */
	Rabbit_Warren* GetNetworkWarren( void );
	/** \brief Calculates and saves distance probabilities */
	void NetworkEvaluation( void );
	/** \brief Returns true if this rabbit belongs to the warren */
	bool IsMember( Rabbit_Base* a_rabbit );
	/** \brief Returns true if there is a female with a burrow and no mate */
	bool IsFreeFemale( void );
	/** \brief Returns true if there is a male with a burrow and no mate */
	bool IsFreeMale( void );
	/** \brief Checks for a dominant female and promotes one if necessary and possible */
	void UpdateDominance( void );
	/** \brief Finds a subdominantfemale and promotes them to dominant */
	void ChooseNewDominant( void );
	/** \brief Is there a vacent burrow? */
	int IsFreeBurrow( void );
	/** \brief Remove this adult from the warren list */
	void Leave( Rabbit_Base* a_rabbit );
	/** \brief Adds this rabbit to the warren list */
	void Join( Rabbit_Base* a_rabbit );
	/** \brief Adds the first rabbit to the warren list */
	void OccupyWarren( Rabbit_Adult* a_rabbit );
	/** \brief Adds this adult to the warren list and house them in a suitable burrow */
	void JoinNOccupy( Rabbit_Adult* a_rabbit );
	/** \brief Adds this rabbit to the warren list and mate him with un-mated female/male with a burrow */
	void JoinNMate(Rabbit_Adult* a_mate, RabbitObjectTypes rob_type);
	/** \brief Mate him with un-mated female/male with a burrow */
	void Mate(Rabbit_Adult* a_mate, RabbitObjectTypes rob_type);
	/** \brief a_rabbit occupies a newly dug burrow */
	void OccupyNewBurrow();
	/** \brief a_rabbit occupies a free burrow */
	void OccupyBurrow();
	/** \brief Supplies the number of breeding rabbits currently in the warren */
	int GetAllBreedingFemaleRabbits();
	/** \brief Supplies the number of big rabbits currently in the warren */
	int GetAllBigFemaleRabbits();
	/** \brief Gets the current mean pesticide concentration per unit forage */
	double GetForagePesticide( void ) {
		return m_forageP;
	}
	/** \brief Gets the warren soil type */
	int GetSoilType(void) { return m_soiltype; }
	//* \brief Debugging method */
	bool DEBUG_InternalTest();
	//* \brief Debugging method */
	bool DEBUG_InternalTest2();
	/** \brief Supply the current disease mortality constant */
	double Disease() { return m_diseaseconstant; }
	/** \brief Calculate the current disease mortality constant */
	void CalcDisease();
	/** \brief Returns the available forage realtive to rabbit numbers */
	double GetAvailableForage( void ) {
		return m_availableforage;
	}
	/** \brief Returns the inverse of available forage realtive to rabbit numbers */
	double GetInvAvailableForage( void ) {
		return m_inv_availableforage;
	}
	/** \brief Returns litter reabsorption chance */
	double GetLitterReabsortionConst( void ) {
		return m_litterreabosorptionchance;
	}
	/** \brief Returns the number of females breeding this year */
	int GetThisYearsBreeders() {
		return m_breedingfemales;
	}
	/** \brief Returns the number of females breeding this year */
	double GetThisYearsBreedersAv() {
		double av = 0;
		if (m_runningavCount>0) av = m_runningavFemales / static_cast<double>(m_runningavCount);
		m_runningavFemales = 0;
		m_runningavCount = 0;
		return av;
	}
	/** \brief Returns the number of 1yr old females */
	int GetThisYears1yrOldFemales() {
		return m_1yrOldFemales;
	}
	/** \brief Returns the number litters produced in the last 12 months */
	int GetLittersThisYear(){
		return m_littersthisyear;
	}
	/** \brief Returns the number of females not breeding this year but older than 1 year */
	int GetThisYearsNonBreeders() {
		return m_nonbreedingfemales;
	}
	/** \brief Calculates and stores the number of breeders and non-breeders */
	void UpdateThisYearsBreeders();
	/** \brief Returns the adult daily mortality multiplier */
	double GetDailyMortalityChanceA() {
		return m_mortalitymultiplierA;
	}
	/** \brief Returns the juvenile daily mortality multiplier */
	double GetDailyMortalityChanceJ() {
		return m_mortalitymultiplierJ;
	}
	/** \brief Records the number of big female rabbits for carrying capacity calculations */
	double GetCCRabbitsR() {
		return m_BigFemaleRabbitsR;
	}
protected:
// ATTRIBUTES
	/** \brief Warren core size in m */
	int m_size;
	/** \brief Warren forage size in m */
	int m_foragesize;
	/** \brief Stores original m_Location_x */
	int m_TL_x;
	/** \brief Stores original m_Location_y */
	int m_TL_y;
	/** \brief Warren maintenence score - if not max then houseing capacity is reduced */
	int m_maintenence;
	/** \brief Records the max number of rabbit pairs possible here */
	int m_CarryingCapacity;
	/** \brief Records the max number of rabbits possible here */
	double m_CarryingCapacityR;
	/** \brief Records the max number of rabbits possible here divided by 2 */
	double m_CarryingCapacityR2;
	/** \brief Records the number of big female rabbits divided by m_CarryingCapacityR2 */
	double m_BigFemaleRabbitsR;
	/** \brief Records the ratio between carrying capacity and no rabbits in warren */
	double m_CarryingCapacityRatio;
    /** \brief Records the maintenence restricted current carrying capacity (burrow number) */
	int m_NoBurrows;
    /** \brief Records the burrows that are occupied */
	int m_NoOccupiedBurrows;
	/** \brief Records the burrows that are being dug */
	int m_BurrowsUnderConstruction;
	/** \brief Variable holding the soil type, 1 = heavy, 0 = sandy, 3 = unsuitable (never used in warrens)  */
	int m_soiltype;
	/** \brief The time taken for burrow construction */
	int m_rabbitdiggingtime;
	/** \brief Variable holding the total permanent forage area */
	int m_permforagearea;
	/** \brief Variable holding the total potential forage area */
	int m_foragearea;
	/** \brief Records the amount of forage currently available in the warren area as a proportion of what is the potential relative to rabbit numbers possible */
	double m_availableforage;
	/** \brief Records the amount of forage currently available in the warren area as a proportion of total forage area */
	double m_foragearearatio;
	/** \brief The inverse of m_availableforage, prevents multiple re-calculation */
	double m_inv_availableforage;
	/** \brief Chance of litter reaborption based on the m_availableforage */
	double m_litterreabosorptionchance;
    /** \brief Records the amount of pesticde as a mean concentration per unit area forage */
	double m_forageP;
	/** \brief a measure of disease mortality likelihood */
	double m_diseaseconstant;
	/** \brief a measure of mortality likelihood - adults */
	double m_mortalitymultiplierA;
	/** \brief a measure of mortality likelihood - juveniles */
	double m_mortalitymultiplierJ;
	/** \brief Storage for rabbit production data */
	int m_ThisYearsProduction[rob_foobar];
    /** \brief Variable to record current behavioural state */
    TTypeOfRabbitState m_CurrentRState;
	/** \brief List of polygons and the area which is part of the warren */
	vector<RabbitWarrenLEInfo> m_LEList;
	/** \brief List of local warrens and their accessibility from this warren */
	vector<LocalWarrenNewtorkEntry> m_LocalWarrenNetwork;
	/** \brief List of rabbits that live here - for easy communication purposes */
	vector<Rabbit_Base*> m_InhabitantsList;
    /** \brief This is a time saving pointer to the correct population manager object */
    Rabbit_Population_Manager*  m_OurPopulationManager;
	/** \brief Keeps track of the number of breeding females */
	int m_runningavFemales;
	/** \brief Keeps track of the number of breeding days */
	int m_runningavCount;

// METHODS
	/** \brief The only warren behaviour - it just is */
	void st_WarrenBeing( void );
	/** \brief Intiates the evaluation of the warren area */
	void InitEvaluation( void );
	/** \brief Returns the total area of permanent forage */
	int GetForageArea( void ) { return m_permforagearea; }
	/** \brief Returns the total area of forage */
	int CalcForageArea(void);
	/** \brief Returns the total area of permanent forage */
	int CalcPermForageArea(void);
	/** \brief Returns the total area of temporary forage */
	int GetTemporaryForageArea( void );
	/** \brief Returns the total area of cover */
	int GetCoverArea( void );
	/** \brief Returns the total area of temporary forage based on veg height  */
	int GetTempForageAreaVeg( void );
	/** \brief Returns the total area of cover based on veg height */
	int GetCoverAreaVeg( void );
	/** \brief Updates the forage information depending upon the vegetation state */
	void UpdateForageInformation(void);
	/** \brief Updates the pesticide concentration in forage polygons */
	void UpdatePesticide(void);
	/** \brief calculates the ratio of rabbits to the carrying capacity */
	void CalcCarryingCapacityRatio1();
	/** \brief calculates the ratio of rabbits to the carrying capacity for the local area */
	double CalcCarryingCapacityRatio2();
};


#endif