/*
*******************************************************************************************************
Copyright (c) 2015, Christopher John Topping, Faarupvej 54, DK-8410 R�nde 
ADAMA Makhteshim Ltd., PO Box 60, Beer-Sheva 84100, Israel

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
********************************************************************************************************

*/
/** \file Rabbit_Population_Manager.h 
\brief <B>The main source code for the rabbit population manager and any associated classes</B>
*/

/**  \file Rabbit_Population_Manager.h
Version of  November 2015 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef Rabbit_Population_ManagerH
#define Rabbit_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Rabbit_Base;

//------------------------------------------------------------------------------

/**
\brief
Used for creation of a new Rabbit object
*/
class struct_Rabbit
{
public:
	/** \brief x-coord */
	int m_x;
	/** \brief y-coord */
	int m_y;
	/** \brief x-coord of birth */
	int m_x2;
	/** \brief y-coord of birth */
	int m_y2;
	/** \brief Landscape pointer */
	Landscape* m_L;
	/** \brief Rabbit_Population_Manager pointer */
	Rabbit_Population_Manager * m_NPM;
	/** \brief The rabbit age */
	int m_age;
	/** \brief The rabbit age in terms of weight */
	int m_weightage;
	/** \brief A pointer to a rabbit base - useful for extra information */
	Rabbit_Young* m_rabbit;
	/** \brief A pointer to the current warren */
	Rabbit_Warren* m_Warren;
	/** \brief The current warren soil type */
	int m_soil;
};

/**
\brief
The class to handle all predator population related matters
*/
class Rabbit_Population_Manager : public Population_Manager
{
public:
// Methods
   /** \brief Rabbit_Population_Manager Constructor */
   Rabbit_Population_Manager(Landscape* L);
   /** \brief Rabbit_Population_Manager Destructor */
   virtual ~Rabbit_Population_Manager (void);
   /** \brief Method for creating a new individual Rabbit */
   void CreateObjects(RabbitObjectTypes ob_type, TAnimal *pvo, struct_Rabbit* a_data, int a_number);
   /** \brief Classify a landscape element type according to the rabbit habitat classification. */
   TTypesOfRabbitHabitat ClassifyHabitat(TTypesOfLandscapeElement a_tole);
   /** \brief Finds the closest warren as the crow flies */
   Rabbit_Warren* FindClosestWarren(int a_x, int a_y, int a_rank);
   /** \brief Determines whether it is breeding season */
   void CheckForRabbitBreedingConditions( void );
   /** \brief Get whether it is breeding season */
   bool IsBreedingSeason( void ) { return m_rabbitBreedingSeason; }
   /** \brief Records pesticide poisoning incidents */
   void PesticideDeathRecord(RabbitObjectTypes ob_type);
   /** \brief Returns the total number of rabbits */
   unsigned SupplyAllBigRabbits() {
	   unsigned sum = 0;
	   for (unsigned r = rob_Male; r < rob_foobar; r++) sum += (unsigned) SupplyListSize(r);
	   return sum;
   }
   /** \brief Get method for the rabbit growth with age */
   double GetGrowth( int a_age ) {
//	   if ((a_age < 0) || (a_age >= 3650)) {
//		   int rubbish = 1;
//	   }
	   return m_RabbitGrowth[ a_age ];
   }
   /** \brief Get method for the forage day flag */
   bool GetForageDay( ) {
	   return m_forageday;
   }
protected:
// Attributes
	/** \brief Holds an input variable for max warren size in m  */
	int m_warrenfixedsize;
	/** \brief Half max warren size in m - for speed  */
   int m_warrenfixedsizediv2;
   /** \brief The breeding season flag */
   bool m_rabbitBreedingSeason;
   /** \brief Prevents continuous breeding season switching */
   int m_reproswitchbuffer;
   /** \brief Holds daily growth potential of rabbits for each day  */
   double m_RabbitGrowth[3650];
   /** \brief Holds the number of rabbits killed each day by pesticides */
   int m_PesticideDeaths[rob_foobar];
   /** \brief The warren occupancy output file */
   ofstream m_WarrenOccupancyFile;
   /** \brief For recording the natal dispersal of adults from birth to death */
   ofstream m_NatalDispersalFile;
   /** \brief For recording the lifetime reproductive success */
   ofstream m_ReproOutputFile;
   /** \brief The pesticide death output file */
   ofstream m_PesticideDeathOFile;
   /** \brief Flag to record whether today is a possible forage day */
   bool m_forageday;
 // Methods
   /** \brief  Things to do before anything else at the start of a timestep  */
   virtual void DoFirst();
   /** \brief Things to do before the Step */
   virtual void DoBefore(){}
   /** \brief Things to do before the EndStep */
   virtual void DoAfter(){}
   /** \brief Things to do after the EndStep */
   virtual void DoLast();
   /** \brief This pre-scans the landscape and determines all potential warren locations on start-up */
   void PreProcessWarrenLocations();
   /** \brief Tests the warrens list to see if this position is legal */
   bool WarrenLegalPos(int &a_x, int a_y);
   /** \brief Save warren locations. */
   void  SaveWarrenLocations( void );
   /** \brief Load warren locations. */
   void  LoadWarrenLocations( void );
   /** \brief Assesses the percentage of forage for a location assuming max warren size. */
   double AssessPctForage(int a_x, int a_y);
   /** \brief Forms the local warren network list for this warren */
   void CreateLocalWarrenNetworkLists( void );
   /** \brief Print warren occupancy record to file */
   void WarrenOccupancyRecordOutput();
   /** \brief Print pesticide death record to file */
   void PesticideDeathRecordOutput();
   /** \brief Opens pesticide death output file */
   void PesticideDeathRecordOutputOpen();
   /** \brief Closes the pesticide death output file */
   void PesticideDeathRecordOutputClose();
   /** \brief Opens the warren occupancy output file */
   void WarrenOccupancyRecordOutputOpen();
   /** \brief Closes the warren occupancy output file */
   void WarrenOccupancyRecordOutputClose();
   /** \brief Does analysis on the warren output file and saves the results to RabbitPOMSummary.txt */
   void WarrenOutputAnalysis();
   /** \brief Does analysis on the lifetime reproductive output file and saves the results to RabbitPOMSummary.txt */
   void LifetimeReproAnalysis();
   /** \brief Does analysis on the natal dispersal output file and saves the results to RabbitPOMSummary.txt */
   void NatalDispersalAnalysis();
   /** \brief Opens the reproductive output file */
   void ReproOutputRecordOutputOpen();
   /** \brief Closes the reproductive output file */
   void ReproOutputRecordOutputClose();
   /** \brief Opens the reproductive output file */
   void NatalDispersalRecordOutputOpen();
   /** \brief Closes the reproductive output file */
   void NatalDispersalRecordOutputClose();
   /** \brief Assigns any static variables needing config variable assignment */
   void AssignStaticVariables();
   /** Calculates the AOR data for output for the Rabbit */
   void TheAOROutputProbe();
public:
	/** \brief Print reproductive record to file */
	void ReproOutputRecordOutput(Rabbit_Female* a_female);
	/** \brief Records the natal dispersal */
	void NatalDispersalRecordOutput(Rabbit_Base* a_rabbit);
};

#endif