ALMaSS Rabbit ODdox Documentation                        {#Rabbit_page}
=========
<br>
<br>
Created by: <br>
<b>Chris J. Topping </b><br>
Dept. Bioscience, <br> 
Aarhus University, <br>
DK-8410, <br>
Roende, <br>
Denmark <br>
11th July 2016\n

<h1>Overview</h1>

##1. Purpose
The rabbit model has been developed with the intention to determine the resilience of the population to agrochemical usage assuming that the stressors affect mortality or reproduction
of rabbits exposed.The aim will be to determine how toxic, and over what area and time, chemicals would have to be applied before significant or lasting rabbit population damage is 
predicted.<br>
<br>
##2. State variables and scales
Individuals in the rabbit model are represented by objects of four different classes, and there is a fifth class to represent locations for warrens and if they are occupied 
the warrens themselves. This means that the individual rabbits are classified as being either Young, Juveniles, adult Males, or adult Females.
Each class has a number of attributes and behaviours associated with it, and these attributes may be transferred between classes e.g. as young mature to become juveniles, or juveniles 
to adult.<br>
All of these classes are based on an ancestor class Rabbit_Base which has some basic functionality common to all rabbits. 
Additionally, a class Rabbit_Adult exists which has functionality common to adult rabbits. 
These two classes are simply programming constructs and don't affect the model behaviour, and there are never rabbits in the simulation based on these classes. 
Note however, that the attributes possessed by Rabbit_Base are inherited by all Rabbit classes, and those possessed by Rabbit_Adult are inherited by Rabbit_Male and Rabbit_Female.<br>

Warrens require special mention. The Rabbit_Warren class handles warren functionality. 
The warren is a square area of the landscape which is of a standard size and must contain a certain proportion of permanent forage habitat (input parameter RABBIT_WARRENMINPCTFORAGE ).
The warren manages its own list of resident rabbits and is responsible for ensuring that the number of rabbits does not rise above its carrying capacity which is determined by the 
total forage available.<br>
Carrying capacity is determined by the number of burrows possible and the number that are actually present. On creation there is no burrows present and a rabbit must first occupy
the warren then dig the burrow before breeding can start. Subsequently the warren will grow as rabbits dig further burrows if the warren is continually occupied. After 2 years it 
will be assumed to have reached the maximum number of burrows possible.<br>
<br>
A class attribute overview is presented below, navigate to the individual entry to obtain further details, or look at 'Process Overview and Scheduling' for functions and behaviours 
below:<br>
<br>
###Rabbit Base
	- Attribute: Rabbit_Base::m_Age The age of the rabbit.
	- Attribute: Rabbit_Base::m_dispersalmortperm The extra dispersal mortality per m travelled.
	- Attribute: Rabbit_Base::m_RabbitType The rabbits type (young, juvenile, male, or female).
	- Attribute: Rabbit_Base::m_CurrentRState Variable to record current behavioural state.
	- Attribute: Rabbit_Base::m_OurPopulationManager This is a time saving pointer to the correct population manager object.
	- Attribute: Rabbit_Base::m_MyMortChance An individual daily mortality probability.
	- Attribute: Rabbit_Base::m_haveBurrow Flag to the rabbit's record burrow status.
	- Attribute: Rabbit_Base::m_myWarren Pointer to the current warren if any.
	- Attribute: Rabbit_Base::m_Mum Pointer to mum.
	- Attribute: Rabbit_Base::m_FedToday Flag for having been fed today.

###Rabbit Young
	- No unique attributes
 
###Rabbit Juvenile
	- No unique attributes

###Rabbit Adult
The adult class handles those attributes and functions common to all adults. This is currently only mate and dominance status.
	- Attribute: Rabbit_Adult::m_dominance Flag to record dominance status (currently unused).
	- Attribute: Rabbit_Adult::m_myMate Pointer to the mate if any, otherwise holds NULL. 

###Rabbit Male
	- No unique attributes

###Rabbit Female
	- Attribute: Rabbit_Female::m_pregnant Flag to indicate pregnancy (true = pregnant).
	- Attribute: Rabbit_Female::m_lactating Flag to indicate lactating (true = lactating).
	- Attribute: Rabbit_Female::m_gestationcounter Counter to record the number of days gestating.
	- Attribute: Rabbit_Female::m_myLitter Holds the current young (so the rabbit knows which young objects to feed).
	- Attribute: Rabbit_Female::m_myLitterSize Holds the current litter size.

###Rabbit Warren
	- Attribute: Rabbit_Warren::m_size Warren size in m (TL to TR).
	- Attribute: Rabbit_Warren::m_TL_x Stores original m_Location_x
	- Attribute: Rabbit_Warren::m_TL_y Stores original m_Location_y.
	- Attribute: Rabbit_Warren::m_maintenence Warren maintenence score - if not max then houseing capacity is reduced.
	- Attribute: Rabbit_Warren::m_CarryingCapacity. Records the max number of rabbit pairs possible here.
	- Attribute: Rabbit_Warren::m_NoBurrows Records the maintenence restricted current carrying capacity (burrow number).
	- Attribute: Rabbit_Warren::m_NoOccupiedBurrows Records the number of burrows that are occupied.
	- Attribute: Rabbit_Warren::m_permforagearea Variable holding the total permanent forage area.
	- Attribute: Rabbit_Warren::m_availableforage Records the amount of forage currently available in the warren area.
	- Attribute: Rabbit_Warren::CurrentRState Variable to record current behavioural state. Needs to be here, but there is only one warren state.
	- Attribute: Rabbit_Warren::m_LEList List of polygons and the area which is part of the warren.
	- Attribute: Rabbit_Warren::m_LocalWarrenNetwork List of local warrens and their accessibility from this warren.
	- Attribute: Rabbit_Warren::m_InhabitantsList List of rabbits that live here - for easy communication purposes.
	- Attribute: Rabbit_Warren::m_OurPopulationManager This is a time saving pointer to the correct population manager object.
	- Attribute: Rabbit_Warren::m_forageP Records the amount of pesticide as a mean concentration per unit area forage. Used only for pesticide ERA and similar applications.


<h2>  3. Process Overview and Scheduling</h2>
<br>
As with all ALMaSS models the rabbit model is based on a state machine which can be described by a state-transition diagram. 
Rabbits are in a behavioural state and due to decisions or external events move via transitions to another state where they will exhibit 
different behaviour.<br>
###Base States & Functions
	- Behaviour: Rabbit_Base::st_Dying This simply removes the object tidily from the simulation. Any information passing to other rabbits needs to be done before calling this behaviour (e.g. tell mother of young death).
	- Function: Rabbit_Base::Rabbit_Base The constructor for the Rabbit_Base class.
	- Function: Rabbit_Base::~Rabbit_Base Rabbit destructor.
	- Function: Rabbit_Base::GetAge Age get functionality.
	- Function: Rabbit_Base::GetHasBurrow Get burrow status.
	- Function: Rabbit_Base::GetRabbitType Returns the type of rabbit (young, juvenilem male or female).
	- Function: Rabbit_Base::GetWarren Get the rabbit's warren pointer.
	- Function: Rabbit_Base::MortalityTest A simple probability test. Input is between 0 and 1.0 exclusive, returns true for mortality, and false for no mortality.
	- Function: Rabbit_Base::OnEvicted Signals mum has a new litter to look after.
	- Function: Rabbit_Base::OnFed Signals food arrived today.
	- Function: Rabbit_Base::OnMumDead Signals death of mum.
	- Function: Rabbit_Base::SetAge Age set functionality.
	- Function: Rabbit_Base::SetHasBurrow Set/unset burrow status.
	- Function: Rabbit_Base::SetWarren Set the warren pointer.
	- Function: Rabbit_Base::st_Dying Default dying state.
	- Function: Rabbit_Base::WalkTo Walks to a location from current location.

###Young States & Functions
	- Behaviour: Rabbit_Young::st_Develop Currently this simple ages the rabbit, calls for a mortality test (probability) and tests for maturation age.
	- Behaviour: Rabbit_Young::st_BecomeJuvenile The young object is replaced in the simulation with a Rabbit_Juvenile object.
	- Function:	Rabbit_Young::Step This method controls the flow of behaviour.
	- Function:	Rabbit_Young::ShouldMature Tests for weaning. Currently the maturation criteria is simply age (21 days old). <br>
\image html RabbitYoungStates.jpg
Figure 1. State transiton diagram for Rabbit_Young class.

###Juvenile States & Functions
	- Behaviour: Rabbit_Juvenile::st_Develop Currently this simple ages the rabbit, calls for a mortality test (probability) and tests for maturation age. If this is reached then the return value causes the maturation behaviour to be called.
	- Behaviour: Rabbit_Juvenile::st_BecomeAdult The juvenile object is replaced in the simulation with a Rabbit_Male or Rabbit_Female object. Currently the probability of males is 50%.
	- Behaviour: Rabbit_Juvenile::st_Explore Can be used to evaluate the local area.
	- Function:	Rabbit_Juvenile::Step This method controls the flow of behaviour.
	- Function:	Rabbit_Juvenile::ShouldMature Tests for maturation to the next stage. Currently the maturation criteria is simply age (105 days old).
\image html RabbitJuvenileStates.jpg
Figure 2. State transiton diagram for Rabbit_Juvenile class.

###Adult States & Functions
	- Function:	Rabbit_Adult::Rabbit_Adult Rabbit_Adult constructor.
	- Function:	Rabbit_Adult::~Rabbit_Adult Rabbit_Adult destructor.
	- Function:	Rabbit_Adult::GetDominance Return the dominance status.
	- Function:	Rabbit_Adult::SetMate Set/unset mate status.
	- Function:	Rabbit_Adult::OnMateDead. Message handler invoked if the rabbit loses its mate.


###Male States & Functions
	- Behaviour: Rabbit_Male::st_EvaluateTerritory The rabbit must evaluate his territory and decide if he will stay or leave. This decision is based on a number of criteria:
		1. An evaluation of the current mate status
		2. An evaluation of the current territory quality
		3. A comparison with potential areas explored nearby. Here we make the assumption that if suitable quality unoccupied territory is known to be available and the rabbit is not resident in a burrow, then it will move to set up a territory there. If none is avaible or known, then the rabbit will remain as subordinate as long as the current territory can support him. If he is forced out he must find a vacent place or will die.<br>
\image html RabbitMaleStates.png
Figure 3. State transiton diagram for Rabbit_Male class.


###Female States & Functions
	- Behaviour: Rabbit_Female::st_GiveBirth Female give birth state. Produces kits. If currently lactating then expels the current young. If it is the breeding season and she has a mate, then she becomes pregnant again immediately. The number of young produced is controlled by Rabbit_Female::CalcLitterSize.
	- Behaviour: Rabbit_Female::st_Lactating Female lactating state. Each young in the young list is fed until it is either weaned, dies or is booted out due to the arrival of a new litter.
	- Behaviour: Rabbit_Female::st_Gestating Female gestating state. No special behaviour here, it is all handled by Rabbit_Female::st_UpdateBreedingStatus
	- Behaviour: Rabbit_Female::st_Dying Female dying state. This has some special behaviour associated with the young lists, otherwise calls the Adult dying state.
	- Behaviour: Rabbit_Female::st_EvaluateTerritory. The rabbit must evaluate her territory and decide if she will stay or leave. This decision is based on a number of criteria:
		1. An evaluation of the current mate status
		2. An evaluation of the current territory quality
		3. A comparison with potential areas explored nearby. Here we make the assumption that if suitable quality unoccupied territory is known to be available and the rabbit is not resident in a burrow, then it will move to set up a territory there. If none is avaible or known, then the rabbit will remain as subordinate as long as the current territory can support her. If she is forced out she must find a vacent place or will die.<br>
	- Behaviour: Rabbit_Female::st_UpdateBreedingStatus Female reproductive update which handles oestrous, gestation, lactation and birth. Determines where we are in the reproductive cycle. Will hold off or switch on oestrous depending upon season, otherwise will update gestation, continue lactating, mate or give birth as necessary. This uses a number of flags and counters specific to the female rabbit to achieve this.NB if she comes into oestrous and has a mate she will automatically become pregnant. Whether oestrous happens depends on her age and on the weather. Initially we assume that the average temperature over the last month determines initiation of breeding. Cessation may be more problematical, but we assume a look ahead function for temperature using a running 30 day mean. If lactating then the young are forced out at 30 days, when she can be giving birth again if it is still breeding season. If it is not breeding season and nothing else is happening then there is a transition to evaluate territory. This requires setting StepDone to true to prevent continuous looping.
	- Function: Rabbit_Female::Rabbit_Female Rabbit_Female constructor.
	- Function: Rabbit_Female::~Rabbit_Female Rabbit_Female destructor.
	- Function: Rabbit_Female::Step The female rabbit step code. The female step code is the most complicated of the rabbit behavioural control. It is very similar to the male's code but with the addition of the behaviours related to reproduction. Each day the cycle involves an evaluation of the current resource status, and then updating the reproductive behaviour.Exactly which behaviour needs to be updated depends on the current status and whether it is breeding season, and of course whether there is a mate.
	- Function: Rabbit_Female::AddYoung. Adds a young to the list of young
	- Function: Rabbit_Female::OnYoungDeath This is a message received on death of a young. The young will be removed from the young list.
	- Function: Rabbit_Female::Weaned This message is received if a young is weaned and needs to be removed from the young list.
	- Function: Rabbit_Female::CalcLitterSize. A function which calculates the litter size at birth. Litter sizes are 3-8 young (controlled by configuration variables), but what controls this is not yet implemented, currently the results are stochastic between these extremes.
\image html RabbitFemaleStates.png
Figure 4. State transition diagram for Rabbit_Female class.


###Warren States & Functions
The warren holds all the information about the cover and forage resources available to the rabbit. In addition it has a list of rabbits that are resident. The number of breeding pairs is controlled by the number of burrows, which itself is controlled by the resources and calculated as a carrying capacity. The actual number of burrows present is also determined by the maintenance of the warren. The warren will achieve its maximum number of burrows if continuously occupied for 2 years. If the warren is abandoned then it will slowly fall into disrepair. The number of burrows possible as a maximum ranges from 3 to 15, fewer than three and the warren is considered unsuitable.
Warrens are linked by local networks. This is a method for allowing the rabbits to have some knowledge of their surroundings. Local warrens will be linked and the probability of visiting them during exploration is dependent on their distance from the home warren.
Warrens also manage breeding in that a rabbit is currently only allowed to breed if it has both a mate and a burrow. This provides a strong density dependence and prevents juveniles from
breeding unless death releases parental burrows.<br>

- GENERAL FUNCTIONS
	- Function: Rabbit_Warren::Rabbit_Warren The Rabbit warren constructor
	- Function: Rabbit_Warren::~Rabbit_Warren The rabbit warren destructor.
	- Function: Rabbit_Warren::Step Warren step code.
	- Function: Rabbit_Warren::InitEvaluation This method is called when the warren in initially formed and is responsible for making the calcuations of forage and cover areas and then calculating the carrying capacity of the warren if fully occupied.
	- Behaviour: Rabbit_Warren::st_WarrenBeing This is the only warren behaviour - the warren just is, and this is where any daily updates to its status are performed. The number of current burrows are calculated, maintenence state is determined and daily food availability is calculated.
	- LOCATION FUNCTIONS
	- Function: Rabbit_Warren::Get_TL_x Get m_TL_x which is the x-coordinate for the Top-Left corner of the warren area.
	- Function: Rabbit_Warren::Get_TL_y Get m_TL_y which is the y-coordinate for the Top-Left corner of the warren area.
	- Function: Rabbit_Warren::GetPointTL Get the TL coords as a point.
	- Function: Rabbit_Warren::GetPopulationSize Gets the total warren population of rabbits.
	- Function: Rabbit_Warren::GetCarryingCapacity Returns the carrying capacity in burrows.
	- Function: Rabbit_Warren::GetCarryingCapacityFilled Checks whether all possible burrows are filled with rabbits.
- NETWORK FUNCTIONS
	- Function: Rabbit_Warren::NetworkEvaluation Calculates and saves distance probabilities.
	- Function: Rabbit_Warren::AddNetworkConnection Adds a warren to the local network list.
	- Function: Rabbit_Warren::GetNetworkWarren Chooses a warren to evaluate based on distance.
	- WARREN RABBITS FUNCTIONS
	- Function: Rabbit_Warren::IsMember Returns true if this rabbit belongs to the warren.
	- Function: Rabbit_Warren::IsFreeFemale Returns true if there is a female with a burrow and no mate.
	- Function: Rabbit_Warren::IsFreeMale Returns true if there is a male with a burrow and no mate.
	- Function: Rabbit_Warren::IsFreeBurrow Is there a vacent burrow?
	- Function: Rabbit_Warren::Leave Remove this adult from the warren list.
	- Function: Rabbit_Warren::Join Adds this rabbit to the warren list.
	- Function: Rabbit_Warren::OccupyWarren Adds the first rabbit to the warren list.
	- Function: Rabbit_Warren::OccupyBurrow A_rabbit occupies a free burrow.
	- Function: Rabbit_Warren::JoinNOccupy Adds this adult to the warren list and house them in a suitable burrow.
	- Function: Rabbit_Warren::JoinNMate Adds this rabbit to the warren list and mates it with an un-mated female/male with a burrow.
	- Function: Rabbit_Warren::Mate Mate a rabbit with un-mated female/male with a burrow.
- RESOURCE AND MORTALITY FUNCTIONS
	- Function: Rabbit_Warren::GetAvailableForage Returns the total area of permanent forage plus suitable area of temporary forage.
	- Function: Rabbit_Warren::CalcForageArea Returns the total area of permanent forage.
	- Function: Rabbit_Warren::GetForageArea Returns the total area of forage.
	- Function: Rabbit_Warren::GetTemporaryForageArea Returns the total area of temporary forage.
	- Function: Rabbit_Warren::GetCoverArea Returns the total area of cover.
	- Function: Rabbit_Warren::GetForageAreaVeg Returns the total area of forage based on veg height.
	- Function: Rabbit_Warren::GetCoverAreaVeg Returns the total area of cover based on veg height.
	- Function: Rabbit_Warren::CalcDisease Calculates a warren-specific disease constant (translates to a probability). This is based on global and local density.
	- Function: Rabbit_Warren::UpdatePesticide. Updates the pesticide concentration in forage polygons.	This is a very 'costly' method. It should not be called if there is no pesticide in use. First it zeros all the pesticide loads in the polygon lists. Then is trawls through every warren location and determines if there is a pesticide value at that location.If so the polygon in the list is identified and the total pesticide for that polygon has the new pesticide value added. Finally, the pesticide values in all polygons are divided by the area in the warren and the concentration determined. The net result is that the mean concentration of pesticide per unit habitat is updated daily and available to foraging rabbits.

Rabbit_Population_Manager
The rabbit population manager is responsible for handling all lists of rabbits and warrens and scheduling their behaviour. It provides the facilities for output and handles central 
rabbit functions needed to interact with the landscape. One key function is the pre-processing of the landscape to determine the potential locations for rabbit warrens and the formation
of local warren networks allowing rabbits to efficiently locate nearby warrens. 
This is a very time-consuming step so will typically be done for each landscape and the results stored and used as input later.<br>

- Function: Rabbit_Population_Manager::Rabbit_Population_Manager Rabbit_Population_Manager Constructor.
- Function: Rabbit_Population_Manager::~Rabbit_Population_Manager The destructor for the Rabbit_Population_Manager class.
- Function: Rabbit_Population_Manager::AssessPctForage Assesses the percentage of forage for a location assuming max warren size. Loops through the potential warren area and finds all the forage and sums this. Then calculate the percentage by area.
- Function: Rabbit_Population_Manager::CheckForRabbitBreedingConditions Determines whether it is breeding season.
- Function: Rabbit_Population_Manager::ClassifyHabitat This is the function that determines the classification of a landscape element type into rabbit habitat types. 
These habitat types are classified according to the enumerator #TTypesOfRabbitHabitat. This is basically a translation function from the element type from the landscape map to the type of 
habitat it represents for the rabbit. This makes no assessment of the vegetation on the element though.
- Function: Rabbit_Population_Manager::CreateLocalWarrenNetworkLists Forms the local warren network list for this warren. Loops through all warrens in a double loop and checks if any warrens 
are within maxWarrenNetworkDist of each other. If so then they are added to the warren local network for that warren. If no warrens are close, then the nearest one is added, so that the list 
is not empty. This is not speed optimised because it is only called once per simulation.
- Function: Rabbit_Population_Manager::CreateObjects This creates a new individual rabbit or warren location. Creates a_number of new rabbit objects of type ob_type using the information
about the rabbit passed in a_data. All rabbit objects created are added to the appropriate rabbit lists held in TheArray.Note that rabbit warrens are also created here to take advantage 
of the code in TAnimal and population manager handling - I know they are not actually rabbits themselves :)
- Function: Rabbit_Population_Manager::DoAfter Things to do before the EndStep.
- Function: Rabbit_Population_Manager::DoBefore Things to do before the Step See PopulationManager::Step.
- Function: Rabbit_Population_Manager::DoFirst Things to do before anything else at the start of a timestep.
- Function: Rabbit_Population_Manager::DoLast Things to do after the EndStep.
- Function: Rabbit_Population_Manager::FindClosestWarren Finds the closest warren as the crow flies. Loops through all warrens and calculates the distance to a_x, a_y. Returns the one ranked 'a_rank' closest as the crow flies. Can handle up to 10 ranks - it will cause an error to ask for no 11. This is primarily thought to be used for the occasions where the closest warren is one the rabbit knows already, so don't ask for that one.
- Function: Rabbit_Population_Manager::IsBreedingSeason  Returns whether it is breeding season or not. Once the temperature comes up over a certain trigger the breeding seasons starts. Rapid switching is prevented by not making the test again for 120 days. When the temperature in the future 90 days drops down below the trigger again then the breeding season stops.
- Function: Rabbit_Population_Manager::LoadWarrenLocations Load warren locations. If warren locations are not to be calculated then they are loaded from a standard file called "RabbitWarrenLocations.txt". If the wrong file is used there is no error checking and the result is usually a mysterious crash or strange results. Great care must be taken to ensure this does not happen.The use of this functionality is controlled by the input parameter RABBIT_WARRENREWADLOCATIONS see #cfg_warrenreadlocations 
- Function: Rabbit_Population_Manager::PreProcessWarrenLocations This pre-scans the landscape and determines all potential warren locations on start-up. It is a very time consuming 
process so this would normally be run once for a landscape and then the results saved using Rabbit_Population_Manager::SaveWarrenLocations and loaded again for later runs using 
Rabbit_Population_Manager::LoadWarrenLocations. This method scans the landscape from random,random to width,height, then 0,0 to start location. Moves left to right, then top to bottom.
At every location a rabbit warren is possible one is created with no population. Warrens cannot overlap and must contain a minimum proportion of fixed forage. This method uses the soil 
type to determine the amount of permanent forage habitat necessary to establish. In sandy soils this is lower than chalky soils.
- Function: Rabbit_Population_Manager::SaveWarrenLocations Save warren locations.    
- Function: Rabbit_Population_Manager::WarrenLegalPos Tests the warrens list to see if this position is legal as part of the pre-processing of the landscape by Rabbit_Population_Manager::PreProcessWarrenLocations.



# Design
<br>
##  4. Design Concepts
<br>
###  4.a Emergence
The breeding season of the rabbits is an emergent property of the weather inputs and the criteria assumed for breeding. This is done by Rabbit_Population_Manager::CheckForRabbitBreedingConditions 
using monthly mean temperature data as input.<br>
The number and spatial occupancy of warrens (spatial distribution) is emergent from rabbit behaviour interacting with warren networks and the resource distribution in terms of cover and forage.<br>
The rabbit population size is emergent as its age-structure.<br>
The exposure of rabbits to pesticides is emergent dependent upon the pattern of spraying (crops and localities) and the foraging pattern of the rabbits. This is primarily 
determined by the warren location and the likelihood of a rabbit foraging from a treated area.
###4.b Adaptation
Not used.
###4.c Fitness
Fitness is measured for the rabbit in terms of whether it has a mate and a burrow, if so it can breed and has a lower mortality rate.
###4.d Prediction
The rabbit population manager uses prediction to determine when the rabbit breeding season stops. This is done by looking ahead 90 days into the future weather.
This is not prediction in the normal sense of the ODdox, but is an unusual case.
###4.e Sensing
Rabbits can sense vacant burrows, other rabbits as potential mates, and can interact with the warren network. The warren network allows them to sense conditions in nearby warrens by
travelling there and "looking". They also sense when the breeding season starts and stops.
###4.f Interaction
Interactions occur between adult rabbits in finding mates, and between rabbits and the warrens in determining the potential for occupation of warrens and therefore breeding. Weather and 
environmental conditions influence rabbits both directly (e.g. breeding season timing with temperature, or pesticide induced physiological changes), and indirectly via determining
the suitability of warrens and their carrying capacity.
###4.g Stochasticity
Stochasticity is used extensively. All rabbits are subject to stochastic mortality, the severity of which is modified by their status and behaviours (e.g. if dispersing). Exploration
of local warrens is also stochastic, but modified by distance such that the chance of visiting a nearby warren is distance dependent. Litter size also has a stochastic component.
###4.h Collectives
Collectives in the strict sense are not used, although the warren does form a useful administrative unit which can collectively manage some behaviour. Otherwise all rabbits are simulated individually from birth to death.
###4.i Observation
Currently the rabbit model uses standard ALMaSS output. This provides a time-indexed population census broken down into young, juvenile, male and females.
## 5. Initialisation
Uses standard ALMaSS input files, but can be set up to use predefined warren maps. This saves time because calculation of the warren networks takes a long time for big maps. 
On start-up male and female rabbits are randomly located in the landscape and must initially establish warrens, requiring a burn in time before the simulation can be used
for scenario modifications, measurements etc.<br>
All other inputs needed for initialisation are described below.
<br>
##6. Inputs
###Input variables
The upper-case names are the names used in the configuration file. See example below.

 - RABBIT_ADULTBASEMORT (float) #cfg_adult_base_mort This is the basic daily mortality probability for adult rabbits before any other modifiers are applied. Default value 0.00013
 - RABBIT_DAILYGROWTHPARAM_ONE (float) #cfg_rabbitdailygrowthparam1 Parameter one of the rabbit growth curve. Default value = 1127.616084
 - RABBIT_DAILYGROWTHPARAM_THREE (float) #cfg_rabbitdailygrowthparam3 Parameter three of the rabbit growth curve. Default value = 0.0
 - RABBIT_DAILYGROWTHPARAM_TWO (float) #cfg_rabbitdailygrowthparam2 Parameter two of the rabbit growth curve. Default value =  -0.013143202
 - RABBIT_DENDEPPERIOD (int) #cfg_rabbitdensitydependencedelay The period between recalculation of rabbit disease probability. Default value = 90
 - RABBIT_DENSITYDEPSCALER (float) #cfg_rabbitdendepscaler Average family size for calculating carrying capacity. Default value = 4.5
 - RABBIT_DIGGINGTIME (int) #cfg_rabbitdiggingtime Minimum burrow construction time in days. Default value: 10
 - RABBIT_DISEASEDENDEPPERIOD (int) #cfg_rabbitdiseasedensitydependencedelay The period between recalculation of rabbit disease probability. Default value = 53
 - RABBIT_DISPERSALMORTPERM (float) #cfg_dispersalmortperm The additional mortality rate with distance for between warren dispersal. Default value 0.0002 allows a max of 20k disp for a very few!
 - RABBIT_FIX_SOIL_TYPE (int) #cfg_rabbit_fix_soiltype The soil type applied to all warrens if a fixed soil type is used. 0 = chalk, 1 = sandy (only two possible at present). Default value = 0
 - RABBIT_FORAGEAREAMORTSCALER (float) #cfg_forageareamortscaler A constant used to increase bad weather mortality as a function of the proportion of forage available. Default value = 6.0
 - RABBIT_GLOBALDISEASEPROBCONSTANT (float) #cfg_globaldisease_probability A constant use to calculate the probability of density related disease.  Default value: 0.05
 - RABBIT_JUVENILEBASEMORT (float) #cfg_juvenile_base_mort This is the basic daily mortality probability for juvenile rabbits before any other modifiers are applied. Default value 0.004
 - RABBIT_LITTERABSOPRTIONCONST (float) #cfg_litterabsorptionconstant Used to calculate the chance of litter re-absorption depending on forage conditions. Default value = 1.8
 - RABBIT_MAXFORAGEHEIGHT (float) #cfg_maxForageHeight The maximum height desirable for forage vegetation. Default value 30.0
 - RABBIT_MAXFORAGERAINFALL (float) #cfg_rabbitmaxforagerainfall The maximum rainfall before rabbits are assumed not to forage. Default value = 3.6
 - RABBIT_MAXKITS  (float) #cfg_rabbitmaxkits The maximum number of kits in a litter. Default value = 9.6
 - RABBIT_MAXWARRENNETWORKDIST (float) #cfg_maxWarrenNetworkDist The maximum distance between directly connected warrens. Default value = 1500
 - RABBIT_MAXWARRENSIZE  (float) #cfg_rabbitmaxwarrensize Used to calculate the maximum size of a warren.  Add min to get the real maximum warren size. Default value = 10 (5 for light soils)
 - RABBIT_MINBREEDINGTEMP (float) #cfg_rabbitminbreedingtemp The minimum 60 day summed temperature for rabbit breeding. If above this then the population manager breeding flag is set and females will mate if possible. Default value = 300.0
 - RABBIT_MINFORAGEDIGESTABILITY (float) #cfg_minForageDigestibility The minimum digestibility desirable for forage vegetation. Default value: 0.50 range 0.5 to 0.8
 - RABBIT_MINFORAGETEMP (float) #cfg_rabbitminimumforagetemp The minimum forage temperature for rabbits. Default value = -0.83
 - RABBIT_MINKITS (float) #cfg_rabbitminkits The minimum number of kits in a litter. Default value = 1.0
 - RABBIT_MINWARRENSIZE (float) #cfg_rabbitminwarrensize The minimum size of a warren in burrows. Default value = 2 (1 for light soils)
 - RABBIT_PESTICDERESPONSETHRESHOLD (float) #cfg_RabbitPesticideResponse The threshold above which a rabbit will be killed if it ingests pesticide. Default value = 0.00001
 - RABBIT_PESTICIDEDEGRADATIONRATE (float) #cfg_rabbit_pesticidedegradationrate Holds the proportion of decay of body burden of pesticide per day. Default of 0.0 will remove all body burden pesticide at the end of each day. Default value = 0.0
 - RABBIT_SOCIALREPROTHRESHOLD  (float) #cfg_rabbitsocialreproductionthreshold The threshold at which social reproduction reduction is tested. 1.0 means 50% chance of reproduction for subordinates.  Default value = 2.3
 - RABBIT_STARTNOS (int) #cfg_RabbitStartNos The starting number of male and female rabbits. Default value is 25000
 - RABBIT_USE FIXED_SOIL_TYPE (bool) #cfg_rabbit_use_fixed_soiltype Assume fixed soil type or use landscape info? Default = true, use fixed soil type
 - RABBIT_USENATALDISPERSALRECORD (bool) #cfg_RabbitUseNatalDispersalRecord Flag to denote using lifetime repro output file or not. Default value = false
 - RABBIT_USENATALDISPERSALRECORDAGE (int) #cfg_RabbitUseNatalDispersalRecordAge Flag to denote using lifetime repro output file or not. Default value = 30*6
 - RABBIT_USEREPROOUTPUT (bool) #cfg_RabbitUseReproOutput Flag to denote using lifetime repro output file or not.Default value = false
 - RABBIT_WARRENFIXEDSIZE (int) #cfg_warrenfixedsize The maximum size for a warren (length m). Default = 45
 - RABBIT_WARRENLOCATIONSFILE (string) #cfg_warrenlocationsfile The warren locations file name. Default value: "RabbitWarrenLocations.txt"
 - RABBIT_WARRENOCCUPANCYRECORDDAY (int) #cfg_warrenoccupancyrecordday Output control  - the day in year for recording warren and population data. Default value:270
 - RABBIT_WARRENREADLOCATIONS (bool) #cfg_warrenreadlocations Should warren locations be calculated or read from a file? If so uses Rabbit_Population_Manager::LoadWarrenLocations to load the locations of all potential warrens at start up. Default value: Read (true)
 - RABBIT_YOUNGBASEMORT (float) #cfg_young_base_mort This is the basic daily mortality probability for young rabbits before any other modifiers are applied, e.g. extra mortality due to non-dominant mother. Default value 0.038

In addition to these simple parameter inputs the file RabbitWarrenLocations.txt can be used to prevent calculation of warren locations at run time. See RABBIT_WARRENREWADLOCATIONS above.<br>
 

## 7. Interconnections
The rabbit model relies on environmental data primarily from the Landscape and Weather classes. They are also dependent on interactions with the Farm and Crop classes
for information on management. Pesticide use and uptake depends on the Pesticide class.
The Rabbit_Population_Manager is descended from Population_Manager and performs the role of an auditor in the simulation. Many of the functions and behaviours needed to execute and ALMaSS model are maintained by the Population_Manager.<br>
<br>
##8. References
<br>
