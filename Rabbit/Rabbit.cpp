


/*
*******************************************************************************************************

Copyright (c) 2015, Christopher John Topping, Faarupvej 54, DK-8410 R�nde
ADAMA Makhteshim Ltd., PO Box 60, Beer-Sheva 84100, Israel

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.

********************************************************************************************************

*/
/** \file Rabbit.cpp 
\brief <B>The main source code for all rabbit lifestages and associated classes</B>
*/
/**  \file Rabbit.cpp
Version of  November 2015 \n
By Chris J. Topping \n \n
*/

//#define __RABBITDEBUG
//#define __EnclosureTest   // Used only for testing POM on rabbits in an enclosure (Bayreuth study)

#include <iostream>
#include <fstream>
#include <vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Rabbit/Rabbit.h"
#include "../Rabbit/Rabbit_Population_Manager.h"
#include <random>
using namespace std;

// Externs

extern MapErrorMsg *g_msg;
extern int g_rabbitexploredistance;
extern int g_land_width;
extern std::default_random_engine g_std_rand_engine;
extern CfgBool cfg_RabbitUseReproOutput;
extern CfgBool cfg_RabbitUseNatalDispersalRecord;
extern CfgInt cfg_RabbitUseNatalDispersalRecordAge;
extern CfgBool l_pest_enable_pesticide_engine;
extern CfgInt cfg_rabbitdensitydependencedelay;

int g_counter = 0;

/** \brief Input variable. The daily base level for young mortality before any modifiers are applied. */
static CfgFloat cfg_young_base_mort("RABBIT_YOUNGBASEMORT",CFG_CUSTOM,0.038);
/** \brief Input variable. The daily base level for juvenile mortality before any modifiers are applied. */
static CfgFloat cfg_juvenile_base_mort("RABBIT_JUVENILEBASEMORT",CFG_CUSTOM,0.004);
/** \brief Input variable. The daily base level for adult mortality before any modifiers are applied. */
static CfgFloat cfg_adult_base_mort("RABBIT_ADULTBASEMORT",CFG_CUSTOM,0.00013);
/** \brief Input variable. The maximum height desirable for forage vegetation */
CfgFloat cfg_maxForageHeight("RABBIT_MAXFORAGEHEIGHT", CFG_CUSTOM, 30.0);
/** \brief Input variable. Input variable. The minimum digestability desirable for forage vegetation */
CfgFloat cfg_minForageDigestibility("RABBIT_MINFORAGEDIGESTABILITY", CFG_CUSTOM, 0.50); // range 0.5 to 0.8
/** \brief Input variable. The additional mortality rate with distance for between warren dispersal */
CfgFloat cfg_dispersalmortperm("RABBIT_DISPERSALMORTPERM", CFG_CUSTOM, 0.0002); 
/** \brief Input variable. The threshold above which a rabbit will be killed if it ingests pesticide */
CfgFloat cfg_RabbitPesticideResponse("RABBIT_PESTICDERESPONSETHRESHOLD",CFG_CUSTOM, 0.00001);
/** \brief Input variable. A constant use to calculate the probability of density related disease */
CfgFloat cfg_globaldisease_probability("RABBIT_GLOBALDISEASEPROBCONSTANT", CFG_CUSTOM, 0.05); 
/** \brief Input variable. Minimum burrow construction time in days */
CfgInt cfg_rabbitdiggingtime("RABBIT_DIGGINGTIME", CFG_CUSTOM, 10);
/** \brief Input variable. Average family size for calculating carrying capacity */
CfgFloat cfg_rabbitdendepscaler("RABBIT_DENSITYDEPSCALER", CFG_CUSTOM, 4.5); // 2 adults and 5.5 kits
/** \brief Input variable. A constant used to increase bad weather mortality as a function of the proportion of forage available */
static CfgFloat cfg_forageareamortscaler("RABBIT_FORAGEAREAMORTSCALER", CFG_CUSTOM, 60.0);
/** \brief The minimum number of kits in a litter */
CfgFloat cfg_rabbitminkits("RABBIT_MINKITS", CFG_CUSTOM, 1.0);
/** \brief The maximum number of kits in a litter */
CfgFloat cfg_rabbitmaxkits("RABBIT_MAXKITS", CFG_CUSTOM, 9.6);
/** \brief The threshold at which social reproduction reduction is tested. 1.0 means 50% chance of reproduction for subordinates. */
static CfgFloat cfg_rabbitsocialreproductionthreshold( "RABBIT_SOCIALREPROTHRESHOLD", CFG_CUSTOM, 2.3 );
/** \brief The minimum size of a warren */
static CfgFloat cfg_rabbitminwarrensize( "RABBIT_MINWARRENSIZE", CFG_CUSTOM, 2 );
/** \brief Use to calculate the maximum size of a warren.  Add min to get the real maximum warren size. */
static CfgFloat cfg_rabbitmaxwarrensize( "RABBIT_MAXWARRENSIZE", CFG_CUSTOM, 10 );
/** \brief Used to calculate the chance of litter reabsorption depending on forage conditions */
static CfgFloat cfg_litterabsorptionconstant( "RABBIT_LITTERABSOPRTIONCONST", CFG_CUSTOM, 1.8 );
/** \brief Holds 1-proportion of decay of body burden of pesticide per day. Default of 0.0 will remove all body burden pesticide at the end of each day */
CfgFloat cfg_rabbit_pesticidedegradationrate( "RABBIT_PESTICIDEDEGRADATIONRATE", CFG_CUSTOM, 0.0 );
// Assign the static variables - NB this will just assign the defaults
double Rabbit_Base::m_pesticidedegradationrate = cfg_rabbit_pesticidedegradationrate.value(); // default of 0.0 will remove all body burden pesticide at the end of each day
double Rabbit_Warren::m_maxForageHeight = cfg_maxForageHeight.value();
double Rabbit_Warren::m_minForageDigestibility = cfg_minForageDigestibility.value();
double Rabbit_Base::m_dispersalmortperm = cfg_dispersalmortperm.value();
double Rabbit_Female::m_MinKitsNo = cfg_rabbitminkits.value();
double Rabbit_Female::m_MaxKitsNo = cfg_rabbitmaxkits.value() - cfg_rabbitminkits.value(); // Here we actually use the difference to calculate range
// End static assigns

//********************************************************************************************************************
//*********************************************RabbitMemory***********************************************************
//********************************************************************************************************************

RabbitMemory::RabbitMemory()
{
	m_MyMemory.resize(0);
}

void RabbitMemory::Update()
{
	for(vector<RabbitMemoryLocation>::iterator it = m_MyMemory.begin(); it != m_MyMemory.end(); ++it) 
	{
		if ((*it).m_decay-- < 1) m_MyMemory.erase(it);
	}
}

void RabbitMemory::AddMemory( RabbitMemoryLocation a_mem)
{
	a_mem.m_decay=100; // Assuming 100 days memory
	m_MyMemory.push_back(a_mem);
}

RabbitMemoryLocation RabbitMemory::GetBestLocation( void )
{
	vector<RabbitMemoryLocation>::iterator best;
	int score = -1;
	for(vector<RabbitMemoryLocation>::iterator it = m_MyMemory.begin(); it != m_MyMemory.end(); ++it) 
	{
		if ((*it).m_quality > score) best=it;
	}
	return (*best);
}

//********************************************************************************************************************
//                                     Rabbit_Base
//********************************************************************************************************************

Rabbit_Base::Rabbit_Base(int p_x, int p_y, int p_x2, int p_y2, Landscape* p_L, Rabbit_Population_Manager* p_NPM, Rabbit_Warren* a_warren) : TAnimal(p_x, p_y, p_L)
{
	// Assign the pointer to the population manager
	m_OurPopulationManager = p_NPM;
	m_CurrentRState = toRabbits_InitialState;
	m_haveBurrow = false;
	m_myWarren = a_warren;
	m_born_location.m_x = p_x2;
	m_born_location.m_y = p_y2;
	if (m_myWarren != NULL) m_myWarren->Join(this);
	m_pesticideInfluenced1 = false;
	m_pesticide_burden = 0.0;
}
//---------------------------------------------------------------------------

Rabbit_Base::~Rabbit_Base(void)
{
	if (m_myWarren != NULL ) m_myWarren->Leave(this);
}
//---------------------------------------------------------------------------

void Rabbit_Base::BeginStep( void ) {
	if (m_Age == cfg_RabbitUseNatalDispersalRecordAge.value()) if (cfg_RabbitUseNatalDispersalRecord.value()) m_OurPopulationManager->NatalDispersalRecordOutput( this );
}
//---------------------------------------------------------------------------

void Rabbit_Base::st_Dying( void )
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
}
//---------------------------------------------------------------------------

inline  bool Rabbit_Base::MortalityTest(double a_prop) 
{ 
	/** 
	* \param [in] a_prop must be between 0 and <1.0 
	* This is a simple probability test. 
	*/
	if (g_rand_uni() < a_prop) return true; 
		else return false; 
}
//---------------------------------------------------------------------------

bool Rabbit_Base::WalkTo(int a_x, int a_y) 
{ 
	/** 
	* \param [in] a_x x-coordinate to move to
	* \param [in] a_y y-coordinate to move to
	* \return True if the rabbit still lives, otherwise false if it was killed in dispersal.
	* 
	* Assumes a rectangluar distance movement with associated probability of mortality by distance.
	*/
	int dist = abs(a_x-m_Location_x) + abs(a_y-m_Location_y);
	double prob = dist * m_dispersalmortperm;
	m_Location_x = a_x;
	m_Location_y = a_y;
	if (g_rand_uni() > prob) return true; 
		else 
		{
			st_Dying();
			g_counter++;
			return false; 
		}
}
//---------------------------------------------------------------------------

//********************************************************************************************************************
//                                     Rabbit_Young
//********************************************************************************************************************

Rabbit_Young::Rabbit_Young(int p_x, int p_y, int p_x2, int p_y2, Rabbit_Female* a_mum, Landscape* p_L, Rabbit_Population_Manager* p_NPM, Rabbit_Warren* a_warren) : Rabbit_Base(p_x, p_y, p_x2, p_y2, p_L, p_NPM, a_warren)
{
	m_Age=0;
	m_weightAge = 0;
	m_weight = 0;
	m_MyMortChance = cfg_young_base_mort.value();
	m_RabbitType = rob_Young;
	m_Mum = a_mum;
	m_FedToday = true;
}
//---------------------------------------------------------------------------

Rabbit_Young::~Rabbit_Young(void)
{
	; // Nothing to do
}
//---------------------------------------------------------------------------

void Rabbit_Young::st_Dying( void )
{
	/*
	* Only differs from base states by the need to tell the mother that they are dead (if she exists).
	*/
	if (m_Mum!=NULL) m_Mum->OnYoungDeath(this);
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
}
//---------------------------------------------------------------------------


TTypeOfRabbitState Rabbit_Young::st_Develop( void )
{
	/**
	* \return new rabbit state toRabbits_Develop, toRabbits_Mature or toRabbits_Die
	*
	* Ages the rabbit, then carries out a mortality test, evaluates whether the rabbit should wean, and if neither then returns toRabbits_Explore.
	* The rabbit must also grow. Here we decide whether it grows based on the weather conditions. If inclement we assume no growth, if OK 
	* then maximal growth is assumed.
	*/
	m_Age++;
	if (MortalityTest(m_MyMortChance))
	{
		return toRabbits_Die; // m_MyMortChance% chance of death
	}
	if (ShouldMature()) return toRabbits_Weaning;
	if (m_OurPopulationManager->GetForageDay()) m_weightAge++;
	m_weight = m_OurPopulationManager->GetGrowth( m_weightAge );
	return toRabbits_Develop;
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Young::st_BecomeJuvenile()
{
	/**
	* Creates a new Rabbit_Juvenile object and passes the data from the young to it, then signals young object removal.
	*/
	struct_Rabbit sR;
	sR.m_NPM = m_OurPopulationManager;
	sR.m_L = m_OurLandscape;
	sR.m_age = m_Age;
	sR.m_weightage = m_weightAge;
	sR.m_x = m_Location_x;
	sR.m_y = m_Location_y;
	sR.m_x2 = m_born_location.m_x;
	sR.m_y2 = m_born_location.m_y;
	sR.m_Warren = m_myWarren;
	sR.m_rabbit = this;
	if (m_Mum != NULL ) m_Mum->Weaned(this);
	m_OurPopulationManager->CreateObjects(rob_Juvenile,this->m_Mum,&sR,1); // 
	return toRabbits_Remove; // Used to remove the object without causing other system effects
}
//---------------------------------------------------------------------------

void Rabbit_Young::BeingStep(void)
{
	/**
	* Resets the feeding flag at the beginning of the day.
	* Call the base class BeginStep
	*/
	Rabbit_Base::BeginStep();
	m_FedToday = false;
}
//---------------------------------------------------------------------------

void Rabbit_Young::Step(void)
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentRState)
	{
	case toRabbits_InitialState: // Initial state always starts with develop
		m_CurrentRState=toRabbits_Develop;
		break;
	case toRabbits_Develop:
		m_CurrentRState=st_Develop(); // Will return movement or die
		m_StepDone=true;
		break;
	case toRabbits_Weaning:
		m_CurrentRState=st_BecomeJuvenile(); // Will return develop
		m_StepDone=true;
		break;
	case toRabbits_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone=true;
		break;
	case toRabbits_Remove:
		m_CurrentStateNo = -1;
		m_StepDone=true;
		break;
	default:
		m_OurLandscape->Warn("Rabbit_Young::Step()","unknown state - default");
		exit(1);
	}
}
//---------------------------------------------------------------------------

void Rabbit_Young::EndStep(void)
{
	/*
	* Controls death by starvation if under solid food age, currently set at 3 weeks. If this happens then a message is sent to mum if she still exists.
	
	*/
	if ((m_FedToday == false) && (m_Age <= 21))
	{
		m_CurrentRState = toRabbits_Die;
		if (m_Mum != NULL) m_Mum->OnYoungDeath( this );
	}

}
//---------------------------------------------------------------------------

bool Rabbit_Young::ShouldMature( void )
{
	/** 
	* Weaning is really controlled by the female who will evict an old litter when a new one comes along. But if the breeding season is over this will not occur
	* and we need to wean the young a bit later. 
	*/
	if (m_Age>=30) return true;
	return false;
}

//********************************************************************************************************************
//                                     Rabbit_Juvenile
//********************************************************************************************************************

Rabbit_Juvenile::Rabbit_Juvenile(int p_x, int p_y, int p_x2, int p_y2, Rabbit_Female* p_M, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_age, int a_weightage, Rabbit_Warren* a_warren) : Rabbit_Young(p_x, p_y, p_x2, p_y2, p_M, p_L, p_NPM, a_warren)
{
	m_Age = a_age;
	m_weightAge = a_weightage;
	m_weight = m_OurPopulationManager->GetGrowth( a_weightage );
	m_RabbitType = rob_Juvenile;
	if (a_warren != NULL) a_warren->RabbitProductionRecord(rob_Juvenile, 1);
	m_MyMortChance = cfg_juvenile_base_mort.value();
}
//---------------------------------------------------------------------------

Rabbit_Juvenile::~Rabbit_Juvenile(void)
{
	;
}
//---------------------------------------------------------------------------

void Rabbit_Juvenile::st_Dying( void )
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Juvenile::st_Develop( void )
{
	/**
	* \return new rabbit state toRabbits_Explore, toRabbits_Mature or toRabbits_Die
	*
	* Ages the rabbit and evaluates whether the rabbit should mature, and if neither then returns toRabbits_Explore
	*/
	m_Age++;
	if (ShouldMature()) return toRabbits_Mature;
	if (m_OurPopulationManager->GetForageDay()) m_weightAge++;
	m_weight = m_OurPopulationManager->GetGrowth( m_weightAge );
	return toRabbits_Explore;
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Juvenile::st_Explore( void )
{
	/**
	* As a juvenile there is no need to explore the surroungings, the big bad world awaits when the rabbit becomes adult.
	*/
	return toRabbits_Develop;
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Juvenile::st_BecomeAdult()
{
	/**
	* \return TTypeOfRabbitState toRabbits_Remove
	*
	* Creates a new Rabbit_Male or Rabbit_Female object and passes the data from the juvenile object to it, then signals young object removal.
	*/
	struct_Rabbit sR;
	sR.m_NPM = m_OurPopulationManager;
	sR.m_L = m_OurLandscape;
	sR.m_age = m_Age;
	sR.m_weightage = m_weightAge;
	sR.m_x = m_Location_x;
	sR.m_y = m_Location_y;
	sR.m_x2 = m_born_location.m_x;
	sR.m_y2 = m_born_location.m_y;
	sR.m_Warren = m_myWarren;
	/**
	* Sex determination occurs here and then returns toRabbits_Remove to remove the object without causing other system effects.
	*/ 
	if (g_rand_uni()<0.4854) m_OurPopulationManager->CreateObjects(rob_Male,NULL,&sR,1); // 100:106 M:F ratio (Brambell (1942) & Stephens (1952)) 
	else m_OurPopulationManager->CreateObjects(rob_Female,NULL,&sR,1); 
	return toRabbits_Remove; 
}
//---------------------------------------------------------------------------

void Rabbit_Juvenile::Step(void)
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
    switch (m_CurrentRState)
    {
     case toRabbits_InitialState: // Initial state always starts with develop
	     m_CurrentRState=toRabbits_Foraging;
	     break;
     case toRabbits_Develop:
	     m_CurrentRState=st_Develop(); // Will return movement or die
	     if (m_CurrentRState==toRabbits_Develop) m_StepDone=true;
		 break;
     case toRabbits_Mature:
	     m_CurrentRState=st_BecomeAdult(); 
	     break;
	 case toRabbits_Foraging:
		 m_CurrentRState = st_Forage();
		 break;
	 case toRabbits_Die:
		 st_Dying(); // No return value - no behaviour after this
		 m_StepDone=true;
		 break;
	 case toRabbits_Explore:
		 m_CurrentRState=st_Explore();
		 m_StepDone=true;
		 break;
	 case toRabbits_Remove:
		 m_CurrentStateNo = -1;
		 m_StepDone=true;
		 break;
	 default:
		 m_OurLandscape->Warn("Rabbit_Juvenile::Step()","unknown state - default");
		 exit(1);
	}
}
//---------------------------------------------------------------------------

void Rabbit_Juvenile::EndStep(void)
{
	/**
	* Here we apply a density dependent mortality as well as a daily mortality rate. 
	* The mortality is dependent on two factors - whether the rabbit has a burrow (will not be the case for juvenile), and whether the carrying capacity 
	* of the warren is reached in terms of number of possible burrows. 
	* Note that this EndStep over-rides the Young::EndStep, which is critical because otherwise the rabbit will die due to not having set the m_FedToday flag.
	*/
	if (m_CurrentStateNo == -1) return; // Already dead
	double mult = 1.0;
	if (m_myWarren != NULL)
	{
		mult = m_myWarren->GetDailyMortalityChanceJ();
	}
	if (MortalityTest(m_MyMortChance * mult))
	{
		m_CurrentRState = toRabbits_Die;
		st_Dying(); // m_MyMortChance% chance of death
		return;
	}
	// Deal with pesticides if necessary
	if (m_pesticide_burden>0) InternalPesticideHandlingAndResponse();
}
//---------------------------------------------------------------------------

bool Rabbit_Juvenile::ShouldMature( void )
{
	if (m_weightAge>=105) return true; // Development linked to weight gain
	return false;
}
//---------------------------------------------------------------------------

//********************************************************************************************************************
//                                     Rabbit_Adult
//********************************************************************************************************************

Rabbit_Adult::Rabbit_Adult(int p_x, int p_y, int p_x2, int p_y2, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_age, int a_weightage, Rabbit_Warren* a_warren) : Rabbit_Base(p_x, p_y, p_x2, p_y2, p_L, p_NPM, a_warren)
{
	/** 
	* Main function is the initialise the attribute values. Lifespan is given as 2 years plus randomly up to two years = maximum four, average three.
	* Dominance is set to 0 (lowest level). It is evaluated in the st_EvaluateTerritory state
	*/
	m_Age = a_age;
	m_weightAge = a_weightage;
	m_weight = m_OurPopulationManager->GetGrowth( a_weightage );
	m_lifespan = int ( (730.0*g_rand_uni()) + 4.5*365); // This allows for the maximum age found by Rodel et al in the German study.
	//m_lifespan = 6.5 * 365; // This allows for the maximum age found by Rodel et al in the German study.
	SetSocialStatus( rabbit_socialstatus_zero ); // Default not dominant
	m_digging = -1;
	m_myMate = NULL;
	m_MyMortChance = cfg_adult_base_mort.value();
}
//---------------------------------------------------------------------------

Rabbit_Adult::~Rabbit_Adult(void)
{
	;
}
//---------------------------------------------------------------------------

void Rabbit_Adult::EndStep( void ) {
	/**
	* Here we apply a density dependent mortality as well as a daily mortality rate.
	* The mortality is dependent on three factors - whether the rabbit has a burrow, and whether the carrying capacity
	* of the warren is reached in terms of number of possible burrows, or whether the rabbit is digging. The carrying capacity is
	* results in extra movement which causes extra mortality (see evaluate territory) as well as increased mortality here.\n
	*
	* Disease is a density dependent mortality factor - used here to increase mortality at high densities. This is step function at the moment
	* but could be altered as necessary. The disease mortality is constant for all members of a warren - updated by the warren do first. See Rabbit_Warren::CalcDisease() \n
	*
	* If we survive all this, then we need to age, and die if we exceed our alloted lifespan. \n
	*
	* If that does not kill us then check for burrow digging completion and if we have done this occupy the burrow and if we have a mate then tell the mate
	* to set the appropriate burrow and digging information.
	*/
	if (m_CurrentStateNo == -1) return; // Already dead
	double mult = 1.0;
	if (m_myWarren != NULL) {
		mult = m_myWarren->GetDailyMortalityChanceA();
	}
	// Mortality depends on the social status
	int mult2 = (rabbit_socialstatus_dominant - m_socialstatus);
	mult *= mult2;
	if (MortalityTest( m_MyMortChance * mult )) {
		m_CurrentRState = toRabbits_Die;
		st_Dying(); // m_MyMortChance% chance of death
		return;
	}
	if (++m_Age > m_lifespan) {
		m_CurrentRState = toRabbits_Die;
		st_Dying();
		return;
	}
	if (m_digging > -1)
		// We are digging a burrow
	{
#ifdef __RABBITDEBUG
		if (m_myMate != NULL) {
			if (m_myMate->GetHasBurrow()) {
				int rubbish = 0;
			}
		}
#endif
		if (--m_digging < 0)
			// Finished digging
		{
			m_myWarren->OccupyNewBurrow();
			SetHasBurrow( true );
			SetSocialStatus( rabbit_socialstatus_subdominant );
			if (m_myMate != NULL) m_myMate->OnMateFinishedDigging( this );
		}
	}
	if (m_OurPopulationManager->GetForageDay()) m_weightAge++;
	m_weight = m_OurPopulationManager->GetGrowth( m_weightAge );
	// Deal with pesticides if necessary
	if (m_pesticide_burden>0) InternalPesticideHandlingAndResponse();
}
//---------------------------------------------------------------------------

void Rabbit_Adult::SetMate( Rabbit_Adult* a_mate )
{
#ifdef __RABBITDEBUG
	if (a_mate->GetRabbitType() == m_RabbitType)
	{
		 m_OurLandscape->Warn("Rabbit_Adult::SetMate","homosexual rabbit!");
		 exit(1);
	}
	if (m_myMate != NULL)
	{
		 m_OurLandscape->Warn("Rabbit_Adult::SetMate","rabbit bigamy!");
		 exit(1);
	}
#endif
	m_myMate = a_mate; 
}
//---------------------------------------------------------------------------

#ifdef __RABBITDEBUG
void Rabbit_Adult::OnMateFinishedDigging(Rabbit_Adult* a_mate)
{
	if (a_mate->GetRabbitType() == m_RabbitType)
	{
		 m_OurLandscape->Warn("Rabbit_Adult::OnMateFinishedDigging","homosexual rabbit!");
		 exit(1);
	}
	if ((m_myMate != a_mate) && (m_CurrentRState != toRabbits_Die))
	{
		 m_OurLandscape->Warn("Rabbit_Adult::OnMateFinishedDigging","rabbit bigamy, mate not recognised!");
		 exit(1);
	}
	if (m_digging < 0) {
		int rubbish = 0;
	}
	if (m_haveBurrow) {
		int rubbish = 0;
	}
#else
void Rabbit_Adult::OnMateFinishedDigging(Rabbit_Adult* /* a_mate */)
{
#endif
		SetDigging(-1); 
		SetHasBurrow(true);
		SetSocialStatus( rabbit_socialstatus_subdominant );
#ifdef __RABBITDEBUG
		m_myWarren->DEBUG_InternalTest();
#endif
}
//---------------------------------------------------------------------------

#ifdef __RABBITDEBUG
void Rabbit_Adult::OnMateDead(Rabbit_Adult* a_mate)
{
	if (a_mate->GetRabbitType() == m_RabbitType)
	{
		 m_OurLandscape->Warn("Rabbit_Adult::OnMateDead","homosexual rabbit!");
		 exit(1);
	}
	if ((m_myMate != a_mate) && (m_CurrentRState != toRabbits_Die))
	{
		 m_OurLandscape->Warn("Rabbit_Adult::OnMateDead","rabbit bigamy, mate not recognised!");
		 exit(1);
	}
#else 
void Rabbit_Adult::OnMateDead(Rabbit_Adult* /* a_mate */)
{
#endif
		m_myMate = NULL; 
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Juvenile::st_Forage( void )
{
	/**
	* This is a critical part of the pesticide handling code. The rabbit forages from the warren area and as a result picks up pesticide. Exactly how much pesticide is picked up
	* depends on the assumptions regarding foraging behaviour. These can be altered here, but the default assumption is that the rabbit will forage from all suitable forgage locations
	* equally, and therefore will pick up an average dose based on the average concentration of residue in the forage areas. This is precalculated by the Rabbit_Warren in UpdatePesticide.
	* The pesticide response code is placed in EndStep - this will determine direct mortality or set any necessary flags indicating pesticide effects later,
	* e.g. for reproduction.
	*/
	if (m_myWarren != NULL) m_pesticide_burden += m_myWarren->GetForagePesticide();
	return toRabbits_Explore;
}
//---------------------------------------------------------------------------

void Rabbit_Adult::st_Dying( void )
{
	/** 
	* Adult dying state. Needs to:
	* - Tell the warren he is leaving
	* - Tell any mate
	* - Set the current state to -1 (signals object destruction to the population manager).
	* - Set StepDone to true to prevent further Step behaviour.
	*/	
	if ( m_myWarren != NULL )
	{
		m_myWarren->Leave(this);
	}
	if (m_myMate != NULL) 
	{
		m_myMate->OnMateDead( this );
		m_myMate = NULL;
	}
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
}
//---------------------------------------------------------------------------


//********************************************************************************************************************
//                                     Rabbit_Male
//********************************************************************************************************************

Rabbit_Male::Rabbit_Male(int p_x, int p_y, int p_x2, int p_y2, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_age, int a_weightage, Rabbit_Warren* a_warren) : Rabbit_Adult(p_x, p_y, p_x2, p_y2, p_L, p_NPM, a_age, a_weightage, a_warren)
{
	m_RabbitType = rob_Male;
	if (a_warren != NULL) a_warren->RabbitProductionRecord(rob_Male, 1);
}
//---------------------------------------------------------------------------

Rabbit_Male::~Rabbit_Male(void)
{
	;
}
//---------------------------------------------------------------------------

void Rabbit_Male::Step(void)
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
    switch (m_CurrentRState)
    {
     case toRabbits_InitialState: // Initial state always starts with develop
	     m_CurrentRState=toRabbits_Foraging;
	     break;
     case toRabbits_EvaluateTerritory: // Initial state always starts with develop
	     m_CurrentRState=st_EvaluateTerritory();
		 m_StepDone=true;
	     break;
	 case toRabbits_Foraging:
		 m_CurrentRState=st_Forage();
		 break;
	 case toRabbits_Die:
		 st_Dying(); // No return value - no behaviour after this
		 m_StepDone=true;
		 break;
	 case toRabbits_Remove:
		 m_CurrentStateNo = -1;
		 m_StepDone=true;
		 break;
	 default:
		 m_OurLandscape->Warn("Rabbit_Male::Step()","unknown state - default");
		 exit(1);
	}
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Male::st_Forage(void)
{
	/**
	* This is a critical part of the pesticide handling code. The rabbit forages from the warren area and as a result picks up pesticide. Exactly how much pesticide is picked up
	* depends on the assumptions regarding foraging behaviour. These can be altered here, but the default assumption is that the rabbit will forage from all suitable forgage locations
	* equally, and therefore will pick up an average or maximum dose based on the concentration of residue in the forage areas. This is precalculated by the Rabbit_Warren in UpdatePesticide.
	* The pesticide response code is placed in EndStep - this will determine direct mortality or set any necessary flags indicating pesticide effects later,
	* e.g. for reproduction.
	*/
	if (m_myWarren != NULL) m_pesticide_burden+= m_myWarren->GetForagePesticide();
	return toRabbits_EvaluateTerritory;
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Male::st_EvaluateTerritory( void ) {
	/**
	* \return The next behavioural state. The only legal returns are toRabbits_EvaluateTerritory and toRabbits_Die
	*
	* The rabbit must evaluate his territory and decide if he will stay or leave. This decision is based on a number of criteria:
	*	-# An evaluation of the social mate status
	*	-# An evaluation of the current mate status
	*	-# An evaluation of the current territory quality
	*	-# A comparison with potential areas explored nearby
	*
	*	Here we make the assumption that if suitable quality unoccupied territory is known to be available and the rabbit is not dominant, then it will move to set up a territory there. If none is avaible or known, then
	*	the rabbit will remain as subordinate as long as the current territory can support him. If he is forced out he must find a vacent place or will die.
	*   If he stays he may become dominant one day.
	*
	* If he has no mate (he may be digging and not have a burrow) then before anything else we need to evaluate our current warren for possibilities - could be vacant burrow or mate here for us.
	* If no warren for some reason (could be start-up conditions), then find one.
	*/
	if (m_socialstatus > rabbit_socialstatus_subordinate) return toRabbits_Foraging; // Do it all again tomorrow, we are at the top or near the top of the heap in social terms
	if (m_myMate != NULL) {
#ifdef __RABBITDEBUG
		if (m_digging < 0) {
			int rubbish = 0;
		}
		if (m_haveBurrow) {
			int rubbish = 0;
		}
#endif
		return toRabbits_Foraging; // Must be digging a burrow
	}

	if (m_myWarren == NULL) {
		// Must find a warren - finds the closest - NB this code should not often be used, so efficiency here has not been optimised
		Rabbit_Warren* aRW = m_OurPopulationManager->FindClosestWarren( m_Location_x, m_Location_y, 1 );
		/** Next he moves there, and assesses whether he can join */
		bool survived = WalkTo( aRW->Supply_m_Location_x(), aRW->Supply_m_Location_y() );
		if (!survived) return toRabbits_Die;
		if (aRW->IsFreeFemale()) {
			aRW->JoinNMate( this, rob_Female );
		}
		/**
		* If there is a free burrow then joining is certain. If the warren site is unoccupied then currently the rabbit will join it and start to dig.
		* This behaviour might need to be altered later, if rabbits might not start up alone so easily.
		*/
		else {
			int burrow = aRW->IsFreeBurrow();
			if (burrow == 1) {
				aRW->JoinNOccupy( this );
			}
			else
				if (burrow == 2) {
					aRW->OccupyWarren( this );
				}
		}
		return toRabbits_Foraging;
	}
	else if (m_myWarren->IsFreeFemale()) {
		m_myWarren->Mate( this, rob_Female );
	}
	else if (m_myWarren->IsFreeBurrow() == 1) {
		m_myWarren->OccupyBurrow( );
		SetDigging( -1 );
		SetSocialStatus( rabbit_socialstatus_subdominant );
		SetHasBurrow( true );
	}
	else 	if (m_digging > 0) {
		return toRabbits_Foraging;
	}
	else
		/**
		* If nothing doing locally the Rabbit will move if conditions elsewhere are better and is not dominant or mated:
		* - If itinerent, possible free territory -> move
		* - If a mate is possible in the new place -> move
		*/
	{
		Rabbit_Warren* aWarren = m_myWarren->GetNetworkWarren();
		if (aWarren != NULL) {
			if (aWarren->IsFreeFemale()) {
				bool survived = WalkTo( aWarren->Supply_m_Location_x(), aWarren->Supply_m_Location_y() );
				if (!survived) return toRabbits_Die;
				m_myWarren->Leave( this );
				aWarren->JoinNMate( this, rob_Female );
			}
			else {
				int burrows = aWarren->IsFreeBurrow();
				if (burrows == 1) {
					bool survived = WalkTo( aWarren->Supply_m_Location_x(), aWarren->Supply_m_Location_y() );
					if (!survived) return toRabbits_Die;
					m_myWarren->Leave( this );
					aWarren->JoinNOccupy( this );
					m_socialstatus = rabbit_socialstatus_subdominant;
				}
				else if (burrows == 2) {
					bool survived = WalkTo( aWarren->Supply_m_Location_x(), aWarren->Supply_m_Location_y() );
					if (!survived) return toRabbits_Die;
					m_myWarren->Leave( this );
					aWarren->OccupyWarren( this );
					m_socialstatus = rabbit_socialstatus_subordinate; // Can dig a burrow but don't have one
				}
				else {
					// No room but we may need to move anyway
					int c = m_myWarren->GetCarryingCapacity();
					int n = m_myWarren->GetPopulationSize() - (c * 2);  // c is number of pairs, but there are 2 + a litter (average 6) as a minimum
					double occupation = n / (double)c;
					if (g_rand_uni() < occupation) {
						bool survived = WalkTo( aWarren->Supply_m_Location_x(), aWarren->Supply_m_Location_y() );
						if (!survived) return toRabbits_Die;
						m_myWarren->Leave( this );
						aWarren->Join( this );
						SetDigging( -1 );
						m_socialstatus = rabbit_socialstatus_zero;
					}
				}
			}
		}
	}
	return toRabbits_Foraging; // Do it all again tomorrow
}
//---------------------------------------------------------------------------


//********************************************************************************************************************
//                                     Rabbit_Female
//********************************************************************************************************************

Rabbit_Female::Rabbit_Female(int p_x, int p_y, int p_x2, int p_y2, Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_age, int a_weightage, Rabbit_Warren* a_warren) : Rabbit_Adult(p_x, p_y, p_x2, p_y2, p_L, p_NPM, a_age, a_weightage, a_warren)
{
	m_RabbitType = rob_Female;
	if (a_warren != NULL) a_warren->RabbitProductionRecord(rob_Female, 1);
	m_lactating = false;
	m_pregnant = false;
	m_gestationcounter = 0;
	m_myLitterSize = 0;
	m_MyOffspring = 0;
	m_MyTotalLitters = 0;
#ifdef __RABBITDEBUG
 if (m_myWarren != NULL) if (m_myWarren->IsMember(this)==false)
 {
		 m_OurLandscape->Warn("Rabbit_Female::Rabbit_Female","not a member of our warren");
		 exit(1);
 }
#endif
}
//---------------------------------------------------------------------------

Rabbit_Female::~Rabbit_Female(void)
{
	;
}
//---------------------------------------------------------------------------

void Rabbit_Female::Step(void)
{
	/**
	* The female step code is the most complicated of the rabbit behavioural control. It is very similar to the male's code but with the addition of the behaviours related to 
	* reproduction. Each day the cycle involves an evaluation of the current resource status, and then updating the reproductive behaviour. Exactly which behaviour needs to 
	* be updated depends on the current status and whether it is breeding season, and of course whether there is a mate. 
	*/
	if (m_StepDone || m_CurrentStateNo == -1) return;
    switch (m_CurrentRState)
    {
     case toRabbits_InitialState: // Initial state always starts with develop
	     m_CurrentRState=toRabbits_Foraging;
	     break;
	 case toRabbits_Foraging:
		 m_CurrentRState=st_Forage();
		 m_StepDone = true;
		 break;
     case toRabbits_EvaluateTerritory: // Initial state always starts with develop
	     m_CurrentRState=st_EvaluateTerritory();
		 break;
	 case toRabbits_UpdateBreedingStatus:
		 m_CurrentRState=st_UpdateBreedingStatus();
		 break;
	 case toRabbits_Gestation:
		 m_CurrentRState=st_Gestating();
		 break;
	 case toRabbits_GiveBirth:
		 m_CurrentRState=st_GiveBirth();
		 break;
	 case toRabbits_Lactating:
		 m_CurrentRState=st_Lactating();
		 break;
	 case toRabbits_Die:
		 st_Dying(); // No return value - no behaviour after this
		 m_StepDone=true;
		 break;
	 case toRabbits_Remove:
		 m_CurrentStateNo = -1;
		 m_StepDone=true;
		 break;
	 default:
		 m_OurLandscape->Warn("Rabbit_Female::Step()","unknown state - default");
		 exit(1);
	}
}
//---------------------------------------------------------------------------

void Rabbit_Female::st_Dying( void )
{
	/** 
	* Female dying state. Needs to:
	* - Tell any young
	* - Pass the mantle of dominance on if they are dominant
	* - Perform the adult dying actions
	*/	
	for(int l=0; l<m_myLitterSize; l++) 
	{
		m_myLitter[l]->OnMumDead();
	}
	if (m_socialstatus == rabbit_socialstatus_dominant) {
		m_myWarren->ChooseNewDominant();
	}
	// Record our repro output if needed
	if (cfg_RabbitUseReproOutput.value()) m_OurPopulationManager->ReproOutputRecordOutput(this);
	Rabbit_Adult::st_Dying();
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Female::st_EvaluateTerritory( void ) {
	/**
	* \return The next behavioural state. Legal returns are toRabbits_Foraging, toRabbits_UpdateBreedingStatus, and toRabbits_Die
	*
	* The rabbit must evaluate her territory and decide if she will stay or leave. This decision is based on a number of criteria:
	*	-# An evaluation of the current mate status
	*	-# An evaluation of the current social status
	*	-# An evaluation of the current territory quality
	*	-# A comparison with potential areas explored nearby
	*
	*	Here we make the assumption that if suitable quality unoccupied territory is known to be available and the rabbit is not dominant, then it will move to set up a territory there.
	*   If none is avaible or known, then the rabbit will remain as subordinate as long as the current territory can support her. If she is forced out she must find a vacent place or
	*   will die. If she stays she may become dominant one day.
	*/
	// We need to manage our annual repro info
	int today = m_OurLandscape->SupplyGlobalDate();
	for (int l = static_cast<int>(m_AnnualLitters.size()-1); l >= 0; l--) {
		if (today - m_AnnualLitters[ l ] > 365) {
			m_AnnualLitters.erase( m_AnnualLitters.begin() + (l) );
		}
	}
#ifdef __RABBITDEBUG
	if (m_myWarren != NULL)
	{
		if (m_myWarren->IsMember( this ) == false) {
			m_OurLandscape->Warn( "Rabbit_Female::st_EvaluateTerritory", "not a member of our warren" );
			exit( 1 );
		}
	}
#endif
	if (m_socialstatus > rabbit_socialstatus_subordinate) {
#ifdef __RABBITDEBUG
		if (m_myMate != NULL) {
			int rubbish = 0;
		}
#endif
		return toRabbits_UpdateBreedingStatus; // No need to check the rest we have it all
	}
	// Update breeding status needs StepDone == false, but otherwise we need to set StepDone here.
	m_StepDone = true;
	if (m_myMate != NULL) {
		return toRabbits_Foraging; // Must be digging a burrow
	}
	/**
	* No mate so before anything else we need to evaluate our current warren for possibilities - could be vacant burrow or mate here for us. If no warren for some reason, then find one.
	*/
	if (m_myWarren == NULL) {
		/**
		* If an itinerant then she must find a warren. Finds the closest - NB this code should not often be used, so efficiency here has not been optimised
		*/
		Rabbit_Warren* aRW = m_OurPopulationManager->FindClosestWarren( m_Location_x, m_Location_y, 1 );
		/** Next she moves there, and assesses whether she can join */
		bool survived = WalkTo( aRW->Supply_m_Location_x(), aRW->Supply_m_Location_y() );
		if (!survived) return toRabbits_Die;
		if (aRW->IsFreeMale()) {
			aRW->JoinNMate( this, rob_Male );
		}
		/**
		* If there is a free burrow then joining is certain. If the warren site is unoccupied then currently the rabbit will join it and start to dig.
		* This behaviour might need to be altered later, if rabbits might not start up alone so easily.
		*/
		else {
			int burrow = aRW->IsFreeBurrow();
			if (burrow == 1) {
				aRW->JoinNOccupy( this );
			}
			else
				if (burrow == 2) {
					aRW->OccupyWarren( this );
				}

		}
		return toRabbits_Foraging;
	}
	// We have a warren but no mate or burrow
	else if (m_myWarren->IsFreeMale()) {
		m_myWarren->Mate( this, rob_Male );
	}
	else if (m_myWarren->IsFreeBurrow() == 1) {
		m_myWarren->OccupyBurrow( );
		SetDigging( -1 );
		SetSocialStatus( rabbit_socialstatus_subdominant );
		SetHasBurrow( true );
	}
	else 	if (m_digging > 0) {
		return toRabbits_Foraging;
	}
	else
		/**
		* If nothing doing locally the Rabbit will move if conditions elsewhere are better and is not dominant or mated:
		* - If itinerent, possible free territory -> move
		* - If a mate is possible in the new place -> move
		* - If itinerent there is a chance of moving even if to nothing better
		*/
	{
		Rabbit_Warren* aWarren = m_myWarren->GetNetworkWarren();
		if (aWarren != NULL) {
			if (aWarren->IsFreeMale()) {
				bool survived = WalkTo( aWarren->Supply_m_Location_x(), aWarren->Supply_m_Location_y() );
				if (!survived) return toRabbits_Die;
				m_myWarren->Leave( this );
				aWarren->JoinNMate( this, rob_Male );
			}
			else {
				int burrows = aWarren->IsFreeBurrow();
				if (burrows == 1) {
					bool survived = WalkTo( aWarren->Supply_m_Location_x(), aWarren->Supply_m_Location_y() );
					if (!survived) return toRabbits_Die;
					m_myWarren->Leave( this );
					aWarren->JoinNOccupy( this );
					m_socialstatus = rabbit_socialstatus_subdominant;
				}
				else if (burrows == 2) {
					bool survived = WalkTo( aWarren->Supply_m_Location_x(), aWarren->Supply_m_Location_y() );
					if (!survived) return toRabbits_Die;
					m_myWarren->Leave( this );
					aWarren->OccupyWarren( this );
					m_socialstatus = rabbit_socialstatus_subdominant;
				}
				else {
					// No room but we may need to move anyway
					double dispersalchance = m_myWarren->GetAvailableForage()/5.0;
					if (g_rand_uni() < dispersalchance) {
						bool survived = WalkTo( aWarren->Supply_m_Location_x(), aWarren->Supply_m_Location_y() );
						if (!survived) return toRabbits_Die;
						m_myWarren->Leave( this );
						aWarren->Join( this );
						SetDigging( -1 );
						m_socialstatus = rabbit_socialstatus_zero;
					}
				}
			}
		}
	}
#ifdef __RABBITDEBUG
	if (m_myWarren != NULL) if (m_myWarren->IsMember( this ) == false) {
		m_OurLandscape->Warn( "Rabbit_Female::st_EvaluateTerritory", "not a member of our warren" );
		exit( 1 );
	}
#endif
	return toRabbits_Foraging; // Do it all again tomorrow
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Female::st_UpdateBreedingStatus( void )
{
	/** 
	* \return The repro behaviour to exhibit. Legal returns are: toRabbits_Gestation, toRabbits_GiveBirth, toRabbits_Lactating, toRabbits_Foraging
	* 
	* Determines where we are in the reproductive cycle. Will hold off or switch on oestrous depending upon season, otherwise will update gestation, continue lactating, mate 
	* or give birth as necessary.
	* This uses a number of flags and counters specific to the female rabbit to achieve this.
	* NB if she comes into oestrous and has a mate she will automatically become pregnant. Whether this will happen depends on the weather. Initially we assume that the average 
	* temperature over the last month determines intiation of breeding. Cessation may be more problematical. 
	* If lactating then the young are forced out at 30 days, when she can be giving birth again if it is still breeding season.
	* If it is not breeding season and nothing else is happening then evaulate territory. This requires setting StepDone to true to prevent continuous looping.
	*/
	if (m_OurPopulationManager->IsBreedingSeason()) 
	{
		// Do we have a litter on the way?
		if (m_pregnant) 
		{
			return toRabbits_Gestation;
		}
		else 
		{
			// Not pregnant but breeding season so check if there is a mate.
			if (m_myMate != NULL)
			{
				m_pregnant = true;
				m_gestationcounter = 0;
			}
			else if (m_myWarren->IsFreeMale())
			{
 				m_myWarren->Mate( this, rob_Male );
			}
		}
		return toRabbits_Foraging;
	}
	// Not breeding season but may have young
	if (m_lactating) return toRabbits_Lactating;
	else
	{
		m_gestationcounter = 0;
		return toRabbits_Foraging;
	}
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Female::st_Forage( void ) {
	/**
	* This is a critical part of the pesticide handling code. The rabbit forages from the warren area and as a result picks up pesticide. Exactly how much pesticide is picked up
	* depends on the assumptions regarding foraging behaviour. These can be altered int GetForagePesticide(), but the default assumption is that the rabbit will forage from all suitable forgage locations
	* equally, and therefore will pick up an average dose based on the average concentration of residue in the forage areas (there is an alternative version that picks the maximum concentration).
	* This is precalculated by the Rabbit_Warren in UpdatePesticide.
	* The pesticide response code is placed in EndStep - this will determine direct mortality or set any necessary flags indicating pesticide effects later,
	* e.g. for reproduction.
	*/
	if (m_myWarren != NULL) m_pesticide_burden += m_myWarren->GetForagePesticide();
	return toRabbits_EvaluateTerritory;
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Female::st_GiveBirth( void ) {
	/**
	* Produces kits. If currently lactating then expels the current young. If it is the breeding season and she has a mate, then she becomes pregnant again immediately.
	* Whether the pregancy is successful will depend on the social status. If social status is too low they cannot reproduce, sub dominant has % chance to reproduce.
	* Reproduction will also depend on the current forage and density
	*/
	if (m_myLitterSize > 0) {
		for (int l = 0; l < m_myLitterSize; l++) {
			m_myLitter[ l ]->OnEvicted();
			m_myLitter[ l ] = NULL;
		}
		m_myLitterSize = 0;
	}
	if (!m_pesticideInfluenced1) {
		// Here we take both social status and weight into account
		if (m_socialstatus != rabbit_socialstatus_dominant) {
			if (((double)m_socialstatus*g_rand_uni()* (m_weightAge / 300.0) < cfg_rabbitsocialreproductionthreshold.value())) return toRabbits_Foraging;
		}
		int kits = 0;
		// Here we have a test for re-abosorption of the litter depending on forage conditions. Low forage high chance of re-absorption
		if (g_rand_uni() > m_myWarren->GetLitterReabsortionConst()) {
			struct_Rabbit* sR = new struct_Rabbit;
			sR->m_age = 0;
			sR->m_L = m_OurLandscape;
			sR->m_NPM = m_OurPopulationManager;
			sR->m_x = m_Location_x;
			sR->m_y = m_Location_y;
			sR->m_x2 = m_Location_x;
			sR->m_y2 = m_Location_y;
			sR->m_Warren = m_myWarren;
			kits = CalcLitterSize();
			m_OurPopulationManager->CreateObjects( rob_Young, this, sR, kits );
			m_myWarren->RabbitProductionRecord( rob_Young, kits );
			delete sR;
		}
		if (kits > 0) {
			m_lactating = true;
			// Record our success
			m_MyTotalLitters++;
			m_MyOffspring += kits;
			// We also need to manage our annual repro info
			m_AnnualLitters.push_back( m_OurLandscape->SupplyGlobalDate() );
		}
	}
	else {
		m_pesticideInfluenced1 = false; // Reset the pesticide flag since we lost the litter
		m_OurPopulationManager->PesticideDeathRecord( rob_Young );
	}
	if (m_OurPopulationManager->IsBreedingSeason()) {
		if (m_myMate != NULL) {
			m_pregnant = true;
			m_gestationcounter = 0;
		}
	}
	return toRabbits_Foraging;
}
//---------------------------------------------------------------------------

int Rabbit_Female::CalcLitterSize( void )
{
	/**
	* This determines how many young are born in a litter. Litter sizes are 3-8 young, but what controls this is not yet implemented, currently the results are stochastic between these
	* extremes. However, there appears to be no consensus in the literature about this. Larger females have more young, but survival seems also to be greater for 
	* individuals of smaller litters. Since this seems to balance out uniform variation and standardised subsequent survival have been implemented.
	*/
	return int(floor(m_MinKitsNo + g_rand_uni() * m_MaxKitsNo));
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Female::st_Lactating( void )
{
	/** 
	* Rotates round each of the current young and gives them milk. If this does not happen for some reason then the young will automatically die
	*/
	for(int lit=0; lit<m_myLitterSize; lit++) 
	{
		m_myLitter[lit]->OnFed();
	}
	return toRabbits_Foraging;
}
//---------------------------------------------------------------------------

TTypeOfRabbitState Rabbit_Female::st_Gestating( void )
{
	/** 
	* Updates gestation counter and if still lactating returns a suitable return state, unless its time to give birth.
	*/
	m_gestationcounter++;
	if (m_gestationcounter >= 30) {
		return toRabbits_GiveBirth;
	}
	if (m_lactating) return toRabbits_Lactating;
	return toRabbits_Foraging;
}
//---------------------------------------------------------------------------

void Rabbit_Female::OnYoungDeath(Rabbit_Young* a_young)
{
	/**
	* If a young has died then it is removed from the litter list. If it is the last one, then lactation is stopped immediately.
	*/
	for(int l=0; l<m_myLitterSize; l++) 
	{
		if (m_myLitter[l] == a_young)
		{
			for (int n=l; n<m_myLitterSize; n++)
			{
				m_myLitter[n] = m_myLitter[n+1];
			}
			m_myLitter[--m_myLitterSize] = NULL;
			if (m_myLitterSize == 0) m_lactating = false;
			return;
		}
	}
	// If we get here there is an error
	m_OurLandscape->Warn("Rabbit_Female::OnYoungDeath","unknown litter member");
	exit(1);
}
//---------------------------------------------------------------------------

void Rabbit_Female::Weaned(Rabbit_Young* a_young)
{
	/**
	* If a young has been weaned then it is removed from the litter list. If it is the last one, then lactation is stopped immediately.
	*/
	for(int l=0; l<m_myLitterSize; l++) 
	{
		if (m_myLitter[l] == a_young)
		{
			for (int n=l; n<m_myLitterSize; n++)
			{
				m_myLitter[n] = m_myLitter[n+1];
			}
			m_myLitter[--m_myLitterSize] = NULL;
			if (m_myLitterSize == 0) m_lactating = false;
			return;
		}
	}
	// If we get here there is an error
	m_OurLandscape->Warn("Rabbit_Female::Weaned","unknown litter member");
	exit(1);
}
//---------------------------------------------------------------------------

//*************************************************************************************
//**************** PESTICIDE RESPONSE CODE FOR ALL RABBIT CLASSES IS HERE *************
//*************************************************************************************

void Rabbit_Juvenile::InternalPesticideHandlingAndResponse() {
	/**
	* This method is re-implemented ffrom Rabbit_Base for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then call pesticide-specific actions by dose
	*/
	TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
	double pesticideInternalConc = m_pesticide_burden / m_weight;

	if (pesticideInternalConc > cfg_RabbitPesticideResponse.value()) {
		switch (tp) {
			case ttop_NoPesticide:
				break;
			case ttop_ReproductiveEffects:
				break;
			case ttop_AcuteEffects:
				GeneralOrganoPhosphate( pesticideInternalConc ); // Calls the GeneralOrganophosphate action code
				break;
			default:
				g_msg->Warn( "Unknown pesticide type used in Rabbit_Juvenile::InternalPesticideHandlingAndResponse() pesticide code ", int( tp ) );
				exit( 47 );
		}
	}
	m_pesticide_burden *= m_pesticidedegradationrate; // Does nothing by default except internal degredation of the pesticide
}

void Rabbit_Male::InternalPesticideHandlingAndResponse() {
	/**
	* This method is re-implemented ffrom Rabbit_Base for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then call pesticide-specific actions by dose
	*/
	TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
	double pesticideInternalConc = m_pesticide_burden / m_weight;

	if (pesticideInternalConc > cfg_RabbitPesticideResponse.value()) {
		switch (tp) {
			case ttop_NoPesticide:
				break;
			case ttop_ReproductiveEffects:
				break;
			case ttop_AcuteEffects:
				GeneralOrganoPhosphate( pesticideInternalConc ); // Calls the GeneralOrganophosphate action code
				break;
			default:
				g_msg->Warn( "Unknown pesticide type used in Rabbit_Male::InternalPesticideHandlingAndResponse() pesticide code ", int( tp ) );
				exit( 47 );
		}
	}
	m_pesticide_burden *= m_pesticidedegradationrate; // Does nothing by default except internal degredation of the pesticide
}

void Rabbit_Female::InternalPesticideHandlingAndResponse() {
	/**
	* This method is re-implemented ffrom Rabbit_Base for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then call pesticide-specific actions by dose
	*/
	TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
	double pesticideInternalConc = m_pesticide_burden / m_weight;

	if (pesticideInternalConc > cfg_RabbitPesticideResponse.value()) {
		switch (tp) {
			case ttop_NoPesticide:
				break;
			case ttop_ReproductiveEffects:
				GeneralEndocrineDisruptor( pesticideInternalConc ); // Calls the EndocrineDisruptor action code
				break;
			case ttop_AcuteEffects:
				GeneralOrganoPhosphate( pesticideInternalConc ); // Calls the GeneralOrganophosphate action code
				break;
			default:
				g_msg->Warn( "Unknown pesticide type used in Rabbit_Female::InternalPesticideHandlingAndResponse() pesticide code ", int( tp ) );
				exit( 47 );
		}
	}
	m_pesticide_burden *= m_pesticidedegradationrate; // Does nothing by default except internal degredation of the pesticide
}
//-------------------------------------------------------------------------------------
//-------------------- GENERAL ENDOCRINE DISRUPTOR EFFECT CODE ------------------------
//-------------------------------------------------------------------------------------

void Rabbit_Female::GeneralEndocrineDisruptor( double /* a_dose */ ) {
	/** 
	* For rabbits, only the female responds to this type of pesticide
	*/
	// May also wish to specify certain gestation days for the effects here
	if (m_gestationcounter > 0) {
		m_pesticideInfluenced1 = true;
	}
}

//-------------------------------------------------------------------------------------
//------------------------ GENERAL ORGANOPHOSPHATE EFFECT CODE ------------------------
//-------------------------------------------------------------------------------------

void Rabbit_Base::GeneralOrganoPhosphate( double /* a_dose */ ) {
	//if (g_rand_uni() > l_pest_daily_mort.value()) return;
	m_OurPopulationManager->PesticideDeathRecord( m_RabbitType );
	m_CurrentRState = toRabbits_Die;
	m_StepDone = true;
	return;
}

//*************************************************************************************
//************************** END PESTICIDE RESPONSE CODE ******************************
//*************************************************************************************


//********************************************************************************************************************
//                                     Rabbit_Warren
//********************************************************************************************************************

Rabbit_Warren::Rabbit_Warren(int p_x, int p_y,Landscape* p_L, Rabbit_Population_Manager* p_NPM, int a_size, int a_soil) : TAnimal(p_x,p_y,p_L)
{
	m_CurrentRState = toRabbits_InitialState;
	m_OurPopulationManager = p_NPM;
	m_size = a_size;
	m_maintenence = 0;
	m_NoOccupiedBurrows = 0;
	m_BurrowsUnderConstruction = 0;
	m_CarryingCapacity = 0;
	m_CarryingCapacityRatio = -1; // No effect for the first period until it is properly calculated
	m_InhabitantsList.resize(0);
	m_soiltype = a_soil;
	m_rabbitdiggingtime = cfg_rabbitdiggingtime.value() * (2 - m_soiltype);
	m_diseaseconstant = 0.0;
	ResetAllRabbitProductionRecord();
	m_BigFemaleRabbitsR = 0.0;
	m_runningavCount=0;
	m_runningavFemales = 0;	/**
	* To get the warrens to display properly the centre location is needed. Unfortunately to work with display this needs to be m_Location_x & m_Location_y so we need to store 
	* TL_x & TL_y too
	*/
	m_Location_x = m_Location_x + (m_size / 2);
	m_Location_y = m_Location_y + (m_size / 2);
	// Now get the forage size.
	m_foragesize = m_size * 2;
	m_TL_x = m_Location_x - (m_foragesize / 2);
	if (m_TL_x < 0) m_TL_x = 0;
	m_TL_y = m_Location_x - (m_foragesize / 2);
	if (m_TL_y < 0) m_TL_y = 0;
	/**
	* Must do an evaluation of the warren area and populate our list of polygons and areas. This is done by #InitEvaluation
	*/
	InitEvaluation();
}
//---------------------------------------------------------------------------

Rabbit_Warren::~Rabbit_Warren( void )
{
	;
}
//---------------------------------------------------------------------------

void Rabbit_Warren::InitEvaluation( void )
{
	/**
	* This method is called when the warren in initially formed and is repsponsible for making the calcuations of forage and cover areas and then calculating the carrying capacity 
	* of the warren if fully occupied.
	*/
	/**
	* First fix any wrap-around problems.
	*/
	if (m_TL_x + m_foragesize > m_OurLandscape->SupplySimAreaWidth()) m_foragesize = (m_OurLandscape->SupplySimAreaWidth())-m_TL_x;
	if (m_TL_y + m_foragesize > m_OurLandscape->SupplySimAreaHeight()) m_foragesize = (m_OurLandscape->SupplySimAreaHeight())-m_TL_y;
	/**
	* Next loop through the area and find all the polygons and areas. 
	*/
	for (int i=m_TL_x; i<m_TL_x + m_foragesize; i++)
	{
		for (int j=m_TL_y; j<m_TL_y + m_foragesize; j++)
		{
			/** 
			* This is done by getting the polygon references and checking if we had this one already. If not save it with an area of 1 otherwise increment the correct record by 1. 
			*/
			int PRef = m_OurLandscape->SupplyPolyRef(i,j);
			bool found=false;
			for (vector<RabbitWarrenLEInfo>::size_type k=0; k<m_LEList.size(); k++)
			{
				if (m_LEList[k].m_ref == PRef)
				{
				  m_LEList[k].m_area++;
				  found = true;
				  break;
				}
			}
			if (!found)
			{
				RabbitWarrenLEInfo a_ele;
				a_ele.m_ref = PRef;
				a_ele.m_area = 1;
				a_ele.m_ele = m_OurLandscape->SupplyLEPointer(m_OurLandscape->SupplyPolyRef(i,j));
				a_ele.m_pesticide_conc = 0;
				a_ele.m_forage = 0;
				a_ele.m_foragearea = 0;
				// don't have this one
				m_LEList.push_back(a_ele);
			}
		}
	}
	/** Once all polygons are identified and counted then they are classified. */
	for (vector<RabbitWarrenLEInfo>::size_type k=0; k<m_LEList.size(); k++)
	{
		TTypesOfLandscapeElement tole = m_LEList[k].m_ele->GetElementType();
		m_LEList[k].m_torh = m_OurPopulationManager->ClassifyHabitat(tole);
		// Do a little bit of forage managing here
		if (m_LEList[k].m_torh == torh_Forage) m_LEList[k].m_foragearea = m_LEList[k].m_area;
		else if (m_LEList[k].m_torh == torh_TemporaryForage)
		{
			// We assume only 50 m strip of fields is usable
			int fr = 50 * m_size;
			if (m_LEList[k].m_area < fr) fr = m_LEList[k].m_area;
			m_LEList[k].m_foragearea = fr;
		}
		else m_LEList[k].m_foragearea = 0;
	}
	m_permforagearea = CalcPermForageArea();
	m_foragearea = CalcForageArea();
	/** Next the carrying capacity is calculated. This is based on forage area possible and is the number of pairs possible. */
	double minpct = 1.0 / 16.0; // Divide by 16 because the minimum is based on 1/16th of the area used for forage
	double pct = m_foragearea/ (double) (m_foragesize * m_foragesize);
	if (pct<minpct)
	{
		// We have a problem, no rabbits can be here
		m_CarryingCapacity = 0;
		return;
	}
	if (pct > 0.9) pct = 0.9;
	// 1 is the lowest density until soil type is taken into account
	// We use the diff between min and actual pct cover, and then add pro-rata the extra proportion of 12 pairs
	// NB minpct must never be >= 0.9
	m_CarryingCapacity = static_cast<int>(cfg_rabbitminwarrensize.value() + (int)((pct - minpct) / (0.9 - minpct) * cfg_rabbitmaxwarrensize.value())); // e.g. 3 + (0.1 / 0.8 *9) = 3+1.5 for minpct 0.2, pct 0.3
	if (m_soiltype == 0) m_CarryingCapacity /= 2; /** We assume that the carrying capacity of sandy soil warrens is halved, unless there is only room for one burrow anyway. */
	m_NoBurrows = 0;
	m_CarryingCapacityR = m_CarryingCapacity * cfg_rabbitdendepscaler.value();
	m_CarryingCapacityR2 = m_CarryingCapacityR / 2.0; // Done here once to prevent it being recalculated possibly a lot of times later.

}
//---------------------------------------------------------------------------

void Rabbit_Warren::CalcDisease(void)
{
	/**
	* To simulate disease we need a globally variying probability with local variations depending upon density
	* This is done by evaluating two levels - the first is the total population density. This is given by the the total number
	* of rabbits divided by the total number of warrens.\n
	* The local density is the number of rabbits related to the carrying capacity.\n
	*
	* This method is only called at a time period specified as an input variable, so disease will not instantly equilibrate.\n
	* The big disease chance is based on global density:
	* prob_density = (totalrabbits / no warren) x (totalrabbits / no warren) * cfg_globaldisease_probability.value();
	* This is then modified by local density:
	* m_diseaseconstant = prob_density * #GetCarryingCapacityRatio
	*/
	// Get the numbers we need
	unsigned totalrabbits = m_OurPopulationManager->SupplyAllBigRabbits();
	unsigned warrens = m_OurPopulationManager->GetLiveArraySize(rob_Warren);
	// Some of this should be done by the population managerfor efficiency but it is here to keep it together.
	//The big chance based on global density
	double prob_density = (totalrabbits / (double)warrens) * (totalrabbits / (double)warrens) * cfg_globaldisease_probability.value();
	// Now the local stuff
	m_diseaseconstant = prob_density * GetCarryingCapacityRatio();  
}
//---------------------------------------------------------------------------

void Rabbit_Warren::UpdatePesticide( void ) {
	/**
	* This is a very 'costly' method. It should not be called if there is no pesticide in use.
	*
	* Runs through the warren forage area and sums the pesticide.If __RABBITMEANPESTICIDECONC is defined then the pesticide values in all polygons are
	* divided by the area of forage in the warren and the concentration determined. Otherwise the maximum concentration is found in forage areas and set as 
	* the value for returning in m_forageP
	*
	*/
	m_forageP = 0.0;
	if (!m_OurLandscape->SupplyPesticideDecay(ppp_1)) return;
	for (int i = m_TL_x; i < m_TL_x + m_foragesize; i++) {
		for (int j = m_TL_y; j < m_TL_y + m_foragesize; j++) {
			// Unfortunately we need to check for each square whether the rabbit can forage here.
			int ele = m_OurLandscape->SupplyPolyRef( i, j );
			for (vector<RabbitWarrenLEInfo>::size_type k = 0; k < m_LEList.size(); k++) {
				if (m_LEList[ k ].m_ref == ele) {
					if (m_LEList[ k ].m_forage > 0.0) {
#ifdef __RABBITMEANPESTICIDECONC
						m_forageP += m_OurLandscape->SupplyPesticide( i, j );
#else
						double pe = m_OurLandscape->SupplyPesticide( i, j , ppp_1);
						if (m_forageP< pe ) m_forageP = pe;
#endif
					}
					break;
				}
			}
		}
	}
	// Now calculate the mean concentration and mean forage concentration if needed
#ifdef __RABBITMEANPESTICIDECONC
	m_forageP /= (double)m_availableforage;
#endif
}
//---------------------------------------------------------------------------

void Rabbit_Warren::UpdateForageInformation( void ) {
	/**
	* This method updates the warren forage information based upon the vegetation height and digestability for each forage polygon.
	* Other options which could be exercised here are to use the vegetation type, biomass or density.\n
	* \n
	* Also calculated here is the amount of available forage relative to the current carrying capacity ratio.
	* Low forage availability will increase death rates in adults, but only if the carrying capacity is too low (based on current forage).
	*/
	double availableforage = m_permforagearea;
	for (vector<RabbitWarrenLEInfo>::size_type k = 0; k < m_LEList.size(); k++) {
		if (m_LEList[ k ].m_torh == torh_TemporaryForage) {
			double vegheight = m_OurLandscape->SupplyVegHeight( m_LEList[ k ].m_ref ); // m_ref is the landscape polyref
			double vegdigestability = m_OurLandscape->SupplyVegDigestibility( m_LEList[ k ].m_ref );
			if ((vegheight < m_maxForageHeight) && (vegdigestability > m_minForageDigestibility)) {
				// 30 cm
				m_LEList[ k ].m_forage = m_LEList[ k ].m_foragearea;
				availableforage += m_LEList[ k ].m_foragearea;
			}
			else m_LEList[ k ].m_forage = 0.0;
		}
	}
	// Only do the expensive pesticide update if there is a chance that there is some pesticide to work with
	if (l_pest_enable_pesticide_engine.value()) UpdatePesticide();

	m_foragearea = CalcForageArea();
	if (m_OurLandscape->SupplyGlobalDate() % cfg_rabbitdensitydependencedelay.value() ==0) {
		m_CarryingCapacityRatio = CalcCarryingCapacityRatio2(); // Normally this is the same as m_BigFemaleRabbitsR, which gets bigger as rabbits get close to CC
	}
	m_foragearearatio = availableforage / m_foragearea;
	if ((m_foragearearatio) < m_CarryingCapacityRatio) {
		m_availableforage = m_foragearearatio / m_CarryingCapacityRatio;
		m_inv_availableforage = 1.0 / m_availableforage;
	}
	else {
		m_availableforage = 1.0;
		m_inv_availableforage = 0.0;
	}
	if (m_OurLandscape->SupplyDayInYear() == cfg_rabbitdensitydependencedelay.value()) {
		m_litterreabosorptionchance = cfg_litterabsorptionconstant.value() * pow( 1.0 - m_availableforage, cfg_litterabsorptionconstant.value() );
	}
}
//---------------------------------------------------------------------------

void Rabbit_Warren::CalcCarryingCapacityRatio1() {
	m_BigFemaleRabbitsR = GetAllBigFemaleRabbits() / (m_CarryingCapacityR2); // m_CarryingCapacityR is (m_CarryingCapacity * cfg_rabbitdendepscaler.value())/2
}

double Rabbit_Warren::CalcCarryingCapacityRatio2() {
	/**
	* Here the issue can be that the local warren is too small a unit to use for carrying capacity calculations for the POM tests on Bayreuth
	* Therefore we have two options: 
	* - 1 use the local rabbits for real landscapes
	* - 2 a combined area calculation (which takes more time) for enclosure tests
	* 
	* To speed this on each warren creates its own daily stats which can be combined here.
	*/
	
#ifdef __EnclosureTest
	double CCRatio = m_BigFemaleRabbitsR;
	int nsz = (int)m_LocalWarrenNetwork.size();
	for (int i = 0; i < nsz; i++) {
		CCRatio += m_LocalWarrenNetwork[i].m_aWarren->GetCCRabbitsR();
	}
	CCRatio /= (nsz + 1);
	return CCRatio;
#endif
	return m_BigFemaleRabbitsR; // Is the number of big female rabbits / ((m_CarryingCapacity * cfg_rabbitdendepscaler.value())/2)
}


void Rabbit_Warren::NetworkEvaluation( void )
{
	int nsz = (int) m_LocalWarrenNetwork.size();
	/**
	* We need to randomise the list because otherwise the probability will always favour the first ones added - which causes NE movement in general
	*/
	std::shuffle (m_LocalWarrenNetwork.begin(), m_LocalWarrenNetwork.end(), g_std_rand_engine );
	for (int i=0; i< nsz; i++ )
	{
		// Probability calculated as m_size/dist
		m_LocalWarrenNetwork[i].m_visitprob = m_foragesize / (double) (m_LocalWarrenNetwork[i].m_dist);
	}
}
//---------------------------------------------------------------------------

int Rabbit_Warren::CalcForageArea()
{
	/**
	* This returns the forge area calcuated by element type.
	*/
	int area = 0;
	for (vector<RabbitWarrenLEInfo>::size_type k = 0; k<m_LEList.size(); k++)
	{
		area += m_LEList[k].m_foragearea;
	}
	return area;
}
//---------------------------------------------------------------------------

int Rabbit_Warren::CalcPermForageArea()
{
	/**
	* This returns the forge area calcuated by element type.
	*/
	int area = 0;
	for (vector<RabbitWarrenLEInfo>::size_type k = 0; k<m_LEList.size(); k++)
	{
		if (m_LEList[k].m_torh == torh_Forage) area += m_LEList[k].m_area;
	}
	return area;
}
//---------------------------------------------------------------------------

int Rabbit_Warren::GetTemporaryForageArea()
{
	/**
	* This returns the forge area calcuated by element type. This is probably mis-leading and the GetTemporaryForageAreaVeg() is better representative of the forage levels.
	*/
	int area = 0;
	for (vector<RabbitWarrenLEInfo>::size_type k=0; k<m_LEList.size(); k++)
	{
		if (m_LEList[k].m_torh == torh_TemporaryForage) area += m_LEList[k].m_area;
	}
	return area;
}
//---------------------------------------------------------------------------

int Rabbit_Warren::GetCoverArea()
{
	/**
	* This returns the cover area calcuated by element type. This is probably mis-leading and the GetCoverAreaVeg() is better representative of the cover leves including high vegetation levels.
	*/
	int area = 0;
	for (vector<RabbitWarrenLEInfo>::size_type k=0; k<m_LEList.size(); k++)
	{
		if (m_LEList[k].m_torh == torh_Cover) area += m_LEList[k].m_area;
	}
	return area;
}
//---------------------------------------------------------------------------

int Rabbit_Warren::GetTempForageAreaVeg()
{
	/**
	* This returns the temporary forge area calcuated by element type and vegetation height.
	*/
	int area = 0;
	for (vector<RabbitWarrenLEInfo>::size_type k=0; k<m_LEList.size(); k++)
	{
		if (m_LEList[k].m_torh == torh_TemporaryForage) 
		{
			if (m_LEList[k].m_ele->GetVegHeight() < m_maxForageHeight )
			{
				area += m_LEList[k].m_area;
			}
		}
	}
	return area;
}
//---------------------------------------------------------------------------

int Rabbit_Warren::GetCoverAreaVeg() {
	/**
	* This returns the cover area calcuated by element type and vegeation height.
	*/
	int area = 0;
	for (vector<RabbitWarrenLEInfo>::size_type k = 0; k<m_LEList.size(); k++) {
		if (m_LEList[ k ].m_ele->GetVegHeight() >= m_maxForageHeight) {
			if (m_LEList[ k ].m_torh != torh_Other) area += m_LEList[ k ].m_area;
		}
	}
	return area;
}
//---------------------------------------------------------------------------


void Rabbit_Warren::UpdateThisYearsBreeders() {
	/**
	* Loops through the list of inhabitants and checks if each is a female > 1 year, if so whether it produced a litter this year.
	*
	* \return the number of females that produced litters this year
	*/
	m_breedingfemales = 0;
	m_nonbreedingfemales = 0;
	m_1yrOldFemales = 0;
	m_littersthisyear = 0;
	int sz = static_cast<int>(m_InhabitantsList.size());
	for (int r = 0; r < sz; r++) {
		if (m_InhabitantsList[ r ]->GetRabbitType() == rob_Female) {
			if (m_InhabitantsList[ r ]->GetAge() < 300) {
				m_1yrOldFemales++;
			}else{
				int litters = dynamic_cast<Rabbit_Female*>(m_InhabitantsList[ r ])->GetLittersThisYear();
				if (litters > 0) m_breedingfemales++; else m_nonbreedingfemales++;
				m_littersthisyear += litters;
			}
		}
	}
}
//---------------------------------------------------------------------------

void Rabbit_Warren::Step( void )
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
    switch (m_CurrentRState)
    {
     case toRabbits_InitialState: 
		 m_CurrentRState = toRabbits_WarrenBeing;
		 break;
     case toRabbits_WarrenBeing: // Being state is never left for warrens - 'they just are'
		 st_WarrenBeing();
		 m_StepDone=true;
		 break;
	 default:
		 m_OurLandscape->Warn("Rabbit_Warren::Step()","unknown state - default");
		 exit(1);
	}
}
//---------------------------------------------------------------------------

void Rabbit_Warren::st_WarrenBeing( void )
{
	/** 
	* This is the only warren behaviour - the warren just is, and this is where any daily updates to its status are performed.
	*
	* First daily job it to determine whether warren maintenence should be kept up, increased or decreased. Then use this to determine the current burrow state.
	* Then a check is made for a dominant female. If there is one then OK, otherwise see if one can be made.
	* 
	* Next the food availability is calculated. This is a cominbination of permanent forage and suitable temporary forage.
	*
	* Finally if there is any chance that there is some pesticide to take into account the UpdatePesticide method is called.
	*/
#ifdef __RABBITDEBUG
	DEBUG_InternalTest();
#endif

	if (m_InhabitantsList.size() == 0)
	{
		m_maintenence -= 2 - m_soiltype; // Double rate of maintenence for light soil
	}
	if (m_maintenence > m_rabbitdiggingtime) m_maintenence = m_rabbitdiggingtime;
	if (m_maintenence < 0)
	{
		m_maintenence = m_rabbitdiggingtime;
		if (m_NoBurrows > 0) m_NoBurrows--;
	}
	// Dominance check
	UpdateDominance();
	// Food update
	UpdateForageInformation();
	// Now calculates the mortality chance for today
	double foragemult = 0;
	if (!m_OurPopulationManager->GetForageDay()) foragemult = 1 + cfg_forageareamortscaler.value(); // *(1 - m_availableforage); // m_foragearearatio is 0-1 and larger available forage is reduced.
	m_mortalitymultiplierA = 1 + m_diseaseconstant + foragemult; // Disease is the only density dependent death for adults
	// We need to de-couple mortality of juveniles and density
	m_mortalitymultiplierJ = 1 + foragemult;
#ifdef __RABBITDEBUG
	DEBUG_InternalTest();
#endif
	// Keep track of breeding females for POM
	if (m_OurPopulationManager->IsBreedingSeason()) {
		m_runningavCount++;
		m_runningavFemales += GetAllBreedingFemaleRabbits();
	}
}
//---------------------------------------------------------------------------


Rabbit_Warren* Rabbit_Warren::GetNetworkWarren(  void )
{
	/** 
	* This method chooses a warren from the network with a probability of choice based on the distance from home. However, we need to start at a random point in the list and 
	* cycle because otherwise we run the risk of only the first few warrens ever being tested. 
	*/
	// Pick a warren
	int nsz = (int) m_LocalWarrenNetwork.size();
	int loopstart = (int) floor(nsz * g_rand_uni());
	for (int i=0; i< nsz; i++ )
	{
		double chance = g_rand_uni();
		int index = (i + loopstart);
		if (index >= nsz) index-=nsz;
		if (chance>m_LocalWarrenNetwork[i].m_visitprob)
		{
			return m_LocalWarrenNetwork[i].m_aWarren;
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

void Rabbit_Warren::UpdateDominance( void ) {
	/**
	* \return false if none, true if unmated female found
	* Checks the list of residents for a female with a burrow and no mate.
	*/
	int sz = (int)m_InhabitantsList.size();
	for (int i = 0; i<sz; i++) {
		if (m_InhabitantsList[ i ]->GetRabbitType() == rob_Female) {
			if (dynamic_cast<Rabbit_Female*>(m_InhabitantsList[ i ])->GetSocialStatus() == rabbit_socialstatus_subdominant) return;
		}
	}
	// If we get here we have no dominant female, so see if one can be created.
	ChooseNewDominant();
}
//---------------------------------------------------------------------------

void Rabbit_Warren::ChooseNewDominant( void ) {
	/**
	* Loops through the list to find suitable females who could be dominant. If no fully grown females then the largest one is taken.
	*/
	Rabbit_Female* rfemale = NULL;
	Rabbit_Female* rfemaleBig = NULL;
	int bigWeight = 0;
	int sz = (int)m_InhabitantsList.size();
	for (int i = 0; i < sz; i++) {
		if (m_InhabitantsList[ i ]->GetRabbitType() == rob_Female) {
			rfemale = dynamic_cast<Rabbit_Female*>(m_InhabitantsList[ i ]);
			if (rfemale->GetSocialStatus() == rabbit_socialstatus_subdominant) {
				if (rfemale->GetweightAge() >= 300) { // 300 represents max growth
					rfemale->SetSocialStatus( rabbit_socialstatus_dominant );
					return;
				}
				else if (rfemale->GetweightAge() > bigWeight) {
					bigWeight = rfemale->GetweightAge();
					rfemaleBig = rfemale;
				}
			}
		}
	}
	if (rfemaleBig!=NULL) rfemaleBig->SetSocialStatus( rabbit_socialstatus_dominant );
}
//---------------------------------------------------------------------------

bool Rabbit_Warren::IsFreeFemale( void ) {
	/**
	* \return false if none, true if unmated female found
	* Checks the list of residents for a female with a burrow and no mate.
	*/
	int sz = (int)m_InhabitantsList.size();
	for (int i = 0; i<sz; i++) {
		if (m_InhabitantsList[ i ]->GetRabbitType() == rob_Female) {
			if (m_InhabitantsList[ i ]->GetMate() == NULL) {
				if ((m_InhabitantsList[ i ]->GetHasBurrow()) || (m_InhabitantsList[ i ]->GetDigging() > -1)) return true;
			}
		}
	}
	return false;
}
//---------------------------------------------------------------------------

bool Rabbit_Warren::IsMember( Rabbit_Base* a_rabbit )
{
	/**
	* \return false if none, true if unmated male found
	* Checks the list of residents for a male with a burrow and no mate. 
	*/
	int sz = (int) m_InhabitantsList.size();
	for (int i=0; i<sz; i++)
	{
		if (m_InhabitantsList[i] == a_rabbit ) 
		{
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

bool Rabbit_Warren::IsFreeMale( void )
{
	/**
	* \return false if none, true if unmated male found
	* Checks the list of residents for a male with a burrow and no mate. 
	*/
	int sz = (int) m_InhabitantsList.size();
	for (int i=0; i<sz; i++)
	{
		if  ( m_InhabitantsList[i]->GetRabbitType() == rob_Male )
		{
			if ( m_InhabitantsList[i]->GetMate()==NULL )
			{
				if ( (m_InhabitantsList[i]->GetHasBurrow() ) || ( m_InhabitantsList[i]->GetDigging() > -1) ) return true;
			}
		}
	}
	return false;
}
//---------------------------------------------------------------------------

int Rabbit_Warren::IsFreeBurrow( void )
{
	/** 
	* Here we need to determine if there are free burrows, but also what to do about an unoccupied location. If the warren is unoccupied then we need to arrange for the rabbit to 
	* slowly make burrow. 
	*
	* \return 0 if all burrows are occupied
	* \return 1 if there is a free burrow
	* \return 2 if the warren is unoccupied and needs a burrow dug or if there is room to make a new burrow
	*/
#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest();
#endif

	if (m_NoBurrows > (m_NoOccupiedBurrows + m_BurrowsUnderConstruction))
	{
#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest2();
#endif
			return 1; // There is a free burrow
	}
	if ((m_NoBurrows + m_BurrowsUnderConstruction < m_CarryingCapacity)) return 2;
	return 0;
}
//---------------------------------------------------------------------------

void Rabbit_Warren::Join( Rabbit_Base* a_rabbit )
{
	/** Join a warren. */
	m_InhabitantsList.push_back(a_rabbit);
	a_rabbit->SetWarren(this);
}
//---------------------------------------------------------------------------

void Rabbit_Warren::OccupyNewBurrow()
{
	m_NoOccupiedBurrows++;
	m_NoBurrows++;
	m_BurrowsUnderConstruction--;
}
//---------------------------------------------------------------------------

void Rabbit_Warren::OccupyBurrow()
{
	m_NoOccupiedBurrows++;
}
//---------------------------------------------------------------------------

void Rabbit_Warren::OccupyWarren( Rabbit_Adult* a_rabbit )
{
	m_BurrowsUnderConstruction++;
	a_rabbit->SetDigging( m_rabbitdiggingtime ); // Heavy soil has double digging time
	Join(a_rabbit);
}
//---------------------------------------------------------------------------

void Rabbit_Warren::JoinNOccupy( Rabbit_Adult* a_rabbit )
{
	/** Join a warren and occupy a burrow. */
#ifdef __RABBITDEBUG
			// Do some safety checks here.
			if ((m_NoBurrows <= m_NoOccupiedBurrows) && (m_NoBurrows!=0))
			{
				m_OurLandscape->Warn("Rabbit_Warren::JoinNOccupy","Attempt to occupy a burrow that is occupied or does not exist.");
				exit(1);
			}
#endif
#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest2();
#endif
	Join(a_rabbit);
	OccupyBurrow( );
	a_rabbit->SetDigging(-1);
	a_rabbit->SetSocialStatus( rabbit_socialstatus_subdominant );
	a_rabbit->SetHasBurrow( true );

#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest();
#endif
}
//---------------------------------------------------------------------------

void Rabbit_Warren::Leave( Rabbit_Base* a_rabbit )
{
	/**
	* A rabbit is leaving. It may be because they are dead or because they are moving warrens, or just leaving. 
	* The rabbit:rabbit interactions are dealt with by the rabbit objects communicating, but burrow occupancy needs to be sorted out here.
	* Both completed and burrows being dug need to be dealt with.
	*/
	int sz = (int) m_InhabitantsList.size();
	for (int i=0; i<sz; i++)
	{
		if (m_InhabitantsList[i]==a_rabbit) 
		{
			// Found the rabbit.
#ifdef __RABBITDEBUG
			// Do some safety checks here.
			if ( (a_rabbit->GetRabbitType() >= rob_Male ) && (a_rabbit->GetCurrentRState() != toRabbits_Die) )
			{
				if ((a_rabbit->GetMate()!=NULL) && (a_rabbit->GetCurrentStateNo() != -1))
				{
					m_OurLandscape->Warn("Rabbit_Warren::Leave","Attempt to leave a warren with a mate.");
					exit(1);
				}
				// Do some safety checks here.
				if ((m_InhabitantsList[i]->GetHasBurrow()) && (a_rabbit->GetMate() != NULL) && (a_rabbit->GetCurrentStateNo() != -1))
				{
					m_OurLandscape->Warn("Rabbit_Warren::Leave","Attempt to leave a warren with a burrow and mate.");
					exit(1);
				}
			}
#endif
			if (a_rabbit->GetRabbitType() > rob_Juvenile)
			{
				if ((a_rabbit->GetHasBurrow()) && (a_rabbit->GetMate() == NULL)) {
					// Single burrow occupier leaving
					m_NoOccupiedBurrows--;
					a_rabbit->SetHasBurrow( false );
				}
				if ((a_rabbit->GetDigging()!=-1) && (a_rabbit->GetMate() == NULL)) {
					// Single digger leaving
					m_BurrowsUnderConstruction--;
					a_rabbit->SetDigging( -1 );
				}
			}
			a_rabbit->SetWarren(NULL);
			// remove the rabbit from the warren list
			m_InhabitantsList.erase(m_InhabitantsList.begin() + i);
			break;
		}
	}
}
//---------------------------------------------------------------------------

void Rabbit_Warren::Mate(Rabbit_Adult* a_mate, RabbitObjectTypes rob_type)
{
	/**
	* Mates two rabbits and also takes care of deciding whether they need to dig a burrow, and sets the social status.
	*/
#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest();
#endif
	int sz = (int) m_InhabitantsList.size();
	for (int i=0; i<sz; i++)
	{
		Rabbit_Adult * p_rabAdult = dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[ i ]);
		if ((m_InhabitantsList[ i ]->GetRabbitType() == rob_type) && (p_rabAdult->GetMate() == NULL))
		{
			if (p_rabAdult->GetHasBurrow())
			{
#ifdef __RABBITDEBUG
				if (p_rabAdult->GetDigging() != -1)
				{
					int rubbish = 0;
				}
#endif
				a_mate->SetDigging( -1 );
				a_mate->SetMate( p_rabAdult );
				p_rabAdult->SetMate( a_mate );
				// If the rabbit already has a burrow then we have a free one now
				if (a_mate->GetHasBurrow()) {
					m_NoOccupiedBurrows--;
				}
				else a_mate->SetHasBurrow( true );
				a_mate->SetSocialStatus( rabbit_socialstatus_subdominant );
				p_rabAdult->SetSocialStatus( rabbit_socialstatus_subdominant );
				break;
			}
			else
				if (p_rabAdult->GetDigging() > -1)
			{
				a_mate->SetMate(dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i]));
				p_rabAdult->SetMate( a_mate );
				int digging = p_rabAdult->GetDigging() / 2; // We have help so half the time
				p_rabAdult->SetDigging( digging );
				a_mate->SetDigging(digging);
				a_mate->SetSocialStatus( rabbit_socialstatus_subordinate );
				p_rabAdult->SetSocialStatus( rabbit_socialstatus_subordinate );
				break;
			}
			// Else they are not interesting and we ignore them
		}
	}
#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest();
#endif
}
//---------------------------------------------------------------------------

void Rabbit_Warren::JoinNMate(Rabbit_Adult* a_mate, RabbitObjectTypes rob_type)
{
	/**
	* Loops through the list of warren inhabitants and looks for the target type rob_type. If found without a mate then ascertain if they have a burrow.
	* Social status will depend on whether one or both is available.
	*/
#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest();
#endif
	int sz = (int) m_InhabitantsList.size();
	for (int r=0; r<sz; r++)
	{
		Rabbit_Adult* p_rabAdult = dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[ r ]);
		if ((m_InhabitantsList[r]->GetRabbitType() == rob_type) && (p_rabAdult->GetMate() == NULL) )
		{
#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest();
#endif
	        if (p_rabAdult->GetHasBurrow())
			{
				a_mate->SetDigging(-1);
				a_mate->SetMate(dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[r]));
				a_mate->SetHasBurrow(true);
				a_mate->SetSocialStatus( rabbit_socialstatus_subdominant );
				p_rabAdult->SetSocialStatus( rabbit_socialstatus_subdominant );
				p_rabAdult->SetMate( a_mate );
				Join(a_mate);
				break;
			}
			else
				if (p_rabAdult->GetDigging() > -1)
			{
				a_mate->SetMate( p_rabAdult );
				a_mate->SetSocialStatus( rabbit_socialstatus_subordinate );
				p_rabAdult->SetSocialStatus( rabbit_socialstatus_subordinate );
				p_rabAdult->SetMate( a_mate );
				int digging = p_rabAdult->GetDigging();
				a_mate->SetDigging(digging);
				Join(a_mate);
				break;
			}

		}
	}
#ifdef __RABBITDEBUG
	this->DEBUG_InternalTest();
#endif
}
//---------------------------------------------------------------------------

int Rabbit_Warren::GetAllBreedingFemaleRabbits( void ) {
	int breeders = 0;
	int sz = (int)m_InhabitantsList.size();
	for (int i = 0; i < sz; i++) {
		if (m_InhabitantsList[ i ]->GetRabbitType() == rob_Female) {
			if (dynamic_cast<Rabbit_Female*>(m_InhabitantsList[ i ])->GetSocialStatus()>rabbit_socialstatus_subordinate) breeders++;
		}
	}
	return breeders;
}
//---------------------------------------------------------------------------

int Rabbit_Warren::GetAllBigFemaleRabbits( void ) {
	int biggies = 0;
	int sz = (int)m_InhabitantsList.size();
	for (int i = 0; i < sz; i++) {
		if (m_InhabitantsList[ i ]->GetRabbitType() == rob_Female) {
			if (m_InhabitantsList[ i ]->GetAge()>300) biggies++;
		}
	}
	return biggies;
}
//---------------------------------------------------------------------------

bool Rabbit_Warren::DEBUG_InternalTest()
{
	int Mburrs = 0;
	int Mmates = 0;
	int MBM = 0;
	int Fburrs = 0;
	int Fmates = 0;
	int FBM = 0;
	int siz = (int) m_InhabitantsList.size();
	int Mdigs = 0;
	int Fdigs = 0;
	for (int i = 0; i<siz; i++)
	{
		if (m_InhabitantsList[i]->GetCurrentStateNo() != -1)
		{
			if (m_InhabitantsList[i]->GetRabbitType() == rob_Male)
			{
				if (m_InhabitantsList[i]->GetHasBurrow()) Mburrs++; // No males with burrow
				if (m_InhabitantsList[i]->GetDigging()>-1) Mdigs++;  // No males digging
				if (dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i])->GetMate() != NULL) Mmates++; // No males with mates
				if ((dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i])->GetMate() != NULL) && (m_InhabitantsList[i]->GetHasBurrow())) MBM++; // No males with mates and burrows
			}
			if (m_InhabitantsList[i]->GetRabbitType() == rob_Female)
			{
				if (m_InhabitantsList[i]->GetHasBurrow()) Fburrs++;
				if (m_InhabitantsList[i]->GetDigging() > -1) Fdigs++;
				if (dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i])->GetMate() != NULL) Fmates++;
				if ((dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i])->GetMate() != NULL) && (m_InhabitantsList[i]->GetHasBurrow())) FBM++;
			}
		}
	}
	int alonemales = Mburrs-MBM;
	int alonefemales = Fburrs-FBM;
	if (MBM != FBM) 
	{
		m_OurLandscape->Warn("IsFreeBurrow","Inconsistent mates and burrows MBM != FBM.");
		exit(1);
	}
	if (MBM + alonemales + alonefemales > m_NoBurrows) 
	{
		m_OurLandscape->Warn("IsFreeBurrow","Too many burrows occupied.");
		exit(1);
	}
	if (MBM + alonemales + alonefemales != this->m_NoOccupiedBurrows) 
	{
		m_OurLandscape->Warn("IsFreeBurrow","Too many burrows occupied.");
		exit(1);
	}
	return true;
}
//---------------------------------------------------------------------------

bool Rabbit_Warren::DEBUG_InternalTest2()
{
	int Mburrs = 0;
	int Mmates = 0;
	int MBM = 0;
	int Fburrs = 0;
	int Fmates = 0;
	int FBM = 0;
	int siz = (int) m_InhabitantsList.size();
	for (int i=0; i<siz; i++)
	{
		if ( m_InhabitantsList[i]->GetRabbitType() == rob_Male)
		{
			if ( m_InhabitantsList[i]->GetHasBurrow()) Mburrs++;
			if ( dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i])->GetMate()!= NULL) Mmates++;
			if (( dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i])->GetMate()!= NULL) && (m_InhabitantsList[i]->GetHasBurrow())) MBM++;
		}
		if ( m_InhabitantsList[i]->GetRabbitType() == rob_Female)
		{
			if ( m_InhabitantsList[i]->GetHasBurrow()) Fburrs++;
			if ( dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i])->GetMate()!= NULL) Fmates++;
			if (( dynamic_cast<Rabbit_Adult*>(m_InhabitantsList[i])->GetMate()!= NULL) && (m_InhabitantsList[i]->GetHasBurrow())) FBM++;
		}
	}
	int alonemales = Mburrs-MBM;
	int alonefemales = Fburrs-FBM;
	if (MBM + alonemales + alonefemales >= m_NoBurrows) 
	{
		m_OurLandscape->Warn("IsFreeBurrow","Too many burrows occupied.");
		exit(1);
	}
	return true;
}
//---------------------------------------------------------------------------

