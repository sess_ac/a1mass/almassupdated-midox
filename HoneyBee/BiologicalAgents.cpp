/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file BiologicalAgents.cpp
    \brief <B>The main header file for biolobical agents</B>
    Version of Feb. 2020 \n
    By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "blitz/array.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../BatchALMaSS/ALMaSS_Random.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "BiologicalAgents.h"
#include "HoneyBee.h"
#include "HoneyBee_Colony.h"
#include "Hive.h"
//---------------------------------------------------------------------------------------
// Externals
extern MapErrorMsg *g_msg;

//---------------------------------------------------------------------------------------

//Weight used to convert stress to immune system
CfgFloat cfg_HoneyBeeStressToImmune("HONEY_BEE_STRESS_TO_IMMUNE",CFG_CUSTOM,0.1);
//Weight used to convert carring number of vorra to immune system
CfgFloat cfg_HoneyBeeVarroaToImmune("HONEY_BEE_Varroa_TO_IMMUNE",CFG_CUSTOM,0.1);
//Weight used to convert carring number of nosema to immune system
CfgFloat cfg_HoneyBeeNosemaToImmune("HONEY_BEE_NOSEMA_TO_IMMUNE",CFG_CUSTOM,0.1);
//First weight (b) used to control the dropping point for immune system reacting to virus number (b-sV)
CfgFloat cfg_HoneyBeeFirstWeightVirusToImmune("HONEY_BEE_FIRST_WEIGHT_VIRUS_TO_IMMUNE",CFG_CUSTOM,0.1);
//Second weight (s) used to control the dropping point for immune system reacting to virus number (b-sV)
CfgFloat cfg_HoneyBeeSecondWeightVirusToImmune("HONEY_BEE_SECOND_WEIGHT_VIRUS_TO_IMMUNE",CFG_CUSTOM,0.1);
//Increasing weight for of ABPV
CfgFloat cfg_HoneyBeeIncreasingWeightABPV("HONEY_BEE_INCREASING_WEIGHT_ABPV",CFG_CUSTOM,0.1);
//Weight for immune system to supressing virus increment of ABPV
CfgFloat cfg_HoneyBeeWeightImmuneToABPV("HONEY_BEE_WEIGHT_IMMUNE_TO_ABPV",CFG_CUSTOM,0.1);
//Increasing weight for of DWV
CfgFloat cfg_HoneyBeeIncreasingWeightDWV("HONEY_BEE_INCREASING_WEIGHT_DWV",CFG_CUSTOM,0.1);
//Weight for immune system to supressing virus increment of DWV
CfgFloat cfg_HoneyBeeWeightImmuneToDWV("HONEY_BEE_WEIGHT_IMMUNE_TO_DWV",CFG_CUSTOM,0.1);
//Minimun life length in days for Varroa
CfgFloat cfg_HoneyBeeMinLifeVarroa("HONEY_BEE_MIN_LIFE_VARROA", CFG_CUSTOM, 5);
//Maxmum life length in days for Varroa
CfgFloat cfg_HoneyBeeMaxLifeVarroa("HONEY_BEE_MAX_LIFE_VARROA", CFG_CUSTOM, 11);
//The probability a varroa is unfertile.
CfgFloat cfg_HoneyBeeProbVarroaUnfertile("HONEY_BEE_PROB_VARROA_UNFERTILE", CFG_CUSTOM, 0.3);
//The average female offspring of varroa for worker cell.
CfgFloat cfg_HoneyBeeAverageOffspringVarroaWorker("HONEY_BEE_AVERAGE_OFFSPRING_VARROA_WORKER", CFG_CUSTOM, 1.45);
//The average female offspring of varroa for drone cell.
CfgFloat cfg_HoneyBeeAverageOffspringVarroaDrone("HONEY_BEE_AVERAGE_OFFSPRING_VARROA_WORKER", CFG_CUSTOM, 3.9);
CfgFloat cfg_HoneyBeeProbaPupaInfectedDWV("HONEY_BEE_PROBA_PUPA_INFECTED_DWV",CFG_CUSTOM, 0.9);
CfgFloat cfg_Honey_Bee_ProbaPupaInfectedABP("HONEY_BEE_PROBA_PUPA_INFECTED_ABP",CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeProbaAdultInfectedDWV("HONEY_BEE_PROBA_ADULT_INFECTED_DWV",CFG_CUSTOM, 0.9);
CfgFloat cfg_HoneyBeeProbaAdultInfectedABP("HONEY_BEE_PROBA_ADULT_INFECTED_ABP",CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeNumDWVWhenInfected("HONEY_BEE_NUM_DWV_WHEN_INFECTED", CFG_CUSTOM, 100);
CfgFloat cfg_HoneyBeeNumABPWhenInfected("HONEY_BEE_NUM_ABP_WHEN_INFECTED", CFG_CUSTOM, 100);
extern CfgInt cfg_HoneyBeeStepSize;



void BiologicalAgents :: updateImmuneValue(){
    //for now we sum all the stress together
    double current_all_stress = m_my_host_bee->supplyFoodStressor() + m_my_host_bee->supplyTempeStressor() + m_my_host_bee->supplyTempeStressor() + m_my_host_bee->supplyDiseaseStressor();
    double current_all_virus = m_ABPV +m_DWV;

    double time_step = cfg_HoneyBeeStepSize.value();
    
    //see the details on the formal model
    m_immune_value += (cfg_HoneyBeeStressToImmune.value()*current_all_stress-(cfg_HoneyBeeVarroaToImmune.value()*m_Varroa+cfg_HoneyBeeNosemaToImmune.value()*(m_nosema_ceranae+m_nosema_apis)*m_immune_value)+(cfg_HoneyBeeFirstWeightVirusToImmune.value()+cfg_HoneyBeeSecondWeightVirusToImmune.value()*current_all_virus)*current_all_virus)*time_step;
}

void BiologicalAgents :: updateVirus(){
    //DWV
    m_DWV += (cfg_HoneyBeeIncreasingWeightDWV.value()-cfg_HoneyBeeWeightImmuneToDWV.value()*m_immune_value)*m_DWV*cfg_HoneyBeeStepSize.value();
    //ABP
    m_ABPV += (cfg_HoneyBeeIncreasingWeightABPV.value()-cfg_HoneyBeeWeightImmuneToABPV.value()*m_immune_value)*m_ABPV*cfg_HoneyBeeStepSize.value();
}

void BiologicalAgents :: updateNosema(){
    //when it is -1, it means there is no nosema
    //when it is larger than -1, we increase the age by one everyday
    if(m_nosema_apis>-1){
        m_nosema_apis++;
    }
    if(m_nosema_ceranae>-1){
        m_nosema_ceranae++;
    }
}

void BiologicalAgents :: updateVarroa(double a_age = 1){
    //we only update it when it is not in reproducting procedure
    if(!m_Varroa_reproduce_flag){
        m_Varroa_age+=a_age;
        //should it die? Now it is linear.
        if(g_rand_uni()<(m_Varroa_age-cfg_HoneyBeeMinLifeVarroa.value())/(cfg_HoneyBeeMaxLifeVarroa.value())){
            m_Varroa = -1;
            m_Varroa_age = -1;
        }

    }
}

void BiologicalAgents :: dropVarroaToAdult(HoneyBee_Worker* the_bee) {
    the_bee->m_MyBiologicalAgent->setVarroa(1, m_Varroa_age);
    if(m_DWV>0){
        if(g_rand_uni()<cfg_HoneyBeeProbaAdultInfectedDWV.value()){
            the_bee->m_MyBiologicalAgent->setDWV(cfg_HoneyBeeNumDWVWhenInfected.value()); //the number does not mean any thing
        }
    }
    if(m_ABPV>0){
        if(g_rand_uni()<cfg_HoneyBeeProbaAdultInfectedABP.value()){
            the_bee->m_MyBiologicalAgent->setABPV(cfg_HoneyBeeNumABPWhenInfected.value());
        }
    }
    m_Varroa -= 1;
    //there is no varroa left
    if(m_Varroa<=0){
        m_Varroa=-1;
        m_Varroa_age = -1;
    }
}

void BiologicalAgents :: dropVarroaToPupa(HoneyBee_WorkerPupa* the_bee) {
    the_bee->m_MyBiologicalAgent->setVarroa(1, m_Varroa_age);
    the_bee->m_MyBiologicalAgent->enableVarroaReproduction();
    if(m_DWV>0){
        if(g_rand_uni()<cfg_HoneyBeeProbaPupaInfectedDWV.value()){
            the_bee->m_MyBiologicalAgent->setDWV(cfg_HoneyBeeNumDWVWhenInfected.value()); //the number does not mean any thing
        }
    }
    if(m_ABPV>0){
        if(g_rand_uni()<cfg_Honey_Bee_ProbaPupaInfectedABP.value()){
            the_bee->m_MyBiologicalAgent->setABPV(cfg_HoneyBeeNumABPWhenInfected.value());
        }
    }
    m_Varroa = -1;
    m_Varroa_age = -1;
}

void BiologicalAgents :: reporduceVarroa(){
    if(m_Varroa_reproduce_flag){
        //unfertile, it will die
        if(g_rand_uni()<cfg_HoneyBeeProbVarroaUnfertile.value()){
            m_Varroa = -1;
            m_Varroa_age = -1;
            return;
        }
        else{
            int temp_box_num = m_my_host_bee->supplyBoxNum();
            int x = m_my_host_bee->Supply_m_Location_x();
            int y = m_my_host_bee->Supply_m_Location_y();
            int z = m_my_host_bee->Supply_m_Location_z();
            if(m_my_host_bee->getHive()->getCellType(temp_box_num,x,y,z)==CellType::Worker_Pupa){
                /** \todo we may need other distributions here. */
                m_Varroa = ceil((g_rand_uni()*2-1)*cfg_HoneyBeeAverageOffspringVarroaWorker.value()+cfg_HoneyBeeAverageOffspringVarroaWorker.value());
                m_Varroa_age = 0;
            }
            if(m_my_host_bee->getHive()->getCellType(temp_box_num,x,y,z)==CellType::Drone_Pupa){
                /** \todo we may need other distributions here. */
                m_Varroa = ceil((g_rand_uni()*2-1)*cfg_HoneyBeeAverageOffspringVarroaDrone.value()+cfg_HoneyBeeAverageOffspringVarroaDrone.value());
                m_Varroa_age = 0;
            }
        }
    }
}