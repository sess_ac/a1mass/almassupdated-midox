/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS doubleERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file BiologicalAgents.h
	\brief <B>The main header file for biological agents</B>
	Version of  Feb. 2020 \n
	By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------
#ifndef BiologicalAgentsH
#define BiologicalAgentsH
//---------------------------------------------------------------------------

//#include "../HoneyBee/HoneyBee.h"
class HoneyBee_Base;
class HoneyBee_Worker;
class HoneyBee_WorkerPupa;
/** \brief A class to hold an individual's virus infection, Varroa and Nosema load. */

class BiologicalAgents
{
	/**
	* Currently, two viruses are considered: Deformed Wing Virus (DWV)and Actute Bee Paralysis Virus (ABPV)
	* One type of Varroa and two types of Nosema are considered
	* We track the number for each
	*/
protected:
	/** \brief Pointer to my host bee. */
	HoneyBee_Base* m_my_host_bee;
	/** \brief Number of ABPV. */
	double m_ABPV;
	/** \brief Number of DWV */
	double m_DWV;
	/** \brief Days of Nosema apis */
	double m_nosema_apis;
	/** \brief Days of Nosema ceranae */
	double m_nosema_ceranae;
	/** \brief Number of Varroa */
	double m_Varroa;
	/** \brief Age of the Varroa */
	double m_Varroa_age;
	/** \brief Immune ability value */
	double m_immune_value;
	/** \brief Flat to show whether the Varroa is in reproducing stage. */
	bool m_Varroa_reproduce_flag;
	
public:
	/** \brief  Standard constructor for a health bee, so everythin is zero except that the immune ability value is one */
	BiologicalAgents(HoneyBee_Base* my_host_bee)
	: m_my_host_bee(my_host_bee), m_ABPV(0), m_DWV(0), m_nosema_apis(-1), m_nosema_ceranae(-1), m_Varroa(0), m_Varroa_reproduce_flag(false), m_Varroa_age(0), m_immune_value(1)
	{
	}
	/** \brief  Copy assignment constructor but without the host bee pointer*/
	BiologicalAgents& operator = (const BiologicalAgents& other) {
		// protect against self assignment
		if (this != &other)
		{
			m_ABPV = other.m_ABPV;
			m_DWV = other.m_DWV;
			m_nosema_apis = other.m_nosema_apis;
			m_nosema_ceranae = other.m_nosema_ceranae;
			m_Varroa = other.m_Varroa;
			m_immune_value = other.m_immune_value;
		}
		return *this;
	}
	/** \brief  Destructor */
	~BiologicalAgents(void) { int i=0; }
	/** \brief  Get ABPV value */
	double getABPV() { return m_ABPV; }
	/** \brief  Set ABPV value */
	void setABPV(double infection) { m_ABPV = infection; }
	/** \brief  Get DWV value */
	double getDWV() { return m_DWV; }
	/** \brief  Set DWV value */
	void setDWV(double infection) { m_DWV = infection; }
	/** \brief  Get NosemaA value */
	double getNosemaA() { return m_nosema_apis; }
	/** \brief  Set NosemaA value */
	void setNosemaA(double infection) { m_nosema_apis = infection; }
	/** \brief  Get NosemaC value */
	double getNosemaC() { return m_nosema_ceranae; }
	/** \brief  Set NosemaC value */
	void setNosemaC(double infection) { m_nosema_ceranae = infection; }
	/** \brief  Get Varroa value */
	double getVarroa() { return m_Varroa; }
	/** \brief  Set Varroa value */
	void setVarroa(double infection, int age) { m_Varroa = infection; m_Varroa_age=age;}
	/** \brief  Get immune ability value */
	double getImmune() { return m_immune_value; }
	/** \brief  Set immune ability value */
	void setImmune(double value) { m_immune_value = value; }
	/** \brief  Update the immune value for each step */
	void updateImmuneValue();
	/** \brief  Update virus number for each time step*/
	void updateVirus();
	/** \brief  Update Varroa to check whether it will die*/
	void updateVarroa(double a_age);
	/** \brief Drop Varroa from a host bee to another adult bee close by*/
	void dropVarroaToAdult(HoneyBee_Worker* the_bee);
	/** \brief Drop Varroa from a host bee to a pupa, then it will start a reproducing producer*/
	void dropVarroaToPupa(HoneyBee_WorkerPupa* the_bee);
	/** \brief Enable Varroa reproduction. */
	void enableVarroaReproduction() {m_Varroa_reproduce_flag = true;}
	/** \brief Return varroa is reproducing?. */
	bool isVarroaReproduction() {return m_Varroa_reproduce_flag;}
	/** \brief Varroa reproduction. This function will be called when a pupa hatches from the cell. The Varroa will also find new hosts. */
	void reporduceVarroa();
	/** \brief Get Varroa from others, e.g., contacting bees, brood cell*/
	void updateNosema();
	/** \brief  Update effects of carried virus for each time step*/
	void updateVirusEffect();
	/** \brief Supply the current immune strength value. */
	double supplyImmuneValue() {return m_immune_value;}
};

#endif
