/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee_Colony.h
    \brief <B>The main header file for population manager</B>
    Version of Feb 2020 \n
    By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/

//Using filed data (polygen ID list) for calibration
//#define __loadFieldResourceData

//---------------------------------------------------------------------------
#ifndef HoneyBeeColonyH
#define HoneyBeeColonyH
//---------------------------------------------------------------------------


//#include "Hive.h"

//#include "HoneyBee.h"
//---------------------------------------------------------------------------

class HoneyBee;
class HoneyBee_Worker;
class BiologicalAgents;
class HoneyBee_Colony;
class Hive;
class HiveThermodynamics;
enum class CellType;
enum class TaskType;
class BMP;
//------------------------------------------------------------------------------

class HoneyBee_Base;

/**
 * \brief Class for information about the pollen and nectar foraged, we use a single class for both nectar and pollen.
 * 
 */

struct PollenNectarInfo{
    /** \brief Used to indicate whether it is used for nectar (true) or pollen (false) */
    bool m_pollen_nectar_flag;
    /** \brief The amount of the foraged resource. */
    double m_amount;
    /** \brief The quality of the foraged resource. */
    double m_quality;
    /** \brief Pesticide concentration from pesticide application. */
    double* m_pesticideapp_concentration;
    /** \brief Pesticide concentration from seed coating. */
    double* m_seedcoating_concentration;
    /** \brief This is used to store the polygen id. */
    int m_poly_id;
};

//------------------------------------------------------------------------------
/**
\brief
Used for the population manager's list of honey bee life types
*/
enum TTypesOfHoneyBee
{
    ttohb_WorkerEgg = 0,
    ttohb_WorkerLarva,
    ttohb_WorkerPupa,
    ttohb_DroneEgg,
    ttohb_DroneLarva,
    ttohb_DronePupa,
    ttohb_Worker,
    ttohb_Forager,
    ttohb_Scout,
    ttohb_Drone,
    ttohb_Queen
};


/** \brief Used for the states of the colony. */
enum StatesOfColony{
    ttoc_Winter,
    ttoc_Summer,
    ttoc_Low_Comb,
    ttoc_Low_Pollen,
    ttoc_Low_Nectar
};

/**
\brief
Used for creation of a new HoneyBee object
*/
class struct_HoneyBee
{
 public:
  /** \brief x-coord */
  int x;
  /** \brief y-coord */
  int y;
  /** \brief z-coord */
  int z;
  /** \brief box num */
  int box_num;
  /** \brief age in days */
  int age;
  /** \brief Landscape pointer */
  Landscape* L;
  /** \brief HoneyBee_Colony pointer */
  HoneyBee_Colony * BPM;
  /** \brief Virus information pointer */
  BiologicalAgents * BA;
  /** \brief Variable to record sugar in the stomache */
  double m_sugar;
  /** \brief Variable to record pollen in the stomache */
  double m_pollen;
  /** \brief Variable to record the development time*/
  double m_developmentTime;
  /** \brief Variable to record whether it is in hive*/
  bool m_inhive;
};

/**
\brief
Used for tranfering foraged nectar for another bee to store it in the hive
*/
class struct_Cache_Nectar
{
 public:
  /** \brief The amount for nectar */
  float amount;
  /** \brief The percentage of surgar in the amount*/
  float sugar_concentration;
  /** \brief The amount for concentration from pesticide application */
  float pes_application;
  /** \brief The type of pesticide application */
  int pes_application_type;
  /** \brief The amount for concentration from seed coating */
  float pes_seed_coating;
  /** \brief The type for pesticide seed coating */
  float pes_seed_coating_type;
};

/**
\brief
Used for tranfering foraged pollen for another bee to store it in the hive
*/
class struct_Cache_Pollen
{
 public:
  /** \brief The amount for pollen */
  float amount;
  /** \brief The percentage of surgar in the amount*/
  float pes_application;
  /** \brief The type of pesticide application */
  int pes_application_type;
  /** \brief The amount for concentration from seed coating */
  float pes_seed_coating;
  /** \brief The type for pesticide seed coating */
  float pes_seed_coating_type;
};

/**
\brief
Used for polygen information with pollen and nectar
*/
class struct_Resource_PN
{
 public:
  /** \brief Poly ID */
  int poly_id;
  /** \brief Resource quality */
  PollenNectarData quality;
  /** \brief Resource amount */
  int amount;
  /** \brief Distance between the hive and polygon center */
  int distance;
  /** \brief Flag for indicating whether it has been scouted */
  bool scouted_flag = false;
  /** \brief Index in the known resource list when the this place is souted. */
  int index_know;
  /** \brief Variable to store the area of the pollen */
  double poly_area;
  double gain; // the gain is defined as quality / quantity / distance
  int visited_time = 0;
};

/**
\brief
Used for known polygen for nectar and pollen
*/
class struct_Known_Resource
{
 public:
  /** \brief Poly ID */
  int poly_id;
  /** \brief Resource direction index */
  int direction_index;
  /** \brief The index to the postion in resource array */
  int list_index;
  /** \brief This is used to recored the visited time. */
  int count_visited;
  /** \brief This is used to store the distance between the hive and resource place. */
  double distance;
  /** \brief This is used to store the area of the pollen. */
  double size;
  /** \brief This is used to store the flower info of the polygon. */
  PollenNectarData quality;
  /** \brief This is used to store the amount of flower resrouce of the polygon. */
  double amount;
  /** \brief This is used to store the gain of the flower resource. */
  double gain; // the gain is defined as quality / quantity / distance
  /** \brief Variable to store the area of the pollen */
  double poly_area;
  int resoure_type; //0 nectar, 1 pollen
};

/**
\brief
The class to handle all predator population related matters
*/
class HoneyBee_Colony : public Population_Manager
{
    friend BMP;
private:
    /** \brief This is used to track the number of foraged nectar that needs to be stored in the hive.*/
    int m_num_foraged_nectar;
    /** \brief This is used to track the number of bees that are out for guarding. */
    int m_current_num_bees_out_guarding;
    /** \brief This is used to track the maximum number of bees for guarding. */
    int m_max_num_bees_out_guarding;
    /** \brief This is used to track the number of bees that are out for pollen. */
    int m_num_bees_out_pollen;
    /** \brief This is used to store the maximum number of bees out for pollen. */
    int m_max_num_bees_out_pollen;
    /** \brief This is used to track the number of bees that are out for nectar. */
    int m_num_bees_out_nectar;
    /** \brief This is used to store the maximum number of bees out for nectar. */
    int m_max_num_bees_out_nectar;
    /** \brief This is used to tract the number of bees that are in for receiving nectar. */
    int m_num_bees_in_receiving_nectar;
    /** \brief This is used to assign a unique ID to a bee object. */
    int currentID;
    /** \brief This is used to track the step number in a day. It is resetted to 0 at 0am. */
    int m_step_num_in_a_day;
    /** \brief This is used to track the step number in an hour. It is used to calculation the hour in a day. */
    int m_step_num_in_a_hour;
    /** \brief This is used to track the hour in a day (0 to 23) */
    int m_hour_in_a_day;
    /** \brief This is used to record the maximum step number in a day. */
    int m_max_step_in_a_day;
    /** \brief The percentage of a time step in an one day time, this used to calculate how much sugar and pollen that a bee needs for a time step. */
    double m_percentage_timestep_in_a_day;
    /** \brief Indicator for the current state of the colony. */
    StatesOfColony m_current_colony_state;
    /** \brief Vector to store the food requirement per day for worker larva, royall jelly, pollen, sugar */
    std::vector<std::vector<double>> m_food_day_worker_larva;
    /** \brief Vector to store the food requirement per day for drone larva, royall jelly, pollen, sugar. */
    std::vector<std::vector<double>> m_food_day_drone_larva;
    /** \brief Vector for storing the wax and hpg produced by one bee in a day. */
    std::vector<std::vector<double>> m_worker_HPG_in_day;
    /** \brief Array to store the priorities for different activities, this will be loaded in the activity priorities file. */
    blitz::Array<int, 2> m_activities_priorities;
    /** \brief Array to store the temperature increment for each activity. */
    blitz::Array<int, 1> m_activities_temperature_incre;
    /** \brief Array to store the priorities for different activities which are weighted by the required numbers of tasks in each time step. */
    blitz::Array<int, 2> m_current_weighted_activities_priorities;
    /** \brief Variable to store the number of activities in the activity priorities file, this will be loaded in the priorites file.*/
    int num_activities;
    /** \brief Variable to store maximum age in days in the activities priorities file, this will be loaded in the priorities file.*/
    int days_activities_priorities;
    /** \brief Variable to record whether guarding is required for the hive*/
    bool m_guarding_required;
    /** \brief This is an array to track the metabolic rates (sugar) of worker bees for different activities. The maximum number is limited to 30 for now. Unit is mg/m*/
    blitz::Array<double, 1>   m_metabolic_rates_sugar_worker;
    /** \brief This is an array to track the metabolic rates (pollen) of worker bees for different activities.  Unit is mg per day*/
    blitz::Array<double, 1>   m_metabolic_rates_pollen_worker;
    /** \brief This is an array to track the required time for different activities. Unit is mg per day*/
    blitz::Array<double, 1>   m_required_time_activities;
    /** \brief Variable to record struct_Resource_PN for the polygen with availabe pollen*/
    vector<vector<struct_Resource_PN>> m_available_pollen_poly;
    /** \brief Variable to record struct_Resource_PN for the polygen with availabe nectar*/
    vector<vector<struct_Resource_PN>> m_available_nectar_poly;
    /** \brief Variable to record the scouted nectar resource. */
    vector<struct_Known_Resource> m_known_nectar_pool;
    /** \brief Variable to record the scouted nectar resource to pass to another bee. */
    vector<struct_Known_Resource> m_nectar_to_another_bee_pool;
    /** \brief Variable to record the scouted nectar resource. */
    vector<struct_Known_Resource> m_known_pollen_pool;
    /** \brief Variable to record the angle between each polygen and the hive*/
    vector<int> m_angle_hive_poly;
    /** \brief Variable to record the distance between each polygen and the hive*/
    vector<int> m_dis_hive_poly;
    /** \brief Pointer to the hive. */
    Hive * m_hive;
    /** \brief Pointer to the BMP object. */
    BMP* m_my_BMP;
    /** \brief Variable for inteval in degrees for tracking resource*/
    int m_resource_scale_degree;
    /** \brief Variable to track how many cold days (defined by cfg_HoneyBeeTemperatureLimitToDefineColdDays) in continuos.*/
    int m_cold_days_in_a_row;
    /** \brief Flag to show whether all the bees finish gathering together in the winter time. */
    bool m_flag_gathering_done;
    /** \brief Flag to show whether it is possible to go out for foraging. This is recalculated every hour. */
    bool m_foraging_flag;
    /** \brief This is used to track the foraging hours in each day. */
    int m_foraging_hours_in_a_day;
    /** \brief This is used to store the visited polygons' resource total amount.*/
    std::map<int, double> m_visited_poly_nectar_amount; 
    std::map<int, double> m_visited_poly_pollen_amount;



    /** \brief The following variables are used for calibrating foraging model. */
    // track the number of foragers for nectar per day
    int m_num_foragers_nectar_daily;
    // trach the number of foragers for pollen per day
    int m_num_foragers_pollen_daily;
    // the maximum number of foragers for nectar per day
    int m_num_max_foragers_nectar_daily;
    // the maximum number of foragers for pollen per day
    int m_num_max_foragers_pollen_daily;
    // the accumulated weight of foraged nectar per day
    double m_accumulated_nectar_daily;
    // the accumulated weight of foraged sugar per day
    double m_accumulated_sugar_daily;
    // the accumulated weight of foraged pollen per day
    double m_accumulated_pollen_daily;
    // the accumulated nectar foraging time per day
    int m_accumulated_nectar_foraging;
    // the accumulated successful trip for nectar
    int m_succ_trip_num_nectar;
    // the accumulated failed trip for nectar
    int m_fail_trip_num_nectar;
    // the accumulated successful trip for pollen
    int m_succ_trip_num_pollen;
    // the accumulated failed trip for pollen
    int m_fail_trip_num_pollen;  

protected:
    /** \brief  Things to do before anything else at the start of a timestep  */
    virtual void DoFirst();
    /** \brief Things to do before the Step */
    virtual void DoBefore(){}
    /** \brief Things to do before the EndStep */
    virtual void DoAfter(){}
    /** \brief Things to do after the EndStep */
    virtual void DoLast();

public:
    #ifdef __loadFieldResourceData
    std::vector <int> m_field_data_poly;
    void loadFieldData();
    #endif
    /** \brief Pointer to hive thermodynamics. */
    HiveThermodynamics* m_our_thermodynamics; 
    // Methods
    /** \brief HoneyBee_Colony Constructor */
    HoneyBee_Colony(Landscape* L);
    /** \brief HoneyBee_Colony Destructor */
    virtual ~HoneyBee_Colony (void);
    /** \brief Supply the position of the hive in the landscape*/
    int supplyHivePositionX();
    int supplyHivePositionY();
    /** \brief Method for reading requirement files for worker larva*/
    void readWorkerLarvaRequirement(string inputfile, int number_col, int number_row);
    /** \brief Method for reading requirement files for drone larva*/
    void readDroneLarvaRequirement(string inputfile, int number_col, int number_row);
    /** \brief Method for reading activities priorities file. */
    void readActivitiesPrioritiesFile(string inputfile);
    /** \brief Method for reading activities requirement file. */
    void readActivitiesRequirementFile(string inputfile);
    /** \brief Method for reading activity temperature increment file. */
    void readActivitiesTempeIncreFile(string inputfile);
    /** \brief Method for reading HPG information file. */
    void readHPGFile(string inputfile);
    /** \brief Function to reininitalized all the available nectar and pollen polygen refs*/
    void reinitializeAllAvailablePNRefs(void);
    /** \brief Function to update all the recorded available nectar and pollen polygen refs.*/
    void updateAvailablePNefs(void);
    /** \brief Sort resource vectors for scouting, for now it is only based on the distance between the hive and resource */
    void sortBothResource(void);
    /** \brief Comparison function for sorting resource */
    static bool sortResourceComparisonFunction(struct_Resource_PN a, struct_Resource_PN b);
    /** \brief Function to update the angle and distance between each polygen and the hive*/
    void calculateAngDisHivePoly(void);
    /** \brief Function is callsed when a bee goes out for scouting a resource, it moves available polyid to known place, then a bee is able to forage in the place. The return value is the index of the palce in the known resource list, -2, nothing found, -1, find a new place, other nonnegative values is the index of the know place list. The fourth argument is the type of the resource(1: nectar; 2: pollen)*/
    bool supplyOneAvailableResourcePlace(int angle, HoneyBee_Worker* the_bee, struct_Known_Resource* known_pointer, int resource_type);
    /** \brief Return the hive pointer. */
    Hive* getHive() {return m_hive;}
    /** \brief Function to generate an ID for a bee object. */
    int nextID() {return currentID++; }
    /** \brief This method is used to refill the food requirement for each bee at the end of each day. */
    void eatFoodAllAdult();    
    /** \brief Function to set the cell type when using template function CreatObject() to add bee. */
    void setHiveInfoforAdd(Hive* hive, int ob_type, int a_box_num, int x, int y, int z, int cellType);
    /** \brief Function to set the bee pointer for a cell when using template function CreatObject() to add a bee. */
    void setBeePointforAdd(Hive* hive, int ob_type, int a_box_num, int x, int y, int z, HoneyBee_Base* a_bee);
    /** \brief Function to add ages for all the bees, this is not used for now.*/
    void addAgesAll();
    /** \brief Step function when a day is finished since we use smaller step size for honey bee. */
    void dayStep();
    /** \brief Function to put all the bees to next stage, this is not used for now. */
    void nextStageAll();
    /** \brief Supply the step counting in a day. */
    int supplyStepNumInaDay(){return m_step_num_in_a_day;}
    /** \brief Supply the maximum number of steps in a day. */
    int supplyMaxStepNumInaDay(){return m_max_step_in_a_day;}
    /** \brief Is it the last step in a day. */
    bool isLastStepInaDay(){return m_step_num_in_a_day==m_max_step_in_a_day;}
    /** \brief Is it the first step in a day. */
    bool isFirstStepInaDay(){return m_step_num_in_a_day==1;}
    /** \brief Set the step counting in a day. */
    void setStepNumInaDay(int val){m_step_num_in_a_day=val;}
    /** \brief Increase the step counting in a day by one. */
    void increaseStepNumInaDay(){m_step_num_in_a_day++;}
    /** \brief Supple the m_percentage_timestep_in_a_day. */
    double supplyPercentageTimestepInaDay(){return m_percentage_timestep_in_a_day;}
    /** \brief Return whether the colony requires guarding. */
    bool isGuardingRequired(){return m_guarding_required;}
    /** \brief Supply required sugar for the given activity. */
    double supplySugarForActivity(TaskType a_task);
    /** \brief Supply required pollen for the given activity. */
    double supplyPollenForActivity(TaskType a_task);
    /** \brief Supply required time the given activity. */
    double supplyTimeForActivity(TaskType a_task);
    /** \brief Supply temperature increment for the given activity. */
    double supplyTempeIncreActivity(TaskType a_task);
    /** \brief Supply the number of known place for foraging nectar. */
    int supplyKnownNumOfPlaceForNectar(){return m_known_nectar_pool.size();}
    /** \brief Supply the number of known place for foraging pollen. */
    int supplyKnownNumOfPlaceForPollen(){return m_known_pollen_pool.size();}
    /** \brief Supply task prorities for a worker bee at the given age (starting tha day it becomes an adult). */
    blitz::Array<int, 1> supplyTaskProritesAge(int development_age);
    /** \brief Supply number of bees for guarding. */
    int supplyGuardingNum(){return m_current_num_bees_out_guarding;}
    /** \brief Increase number of bees for guarding. */
    void increGuardingNum(){m_current_num_bees_out_guarding++;}
    /** \brief Decrease number of bees for guarding. */
    void decreaseGuardingNum(){m_current_num_bees_out_guarding--;}
    /** \brief Check whether we have enough guarders. */
    bool isEnoughGuarder(){return m_current_num_bees_out_guarding>m_max_num_bees_out_guarding;}
    /** \brief Get the ratio between the current guarder and the maximum guarder. */
    double supplyRatioGuarder(){return double(m_current_num_bees_out_guarding)/double(m_max_num_bees_out_guarding);}
    /** \brief Supply whether more foragers are needed for nectar. */
    bool isEnoughForagerForNectar(){if(m_max_num_bees_out_nectar>=1) return m_num_bees_out_nectar>=m_max_num_bees_out_nectar; return true;}
    /** \brief Supply whether more foragers are needed for pollen. */
    bool isEnoughForagerForPollen(){if(m_max_num_bees_out_pollen>=1) return m_num_bees_out_pollen>=m_max_num_bees_out_pollen; return true;}
    /** \brief Increase the number of bees out for nectar. */
    void increaseForagerForNectarNumber(){m_num_bees_out_nectar++;}
    /** \brief Decrease the number of bees out for nectar. */
    void decreaseForagerForNectarNumber(){;}//{m_num_bees_out_nectar--;}
    /** \brief Increase the number of bees out for pollen. */
    void increaseForagerForPollenNumber(){m_num_bees_out_pollen++;}
    /** \brief Decrease the number of bees out for nectar. */
    void decreaseForagerForPollenNumber(){;}//{m_num_bees_out_pollen--;}
    /** \brief Supply the ratio between current foragers for nectar and the maximum allowed number. */
    double supplyRatioForagerForNectar(){return double(m_num_bees_out_nectar)/double(m_max_num_bees_out_nectar);}
    /** \brief Supply the ratio between current foragers for pollent and the maximum allowed number. */
    double supplyRatioForagerForPollen(){return double(m_num_bees_out_pollen)/double(m_max_num_bees_out_pollen);}
    /** \brief Increase the foraged nectar number. */
    void increaseForagedNectarNumber(){m_num_foraged_nectar++;}
    /** \brief Increase the foraged nectar number. */
    void decreaseForagedNectarNumber(){m_num_foraged_nectar--;}
    /** \brief Supply the number of foraged nectar. */
    int supplyForagedNectarNumber(){return m_num_foraged_nectar;}
    /** \brief Add one foraged nectar resource to anther bee.*/
    void addOneForagedNectarPlace(struct_Known_Resource a_nectar_place){m_nectar_to_another_bee_pool.push_back(a_nectar_place);}
    /** \brief Pass one foraged nectar resource to anther bee.*/
    struct_Known_Resource passOneForageNectarPlace(){
        struct_Known_Resource a_place;
        a_place.poly_id=-1;
        if(m_nectar_to_another_bee_pool.size()>0){
            a_place = m_nectar_to_another_bee_pool.back();
            m_nectar_to_another_bee_pool.pop_back();
        }
        return a_place;
    }
    /** \brief Supply the first polygen id for foraging nectar. */
    int supplyPolygenIDforNectar(){
        if(m_known_nectar_pool.size()>0){
            return m_known_nectar_pool.at(0).poly_id;
        }
        else{
            return -1;//there is no nectar available
        }
    }
    /** \brief Move a place to known nectar place list if it does exist yet. */
    void moveAvailableNectarToKnownOrUpdate(struct_Known_Resource a_place_to_add);
    /** \brief Move a place to known pollen place list if it does exist yet. */
    void moveAvailablePollenToKnownOrUpdate(struct_Known_Resource a_place_to_add);
    /** \brief Return a random nectar place for scouting. */
    struct_Known_Resource supplyKnownNectarPlace(){return m_known_nectar_pool.at(random(m_known_nectar_pool.size()));}
    /** \brief Return a random pollen place for scouting. */
    struct_Known_Resource supplyKnownPollenPlace(){return m_known_pollen_pool.at(random(m_known_pollen_pool.size()));}
    /** \brief Supply food requirement for a worker larva, first argument: type 0: royal jelly 1: pollen, 2: sugar. */
    double supplyFoodReqWokerLarva(int type, int day){return m_food_day_worker_larva.at(type).at(day);}
    /** \brief Supply food requirement for a drone larva, first argument: type 0: royal jelly 1: pollen, 2: sugar. */
    double supplyFoodReqDroneLarva(int type, int day){return m_food_day_drone_larva.at(type).at(day);}
    /** \brief Supply wax amount generated on a specific age of an adult bee in one day. */
    double supplyWaxOneDay(int day){
        if(day>=m_worker_HPG_in_day.size()-1){
            return m_worker_HPG_in_day.at(0).at(m_worker_HPG_in_day.size()-1);
        }
        else{
            return m_worker_HPG_in_day.at(0).at(day);
        }
    }
    /** \brief Supply royal jelly amount generated on a specific age of an adult bee in one day. */
    double supplyJellyOneDay(int day){
        if(day>=m_worker_HPG_in_day.size()-1){
            return m_worker_HPG_in_day.at(1).at(m_worker_HPG_in_day.size()-1);
        }
        else{
            return m_worker_HPG_in_day.at(1).at(day);
        }
    }
    /** \brief Return whether the bee should put debris in hive or not. */
    bool shouldBeePutDebrisInHive();

    //Below are the template functions
    /** \brief Method used to return whether a bee is in the hive or not, mainly used for displaying the bees in the landscape. */
    template <class T>
    bool isInHive(int ob_type, int j)
    {
        return dynamic_cast<T*>(SupplyAnimalPtr(ob_type,j))->isInHive();
    }

    /** \brief Method for creating a new individual HoneyBee, template based. When the bee grows up to the next life stage, the first augment is used. When it is a new created one, the second augment is used. */
    template <class Tfrom, class Tto>
    void CreatObject(TAnimal* animal=NULL, struct_HoneyBee* a_s_honeybee=NULL)
    {
        Tto* destination;
        Tfrom* source;
        int ob_type=Tto::myID;
        int source_type=Tfrom::myID;


        int temp_box_num, x, y, z;
        if(animal){
            source=dynamic_cast<Tfrom*>(animal);
            temp_box_num = source->supplyBoxNum();
            x = source->Supply_m_Location_x();
            y = source->Supply_m_Location_y();
            z = source->Supply_m_Location_z();
        }
        else{
            temp_box_num = a_s_honeybee->box_num;
            x = a_s_honeybee->x;
            y = a_s_honeybee->y;
            z = a_s_honeybee->z;
        }
        
        if (SupplyListSize(ob_type)>GetLiveArraySize(ob_type))
        {
            //cout << "Reusing: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
            destination = dynamic_cast<Tto*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)));
            setBeePointforAdd(m_hive, ob_type,temp_box_num, x, y, z, dynamic_cast<HoneyBee_Base*>(destination));
            destination->ReInit(temp_box_num, x,y,z,m_TheLandscape, this);
            //destination->SetAge(dynamic_cast<Tfrom*>(animal)->GetAge());
            if(animal){
                destination->CopyHBdata(source);
            }
            else{
                destination->FillHBdata(a_s_honeybee);
            }
            IncLiveArraySize(ob_type);
        }
        else
        {
            //      cout << "Adding: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
            TAnimal* newAnimal = new Tto(temp_box_num,x,y,z,m_TheLandscape, this);
            destination = dynamic_cast<Tto*>(newAnimal);
            setBeePointforAdd(m_hive, ob_type,temp_box_num, x, y, z, dynamic_cast<HoneyBee_Base*>(destination));
            //dynamic_cast<Tto*>(newAnimal)->SetAge(dynamic_cast<Tfrom*>(animal)->GetAge());
            if(animal){
                destination->CopyHBdata(source);
            }
            else{
                destination->FillHBdata(a_s_honeybee);
            }
            PushIndividual(ob_type, newAnimal);
            //delete newAnimal;
            IncLiveArraySize(ob_type);
        }
        if (destination->isInHive()){
            setHiveInfoforAdd(m_hive, ob_type, temp_box_num, x, y, z, (int)Tto::cellType);
        }            
    }
    /** \brief Set the flag for gathering together. **/
    void setGatheringFlag(bool a_flag){m_flag_gathering_done=a_flag;}

    /** \brief Below are the functions for saving result. */
    virtual void TheReallyBigOutputProbe();

    /** \brief Supply the curret hour for today. */
    int supplyHourToday(void) {return m_hour_in_a_day;}
    /** \brief Update foraging flag. */
    void updateForagingFlag(void);

    /** \brief Update the resource info in the known lists. */
    void updateResourceKnownPlace(void);

    /** \brief Update the visited resource. */
    void updateVisitedResource(void);

    /** \brief Remove the foraged flower resoure from the polygon. */
    void deduceForagedNectar(int angle, int index, double amount) {m_available_nectar_poly.at(angle).at(index).amount-=amount;}
    void deduceForagedPollen(int angle, int index, double amount) {m_available_pollen_poly.at(angle).at(index).amount-=amount;}
    double supplyAvailableNectarPoly(int angle, int index) {return m_available_nectar_poly.at(angle).at(index).amount;}
    double supplyAvailablePollenPoly(int angle, int index) {return m_available_pollen_poly.at(angle).at(index).amount;}

    /** \brief Supply the foraging flag*/
    bool supplyForagingFlag() {return m_foraging_flag;}

    //the functions below are used for the calibration of the foraging model
    void increaseForagerNumNectar(void) {m_num_foragers_nectar_daily+=1;}
    void decreaseForagerNumNectar(void) {m_num_foragers_nectar_daily-=1;}
    void increaseForagerNumPollen(void) {m_num_foragers_pollen_daily+=1;}
    void decreaseForagerNumPollen(void) {m_num_foragers_pollen_daily-=1;}

    bool isEnoughForagerNectar(void) {return m_num_foragers_nectar_daily < 5000;}
    bool isEnoughForagerPollen(void) {return m_num_foragers_pollen_daily < 1000;}
    void addForagedNectar(double a_w_nectar) {m_accumulated_nectar_daily+=a_w_nectar;}
    void addForagedSugar(double a_w_sugar) {m_accumulated_sugar_daily+=a_w_sugar;}
    void addForagedPollen(double a_w_pollen) {m_accumulated_pollen_daily+=a_w_pollen;}
    void calForagerNum(void);
    void addSuccTripNectar(void){m_succ_trip_num_nectar++;}
    void addFailTripNectar(void){m_fail_trip_num_nectar++;}
    void addSuccTripPollen(void){m_succ_trip_num_pollen++;}
    void addFailTripPollen(void){m_fail_trip_num_pollen++;}
    void getDailyTotalResourceAmount(double& nectar, double& sugar, double& pollen);
    void addForagingNectarActivity(void) {m_accumulated_nectar_foraging++; }
};

#endif
