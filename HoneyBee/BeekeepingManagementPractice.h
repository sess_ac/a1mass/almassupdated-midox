/*
*******************************************************************************************************
Copyright (c) 2019, Christopher John Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file BeekeepingManagementPratice.h
    \brief <B>The main source code for hive management</B>
    Version of Feb. 2020 \n
    By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/
#ifndef BMP_H
#define BMP_H

class Hive;
class HoneyBee_Colony;

typedef enum{
    mt_KillVarroa,
    mt_AddHoneyFrame,
    mt_ReplaceOneFrame
} ManagementType;



class BMP{
private:
	Hive* m_hive;
    HoneyBee_Colony* m_population_manager;
    /** \brief Vector to store the event scheduled for each day in a year. */
    std::vector<ManagementType> m_management_queue[365];
    /** \brief Vector to store the frame ID when a event requires a frame id. */
    std::vector<int> m_frameid_for_management_queue[365];

public:
	BMP(Hive * thehive, HoneyBee_Colony* a_population_manager);
    ~BMP(){std::cout<<"~BMP";};
	void readEvents();
    /** \brief Apply management. */
    void applyManagement();
    /** \brief To kill the varroa. */
    void killVarroa();
    /** \brief To add one frame with honey. */
    void addHoneyFrame(int box_num, int a_frame_id);
    /** \brief To replace one frame with empty frame. */
    void replaceOneFrame(int box_num, int a_frame_id);
    /** \brief To swp two frames. */
    void swapTwoFrame(int box_num_one, int a_frame_id_one, int box_num_two, int a_frame_id_two);
    /** \brief Parse job types. */
    ManagementType parseJobs(std::string task);
};
#endif