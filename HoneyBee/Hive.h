/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Hive.h
   \brief <B>The main header file for hive management</B>
   Version of Feb. 2020 \n
   By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/

#ifndef HIVE_H
#define HIVE_H

/**
 * \brief Simple enum class for indicating the side of a frame.
 */
enum class FrameSide {Front=0, Back=1};


/**
 * \brief Enum class CellType used to show what is in a {Empty=0, Nectar, Honey, Pollen, Worker_Egg, Worker_Pupa, Worker_Larva};  
 */
enum class CellType
{
   NotPresent,
   Empty,
   Nectar,
   Honey,
   Pollen,
   MarkedNectar, //5
   MarkedPollen,
   Worker_Egg,
   Worker_Egg_Layed,
   Worker_Larva,
   Worker_Pupa, //10
   Worker, //usually, this is not used since an adult bee usually not stay in a cell unless they are doing something in the ce.
   Drone_Egg,
   Drone_Egg_Layed,
   Drone_Larva,
   Drone_Pupa, //15
   Drone, //simillar as worker
   Queen, //simillar as worker and drone
   DeadBody
};

/**
 * \brief Enum class for status of cell related tasks. 
 */
enum class TaskStatus{
   Null, // 0, nothing to do for this cell
   Demanding, //no worker bee is doing the work
   Ongoing,
   Finished
};

/**
 * \brief Enum class for bee tasks, it is related to a cell
 */
enum class TaskType : int{
   Null, // 0, nothing to do, for cell without any need
   CappingBrood,
   FeedingBrood,
   WarmingUp,
   DepositingNectar,
   RipeningNectar,//5
   CappingHoney,
   DepositingPollen,
   BuildingCell,
   CoolingBrood,
   Guarding,//10
   RemovingDebris,
   ForagingNectar,
   ForagingPollen,
   CleaningCell,
   MovingAround,
   UncappingHoney,//
   Scouting,
   Count //This is a special one to count the number of elements of this enum class   
};

/**
 * \brief This is a global function to convert a enum class to an unsigned int, which can be used to index a array.
 */
template <typename enumclass>
constexpr typename std::underlying_type<enumclass>::type EnumClassAsIndex(enumclass e) {
    return static_cast<typename std::underlying_type<enumclass>::type>(e);
}

/**
 * \brief Simple class used to store the cell index in 3D.
 */
struct Coords3D {
   int x, y, z;
   Coords3D(int a=0,int b=0,int c=0){
      x=a;
      y=b;
      z=c;
   }
};

/**
 * \brief Simple class for pollen infomation which is used for return the amount and type of pollen consummed from the hive.
 */
struct PollenInfo {
   int pollen_type;
   int amount;
};

/**
 * \brief Simple class for box information.
 */
 
struct BoxInfo {
   int dim_x;
   int dim_y;
   int dim_z;
   int box_num;
   bool in_use_flag;
   bool queen_excluder_flag;
};

class HoneyBee_Base;
class HoneyBee_Colony;
class HoneyBee_WorkerEgg;
class HoneyBee_WorkerLarva;
class HoneyBee_WorkerPupa;
class HoneyBee_Worker;
class HiveThermodynamics;


//---------------------------------------------------------------------------
//                      Hive Class
//---------------------------------------------------------------------------
class Hive
{
private:
   /** \brief Variable to store the allowed maximum number of boxes for the colony. */
   int m_num_of_allowed_box;
   /** \brief Variable to store the number of current used boxes. We assume that all the boxes have the same dimension. */
   int m_num_of_used_box;
   /** \brief Array to store the box information for the colony */
   BoxInfo* m_box_info;
   /** \brief These are the maximum value (starting from 1) for indexing cells in the hive. \note This is the dimension for the first bottom box. */
   int xrange, yrange, zrange;
   /** \brief There two are used to record the postion of the hive in the landscape. */
   int m_hive_position_x, m_hive_position_y;
   /** \brief Pointer to the population manager.*/
   HoneyBee_Colony* m_our_population_manager;
   /** \brief Pointer to the landscape. */
   Landscape* m_TheLandscape;
   /** \brief This is used to track the nectar resource in each cell, four-deimension, b,x,y,z, the first is the box number starting from 0, the fifth is the volume and sugar concentration */
   blitz::Array<double, 5> m_nectar;
   /** \brief This is used to track the honey resource in each cell, four-deimension, b,x,y,z, the first is the box number starting from 0, the forth is the volume and sugar concentration */
   blitz::Array<double, 5> m_honey;
   /** \brief This is used to track the pollen resource in each cell, four-deimension, x,y,z, the first is the box number starting from 0, the forth is the volume and type of the stored pollen*/
   blitz::Array<double, 5> m_pollen;
   /** \brief This is the pointer for the thermodynamics object. */
   HiveThermodynamics* m_our_thermodynamics;
   /** \brief This is used to track the temperature of each cell in the hive, which needs to be updated by the hive thermodynamics pointer at each time step. We only model the temperature distributions during winter when there is only one box*/
   blitz::Array<double,3> m_hive_temperature;
   /** \brief THis is used to track the number of bees on each cell. The first index is the box number starting from 0. */
   blitz::Array<int, 4> m_beeCount;
   /** \brief Array to store cell type for each cell. The first index is the box number starting from 0. */
   blitz::Array<int,4> m_celltype;
   /** \brief Array to track the pesticide concentration from pesticide application in each cell. The first index is the box number starting from 0. The fifth is the number of the pesticide types. */
   blitz::Array<double,5> m_pest_application;
   /** \brief Array to track the pesticide concentration from seed coating in each cell. The first index is the box number starting from 0. The fifth is the number of the pesticide type. \note We might not need this anymore if we use same types both for spray application and seed coating treatment. */
   blitz::Array<double,5> m_pest_seeds_coating;
   /** \brief Array to store the DT50 for pesticide from application, this is not used*/
   blitz::Array<float,1> m_degradation_rate_pest_application;
   /** \brief Array to store the DT50 for pesticide from seed coating, this is not used */
   blitz::Array<float,1> m_degradation_rate_pest_seed_coating;
   /** \brief Array to track the pesticide concentration from pesticide application in the stored resource in each cell. \note this is not used. */
   blitz::Array<float*,5> m_pest_application_resource;
   /** \brief Array to track the pesticide concentration from seed coating in the stored resource in each cell. \note this is not used. */
   blitz::Array<float*,5> m_pest_seeds_coating_resource;
   /** \brief Array for searing sequence, updated by generate search sequence. For now, we generate the same searching sequence for all the boxes.*/
   blitz::Array<int,1> m_search_resource_list;
   /** \brief It is used to store the bee's object pointer belonging the cell. The first index is the box number starting from 0.*/
   blitz::Array<HoneyBee_Base*,4> m_bee_pointers;
   /** \brief Array to track the required task for each cell. The first index is the box number starting from 0. */
   blitz::Array<TaskType,4> m_required_tasks;
   /** \brief Array to track the status for the task on each cell. The first index is the box number starting from 0. */
   blitz::Array<TaskStatus,4> m_task_status;
   /** \brief Vector to track tee cells marked for nectar. */
   std::vector<std::vector<std::vector<Coords3D>>> m_marked_for_nectar_cells;
   /** \brief Vector to track tee cells marked for pollen. */
   std::vector<std::vector<std::vector<Coords3D>>> m_marked_for_pollen_cells;
   /** \brief Vector to track the cells with nectar.*/
   std::vector<std::vector<std::vector<Coords3D>>> m_nectar_cells;
   /** \brief Vector to track the cells with honey.*/
   std::vector<std::vector<std::vector<Coords3D>>> m_honey_cells;
   /** \brief Vector to track the cells with pollen*/
   std::vector<std::vector<std::vector<Coords3D>>> m_pollen_cells;
   /** \brief Vector to track the cells available for egg (worker and drone), there is a vector for each side. */
   std::vector<std::vector<std::vector<Coords3D>>> m_egg_cells;
   /** \brief Vector to track the cells for Larva, which requires feeding. */
   std::vector<std::vector<std::vector<Coords3D>>> m_larva_cells;
   /** \brief Vector to track the cells of Pupa needing capping. */
   std::vector<std::vector<std::vector<Coords3D>>> m_capping_brood;
    /** \brief Vector to track the cells of nectar needing capping. */
   std::vector<std::vector<std::vector<Coords3D>>> m_capping_nectar;
   /** \brief Vector to track the cells with cleaning requirement. */
   std::vector<std::vector<std::vector<Coords3D>>> m_req_cleaning_cells;
   /** \brief Array for store whether there is nosema in the cell, for now we have two types, 0: apis, 1 : ceranae. */
   blitz::Array<bool, 5> m_nosema;
   /** \brief Array for storing the numbers of different available jobs. */
   blitz::Array<int, 1> m_available_job;
   /** \brief Daily decay fraction in hive. */
   double m_seedcoating_daily_decay_frac_hive;
   double m_pesticide_daily_decay_frac_hive;
   /** \brief Global waste count in the hive. */
   double m_global_waste_count;
   /** \brief Flag for Nosema apis in the global waste. */
   bool m_nosema_apis_global_flag;
   /** \brief Flag for Mosema ceranae in the global waste. */
   bool m_nosema_ceranae_global_flag;
   /** \brief Array to store the summation of the bees's body temperature belonging to each cell. */
   blitz::Array<double, 3> m_group_bees_temperature_sum; 
   /** \brief Array to store the average body temperatrue of bees belongting to each cell. */
   blitz::Array<double, 3> m_average_body_temmperature;
   /** \brief Array to store the distance of each cell to the hive centre. \note This is only used for the first bottom box.*/
   blitz::Array<double, 3> m_dist_to_hive_centre;
   
    
public:

   /** \brief Default constructor, x, y, z, is used to set all the boxes to the same dimension, when the dimensions of the box are different, setBox() function should be used.*/ 
   Hive(int box_count, const unsigned x, const unsigned y, const unsigned z, const float cellRadius, const float frameSpacing, const float sideOffset, HiveThermodynamics* a_thermodynamics, int a_hive_position_x, int a_hive_position_y, HoneyBee_Colony* a_population_manager, Landscape* a_landscape);
   /** \brief Default destructor. */
   virtual ~Hive();
   /** \brief Add a new box on top of the current one(s). */
   void addBox(const unsigned x, const unsigned y, const unsigned z);
   /** \brief Function to set the given box's dimension. */
   void setBox(int box_num, const unsigned x, const unsigned y, const unsigned z);
   /** \brief Function to remove queen excluder, box_num starts from 0. */
   void removeQueenExluder(int box_num);
   /** \brief Function used to initialise a hive. */
   void initialiseHive(int box_count, const int x, const int y, const int z, const float cellRadius, const float frameSpacing, const float sideOffset);
   /** \brief Test whether the given cell is empty. */
   bool isFree(const int box_num, const int x, const int y, const int z){return m_celltype(box_num, x,y,z)==(int)CellType::Empty;}
   /** \brief Generate frame side searching list, when a bee is going to search for some resource in the hive, it will follow this order for the frame side. It always starts from its current frame side.*/
   void generateSearchingList(int start);
   /** \brief When a bee needs resource, it uses this function., The first argument:1:honey, 2:nectar, 3:pollen */
   double consumeResource(int resource_type, int eating_y, int eating_box, HoneyBee_Worker* the_bee, double sugar_in_mg);
   /** \brief When a bee eats honey, it uses this function. */
   double consumeSugar(int eating_box, int eating_y, HoneyBee_Worker* the_bee, double sugar_in_mg);
   /** \brief When a bee eats pollen, it uses this function. */
   double consumePollen(int eating_box, int eating_y, HoneyBee_Worker* the_bee, double pollen_in_mg);
   /** \brief This is used for a queen to lay eggs in the available cells. */
   int layEggsInCells(int egg_num, int box_num, int current_frame_side);

   /** \brief This is used to update the availabe task numbers in the hive. */
   void updateAvailableJobNums();
   /** \brief This is used to set the number for the given task. */
   void setAvailableJobNum(TaskType a_job, int a_num){m_available_job(EnumClassAsIndex(a_job))=a_num;}
 
   /** \brief Functions to supply the maximum numbers of cells in three dimension for one box. */
   int supplyXrange(){return xrange;}
   int supplyYrange(){return yrange;}
   int supplyZrange(){return zrange;}
   blitz::TinyVector<int,4> shape() {return blitz::TinyVector<int,4>(m_num_of_used_box,xrange,yrange,zrange);}

   /** \brief Functions to supply the position of the hive in the landscape. */
   int supplyHivePositionX(){return m_hive_position_x;}
   int supplyHivePositionY(){return m_hive_position_y;}

   /** \brief These two functions are used to decrease and increase the bee numbers for a cell. */
   void incBeeCount(const int box_num, const int x, const int y, const int z) { if (z < 0) return;  m_beeCount(box_num, x, y, z) += 1; }
   void decBeeCount(const int box_num, const int x, const int y, const int z) { if (z < 0) return; m_beeCount(box_num,x,y,z)-=1;}
   int supplyBeeCount(const int box_num, const int x, const int y, const int z) {return m_beeCount(box_num, x,y,z);}

   /** \brief These functions are used to return different resource on one specific frame side. */
   blitz::Array<double,2> getFrameTemp(const int frame, const FrameSide side) {return m_hive_temperature(blitz::Range::all(),(frame*2) + (int)side,blitz::Range::all());}
   blitz::Array<double,2> getFrameHoney(const int box_num, const int frame, const FrameSide side){return m_honey(box_num, blitz::Range::all(),(frame*2) + (int)side, blitz::Range::all(), 0);}
   blitz::Array<double,2> getFramePollen(const int box_num, const int frame, const FrameSide side){return m_pollen(box_num, blitz::Range::all(),(frame*2) + (int)side, blitz::Range::all(), 0);}
   blitz::Array<int,2> getFrameCellType(const int box_num, const int frame, const FrameSide side){return m_celltype(box_num, blitz::Range::all(),(frame*2) + (int)side,blitz::Range::all());}
   blitz::Array<int,2> getFrameBeeNums(const int box_num, const int frame, const FrameSide side){return m_beeCount(box_num, blitz::Range::all(),(frame*2) + (int)side,blitz::Range::all());}

   /** \brief There functions are used to set the cell type. */
   void setCellType(const int box_num, const int x, const int y, const int z, const int val){m_celltype(box_num, x,y,z)=val;}
   void setCellType(const blitz::TinyVector<int,4> p, const int val) {m_celltype(p(0),p(1),p(2),p(3))=val;}
   void setCellType(const blitz::TinyVector<int,4> p, const CellType val) {m_celltype(p(0),p(1),p(2),p(3))=(int)val;}

   /** \brief These functions are used to return the resource arrays. */
   blitz::Array<int,4> getCellTypeArray(){return m_celltype;}
   blitz::Array<int,4> getBeeCountArray(){return m_beeCount;}
   blitz::Array<double,3> getTemperatureArray() { return m_hive_temperature;}
   blitz::Array<double,3>* supplyTemperatureArrayPointer() {return &m_hive_temperature;}

   /** \brief Function used to set the cell type to be empty for all the cells. */
   void clearCellTypeArray();

   /** \brief There functions are used to supply the cell type of a given cell. */
   CellType getCellType(blitz::TinyVector<int,4> cell) {return (CellType)m_celltype(cell);}
   CellType getCellType(const int a_box_num, const int x, const int y, const int z) {return (CellType)m_celltype(a_box_num, x,y,z);}

   /** \brief Return the adult bee pointer for one given cell. */
   HoneyBee_Base * supplyBeePointer(const int a_box_num, const int x, const int y, const int z){return m_bee_pointers(a_box_num,x,y,z);}

   /** \brief Set the bee pointer for one given cell. Although, it is possible to have more than one workers at a cell. We only track one. When a bee visit one cell, it will randomly choose to set the pointer or not. */
   void setBeePointer(const int a_box_num, const int x, const int y, const int z, HoneyBee_Base * theBee){m_bee_pointers(a_box_num,x,y,z)=theBee;}

   /** \brief Supply the 8random neighbours with the given position. The first six are the ones on the same frame side, the seventh is on the other frame, the eighth on is on the same frame but the other frame side. */
   blitz::TinyVector<Coords3D,8> supplyNeighbours(const int x, const int y, const int z);

   /** \brief Return the cell will highest temperature with the given type. */
   blitz::TinyVector<int,3> warmestCell(CellType cellType);

   /** \brief Return a cell postion randomly. */
   blitz::TinyVector<int,4> randomCell();

   /** \brief Return a cell with the given type ramdomly. */
   blitz::TinyVector<int,4> randomCell(CellType cellType);

   /** \brief Supply task type for a given cell. */
   TaskType supplyCellTaskType(int box_num, int x, int y, int z) {return m_required_tasks(box_num,x,y,z);}
   /** \brief Supply task status for a given cell. */
   TaskStatus supplyCellTaskStatus(int box_num, int x, int y, int z) {return m_task_status(box_num,x,y,z);}
   /** \brief Set task for a given cell. */
   void setCellTaskType(int box_num, int x, int y, int z, TaskType a_task_type) {m_required_tasks(box_num, x,y,z)=a_task_type;}
   /** \brief Set task status for a given cell. */
   void setCellTaskStatus(int box_num, int x, int y, int z, TaskStatus a_task_type) {m_task_status(box_num, x,y,z)=a_task_type;}
   /** \brief Add the position of a cell which requires feeding. */
   void addCellForFeeding(int box_num, Coords3D position){m_larva_cells.at(box_num).at(position.y).push_back(position);}
   /** \brief Function used for a nurse bee to feed a larva. */
   bool feedLarva(int box_num, int current_y, HoneyBee_Worker* the_bee, int feeding_num, double* a_sugar, double* a_pollen, double* a_jelly);
   /** \brief Function used for a nurse bee to cap a cell, the final argument: 1: brood, 2: sugar. */
   bool capCell(int box_num, int current_y, HoneyBee_Worker* the_bee, int caping_num, double* a_wax, int task_type);
   /** \brief Function used for a nurse bee to clean a cell with dead body. */
   bool cleanCell(int box_num, int current_y, HoneyBee_Worker* the_bee, int clean_num);
   /** \brief Function for hatching from a cell. */
   void hatchFromCell(int box_num, int x, int y, int z, bool alive);
   /** \brief Add the postion of cell which requires capping brood. */
   void addBroodCellForCapping(int box_num, Coords3D position){m_capping_brood.at(box_num).at(position.y).push_back(position);}
   /** \brief Supply the available numbers of different task, it is used for a bee to choose task in the beginning of each time step. */
   blitz::Array<int, 1> supplyNumsAvailabeTasks() {return m_available_job;}
   /** \brief Move a worker to another position in the hive.*/
   void moveWorkerBee(HoneyBee_Worker* the_bee, int box_num, int a_x, int a_y, int a_z);
   /** \brief Store resource to a cell. */
   bool storeResourceToCell(int resource_type, int box_num, int storing_y, HoneyBee_Worker* the_bee, PollenNectarInfo a_resource);
   /** \brief Store pollen to a cell. */
   bool storePollenToCell(PollenNectarInfo a_resource, HoneyBee_Worker* the_bee, int box_num);
   /** \brief Store nectar to a cell. */
   bool storeNectarToCell(PollenNectarInfo a_resource, HoneyBee_Worker* the_bee, int box_num);
   /** \brief Evaporate the nectar to honey. */
   void evaporateNectar(void);
   /** \brief Update the pesticide concentration based on DT50. */
   void updatePesticideConcentration(void);
   /** \brief Supply the pesticide from seed coating. */
   double supplyConcentrationSeedCoating(TTypesOfVegetation tov_PermanentGrassGrazed);
   /** \brief Add pestide for pollen. */
   void addPesticidePollen(int box_num, Coords3D temp, int a_poly_id, double a_mount);
   /** \brief Add pestide for nectar. */
   void addPesticideNectar(int box_num, Coords3D temp, int a_poly_id, double a_mount);
   /** \brief Add contacting exposures for bees. */
   void addContactingPesticide(HoneyBee_Worker* the_bee, int a_poly_id);
   /** \brief Set the nosema flag in the cell. */
   void setNosemaCell(int box_num, int x, int y, int z, int a_type, bool a_value){m_nosema(box_num, x, y, z, a_type)=a_value;}
   /** \brief Return whether there is nosema in the cell. */
   bool supplyNosemaCellFlag(int box_num, int x, int y, int z, int a_type){return m_nosema(box_num, x,y,z,a_type);}
   /** \brief Increse the global waste by one. */
   void increaseGlobalWaste(){m_global_waste_count++;}
   /** \brief Decrease the global waste by one. */
   void decreaseGlobalWaste(){m_global_waste_count--;}
   /** \brief Return if there is global waste or not in the hive. */
   bool isGlobalWaste(){return m_global_waste_count>0;}
   /** \brief Set the flag for nosema apis in the global waste. */
   void setGlobalApis(bool a_val){m_nosema_apis_global_flag = a_val;}
   /** \brief Set the flag for  nosema ceranae in the global waste. */
   void setGlobalCeranae(bool a_val){m_nosema_ceranae_global_flag = a_val;}
   /** \brief Return if there is nosema apis in the global waste. */
   bool isApisInGlobalWaste(){return m_nosema_apis_global_flag;}
   /** \brief Return if there is nosema ceranae in the global waste. */
   bool isCeranaeInGlobalWaste(){return m_nosema_ceranae_global_flag;}
   /** \brief Remove one frame, which will remove all the resource on the frame. */
   void removeOneFrame(int box_num, int a_frame_id);
   /** \brief Fill one empty frame full of honey. */
   void fillHoneyFrameFull(int box_num, int a_frame_id);
   /** \brief Reset the bees' group body temperature to zero. */
   void clearBeesGroupTemperature(){m_group_bees_temperature_sum=0;}
   /** \brief Add the bees' group body temperature for the given cell. */
   //void addBeeGroupTemperature(int x, int y, int z, double a_tempe){m_group_bees_temperature_sum(x,y,z)+=a_tempe;}
   void addBeeGroupTemperature(int x, int y, int z, double a_tempe){;}
   /** \brief Return the bees' group body temperature for the given cell. */
   double supplyBeeGroupTemperature(int x, int y, int z){return m_group_bees_temperature_sum(x,y,z);}
   /** \brief Add one cell that needs cleaning. */
   void addCellForCleaning(int b, int x, int y, int z); 
   /** \brief Calculate the average body temperature array. */
   void calAverageTemperature();
   /** \brief Return the body average body temperature array. */
   blitz::Array<double, 3>* supplyAverageTemperatureArray(){return &m_average_body_temmperature;}
   /** \brief Return the number of honey cells in the hive. */
   int supplyHoneyCellNum();
   /** \brief Return the number of nectar cells in the hive. */
   int supplyNectarCellNum();
   /** \brief Return the number of pollen cells in the hive. */
   int supplyPollenCellNum();
   /** \brief Return the number of egg cells in the hive. */
   int supplyEggCellNum();
   /** \brief Return the number of larvae cells in the hive. */
   int supplyLarvaeCellNum();
   /** \brief Return the number of pupa cells in the hive. */
   int supplyPupaCellNum();
   /** \brief Return the number of cells with requirement of cleaning in the hive. */
   int supplyCleaningReqCellNum();


};

#endif // HIVE_H
