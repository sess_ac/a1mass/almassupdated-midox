/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Hive.cpp
    \brief <B>The main source code for hive management</B>
    Version of Feb. 2020 \n
    By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/

#include <fstream>
#include <vector>
#include "math.h"
#include <iostream>
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../BatchALMaSS/ALMaSS_Random.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../Landscape/Configurator.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "HoneyBee_Colony.h"
#include "HoneyBee.h"
#include "Hive.h"
#include "BiologicalAgents.h"

//using namespace blitz;

//These are the parameters for hive settings.
CfgInt cfg_HoneyBeeMaxBoxNumPerColony("HONEY_BEE_MAX_BOX_NUM_PER_COLONY", CFG_CUSTOM, 3);


//These are the parameters for pesticide.
extern CfgBool l_pest_enable_pesticide_engine;
extern CfgInt l_pest_NoPPPs;
CfgFloat cfg_HoneyBeeCoefficientForPesticideBetweenCellAndResource("HONEY_BEE_COEFFICIENT_FOR_PESTICIDE_BETWEEN_CELL_AND_RESOURCE", CFG_CUSTOM, 20);
//TO DEL
CfgFloat cfg_PestFromSeedCoatingToPollen("PEST_FROM_SEED_COATING_TO_POLLEN", CFG_CUSTOM, 0.1);
CfgFloat cfg_PestFromSeedCoatingToNectar("PEST_FROM_SEED_COATING_TO_NECTAR", CFG_CUSTOM, 0.1);
CfgFloat cfg_PestFromApplicationToPollen("PEST_FROM_APPLICATION_TO_POLLEN", CFG_CUSTOM, 0.1);
CfgFloat cfg_PestFromApplicationToNectar("PEST_FROM_APPLICATION_TO_NECTAR", CFG_CUSTOM, 0.1);

CfgFloat cfg_HoneyBeeContactingPestRateBee("HONEY_BEE_CONTACTING_PEST_RATE_BEE", CFG_CUSTOM, 0.01);

//There are the parameters for biological agents
CfgFloat cfg_HoneyBeeProbaInfectedNosemaFromEating("HONEY_BEE_PROBA_INFECTED_NOSEMA_FROM_EATING", CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeProbaNosemaFromStoringResource("HONEY_BEE_PROBA_NOSEMA_FROM_STORING_RESOURCE", CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeProbaVarroaFromAdultToPupa("HONEY_BEE_PROBA_VARROA_FROM_ADULT_TO_PUPA", CFG_CUSTOM, 0.8);


CfgFloat cfg_HoneyBeeMiniTemperatureForOutActivities("HONEY_BEE_MINI_TEMPERATURE_FOR_OUT_ACTIVITIES", CFG_CUSTOM, 15);

static CfgFloat l_seed_coating_half_life_in_cell("SEED_COATING_HALF_LIFE_IN_HIVE", CFG_CUSTOM, 30.0);
static CfgFloat l_pest_half_life_in_cell("PEST_HALF_LIFE_IN_HIVE", CFG_CUSTOM, 30.0);



//These are the honeybee food related parameters.

static CfgFloat cfg_HoneyBeeHoneycell_Volume("HONEY_BEE_HONEYCELL_VOLUME",CFG_CUSTOM,200000);
CfgInt cfg_HoneyBeeMaxBeeNumPerCell("HONEY_BEE_MAX_BEE_NUM_PER_CELL",CFG_CUSTOM,6);
CfgFloat cfg_HoneyBeeWaxForCapingACell("HONEY_BEE_WAX_FOR_CAPING_A_CELL",CFG_CUSTOM,10);
extern CfgFloat cfg_HoneyBeeVolumeForagedNectarOneTime;
extern CfgFloat cfg_HoneyBeeVolumeForagedPollenOneTime;
CfgInt cfg_HoneyBeeStepNumNeedFromNectarToHoney("HONEY_BEE_STEP_NUM_NEED_FROM_NECTAR_TO_HONEY", CFG_CUSTOM, 432);
CfgFloat cfg_HoneyBeeSugarConcentrationInHoney("HONEY_BEE_SUGAR_CONCENTRATION_IN_HONEY", CFG_CUSTOM, 0.9);
extern CfgFloat cfg_HoneyBeeTemperatureLimitToDefineColdDays;
//Forward declaration
class struct_HoneyBee;
class HoneyBee_WorkerEgg;
class HoneyBee_DroneEgg;

Hive::Hive(int box_count, unsigned x, unsigned y, unsigned z, const float cellRadius, const float frameSpacing, const float sideOffset,HiveThermodynamics* a_thermodynamics, int a_hive_position_x, int a_hive_position_y,HoneyBee_Colony* a_population_manager, Landscape* a_landscape) : m_hive_position_x(a_hive_position_x), m_hive_position_y(a_hive_position_y), m_our_population_manager(a_population_manager), m_TheLandscape(a_landscape)
{
    m_box_info = new BoxInfo[cfg_HoneyBeeMaxBoxNumPerColony.value()];
    m_our_thermodynamics = a_thermodynamics;
    initialiseHive(box_count, x,y,z,cellRadius,frameSpacing,sideOffset);
}

Hive::~Hive(){
    delete []m_box_info;
    std::cout<<"~Hive";
}

void Hive::updatePesticideConcentration(){
    if(l_pest_enable_pesticide_engine.value()){
        m_pest_application=m_pest_application*m_pesticide_daily_decay_frac_hive; /** \todo Each type of pesticide should have its own degradation rate. */
        m_pest_seeds_coating=m_pest_seeds_coating*m_seedcoating_daily_decay_frac_hive;
    }
}

void Hive::evaporateNectar(){
    //we only count days, when the maximum days reach, nectar becomes honey
    for(int k=0; k<m_num_of_used_box; k++){
        for(int i=0; i<yrange;i++){
            if(m_nectar_cells.at(k).at(i).size()>0){ //if it is a cell for nectar
                for(int j=0;j<m_nectar_cells.at(k).at(i).size();j++){
                    Coords3D temp_cell = m_nectar_cells.at(k).at(i).at(j);
                    //add one time step
                    m_nectar(k, temp_cell.x, temp_cell.y, temp_cell.z, 2)++;
                    if(m_nectar(k, temp_cell.x, temp_cell.y, temp_cell.z, 2) > cfg_HoneyBeeStepNumNeedFromNectarToHoney.value()){
                        //time to become honey
                        if(j>=(m_nectar_cells.at(k).at(i).size()-1)){//if it is the last one, just pop it out
                            m_nectar_cells.at(k).at(i).pop_back();
                        }
                        else{
                            m_nectar_cells.at(k).at(i).at(j) = m_nectar_cells.at(k).at(i).back();//if it is not the last one, switch it with the last one in the vector, this is for speed sake
                            m_nectar_cells.at(k).at(i).pop_back();
                        }
                        //move the cell for capping
                        //m_capping_nectar.at(temp_cell.y).push_back(temp_cell);
                        m_honey_cells.at(k).at(temp_cell.y).push_back(temp_cell);
                        m_celltype(k, temp_cell.x, temp_cell.y, temp_cell.z) = (int)CellType::Honey;
                        m_honey(k, temp_cell.x, temp_cell.y, temp_cell.z,0)=m_nectar(k, temp_cell.x, temp_cell.y, temp_cell.z, 0)*m_nectar(k,temp_cell.x, temp_cell.y, temp_cell.z, 1)/cfg_HoneyBeeSugarConcentrationInHoney.value();
                        m_honey(k, temp_cell.x, temp_cell.y, temp_cell.z,1)=cfg_HoneyBeeSugarConcentrationInHoney.value();
                    }
                }
            }        
        }
    }
}

void Hive::updateAvailableJobNums(){
    /** \todo We need to figure out how to put different weight on different jobs. */
    //first clear all the numbers
    m_available_job = 0;
    double current_tempe = m_TheLandscape->SupplyTemp();
    //current_tempe = 15;
    if(current_tempe<=cfg_HoneyBeeTemperatureLimitToDefineColdDays.value()){
        m_available_job(EnumClassAsIndex(TaskType::WarmingUp)) = 1000;
        //we still want them to attend brood if it is possible
        //for (int i=0; i<yrange; i++){
        //    m_available_job(EnumClassAsIndex(TaskType::FeedingBrood))+=1000*m_larva_cells.at(i).size();
        //}
        return;
    }
    for (int k=0; k<m_num_of_used_box; k++){
        for (int i=0; i<yrange; i++){
            m_available_job(EnumClassAsIndex(TaskType::CappingBrood))+=m_capping_brood.at(k).at(i).size();
            m_available_job(EnumClassAsIndex(TaskType::FeedingBrood))+=10000*m_larva_cells.at(k).at(i).size();
            m_available_job(EnumClassAsIndex(TaskType::CleaningCell))+=m_req_cleaning_cells.at(k).at(i).size();
            if(current_tempe >= cfg_HoneyBeeMiniTemperatureForOutActivities.value()){
                m_available_job(EnumClassAsIndex(TaskType::ForagingNectar))+=m_marked_for_nectar_cells.at(k).at(i).size();
                m_available_job(EnumClassAsIndex(TaskType::ForagingPollen))+=m_marked_for_pollen_cells.at(k).at(i).size();
            }
            m_available_job(EnumClassAsIndex(TaskType::RipeningNectar))+=m_nectar_cells.at(k).at(i).size();
            m_available_job(EnumClassAsIndex(TaskType::CappingHoney))+=m_capping_nectar.at(k).at(i).size();
            //m_available_job(EnumClassAsIndex(TaskType::DepositingNectar))+=m_marked_for_nectar_cells.at(i).size();
        }
    }
    //try to avoid a lot bees doing this
    if(m_available_job(EnumClassAsIndex(TaskType::RipeningNectar))>0){m_available_job(EnumClassAsIndex(TaskType::RipeningNectar))=1;}
    //if(current_tempe>cfg_HoneyBeeMiniTemperatureForOutActivities.value()){
    //cout<<m_available_job;}

    //comment this out, this is only used for calibration of foraging model
    m_available_job = 0;
}


void Hive::generateSearchingList(int start){
    m_search_resource_list(0) = start;
    if(start==0){
        for(int i=1;i<yrange;i++){
            m_search_resource_list(i)=i;
        }
        return;
    }
    int j = 1;
    if(start==yrange-1){
        for(int i=yrange-2;i>=0;i--){
            m_search_resource_list(j++)=i;
        }
        return;
    }
    j=1;
    if (g_rand_uni()>0.5){
        for (int i=start+1; i<yrange; i++){
            m_search_resource_list(j++)=i;
        }
        for (int i=start-1; i>=0; i--){
            m_search_resource_list(j++)=i;
        }
    }
    else{
        for (int i=start-1; i>=0; i--){
            m_search_resource_list(j++)=i;
        }
        for (int i=start+1; i<yrange; i++){
            m_search_resource_list(j++)=i;
        }
    }
}


double Hive::consumeResource(int resource_type, int eating_box,  int eating_y,HoneyBee_Worker* the_bee, double resource_in_mg){
    double req_resource = resource_in_mg;
    //first to generate a list to search for resource
    std::vector<std::vector<std::vector<Coords3D>>>* p_resource_cells;
    blitz::Array<double, 5>* p_resource;
    std::vector<std::vector<std::vector<Coords3D>>>* p_marked_for_resource_cells;

    if(resource_type==1){
        p_resource_cells =&m_honey_cells;
        p_resource = &m_honey;
        p_marked_for_resource_cells = &m_marked_for_nectar_cells;
    }
    if(resource_type==3){
        p_resource_cells =&m_pollen_cells;
        p_resource = &m_pollen;
        p_marked_for_resource_cells = &m_marked_for_pollen_cells;
    }

    //generate a radom searching sequence for the frames in the given box
    generateSearchingList(eating_y);
    vector<int> eat_sequence;
    for(int i=0; i<yrange;i++){
        if ((*p_resource_cells).at(eating_box).at(m_search_resource_list(i)).size()>0){
            eat_sequence.push_back(m_search_resource_list(i));
        }
    }
    //there is no honey found, later we  need to change this, because they can also eat nectar
    if(eat_sequence.size()<1)
    {
        return 0;
    }
    for(int i=0; i<eat_sequence.size();i++){
        while((*p_resource_cells).at(eating_box).at(eat_sequence.at(i)).size()>0){
            Coords3D temp =(*p_resource_cells).at(eating_box).at(eat_sequence.at(i)).at(0);
            //do we have enough food for this cell?
            double available_resource = (*p_resource)(eating_box,temp.x,temp.y,temp.z,0)*(*p_resource)(eating_box, temp.x,temp.y,temp.z,1);
            //set nosema for bee
            if(g_rand_uni()<cfg_HoneyBeeProbaInfectedNosemaFromEating.value()){
                if(m_nosema(eating_box, temp.x, temp.y, temp.y, 0)){
                    if(the_bee->m_MyBiologicalAgent->getNosemaA()<1){
                        the_bee->m_MyBiologicalAgent->setNosemaA(1);
                    }                
                }
                if(m_nosema(eating_box, temp.x, temp.y, temp.y, 1)){
                    if(the_bee->m_MyBiologicalAgent->getNosemaC()<1){
                        the_bee->m_MyBiologicalAgent->setNosemaC(1);
                    }                
                }
            }
            if(available_resource>=resource_in_mg){
                double deduce_amount = (resource_in_mg/(*p_resource)(eating_box, temp.x,temp.y,temp.z,1));
                //pesticide code here
                if(l_pest_enable_pesticide_engine.value()){
                    for(int ppp=0; ppp<l_pest_NoPPPs.value(); ppp++){
                        double pest_amount = m_pest_application(eating_box, temp.x, temp.y, temp.z, ppp)*deduce_amount/((*p_resource)(eating_box, temp.x,temp.y,temp.z,0)+cfg_HoneyBeeCoefficientForPesticideBetweenCellAndResource.value());
                        the_bee->addBeePesticideApplication(ppp, pest_amount);
                        m_pest_application(eating_box, temp.x, temp.y, temp.z, ppp)-=pest_amount;
                    }
                    for(int ppp=0; ppp<l_pest_NoPPPs.value(); ppp++){
                        double pest_amount = m_pest_application(eating_box, temp.x, temp.y, temp.z, ppp)*deduce_amount/((*p_resource)(eating_box, temp.x,temp.y,temp.z,0)+cfg_HoneyBeeCoefficientForPesticideBetweenCellAndResource.value());
                        the_bee->addBeePesticideApplication(ppp, pest_amount);
                        m_pest_seeds_coating(eating_box, temp.x, temp.y, temp.z, ppp)-=pest_amount;
                    }
                }
                (*p_resource)(eating_box, temp.x,temp.y,temp.z,0)-= deduce_amount;
                return req_resource;
            }
            //the cell is empty, we need to find another cell
            else{
                //pesticide code here
                double deduce_amount = (*p_resource)(eating_box, temp.x,temp.y,temp.z);
                if(l_pest_enable_pesticide_engine.value()){
                    for(int ppp=0; ppp<l_pest_NoPPPs.value(); ppp++){
                        double pest_amount = m_pest_application(eating_box, temp.x, temp.y, temp.z, ppp)*deduce_amount/((*p_resource)(eating_box, temp.x,temp.y,temp.z,0)+cfg_HoneyBeeCoefficientForPesticideBetweenCellAndResource.value());
                        the_bee->addBeePesticideApplication(ppp, pest_amount);
                        m_pest_application(eating_box, temp.x, temp.y, temp.z, ppp)-=pest_amount;
                    }
                    for(int ppp=0; ppp<l_pest_NoPPPs.value(); ppp++){
                        double pest_amount = m_pest_application(eating_box, temp.x, temp.y, temp.z, ppp)*deduce_amount/((*p_resource)(eating_box, temp.x,temp.y,temp.z,0)+cfg_HoneyBeeCoefficientForPesticideBetweenCellAndResource.value());
                        the_bee->addBeePesticideApplication(ppp, pest_amount);
                        m_pest_seeds_coating(eating_box, temp.x, temp.y, temp.z, ppp)-=pest_amount;
                    }
                }
                //set 0 for the resource
                (*p_resource)(eating_box, temp.x,temp.y,temp.z)=0;
                m_celltype(eating_box, temp.x,temp.y,temp.z)=(int)CellType::Empty;
                //m_pest_seeds_coating_resource(temp.x,temp.y,temp.z)=0;
                //m_pest_application_resource(temp.x,temp.y,temp.z)=0;
                //remove the cell from the available resource list
                (*p_resource_cells).at(eating_box).at(eat_sequence.at(i)).at(0)=(*p_resource_cells).at(eating_box).at(eat_sequence.at(i)).back();
                (*p_resource_cells).at(eating_box).at(eat_sequence.at(i)).pop_back();
                //mark it for storing nectar
                if(resource_type==1){
                    m_celltype(eating_box, temp.x,temp.y,temp.z)=(int)CellType::MarkedNectar;
                }
                if(resource_type==3){
                    m_celltype(eating_box, temp.x,temp.y,temp.z)=(int)CellType::MarkedPollen;
                }
                
                (*p_marked_for_resource_cells).at(eating_box).at(temp.y).push_back(temp);
                resource_in_mg -= available_resource;
                //todo, add pesticide code here
            }
        }
    }
    //no food found at all, the colony is dying...
    return (req_resource-resource_in_mg);

}

double Hive::consumePollen(int eating_box, int eating_y, HoneyBee_Worker* the_bee, double pollen_in_mg){
    return consumeResource(3, eating_box, eating_y, the_bee,  pollen_in_mg);
}

double Hive::consumeSugar(int eating_box, int eating_y, HoneyBee_Worker* the_bee, double sugar_in_mg){
    return consumeResource(1, eating_box, eating_y, the_bee, sugar_in_mg);
    /*
    double req_sugar = sugar_in_mg;
    //first to generate a list to search for resource
    generateSearchingList(eating_y);
    vector<int> eat_sequence;
    for(int i=0; i<yrange;i++){
        if (m_honey_cells.at(m_search_resource_list[i]).size()>0){
            eat_sequence.push_back(m_search_resource_list[i]);
        }
    }
    //there is no honey found, later we  need to change this, because they can also eat nectar
    if(eat_sequence.size()<1)
    {
        return 0;
    }
    for(int i=0; i<eat_sequence.size();i++){
        while(m_honey_cells.at(eat_sequence.at(i)).size()>0){
            Coords3D temp =m_honey_cells.at(eat_sequence.at(i)).at(0);
            //do we have enough food for this cell?
            double available_sugar = m_honey(temp.x,temp.y,temp.z,0)*m_honey(temp.x,temp.y,temp.z,1);
            if(available_sugar>=sugar_in_mg){
                m_honey(temp.x,temp.y,temp.z,0)-=(sugar_in_mg/m_honey(temp.x,temp.y,temp.z,1));
                //to do, add pesticide code here
                return req_sugar;
            }
            //the cell is empty, we need to find another cell
            else{
                //set 0 for the resource
                m_honey(temp.x,temp.y,temp.z)=0;
                m_celltype(temp.x,temp.y,temp.z)=(int)CellType::Empty;
                m_pest_seeds_coating_resource(temp.x,temp.y,temp.z)=0;
                m_pest_application_resource(temp.x,temp.y,temp.z)=0;
                //remove the cell from the available resource list
                m_honey_cells.at(eat_sequence.at(i)).at(0)=m_honey_cells.at(eat_sequence.at(i)).back();
                m_honey_cells.at(eat_sequence.at(i)).pop_back();
                //mark it for storing nectar
                m_celltype(temp.x,temp.y,temp.z)=(int)CellType::MarkedNectar;
                m_marked_for_nectar_cells.at(temp.y).push_back(temp);
                sugar_in_mg -= available_sugar;
                //todo, add pesticide code here
            }
        }
    }
    //no food found at all, the colony is dying...
    return (req_sugar-sugar_in_mg);*/
}

/*
double::consumePollen(int eating_y, HoneyBee_Worker* the_bee, double pollen_in_mg){
    PollenInfo return_value;
    return_value.amount = 0;
    return_value.pollen_type = -1; // used for nothing found
    //first to generate a list to search for resource
    generateSearchingList(eating_y);
    vector<int> eat_sequence;
    for(int i=0; i<yrange;i++){
        if (m_pollen_cells.at(m_search_resource_list[i]).size()>0){
            eat_sequence.push_back(m_search_resource_list[i]);
        }
    }
    //there is no honey found, later we  need to change this, because they can also eat nectar
    if(eat_sequence.size()<1)
    {
        return return_value;
    }
    for(int i=0; i<eat_sequence.size();i++){
        while(m_pollen_cells.at(eat_sequence.at(i)).size()>0){
            Coords3D temp =m_pollen_cells.at(eat_sequence.at(i)).at(0);
            //do we have enough food for this cell?
            double available_pollen = m_pollen(temp.x,temp.y,temp.z,1);
            if(available_pollen>=pollen_in_mg){
                m_pollen(temp.x,temp.y,temp.z,1)-=pollen_in_mg;
                //to do, add pesticide code here
                return_value.pollen_type = m_pollen(temp.x,temp.y,temp.z,0);
                return_value.amount = pollen_in_mg;
                return return_value;
            }
            //the cell is empty, we need to find another cell
            else{
                //set 0 for the resource ammount and -1 for the pollen type
                m_pollen(temp.x,temp.y,temp.z,1)=0;
                m_pollen(temp.x,temp.y,temp.z,0)=-1;
                m_celltype(temp.x,temp.y,temp.z)=(int)CellType::Empty;
                m_pest_seeds_coating_resource(temp.x,temp.y,temp.z)=0;
                m_pest_application_resource(temp.x,temp.y,temp.z)=0;
                //remove the cell from the available resource list
                m_pollen_cells.at(eat_sequence.at(i)).at(0)=m_pollen_cells.at(eat_sequence.at(i)).back();
                m_pollen_cells.at(eat_sequence.at(i)).pop_back();
                //todo, add pesticide code here
                return_value.pollen_type = m_pollen(temp.x,temp.y,temp.z,0);
                return_value.amount = available_pollen;
                return return_value;
            }
        }
    }
}
*/

bool Hive::cleanCell(int box_num, int current_y, HoneyBee_Worker* the_bee, int cleaning_num){
    generateSearchingList(current_y);
    vector<int> clean_sequence;
    for(int i=0; i<yrange;i++){
        if (m_req_cleaning_cells.at(box_num).at(m_search_resource_list(i)).size()>0){
            clean_sequence.push_back(m_search_resource_list(i));
        }
    }
    //there is no cell found with dead body
    if(clean_sequence.size()<1)
    {
        return false;
    }

    int cleaned_num = 0;
    Coords3D temp;
    for(int i=0; i<clean_sequence.size();i++){
        while(m_req_cleaning_cells.at(box_num).at(clean_sequence.at(i)).size()>0){
            temp =m_req_cleaning_cells.at(box_num).at(clean_sequence.at(i)).at(0);
            if (m_celltype(box_num, temp.x,temp.y,temp.z)==(int)(CellType::DeadBody)){
                m_task_status(box_num, temp.x,temp.y,temp.z)=TaskStatus::Ongoing;

                //add pesticide and disease
                m_task_status(box_num, temp.x,temp.y,temp.z)=TaskStatus::Finished;
                m_required_tasks(box_num, temp.x,temp.y,temp.z)=TaskType::Null;
                m_task_status(temp.x,temp.y,temp.z)=TaskStatus::Null;
                    
                m_req_cleaning_cells.at(box_num).at(clean_sequence.at(i)).at(0)=m_req_cleaning_cells.at(box_num).at(clean_sequence.at(i)).back();
                m_req_cleaning_cells.at(box_num).at(clean_sequence.at(i)).pop_back();
                //egg again
                if(g_rand_uni()>0.05){
                    m_egg_cells.at(box_num).at(temp.y).push_back(Coords3D(temp.x, temp.y, temp.z));
                    if(g_rand_uni()<0.05){
                        m_celltype(box_num, temp.x, temp.y, temp.z) = (int)CellType::Drone_Egg;
                    }
                    else{
                        m_celltype(box_num, temp.x, temp.y, temp.z) = (int)CellType::Worker_Egg;
                    }
                }
                //marked for pollen cell
                else{
                    m_marked_for_pollen_cells.at(box_num).at(temp.y).push_back(Coords3D(temp.x, temp.y, temp.z));
                    m_celltype(box_num, temp.x, temp.y, temp.z) = (int)CellType::MarkedPollen;
                }
                cleaned_num++;
                if(cleaned_num>=cleaned_num){
                    moveWorkerBee(the_bee, box_num, temp.x, temp.y, temp.z);
                    return true;
                }
            }          
        }
    }
    return false;
}

bool Hive::capCell(int box_num, int current_y, HoneyBee_Worker* the_bee, int caping_num, double* a_wax, int cell_type){
    std::vector<std::vector<std::vector<Coords3D>>>* capping_cells_pointer;
    if(cell_type==1){
        capping_cells_pointer = &m_capping_brood;
    }
    if(cell_type==2){
        capping_cells_pointer = &m_capping_nectar;
    }
    //first to generate a list to search for cell needing caping
    generateSearchingList(current_y);
    vector<int> caping_sequence;
    for(int i=0; i<yrange;i++){
        if ((*capping_cells_pointer).at(box_num).at(m_search_resource_list(i)).size()>0){
            caping_sequence.push_back(m_search_resource_list(i));
        }
    }
    //there is no cell found requiring caping return false, the bee should find another task
    if(caping_sequence.size()<1)
    {
        return false;
    }

    int caped_num = 0;
    Coords3D temp;
    for(int i=0; i<caping_sequence.size();i++){
        while((*capping_cells_pointer).at(box_num).at(caping_sequence.at(i)).size()>0){
            temp =(*capping_cells_pointer).at(box_num).at(caping_sequence.at(i)).at(0);
            //we need to make sure the larva is not dead
            if (m_celltype(box_num, temp.x,temp.y,temp.z)!=(int)(CellType::DeadBody)){
                if((*a_wax)>=cfg_HoneyBeeWaxForCapingACell.value()){
                    int a = m_celltype(box_num, temp.x,temp.y,temp.z);
                    m_task_status(box_num, temp.x,temp.y,temp.z)=TaskStatus::Ongoing;
                    //when the cell is with a pupa
                    //if(m_celltype(box_num, temp.x,temp.y,temp.z)==(int)(CellType::Worker_Larva)||m_celltype(box_num, temp.x,temp.y,temp.z)==(int)(CellType::Drone_Larva)){
                    if(m_celltype(box_num, temp.x,temp.y,temp.z)==(int)(CellType::Worker_Pupa)||m_celltype(box_num, temp.x,temp.y,temp.z)==(int)(CellType::Drone_Pupa)){
                        HoneyBee_WorkerPupa* current_pupa =dynamic_cast<HoneyBee_WorkerPupa*>(m_bee_pointers(box_num, temp.x,temp.y,temp.z));
                        current_pupa->setCapBroodDone();
                        current_pupa->setAdultBeePointer(the_bee);
                        //varroa
                        if(the_bee->m_MyBiologicalAgent->getVarroa()>0){
                            if(g_rand_uni()<=cfg_HoneyBeeProbaVarroaFromAdultToPupa.value()){
                                the_bee->m_MyBiologicalAgent->dropVarroaToPupa(current_pupa);
                            }
                        }
                    }
                    // when the cell is with nectar
                    if(m_celltype(box_num, temp.x,temp.y,temp.z)==(int(CellType::Nectar))){
                        m_honey_cells.at(box_num).at(temp.y).push_back(temp);
                        m_celltype(box_num, temp.x, temp.y, temp.z) = (int)CellType::Honey;
                        m_honey(box_num, temp.x, temp.y, temp.z,0)=m_nectar(box_num, temp.x, temp.y, temp.z, 0)*m_nectar(box_num, temp.x, temp.y, temp.z, 1)/cfg_HoneyBeeSugarConcentrationInHoney.value();
                        m_honey(box_num, temp.x, temp.y, temp.z,1)=cfg_HoneyBeeSugarConcentrationInHoney.value();
                        m_nectar(box_num, temp.x, temp.y, temp.z)=0;
                        //set bee pointer
                        m_bee_pointers(box_num, temp.x, temp.y, temp.z)=the_bee;
                    }
                    (*a_wax) -= cfg_HoneyBeeWaxForCapingACell.value();
                    m_task_status(box_num, temp.x,temp.y,temp.z)=TaskStatus::Finished;
                    m_required_tasks(box_num, temp.x,temp.y,temp.z)=TaskType::Null;
                    m_task_status(box_num, temp.x,temp.y,temp.z)=TaskStatus::Null;
                    (*capping_cells_pointer).at(box_num).at(caping_sequence.at(i)).at(0)=(*capping_cells_pointer).at(box_num).at(caping_sequence.at(i)).back();
                    (*capping_cells_pointer).at(box_num).at(caping_sequence.at(i)).pop_back();
                    caped_num +=1;
                    if(caped_num>=caping_num){
                        //change the worker's position
                        moveWorkerBee(the_bee, box_num, temp.x, temp.y, temp.z);
                        return true;
                    }
                }
                else{
                    return false;
                }
            }
            //the pupa is dead, remove it from the pool
            else{
                (*capping_cells_pointer).at(box_num).at(caping_sequence.at(i)).at(0)=(*capping_cells_pointer).at(box_num).at(caping_sequence.at(i)).back();
                (*capping_cells_pointer).at(box_num).at(caping_sequence.at(i)).pop_back();
            }
        }
    }

    if(caped_num>=caping_num){
        //change the worker's position
        moveWorkerBee(the_bee, box_num, temp.x, temp.y, temp.z);
        return true;
    }
    else {
        return false;
    }
}

/** \todo Need to move the bee between different boxes */
void Hive::moveWorkerBee(HoneyBee_Worker* the_bee, int box_num, int a_x, int a_y, int a_z){
    decBeeCount(the_bee->supplyBoxNum(), the_bee->Supply_m_Location_x(), the_bee->Supply_m_Location_y(), the_bee->Supply_m_Location_z());
    the_bee->SetBoxNum(box_num);
    the_bee->SetX(a_x);
    the_bee->SetY(a_y);
    the_bee->SetZ(a_z);
    incBeeCount(box_num,a_x,a_y,a_z);
}

bool Hive::feedLarva(int box_num, int current_y, HoneyBee_Worker* the_bee, int req_feeding_num, double* a_sugar, double* a_pollen, double* a_jelly){
    //first to generate a list to search for larva
    generateSearchingList(current_y);
    vector<int> feeding_sequence;
    for(int i=0; i<yrange;i++){
        if (m_larva_cells.at(box_num).at(m_search_resource_list(i)).size()>0){
            feeding_sequence.push_back(m_search_resource_list(i));
        }
    }
    //there is no lavar cell found return false, the bee should find another task
    if(feeding_sequence.size()<1)
    {
        return false;
    }
    Coords3D temp;
    int feeded_num = 0;
    for(int i=0; i<feeding_sequence.size();i++){
        while(m_larva_cells.at(box_num).at(feeding_sequence.at(i)).size()>0){
            temp =m_larva_cells.at(box_num).at(feeding_sequence.at(i)).at(0);
            //we need to make sure the larva is not dead
            if (m_celltype(box_num,temp.x,temp.y,temp.z)==(int)(CellType::Worker_Larva)||m_celltype(box_num,temp.x,temp.y,temp.z)==(int)(CellType::Drone_Larva)){
                m_task_status(box_num,temp.x,temp.y,temp.z)=TaskStatus::Ongoing;
                HoneyBee_WorkerLarva* current_larva =dynamic_cast<HoneyBee_WorkerLarva*>(m_bee_pointers(box_num,temp.x,temp.y,temp.z));
                *a_sugar = current_larva->supplyReqSugar();
                *a_pollen = current_larva->supplyReqPollen();
                double current_req_jelly = current_larva->supplyReqJelly();
                //if this bee has enough royall jelly, finish the task
                if(current_req_jelly<=(*a_jelly)){
                    current_larva->feedLarva(*a_sugar, *a_pollen, current_req_jelly);
                    *a_jelly-=current_req_jelly;
                    m_task_status(box_num,temp.x,temp.y,temp.z)=TaskStatus::Finished;
                    m_required_tasks(box_num,temp.x,temp.y,temp.z)=TaskType::Null;
                    m_task_status(box_num,temp.x,temp.y,temp.z)=TaskStatus::Null;
                    m_larva_cells.at(box_num).at(feeding_sequence.at(i)).at(0)=m_larva_cells.at(box_num).at(feeding_sequence.at(i)).back();
                    m_larva_cells.at(box_num).at(feeding_sequence.at(i)).pop_back();
                }
                //royal jelly is not enough, needs another bee continuing the task
                else{
                    current_larva->feedLarva(*a_sugar, *a_pollen, *a_jelly);
                    //nothing left
                    *a_jelly=0.0;
                    m_task_status(box_num,temp.x,temp.y,temp.z)=TaskStatus::Demanding;
                }
                feeded_num++;
                if(feeded_num>=req_feeding_num){
                    //change the worker's position
                    moveWorkerBee(the_bee, box_num, temp.x, temp.y, temp.z);
                    return true;
                }
            }
            //if it is dead, remove it from feeding list
            //if (m_celltype(box_num,temp.x,temp.y,temp.z)==(int)(CellType::DeadBody))
            else{
                m_larva_cells.at(box_num).at(feeding_sequence.at(i)).at(0)=m_larva_cells.at(box_num).at(feeding_sequence.at(i)).back();
                m_larva_cells.at(box_num).at(feeding_sequence.at(i)).pop_back();
            }
        }
    }
    
    if(feeded_num>=req_feeding_num){
        //change the worker's position
        moveWorkerBee(the_bee, box_num, temp.x, temp.y, temp.z);
        return true;
    }
    else {
        return false;
    }    
}


void Hive::addPesticidePollen(int box_num, Coords3D temp, int a_poly_id, double a_mount){
    /** \todo we may need to consider the biomass here. */ 
    for(int p=0;p<l_pest_NoPPPs.value();p++){
        m_pest_seeds_coating(box_num, temp.x, temp.y, temp.z, p) = m_pest_application(box_num, temp.x, temp.y,temp.z, p)+m_TheLandscape->SupplyPesticidePollen(a_poly_id, PlantProtectionProducts(p))*cfg_PestFromSeedCoatingToPollen.value()*a_mount;
    }
    for(int p=0;p<l_pest_NoPPPs.value();p++){
        m_pest_application(box_num, temp.x, temp.y, temp.z, p) = m_pest_application(box_num, temp.x, temp.y,temp.z, p)+m_TheLandscape->SupplyPesticidePollen(a_poly_id, PlantProtectionProducts(p))*cfg_PestFromApplicationToPollen.value()*a_mount;
    }
}

void Hive::addPesticideNectar(int box_num, Coords3D temp, int a_poly_id, double a_mount){
    /** \todo we may need to consider the biomass here. */ 
    for(int p=0;p<l_pest_NoPPPs.value();p++){
        m_pest_seeds_coating(box_num, temp.x, temp.y, temp.z, p) = m_pest_application(box_num, temp.x, temp.y,temp.z, p)+m_TheLandscape->SupplyPesticideNectar(a_poly_id, PlantProtectionProducts(p))*cfg_PestFromSeedCoatingToNectar.value()*a_mount;
    }
    for(int p=0;p<l_pest_NoPPPs.value();p++){
        m_pest_application(box_num, temp.x, temp.y, temp.z, p) = m_pest_application(box_num, temp.x, temp.y,temp.z, p)+m_TheLandscape->SupplyPesticideNectar(a_poly_id, PlantProtectionProducts(p))*cfg_PestFromApplicationToNectar.value()*a_mount;
    }
}

void Hive::addContactingPesticide(HoneyBee_Worker* the_bee, int a_poly_id){
    for(int p=0;p<l_pest_NoPPPs.value();p++){
        the_bee->addBeePesticideApplication(p, m_TheLandscape->SupplyPesticidePlantSurface(a_poly_id, PlantProtectionProducts(p))*cfg_HoneyBeeContactingPestRateBee.value());
    }
}

bool Hive::storeResourceToCell(int resource_type, int box_num, int storing_y, HoneyBee_Worker* the_bee, PollenNectarInfo a_resource){
    std::vector<std::vector<std::vector<Coords3D>>>* p_resource_cells;
    blitz::Array<double, 5>* p_resource;
    std::vector<std::vector<std::vector<Coords3D>>>* p_marked_for_resource_cells;
    double foraged_amount;

    //first move the bee back into the hive, if it comes back from out
    if(the_bee->Supply_m_Location_z()<0){
        the_bee->SetX(0);
        the_bee->SetY(yrange/2);
        the_bee->SetZ(zrange/2);
        incBeeCount(the_bee->supplyBoxNum(), the_bee->Supply_m_Location_x(), the_bee->Supply_m_Location_y(), the_bee->Supply_m_Location_z());
    }

    //nectar
    if(resource_type==2){
        p_resource_cells =&m_nectar_cells;
        p_resource = &m_nectar;
        p_marked_for_resource_cells = &m_marked_for_nectar_cells;
        foraged_amount = cfg_HoneyBeeVolumeForagedNectarOneTime.value();
    }
    //pollen
    if(resource_type==3){
        p_resource_cells =&m_pollen_cells;
        p_resource = &m_pollen;
        p_marked_for_resource_cells = &m_marked_for_pollen_cells;
        foraged_amount = cfg_HoneyBeeVolumeForagedPollenOneTime.value();
    }
    generateSearchingList(storing_y);
    vector<int> storing_sequence;
    for(int i=0; i<yrange;i++){
        if ((*p_marked_for_resource_cells).at(box_num).at(m_search_resource_list(i)).size()>0){
            storing_sequence.push_back(m_search_resource_list(i));
        }
    }
    //there is no available cell found to store the resource
    if(storing_sequence.size()<1)
    {
        return false;
    }
    for(int i=0; i<storing_sequence.size();i++){
        while((*p_marked_for_resource_cells).at(box_num).at(storing_sequence.at(i)).size()>0){
            Coords3D temp =(*p_marked_for_resource_cells).at(box_num).at(storing_sequence.at(i)).at(0);
            m_bee_pointers(box_num, temp.x, temp.y, temp.z)=the_bee;
            //move the bee's position
            decBeeCount(the_bee->supplyBoxNum(), the_bee->Supply_m_Location_x(), the_bee->Supply_m_Location_y(), the_bee->Supply_m_Location_z());
            the_bee->SetBoxNum(box_num);
            the_bee->SetX(temp.x);
            the_bee->SetY(temp.y);
            the_bee->SetZ(temp.z);
            incBeeCount(the_bee->supplyBoxNum(), the_bee->Supply_m_Location_x(), the_bee->Supply_m_Location_y(), the_bee->Supply_m_Location_z());
            //is the cell full?
            double available_volume = cfg_HoneyBeeHoneycell_Volume.value() - (*p_resource)(box_num,temp.x,temp.y,temp.z,0);
            //contacting exposure
            addContactingPesticide(the_bee, a_resource.m_poly_id);
            //set nosema
            if(g_rand_uni()<cfg_HoneyBeeProbaNosemaFromStoringResource.value()){
                if(the_bee->m_MyBiologicalAgent->getNosemaA()>0){
                    setNosemaCell(box_num, temp.x, temp.y, temp.z, 0, true);
                }
                if(the_bee->m_MyBiologicalAgent->getNosemaC()>0){
                    setNosemaCell(box_num, temp.x, temp.y, temp.z, 1, true);
                }
            }
            //enough space left for the resource
            if(available_volume > foraged_amount){
                (*p_resource)(box_num, temp.x,temp.y,temp.z,0)+=foraged_amount;
                m_bee_pointers(box_num, temp.x, temp.y, temp.z)=the_bee;
                //pollen
                if(resource_type==3){
                    /** \todo we may need to change here for multiple types of pollen. */ 
                    (*p_resource)(box_num,temp.x,temp.y,temp.z,1) = 1;//a_resource.m_quality;
                    addPesticidePollen(box_num, temp, a_resource.m_poly_id, foraged_amount);
                    return true;
                }
                //nectar, update the sugar concentration
                if(resource_type==2){
                    (*p_resource)(box_num, temp.x,temp.y,temp.z,1) = ((a_resource.m_quality*foraged_amount)+(*p_resource)(box_num,temp.x,temp.y,temp.z,1)*(*p_resource)(box_num,temp.x,temp.y,temp.z,0))/((*p_resource)(box_num,temp.x,temp.y,temp.z,0)+foraged_amount);
                    addPesticideNectar(box_num, temp, a_resource.m_poly_id, foraged_amount);
                    return true;
                }                
            }
            if(available_volume<=foraged_amount){
                foraged_amount-=available_volume;
                m_bee_pointers(box_num, temp.x, temp.y, temp.z)=the_bee;
                if(resource_type==2){
                    m_celltype(box_num,temp.x,temp.y,temp.z)=(int)CellType::Nectar;
                    (*p_resource)(box_num,temp.x,temp.y,temp.z,0)+=available_volume;
                    (*p_resource)(box_num,temp.x,temp.y,temp.z,1) = ((a_resource.m_quality*available_volume)+(*p_resource)(box_num,temp.x,temp.y,temp.z,1)*(*p_resource)(box_num,temp.x,temp.y,temp.z,0))/((*p_resource)(box_num,temp.x,temp.y,temp.z,0)+available_volume);
                    (*p_resource)(box_num,temp.x,temp.y,temp.z,2) = 0;
                     /** \todo we may need to consider the biomass here. */ 
                    addPesticideNectar(box_num,temp, a_resource.m_poly_id, available_volume);
                }
                if(resource_type==3){
                    m_celltype(box_num,temp.x,temp.y,temp.z)=(int)CellType::Pollen;
                    (*p_resource)(box_num,temp.x,temp.y,temp.z,0)+=available_volume;
                    /** \todo we may need to change here for multiple types of pollen. */ 
                    (*p_resource)(box_num,temp.x,temp.y,temp.z,1) = 1;//a_resource.m_quality;
                    addPesticidePollen(box_num,temp, a_resource.m_poly_id, available_volume);
                }
                //remove the marked cell
                (*p_marked_for_resource_cells).at(box_num).at(storing_sequence.at(i)).at(0)=(*p_marked_for_resource_cells).at(box_num).at(storing_sequence.at(i)).back();
                (*p_marked_for_resource_cells).at(box_num).at(storing_sequence.at(i)).pop_back();
                //add to the resource cells
                (*p_resource_cells).at(box_num).at(storing_sequence.at(i)).push_back(temp);
                //the resource has been stored ssuccefully
                if(foraged_amount<=0){
                    return true;
                }
            }
        }
    }
    //there is no availabe space, the resource is disposed
    return false;
}

bool Hive::storePollenToCell(PollenNectarInfo a_resource,HoneyBee_Worker* the_bee, int box_num){
    return storeResourceToCell(3, box_num, yrange/2, the_bee, a_resource);
}

bool Hive::storeNectarToCell(PollenNectarInfo a_resource, HoneyBee_Worker* the_bee, int box_num){
    //always start with the center
    return storeResourceToCell(2, box_num, yrange/2, the_bee, a_resource);
}

int Hive::layEggsInCells(int egg_num, int box_num, int current_frame_side){
    generateSearchingList(current_frame_side);
    //the sequence of frame side used to lay eggs
    vector<int> laying_sequence;
    for(int i=0; i<yrange;i++){
        if (m_egg_cells.at(box_num).at(i).size()>0){
            laying_sequence.push_back(i);
        }
    }
    //there is no honey found, later we  need to change this, because they can also eat nectar
    if(laying_sequence.size()<1)
    {
        return 0; // there is no place for laying eggs, therefore, no egg could be laid
    }

    int eggs_laid_count = 0;

    for(int i=0; i<laying_sequence.size();i++){
        while(m_egg_cells.at(box_num).at(laying_sequence.at(i)).size()>0){
            Coords3D temp =m_egg_cells.at(box_num).at(laying_sequence.at(i)).at(0);
            //check the queen should lay 
            int current_cell_type = m_celltype(box_num,temp.x,temp.y,temp.z);
            //worker
            struct_HoneyBee sp;
            sp.BPM = m_our_population_manager;
            sp.L = m_TheLandscape;
            sp.x = temp.x;
            sp.y = temp.y;
            sp.z = temp.z;
            sp.box_num = box_num;
            sp.age = 0;
            sp.BA = NULL;
            sp.m_sugar = 0;
            sp.m_pollen =0;
            sp.m_developmentTime=0;
            sp.m_inhive = true;

            if (current_cell_type == (int) CellType::Worker_Egg){
                //create worker egg 
                m_our_population_manager->CreatObject<HoneyBee_WorkerEgg, HoneyBee_WorkerEgg>(NULL, &sp);
                m_celltype(box_num, temp.x, temp.y,temp.z) = (int)CellType::Worker_Egg_Layed;                
            }

            //drone
            if(current_cell_type == (int) CellType::Drone_Egg){
                //create worker egg
                m_our_population_manager->CreatObject<HoneyBee_DroneEgg, HoneyBee_DroneEgg>(NULL, &sp);
                m_celltype(box_num, temp.x, temp.y,temp.z) = (int)CellType::Drone_Egg_Layed;                
            }
            
            eggs_laid_count += 1;
            //remove the cell from the availabe egg cell list
            m_egg_cells.at(box_num).at(laying_sequence.at(i)).at(0)=m_egg_cells.at(box_num).at(laying_sequence.at(i)).back();
            m_egg_cells.at(box_num).at(laying_sequence.at(i)).pop_back();
            //std::cout<<eggs_laid_count<<" eggs layed."<< egg_num<<endl;
            //is the queen done for this time?
            if(eggs_laid_count>=15){
                return 0;
            }     
        }
    }
    
    //there is not enough cell for the required egg numbers, so return the laid egg number
    return eggs_laid_count;
}

void Hive::removeQueenExluder(int box_num){
    m_box_info[box_num].queen_excluder_flag=false;
}

void Hive::initialiseHive(int box_count, int x, int y, int z, const float cellRadius, const float frameSpacing, const float sideOffset){
    //set the maximum number of allowed boxes in the colony
    m_num_of_allowed_box = cfg_HoneyBeeMaxBoxNumPerColony.value();
    //initialise the num of used boxes
    m_num_of_used_box = box_count;
    //initialise the used boxes
    for (int i=0; i<box_count; i++){
        m_box_info[i].in_use_flag = true;
        m_box_info[i].box_num = i;
        m_box_info[i].dim_x=x;
        m_box_info[i].dim_y=2*y;
        m_box_info[i].dim_z=z;
        m_box_info[i].queen_excluder_flag=true;
    }

    //By default, the queen is always allowed to lay eggs in the first box, i.e., the bottom box
    m_box_info[0].queen_excluder_flag=false;
    
    //set the unused flag for the unused boxes
    if (box_count<cfg_HoneyBeeMaxBoxNumPerColony.value()){
        for(int i=box_count; i<cfg_HoneyBeeMaxBoxNumPerColony.value(); i++){
            m_box_info[i].in_use_flag = false;
        }
    }
   
    //this is the first bottom box's dimension
    xrange=x;
    yrange=2*y; //we need to double y since there are two sides for one frame
    zrange=z;

    m_search_resource_list.resize(yrange);
    m_pest_application.resize(m_num_of_allowed_box, xrange,yrange,zrange,l_pest_NoPPPs.value());
    m_degradation_rate_pest_application.resize(l_pest_NoPPPs.value());
    //m_pest_application_resource.resize(m_num_of_allowed_box, xrange,yrange,zrange,l_pest_NoPPPs.value());
    m_pest_seeds_coating.resize(m_num_of_allowed_box, xrange,yrange,zrange,l_pest_NoPPPs.value());
    m_degradation_rate_pest_seed_coating.resize(l_pest_NoPPPs.value());
    //m_pest_seeds_coating_resource.resize(xrange,yrange,zrange,l_pest_NoPPPs.value());
    m_bee_pointers.resize(m_num_of_allowed_box, xrange,yrange,zrange);
    m_required_tasks.resize(m_num_of_allowed_box, xrange,yrange,zrange);
    m_required_tasks = TaskType::Null;
    m_task_status.resize(m_num_of_allowed_box, xrange,yrange,zrange);
    m_task_status = TaskStatus::Null;
    m_nectar.resize(m_num_of_allowed_box, xrange,yrange,zrange,3);//for now, the fourth is resized to 3 for that besides volume and sugar concentration now, the third one is the counter of the time step from the cell is full of nectar, after which it will start to become honey
    m_nectar = 0; //we initialize every thing to 0
    m_honey.resize(m_num_of_allowed_box,xrange,yrange,zrange,2);
    m_honey = 0;
    m_pollen.resize(m_num_of_allowed_box, xrange,yrange,zrange,2);
    m_pollen = 0;
    m_beeCount.resize(m_num_of_allowed_box, xrange,yrange,zrange);
    m_pollen = 0;
    m_celltype.resize(m_num_of_allowed_box, xrange,yrange,zrange);
    m_celltype = (int)CellType::Empty;
    m_hive_temperature.resize(xrange,yrange,zrange);
    m_hive_temperature = 0;
    m_nosema.resize(m_num_of_allowed_box, xrange,yrange,zrange,2); //we have two types of Nosema
    m_nosema=false;
    m_dist_to_hive_centre.resize(xrange,yrange,zrange);
    m_dist_to_hive_centre=0;
    m_global_waste_count = 0;
    m_nosema_apis_global_flag = false;
    m_nosema_ceranae_global_flag = false;

    m_available_job.resize(EnumClassAsIndex(TaskType::Count));
    m_available_job = 0;

    //All of these are used during winter when there is only one box
    m_group_bees_temperature_sum.resize(xrange,yrange,zrange);
    m_group_bees_temperature_sum=0;
    m_average_body_temmperature.resize(xrange,yrange,zrange);
    m_average_body_temmperature=0;

    //initialize the vectors used to track the availabe resource in the hive
    for (int i=0; i<m_num_of_allowed_box; i++){
        m_egg_cells.push_back(std::vector<std::vector<Coords3D>>());
        m_honey_cells.push_back(std::vector<std::vector<Coords3D>>());
        m_pollen_cells.push_back(std::vector<std::vector<Coords3D>>());
        m_nectar_cells.push_back(std::vector<std::vector<Coords3D>>());
        m_larva_cells.push_back(std::vector<std::vector<Coords3D>>());
        m_capping_brood.push_back(std::vector<std::vector<Coords3D>>());
        m_capping_nectar.push_back(std::vector<std::vector<Coords3D>>());
        m_req_cleaning_cells.push_back(std::vector<std::vector<Coords3D>>());
        m_marked_for_nectar_cells.push_back(std::vector<std::vector<Coords3D>>());
        m_marked_for_pollen_cells.push_back(std::vector<std::vector<Coords3D>>());
    }
    for (int i=0; i<m_num_of_allowed_box; i++){
        for (int j = 0; j<yrange; j++){
            m_egg_cells.at(i).push_back(std::vector<Coords3D>());
            m_honey_cells.at(i).push_back(std::vector<Coords3D>());
            m_pollen_cells.at(i).push_back(std::vector<Coords3D>());
            m_nectar_cells.at(i).push_back(std::vector<Coords3D>());
            m_larva_cells.at(i).push_back(std::vector<Coords3D>());
            m_capping_brood.at(i).push_back(std::vector<Coords3D>());
            m_capping_nectar.at(i).push_back(std::vector<Coords3D>());
            m_req_cleaning_cells.at(i).push_back(std::vector<Coords3D>());
            m_marked_for_nectar_cells.at(i).push_back(std::vector<Coords3D>());
            m_marked_for_pollen_cells.at(i).push_back(std::vector<Coords3D>());
        }
    }

    //the distance of each cell in the first bottom box to the center of the first bottom box
    int center_x=xrange/2;
    int center_y=2*yrange/2;
    int center_z=zrange/2;
    double max_half_side;
    (center_x>center_y)?(max_half_side=center_x):(max_half_side=center_y);
    (max_half_side>center_z)?(max_half_side):(max_half_side=center_z);
    for (int k=0;k<yrange;k++){
        for (int i=0;i<xrange;i++){
            for (int j=0;j<zrange;j++){
                m_dist_to_hive_centre(i,k,j)=sqrt(double(pow((i-center_x),2)+pow(2*k-center_y,2)+pow(j-center_z,2)));
            }
        }
    }

    /*The codes below are used to initialise a hive with one box to begin the simulation.*/

    //In the beginning of the simulation in Jan., there is only one box
    //first we mark all the cells as marked for nectar, then change it to different types based on its distance to the centre of the 
    int temp_box_num = 0;
    m_celltype = (int)(CellType::MarkedNectar);
    for (int i=0; i<xrange; i++){
        for (int j=0; j<yrange; j++){
            for (int k=0; k<zrange; k++){
                if(m_dist_to_hive_centre(i,j,k)<=3*max_half_side/8){
                    m_celltype(temp_box_num, i,j,k)=(int)(CellType::Worker_Egg);
                    if(g_rand_uni()<0.01){
                        m_celltype(temp_box_num, i,j,k)=(int)(CellType::Drone_Egg);
                    }
                }
                if(m_dist_to_hive_centre(i,j,k)>3*max_half_side/8&&m_dist_to_hive_centre(i,j,k)<=1*max_half_side/2){
                    if(g_rand_uni()<0.7){
                        m_celltype(temp_box_num, i,j,k)=(int)(CellType::Worker_Egg);
                        if(g_rand_uni()<0.01){
                            m_celltype(temp_box_num, i,j,k)=(int)(CellType::Drone_Egg);
                        }
                    }
                    else{
                        m_celltype(temp_box_num, i,j,k)=(int)(CellType::MarkedPollen);
                    }
                }
                if(m_dist_to_hive_centre(i,j,k)>max_half_side/2&&m_dist_to_hive_centre(i,j,k)<=5*max_half_side/8){
                    m_celltype(temp_box_num, i,j,k)=(int)(CellType::MarkedPollen);
                
                }
                if(m_dist_to_hive_centre(i,j,k)>5*max_half_side/8&&m_dist_to_hive_centre(i,j,k)<=6*max_half_side/8){
                    if(g_rand_uni()<0.7){
                        m_celltype(temp_box_num, i,j,k)=(int)(CellType::MarkedPollen);
                    }
                }
            }
        }
    }
    //put some honey and pollen for initialization,
    int middle_frame_side = (yrange+1)/2;
    for (int i=0; i<xrange; i++){
        for (int j=middle_frame_side-1;j<=middle_frame_side+1;j++){
            for (int k=0; k<zrange;k++){
                if(m_celltype(temp_box_num, i,j,k)==(int)(CellType::MarkedNectar)){
                    m_honey(temp_box_num, i,j,k,0)=cfg_HoneyBeeHoneycell_Volume.value();
                    m_honey(temp_box_num, i,j,k,1)=cfg_HoneyBeeSugarConcentrationInHoney.value();
                    m_celltype(temp_box_num, i,j,k)=(int)(CellType::Honey);
                }
                if(m_celltype(temp_box_num, i,j,k)==(int)(CellType::MarkedPollen)){
                    m_pollen(temp_box_num, i,j,k,0)=cfg_HoneyBeeHoneycell_Volume.value();;
                    m_pollen(temp_box_num, i,j,k,1)=1;//default type
                    m_celltype(temp_box_num, i,j,k)=(int)(CellType::Pollen);
                }
            }
        }
    }
    /**
    //put some honey for initialization,
    int middle_frame_side = (yrange+1)/2;
    m_honey(blitz::Range::all(),middle_frame_side+1,blitz::Range::all(), 0)=cfg_HoneyBeeHoneycell_Volume.value();
    m_honey(blitz::Range::all(),middle_frame_side+1,blitz::Range::all(), 1)=0.8;
    m_celltype(blitz::Range::all(),middle_frame_side+1,blitz::Range::all())=(int)(CellType::Honey);
    m_honey(blitz::Range::all(),middle_frame_side-2,blitz::Range::all(), 0)=cfg_HoneyBeeHoneycell_Volume.value();
    m_honey(blitz::Range::all(),middle_frame_side-2,blitz::Range::all(), 1)=0.8;
    m_celltype(blitz::Range::all(),middle_frame_side-2,blitz::Range::all())=(int)(CellType::Honey);
    //put some pollen for initialization
    for (int i = middle_frame_side-1; i<=middle_frame_side;i++){
        m_honey(blitz::Range::all(),i,blitz::Range::all(), 0)=cfg_HoneyBeeHoneycell_Volume.value();
        m_honey(blitz::Range::all(),i,blitz::Range::all(), 1)=0.8;
        m_celltype(blitz::Range::all(),i,blitz::Range::all())=(int)(CellType::Honey);
        m_honey(blitz::Range((int)(xrange/4),(int)(3*xrange/4)), i,blitz::Range((int)(zrange/4),(int)(3*zrange/4)),0)=0;
        m_honey(blitz::Range((int)(xrange/4),(int)(3*xrange/4)), i,blitz::Range((int)(zrange/4),(int)(3*zrange/4)),1)=0;
        m_pollen(blitz::Range((int)(xrange/4),(int)(3*xrange/4)), i,blitz::Range((int)(zrange/4),(int)(3*zrange/4)),0)=cfg_HoneyBeeHoneycell_Volume.value();
        m_pollen(blitz::Range((int)(xrange/4),(int)(3*xrange/4)), i,blitz::Range((int)(zrange/4),(int)(3*zrange/4)),1)=1;//default type
        m_celltype(blitz::Range((int)(xrange/4),(int)(3*xrange/4)), i,blitz::Range((int)(zrange/4),(int)(3*zrange/4)))=(int)(CellType::Pollen);
        //mark for laying eggs
        m_pollen(blitz::Range((int)(3*xrange/8),(int)(5*xrange/8)), i,blitz::Range((int)(3*zrange/8),(int)(5*zrange/8)),0)=0;//default type
        m_pollen(blitz::Range((int)(3*xrange/8),(int)(5*xrange/8)), i,blitz::Range((int)(3*zrange/8),(int)(5*zrange/8)),1)=0;
        m_celltype(blitz::Range((int)(3*xrange/8),(int)(5*xrange/8)), i,blitz::Range((int)(3*zrange/8),(int)(5*zrange/8)))=(int)(CellType::Worker_Egg);
    }
    */


    //update the resource list
    for (int k=0;k<yrange;k++){
        for (int i=0;i<xrange;i++){
            for (int j=0;j<zrange;j++){
                if (m_celltype(temp_box_num,i,k,j)==(int)CellType::Honey){
                    m_honey_cells.at(temp_box_num).at(k).push_back(Coords3D(i,k,j));
                } 
                if (m_celltype(temp_box_num,i,k,j)==(int)CellType::Pollen){
                    m_pollen_cells.at(temp_box_num).at(k).push_back(Coords3D(i,k,j));
                }
                if (m_celltype(temp_box_num,i,k,j)==(int)CellType::Worker_Egg || m_celltype(temp_box_num,i,k,j)==(int)CellType::Drone_Egg){
                    m_egg_cells.at(temp_box_num).at(k).push_back(Coords3D(i,k,j));
                }
                if (m_celltype(temp_box_num,i,k,j)==(int)CellType::MarkedNectar){
                    m_marked_for_nectar_cells.at(temp_box_num).at(k).push_back(Coords3D(i,k,j));
                }
                if (m_celltype(temp_box_num,i,k,j)==(int)CellType::MarkedPollen){
                    m_marked_for_pollen_cells.at(temp_box_num).at(k).push_back(Coords3D(i,k,j));
                }
            }
        }
    }

    //calculation of the degradation rate
    m_seedcoating_daily_decay_frac_hive=pow(10.0, log10(0.5)/(double)l_seed_coating_half_life_in_cell.value());
    m_pesticide_daily_decay_frac_hive=pow(10.0, log10(0.5)/(double)(l_pest_half_life_in_cell.value()));
}

void Hive::addBox(const unsigned x, const unsigned y, const unsigned z){
    //if we already have the maximum allowed number of boxes, return without doing anything
    if (m_num_of_used_box+1>=cfg_HoneyBeeMaxBoxNumPerColony.value()){
        return;
    }
    //if it is still allowed to have one more box, then add it
    else{
        m_box_info[m_num_of_used_box].box_num=m_num_of_used_box;
        m_box_info[m_num_of_used_box].dim_x=x;
        m_box_info[m_num_of_used_box].dim_y=2*y;
        m_box_info[m_num_of_used_box].dim_z=z;
        m_box_info[m_num_of_used_box].in_use_flag=true;
        m_box_info[m_num_of_used_box].queen_excluder_flag=true;
        m_num_of_used_box++;
    }

}

void Hive::setBox(int box_num, const unsigned x, const unsigned y, const unsigned z){
    ;
}

void Hive::clearCellTypeArray()
{
    m_celltype=(int)CellType::Empty;
}

void Hive::hatchFromCell(int box_num, int x, int y, int z, bool alive){
    if(alive){
        m_celltype(box_num,x,y,z)=(int)(CellType::Worker_Egg);
        m_egg_cells.at(box_num).at(y).push_back(Coords3D(x,y,z));
    }
    else{
        m_celltype(box_num,x,y,z)=(int)(CellType::DeadBody);
    }
}

// Change this to return blitz::TinyVector<int,3>
blitz::TinyVector<Coords3D,8>Hive::supplyNeighbours(const int x, const int y, const int z)
{
    blitz::TinyVector<Coords3D,8> neighbours;
    int temp_one_coord_x;
    int temp_one_coord_y;
    int temp_one_coord_z;;
    //from the upper row
    temp_one_coord_y = y-1;
    if(temp_one_coord_y<0){
        neighbours(0)=Coords3D(-1,-1,-1);
    }
    else{
        neighbours(0)=Coords3D(x, temp_one_coord_y, z);
    }
    //in the same row
    temp_one_coord_x=x-1;
    if(temp_one_coord_x<0){
        neighbours(1)=Coords3D(-1,-1,-1);
    }
    else{
        neighbours(1)=Coords3D(temp_one_coord_x, y, z);
    }
    temp_one_coord_x=x+1;
    if(temp_one_coord_x>xrange-1){
        neighbours(2)=Coords3D(-1,-1,-1);
    }
    else{
        neighbours(2)=Coords3D(temp_one_coord_x, y, z);
    }
    //in the row below
    temp_one_coord_z=z+1;
    if(temp_one_coord_z>zrange-1){
        neighbours(3)=Coords3D(-1,-1,-1);
        neighbours(4)=Coords3D(-1,-1,-1);
        neighbours(5)=Coords3D(-1,-1,-1);
    }
    else{
        neighbours(4)=Coords3D(x, y, temp_one_coord_z);
        temp_one_coord_x=x-1;
        if(temp_one_coord_x<0){
            neighbours(3)=Coords3D(-1,-1,-1);
        }
        else{
            neighbours(3)=Coords3D(temp_one_coord_x, y, temp_one_coord_z);
        }
        temp_one_coord_x=x+1;
        if(temp_one_coord_x>xrange-1){
            neighbours(5)=Coords3D(-1,-1,-1);
        }
        else{
            neighbours(5)=Coords3D(temp_one_coord_x, y, temp_one_coord_z);
        }
    }
    //on another frame or the other frame side, we always make the final element is the neighbour on the other fameside of the same frame
    if(y-1<0 || y+1>yrange-1){
        neighbours(6)=Coords3D(-1,-1,-1);
        neighbours(7)=Coords3D(-1,-1,-1);
    }
    if(y%2==0){
        neighbours(6)=Coords3D(x,y-1,z);
        neighbours(7)=Coords3D(x,y+1,z);
    }
    else{
        neighbours(6)=Coords3D(x,y+1,z);
        neighbours(7)=Coords3D(x,y-1,z);
    }
    return neighbours;
}

void Hive::removeOneFrame(int box_num, int a_frame_id){
    for(int i=2*(a_frame_id-1); i<=2*(a_frame_id-1)+1;i++){
        m_nectar(box_num, blitz::Range::all(), i, blitz::Range::all(), blitz::Range())=0;
        m_honey(box_num, blitz::Range::all(), i, blitz::Range::all(), blitz::Range())=0;
        m_pollen(box_num,blitz::Range::all(), i, blitz::Range::all(), blitz::Range())=0;
        m_celltype(box_num,blitz::Range::all(), i, blitz::Range::all())=(int)CellType::Empty;
        m_pest_application(box_num,blitz::Range::all(), i, blitz::Range::all(), blitz::Range())=0;
        m_pest_seeds_coating(box_num,blitz::Range::all(), i, blitz::Range::all(), blitz::Range())=0;
        m_required_tasks(box_num,blitz::Range::all(), i, blitz::Range::all())=TaskType::Null;
        m_task_status(box_num,blitz::Range::all(), i, blitz::Range::all())=TaskStatus::Null;
        m_marked_for_nectar_cells.at(box_num).at(i).clear();
        m_marked_for_pollen_cells.at(box_num).at(i).clear();
        m_nectar_cells.at(box_num).at(i).clear();
        m_honey_cells.at(box_num).at(i).clear();
        m_pollen_cells.at(box_num).at(i).clear();
        m_egg_cells.at(box_num).at(i).clear();
        m_larva_cells.at(box_num).at(i).clear();
        m_capping_brood.at(box_num).at(i).clear();
        m_capping_nectar.at(box_num).at(i).clear();
        m_nosema(box_num, blitz::Range::all(), i, blitz::Range::all(), blitz::Range::all())=false;
    }
}

void Hive::fillHoneyFrameFull(int box_num, int a_frame_id){
    for(int i=2*(a_frame_id-1); i<=2*(a_frame_id-1)+1;i++){
        m_honey(box_num, blitz::Range::all(),i,blitz::Range::all(), 0)=cfg_HoneyBeeHoneycell_Volume.value();
        m_honey(box_num, blitz::Range::all(),i,blitz::Range::all(), 1)=0.8;
        m_celltype(box_num, blitz::Range::all(),i,blitz::Range::all())=(int)CellType::Honey;
        for (int k=0;k<xrange;k++){
            for (int j=0;j<zrange;j++){
                m_honey_cells.at(box_num).at(i).push_back(Coords3D(k,i,j));
            }
        }
    }
}

void Hive::addCellForCleaning(int b, int x, int y, int z){
    m_req_cleaning_cells.at(b).at(y).push_back(Coords3D(x,y,z));
}

int Hive::supplyHoneyCellNum(){
    int return_value = 0;
    for (int b=0; b<m_num_of_used_box;b++){
        for (int i=0; i<yrange;i++){
            return_value+=m_honey_cells.at(b).at(i).size();
        }
    }
    return return_value;
}

int Hive::supplyNectarCellNum(){
    int return_value = 0;
    for (int b=0; b<m_num_of_used_box;b++){
        for (int i=0; i<yrange;i++){
            return_value+=m_nectar_cells.at(b).at(i).size();
        }
    }
    return return_value;
}

int Hive::supplyPollenCellNum(){
    int return_value = 0;
    for (int b=0; b<m_num_of_used_box;b++){
        for (int i=0; i<yrange;i++){
            return_value+=m_pollen_cells.at(b).at(i).size();
        }
    }
    return return_value;
}

int Hive::supplyEggCellNum() {
    int return_value = 0;
    for (int b=0; b<m_num_of_used_box;b++){
        for (int i=0; i<yrange;i++){
            return_value+=m_egg_cells.at(b).at(i).size();
        }
    }
    return return_value;
}

int Hive::supplyLarvaeCellNum(){
    int return_value = 0;
    for (int b=0; b<m_num_of_used_box;b++){
        for (int i=0; i<yrange;i++){
            return_value+=m_larva_cells.at(b).at(i).size();
        }
    }
    return return_value;
}

int Hive::supplyPupaCellNum(){
    int return_value = 0;
    for (int b=0; b<m_num_of_used_box;b++){
        for (int x=0; x<xrange; x++){
            for (int y=0; y<yrange; y++){
                for (int z=0; z<zrange; z++){
                    if (m_celltype(b,x,y,z)==(int)(CellType::Worker_Pupa) || m_celltype(b,x,y,z)==(int)(CellType::Drone_Pupa)){
                        return_value+=1;
                    }
                }
            }
        }       
    }
    return return_value;
}

int Hive::supplyCleaningReqCellNum(){
    int return_value = 0;
    for (int b=0; b<m_num_of_used_box;b++){
        for (int i=0; i<yrange;i++){
            return_value+=m_req_cleaning_cells.at(b).at(i).size();
        }
    }
    return return_value;
}

void Hive::calAverageTemperature(){
    m_average_body_temmperature=m_group_bees_temperature_sum/(m_beeCount(0, blitz::Range::all(), blitz::Range::all(), blitz::Range::all())+1);
}

blitz::TinyVector<int,3> Hive::warmestCell(CellType cellType)
{
    blitz::Array<float,3> temp;
    temp.resize(getTemperatureArray().shape());
    temp=blitz::where((getCellTypeArray())(0, blitz::Range::all(), blitz::Range::all(), blitz::Range::all())==(int)cellType, getTemperatureArray(), 0);
    blitz::TinyVector<int,3> inds;
    inds=blitz::maxIndex(temp);
    return inds;
}

blitz::TinyVector<int,4> Hive::randomCell()
{
    blitz::TinyVector<int,4> shp=shape();
    blitz::TinyVector<int,4> r={rand()%shp(0),rand()%shp(1),rand()%shp(2),rand()%shp(3)};
    return r;
}

// Maybe not the fastest algorithm.
blitz::TinyVector<int,4> Hive::randomCell(CellType cellType)
{
    blitz::TinyVector<int,4> r=randomCell();
    while (getCellType(r) != cellType)
    {
        r=randomCell();
    }
    return r;
}