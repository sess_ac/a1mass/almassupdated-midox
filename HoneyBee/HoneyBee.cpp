/*
*******************************************************************************************************
Copyright (c) 2017, Chris J. Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee.cpp
    \brief <B>The main source code for all HoneyBee lifestage classes</B>
    Version of  Feb 2020 \n
    By Chris J. Topping, David Warren Wallis and Xiaodong Duan\n \n
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include "blitz/array.h"
#include <boost/random.hpp>
#include "../BatchALMaSS/ALMaSS_Random.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "BiologicalAgents.h"
#include "HoneyBee_Colony.h"
#include "HoneyBee.h"
#include "Hive.h"
#include "HiveThermodynamics.h"
extern MapErrorMsg *g_msg;

//These are the parameters for calculating temperature stressor
CfgFloat cfg_HoneyBeeLowestBeeComfortDegree("HONEY_BEE_LOWEST_BEE_COMFORT_DEGREE",CFG_CUSTOM,17);
CfgFloat cfg_HoneyBeeColdEndure("HONEY_BEE_COLD_ENDURE",CFG_CUSTOM,40);
CfgFloat cfg_HoneyBeeHighestComfortDegree("HONEY_BEE_HIGHEST_COMFORT_DEGREE",CFG_CUSTOM,35);
CfgFloat cfg_HoneyBeeHotEndure("HONEY_BEE_HOT_ENDURE",CFG_CUSTOM,40);
CfgFloat cfg_HoneyBeeTempStressorRecoverPerTimestep("HONEY_BEE_TEMP_STRESSOR_RECOVER_PER_TIMESTEP",CFG_CUSTOM,0.01);

//These are the parameters for development of bees, please note the development is counted from day 0 for the corresponding life stage
CfgFloat cfg_HoneyBeeWorkerEggDevelopmentTime("HONEY_BEE_WORKER_EGG_DEVELOPMENT_TIME",CFG_CUSTOM,3);
CfgFloat cfg_HoneyBeeWorkerLarvaDevelopmentTime("HONEY_BEE_WORKER_LARVA_DEVELOPMENT_TIME",CFG_CUSTOM,6);
CfgFloat cfg_HoneyBeeWorkerPupaDevelopmentTime("HONEY_BEE_WORKER_PUPA_DEVELOPMENT_TIME",CFG_CUSTOM,12);
CfgFloat cfg_HoneyBeeWorkerMaxAge("HONEY_BEE_WORKER_MAX_AGE",CFG_CUSTOM, 65);
CfgFloat cfg_HoneyBeeDroneEggDevelopmentTime("HONEY_BEE_DRONE_EGG_DEVELOPMENT_TIME",CFG_CUSTOM,3);
CfgFloat cfg_HoneyBeeDroneLarvaDevelopmentTime("HONEY_BEE_DRONE_LARVA_DEVELOPMENT_TIME",CFG_CUSTOM,7);
CfgFloat cfg_HoneyBeeDronePupaDevelopmentTime("HONEY_BEE_DRONE_PUPA_DEVELOPMENT_TIME",CFG_CUSTOM,13);
CfgFloat cfg_HoneyBeeDroneMaxAge("HONEY_BEE_DRONE_MAX_AGE",CFG_CUSTOM, 60);

//These are the parameters for calculating food stressor
CfgFloat cfg_HoneyBeeFoodShortageTolerance("HONEY_BEE_FOOD_SHORTAGE_TOLERANCE",CFG_CUSTOM, 0.9); //when a bee can have 0.9 of the required food, it has no stress. This is also used for development
CfgFloat cfg_HoneyBeeFoodStressorRecoverPerTimestep("HONEY_BEE_FOOD_STRESSOR_RECOVER_PER_TIMESTEP",CFG_CUSTOM,0.1);

//These are the parameters for bee development
CfgFloat cfg_HoneyBeeOptimalDevelopmentTemperature("HONEY_BEE_OPTIMAL_DEVELOPMENT_TEMPERATURE",CFG_CUSTOM,34.5);
CfgFloat cfg_HoneyBeeTemperatureDevelopmentCoefficient("HONEY_BEE_TEMPERATURE_DEVELOPMENT_COEFFICIENT",CFG_CUSTOM,0.8);//The bigger more weights on temperature.
CfgInt cfg_HoneyBeeNeedForScoutingNectarNum("HONEY_BEE_NEED_FOR_SCOUTING_NECTAR_NUM",CFG_CUSTOM,10);//When the known number of place for nectar is below this value, we need to scout
CfgInt cfg_HoneyBeeNeedForScoutingPollenNum("HONEY_BEE_NEED_FOR_SCOUTING_POLLEN_NUM",CFG_CUSTOM,10);//When the known number of place for pollen is below this value, we need to scout

//These are the parameters for pesticide
CfgFloat cfg_HoneyBeeMaxAllowedPestBurden("HONEY_BEE_MAX_ALLOWED_PEST_BURDEN", CFG_CUSTOM, 10);
CfgFloat cfg_HoneyBeeRecoveryRateFromPest("HONEY_BEE_RECOVERY_RATE_FROM_PEST", CFG_CUSTOM, 0.01);
CfgFloat cfg_HoneyBeeQueenMinBurdenPestEffect("HONEY_BEE_QUEEN_MIN_BURDEN_PEST_EFFECT", CFG_CUSTOM, 5);
CfgFloat cfg_HoneyBeeQueenMaxBurdenPestEffect("HONEY_BEE_QUEEN_MAX_BURDEN_PEST_EFFECT", CFG_CUSTOM, 10);
CfgFloat cfg_HoneyBeeQueenRecoveryRatePestEffect("HONEY_BEE_QUEEN_RECOVERY_RATE_PEST_EFFECT", CFG_CUSTOM, 0.1);
CfgFloat cfg_ConvertPesticidePerBiomassToNectar("CONVERT_PESTICIDE_PER_BIOMASS_TO_NECTAR",CFG_CUSTOM,0.1);
CfgFloat cfg_HoneyBeeGlandReductionThresholdPest("HONEY_BEE_GLAND_REDUCTION_THRESHOLD_PEST", CFG_CUSTOM, 20);
CfgFloat cfg_HoneyBeeGlandReductionRatePest("HONEY_BEE_GLAND_REDUCTION_RATE_PEST", CFG_CUSTOM, 0.85);
CfgFloat cfg_HoneyBeeGlandRecoveryRatePest("HONEY_BEE_GLAND_RECOVERY_RATE_PEST", CFG_CUSTOM, 0.1);
static CfgFloat l_seed_coating_half_life_in_bee("SEED_COATING_HALF_LIFE_IN_BEE", CFG_CUSTOM, 30.0);
static CfgFloat l_pest_half_life_in_bee("PEST_HALF_LIFE_IN_BEE", CFG_CUSTOM, 30.0);
extern CfgInt l_pest_NoPPPs;
extern CfgBool l_pest_enable_pesticide_engine;
extern CfgInt cfg_HoneyBeeStepSize;
extern CfgInt cfg_HoneyBeeResourceScaleDegree;

//These are the parameters for biological agents
CfgFloat cfg_HoneyBeeProbaInfectedNosemaFromGlobalWaste("HONEY_BEE_PROBA_INFECTED_NOSEMA_FROM_GLOBAL_WASTE", CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeIncrementFoodCausedByNosema("HONEY_BEE_INCREMENT_FOOD_CAUSED_BY_NOSEMA", CFG_CUSTOM, 1.01);
CfgFloat cfg_HoneyBeeParameterNosemaStressor("HONEY_BEE_PARAMETER_NOSEMA_STRESSOR", CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeMortalityIncrementVorroa("HONEY_BEE_MORTALITY_INCREMENT_VORROA", CFG_CUSTOM, 0.0001);
CfgFloat cfg_HoneyBeeMortalityPupaAPV("HONEY_BEE_MORTALITY_PUPA_APV", CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeMortalityPupaDWV("HONEY_BEE_MORTALITY_PUPA_DWV", CFG_CUSTOM, 0.02);
CfgFloat cfg_HoneyBeeLowestImmuneForDeath("HONEY_BEE_LOWEST_IMMUNE_FOR_DEATH", CFG_CUSTOM, 0.1);
CfgFloat cfg_HoneyBeeLargestNumDWVForDeath("HONEY_BEE_LARGEST_NUM_DWV_FOR_DEATH", CFG_CUSTOM, 20000);
CfgFloat cfg_HoneyBeeLargestNumABPForDeath("HONEY_BEE_LARGEST_NUM_ABP_FOR_DEATH", CFG_CUSTOM, 2000);
CfgFloat cfg_HoneyBeeProbaVarroaDropFromAdultToAdult("HONEY_BEE_PROBA_VARROA_DROP_FROM_ADULT_TO_ADULT", CFG_CUSTOM, 0.5);

//These are the parameters for foraging
//CfgFloat cfg_MaximumAmountPerForageNectar("MAXIMUM_AMOUNT_PER_FORAGE_NECTAR",CFG_CUSTOM,50);
//CfgFloat cfg_MaximumAmountPerForagePollen("MAXIMUM_AMOUNT_PER_FORAGE_POLLEN",CFG_CUSTOM,50);
CfgFloat cfg_HoneyBeeMaximumFlyDistancePerStep("HONEY_BEE_MAXIMUM_FLY_DISTANCE_PER_STEP",CFG_CUSTOM,4000.0);
CfgFloat cfg_HoneyBeeForagingMortalityIncrement("HONEY_BEE_FORAGING_MORTALITY_INCREMENT", CFG_CUSTOM, 0.0001);
CfgFloat cfg_HoneyBeeVolumeForagedNectarOneTime("HONEY_BEE_VOLUME_FORAGED_NECTAR_ONE_TIME", CFG_CUSTOM, 50); //mg
CfgFloat cfg_HoneyBeeVolumeForagedPollenOneTime("HONEY_BEE_VOLUME_FORAGED_POLLEN_ONE_TIME", CFG_CUSTOM, 1.5);  //mg
CfgFloat cfg_HoneyBeeMaxFlyDistancePerDay("HONEY_BEE_MAX_FLY_DISTANCE_PER_DAT",CFG_CUSTOM, 20000.0);

//These are the parameters for pesticide overspray
CfgFloat cfg_HoneyBeeBodySurface("HONEY_BEE_BODY_SURFACE", CFG_CUSTOM, 0.01);
CfgFloat cfg_HoneyBeeOverSprayRate("HONEY_BEE_OVER_SPRAY_RATE", CFG_CUSTOM, 0.9);

//These are the parameters for guarding
CfgFloat cfg_HoneyBeeGuardingMortalityIncrement("HONEY_BEE_GUARDING_MORTALITY_INCREMENT", CFG_CUSTOM, 0.01);


//These are the parameters for queen
CfgInt cfg_HoneyBeeNumOfEggsPerTimestep("HONEY_BEE_NUM_OF_EGGS_PER_TIMESTEP", CFG_CUSTOM, 20);
CfgFloat cfg_HoneyBeeMinimumTemperatureForEgg("HONEY_BEE_MINIMUM_TEMPERATURE_FOR_EGG", CFG_CUSTOM, 10);

//These are the parameters for warming up
CfgFloat cfg_HoneyBeeIncreaseTemperatureWarmingUp("HONEY_BEE_INCREASE_TEMPERATURE_WARMING_UP", CFG_CUSTOM, 15);
extern CfgFloat cfg_HoneyBeeMiniTemperatureForOutActivities;
extern CfgFloat cfg_HoneyBeeTemperatureLimitToDefineColdDays;


//These are the parameters for vitality calculation
CfgFloat cfg_HoneyBeeVitalityRecoveryRate("HONEY_BEE_VITALITY_RECOVERY_RATE", CFG_CUSTOM, 0.1);
CfgFloat cfg_HoneyBeeVitalityWeightStressor("HONEY_BEE_VITALITY_WEIGHT_STRESSOR", CFG_CUSTOM, 0.1);

extern CfgInt cfg_HoneyBeeForagingStrategy;

//******************************************************************************************************
//******************************* START HoneyBee_Base Class ********************************************
//******************************************************************************************************
void HoneyBee_Base::FillHBdata(struct_HoneyBee* a_data) {
   m_OurPopulationManager=a_data->BPM;
   m_OurLandscape=a_data->L;
   m_Age=a_data->age;
   m_Location_x=a_data->x;
   m_Location_y=a_data->y;
   m_Location_z=a_data->z;
   //m_MyBiologicalAgent=a_data->BA;
   m_development_time=a_data->m_developmentTime;
   inHive=a_data->m_inhive;
   //todo we need to add how to handle biological agents pointer here
}

void HoneyBee_Base::CopyHBdata(HoneyBee_Base* hb_source) {
   m_OurPopulationManager=hb_source->m_OurPopulationManager;
   m_OurLandscape=hb_source->m_OurLandscape;
   m_Age=hb_source->m_Age;
   m_Location_x=hb_source->m_Location_x;
   m_Location_y=hb_source->m_Location_y;
   m_Location_z=hb_source->m_Location_z;
   m_MyBiologicalAgent=hb_source->m_MyBiologicalAgent;
   //m_development_time=hb_source->m_development_time;
   inHive = hb_source->inHive;
   //todo we need to add how to handle biological angents pointer here
   
}

HoneyBee_Base::HoneyBee_Base(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : TAnimal( a_x ,  a_y , p_L)
{
   //biological agents
   m_MyBiologicalAgent = new BiologicalAgents(this);
   m_bee_task_activities.resize(EnumClassAsIndex(TaskType::Count));
   m_pest_burden_total.resize(l_pest_NoPPPs.value());
   m_pest_burden_coating.resize(l_pest_NoPPPs.value());
   m_pest_burden_intake.resize(l_pest_NoPPPs.value());
   m_pest_burden_overspray.resize(l_pest_NoPPPs.value());
   m_seedcoating_daily_decay_frac_bee=pow(10.0, log10(0.5)/(double)l_seed_coating_half_life_in_bee.value());;
   m_pesticide_daily_decay_frac_bee=pow(10.0, log10(0.5)/(double)(l_pest_half_life_in_bee.value()));
   ReInit(a_box_num, a_x, a_y, a_z,p_L, p_BPM);
   m_box_num = 0; //by default, there is only one box   
}


void HoneyBee_Base::ReInit(const int a_box_num, const int  a_x, const int  a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   TAnimal::ReinitialiseObject(a_x,a_y,p_L);
   // Assign the pointer to the population manager
   m_OurPopulationManager = p_BPM;
   
   m_sugar_required_day = 0;
   m_pollen_required_day = 0;
   m_preffered_pollen_type = 0;
   m_jelly_required_day = 0;
   m_sugar = 0;
   m_pollen = 0;
   m_jelly = 0,
   m_mortality_rate = 0.0;
   m_food_stressor = 1.0;
   m_temp_stressor = 1.0;
   m_disease_stressor = 1.0;
   m_pesticide_stressor = 1.0;
   m_vitality_value = 1.0;
   m_immune_value = 1.0;
   SetZ(a_z);
   SetBoxNum(a_box_num);
   m_development_time=999;
   m_next_stage_time=999;
   id= m_OurPopulationManager->nextID();
   m_Age = 0;
   m_CurrentStateNo=toHoneyBeeS_InitialState;
   m_CurrentHBState = toHoneyBeeS_InitialState;
   //getHive()->incBeeCount(a_x,a_y,a_z);
   inHive=true;   
   m_gland_state = ttogp_None; //This means that there is no gland product.
}

HoneyBee_Base::~HoneyBee_Base(void)
{
   //getHive()->decBeeCount(Supply_m_Location_x(),Supply_m_Location_y(),Supply_m_Location_z());
   //delete []m_MyBiologicalAgent;
}

void HoneyBee_Base::updatePesticideConcentration(){
   m_pest_burden_intake = m_pest_burden_intake*m_pesticide_daily_decay_frac_bee;
   m_pest_burden_coating = m_pest_burden_coating*m_seedcoating_daily_decay_frac_bee;
}

double HoneyBee_Base::updateRemainingTimeActivity(TaskType activity_type){
   double time_spent;
   //the beginning of the activity, initialize the required time
   if (m_required_remaining_time_current_activity < 0){
      m_required_remaining_time_current_activity = m_OurPopulationManager->supplyTimeForActivity(activity_type);
      //set the current activity is ongoing
      m_current_activity_done = false;
   }

   //for activities, e.g., guarding, bees will do all the time unless some situations happened stops it from doing it, so use all the remaining time in current time step
   if (m_required_remaining_time_current_activity < 0){
      double time_spent = m_time_left_timestep;
      m_time_left_timestep = 0;
      //set the current activity is done, if needed it can choose another activity
      m_current_activity_done = true;
   }
   //for other activities, e.g., capping brood, there is required time for that
   else{
      if(m_required_remaining_time_current_activity<=m_time_left_timestep){
         //this activity could be fininshed within the current time step, set the required remaining time for this activity to be -1 which means it has been done
         time_spent = m_required_remaining_time_current_activity;
         m_time_left_timestep -= m_required_remaining_time_current_activity;
         m_required_remaining_time_current_activity = -1;
         //set the current activity is done, the bee can choose another activity
         m_current_activity_done = true;
      }
      //we need more time for this activity, use all the time in current time step and set it to zero
      else{
         time_spent = m_time_left_timestep;
         m_required_remaining_time_current_activity -= m_time_left_timestep;
         m_time_left_timestep = 0;
      }      
   }

   //there is no time left for the current time step, so mark m_StepDone as true in order to finish current step.
   if (m_time_left_timestep == 0){
      m_time_left_timestep = cfg_HoneyBeeStepSize.value();
      m_StepDone = true;
   }
   return time_spent;
}

void HoneyBee_Base::updateVitality(){
   /** \todo Considering adding time here. */
   m_vitality_value = (cfg_HoneyBeeVitalityRecoveryRate.value()+cfg_HoneyBeeVitalityWeightStressor.value()*(m_food_stressor+m_temp_stressor+m_immune_value-1.5))*m_vitality_value;
}

void HoneyBee_Base::Step(void)
{
   if (m_StepDone || m_CurrentStateNo == -1) {
      developInStep();
      return;
   }
   switch (m_CurrentHBState)
   {
   case toHoneyBeeS_InitialState: // Initial state always starts with nothing
      //m_CurrentHBState = toHoneyBeeS_Develop;
      break;        
   //case toHoneyBeeS_Develop:
   //   m_CurrentHBState=st_Develop(); // Will return movement or die
   //   m_StepDone=true;
   //   break;
   case toHoneyBeeS_Progress:
      m_CurrentHBState = st_Progress();
      m_StepDone = true;
      break;
   case toHoneyBeeS_Die:
      st_Dying(); // No return value - no behaviour after this
      m_StepDone = true;
      break;
   default:
      m_OurLandscape->Warn("HoneyBee_Base::Step()", "unknown state - default");
      exit(1);
   }
}

void HoneyBee_Base::updateMortalityRate(){
   //for now we use the maximum value for the four stressors
   m_mortality_rate = 1-min(min(min(m_food_stressor, m_temp_stressor), m_pesticide_stressor), m_disease_stressor);
   //we use tanh function to combine all stressors into mortality rate
   m_mortality_rate = tanh(4-m_food_stressor-m_temp_stressor-m_pesticide_stressor-m_disease_stressor);
}

//This function needs to be rewritten for any class.
void HoneyBee_Base::developInStep()
{
   if (g_rand_uni() < m_mortality_rate)
      m_CurrentHBState = toHoneyBeeS_Die;
}

TTypeOfHoneyBeeState HoneyBee_Base::st_Progress(void)
{
   /**
      This creates a data object to pass on to the population manager to create a worker larva, then signals that this object should be destroyed.
   */
   nextStage();
   m_CurrentStateNo = -1;
   m_StepDone = true;
   return toHoneyBeeS_Progress; //
}

void HoneyBee_Base::st_Dying(void)
{
   m_StepDone = true;
   m_CurrentStateNo = -1; // this will kill the animal object and free up space
}


Hive * HoneyBee_Base::getHive()
{
   return m_OurPopulationManager->getHive();
}

void HoneyBee_Base::updateFoodStressor(){
   double temp_stressor = 1.0;
   double temp_rate;

   if(m_sugar_required_day>0){
      temp_rate = m_sugar/m_sugar_required_day;
      if(temp_rate<cfg_HoneyBeeFoodShortageTolerance.value()){
         temp_stressor*=temp_rate;
      }
   }
   if(m_pollen_required_day>0){
      temp_rate*= (m_pollen/m_pollen_required_day);
      if(temp_rate<cfg_HoneyBeeFoodShortageTolerance.value()){
         temp_stressor*=temp_rate;
      }
   }
   if(m_jelly_required_day>0){
      temp_rate = (m_jelly/m_jelly_required_day);
      if(temp_rate<cfg_HoneyBeeFoodShortageTolerance.value()){
         temp_stressor*=temp_rate;
      }
   }
   if(temp_stressor >= 1.0){//no new food stress within this time step, so recover from it
      m_food_stressor += cfg_HoneyBeeFoodStressorRecoverPerTimestep.value();
   }
   else{
      m_food_stressor -= (1-temp_stressor); //when a bee is full, there is no stressor
   }

   if(m_food_stressor<0){
      m_food_stressor = 0.0; //make it dead since it is fully stressed by food shortage
   }
   if(m_food_stressor>1){
      m_food_stressor=1.0;
   } 
}

void HoneyBee_Base::updateTemperatureStressor(){
   if (m_current_body_temperature<cfg_HoneyBeeLowestBeeComfortDegree.value()){
      m_temp_stressor += (cfg_HoneyBeeStepSize.value())*(m_current_body_temperature-cfg_HoneyBeeLowestBeeComfortDegree.value())/cfg_HoneyBeeColdEndure.value();
   }
   else if (m_current_body_temperature>cfg_HoneyBeeHighestComfortDegree.value()){
      m_temp_stressor += cfg_HoneyBeeStepSize.value()*(cfg_HoneyBeeHighestComfortDegree.value()-m_current_body_temperature)/cfg_HoneyBeeHotEndure.value();
   }
   else{//no new temperature stress within this time step, so recover from it
      m_temp_stressor += cfg_HoneyBeeTempStressorRecoverPerTimestep.value(); 
   }
   if(m_temp_stressor>1){
      m_temp_stressor = 1.0;
   }
   //It is fully stressed, so make it to 0.0
   if(m_temp_stressor<=0){
      m_temp_stressor = 0.0;
   }
}

double HoneyBee_Base::supplySumPestBurden(){
   return sum(m_pest_burden_total);
}

void HoneyBee_Base::updatePestStressor(){
   m_pesticide_stressor -= supplySumPestBurden()/cfg_HoneyBeeMaxAllowedPestBurden.value()+cfg_HoneyBeeRecoveryRateFromPest.value()*m_immune_value;
   if(m_pesticide_stressor<0) m_pesticide_stressor=0;
   if(m_pesticide_stressor>1) m_pesticide_stressor=1;
}

void HoneyBee_Base::updateDiseaseStressor(){
   m_disease_stressor = exp(-1.0*(m_MyBiologicalAgent->getNosemaC()+m_MyBiologicalAgent->getNosemaA()));
}

void HoneyBee_Base::updateDevelopmentTime() {
   double development_rate = 1.0;
   //temperature
   double current_change = 1.0-pow(m_current_body_temperature-cfg_HoneyBeeOptimalDevelopmentTemperature.value(),2);

   //food shortage
   double temp_rate = m_sugar/m_sugar_required_day;
   if(temp_rate<cfg_HoneyBeeFoodShortageTolerance.value()){
      current_change*=temp_rate;
   }
   temp_rate*= (m_pollen/m_pollen_required_day);
   if(temp_rate<cfg_HoneyBeeFoodShortageTolerance.value()){
      current_change*=temp_rate;
   }
   temp_rate = (m_jelly/m_jelly_required_day);
   if(temp_rate<cfg_HoneyBeeFoodShortageTolerance.value()){
      current_change*=temp_rate;
   }
   m_development_time += (cfg_HoneyBeeStepSize.value())/1440*development_rate*current_change;
}

//to do we need to add multiple types of pesticide here
bool HoneyBee_Base::OnFarmEvent( FarmToDo event ){
   bool result=false;
   switch ( event )
   {
   case product_treat:
      result=true;
      break;
   }
   return result;
}

void HoneyBee_Base::findNewVarroaHosts(){
   blitz::TinyVector<Coords3D, 8> current_neighbours = getHive()->supplyNeighbours(m_Location_x, m_Location_y, m_Location_z);
   for(int i=0;i<7;i++){
      Coords3D curruent_nei_position = current_neighbours(i);
      HoneyBee_Base* current_nei_beeP = getHive()->supplyBeePointer(m_box_num, curruent_nei_position.x, curruent_nei_position.y, curruent_nei_position.z);
      if(current_nei_beeP->myID != WorkerAdult_e || current_nei_beeP->myID != DroneAdult_e){
         current_nei_beeP = current_nei_beeP->supplyAdultBeePointer();
      }
      m_MyBiologicalAgent->dropVarroaToAdult(dynamic_cast<HoneyBee_Worker*>(current_nei_beeP));
      if(m_MyBiologicalAgent->getVarroa()<=1){
         break;
      }
   }
}
void HoneyBee_Base::setCellAfterDeadInCell(){
   getHive()->setCellType(m_box_num, m_Location_x,m_Location_y,m_Location_z,(int)CellType::DeadBody); //this for egg, larva, and Pupa, not for adults
   getHive()->addCellForCleaning(m_box_num, m_Location_x, m_Location_y, m_Location_z);
   getHive()->setCellTaskType(m_box_num, m_Location_x,m_Location_y,m_Location_z,TaskType::CleaningCell);
   getHive()->setCellTaskStatus(m_box_num, m_Location_x,m_Location_y,m_Location_z,TaskStatus::Demanding);
}

//******************************************************************************************************
//***************************** START HoneyBee_WorkerEgg Class *****************************************
//******************************************************************************************************
const CellType HoneyBee_WorkerEgg::cellType=CellType::Worker_Egg_Layed;
HoneyBee_WorkerEgg::HoneyBee_WorkerEgg(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_Base(a_box_num, a_x, a_y, a_z, p_L, p_BPM)
{
   initWorkerEgg(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
}

void HoneyBee_WorkerEgg::initWorkerEgg(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM){
   m_StepDone = true; //Because they do not have anything to do except growing to next stage, this is always true from the beginning.
   m_development_time=0;
   m_next_stage_time=cfg_HoneyBeeWorkerEggDevelopmentTime.value();
   m_Age=0;
   //if (inHive)
      //getHive()->setCellType(a_x,a_y,a_z,(int)CellType::Worker_Egg_Layed);
   m_current_body_temperature = cfg_HoneyBeeOptimalDevelopmentTemperature.value();
   m_current_worker_pointer = NULL;
}

void HoneyBee_WorkerEgg::ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM){
   HoneyBee_Base::ReInit(a_box_num, a_x, a_y, a_z,p_L, p_BPM);
   initWorkerEgg(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
}

void HoneyBee_WorkerEgg:: nextStage() {
   m_OurPopulationManager->CreatObject<HoneyBee_WorkerEgg, HoneyBee_WorkerLarva>(this);
}

void HoneyBee_WorkerEgg::Step(){
   m_StepDone = true; 
   return; //nothing to do, since it is an egg, only wait for the next stage.
}

void HoneyBee_WorkerEgg::EndStep(){
   if(m_CurrentHBState!=toHoneyBeeS_Dummy){
      //not dead, grow up
      if (m_CurrentStateNo!=-1){
         developInStep();
         if(m_OurPopulationManager->isLastStepInaDay()){
            m_Age++; //add the calendar age when it reaches the last step in a day
         }
         //old enough, go for next stage
         if (m_CurrentHBState==toHoneyBeeS_Progress){
            nextStage();
            m_CurrentStateNo = -1;//the memory could be reused
            m_CurrentHBState = toHoneyBeeS_Dummy; //nothing to do, the memory could be reused
         }
      }
      //the egg dies in the cell, this will change the task
      if (m_CurrentHBState == toHoneyBeeS_Die){
         setCellAfterDeadInCell();
         m_CurrentStateNo = -1;
         m_CurrentHBState = toHoneyBeeS_Dummy; //nothing to do, the memory could be reused
      }
   }
}

void HoneyBee_WorkerEgg::developInStep(){
   if(m_OurPopulationManager->isLastStepInaDay()){ //we only update the pesticide concentration per day
      if(l_pest_enable_pesticide_engine.value()){
         updatePesticideConcentration();
      }
   }
   //first, update accumulated develop time
   updateDevelopmentTime();
   //second, update mortality rate
   updateMortalityRate(); 
   //should she die?
   if(g_rand_uni()<m_mortality_rate){
      m_CurrentHBState =toHoneyBeeS_Die;
      m_CurrentStateNo=-1;
      return;
   }
   //the egg is old enough, go to next stage
   if (isReadyNextStage()){
      m_CurrentHBState = toHoneyBeeS_Progress;
   }
}

void HoneyBee_WorkerEgg::updateDevelopmentTime() {
   //temperature, for egg, only temperature matters
   //double development_rate = 1.0-pow(m_current_body_temperature-cfg_HoneyBeeOptimalDevelopmentTemperature.value(),2);
   double development_rate = 1.0;
   m_development_time += (double)cfg_HoneyBeeStepSize.value()/1440.0*development_rate;
}

void HoneyBee_WorkerEgg::updateMortalityRate(){
   //for eggs, only temperature matters, ToDo: maybe we should add pesticide and disease latter
   //updateTemperatureStressor();
   //m_mortality_rate = 1-m_temp_stressor;
}




//******************************************************************************************************
//*************************** START HoneyBee_WorkerLarva Class *****************************************
//******************************************************************************************************

const CellType HoneyBee_WorkerLarva::cellType=CellType::Worker_Larva;

void HoneyBee_WorkerLarva::initLarva(){
   m_next_stage_time=cfg_HoneyBeeWorkerLarvaDevelopmentTime.value();
   //a new larva, nonthing has developped
   m_development_time=0;
   
   //This is the perfect case, the age is the required development time for egg
   m_Age=cfg_HoneyBeeWorkerEggDevelopmentTime.value();
   inHive = true;
}

HoneyBee_WorkerLarva::HoneyBee_WorkerLarva(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerEgg(a_box_num, a_x, a_y, a_z, p_L, p_BPM)
{
   initLarva();
}

void HoneyBee_WorkerLarva::ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_WorkerEgg::ReInit(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
   initLarva();
}

void HoneyBee_WorkerLarva::Step()
{
   m_StepDone = true;//Larva have nothing to do, only wait for feeding, growing up or dying
   return;
}

void HoneyBee_WorkerLarva::updateFoodReqLarva(){
   int current_develop_day = (int)m_development_time;
   m_sugar_required_day = m_sugar_required_day-m_sugar+m_OurPopulationManager->supplyFoodReqWokerLarva(2, current_develop_day);
   m_pollen_required_day = m_pollen_required_day-m_pollen+m_OurPopulationManager->supplyFoodReqWokerLarva(1, current_develop_day);
   m_jelly_required_day = m_jelly_required_day-m_jelly+m_OurPopulationManager->supplyFoodReqWokerLarva(0, current_develop_day);
}

void HoneyBee_WorkerLarva::BeginStep(){
   //completely new one
   if(m_CurrentHBState==toHoneyBeeS_InitialState){
      m_CurrentHBState=toHoneyBeeS_Develop;
   }
   //one day begins, require feeding
   if (m_OurPopulationManager->isFirstStepInaDay()){
      updateFoodReqLarva();
      m_sugar = 0;
      m_jelly = 0;
      m_pollen = 0;
      //we need to check whether she has been fed from yesterday
      if(getHive()->supplyCellTaskStatus(m_box_num, m_Location_x,m_Location_y, m_Location_z)!=TaskStatus::Demanding){
         m_OurPopulationManager->getHive()->setCellTaskType(m_box_num, m_Location_x, m_Location_y, m_Location_z, TaskType::FeedingBrood);
         m_OurPopulationManager->getHive()->setCellTaskStatus(m_box_num, m_Location_x, m_Location_y, m_Location_z, TaskStatus::Demanding);
         m_OurPopulationManager->getHive()->addCellForFeeding(m_box_num, Coords3D(m_Location_x,m_Location_y,m_Location_z));
      }
   }
}

void HoneyBee_WorkerLarva::EndStep(){
   if(m_CurrentHBState!=toHoneyBeeS_Dummy&&m_CurrentHBState!=toHoneyBeeS_InitialState){
      //not dead, grow up
      if (m_CurrentStateNo!=-1){
         developInStep();
         if(m_OurPopulationManager->isLastStepInaDay()){ //update the calendar age in the last step per day
            m_Age++;
         }
         //old enough, go for next stage
         if (m_CurrentHBState==toHoneyBeeS_Progress){
            nextStage();
            getHive()->setCellTaskType(m_box_num, m_Location_x, m_Location_y, m_Location_z, TaskType::CappingBrood);
            getHive()->setCellTaskStatus(m_box_num, m_Location_x, m_Location_y, m_Location_z, TaskStatus::Demanding);
            getHive()->addBroodCellForCapping(m_box_num, Coords3D(m_Location_x,m_Location_y,m_Location_z));
            m_CurrentStateNo = -1;//the memory could be reused
            m_CurrentHBState = toHoneyBeeS_Dummy; //nothing to do, the memory could be reused
         }
      }
      //the larva dies in the cell, this will change the task
      if (m_CurrentHBState == toHoneyBeeS_Die){
         setCellAfterDeadInCell();
         m_CurrentStateNo = -1;
         m_CurrentHBState = toHoneyBeeS_Dummy; //nothing to do, the memory could be reused
      }
   }
}

void HoneyBee_WorkerLarva::updateDevelopmentTime(){
   //temperature
   //double development_rate = 1.0-pow(m_current_body_temperature-cfg_HoneyBeeOptimalDevelopmentTemperature.value(),2);
   //food, for Larva, we need to consider whether they have enough food or not
   double development_rate = 1.0;
   m_development_time += (double)cfg_HoneyBeeStepSize.value()/1440*development_rate;
   if (isReadyNextStage()){
      m_CurrentHBState = toHoneyBeeS_Progress;
   }
}

void HoneyBee_WorkerLarva::updateMortalityRate(){
   //temperature stress
   //updateTemperatureStressor();
   //food stress, for larva, we also need to consider food
   if( m_OurPopulationManager->isLastStepInaDay()){
      updateFoodStressor();
   }   
   //m_mortality_rate = 1-min(m_food_stressor, m_temp_stressor);
   //m_mortality_rate = tanh(2-m_food_stressor-m_temp_stressor);
   m_mortality_rate = 1- m_food_stressor;
}

void HoneyBee_WorkerLarva:: nextStage() {
   m_OurPopulationManager->CreatObject<HoneyBee_WorkerLarva,HoneyBee_WorkerPupa>(this);
}

void HoneyBee_WorkerLarva::developInStep(){
   //first, update accumulated develop time
   updateDevelopmentTime();
   //second, update mortality rate
   
   //should she die?
   if(m_OurPopulationManager->isLastStepInaDay()){
   updateMortalityRate(); 
   //if(g_rand_uni()<m_mortality_rate || m_MyBiologicalAgent->getDWV()>cfg_HoneyBeeLargestNumDWVForDeath.value()||m_MyBiologicalAgent->getABPV()>cfg_HoneyBeeLargestNumABPForDeath.value()||m_MyBiologicalAgent->getImmune()<cfg_HoneyBeeLowestImmuneForDeath.value()){
      m_mortality_rate = 0.01;
      if(g_rand_uni()<m_mortality_rate){
      m_CurrentHBState =toHoneyBeeS_Die;
      m_CurrentStateNo=-1;
      return;
   }}
}

void HoneyBee_WorkerLarva::feedLarva(double a_sugar, double a_pollen, double a_jelly){
   m_sugar += a_sugar;
   m_pollen += a_pollen;
   m_jelly += a_jelly;
}

/////////////////////////////////////START HoneyBee_WorkerPupa////////////////////////////

const CellType HoneyBee_WorkerPupa::cellType=CellType::Worker_Pupa;

void HoneyBee_WorkerPupa::initPupa(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM){
   m_next_stage_time=cfg_HoneyBeeWorkerPupaDevelopmentTime.value();
   m_development_time=0;
   //This is the perfect case, the age is the summation of the development time of egg and larva
   m_Age = cfg_HoneyBeeWorkerLarvaDevelopmentTime.value()+cfg_HoneyBeeWorkerEggDevelopmentTime.value();
   inHive = true;
   m_caped_flag = false;
}

HoneyBee_WorkerPupa::HoneyBee_WorkerPupa(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerLarva(a_box_num, a_x, a_y, a_z, p_L, p_BPM)
{
   initPupa(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
}

void HoneyBee_WorkerPupa::ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_WorkerLarva::ReInit(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
   initPupa(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
}

void HoneyBee_WorkerPupa::Step()
{
   m_StepDone = true;
   return;
}

void HoneyBee_WorkerPupa::updateDevelopmentTime(){
   //temperature
   //double development_rate = 1.0-pow(m_current_body_temperature-cfg_HoneyBeeOptimalDevelopmentTemperature.value(),2);
   //disease, for Pupa, we need to consider whether they have disease and/or parasite
   double development_rate = 1.0;
   m_development_time += (double)cfg_HoneyBeeStepSize.value()/1440*development_rate;
   if(isReadyNextStage()){m_CurrentHBState=toHoneyBeeS_Progress;}
}

void HoneyBee_WorkerPupa::updateMortalityRate(){
   //temperature stress
   //updateTemperatureStressor();
   //m_mortality_rate = 1-m_temp_stressor;
   //disease stress, for pupa, we also need to consider disease and/or parasite
   if(m_MyBiologicalAgent->getVarroa()>0){
      m_mortality_rate += cfg_HoneyBeeMortalityIncrementVorroa.value();
   }
   if(m_MyBiologicalAgent->getABPV()>0){
      m_mortality_rate += cfg_HoneyBeeMortalityPupaAPV.value();
   }
   if(m_MyBiologicalAgent->getDWV()>0){
      m_mortality_rate += cfg_HoneyBeeMortalityPupaDWV.value();
   }
}

void HoneyBee_WorkerPupa::developInStep(){
   //first, update accumulated develop time
   updateDevelopmentTime();
   //second, update mortality rate
   updateMortalityRate(); 
   //should she die?
   if(m_OurPopulationManager->isLastStepInaDay()){
   if(g_rand_uni()<m_mortality_rate){
      m_CurrentHBState =toHoneyBeeS_Die;
      m_CurrentStateNo=-1;
      return;
   }}
}

void HoneyBee_WorkerPupa::BeginStep(){
   //for the completely new ones
   if(m_CurrentHBState == toHoneyBeeS_InitialState){
      m_CurrentHBState = toHoneyBeeS_Develop;
   }
}

void HoneyBee_WorkerPupa::EndStep(){
   if(m_CurrentHBState!=toHoneyBeeS_Dummy&&m_CurrentHBState!=toHoneyBeeS_InitialState){
      //not dead, grow up
      if (m_CurrentStateNo!=-1){
         developInStep();
         if(m_OurPopulationManager->isLastStepInaDay()){ //update the calendar age per day
            m_Age++;
         }
         //old enough, go for next stage
         if (m_CurrentHBState==toHoneyBeeS_Progress){
            nextStage();
            m_CurrentStateNo = -1;//the memory could be reused
            m_CurrentHBState = toHoneyBeeS_Dummy; //nothing to do, the memory could be reused
         }
      }
      //the pupa dies in the cell, this will change the task
      if (m_CurrentHBState == toHoneyBeeS_Die){
         setCellAfterDeadInCell();
         m_CurrentStateNo = -1;
         m_CurrentHBState = toHoneyBeeS_Dummy; //nothing to do, the memory could be reused
      }
   }
}

void HoneyBee_WorkerPupa::nextStage() {
   if(m_MyBiologicalAgent->isVarroaReproduction()){
      m_MyBiologicalAgent->reporduceVarroa();
      double current_varroa = m_MyBiologicalAgent->getVarroa();
      if(current_varroa>1){
         findNewVarroaHosts();
      }
      //only keep one alive
      if(m_MyBiologicalAgent->getVarroa()>1){
         m_MyBiologicalAgent->setVarroa(1,0);
      }

   }
   m_OurPopulationManager->CreatObject<HoneyBee_WorkerPupa,HoneyBee_Worker>(this);
   //todo, add code for cleaning the cell hatched from
   m_OurPopulationManager->getHive()->hatchFromCell(m_box_num, m_Location_x, m_Location_y, m_Location_z, true);
}


/////////////////////////////////////START HoneyBee_Worker////////////////////////////
const CellType HoneyBee_Worker::cellType=CellType::Empty;

HoneyBee_Worker::HoneyBee_Worker(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerPupa(a_box_num, a_x, a_y, a_z, p_L, p_BPM)

{
   initWorker(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
}

HoneyBee_Worker::~HoneyBee_Worker(){
   delete m_current_foraging_place;
}

void HoneyBee_Worker::ReInit(const int a_box_num, const int  a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
   initWorker(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
}

void HoneyBee_Worker::initWorker(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM){
   m_development_time=0;
   //This is the perfect case, the age is the summation of the development time to egg, larva and pupa
   m_Age = cfg_HoneyBeeWorkerEggDevelopmentTime.value()+cfg_HoneyBeeWorkerLarvaDevelopmentTime.value()+cfg_HoneyBeeWorkerPupaDevelopmentTime.value();
   m_num_steps_back=-1;
   m_receive_nectar_flag = false;
   m_current_foraging_place = new struct_Known_Resource;
   m_current_foraging_place->list_index = -1;
   m_bee_task_activities.resize(EnumClassAsIndex(TaskType::Count));
   m_CurrentHBState=toHoneyBeeS_InitialState;
   m_gland_rate=1.0;
   m_foraging_status = -1; //from the beginning, there is no foraging activity.
}

void HoneyBee_Worker::updateDevelopmentTime(){
   //temperature
   //make the bee live longer in order to overwinter
   double development_rate = 0.05;
   if(m_OurLandscape->SupplyTemp()>=cfg_HoneyBeeMiniTemperatureForOutActivities.value()){
      development_rate = 1.0;//-pow(m_current_body_temperature-cfg_HoneyBeeOptimalDevelopmentTemperature.value(),2);
   }
   //disease, for Pupa, we need to consider whether they have disease and/or parasite
   m_development_time += (double)cfg_HoneyBeeStepSize.value()/1440.0*development_rate;
}

void HoneyBee_Worker::updateMortalityRate(){
   //temperature stress
   updateTemperatureStressor();
   //disease stress, for worker, we also need to consider disease and/or parasite
   if(m_MyBiologicalAgent->getVarroa()>0){
      m_mortality_rate+=cfg_HoneyBeeMortalityIncrementVorroa.value();
   }
   //m_mortality_rate = 1-m_temp_stressor;

   //too old
   double age_mortality = 1- (cfg_HoneyBeeWorkerMaxAge.value()-m_development_time)/cfg_HoneyBeeWorkerMaxAge.value();
   if (age_mortality > 0.015 && age_mortality <0.99) age_mortality = g_rand_uni()*1.5/100.0;

   if(age_mortality>m_mortality_rate){
      m_mortality_rate=age_mortality;
   }
   //food stress
   //updateFoodStressor();
   if(1-m_food_stressor>m_mortality_rate) m_mortality_rate=1-m_food_stressor;

   //comment out this, othervise, the bee will not die!!!!
   m_mortality_rate = 0;
}

void HoneyBee_Worker::updateTemperatureStressor(){
   double real_temperature = m_current_body_temperature;
   if(m_OurLandscape->SupplyTemp()<=cfg_HoneyBeeTemperatureLimitToDefineColdDays.value()){
      if (real_temperature<cfg_HoneyBeeLowestBeeComfortDegree.value()){
         m_temp_stressor += (cfg_HoneyBeeStepSize.value())*(real_temperature-cfg_HoneyBeeLowestBeeComfortDegree.value())/cfg_HoneyBeeColdEndure.value();
      }
      else{//no new temperature stress within this time step, so recover from it
         m_temp_stressor += cfg_HoneyBeeTempStressorRecoverPerTimestep.value(); 
      }
   }
   if(m_temp_stressor>1){
      m_temp_stressor = 1.0;
   }
   //It is fully stressed, so make it to 0.0
   if(m_temp_stressor<=0){
      m_temp_stressor = 0.0;
   }
}

void HoneyBee_Worker::developInStep(){
   //based on the activity, she will eat what she needs
   //eatSugar();
   //first, update accumulated develop time
   updateDevelopmentTime();
   //second, update mortality rate
   updateMortalityRate(); 
   
   //based on what she did, add what she needs
   m_sugar_required_day+=(m_OurPopulationManager->supplyPercentageTimestepInaDay())*m_OurPopulationManager->supplySugarForActivity(m_current_activity);
   m_pollen_required_day+=(m_OurPopulationManager->supplyPercentageTimestepInaDay())*m_OurPopulationManager->supplyPollenForActivity(m_current_activity);
   //should she die? 
   
   
   if(m_OurPopulationManager->isLastStepInaDay())
   {double test_random = g_rand_uni();
   
   if(test_random < m_mortality_rate){
      //std::cout<<m_mortality_rate<<test_random<<endl;
      m_CurrentHBState =toHoneyBeeS_Die;
      if(inHive && m_Location_z > -1){
         getHive()->increaseGlobalWaste();
         getHive()->decBeeCount(m_box_num, m_Location_x, m_Location_y, m_Location_z);
         //it is dead, removing the contribution for warming up
         if(m_current_activity==TaskType::WarmingUp){
            getHive()->addBeeGroupTemperature(m_Location_x, m_Location_y, m_Location_z, -1*m_current_body_temperature);
            //to move again, one died
            m_OurPopulationManager->setGatheringFlag(false);
         }
         
      }
      m_CurrentStateNo=-1;
      return;
   }}
   //temperature increase
   else{
      if(inHive && m_Location_z>-1 && m_current_activity!=TaskType::ForagingNectar && m_current_activity!=TaskType::ForagingPollen &&m_current_activity!=TaskType::Guarding){
         if(m_current_activity!=TaskType::WarmingUp){
            getHive()->addBeeGroupTemperature(m_Location_x, m_Location_y, m_Location_z, m_current_body_temperature+m_OurPopulationManager->supplyTempeIncreActivity(m_current_activity));
         }
      }
      else{
         //we assume when a bee is out, it is able to maintain its body temperature
         m_current_body_temperature=cfg_HoneyBeeOptimalDevelopmentTemperature.value();
      }
   }
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_MoveRandom()
{    
   //return toHoneyBeeS_Decide;
   //std::array<int,3> p = getHive()->randomNeighbour(Supply_m_Location_x(),Supply_m_Location_y(),Supply_m_Location_z());
   if(m_Location_z>0){
       getHive()->decBeeCount(supplyBoxNum(),Supply_m_Location_x(),Supply_m_Location_y(),Supply_m_Location_z());
       //we randomly move her to another frame side
       SetY((int)(g_rand_uni()*(getHive()->supplyYrange())));
       getHive()->incBeeCount(supplyBoxNum(),Supply_m_Location_x(),Supply_m_Location_y(),Supply_m_Location_z());
   }
   else{
       m_Location_x = 0;
       m_Location_y = (getHive()->supplyYrange()) / 2;
       m_Location_z = 0;
   }
   m_current_activity = TaskType::MovingAround;
   //bool find_activity = false;
   //to do, add finding activitity code here
   //if (g_rand_uni()>0.5) return toHoneyBeeS_ScoutNectar;
   //if (!find_activity){
   //   return toHoneyBeeS_Move;
   //}
   m_StepDone = true;
   return toHoneyBeeS_Decide;
}

void HoneyBee_Worker::updateSpecialTaskPriorities(){
   //cout << m_bee_task_activities;
   //produce wax or there is no jelly left for today, so disable jelly related task
   if(m_HPG_type==0 || m_available_jelly_day<=0){
      //cout << m_bee_task_activities;
      m_bee_task_activities(EnumClassAsIndex(TaskType::FeedingBrood))=0;
   }
   //produce jelly, so disable wax related task
   if(m_HPG_type==1 || m_available_wax_day<=0){
      m_bee_task_activities(EnumClassAsIndex(TaskType::CappingBrood))=0;
      m_bee_task_activities(EnumClassAsIndex(TaskType::CappingHoney))=0;
      m_bee_task_activities(EnumClassAsIndex(TaskType::BuildingCell))=0;
   }

   //check we still need more guarders or not
   if(m_OurPopulationManager->isEnoughGuarder()){
      m_bee_task_activities(EnumClassAsIndex(TaskType::Guarding))=0;
   }

   //check we still need more foragers for nectar or the bee has used the flying budget
   if(m_OurPopulationManager->isEnoughForagerForNectar() || m_dis_foraging_daily > cfg_HoneyBeeMaxFlyDistancePerDay.value()){
      m_bee_task_activities(EnumClassAsIndex(TaskType::ForagingNectar))=0;
   }

   //check we still need more foragers for pollen or the bee has used the flying budget
   if(m_OurPopulationManager->isEnoughForagerForPollen() || m_dis_foraging_daily > cfg_HoneyBeeMaxFlyDistancePerDay.value()){
      m_bee_task_activities(EnumClassAsIndex(TaskType::ForagingPollen))=0;
   }
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_Decide()

{
   if(!(m_OurPopulationManager->isEnoughForagerForNectar())){
      m_OurPopulationManager->increaseForagerForNectarNumber();
      return(toHoneyBeeS_ForageNectar);
   }
   if(!(m_OurPopulationManager->isEnoughForagerForPollen())){
      m_OurPopulationManager->increaseForagerForPollenNumber();
      return(toHoneyBeeS_ForagePollen);
   }
   return toHoneyBeeS_Move;

   // the first day they do nothing
   if(m_Age<=21 && m_development_time<=1){
      return toHoneyBeeS_Move;
   }

   updateSpecialTaskPriorities();
   //cout << m_bee_task_activities;
   int most_index = (blitz::maxIndex(m_bee_task_activities))(0);
   if(m_bee_task_activities(most_index)<1){
      //no task could be chosen, it may be caused by the bee is too old, for now we use 14
      //or it is developing very slowly in order to over winter
      if(m_development_time>14 || m_development_time<1){
         //we set every thing to one
         m_bee_task_activities = 1;
         updateSpecialTaskPriorities();
         //cout << m_bee_task_activities;
         m_bee_task_activities = m_bee_task_activities*(getHive()->supplyNumsAvailabeTasks());
         //cout << m_bee_task_activities;
         most_index = (blitz::maxIndex(m_bee_task_activities))(0);
         //still can not find any thing, just move around
         if(m_bee_task_activities(most_index)<1){
            most_index=EnumClassAsIndex(TaskType::MovingAround);
         }
      }
   }

   TTypeOfHoneyBeeState chosen_state;
   switch(most_index){
      case EnumClassAsIndex(TaskType::CappingBrood):
         chosen_state = toHoneyBeeS_CapBrood;
         break;
      case EnumClassAsIndex(TaskType::CappingHoney):
         chosen_state = toHoneyBeeS_CapHoney;
         break;
      case EnumClassAsIndex(TaskType::FeedingBrood):
         chosen_state = toHoneyBeeS_AttendBrood;
         break;
      case EnumClassAsIndex(TaskType::Guarding):
         chosen_state = toHoneyBeeS_Guard;
         break;
      case EnumClassAsIndex(TaskType::ForagingNectar):
         chosen_state = toHoneyBeeS_ForageNectar;
         break;
      case EnumClassAsIndex(TaskType::ForagingPollen):
         chosen_state = toHoneyBeeS_ForagePollen;
         break;
      case EnumClassAsIndex(TaskType::RipeningNectar):
         chosen_state = toHoneyBeeS_RipenNectar;
         break;
      case EnumClassAsIndex(TaskType::WarmingUp):
         chosen_state = toHoneyBeeS_WarmUp;
         break;
      case EnumClassAsIndex(TaskType::CleaningCell):
         chosen_state = toHoneyBeeS_CleanCell;
         break;
      default:
         chosen_state = toHoneyBeeS_Move;
   }

   //in the previous, the bee is out for guarding, do we need to bring it back?
   if(inHive==false && m_current_activity == TaskType::Guarding && most_index != EnumClassAsIndex(TaskType::Guarding)){
      //does she want to come back?, the second condition is to make sure it is not in night
      //cout<<getHive()->supplyNumsAvailabeTasks();
      if(g_rand_uni()<m_OurPopulationManager->supplyRatioGuarder()){//||(getHive()->supplyNumsAvailabeTasks())(EnumClassAsIndex(TaskType::Guarding))<0.5){
         inHive = true;
         m_OurPopulationManager->decreaseGuardingNum();
         m_Location_x = 0;
         m_Location_y = (getHive()->supplyYrange())/2;
         m_Location_z = 0;
         getHive()->incBeeCount(m_box_num,m_Location_x,m_Location_y,m_Location_z);
         //chosen_state = toHoneyBeeS_Move;
      }
      else{
         chosen_state = toHoneyBeeS_Guard;
      }      
   }

   /*
   //this is handled in beginstep()
   //in the previous the bee is out for foraging nectar, do we need to bring it back?
   if(inHive==false && m_current_activity == TaskType::ForagingNectar && most_index != EnumClassAsIndex(TaskType::ForagingNectar)){
      //does she want to come back? The second condition is to make sure it is not in night
      //if(g_rand_uni()<m_OurPopulationManager->supplyRatioForagerForNectar()){// || (getHive()->supplyNumsAvailabeTasks())(EnumClassAsIndex(TaskType::ForagingNectar))<1){
      inHive = true;
         //m_OurPopulationManager->decreaseForagedNectarNumber();
      m_Location_x = 0;
      m_Location_y = (getHive()->supplyYrange())/2;
      m_Location_z = 0;
      getHive()->incBeeCount(m_Location_x,m_Location_y,m_Location_z);
      m_OurPopulationManager->decreaseForagerForNectarNumber();
      //}
      //else{
      //   chosen_state = toHoneyBeeS_ForageNectar;
      //}      
   }
   */

   //we need to change the number of bees going out for guarding
   if(chosen_state==toHoneyBeeS_Guard){
      m_OurPopulationManager->increGuardingNum();
   }

   //we need to change the number of bees going out for forageing nectar
   if(chosen_state==toHoneyBeeS_ForageNectar){
      m_OurPopulationManager->increaseForagerForNectarNumber();
   }
   if(chosen_state==toHoneyBeeS_ForagePollen){
      m_OurPopulationManager->increaseForagerForPollenNumber();
   }
   return chosen_state;   
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_ForagePollen()
{
   m_current_activity = TaskType::ForagingPollen;
   //no flying budget today
   if (m_dis_foraging_daily>cfg_HoneyBeeMaxFlyDistancePerDay.value()){
       m_StepDone = true;
       //cout << "too much flying!!!"<<endl;
       return toHoneyBeeS_Decide;
   }

   if (!(m_OurPopulationManager->supplyForagingFlag())){
      //cout << "can't foraging!!!"<<endl;
      m_StepDone = true;
      return toHoneyBeeS_ForagePollen;
   }

   //cout << "foraging!!!"<<endl;
   //move the bee out
   inHive=false;
   if(m_Location_z>-1){
      getHive()->decBeeCount(m_box_num, m_Location_x, m_Location_y, m_Location_z);
   }

   //check what the bee is doing for foraging, if the bee is not foraging or foraging for nectar, the bee needs to find a new place
   if (m_foraging_status==-1 || m_foraging_status==1){
      m_current_foraging_place->list_index = -1;
      m_foraging_status = 2; //set for foraging pollen
   }
   //increase the mortality rate
   m_mortality_rate+=cfg_HoneyBeeForagingMortalityIncrement.value();

   forageResource(&HoneyBee_Colony::supplyOneAvailableResourcePlace, &HoneyBee_Colony::supplyKnownPollenPlace,&HoneyBee_Colony::supplyKnownNumOfPlaceForPollen,2);
   #ifdef __APISRAM_FORAGE_DEBUG
   m_OurPopulationManager->addFailTripPollen();
   #endif
   m_current_foraging_place->resoure_type = 1;
   
   m_current_activity = TaskType::ForagingPollen;
   m_StepDone = true;
   return toHoneyBeeS_Decide;
   //return toHoneyBeeS_StorePollen;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_WayBack(){
   m_num_steps_back--;
   m_StepDone=true;
   if(m_num_steps_back ==1){
      if(m_current_foraging_place->list_index>=0 && m_current_activity==TaskType::ForagingNectar){
         m_OurPopulationManager->increaseForagedNectarNumber();
      }
   }
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_WarmUp(){
   m_StepDone=true;
   m_current_activity = TaskType::WarmingUp;
   //remove these codes, only for calibration of foraging
   m_current_body_temperature = cfg_HoneyBeeOptimalDevelopmentTemperature.value();
   return toHoneyBeeS_Decide;
   //remove these codes up

   if(m_current_body_temperature>=cfg_HoneyBeeOptimalDevelopmentTemperature.value()){
      getHive()->addBeeGroupTemperature(m_Location_x, m_Location_y, m_Location_z, m_current_body_temperature+1);
      return toHoneyBeeS_Decide;
   }
   if(m_current_body_temperature<cfg_HoneyBeeOptimalDevelopmentTemperature.value()&&m_current_body_temperature>=cfg_HoneyBeeLowestBeeComfortDegree.value()){
      getHive()->addBeeGroupTemperature(m_Location_x, m_Location_y, m_Location_z, m_current_body_temperature+2*cfg_HoneyBeeIncreaseTemperatureWarmingUp.value()*(cfg_HoneyBeeOptimalDevelopmentTemperature.value()-m_current_body_temperature)/(cfg_HoneyBeeOptimalDevelopmentTemperature.value()-cfg_HoneyBeeLowestBeeComfortDegree.value()));
      return toHoneyBeeS_Decide;
   }
   double weight = 1;
   if(m_current_body_temperature<cfg_HoneyBeeLowestBeeComfortDegree.value()) weight=2;
   getHive()->addBeeGroupTemperature(m_Location_x, m_Location_y, m_Location_z, m_current_body_temperature+weight*cfg_HoneyBeeIncreaseTemperatureWarmingUp.value());
   /** \todo Move the bees closer */
   return toHoneyBeeS_Decide;
}

void HoneyBee_Worker::forageResource(bool (HoneyBee_Colony::*supplyOneAvailablePlaceFuncP)(int, HoneyBee_Worker* , struct_Known_Resource*,int ), struct_Known_Resource (HoneyBee_Colony::*supplyKnownPlaceFuncP)(void), int (HoneyBee_Colony::*supplyKnownNumOfPlaceFuncP)(void), int resource_type){
   //first, we check if the bee has already know one place for foraging
   if(m_current_foraging_place->list_index!=-1){
      if ((m_OurPopulationManager->*supplyKnownNumOfPlaceFuncP)()>0)
      {
         struct_Known_Resource temp_foraing_place = (m_OurPopulationManager->*supplyKnownPlaceFuncP)();
         bool comparasion_result = false;
         if(cfg_HoneyBeeForagingStrategy.value() == 1 && temp_foraing_place.distance<m_current_foraging_place->distance && temp_foraing_place.amount>0){ comparasion_result = true;} //foraging strategy
         if(cfg_HoneyBeeForagingStrategy.value() == 2 && temp_foraing_place.poly_area >m_current_foraging_place->poly_area && temp_foraing_place.amount>0){ comparasion_result = true;}
         if(cfg_HoneyBeeForagingStrategy.value() == 3 && temp_foraing_place.amount>m_current_foraging_place->amount){ comparasion_result = true;}
         if(cfg_HoneyBeeForagingStrategy.value() == 4 && temp_foraing_place.quality.m_quality >m_current_foraging_place->quality.m_quality){ comparasion_result = true;}
         if(cfg_HoneyBeeForagingStrategy.value() == 5 && temp_foraing_place.gain >m_current_foraging_place->gain){ comparasion_result = true;}
         //
         if(comparasion_result){
            if(g_rand_uni()>0.5){//forage in the new place
               *m_current_foraging_place=temp_foraing_place;
            }
         }
      }
      SetX((m_OurLandscape)->SupplyCentroidX(m_current_foraging_place->poly_id));
      SetY((m_OurLandscape)->SupplyCentroidY(m_current_foraging_place->poly_id));
      SetZ(-1);
      inHive = false;
      m_num_steps_back = ceil(m_current_foraging_place->distance/cfg_HoneyBeeMaximumFlyDistancePerStep.value());
      m_dis_foraging_daily += m_current_foraging_place->distance;
   }
   else
   { //if she doesn't know one, we check if we have enough nectar places for foraging, if yes, we go for foraging directly, otherwise, we go for scouting instead
      if ((m_OurPopulationManager->*supplyKnownNumOfPlaceFuncP)()>=cfg_HoneyBeeNeedForScoutingNectarNum.value()){
         *m_current_foraging_place = (m_OurPopulationManager->*supplyKnownPlaceFuncP)();
         SetX((m_OurLandscape)->SupplyCentroidX(m_current_foraging_place->poly_id));
         SetY((m_OurLandscape)->SupplyCentroidY(m_current_foraging_place->poly_id));
         SetZ(-1);
         inHive = false;
         m_num_steps_back = ceil(2*m_current_foraging_place->distance/cfg_HoneyBeeMaximumFlyDistancePerStep.value());
         m_dis_foraging_daily += 2*m_current_foraging_place->distance;     
      }
      // there is not enough place, so she will try to find a new place(scouting)
      else{
         int scout_direction = int(g_rand_uni()*360/ cfg_HoneyBeeResourceScaleDegree.value());
         bool scout_result = (m_OurPopulationManager->*supplyOneAvailablePlaceFuncP)(scout_direction, this, m_current_foraging_place, resource_type);
         //no place find
         if(!scout_result){
            m_current_foraging_place->list_index=-1;//this means nothing found for foraging
            m_num_steps_back=2;
            m_dis_foraging_daily += 4000;
            inHive=false;
            SetX(getHive()->supplyHivePositionX()-2);
            SetY(getHive()->supplyHivePositionY()-2);
            SetZ(-1);
         }
         else{
            m_num_steps_back = ceil(2*m_current_foraging_place->distance/cfg_HoneyBeeMaximumFlyDistancePerStep.value());
            m_dis_foraging_daily += 2*m_current_foraging_place->distance;
            inHive=false;
            SetX((m_OurLandscape)->SupplyCentroidX(m_current_foraging_place->poly_id));
            SetY((m_OurLandscape)->SupplyCentroidY(m_current_foraging_place->poly_id));
            SetZ(-1);
         }
      }
   }
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_ForageNectar()
{
   m_current_activity = TaskType::ForagingNectar;
   if (m_dis_foraging_daily>cfg_HoneyBeeMaxFlyDistancePerDay.value()){
       m_StepDone = true;
       return toHoneyBeeS_Decide;
   }

   if (!(m_OurPopulationManager->supplyForagingFlag())){
      m_StepDone = true;
      return toHoneyBeeS_ForageNectar;
   }
   //move the bee out
   inHive=false;
   if(m_Location_z>-1){
      getHive()->decBeeCount(m_box_num, m_Location_x, m_Location_y, m_Location_z);
   }

    //check what the bee is doing for foraging, if the bee is not foraging or foraging for pollen, the bee needs to find a new place
   if (m_foraging_status==-1 || m_foraging_status==2){
      m_current_foraging_place->list_index = -1;
      m_foraging_status = 1; //set for foraging nectar
   }
   //increase the mortality rate
   m_mortality_rate+=cfg_HoneyBeeForagingMortalityIncrement.value();

   forageResource(&HoneyBee_Colony::supplyOneAvailableResourcePlace, &HoneyBee_Colony::supplyKnownNectarPlace,&HoneyBee_Colony::supplyKnownNumOfPlaceForNectar,1);
   #ifdef __APISRAM_FORAGE_DEBUG
   m_OurPopulationManager->addFailTripNectar();
   #endif
   m_current_foraging_place->resoure_type = 0;
   
   m_current_activity = TaskType::ForagingNectar;
   m_StepDone = true;
   m_OurPopulationManager->addForagingNectarActivity();
   //she is comeing back in the beginning of step
   if(m_num_steps_back ==1){
      if(m_current_foraging_place->list_index>=0){
         //check if this place is without any resource
         //cout<<"resource?"<<m_OurPopulationManager->supplyAvailableNectarPoly(m_current_foraging_place->direction_index, m_current_foraging_place->list_index);
         if (m_OurPopulationManager->supplyAvailableNectarPoly(m_current_foraging_place->direction_index, m_current_foraging_place->list_index)<=0){
            m_current_foraging_place->list_index = -1;
         }
         else{
            m_OurPopulationManager->increaseForagedNectarNumber();            
         }
      }
   }
   return toHoneyBeeS_Decide;
   //return toHoneyBeeS_StoreNectarSelf;
   //set for bring resource back
   /*
      if(l_pest_enable_pesticide_engine.value()){
         //to do, add to support more type of pesticide
         
         for (int i=0;i<cfg_PesticideTypesUsed.value();i++){
            double current_pes_ammount = m_OurLandscape->SupplyPesticideP(foraging_polyid, PlantProtectionProducts(ppp_1+i));
            double current_biomass = m_OurLandscape->SupplyVegBiomass(foraging_polyid);
            m_pest_burden_intake[i] += cfg_MaximumAmountPerForageNectar.value()*cfg_ConvertPesticidePerBiomassToNectar.value()*current_pes_ammount/current_biomass;
            if (cfg_Enable_Seed_Coating.value()){
               int temp;
               //to do, add seed coating
               //int m_current_coating_type = m_OurLandscape->Supply
            }
         }
         
      }
      */
   
}

void HoneyBee_Worker::putPollenInCell(struct_Known_Resource* a_resource_place){
   int foraging_polyid = a_resource_place->poly_id;
   PollenNectarInfo current_resource;
   current_resource.m_pollen_nectar_flag =false;
   current_resource.m_amount=cfg_HoneyBeeVolumeForagedPollenOneTime.value();
   current_resource.m_quality =m_OurLandscape->SupplyPollen(foraging_polyid).m_quality;
   current_resource.m_poly_id=a_resource_place->poly_id;
   getHive()->storePollenToCell(current_resource, this, m_box_num); /** \todo Add how to choose box for storing pollen. */
   //this is for calibration of foraging model
   //if (m_current_foraging_place->list_index>=0){
   
   if(m_OurPopulationManager->supplyAvailablePollenPoly(m_current_foraging_place->direction_index, m_current_foraging_place->list_index)<=0){
      m_current_foraging_place->list_index = -1;
   //m_OurPopulationManager->deduceForagedPollen(m_current_foraging_place->direction_index, m_current_foraging_place->list_index, cfg_HoneyBeeVolumeForagedPollenOneTime.value());
   }
   #ifdef __APISRAM_FORAGE_DEBUG
   m_OurPopulationManager->addSuccTripPollen();
   m_OurPopulationManager->addForagedPollen(cfg_HoneyBeeVolumeForagedPollenOneTime.value());
   #endif
   
}

void HoneyBee_Worker::putNectarInCell(struct_Known_Resource* a_resource_place){
   /**
   int foraging_polyid = a_resource_place->poly_id;
   PollenNectarInfo current_resource;
   current_resource.m_pollen_nectar_flag =true;
   current_resource.m_amount=cfg_HoneyBeeVolumeForagedNectarOneTime.value();
   current_resource.m_quality =( m_OurLandscape)->SupplyNectar(foraging_polyid).m_quality;
   current_resource.m_poly_id=a_resource_place->poly_id;
   getHive()->storeNectarToCell(current_resource, this, m_box_num); /** \todo Add how to choose box for soring nectar. */

   //this is for calibration of foraging model
   if(m_OurPopulationManager->supplyAvailableNectarPoly(m_current_foraging_place->direction_index, m_current_foraging_place->list_index)<=0){
      m_current_foraging_place->list_index = -1;
      //m_OurPopulationManager->deduceForagedNectar(m_current_foraging_place->direction_index, m_current_foraging_place->list_index, cfg_HoneyBeeVolumeForagedNectarOneTime.value());

   }
   #ifdef __APISRAM_FORAGE_DEBUG
   if (m_OurLandscape->SupplyNectar(a_resource_place->poly_id).m_quantity>0){
      m_OurPopulationManager->addSuccTripNectar();
      m_OurPopulationManager->addForagedNectar(cfg_HoneyBeeVolumeForagedNectarOneTime.value());
      m_OurPopulationManager->addForagedSugar(cfg_HoneyBeeVolumeForagedNectarOneTime.value()*m_OurLandscape->SupplyNectar(a_resource_place->poly_id).m_quality/m_OurLandscape->SupplyNectar(a_resource_place->poly_id).m_quantity);
   }
   #endif   

   /*
   if(l_pest_enable_pesticide_engine.value()){
      //to do, add to support more type of pesticide         
      for (int i=0;i<cfg_PesticideTypesUsed.value();i++){
         double current_pes_ammount = m_OurLandscape->SupplyPesticideP(foraging_polyid, PlantProtectionProducts(ppp_1+i));
         double current_biomass = m_OurLandscape->SupplyVegBiomass(foraging_polyid);
         m_pest_burden_intake[i] += cfg_MaximumAmountPerForageNectar.value()*cfg_ConvertPesticidePerBiomassToNectar.value()*current_pes_ammount/current_biomass;
         if (cfg_Enable_Seed_Coating.value()){
            int temp;
            //to do, add seed coating
            //int m_current_coating_type = m_OurLandscape->Supply
         }
      }      
   }
   */
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_StorePollen()
{
   //check if there is pollen available in the polygon
   if(m_OurPopulationManager->supplyAvailablePollenPoly(m_current_foraging_place->direction_index, m_current_foraging_place->list_index)>0){
      m_OurPopulationManager->moveAvailablePollenToKnownOrUpdate(*m_current_foraging_place);
      putPollenInCell(m_current_foraging_place);
   }
   else{
      m_current_foraging_place->list_index=-1;
   }
   m_StepDone=true;
   inHive=true;
   m_current_activity = TaskType::DepositingPollen;
   //return toHoneyBeeS_Decide;
   return toHoneyBeeS_ForagePollen;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_StoreNectar()
{
   struct_Known_Resource current_resource_place = m_OurPopulationManager->passOneForageNectarPlace();
   if(current_resource_place.poly_id>0 && m_OurPopulationManager->supplyAvailableNectarPoly(current_resource_place.direction_index, current_resource_place.list_index)>0){
      m_OurPopulationManager->moveAvailableNectarToKnownOrUpdate(current_resource_place);
      putNectarInCell(&current_resource_place);
      m_StepDone=true;
   }
   inHive=true;
   //turn off the flag, only do this when it is required
   m_receive_nectar_flag = false;
   m_current_activity = TaskType::DepositingNectar;
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_StoreNectarSelf(){
   //cout<<"storing nectar"<<endl;
   //cout<<m_current_foraging_place->list_index;
   if(m_current_foraging_place->list_index>=0 && m_OurPopulationManager->supplyAvailableNectarPoly(m_current_foraging_place->direction_index, m_current_foraging_place->list_index)>0){
      m_OurPopulationManager->moveAvailableNectarToKnownOrUpdate(*m_current_foraging_place);
      putNectarInCell(m_current_foraging_place);
   }
   else{
      m_current_foraging_place->list_index=-1;
   }
   m_StepDone=true;
   inHive=true;
   //turn off the flag, only do this when it is required
   m_receive_nectar_flag = false;
   m_current_activity = TaskType::DepositingNectar;
   //return toHoneyBeeS_Decide;
   return toHoneyBeeS_ForageNectar;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_RipenNectar(){
   m_StepDone=true;
   inHive=true;
   m_current_activity = TaskType::RipeningNectar;
   return toHoneyBeeS_Decide;
}

//The bee will not do anything, but will change...
TTypeOfHoneyBeeState HoneyBee_Worker::st_Guard(){
   //update time spent for activity
   //double time_spent = updateRemainingTimeActivity(TaskType::Guarding);


   //increase the mortality rate since it is guarding
   m_mortality_rate += cfg_HoneyBeeGuardingMortalityIncrement.value(); 
   
   //we move it a bit out of the hive
   if(inHive){
      inHive=false;
      getHive()->decBeeCount(m_box_num, m_Location_x, m_Location_y, m_Location_z);
      SetX(getHive()->supplyHivePositionX()+2);
      SetY(getHive()->supplyHivePositionY()+2);
      SetZ(-1);
   }
   m_current_activity = TaskType::Guarding;
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_AttendBrood(){
   if(m_HPG_type==-1) m_HPG_type=1;
   if(m_HPG_type==1){
      double req_sugar=0;
      double req_pollen=0;
      bool feeding_result = m_OurPopulationManager->getHive()->feedLarva(m_box_num, m_Location_y, this, 1, &req_sugar, &req_pollen, &m_available_jelly_day);/** \todo Add how to choose box to feed larva. */
      //we need to add these current bee, then the bee can refill it
      m_sugar_required_day+=req_sugar;
      m_pollen_required_day+=req_pollen;
   }
   m_current_activity = TaskType::FeedingBrood;
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_CapBrood(){
   if(m_HPG_type==-1) m_HPG_type=0;
   if(m_HPG_type==0){
      bool caping_result = m_OurPopulationManager->getHive()->capCell(m_box_num, m_Location_y, this, 1, &m_available_wax_day,1);/** \todo Add how to choose box for capping brood. */
   }
   m_current_activity = TaskType::CappingBrood;
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_CleanCell(){
   bool clean_result = m_OurPopulationManager->getHive()->cleanCell(m_box_num, m_Location_y, this, 1);/** \todo Add how to choose box for cleaning cell. */
   m_current_activity = TaskType::CleaningCell;
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_CapHoney(){
   if(m_HPG_type==-1) m_HPG_type=0;
   if(m_HPG_type==0){
      bool caping_result = m_OurPopulationManager->getHive()->capCell(m_box_num, m_Location_y, this, 1, &m_available_wax_day,2); /** \todo Add how to choose box for capping honey. */
   }
   m_current_activity = TaskType::CappingHoney;
   return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_RemoveDebris(){
   //if there is waste, if not try to find anther task
   if(getHive()->isGlobalWaste()){
      m_StepDone = true;
      getHive()->decreaseGlobalWaste();
      //check if there is nosema in the globas waste
      if(getHive()->isApisInGlobalWaste()){
         if(m_MyBiologicalAgent->getNosemaA()<1){
            if(g_rand_uni()>=cfg_HoneyBeeProbaInfectedNosemaFromGlobalWaste.value()){
               m_MyBiologicalAgent->setNosemaA (1); // make it infected by Nosema apis
            }
         }
         
      }
      if(getHive()->isCeranaeInGlobalWaste()){
         if(m_MyBiologicalAgent->getNosemaC()<1){
            if(g_rand_uni()>=cfg_HoneyBeeProbaInfectedNosemaFromGlobalWaste.value()){
               m_MyBiologicalAgent->setNosemaC(1); //make it infected by Nosema ceranae
            }
         }
         
      }
   }
   m_current_activity = TaskType::RemovingDebris;
   return toHoneyBeeS_Decide;
}

void HoneyBee_Worker::updateGlandProductionAbility(){
   double current_total_burden = supplySumPestBurden();
   if(current_total_burden>cfg_HoneyBeeGlandReductionThresholdPest.value()){
      m_gland_rate=cfg_HoneyBeeGlandReductionRatePest.value();
   }
   if(current_total_burden<=cfg_HoneyBeeGlandReductionThresholdPest.value()&&m_gland_rate<1){
      m_gland_rate+=cfg_HoneyBeeGlandRecoveryRatePest.value();
      if(m_gland_rate>1){
         m_gland_rate=1;
      }
   }

}

void HoneyBee_Worker::Step(void)
{
   if(m_CurrentStateNo == -1 || m_StepDone ) return;
   switch (m_CurrentHBState)
   {
   case toHoneyBeeS_Progress:
      m_StepDone = true;
      break;
   case toHoneyBeeS_InitialState: // Initial state always starts with develop
      //m_CurrentHBState = toHoneyBeeS_ForageNectar;
//        m_CurrentHBState = toHoneyBeeS_Move;
      m_CurrentHBState = toHoneyBeeS_AttendBrood;
      //m_StepDone = true;
      break;
   case toHoneyBeeS_Move:
      m_CurrentHBState=st_MoveRandom(); // Will return movement or die
      m_StepDone = true;
      break;
   case toHoneyBeeS_Decide:
      m_CurrentHBState=st_Decide();
      break;
   case toHoneyBeeS_ForageNectar:
      m_CurrentHBState=st_ForageNectar();
      m_StepDone = true;
      break;
   case toHoneyBeeS_WayBack:
      m_CurrentHBState=st_WayBack();
      break;
   case toHoneyBeeS_StoreNectar:
      m_CurrentHBState=st_StoreNectar();
      break;
   case toHoneyBeeS_StoreNectarSelf:
      m_CurrentHBState=st_StoreNectarSelf();
      break;
   case toHoneyBeeS_ForagePollen:
      m_CurrentHBState=st_ForagePollen();
      break;
   case toHoneyBeeS_StorePollen:
      m_CurrentHBState=st_StorePollen();
      break;
   case toHoneyBeeS_AttendBrood:
      m_CurrentHBState=st_AttendBrood();
      m_StepDone=true;
      break;
   case toHoneyBeeS_CapBrood:
      m_CurrentHBState=st_CapBrood();
      m_StepDone=true;
      break;
   case toHoneyBeeS_CapHoney:
      m_CurrentHBState=st_CapHoney();
      m_StepDone=true;
      break;
   case toHoneyBeeS_Guard:
      m_CurrentHBState = st_Guard();
      m_StepDone=true;
      break;
   case toHoneyBeeS_RipenNectar:
      m_CurrentHBState = st_RipenNectar();
      m_StepDone=true;
      break;
   case toHoneyBeeS_WarmUp:
      m_CurrentHBState = st_WarmUp();
      m_StepDone=true;
      break;
   case toHoneyBeeS_CleanCell:
      m_CurrentHBState = st_CleanCell();
      m_StepDone=true;
      break;
   case toHoneyBeeS_RemoveDebris:
      m_CurrentHBState = st_RemoveDebris();
      m_StepDone=true;
      break;
   default:
      m_OurLandscape->Warn("HoneyBee_Worker::Step()", "unknown state - default"+to_string((int)m_CurrentHBState));
      exit(1);
   }
}

void HoneyBee_Worker::BeginStep(){
   //for the complete new one
   if (m_CurrentHBState==toHoneyBeeS_InitialState){
      m_CurrentHBState = toHoneyBeeS_Decide;
   }
   if (m_OurPopulationManager->isFirstStepInaDay()){
      //set it to be -1, since it is the beginning of one day
      m_HPG_type = -1;
      int current_age = (int)(m_development_time);
      m_available_jelly_day = m_OurPopulationManager->supplyJellyOneDay(current_age);
      m_available_wax_day = m_OurPopulationManager->supplyWaxOneDay(current_age);
      m_dis_foraging_daily = 0;
      m_CurrentHBState = toHoneyBeeS_Decide;
      if (m_current_foraging_place->list_index != -1){
         if(m_current_foraging_place->resoure_type){
            if(!(m_OurPopulationManager->isEnoughForagerForPollen())){
                m_OurPopulationManager->increaseForagerForPollenNumber();
               m_CurrentHBState = toHoneyBeeS_ForagePollen;}
         }
         else{
            if(!(m_OurPopulationManager->isEnoughForagerForNectar())){
                m_OurPopulationManager->increaseForagerForNectarNumber();
               m_CurrentHBState = toHoneyBeeS_ForageNectar;}
         }
      }
      //if(m_CurrentHBState==toHoneyBeeS_InitialState){
      //    m_CurrentHBState = toHoneyBeeS_Decide;
      //}
   }
   //update the task priorities
   m_bee_task_activities = m_OurPopulationManager->supplyTaskProritesAge((int)m_development_time);
   //m_CurrentHBState = toHoneyBeeS_Decide; // this might need to be commented out
   // check if she needs to receive nectar
   //if(m_receive_nectar_flag){
      //m_OurPopulationManager->decreaseForagedNectarNumber();
   //   m_CurrentHBState = toHoneyBeeS_StoreNectar;
   //}

   //check if she need to bring a bee back from fraging
   if(inHive==false && (m_current_activity == TaskType::ForagingNectar || m_current_activity == TaskType::ForagingPollen) && m_CurrentHBState!=toHoneyBeeS_Die){
      //she still needs time for back
      if(m_num_steps_back > 1){
         m_CurrentHBState = toHoneyBeeS_WayBack; //still needs time to come back
      }
      if(m_num_steps_back==1){
         m_num_steps_back = -1;
         //for nectar, we need to find if there is another bee can take over the nectar
         if(m_current_activity == TaskType::ForagingNectar){
            if(m_current_foraging_place->list_index>=0){//pass the nectar to another bee
               if(m_OurPopulationManager->supplyForagedNectarNumber()<1){
                  m_OurPopulationManager->addOneForagedNectarPlace(*m_current_foraging_place);
                  if((getHive()->supplyNumsAvailabeTasks())(EnumClassAsIndex(TaskType::ForagingNectar))>1){
                     m_CurrentHBState = toHoneyBeeS_ForageNectar;
                  }                  
                  else{
                    m_CurrentHBState = toHoneyBeeS_Decide;
                    m_OurPopulationManager->decreaseForagerForNectarNumber();
                  }
                  
                  //put it back, since it is in the hive, although it may go out again soon
                  m_Location_x = 0;
                  m_Location_y = (getHive()->supplyYrange())/2;
                  m_Location_z = 0;
                  inHive = true;
                  getHive()->incBeeCount(m_box_num, m_Location_x,m_Location_y,m_Location_z);
               }
               //no bee availabe, store it by it self
               else{
                  m_CurrentHBState = toHoneyBeeS_StoreNectarSelf;
                  m_OurPopulationManager->decreaseForagerForNectarNumber();
                  m_OurPopulationManager->decreaseForagedNectarNumber();
                  m_Location_x = 0;
                  m_Location_y = (getHive()->supplyYrange()) / 2;
                  m_Location_z = 0;
                  inHive = true;
                  getHive()->incBeeCount(m_box_num, m_Location_x, m_Location_y, m_Location_z);
               }
            }
               //nothing found, go back and check what should do next
            else{
               inHive = true;
               m_Location_x = 0;
               m_Location_y = (getHive()->supplyYrange())/2;
               m_Location_z = 0;
               getHive()->incBeeCount(m_box_num,m_Location_x,m_Location_y,m_Location_z);
               m_OurPopulationManager->decreaseForagerForNectarNumber();
               m_CurrentHBState = toHoneyBeeS_ForageNectar;
            }
         }
         //for pollen, the bee needs to store the pollen by it self
         if(m_current_activity == TaskType::ForagingPollen){
            m_OurPopulationManager->decreaseForagerForPollenNumber();
            if(m_current_foraging_place->list_index>=0){
               m_CurrentHBState = toHoneyBeeS_StorePollen;
               inHive = true;
               //put her back into the hive for storing pollen
               m_Location_x = 0;
               m_Location_y = (getHive()->supplyYrange())/2;
               m_Location_z = 0;
               getHive()->incBeeCount(m_box_num,m_Location_x,m_Location_y,m_Location_z);
            }
               //nothing found, go back and check what should do next
            else{
               inHive = true;
               m_Location_x = 0;
               m_Location_y = (getHive()->supplyYrange())/2;
               m_Location_z = 0;
               getHive()->incBeeCount(m_box_num,m_Location_x,m_Location_y,m_Location_z);
               m_CurrentHBState = toHoneyBeeS_ForagePollen;
            }
         }
      }
   }
   //update body temperature
   //if(inHive&&m_Location_z>-1&&m_current_activity==TaskType::WarmingUp){
   if(inHive&&m_Location_z>-1){
      m_current_body_temperature=(m_OurPopulationManager->m_our_thermodynamics)->supplyGroupBeesTemp(m_Location_x,m_Location_y, m_Location_z);
   }
   /*
   if(inHive&&m_Location_z>-1&&m_current_activity==TaskType::WarmingUp&&(m_OurLandscape)->SupplyTemp()>cfg_HoneyBeeTemperatureLimitToDefineColdDays.value()){
      m_current_body_temperature=cfg_HoneyBeeOptimalDevelopmentTemperature.value();
      m_OurPopulationManager->setGatheringFlag(false);
   }*/
   //cout << m_bee_task_activities;
   //m_bee_task_activities(0) = 1000;
   //cout << m_bee_task_activities;
   //cout << m_OurPopulationManager->supplyTaskProritesAge((int)m_development_time);

}

void HoneyBee_Worker::EndStep(){
   //we do the eat here, the third condition is to make sure the new worker from a pupa wil not do anything in the same time step
   if(m_CurrentStateNo != -1&&m_CurrentHBState!=toHoneyBeeS_Dummy&&m_CurrentHBState!=toHoneyBeeS_InitialState){
      //first we check whether this this the final 10 mins for one day of the bee's life, if it is, let she eats what it needs.
      if(m_OurPopulationManager->isLastStepInaDay()){
         if(m_Location_z>-1){
            eatSugar();
            eatPollen();
            updateFoodStressor();
         }
         m_dis_foraging_daily=0;
      }
      developInStep();
      //If there is varroa, check whether it will jump to another host close by
      if(m_MyBiologicalAgent->getVarroa()>0){
         if(g_rand_uni()<cfg_HoneyBeeProbaVarroaDropFromAdultToAdult.value()){
         findNewVarroaHosts();
         }
      }
      //when she is foraging nectar and will be back to hive in the next time step, increase the number in order to find a bee in the hive, that will receive the nectar in the next stage.
      /*
      if(m_current_activity == TaskType::ForagingNectar && m_num_steps_back ==1){
         //we need to check whether it found anything or not
         if(m_current_foraging_place->list_index>=0){
            m_OurPopulationManager->increaseForagedNectarNumber();
         }
      }*/

      //we need to check again whether it dies in developInStep()
      //when she is in hive, and there is foraged back nectar, make the receive_nectar to be true, it will receive the nectar and store it in the next time step
      if(m_CurrentHBState != toHoneyBeeS_Die){
         /*
         if(inHive && m_OurPopulationManager->supplyForagedNectarNumber()>0){
            m_receive_nectar_flag = true;
            m_OurPopulationManager->decreaseForagedNectarNumber();
         }
         else{
            m_receive_nectar_flag=false;
         }
         */
         
         if(m_OurPopulationManager->isLastStepInaDay()){ //update the calendar age in days
            m_Age += 1;
         }
      }
   }
}

void HoneyBee_Worker::eatSugar()
{
   // Consume sugar
   //double sugarConsumed=10*m_OurPopulationManager->m_percentage_timestep_in_a_day;
   //first, try the current frame side
   if(inHive){
      int eating_y = Supply_m_Location_y();
      if(m_MyBiologicalAgent->getNosemaA()>0||m_MyBiologicalAgent->getNosemaC()>0) m_sugar_required_day*=cfg_HoneyBeeIncrementFoodCausedByNosema.value();
      m_sugar = m_OurPopulationManager->getHive()->consumeSugar(m_box_num, eating_y, this, m_sugar_required_day);/** \todo Add how to choose box for consuming sugar. */
   }
}

void HoneyBee_Worker::eatPollen(){
   if(inHive){
      int eating_y = Supply_m_Location_y();
      if(m_MyBiologicalAgent->getNosemaA()>0||m_MyBiologicalAgent->getNosemaC()>0) m_pollen_required_day*=cfg_HoneyBeeIncrementFoodCausedByNosema.value();
      m_pollen = m_OurPopulationManager->getHive()->consumePollen(m_box_num, eating_y, this, m_pollen_required_day); /** \todo Add how to choose box for consuming pollen. */
   }
}

void HoneyBee_Worker::putDebrisInHive(){
   if(m_OurPopulationManager->shouldBeePutDebrisInHive()){
      getHive()->increaseGlobalWaste();
      if(m_MyBiologicalAgent->getNosemaA()>0){
         getHive()->setGlobalApis(true);
      }
      if(m_MyBiologicalAgent->getNosemaC()>0){
         getHive()->setGlobalCeranae(true);
      }
   }
}

double HoneyBee_Worker::supplyBodyTemperature(){
   //if(m_current_activity==TaskType::WarmingUp){
   //   return m_current_body_temperature-cfg_HoneyBeeIncreaseTemperatureWarmingUp.value();
   //}
   return m_current_body_temperature;
}


//******************************************************************************************************
//***************************** START HoneyBee_DroneEgg Class *****************************************
//******************************************************************************************************

const CellType HoneyBee_DroneEgg::cellType=CellType::Drone_Egg;

HoneyBee_DroneEgg::HoneyBee_DroneEgg(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerEgg(a_box_num, a_x, a_y, a_z, p_L, p_BPM)
{
   //m_development_time=3;
   m_next_stage_time=cfg_HoneyBeeDroneEggDevelopmentTime.value();
   inHive = true;
}

void HoneyBee_DroneEgg::nextStage() {
   m_OurPopulationManager->CreatObject<HoneyBee_DroneEgg,HoneyBee_DroneLarva>(this);
   inHive = true;
}

// void HoneyBee_DroneEgg::Step()
// {
//    if (m_StepDone || m_CurrentStateNo == -1) return;
//    m_StepDone = true;
// }

//******************************************************************************************************
//*************************** START HoneyBee_DroneLarva Class *****************************************
//******************************************************************************************************

const CellType HoneyBee_DroneLarva::cellType=CellType::Drone_Larva;

HoneyBee_DroneLarva::HoneyBee_DroneLarva(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerLarva(a_box_num, a_x, a_y, a_z, p_L, p_BPM)
{
   m_next_stage_time = cfg_HoneyBeeDroneLarvaDevelopmentTime.value();
   inHive = true;

}

void HoneyBee_DroneLarva::ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   //HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
   HoneyBee_WorkerLarva::ReInit(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
   m_next_stage_time = cfg_HoneyBeeDroneLarvaDevelopmentTime .value();
   inHive = true;
}

void HoneyBee_DroneLarva::nextStage() {
   m_OurPopulationManager->CreatObject<HoneyBee_DroneLarva,HoneyBee_DronePupa>(this);
}

void HoneyBee_DroneLarva::updateFoodReqLarva(){
   int current_develop_day = (int)m_development_time;
   m_sugar_required_day = m_sugar_required_day-m_sugar+m_OurPopulationManager->supplyFoodReqDroneLarva(2, current_develop_day);
   m_pollen_required_day = m_pollen_required_day-m_pollen+m_OurPopulationManager->supplyFoodReqDroneLarva(1, current_develop_day);
   m_jelly_required_day = m_jelly_required_day-m_jelly+m_OurPopulationManager->supplyFoodReqDroneLarva(0, current_develop_day);
}

//******************************************************************************************************
//*************************** START HoneyBee_DronePupa Class *****************************************
//******************************************************************************************************

const CellType HoneyBee_DronePupa::cellType=CellType::Drone_Pupa;

HoneyBee_DronePupa::HoneyBee_DronePupa(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerPupa(a_box_num, a_x, a_y, a_z, p_L, p_BPM)
{
   m_next_stage_time = cfg_HoneyBeeDronePupaDevelopmentTime.value();
   inHive = true;
}

void HoneyBee_DronePupa::ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   //HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
   HoneyBee_WorkerPupa::ReInit(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
   m_next_stage_time = cfg_HoneyBeeDronePupaDevelopmentTime.value();
   inHive = true;
}


void HoneyBee_DronePupa::nextStage() {
   m_OurPopulationManager->CreatObject<HoneyBee_DronePupa,HoneyBee_Drone>(this);
}

//******************************************************************************************************
//*************************** START HoneyBee_Drone Class *****************************************
//******************************************************************************************************

const CellType HoneyBee_Drone::cellType=CellType::Empty;

HoneyBee_Drone::HoneyBee_Drone(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_DronePupa(a_box_num, a_x, a_y, a_z, p_L, p_BPM)
{
   m_next_stage_time=cfg_HoneyBeeDroneMaxAge.value();;
   m_Age = 25;
   inHive = true;
}

void HoneyBee_Drone::ReInit(const int a_box_num, const int  a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_DronePupa::ReInit(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
   inHive = true;
   m_next_stage_time=cfg_HoneyBeeDroneMaxAge.value();;
   m_Age = 25;
}

void HoneyBee_Drone::Step()
{
   updateDevelopmentTime();
   if (m_development_time >= m_next_stage_time){
      st_Dying();
   }
   m_StepDone = true;
   if (m_StepDone || m_CurrentStateNo == -1) return;
}

/////////////////////////Queen//////////////////////

const CellType HoneyBee_Queen::cellType=CellType::Queen;

HoneyBee_Queen::HoneyBee_Queen(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
   : HoneyBee_WorkerPupa(a_box_num, a_x, a_y, a_z, p_L, p_BPM)

{
   m_development_time = 999999;
   m_next_stage_time=999999;
   m_laying_egg_rate = 1.0;
}

void HoneyBee_Queen::ReInit(const int a_box_num, const int  a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
   HoneyBee_Base::ReInit(a_box_num, a_x, a_y, a_z, p_L, p_BPM);
   m_laying_egg_rate = 1.0;
}

/*
bool HoneyBee_Queen::FindAvailableFrameSide(int* current_z, int*availab_cell_num){
   std::random_shuffle( m_OurPopulationManager->hive->random_z_index.begin(), m_OurPopulationManager->hive->random_z_index.end());
   for(int i=0;i<m_OurPopulationManager->hive->random_z_index.size();i++){
      int temp_z = m_OurPopulationManager->hive->random_z_index[i];
      // here we assume the queen can move to any place within 10 mins
      if (temp_z != *current_z){
         *availab_cell_num = m_OurPopulationManager->hive->egg_cells[temp_z].size();
         *current_z = temp_z;
         SetZ(*current_z);
         if (availab_cell_num>0){
            return true;
         }
      }
   }
   return false;
}
*/
void HoneyBee_Queen::LayEggs(){
   if(m_OurLandscape->SupplyTemp()>cfg_HoneyBeeMinimumTemperatureForEgg.value()){
      int current_frame_side = Supply_m_Location_y();
      int egg_num_layed = m_OurPopulationManager->getHive()->layEggsInCells(cfg_HoneyBeeNumOfEggsPerTimestep.value(), m_box_num, current_frame_side); /** \todo Add how to choose box for laying eggs. */
   }   
}

/*
void HoneyBee_Queen::LayEggs(){
   blitz::Array<float,3> xx = m_OurPopulationManager->getHive()->Supply_m_Location_x();
   int xrange=xx.shape()[0]-1;
   int yrange=xx.shape()[1]-1;
   int zrange=xx.shape()[2]-1;
   HoneyBee_WorkerEgg egg(0,0,0,m_OurLandscape,m_OurPopulationManager);
   HoneyBee_DroneEgg egg_d(0,0,0,m_OurLandscape,m_OurPopulationManager);

   //get the queens z-index
   int current_queen_z = Supply_m_Location_z();

   //find out whether there is availabe cell for egg
   int availab_cell_num = m_OurPopulationManager->hive->egg_cells[current_queen_z].size();

   //if there is no available cells, then move to other place
   if (availab_cell_num<=0){
      //no availabe cell in the hive, return
      if(!FindAvailableFrameSide(&current_queen_z, &availab_cell_num)){
         SetX(random(xrange));
         SetY(random(yrange));
         return;      
      }
   }

   // first lay egg, if still have time find another frame side
   int egg_layed_count = 0;
   while(true){
      if(availab_cell_num>0){
         availab_cell_num-=1;
         egg_layed_count+=1;
         //randomly choose one cell
         int index = random(availab_cell_num-1);
         Coords3D temp_coord = m_OurPopulationManager->hive->egg_cells[current_queen_z][index];
         //remove it from egg_cells
         m_OurPopulationManager->hive->egg_cells[current_queen_z][index] = m_OurPopulationManager->hive->egg_cells[current_queen_z].back();
         m_OurPopulationManager->hive->egg_cells[current_queen_z].pop_back();

         if(m_OurPopulationManager->hive->m_celltype(temp_coord.x,temp_coord.y,temp_coord.z) == int(CellType::Worker_Egg)){
            //make the cell as Work_Egg_Layed
            m_OurPopulationManager->hive->m_celltype(temp_coord.x,temp_coord.y,temp_coord.z) = int(CellType::Worker_Egg_Layed);
            egg.SetX(temp_coord.x);
            egg.SetY(temp_coord.y);
            egg.SetZ(temp_coord.z);
            egg.SetAge(0);
            m_OurPopulationManager->CreatObject<HoneyBee_WorkerEgg, HoneyBee_WorkerEgg>(&egg);
         }
         if(m_OurPopulationManager->hive->m_celltype(temp_coord.x,temp_coord.y,temp_coord.z) == int(CellType::Drone_Egg)){
            m_OurPopulationManager->hive->m_celltype(temp_coord.x,temp_coord.y,temp_coord.z) = int(CellType::Drone_Egg_Layed);
            egg_d.SetX(temp_coord.x);
            egg_d.SetY(temp_coord.y);
            egg_d.SetZ(temp_coord.z);
            egg_d.SetAge(0);
            m_OurPopulationManager->CreatObject<HoneyBee_DroneEgg, HoneyBee_DroneEgg>(&egg_d);
         }
      }

      if (egg_layed_count>=m_OurPopulationManager->m_eggsperstep){
         break;
      }
      else{
         if(!FindAvailableFrameSide(&current_queen_z, &availab_cell_num)){
            SetX(random(xrange));
            SetY(random(yrange));
            break;      
         }
      }
   }   
}
*/

void HoneyBee_Queen::Step(){
   //std::cout << "Queen step\n";
   if (m_StepDone || m_CurrentStateNo == -1) return;
   bool should_lay = false;

   //need to add calculation whether laying egg or not, for now only the environmental temparature is used

   int temp_month = m_OurLandscape->SupplyMonth();
   if(temp_month>=3 && temp_month<=8) {
      should_lay = true;
   }

   //if (temp_month==9 && m_OurLandscape->SupplyDayInMonth()<=15) should_lay = true;


   if (should_lay) LayEggs(); //Rember to put this back, we need the queen to lay eggs.
   
   m_StepDone = true;
}

void HoneyBee_Queen::updateSubletalPest(){
   double current_total_pest = supplySumPestBurden();
   if(current_total_pest>=cfg_HoneyBeeQueenMinBurdenPestEffect.value() &&current_total_pest<=cfg_HoneyBeeQueenMaxBurdenPestEffect.value()){
      m_laying_egg_rate *= ((current_total_pest-cfg_HoneyBeeQueenMinBurdenPestEffect.value())/(cfg_HoneyBeeQueenMaxBurdenPestEffect.value()-cfg_HoneyBeeQueenMinBurdenPestEffect.value()));
   }
   //too much pesticide, no eggs at all
   if(current_total_pest>=cfg_HoneyBeeQueenMaxBurdenPestEffect.value()){
      m_laying_egg_rate = 0;
   }
   //no sublethal effect anymore, recover from it.
   if(current_total_pest<=cfg_HoneyBeeQueenMinBurdenPestEffect.value() && m_laying_egg_rate<1){
      m_laying_egg_rate += cfg_HoneyBeeQueenRecoveryRatePestEffect.value();
      if (m_laying_egg_rate>1) m_laying_egg_rate=1;
   }
}
