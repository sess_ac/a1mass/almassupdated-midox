/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee_Colony.cpp
    \brief <B>The main source code for honeybee population manager</B>
    Version of  Feb 2020 \n
    By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include "blitz/array.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "BiologicalAgents.h"
#include "HoneyBee.h"
#include "HoneyBee_Colony.h"
#include "Hive.h"
#include "HiveThermodynamics.h"
#include "BeekeepingManagementPractice.h"

//---------------------------------------------------------------------------------------
// Externals
extern MapErrorMsg *g_msg;

//---------------------------------------------------------------------------------------
#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

extern std::default_random_engine g_std_rand_engine;

//field data resource file
CfgStr l_map_resource_file( "MAP_RESOURCE_FILE", CFG_CUSTOM, "ES_resource.txt" );

// Configuration inputs - parameter default values
/** \brief The minimum time step considered for any bees */
CfgFloat cfg_EggDailyMortality("HONEYBEE_EGGDAILYMORT", CFG_CUSTOM, 0.001);
/** \brief The minimum time step considered for any bees */
CfgFloat cfg_LarvaDailyMortality("HONEYBEE_LARVADAILYMORT", CFG_CUSTOM, 0.001);
/** \brief The basic needing of sugar per day */
CfgFloat cfg_BasicSugarDay("HONEYBEE_BASICSUGARDAY", CFG_CUSTOM, 3);
/** \brief The basic needing of pollen per day */
CfgFloat cfg_BasicPollenDay("HONEYBEE_BASICPOLLENDAY", CFG_CUSTOM, 1);
static CfgInt cfg_eggsperstep( "EGGSPERSTEP", CFG_CUSTOM, 0); //step size in mins
//TO DEL ABOVE

/** \brief Degree scale for resource */
CfgInt cfg_HoneyBeeResourceScaleDegree("HONEY_BEE_RESOURCE_SCALE_DEGREE", CFG_CUSTOM, 1);
//These are the parameters for food requirement
CfgStr cfg_HoneyBeeWorkerLarvaFoodFile("HONEY_BEE_WORKER_LARVA_FOOD_FILE",CFG_CUSTOM,"HoneyBee/worker_larva_food.txt");
CfgStr cfg_HoneyBeeDroneLarvaFoodFile("HONEY_BEE_DRONE_LARVA_FOOD_FILE",CFG_CUSTOM,"HoneyBee/drone_larva_food.txt");
CfgStr cfg_HoneyBeeWorkerActivitiesPrioritiesFile("HONEY_BEE_WORKER_ACTIVITIES_PRIORITIES_FILE", CFG_CUSTOM, "HoneyBee/activities_priorities.txt");
CfgStr cfg_HoneyBeeWorkerActivitiesRequirementFile("HONEY_BEE_WORKER_ACTIVITIES_REQUIREMENT_FILE",CFG_CUSTOM, "HoneyBee/activities_food_time_requirement.txt");
CfgStr cfg_HoneyBeeWorkerActivitiesTempeIncreFile("HONEY_BEE_WORKER_ACTIVITIES_TEMPE_INCRE_FILE", CFG_CUSTOM, "HoneyBee/activities_temp_incre.txt");
CfgStr cfg_HoneyBeeWorkerHPGFile("HONEY_BEE_WORKER_HPG_FILE", CFG_CUSTOM, "HoneyBee/worker_HPG.txt");
CfgInt cfg_HoneyBeeStepSize( "HONEY_BEE_STEP_SIZE", CFG_CUSTOM, 10 ); //step size in mins

extern CfgFloat cfg_HoneyBeeWorkerLarvaDevelopmentTime;
extern CfgInt cfg_HoneyBeeMaxBeeNumPerCell;
extern CfgFloat cfg_HoneyBeeDroneLarvaDevelopmentTime;
extern CfgInt cfg_HoneyBeeNeedForScoutingNectarNum;
extern CfgInt cfg_HoneyBeeNeedForScoutingPollenNum;

CfgFloat cfg_HoneyBeeTemperatureLimitToDefineColdDays("HONEY_BEE_TEMPERATURE_TO_DEFINE_COLD", CFG_CUSTOM, 5);
CfgInt cfg_HoneyBeeMaxColdDaysForBeePuttingDebrisInHive("HONEY_BEE_MAX_DAYS_PUTTING_DEBRIS_IN_HIVE", CFG_CUSTOM, 7);
CfgInt cfg_HoneyBeeInitialBeeNumPerCell("HONEY_BEE_INITIAL_BEE_NUM_PER_CELL", CFG_CUSTOM, 5);

CfgInt cfg_HoneyBeeInitialBoxNum("HONEY_BEE_INITIAL_BOX_NUM", CFG_CUSTOM, 1);

//Setting for hive initialization
/** \brief Number of cells in a row. -x */
CfgInt cfg_HoneyBeeCellsNumInRow("HONEY_BEE_CELLS_NUM_IN_ROW", CFG_CUSTOM, 100);
/** \brief Number of frames in the hive. -y*/
CfgInt cfg_HoneyBeeFramesNumInHive("HONEY_BEE_FRAMES_NUM_IN_HIVE", CFG_CUSTOM, 10);
/** \brief Number of cells in a column. -z*/
CfgInt cfg_HoneyBeeCellsNumInColumn("HONEY_BEE_CELLS_NUM_IN_COLUMN", CFG_CUSTOM, 80);
/** \brief Cell radius. */
CfgFloat cfg_HoneyBeeCellRadius("HONEY_BEE_CELL_RADIUS", CFG_CUSTOM, 5);
/** \brief Frame Spaceing. */
CfgInt cfg_HoneyBeeFrameSpacing("HONEY_BEE_FRAME_SAPCING", CFG_CUSTOM, 8);
/** \brief Side offset. */
CfgInt cfg_HoneyBeeHiveSideOffset("HONEY_BEE_HIVE_SIDE_OFFSET", CFG_CUSTOM, 8);

//Parameters for pesticide
extern CfgInt l_pest_NoPPPs;

//Parameters for the maximum percent of worker bees with different tasks
CfgFloat cfg_HoneyBeeMaxGuarderPercent("HONEY_BEE_MAX_GUARDER_NUM_PERCENT", CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeMaxOutForNectarPercent("HONEY_BEE_MAX_OUT_FOR_NECTAR_PERCENT", CFG_CUSTOM, 30);
CfgFloat cfg_HoneyBeeMaxOutForPollenPercent("HONEY_BEE_MAX_OUT_FOR_NECTAR_PERCENT", CFG_CUSTOM, 15);
CfgFloat cfg_HoneyBeeForagingMaxWindSpeed("HONEY_BEE_FORAGING_MAX_WIND_SPEED",CFG_CUSTOM, 24.0);

CfgInt cfg_HoneyBeeForagingStrategy("HONEY_BEE_FORAGEING_STRATEGY", CFG_CUSTOM, 1); //1:distance, 2:area, 3:amount, 4:quality, 5:gain
CfgInt cfg_HoneyBeeScoutingStrategy("HONEY_BEE_SCOUTING_STRATEGY", CFG_CUSTOM, 1); //1:random, 2:distance, 3:area
CfgInt cfg_HoneyBeeHivePositionX("HONEY_BEE_HIVE_POSITION_X", CFG_CUSTOM, 5000);
CfgInt cfg_HoneyBeeHivePositionY("HONEY_BEE_HIVE_POSITION_Y", CFG_CUSTOM, 5000);


extern CfgFloat cfg_HoneyBeeMiniTemperatureForOutActivities;
extern CfgBool cfg_ReallyBigOutput_used;
extern CfgFloat cfg_HoneyBeeWarmingTemperatureByHive;
extern CfgFloat cfg_HoneyBeeVolumeForagedPollenOneTime;
extern CfgFloat cfg_HoneyBeeVolumeForagedNectarOneTime;

//---------------------------------------------------------------------------------------
// Assign default static member values (these will be changed later).

//double HoneyBee_WorkerEgg::m_EggDailyMortality = 0;
//double HoneyBee_WorkerEgg::m_LarvaDailyMortality = 0;

//---------------------------------------------------------------------------

void HoneyBee_Colony::readHPGFile(string inputfile){
    int num_HPG, num_days;
    ifstream ist(inputfile, ios::in);
    ist >> num_HPG;
    ist >> num_days;
    string temp_dumy;
    for (int i=0; i<num_HPG; i++){
        m_worker_HPG_in_day.push_back(std::vector<double>());
        for (int j=0; j<num_days; j++){
            double temp;
            ist >> temp;
            m_worker_HPG_in_day.at(i).push_back(temp);
        }
    }
}

void HoneyBee_Colony::readWorkerLarvaRequirement(string inputfile, int number_col, int number_row){
    ifstream ist(inputfile, ios::in);
    for (int i=0;i<number_row;i++){
        for (int j=0;j<number_col;j++){
            double temp_input;
            ist >> temp_input;
            m_food_day_worker_larva.at(j).push_back(temp_input);
        }
    }
}

void HoneyBee_Colony::readDroneLarvaRequirement(string inputfile, int number_col, int number_row){
    ifstream ist(inputfile, ios::in);
    for (int i=0;i<number_row;i++){
        for (int j=0;j<number_col;j++){
            double temp_input;
            ist >> temp_input;
            m_food_day_drone_larva.at(j).push_back(temp_input);
        }
    }
}

void HoneyBee_Colony:: readActivitiesPrioritiesFile(string inputfile){
    ifstream ist(inputfile, ios::in);
    ist >> num_activities;
    ist >> days_activities_priorities;
    string temp_dumy;
    m_activities_priorities.resize(days_activities_priorities,EnumClassAsIndex(TaskType::Count));
    m_current_weighted_activities_priorities.resize(days_activities_priorities,EnumClassAsIndex(TaskType::Count));

    m_activities_priorities = 0;
    
    //ignore the activities names
    for (int i=0; i<num_activities;i++){
        ist >> temp_dumy;
    }
    for (int i=0; i<days_activities_priorities; i++){
        //j starts at 1 for that the first activity is null
        for(int j=1; j<num_activities+1;j++){
            int temp_int;
            ist >> temp_int;
            m_activities_priorities(i,j)=temp_int;
            //cout<<m_activities_priorities(i,j);
        }
    }
}

void HoneyBee_Colony::readActivitiesTempeIncreFile(string inputfile){
    ifstream ist(inputfile, ios::in);
    m_activities_temperature_incre.resize(EnumClassAsIndex(TaskType::Count));
    m_activities_temperature_incre = 0;
    int temp_num_activities;
    ist >> temp_num_activities;
    string temp_dumy;   
    //ignore the activities names
    for (int i=0; i<temp_num_activities;i++){
        ist >> temp_dumy;
    }
    double temp_val;
    for(int i=0; i<temp_num_activities;i++){
        ist >> temp_val;
        m_activities_temperature_incre(i+1)=temp_val;
    }
}

void HoneyBee_Colony::readActivitiesRequirementFile(string inputfile){
    m_metabolic_rates_sugar_worker.resize(EnumClassAsIndex(TaskType::Count));
    m_metabolic_rates_sugar_worker = 10;
    m_metabolic_rates_pollen_worker.resize(EnumClassAsIndex(TaskType::Count));
    m_metabolic_rates_pollen_worker = 10;
    m_required_time_activities.resize(EnumClassAsIndex(TaskType::Count));
    m_required_time_activities = 1;
    ifstream ist(inputfile, ios::in);
    int temp_num_activities;
    int temp_num_requirement;
    ist >> temp_num_activities;
    ist >> temp_num_requirement;
    string temp_dumy;   
    //ignore the activities names
    for (int i=0; i<temp_num_activities;i++){
        ist >> temp_dumy;
    }
    double temp_val;
    for(int i=0; i<temp_num_activities;i++){
        ist >> temp_val;
        m_metabolic_rates_sugar_worker(i+1)=temp_val;
    }
    for(int i=0; i<temp_num_activities;i++){
        ist >> temp_val;
        m_metabolic_rates_pollen_worker(i+1)=temp_val;
    }
    for(int i=0; i<temp_num_activities;i++){
        ist >> temp_val;
        m_required_time_activities(i+1)=temp_val;
    }
}

blitz::Array<int, 1> HoneyBee_Colony::supplyTaskProritesAge(int development_age){
    if(development_age>m_current_weighted_activities_priorities.ubound(blitz::firstDim)){
        development_age = m_current_weighted_activities_priorities.ubound(blitz::firstDim);
    }
    //cout<<m_current_weighted_activities_priorities(development_age, blitz::Range::all());
    return m_current_weighted_activities_priorities(development_age, blitz::Range::all());
}

void HoneyBee_Colony::DoLast(){
    //all have finishd the steps in a whole, here are the tasks that are needed to update only once per day
    if(m_step_num_in_a_day==m_max_step_in_a_day){
        //no need to reinitialise the flower resource polygons, this is only needed once when creating the manager
        //if (m_available_pollen_poly.size()< 1 || m_available_nectar_poly.size()<1 ){
        //    reinitializeAllAvailablePNRefs();
        //}
        m_hive->updatePesticideConcentration(); //update pesticide concentration in the hive
        //updateAvailablePNefs(); //update the resource in the landscape, these should be done everyday in the morning.
        //reinitializeAllAvailablePNRefs();
        //count the cold days in a row
        if(m_TheLandscape->SupplyTemp()<=cfg_HoneyBeeTemperatureLimitToDefineColdDays.value()){
            m_cold_days_in_a_row++;
        }
        else{
            m_cold_days_in_a_row=0;
        }
    }
    //add step counter by 1
    m_step_num_in_a_day+=1;
    if(m_step_num_in_a_day>m_max_step_in_a_day){
        m_step_num_in_a_day=1; //reset the counter as 1, a new day starts when DoFirst() calls
    }
    //calculate the hour in a day
    m_step_num_in_a_hour+=1;
    if(m_step_num_in_a_hour==6) //an hour is done
    {
        m_hour_in_a_day += 1;
        m_step_num_in_a_hour = 0;
        if(m_hour_in_a_day == 24) //a day is done
        {
            m_hour_in_a_day = 0;
        }        
    }
    /*
    //if(m_TheLandscape->SupplyTemp()<=cfg_HoneyBeeTemperatureLimitToDefineColdDays.value()){
        m_our_thermodynamics->setEnviromentalTemperature(m_TheLandscape->SupplyTemp());
        //reset the bee's body temperature
        m_our_thermodynamics->resetBeesTemperature();
        //update the bee's body temperature
        m_hive->calAverageTemperature();
        m_our_thermodynamics->updateGroupBeesTemp(m_hive->supplyAverageTemperatureArray());
        //do the convolutional operation
        m_our_thermodynamics->applyConvolutionalOperations();
        //copy back the bee's body temperature
        //copy back the cell's body temperature
        m_our_thermodynamics->copyCellTemperature(m_hive->supplyTemperatureArrayPointer());
        //std::cout<<*(hive->supplyTemperatureArrayPointer());
    //}
    */
}

void HoneyBee_Colony::DoFirst()
{
    //beginning of the day, update the available flower resource
    if(m_step_num_in_a_day==1){
        updateAvailablePNefs();
        updateResourceKnownPlace();
        m_foraging_hours_in_a_day = 0; //beginning of a day, reset the foraging hours.
        m_accumulated_nectar_daily = 0;
        m_accumulated_pollen_daily = 0;
        m_accumulated_sugar_daily = 0;
        m_accumulated_nectar_foraging = 0;
        //below are used for calibration of foraging
        calForagerNum();
        m_max_num_bees_out_nectar = m_num_max_foragers_nectar_daily;
        m_max_num_bees_out_pollen = m_num_max_foragers_pollen_daily;
        m_num_bees_out_nectar = 0;
        m_num_bees_out_pollen = 0;
        m_succ_trip_num_nectar = 0;
        m_fail_trip_num_nectar = 0;
        m_succ_trip_num_pollen = 0;
        m_fail_trip_num_pollen = 0;
    }

    //beginning of an hour, update the foraging flag
    if(m_step_num_in_a_hour==0){
        updateForagingFlag();
    }
    /*
    //first check whether it is too cold outside, if yes, move the adults bees closer to warm up each other
    if(m_TheLandscape->SupplyTemp()<=cfg_HoneyBeeTemperatureLimitToDefineColdDays.value() && !m_flag_gathering_done){
        // first we move the queen
        int center_x = m_hive->supplyXrange()/2;
        int center_y = m_hive->supplyYrange()/2;
        int center_z = m_hive->supplyZrange()/2;
        unsigned listindex = 8;
        unsigned temp_alive_num = 0;
        temp_alive_num = (unsigned) GetLiveArraySize(listindex);
        if(temp_alive_num>0){
            HoneyBee_Queen* current_queen = dynamic_cast<HoneyBee_Queen*> (TheArray.at(listindex).at(0));
            //m_hive->decBeeCount(current_queen->Supply_m_Location_x(), current_queen->Supply_m_Location_y(), current_queen->Supply_m_Location_z());
            current_queen->SetX(center_x);
            current_queen->SetY(center_y);
            current_queen->SetZ(center_z);
            //m_hive->incBeeCount(current_queen->Supply_m_Location_x(), current_queen->Supply_m_Location_y(), current_queen->Supply_m_Location_z());
        }
        //then the adult workers
        listindex = 3;
        temp_alive_num = (unsigned) GetLiveArraySize(listindex);
        unsigned current_index = 0;
        blitz::Array<bool, 2> temp_used_flag;
        temp_used_flag.resize(m_hive->supplyXrange(), m_hive->supplyZrange());
        temp_used_flag = false;
        if(temp_alive_num>0){
            int position_shift = 1;
            while(current_index<temp_alive_num){
                if(m_flag_gathering_done) break;
                for (int x=center_x-position_shift;x<=center_x+position_shift;x++){
                    if(m_flag_gathering_done) break;
                    for (int z=center_z-position_shift;z<=center_z+position_shift;z++){
                        if(m_flag_gathering_done) break;
                        int temp_same_position_count = 0;
                        if(x<0||x>=m_hive->supplyXrange()||z<0||z>=m_hive->supplyZrange()){
                            continue;
                        }
                        if(sqrt(double(pow((x-center_x),2)+pow((z-center_z),2)))<=position_shift){
                            while(temp_same_position_count<cfg_HoneyBeeMaxBeeNumPerCell.value() && !temp_used_flag(x,z)){
                                HoneyBee_Worker* current_worker = dynamic_cast<HoneyBee_Worker*> (TheArray.at(listindex).at(current_index));
                                if(current_worker->Supply_m_Location_z()>=0){
                                    m_hive->decBeeCount(current_worker->supplyBoxNum(), current_worker->Supply_m_Location_x(), current_worker->Supply_m_Location_y(), current_worker->Supply_m_Location_z());
                                }
                                //the bee is out for foraging, make it die for cold outside

                                
                                //else{
                                //    current_worker->makeMeDieOutHive();
                                //    cout<<"died out"<<current_worker->Supply_m_Location_x()<<current_worker->Supply_m_Location_y()<<current_worker->Supply_m_Location_z()<<endl;
                                //    current_index++;
                                //    break;
                                //}
                                current_worker->SetX(x);
                                current_worker->SetY(center_y);
                                current_worker->SetZ(z);
                                
                                m_hive->incBeeCount(current_worker->supplyBoxNum(), current_worker->Supply_m_Location_x(), current_worker->Supply_m_Location_y(), current_worker->Supply_m_Location_z());
                                current_index++;
                                temp_same_position_count++;
                                if(temp_same_position_count>=cfg_HoneyBeeMaxBeeNumPerCell.value()){
                                    temp_used_flag(x,z)=true;
                                }
                                if(current_index>=temp_alive_num){
                                    m_flag_gathering_done = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                position_shift+=1;
            }
            std::cout<<m_TheLandscape->SupplyDayInYear()<<"Gatherd: "<<m_flag_gathering_done<<" "<<center_x<<center_y<<center_z<<endl;
        }
        m_flag_gathering_done = true;
        if(g_date->DayInYear()<2){
            m_our_thermodynamics->resetWholeTemperature(m_TheLandscape->SupplyTemp()+cfg_HoneyBeeWarmingTemperatureByHive.value());
        }
    }
    

    if(m_TheLandscape->SupplyTemp()>cfg_HoneyBeeMiniTemperatureForOutActivities.value()){
        m_flag_gathering_done=false;
        m_hive->setAvailableJobNum(TaskType::ForagingNectar, 1000);
    }
    */

    //reset the bees' group body temperature to zero
    m_hive->clearBeesGroupTemperature();
    //check beekeeping management practice
    if(m_step_num_in_a_day==1){
        m_my_BMP->applyManagement();
    }  
    //for now, only one thing here, update the available job numbers in the hive
    m_hive->evaporateNectar();
    m_hive->updateAvailableJobNums();
    //m_hive->updateAvailableJobNums();
    //update the maximum number of bees for guarding, foraging and scouting
    m_max_num_bees_out_guarding = GetLiveArraySize(3)*cfg_HoneyBeeMaxGuarderPercent.value()/100;
    //m_max_num_bees_out_nectar = GetLiveArraySize(3)*cfg_HoneyBeeMaxOutForNectarPercent.value()/100;
    //m_max_num_bees_out_pollen = GetLiveArraySize(3)*cfg_HoneyBeeMaxOutForPollenPercent.value()/100;
    if(m_TheLandscape->SupplyTemp()>cfg_HoneyBeeMiniTemperatureForOutActivities.value()){
        m_hive->setAvailableJobNum(TaskType::Guarding, m_max_num_bees_out_guarding+2-m_current_num_bees_out_guarding);
    }
    
    //the four lines below are needed to be deleted
    m_hive->setAvailableJobNum(TaskType::ForagingNectar, 1000000);
    m_hive->setAvailableJobNum(TaskType::ForagingPollen, 1000000);
    //m_hive->setAvailableJobNum(TaskType::Guarding, 0);
    //m_max_num_bees_out_nectar = 10000;
    //m_max_num_bees_out_pollen = 3000;

    /*
    if(m_step_num_in_a_day<36 || m_step_num_in_a_day>108){
        m_hive->setAvailableJobNum(TaskType::ForagingNectar, 0);
        m_hive->setAvailableJobNum(TaskType::ForagingPollen, 0);
        m_hive->setAvailableJobNum(TaskType::Guarding, 0);
        m_max_num_bees_out_guarding=1;
        m_max_num_bees_out_nectar=1;
        m_max_num_bees_out_pollen=1;
    }
    */

    //disable the out hive activity
    if(!m_foraging_flag){
        m_hive->setAvailableJobNum(TaskType::ForagingNectar, 0);
        m_hive->setAvailableJobNum(TaskType::ForagingPollen, 0);
        m_hive->setAvailableJobNum(TaskType::Guarding, 0);
        //m_max_num_bees_out_guarding=1;
        //m_max_num_bees_out_nectar=1;
        //m_max_num_bees_out_pollen=1;
    }
    
    for (int i= 0; i<=m_activities_priorities.ubound(blitz::firstDim);i++){
        //cout<<m_current_weighted_activities_priorities;
        //cout<<m_activities_priorities;
        //cout<<hive->supplyNumsAvailabeTasks();
        m_current_weighted_activities_priorities(i,blitz::Range::all())= (m_activities_priorities(i, blitz::Range::all())*(m_hive->supplyNumsAvailabeTasks()));
        //if guarding needs, we need to increase it s priorities here.
        //cout<<m_current_weighted_activities_priorities;
        //cout<<m_activities_priorities;
        //cout<<hive->supplyNumsAvailabeTasks();
    }    
    //std::cout << m_hive->supplyNumsAvailabeTasks();
}

HoneyBee_Colony::~HoneyBee_Colony (void)
{
    std::cout<<"~HoneyBee_Colony";
    //    
    delete m_my_BMP;
    delete m_our_thermodynamics;
    //delete m_hive;
}


HoneyBee_Colony::HoneyBee_Colony(Landscape* L) : Population_Manager(L, 9)
{
    #ifdef __loadFieldResourceData
        loadFieldData();
    #endif
    m_flag_gathering_done = false;
    m_cold_days_in_a_row=0;
    m_num_foraged_nectar=0;
    m_current_num_bees_out_guarding = 0;
    m_max_num_bees_out_guarding = 0;
    m_guarding_required = true;
    m_num_bees_out_pollen = 0;
    m_num_bees_out_nectar = 0;
    m_num_bees_in_receiving_nectar = 0;
    m_step_num_in_a_day = 1;
    m_step_num_in_a_hour = 0;
    m_hour_in_a_day = 0;
    m_max_step_in_a_day = 1440/cfg_HoneyBeeStepSize.value();
    m_percentage_timestep_in_a_day = (double)(cfg_HoneyBeeStepSize.value())/1440.0;
    //read food requirement for worker larva
    //we have three type of food requirement
    for (int i = 0; i<3; i++){
        m_food_day_worker_larva.push_back(std::vector<double>());
        m_food_day_drone_larva.push_back(std::vector<double>());
    }
    readWorkerLarvaRequirement(cfg_HoneyBeeWorkerLarvaFoodFile.value(), 3, cfg_HoneyBeeWorkerLarvaDevelopmentTime.value());
    readDroneLarvaRequirement(cfg_HoneyBeeDroneLarvaFoodFile.value(), 3, cfg_HoneyBeeDroneLarvaDevelopmentTime.value());
    readActivitiesPrioritiesFile(cfg_HoneyBeeWorkerActivitiesPrioritiesFile.value());
    m_resource_scale_degree = cfg_HoneyBeeResourceScaleDegree.value();
    readActivitiesRequirementFile(cfg_HoneyBeeWorkerActivitiesRequirementFile.value());
    readActivitiesTempeIncreFile(cfg_HoneyBeeWorkerActivitiesTempeIncreFile.value());
    readHPGFile(cfg_HoneyBeeWorkerHPGFile.value());
    //Initialize the vector of vector for pollen and nectar
    for(int i=0; i< 360/m_resource_scale_degree; i++){
        m_available_nectar_poly.push_back( vector<struct_Resource_PN>() );
        m_available_pollen_poly.push_back( vector<struct_Resource_PN>() );
    }

    // Load List of Animal Classes
    m_ListNames[0] = "Worker Egg";
    m_ListNames[1] = "Worker Larva";
    m_ListNames[2] = "Worker Pupa";
    m_ListNames[3] = "Worker Adult";
    m_ListNames[4] = "Drone Egg";
    m_ListNames[5] = "Drone Larva";
    m_ListNames[6] = "Drone Pupa";
    m_ListNames[7] = "Drone Adult";
    m_ListNames[8] = "Queen";
    m_ListNameLength = 9;
    m_SimulationName = "HoneyBee";
	
    currentID=0;
    m_our_thermodynamics = new HiveThermodynamics(2+cfg_HoneyBeeCellsNumInRow.value(), 2+4*cfg_HoneyBeeFramesNumInHive.value(), 2+cfg_HoneyBeeCellsNumInColumn.value());
    //m_our_thermodynamics->resetWholeTemperature(34.5);
    //make a new hive and put it in the center of the landscape
    m_hive = new Hive(cfg_HoneyBeeInitialBoxNum.value(), cfg_HoneyBeeCellsNumInRow.value(), cfg_HoneyBeeFramesNumInHive.value(),cfg_HoneyBeeCellsNumInColumn.value(), cfg_HoneyBeeCellRadius.value(), cfg_HoneyBeeFrameSpacing.value(), cfg_HoneyBeeHiveSideOffset.value(), m_our_thermodynamics, cfg_HoneyBeeHivePositionX.value(),cfg_HoneyBeeHivePositionY.value(), this, m_TheLandscape);
    m_my_BMP = new BMP(m_hive, this);
    int xrange=getHive()->supplyXrange();
    int yrange=getHive()->supplyYrange();
    int zrange=getHive()->supplyZrange();
    struct_HoneyBee bee;
    //HoneyBee_Worker bee(0,0,0, m_TheLandscape,this);
    bee.L = m_TheLandscape;
    bee.BPM = this;

//    waggleDance.addDance(5,25,.3);

    int middle_frame_side = yrange/2;
    for (int i = middle_frame_side-1; i<=middle_frame_side;i++){
        for (int j=0;j<xrange;j++ ){
            for (int k=0; k<zrange;k++){
                if(m_hive->getCellType(0,j,i,k)==CellType::Worker_Egg || m_hive->getCellType(0,j,i,k)==CellType::Drone_Egg){
                    for (int l=0; l<cfg_HoneyBeeInitialBeeNumPerCell.value();l++){
                        bee.x = j;
                        bee.y = i;
                        bee.z = k;
                        bee.age =40+random(7)-3; //we need to set how old are they
                        bee.m_developmentTime = random(7);
                        bee.m_inhive = true;
                        bee.box_num=0; //from the beginning there is only one box
                        //std::cout << bee.x << " " << bee.y << " " << bee.z << std::endl;        
                        CreatObject<HoneyBee_Worker,HoneyBee_Worker>(NULL, &bee);
                    }
                }
                /*
                m_hive->setCellType(j,i,k,(int)CellType::Worker_Egg);
                //make few drones
                if(g_rand_uni()<0.05){
                    m_hive->setCellType(j,i,k,(int)CellType::Drone_Egg);
                
            }    */
            }
        }
    }

    /*
    for (int i=0; i< 1000; i++) // This will need to be an input variable (config)
	{
        int temp_x = random(xrange);
        int temp_y = random(yrange);
        int temp_z = random(zrange);
        bee.x = temp_x;
        bee.y = temp_y;
        bee.z = temp_z;
        bee.age =23; //we need to set how old are they
        bee.m_developmentTime = 23;
        bee.m_inhive = true;
        std::cout << bee.x << " " << bee.y << " " << bee.z << std::endl;        
        CreatObject<HoneyBee_Worker,HoneyBee_Worker>(NULL, &bee);
    }

    for (int i=0; i< 1000; i++) // This will need to be an input variable (config)
	{
        int temp_x = random(m_TheLandscape->SupplySimAreaWidth());
        int temp_y = random(m_TheLandscape->SupplySimAreaWidth());
        int temp_z = random(zrange);
        bee.x = temp_x;
        bee.y = temp_y;
        bee.z = temp_z;
        bee.age =23; //we need to set how old are they
        bee.m_developmentTime = 23;
        bee.m_inhive = false;
        std::cout << bee.x << " " << bee.y << " " << bee.z << std::endl;        
        CreatObject<HoneyBee_Worker,HoneyBee_Worker>(NULL, &bee);
    }
    */

   
    //we put the queen in the middle
    bee.x = (int)(xrange/2);
    bee.y = middle_frame_side;
    bee.z = (int(yrange/2));
    bee.box_num = 0;
    CreatObject<HoneyBee_Queen,HoneyBee_Queen>(NULL, &bee);
    //spreadsheet.load();
    //This needs to be called before UpdateAvailablePNRefs(), but only for the first time
    calculateAngDisHivePoly();
    //from the beginning of simulation, we need to update the landscape resource
    reinitializeAllAvailablePNRefs();
    if ( cfg_ReallyBigOutput_used.value() ) {
        OpenTheReallyBigProbe();
    } 
    else{
        ReallyBigOutputPrb=NULL;
    }

    // the following are used for calibration of foraging model
    m_num_foragers_nectar_daily = 0;
    m_num_foragers_pollen_daily = 0;
    m_accumulated_nectar_daily = 0;
    m_accumulated_sugar_daily = 0;
    m_accumulated_pollen_daily = 0;
    m_accumulated_nectar_foraging = 0;
}

void HoneyBee_Colony :: addAgesAll(){
    unsigned size2;
    unsigned size1 =  (unsigned)SupplyListIndexSize();
    for ( unsigned listindex = 0; listindex < size1; listindex++ ) {
        size2 = (unsigned)GetLiveArraySize( listindex );
        for (unsigned j = 0; j < size2; j++) {
        dynamic_cast<HoneyBee_Base*>(SupplyAnimalPtr(listindex,j))->addAge();
        }
    }
}

void HoneyBee_Colony :: eatFoodAllAdult(){
    unsigned size2;
    unsigned size1 =  SupplyListIndexSize();
    unsigned listindex = 3;
    size2 = (unsigned)GetLiveArraySize( listindex );
    for (unsigned j = 0; j < size2; j++) {
    dynamic_cast<HoneyBee_Worker*>(SupplyAnimalPtr(listindex, j))->eatSugar();
    }
    
}

void HoneyBee_Colony :: nextStageAll(){
    unsigned size2;
    unsigned size1 =  (unsigned) SupplyListIndexSize();
    for ( unsigned listindex = 0; listindex < size1; listindex++ ) {
        size2 = (unsigned)GetLiveArraySize( listindex );
        for (unsigned j = 0; j < size2; j++) {
        HoneyBee_Base* current_bee = dynamic_cast<HoneyBee_Base*>(SupplyAnimalPtr(listindex,j));
        if (current_bee->supplyHBState() != toHoneyBeeS_NextStage && current_bee->supplyHBState() !=toHoneyBeeS_Die)
        {
            if (g_rand_uni() < current_bee->supplyMortalityRate()){
                current_bee->setHoneyBeeState(toHoneyBeeS_Die);
                current_bee->st_Dying();
                int temp_box_num = current_bee->supplyBoxNum();
                int x = current_bee->Supply_m_Location_x();
                int y = current_bee->Supply_m_Location_y();
                int z = current_bee->Supply_m_Location_z();
                m_hive->decBeeCount(temp_box_num,x,y,z);
                }
            if (current_bee->isReadyNextStage())
            {
                current_bee->nextStage();
                current_bee->setHoneyBeeState(toHoneyBeeS_NextStage);
            }
        }
        }
    }
}

void HoneyBee_Colony :: dayStep(){
    //addAgesAll();
    //nextStageAll();
    //let all the adult bees to refill food, for the sake of simplicity we only do this once perday, although it is not realistic
    //eatFoodAllAdult();
    //each day we need to update the availabe resource information on the landscape
    updateAvailablePNefs();
    m_step_num_in_a_day=0;
    //count the cold days in a row
    if(m_TheLandscape->SupplyTemp()<=cfg_HoneyBeeTemperatureLimitToDefineColdDays.value()){
        m_cold_days_in_a_row++;
    }
    else{
        m_cold_days_in_a_row=0;
    }
}

void HoneyBee_Colony :: updateAvailablePNefs(void){
    //pollen
    for (int i = 0; i<m_available_pollen_poly.size(); i++){
        for (int j=0; j<m_available_pollen_poly.at(i).size(); j++){
            int poly_id = m_available_pollen_poly.at(i).at(j).poly_id;
            m_available_pollen_poly.at(i).at(j).amount = m_TheLandscape->SupplyTotalPollen(poly_id);
            m_available_pollen_poly.at(i).at(j).quality = m_TheLandscape->SupplyPollen(poly_id);
            if(m_available_pollen_poly.at(i).at(j).quality.m_quantity>0 && m_available_pollen_poly.at(i).at(j).distance >0){
                m_available_pollen_poly.at(i).at(j).gain = m_available_pollen_poly.at(i).at(j).quality.m_quality/m_available_pollen_poly.at(i).at(j).quality.m_quantity/m_available_pollen_poly.at(i).at(j).distance*m_available_pollen_poly.at(i).at(j).poly_area;
            }

        }
    }
    //nectar
    for (int i=0; i<m_available_nectar_poly.size(); i++){
        for (int j=0; j<m_available_nectar_poly.at(i).size(); j++){
            int poly_id = m_available_nectar_poly.at(i).at(j).poly_id;
            m_available_nectar_poly.at(i).at(j).amount = m_TheLandscape->SupplyTotalNectar(poly_id);
            m_available_nectar_poly.at(i).at(j).quality = m_TheLandscape->SupplyNectar(poly_id);
            if(m_available_nectar_poly.at(i).at(j).quality.m_quantity>0 && m_available_nectar_poly.at(i).at(j).distance >0){
                m_available_nectar_poly.at(i).at(j).gain = m_available_nectar_poly.at(i).at(j).quality.m_quality/m_available_nectar_poly.at(i).at(j).quality.m_quantity/m_available_nectar_poly.at(i).at(j).distance*m_available_nectar_poly.at(i).at(j).poly_area;
            }
        }
    }
}

void HoneyBee_Colony::updateVisitedResource(void){
    //pollen
    //for (int i = 0; i<m_visited_poly_pollen_amount.size(); i++){
    //    m_visited_poly_pollen_amount.at(i)=m_TheLandscape->SupplyTotalPollen(m_visited_poly_pollen_amount.k)
    ;//}
}

void HoneyBee_Colony :: updateResourceKnownPlace(void){
    //pollen
    vector<struct_Known_Resource> temp_know_resource;
    for (int i = 0; i<m_known_pollen_pool.size(); i++){
        int poly_id = m_known_pollen_pool.at(i).poly_id;
        m_known_pollen_pool.at(i).quality = m_TheLandscape->SupplyPollen(poly_id);
        m_known_pollen_pool.at(i).amount = m_TheLandscape->SupplyTotalPollen(poly_id);
        if(m_known_pollen_pool.at(i).amount>0){
            temp_know_resource.push_back(m_known_pollen_pool.at(i));
        }
    }
    if(temp_know_resource.size()<m_known_pollen_pool.size()){
        m_known_pollen_pool.clear();
        for (int i = 0; i<temp_know_resource.size(); i++){
            if(temp_know_resource.at(i).distance>0 && temp_know_resource.at(i).quality.m_quantity>0){
                temp_know_resource.at(i).gain = temp_know_resource.at(i).quality.m_quality/temp_know_resource.at(i).quality.m_quantity/temp_know_resource.at(i).distance*temp_know_resource.at(i).poly_area;
            }
            else  temp_know_resource.at(i).gain = -1;
            m_known_pollen_pool.push_back(temp_know_resource.at(i));
        }
    }
    
    //nectar
    temp_know_resource.clear();
    for (int i = 0; i<m_known_nectar_pool.size(); i++){
        int poly_id = m_known_nectar_pool.at(i).poly_id;
        m_known_nectar_pool.at(i).quality = m_TheLandscape->SupplyNectar(poly_id);
        m_known_nectar_pool.at(i).amount = m_TheLandscape->SupplyTotalNectar(poly_id);
         if(m_known_nectar_pool.at(i).amount>0){
            temp_know_resource.push_back(m_known_nectar_pool.at(i));
        }
    }
    if(temp_know_resource.size()<m_known_nectar_pool.size()){
        m_known_nectar_pool.clear();
        for (int i = 0; i<temp_know_resource.size(); i++){
            if(temp_know_resource.at(i).distance>0 && temp_know_resource.at(i).quality.m_quantity>0){
                temp_know_resource.at(i).gain = temp_know_resource.at(i).quality.m_quality/temp_know_resource.at(i).quality.m_quantity/temp_know_resource.at(i).distance*temp_know_resource.at(i).poly_area;
            }
            else  temp_know_resource.at(i).gain = -1;
            m_known_nectar_pool.push_back(temp_know_resource.at(i));
        }
    }
}

void HoneyBee_Colony :: reinitializeAllAvailablePNRefs(void){
    //we remove all the available ones
    for(int i=0; i< 360/m_resource_scale_degree; i++){
        m_available_nectar_poly.at(i).clear();
        m_available_pollen_poly.at(i).clear();
    }

    struct_Resource_PN temp_resource;
    int temp_poly_id;
    vector <int> polys_with_flower_resource = m_TheLandscape->SupplyPolyWithFlowers();
    for (int i= 0; i<polys_with_flower_resource.size(); i++){
        temp_poly_id = polys_with_flower_resource.at(i);

        temp_resource.poly_id=temp_poly_id;
        temp_resource.distance=m_dis_hive_poly.at(temp_poly_id);
        temp_resource.poly_area = m_TheLandscape->SupplyPolygonArea(temp_poly_id);
        temp_resource.gain = -1;
        
        //pollen
        temp_resource.amount=m_TheLandscape->SupplyTotalPollen(temp_poly_id);
        temp_resource.quality=m_TheLandscape->SupplyPollen(temp_poly_id);
        m_available_pollen_poly.at(m_angle_hive_poly.at(temp_poly_id)).push_back(temp_resource);

        //nectar
        temp_resource.amount=m_TheLandscape->SupplyTotalNectar(temp_poly_id);
        temp_resource.quality=m_TheLandscape->SupplyNectar(temp_poly_id);
        m_available_nectar_poly.at(m_angle_hive_poly.at(temp_poly_id)).push_back(temp_resource);

    }
    //sort the source
    if (cfg_HoneyBeeScoutingStrategy.value() == 1){
         for(int i=0; i< 360/m_resource_scale_degree; i++){
            //std::random_shuffle(m_available_nectar_poly.at(i).begin(), m_available_nectar_poly.at(i).end());
            //std::random_shuffle(m_available_pollen_poly.at(i).begin(), m_available_pollen_poly.at(i).end());
            std::shuffle(m_available_nectar_poly.at(i).begin(), m_available_nectar_poly.at(i).end(), g_std_rand_engine);
            std::shuffle(m_available_pollen_poly.at(i).begin(), m_available_pollen_poly.at(i).end(), g_std_rand_engine);
        }
    }
    else{
        sortBothResource();
    } 
    

    /*
    //we loop all the polygens to update the two lists
    int num_poly = m_TheLandscape->SupplyNumberOfPolygons();
    struct_Resource_PN temp_resource;
    for (int i = 0; i<num_poly; i++){

        #ifdef __loadFieldResourceData
        //insert the poly ids from field data

        if (*find(m_field_data_poly.begin(), m_field_data_poly.end(), i) == i){
            temp_resource.amount=999;
            temp_resource.poly_id=i;
            temp_resource.distance=m_dis_hive_poly.at(i);
            temp_resource.quality=PollenNectarData(999, 1);
            m_available_pollen_poly.at(m_angle_hive_poly.at(i)).push_back(temp_resource);
            m_available_nectar_poly.at(m_angle_hive_poly.at(i)).push_back(temp_resource);
            continue;
        }

        #endif
        //pollen
        temp_resource.amount=m_TheLandscape->SupplyTotalPollen(i);
        temp_resource.poly_id=i;
        temp_resource.distance=m_dis_hive_poly.at(i);
        temp_resource.quality=m_TheLandscape->SupplyPollen(i);

        if (temp_resource.quality.m_quality>0 && m_TheLandscape->SupplyPolygonArea(i)>1000){
            m_available_pollen_poly.at(m_angle_hive_poly.at(i)).push_back(temp_resource);
        }
        //nectar
        temp_resource.amount=m_TheLandscape->SupplyTotalNectar(i);
        temp_resource.quality=m_TheLandscape->SupplyNectar(i);

        if (temp_resource.quality.m_quality>0 && m_TheLandscape->SupplyPolygonArea(i)>1000){
            m_available_nectar_poly.at(m_angle_hive_poly.at(i)).push_back(temp_resource);
        }
    }*/
}

void HoneyBee_Colony :: calculateAngDisHivePoly(void){
    double num_poly = m_TheLandscape->SupplyNumberOfPolygons();
    double half_width = m_TheLandscape->SupplySimAreaWidth()/2;
    double half_height = m_TheLandscape->SupplySimAreaHeight()/2;
    double diameter = sqrt(pow(half_width,2)+pow(half_height,2))*2;
    for (int i = 0; i<num_poly; i++){
        bool mirror_flag = false;
        double hive_x = m_hive->supplyHivePositionX();
        double hive_y = m_hive->supplyHivePositionY();
        double poly_centorid_x = m_TheLandscape->SupplyCentroidX(i);
        double poly_centorid_y = m_TheLandscape->SupplyCentroidY(i);
        double dif_x = poly_centorid_x-hive_x;
        double dif_y = poly_centorid_y-hive_y;
        double distance = sqrt(pow(dif_x,2)+pow(dif_y,2));
        if(distance > diameter/2) {
            distance = diameter-distance;
            mirror_flag = true;
        }
        m_dis_hive_poly.push_back(int(distance));
        double ang;
        if (dif_x>0) ang=atan(dif_y/dif_x);
        if (dif_y>=0 && dif_x<0) ang=M_PI+atan(dif_y/dif_x);
        if (dif_y<0 && dif_x<0) ang=-M_PI+atan(dif_y/dif_x);
        if (dif_y>0 && dif_x==0) ang=M_PI/2;
        if (dif_y<0 && dif_x==0) ang=-M_PI/2;
        if (ang<0) ang+=2*M_PI;
        if (mirror_flag){
            ang += M_PI; // mirror it
            if (ang > 2*M_PI) ang-=2*M_PI;
        }
        int temp_angle_in_degree = int(ang*360/2/M_PI);
        if(temp_angle_in_degree==360) temp_angle_in_degree = 0;
        m_angle_hive_poly.push_back(temp_angle_in_degree/m_resource_scale_degree);
    }
}

void HoneyBee_Colony :: sortBothResource(){
    for (int i=0; i<m_available_nectar_poly.size(); i++){
        std::sort (m_available_nectar_poly.at(i).begin(), m_available_nectar_poly.at(i).end(), sortResourceComparisonFunction);
        std::sort (m_available_pollen_poly.at(i).begin(), m_available_pollen_poly.at(i).end(), sortResourceComparisonFunction);
    }
}

void HoneyBee_Colony :: moveAvailableNectarToKnownOrUpdate(struct_Known_Resource a_place_to_add){
    int index_in_available = a_place_to_add.list_index;

    // remove the resource
    m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).amount -= cfg_HoneyBeeVolumeForagedNectarOneTime.value();
    //already in the known pool, just update it
    int found_index = -1;
    for(int i = 0; i<m_known_nectar_pool.size();i++){
        if(m_known_nectar_pool.at(i).poly_id == a_place_to_add.poly_id){ //found it in the know list
            found_index=i;
            break;
        }
    }
    if(found_index>-1){
        m_known_nectar_pool.at(found_index).count_visited++;
        m_known_nectar_pool.at(found_index).amount -= cfg_HoneyBeeVolumeForagedNectarOneTime.value();
        if(m_known_nectar_pool.at(found_index).amount<=0){ //remove it when it is empty
            m_known_nectar_pool.at(found_index) = m_known_nectar_pool.at(m_known_nectar_pool.size()-1);
            m_known_nectar_pool.pop_back();
            m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag=false;
        }
    }
    //if(m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag){
    //    int index_in_known = m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).index_know;
    //    m_known_nectar_pool.at(index_in_known).count_visited++;
    //}
    //add new one
    else
    {
        if(m_known_nectar_pool.size()<cfg_HoneyBeeNeedForScoutingNectarNum.value()){
            m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag=true;
            m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).index_know=m_known_nectar_pool.size();
            a_place_to_add.count_visited = 1;
            m_known_nectar_pool.push_back(a_place_to_add);
        }
        else{
            for(int i = 0; i<m_known_nectar_pool.size(); i++){
                bool comparasion_result = false;
                if(cfg_HoneyBeeForagingStrategy.value() == 1 && m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).distance<m_known_nectar_pool.at(i).distance && m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).amount>0){ comparasion_result = true;}//foraging strategy
                if(cfg_HoneyBeeForagingStrategy.value() == 2 && m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).poly_area>m_known_nectar_pool.at(i).poly_area && m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).amount>0){ comparasion_result = true;}
                if(cfg_HoneyBeeForagingStrategy.value() == 3 && m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).amount>m_known_nectar_pool.at(i).amount){ comparasion_result = true;}
                if(cfg_HoneyBeeForagingStrategy.value() == 4 && m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).quality.m_quality>m_known_nectar_pool.at(i).quality.m_quality){ comparasion_result = true;}
                if(cfg_HoneyBeeForagingStrategy.value() == 5 && m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).gain>m_known_nectar_pool.at(i).gain){ comparasion_result = true;}
                
                if(comparasion_result && m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).amount>0){
                    m_available_nectar_poly.at(m_known_nectar_pool.at(i).direction_index).at(m_known_nectar_pool.at(i).list_index).scouted_flag = false;
                    a_place_to_add.count_visited = 1;
                    m_known_nectar_pool.at(i)=a_place_to_add;
                    m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).index_know=i;
                    m_available_nectar_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag=true;
                    break;
                }
            }
        }
    }

}

void HoneyBee_Colony :: moveAvailablePollenToKnownOrUpdate(struct_Known_Resource a_place_to_add){
    int index_in_available = a_place_to_add.list_index;

    //remove the foraged resource
    m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).amount -= cfg_HoneyBeeVolumeForagedPollenOneTime.value();
    //already in the known pool, just update it
    int found_index = -1;
    for(int i = 0; i<m_known_pollen_pool.size();i++){
        if(m_known_pollen_pool.at(i).poly_id == a_place_to_add.poly_id){ //found it in the know list
            found_index=i;
            break;
        }
    }
    if(found_index>-1){
        m_known_pollen_pool.at(found_index).count_visited++;
        m_known_pollen_pool.at(found_index).amount -= cfg_HoneyBeeVolumeForagedPollenOneTime.value();
        if(m_known_pollen_pool.at(found_index).amount<=0){ //remove it when it is empty
            m_known_pollen_pool.at(found_index) = m_known_pollen_pool.at(m_known_pollen_pool.size()-1);
            m_known_pollen_pool.pop_back();
            m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag=false;
        }
    }
    //if(m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag){
    //    int index_in_known = m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).index_know;
    //    m_known_pollen_pool.at(index_in_known).count_visited++;
    //}
    //add new one
    else
    {
        if(m_known_pollen_pool.size()<cfg_HoneyBeeNeedForScoutingPollenNum.value()){
            m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag=true;
            m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).index_know=m_known_pollen_pool.size();
            a_place_to_add.count_visited = 1;
            m_known_pollen_pool.push_back(a_place_to_add);
        }
        else{
            m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag=true;
            for(int i = 0; i<m_known_pollen_pool.size(); i++){
                bool comparasion_result = false;
                if(cfg_HoneyBeeForagingStrategy.value() == 1 && m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).distance<m_known_pollen_pool.at(i).distance) {comparasion_result = true;} //foraging strategy
                if(cfg_HoneyBeeForagingStrategy.value() == 2 && m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).poly_area>m_known_pollen_pool.at(i).poly_area){comparasion_result = true;}
                if(cfg_HoneyBeeForagingStrategy.value() == 3 && m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).amount>m_known_pollen_pool.at(i).amount){comparasion_result = true;}
                if(cfg_HoneyBeeForagingStrategy.value() == 4 && m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).quality.m_quality>m_known_pollen_pool.at(i).quality.m_quality){comparasion_result = true;}
                if(cfg_HoneyBeeForagingStrategy.value() == 5 && m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).gain>m_known_pollen_pool.at(i).gain){comparasion_result = true;}
                
                if (comparasion_result&& m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).amount>0){
                    m_available_pollen_poly.at(m_known_pollen_pool.at(i).direction_index).at(m_known_pollen_pool.at(i).list_index).scouted_flag = false;
                    a_place_to_add.count_visited = 1;
                    m_known_pollen_pool.at(i)=a_place_to_add;
                    m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).index_know=i;
                    m_available_pollen_poly.at(a_place_to_add.direction_index).at(index_in_available).scouted_flag = true;
                    break;
                }
            }

        }
    }

}

bool HoneyBee_Colony :: supplyOneAvailableResourcePlace(int angle, HoneyBee_Worker* the_bee, struct_Known_Resource* known_pointer, int resource_type){
    vector<vector<struct_Resource_PN>>* a_current_resource;
    vector<struct_Known_Resource>* a_current_known_resource;
    double temp_threshold;
    if(resource_type==1){
        a_current_resource = &m_available_nectar_poly;
        a_current_known_resource = &m_known_nectar_pool;
        temp_threshold = 0*cfg_HoneyBeeVolumeForagedNectarOneTime.value();
    }
    if(resource_type==2){
        a_current_resource = &m_available_pollen_poly;
        a_current_known_resource = &m_known_pollen_pool;
        temp_threshold = 0*cfg_HoneyBeeVolumeForagedPollenOneTime.value();
    }
    //there is nothing in this direction, return false
    if((*a_current_resource).at(angle).size()<1){
        return false;
    }
    else{
        for(int i=0; i<(*a_current_resource).at(angle).size(); i++){     
            //the place has not been visited before  
            if(!((*a_current_resource).at(angle).at(i).scouted_flag)&&(*a_current_resource).at(angle).at(i).amount>temp_threshold){
                (*known_pointer).poly_id=(*a_current_resource).at(angle).at(i).poly_id;
                (*known_pointer).direction_index=angle;
                (*known_pointer).list_index=i;
                (*known_pointer).count_visited=0;
                (*known_pointer).distance=(*a_current_resource).at(angle).at(i).distance;
                (*known_pointer).quality=(*a_current_resource).at(angle).at(i).quality;
                (*known_pointer).size = (*a_current_resource).at(angle).at(i).poly_area;
                (*known_pointer).gain = (*known_pointer).quality.m_quality/(*known_pointer).quality.m_quantity/(*known_pointer).distance*(*known_pointer).size;
                (*a_current_resource).at(angle).at(i).gain = (*known_pointer).gain;
                //(*a_current_resource).at(angle).at(i).visited_time += 1;
                //if((*a_current_resource).at(angle).at(i).visited_time>10)  (*a_current_resource).at(angle).at(i).scouted_flag = true;
                (*known_pointer).amount = (*a_current_resource).at(angle).at(i).amount;
                (*known_pointer).poly_area = (*a_current_resource).at(angle).at(i).poly_area;
                return true;
            }
            //else{
                //(*known_pointer)=(*a_current_known_resource).at((*a_current_resource).at(angle).at(i).index_know);
                //return false;
            //}
        }              
    }
    //back from some place already know
    return false;
}

//For now, it is only distance based
bool HoneyBee_Colony :: sortResourceComparisonFunction(struct_Resource_PN a, struct_Resource_PN b) {
    if (cfg_HoneyBeeScoutingStrategy.value() == 2)
    {
        return (a.distance<b.distance);
    }
    if (cfg_HoneyBeeScoutingStrategy.value() == 3)
    {
        return(a.poly_area>b.poly_area);
    }
}


void HoneyBee_Colony::setHiveInfoforAdd(Hive* hive, int ob_type, int a_box_num, int x, int y, int z, int cellType){
    
    //when it grows up to an adult worker or drone, it hatches from the cell, so set the cell type as empty
    //if (ob_type == WorkerAdult_e || ob_type == DroneAdult_e || ob_type == Queen_e){
    if (ob_type == WorkerAdult_e || ob_type == DroneAdult_e){
        hive->incBeeCount(a_box_num, x, y, z);
        //hive->setCellType(x,y,z,(int)(CellType::Empty));
    }
    else{
        hive->setCellType(a_box_num, x,y,z, cellType);
    }
}

void HoneyBee_Colony::setBeePointforAdd(Hive* hive, int ob_type, int a_box_num, int x, int y, int z, HoneyBee_Base* a_bee){
    if (ob_type != WorkerAdult_e && ob_type != DroneAdult_e){
        getHive()->setBeePointer(a_box_num, x,y,z,a_bee);
    }
}

double HoneyBee_Colony::supplySugarForActivity(TaskType a_task){
    return m_metabolic_rates_sugar_worker(EnumClassAsIndex(a_task));
}

double HoneyBee_Colony::supplyTempeIncreActivity(TaskType a_task){
    return m_activities_temperature_incre(EnumClassAsIndex(a_task));
}

double HoneyBee_Colony::supplyPollenForActivity(TaskType a_task){
    return m_metabolic_rates_pollen_worker(EnumClassAsIndex(a_task));
}

double HoneyBee_Colony::supplyTimeForActivity(TaskType a_task){
    return m_required_time_activities(EnumClassAsIndex(a_task));
}

bool HoneyBee_Colony::shouldBeePutDebrisInHive(){
    return m_cold_days_in_a_row > cfg_HoneyBeeMaxColdDaysForBeePuttingDebrisInHive.value();
}

void HoneyBee_Colony::TheReallyBigOutputProbe(){
    
    if(m_step_num_in_a_day!=1){
        return;
    }
    

   /**
    //store data every hour
    if(m_step_num_in_a_hour !=0){
        return;
    }
    **/
    
    unsigned size2;
	unsigned size1 =  SupplyListIndexSize();
    std::cout<<m_TheLandscape->SupplyDayInYear()<<endl;
    //if(m_TheLandscape->SupplyDayInYear()==105){
    //    std::cout<<*(m_our_thermodynamics->supplyWholeCube())<<std::endl;
    //}
    fprintf(ReallyBigOutputPrb, "%d, ", m_TheLandscape->SupplyDayInYear());
    //for ( unsigned listindex = 0; listindex < size1; listindex++ ){
    //    size2 = (unsigned) GetLiveArraySize(listindex);
    //    fprintf(ReallyBigOutputPrb,"%d,  ", size2);
	//}
    /*
    fprintf(ReallyBigOutputPrb, "%d, ", (int)(sum(m_hive->getBeeCountArray())));
    if(TheArray.at(3).size()>0){
        //HoneyBee_Worker* current_worker = dynamic_cast<HoneyBee_Worker*> (TheArray.at(3).at(0));
        //fprintf(ReallyBigOutputPrb, "%d %f %f %d ", current_worker->supplyAge(), current_worker->supplyDeveloppedTime(), current_worker->supplyBodyTemperature(), current_worker->supplyHoneyBeeState());
        size2=(unsigned) GetLiveArraySize(3);
        double temp_max = -999;
        double temp_min = 999;
        int temp_state = -1;
        int x = -1;
        int y = -1;
        int z = -1;
        HoneyBee_Worker* current_worker;
        for(unsigned i=0;i<size2;i++){
            current_worker = dynamic_cast<HoneyBee_Worker*> (TheArray.at(3).at(i));
            if(current_worker->supplyBodyTemperature()>temp_max) {
                temp_max= current_worker->supplyBodyTemperature();
                temp_state=current_worker->supplyHoneyBeeState();
                x=current_worker->Supply_m_Location_x();
                y=current_worker->Supply_m_Location_y();
                z=current_worker->Supply_m_Location_z();
            }
            if(current_worker->supplyBodyTemperature()<temp_min) temp_min = current_worker->supplyBodyTemperature();
        }
        fprintf(ReallyBigOutputPrb, "%f, %f, %d, %d, %d, %d, ", temp_max, temp_min, temp_state, x, y, z);
        
    }
    fprintf(ReallyBigOutputPrb, "%f, ", m_TheLandscape->SupplyTemp());*/
    //fprintf(ReallyBigOutputPrb,"%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d", m_hive->supplyHoneyCellNum(), m_hive->supplyPollenCellNum(), m_hive->supplyNectarCellNum(), m_hive->supplyEggCellNum(), m_hive->supplyLarvaeCellNum(), m_hive->supplyPupaCellNum(), m_hive->supplyCleaningReqCellNum(), m_num_foraged_nectar, m_num_bees_out_nectar, m_max_num_bees_out_nectar, m_num_bees_out_pollen, m_max_num_bees_out_pollen);
    //store the number of adult bees
    //fprintf(ReallyBigOutputPrb, ", %d, %d", (GetLiveArraySize(3)+GetLiveArraySize(7)+GetLiveArraySize(8)), m_foraging_hours_in_a_day);
    /*
    for(int i=0; i<EnumClassAsIndex(TaskType::Count); i++){
        fprintf(ReallyBigOutputPrb, ", %d",  (m_hive->supplyNumsAvailabeTasks())(i));
    }
    */

    //these are used for calibration of the foraging model
    double temp_nectar, temp_sugar, temp_pollen;
    getDailyTotalResourceAmount(temp_nectar, temp_sugar, temp_pollen);
    fprintf(ReallyBigOutputPrb, "%f, %f, %f, %d, %d, %d, %f, %f, %f, %d, %d, %d, %d, %d, %d", m_accumulated_nectar_daily, m_accumulated_sugar_daily, m_accumulated_pollen_daily, m_num_max_foragers_nectar_daily, m_num_max_foragers_pollen_daily, m_foraging_hours_in_a_day, temp_nectar, temp_sugar, temp_pollen, m_num_bees_out_nectar, m_num_bees_out_pollen, m_succ_trip_num_nectar, m_fail_trip_num_nectar, m_succ_trip_num_pollen, m_fail_trip_num_pollen);

    fprintf(ReallyBigOutputPrb,"\n");
    fflush(ReallyBigOutputPrb);
}

int HoneyBee_Colony::supplyHivePositionX() {return m_hive->supplyHivePositionX();}
int HoneyBee_Colony::supplyHivePositionY() {return m_hive->supplyHivePositionY();}

void HoneyBee_Colony::updateForagingFlag(void){
    //if it is raining, no foraging
    if (m_TheLandscape->SupplyRainHour(m_hour_in_a_day)>0){
        m_foraging_flag = false;
        return;
    }
    //if it is too windy, no foraging
    if (m_TheLandscape->SupplyWindHour(m_hour_in_a_day)>cfg_HoneyBeeForagingMaxWindSpeed.value()){
        m_foraging_flag = false;
        return;
    }
    //if it is too cold or too dark, no foraging
    double current_temp = m_TheLandscape->SupplyTempHour(m_hour_in_a_day);
    double threshold_radiation = 2261.9* exp(-0.164*current_temp);
    if (m_TheLandscape->SupplyRadiationHour(m_hour_in_a_day)<threshold_radiation){
        m_foraging_flag = false;
        return;
    }
    m_foraging_flag =true;
    m_foraging_hours_in_a_day += 1; //add one hour for foraging.
}

#ifdef __loadFieldResourceData
    void HoneyBee_Colony::loadFieldData(){
        std::fstream resource_file(l_map_resource_file.value(), std::ios_base::in);
        int poly_id;
        while (resource_file >> poly_id){
            m_field_data_poly.push_back(poly_id);
        }
    }
#endif

void HoneyBee_Colony::calForagerNum(void){
    int current_day = m_TheLandscape->SupplyDayInYear();
    if (current_day>=305 || current_day<=59){
        m_num_max_foragers_nectar_daily=700;
        m_num_max_foragers_pollen_daily=300;
    }
    else{
        m_num_max_foragers_nectar_daily = 0.0015*pow(current_day,3)-1.2047*pow(current_day,2)+263.66*current_day-10895;
        m_num_max_foragers_pollen_daily = 0.0007*pow(current_day,3)-0.5163*pow(current_day,2)+113*current_day-4669.1;
    }

}

void HoneyBee_Colony::getDailyTotalResourceAmount(double& nectar, double& sugar, double& pollen){
    nectar=0;
    sugar=0;
    pollen=0;
    vector <int> polys_with_flower_resource = m_TheLandscape->SupplyPolyWithFlowers();
    for (int i= 0; i<polys_with_flower_resource.size(); i++){
        int temp_poly_id = polys_with_flower_resource.at(i);
        nectar += m_TheLandscape->SupplyTotalNectar(temp_poly_id);
        double temp_nectar_quality = (m_TheLandscape->SupplySugar(temp_poly_id));
        sugar += m_TheLandscape->SupplyPolygonArea(temp_poly_id) * temp_nectar_quality;
        pollen += m_TheLandscape->SupplyTotalPollen(temp_poly_id);
    }
}
