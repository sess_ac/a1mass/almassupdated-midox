/*
*******************************************************************************************************
Copyright (c) 2019, Christopher John Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file BeekeepingManagementPratice.cpp
    \brief <B>The main source code for hive management</B>
    Version of Feb. 2020 \n
    By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/

#include <fstream>
#include <vector>
#include "math.h"
#include <iostream>
#include <blitz/array.h>
#include <nlohmann/json.hpp>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../BatchALMaSS/ALMaSS_Random.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../Landscape/Calendar.h"
#include "../Landscape/Configurator.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "HoneyBee_Colony.h"
#include "HoneyBee.h"
#include "Hive.h"
#include "BeekeepingManagementPractice.h"
#include "BiologicalAgents.h"
using json = nlohmann::json;

CfgStr cfg_HoneyBeeFileInputForBMP("HONEY_BEE_FILE_INPUT_FOR_BMP", CFG_CUSTOM, "./HoneyBee/BMP.json");
CfgFloat cfg_HoneyBeeProbaVarroaKilledBMP("HONEY_BEE_PROBA_VARROA_KILLED_BMP", CFG_CUSTOM, 0.7);
CfgFloat cfg_HoneyBeeProbaBeesKilledBMP("HONEY_BEE_PROBA_BEES_KILLED_BMP", CFG_CUSTOM, 0.01);
CfgFloat cfg_HoneyBeeReduceEggsLayingRateBMP("HONEY_BEE_REDUCE_EGGS_LAYING_RATE_BMP", CFG_CUSTOM, 0.9);
CfgBool cfg_HoneyBeeExperiencedBeekeeperFlag("HONEY_BEE_EXPERIENCED_BEEKEEPER_FLAG", CFG_CUSTOM, true);
extern CfgInt cfg_HoneyBeeNumOfEggsPerTimestep;

BMP::BMP(Hive* thehive, HoneyBee_Colony* a_population_manager){
	m_hive=thehive;
    m_population_manager=a_population_manager;	
    readEvents();
	
}

ManagementType BMP::parseJobs(std::string task){
    if(task=="KillVarroa") {return mt_KillVarroa;} 
    if(task=="AddHoneyFrame") {return mt_AddHoneyFrame;}
    if(task=="ReplaceOneFrame") {return mt_ReplaceOneFrame;}
}

void BMP::applyManagement(){
    int current_day_in_year = m_population_manager->m_TheLandscape->SupplyDayInYear();
    int current_management_num = m_management_queue[current_day_in_year].size();
    if(current_management_num>0){
        for (int i=0;i<current_management_num;i++){
            switch(m_management_queue[current_day_in_year].at(i)){
                case mt_AddHoneyFrame:
                    addHoneyFrame(0, m_frameid_for_management_queue[current_day_in_year].at(i)); // from the beginning, we only have one box
                    break;
                case mt_ReplaceOneFrame:
                    replaceOneFrame(0, m_frameid_for_management_queue[current_day_in_year].at(i)); // from the beginning, we only have one box
                    break;
                case mt_KillVarroa:
                    killVarroa();
                    break;
                default:
                    m_population_manager->m_TheLandscape->Warn("BMP::applyManagement()", "unknown state - default: "+to_string((int)m_management_queue[current_day_in_year].at(i)));
                    exit(1);
            }
        }
        //clean the management queue
        m_frameid_for_management_queue[current_day_in_year].clear();
        m_management_queue[current_day_in_year].clear();
    }
}

void BMP::readEvents(){
    std::ifstream json_file(cfg_HoneyBeeFileInputForBMP.value());
	json bmp_json = json::parse(json_file);
    for (auto it : bmp_json["Jobs"]){
        if(it["type"].get<std::string>()=="Timed"){
            std::string task_name = it["task"].get<std::string>();
            int day = std::stoi(it["day"].get<std::string>());
            int month = std::stoi(it["month"].get<std::string>());
            int day_in_year = g_date->DayInYear(day, month);
            int frame = std::stoi(it["frame"].get<std::string>());
            //std::cout<<task_name<<day<<month<<frame<<endl;
            m_management_queue[day_in_year].push_back(parseJobs(task_name));
            m_frameid_for_management_queue[day_in_year].push_back(frame);
        }
    }
}

void BMP::killVarroa(){
    unsigned size2;
	unsigned size1 =  (unsigned) m_population_manager->SupplyListIndexSize();
    HoneyBee_Base* bee_pointer;
    for ( unsigned listindex = 0; listindex < size1; listindex++ ) {
 		  size2 = (unsigned)m_population_manager->GetLiveArraySize( listindex );
		  for (unsigned j = 0; j < size2; j++) {
            bee_pointer = dynamic_cast<HoneyBee_Base*>(m_population_manager->SupplyAnimalPtr(listindex, j));
            //treat the varroa
            if(listindex==2 || listindex==3 || listindex==6 || listindex==7){
                if(bee_pointer->m_MyBiologicalAgent->getVarroa()>0){
                    if(g_rand_uni()<cfg_HoneyBeeProbaVarroaKilledBMP.value()){
                        bee_pointer->m_MyBiologicalAgent->setVarroa(-1, -1);
                    }
                }
            }
            //to check the bee is also killed by the 
            if(g_rand_uni()<cfg_HoneyBeeProbaBeesKilledBMP.value()){
                bee_pointer->setHoneyBeeState(toHoneyBeeS_Die);
            }
            //change the rate of the laying eggs
            cfg_HoneyBeeNumOfEggsPerTimestep.set(cfg_HoneyBeeNumOfEggsPerTimestep.value()*cfg_HoneyBeeReduceEggsLayingRateBMP.value());
        }
    }
}

void BMP::addHoneyFrame(int box_num, int a_frame_id){
    m_hive->removeOneFrame(box_num, a_frame_id);
    m_hive->fillHoneyFrameFull(box_num, a_frame_id);
}

void BMP::replaceOneFrame(int box_num, int a_frame_id){
    m_hive->removeOneFrame(box_num, a_frame_id);
}