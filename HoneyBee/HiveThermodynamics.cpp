/*
*******************************************************************************************************
Copyright (c) 2019, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HiveThermodynamics.cpp
    \brief <B>The main source code for hive thermodynamics</B>
    Version of Feb 2020 \n
    By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "blitz/array.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "HiveThermodynamics.h"
#include "HoneyBee.h"

CfgStr cfg_HoneyBeeHiveThermalKernelFile("HONEY_BEE_HIVE_THERMAL_KERNEL_FILE", CFG_CUSTOM, "HoneyBee/temperature_kernel.txt");
CfgFloat cfg_HoneyBeeCoefficientCovolution("HONEY_BEE_COEFFICIENT_COVOLUTION", CFG_CUSTOM, 1);
CfgFloat cfg_HoneyBeeWarmingTemperatureByHive("HONEY_BEE_WARMING_TEMPERATURE_BY_HIVE", CFG_CUSTOM, 15);
extern CfgFloat cfg_HoneyBeeOptimalDevelopmentTemperature;

using namespace blitz;

HiveThermodynamics::HiveThermodynamics(int x_num, int y_num, int z_num){
    m_x_size=x_num;
    m_y_size=y_num;
    m_z_size=z_num;
    m_temperature.resize(x_num, y_num, z_num);
    m_temperature = cfg_HoneyBeeOptimalDevelopmentTemperature.value();
    m_temperature_copy.resize(x_num, y_num, z_num);
    m_temperature_copy=cfg_HoneyBeeOptimalDevelopmentTemperature.value();
    m_temp_result.resize(x_num, y_num, z_num);
    //load the kernel from file
    readKernelFile(cfg_HoneyBeeHiveThermalKernelFile.value());
    m_normalization=m_x_kernel*m_y_kernel*m_z_kernel-1;
    m_y_bee_shift.resize((m_y_size-2)/2);
    m_y_bee_shift(0)=1;
    int temp_counter=1;
    for(int i=1;i<(m_y_size-2)/2-1;i+=2){
        m_y_bee_shift(i)=2*temp_counter+1;
        m_y_bee_shift(i+1)=2*temp_counter+1;
        temp_counter++;
    }
    m_y_bee_shift((m_y_size-2)/2-1)=2*temp_counter+1;
    m_y_cell_shift.resize((m_y_size-2)/2);
    temp_counter=1;
    for(int i=0;i<(m_y_size-2)/2;i+=2){
        m_y_cell_shift(i)=2*temp_counter;
        m_y_cell_shift(i+1)=2*temp_counter;
        temp_counter++;
    }
    //std::cout<<m_y_cell_shift;
}

void HiveThermodynamics::readKernelFile(string inputfile){
    ifstream ist(inputfile, ios::in);
    //the first line is the size of the kernel, three-dimension
    ist >> m_x_kernel >> m_y_kernel >>m_z_kernel;
    //resize the cube for the kernel
    m_kernel.resize(m_x_kernel,m_y_kernel,m_z_kernel);
    //read all the elements of the kernel
    for(int i=0; i<m_x_kernel;i++){
        for(int j=0; j<m_y_kernel;j++){
            for(int k=0; k<m_z_kernel;k++){
                ist>>m_kernel(i,j,k);
            }            
        }        
    }
}

void HiveThermodynamics::initialiseThermodynamics(double hive_temperature, double env_temperature){
    m_temperature=hive_temperature;
    setEnviromentalTemperature(env_temperature);
}

void HiveThermodynamics::updateGroupBeesTemp(int x, int y, int z, double a_tempe){
    m_temperature(x+1, y+m_y_bee_shift(y), z+1)=a_tempe;
}

void HiveThermodynamics::updateGroupBeesTemp(blitz::Array<double, 3>* a_tempe_array){
    //std::cout<<m_y_bee_shift<<endl;

    for (int y=0; y<m_y_size/2-1;y++){
        m_temperature(Range(1, m_x_size-2), y+m_y_bee_shift(y), Range(1, m_z_size-2))=where((*a_tempe_array)(blitz::Range::all(), y, blitz::Range::all())>0,(*a_tempe_array)(blitz::Range::all(), y, blitz::Range::all()),m_temperature(Range(1, m_x_size-2), y+m_y_bee_shift(y), Range(1, m_z_size-2)));
    }
    //std::cout<<m_temperature<<endl;
}

double HiveThermodynamics::supplyGroupBeesTemp(int x, int y, int z){
    return m_temperature_copy(x+1, y+m_y_bee_shift(y), z+1);
}

void HiveThermodynamics::updateTemperature(){
    ;
}

void HiveThermodynamics::resetBeesTemperature(){
    //i is used to index the bees position, so we set one frame each time
    //in case, there is no bees for some postions, we use the average temperature between the two sides to set
    //we first set the two frame side close to the box
    m_temperature(Range(1, m_x_size-2), 1, Range(1, m_z_size-2))=(m_temperature(Range(1, m_x_size-2), 0, Range(1, m_z_size-2))+m_temperature(Range(1, m_x_size-2), 2, Range(1, m_z_size-2)))/2;
    m_temperature(Range(1, m_x_size-2), m_y_size-2, Range(1, m_z_size-2))=(m_temperature(Range(1, m_x_size-2), m_y_size-3, Range(1, m_z_size-2))+m_temperature(Range(1, m_x_size-2), m_y_size-1, Range(1, m_z_size-2)))/2;
    for (int i=4; i<m_y_size-2; i+=4){
        //we do not change the elements for the environmental temperature
        m_temperature(Range(1, m_x_size-2),i, Range(1, m_z_size-2)) = (m_temperature(Range(1, m_x_size-2),i-1, Range(1, m_z_size-2))+m_temperature(Range(1, m_x_size-2),i+2, Range(1, m_z_size-2)))/2;
        m_temperature(Range(1, m_x_size-2),i+1, Range(1, m_z_size-2)) = (m_temperature(Range(1, m_x_size-2),i-1, Range(1, m_z_size-2))+m_temperature(Range(1, m_x_size-2),i+2, Range(1, m_z_size-2)))/2;
    }
    //std::cout << m_temperature; 
}

void HiveThermodynamics::setEnviromentalTemperature(double tempe_value){
    double current_tempe = tempe_value + cfg_HoneyBeeWarmingTemperatureByHive.value();
    m_temperature(m_x_size-1, Range::all(), Range::all()) = current_tempe;
    m_temperature(0, Range::all(), Range::all()) = current_tempe;
    m_temperature(Range::all(), m_y_size-1, Range::all()) = current_tempe;
    m_temperature(Range::all(), 0, Range::all()) = current_tempe;
    m_temperature(Range::all(), Range::all(), m_z_size-1) = current_tempe;
    m_temperature(Range::all(), Range::all(), 0) = current_tempe;
}

void HiveThermodynamics::applyConvolutionalOperations(){
    Range I(1,m_x_size-2), J(1,m_y_size-2), K(1,m_z_size-2);
    m_temperature_copy = m_temperature.copy();
    m_temp_result=0;
    /**
    for(int ii=1;ii<m_x_size-1;ii++){
        for(int jj=1;jj<m_y_size-1;jj++){
            for(int kk=1;kk<m_z_size-1;kk++){
                double temp=0;
                for(int i=0; i<m_x_kernel; i++){
                    for(int j=0; j<m_y_kernel; j++){
                        for(int k=0; k<m_z_kernel; k++){
                            temp +=m_temperature_copy(ii-1+i,jj-1+j,kk-1+k)*m_kernel(i,j,k);
                             //m_temperature(I,J,K)=-10;
                        }
                    }
                }
                m_temp_result(ii,jj,kk)=temp;
            }
        }
    }*/

    for(int i=0; i<m_x_kernel; i++){
        for(int j=0; j<m_y_kernel; j++){
            for(int k=0; k<m_z_kernel; k++){
                if(m_kernel(i,j,k)!=0){
                    m_temp_result(I,J,K) = m_temp_result(I,J,K)+m_temperature_copy(I-1+i,J-1+j,K-1+k)*m_kernel(i,j,k);
                }
            }
        }
    }

    //std::cout<<m_temperature(1,1,1);
    //std::cout<<m_temp_result;
    m_temperature -= cfg_HoneyBeeCoefficientCovolution.value()*m_temp_result/m_normalization;
    m_temperature_copy = m_temperature.copy();
    //std::cout << m_temperature;
    //std::cout<<m_temperature;
    
    /*
    for (int i = 1; i<x_size; i++){
        for (int j=1; j<y_size; j++){
            for (int k=1; k<z_size; k++){
               temperature(i,j,k) = (temperature(i-1,j,k)+temperature(i+1,j,k))/2;
            }
        }
    }
    */
}

void HiveThermodynamics::copyCellTemperature(blitz::Array<double,3> *cells_temperature){
    for (int i=0; i<(m_y_size-2)/2; i++){
        (*cells_temperature)(Range::all(),i,Range::all())=(m_temperature(Range(1,m_x_size-2), m_y_cell_shift(i)+i, Range(1,m_z_size-2)));
    }
}