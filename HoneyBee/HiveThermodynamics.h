/*
*******************************************************************************************************
Copyright (c) 2019, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS doubleERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HiveThermodynamics.h
	\brief <B>The main header file for thermodynamics</B>
	Version of  Feb 2020 \n
	By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#ifndef HiveThermodynamicsH
#define HiveThermodynamicsH
//---------------------------------------------------------------------------

class HoneyBee_Base;

/** \brief A class to calculate the hive temperature distribution. */

class HiveThermodynamics
{
protected:
	/** \brief This is to record the x_size of the temperature tube. */
	int m_x_size;
	/** \brief This is to record the y_size of the temperature tube. */
	int m_y_size;
	/** \brief This is to record the z_size of the temperature tube. */
	int m_z_size;
	/** \brief This is to record the x_size of the kernel tube. */
	int m_x_kernel;
	/** \brief This is to record the y_size of the kernel tube. */
	int m_y_kernel;
	/** \brief This is to record the z_size of the kernel tube. */
	int m_z_kernel;
	/** \brief Array to store temperature for each cell and the bees belong to each cell. */
   	blitz::Array<double,3> m_temperature;
	/** \brief Array to store a copy of temperature for each cell and the bees belong to each cell in order to do the convolutional operations. */
   	blitz::Array<double,3> m_temperature_copy;
	/** \brief Array to for the convolutional operations on the temperature array. */
	blitz::Array<double,3> m_kernel;
	/** \brief Variable used to normalize the convolutional operation. */
	double m_normalization;
	/** \brief Array to store the itermedia results of convolutional operation. */
	blitz::Array<double, 3> m_temp_result;
	/** \brief Array to store shift for converting the bee's position in hive to the one in the temperature tube. */
	blitz::Array<int, 1> m_y_bee_shift;
	/** \brief Array to store shift for converting the cell's position in hive to the one in the temperature tube. */
	blitz::Array<int, 1> m_y_cell_shift;
	friend class HoneyBee_Base;
	
public:
	/** \brief  Standard constructor, x_num: 2+num. of cells in a row of a frame, y_num: 2+4*num. of frames in a hive, z_num 2+(num. of cells in a colum of a frame)*num. of box. */
	HiveThermodynamics(int x_num, int y_num, int z_num);
	/** \brief Reset the whole tube to the given temperature. */
	void resetWholeTemperature(double a_tempe){m_temperature=a_tempe;m_temperature_copy=a_tempe;}
	/** \brief Method for reading activities priorities file. */
  	void readKernelFile(string inputfile);
	/** \brief Method to initialise the temperature tube. */
	void initialiseThermodynamics(double hive_temperature, double env_temperature);
	/** \brief  Destructor */
	~HiveThermodynamics(void) { std::cout<<"~HiveThermodynamics"; }
	/** \brief Method to update the group bees temperature in the same location, this should be called by each bee in the end of each time step if the bee is in hive. */
	void updateGroupBeesTemp(int x, int y, int z, double a_tempe);
	void updateGroupBeesTemp(blitz::Array<double, 3>* a_tempe_array);
	/** \brief Return the average body temperature of the bees belonging to the same cell position. */
	double supplyGroupBeesTemp(int x, int y, int z);
	/** \brief Method to update the temperature tube using convolutional operations in each time step. */
	void updateTemperature();
	/** \brief Method to set the temperatures for all the bees to be the average temperature of the cell on two closet frame sides. */
	void resetBeesTemperature();
	/** \brief Method to set the environmental temperature, this should be called from the beginning of the day, if daily temperature is used.*/
	void setEnviromentalTemperature(double tempe_value);
	/** \brief Method for convolutional operations for heating exchange.*/
	void applyConvolutionalOperations();
	/** \brief Method for copy cell temperatures.*/
	void copyCellTemperature(blitz::Array<double,3> *cells_temperature);
	int suppleKernelX(){return m_x_kernel;}
	blitz::Array<double, 3>* supplyWholeCube(){return &m_temperature; }
};
#endif
