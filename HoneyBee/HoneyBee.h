/*
*******************************************************************************************************
Copyright (c) 2017, Chris J. Topping, David Warren Wallis and Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee.h
    \brief <B>The main header file for all HoneyBee lifestage classes</B>
    Version of  Feb 2020 \n
    By Chris J. Topping, David Warren Wallis and Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------
#ifndef HONEYBEEH
#define HONEYBEEH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//class HoneyBee_Base;
//class struct_HoneyBee;
//class BeeVirusInfectionLevels;
//#include "BeeVirusInfectionLevels.h"
//#include "HoneyBee_Colony.h"
//#include "Hive.h"

//#include "../BatchALMaSS/PopulationManager.h"
//class Hive;

class HoneyBee_Colony;
class BiologicalAgents;
class Hive;
class struct_HoneyBee;
class struct_Known_Resource;
enum class CellType;
enum class TaskType;
class HoneyBee_Worker;
/**
\brief
Used for indicating the type of gland product for a bee producing in one day, this is used to stop a bee from doing a secend job with different gland product.
*/
enum TTypesOfGlandProduct
{
    ttogp_None = 0,
    ttogp_Wax,
    ttogp_RoyalJelly
};

/**
\brief HoneyBee like other ALMaSS animals work using a state/transition concept. These are the HoneyBee behavioural states.
*/
typedef enum
{
   toHoneyBeeS_InitialState = 0,
   toHoneyBeeS_Develop,
   toHoneyBeeS_Progress,
   toHoneyBeeS_Decide,
   toHoneyBeeS_Move,
   toHoneyBeeS_Die,//5
   toHoneyBeeS_CapBrood, 
   toHoneyBeeS_AttendBrood,
   toHoneyBeeS_WarmUp,
   toHoneyBeeS_StoreNectar,
   toHoneyBeeS_StoreNectarSelf,//10
   toHoneyBeeS_RipenNectar,
   toHoneyBeeS_CapHoney,
   toHoneyBeeS_PackPollen,
   toHoneyBeeS_BuildComb,
   toHoneyBeeS_Ventilate,//15
   toHoneyBeeS_Guard,//16
   toHoneyBeeS_RemoveDebris,
   toHoneyBeeS_ForagePollen,
   toHoneyBeeS_ForageNectar,//end of activities in the activities_priorities.txt file
   toHoneyBeeS_CleanCell,
   toHoneyBeeS_ScoutPollen,
   toHoneyBeeS_StorePollen,
   toHoneyBeeS_ScoutNectar,
   toHoneyBeeS_NextStage,
   toHoneyBeeS_WayBack, // used for a bee coming back from foraging/scouting
   toHoneyBeeS_Dummy,   
   toHoneyBeeS_foobar
} TTypeOfHoneyBeeState;


/**
   \brief HoneyBee caste life stage enum.
*/
typedef enum
{
   WorkerEgg_e = 0,
   WorkerLarva_e,
   WorkerPupa_e,
   WorkerAdult_e,
   DroneEgg_e,
   DroneLarva_e,
   DronePupa_e,
   DroneAdult_e,
   Queen_e
} CasteOfHoneyBee;

/** \brief This the base class for all Honeybee life/cast combination classes. */
class HoneyBee_Base : public TAnimal
{
   /**
      Inherits m_Location_x, m_Location_y, m_OurLandscape from TAnimal
      In addition, there is m_Location_z. When the bee is in hive, these three are used for the bee's position in the hive. When the bee is out in the landscape, only the first two used to indicate the bee's position on the landscape.

      This class should never actually be used to create an object, it is a base class from which all other bee types are defined.
   */

protected:
   /** \brief Accumulated sugar requirement for one day. */
   double m_sugar_required_day;
   /** \brief Accumulated pollen requirement for one day. */
   double m_pollen_required_day;
   /** \brief Preffered pollen type.*/
   int m_preffered_pollen_type;
   /** \brief Variable to record required royal jelly per day */
   double m_jelly_required_day;
   /** \brief Variable to record sugar within the bee */
   double m_sugar;
   /** \brief Variable to record pollen within the bee */
   double m_pollen;
   /** \brief Variable to record current royal jelly within the bee */
   double m_jelly;
   /** \brief This is the state of the gland production */
   TTypesOfGlandProduct m_gland_state;
   /** \brief This is the variable to track which box the bee is in. It starts from 0. */
   int m_box_num;
   /** \brief This is the third coordinate when the bee is in the hive. */
   int m_Location_z;  
   /** \brief Holds the age of the bee in days, this is the real time */
   int m_Age;
   /** \brief Holds the ID of a bee*/
   unsigned long id;
   /** \brief Holds the current body temperature */
   double m_current_body_temperature;
   /** \brief Flag to show whether the current activity is done */
   bool m_current_activity_done;
   /** \brief Variable to show which type of gland product that the bee is producing */
   TTypesOfGlandProduct m_current_gland_product;
   /** \brief Hold the current activity. */
   TaskType m_current_activity;
   /** \brief This is a variable to track the required remaining time to fininsh the current activity, -1 means that one activity is just finished, a new value should be set when a new activity is choosen for the bee */
   double  m_required_remaining_time_current_activity;
   /** \brief This is a variable to track the remaining available time for each timestep, this will be set to the timestep in the BeginStep() funcation, and deduced the amount used for one activity */
   double  m_time_left_timestep;
   /** \brief This is a time saving pointer to the correct population manager object, i.e. HoneyBee_Colony */
   HoneyBee_Colony*  m_OurPopulationManager;
   /** \brief Variable to record the starting maximum age for death or next stage.*/
   double m_next_stage_time;
   /** \brief Variable to accumulated development time, this is updated by the updateDevelopmentTime function: When a real time passes, this can be diffent from the real time based on the status of the bee, which means it can be longer or shorter than the real passed time. A bee can develop to the next stage faster. It can also be alive longer in order to over the winter. */
   double m_development_time;
   /** \brief Variable to record the current state */
   TTypeOfHoneyBeeState m_CurrentHBState;
   /** \brief To show the life stage of the bee. -1 means a base class */
   static const int myID=-1;
   /** \brief Binary indicator of the bee is in hive or not. */
   bool inHive;
   /** \brief Hold the mortality rate, which is updated by updateMortalityRate. */
   double m_mortality_rate;
   /** \brief Hold the food stressor, which is updated by updateFoodStressor. */
   double m_food_stressor;
   /** \brief Hold the temperature stressor, which is updated by updateTemperatureStressor. */
   double m_temp_stressor;
   /** \brief Hold the pesticide stressor, which is updated by updatePesticideStressor. */
   double m_pesticide_stressor;
   /** \brief Hold the disease stressor, which is updated by updateDiseaseStressor. */
   double m_disease_stressor;
   /** \brief Hold the total pesticide body burden from different paths. */
   blitz::Array<double, 1> m_pest_burden_total;
   /** \brief Hold the pesticide body burden from food for the honeybee, maximum ten types are supported now. */
   blitz::Array<double, 1> m_pest_burden_intake;
   /** \brief Hold the pesticide body burden from overspray for the honeybee, maximum ten types are supported now. */
   blitz::Array<double, 1>  m_pest_burden_overspray;
   /** \brief Hold the pesticide body burden from seed coating for the honeybee. */
   blitz::Array<double, 1>  m_pest_burden_coating;
   /** \brief Bee's priorities for different task. */
   blitz::Array<int, 1> m_bee_task_activities;
   /** \brief Daily decay fraction in bee. */
   double m_seedcoating_daily_decay_frac_bee;
   double m_pesticide_daily_decay_frac_bee;
   /** \brief This is used to track the pointer of the adult bee on the postion of egg, larva, pupa. */
   HoneyBee_Worker* m_current_worker_pointer;
   /** \brief This is the immune strength value ranging from 0 to 1. */
   double m_immune_value;
   /** \brief This is the vitality value ranging from 0 to 1. */
   double m_vitality_value;

public:
   /** \brief Holds the object for virus infection */
   BiologicalAgents* m_MyBiologicalAgent;     
   // Methods
   /** \brief HoneyBee_Base constructor */
   HoneyBee_Base(const int box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief HoneyBee_Base destructor */
   virtual ~HoneyBee_Base();
   /** \brief ReInit for object pool */
   virtual void ReInit(const int box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief This is used to handle the overspray event.*/
	bool OnFarmEvent( FarmToDo event );
   /** \brief Function to move a bee to the next life state. */
   virtual void nextStage() {}

   /** \brief Updating remaining time for each activity. It will return the time spent, either the lenghth of the time step or less than it. Through this function a bee is able to finish part of an activity or several activities in a single time step*/
   double updateRemainingTimeActivity(TaskType activity_type);
   /** \brief Updating development time. This function should be called in the beginning of each activity function */
   virtual void updateDevelopmentTime();
   /** \brief Updating food stressor. */
   void updateFoodStressor();
   /** \brief Updating temperature stressor. */
   void updateTemperatureStressor();
   /** \brief Updating pesticide stressor. */
   void updatePestStressor();
   /** \brief Updating disease stressor. */
   void updateDiseaseStressor();
   /** \brief Updating mortality rate. */
   virtual void updateMortalityRate();
   /** \brief This is the function for a bee to develop in the end of Step() function, it will add ages, update stress and mortality rate. No mattter how old is the bee, it needs to develop until it dies. */
   virtual void developInStep();
   /** \brief Behavioural state develop */
   TTypeOfHoneyBeeState st_Progress(void);
   /** \brief Behavioural state progressing */
   void st_Dying(void);
   /** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void BeginStep(void) {} // NB this is not used in the HoneyBee_Base code
   /** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
   virtual void Step();
   /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void EndStep(void) {} // NB this is not used in the HoneyBee_Base code
   /** \brief Returns the age in days */
   unsigned getAge() { return m_Age; }
   /** \brief Add one day to a bee's age. */
   void addAge() {m_Age+=1;}
   /** \brief Set a bee's age. */
   void setAge(unsigned int age) {m_Age=age;}
   /** \brief For filling in a honeybee_base object from a struct_HoneyBee */
   void FillHBdata(struct_HoneyBee* a_data);
    /** \brief For copying data from a honeybee_base object*/
   void CopyHBdata(HoneyBee_Base* hb_source);
   /** \brief Set coordinate z */
   void SetZ(int a_z) { m_Location_z = a_z;}
   /** \brief Set the box num for a bee. */
   void SetBoxNum(int a_box_num) {m_box_num = a_box_num;}
   /** \brief Supply box number */
   int supplyBoxNum() {return m_box_num;}
   /** \brief Supply coordinate z */
   int Supply_m_Location_z(){ return m_Location_z; }
   Hive * getHive();
   /** \brief Function for testing whether a bee should go to the next stage. */
   bool isReadyNextStage(){ return (m_development_time>=m_next_stage_time);}
   /** \brief Function for supplying the bee's current state. */
   TTypeOfHoneyBeeState supplyHoneyBeeState(){return m_CurrentHBState;}
   /** \brief Function for setting the bee's state. */
   void setHoneyBeeState(TTypeOfHoneyBeeState a_state){m_CurrentHBState=a_state;}
   /** \brief Function to supply the current mortality rate. */
   double supplyMortalityRate(){return m_mortality_rate;}
   /** \brief Function to show whether a bee is in hive or not. */
   bool isInHive(){return (inHive && m_Location_z>-1);}
   /** \brief Function to supply the food stressor. */
   double supplyFoodStressor(){return m_food_stressor;}
   /** \brief Function to supply the temperature stressor. */
   double supplyTempeStressor(){return m_temp_stressor;}
   /** \brief Function to supply the pesticide stressor. */
   double supplyPesticideStressor(){return m_pesticide_stressor;}
   /** \brief Function to supply the disease stressor. */
   double supplyDiseaseStressor(){return m_disease_stressor;}
   /** \brief Used to set cell status and required task when the young ones die in the cell.*/
   void setCellAfterDeadInCell();
   /** \brief Return whether the bee is still alive. */
   bool isAlive(){return m_CurrentStateNo!=-1;}
   /** \brief Apply pesticide concentration degaradation every day. */
   void updatePesticideConcentration();
   /** \brief Supply the summation of all the pesticide burden. */
   double supplySumPestBurden();
   /** \brief Set the adult bee pointer. */
   void setAdultBeePointer(HoneyBee_Worker* the_adult_bee){m_current_worker_pointer=the_adult_bee;}
   /** \brief Supply the adult bee pointer on the position. */
   HoneyBee_Worker* supplyAdultBeePointer(){return m_current_worker_pointer;}
   /** \brief Delete the adult bee pointer. */
   void delAdultBeePointer(){m_current_worker_pointer=NULL;}
   /** \brief Find new host(s) for Varroa. */
   void findNewVarroaHosts();
   /** \brief Return developped time. */
   double supplyDeveloppedTime(){return m_development_time;}
   /** \brief Return the age. */
   int supplyAge(){return m_Age;}
   /** \brief Return honey bee state. */
   int suppleHoneyBeeState(){return m_CurrentHBState;}
   /** \brief Update vitality. */
   void updateVitality();
   /** \brief Supply vitality value. */
   double supplyVitalityValue() {return m_vitality_value;}
   /** \brief Set vitality value. */
   void setVitalityValue(double a_vitality) {m_vitality_value=a_vitality;} 
   /** \brief Supply the HB state*/
   TTypeOfHoneyBeeState supplyHBState(void) {return m_CurrentHBState;}
};

class HoneyBee_WorkerLarva;
class HoneyBee_WorkerPupa;
class HoneyBee_Worker;


class HoneyBee_WorkerEgg : public HoneyBee_Base
{
   /**
      Extends the HoneyBee_Base for specialisms related to being a worker egg.
      \todo Should we add diseas and pesticide for eggs?
   */
   
public:
   /** \brief HoneyBee_Egg constructor */
   HoneyBee_WorkerEgg(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief Initialise a worker egg. */
   void initWorkerEgg(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief ReInit for object pool */
   void ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief Behavioural state for egg hatch */
   //virtual TTypeOfHoneyBeeState st_Hatch(void);
   static const int myID=WorkerEgg_e;
   static const CellType cellType;

   void nextStage();
   void Step();
   void developInStep();
   void updateDevelopmentTime();
   void updateMortalityRate();
   /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void EndStep(void);

};


class HoneyBee_WorkerLarva : public HoneyBee_WorkerEgg
{
   /**
      Extends the HoneyBee_Egg for specialisms related to being a worker larva
   */
public:
   /** \brief HoneyBee_WorkerLarva constructor */
   /** \brief HoneyBee_WorkerLarva constructor */
   HoneyBee_WorkerLarva(const int a_box_num, const int a_x, const int a_y,const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief ReInit for object pool */
   void ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief Init a larva */
   void initLarva();
   virtual void Step();
   static const int myID=WorkerLarva_e;
   static const CellType cellType;

   void nextStage();
   void developInStep();
   void updateDevelopmentTime();
   void updateMortalityRate();
   virtual void BeginStep(void);
   /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void EndStep(void);
   /** \brief Function to update the larva food requirement. */
   virtual void updateFoodReqLarva(void);
   /** \brief Function to feed a larva.*/
   void feedLarva(double a_sugar, double a_pollen, double a_jelly);
   /** \brief Function to return the required amount of sugar.*/
   double supplyReqSugar(){return m_sugar_required_day-m_sugar;}
   /** \brief Function to return the required amount of pollen.*/
   double supplyReqPollen(){return m_pollen_required_day-m_pollen;}
   /** \brief Function to return the required amount of royal jelly.*/
   double supplyReqJelly(){return m_jelly_required_day-m_jelly;}
};


class HoneyBee_WorkerPupa : public HoneyBee_WorkerLarva
{
   /**
    * Extends the HoneyBee_Larva for specialisms related to being a pupa.
    */
private:
   bool m_caped_flag;
public:
   /** \brief Function to init a pupa. */
   void initPupa(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief HoneyBee_WorkerPupa constructor */
   HoneyBee_WorkerPupa(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief ReInit for object pool */
   void ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief Set the caping flag to be true */
   void setCapBroodDone() {m_caped_flag=true;}
   void Step();
   static const int myID=WorkerPupa_e;
   static const CellType cellType;

   void nextStage();
   void developInStep();
   void updateDevelopmentTime();
   void updateMortalityRate();
   virtual void BeginStep(void);
   virtual void EndStep(void);
};


class HoneyBee_Worker : public HoneyBee_WorkerPupa
{
   /**
    * Extends the HoneyBee_WorkerPupa for specialisms related to being a worker.
    */
private:
   /** \brief Variable to indicate the HPG type in one day, -1 means not decided yet, 0 means wax, 1 means jelly.*/
   int m_HPG_type;
   /** \brief Variable to store the available amount of wax that can still be generated in one day.*/
   double m_available_wax_day;
   /** \brief Variable to store the available amount of jelly that can still be generated in one day.*/
   double m_available_jelly_day;
   /** \brief Variable to store the required number of timesteps for a bee to finish foraging/scouting. */
   double m_num_steps_back;
   /** \brief Place for foraging. */
   struct_Known_Resource* m_current_foraging_place;
   /** \brief Flag to show whether the bee should receive nectar in the next step. */
   bool m_receive_nectar_flag;
   /** \brief Bee's priorities for different task. */
   blitz::Array<int, 1> m_bee_task_activities;
   /** \brief Bee's gland product ability rate, the normal rate is 1, when its pesticide body burden is larger than a threshold, it will be reduced by 15%.*/
   double m_gland_rate;
   /** \brief Foraging status, -1, no foraging activity, 1, foraging nectar, 2, foraging pollen. */
   int m_foraging_status;
   /** \brief Foraging trip counter for one day. */
   int m_num_foraging_daily;
   /** \brief Foraging trip counter for lifetime. */
   int m_num_foraging_lifetime;
   /** \brief Foraging distance for one day. */
   double m_dis_foraging_daily;
   /** \brief Foraging distance for lifetime. */
   double m_dis_foraging_lifetime; 
public:
   /** \brief HoneyBee_Worker constructor */
   HoneyBee_Worker(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   virtual ~HoneyBee_Worker();
   /** \brief ReInit for object pool */
   void ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief Init a worker bee. */
   void initWorker(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   virtual void Step();
   virtual void BeginStep();
   virtual void EndStep();

   /** \brief This is a function for a worker to forage nectar or pollen. */
   void forageResource(bool (HoneyBee_Colony::*supplyOneAvailablePlaceFuncP)(int, HoneyBee_Worker* , struct_Known_Resource*,int), struct_Known_Resource (HoneyBee_Colony::*supplyKnownPlaceFuncP)(void), int (HoneyBee_Colony::*supplyKnownNumOfPlaceFuncP)(void), int resource_type);
   /** \brief This is a function used to update activity priorities, e.g., when there are enough guarders lower its priorities. */
   void updateSpecialTaskPriorities();
   /** \brief This is for a free bee to put foraged nectar into a cell in the hive.*/
   void putNectarInCell(struct_Known_Resource* nectar_place);
   /** \brief This is for a free bee to put foraged pollen into a cell in the hive.*/
   void putPollenInCell(struct_Known_Resource* pollen_place);
   /** \brief This is for a free bee to walk randomly in the hive to look for tasks.*/
   TTypeOfHoneyBeeState st_MoveRandom();
   /** \brief This is for a bee to decide what to do next.*/
   TTypeOfHoneyBeeState st_Decide();
   /** \brief This is for a bee to froage pollen in the landscape.*/
   TTypeOfHoneyBeeState st_ForagePollen();
   /** \brief This is for a bee to store pollen into a cell in the hive. */
   TTypeOfHoneyBeeState st_StorePollen();
   /** \brief This is for a bee to forage nectar in the landscape.*/
   TTypeOfHoneyBeeState st_ForageNectar();
   /** \brief This is for a bee to receive nectar from a forager and store it into a cell in the hive.*/
   TTypeOfHoneyBeeState st_StoreNectar();
    /** \brief This is for a bee to store it into a cell in the hive by itself.*/
   TTypeOfHoneyBeeState st_StoreNectarSelf();
   /** \brief Guarding function for worker bees, this activity will use the whole time for each time step*/
   TTypeOfHoneyBeeState st_Guard();
   /** \brief This is for a worker bee to cap  brood.*/
   TTypeOfHoneyBeeState st_CapBrood();
   /** \brief This is for a worker bee to cap honey.*/
   TTypeOfHoneyBeeState st_CapHoney();
   /** \brief This is for a worker bee to attend brood.*/
   TTypeOfHoneyBeeState st_AttendBrood();
   /** \brief This is for a worker bee to ripen a nectar cell.*/
   TTypeOfHoneyBeeState st_RipenNectar();
   /** \brief This is for a worker bee to cap a honey cell.*/
   TTypeOfHoneyBeeState st_CapHoneyCell();
   /** \brief This is for a worker bee to pack a pollen cell.*/
   TTypeOfHoneyBeeState st_PackPollenCell();
   /** \brief This is for a worker bee to build comb.*/
   TTypeOfHoneyBeeState st_BuildComb();
   /** \brief This is for a worker bee to ventilate the hive.*/
   TTypeOfHoneyBeeState st_VentilateHive();
   /** \brief This is for a worker bee to warm up.*/
   TTypeOfHoneyBeeState st_WarmUp();
   /** \brief This is for a worker bee to remove debris from the hive.*/
   TTypeOfHoneyBeeState st_RemoveDebris();
   /** \brief This is for a bee dying.*/
   TTypeOfHoneyBeeState st_Dying(){
      m_CurrentStateNo=-1;
      return toHoneyBeeS_Die;
   }
   /** \brief This is for a bee foraging, for now 1 means nectar and 2 means pollen. */
   TTypeOfHoneyBeeState st_WayBack();
   /** \brief This is used to clean the dead body in a cell with dead egg, larva or pupa. */
   TTypeOfHoneyBeeState st_CleanCell();

   void developInStep();
   void updateDevelopmentTime();
   void updateMortalityRate();
   /** \brief Updating temperature stressor. */
   void updateTemperatureStressor();

   /** \brief Update gland production ability rate according to pesticide body burden. */
   void updateGlandProductionAbility();
   

   // Metabolic functions
   void eatSugar();
   void eatPollen();
   /** \brief This is used to set the seed coating burden array. */
   void addBeeSeedCoating(int index, double amount){m_pest_burden_coating(index)+=amount;}
   /** \brief This is used to set the pesticide application intake burden array. */
   void addBeePesticideApplication(int index, double amount){m_pest_burden_intake(index)+=amount;}
   double pollenDemand();
   double sugarDemand();
   /** \brief This is used when a bee has to put debris in the hive. */
   void putDebrisInHive();
   /** \brief Return the body temperature. */
   double supplyBodyTemperature();
   /** \brief Make the bee die out of hive. */
   void makeMeDieOutHive(){m_CurrentHBState=toHoneyBeeS_Die; m_CurrentStateNo=-1;m_StepDone=true;}
   /** \brief Return how many steps back to hive. */
   int supplyStepsBac(){return m_num_steps_back;}
   static const int myID=WorkerAdult_e;
   static const CellType cellType;

   /** \brief This function is used to set the worker's state. */
   void setWorkerState(TTypeOfHoneyBeeState a_worker_state) {m_CurrentHBState = a_worker_state;}
};



///////////////////////////// DRONE ////////////////////////////

class HoneyBee_DroneLarva;
class HoneyBee_DronePupa;
class HoneyBee_Drone;

class HoneyBee_DroneEgg : public HoneyBee_WorkerEgg
{
   /**
      Extends the HoneyBee_Base for specialisms related to being a drone egg
   */
public:
   /** \brief Drone Egg constructor */
   HoneyBee_DroneEgg(const int a_box_num, const int a_x , const int a_y, const int a_z,  Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief ReInit for object pool */
   //void ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief Behavioural state for egg hatch */
   //virtual TTypeOfHoneyBeeState st_Hatch(void);
   static const int myID=DroneEgg_e;
   static const CellType cellType;
   //virtual void Step(); 
   virtual void nextStage();
};

class HoneyBee_DroneLarva : public HoneyBee_WorkerLarva
{
   /**
      Extends the HoneyBee_DroneEgg for specialisms related to being a drone larva
   */
public:
   /** \brief Drone Larva constructor */
   HoneyBee_DroneLarva(const int a_box_num, const int a_x, const int a_y,const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief ReInit for Drone Larva */
   void ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief Behavioural state for pupation */
   //virtual TTypeOfHoneyBeeState st_Pupate(void);
   /** \brief Behavioural state for larval development - this overrides the egg st_Develop */
   //virtual TTypeOfHoneyBeeState st_Develop(void);
   //virtual void Step();
   /** \brief Function to update the larva food requirement. */
   virtual void updateFoodReqLarva(void);
   static const int myID=DroneLarva_e;
   static const CellType cellType;

   virtual void nextStage();
};


class HoneyBee_DronePupa : public HoneyBee_WorkerPupa
{
   /**
    * Extends the HoneyBee_DroneLarva for specialisms related to being a Drone pupa.
    */
public:
   /** \brief Drone Pupa constructor */
   HoneyBee_DronePupa(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief Init a pupa.*/
   void ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   
   static const int myID=DronePupa_e;
   static const CellType cellType;
   virtual void nextStage();
};


class HoneyBee_Drone : public HoneyBee_DronePupa
{
   /**
    * Extends the HoneyBee_DronePupa for specialisms related to being a drone.
    */
   public:
   static const int myID = DroneAdult_e;
   static const CellType cellType;
   HoneyBee_Drone(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   /** \brief ReInit for Drone */
   void ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
   virtual void BeginStep(void){};
   virtual void EndStep(void){};
   virtual void Step();
};

/////////////////////////// Queen ///////////////

class HoneyBee_Queen : public HoneyBee_WorkerPupa
{
   /**
    * Extends the HoneyBee_WorkerPupa for specialisms related to being a queen.
    */
   private:
      double m_laying_egg_rate;

   public:
      static const int myID = Queen_e;
      static const CellType cellType;
      /** \brief HoneyBee_Queen constructor */
      HoneyBee_Queen(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
      /** \brief ReInit for object pool */
      void ReInit(const int a_box_num, const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
      /** \brief The function for the queen to lay eggs */
      void LayEggs();
      /** \brief The function for the queen to find available frame side for laying eggs */
      //bool FindAvailableFrameSide(int* current_z, int*availab_cell_num);
      virtual void Step();
      virtual void BeginStep(void){};
      /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. -Empty */
      virtual void EndStep(void) {} // NB this is not used in the HoneyBee_Queen code
      /** \brief This function is used to update the sublethal effect of pesticide. */
      void updateSubletalPest();
};
#endif
