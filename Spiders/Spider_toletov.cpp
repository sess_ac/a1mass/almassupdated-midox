#include "../Spiders/Spider_toletov.h"

bool spider_tole_egss_position_valid(Landscape* m_OurLandscape, int x, int y)
{
    /* Not on water */
    if (m_OurLandscape->SupplyAttIsWater(x, y))
        return false;

    TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType(x, y);

    switch (ele) {
    // Edit this list to add more impossible element types
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_Scrub:
    case tole_Track:
        return false;
        break;
    default:
        return true;
    }

}

bool spider_tole_juvenile_maturation_valid(Landscape* m_OurLandscape, int x, int y)
{

    /* Not on water */
    if (m_OurLandscape->SupplyAttIsWater(x, y))
        return false;

    /*
    TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType(x, y);
    switch (ele) {
        // Edit this list to add more impossible element types
    case tole_xxxx:
        return false;
    }
    */

    return true;
}

bool spider_tole_lethal(Landscape* m_OurLandscape, int x, int y)
{
    /* Not on water */
    if (m_OurLandscape->SupplyAttIsWater(x, y))
        return true;

    return false;
}

int spider_tov_index(Landscape * m_OurLandscape, int x, int y)
{
    TTypesOfVegetation tov_type = tov_Undefined;
    tov_type = m_OurLandscape->SupplyVegType(x, y);
    switch (tov_type) {
    case tov_PermanentSetAside:
    case tov_Potatoes:
    case tov_PotatoesIndustry:
    case tov_SetAside:
    case tov_OSetAside:
    case tov_OPotatoes:
    case tov_Heath:
        return 2;
    default:
        return 3;

    }
}

int spider_tole_movemap_init(Landscape* m_OurLandscape, int x, int y)
{

    TTypesOfLandscapeElement tole = m_OurLandscape->SupplyElementType(x, y);
    int colour = 0;
    char error_num[20];

    // Erigone
    switch (tole)
    {
    case tole_IndividualTree:
    case tole_WindTurbine:
    case tole_WoodyEnergyCrop:
    case tole_WoodlandMargin:
    case tole_EnergyCrop:
    case tole_OEnergyCrop:
    case tole_Pylon:
    case tole_NaturalGrassWet:
    case tole_MetalledPath:
    case tole_Carpark:
    case tole_Churchyard:
    case tole_Saltmarsh:
    case tole_HeritageSite:
    case tole_Hedges:
    case tole_HedgeBank:
    case tole_BeetleBank:
    case tole_Marsh:
    case tole_PermanentSetaside:
    case tole_NaturalGrassDry:
    case tole_RiversidePlants:
    case tole_RiversideTrees:
    case tole_DeciduousForest:
    case tole_MixedForest:
    case tole_ConiferousForest:
    case tole_YoungForest:
    case tole_ForestAisle:
    case tole_ChristmasTrees:
    case tole_OChristmasTrees:
    case tole_OFarmForest:
    case tole_FarmForest:
    case tole_FarmYoungForest:
    case tole_OFarmYoungForest:
    case tole_StoneWall:
    case tole_Fence:
    case tole_Building:
    case tole_ActivePit:
    case tole_PitDisused:
    case tole_Scrub:
    case tole_Track:
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_BareRock:
    case tole_UrbanNoVeg:
    case tole_SandDune:
    case tole_Copse:
    case tole_MownGrass:
    case tole_UnknownGrass:
    case tole_Orchard:
    case tole_SolarPanel:
        colour = 2;
        break;
    case tole_BushFruit:
    case tole_OBushFruit:
    case tole_PlantNursery:
    case tole_Vildtager:
    case tole_RoadsideSlope:
    case tole_OPermPasture:
    case tole_PermPasture: //moved from case 2 to case 3
    case tole_PermPasturePigs:
    case tole_OPermPasturePigs:
    case tole_PermPastureLowYield:
    case tole_OPermPastureLowYield:
    case tole_PermPastureTussocky: //moved from case 2 to case 3
    case tole_RoadsideVerge:
    case tole_Railway:
    case tole_FieldBoundary:
    case tole_Garden:
    case tole_Coast:
    case tole_Heath:
    case tole_AmenityGrass:
    case tole_Parkland:
    case tole_BuiltUpWithParkland:
    case tole_UrbanPark:
    case tole_UrbanVeg:
    case tole_Wasteland:
    case tole_WaterBufferZone:
    case tole_DrainageDitch:
    case tole_RefuseSite:
        colour = 3;
        break;
    case tole_Pond:
    case tole_Canal:
    case tole_Freshwater:
    case tole_FishFarm:
    case tole_River:
    case tole_Saltwater:
    case tole_Stream:
        colour = 0;
        break;
    case tole_Field: // Special case
    case tole_UnsprayedFieldMargin:  // Special case
        colour = 1;
        break;
    default:
        sprintf(error_num, "%d", tole);
        g_msg->Warn(WARN_FILE,
            "MovementMap::Init(): Unknown landscape element type:",
            error_num);
        exit(1);
    }// End spider

    return colour;
}


