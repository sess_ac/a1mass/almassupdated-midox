// Version of 5th May 2020
/*
*******************************************************************************************************
Copyright (c) 2020, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
// Version of 7th January 2020
//---------------------------------------------------------------------------
#ifndef Spider_BaseH
#define Spider_BaseH
//---------------------------------------------------------------------------

//#define __SpidDebug

// Forward Declarations

class Landscape;
class Spider_Population_Manager;

//------------------------------------------------------------------------------

/**
\brief The enumeration lists all spider life stages used by all spider species
*/
enum TTypesOfSpiders : unsigned
{
    tspi_Egg = 0,
    tspi_Spiderling,
    tspi_Female
};

    /**
    \brief The enumeration lists all spider behavioural states used by all spider species
    */
    enum TTypesOfSpiderState : unsigned
{
    // General
    tosps_Initiation = 0,
    tosps_AssessHabitat,
    tosps_Develop,
    tosps_Move,
    tosps_Dying,
    // Egg
    tosps_Hatch,
    // Juvenile
    tosps_JBalloon,
    tosps_Mature,
    tosps_JWalk,
    // Female
    tosps_Reproduce,
    tosps_FBalloon,
    tosps_FWalk,
    // Destroy
    tosps_Destroy

};
//------------------------------------------------------------------------------

 /**
 \brief This is a data class that is used to create new instances of spider objects by CreateObjects in the relevant Spider_Population_Manager descendent class
 */
 class struct_Spider
 {
    public:
        int x;
        int y;
        Landscape* L;
        Spider_Population_Manager* SpPM;
        int noEggs;
    };
    //------------------------------------------------------------------------------

 class Spider_Population_Manager : public Population_Manager
 {
     // Attributes
 public:
     /** \brief A representation of the landscape in terms of quality - NB MUST be assigned by the descendent population manager class Init() method  */
     MovementMapUnsigned* m_MoveMap;
     /** \brief Pointer to the egg position map  */
     SimplePositionMap* m_EggPosMap;
     /** \brief Pointer to the egg position map  */
     SimplePositionMap* m_JuvPosMap;
     /** \brief Pointer to the egg position map  */
     SimplePositionMap* m_AdultPosMap;
     /** \brief An array to hold the dispersal distances possible as a probability 0-30 (x out of 10000) */
     static std::array<int, 31> m_DispDistances;
 protected:
     /** \brief The daily probability of a juvenile dying - to be assigned in a descendent classes population manager  */
     double m_DailyJuvMort;
     /** \brief The distribution of dispersal distances - to be assigned in a descendent classes population manager  */
     int m_DispersalDistances[10000];
     /** \brief The daily number of ballooning hours - to be assigned in a descendent classes population manager  */
     double BallooningHrs[52 * 7];
     /** \brief The number of days since last rain  */
     int m_DaysSinceRain;
     /** \brief Holds the current month  */
     int m_TodaysMonth;
     /** \brief Holds the current drought score for three vegetation classes  */
     int m_TodaysDroughtScore[3];
     /** \brief The current day's ballooning time  */
     double m_TodaysBallooningTime;
     /** \brief Used to spread spiderlings on day 1 of hatch - to be assigned in a descendent classes population manager  */
     int m_EggSacSpread;
     /** \brief Twice the m_EggSacSpread, used to save multiplications  */
     int m_DoubleEggSacSpread;
     /** \brief The mortality of ballooned distance - to be assigned in a descendent classes population manager  */
     double m_BallooningMortalityPerMeter;
     /** \brief  A flag for passing minimum temperature for producing eggs */
     bool m_EggProdThresholdPassed;
     /** \brief Todays wind direction  */
     int m_WindDirection;
     /** \brief A flag for whether the weather is correct for ballooning  */
     bool m_BallooningWeather;
     /** \brief A limiter for the day in year to start ballooning - to be assigned in a descendent classes population manager  */
     int m_BallooningStart;
     /** \brief A limiter for the day in year to stop ballooning - to be assigned in a descendent classes population manager  */
     int m_BallooningStop;
     /** \brief The lower threshold for egg development - to be assigned in a descendent classes population manager  */
     double m_EggDevelopmentThreshold;
     /** \brief Cumulative day degrees from 1st Jan  */
     double m_EggDegrees[365];
     /** \brief  Contribution to day degrees under good food */
     double m_JuvDegreesGood;
     /** \brief Contribution to day degrees under intermediate food  */
     double m_JuvDegreesIntermediate;
     /** \brief Contribution to day degrees under poor food  */
     double m_JuvDegreesPoor;
     /** \brief Links reproduction to food levels and day degrees - good food */
     double m_EggProdDDegsGood;
     /** \brief  Links reproduction to food levels and day degrees - intermediate food   */
     double m_EggProdDDegsInt;
     /** \brief  Links reproduction to food levels and day degrees - poor food   */
     double m_EggProdDDegsPoor;
     /** \brief A flag to denote the spider does not balloon  */
     bool m_WalkingOnly;
     /** \brief A flag to show whether minimum walking temperature is reached  */
     bool m_MinWalkTemp;
     /** \brief A minimum dispersal by walking temperature  */
     double m_MinWalkTempThreshold;
#ifdef __SpidDebug
     /** \brief For debug to record adult death types */
     int m_Deaths[10];
     /** \brief For debug to record juv death */
     int m_JDeaths;
     /** \brief For debug to record maturation */
     int m_Mature;
#endif
     // Methods
 public:
     /** \brief Return the EggSacSpread value  */
     inline int GetEggSacSpread() {
         return m_EggSacSpread;
     }
     /** \brief Return 2 x EggSacSpread value   */
     inline int GetDoubleEggSacSpread() {
         return m_DoubleEggSacSpread;
     }
     /** \brief Return ballooning mortality per meter  */
     double GetBallooningMortalityPerMeter() {
         return m_BallooningMortalityPerMeter;
     }
     /** \brief Return the number of days since rain  */
     int GetDaysSinceRain() {
         return m_DaysSinceRain;
     }
     /** \brief Return current month  */
     int GetTodaysMonth() { 
         return m_TodaysMonth; }
     /** \brief Returns the drought score for high, medium or low plant biomass  */
     int GetTodaysDroughtSc(int index) { 
         return m_TodaysDroughtScore[index]; }
     /** \brief Return current juvenile mortality rate  */
     double GetJuvMort() { 
         return m_DailyJuvMort; }
     /** \brief Returns the dispsersal distance associated with a particular frequency value  */
     int GetDispDist(int chance) {
         return m_DispersalDistances[chance];
     }
     /** \brief Returns ballooning hours for a given day  */
     double GetBTime(int day) {
         return BallooningHrs[day];
     }
     /** \brief Returns todays ballooning time  */
     double GetBTimeToday() { 
         return m_TodaysBallooningTime; }
     /** \brief Returns the wind direction  */
     int GetWindDirection() { 
         return m_WindDirection; }
     /** \brief Returns the flag for ballooning weather  */
     bool IsBallooningWeather() { 
         return m_BallooningWeather; };
     /** \brief Returns todays egg sac production day degrees for poor food  */
     double GetEggDegreesPoor() { 
         return m_EggProdDDegsPoor; }
     /** \brief Returns todays egg sac production day degrees for intermediate food  */
     double GetEggDegreesInt() {
         return m_EggProdDDegsInt; }
     /** \brief Returns todays egg sac production day degrees for good food  */
     double GetEggDegreesGood() {
         return m_EggProdDDegsGood; }
     /** \brief Returns todays juvenile development  day degrees for good food  */
     double GetJuvDegrees_good() { 
         return m_JuvDegreesGood; }
     /** \brief Returns todays juvenile development  day degrees for intermediate food  */
     double GetJuvDegrees_intermediate() { 
         return m_JuvDegreesIntermediate; }
     /** \brief Returns todays juvenile development  day degrees for poor food  */
     double GetJuvDegrees_poor() { 
         return m_JuvDegreesPoor; }
     /** \brief Returns todays egg development  day degrees  */
     double GetEggDevelDegrees(int day) { 
         return m_EggDegrees[day]; }
     /** \brief Returns humid or not  */
     bool CheckHumidity(int /* x */, int /* y */) { 
         return true; } // TODO
     /** \brief Returns walking only flag  */
     bool GetWalking() { 
         return m_WalkingOnly; }
     /** \brief Returns the minimum walking temperature  */
     bool GetMinWalkTemp() {
         return m_MinWalkTemp; }
     /** \brief The constructor  */
     Spider_Population_Manager(Landscape* p_L, int N);
     /** \brief An initiation method to initialise all the necessary values - must be overridden for species specific functionality  */
     virtual void Init(void);
     /** \brief DoFirst method, to be overridden in descendent classes  */
     virtual void DoFirst(void) { ; }
     /** \brief Destructor  */
     virtual ~Spider_Population_Manager();
     /** \brief A method to generate the AOR probe output  */
     virtual void TheAOROutputProbe() { ; }
     /** \brief A method to generate the Ripley probe output  */
     virtual void TheRipleysOutputProbe(FILE* a_prb) { ; }
     /** \brief Returns true if p_x,p_y is inside the square defined by p_sqz,p_sqy as TL corner and p_range size length  */
     bool InSquare(int p_x, int p_y, int p_sqx, int p_sqy, int p_range);
     /** \brief Allows for the possibility to create population level mortality  */
     void virtual Catastrophe() { ; }
     /** \brief Creates new spider objects - this must be overridden in descendent classes  */
     virtual void CreateObjects(int ob_type, TAnimal* pvo, struct_Spider* data, int number) { ; }
#ifdef __SpidDebug
     /** \brief For debug to record death types */
     void RecordDeath(int a_type) { m_Deaths[a_type]++; }
     /** \brief For debug to record death types */
     void RecordJDeath() { m_JDeaths++; }
     /** \brief For debug to record death types */
     void RecordMature() { m_Mature++; }
#endif
};
//---------------------------------------------------------------------------

class Spider_Base : public TAnimal
{
    // Attributes
public:
    /** \brief This is the number of local spiders needed before density dependent mortality will kill   */
    static int m_DenDependenceConst0;
    /** \brief The width of the landscape  */
    static int m_SimW;
    /** \brief The height of the landscape  */
    static int m_SimH;
    /** \brief Body-burden of pesticde  */
    double m_pesticide_accum;
    /** \brief A local pointer the population manager  */
protected:
    /** \brief Stores the spiders age in day degrees  */
    double m_AgeDegrees;
    /** \brief Stores the spiders age in days  */
    int m_Age;
    /** \brief Stores the current behavioural state  */
    TTypesOfSpiderState m_CurrentSpState;
    // Methods
public:
    Spider_Population_Manager* m_OurPopulationManager;
    /** \brief Constructor  */
    Spider_Base(int x, int y, Landscape* L, Spider_Population_Manager* SpMan);
    /** \brief Reinitialise object code  */
    void ReInit(int x, int y, Landscape* L, Spider_Population_Manager* SpMan);
    /** \brief Used it initialise objects (also used by ReInit)  */
    void Init(Spider_Population_Manager* p_spMan);
    /** \brief The EndStep code  */
    virtual void EndStep();
    /** \brief Returns the current spider behavioural state  */
    virtual int WhatState() { return int(m_CurrentSpState); }
    /** \brief Determines the impact of any farm management events at the spiders location - must be overridden */
    virtual bool OnFarmEvent(FarmToDo /* event */) { return true; }
    /** \brief Destroys the spider  */
    virtual void KillThis() { 
        m_CurrentSpState = TTypesOfSpiderState::tosps_Dying; 
    }
protected:
    /** \brief Checks if its possible to create an eggsac here  */
    virtual bool EggPosValid(unsigned a_x, unsigned a_y);
    /** \brief Contains a pointer to the relevant position map - allocation of this must be controlled by the descendent class init */
    SimplePositionMap* m_OurPosMap;
    /** \brief Checks for density-dependent mortality at this location  */
    bool HatchDensityMort(int a_x, int a_y, int a_range);
    /** \brief Returns the value in m_OurPosMap for this location  */
    virtual int CheckPosMap(unsigned x, unsigned y);
    /** \brief Returns whether there are any non-zero values within range of this coordinate (TL corner)  */
    virtual bool GetPosMapPositive(unsigned x, unsigned y, unsigned range);
    /** \brief Returns total number of non-zero locations within range of this coordinate (TL corner)  */
    virtual int GetPosMapDensity(unsigned x, unsigned y, unsigned range);
    /** \brief Clears a PosMap location  */
    virtual void ClearPosMap(unsigned x, unsigned y);
    /** \brief Sets a PosMap location to non-zero  */
    virtual void SetPosMap(unsigned x, unsigned y);
};

/** \brief The generic base class for spider eggsacs  */
class Spider_Egg : public Spider_Base
{
// Attributes
public:
    /** \brief This is a space for holding cfg species specific parameters - this is the mortality constant associated with Spider_Egg::Hatch */
    static int m_HatDensityDepMortConst;
    /** \brief This is a space for holding cfg species specific parameters - this is the daily egg mortality probability Spider_Egg::st_Develop */
    static int m_DailyEggMortConst;
    /** \brief This is a space for holding cfg species specific parameters - this is a developmental threshold for hatch in Spider_Egg::st_Develop */
    static double m_EggDevelConst;
protected:
    /** \brief The day in year the eggsac was produced  */
    int m_DateLaid;
    /** \brief The number of eggs contained in the eggsac  */
    int m_NoEggs;
// Methods
public:
    /** \brief The Spider_Egg constructor  */
    Spider_Egg(int x, int y, Landscape* L, Spider_Population_Manager* EPM, int Eggs);
    /** \brief Used to reinitialise and object  */
    virtual void ReInit(int x, int y, Landscape* L, Spider_Population_Manager* EPM, int Eggs);
    /** \brief The begin step code  */
    virtual void BeginStep();
    /** \brief The Step code  */
    virtual void Step();
    /** \brief Sets the number of eggs attribute  */
    void SetNoEggs(int Eggs) { m_NoEggs = Eggs; }
    /** \brief Returns the number of eggs in the eggsac  */
    int GetNoEggs() { return m_NoEggs; }
    /** \brief Determines the impact of any farm management events at the eggsac's location  */
    virtual bool OnFarmEvent(FarmToDo event) { return true; }
protected:
    /** \brief The behavioural state develop  */
    TTypesOfSpiderState st_Develop();
    /** \brief The behavioural state hatch  */
    virtual void st_Hatch() ;
    /** \brief The behavioural state die  */
    virtual void st_Die();
    /** \brief Determines the number and location of spiderlings to survive hatching and triggers creation of juvenile objects  */
    virtual void Hatch(int a_eggsackspread, unsigned a_doubleeggsacspread, unsigned a_noeggs, unsigned a_range);
};
//------------------------------------------------------------------------------

/** \brief The generic base class for juvenile spiders  */
class Spider_Juvenile : public Spider_Base
{
public:
    // Static parameter passing attributes
    /** \brief Juvenile density dependent mortality threshold  */
    static int m_JuvDensityDepMortConst;
    /** \brief Disperals probability as a function of bad habitat days  */
    static std::array<int, 40> m_DispersalChance;
    /** \brief Day degrees maturation threshold  */
    static double m_JuvDevelConst;
protected:
    /** \brief The currrent movement direction  */
    char m_MyDirection;
    /** \brief Flag to force ballooning if possible  */
    bool m_MustBalloon;
    /** \brief The number of days in bad conditions  */
    int m_BadHabitatDays;
public:
    /** \brief the constructor  */
    Spider_Juvenile(int x, int y, Landscape* L, Spider_Population_Manager* SpPM);
    /** \brief Used to reinitialise reused objects  */
    virtual void ReInit(int x, int y, Landscape* L, Spider_Population_Manager* SpPM);
    /** \brief The BeginStep code  */
    virtual void BeginStep();
    /** \brief The Step code  */
    virtual void Step();
    /** \brief Determines the impact of any farm management events at the juvenile's location  */
    virtual bool OnFarmEvent(FarmToDo event) { return true; }
    /** \brief Used to impose extra mortaltiy to those spiderlings that don't reach a minimum developmental stage by the end of the year  */
    virtual void SpecialWinterMort();
protected:
    /** \brief The behavioural state development  */
    virtual TTypesOfSpiderState st_Develop();
    /** \brief The behavioural state assess habitat  */
    virtual TTypesOfSpiderState st_AssessHabitat() {
        return AssessHabitat();
    };
    /** \brief The behavioural state balloon  */
    virtual TTypesOfSpiderState st_Balloon();
    /** \brief The bheavioural state walk  */
    virtual TTypesOfSpiderState st_Walk();
    /** \brief The behavioural state maturation  */
    virtual void Maturation();
    /** \brief Determines the ballooning mortality associated with distance dist  */
    virtual bool BallooningMortality(int dist);
    /** \brief Carry out ballooning in a given direction and distance  */
    virtual int BalloonTo(int direction, int distance);
    /** \brief Walk in a given direction  */
    virtual int WalkTo(int direction);
    /** \brief Evaluates the habitat at current location  */
    virtual TTypesOfSpiderState AssessHabitat();
    /** \brief Carries out ballooning  */
    virtual int Balloon();
    /** \brief Carries out walking  */
    virtual int Walk();
    /** \brief Evaluates food status at current location  */
    virtual int AssessFood();
    /** \brief Returns the movement map value at current location  */
    virtual int CheckToleTovIndex();
    /** \brief Increments bad habitat days up to max of 39  */
    virtual void AddToBadHabitatDays() { if (m_BadHabitatDays < 39) m_BadHabitatDays++; }
};
//------------------------------------------------------------------------------

/** \brief The generic base class for female spiders  */
class Spider_Female : public Spider_Juvenile
{
// Attributes
public:
    /** \brief Daydegrees threshold needed to produce an eggsac  */
    static double m_EggProducConst;
    /** \brief The maximum number of eggs possible  */
    static int m_Max_Egg_Production;
    /** \brief Daily mortality probability  */
    static int m_DailyFemaleMort;
protected:
    /** \brief Day degrees sum for eggsac production  */
    double m_EggSacDegrees;
    /** \brief Record of the number of eggs produced  */
    int m_EggsProduced;
    /** \brief Limits the age to which the female spider can reach  */
    int m_lifespan;
// Methods
public:
    /** \brief The construtor  */
    Spider_Female(int x, int y, Landscape* L, Spider_Population_Manager* EPM);
    /** \brief Reinitialise reused objects  */
    void ReInit(int x, int y, Landscape* L, Spider_Population_Manager* EPM);
    /** \brief The BeingStep code  */
    virtual void BeginStep();
    /** \brief The Step code  */
    virtual void Step();
    /** \brief Determines the impact of any farm management events at the female's location  */
    virtual bool OnFarmEvent(FarmToDo event) { return true; }
    /** \brief Reset eggsac day degrees counter to zero  */
    void ZeroEggSacDegrees() { m_EggSacDegrees = 0; }
protected:
    /** \brief The behavioural state reproduce  */
    int st_Reproduce();
    /** \brief Produce an eggsac at current location  */
    bool ProduceEggSac();
    /** \brief Determines the number of eggs per egg sac - This must be overidden in descendent classes */
    virtual int CalculateEggsPerEggSac() { return 0; }
    /** \brief Creates the egg sac in the system */
    void CreateEggSac(int a_NoEggs);
    /** \brief The behavioural state balloon  */
    virtual TTypesOfSpiderState st_Balloon();
    /** \brief the behavioural state walk  */
    virtual TTypesOfSpiderState st_Walk();
};
//------------------------------------------------------------------------------

#endif

