﻿/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../BatchALMaSS/PositionMap.h"
#include "../Spiders/Spider_BaseClasses.h"
#include "../Spiders/Erigone.h"
#include "../Spiders/Erigone_Population_Manager.h"
//---------------------------------------------------------------------------


extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;
extern CfgBool cfg_ReallyBigOutputMonthly_used;
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;
extern CfgBool cfg_ReallyBigOutput_used;
//extern CfgFloat cfg_Erig_SpiderMinWalkTemp;

static CfgInt cfg_Erig_DailyEggMort("ERIGONE_DAILYEGGMORT", CFG_CUSTOM, 40);
static CfgFloat cfg_Erig_EggDevelConst2("ERIGONE_EGGDEVELCONSTTWO", CFG_CUSTOM, 1000);
static CfgFloat cfg_Erig_JuvDevelConst2("ERIGONE_JUVDEVELCONSTTWO", CFG_CUSTOM, 1000.0);
static CfgInt cfg_Erig_JuvDensityDepMortConst("ERIGONE_JUVDENSITYDEPMORTCONST", CFG_CUSTOM, 4); //  Was 4
static CfgInt cfg_Erig_HatDensityDepMortConst("ERIGONE_HATDENSITYDEPMORTCONST", CFG_CUSTOM, 3); //  Was 3
static CfgFloat cfg_Erig_EggProducConst("ERIGONE_EGGPRODUCCONST", CFG_CUSTOM, 1000);
static CfgInt cfg_Erig_Max_Egg_Production("ERIGONE_MAX_EGG_PRODUCTION", CFG_CUSTOM, 100);
static CfgInt cfg_Erig_DailyFemaleMort("ERIGONE_DAILYFEMALEMORT", CFG_CUSTOM, 3);

const std::array <int,40> Erig_DispersalChance = // Depending on days starved
// uses a index up to 40 because m_BadHabiatDays is used as the index
// which can only vary 0->39
{
     8,   9, 10, 11, 12, 14, 15, 16, 18, 22,
    29,  49, 61, 65, 67, 69, 71, 72, 74, 75,
    77,  79, 80, 81, 82, 82, 82, 82, 82, 82,
    82,  82, 82, 82, 82, 82, 82, 82, 82, 82
};


static CfgInt cfg_SpeciesRef( "SPID_SPECIES_REF",CFG_CUSTOM,0); // default=Erigone
const double MinBallooningTemp = 5.0;

//thermal threshold for developmet of eggsacs
static CfgFloat cfg_Erig_EggDevelThreshold("ERIGONE_EGGDEVELTHRESHOLD",CFG_CUSTOM,0.0);
static CfgFloat cfg_Erig_JuvDevelThreshold("ERIGONE_JUVDEVELTHRESHOLD",CFG_CUSTOM,0.0);
//good food levels
static CfgFloat cfg_Erig_EggProducSlope("ERIGONE_EGGPRODUCSLOPE",CFG_CUSTOM,0.0317);
//factor to multiply Eggsac production rate by at intermediate food levels
static CfgFloat cfg_Erig_EggProducIntemediate("ERIGONE_EGGPRODUCINTERMEDIATE",CFG_CUSTOM, 0.66);
//factor to multiply Eggsac production rate by at poor food levels
static CfgFloat cfg_Erig_EggProducPoor("ERIGONE_EGGPRODUCPOOR",CFG_CUSTOM, 0.33);
static CfgFloat cfg_Erig_EggDevelRHO25("ERIGONE_EGGVDEVELRH",CFG_CUSTOM,0.145043);
static CfgFloat cfg_Erig_EggDevelHA("ERIGONE_EGGVDEVELHA",CFG_CUSTOM,21132.95);
static CfgFloat cfg_Erig_JuvDevelRHO25("ERIGONE_EGGVDEVELRH",CFG_CUSTOM,0.131541);
static CfgFloat cfg_Erig_JuvDevelHA("ERIGONE_JUVVDEVELHA",CFG_CUSTOM,31273.77);
//factor to multiply developmental rate by at intermediate food levels
static CfgFloat cfg_Erig_JuvDevelIntermediate("ERIGONE_JUVDEVELINTERMEDIATE",CFG_CUSTOM,0.66666666); 
// factor to multiply developmental rate by at poor food levels
static CfgFloat cfg_Erig_JuvDevelPoor("ERIGONE_JUVDEVELPOOR",CFG_CUSTOM,0.33333333); 
static CfgFloat cfg_Erig_EggProducThreshold("ERIGONE_EGGPRODUCTHRESHOLD",CFG_CUSTOM,6.0);
// good food levels
static CfgFloat cfg_Erig_EggProducIntercept("ERIGONE_EGGPRODUCINTERCEPT",CFG_CUSTOM,-0.1905);
static CfgFloat cfg_Erig_EggProducRHO25("ERIGONE_EGGPRODUCRH",CFG_CUSTOM,0.5340);
static CfgFloat cfg_Erig_EggProducHA("ERIGONE_EGGPRODUCHA",CFG_CUSTOM,30981.0);
//Dispersal
static CfgInt cfg_Erig_BallooningStart("ERIGONE_BALLOONINGSTART", CFG_CUSTOM, 0);
static CfgInt cfg_Erig_BallooningStop("ERIGONE_BALLOONINGSTOP", CFG_CUSTOM, 366);
static CfgFloat cfg_Erig_SpiderMinWalkTemp("ERIGONE_MIN_WALK_TEMP", CFG_CUSTOM, 3);
static CfgFloat  cfg_Erig_MaxBalloonPercent("ERIGONE_MAXBALLOONPERCENT", CFG_CUSTOM, 5);
static CfgFloat  cfg_Erig_MaxDistBalloonable("ERIGONE_MAXDISTBALLOONABLE", CFG_CUSTOM, 2200);
/** \brief The minimum ballooning distnace */
static CfgInt cfg_Erig_SmallestBallooningDistance("ERIGONE_SMALLESTBALLOONDIST", CFG_CUSTOM, 70);
/** \brief The size of the frequency category for ballooning distances */
static CfgInt cfg_Erig_BallooningDistanceInterval("ERIGONE_BALLOONINGSTEP", CFG_CUSTOM, 10);
//Reproduction
static CfgInt cfg_Erig_EggSacSpread("ERIGONE_EGG_SAC_SPREAD", CFG_CUSTOM, 10);
// Mortality
static CfgFloat cfg_Erig_DailyJuvMort("ERIGONE_DAILYJUVMORT", CFG_CUSTOM, 0.003);
/** Based one De Keer R & Maelfait J‐P (1988a) Laboratory observations on the development and reproduction of Erigone atra, Blackwall, 1833 (Aranea, Linyphiidae). Bulletin of the British Arachnological Society 7: 237–242. 
** We use a temperature related daily juvenile mortality. 
** Using the admittedly small numbers of spiders in temperature range 10-20 degrees we get a linear relationship between temperature and survivial, such that daily mortality can be described by
** daily mort = -0.0006temp + 0.011. If we assume a higher mortality in the field the we can still use the value ERIGONE_DAILYJUVMORT, but add this additional mortality. This way we avoid the
** artificial over wintering mortality function.
*/
static CfgFloat cfg_Erig_TempDailyJuvMortParameterA("ERIGONE_DAILYTEMPJUVMORTPARAMA", CFG_CUSTOM, -0.0006);
static CfgFloat cfg_Erig_TempDailyJuvMortParameterB("ERIGONE_DAILYTEMPJUVMORTPARAMB", CFG_CUSTOM, 0.011);

const int Erig_Droughtintercepts[3] = { 112, 126, 140 };
const int Erig_DroughtMortConst = 20;
const double Erig_DroughtSlope = -2.8;
static CfgInt cfg_ErigoneDensityDependentConstantZero("ERIGONE_DENDEPCONSTZERO", CFG_CUSTOM, 0);
static CfgInt cfg_ErigoneStartNos("ERIGONE_STARTNO", CFG_CUSTOM, 50000);
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//                      Erigone_Population_Manager
//---------------------------------------------------------------------------


Erigone_Population_Manager::Erigone_Population_Manager(Landscape* p_L)
   : Spider_Population_Manager(p_L, 3)
{
  // Three lists are needed so need to remove 6 of the ten default arrays
    /** Loads the list of Animal Classes. */
    m_ListNames[0] = "Egg";		// from egg laying to the beginning of feeding
    m_ListNames[1] = "Juvenile";	// larval development + cocoon spinning
    m_ListNames[2] = "Adult";	// the summer diapause period
    m_ListNameLength = 3;
    Init();
}
//---------------------------------------------------------------------------


Erigone_Population_Manager::~Erigone_Population_Manager()
{
    delete m_MoveMap;
}
//---------------------------------------------------------------------------


void Erigone_Population_Manager::Init()
{
    m_EggSacSpread = cfg_Erig_EggSacSpread.value();
    m_DoubleEggSacSpread = 2 * m_EggSacSpread;
    m_BallooningMortalityPerMeter = cfg_Erig_MaxBalloonPercent.value() / cfg_Erig_MaxDistBalloonable.value();
    // Juvenile mortality is base mortality, but to this is added a temperature related daily mortality.
    m_DailyJuvMort = cfg_Erig_DailyJuvMort.value();
    m_EggSacSpread = cfg_Erig_EggSacSpread.value();
    m_BallooningStart = cfg_Erig_BallooningStart.value();
    m_BallooningStop = cfg_Erig_BallooningStop.value();

    m_EggDevelopmentThreshold = cfg_Erig_EggDevelThreshold.value();
    m_MinWalkTempThreshold = cfg_Erig_SpiderMinWalkTemp.value();

  m_MoveMap = new MovementMapUnsigned(m_TheLandscape, 2); // 2 for Erigone
  // Ballooning distances
  int count = 0;
  // ***Lumi*** can we replace this with a new distribution function?
  int distance = cfg_Erig_SmallestBallooningDistance.value();
  for (int i = 0; i < 31; i++)
  {
      while (count < m_DispDistances[i])
      {
          m_DispersalDistances[count++] = distance;
      }
      distance += cfg_Erig_BallooningDistanceInterval.value();
  }
  
  if ( cfg_RipleysOutput_used.value() ) {
    OpenTheRipleysOutputProbe("");
  }
  if ( cfg_ReallyBigOutput_used.value() ) {
    OpenTheReallyBigProbe();
  } else ReallyBigOutputPrb=0;
 // Set the simulation name
  m_SimulationName =  "Erigone";
 /**
 * A few objects are created here just to set the species specific parameter values.
 * It is preferred that this is done here rather than the constructure for parameters that are const since this only needs doing once.
 */
 Spider_Egg* spegg = new Spider_Egg(0, 0, m_TheLandscape, this,0);
 spegg->m_SimW = m_TheLandscape->SupplySimAreaWidth();
 spegg->m_SimH = m_TheLandscape->SupplySimAreaHeight();
 spegg->m_DenDependenceConst0 = cfg_ErigoneDensityDependentConstantZero.value();
 spegg->m_DailyEggMortConst = cfg_Erig_DailyEggMort.value();
 spegg->m_EggDevelConst = cfg_Erig_EggDevelConst2.value();
 spegg->m_HatDensityDepMortConst = cfg_Erig_HatDensityDepMortConst.value();
 delete spegg;
 Spider_Juvenile* spjuv = new Spider_Juvenile(0, 0, m_TheLandscape, this);
 spjuv->m_JuvDensityDepMortConst = cfg_Erig_JuvDensityDepMortConst.value();
 spjuv->m_JuvDevelConst = cfg_Erig_JuvDevelConst2.value();
 delete spjuv;
 Spider_Female* spad = new Spider_Female(0, 0, m_TheLandscape, this);
 spad->m_EggProducConst = cfg_Erig_EggProducConst.value();
 spad->m_Max_Egg_Production = cfg_Erig_Max_Egg_Production.value();
 spad->m_DailyFemaleMort = cfg_Erig_DailyFemaleMort.value();
 spad->m_DispersalChance = Erig_DispersalChance; 

 delete spad;

 // Create adults
 struct_Spider* aps;
 aps = new struct_Spider;
 aps->SpPM = this;
 aps->L = m_TheLandscape;
 aps->x = random(m_TheLandscape->SupplySimAreaWidth());
 aps->y = random(m_TheLandscape->SupplySimAreaHeight());

 for (int i=0; i<cfg_ErigoneStartNos.value(); i++) // Start number of spiders
 {
   aps->x = random(m_TheLandscape->SupplySimAreaWidth());
   aps->y = random(m_TheLandscape->SupplySimAreaHeight());
   if (m_AdultPosMap->GetMapValue(aps->x,aps->y)==0)
   {
      m_AdultPosMap->SetMapValue(aps->x,aps->y);
      CreateObjects(tspi_Female,NULL,aps,1);
   }
 }
 delete aps;
  m_population_type = TOP_Erigone;

  // Ensure that spider's execution order is shuffled after each time step
 // or do nothing if that is prefered
  BeforeStepActions[0]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[1]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[2]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing

#ifdef __RECORD_RECOVERY_POLYGONS
	/* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::out);
	ofile << "This file records the number of females in each polygon each day" << endl;
	ofile.close();
	/* Open the polygon recovery file and read in polygons to m_RecoveryPolygons */
	ifstream ifile("RecoveryPolygonsList.txt",ios::in);
	int n;
	ifile >> n;
	m_RecoveryPolygons[0] = n;
	for (int i=0; i<n; i++) ifile >> m_RecoveryPolygons[1+i];
	for (int i=0; i<n; i++) m_RecoveryPolygonsC[1+i]=0;
	ifile.close();
#endif
}
//---------------------------------------------------------------------------
#ifdef __RECORD_RECOVERY_POLYGONS
void Erigone_Population_Manager::RecordRecoveryPolygons()
{
	/**
	Only used for recovery pesticide experiment. 
	Loops through all beetle and records the numbers in any polygon identified as needing to be recorded in m_RecoveryPolygons[]
	*/
	int sz = (int)GetLiveArraySize(1);
    for (int j=0; j<sz; j++)
    {
      int p = TheArray[1][j]->SupplyPolygonRef();
	  for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	  {
		  if (p == m_RecoveryPolygons[i]) 
		  {
			  m_RecoveryPolygonsC[i]++;
			  break;
		  }
	  }
	}
	sz = (int)GetLiveArraySize(2);
    for (int j=0; j<sz; j++)
    {
      int p = TheArray[2][j]->SupplyPolygonRef();
	  for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	  {
		  if (p == m_RecoveryPolygons[i]) 
		  {
			  m_RecoveryPolygonsC[i]++;
			  break;
		  }
	  }
    }
	/* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::app);
	for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	{
		ofile << m_RecoveryPolygonsC[i] << '\t';
		m_RecoveryPolygonsC[i] = 0;
	}
	ofile << endl;
	ofile.close();
}
//---------------------------------------------------------------------------
#endif

void Erigone_Population_Manager::DoFirst()
{
#ifdef __RECORD_RECOVERY_POLYGONS
    /**
    There is also a special bit of code that is used for pesticide tests
    */
    RecordRecoveryPolygons();
#endif
    int today = m_TheLandscape->SupplyDayInYear();
    if (today == 364)
    {
        // Kill all eggs
        unsigned s = (unsigned)GetLiveArraySize(0);
        for (unsigned j = 0; j < s; j++)
        {
            int x{0}, y{0};
            SupplyLocXY(0,j,x,y);
            m_EggPosMap->ClearMapValue(x, y);
            //TheArray[0][j]->SetCurrentStateNo(-1); removed 15/10/2013 - seems superfluous given the call to KillThis
            SupplyAnimalPtr(0,j)->KillThis();
        }
        // Ensure all females have zero egg sac devel. degrees
        s = (unsigned)GetLiveArraySize(2);
        Erigone_Female* SFem;
        for (unsigned j = 0; j < s; j++)
        {
            SFem = (Erigone_Female*) SupplyAnimalPtr(2,j);
            SFem->ZeroEggSacDegrees();
        }
        /*  CJT: Removed due to temperature related mortality calculation
        // We also need to remove juveniles that are small and won't make it to adult before breeding
        Erigone_Juvenile* SJuv;
        s=(unsigned) GetLiveArraySize(1);
        for (unsigned j=0; j<s; j++)
        {
          SJuv= (Erigone_Juvenile *) TheArray[1][j];
          SJuv->SpecialWinterMort();
        }
        */
    }
    // Zero the daydegrees for the beginning of the year
    if (today == 0)
    {
        for (int i = 0; i < 365; i++) m_EggDegrees[i] = 0;
        m_EggProdThresholdPassed = false;
    }
    // Set the wind direction for today
  //  m_WindDirection=TheLandscape->SupplyWindDirection();
    m_WindDirection = random(8);  // until we have data
      // Is it ballooning time of year?
    if (!((today >= m_BallooningStart) && (today < m_BallooningStop)))
    {
        m_WalkingOnly = true;
    }
    else
    {
        m_WalkingOnly = false;
        // Is it ballooning weather today?
        double TheTemp = m_TheLandscape->SupplyTemp();
        if (TheTemp >= MinBallooningTemp)
        {
            double wi = m_TheLandscape->SupplyWind();
            if (wi < 5.0) m_TodaysBallooningTime = 17.0 - (3.7 * wi);
            else m_TodaysBallooningTime = 0;
            if (m_TodaysBallooningTime > BallooningHrs[today])
                m_TodaysBallooningTime = BallooningHrs[today];
        }
        else m_TodaysBallooningTime = 0;
        if (TheTemp >= m_MinWalkTempThreshold) m_MinWalkTemp = true;
        else m_MinWalkTemp = false;
    }

    //Create proportion (per mille, i.e. 1000 to hatch) of development finished for eggs and juveniles
 //eggs

    double temp = m_TheLandscape->SupplyTemp();
    double Added = 0;
    if (temp > m_EggDevelopmentThreshold)
        Added = 1000 * ((cfg_Erig_EggDevelRHO25.value() * ((273.15 + temp) / 298.15)) * //1.987=Gas constant
            exp((cfg_Erig_EggDevelHA.value() / 1.987) * ((1 / 298.15) - (1 / (273.15 + temp)))));

    // Add today's day degrees to the day degrees experienced each day to date
    for (int d = 0; d <= today; d++)
    {
        m_EggDegrees[d] += Added;
    }


    // juveniles
    // these differ from the eggs because we have two rates of growth dependent
    // upon the food supply at the spiders location
    if (temp > cfg_Erig_JuvDevelThreshold.value())
    {
        m_JuvDegreesGood = 1000 * ((cfg_Erig_JuvDevelRHO25.value() * ((273.15 + temp) / 298.15)) *
            exp((cfg_Erig_JuvDevelHA.value() / 1.987) * ((1 / 298.15) - (1 / (273.15 + temp))))); ;
    }
    else m_JuvDegreesGood = 0;
    if (temp > cfg_Erig_JuvDevelThreshold.value())
    {

        m_JuvDegreesPoor = cfg_Erig_JuvDevelPoor.value() * m_JuvDegreesGood;
    }
    else m_JuvDegreesPoor = 0;
    if (temp > cfg_Erig_JuvDevelThreshold.value())
    {

        m_JuvDegreesIntermediate = cfg_Erig_JuvDevelIntermediate.value() * m_JuvDegreesGood;
    }
    else m_JuvDegreesIntermediate = 0;
    //
    // Eggsac production
    if (m_EggProdThresholdPassed) {
        if (temp > 0) {
            if (cfg_SpeciesRef.value() == 0) // Erigone
            {
                /* CJT Modified 6 December 2006

                                m_EggProdDDegsGood=1000*(cfg_Erig_EggProducIntercept.value()
                                                                      +cfg_Erig_EggProducSlope.value()*temp);
                */
                /*New version   using biophysical model
                */
                //1.069	17794	89.9

                m_EggProdDDegsGood = (1000 * ((1.069 * ((273.15 + temp) / 298.15)) * exp((17794 / 1.987) * ((1 / 298.15) - (1 / (273.15 + temp)))))) - 89.9;
            }
            else {
                m_EggProdDDegsGood = 1000 * ((cfg_Erig_EggProducRHO25.value() * ((273.15 + temp) /
                    298.15)) * exp((cfg_Erig_EggProducHA.value() / 1.987) * ((1 / 298.15) -
                    (1 / (273.15 + temp)))));
            }
            if (m_EggProdDDegsGood < 0) m_EggProdDDegsGood = 0.0;
            m_EggProdDDegsPoor = cfg_Erig_EggProducPoor.value() * m_EggProdDDegsGood;
            m_EggProdDDegsInt = cfg_Erig_EggProducIntemediate.value() * m_EggProdDDegsGood;
        }
        else {
            m_EggProdDDegsPoor = 0;
            m_EggProdDDegsInt = 0;
            m_EggProdDDegsGood = 0;
        }
    }
    else
    {
        long date = m_TheLandscape->SupplyGlobalDate();
        double SevenDayTemp = 0;
        for (long d = date; d > date - 14; d--)
        {
            SevenDayTemp += (double)m_TheLandscape->SupplyTemp(d);
        }
        SevenDayTemp /= 14.0;
        if (SevenDayTemp > cfg_Erig_EggProducThreshold.value()) {
            m_EggProdThresholdPassed = true;
        }
    }
    temp = m_TheLandscape->SupplyTemp();
    // Work out drought for each of three veg classes
    if (m_TheLandscape->SupplyRain() > 0)
    {
        m_DaysSinceRain = 0;
        m_TodaysDroughtScore[0] = 0;
        m_TodaysDroughtScore[1] = 0;
        m_TodaysDroughtScore[2] = 0;
    }
    else
    {
        if (temp < 0) temp = 0; // means no extra drought at negative temps
        //drought function changed by **per**, instead of three slopes now one slope but three intercepts
        if (m_DaysSinceRain >= (Erig_Droughtintercepts[0] + Erig_DroughtSlope * temp)) m_TodaysDroughtScore[0] = 1;
        if (m_DaysSinceRain >= (Erig_Droughtintercepts[1] + Erig_DroughtSlope * temp)) m_TodaysDroughtScore[1] = 1;
        if (m_DaysSinceRain >= (Erig_Droughtintercepts[2] + Erig_DroughtSlope * temp)) m_TodaysDroughtScore[2] = 1;
    }
    // Juvenile temperature related mortality
    if (temp < 0) temp = 0; else if (temp > 20.0) temp = 20.0; // This limits the effect of temperature decreasing mortality below 1.2% per day.
    m_DailyJuvMort = cfg_Erig_DailyJuvMort.value() + cfg_Erig_TempDailyJuvMortParameterA.value() * temp + cfg_Erig_TempDailyJuvMortParameterB.value();
    m_TodaysMonth = m_TheLandscape->SupplyMonth();
}

//---------------------------------------------------------------------------



void Erigone_Population_Manager::CreateObjects(int ob_type,
           TAnimal * /* pvo */, struct_Spider * data,int number)
{
   Erigone_Female* new_Female;
   Erigone_Juvenile* new_Juvenile;
   Erigone_Egg* new_Egg;

   for (int i=0; i<number; i++)
   {
    if (ob_type == 0)
    {
        if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
            // We need to reuse an object
            dynamic_cast<Erigone_Egg*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this, data->noEggs);
            IncLiveArraySize(ob_type);
        }
        else {
            new_Egg = new Erigone_Egg(data->x, data->y, data->L, this, data->noEggs);
            PushIndividual(ob_type, new_Egg);
            IncLiveArraySize(ob_type);
        }
    }
    if (ob_type == 1)
    {
        if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
            // We need to reuse an object
            dynamic_cast<Erigone_Juvenile*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
            IncLiveArraySize(ob_type);
        }
        else {
            new_Juvenile = new Erigone_Juvenile(data->x, data->y, data->L, this);
            PushIndividual(ob_type, new_Juvenile);
            IncLiveArraySize(ob_type);
        }
    }
    if (ob_type == 2)
    {
        if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
            // We need to reuse an object
            dynamic_cast<Erigone_Female*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
            IncLiveArraySize(ob_type);
        }
        else {
            new_Female = new Erigone_Female(data->x, data->y, data->L, this);
            PushIndividual(ob_type, new_Female);
            IncLiveArraySize(ob_type);
        }
    }
   }
}
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void Erigone_Population_Manager::Catastrophe( void ) {
	// This version simply alters populations on 1st January - it is very dangerous to
	// add individuals in many of the models so beware!!!!

	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=1) return;
	// First do we have the right year?
	int year = m_TheLandscape->SupplyYearNumber();
	if (year%cfg_pm_eventfrequency.value()!=0) return;
    Erigone_Egg* SpE = NULL;
	Erigone_Juvenile* SpJ = NULL;
    Erigone_Female*  SpF = NULL;
	// Now if the % decrease is higher or lower than 100 we need to do different things
	int esize=cfg_pm_eventsize.value();
	if (esize<100) {
		unsigned size2;
			size2 = (unsigned)GetLiveArraySize(0);
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) >= esize)  {
                        SpE = dynamic_cast < Erigone_Egg * > ( SupplyAnimalPtr(0, j) );
                        SpE->KillThis(); // Kill it
				}
				}
			size2 = (unsigned) GetLiveArraySize(1);
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) >= esize) {
                        SpJ = dynamic_cast < Erigone_Juvenile * > ( SupplyAnimalPtr(1, j) );
                        SpE->KillThis(); // Kill it
                }
				}
			size2 = (unsigned) GetLiveArraySize(2);
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) >= esize) {
                        SpF = dynamic_cast <Erigone_Female * > ( SupplyAnimalPtr(2, j) );
                        SpE->KillThis(); // Kill it
                }
				}
		}
	else return; // No change so do nothing
}
//-----------------------------------------------------------------------------

void Erigone_Population_Manager::TheAOROutputProbe()
{
	m_AOR_Probe->DoProbe(2);
}
//-----------------------------------------------------------------------------

void Erigone_Population_Manager::TheRipleysOutputProbe( FILE* a_prb  ) {
  Erigone_Female* FS;
  unsigned totalF=(unsigned)GetLiveArraySize(2);
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
	  FS=dynamic_cast<Erigone_Female*>(SupplyAnimalPtr(2, j));
	  x=FS->Supply_m_Location_x();
	  y=FS->Supply_m_Location_y();
	  fprintf(a_prb,"%d\t%d\n", x,y);
  }
  fflush(a_prb);
}

//-----------------------------------------------------------------------------


