﻿/*
*******************************************************************************************************
Copyright (c) 2019, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Haplodrassus_Population_Manager.cpp
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
Version of  28 December 2019 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PositionMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Spiders/Spider_BaseClasses.h"
#include "../Spiders/Haplodrassus.h"
#include "../Spiders/Haplodrassus_Population_Manager.h"

extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;
extern CfgBool cfg_ReallyBigOutputMonthly_used;
extern CfgBool cfg_ReallyBigOutput_used;
//---------------------------------------------------------------------------------------

//ofstream _Debugger_HPM { "_Debugger_HPM", ios::out | ios::trunc }; // Haplodrassus Population Manager Debugger File

//---------------------------------------------------------------------------------------
/** \brief The number of spiders allowed to share an area before one must die */
static CfgInt cfg_Haplodrassus_DailyEggMort("HAPLODRASSUS_DAILYEGGMORT", CFG_CUSTOM, 0.001);
static CfgFloat cfg_Haplodrassus_EggDevelConst2("HAPLODRASSUS_EGGDEVELCONSTTWO", CFG_CUSTOM, 320.0);
static CfgFloat cfg_Haplodrassus_JuvDevelConst2("HAPLODRASSUS_JUVDEVELCONSTTWO", CFG_CUSTOM, 320.0);
static CfgInt cfg_Haplodrassus_JuvDensityDepMortConst("HAPLODRASSUS_JUVDENSITYDEPMORTCONST", CFG_CUSTOM, 0); //  Was 4
static CfgInt cfg_Haplodrassus_HatDensityDepMortConst("HAPLODRASSUS_HATDENSITYDEPMORTCONST", CFG_CUSTOM, 0); //  Was 3
static CfgFloat cfg_Haplodrassus_EggProducConst("HAPLODRASSUS_EGGPRODUCCONST", CFG_CUSTOM, 1000);
static CfgInt cfg_Haplodrassus_Max_Egg_Production("HAPLODRASSUS_MAX_EGG_PRODUCTION", CFG_CUSTOM, 10);
static CfgInt cfg_Haplodrassus_DailyFemaleMort("HAPLODRASSUS_DAILYFEMALEMORT", CFG_CUSTOM, 3);

const std::array <int, 40> Haplodrassus_DispersalChance = // Depending on days starved
// uses a index up to 40 because m_BadHabiatDays is used as the index
// which can only vary 0->39
{
     8,   9, 10, 11, 12, 14, 15, 16, 18, 22,
    29,  49, 61, 65, 67, 69, 71, 72, 74, 75,
    77,  79, 80, 81, 82, 82, 82, 82, 82, 82,
    82,  82, 82, 82, 82, 82, 82, 82, 82, 82
};


//static CfgInt cfg_SpeciesRef("SPID_SPECIES_REF", CFG_CUSTOM, 0); // default=Erigone
const double MinBallooningTemp = 5.0;

//thermal threshold for developmet of eggsacs
static CfgFloat cfg_Haplodrassus_EggDevelThreshold("HAPLODRASSUS_EGGDEVELTHRESHOLD", CFG_CUSTOM, 0.0);
static CfgFloat cfg_Haplodrassus_JuvDevelThreshold("HAPLODRASSUS_JUVDEVELTHRESHOLD", CFG_CUSTOM, 0.0);
//good food levels
static CfgFloat cfg_Haplodrassus_EggProducSlope("HAPLODRASSUS_EGGPRODUCSLOPE", CFG_CUSTOM, 0.0317);
//factor to multiply Eggsac production rate by at intermediate food levels
static CfgFloat cfg_Haplodrassus_EggProducIntemediate("HAPLODRASSUS_EGGPRODUCINTERMEDIATE", CFG_CUSTOM, 0.66);
//factor to multiply Eggsac production rate by at poor food levels
static CfgFloat cfg_Haplodrassus_EggProducPoor("HAPLODRASSUS_EGGPRODUCPOOR", CFG_CUSTOM, 0.33);
static CfgFloat cfg_Haplodrassus_EggDevelRHO25("HAPLODRASSUS_EGGVDEVELRH", CFG_CUSTOM, 0.145043);
static CfgFloat cfg_Haplodrassus_EggDevelHA("HAPLODRASSUS_EGGVDEVELHA", CFG_CUSTOM, 21132.95);
static CfgFloat cfg_Haplodrassus_JuvDevelRHO25("HAPLODRASSUS_EGGVDEVELRH", CFG_CUSTOM, 0.131541);
static CfgFloat cfg_Haplodrassus_JuvDevelHA("HAPLODRASSUS_JUVVDEVELHA", CFG_CUSTOM, 31273.77);
//factor to multiply developmental rate by at intermediate food levels
static CfgFloat cfg_Haplodrassus_JuvDevelIntermediate("HAPLODRASSUS_JUVDEVELINTERMEDIATE", CFG_CUSTOM, 0.66);
// factor to multiply developmental rate by at poor food levels
static CfgFloat cfg_Haplodrassus_JuvDevelPoor("HAPLODRASSUS_JUVDEVELPOOR", CFG_CUSTOM, 0.33);
static CfgFloat cfg_Haplodrassus_EggProducThreshold("HAPLODRASSUS_EGGPRODUCTHRESHOLD", CFG_CUSTOM, 6.0);
// good food levels
static CfgFloat cfg_Haplodrassus_EggProducIntercept("HAPLODRASSUS_EGGPRODUCINTERCEPT", CFG_CUSTOM, -0.1905);
static CfgFloat cfg_Haplodrassus_EggProducRHO25("HAPLODRASSUS_EGGPRODUCRH", CFG_CUSTOM, 0.5340);
static CfgFloat cfg_Haplodrassus_EggProducHA("HAPLODRASSUS_EGGPRODUCHA", CFG_CUSTOM, 30981.0);
//Dispersal
static CfgInt cfg_Haplodrassus_BallooningStart("HAPLODRASSUS_BALLOONINGSTART", CFG_CUSTOM, 0);
static CfgInt cfg_Haplodrassus_BallooningStop("HAPLODRASSUS_BALLOONINGSTOP", CFG_CUSTOM, 366);
static CfgFloat cfg_Haplodrassus_SpiderMinWalkTemp("HAPLODRASSUS_MIN_WALK_TEMP", CFG_CUSTOM, 3);
static CfgFloat  cfg_Haplodrassus_MaxBalloonPercent("HAPLODRASSUS_MAXBALLOONPERCENT", CFG_CUSTOM, 5);
static CfgFloat  cfg_Haplodrassus_MaxDistBalloonable("HAPLODRASSUS_MAXDISTBALLOONABLE", CFG_CUSTOM, 2200);
/** \brief The minimum ballooning distnace */
static CfgInt cfg_Haplodrassus_SmallestBallooningDistance("HAPLODRASSUS_SMALLESTBALLOONDIST", CFG_CUSTOM, 70);
/** \brief The size of the frequency category for ballooning distances */
static CfgInt cfg_Haplodrassus_BallooningDistanceInterval("HAPLODRASSUS_BALLOONINGSTEP", CFG_CUSTOM, 10);
//Reproduction
static CfgInt cfg_Haplodrassus_EggSacSpread("HAPLODRASSUS_EGG_SAC_SPREAD", CFG_CUSTOM, 10);
// Mortality
static CfgFloat cfg_Haplodrassus_DailyJuvMort("HAPLODRASSUS_DAILYJUVMORT", CFG_CUSTOM, 0.003);
/** Based one De Keer R & Maelfait J‐P (1988a) Laboratory observations on the development and reproduction of Erigone atra, Blackwall, 1833 (Aranea, Linyphiidae). Bulletin of the British Arachnological Society 7: 237–242.
** We use a temperature related daily juvenile mortality.
** Using the admittedly small numbers of spiders in temperature range 10-20 degrees we get a linear relationship between temperature and survivial, such that daily mortality can be described by
** daily mort = -0.0006temp + 0.011. If we assume a higher mortality in the field the we can still use the value HAPLODRASSUS_DAILYJUVMORT, but add this additional mortality. This way we avoid the
** artificial over wintering mortality function.
*/
static CfgFloat cfg_Haplodrassus_TempDailyJuvMortParameterA("HAPLODRASSUS_DAILYTEMPJUVMORTPARAMA", CFG_CUSTOM, -0.0006);
static CfgFloat cfg_Haplodrassus_TempDailyJuvMortParameterB("HAPLODRASSUS_DAILYTEMPJUVMORTPARAMB", CFG_CUSTOM, 0.11);

const int Haplodrassus_Droughtintercepts[3] = { 112, 126, 140 };
const int Haplodrassus_DroughtMortConst = 20;
const double Haplodrassus_DroughtSlope = -2.8;
static CfgInt cfg_Haplodrassus_DensityDependentConstantZero("HAPLODRASSUS_DENDEPCONSTZERO", CFG_CUSTOM, 0);
//static CfgInt cfg_Haplodrassus_StartNos("HAPLODRASSUS_STARTNO", CFG_CUSTOM, 50000);
static CfgInt cfg_Haplodrassus_StartNos("HAPLODRASSUS_STARTNO", CFG_CUSTOM, 300);


//---------------------------------------------------------------------------

Haplodrassus_Population_Manager::~Haplodrassus_Population_Manager(void)
{
    // Should all be done by the Population_Manager destructor
}
//---------------------------------------------------------------------------

Haplodrassus_Population_Manager::Haplodrassus_Population_Manager(Landscape* L) : Spider_Population_Manager(L, 3)
{
    // We need one vector for each life stage
    m_SimulationName = "Haplodrassus";
	// Create some animals


    /** Must assign the correct values to static members of this object but also spider base classes */

    m_EggSacSpread = cfg_Haplodrassus_EggSacSpread.value();
    m_DoubleEggSacSpread = 2 * m_EggSacSpread;
    m_BallooningMortalityPerMeter = cfg_Haplodrassus_MaxBalloonPercent.value() / cfg_Haplodrassus_MaxDistBalloonable.value();
    // Juvenile mortality is base mortality, but to this is added a temperature related daily mortality.
    m_DailyJuvMort = cfg_Haplodrassus_DailyJuvMort.value();
    m_EggSacSpread = cfg_Haplodrassus_EggSacSpread.value();
    m_BallooningStart = cfg_Haplodrassus_BallooningStart.value();
    m_BallooningStop = cfg_Haplodrassus_BallooningStop.value();

    m_EggDevelopmentThreshold = cfg_Haplodrassus_EggDevelThreshold.value();
    m_MinWalkTempThreshold = cfg_Haplodrassus_SpiderMinWalkTemp.value();

    m_MoveMap = new MovementMapUnsigned(m_TheLandscape, 2); // 2 for Erigone
    // Ballooning distances
    int count = 0;
    // ***Lumi*** can we replace this with a new distribution function?
    int distance = cfg_Haplodrassus_SmallestBallooningDistance.value();
    for (int i = 0; i < 31; i++)
    {
        while (count < m_DispDistances[i])
        {
            m_DispersalDistances[count++] = distance;
        }
        distance += cfg_Haplodrassus_BallooningDistanceInterval.value();
    }

    if (cfg_RipleysOutput_used.value()) {
        OpenTheRipleysOutputProbe("");
    }
    if (cfg_ReallyBigOutput_used.value()) {
        OpenTheReallyBigProbe();
    }
    else ReallyBigOutputPrb = 0;

    /**
    * A few objects are created here just to set the species specific parameter values.
    * It is preferred that this is done here rather than the constructor in the species for parameters that are const since this only needs doing once.
    */
    Spider_Egg* spegg = new Spider_Egg(0, 0, m_TheLandscape, this, 0);
    spegg->m_SimW = m_TheLandscape->SupplySimAreaWidth();
    spegg->m_SimH = m_TheLandscape->SupplySimAreaHeight();
    spegg->m_DenDependenceConst0 = cfg_Haplodrassus_DensityDependentConstantZero.value();
    spegg->m_DailyEggMortConst = cfg_Haplodrassus_DailyEggMort.value();
    spegg->m_EggDevelConst = cfg_Haplodrassus_EggDevelConst2.value();
    spegg->m_HatDensityDepMortConst = cfg_Haplodrassus_HatDensityDepMortConst.value();
    delete spegg;
    Spider_Juvenile* spjuv = new Spider_Juvenile(0, 0, m_TheLandscape, this);
    spjuv->m_JuvDensityDepMortConst = cfg_Haplodrassus_JuvDensityDepMortConst.value();
    spjuv->m_JuvDevelConst = cfg_Haplodrassus_JuvDevelConst2.value();
    delete spjuv;
    Spider_Female* spad = new Spider_Female(0, 0, m_TheLandscape, this);
    spad->m_EggProducConst = cfg_Haplodrassus_EggProducConst.value();
    spad->m_Max_Egg_Production = cfg_Haplodrassus_Max_Egg_Production.value();
    spad->m_DailyFemaleMort = cfg_Haplodrassus_DailyFemaleMort.value();
    spad->m_DispersalChance = Haplodrassus_DispersalChance;
    delete spad;

    // Create adults
    struct_Spider* aps;
    aps = new struct_Spider;
    aps->SpPM = this;
    aps->L = m_TheLandscape;
    aps->x = random(m_TheLandscape->SupplySimAreaWidth());
    aps->y = random(m_TheLandscape->SupplySimAreaHeight());

    for (int i = 0; i < cfg_Haplodrassus_StartNos.value(); i++) // Start number of spiders
    {
        aps->x = random(m_TheLandscape->SupplySimAreaWidth());
        aps->y = random(m_TheLandscape->SupplySimAreaHeight());
        if (m_AdultPosMap->GetMapValue(aps->x, aps->y) == 0)
        {
            m_AdultPosMap->SetMapValue(aps->x, aps->y);
            CreateObjects(tspi_Female, NULL, aps, 1);
        }
    }
    delete aps;
    m_population_type = TOP_Haplodrassus;

    // Ensure that spider's execution order is shuffled after each time step
   // or do nothing if that is prefered
    BeforeStepActions[0] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
    BeforeStepActions[1] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
    BeforeStepActions[2] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing

}
//------------------------------------------------------------------------------------------------------------------

void Haplodrassus_Population_Manager::CreateObjects(int ob_type, TAnimal*, struct_Spider* data, int number)
{
	//_Debugger_HPM << "[ CreateObjects() - Method Call ] " << endl;
	{
		Haplodrassus_Female* new_Female;
		Haplodrassus_Spiderling* new_Spiderling;
		Haplodrassus_Egg* new_Egg;

        for (int i = 0; i < number; i++)
        {
            if (ob_type == 0)
            {
                if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
                    // We need to reuse an object
                    dynamic_cast<Haplodrassus_Egg*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this, data->noEggs);
                    IncLiveArraySize(ob_type);
                }
                else {
                    new_Egg = new Haplodrassus_Egg(data->x, data->y, data->L, this, data->noEggs);
                    PushIndividual(ob_type, new_Egg);
                    IncLiveArraySize(ob_type);
                }
            }
            if (ob_type == 1)
            {
                if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
                    // We need to reuse an object
                    dynamic_cast<Haplodrassus_Spiderling*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
                    IncLiveArraySize(ob_type);
                }
                else {
                    new_Spiderling = new Haplodrassus_Spiderling(data->x, data->y, data->L, this);
                    PushIndividual(ob_type, new_Spiderling);
                    IncLiveArraySize(ob_type);
                }
            }
            if (ob_type == 2)
            {
                if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
                    // We need to reuse an object
                    dynamic_cast<Haplodrassus_Female*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
                    IncLiveArraySize(ob_type);
                }
                else {
                    new_Female = new Haplodrassus_Female(data->x, data->y, data->L, this);
                    PushIndividual(ob_type, new_Female);
                    IncLiveArraySize(ob_type);
                }
            }
        }
    }
}
//---------------------------------------------------------------------------

//--------------------------------------- STARTING HABITAT -----------------------------------------------------------------------

/*bool Haplodrassus_Population_Manager::IsStartHabitat(int p_x, int p_y) {
    _Debugger_HPM << "[ IsStartHabitat() - Method Call ] " << endl;

    TTypesOfLandscapeElement tole_element = m_TheLandscape->SupplyElementType(p_x, p_y); // Get the Tole present in the female location

    switch (tole_element) { // Invalid Habitas
    case tole_ActivePit:
    case tole_BareRock:
    case tole_Building:
    case tole_Carpark:
    case tole_Coast:
    case tole_DrainageDitch:
    case tole_Freshwater:
    case tole_LargeRoad:
    case tole_Marsh:
    case tole_Pond:
    case tole_Pylon:
    case tole_Railway:
    case tole_River:
    case tole_RiverBed:
    case tole_RoadsideSlope:
    case tole_Saltwater:
    case tole_SmallRoad:
    case tole_Stream:
    case tole_Track:
    case tole_WindTurbine:
        _Debugger_HPM << "  -> Invalid Tole -> Returning False. " << endl;
        _Debugger_HPM << "[ IsStartHabitat() - Method Call Ended ] " << endl;
        return false; // Is not a starting habitat
    default:
        _Debugger_HPM << "  -> Valid Tole -> Returning True. " << endl;
        _Debugger_HPM << "[ IsStartHabitat() - Method Call Ended ] " << endl;
        return true; // Is a starting habitat
    }
}*/






