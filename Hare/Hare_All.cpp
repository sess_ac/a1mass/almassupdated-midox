/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\brief
\file
Hare_all.cpp - contains the code for all hare related classes except THare
*/
//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include<vector>
#ifdef __UNIX
#undef max
#endif
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../Hare/Hare_All.h"
#include "../Hare/Hare_toletov.h"



//---------------------------------------------------------------------------
// Globals

double g_hare_peg_inertia;
double g_RMRrainFactor;
extern Landscape* g_land;

//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

// externals

//extern void FloatToDouble(double &d, float f);
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;
extern CfgBool cfg_ReallyBigOutput_used;
extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;
extern CfgBool cfg_BeetleBankInvert;
extern CfgInt cfg_BeetleBankMinX;
extern CfgInt cfg_BeetleBankMaxX;
extern CfgInt cfg_BeetleBankMinY;
extern CfgInt cfg_BeetleBankMaxY;
extern CfgFloat l_pest_daily_mort;
extern CfgInt cfg_CatastropheEventStartYear;


//---------------------------------------------------------------------------
CfgFloat cfg_HareWalkingPowerConst("HARE_WALKINGPOWERCONST", CFG_CUSTOM, -0.316);
CfgFloat cfg_VegHeightForageReduction("HARE_VEGHEIGHTFORAGEREDUCTION", CFG_CUSTOM, 0.0003);
CfgFloat cfg_FarmIntensiveness("HARE_FARMINTENSIVENESS", CFG_CUSTOM, 0.01);
CfgInt cfg_FarmIntensivenessH("HARE_FARMINTENSIVENESSH", CFG_CUSTOM, 10);
CfgFloat cfg_HareHunting("HARE_HUNTING", CFG_CUSTOM, 0.15);
CfgFloat cfg_HareHuntingBeetleBankArea("HARE_HUNTING_BEETLEBANKAREA",CFG_CUSTOM,0.95);
CfgInt cfg_HareHuntingThreshold("HARE_HUNTING_THRESHOLD",CFG_CUSTOM,0);
CfgInt cfg_HareHuntingType("HARE_HUNTING_TYPE",CFG_CUSTOM,0); // 1 = Threshold  2 = Differentiated for beetle bank areas.
CfgInt cfg_HareHuntingDate("HARE_HUNTING_DATE",CFG_CUSTOM,312); // Middle of DK hunting season
CfgInt cfg_fixadult_starve("HARE_FIXADULT_STARVE",CFG_CUSTOM,360);
CfgInt cfg_adult_starve("HARE_ADULT_STARVE",CFG_CUSTOM,5000);
CfgInt cfg_juv_starve("HARE_JUV_STARVE",CFG_CUSTOM,2500);
CfgInt cfg_young_ddepmort("HARE_YOUNGDDEPMORT",CFG_CUSTOM,16);
CfgFloat cfg_RMRrainFactor("HARE_RMRRAINFACTOR",CFG_CUSTOM,0.47);
CfgFloat cfg_JuvDDScale("HARE_JUVSCALEDD",CFG_CUSTOM,0.5);
CfgInt cfg_HareThresholdDD("HARE_THRESHOLDDD",CFG_CUSTOM,5);
CfgFloat cfg_min_growth_attain("HARE_MINGROWTHATTAIN",CFG_CUSTOM,0.475); // only used if __MINGROWTHATTAIN is defined
CfgFloat cfg_HareMortStochasticity("HARE_MORTSTOCHASTICITY",CFG_CUSTOM,1.0);  // 1.0 gives +/- 100%
CfgFloat cfg_hare_foetusenergyproportion("HARE_FOETUSENERGYPROPORTION",CFG_CUSTOM,0.024);
CfgInt cfg_HareFemaleReproMortValue("HARE_FEMALEREPROMORT",CFG_CUSTOM,0); // zero means no mort
CfgInt cfg_HareFemaleSicknessDensityDepValue("HARE_FEMALESICKNESSDENDEPVALUE",CFG_CUSTOM,40); // can be any number, but 30-100 is probably right
CfgFloat cfg_HareFemaleDensityDepValue("HARE_FEMALEDENDEPVALUE",CFG_CUSTOM,0.1);
//CfgInt cfg_Hare_YoungDD_Value("HARE_YOUNGDENDEPVALUE",CFG_CUSTOM,1);
CfgFloat cfg_HareMaleDensityDepValue("HARE_MALEDENDEPVALUE", CFG_CUSTOM, 0.1);
// ********** PESTICIDE RESPONSE INPUTS **********
/** \brief
* If set to true then hares will collect and respond to pesticide information. 
* This will slow the simulation down and is contingent upon the pesticide engine also being activated.
*/
CfgBool cfg_hare_pesticideresponse_on("HARE_PESTICIDERESPONSE_ON", CFG_CUSTOM, false);
/** \brief
* This is the value that triggers pesticide response, which may be a threshold, or if simply set to 0.0 (default) then a dose-response.
*/
CfgFloat cfg_HarePesticideAccumulationThreshold("HARE_PESTICIDEACCUMULATIONTHRESHOLD", CFG_CUSTOM, 0.0);
// ********** END PESTICIDE RESPONSE INPUTS **********

/**
This controls the % chance of dying due to agricultural mortality for infants. Invoked by any cutting or soil cultivation operation.
*/
CfgInt cfg_hare_i_cut("HARE_CUTTING_MORT_INFANT", CFG_CUSTOM, 50);
/**
This controls the % chance of dying due to agricultural mortality for young. Invoked by any cutting or soil cultivation operation.
*/
CfgInt cfg_hare_y_cut( "HARE_CUTTING_MORT_YOUNG", CFG_CUSTOM, 10 );
//
CfgInt cfg_Hare_Recovery_Time("HARE_RECOVERY_TIME", CFG_CUSTOM, 10); // in minutes
// Physiological life-span
CfgInt cfg_hare_max_age("HARE_MAX_AGE", CFG_CUSTOM, (int)(365*12.5));
CfgInt cfg_hare_max_age_var("HARE_MAX_AGE_VAR",CFG_CUSTOM,180);
// Energetic values
// CfgFloat cfg_Hare_StdKJRunning("HARE_STDKJRUNNING",CFG_CUSTOM,99999);
// CfgFloat cfg_Hare_StdKJWalking("HARE_STDKJWALKING",CFG_CUSTOM,99999);
// CfgFloat cfg_Hare_StdKJForaging("HARE_STDKJFORAGING",CFG_CUSTOM,99999);
CfgFloat cfg_Hare_StdSpeedWalking("HARE_STD_SPEEDWALKING",CFG_CUSTOM,0.65*60); // Slow amble!!! Just a guess - is 1/20th of max run speed
CfgFloat cfg_Hare_StdSpeedRunning("HARE_STD_SPEEDRUNNING",CFG_CUSTOM,13*60); // http://hypertextbook.com/facts/2001/RobertCohen.shtml 13/m/sec
// Misc
static CfgInt cfg_hare_StartingNo("HARE_START_NO",CFG_CUSTOM,50);
static CfgFloat cfg_HareStartWeight("HARE_STARTWEIGHT",CFG_CUSTOM,121.0*0.88*0.33); // Hacklander et al, 2002 but in dry weight without ingesta
static CfgInt cfg_hare_sex_ratio("HARE_SEX_RATIO", CFG_CUSTOM, 50); // Default 50:50
// The next value is out of 10000 and gives the backround rate of disturbance due
// to proximity of predators/people. 1000 will result in one event per 10 days
// on a probabilistic basis
static CfgFloat cfg_hare_proximity_alert("HARE_PROXIMITY_ALERT",CFG_CUSTOM,0.05); // 0.05 once in 20 days
// Below are the background mortality rates for the different stages in terms
// of mortality chance out of 10000 per day
//static CfgFloat cfg_hare_infant_predation("HARE_INFANT_PREDATION",CFG_CUSTOM,0.0046);
static CfgFloat cfg_hare_young_predation("HARE_YOUNG_PREDATION",CFG_CUSTOM,0.002);
static CfgFloat cfg_hare_juvenile_predation("HARE_JUVENILE_PREDATION",CFG_CUSTOM,0.0016);
static CfgFloat cfg_hare_male_predation("HARE_MALE_PREDATION",CFG_CUSTOM,0.0023);
static CfgFloat cfg_hare_female_predation("HARE_FEMALE_PREDATION",CFG_CUSTOM,0.0023); // Daily predation per year is calculated by (1-(10/10000) ^365) * 1.0
CfgFloat cfg_hare_adult_predation("HARE_ADULT_PREDATION",CFG_CUSTOM,0.0023); // Daily predation per year is calculated by (1-(10/10000) ^365) * 1.0

//**** MRR config variables
static CfgInt cfg_MRR_FirstYear("HARE_MRRFIRSTYEAR",CFG_CUSTOM,10000);
static CfgInt cfg_MRR_LastYear("HARE_MRRLASTYEAR",CFG_CUSTOM,0);
static CfgInt cfg_MRR1("HARE_MMRONE",CFG_CUSTOM,999);  // 75
static CfgInt cfg_MRR2("HARE_MMRTWO",CFG_CUSTOM,999);  // 270
static CfgInt cfg_MRR3("HARE_MMRTHREE",CFG_CUSTOM,999); // 285
static CfgInt cfg_MRR4("HARE_MMRFOUR",CFG_CUSTOM,999); // 315
static CfgInt cfg_MRR5("HARE_MMRFIVE",CFG_CUSTOM,270); // 345

// Energetic values
/*
static CfgFloat cfg_Hare_YoungSpeedWalking("HARE_YOUNG_SPEEDWALKING",CFG_CUSTOM,99999);
static CfgFloat cfg_Hare_JuvenileSpeedWalking("HARE_JUVENILE_SPEEDWALKING",CFG_CUSTOM,99999);
static CfgFloat cfg_Hare_AdultSpeedWalking("HARE_ADULT_SPEEDWALKING",CFG_CUSTOM,99999);
static CfgFloat cfg_Hare_YoungSpeedRunning("HARE_YOUNG_SPEEDRUNNING",CFG_CUSTOM,99999);
static CfgFloat cfg_Hare_JuvenileSpeedRunning("HARE_JUVENILE_SPEEDRUNNING",CFG_CUSTOM,99999);
static CfgFloat cfg_Hare_AdultSpeedRunning("HARE_ADULT_SPEEDRUNNING",CFG_CUSTOM,99999);
*/
static CfgFloat cfg_MaxEnergyIntakeScaler("HARE_MAXENERGYINTAKESCALER",CFG_CUSTOM, 3.0);
//static CfgFloat cfg_MaxGrowthEnergy("HARE_MAXGROWTHENERGY",CFG_CUSTOM, 82.0);
//static CfgFloat cfg_AdultPropGrowthEnergy("HARE_ADULT_PROPGROWTHENERGY",CFG_CUSTOM, 0.1);
/** 4% of total body weight is max - Hacklander pers comm (wet weight incl ingesta) */
static CfgFloat cfg_AdultMaxFat("HARE_ADULT_MAXFAT",CFG_CUSTOM, 0.04/(0.33*0.88));
//static CfgFloat cfg_KJpergLeveret("HARE_KJPERGLEVERET",CFG_CUSTOM,6.8); // Guessed - better value needed
//static CfgFloat cfg_milkconvrate("HARE_MILKCONVRATE",CFG_CUSTOM,0.58); // Hacklander et al
//static CfgFloat cfg_milkAsbsorption("HARE_MILKABSORPTION",CFG_CUSTOM, 0.99675); // Hacklander et al
//static CfgFloat cfg_solidAsbsorption("HARE_SOLIDABSORPTION",CFG_CUSTOM, 0.5); // Estimate
//static CfgFloat cfg_milktogLeveret("HARE_MILKTOLEVERET",CFG_CUSTOM, 0.704552); // Fitted EnergyCalcs.xls
//static CfgFloat cfg_solidtoghare("HARE_MILKTOLEVERET",CFG_CUSTOM, 0.5); // Estimate?
/** KJ extracted per minute with 100% access and digestability is 5.94 */
CfgFloat cfg_hare_ExtEff("HARE_EXTEFF",CFG_CUSTOM, 3.25);
// Reproduction parameters
static CfgInt cfg_DaysToOestrous("HARE_DAYSTOOESTROUS",CFG_CUSTOM,20); // This is rubbish, but to try to make the program work
static CfgInt cfg_hare_DaysToGestation("HARE_GESTATIONDAYS",CFG_CUSTOM,41);
//static CfgFloat cfg_hare_foetalConversionEff("HARE_FOETALCONVEFF",CFG_CUSTOM,(1/42.7) );
//static CfgFloat cfg_hare_foetusKJprop("HARE_FOETUSKJPROP",CFG_CUSTOM,0.029); // Fitting value
static CfgFloat cfg_maxLeveretBirthWeight("HARE_MAXLEVERETBIRTHWEIGHT",CFG_CUSTOM,125*0.88*0.33);
static CfgFloat cfg_minLeveretBirthWeight("HARE_MINLEVERETBIRTHWEIGHT",CFG_CUSTOM,95*0.88*0.33);
static CfgInt cfg_ReproStartDay("HARE_REPROSTARTDAY",CFG_CUSTOM,18);
static CfgInt cfg_ReproEndDay("HARE_REPROENDDAY",CFG_CUSTOM,240);
//
/** The value is the fatReserve in g at which the adult disperses looking for better food */
static CfgFloat cfg_hare_adult_dispersal_threshold("HARE_ADULT_DISP_THESHOLD",CFG_CUSTOM, 1600*0.02); // Arbitrary ca 2% of max body weight
/** The value below which reproduction is suspended */
static CfgFloat cfg_hare_adult_breed_threshold("HARE_ADULT_BREED_THESHOLD",CFG_CUSTOM, 1600*0.03); // Fitting parameter
// ditto juvenile
static CfgFloat cfg_hare_juvenile_dispersal_threshold("HARE_JUVENILE_DISP_THESHOLD",CFG_CUSTOM,99999);
// Starvation thresholds
static CfgInt cfg_infant_starvation_threshold("HARE_INFANT_STARVE_THRESHOLD",CFG_CUSTOM,4);
static CfgInt cfg_young_starvation_threshold("HARE_YOUNG_STARVE_THRESHOLD",CFG_CUSTOM,4);
static CfgInt cfg_juvenile_starvation_threshold("HARE_JUVENILE_STARVE_THRESHOLD",CFG_CUSTOM,16);
static CfgInt cfg_adult_starvation_threshold("HARE_ADULT_STARVE_THRESHOLD",CFG_CUSTOM,16);
CfgInt cfg_hare_minimum_breeding_weight("HARE_MIN_BREEDING_WT",CFG_CUSTOM,780);
//
// Movement
static CfgInt cfg_hare_max_dispersal("HARE_MAX_DISPERSAL",CFG_CUSTOM,1000); // m
// Maximum escape distances
CfgInt cfg_hare_escape_dist("HARE_ESCAPE_DIST",CFG_CUSTOM,100);
// Foraging
CfgFloat cfg_ForageRestingRatio("HARE_FORAGERESTRATIO",CFG_CUSTOM,0.67); // 2/3 forage time
CfgFloat cfg_HareInterferenceConstant("HARE_INTERFERENCECONSTANT",CFG_CUSTOM,-0.03);
CfgFloat cfg_AgeRelatedInterferenceScaling("HARE_AGERELATEDINTERFERENCESCALING",CFG_CUSTOM,1.0);
//
// Litter size  From Hanson 1992, Acta Theriologica 37:27-40
/*
static CfgFloat cfg_littersize_mean1("HARE_LITTERSIZE_MEAN_A",CFG_CUSTOM,1.535);
static CfgFloat cfg_littersize_mean2("HARE_LITTERSIZE_MEAN_B",CFG_CUSTOM,2.508);
static CfgFloat cfg_littersize_mean3("HARE_LITTERSIZE_MEAN_C",CFG_CUSTOM,2.514);
static CfgFloat cfg_littersize_mean4("HARE_LITTERSIZE_MEAN_D",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean5("HARE_LITTERSIZE_MEAN_E",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean6("HARE_LITTERSIZE_MEAN_F",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_SD1("HARE_LITTERSIZE_SD_A",CFG_CUSTOM,0.685);
static CfgFloat cfg_littersize_SD2("HARE_LITTERSIZE_SD_B",CFG_CUSTOM,0.957);
static CfgFloat cfg_littersize_SD3("HARE_LITTERSIZE_SD_C",CFG_CUSTOM,0.955);
static CfgFloat cfg_littersize_SD4("HARE_LITTERSIZE_SD_D",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD5("HARE_LITTERSIZE_SD_E",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD6("HARE_LITTERSIZE_SD_F",CFG_CUSTOM,0.6415);
*/

// From Kurt Hansens data  mean litter size with year
//Mean	1.535	2.508	2.514	1.722
//SD	0.685	0.957	0.955	0.642

static CfgFloat cfg_littersize_mean1("HARE_LITTERSIZE_MEAN_A",CFG_CUSTOM,1.535);
static CfgFloat cfg_littersize_mean2("HARE_LITTERSIZE_MEAN_B",CFG_CUSTOM,2.508);
static CfgFloat cfg_littersize_mean3("HARE_LITTERSIZE_MEAN_C",CFG_CUSTOM,2.514);
static CfgFloat cfg_littersize_mean4("HARE_LITTERSIZE_MEAN_D",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean5("HARE_LITTERSIZE_MEAN_E",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean6("HARE_LITTERSIZE_MEAN_F",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean7("HARE_LITTERSIZE_MEAN_G",CFG_CUSTOM,2.17);
static CfgFloat cfg_littersize_mean8("HARE_LITTERSIZE_MEAN_H",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean9("HARE_LITTERSIZE_MEAN_I",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean10("HARE_LITTERSIZE_MEAN_J",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean11("HARE_LITTERSIZE_MEAN_K",CFG_CUSTOM,1.72);
static CfgFloat cfg_littersize_mean12("HARE_LITTERSIZE_MEAN_L",CFG_CUSTOM,0.0);
static CfgFloat cfg_littersize_SD1("HARE_LITTERSIZE_SD_A",CFG_CUSTOM,0.685);
static CfgFloat cfg_littersize_SD2("HARE_LITTERSIZE_SD_B",CFG_CUSTOM,0.957);
static CfgFloat cfg_littersize_SD3("HARE_LITTERSIZE_SD_C",CFG_CUSTOM,0.955);
static CfgFloat cfg_littersize_SD4("HARE_LITTERSIZE_SD_D",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD5("HARE_LITTERSIZE_SD_E",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD6("HARE_LITTERSIZE_SD_F",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD7("HARE_LITTERSIZE_SD_G",CFG_CUSTOM,0.955);
static CfgFloat cfg_littersize_SD8("HARE_LITTERSIZE_SD_H",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD9("HARE_LITTERSIZE_SD_I",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD10("HARE_LITTERSIZE_SD_J",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD11("HARE_LITTERSIZE_SD_K",CFG_CUSTOM,0.6415);
static CfgFloat cfg_littersize_SD12("HARE_LITTERSIZE_SD_L",CFG_CUSTOM,0.0);
static CfgFloat cfg_hare_peg_inertia("HARE_PEG_INERTIA",CFG_CUSTOM,0.20);

/* Female hare sterility

No mechanism known but the following are Trines data for breeding and non-breeding females

Alder	med ar	uden ar	% uden ar
1	24	2	7.692307692
2	27	8	22.85714286
3	20	7	25.92592593
4	15	1	6.25
5+	8	3	27.27272727
alle	94	21	18.26086957

Mean without 1st year		20.57644901

*/

static CfgInt cfg_hare_firstyearsterility("HARE_FIRSTYEARSTERILITY",CFG_CUSTOM,769); // out of 10000
static CfgInt cfg_hare_femalesterility("HARE_FEMALESTERILITY",CFG_CUSTOM,1401);      // out of 10000 ( together with first year value gives a overall infertility of 20.57645%)


//---------------------------------------------------------------------------

/** \brief Used to scale access to crops for modern day farm intensiveness */
double g_VegHeightForageReduction;
double g_FarmIntensiveness;
double g_FarmIntensivenessH;

/**
The proportion of solid food assumed as a function of age (days)
*/
const double g_PropSolidFood[36] = {
  0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0.0114,  0.0229,  0.0343,
  0.0457,  0.0571,  0.0686,  0.0800,  0.0929,  0.1057,  0.1186,  0.1314,
  0.1443,  0.1571,  0.1700,  0.1986,  0.2271,  0.2557,  0.2843,  0.3129,
  0.3414,  0.3700,  0.5800,  0.7900,  1.0000
};

/**
The maximum energy that a leveret can use per day for growth (=max growth).\n
The values are the result of the sum of milk and solid food energy requirements in KJ per day
*/
const double g_MaxLeveretGrowthEnergy[36] = {
	0,15.5,24.6,33.8,43.0,52.1,61.3,70.5,79.6,88.8,98.0,107.1,116.3,125.4,134.6,143.8,152.9,162.1,171.3,180.4,189.6,198.8,207.9,217.1,226.2,
	235.4,244.6,253.7,262.9,272.1,281.2,290.4,299.5,308.7,317.9,327.0
};

/*
// from EnergeticCalcs_cro.xls in KJ per day
const double g_MaxMilkEnergy[36] = {
156.66,
173.60,
190.20,
206.49,
222.53,
238.36,
253.99,
269.45,
284.75,
299.91,
314.94,
326.10,
336.77,
347.04,
356.88,
366.31,
375.28,
383.90,
391.47,
398.66,
405.38,
411.72,
417.61,
423.13,
428.20,
424.55,
420.12,
414.82,
408.69,
401.75,
394.06,
385.50,
262.69,
134.18,
0.00,
0.00
};
*/

/**
Values providing the maximum energy possible to use on foetal mass per day per foetus in fat g equivalents
*/
const double g_hare_maxFoetalKJ[41] = {
	1.714019512,1.671169024,1.628318537,1.585468049,1.542617561,1.499767073,1.456916585,1.414066098,1.37121561,1.328365122,1.285514634,1.242664146,
	1.199813659,1.156963171,1.114112683,1.071262195,1.028411707,0.98556122,0.942710732,0.899860244,0.857009756,0.814159268,0.77130878,0.728458293,
	0.685607805,0.642757317,0.599906829,0.557056341,0.514205854,0.471355366,0.428504878,0.38565439,0.342803902,0.299953415,0.257102927,0.214252439,
	0.171401951,0.128551463,0.085700976,0.042850488,0.001
 };

// Static member intialisation
double* THare::m_vegPalatability = NULL;

//---------------------------------------------------------------------------

/**
\brief
THare_Population_Manager destructor
*/
THare_Population_Manager::~THare_Population_Manager (void)
{
	delete THare::m_vegPalatability;
	fclose(BodyBurdenPrb);
	free(m_PolyFood);
	delete m_OurMRRData;
}
//---------------------------------------------------------------------------

/**
\brief
 This is the constructor for the hare population manager.
*/
/**
  It simply dumps unnecessary lists from the main population record (TheArray),
  then calls Init which initialises the variables needed before running.
*/
THare_Population_Manager::THare_Population_Manager(Landscape* L)
	: Population_Manager(L, 5)
{
	Init();
}

//---------------------------------------------------------------------------
/**
\brief
Sets up data structures and calculations prior to starting simulation
*/
/**
Creates some hares in semi-sensible places.

*/
void THare_Population_Manager::Init()
{
	g_VegHeightForageReduction = cfg_VegHeightForageReduction.value();
	g_FarmIntensiveness = cfg_FarmIntensiveness.value();
	g_FarmIntensivenessH = cfg_FarmIntensivenessH.value();
	g_RMRrainFactor = cfg_RMRrainFactor.value();
	m_RefNums=0;
	m_MortStochast = cfg_HareMortStochasticity.value();
	m_HareThresholdDD = cfg_HareThresholdDD.value();
  if ( cfg_RipleysOutput_used.value() ) {
     OpenTheRipleysOutputProbe("");
  }
  if ( cfg_ReallyBigOutput_used.value() ) {
    OpenTheReallyBigProbe();
  } else ReallyBigOutputPrb=0;
  /**
  open the probe
  */
  BodyBurdenPrb = fopen("HareBodyBurden.txt", "w");
  if (!BodyBurdenPrb) {
		  g_msg->Warn(WARN_FILE, "Population_Manager::OpenBodyBurdenProbe(): ""Unable to open probe file", "HareBodyBurden.txt");
		  exit(1);
  }
  //-----------------------------------------------------------------------------
  // Initialisation code for the hare population manager
  m_SimulationName =  "Hare";
  // Load List of Hare Classes
  m_ListNames[0]="Infant";
  m_ListNames[1]="Young";
  m_ListNames[2]="Juvenile";
  m_ListNames[3]="Male";
  m_ListNames[4]="Female";
  m_ListNameLength = 5;
  m_population_type = TOP_Hare;
  // Create an instance of THare to set static members
  THare* thare = new THare( 0, 0, m_TheLandscape, this );
  thare->loadVegPalatability();
  delete thare;
  /** Set up before step action sorts
   This determines how we handle the arrays between steps
  */
  BeforeStepActions[0]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[1]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[2]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[3]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[4]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing

  /**
  Load State Names - these are just used to tell us what the animal is doing in the GUI
  */
  StateNames[tohs_InitialState] = "Initial State";
  StateNames[  tohs_NextStage ] = "Next Stage";
  StateNames[  tohs_Developing ] = "Development";
  StateNames[  tohs_Dispersal ] = "Dispersal";
  StateNames[  tohs_Foraging ] = "Foraging";
  StateNames[  tohs_Resting ] = "Resting";
  StateNames[  tohs_Running ] = "Running";
  StateNames[  tohs_ReproBehaviour ] = "ReprodBehaviour";
  StateNames[  tohs_Dying ] = "Dying";

  // Set udefined attributes for LE hare specifics
  m_TheLandscape->Set_all_Att_UserDefinedBool(hare_tole_init_friendly);

  // Deal with peg global
  g_hare_peg_inertia=cfg_hare_peg_inertia.value();
  // Create some hares
  // This is only used when the simulation starts and puts a starting number of
  // hares in the landscape at random positions
  //
  struct_Hare* sp;
  sp = new struct_Hare;
  sp->HM = this;
  sp->L = m_TheLandscape;
  sp->age=365;
  sp->weight=3800/3;
  sp->RefNum=0;
  for (int i=0; i<cfg_hare_StartingNo.value(); i++)
    {
		do {
			sp->x = random(SimW);
			sp->y = random(SimH);
			}
		while (!m_TheLandscape->SupplyAttUserDefinedBool(sp->x, sp->y));
		CreateObjects(3,NULL,NULL,sp,1); // 3 = Males
  }
  for (int i=0; i<cfg_hare_StartingNo.value(); i++)
  {
		do {
			sp->x = random(SimW);
			sp->y = random(SimH);
			}
		while (!m_TheLandscape->SupplyAttUserDefinedBool(sp->x, sp->y));
		CreateObjects(4,NULL,NULL,sp,1); // 4 = Females
  }
  delete sp;
  /** Loads a data file giving max daily growth at age
  together with cost of that growth per g. These are up to day 365. After 365 the cost is constant
  and assumes only fat is added
  */
  double weightDM, rmr, f0, f1,f2,protcost,fatcost, totalgrowthcost;
  ifstream* HareDataFile = new ifstream("Hare_GrowthALMaSS_Input.txt", ios::in);
  if (!HareDataFile->is_open()) {
	  // This is an error since we are running hares - so say so.
	  m_TheLandscape->Warn("THare_Population_Manager::Init - Hare_GrowthALMaSS_Input.txt is missing or in use", "");
	  exit(1);
  }
  else {
	  // File format: "Age"	"Ing-free dmBW (g,dm)"	"Protein W-gain (g,dm)"	"Fat W-gain (g,dm)"
	  // "KJcost prot"	"KJ cost fat"	"Energy cost (kJ) to achieve BW gain	" "resting energy kJ"
	  for (int i = 0; i<5000; i++) {
		  (*HareDataFile) >> f0 >> weightDM >> f1 >> f2 >> protcost >> fatcost >> totalgrowthcost >> rmr;
		  // These arrays are little wasteful of space because many are only really used for the first 36 days, but
		  // if this is a problem it can be fixed later CJT 12-09-2006
		  m_MaxDailyGrowthEnergyP[i] = protcost; // How much energy for protein growth max at age i
		  if (protcost>0) m_GrowthEfficiencyP[i] = f1 / protcost; // KJ per g protein at age i
		  else m_GrowthEfficiencyP[i] = 0;
		  m_MaxDailyGrowthEnergyF[i] = fatcost; // How much energy for fat growth max at age i
		  if (fatcost>0) m_GrowthEfficiencyF[i] = f2 / fatcost; // KJ per g fat at age i
		  else m_GrowthEfficiencyF[i] = 0;
		  // No 366 is in fact the value for day 366 and rest of life and is about adding only fat
		  m_MaxDailyGrowthEnergy[i] = totalgrowthcost;
		  m_GrowthEfficiency[i] = (f1 + f2) / totalgrowthcost;
		  m_DMWeight[i] = weightDM*cfg_min_growth_attain.value(); // Used to test for falling behind in growth
	  }
	  HareDataFile->close();
  }

  /**
  Calculates RMR based on Hacklander - these are the thermoneutral values
  */
  m_RMR[0][0]=0;
  m_RMR[1][0]=0;
  /* OLD CODE
  for (int i=1; i<7001; i++) {
    m_RMR[i]=((3.8*pow((double(i)/(0.88*0.33)),-0.27))*20.1*24)*((double(i)/(0.88*0.33))/1000.0); // KJ/day See energetics notes
  }
  */
	for (int i=1; i<=37; i++) {
		m_RMR[0][i]=(-1.5658*i + 58.487); // Slope KJ/day/degree See energetics notes
		m_RMR[1][i]=1525.4*pow(i,-0.3997); // KJ/day See energetics notes
	}


  // Energy for walking dependent upon weight KJ per m
  for (int i=1; i<7001; i++) {
      //m_KJperM[i]=(9.08*pow(((double (i)/(0.88*0.33))/1000.0),0.77344))*0.001; // KJ/M for each weight
	  double fi=3.75*i/1000.0; // Corrected for dw to ww + ingesta
      // m_KJperM[i]=(10.7*pow(fi,-0.316)*fi)/1000.0;
	  m_KJperM[i] = (10.7*pow(fi, cfg_HareWalkingPowerConst.value())*fi) / 1000.0;
  }
 m_KJperM[0]=0.0;

  double mean=cfg_littersize_mean1.value();
  double SD=cfg_littersize_SD1.value();
  CalcLitterSize(mean,SD,0);

  mean=cfg_littersize_mean2.value();
  SD=cfg_littersize_SD2.value();
  CalcLitterSize(mean,SD,1);

  mean=cfg_littersize_mean3.value();
  SD=cfg_littersize_SD3.value();
  CalcLitterSize(mean,SD,2);

  mean=cfg_littersize_mean4.value();
  SD=cfg_littersize_SD4.value();
  CalcLitterSize(mean,SD,3);

  mean=cfg_littersize_mean5.value();
  SD=cfg_littersize_SD5.value();
  CalcLitterSize(mean,SD,4);

  mean=cfg_littersize_mean6.value();
  SD=cfg_littersize_SD6.value();
  CalcLitterSize(mean,SD,5);

  mean=cfg_littersize_mean7.value();
  SD=cfg_littersize_SD7.value();
  CalcLitterSize(mean,SD,6);

  mean=cfg_littersize_mean8.value();
  SD=cfg_littersize_SD8.value();
  CalcLitterSize(mean,SD,7);

  mean=cfg_littersize_mean9.value();
  SD=cfg_littersize_SD9.value();
  CalcLitterSize(mean,SD,8);

  mean=cfg_littersize_mean10.value();
  SD=cfg_littersize_SD10.value();
  CalcLitterSize(mean,SD,9);

  mean=cfg_littersize_mean11.value();
  SD=cfg_littersize_SD11.value();
  CalcLitterSize(mean,SD,10);

  mean=cfg_littersize_mean12.value();
  SD=cfg_littersize_SD12.value();
  CalcLitterSize(mean,SD,11);

  for (int i=1; i<2500; i++) {
		m_Interference[i]=exp(i*cfg_HareInterferenceConstant.value());
	}
	m_Interference[0]=1.0;

  /**
  Ensure zeros in the density map!
  */
  for (int i=0; i<200; i++) {
    for (int j=0; j<200; j++) {
      m_DensityMap[0][i][j]=0;
      m_DensityMap[1][i][j]=0;
      m_DensityMap[2][i][j]=0;
      m_DensityMap[3][i][j]=0;
      m_DensityMap[4][i][j]=0;
      m_DensityMap[5][i][j]=0;
      m_DensityMap[6][i][j]=0;
      m_DensityMap[7][i][j]=0;
    }
  }
  m_PolyFood = (double*)malloc(sizeof(double)* (1+m_TheLandscape->SupplyLargestPolyNumUsed()) );
  if ( m_PolyFood == NULL ) {
	  g_msg->Warn( WARN_FILE, "HarePopulationManager::Init: Out of memory!", "" );
    exit( 1 );
  }
  /**
  Intitialise the Pattern Oriented Modelling output file
  */
  FILE* fp;
  fp=fopen("LitterProduction.txt","w");
  fclose(fp);
  fp=fopen("EnergyCheck.txt","w");
  fclose(fp);
  fp=fopen("POM_Hare.txt","w");
	fprintf(fp,"1) Timestep,\n");
	fprintf(fp,"2-6) Population Size of add stages,\n");
	fprintf(fp,"7) Females < 365 days old,\n");
	fprintf(fp,"8) Females >=1 & < 2 yrs old,\n");
	fprintf(fp,"9) Females >=2 & < 3 yrs old,\n");
	fprintf(fp,"10) Females >=3 & < 4 yrs old,\n");
	fprintf(fp,"11) Females >=4 & < 5 yrs old,\n");
	fprintf(fp,"12) Females >=4 yrs old,\n");
	fprintf(fp,"13) Mean bodyweight of females (dw),\n");
	fprintf(fp,"14) variance in bodyweight,\n");
  fclose(fp);
  fp=fopen("POM_FemaleHareWeights.txt","w");
  fclose(fp);
  // Mortality variables
  m_variableDD=10;
  // MRR
  m_OurMRRData = new MRR_Data;
}
//---------------------------------------------------------------------------
void THare_Population_Manager::CalcLitterSize(double mean, double SD, int index)
{
  /** Calculates probabilities out of 10000 that a litter of a certain size is obtained.\n
  Assumes a normal distribution around mean and SD. NB - will give problems if the SD==0.0
  */
  double P;
  double Distribution[7];
  double total=0;
  // Max 6 leverets
  if (mean<=0) {
	  for (int i=0; i<7; i++) {
		m_LitterSize[index][i]=10000;
	  }
  } else {
	  // (from Zar page 79).
	  for (int i=0; i<7; i++) {
		P=(1.0/SD*sqrt(2*3.14159265) );
		P*=exp(-1*(((i-mean)*(i-mean))/(2*SD*SD)));
		Distribution[i]=P;
		total+=P;
	  }
	  int last=0;
	  for (int i=0; i<7; i++) {
		m_LitterSize[index][i]=(int)(floor(0.5+((Distribution[i]/total)*10000)+last));
		last=m_LitterSize[index][i];
	  }
  }
}
//---------------------------------------------------------------------------
/**
   \brief Returns the litter size.
*/
/**
   Uses a frequency distribution out of 0-9999 and 6 bins and returns one of the bins as the return based on a random number from 0-9999
*/
int THare_Population_Manager::GetLitterSize(int litter)
{
  int testing=int(g_rand_uni()*10000);
  for (int i=0; i<7; i++) {
    if (testing<=m_LitterSize[litter][i]) return i;
  }
  // something wrong if the program gets to here
    m_TheLandscape->Warn("THare_Population_Manager::GetLitterSize - litter size error",NULL);
    exit(1);
}
//---------------------------------------------------------------------------

/**
   \brief The last method called before the new time-step starts
*/
/**
   Called after EndStep and before the landscape turns for the next time-step.\n
   Does special end of step actions here. In this case zeroing the density maps, optional debug and mark-release-recapture outputs
*/
void THare_Population_Manager::DoLast()
{
  Population_Manager::DoLast();

// Do our special end of time step actions here
  if (m_TheLandscape->SupplyDayInYear() == cfg_HareHuntingDate.value())
  {
	  if (cfg_HareHuntingType.value() == 1) HuntingGrid();
	  else if (cfg_HareHuntingType.value() == 2) HuntingDifferentiatedBeetleBankArea();
	  else Hunting();
  }
  // Clear the old data
  for (int i=0; i<100; i++) {
    for (int j=0; j<100; j++) {
      m_DensityMap[0][i][j]=0;
      m_DensityMap[1][i][j]=0;
      m_DensityMap[2][i][j]=0;
      m_DensityMap[3][i][j]=0;
      m_DensityMap[4][i][j]=0;
      m_DensityMap[5][i][j]=0;
    }
  }

  //Special hare debugging test
  //if (m_TheLandscape->SupplyDayInMonth()==1) {
  /*
  if (TheArray[hob_Female].size()>0) {
		Hare_Female*  HF;
        HF = dynamic_cast<Hare_Female*>(TheArray[hob_Female][0]);
		HF->dumpEnergy();
  }
  */
  /*
	FILE* hareweights=fopen(hareweights,"HareWeights.txt","a");
	  for (unsigned listindex=0; listindex<5; listindex++)
	  {
		unsigned size=TheArray[listindex].size();
		if (size>0) {
			for (unsigned j=0; j<size; j++)
		{
			int age=dynamic_cast < THare * > ( TheArray[listindex][j] )->GetAge();
			double w=dynamic_cast < THare * > ( TheArray[listindex][j] )->GetWeight();
			int  weight=int (floor(0.5+w));
			fprintf(hareweights,"%i\t%i\n",age, weight);
		}
		}
  }
  fclose(hareweights);
	  // End special
*/
	MRROutputs();
}
//---------------------------------------------------------------------------

/**
   \brief The first method called of the new time-step
*/
/**
   Does special start of step actions here. In this case optional changes to parameter values e.g. mortality rates, data outputs e.g. POMOutputs(), and clearing the forage food array.
*/
void THare_Population_Manager::DoFirst()
{
#ifdef __EXTRAPOPMORT
	ExtraPopMort();
#endif
	if (m_TheLandscape->SupplyDayInYear()==1) {

#ifdef __VARIABLEDD
		m_variableDD=random(21);
#endif
#ifdef __YEARLYVARIABLEFOODQUALITY
		m_GoodYearBadYear = (float) (1.0 + float(random(40)-20)/100.0);
#endif
#ifdef __MORTSTOCHASTICITY
		double MortStochasticity = ((random(200)/100.0)-1.0)* m_MortStochast; // -1.0 to +1.0 * m_MortStochast
		m_YoungMortRate = cfg_hare_young_predation.value() + (cfg_hare_young_predation.value()*(MortStochasticity)); //
		m_JuvMortRate = cfg_hare_juvenile_predation.value()+ (cfg_hare_juvenile_predation.value()*(MortStochasticity)); //
		m_AdultMortRate = cfg_hare_adult_predation.value()+ (cfg_hare_adult_predation.value()*(MortStochasticity)); //
#else
		m_YoungMortRate = cfg_hare_young_predation.value();
		m_JuvMortRate=cfg_hare_juvenile_predation.value(); //
		m_AdultMortRate=cfg_hare_adult_predation.value(); //
#endif
	}
	//else if (m_TheLandscape->SupplyDayInYear()==(cfg_HareHuntingDate.value()-1)) {
  int x,y;
  for (unsigned listindex=0; listindex<2;listindex++) {
	  unsigned size = GetLiveArraySize(listindex);
    for (unsigned j=0; j<size; j++)
    {

      SupplyLocXY(listindex, j, x, y);
	  AddHareDensity(x, y, (Hare_Object) listindex);
    }
  }
  for (unsigned listindex=2; listindex<SupplyListIndexSize();listindex++) {
    unsigned size= GetLiveArraySize(listindex);
    for (unsigned j=0; j<size; j++)
    {
      SupplyLocXY(listindex, j, x, y);
      x=x>>__DENSITYSHIFT;
      y=y>>__DENSITYSHIFT;
      m_DensityMap[listindex][x][y]++;
      m_DensityMap[5][x][y]++;
      m_DensityMap[7][x][y]++;
    }
  }
  if (m_TheLandscape->SupplyDayInYear()==October) {
	  for (int i=0; i<100; i++) {
		  for (int j=0; j<100; j++) {
			  m_DensityMap[6][i][j]=m_DensityMap[7][i][j]/365;
			  m_DensityMap[7][i][j]=0;
		  }
	  }
  }

  if (m_TheLandscape->SupplyDayInYear() == (270))
  {
	  POMOutputs();
#ifdef __STERILITY
	  unsigned size = GetLiveArraySize(hob_Female);
	  for (unsigned j = 0; j<size; j++) {
		  Hare_Female*  HF;
		  HF = dynamic_cast<Hare_Female*>(TheArray[hob_Female][j]);
		  if (HF->GetAge() < 730) {
			  int test = int(g_rand_uni() * 10000);
			  if (HF->GetAge() < 730) {
				  if (test<cfg_hare_firstyearsterility.value()) HF->SetSterile();
			  }
			  else {
				  if (test<cfg_hare_femalesterility.value()) HF->SetSterile();
			  }
		  }
	  }
#endif
  }

  // Now do clear the forage food array
  for (int i=0; i<=m_TheLandscape->SupplyLargestPolyNumUsed(); i++) {
	  m_PolyFood [i]=-1;
  }
}
//---------------------------------------------------------------------------


/**
\brief
Method used to create new hares.
*/
/**
Creates new hare objects that have just been born, or because of changes of life-stage require the creation of a new object. Input data is passed in the struct_Hare
\n
   This is a very important method - each type of population manager has its own implementation. This is the method that must be called every time a new Hare object is created.
*/
void THare_Population_Manager::CreateObjects(int ob_type,
	TAnimal* pvo, void* /* null */, struct_Hare * data, int number)
{
	Hare_Infant*  new_Infant;
	Hare_Young*  new_Young;
	Hare_Juvenile*  new_Juvenile;
	Hare_Female*  new_Female;
	Hare_Female*  Mum;
	Hare_Male*  new_Male;
	for (int i = 0; i < number; i++)
	{
		if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
			// We need to reuse an object
			if (ob_type == 0)   // Infant
			{
				new_Infant = dynamic_cast<Hare_Infant*> (SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Infant->ReInit((*data));
				Mum = dynamic_cast<Hare_Female*>(pvo);
				if (Mum) new_Infant->SetMum(Mum);
				new_Infant->SetWeight(data->weight);
				if (Mum) Mum->AddYoung(new_Infant);
			}
			if (ob_type == 1)   // Young
			{
				new_Young = dynamic_cast<Hare_Young*> (SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Young->ReInit((*data));
				new_Young->SetMum(data->Mum); // NB Mum may be NULL - this is OK								 
				THare* old_Young = (THare*)pvo;
				if (data->Mum) {
					data->Mum->UpdateYoung(old_Young, new_Young);
				}
			}
			if (ob_type == 2)  // Juvenile
			{
				new_Juvenile = dynamic_cast<Hare_Juvenile*> (SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Juvenile->ReInit((*data));
				new_Juvenile->SetMum(NULL);
			}
			if (ob_type == 3)   // Male
			{
				new_Male = dynamic_cast<Hare_Male*> (SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Male->ReInit((*data));
			}
			if (ob_type == 4)  // Female
			{
				new_Female = dynamic_cast<Hare_Female*> (SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type)));
				new_Female->ReInit((*data));
			}
		}
		else {

			if (ob_type == 0)   // Infant
			{
				new_Infant = new Hare_Infant(data->x, data->y, data->L, data->HM);
                PushIndividual(ob_type, new_Infant);
				Mum = dynamic_cast<Hare_Female*>(pvo);
				if (Mum) new_Infant->SetMum(Mum);
				new_Infant->SetWeight(data->weight);
				if (Mum) Mum->AddYoung(new_Infant);
			}
			if (ob_type == 1)   // Young
			{
				new_Young = new Hare_Young(data->x, data->y, data->L, data->HM,
					data->weight);
				new_Young->SetMum(data->Mum); // NB Mum may be NULL - this is OK
				// *** DeBug ***
				//new_Young->m_MyOldMum=data->oldMum;
				THare* old_Young = (THare*)pvo;
				if (data->Mum) {
					data->Mum->UpdateYoung(old_Young, new_Young);
				}
                PushIndividual(ob_type, new_Young);

			}
			if (ob_type == 2)  // Juvenile
			{
				new_Juvenile = new Hare_Juvenile(data->x, data->y, data->L,
					data->HM, data->weight);
				new_Juvenile->SetMum(NULL);
				// *** DeBug ***
				//new_Juvenile->m_MyOldMum=data->oldMum;

                PushIndividual(ob_type, new_Juvenile);
			}
			if (ob_type == 3)   // Male
			{
				new_Male = new Hare_Male(data->x, data->y, data->L, data->HM,
					data->weight, data->age, data->RefNum);
                PushIndividual(ob_type, new_Male);
			}
			if (ob_type == 4)  // Female
			{
				new_Female = new Hare_Female(data->x, data->y, data->L, data->HM,
					data->weight, data->age, data->RefNum);
                PushIndividual(ob_type, new_Female);
			}
		}
		IncLiveArraySize(ob_type);
	}
}

//---------------------------------------------------------------------------

/**
 RMR is weight dependent based on RMR=69.1*4.1868)w^0.808 (McNab 1988)\n
 But affected by temperature. We assume that after 35days 1kg weight, that this effect is constant per kg\n
*/
double THare_Population_Manager::GetRMR(int a_age, double a_size) {
	double a_temp = m_TheLandscape->SupplyTemp();
	if (a_temp>20.0) a_temp=20.0;
	double tempdiff=18.0-a_temp;// 18 is used because this was the value where Hacklander
							    //started his expts - 20 is assumed thermoneutral point and
							    // a linear relationship between these two
	// Adjust for rainfall - assume that if we have a rainfall of >5mm then the temp diff is maximum - removing this might help add variation to juvenile survivorship!
	double rainfall = m_TheLandscape->SupplyRain();
	if (rainfall>5.0) rainfall=5.0;
	tempdiff*=1.0+(rainfall*g_RMRrainFactor);
	//tempdiff*=1.0+((rainfall*rainfall*rainfall*g_RMRrainFactor)/25); // Gives a curve sloping up

#ifdef __BOUNDSCHECK
	   if (a_age<0)  {
		     m_TheLandscape->Warn( "Hare GetRMR a_age range out of bounds", NULL );
			 exit( 1 );
	   }
#endif
	a_size/=1000.0;
	double RMR;
	if (a_age<37) {
		RMR=((tempdiff*m_RMR[0][a_age])+m_RMR[1][a_age])*(a_size);
	} else
	{
		a_age=36;
		RMR=(69.1*4.1868)*pow(a_size,0.808);
		RMR=RMR+(tempdiff*m_RMR[0][a_age]*a_size);
	}
	return RMR;
}
//---------------------------------------------------------------------------

/**
	 \brief
	 This method is called to dump the information needed for POM approaches
*/
/**
	 The data dumped is variable dependent upon what is needed at the time. Contents vary from version to version.
*/
void THare_Population_Manager::POMOutputs() {
	//
	// Outputs are:
	// 1) Timestep
	// 2-6) Population Size of add stages
	// 7) Females < 2 yrs old
	// 8) Females >=2 & < 3 yrs old
	// 9) Females >=3 & < 4 yrs old
	// 10) Females >=4 & < 5 yrs old
	// 11) Females >=5 yrs old
	// 12) Mean bodyweight of females (dw)
	// 13) variance in bodyweight
	// 14) Mean yearlings
	// 14) Var yearlings
	// 16) m_shot

	int age0 = 0;
	int age1 = 0;
	int age2 = 0;
	int age3 = 0;
	int age4 = 0;
	int age5 = 0;
	int age6 = 0;
	int age7 = 0;
	double totweight = 0.0;
	double totweight2 = 0.0;
	double yearlingstotweight = 0.0;
	double yearlingstotweight2 = 0.0;
	FILE* POMOut = fopen("POM_Hare.txt", "a");
	FILE* POMOut2 = fopen("POM_FemaleHareWeights.txt", "a");
	if (!POMOut) {
		// something wrong if the program gets to here
		m_TheLandscape->Warn("THare_Population_Manager::POMOutputs - POM_Output.txt can't be opened", NULL);
		exit(1);
	}
	// Now sort out the data for the females
	int yearlingssize = 0;
	unsigned size = GetLiveArraySize(hob_Juvenile);
	for (unsigned j = 0; j<size; j += 2) { // Every other one, so assume 50% males
		Hare_Juvenile*  HJ;
		HJ = dynamic_cast<Hare_Juvenile*> (SupplyAnimalPtr(hob_Juvenile,j));
		double bw = HJ->GetTotalWeight();
		if (bw>1000) {
			yearlingstotweight += bw; // This removes hares that are still almost leverets
			yearlingstotweight2 += (bw*bw);
			yearlingssize++;
			fprintf(POMOut2, "0\t%g\n", (float)bw);
		}
	}
	size = GetLiveArraySize(hob_Female);
	for (unsigned j = 0; j < size; j++) {
		Hare_Female*  HF;
		HF = dynamic_cast<Hare_Female*> (SupplyAnimalPtr(hob_Female,j));
		double bw = HF->GetTotalWeight();
		if (HF->GetAge() < 365) {
			age0++;
			if (bw>333) {
				yearlingstotweight += bw; // This removes hares that are still almost leverets
				yearlingstotweight2 += (bw*bw);
				yearlingssize++;
			}
			fprintf(POMOut2, "0\t%g\n", (float)bw);
		}
		else {
			totweight += bw;
			totweight2 += (bw*bw);
			if (HF->GetAge() < 730) {
				age1++;
				fprintf(POMOut2, "1\t%g\n", (float)bw);
			}
			else if (HF->GetAge() < 1095) {
				age2++;
				fprintf(POMOut2, "2\t%g\n", (float)bw);
			}
			else if (HF->GetAge() < 1460) {
				age3++;
				fprintf(POMOut2, "3\t%g\n", (float)bw);
			}
			else if (HF->GetAge() < 1825) {
				age4++;
				fprintf(POMOut2, "4\t%g\n", (float)bw);
			}
			else if (HF->GetAge() < 2190) {
				age5++;
				fprintf(POMOut2, "5\t%g\n", (float)bw);
			}
			else if (HF->GetAge() < 2555) {
				age6++;
				fprintf(POMOut2, "6\t%g\n", (float)bw);
			}
			else {
				age7++;
				fprintf(POMOut2, "7\t%g\n", (float)bw);
			}
		}
		// Do the bodyweight stuff
	}
	double yearlingsvar = (yearlingstotweight2 - ((yearlingstotweight*yearlingstotweight) / double(yearlingssize))) / double(yearlingssize);
	double yearlingsmean = yearlingstotweight / double(yearlingssize);
	double var = (totweight2 - ((totweight*totweight) / double(size))) / double(size);
	double mean = totweight / double(size);
	float meanf = (float)mean;
	float varf = (float)var;
	float meany = (float)yearlingsmean;
	float vary = (float)yearlingsvar;
	/*
	// Lets test the density grid
	int den = 0;
	for (int i = 0; i < 200; i++)
	for (int j = 0; j < 200; j++)
	{
	for (int l = 0; l < hob_Foobar; l++)
	den += m_DensityMap[l][i][j];
	}
	*/
	fprintf(POMOut, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%g\t%g\t%g\t%g\t%d\n", (int)m_TheLandscape->SupplyDayInYear(), GetLiveArraySize(hob_Infant), GetLiveArraySize(hob_Young), GetLiveArraySize(hob_Juvenile), GetLiveArraySize(hob_Male), GetLiveArraySize(hob_Female), age0, age1, age2, age3, age4, age5, age6, age7, meanf, varf, meany, vary, m_shot);
	fclose(POMOut);
	fclose(POMOut2);
}

//---------------------------------------------------------------------------

/**
Special probe for data to be used in spatial statistics
*/
void THare_Population_Manager::TheRipleysOutputProbe( FILE* a_prb ) {
  // Currently just counts females
	Hare_Female* FH;
	unsigned totalF= GetLiveArraySize(hob_Female);
	int x,y;
	int w = m_TheLandscape->SupplySimAreaWidth();
	int h = m_TheLandscape->SupplySimAreaHeight();
	fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
	for (unsigned j=0; j<totalF; j++)      //adult females
	{
		FH=dynamic_cast<Hare_Female*>(SupplyAnimalPtr(hob_Female,j));
		x=FH->Supply_m_Location_x();
		y=FH->Supply_m_Location_y();
		fprintf(a_prb,"%d\t%d\n", x,y);
	}
	fflush(a_prb);
}
//-----------------------------------------------------------------------------

void THare_Population_Manager::TheAOROutputProbe() {
	m_AOR_Probe->DoProbe(hob_Female);
}
//-----------------------------------------------------------------------------

/**
\brief
Special probe for data to be used in mark-release-recapture simulations
*/
/**
What this has to do is to print to the MMR file the occurence if any of each animal that ever existed between two dates.    \n
If that animal has been counted before then add to that animals line, otherwise create a new line.    \n
Requires that each hare has a unique ID number. \n
NB this counts the population with zero error.
*/
void THare_Population_Manager::MRROutputs() {
    //
	// Step one - do we count today?
	if ((m_TheLandscape->SupplyYearNumber()<cfg_MRR_FirstYear.value())||(m_TheLandscape->SupplyYearNumber()>cfg_MRR_LastYear.value())) return;
    int day=m_TheLandscape->SupplyDayInYear();
    if ((day!=cfg_MRR1.value()) && (day!=cfg_MRR2.value()) && (day!=cfg_MRR3.value()) && (day!=cfg_MRR4.value()) && (day!=cfg_MRR5.value())) return;
    // We want to count today
    // Loop through all Juvenile and Adult hares and put their details in the MRR data array
	unsigned size = GetLiveArraySize(hob_Juvenile);
	for (unsigned j=0; j<size; j++) { // Every other one, so assume 50% males
		Hare_Juvenile*  HJ;
        HJ = dynamic_cast<Hare_Juvenile*>(SupplyAnimalPtr(hob_Juvenile,j));
		m_OurMRRData->AddEntry(HJ->GetRefNum());
	}
	size = GetLiveArraySize(hob_Female);
	for (unsigned j=0; j<size; j++) {
		Hare_Female*  HF;
        HF = dynamic_cast<Hare_Female*>(SupplyAnimalPtr(hob_Female,j));
		m_OurMRRData->AddEntry(HF->GetRefNum());
	}
	size= GetLiveArraySize(hob_Male);
	for (unsigned j=0; j<size; j++) {
		Hare_Male*  HM;
        HM = dynamic_cast<Hare_Male*>(SupplyAnimalPtr(hob_Male,j));
		m_OurMRRData->AddEntry(HM->GetRefNum());
	}

	// If we need to dump the results this time do this
	if ((day==cfg_MRR5.value() && (m_TheLandscape->SupplyYearNumber()==cfg_MRR_LastYear.value()))) m_OurMRRData->OutputToFile();
	m_OurMRRData->IncTrapping();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//       Hare_Infant CODE
//---------------------------------------------------------------------------

Hare_Infant::Hare_Infant(int p_x, int p_y,
                           Landscape * p_L, THare_Population_Manager* p_PPM)
                                          : THare(p_x,p_y,p_L,p_PPM)
{
	Init();
}
void Hare_Infant::ReInit(struct_Hare a_data)
{
	m_Location_x = a_data.x;
	m_Location_y = a_data.y;
	m_OurLandscape = a_data.L;
	m_CurrentStateNo = 0;
	THareInit(a_data.x, a_data.y, a_data.HM);
	Init();
}
void Hare_Infant::Init()
{
	m_Type = hob_Infant;
	m_weight = -1; // just so we know we must set this somewhere else
	m_old_weight = 0;
	m_Age = 1; // We are 0 day old today
}
//---------------------------------------------------------------------------


Hare_Infant::~Hare_Infant()
{
    //Nothing to do
}
//---------------------------------------------------------------------------
/**
\brief
BeginStep for the Hare_Infant
*/
/**
Tests for mortality. Both standard mortality tests and optional density dependent mortality.
NB this differs from all other ALMaSS models in that the farm events do not have any direct impact on any hare stage except this one. I assume that all other stages can run away - seems not to cause a problem with fitting to Illum�
*/
void Hare_Infant::BeginStep()
{
  if (m_CurrentStateNo == -1) return;
  // These are all the same value - this may change to so I have kept all three
  // for now ver 00005
  m_KJWalking=m_OurPopulationManager->GetKJperM((int) m_weight);
  m_KJRunning=m_KJWalking*2;
  m_KJForaging=m_KJWalking;
  m_ActivityTime=1440; // Start the day
  m_old_weight=m_weight;
#ifdef __DDEPMORT
	int dens=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
#ifdef __THRESHOLD_DD
  if (dens<m_OurPopulationManager->m_HareThresholdDD) dens=0;
#endif
    double inter=m_OurPopulationManager->GetInterference(dens);
	if (g_rand_uni() > inter) {
			ON_Dead();
			m_StepDone=true; // We need to skip the step code, we are dead
			return;
	}
#endif
	if (g_rand_uni()<m_OurPopulationManager->m_YoungMortRate) {
    ON_Dead();
    m_StepDone=true; // We need to skip the step code, we are dead
  }

  if (m_MyMum==NULL) {
    ON_Dead();
    m_StepDone=true; // We need to skip the step code, we are dead
  }
  // Set out maximum intake possible
  CheckManagement();
}
//---------------------------------------------------------------------------
/**
\brief
Step for the Hare_Infant
*/
/**
	The step code is the main activity sub-stepfor each time-step.
	A dead animal will have m_CurrentStateNo set to -1
	It is essential that before an animal is killed that it has sent all
	the necessary messages to others (in this case to its mother). \n\n

   m_CurrentHState holds the current behavioural state
*/
void Hare_Infant::Step()
{
  if (m_StepDone || m_CurrentStateNo == -1) return;
  // The next line causes a jump to the correct behavioural state
  switch (m_CurrentHState)
  {
   case tohs_InitialState: // Initial state
    m_CurrentHState=tohs_Developing;
    break;
   case tohs_Developing:
     m_StepDone=true;
     break;
   case tohs_NextStage:
    // Legal returns are:
    // NONE
    st_NextStage();
    m_StepDone=true;
    break;
    case tohs_Running:
      m_CurrentHState=tohs_Developing;
      break;
    case tohs_Dying:
      m_StepDone=true;
      break;
   default:
    m_OurLandscape->Warn("Hare_Infant::Step - unknown state",NULL);
    exit(1);
   }
}
//---------------------------------------------------------------------------

/**
\brief
EndStep for the Hare_Infant
*/
void Hare_Infant::EndStep()
{
  if (m_CurrentHState==tohs_Developing) {
    // Legal returns are:
    // tohs_Developing
    // tohs_NextStage
    // tohs_Dying
    m_CurrentHState=st_Developing();
  }
  if (m_CurrentHState==tohs_Dying) {
    ON_Dead();
  } else MovePeg();
}
//---------------------------------------------------------------------------
/**
Checks to see if any nasty farm event has caused the death of the infant. \n
Currently there is only one response to those events where death is possible. However, this may not always be the case so the code is structured as for the other ALMaSS animals (i.e. it could be a bit easier to overview if it were written differently).
*/
bool Hare_Infant::OnFarmEvent( FarmToDo event )
{
	bool result=false;
  switch ( event )
  {
    case sleep_all_day:
    case glyphosate:
    break;
    case autumn_plough:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
	case stubble_plough:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
	case stubble_cultivator_heavy:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
	case heavy_cultivator_aggregate:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
    case autumn_harrow:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
	case preseeding_cultivator:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
	case preseeding_cultivator_sow:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
    case autumn_roll:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case autumn_sow:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case winter_plough:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case deep_ploughing:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case spring_plough:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case spring_harrow:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
	case shallow_harrow:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
    case spring_roll:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case spring_sow:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
	case spring_sow_with_ferti:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
    case fp_npks:
		break;
    case fp_npk:
		break;
    case fp_pk:
	case  fp_k:
	case  fp_p:
		break;
    case fp_liquidNH3:
		break;
    case fp_slurry:
		break;
	case fp_ammoniumsulphate:
		break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case fp_sludge:
    break;
	case fp_rsm:
		break;
	case fp_calcium:
		break;
	case fa_npks:
		break;
    case fa_npk:
    break;
    case fa_pk:
	case  fa_k:
	case  fa_p:
    break;
    case fa_slurry:
    break;
    case fa_ammoniumsulphate:
    break;
	case fa_manganesesulphate:
		break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
	case  fa_rsm:
		break;
	case  fa_calcium:
		break;
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
	case trial_insecticidetreat:
	case trial_toxiccontrol:
	case trial_control:
    case syninsecticide_treat:
    case insecticide_treat:
	case product_treat:
	case biocide:
    break;
    case molluscicide:
    break;
    case row_cultivation:
    break;
    case strigling:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
	case strigling_hill:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
    case hilling_up:
    break;
    case water:
    break;
    case swathing:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case harvest:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case cattle_out:
    break;
    case cattle_out_low:
    break;
    case cut_to_hay:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case cut_to_silage:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case straw_chopping:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case hay_turning:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case hay_bailing:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;

    case stubble_harrowing:
    break;
    case autumn_or_spring_plough:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case burn_straw_stubble:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case mow:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case cut_weeds:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
    case pigs_out:
      result=true;
    break;
    case strigling_sow:
      if ( random( 100 ) < cfg_hare_i_cut.value() ) result=true;
    break;
	case bed_forming:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
	case flower_cutting:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
	case bulb_harvest:
		if (random(100) < cfg_hare_i_cut.value()) result = true;
		break;
	case straw_covering:
		break;
	case straw_removal:
		break;
    default:
      m_OurLandscape->Warn( "Hare_Infant::OnFarmEvent(): Unknown event type:", m_OurLandscape->EventtypeToString( event ) );
      exit( 1 );
  }
  if (result) {
	  ON_Dead();
  }
  return result;
}
//---------------------------------------------------------------------------
/**
\brief
Developmental behaviour for the infant hare
*/
TTypeOfHareState Hare_Infant::st_Developing()
{
   /** This code must be called by the EndStep. \n
   To do this we need to add growth and energy requirements. The idea is to grow immediately on milk supply - handled via ON_someMilk()but if there are any other adjustments to be made they ought to be done here,
   e.g. if energy is needed for movement or thermoregululation. The easiest way to manage things is to use weight as a measure of energy, and convert freely between the two. This means that conversion efficiencies need to be applied to the energy inputs (done in on_BeingFed)
   */
   /** First remove our BMR */
   //int weight=(int) (floor(m_weight+0.5));
   double lost = m_OurPopulationManager->GetRMR(m_Age,GetTotalWeight()); // Uses age, weight and ambient temperature
   double eff=m_OurPopulationManager->GetGrowthEfficiency(m_Age);
   //** Then grow if possible, or shrink if -ve energy
   double gained=(m_TodaysEnergy-lost);
   if (gained>m_OurPopulationManager->GetMaxDailyGrowthEnergyP(m_Age)) gained = m_OurPopulationManager->GetMaxDailyGrowthEnergyP(m_Age);
   m_weight+=gained*eff;

   // We need to know whether the growth rate is positive or negative.
   // We could look at lost here, but it is probably easier to use the same
   // approach for Young as well, and they need the oldweight variable, so might as well use this.
#ifdef __MINGROWTHATTAIN
   // Take a check on our growth - if we are below X% of the max expected weight then die
   if (m_weight< m_OurPopulationManager->m_DMWeight[m_Age]) return tohs_Dying;
#else
   if (m_weight<m_old_weight) {
	   m_StarvationDays++;
   }
   else m_StarvationDays=0;
   /**
   Must die if starvation threshold is exceeded
   */
   if (m_StarvationDays>cfg_infant_starvation_threshold.value()) {
     return tohs_Dying;
   }
#endif

   /** Make sure m_TodaysEnergy is zero before we run the risk of getting milk from Mum again during Step tomorrow. */
   m_TodaysEnergy=0;
	/** If all is well then mature to become a Hare_Young */
	if (++m_Age>10) return tohs_NextStage;
	return tohs_Developing;
}
//---------------------------------------------------------------------------
/**
\brief 'mature' to become a young
*/
void Hare_Infant::st_NextStage()
{
  /**
  This creats a Hare_Young object via a call to create objects then sets the flag for destruction of this object.
  */
  struct_Hare* sp;
  sp = new struct_Hare;
  sp->HM = m_OurPopulationManager;
  sp->L = m_OurLandscape;
  sp->x = m_Location_x;
  sp->y = m_Location_y;
  sp->weight = m_weight;
  sp->age = m_Age;
  // Updating of Mum is done through the population manager
  sp->Mum = m_MyMum;
  // *** Debug ***
  //sp->oldMum = m_MyOldMum;
  //
  m_OurPopulationManager->CreateObjects(1,this,NULL,sp,1);
  // Clean-up
  m_CurrentStateNo=-1;  // Destroys the object at the next opportunity
  m_CurrentHState=tohs_DestroyObject;
  m_MyMum=NULL;
  delete sp;
}
//---------------------------------------------------------------------------
/**
Do the housekeeping necessary before dying
*/
void Hare_Infant::ON_Dead()
{
	st_Dying();
}
//---------------------------------------------------------------------------

/**
\brief
Get energy from milk given
*/
void Hare_Infant::ON_BeingFed(double a_someMilk)
{
   /** The conversion efficiency is pre-calcualted in GetgperKJ so this function is quite simply a conversion from KJ to g\n
   NOTE There are no limits here - the limit to growth has to be by the limited amount of milk supplied - the ultimate limit to this has got
   to be defined somewhere else (e.g. female forage).
   */
   m_TodaysEnergy+=a_someMilk;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//       Hare_Young CODE
//---------------------------------------------------------------------------

/**
\brief
Constructor for Hare_Young
*/
Hare_Young::Hare_Young(int p_x, int p_y,
             Landscape * p_L, THare_Population_Manager* p_PPM, double p_weight)
                                          : Hare_Infant(p_x,p_y,p_L,p_PPM)
{
	Init(p_weight);
}
void Hare_Young::ReInit(struct_Hare a_data)
{
	m_Location_x = a_data.x;
	m_Location_y = a_data.y;
	m_OurLandscape = a_data.L;
	m_CurrentStateNo = 0;
	THareInit(a_data.x, a_data.y, a_data.HM);
	Init(a_data.weight);
}
//---------------------------------------------------------------------------

void Hare_Young::Init(double p_weight)
{
	m_Type = hob_Young;
	m_weight = p_weight;
	m_Age = 11; // By definition we are 11 days old today
}

/**
\brief
Destructor for Hare_Young
*/
Hare_Young::~Hare_Young()
{
    //Nothing to do
}
//---------------------------------------------------------------------------

/**
\brief
BeginStep for Hare_Young
*/
void Hare_Young::BeginStep()
{
  if (m_CurrentStateNo == -1) return;
  m_old_weight=m_weight;
  // These are all the same value - this may change to so I have kept all three
  // for now ver 00005
  m_KJWalking=m_OurPopulationManager->GetKJperM((int) floor(0.5+m_weight));
  m_KJRunning=m_KJWalking*2;
  m_KJForaging=m_KJWalking;
	/**
	Resets the day's activity counter and checks for default mortalities and potentially extra mortalities.
	Calculates energy usage for movement based on todays weight
	*/
	m_ActivityTime=1440; // Start the day
#ifdef __DDEPMORT
	int dens=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
#ifdef __THRESHOLD_DD
  if (dens<m_OurPopulationManager->m_HareThresholdDD) dens=0;
  else dens-=m_OurPopulationManager->m_HareThresholdDD;
#endif
    double inter=m_OurPopulationManager->GetInterference(dens);
	if (g_rand_uni() > inter) {
			ON_Dead();
			m_StepDone=true; // We need to skip the step code, we are dead
			return;
	}
#endif
	if (g_rand_uni()<m_OurPopulationManager->m_YoungMortRate) {
    ON_Dead();
    m_StepDone=true; // We need to skip the step code, we are dead
    return;
  }
  // Set the maximum possible energy intake
  m_EnergyMax = GetRMR()*cfg_MaxEnergyIntakeScaler.value();
  // Move the hares about if they get scared by a random proximity event
  if (g_rand_uni()<cfg_hare_proximity_alert.value()) {
	  Running(cfg_hare_escape_dist.value());
  }
  CheckManagement();

}
//---------------------------------------------------------------------------

/**
\brief
Step code for Hare_Young
*/
void Hare_Young::Step()
{
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentHState)
  {
   case tohs_InitialState: // Initial state
    m_CurrentHState=tohs_Dispersal;
    break;
    case tohs_Dispersal:
     // Legal returns are:
     // tohs_YResting
     // tohs_YForaging
     m_CurrentHState=st_Dispersal();
	 break;
    case tohs_Foraging:
    // Legal returns are:
    // tohs_YResting
     m_CurrentHState=st_Foraging();
     break;
    case tohs_Resting:
     // Legal returns are:
     // tohs_YDeveloping
     m_CurrentHState=st_Resting();
     break;
    case tohs_Developing:
     m_StepDone=true;
     break;
    case tohs_NextStage:
     // Legal returns are:
     // NONE
     st_NextStage();
     m_StepDone=true;
     break;
   case tohs_Dying:
     m_StepDone=true;
     break;
   default:
    m_OurLandscape->Warn("Hare_Young::Step - unknown state",NULL);
    exit(1);
   }
}
//---------------------------------------------------------------------------
/**
\brief
EndStep code for Hare_Young
*/
/**
NB development must be called here because they hares may be still getting milk from Mum.\n
In EndStep, the home-range centre peg gravitates a little way towards the daily activity area for today.
*/
void Hare_Young::EndStep()
{
    if (cfg_hare_pesticideresponse_on.value()) InternalPesticideHandlingAndResponse();
    if (m_CurrentHState==tohs_Developing) {
    // Legal returns are:
    // tohs_Developing
    // tohs_NextStage
    // tohs_Dying
    m_CurrentHState=st_Developing();
  }
  if (m_CurrentHState==tohs_Dying) {
    ON_Dead();
  } else MovePeg();
}
//---------------------------------------------------------------------------

   /**
   The amount of time spent on this is dependent upon age.
   They can take in food up to a proportion of their needs increasing with
   age. This gives us a little problem because we don't explicitly define their
   needs. We assume therefore that the milk supply is maximal and that the
   the time available is all day - but that the max solid food intake possible is
   defined by age, following the data from Hacklander - this gives no flexibility in
   the proportion of max food but will cause differential growth w.r.t. milk supply.
   */
TTypeOfHareState Hare_Young::st_Foraging()
{

   double time=m_ActivityTime;
   time*=cfg_ForageRestingRatio.value()*g_PropSolidFood[m_Age];
   // We need to rest if there is no more time
   if (time < 30) return tohs_Resting;
   //time+=m_TimePerForageSquare;
   // time now has the time to use foraging
   // Given the time we have then we need to feed, which will return an energy
   // Value obtained.
   // m_TodaysEnergy is zero on entry, except in the case where the young were
   // created today. So we can't use m_TodaysEnergy directly
   int a_time = (int) time;
   double ForageEnergy;
   if (cfg_hare_pesticideresponse_on.value())
   {
	   ForageEnergy = ForageP(a_time);
   }
   else
   {
	   ForageEnergy = Forage(a_time);
   }
   // We need to spend energy on foraging and associated movement
   //
   TimeBudget(activity_Foraging, (int)time-a_time);
   //
   double Target=(m_OurPopulationManager->GetRMR(m_Age, GetTotalWeight()) + g_MaxLeveretGrowthEnergy[m_Age])*g_PropSolidFood[m_Age];
   // If we got too much then cap it
   if (ForageEnergy>Target) ForageEnergy=Target;
#ifdef __NOSTARVE
   ForageEnergy=Target;
#endif
   m_TodaysEnergy+=ForageEnergy;// pre-calculated in the ForageEnergy // *cfg_solidAsbsorption.value();
   return tohs_Resting;
}
//---------------------------------------------------------------------------
/**
Unused at present
*/
TTypeOfHareState Hare_Young::st_Dispersal()
{
   // This behavioural state is not used at present
   return tohs_Foraging;
}
//---------------------------------------------------------------------------

/**
Just uses some time and energy sitting around. The time used is important because it removes time for foraging.
*/
TTypeOfHareState Hare_Young::st_Resting()
{
   // TODO Need to find a position - use some energy walking too?
   EnergyBalance(activity_Resting, 0); // Just removes a days BMR
   TimeBudget(activity_Resting, m_ActivityTime);
   return tohs_Developing;
}
//---------------------------------------------------------------------------
/**
\brief
Developmental code for the young hare
*/
TTypeOfHareState Hare_Young::st_Developing()
{
  // This code must be called by the EndStep
  // NB foraged food and milk are in the same energetic currency
  double me=m_OurPopulationManager->GetMaxDailyGrowthEnergyP(m_Age);
  if (m_TodaysEnergy>me) m_TodaysEnergy=me;
  double growth=m_TodaysEnergy*m_OurPopulationManager->GetGrowthEfficiencyP(m_Age);
  m_weight+=growth;
  // We need to know whether the growth rate is positive or negative
//#ifndef __MINGROWTHATTAIN
  if (m_weight<m_old_weight) m_StarvationDays++; else m_StarvationDays=0;
  if (m_StarvationDays>cfg_young_starvation_threshold.value()) {
    return tohs_Dying;
  }
//#endif
  // Make sure m_TodaysEnergy is zero;
  m_TodaysEnergy=0;
  if (++m_Age>34) {
	  return tohs_NextStage;
  }
#ifdef __MINGROWTHATTAIN
		// Take a check on our growth - if we are below 90% of the min repro weight, assume we died
		//if (m_weight < (cfg_hare_minimum_breeding_weight.value()*cfg_min_growth_attain.value())) return tohs_Dying;
		if (m_weight< m_OurPopulationManager->m_DMWeight[m_Age]) return tohs_Dying;
#endif
  return tohs_Foraging;
}
//---------------------------------------------------------------------------
/**
\brief
Young 'matures' to become a Hare_Juvenile
*/
void Hare_Young::st_NextStage()
{
  /**
  This creats a Hare_Juvenile object via a call to create objects,
  then sets the flag for destruction of this object.
  */
  struct_Hare* sp;
  sp = new struct_Hare;
  sp->HM = m_OurPopulationManager;
  sp->L = m_OurLandscape;
  sp->x = m_Location_x;
  sp->y = m_Location_y;
  sp->weight = m_weight;
  // Mum no longer cares about the young, it is too old
  // Removal of the young from Mum's list
  if (m_MyMum!=NULL) m_MyMum->ON_RemoveYoung(this);
  // If m_MyMum is NULL then it means that she deserted the young but they survived anyway
  m_MyMum=NULL;
  m_OurPopulationManager->CreateObjects(2,this,NULL,sp,1);
  // Clean-up
  m_CurrentStateNo=-1;  // Destroys the object at the next opportunity
  m_CurrentHState=tohs_DestroyObject;
  delete sp;
}
//---------------------------------------------------------------------------
/**
Checks to see if any nasty farm event has caused the death of the infant. \n
Currently there is only one response to those events where death is possible. However, this may not always be the case so the code is structured as for the other ALMaSS animals (i.e. it could be a bit easier to overview if it were written differently).
*/
bool Hare_Young::OnFarmEvent( FarmToDo event )
{
	bool result = false;
	switch (event)
	{
		case sleep_all_day:
		case glyphosate:
			break;
		case autumn_plough:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case stubble_plough:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case stubble_cultivator_heavy:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case heavy_cultivator_aggregate:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case autumn_harrow:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case preseeding_cultivator:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case preseeding_cultivator_sow:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case autumn_roll:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case autumn_sow:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case winter_plough:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case deep_ploughing:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case spring_plough:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case spring_harrow:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case shallow_harrow:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case spring_roll:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case spring_sow:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case spring_sow_with_ferti:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case fp_npks:
			break;
		case fp_npk:
			break;
		case fp_pk:
		case  fp_k:
		case  fp_p:
			break;
		case fp_liquidNH3:
			break;
		case fp_slurry:
			break;
		case  fp_ammoniumsulphate:
			break;
		case fp_manganesesulphate:
			break;
		case fp_manure:
			break;
		case fp_greenmanure:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case fp_sludge:
			break;
		case  fp_rsm:
			break;
		case  fp_calcium:
			break;
		case fa_npk:
			break;
		case fa_npks:
			break;
		case fa_pk:
		case  fa_k:
		case  fa_p:
			break;
		case fa_slurry:
			break;
		case fa_ammoniumsulphate:
			break;
		case  fa_manganesesulphate:
			break;
		case fa_manure:
			break;
		case fa_greenmanure:
			break;
		case fa_sludge:
			break;
		case  fa_rsm:
			break;
		case  fa_calcium:
			break;
		case herbicide_treat:
			break;
		case growth_regulator:
			break;
		case fungicide_treat:
			break;
		case trial_insecticidetreat:
		case trial_toxiccontrol:
		case trial_control:
		case syninsecticide_treat:
		case insecticide_treat:
		case product_treat:
			break;
		case biocide:
			break;
		case molluscicide:
			break;
		case row_cultivation:
			break;
		case strigling:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case hilling_up:
			break;
		case water:
			break;
		case swathing:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case harvest:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case cattle_out:
			break;
		case cattle_out_low:
			break;
		case cut_to_hay:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case cut_to_silage:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case straw_chopping:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case hay_turning:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case hay_bailing:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case stubble_harrowing:
			break;
		case autumn_or_spring_plough:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case burn_straw_stubble:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case mow:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case cut_weeds:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case pigs_out:
			result = true;
			break;
		case strigling_sow:
			if (random( 100 ) < cfg_hare_y_cut.value()) result = true;
			break;
		case strigling_hill:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case bed_forming:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case flower_cutting:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case bulb_harvest:
			if (random(100) < cfg_hare_y_cut.value()) result = true;
			break;
		case straw_covering:
			break;
		case straw_removal:
			break;
		default:
			m_OurLandscape->Warn( "Hare_Young::OnFarmEvent(): Unknown event type:", m_OurLandscape->EventtypeToString( event ) );
			exit( 1 );
	}
	if (result)
	{
		ON_Dead();
	}
	return result;
}
//---------------------------------------------------------------------------
/**
Housekeeping for removal of the object, lets Mum know if there is a Mum
*/
void Hare_Young::ON_Dead()
{
	st_Dying();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//       Hare_Juvenile CODE
//---------------------------------------------------------------------------

/**
\brief
Constructor for the juvenile hare object
*/
Hare_Juvenile::Hare_Juvenile(int p_x, int p_y,
             Landscape * p_L, THare_Population_Manager* p_PPM, double p_weight)
                                          : THare(p_x,p_y,p_L,p_PPM)
{
	Init(p_weight);
}
void Hare_Juvenile::ReInit(struct_Hare a_data)
{
	m_Location_x = a_data.x;
	m_Location_y = a_data.y;
	m_OurLandscape = a_data.L;
	m_CurrentStateNo = 0;
	THareInit(a_data.x, a_data.y, a_data.HM);
	Init(a_data.weight);
}
//---------------------------------------------------------------------------

void Hare_Juvenile::Init(double p_weight)
{
	m_Type = hob_Juvenile;
	m_weight = p_weight;
	m_Age = 35; // By definition we are 35 days old today
}

/**
\brief
Destructor for the juvenile hare object
*/
Hare_Juvenile::~Hare_Juvenile()
{
    //Nothing to do
}
//---------------------------------------------------------------------------

/**
\brief
BeginStep for Hare_Juvenile
*/
/**
Resets the day's activity counter and checks for default mortalities and potentially extra mortalities.
Calculates energy usage for movement based on todays weight
*/
void Hare_Juvenile::BeginStep()
{
  if (m_CurrentStateNo == -1) return;
  m_ActivityTime=1440; // Start the day
  m_old_weight=m_weight-1; // we are not interested in tiny fluctuations
  m_KJWalking=m_OurPopulationManager->GetKJperM((int)m_weight);
  m_KJRunning=m_KJWalking*2;
  m_KJForaging=m_KJWalking;

#ifdef __DDEPMORT
	int dens=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
#ifdef __THRESHOLD_DD
  if (dens<m_OurPopulationManager->m_HareThresholdDD) dens=0;
  else dens-=m_OurPopulationManager->m_HareThresholdDD;
#endif
    double inter=m_OurPopulationManager->GetInterference(dens);
	if (g_rand_uni() > inter) {
			ON_Dead();
			m_StepDone=true; // We need to skip the step code, we are dead
			return;
	}
#endif

	if (g_rand_uni()<m_OurPopulationManager->m_JuvMortRate) {
    ON_Dead();
    m_StepDone=true; // We need to skip the step code, we are dead
    return;
  }

  if (g_rand_uni()<cfg_hare_proximity_alert.value()) {
    Running(cfg_hare_escape_dist.value());
  }
#ifdef __DISEASEDDM3
	m_ddindex++;
	if (m_ddindex==365) {
		m_ddindex=0;
	}
	m_DensitySum-=m_expDensity[m_ddindex];
	m_expDensity[m_ddindex]=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
	m_DensitySum+=m_expDensity[m_ddindex];
	m_lastYearsDensity= int(m_DensitySum * (1.0/365.0));
#endif
	// Set the maximum possible energy intake
	m_EnergyMax = GetRMR()*cfg_MaxEnergyIntakeScaler.value();
	CheckManagement();
}
//---------------------------------------------------------------------------

/**
\brief
Step for Hare_Juvenile
*/
/**
As with all other ALMaSS animal objects, Step is where the primary state/transtion work is done
*/
void Hare_Juvenile::Step()
{
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentHState)
  {
   case tohs_InitialState: // Initial state
    m_CurrentHState=tohs_Dispersal;
    break;
    case tohs_Dispersal:
   // Dispersal is risky therefore take a test again
     // Legal returns are:
     // tohs_Foraging
#ifdef __DISPERSALDDM
	if (g_rand_uni()<(cfg_hare_juvenile_predation.value())) {
		ON_Dead();
		m_StepDone=true; // We need to skip the step code, we are dead
		return;
	}
#endif
m_CurrentHState=st_Dispersal();
     break;
    case tohs_Foraging:
    // Legal returns are:
    // tohs_JResting
     m_CurrentHState=st_Foraging();
	 break;
    case tohs_Resting:
     // Legal returns are:
     // tohs_Developing
     m_CurrentHState=st_Resting();
     break;
    case tohs_Developing:
     // Legal returns are:
     // tohs_Dispersal
     // tohs_NextStage
     // tohs_Resting
     m_CurrentHState=st_Developing();
     if (m_CurrentHState==tohs_Foraging) m_StepDone=true;
     break;
    case tohs_NextStage:
     // Legal returns are:
     // NONE
     st_NextStage();
     m_StepDone=true;
     break;
   case tohs_Dying:
     ON_Dead();
     m_StepDone=true;
     break;
   default:
    m_OurLandscape->Warn("Hare_Juvenile::Step - unknown state",NULL);
    exit(1);
   }
}
//---------------------------------------------------------------------------

/**
\brief
BeginStep for Hare_Juvenile
*/
/**
Just moves the homerange centre peg closer to where the hare has been active today
*/
void Hare_Juvenile::EndStep()
{
	MovePeg();
	if (cfg_hare_pesticideresponse_on.value()) InternalPesticideHandlingAndResponse();
}
//---------------------------------------------------------------------------
TTypeOfHareState Hare_Juvenile::st_Foraging()
{
  /**
  Given the time we have then we need to feed, which will return an energy value obtained.
  */
  double atime= m_ActivityTime*cfg_ForageRestingRatio.value();
  // We need to rest if there is no more time
  if (atime < 30) return tohs_Resting;
  // This version uses adult density for interference, other densities seem not to work so well
  //int hares=(m_OurPopulationManager->GetAdultDensity(m_Location_x, m_Location_y));
  //int hares=(m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y));
  int hares = 1;
#ifdef __THRESHOLD_AD_DD
  if (hares<m_OurPopulationManager->m_HareThresholdDD) {
	  hares=0;
  } else {
	  hares-=m_OurPopulationManager->m_HareThresholdDD;
  }
#endif
#ifdef __THRESHOLD_DDJ
// this section has been altered so many times it was not worth making it general - if it is used in release versions this can be implemented when we know how it should look - right now remember to change in code before compile-
  if (hares<6) hares=0;
	else hares*=hares;
#endif

#ifdef __SCALINGYOUNGDDM
  hares = (int) floor (0.5+(double)hares*cfg_JuvDDScale.value());
#endif

  //interfer is between 1.0 and 0
  //double slope = 1.0+(cfg_AgeRelatedInterferenceScaling.value() *  (1.0-(double(m_Age/365.0))));
  //double hs=hares*slope;
#ifndef __DDEPMORT
  double inter = m_OurPopulationManager->GetInterference((int)(hares));
#else
  double inter=1.0;
#endif
  if ((inter<0.5) && (atime > 300)){
	  if (g_rand_uni() > inter) // added to reduce rate of dispersal
	  {
		  //m_ActivityTime = (int)(atime / cfg_ForageRestingRatio.value());
		  return tohs_Dispersal; // Added Sat 15th Dec 2007
	  }
  }
  atime*= inter;
  int a_time = (int) atime;
  if (cfg_hare_pesticideresponse_on.value())
  {
	  m_TodaysEnergy = ForageP(a_time);
  }
  else
  {
	  m_TodaysEnergy = Forage(a_time);
  }
  // We need to spend energy on foraging and associated movement
  //
  TimeBudget(activity_Foraging, (int)atime-a_time);
  return tohs_Resting;
}
//---------------------------------------------------------------------------

/**
Makes a test of habitat quality (basically food) in 8 directions, then moves to the best one. This is basically a teleport move, so testing for barriers is not done. This will need to be added if it becomes a problem (e.g. hares appearing on islands they could not otherwise get too), however, this will also confer an extreme performance penalty!
*/
TTypeOfHareState Hare_Juvenile::st_Dispersal()
{
#ifdef __DISPMORTALITY
  if (WasPredated()) return tohs_Dying;
#endif
  // Now to make this a little more clever we need to test all 8 directions
  int ttx, tty;
  int tx=m_Location_x;
  int ty=m_Location_y;
  ttx=tx;
  tty=ty;
  double fv=0.0;
  double oldfv = -99999999.0;
  int dstart=random(8);
  int d2=0;
  for (int j=0; j<100; j++) {
	  int dist=100+random(cfg_hare_max_dispersal.value()); 
	  for (int d=dstart; d<8+dstart; d++) {
		// We need to avoid a bias movement now too
		int d1=d & 7; // 0-7
		Walking(dist, d1); // This modifies m_Location_x, m_Location_y
		// How much food here?
		int polyref=m_OurLandscape->SupplyPolyRef(m_Location_x,m_Location_y);
		fv = m_OurLandscape->GetHareFoodQuality(polyref) * m_vegPalatability[m_OurLandscape->SupplyVegType(polyref)] * m_OurPopulationManager->GetInterference((m_OurPopulationManager->GetAdultDensity(m_Location_x, m_Location_y)));
		if (oldfv<=fv) {
			oldfv=fv;
			ttx=m_Location_x;
			tty=m_Location_y;
			d2=dist;
			// The best we can ever get is 0.8 so we can speed things up by dropping out if we are close to this.
			if (fv>=0.75) {
				j=101;
				break;
			}
		}
		// Set the xy back before trying the next direction
		m_Location_x=tx;
		m_Location_y=ty;
	  }
  }
  // Now do it for real
  // We need to update the density maps temporarily to stop all the animals responding to old data until the beginning of the next timestep
  m_OurPopulationManager->SubtractHareDensity(m_Location_x, m_Location_y, m_Type);
  m_Location_x = ttx;
  m_Location_y = tty;
  m_OurPopulationManager->AddHareDensity(m_Location_x, m_Location_y, m_Type);
  d2 *= 5; // Lets assume it is not a beeline, and 5x distance is needed to find the place.
  EnergyBalance(activity_Dispersal, d2);
  TimeBudget(activity_Dispersal,d2 );
  // Move the peg with us.
  //m_peg_x = m_Location_x; /** Testing whether the peg will be dragged anyway if we don't set this 31/7/2014 */
  //m_peg_y = m_Location_y;
  return tohs_Foraging;
}
//---------------------------------------------------------------------------
/**
The hare spends some time resting (using time and a little energy)
*/
TTypeOfHareState Hare_Juvenile::st_Resting()
{
  EnergyBalance(activity_Resting, 0); // Just removes a days BMR
  TimeBudget(activity_Resting, m_ActivityTime);
  return tohs_Developing;
}
//---------------------------------------------------------------------------
/**
\brief
The development code for Hare_Juvenile
*/
/**
Tests for the onset of maturation. Also tests for minimum growth attainment if this is switched on - too low growth results in death.\n

*/
TTypeOfHareState Hare_Juvenile::st_Developing()
{
	// Test for the onset of reproductive activity
	// This should be done now because otherwise it is possible to skip this test and get too old
	if (ShouldMature()) {
		return tohs_NextStage;
	}
#ifdef __MINGROWTHATTAIN
		// Take a check on our growth - if we are below X% of the min repro weight, assume we died
		//if (m_weight < (cfg_hare_minimum_breeding_weight.value()*cfg_min_growth_attain.value())) return tohs_Dying;
		if (m_weight< m_OurPopulationManager->m_DMWeight[m_Age]) {
			return tohs_Dying;
		}
#endif

/**
	Assumes that the fatReserve has been added to by st_Foraging from yesterday.\n
	This is the last behaviour state called each day, so we can use this to sort out
	the energetics.
	 */
	double Target=m_OurPopulationManager->GetMaxDailyGrowthEnergyP(m_Age);
	if (m_TodaysEnergy<Target) Target=m_TodaysEnergy;
	double addedtoday=Target*m_OurPopulationManager->GetGrowthEfficiencyP(m_Age);
	m_weight+=addedtoday;
	m_TodaysEnergy-=Target;
	Target=m_OurPopulationManager->GetMaxDailyGrowthEnergyF(m_Age);
	if (m_TodaysEnergy<Target) Target=m_TodaysEnergy;
	addedtoday=Target*m_OurPopulationManager->GetGrowthEfficiencyF(m_Age);
	m_weight+=addedtoday;
	m_TodaysEnergy-=Target;
 if (m_TodaysEnergy>0) {
		// Put what remains back into fat reserve
		m_fatReserve+=m_TodaysEnergy*m_OurPopulationManager->GetGrowthEfficiencyF(366); // 366 is the fat conversion efficiency
		m_TodaysEnergy=0;
		// Cap the fat reserve and store surplus energy for today
		if (m_fatReserve>(cfg_AdultMaxFat.value() * m_weight)) {
			m_fatReserve=cfg_AdultMaxFat.value() * m_weight;
		}
		m_StarvationDays=0; // Not starving
		return tohs_Foraging;
	}
	else {
			m_fatReserve+=m_TodaysEnergy*m_OurPopulationManager->GetGrowthEfficiencyF(366); // 366 is the fat conversion efficiency
			if (m_fatReserve<0) {
				m_TodaysEnergy=m_fatReserve*m_OurPopulationManager->GetGrowthEfficiencyF(366);
				m_fatReserve=0.0;
			}
			else m_TodaysEnergy=0;
			// What should the criteria for a starvation day be? Here chosen to be any negative energy balance.
			// Have also considered and rejected a proportion of rmr:
			//double rmr=m_OurPopulationManager->GetRMR(m_Age, GetTotalWeight());
//			if (m_TodaysEnergy<(0.0-(rmr*0.25)))
			if (m_TodaysEnergy<0.0)
			{
#ifndef __NOJUVSTARVE
				// Oh oh, we are starving need to lose body weight and maybe we will die?
#ifdef __ADULT_WT_STARVE_CHANCE
				if (++m_StarvationDays > cfg_juvenile_starvation_threshold.value()) {
					if ((g_rand_uni() * 10000) > (cfg_juv_starve.value()))
					{
						return tohs_Dying;
					}
				}
#else
				if (++m_StarvationDays > cfg_juvenile_starvation_threshold.value()) {
					return tohs_Dying;
				}
#endif
#endif
			}
			else {
				m_StarvationDays--; // Not starving
				if (m_StarvationDays<0) m_StarvationDays=0;
				m_TodaysEnergy=0;
			}
	}
	return tohs_Foraging;

}
//---------------------------------------------------------------------------

/**
  This creates a Hare_Juvenile object via a call to create objects,
  then sets the flag for destruction of this object.
*/
void Hare_Juvenile::st_NextStage()
{
  struct_Hare* sp;
  sp = new struct_Hare;
  sp->HM = m_OurPopulationManager;
  sp->L = m_OurLandscape;
  sp->x = m_Location_x;
  sp->y = m_Location_y;
  sp->weight = m_weight;
  sp->Mum = NULL;
  sp->age = m_Age;
  sp->RefNum = m_RefNum;
  // Sex ratio is set to be 50:50 at birth
  if ((m_RefNum & 0x01) == 0)  // Male
                       m_OurPopulationManager->CreateObjects(3,this,NULL,sp,1);
  else m_OurPopulationManager->CreateObjects(4,this,NULL,sp,1);
  // Clean-up
  m_CurrentStateNo=-1;  // Destroys the object at the next opportunity
  m_CurrentHState=tohs_DestroyObject;
  delete sp;
}
//---------------------------------------------------------------------------

bool Hare_Juvenile::ShouldMature()
{
   /**
   This method will be used to determine whether the hare becomes sexually
   active - and therefore needs to become an adult hare.
   */
   if (++m_Age<180) return false;
   // Do not do it below six months
   // If 6mths old and it is before October, then can mature
   if (m_OurLandscape->SupplyDayInYear() < 270) return true;
   if (m_Age>364) return true;
   return true;
}
//---------------------------------------------------------------------------

bool Hare_Juvenile::WasPredated() {
		if (g_rand_uni()<m_OurPopulationManager->m_JuvMortRate) return true;
		return false;
}
//---------------------------------------------------------------------------

/**
Do the housekeeping necessary for removal of the object - in this case minimal.
*/
void Hare_Juvenile::ON_Dead()
{
   m_CurrentHState=tohs_DestroyObject;
   m_CurrentStateNo=-1;
   m_StepDone=true;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//       Hare_Male CODE
//---------------------------------------------------------------------------

/**
\brief Hare_Male constructor
*/
Hare_Male::Hare_Male(int p_x, int p_y,
               Landscape * p_L, THare_Population_Manager* p_PPM,
                        double p_weight, int a_age, int a_Ref)
                             : Hare_Juvenile(p_x,p_y,p_L,p_PPM,p_weight)
{
	Init(p_weight, a_age, a_Ref);
}
void Hare_Male::ReInit(struct_Hare a_data)
{
	m_Location_x = a_data.x;
	m_Location_y = a_data.y;
	m_OurLandscape = a_data.L;
	m_CurrentStateNo = 0;
	THareInit(a_data.x, a_data.y, a_data.HM);
	Init(a_data.weight, a_data.age, a_data.RefNum);
}
//---------------------------------------------------------------------------

void Hare_Male::Init(double p_weight, int a_age, int a_Ref)
{
	m_Type = hob_Male;
	m_fatReserve = p_weight*0.04;
	m_weight = p_weight;
	m_Age=a_age;
	m_RefNum=a_Ref;
}
//---------------------------------------------------------------------------

/**
\brief Hare_Male destructor
*/
Hare_Male::~Hare_Male()
{
  // Nothing to do
}
//---------------------------------------------------------------------------

/**
\brief
BeginStep for Hare_Male
*/
/**
Resets the day's activity counter and checks for default mortalities and potentially extra mortalities.
Calculates energy usage for movement based on todays weight. \n
Also contains optional code designed to test for disease and size related death probabilities - part of the POM exercise
\n Unlike younger classes this method also tests for reaching the end of physiological lifespan
*/
void Hare_Male::BeginStep()
{
  m_ActivityTime=1440; // Start the day
  m_KJWalking=m_OurPopulationManager->GetKJperM((int)m_weight);
  m_KJRunning=m_KJWalking*2;
  m_KJForaging=m_KJWalking;
  m_foragingenergy=0;
#ifdef __SIZERELATEDDEATH
	if ((m_OurLandscape->SupplyDayInYear()>0) && ((m_OurLandscape->SupplyDayInYear()<90))) {
		if (m_Age>365) {
			if (m_weight<cfg_hare_minimum_breeding_weight.value()) {
				ON_Dead();
				m_StepDone=true; // We need to skip the step code, we are dead
				return;
			}
		}
	}
#endif
#ifdef __DISEASEDDMORTALITY
	if (m_IamSick) {
		ON_Dead();
		m_StepDone=true; // We need to skip the step code, we are dead
		return;
	}
#endif
	if (WasPredated()) {
		ON_Dead();
		m_StepDone=true; // We need to skip the step code, we are dead
		return;
	}
  // Age physiolocally
  if (++m_Age > m_Lifespan) {
    ON_Dead();
    m_StepDone=true; // We need to skip the step code, we are dead
    return;
  }
#ifdef __DISEASEDDM
	m_experiencedDensity+=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
	if (m_OurLandscape->SupplyDayInYear()==0) {
		m_lastYearsDensity=m_experiencedDensity/365;
		m_experiencedDensity=0;
		// Now assume that if there are 100 hares on average that there is 100% chance of getting sick
		if (random(cfg_HareFemaleSicknessDensityDepValue.value())<m_lastYearsDensity) {
			m_IamSick=true;
		}
		else m_IamSick=false;
	}
#endif
  if (g_rand_uni()<cfg_hare_proximity_alert.value()) {
    Running(cfg_hare_escape_dist.value());
  }

#ifdef __DISEASEDDM2
	m_experiencedDensity+=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
	if (m_OurLandscape->SupplyDayInYear()==0) {
		m_lastYearsDensity=m_experiencedDensity/365;
		m_experiencedDensity=0;
	}
#endif
#ifdef __DISEASEDDM3
	m_ddindex++;
	if (m_ddindex==365) {
		m_ddindex=0;
	}
	m_DensitySum-=m_expDensity[m_ddindex];
	m_expDensity[m_ddindex]=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
	m_DensitySum+=m_expDensity[m_ddindex];
	m_lastYearsDensity= int(m_DensitySum * (1.0/365.0));
#endif
	// Set the maximum possible energy intake
	m_EnergyMax = GetRMR()*cfg_MaxEnergyIntakeScaler.value();
	CheckManagement();
}
//---------------------------------------------------------------------------

/**
\brief
Step for Hare_Male
*/
/**
Step controls all the state/transition behaviour for the male hare.\n
Has some optional code for dispersal related mortality for use in POM testing.
*/
void Hare_Male::Step()
{
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentHState)
  {
   case tohs_InitialState: // Initial state
    m_CurrentHState=tohs_Foraging;
    break;
    case tohs_Dispersal:
     // Legal returns are:
     // tohs_MResting
     // tohs_MReproBehaviour
   // Dispersal is risky therefore take a test again
#ifdef __DISPERSALDDM
  if (g_rand_uni()<(cfg_hare_adult_predation.value())) {
		ON_Dead();
		m_StepDone=true; // We need to skip the step code, we are dead
		return;
	}
#endif
     m_CurrentHState=st_Dispersal();
     break;
    case tohs_Foraging:
    // Legal returns are:
    // tohs_MDispersal
    // tohs_MResting
    // tohs_MReproBehaviour
     m_CurrentHState=st_Foraging();
     break;
    case tohs_Resting:
     // Legal returns are:
     // tohs_MDeveloping
     m_CurrentHState=st_Resting();
     break;
    case tohs_ReproBehaviour:
     // Legal returns are:
     // tohs_MResting
     // tohs_MDeveloping
     m_CurrentHState=st_ReproBehaviour();
     break;
    case tohs_Developing:
     // Legal returns are:
     // tohs_Dying
     // tohs_MForaging
     m_CurrentHState=st_Developing();
     m_StepDone=true;
     break;
   case tohs_Dying:
    ON_Dead();
    m_StepDone=true;
    break;
   default:
    m_OurLandscape->Warn("Hare_Male::Step - unknown state",NULL);
    exit(1);
   }
}
//---------------------------------------------------------------------------
/**
\brief
EndStep code for Hare_Male
*/
void Hare_Male::EndStep()
{
	/**
	In EndStep, the home-range centre peg gravitates a little way towards the daily activity area for today.
	*/
	MovePeg();
	if (cfg_hare_pesticideresponse_on.value()) InternalPesticideHandlingAndResponse();
}
//---------------------------------------------------------------------------
/**
\brief
Foraging code for the Hare_Male
*/
TTypeOfHareState Hare_Male::st_Foraging()
{
	/**
	Foraging time is reduced dependent upon the density of hares in the local area. There are a number of options for calculting this effect used in the POM
	*/
	double time = (double)m_ActivityTime*cfg_ForageRestingRatio.value();;
   // We need to rest if there is no more time
   if (time < 30 ) return tohs_Resting;
   // If there are more hares here then interference reduces the activity time
#ifdef __DELAYEDDD
   int hares=(m_OurPopulationManager->GetDelayedAdultDensity(m_Location_x, m_Location_y));
#else
   int hares=(m_OurPopulationManager->GetAdultDensity(m_Location_x, m_Location_y));
#endif
#ifdef __THRESHOLD_AD_DD
  if (hares<m_OurPopulationManager->m_HareThresholdDD) hares=0;
  else hares-=m_OurPopulationManager->m_HareThresholdDD;
#endif
   //double interference=(1-(hares*hares)*cfg_HareMaleDensityDepValue.value());
   //if (interference<0) interference=0;
#ifdef __DISEASEDDM2
	double inter=m_OurPopulationManager->GetInterference(m_lastYearsDensity);
#else
#ifdef __DISEASEDDM3
	double inter=m_OurPopulationManager->GetInterference(m_lastYearsDensity);
#else
#ifndef __DDEPMORT
  double inter=m_OurPopulationManager->GetInterference(hares);
#else
	#ifndef __DISEASEDDM
	  double inter=m_OurPopulationManager->GetInterference(hares);
	#else
      double inter=1.0;
	#endif
#endif
#endif
#endif
  if ((inter<0.5) && (time > 300))
  {
	  if (g_rand_uni() > inter*2) // added to reduce rate of dispersal
	  {
		  // m_ActivityTime = (int)(time / cfg_ForageRestingRatio.value()); removed because it is superfluous
		  return tohs_Dispersal; // Added Sat 15th Dec 2007
	  }
  }
   time*=inter;
   // Given the time we have then we need to feed, which will return an energy
   // Value obtained.
   int a_time = (int) time;
   if (cfg_hare_pesticideresponse_on.value())
   {
	   m_TodaysEnergy = ForageP(a_time);
   }
   else
   {
	   m_TodaysEnergy = Forage(a_time);
   }
   // We need to spend energy on foraging and associated movement - but this is done in ForageSquare()
   TimeBudget(activity_Foraging, (int)time-a_time);
  // We need to rest
   return tohs_Resting;
}

//---------------------------------------------------------------------------

TTypeOfHareState Hare_Male::st_Resting()
{
  /**
  This method uses up the rest of the day in resting
  */
   return tohs_Developing;
}
//---------------------------------------------------------------------------

TTypeOfHareState Hare_Male::st_Developing()
{
  //Assumes that the fatReserve has been added to by st_Foraging from yesterday
  //
	// This is the last behaviour state called each day, so we can use this to sort out
	// the energetics
  //Assumes that the fatReserve has been added to by st_Foraging from yesterday
  //

	EnergyBalance(activity_Resting, 1440); // calculates the energy use by BMR
	if (m_TodaysEnergy<0) {
		m_fatReserve+=(m_TodaysEnergy * (1.0/42.7));
		m_TodaysEnergy=0;
	}
 // Grow
 // Solid food absorption is already taken into account by this time
 if (m_Age<365) {
	double Target=m_OurPopulationManager->GetMaxDailyGrowthEnergyP(m_Age);
	if (m_TodaysEnergy<Target) Target=m_TodaysEnergy;
	double addedtoday=Target*m_OurPopulationManager->GetGrowthEfficiencyP(m_Age);
	m_weight+=addedtoday;
	m_TodaysEnergy-=Target;
	Target=m_OurPopulationManager->GetMaxDailyGrowthEnergyF(m_Age);
	if (m_TodaysEnergy<Target) Target=m_TodaysEnergy;
	addedtoday=Target*m_OurPopulationManager->GetGrowthEfficiencyF(m_Age);
	m_weight+=addedtoday;
	m_TodaysEnergy-=Target;
 } else {
	double Target=m_OurPopulationManager->GetMaxDailyGrowthEnergyF(m_Age);
	if (m_TodaysEnergy<Target) Target=m_TodaysEnergy;
	double gf=m_OurPopulationManager->GetGrowthEfficiencyF(m_Age);
	double addedtoday=Target*gf;
	m_weight+=addedtoday;
	m_TodaysEnergy-=Target;
 }
 if (m_TodaysEnergy>0) {
		// Put what remains back into fat reserve
		m_fatReserve+=m_TodaysEnergy*m_OurPopulationManager->GetGrowthEfficiencyF(366); // 366 is the fat conversion efficiency
		m_TodaysEnergy=0;
		// Cap the fat reserve and store surplus energy for today
		if (m_fatReserve>(cfg_AdultMaxFat.value() * m_weight)) {
			m_fatReserve=cfg_AdultMaxFat.value() * m_weight;
		}
		m_StarvationDays=0; // Not starving
		return tohs_Foraging;
	}
	else {
			m_fatReserve+=m_TodaysEnergy*m_OurPopulationManager->GetGrowthEfficiencyF(366); // 366 is the fat conversion efficiency
			if (m_fatReserve<0) {
				m_TodaysEnergy=m_fatReserve/m_OurPopulationManager->GetGrowthEfficiencyF(366);
				m_fatReserve=0.0;
			}
			else m_TodaysEnergy=0;
//			double rmr=m_OurPopulationManager->GetRMR(m_Age, GetTotalWeight());
//			if (m_TodaysEnergy<(0.0-(rmr*0.25)))
			if (m_TodaysEnergy<0.0)
			{
				// Oh oh, we are starving need to lose body weight and maybe we will die?
#ifdef __ADULT_WT_STARVE_CHANCE
				int testval=0;
				if (m_Age<cfg_fixadult_starve.value()) {
					// Somewhere between juvenile and adult so use in between values
					int diff = cfg_adult_starve.value() - cfg_juv_starve.value();
					testval=cfg_adult_starve.value()-(int)((1.0-(cfg_fixadult_starve.value()/(double)m_Age))*diff);

				} else testval=cfg_adult_starve.value();
				if (++m_StarvationDays > cfg_adult_starvation_threshold.value()) {
					if (random(10000)> testval) {
						return tohs_Dying;
					}
				}
#else
				if (++m_StarvationDays > cfg_adult_starvation_threshold.value()) {
					return tohs_Dying;
				}
#endif
			}
			else {
				m_StarvationDays--; // Not starving
				if (m_StarvationDays<0) m_StarvationDays=0;
				m_TodaysEnergy=0;
			}
			if (random(3)==0) return tohs_Dispersal; // We are hungry, we need to disperse
	}
	return tohs_Foraging;
}
//---------------------------------------------------------------------------

TTypeOfHareState Hare_Male::st_ReproBehaviour()
{
  // TODO - this will need implementing if we have to simulate male reproduction (e.g. for genetic effects) or if we find we need this to alter survival in some way.
   return tohs_Resting;
}
//---------------------------------------------------------------------------

/**
Minimal housekeeping for dying for the male
*/
void Hare_Male::ON_Dead()
{
   m_CurrentHState=tohs_Dying;
   m_CurrentStateNo=-1;
   m_StepDone=true;
}
//---------------------------------------------------------------------------
void Hare_Male::InternalPesticideHandlingAndResponse()
{
	/**
	* This method is re-implemented ffrom THare for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then call pesticide-specific actions by dose
	*/
	TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
	double pesticideInternalConc = m_pesticide_burden / m_weight;
	if (m_pesticide_burden>0) m_OurPopulationManager->BodyBurdenOut(m_OurLandscape->SupplyYearNumber(), m_OurLandscape->SupplyDayInYear(), m_pesticide_burden, pesticideInternalConc);

	if (pesticideInternalConc > cfg_HarePesticideAccumulationThreshold.value())
	{
		switch (tp)
		{
		case ttop_NoPesticide:
			break;
		case ttop_ReproductiveEffects:
			GeneralEndocrineDisruptor(pesticideInternalConc); // Calls the EndocrineDisruptor action code
			break;
		case ttop_AcuteEffects:
			GeneralOrganoPhosphate(pesticideInternalConc); // Calls the GeneralOrganophosphate action code
			break;
		default:
			exit(47);
		}
	}
	m_pesticide_burden *= m_pesticidedegradationrate; // Does nothing by default except internal degredation of the pesticide
}
//---------------------------------------------------------------------------

//*************************************************************************************
//************************** PESTICIDE SPECIFIC EFFECT CODE ***************************
//*************************************************************************************


//-------------------------------------------------------------------------------------
//-------------------- GENERAL ENDOCRINE DISRUPTOR EFFECT CODE ------------------------
//-------------------------------------------------------------------------------------

void Hare_Male::GeneralEndocrineDisruptor(double /* a_dose */)
{
	return;
}

//-------------------------------------------------------------------------------------
//------------------------ GENERAL ORGANOPHOSPHATE EFFECT CODE ------------------------
//-------------------------------------------------------------------------------------

void Hare_Male::GeneralOrganoPhosphate(double /* a_dose */)
{
	if (g_rand_uni() > l_pest_daily_mort.value()) return;
	m_CurrentHState = tohs_Dying;
	m_StepDone = true;
	return;
}

//#####################################################################################
//########################### END PESTICIDE HANDLING CODE #############################
//#####################################################################################


//---------------------------------------------------------------------------
//      Hare_Female CODE
//---------------------------------------------------------------------------


Hare_Female::Hare_Female(int p_x, int p_y, Landscape * p_L, THare_Population_Manager* p_PPM, double p_weight, int a_age, int a_Ref) : Hare_Juvenile(p_x, p_y, p_L, p_PPM, p_weight)
{
	Init(p_weight,a_age,a_Ref);
}
//---------------------------------------------------------------------------

void Hare_Female::ReInit(struct_Hare a_data)
{
	m_Location_x = a_data.x;
	m_Location_y = a_data.y;
	m_OurLandscape = a_data.L;
	m_CurrentStateNo = 0;
	THareInit(a_data.x, a_data.y, a_data.HM);
	Init(a_data.weight, a_data.age, a_data.RefNum);
}
//---------------------------------------------------------------------------

void Hare_Female::Init( double p_weight, int a_age, int a_Ref)
{
	
	m_Type = hob_Female;
	m_fatReserve = p_weight * 3.8*0.04; // To get 4% of WW+ingesta
	m_sterile = false;
	m_weight = p_weight;
	m_old_weight = p_weight;
	m_Age = a_age;
	m_OestrousCounter = 0;
	m_GestationCounter = 0;
	m_LeveretMaterial = 0;
	m_reproActivity = activity_oestrouscycle;
	m_litter_no = 0;
	m_KJWalking = m_OurPopulationManager->GetKJperM((int)floor(0.5 + m_weight));
	m_KJRunning = m_KJWalking * 2;
	m_KJForaging = m_KJWalking;
	m_IamSick = false;
	m_RefNum = a_Ref;
}
//---------------------------------------------------------------------------

Hare_Female::~Hare_Female()
{
    //Nothing to do
}
//---------------------------------------------------------------------------

void Hare_Female::BeginStep()
{
	m_ActivityTime=1440; // Start the day
#ifdef __SIZERELATEDDEATH
	if ((m_OurLandscape->SupplyDayInYear()>0) && ((m_OurLandscape->SupplyDayInYear()<90))) {
		if (m_Age>365) {
			if (m_weight<cfg_hare_minimum_breeding_weight.value()) {
				ON_Dead();
				m_StepDone=true; // We need to skip the step code, we are dead
				return;
			}
		}
	}
#endif
#ifdef __DISEASEDDMORTALITY
	if (m_IamSick) {
		ON_Dead();
		m_StepDone=true; // We need to skip the step code, we are dead
		return;
	}
#endif
	if (WasPredated()) {
		ON_Dead();
		m_StepDone=true; // We need to skip the step code, we are dead
		return;
	}
	// Age physiolocally
	if (++m_Age > m_Lifespan) {
		ON_Dead();
		m_StepDone=true; // We need to skip the step code, we are dead
	return;
	}
	m_KJWalking=m_OurPopulationManager->GetKJperM((int) floor(0.5+m_weight));
	m_KJRunning=m_KJWalking*2;
	m_KJForaging=m_KJWalking;
	if (g_rand_uni()<cfg_hare_proximity_alert.value()) {
		Running(cfg_hare_escape_dist.value());
	}
	// Checking code ***CJT***
	m_foragingenergy=0;
#ifdef __DISEASEDDM
	m_experiencedDensity+=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
	if (m_OurLandscape->SupplyDayInYear()==0) {
		m_lastYearsDensity=m_experiencedDensity/365;
		m_experiencedDensity=0;
		if (random(cfg_HareFemaleSicknessDensityDepValue.value())<m_lastYearsDensity) {
			m_IamSick=true;
		}
		else m_IamSick=false;
	}
#endif
#ifdef __DISEASEDDM2
	m_experiencedDensity+=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
	if (m_OurLandscape->SupplyDayInYear()==0) {
		m_lastYearsDensity=m_experiencedDensity/365;
		m_experiencedDensity=0;
	}
#endif
#ifdef __DISEASEDDM3
	m_ddindex++;
	if (m_ddindex==365) {
		m_ddindex=0;
	}
	m_DensitySum-=m_expDensity[m_ddindex];
	m_expDensity[m_ddindex]=m_OurPopulationManager->GetTotalDensity(m_Location_x, m_Location_y);
	m_DensitySum+=m_expDensity[m_ddindex];
	m_lastYearsDensity= int(m_DensitySum * (1.0/365.0));
#endif
	// Set the maximum possible energy intake
	m_EnergyMax = GetRMR()*cfg_MaxEnergyIntakeScaler.value();
	CheckManagement();
}
//---------------------------------------------------------------------------

void Hare_Female::Step()
{
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentHState)
  {
   case tohs_InitialState: // Initial state
    m_CurrentHState=tohs_Foraging;
    break;
   case tohs_Dispersal:
    // Legal returns are:
    // tohs_Foraging
   // Dispersal is risky therefore take a test again
#ifdef __DISPERSALDDM
	   if (g_rand_uni()<(cfg_hare_adult_predation.value())) {
		ON_Dead();
		m_StepDone=true; // We need to skip the step code, we are dead
		return;
	}
#endif
	m_CurrentHState=st_Dispersal();
    break;
   case tohs_Foraging:
   // Legal returns are:
   // tohs_ReproBehaviour
    m_CurrentHState=st_Foraging();
    break;
   case tohs_Resting:
    // Legal returns are:
    // tohs_Developing
    m_CurrentHState=st_Resting();
    break;
   case tohs_ReproBehaviour:
    // Legal returns are:
    // tohs_Resting (& possibly dying)
    m_CurrentHState=st_ReproBehaviour();
    break;
   case tohs_Developing:
    // Legal returns are:
    // tohs_Dying
    // tohs_Foraging
    // tohs_Dispersal
    m_CurrentHState=st_Developing();
    m_StepDone=true;
    break;
   case tohs_Dying:
    ON_Dead();
    m_StepDone=true;
    break;
   default:
   m_OurLandscape->Warn("Hare_Female::Step - unknown state",NULL);
    exit(1);
   }
}
//---------------------------------------------------------------------------

void Hare_Female::EndStep() {
	MovePeg();
	if (cfg_hare_pesticideresponse_on.value()) InternalPesticideHandlingAndResponse();
}
//---------------------------------------------------------------------------

TTypeOfHareState Hare_Female::st_Developing()
{
  //Assumes that the fatReserve has been added to by st_Foraging from yesterday
  //
	// This is the last behaviour state called each day, so we can use this to sort out
	// the energetics
  //Assumes that the fatReserve has been added to by st_Foraging from yesterday
  //

	EnergyBalance(activity_Resting, 1440); // calculates the energy use by BMR
	if (m_TodaysEnergy<0) {
		m_fatReserve+=(m_TodaysEnergy * (1.0/42.7));
		m_TodaysEnergy=0;
	}
	// Grow
	// Solid food absorption is already taken into account by this time
	if (m_Age<365) 
	{
		double Target=m_OurPopulationManager->GetMaxDailyGrowthEnergyP(m_Age);
		if (m_TodaysEnergy<Target) Target=m_TodaysEnergy;
		double addedtoday=Target*m_OurPopulationManager->GetGrowthEfficiencyP(m_Age);
		m_weight+=addedtoday;
		m_TodaysEnergy-=Target;
		Target=m_OurPopulationManager->GetMaxDailyGrowthEnergyF(m_Age);
		if (m_TodaysEnergy<Target) Target=m_TodaysEnergy;
		addedtoday=Target*m_OurPopulationManager->GetGrowthEfficiencyF(m_Age);
		m_weight+=addedtoday;
		m_TodaysEnergy-=Target;
	} 
	else 
	{
		double Target=m_OurPopulationManager->GetMaxDailyGrowthEnergyF(m_Age);
		if (m_TodaysEnergy<Target) Target=m_TodaysEnergy;
		double gf=m_OurPopulationManager->GetGrowthEfficiencyF(m_Age);
		double addedtoday=Target*gf;
		m_weight+=addedtoday;
		m_TodaysEnergy-=Target;
	}
	if (m_TodaysEnergy>0) 
	{
		// Put what remains back into fat reserve
		m_fatReserve+=m_TodaysEnergy*m_OurPopulationManager->GetGrowthEfficiencyF(366); // 366 is the adult fat conversion efficiency
		m_TodaysEnergy=0;
		// Cap the fat reserve and store surplus energy for today
		if (m_fatReserve>(cfg_AdultMaxFat.value() * m_weight)) {
			m_fatReserve=cfg_AdultMaxFat.value() * m_weight;
		}
		m_StarvationDays=0; // Not starving
		return tohs_Foraging;
	}
	else {
			m_fatReserve+=m_TodaysEnergy*m_OurPopulationManager->GetGrowthEfficiencyF(366); // 366 is the adult fat conversion efficiency
			if (m_fatReserve<0) 
			{
				m_TodaysEnergy=m_fatReserve/m_OurPopulationManager->GetGrowthEfficiencyF(366);
				m_fatReserve=0.0;
			}
			else m_TodaysEnergy=0;
			// What should the criteria for a starvation day be?
			//double rmr=m_OurPopulationManager->GetRMR(m_Age, GetTotalWeight());
			//if (m_TodaysEnergy<(0.0-(rmr*0.25)))
			if (m_TodaysEnergy<0.0)
			{
				// Oh oh, we are starving need to lose body weight and maybe we will die?
#ifdef __ADULT_WT_STARVE_CHANCE
				int testval=0;
				if (m_Age<cfg_fixadult_starve.value()) {
					// Somewhere between juvenile and adult so use in between values
					int diff = cfg_adult_starve.value() - cfg_juv_starve.value();
					testval=cfg_adult_starve.value()- (int)((1.0-((double)m_Age/cfg_fixadult_starve.value()))*diff);

				} else testval=cfg_adult_starve.value();
				if (++m_StarvationDays > cfg_adult_starvation_threshold.value()) {
					if (random(10000)> testval) {
						return tohs_Dying;
					}
				}
	#ifdef __NOKNOCKONENERGYEFFECT
				else m_TodaysEnergy=0.0;
	#endif
#else
				if (++m_StarvationDays > cfg_adult_starvation_threshold.value()) {
					return tohs_Dying;
				}
#endif
			}
			else {
				m_StarvationDays--; // Not starving
				if (m_StarvationDays<0) m_StarvationDays=0;
				m_TodaysEnergy=0;
			}
			// If we have kids then we expect this for a while, so only dispere if fat reserves are less than 0
			if (m_reproActivity!=activity_lactation || m_fatReserve<0) {
							if (random(3)==0) return tohs_Dispersal; // We are hungry, we need to disperse
			}
	} // end if energy<0
	return tohs_Foraging;
}
//---------------------------------------------------------------------------

TTypeOfHareState Hare_Female::st_Foraging()
{
   /**
   Effectively the day is started here, unless we have been dispersing.
   \n
   At this point we have a whole day to use and need to figure out how to use it.   \n
   There are some constraints on the time we can use for foraging such that
   we need a certain amount of time for resting e.g. coprophagy.
   For now I am going to assume that the resting time required is
   proportional to forage time used and the ratio of the two is in
   cfg_ForageRestingRatio \n
   \n
   Four different ways of implementing density dependence are selectable here.
   */
   double time= (double) m_ActivityTime * cfg_ForageRestingRatio.value();
   // We need to rest if there is no more time
   if (time < 30 ) return tohs_Resting;
   // If there are more hares here then interference reduces the activity time
#ifdef __DELAYEDDD
   int hares=(m_OurPopulationManager->GetDelayedAdultDensity(m_Location_x, m_Location_y));
#else
   int hares=(m_OurPopulationManager->GetAdultDensity(m_Location_x, m_Location_y));
#endif
#ifdef __THRESHOLD_AD_DD
  if (hares<m_OurPopulationManager->m_HareThresholdDD) hares=0;
  else hares-=m_OurPopulationManager->m_HareThresholdDD;
#endif
#ifdef __DISEASEDDM2
	double inter=m_OurPopulationManager->GetInterference(m_lastYearsDensity);
#else
#ifdef __DISEASEDDM3
	double inter=m_OurPopulationManager->GetInterference(m_lastYearsDensity);
#else
#ifndef __DDEPMORT
  double inter=m_OurPopulationManager->GetInterference(hares);
#else
	#ifndef __DISEASEDDM
	  double inter=m_OurPopulationManager->GetInterference(hares);
	#else
      double inter=1.0;
	#endif
#endif
#endif
#endif
	if ((inter<0.5) && (time > 300))
	{
		if (m_reproActivity!=activity_lactation) {  // Don't abandon kids just because others want to be here too
			if (g_rand_uni() > inter) // added to reduce rate of dispersal
			{
				//m_ActivityTime = (int)(time / cfg_ForageRestingRatio.value());
				return tohs_Dispersal; // Added Sat 15th Dec 2007
			}
		}
	}
   time*=inter;
   // Given the time we have then we need to feed, which will return an energy value obtained.
   int a_time = (int) time;
   if (cfg_hare_pesticideresponse_on.value())
   {
	   m_TodaysEnergy = ForageP(a_time);
   }
   else
   {
	   m_TodaysEnergy = Forage(a_time);
   }
   // We need to spend energy on foraging and associated movement - done in ForageSquare()
   //
   TimeBudget(activity_Foraging, (int)time-a_time);
   // Print the energy checking output
#ifdef __saveEnergyInfo
   dumpEnergy();
#endif
   return tohs_ReproBehaviour;
}
//---------------------------------------------------------------------------
void Hare_Female::dumpEnergy() {
   FILE* fp=fopen("EnergyCheck.txt","a");
   int wt=int(floor(0.5+m_weight));
   int fe=int(floor(0.5+m_foragingenergy));
   int fr=int(floor(0.5+m_fatReserve));
   int te=int(floor(0.5+m_TodaysEnergy));
   fprintf (fp,"%d\t%d\t%d\t%d\t%d\n",(int) m_OurLandscape->SupplyDayInYear(), wt, fe, fr, te);
   fclose(fp);
}
//---------------------------------------------------------------------------
/**
Uses the base class st_dispersal but then adds some special female functionality.
*/
TTypeOfHareState Hare_Female::st_Dispersal()
{
	TTypeOfHareState tohs=Hare_Juvenile::st_Dispersal();
	if (tohs==tohs_Dying) return tohs_Dying;
	// If dispersal is called, then she must be deserting any young she has
	// The easiest way to do this is to lie, and tell them she is dead
	vector<THare*>::iterator current = m_MyYoung.begin();
	while (current != m_MyYoung.end()) {
		(*current)->ON_MumDead(this);
		current++;
	}
	// Now must clear the list.
	if (m_MyYoung.size()>=1) {
		m_MyYoung.erase(m_MyYoung.begin(),m_MyYoung.end()); // Remove all elements
		m_reproActivity=activity_oestrouscycle;
	}
	m_peg_x = m_Location_x;
	m_peg_y = m_Location_y;
	return tohs_Foraging;
}
//---------------------------------------------------------------------------

TTypeOfHareState Hare_Female::st_Resting()
{
  /**
  This method uses up the rest of the day in resting
  */
   return tohs_Developing;
}
//---------------------------------------------------------------------------

/**
\brief
A switch determining what repro behaviour to carry out
*/
TTypeOfHareState Hare_Female::st_ReproBehaviour()
{
   /**
   Repro behaviour to consider:\n
    1) Mating - either a function of her or the male - but lets keep it here
       so the male is a simple as possible. \n
    2) Update Gestation. \n
    3) Give Birth. \n
    4) Lactation. \n
    5) Getting resources for leveret production. \n
    6) Determining leveret size and litter size? \n
	\n
	Also optional mortality based upon reproductive mortality
	*/

   // First find out what kind of behaviour we are in:
#ifdef __REPROMORTCHANCE
		 // Repromortchance is a function of size, the bigger you are the less chance of mortality
		 int mc=0;
#endif
   switch(m_reproActivity)
   {
     case activity_oestrouscycle:
       UpdateOestrous();
       break;
     case activity_inoestrous:
       Mating();
       break;
     case activity_gestation:
       UpdateGestation();
       break;
     case activity_givebirth:
#ifdef __REPROMORTCHANCE
		 // Repromortchance is a function of size, the bigger you are the less chance of mortality
		 mc=random(100+(int)m_weight-cfg_hare_minimum_breeding_weight.value());
		 if (mc<cfg_HareFemaleReproMortValue.value()) {
			 return tohs_Dying;
		 }
#endif
       GiveBirth();
       break;
     case activity_lactation:
       DoLactation();
       break;
     default:
       m_OurLandscape->Warn("Hare_Female::st_ReproBehaviour unknown activity"
                                                                         ,NULL);
       exit(1);
   }
   return tohs_Resting;
}
//---------------------------------------------------------------------------

/**
\brief
Updates Oestrous if it is the breeding season.
*/
/**
Optional disease and minimum reproductive weight code for POM.
*/
void Hare_Female::UpdateOestrous()
{
#ifdef __DISEASEDDM
	if (m_IamSick) {
		return; // will loop each day until not sick
	}
#endif
#ifdef	__MINREPWEIGHT
	if (m_weight<cfg_hare_minimum_breeding_weight.value()) return; // No breeding below this weight
#endif

	// No breeding outside the breeding season
	int today=m_OurLandscape->SupplyDayInYear();
	if ((today<cfg_ReproStartDay.value()) || (today>cfg_ReproEndDay.value()) )
	{
		m_OestrousCounter=0;
		m_litter_no=0;
		return;
	}
	if (++m_OestrousCounter>cfg_DaysToOestrous.value()) {
		// No breeding if in -ve energy balance
		if (cfg_hare_adult_breed_threshold.value()<=m_fatReserve) {
			m_reproActivity=activity_inoestrous;
			m_OestrousCounter=0;
	  }
	}
}
//---------------------------------------------------------------------------
/**
*/
void Hare_Female::Mating()
{
	// Sigh, need to develop code to determine if a male is close - how does
	// she choose a mate?? TODO if we ever consider males limiting or need population genetics
	// This is complicated by multiple matings in real life too.
	//
	// Assuming she has a mate and is mated
	m_GestationCounter=cfg_hare_DaysToGestation.value();
	m_reproActivity=activity_gestation;
	m_LeveretMaterial=0;
}
//---------------------------------------------------------------------------
/**
\brief
Counts down gestation and grows foetal mass
*/
void Hare_Female::UpdateGestation()
{
	// Now are they ready to be born?
	if (--m_GestationCounter<1) {
		m_reproActivity=activity_givebirth;
	}
	/** Figures out how much energy to use on growing the kids
	 and add this to the m_LeveretMaterial\n
	 Some assumptions:\n
	 1) Foetal growth is linear, with a max per day\n
	 2) If the female's fat reserves fall below the hunger threshold value\n
	she does not use energy on the foetus\n
	 3) maximum X% of the free energy is used on foetus production - this is an input variable.
	 */

	// ALL the calcuations below are in g fat, this is just to save on unnecessary conversions
	// and divides later
	if (m_fatReserve>cfg_hare_adult_dispersal_threshold.value()) {
		double en=(m_fatReserve-cfg_hare_adult_dispersal_threshold.value())*cfg_hare_foetusenergyproportion.value();
		double fe=4*g_hare_maxFoetalKJ[m_GestationCounter];
		if (en>fe) en=fe;
		m_fatReserve-=en;
		m_LeveretMaterial+=en;
	}
}
//---------------------------------------------------------------------------
/**
\brief
Give birth to Hare_Infants
*/
void Hare_Female::GiveBirth()
{
  double leveret_size;
  int NoLeverets=0;
/**
	Lets assume we do this a deterministic way. So the number of leverets
	depends on the mass of leveret material collected.\n
	The size is determined by the mass divided by the number possible such that
	size stays in the range 95-125g	\n
	Litter size limited to max 4
*/

   if (m_pesticideInfluenced1)
   {
		// Could do lots now, e.g. take a test to determine if we reduce repro or abandon it.	
		// The option below reduces the energy for leveret development by 0-100%
		//m_LeveretMaterial *= g_rand_uni();
	   // Here we take it all
	   m_LeveretMaterial = 0;
		m_pesticideInfluenced1 = false;
   }

   // THIS CODE IS FOR DOING IT ON THE BASIS OF ENERGY
  double mi=cfg_minLeveretBirthWeight.value();
  if (m_LeveretMaterial/4.0 > mi) NoLeverets=4;
    else if (m_LeveretMaterial/3.0 > mi) NoLeverets=3;
      else if (m_LeveretMaterial/2.0 > mi) NoLeverets=2;
        else if (m_LeveretMaterial > mi) NoLeverets=1;
  if (NoLeverets==0) {
    // Can't produce a leveret - abort the pregnancy
    m_LeveretMaterial=0;
	m_reproActivity=activity_oestrouscycle;
    m_OestrousCounter=cfg_DaysToOestrous.value();
	return;
  }
  leveret_size=m_LeveretMaterial/(double)NoLeverets;
  if (leveret_size>cfg_maxLeveretBirthWeight.value())
  {
	  leveret_size=cfg_maxLeveretBirthWeight.value();
  }
  // END ENERGETIC
  /*
  // THIS IS THE ALTERNATIVE 1 _ GET THE NUMBER BASED ON WHETHER IT IS FIRST, 2ND, 3RD etc..
  NoLeverets=m_OurPopulationManager->GetLitterSize(m_litter_no++);
  if (NoLeverets<1) {
    AllYoungKilled();
    return;
  }
  leveret_size=cfg_HareStartWeight.value(); // NB No variability in start weight
  */
  // END ALTERNATE 1
  /* ALTERNATE 2 - Here we adjust for the weight of the female

  if (m_weight > cfg_hare_minimum_breeding_weight.value() ) {
	NoLeverets=m_OurPopulationManager->GetLitterSize(m_litter_no++);
  }
  else {
  }
  if (NoLeverets<1) {
    AllYoungKilled();
    return;
  }
  leveret_size=cfg_HareStartWeight.value(); // NB No variability in start weight
  */
// END ALTERNATE 2


  // Create the data required
  struct_Hare* sp;
  sp = new struct_Hare;
  sp->HM = m_OurPopulationManager;
  sp->L = m_OurLandscape;
  sp->Mum=this;
  sp->weight=leveret_size;
  /** Once they are born they need to be placed somewhere. This should be somewhere where the vegetation is not too sparse */
  APoint pt = PlaceYoung();
  sp->x = pt.m_x;
  sp->y = pt.m_y;
  // Make the leverets
  m_OurPopulationManager->CreateObjects(0,this,NULL,sp,NoLeverets);
  delete sp;
  m_litter_no++;
  m_reproActivity=activity_lactation;
#ifdef __saveLitterInfo
	//Temporary Output Start
	FILE* fp=fopen("LitterProduction.txt","a");
	int month=m_OurLandscape->SupplyMonth();
	fprintf(fp,"%i\t%i\t%f\t%d\n",month, NoLeverets, leveret_size, m_litter_no);
	fclose(fp);
	// End
#endif
	// Feed them today
	DoLactation();
}
//---------------------------------------------------------------------------
/**
\brief
Find a place to put the young
*/
APoint Hare_Female::PlaceYoung()
{
	APoint pt;
	int loop = 0;
	while (loop++ < 500)
	{
		pt = m_OurLandscape->CorrectCoordsPt(m_Location_x - loop, m_Location_y - loop);
		if (m_OurLandscape->SupplyVegHeight(pt.m_x, pt.m_y) >= 30) return pt;
		pt = m_OurLandscape->CorrectCoordsPt(m_Location_x, m_Location_y - loop);
		if (m_OurLandscape->SupplyVegHeight(pt.m_x, pt.m_y) >= 30) return pt;
		pt = m_OurLandscape->CorrectCoordsPt(m_Location_x + loop, m_Location_y - loop);
		if (m_OurLandscape->SupplyVegHeight(pt.m_x, pt.m_y) >= 30) return pt;
		pt = m_OurLandscape->CorrectCoordsPt(m_Location_x - loop, m_Location_y);
		if (m_OurLandscape->SupplyVegHeight(pt.m_x, pt.m_y) >= 30) return pt;
		pt = m_OurLandscape->CorrectCoordsPt(m_Location_x + loop, m_Location_y);
		if (m_OurLandscape->SupplyVegHeight(pt.m_x, pt.m_y) >= 30) return pt;
		pt = m_OurLandscape->CorrectCoordsPt(m_Location_x - loop, m_Location_y + loop);
		if (m_OurLandscape->SupplyVegHeight(pt.m_x, pt.m_y) >= 30) return pt;
		pt = m_OurLandscape->CorrectCoordsPt(m_Location_x, m_Location_y + loop);
		if (m_OurLandscape->SupplyVegHeight(pt.m_x, pt.m_y) >= 30) return pt;
		pt = m_OurLandscape->CorrectCoordsPt(m_Location_x + loop, m_Location_y + loop);
		if (m_OurLandscape->SupplyVegHeight(pt.m_x, pt.m_y) >= 30) return pt;
	}
	pt.m_x = m_Location_x;
	pt.m_y = m_Location_y;
	return pt;
}
	//---------------------------------------------------------------------------
	/**
	\brief
	Feed the young milk
	*/
	void Hare_Female::DoLactation()
	{
		/**
  Here the female uses energy and converts it to milk, which is shared
  equally between her leverets
  Assumptions:\n
   a) There is a maximum amount of milk possible to feed to a leveret\n
   b) Equal distribution of inadequate resources\n \n
   NB the currency is KJ energy not milk per se
*/
  double sz= (double) m_MyYoung.size();
  if (m_fatReserve>0) { // Zero is used here because it is possible to use all fat reserves on this
    double en=63.7*m_fatReserve;
    // en is the energy available for milk
	double rmr=m_MyYoung[0]->GetRMR();
    double maxMilk= (rmr+m_OurPopulationManager->GetMaxDailyGrowthEnergy(m_MyYoung[0]->GetAge())) * sz * (1-g_PropSolidFood[m_MyYoung[0]->GetAge()]);
    if (en>maxMilk){
      en=maxMilk;
    }
#ifdef __NOSTARVE
	en=maxMilk;
#endif
    // remove the energy used
    m_fatReserve-=en* (1.0/63.7); // 63.7 is the cost of 1g dw fat
    // KJ per young
    en/=(double) sz;
    vector<THare*>::iterator current = m_MyYoung.begin();
    while (current != m_MyYoung.end())
    {
      dynamic_cast<Hare_Infant*>(*current)->ON_BeingFed(en);
      current++;
    }
  }
}
//---------------------------------------------------------------------------

bool Hare_Female::UpdateYoung(THare* a_old, THare* a_new)
{
/**
This functions simply sorts through the m_Young vector and replaces the occurence of a_old with a_new. It is used to replace Hare_Infant pointers with Hare_Young pointers
*/
vector<THare*>::iterator current = m_MyYoung.begin();
while (current != m_MyYoung.end())
{
  if (*current == a_old)
  {
    *current=a_new;
    return true;
  }
  current++;
}
return false;
}
//---------------------------------------------------------------------------
/**
Adds a new young to the mothers list
*/
void Hare_Female::AddYoung(THare* a_new)
{
  m_MyYoung.push_back(a_new);
}
//---------------------------------------------------------------------------

void Hare_Female::ON_YoungKilled(THare* a_young)
{
  /**
  This functions sorts through the m_Young vector and removes the dead young
  */
  m_NoYoung= (int) m_MyYoung.size();
  vector<THare*>::iterator current = m_MyYoung.begin();
  while (current != m_MyYoung.end())
  {
    if (*current == a_young)
    {
      m_MyYoung.erase(current);
      if (m_MyYoung.size()==0) {
        // All young eaten do something appropriate
        AllYoungKilled();
      }
      return;
    }
    current++;
  }
  // Should never get here...raise an error and duck out of the program.
  //
  m_OurLandscape->Warn("Hare_Female::ON_YoungKilled - unknown young",NULL);
  exit(1);
}
//---------------------------------------------------------------------------
/**
Behavioural switch to determine what to do on death of all young. Depends on what we were doing before.
*/
void Hare_Female::AllYoungKilled()
{
  switch (m_reproActivity) {
    case activity_oestrouscycle:
      break;
    case activity_inoestrous:
      break;
    case activity_gestation:
      break;
    case activity_givebirth:
    case activity_lactation:
      m_reproActivity=activity_oestrouscycle;
      m_OestrousCounter=cfg_DaysToOestrous.value();
      break;
    default:
      m_OurLandscape->Warn("Hare_Female::AllYoungKilled unknown activity"
                                                                        ,NULL);
      exit(1);
  }
}
//---------------------------------------------------------------------------

  void Hare_Female::ON_RemoveYoung(THare* a_young)
  {
  /**
  This functions simply sorts through the m_Young vector and removes the matured young.\n
  This is identical to ON_YoungKilled, but differs in the result of removing
  the last one.
  */
  m_NoYoung= (int) m_MyYoung.size();
  vector<THare*>::iterator current = m_MyYoung.begin();
  while (current != m_MyYoung.end())
  {
    if (*current == a_young)
    {
      m_MyYoung.erase(current);
      if (m_MyYoung.size()==0) {
        // All young grown up do something appropriate
        AllYoungMatured();
      }
      m_NoYoung= (int) m_MyYoung.size();
	  return;
    }
    current++;
  }
  // Should never get here...raise an error and duck out of the program.
  //
  m_OurLandscape->Warn("Hare_Female::ON_RemoveYoung - unknown young",NULL);
  exit(1);
}
//---------------------------------------------------------------------------

void Hare_Female::AllYoungMatured()
{
	m_NoYoung=0;
  switch (m_reproActivity) {
    case activity_oestrouscycle:
      break;
    case activity_inoestrous:
      break;
    case activity_gestation:
      break;
    case activity_givebirth:
      break;
    case activity_lactation:
      m_reproActivity=activity_oestrouscycle;
      m_OestrousCounter=cfg_DaysToOestrous.value();
      break;
    default:
      m_OurLandscape->Warn("Hare_Female::AllYoungMatured unknown activity",NULL);
      exit(1);
  }
}
//---------------------------------------------------------------------------
/**
Do the housekeeping needed for object destruction
*/
void Hare_Female::ON_Dead()
{
   // if she has young she must tell them she is dead
   vector<THare*>::iterator current = m_MyYoung.begin();
   while (current != m_MyYoung.end())
   {
     (*current)->ON_MumDead(this);
     current++;
   }
   m_MyYoung.clear(); // Needed because of the potential for object re-use
   m_CurrentHState=tohs_Dying;
   m_CurrentStateNo=-1;
   m_StepDone=true;
}
//---------------------------------------------------------------------------
/**
  Debug code for checking young lists.
*/
bool Hare_Female::ON_AreYouMyMum(THare* a_young)
{
  m_NoYoung= (int) m_MyYoung.size();
  vector<THare*>::iterator current = m_MyYoung.begin();
  while (current != m_MyYoung.end())
  {
    if (*current == a_young)
    {
      return true;
    }
    current++;
  }
  return false;
}
//---------------------------------------------------------------------------
/**
  Debug code for checking young lists.
*/
bool Hare_Female::SanityCheckYoungList()
{
  m_NoYoung= (int) m_MyYoung.size();
  unsigned int current=0;
  while (current < m_MyYoung.size())
  {
    if (m_MyYoung[current]->GetMum()!=this)
    {
      return false;
    }
    current++;
  }
  return true;
}
//---------------------------------------------------------------------------

void Hare_Female::InternalPesticideHandlingAndResponse()
{
	/**
	* This method is re-implemented ffrom THare for any class which has pesticide response behaviour.
	* If the body burden exceeds the trigger then call pesticide-specific actions by dose 
	*/
	TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
	double pesticideInternalConc = m_pesticide_burden / m_weight;

	if (m_pesticide_burden > 0) m_OurPopulationManager->BodyBurdenOut(m_OurLandscape->SupplyYearNumber(), m_OurLandscape->SupplyDayInYear(), m_pesticide_burden, pesticideInternalConc);

	if (pesticideInternalConc > cfg_HarePesticideAccumulationThreshold.value())
	{
		switch (tp)
		{
		case ttop_NoPesticide:
			break;
		case ttop_ReproductiveEffects:
			GeneralEndocrineDisruptor(pesticideInternalConc); // Calls the EndocrineDisruptor action code
			break;
		case ttop_AcuteEffects:
			GeneralOrganoPhosphate(pesticideInternalConc); // Calls the GeneralOrganophosphate action code
			break;
		default:
			exit(47);
		}
	}
	m_pesticide_burden *= m_pesticidedegradationrate; // Does nothing by default except internal degredation of the pesticide
}
//---------------------------------------------------------------------------

/**
\brief
Annual global change in population numbers
*/
/**
Remove or add hares to the population on a certain day and year - used for theoretical scenarios & testing
*/
void THare_Population_Manager::Catastrophe( void ) {
	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=1) return;
	// First do we have the right year?
	int year = m_TheLandscape->SupplyYearNumber()- cfg_CatastropheEventStartYear.value();
	;
	if (year%cfg_pm_eventfrequency.value()!=0) return;

	THare* AH = NULL;
	// Now if the % decrease is higher or lower than 100 we need to do different things
	int esize=cfg_pm_eventsize.value();
	if (esize<100) {
		unsigned size2;
		size2 = GetLiveArraySize(hob_Juvenile);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (random(100) > esize) {
                    AH = dynamic_cast < THare * > (SupplyAnimalPtr(hob_Juvenile,j));
				    AH->Dying(); // Kill it
			}
		}
		size2 = GetLiveArraySize(hob_Female);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (random(100) > esize) {
                    AH = dynamic_cast < THare * > (SupplyAnimalPtr(hob_Female,j));
				    AH->Dying(); // Kill it
			}
		}
		size2 = GetLiveArraySize(hob_Male);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (random(100) > esize) {
                    AH = dynamic_cast < THare * > (SupplyAnimalPtr(hob_Male,j));
				    AH->Dying(); // Kill it
			}
		}
	}
	else if (esize>100) {
/*
	Not implemented or needed as of yet
*/
	}
	else return; // No change so do nothing
}
//-----------------------------------------------------------------------------
/**
	Like Catastrophe() this just kills hares but always on Jan 1st.\n
	Used for POM testing only.
*/
void THare_Population_Manager::ExtraPopMort( void ) {
	THare* AH = NULL;
	// This version simply alters populations on 1st January
	int esize= random(50);
	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=1) return;
	// First do we have the right year?
	// First do we have the right year?
	int year = m_TheLandscape->SupplyYearNumber();
	if (year%1!=0) return;
		unsigned size2;
		size2 = GetLiveArraySize(hob_Juvenile);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (random(100) < esize) {
                    AH = dynamic_cast < THare * > (SupplyAnimalPtr(hob_Juvenile,j));
				    AH->Dying(); // Kill it
			}
		}
		size2 = GetLiveArraySize(hob_Male);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (random(100) < esize) {
                    AH = dynamic_cast < THare * > (SupplyAnimalPtr(hob_Male,j));
				    AH->Dying(); // Kill it
			}
		}
		size2 = GetLiveArraySize(hob_Female);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (random(100) < esize) {
                    AH = dynamic_cast < THare * > (SupplyAnimalPtr(hob_Female,j));
				    AH->Dying(); // Kill it
			}
		}
}
//-----------------------------------------------------------------------------
/**
	Takes a fixed proportion of the population (input variable), in the middle of the hunting season.
*/
void THare_Population_Manager::Hunting( void ) {
	m_shot=0;
	double huntpercent=cfg_HareHunting.value();
	Hare_Juvenile* AJH = NULL;
	unsigned size2;
	size2 = GetLiveArraySize(hob_Young);
	for ( unsigned j = 0; j < size2; j++ ) {
		AJH = dynamic_cast < Hare_Juvenile * > (SupplyAnimalPtr(hob_Juvenile,j));
		if ( g_rand_uni()< huntpercent) {
			// Must check if it is already dead of some other cause - is so can't kill it again
			if (AJH->GetCurrentStateNo() != -1)
			{
				AJH->ON_Dead(); // Kill it
				m_shot++;
			}
		}
	}
	Hare_Male* AH = NULL;
	size2 = GetLiveArraySize(hob_Male);
	for ( unsigned j = 0; j < size2; j++ ) {
		AH = dynamic_cast < Hare_Male * > (SupplyAnimalPtr(hob_Male,j));
		if ( g_rand_uni()< huntpercent) {
			if (AH->GetCurrentStateNo() != -1)
			{
				AH->ON_Dead(); // Kill it
				m_shot++;
			}
		}
	}
	Hare_Female* AFH = NULL;
	size2 = GetLiveArraySize(hob_Female);
	for ( unsigned j = 0; j < size2; j++ ) {
		AFH = dynamic_cast < Hare_Female * > (SupplyAnimalPtr(hob_Female,j));
		if ( g_rand_uni()< huntpercent) {
			if (AFH->GetCurrentStateNo() != -1)
			{
				AFH->ON_Dead(); // Kill it
				m_shot++;
			}
		}
	}
}

/**
*	Differential hunting inside a specific area designated as beetlebank addition area. 
*	Takes a fixed proportion of the population (input variable for inside and outside), in the middle of the hunting season
*/
void THare_Population_Manager::HuntingDifferentiatedBeetleBankArea( void ) {
	m_shot=0;
	int tx1 = cfg_BeetleBankMinX.value();
	int tx2 = cfg_BeetleBankMaxX.value();
	int ty1 = cfg_BeetleBankMinY.value();
	int ty2 = cfg_BeetleBankMaxY.value();
	double huntpercent_out, huntpercent_in;
	if (!cfg_BeetleBankInvert.value())  
	{
		huntpercent_out =cfg_HareHunting.value();
		huntpercent_in =cfg_HareHuntingBeetleBankArea.value();
	}
	else
	{
		huntpercent_in =cfg_HareHunting.value();
		huntpercent_out =cfg_HareHuntingBeetleBankArea.value();
	}
	THare* AH = NULL;
	unsigned size2;
	for (int ind =hob_Juvenile; ind < hob_Foobar; ind++)
	{
		size2 = (unsigned)GetLiveArraySize(ind);
		for ( unsigned j = 0; j < size2; j++ ) 
		{
			AH = dynamic_cast < THare * > (SupplyAnimalPtr(ind,j));
			APoint xy = AH->SupplyPoint();
			if ((xy.m_x >= tx1) && (xy.m_y >= ty1) && (xy.m_x <= tx2) && (xy.m_y <= ty2)) 
			{
				if ( g_rand_uni()< huntpercent_in) 
				{
					AH->ON_Dead(); // Kill it
					m_shot++;
				}
			} else
			{
				if ( g_rand_uni()< huntpercent_out) 
				{
					AH->ON_Dead(); // Kill it
					m_shot++;
				}
			}
		}
	}
}

/**
Hunting version based on grid squares - hunting occurs only if there  are enough hares in each grid square. \n
Density squares are 256x256m in size, so for each ca 1 square km we need 8x8 of these squares, if total numbers is greater than a threshold then we hunt.
*/
void THare_Population_Manager::HuntingGrid( void ) {
	int N;
	m_shot=0;
	for (int x=500; x<SimW; x+=1000) {
		for (int y=500; y<SimH; y+=1000) {
			// For each 1km square
			N=0;
			for (int xx=x-(256+128); xx<=x+(256+128); xx+=256) {
				for (int yy=y-(256+128); yy<=y+(256+128); yy+=256) {
					N+=GetTotalDensity(xx,yy);
				}
			}
			if (N>cfg_HareHuntingThreshold.value()) {
				double huntpercent=cfg_HareHunting.value();
				Hare_Juvenile* AJH = NULL;
				unsigned size2;
				size2 = GetLiveArraySize(hob_Juvenile);
				for ( unsigned j = 0; j < size2; j++ ) {
					AJH = dynamic_cast < Hare_Juvenile * > (SupplyAnimalPtr(hob_Juvenile,j));
					if ((AJH->Supply_m_Location_x() >=x-500) &&(AJH->Supply_m_Location_x()< x+500) && (AJH->Supply_m_Location_y() >=y-500) && (AJH->Supply_m_Location_y()< y+500)) {
						if ( g_rand_uni()< huntpercent) {
							if (AJH->GetCurrentStateNo() != -1) {
								AJH->ON_Dead(); // Kill it
								m_shot++;
							}
						}
					}
				}
				Hare_Male* AH = NULL;
				size2 = GetLiveArraySize(hob_Male);
				for ( unsigned j = 0; j < size2; j++ ) {
					AH = dynamic_cast < Hare_Male * > (SupplyAnimalPtr(hob_Male,j));
					if ((AH->Supply_m_Location_x() >=x-500) &&(AH->Supply_m_Location_x()< x+500) && (AH->Supply_m_Location_y() >=y-500) && (AH->Supply_m_Location_y()< y+500)) {
						if ( g_rand_uni()< huntpercent) {
							if (AH->GetCurrentStateNo() != -1) {
								AH->ON_Dead(); // Kill it
								m_shot++;
							}
						}
					}
				}
				Hare_Female* AFH = NULL;
				size2 = GetLiveArraySize(hob_Female);
				for ( unsigned j = 0; j < size2; j++ ) {
					AFH = dynamic_cast < Hare_Female * > (SupplyAnimalPtr(hob_Female,j));
					if ((AFH->Supply_m_Location_x() >=x-500) && (AFH->Supply_m_Location_x()< x+500) && (AFH->Supply_m_Location_y() >=y-500) && (AFH->Supply_m_Location_y()< y+500)) {
						if ( g_rand_uni()< huntpercent) {
							if (AFH->GetCurrentStateNo() != -1) {
								AFH->ON_Dead(); // Kill it
								m_shot++;
							}
						}
					}
				}
			}
		}
	}
}
//-----------------------------------------------------------------------------

/**
Constructor for MRR_Data
*/
MRR_Data::MRR_Data() {
	m_currenttrapping=0;
	m_Entries.resize(0);
}
//-----------------------------------------------------------------------------
/**
Adds a new entry to the Mark Release Recapture data vector
*/
void MRR_Data::AddEntry(int a_RefNum) {
	// First do we have this RefNum in the vector?
	vector<MRR_Entry>::iterator current = m_Entries.begin();
	vector<MRR_Entry>::iterator found = m_Entries.end();
	bool flag=false;
	while (current != found)
	{
		if ( (*current).m_HareRefNum==a_RefNum ) {
			found=current;
			flag=true;
		} else current++;
	}
	MRR_Entry mre;
	if (flag) {
		// Get the entry out to mre
//		mre=(*found);
		if (m_currenttrapping<32) {
			unsigned int mask = 0x01 << m_currenttrapping;
			(*found).m_trappings1 += mask;
		} else {
			unsigned int mask = 0x01 << (m_currenttrapping-32);
			(*found).m_trappings2 += mask;
		}
	} else {
		mre.m_HareRefNum=a_RefNum;
		mre.m_trappings1=0;
		mre.m_trappings2=0;
		if (m_currenttrapping<32) {
			unsigned int mask = 0x01 << m_currenttrapping;
			mre.m_trappings1 += mask;
		} else {
			unsigned int mask = 0x01 << (m_currenttrapping-32);
			mre.m_trappings2 += mask;
		}
		m_Entries.insert(m_Entries.begin(),mre); // Put at the beginning because this is the one most likely found next time
	}
}
//-----------------------------------------------------------------------------

/**
Produces a file output for the Mark Release Recapture data
*/
void MRR_Data::OutputToFile() {
	FILE* MRROut = fopen("MRR_Hare.txt","w");
	if (!MRROut) {
           // something wrong if the program gets to here
           g_land->Warn("THare_Population_Manager::MRROutputs - MRR_Hare.txt can't be opened",NULL);
           exit(1);
	}
	vector<MRR_Entry>::iterator current = m_Entries.begin();
	MRR_Entry mre;
	for (vector<MRR_Entry>::iterator i = m_Entries.begin(); i< m_Entries.end(); i++) {
			mre=(*i);
			unsigned int mask= 0x01;
			for (int j=0; j<32; j++) {
				if ((mask & mre.m_trappings1) > 0 ) {
					fprintf(MRROut,"%d\t",1);
				} else fprintf(MRROut,"%d\t",0);
				mask = mask << 1;
			}
			mask=0;
			for (int j=0; j<32; j++) {
				if ((mask & mre.m_trappings2) > 0 ) {
					fprintf(MRROut,"%d\t",1);
				} else fprintf(MRROut,"%d\t",0);
				mask = mask << 1;
			}
			fprintf(MRROut,"%d\n",mre.m_HareRefNum);
	}
	fclose(MRROut);
}
//-----------------------------------------------------------------------------

//*************************************************************************************
//************************** PESTICIDE SPECIFIC EFFECT CODE ***************************
//*************************************************************************************


//-------------------------------------------------------------------------------------
//-------------------- GENERAL ENDOCRINE DISRUPTOR EFFECT CODE ------------------------
//-------------------------------------------------------------------------------------

void Hare_Female::GeneralEndocrineDisruptor(double /* a_dose */)
{
	// May also wish to specify certain gestation days for the effects here
	if (m_GestationCounter > 0)
	{
		m_pesticideInfluenced1 = true;
	}
}

//-------------------------------------------------------------------------------------
//------------------------ GENERAL ORGANOPHOSPHATE EFFECT CODE ------------------------
//-------------------------------------------------------------------------------------

void Hare_Female::GeneralOrganoPhosphate(double /* a_dose */)
{
	if (g_rand_uni() > l_pest_daily_mort.value()) return;
	m_CurrentHState = tohs_Dying;
	m_StepDone = true;
	return;
}

//#####################################################################################
//########################### END PESTICIDE HANDLING CODE #############################
//#####################################################################################