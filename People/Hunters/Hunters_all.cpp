/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Hunters_all.cpp
\brief <B>The code file for all hunter types and population manager class</B>
*/
/**  \file Hunters_all.cpp
Version of  10 October 2013. \n
By Chris J. Topping
*/

//---------------------------------------------------------------------------

#define __DEBUG_DENSITY


#include <cmath>
#include <iostream>
//#include <fstream>
#include <vector>
#include <list>
#include "../../Landscape/ls.h"
//#include "../../BatchALMaSS/PopulationManager.h"
#include "../../GooseManagement/GooseMemoryMap.h"
#include "../../GooseManagement/Goose_Base.h"
//#include "../../BatchALMaSS/CurveClasses.h"
#include "../Hunters/Hunters_all.h"
//#include "../../GooseManagement/Goose_Population_Manager.h"
//#include "../../GooseManagement/GoosePinkFooted_All.h"
//#include "../../GooseManagement/GooseGreylag_All.h"
//#include "../../GooseManagement/GooseBarnacle_All.h"


/* Links to globals */

/* Links to the goose population */
/*
extern CfgFloat cfg_goose_MinForageOpenness;
extern CfgInt cfg_goose_pinkfootopenseasonstart;
extern CfgInt cfg_goose_pinkfootopenseasonend;
extern CfgInt cfg_goose_greylagopenseasonstart;
extern CfgInt cfg_goose_greylagopenseasonend;

*/
//*******************************************************************************************************************/
//                                        Class Hunter_Population_Manager
//*******************************************************************************************************************/

Hunter_Population_Manager::Hunter_Population_Manager(Landscape* p_L) : Population_Manager(p_L, 1)
{
    // Load List of Animal Classes
	m_ListNames[0] = "Goose Hunter";
	m_ListNameLength = 1;
	BeforeStepActions[ 0 ] = 4; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	/** 
	* There are two possibilities here - we can be in simulation mode or distribution mode. Simulation mode calls Init and runs 
	* hunters as normal whereas distribution mode will attempt to distribute hunters to hunting locations based on model rules.
	* This is also needed for the first time simulations are run to distribute the hunters and produce input for Hunter_Initiation.txt
	*/
	if (!HunterCfg.cfg_Hunters_Distribute.value()) Init(); else DistributeHunters();
	m_HuntingSeasonStart = -1;
	m_HuntingSeasonEnd = -1;
	SetHuntingSeason();
    m_goose_MinForageOpenness=(static_pointer_cast<Goose_Population_Manager>) (m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(TOP_Goose))->GooseCfg.cfg_goose_MinForageOpenness.value();
}

Hunter_Population_Manager::~Hunter_Population_Manager ()
{
	// close down output
	if (HunterCfg.cfg_Hunters_RecordBag.value())
	{
		m_HuntingBagRecord->close();
		delete m_HuntingBagRecord;
	}
}

void Hunter_Population_Manager::DistributeHunters()
{
//	FarmManager* FManager = m_TheLandscape->SupplyFarmManagerPtr();
	/** Reads in the hunter home data which consists of a hunter reference and home x,y coordinates only.
	Hunter type is by default 0 i.e. goose hunter (to be changed when we have more hunter base types) */
	ifstream huntercoords("./HunterHomeLocations.txt", ios::in);
	if (!huntercoords.is_open()) {
		m_TheLandscape->Warn( "Hunter_Population_Manager::DistributeHunters()", "HunterHomeLocations.txt missing" );
		exit( 1 );
	}
	int no_hunters;
	huntercoords >> no_hunters;
	auto* OurHunters = new vector<HunterInfo>;
	OurHunters->resize( no_hunters );
	for (int i = 0; i < no_hunters; i++)
	{
		huntercoords >> (*OurHunters)[ i ].refID >> (*OurHunters)[ i ].homeX >> (*OurHunters)[ i ].homeY;
		/** 
		* We now need to add the number of hunting locations to each hunter. 
		* This is done by the method GetNoHuntLocs() which returns the number of locations. We use this to resize the storage for the number of locations.
		*/
		(*OurHunters)[ i ].FarmHuntRef.resize( GetNoHuntLocs() );
		for (int & hl : (*OurHunters)[i].FarmHuntRef) hl = -1;
		//(*OurHunters)[ i ].CheckedFarms.resize( FManager->GetNoFarms() );
	}
	huntercoords.close();
	// Do the distribution
	DistributeHuntersByRules(OurHunters, no_hunters, HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
	// Now save the results
	SaveDistributedHunters(OurHunters, no_hunters);
	SaveFarmHunters(OurHunters, no_hunters);
	g_msg->Warn("Normal exit after hunter distribution. No of records written to Hunter_Hunting_Locations.txt was: ", no_hunters);
	exit(0);
}

unsigned Hunter_Population_Manager::GetNoHuntLocs() const
{
	/** Calculates the number of hunting locations based on a distribution. Uses probability of one location, two locations or three+ */
	double chance = g_rand_uni();
	if (chance < HunterCfg.cfg_hunterlocONE.value())   return 1;
	if (chance < HunterCfg.cfg_hunterlocTWO.value())   return 2;
	if (chance < HunterCfg.cfg_hunterlocTHREE.value()) return 3;
	if (chance < HunterCfg.cfg_hunterlocFOUR.value())  return 4;
	return 5;

}

void Hunter_Population_Manager::SaveDistributedHunters( vector<HunterInfo>* a_hunterlist, int a_no_hunters )
{
	ofstream huntlocs("./Hunter_Hunting_Locations.txt", ios::out);
	huntlocs << a_no_hunters << endl;
	huntlocs << "HunterID" << '\t' << "HunterType" << '\t' << "HuntingDays" << '\t' << "WeekdayHunterChance" << '\t' << "GooseLookChance" << '\t' << "Efficiency" << '\t';
	huntlocs << "HomeX" << '\t' << "HomeY" << '\t';
	huntlocs << "NoFarmrefs" << '\t' << "FarmRef1" << '\t' << "FarmRef2" << '\t' << "FarmRef3" << '\t' << "FarmRef4" << '\t' << "FarmRef5" << '\t' << "FarmRef6" << '\t' << "FarmRef7" << '\t' << "FarmRef8" << '\t' << "FarmRef9" << '\t' << "FarmRef10" << endl;
		
		//<< '\t' << "FarmCentroidX" << '\t' << "FarmCentroidY" << '\t' << "FarmTotalSize" << '\t' << "FarmArableSize" << '\t' << "FarmType" << '\t' << "NoFields" << '\t' << "NoOpenFields" << '\t' << "ValidFarmX" << '\t' << "ValidFarmY" << endl;
	/*FarmManager* FManager = m_TheLandscape->SupplyFarmManagerPtr();*/
	for (int i = 0; i < a_no_hunters; i++)
	{
		int nolocs = int((*a_hunterlist)[i].FarmHuntRef.size());
		huntlocs << (*a_hunterlist)[ i ].refID << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t';
		huntlocs << (*a_hunterlist)[ i ].homeX << '\t' << (*a_hunterlist)[ i ].homeY << '\t' << nolocs;
		for (int j = 0; j < nolocs; j++)
		{
			int ref = (*a_hunterlist)[ i ].FarmHuntRef[ j ];
			/* 
			APoint pt = FManager->GetFarmCentroid(ref);
			int FarmASize = FManager->GetFarmArableSize(ref);
			int FarmTSize = FManager->GetFarmTotalSize(ref);
			int FarmType = FManager->GetFarmType(ref);
			int OpenFields = FManager->GetFarmNoOpenFields(ref, (int)cfg_GooseMinForageOpenness.value());
			int NoFields = FManager->GetFarmNoFields(ref);
			APoint valid = FManager->GetFarmValidCoords(ref);*/
			huntlocs << '\t' << ref;
		}
		for (int j = 10; j > nolocs; j--) huntlocs << '\t' << "NA";
		huntlocs << endl;
	}
	huntlocs.close();
}


void Hunter_Population_Manager::SaveFarmHunters( vector<HunterInfo>* a_hunterlist, int a_no_hunters ) {
	FarmManager* FManager = m_TheLandscape->SupplyFarmManagerPtr();
	ofstream huntlocs("./Hunter_Hunting_Locations_Farms.txt", ios::out);
	huntlocs << "FarmRef" << '\t' << "FarmCentroidX" << '\t' << "FarmCentroidY" << '\t' << "FarmType" << '\t' << "FarmSize" << '\t'
		<< "FarmArableSize" << '\t' << "NoFields" << '\t' << "NoOpenFields" << '\t' <<"AreaOpenFields" << '\t' << "NoHunters" << endl;
	vector<farminfo*> FarmList;
	for (int i = 0; i < a_no_hunters; i++)
	{
		for (int j : (*a_hunterlist)[ i ].FarmHuntRef)
		{
			int ref = j;
			int found = -1;
			for (int f = 0; f < (int)FarmList.size(); f++) {
				if (FarmList[f]->m_farmref == ref) {
					found = f;
					break;
				}
			}
			if (found == -1) {
				auto* fi = new farminfo;
				fi->m_farmcentroid = FManager->GetFarmCentroid( j );
				fi->m_farmvalid = FManager->GetFarmValidCoords( j );
				fi->m_farmarable = FManager->GetFarmArableSize( j );
				fi->m_farmsize = FManager->GetFarmTotalSize( j );
				fi->m_farmtype = FManager->GetFarmType( j );
				fi->m_openfields = FManager->GetFarmNoOpenFields( j, (int)m_goose_MinForageOpenness );
				fi->m_areaopenfields = FManager->GetFarmAreaOpenFields( j, (int)m_goose_MinForageOpenness );
				fi->m_nofields = FManager->GetFarmNoFields( j );
				fi->m_farmref = j;
				fi->m_NoHunters = 1;
				FarmList.push_back(fi);
			}
			else FarmList[found]->m_NoHunters++;
		}
	}
	// Now we don't want any gaps so check all farms and then check them against our list of hunted farms
	int nofarms = FManager->GetNoFarms();
	for (int fa = 0; fa < nofarms; fa++) {
		int found = -1;
		Farm* farmp = FManager->GetFarmPtrIndex(fa);
		int ref = farmp->GetFarmNumber();
		cout << ref << '\t';
		for (int hf = 0; hf < FarmList.size( ); hf++) 
		{
			if (FarmList[hf]->m_farmref == ref) {
				found = hf;
				cout << ref << '\t' << found << endl;
				break;
			}
		}
		if (found == -1) {
			APoint pt = FManager->GetFarmCentroid( ref );
			huntlocs << ref << '\t' << pt.m_x << '\t' << pt.m_y << '\t' << FManager->GetFarmType(ref) << '\t' << FManager->GetFarmTotalSize(ref) << '\t'
				<< FManager->GetFarmArableSize(ref) << '\t' << FManager->GetFarmNoFields(ref) << '\t' 
				<< FManager->GetFarmNoOpenFields(ref, (int)m_goose_MinForageOpenness) << '\t' << FManager->GetFarmAreaOpenFields(ref, (int)m_goose_MinForageOpenness) << '\t' << 0 << endl;
		}
		else {
			huntlocs << ref << '\t' << FarmList[found]->m_farmcentroid.m_x << '\t' << FarmList[found]->m_farmcentroid.m_y << '\t' << FarmList[found]->m_farmtype << '\t' << FarmList[found]->m_farmsize << '\t'
				<< FarmList[found]->m_farmarable << '\t' << FarmList[found]->m_nofields << '\t' << FarmList[found]->m_openfields << '\t' << FarmList[found]->m_areaopenfields << '\t' << FarmList[found]->m_NoHunters << endl;
		}

	}
	huntlocs.close();
}



bool Hunter_Population_Manager::CheckDensity(int a_ref, vector<int> * a_illegalfarms, vector<FarmOccupcancyData>* a_FarmOccupancy)
{
	/**
	*
	* \param  [in] a_ref The farm reference number
	* \param  [in] a_illegalfarms The list of farms with densities exceeding the limit set for this run
	* \param  [in] a_FarmOccupancy The list of farms that are occupied and the number of hunters allocated to them
	* \return bool true if the hunter could be allocated, false if not
	*/
	FarmManager* FManager = m_TheLandscape->SupplyFarmManagerPtr();
	/** Area is in m2, so divide by 10000. Hunter density in hunters/ha */
	//double area = m_TheLandscape->SupplyFarmManagerPtr()->GetFarmTotalSize( a_ref ) / 10000.0;
	double area = m_TheLandscape->SupplyFarmManagerPtr()->GetFarmAreaOpenFields( a_ref, (int)m_goose_MinForageOpenness) / 10000.0;
	/** Loop through all the farms that are already occupied and figure out if we have this one already */
	for (auto & f : *a_FarmOccupancy) {
		// test if we have this one
		if (f.m_FarmRef == a_ref) {
			double newhunters = f.m_no_hunters + 1.0;
			if ( (newhunters/ area) < HunterCfg.cfg_Hunters_MaxDensity.value()) {
				f.m_no_hunters++; // Add ourselves
				return true; // The farm was OK
			}
			else {
				FManager->AddToIllegalList(a_ref, a_illegalfarms);
				return false;
			}
		}
	}
	// If we reach here it must be a new farm
	// New farm, so is it big enough for one hunter?
	if (1.0 / area > HunterCfg.cfg_Hunters_MaxDensity.value())
	{
		// Need to add to the illegal list
		FManager->AddToIllegalList(a_ref, a_illegalfarms);
		return false;
	}
	else {
		FarmOccupcancyData pt{};
		pt.m_FarmRef = a_ref;
		pt.m_no_hunters = 1;
		a_FarmOccupancy->push_back(pt);
		return true;
	}
}


void Hunter_Population_Manager::DistributeHuntersByRules(vector<HunterInfo>* a_hunterlist, int a_no_hunters, int a_ruleset)
{
	/**
	* To distribute the hunters to farms we need the farm information. This resides in the FarmManager held by the landscape, so first we need
	* a pointer to that. The Farm Manager has handy routines for use with the farm allocations . \n
	* \n
	* We need to be able to do the following:\n
	* - Pick a random farm (set 0)
	* - Pick the closest farm (set 1)
	* - Pick a random farm with openness above x (set 2)
	* - Pick a random farm but upto a maximum density of hunters (set 3)
	* - The closest farm  with openness above x (set 4)
	* - Pick the closest farm but upto a max hunter density (set 5)
	* - Pick  a random farm with openness above x but upto a max hunter density (set 6)
	* - Pick the closest farm with openness above and upto a maximum density of hunters (set 7)
	*
	*/

	FarmManager* FManager = m_TheLandscape->SupplyFarmManagerPtr();
	// Now we distribute the hunters to farms depending on which rule set we are using


	FManager->CalcCentroids(); // Makes sure that farm centres are calculated
	if (a_ruleset == 0)
	{
		/** Rule set 0 is simply random distribution to any farm */
		for (int i = 0; i < a_no_hunters; i++)
		{
			// It is certain that some will be allocated to the same farm twice or more at some point, so we need to stop this. 
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++)
			{
				int theref = -1;
				do {
					// The line below actuall does the interesting bit
					theref = FManager->GetRandomFarmRefnum();
				} while (FManager->IsDuplicateRef(theref, &(*a_hunterlist)[i]));
				(*a_hunterlist)[i].FarmHuntRef[h] = theref;
			}
		}
	}
	else if (a_ruleset == 1)
	{
		/** Rule set 1 - the closest farm */
		auto* list = new vector<int>;
		list->resize(1);
		(*list)[0] = 0;
		for (int i = 0; i < a_no_hunters; i++)
		{
			// It is certain that some will be allocated to the same farm twice or more at some point, so we need to stop this. 
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++)
			{
				// The line below actuall does the interesting bit
				(*a_hunterlist)[i].FarmHuntRef[h] = FManager->FindClosestFarm((*a_hunterlist)[i], &list[0]);
			}
		}
	}
	else if (a_ruleset == 2)
	{
		/** Rule set 2 - Random with openness above cfg_GooseMinForageOpenness */
		for (int i = 0; i < a_no_hunters; i++)
		{
			// It is certain that some will be allocated to the same farm twice or more at some point, so we need to stop this. 
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++)
			{
				int theref = -1;
				do {
					// The line below actually does the interesting bit
					theref = FManager->FindOpennessFarm((int)m_goose_MinForageOpenness);
				} while (FManager->IsDuplicateRef(theref, &(*a_hunterlist)[i]));
				(*a_hunterlist)[i].FarmHuntRef[h] = theref;
			}
		}
	}
	else if (a_ruleset == 3)
	{
		/** Rule set 3 - random subject to hunter density restrictions */
		auto* illegalfarms = new vector<int>;
		auto* FarmOccupancy = new vector<FarmOccupcancyData>;
		for (int i = 0; i < a_no_hunters; i++)
		{
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++)
			{
				bool found1 = false;
				int theref = -1;
				// While a farm is not allocated
				int iteration = 0;
				while (!found1)	{
					if (++iteration > FManager->GetNoFarms())
					{
						g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - unable to allocate hunter, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
						exit(0);
					}
					// Choose a new farm ref to test
					theref = FManager->FindFarmWithRandom(illegalfarms);
					found1 = CheckDensity(theref, illegalfarms, FarmOccupancy);

				}
				(*a_hunterlist)[i].FarmHuntRef[h] = theref;
#ifdef __DEBUG_DENSITY
				double areaHa = FManager->GetFarmTotalSize(theref) / 10000.0;
				double nohunters = 0;
				for (int g = 0; g < a_no_hunters; g++)
				{

					for (int hs : (*a_hunterlist)[g].FarmHuntRef)
					{
						if (hs == theref) nohunters++;
					}
				}
				if (nohunters / areaHa > HunterCfg.cfg_Hunters_MaxDensity.value())
				{
					g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - debug density error, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
					exit(0);
				}
				for (auto & frm : *FarmOccupancy)
				{

					nohunters = 0;
					for (int g = 0; g < a_no_hunters; g++)
					{

						for (int hs : (*a_hunterlist)[g].FarmHuntRef)
						{
							if (hs == frm.m_FarmRef) nohunters++;
						}
					}
					if (frm.m_no_hunters != nohunters)
					{
						g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - debug density error, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
						exit(0);
					}
				}
#endif				

			}
		}
	}
	else if (a_ruleset == 4)
	{
		/** Rule set 4 - openness above cfg_GooseMinForageOpenness but closest to home  */
		auto* list = new vector<int>;
		list->resize(1);
		(*list)[0] = 0;
		for (int i = 0; i < a_no_hunters; i++)
		{
			for (int f = 0; f < int((*a_hunterlist)[i].FarmHuntRef.size()); f++)
			{
				(*a_hunterlist)[i].FarmHuntRef[f] = FManager->FindClosestFarmOpenness((*a_hunterlist)[i], list, (int)m_goose_MinForageOpenness);
			}
		}
	}
	else if (a_ruleset == 5) {
		/** Rule set 5 - Pick the closest farm but upto a max hunter density */
		// First calculate the hunter densities
		// This depends on the way we want to do this, one way is total farm area, the other is open farm area.
		// Both ways are listed here.
		auto* illegalfarms = new vector<int>;
		auto* FarmOccupancy = new vector<FarmOccupcancyData>;
		// When we enter this no hunters will be allocated, so we need to keep a list uptodate as we go along
		// For each hunter
		for (int i = 0; i < a_no_hunters; i++) {
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++)
			{
				bool found1 = false;
				// While a farm is not allocated
				int iteration = 0;
				int theref = -1;
				while (!found1)
				{
					if (++iteration > FManager->GetNoFarms())
					{
						g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - unable to allocate hunter, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
						exit(0);
					}
					// Choose a new farm ref to test
					theref = FManager->FindClosestFarm((*a_hunterlist)[i], illegalfarms);
					found1 = CheckDensity(theref, illegalfarms, FarmOccupancy);
				}
				(*a_hunterlist)[i].FarmHuntRef[h] = theref;
#ifdef __DEBUG_DENSITY
				double areaHa = FManager->GetFarmTotalSize(theref) / 10000.0;
				double nohunters = 0;
				for (int g = 0; g < a_no_hunters; g++)
				{

					for (int hs : (*a_hunterlist)[g].FarmHuntRef)
					{
						if (hs == theref) nohunters++;
					}
				}
				if (nohunters / areaHa > HunterCfg.cfg_Hunters_MaxDensity.value())
				{
					g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - debug density error, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
					exit(0);
				}
				for (auto & frm : *FarmOccupancy)
				{

					nohunters = 0;
					for (int g = 0; g < a_no_hunters; g++)
					{

						for (int hs : (*a_hunterlist)[g].FarmHuntRef)
						{
							if (hs == frm.m_FarmRef) nohunters++;
						}
					}
					if (frm.m_no_hunters != nohunters)
					{
						g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - debug density error, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
						exit(0);
					}
				}
#endif				
			}
		}
	}
	else if (a_ruleset == 6) {
		/** Rule set 6 - Pick a random farm with openness above and upto a maximum density of hunters */
		auto* illegalfarms = new vector<int>;
		auto* FarmOccupancy = new vector<FarmOccupcancyData>;
		int TheOpenness = (int)m_goose_MinForageOpenness;
		// When we enter this no hunters will be allocated, so we need to keep a list uptodate as we go along
		// For each hunter
		for (int i = 0; i < a_no_hunters; i++) {
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++) {
				int Farms = FManager->GetNoFarms();
				bool found1 = false;
				int iteration = 0;
				int theref = -1;
				do {
					if (++iteration > Farms) {
						g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - unable to allocate hunter, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
						exit(0);
					}
					// Choose a new farm ref to test
					do {
						// The line below actually does the interesting bit
						theref = FManager->FindFarmWithOpenness(illegalfarms, TheOpenness);
						if (theref == -1) {
							g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - unable to allocate hunter, in openness, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
							exit(0);
						}
						if (illegalfarms->size() >= Farms - ((*a_hunterlist)[ i ].FarmHuntRef.size())) {
							g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - unable to allocate hunter, all farms illegal, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
							exit(0);
						}
					} while (FManager->IsDuplicateRef(theref, &(*a_hunterlist)[i]));
					found1 = CheckDensity(theref, illegalfarms, FarmOccupancy);
				} while (!found1);
				(*a_hunterlist)[i].FarmHuntRef[h] = theref;
#ifdef __DEBUG_DENSITY
				double areaHa = FManager->GetFarmTotalSize(theref) / 10000.0;
				double nohunters = 0;
				for (int g = 0; g < a_no_hunters; g++)
				{

					for (int hs : (*a_hunterlist)[g].FarmHuntRef)
					{
						if (hs == theref) nohunters++;
					}
				}
				if (nohunters / areaHa > HunterCfg.cfg_Hunters_MaxDensity.value())
				{
					g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - debug density error, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
					exit(0);
				}
				for (auto & frm : *FarmOccupancy)
				{

					nohunters = 0;
					for (int g = 0; g < a_no_hunters; g++)
					{

						for (int hs : (*a_hunterlist)[g].FarmHuntRef)
						{
							if (hs == frm.m_FarmRef) nohunters++;
						}
					}
					if (frm.m_no_hunters != nohunters)
					{
						g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - debug density error, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
						exit(0);
					}
				}
#endif				
			}
		}
		delete illegalfarms;
	}
	else if (a_ruleset == 7) {
		/** Rule set 7 - Pick the closest farm with openness above and upto a maximum density of hunters */
		// First calculate the hunter densities
		// This depends on the way we want to do this, one way is total farm area, the other is open farm area.
		// Both ways are listed here.
		auto* illegalfarms = new vector<int>;
		auto* FarmOccupancy = new vector<FarmOccupcancyData>;
		// When we enter this no hunters will be allocated, so we need to keep a list uptodate as we go along
		// For each hunter
		for (int i = 0; i < a_no_hunters; i++)
		{
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++)
			{
				bool found1 = false;
				int theref = -1;
				// While a farm is not allocated
				int iteration = 0;
				while (!found1)
				{
					if (++iteration > FManager->GetNoFarms())
					{
						g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - unable to allocate hunter, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
						exit(0);
					}
					// Choose a new farm ref to test
					theref = FManager->FindClosestFarmOpenness((*a_hunterlist)[i], illegalfarms, (int)m_goose_MinForageOpenness);
					found1 = CheckDensity(theref, illegalfarms, FarmOccupancy);
				}
				(*a_hunterlist)[i].FarmHuntRef[h] = theref;
#ifdef __DEBUG_DENSITY
				double areaHa = FManager->GetFarmTotalSize(theref) / 10000.0;
				double nohunters = 0;
				for (int g = 0; g < a_no_hunters; g++)
				{

					for (int hs : (*a_hunterlist)[g].FarmHuntRef)
					{
						if (hs == theref) nohunters++;
					}
				}
				if (nohunters / areaHa > HunterCfg.cfg_Hunters_MaxDensity.value())
				{
					g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - debug density error, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
					exit(0);
				}
				for (auto & frm : *FarmOccupancy)
				{

					nohunters = 0;
					for (int g = 0; g < a_no_hunters; g++)
					{

						for (int hs : (*a_hunterlist)[g].FarmHuntRef)
						{
							if (hs == frm.m_FarmRef) nohunters++;
						}
					}
					if (frm.m_no_hunters != nohunters)
					{
						g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - debug density error, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
						exit(0);
					}
				}
#endif
			}
		}
	}
	else if (a_ruleset == 8) {
		/** Rule set 8 - the closest farm but based on probability. Requires a probability function which is a power curve using one parameter.*/
		auto* list = new vector<int>;
		list->resize(1);
		(*list)[0] = 0;
		for (int i = 0; i < a_no_hunters; i++) {
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++) {
				(*a_hunterlist)[i].FarmHuntRef[h] = FManager->FindClosestFarmOpennessProb((*a_hunterlist)[i], &list[0], -1); // -1 means no openness will be used
			}
		}
	}
	else if (a_ruleset == 9) {
		/** Rule set 9 - the closest farm with openness above X, but based on probability for closeness. Requires a probability function which is a power curve using one parameter.*/
		auto* list = new vector<int>;
		list->resize(1);
		(*list)[0] = 0;
		for (int i = 0; i < a_no_hunters; i++) {
			for (int h = 0; h < int((*a_hunterlist)[i].FarmHuntRef.size()); h++) {
				(*a_hunterlist)[i].FarmHuntRef[h] = FManager->FindClosestFarmOpennessProb((*a_hunterlist)[i], &list[0], (int)m_goose_MinForageOpenness);
			}
		}
	}
	else if (a_ruleset == 10) {
		/** Rule set 10 - the closest farm with openness above X, on probability for closeness and on density. Requires a probability function which is a power curve using one parameter.*/
		RuleSet10Based( a_no_hunters, nullptr, a_hunterlist, nullptr, 10 );
	}
	else if (a_ruleset == 11) {
		/** Rule set 11 - the closest farm with openness above X, on probability for closeness and on a probability of the acceptance being reduced with farm size.
		Requires a probability function which is a power curve using one parameter for distance and a second for area. */
		// Need to read in the areas real areas for the farms
		auto* farmsizes = new vector<int>;
		//read the input file
		ifstream ifile( "FarmTotalSizeData.txt", ios::in );
		//check if there is an input file
		if (!ifile.is_open()) {
			cout << "Cannot open input file " << "FarmTotalSizeData.txt" << endl;
			char ch;
			cin >> ch; //so that it doesn't close immediately
			exit( 1 );
		}
		//get the number of farms
		int noFarms;
		ifile >> noFarms; // Could do a check here that this is the right number of farms
		if (noFarms != m_TheLandscape->SupplyNumberOfFarms()) {
			cout << "Number of farms does not match farm number in ALMaSS " << noFarms << " != " << m_TheLandscape->SupplyNumberOfFarms() << endl;
			char ch;
			cin >> ch; //so that it doesn't close immediately
			exit( 1 );
		}
		//read the data and assign values to the farm's variables
		for (int f = 0; f < noFarms; f++) {
			int data;
			ifile >> data; // this is the farm number
			ifile >> data;
			farmsizes->push_back( data );
		}
		ifile.close();
		// OK farm size data read in, lets continue
		RuleSet10Based( a_no_hunters, farmsizes, a_hunterlist, nullptr, 11 );
		delete farmsizes;
	}
	else if (a_ruleset == 12) {
		/** Rule set 12 - the closest farm with openness above X, on probability for closeness 
		and on a probability of the acceptance being reduced with distance from roost.
		Requires a probability function which is a power curve using one parameter for distance and a second for roost distance. */
		// Need to read in the areas real areas for the farms
		auto* roostlocs = new vector<APoint>;
		//read the input file
		ifstream ifile( "GooseRoosts.txt", ios::in );
		//check if there is an input file
		if (!ifile.is_open()) {
			cout << "Cannot open input file " << "GooseRoosts.txt" << endl;
			char ch;
			cin >> ch; //so that it doesn't close immediately
			exit( 1 );
		}
		//get the number of farms
		int noroosts;
		ifile >> noroosts; // Could do a check here that this is the right number of farms
		//read the data and assign values to the roost locs
		for (int r = 0; r < noroosts; r++) {
			APoint data; 
			int species;
			ifile >> species >> data.m_x >> data.m_y;
			roostlocs->push_back( data );
		}
		ifile.close();
		// OK farm size data read in, lets continue
		RuleSet10Based( a_no_hunters, nullptr, a_hunterlist, roostlocs, 12 );
		delete roostlocs;
	}
	else {
		g_msg->Warn("Hunter_Population_Manager::DistributeHunters() - Unknown hunter distribution rule set", HunterCfg.cfg_Hunters_Distribute_Ruleset.value());
		exit(0);
	}
}

void Hunter_Population_Manager::RuleSet10Based( int a_no_hunters, vector<int>* a_farmsizes, vector<HunterInfo>* a_hunterlist, vector<APoint>* a_roostlocs, int a_ruleset ) {
	FarmManager* FManager = m_TheLandscape->SupplyFarmManagerPtr();
	auto* illegalfarms = new vector<int>;
	auto* FarmOccupancy = new vector<FarmOccupcancyData>;
	for (int i = 0; i < a_no_hunters; i++) {
		for (int h = 0; h < int( (*a_hunterlist)[ i ].FarmHuntRef.size() ); h++) {
			bool found1 = false;
			int theref = -1;
			// While a farm is not allocated
			int iteration = 0;
			while (!found1) {
				if (++iteration > FManager->GetNoFarms()) {
					g_msg->Warn( "Hunter_Population_Manager::DistributeHunters() - unable to allocate hunter, rule set ", HunterCfg.cfg_Hunters_Distribute_Ruleset.value() );
					exit( 0 );
				}
				// Choose a new farm ref to test
				if (a_ruleset == 10) {
					theref = FManager->FindClosestFarmOpennessProb( (*a_hunterlist)[ i ], illegalfarms, (int)m_goose_MinForageOpenness );
					found1 = CheckDensity( theref, illegalfarms, FarmOccupancy );
				}
				else if (a_ruleset == 11) {
					// Choose a new farm ref to test
					theref = FManager->FindClosestFarmOpennessProbSmallIsBest( (*a_hunterlist)[ i ], illegalfarms, (int)m_goose_MinForageOpenness, a_farmsizes );
					found1 = CheckDensity( theref, illegalfarms, FarmOccupancy );
				}
				else if (a_ruleset == 12) {
					// Choose a new farm ref to test
					theref = FManager->FindClosestFarmOpennessProbNearRoostIsBest( (*a_hunterlist)[ i ], illegalfarms, (int)m_goose_MinForageOpenness, a_roostlocs );
					found1 = CheckDensity( theref, illegalfarms, FarmOccupancy );
				}
			}
			(*a_hunterlist)[ i ].FarmHuntRef[ h ] = theref;
		}
	}
	delete illegalfarms;
}

void Hunter_Population_Manager::Init()
{
	/**
	* Here we need to create the initial hunter population with each individual with its own hunting pattern
	* in terms of timing, location, and intensity/efficiency.
	* Information on hunters is specified in the Hunter_Hunting_Locations.txt file.
	*
	* One of the challenges is to ensure that we have a matching prey population for each hunter. This is achieved by two structures, the first being
	* a variable in each Population_Manager that states its type (m_population_type) and its associated enum TTypesOfPopulation. The second is a globally
	* visible structure holding a list of any active population managers.
	*
	* This method also initializes any output options.
	*/
	int no_hunters;
	struct_Hunter sh;
	sh.m_L = m_TheLandscape;
	ifstream 	hunterinifile("./Hunter_Hunting_Locations.txt", ios::in);
	if (!hunterinifile.is_open()) {
		m_TheLandscape->Warn( "Hunter_Population_Manager::Init", "Hunter_Hunting_Locations.txt missing" );
		exit( 1 );
	}
	hunterinifile >> no_hunters;
	// Now we skip the header line:
	string line;
	for (int i = 0; i < 19; i++) hunterinifile >> line;
	if (line != "FarmRef10") {
		m_TheLandscape->Warn( "Hunter_Population_Manager::Init", "Header line missing from Hunter_Hunting_Locations.txt" );
		exit( 1 );
	}
	/*
	* Reads the hunter data line by hunter line.\n
	* Each hunter entry is of the following format:\n
	* - Hunter type (0 = Goose Hunter) (int) 
	* - x/y home location (int) 
	* - Hunting day limit, ie how many days the hunter will go out (int) 
	* - Active hunting days of the week, 1 = everyday, 0 weekends only (int) 
	* - Efficiency (% hits) when geese are present (double) 
	* - Daily chance of checking for geese (prob 0.0-1.0) (double) 
	* - Number of hunting locations (int)
	* - Farm reference numbers where they hunt (up to 10).
	*
	*/
	for (int h = 0; h < no_hunters; h++)
	{
		int no_huntlocs;
		hunterinifile >> sh.m_ref >> sh.m_HType >> sh.m_huntingdayslimit >> sh.m_weekend >> sh.m_goosecountchance >> sh.m_efficiency >> sh.m_home.m_x >> sh.m_home.m_y >> no_huntlocs;
		// resize our vectors
		// TODO one day - create another struct to make this a single vector - just to make the code a little more tidy
		sh.m_farms.resize(no_huntlocs);
		sh.m_huntlocrefs.resize(no_huntlocs);
		sh.m_huntlocs.resize(no_huntlocs);
		for (int i = 0; i < no_huntlocs; i++)
		{
			hunterinifile >> sh.m_huntlocrefs[i];
			APoint fp = m_TheLandscape->SupplyFarmManagerPtr()->GetFarmCentroid( sh.m_huntlocrefs[ i ] );
			sh.m_huntlocs[ i ].m_x = fp.m_x;
			sh.m_huntlocs[ i ].m_y = fp.m_y;
			// Need to get the farm* from farmref
			sh.m_farms[i] = m_TheLandscape->SupplyFarmManagerPtr()->GetFarmPtr(sh.m_huntlocrefs[i]);
		}
		for (int i = 0; i < 10-no_huntlocs; i++) 
		{
			hunterinifile >> line; // Skip the NA's
		}
		/*
		 * Here until the full transfer to smart pointers use with PMs the transfer to raw pointer was used (using a GetPopulation() function that returns a raw pointer).
		 * The issue opened to perform the transfer (should be fast and easy task)
		 * */
		if (sh.m_HType == toh_GooseHunter) sh.m_preyPM = this->m_TheLandscape->SupplyThePopManagerList()->GetPopulation(TOP_Goose);
		else
		{
			g_msg->Warn("Hunter_Population_Manager::Init unknown hunter type: ", sh.m_HType);
			exit(0);
		}
		CreateObjects( sh.m_HType, nullptr, &sh, 1 );
	}
	hunterinifile.close();
		
	m_daytime = 0; // Make sure we start the simulation at dawn (as the geese do).
	// Initialise output
	if (HunterCfg.cfg_Hunters_RecordBag.value())
	{
		m_HuntingBagRecord = new ofstream("HuntingBagRecord.txt", ios::out);
		(*m_HuntingBagRecord) << "Year" << '\t' << "Day" << '\t' << "Hour" << '\t' << "Minute" << '\t' << "Season" << '\t' <<
			"HunterRef" << '\t' << "PolygonRef" << '\t' << "x-coord" << '\t' << "y-coord" << '\t' << "GameType" << endl;
	}
	ofstream ofile("HuntingOpportunities.txt", ios::out);
	ofile << "Year" << '\t' << "HunterRef" << '\t' << "HuntingDays" << '\t' << "TotalBag" << '\t' << "NumberOfShots" << '\t' <<
		"GameType0" << '\t' << "GameType1" << '\t' << "GameType2" << '\t' << "GameType3" << '\t' << "GameType4" << '\t' << "GameType5" << endl;
	ofile.close();
}

void Hunter_Population_Manager::CreateObjects(int a_ob_type, TAnimal* /* pvo */, struct_Hunter* p_data,int a_number)
{
	GooseHunter* GHunter = nullptr;
	for (int i=0; i<a_number; i++)
	{
		switch (a_ob_type)
		{
		case toh_GooseHunter:
			if (this->m_TheLandscape->SupplyThePopManagerList()->GetPopulation(TOP_Goose) == nullptr) break; // Don't make goose hunters if we are not running geese
			GHunter = new GooseHunter(p_data, this);
            PushIndividual(a_ob_type,GHunter);
		    break;
		default:
            char ob[ 255 ];
            sprintf( ob, "%d", (int) a_ob_type);
			m_TheLandscape->Warn("Goose_Population_Manager::CreateObjects() unknown object type - ", ob);
			exit(1);
		    break;
		}
	}
}

void Hunter_Population_Manager::DoFirst()
{
	int today = m_TheLandscape->SupplyDayInYear();
	if (m_daytime == 0)
	{
		// Start of day so do some housekeeping.
		// Reset all clocks
		for (unsigned i=0; i<m_ListNameLength; i++)
		{
			int sz = (int) SupplyListSize(i);
			for (int j=0; j<sz; j++)
			{
				auto* h = (GooseHunter*) SupplyAnimalPtr(i,j);
				h->ResetClock();
				h->OnMorning();
				if (h->IsOutHunting()) {
					g_msg->Warn("Hunter_Population_Manager::DoFirst() - hunter out hunting at midnight at ref ", h->GetHuntField());
					g_msg->Warn("Hunter_Population_Manager::DoFirst() - hunter clock is  ", h->GetClock());
					g_msg->Warn("Hunter_Population_Manager::DoFirst() - hunter is leader ", h->IsLeader());
					g_msg->Warn("Day in year is: ", m_TheLandscape->SupplyDayInYear());
					g_msg->Warn("Year is: ", m_TheLandscape->SupplyYearNumber());
					exit(0);
				}
			}
		}
		// Clear all active hunting location information
		for (unsigned i = 0; i < m_ActiveHuntingLocationsPolyrefs.size(); i++) {
			delete m_ActiveHuntingLocationsHunters[ i ];
		}
		m_ActiveHuntingLocationsHunters.clear();
		m_ActiveHuntingLocationsPolyrefs.clear();
		// If end of season, reset bags etc. 
		// Here we only need to ask the first hunter in each list, the answer is the same for the rest 
		for (unsigned i = 0; i < m_ListNameLength; i++) {
			if ((int) SupplyListSize(i) > 0) {
				auto* h = (Hunter*) SupplyAnimalPtr(i,0);
				if (h->IsSeasonEnd( today ) && m_SeasonNumber > 0) {
					ofstream ofile( "HuntingOpportunities.txt", ios::app | ios::out);
					int year = m_TheLandscape->SupplyYearNumber()-1; // This makes it zero based and cuts out the hidden year. 
					int sz = (int) SupplyListSize(i);
					for (int j = 0; j < sz; j++) {
						h = (Hunter*) SupplyAnimalPtr(i,j);
						ofile << year << '\t';
						h->SaveMyData( &ofile );
						h->ResetSeasonData();
					}
					ofile.close();
				}
			}
		}
	}
	m_daytime = ( m_daytime + 10 ) % 1440; // We have 10 minutes timesteps
	if (today == 183 && m_daytime == 1430) m_SeasonNumber++;
}

void Hunter_Population_Manager::RecordHuntingSuccess(int a_poly, int a_birds, int a_hunter)
{
	/**
	* This method stores information about the birds that have been shot. It is essential that the hunting bag record
	* file is open when this is called - there is no checking. This is controlled by a configuration variable cfg_Hunters_RecordBag
	*/
	APoint pt = m_TheLandscape->SupplyCentroid(a_poly);
	(*m_HuntingBagRecord) << m_TheLandscape->SupplyYear() << '\t' << m_TheLandscape->SupplyDayInYear() << '\t' << g_date->GetHour() << '\t' <<
		g_date->GetMinute() << '\t' << GetSeasonNumber() << '\t' << a_hunter << '\t' << a_poly << '\t' << pt.m_x << '\t' << pt.m_y << '\t' << a_birds << endl;

	
}


APoint Hunter_Population_Manager::GetHunterHome(int a_index, int a_list)
{
	return dynamic_cast<Hunter*>(SupplyAnimalPtr(a_list,a_index))->GetHome();
}

APoint Hunter_Population_Manager::GetHunterHuntLoc(int a_index, int a_list, unsigned a_ref)
{
	return dynamic_cast<Hunter*>(SupplyAnimalPtr(a_list,a_index))->GetHuntLoc(a_ref);
}


void Hunter_Population_Manager::HunterLeaderMessage(TypeOfHunterLeaderMessage a_signal, int a_polyref)
{
	/**
	* This is used to relay a message from a leader hunter to any other hunters in a field at the same time as the leader.
	* The leader passes the polygon reference number which is used to find the list of hunters. They are then each called
	* passing them an event specified by a_signal (currently there is only two types of message, but this can be expanded).
	*/
	for (unsigned i = 0; i < m_ActiveHuntingLocationsPolyrefs.size(); i++) {
		if (a_polyref == m_ActiveHuntingLocationsPolyrefs[i])
		{
			for (int h = 1; h < m_ActiveHuntingLocationsHunters[i]->size(); h++)
			{
				switch (a_signal)
				{
				case hlm_shoot:
					(*m_ActiveHuntingLocationsHunters[i])[h]->OnShoot();
					break;
				case hlm_gohome:
					(*m_ActiveHuntingLocationsHunters[i])[h]->OnGoHome();
					break;
				default:
					g_msg->Warn("Hunter_Population_Manager::HunterLeaderMessage unknown leader message: ", a_signal);
					exit(0);
				}
			}
			return;
		}
	}
	g_msg->Warn("Hunter_Population_Manager::HunterLeaderMessage polyref does not match any of the active polygons in list: ", a_polyref);
	exit(0);
}

bool Hunter_Population_Manager::IsPolyrefOnActiveList( int a_polyref ) 
{
	bool found = false;
	for (int m_ActiveHuntingLocationsPolyref : m_ActiveHuntingLocationsPolyrefs) {
		if (a_polyref == m_ActiveHuntingLocationsPolyref) {
			found = true;
			break;
		}
	}
	return found;
}

//*******************************************************************************************************************/
//                                              Class Hunter
//*******************************************************************************************************************/

Hunter::Hunter(struct_Hunter* p_data, Hunter_Population_Manager* p_PPM) : TAnimal(p_data->m_home.m_x, p_data->m_home.m_y, p_data->m_L)
{
	m_OurPopulationManager = p_PPM;
	Init(p_data);
}

Hunter::~Hunter()
{
	;
}

void Hunter::Init(struct_Hunter* p_data)
{
	m_Home = p_data->m_home;
	m_HuntLocs = p_data->m_huntlocs;
	m_HuntLocRefs = p_data->m_huntlocrefs;
	m_NoHuntLocs = int( m_HuntLocs.size() );
	m_CurrentHState = tohts_InitialState;
	m_bag = 0;
	m_myShots = 0;
	m_huntingdays = 0;
	m_baglimit = 999999999; // p_data->m_baglimit;
	m_huntlimit = p_data->m_huntingdayslimit;
	m_goosecountchance = p_data->m_goosecountchance;
	m_efficiency = p_data->m_efficiency;
	m_OurFarmers = p_data->m_farms;
	m_weekend = p_data->m_weekend;
    m_myname = p_data->m_ref;
	m_clock = 0;
	m_lasthuntday = 0;
	m_myMagazine = m_OurPopulationManager->HunterCfg.cfg_hunter_magazinecapacity.value();
	//m_AnnualHuntingAttempts = 0;
}

TypeOfHunterState Hunter::st_ShouldGoHunting( )
{
	/**
	* This is called at the beginning of each day, if it is decided to go hunting then this is done for each 10 minute period.
	* First check we have not shot our limit this year or done our allocated number of days, if so do nothing.
	*/
	if ((m_bag >= m_baglimit) || (m_huntingdays >= m_huntlimit)) return tohts_Resting;
	/** Otherwise we want to find out if we should hunt today - this depends on the season for our game species, and
	* whether we want to hunt today - which depends on whether it is a weekend or not - and then how eager we are.
	*/
	int day = m_OurLandscape->SupplyDayInYear();
	if (!InSeason( day )) return tohts_Resting;
	// is it a weekday?
	/**
	* We've established that it is legal to hunt today but whether the hunter goes depends on how eager he is.
	* This is determined by a probability that is based on the number of days needed to fulfil the hunting day limit, and the number of possible days left in the season.
	* This will also depend on whether they are a weekend hunter or not, and finally whether he checks if there are geese on his hunting locations (done in st_OutHunting ).
	*
	* When all the days are used up, the hunter will no longer enter this decision process.
	*/
	if (IsTodayAPreferredHuntDay(day)) {
		if (IsTodayAChosenHuntDay( day )) {
			return tohts_OutHunting;
		}
	}
	return tohts_Resting; // This is important that it returns resting and not hunting - resting stops the hunter doing anything until the next day.
}

inline bool Hunter::IsTodayAPreferredHuntDay( int a_today ) {
	/**
	* \param[in]  a_today  The day in the year
	* \return true = go hunting today, or false = don't go hunting today
	*
	* Checks whether today is a weekend, if so and weekend hunter then hunt chance today, if not no chance.
	* Otherwise if not a weekend hunter all is good to hunt.
	*/
	if (m_weekend == 0) // 0 means only hunt at weekend
	{
		// 5 & 6 are assumed to be weekends
		if (a_today % 7 > 4)	return true; else return false;
	}
	if (m_weekend == 2) // 2 means leave cfg_hunterrefractionperiod days between hunts
	{

		if (m_OurLandscape->SupplyGlobalDate() - m_lasthuntday >= m_OurPopulationManager->HunterCfg.cfg_hunterrefractionperiod.value())	return true; else return false;
	}
	return true;
}

inline bool Hunter::IsTodayAChosenHuntDay( int a_today ) {
	/**
	* \param[in]  a_today  The day in the year
	* \return true = go hunting today, or false = don't go hunting today
	*
	* Calculates the season length left, then divides the number of hunting days still needed by this to get a probability of going each day.
	* This probability is scaled to hedge our bets for going earlier in the season.
	* Then a probability test is taken and the value returned as true = go today, or false = don't go
	* To calculate this we need the remaining season length. Then if a weekend hunter this is multiplied by 2/7 to get weekends.
	* We assume that we will hedge our bets to some extent, so the probability is based on scaler*daysneeded/dayspossible
	* e.g. if scaler is 2 and we need 2 days and 10 are left, the chance of going is 2*2/10 per day = 40%.
	*/
	auto seasonleft = double( GetSeasonLengthLeft( a_today ) );
	if (m_weekend == 0) seasonleft *= 2.0 / 7.0;
	double prob = m_OurPopulationManager->HunterCfg.cfg_hunterhuntdayprobscaler.value()*(m_huntlimit - m_huntingdays) / seasonleft;
	if (g_rand_uni() < prob) return true;
	return false;
}

TypeOfHunterState Hunter::st_OutHunting( )
{
	/**
	* Must not be called, has to be overridden in descendent classes.
	*/
	return tohts_foobar;
}

TypeOfHunterState Hunter::st_Resting( )
{
	/**
	* Just stays doing this until called out again by the population manager
	*/
	return tohts_Resting;
}

//*******************************************************************************************************************/
//                                              Class Goose Hunter
//*******************************************************************************************************************/

GooseHunter::GooseHunter(struct_Hunter* p_data, Hunter_Population_Manager* p_PPM) : Hunter(p_data, p_PPM)
{
	m_preyPopulationManger = dynamic_cast<Goose_Population_Manager*>(p_data->m_preyPM);
	m_OurFarmers = p_data->m_farms;
	m_leader = false;
	m_pinkfootbaglimit = m_OurPopulationManager->HunterCfg.cfg_hunter_pinkfootbaglimit.value();
	m_greylagbaglimit = m_OurPopulationManager->HunterCfg.cfg_hunter_greylagbaglimit.value();
	Init();
}

GooseHunter::~GooseHunter()
{
	delete m_huntfields;
}

void GooseHunter::Init()
{
	/**
	* Here we need to create a list of possible goose hunting fields in our hunting locations.
	* So we ask for openness scores for any fields with at each of our hunting locations.
	* This is provided by Farm::ListOpenFields
	*/
	m_huntfields = new polylist;
	for (int i = 0; i < m_NoHuntLocs; i++)
	{
		polylist* pl = m_OurFarmers[i]->ListOpenFields((int) m_preyPopulationManger->GooseCfg.cfg_goose_MinForageOpenness.value());
		m_huntfields->insert(m_huntfields->begin(), pl->begin(), pl->end()); // add the new one to the main polylist
		delete pl;
	}
	ResetClock();
	ResetSeasonData();
}


int GooseHunter::CheckForGame() {
	/**
	* \return int a polygon reference if we are to go hunting, or -1 = don't go hunting today
	*
	* The hunter checks for legal game at their hunting fields if they are set to do so, otherwise they take a random shot at a place. \n
	* If they don't check then they automatically assume they can go out hunting.\n
	* Here we assume they check all their hunting locations and pick the best.
	* We might also want as a later refinement to know how many geese because and only go out when there are a lot.
	*/
	int sz = int(m_huntfields->size());
	if (g_rand_uni() >= m_goosecountchance) {
		int thepolyref = (int) (*m_huntfields)[int(g_rand_uni()*sz)]; // returns 0 to sz-1;
		return thepolyref;
	}
	else {
		// We are checking today, so check out hunting fields
		int polyref = -1;
		int maxgeese = 0;
			for (int hfield = 0; hfield < sz; hfield++) {
				int geese =  m_OurLandscape->GetQuarryNumbers((int) (*m_huntfields)[hfield]);
				if (geese > maxgeese) {
					maxgeese = geese;
					polyref = (int) (*m_huntfields)[hfield];
				}
			}
		return polyref;
	}
}

bool GooseHunter::IsSeasonEnd(int day) {
	int end = m_OurPopulationManager->GetHuntingSeasonEnd();
	if (end == 365){
		m_OurLandscape->Warn("GooseHunter::IsSeasonEnd()", "Hunting season end at day 365. First day of the year is 0 hence last day of year is 364");
		exit(1);
	}
	if (day == 1 + end) return true;
	if (day == 0 && end == 364) return true; // if the hunting season ends at the end of the year, then day 0 is after it.
	return false;
}

bool GooseHunter::InSeason(int day) {
	/** The goose hunting season is complicated because it runs over a year boundary (or it can)
	Particularly the first year of the simulation is tricky, because we start out in January which can be legal hunting season,
	but the geese don't arrive until the fall.
	*/
	int start = m_OurPopulationManager->GetHuntingSeasonStart();
	int end = m_OurPopulationManager->GetHuntingSeasonEnd();
	if (m_OurPopulationManager->GetSeasonNumber() == 0) {

		return false;  // We don't want them to start in January in the first year of the sim.
	}
	if (start > end) {
		if ((day < start) && (day > end)) return false;
		else {
			return true;  // must be in the first year after the start of season
		}
	}
	else {
		// Season all in one year
		if (day < start || day > end) return false;
		else {
			return true;
		}
	}
}
int GooseHunter::GetSeasonLengthLeft(int day) {
	if (m_OurPopulationManager->GetSeasonNumber() == 0) return 0;  // We don't want them to start in January in the first year of the sim.
	int left;
	int start = m_OurPopulationManager->GetHuntingSeasonStart();
	int end = m_OurPopulationManager->GetHuntingSeasonEnd();
	/** The goose hunting season is complicated because it runs over a year boundary (or it can) */
	if (start > end) {
		// We have a year boundary issue, if the day is in the autumn then we have til the end of year + next year, otherwise we must be in next year already.
		if (day >= start) left = (365 - day) + end;
		else left = end - day;
	}
	else {
		left = end - day;
	}
	// We don't want to return a negative number of days, so rather want to say no days left, i.e. 0
	if (left < 0) return 0;
	return left;
}

TypeOfHunterState GooseHunter::st_ShouldGoHunting()
{
	/**
	* Here we check for any goose specific bag limits
	*/
	if ((m_goosebag[gst_PinkfootFamilyGroup] + m_goosebag[gst_PinkfootNonBreeder]) >= m_pinkfootbaglimit) return tohts_Resting;
	/** Otherwise we want to find out if we should hunt today - this depends on the season for our game species, and
	* whether we want to hunt today - which depends on whether it is a weekend or not - and then how eager we are.
	*/
	else
	{
		return(Hunter::st_ShouldGoHunting());
	}
	
}


TypeOfHunterState GooseHunter::st_OutHunting()
{
	/**
	* First at last check if the hunter checks for geese before going, if there are none he does not go out.
	* Simulates a 10 minute hunting timestep.
	* Must determine the time spent hunting and the time of day.
	* Then wait for geese, and determine the probability of shooting them.
	* Once the hunting time is used up, then come home.
	*/
	m_clock += 10;  // this is not strictly necessary, but makes useful debug info (only leader cares)
	if (!m_dugin) {
		m_huntfield_polyref = CheckForGame();
		if (m_huntfield_polyref == -1) return tohts_Resting;
		else {
			m_huntingdays++;
			m_dugin = true;
			m_leader = m_OurPopulationManager->AddHunterHunting(m_huntfield_polyref, this);
			m_lasthuntday = m_OurLandscape->SupplyGlobalDate();
			return tohts_OutHunting;
		}
	}
	if (m_leader) {
		// Only the leader assesses this 
		if (m_clock > m_OurPopulationManager->HunterCfg.cfg_huntlength.value())
		{
			m_dugin = false;
			m_OurPopulationManager->HunterLeaderMessage(hlm_gohome, m_huntfield_polyref);
			m_huntfield_polyref = -1;
			return tohts_Resting; // Come home
		}
		/** We have a place to hunt - so look and see if there is anything to shoot
		* Must assess a range from our location - we will be centrally placed in a field - so the real location is not precise beyond this resolution.
		* Birds will also be located at the field level, so a simplifying assumption here is that a bird in the same field is 'fair game', outside the field it is not.
		* Once we determine that there is something to shoot at, then we need to determine whether, we try and shoot, if so whether we hit the bird.
		* If we shoot this must be registered in the system as an event, and any goose near enough to be affected will need to be told of this.
		*
		* m_huntloc_index holds the polyref, from which we can get access to the forage locations in the goose manager - this has a list of who is there at any one
		* moment in time.
		*
		* When assessing whether to shoot the mix of the birds present will be critical. If it is primarily Barnacles then these are protected so the hunter will not shoot
		* if not he can go ahead. The current solution just ignores barnacle geese - they cannot be shot and are not counted in the number of geese on a field.
		*
		* A further consideration is whether the geese land near to the hunter. If too far away then they will not shoot. This depends on the area of the field. We assume
		* that any field of less than 2Ha, then the geese are close enough, but if over this then there is a cfg_largefieldgooseproximity chance of shooting.
		*/
		double fieldsize = m_OurLandscape->SupplyPolygonArea( m_huntfield_polyref );
		double shootingchance = 1.0;
		if (fieldsize > m_OurPopulationManager->HunterCfg.cfg_largefieldgooseproximitysizecutoff.value()) {
			shootingchance = m_OurPopulationManager->HunterCfg.cfg_largefieldgooseproximity.value();
		}
		/*double pct = 0.0;*/
		int birds = m_preyPopulationManger->BirdsToShootAtPoly(m_huntfield_polyref /*, pct */);
		
		if ((birds > 0) && (g_rand_uni() < shootingchance)  /* && (pct<cfg_huntermaxprotectedpct.value())*/	) {
			/**
			* To Determine if we try to shoot and the chance of hitting, right now we assume we always shoot and the chance of hitting something is our skill - held in
			* m_efficiency. The hunter has as many shots as is in his magazine (determined by the member variable m_myMagazine)
			*
			* When a bird is shot the message is sent to the goose population manager. This works out which birds are shot and removes them, calling back with an OnShotABird message so the hunter
			* can record what has been shot. It is here that the bag record is kept.
			*/
			int numbershot = 0;
			for (int i = 0; i < m_myMagazine; ++i) {
				if ((birds > i) || (numbershot == 0))
				{
					if (g_rand_uni() < m_efficiency) numbershot++;
					m_myShots++;
				}
			}
			if (numbershot > 0) {
				m_preyPopulationManger->BirdsShot(m_huntfield_polyref, numbershot, this);
			}
			m_OurPopulationManager->HunterLeaderMessage(hlm_shoot, m_huntfield_polyref);
			// All shots are fired then make some sound
			// Must scare any birds off that are near here and get them to remember the event
			m_preyPopulationManger->BangAtPoly(m_huntfield_polyref);
		}
	}
	return tohts_OutHunting;
}

void GooseHunter::OnShotABird(int a_birdtype, int a_poly) {
	m_bag++;
	m_goosebag[a_birdtype]++;
	if (m_OurPopulationManager->HunterCfg.cfg_Hunters_RecordBag.value()) m_OurPopulationManager->RecordHuntingSuccess(a_poly, a_birdtype, m_myname);
}

void GooseHunter::OnShoot() {
	/**
	* This is a message handler for the situation when a hunter is in a team but not the leader. When the leader decides to shoot the other team members hunter will shoot.
	* When a bird is shot the message is sent to the goose population manager. This works out which birds are shot and removes them, calling back with an OnShotABird message so the hunter
	* can record what has been shot. It is here that the bag record is kept.
	*/
	int birds = m_preyPopulationManger->BirdsToShootAtPoly(m_huntfield_polyref /*, pct */);
	int numbershot = 0;
	m_myShots++;
	if (g_rand_uni() < m_efficiency) numbershot++;
	if (birds > 1){
		if (g_rand_uni() < m_efficiency) numbershot++;
		m_myShots++;
	}
	if (numbershot > 0) {
		m_preyPopulationManger->BirdsShot(m_huntfield_polyref, numbershot, this);
	}
}

void GooseHunter::OnGoHome() {
	m_dugin = false;  // End of the hunt, so no longer dugin
	m_leader = false;  // Hunt is over, so nothing to lead.
	m_huntfield_polyref = -1;  // This has to be reset. We find a new one next time we go hunting.
	m_CurrentHState = tohts_Resting; // Come home
}


void GooseHunter::Step( )
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentHState)
	{
	case tohts_InitialState: // Initial state always starts by setting the behaviour to tohts_Hunting
		m_CurrentHState = tohts_Hunting;
		break;
	case tohts_Hunting:
		m_CurrentHState = st_ShouldGoHunting();
		break;
	case tohts_OutHunting:
		m_CurrentHState = st_OutHunting();
		break;
	case tohts_Resting:
		m_CurrentHState = st_Resting();
		break;
	default:
		m_OurLandscape->Warn("GooseHunter::Step()","unknown state - default");
		exit(1);
	}
}

void GooseHunter::SaveMyData( ofstream* a_ofile ) {
	(*a_ofile) << m_myname << '\t' << GetHuntingDays() << '\t' << GetBag() << '\t' << GetShots();
	for (int i : m_goosebag) {
		(*a_ofile) << '\t' << i;
	}
	(*a_ofile) << endl;

}

bool Hunter_Population_Manager::AddHunterHunting( int a_polyref, Hunter* a_hunter ) {
	/**
	* The field that a hunter is hunting is referenced by a_polyref. First we need to check whether this location already exists. NB this list is emptied every day, so only contains
	* locations where hunters have been or are allocated today.
	*/
	for (unsigned i = 0; i < m_ActiveHuntingLocationsPolyrefs.size(); i++) {
		if (m_ActiveHuntingLocationsPolyrefs[ i ] == a_polyref) {
			m_ActiveHuntingLocationsHunters[ i ]->push_back( a_hunter );
			/** If the location is found, then the hunter is not a leader and we need to tell them that (returns false) */
			return false;
		}
	}
	/** If the polyref does not already exist the location was not found, it must be new, so we need to add it to the list and tell the hunter he is a leader */
	auto* p_newhunterlist = new HunterList;
	p_newhunterlist->push_back( a_hunter );
	m_ActiveHuntingLocationsHunters.push_back( p_newhunterlist );
	m_ActiveHuntingLocationsPolyrefs.push_back( a_polyref );
	return true;
}

void Hunter_Population_Manager::SetHuntingSeason() {
	/** The goose hunting season is complicated because it runs over a year boundary (or it can) */
	int start1 = (static_pointer_cast<Goose_Population_Manager>) (m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(TOP_Goose))->GooseCfg.cfg_goose_pinkfootopenseasonstart.value();
	int end1 = (static_pointer_cast<Goose_Population_Manager>) (m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(TOP_Goose))->GooseCfg.cfg_goose_pinkfootopenseasonend.value();
	int start2 = (static_pointer_cast<Goose_Population_Manager>) (m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(TOP_Goose))->GooseCfg.cfg_goose_greylagopenseasonstart.value();
	int end2 = (static_pointer_cast<Goose_Population_Manager>) (m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(TOP_Goose))->GooseCfg.cfg_goose_greylagopenseasonend.value();
	/*Both start and end in the same year*/
	if (end1 > start1 && end2 > start2) {
		m_HuntingSeasonEnd = end1;
		if (m_HuntingSeasonEnd < end2)
		{
			m_HuntingSeasonEnd = end2;
		}
	}
	/*1 end in the second year*/
	if (start1 > end1 && start2 < end2)
	{
		m_HuntingSeasonEnd = end1;
	}
	/*2 end in the second year*/
	if (start1 < end1 && start2 > end2)
	{
		m_HuntingSeasonEnd = end2;
	}
	/*Both end in the second year*/
	if (start1 > end1 && start2 > end2)
	{
		m_HuntingSeasonEnd = end1;
		if (m_HuntingSeasonEnd < end2)
		{
			m_HuntingSeasonEnd = end2;
		}
	}
	if (start1 > start2)
	{
		m_HuntingSeasonStart = start2;
	}
	else m_HuntingSeasonStart = start1;
	if (m_HuntingSeasonStart < 0)
	{
		g_msg->Warn("Goose_Population_Manager::SetHuntingSeason() Start of hunting season:", m_HuntingSeasonStart);
		exit(1);
	}
	if (m_HuntingSeasonEnd < 0)
	{
		g_msg->Warn("Goose_Population_Manager::SetHuntingSeason() End of hunting season:", m_HuntingSeasonEnd);
		exit(1);
	}
}