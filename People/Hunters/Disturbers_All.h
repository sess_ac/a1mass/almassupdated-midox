//
// Created by andrey on 3/9/21.
//

#ifndef ALMASS_DISTURBERS_ALL_H
#define ALMASS_DISTURBERS_ALL_H


//#include <Landscape/Landscape.h>
//#include "Hunters_all.h"

#include <blitz/array.h>
#include "../../Landscape/ls.h"
#include <unordered_set>
#include <utility>
#include "BatchALMaSS/PopulationManager.h"
#ifdef __USE_BLITZ_RANDOM
#include <blitz/random/uniform.h>
using namespace ranlib;
#endif
#include "BatchALMaSS/ALMaSS_Random.h"

using namespace std;
class Disturbers;
class Disturber_Population_Manager;

/**
 * \brief Disturber (not sure if this is the right name) is a type of Hunter
 * that always misses (even though that does not need to be necessarily true and we can address part of the general
 * mortality through it) and essentially just scares the animal it is paired with.
 * One could think of a Disturber as of a walker/predator/car/farmer/tractor that by appearing in the landscape
 * urges the animal to stop its current behaviour and hide/escape etc
 *
 * In the beginning I will implement a single type of Disturber: StaticDisturber. It is just sitting quietly on the allocated
 * place and have a random chance of bang at each timestep
 * In future the Disturber class can be a base class for the whole range of different disturbances. Those ones, that we
 * are not interested in detailed implementation of, and (unlike predators in predator-prey model) are defined solely through interaction with the model in question.
 * When in the beginning we will allocate the Disturber to its home,
 * some areas are more likely than the others. For example populated areas/roads/farms are more likely to be a source of
 * the human-caused disturbances.
 * The Disturber could be of different types: carDisturber can move only on the road
 * TractorDisturber only on the field PredatorDisturber can appear everywhere and can move far
 * When on hunt Disturber moves between the locations and bangs at random times.
 *
 * */
typedef vector < std::unique_ptr<Disturbers> > smartTListOfDistubers;

typedef enum class
Disturber_types{
    StaticGooseDisturber_type0=0,
    //StaticGooseDisturber_type1=1,
    foobar
} TTypeOfDisturbers;

/**
\brief
Used for creation of a new disturber object
 For now we will comment it all out. In the current implementation there will be only
 a population manager
*/

class struct_Disturbers
{
public:

    unordered_set<TTypesOfLandscapeElement> m_DisturberProhibitedToles{};
    int DisturbersNo;
    Landscape* L;

    int m_x;
    int m_y;
    int m_ScalingFactor;

};

//*******************************************************************************************************************/
//                                        Base Class Disturber
//*******************************************************************************************************************/
//This is a base class of Disturbers
//The more specific Disturber types will inherit it The population manager will unite N of them
class Disturbers {
 protected:

     /** The range around the disturber home where it can travel (for the dynamic disturbers)*/
//     const double m_MaximumTravelDistance{0};
     /** The period (days of the year) this disturber is active*/
     const int m_ActivityStartDate{0};
     const int m_ActivityEndDate{365};
     /** The time of the day (if it is ran in timesteps shorter than a day) this disturber is active*/
     const int m_ActivityStartTime{0}; // for geese 0 is sunrise
     const int m_ActivityEndTime{720}; // for 12 hours of activity by default (72 ten mins intervals)
     const bool m_ActiveInTheNight{false};
     const bool m_ActiveInTheDay{true};
     /** The chance of bang in each timestep */
     double m_BangChance{0.001};
     /** The start of the interval after which the BangChance is estimated */
     double m_BangInterval_start{0};
     /** The pointer to boolean map that defines whether there is a disturber */
     shared_ptr<blitz::Array<bool,2>> m_DisturbersMap;

     shared_ptr<Disturber_Population_Manager> m_myDPM;

     unique_ptr<blitz::Array<bool,2>> m_Allowed_tiles;

     int m_NumberAllowed;
     int m_x;
     int m_y;
     int m_ScalingFactor;
     Landscape* m_TheLandscape;
     unordered_set<TTypesOfLandscapeElement> m_DisturberProhibitedToles;



 public:


    Disturbers(shared_ptr<Disturber_Population_Manager>, std::unique_ptr<struct_Disturbers>);
    void ResetMap();
    bool IsActive(int time, bool a_daylight, int day) const{
        if(time>=m_ActivityStartTime&&time<m_ActivityEndTime
        &&day>=m_ActivityStartDate&&day<m_ActivityEndDate&&
                ((a_daylight&&m_ActiveInTheDay)||(!a_daylight&&m_ActiveInTheNight)))
            return true;
        return false;
    };
    //void CalcDisturbedTiles();
    //vector<APoint> SupplyDisturbedTiles(){
     //   return m_DisturbedCoords; // is copy-constructing ok here?
    //};
     shared_ptr<blitz::Array<bool,2>> SupplyDisturbersMap(){
        return m_DisturbersMap;
    }

    void ResetInterval();
    int m_DisturbersNo{0};
    void Init();
    bool IsDisturbed(float, int, int);
};

//*******************************************************************************************************************/
//                                        Class StaticGooseDisturber
//*******************************************************************************************************************/
class StaticGooseDisturber : public Disturbers{
public:

    StaticGooseDisturber(shared_ptr<Disturber_Population_Manager> DPM, unique_ptr<struct_Disturbers> data):Disturbers(std::move(DPM), std::move(data)){;};
};
//*******************************************************************************************************************/
//                                        Class Disturber_Config
//*******************************************************************************************************************/
class DisturberConfigs{
public:
    DisturberConfigs()=default;
    CfgArray_Int cfg_disturber_reset_days{"DISTURBERS_RESETDAYS", CFG_CUSTOM, 1, vector<int> {364}};
};
//*******************************************************************************************************************/
//                                        Class Disturber_Population_Manager
//*******************************************************************************************************************/

class Disturber_Population_Manager : public Population_Manager, public enable_shared_from_this<Disturber_Population_Manager>
{
protected:
    /** \brief Size of the landcape in x dimension*/
    int m_x;
    /** \brief Size of the landcape in y dimension*/
    int m_y;
    /** \brief The number of different disturber lists: they can have a similar or different type    * */

    int m_NumOfDisturbers;
    /**variable that defines the default number of disturbers*/
    CfgInt cfg_DefaultNoDisturbers;
    /**\brief The number of Disturbed coords in this particular timestep */
    int m_DisturbedNum{0};
    std::vector<int> m_DisturbersResetDate;
    std::unique_ptr< smartTListOfDistubers > TheArray_new{nullptr};
    /**The map that is used for seeding the Disturbers*/
    unique_ptr<blitz::Array<double,2>> m_RandomSeedMap;
    TTypesOfPopulation m_Target;
    /**\brief a scaling factor of the map in each dimension
     * essentially the size of the disturber on the map*/
    int m_ScalingFactor{1};
    /**THe following work only with Goose Model, since it goes by the timesteps of ten minutes starting from sunrise*/
    /**Therefore it should be moved to a specific GooseDisturberPopulation Manager
     * */
    int m_daytime{-10};
    bool m_daylight;

    // methods
    void CreateObjects(TTypeOfDisturbers , std::unique_ptr<struct_Disturbers> data,int number);
public:
    // variables
    DisturberConfigs DisturberCfg;
#ifdef __USE_BLITZ_RANDOM
    Uniform<float> m_RNG;
#endif
    // methods
    void Init();
    /** \brief Disturber population manager constructor */
    Disturber_Population_Manager(int, Landscape *, TTypesOfPopulation );
    /** \brief Disturber population manager destructor */
    ~Disturber_Population_Manager() override;
    /** \brief The function that resets the disturbers */
    void Reset_Disturbers();
    //bool IsDisturber(int , int );
    void Run(int NoTSteps) override;
    void DoFirst() override;
    void DoLast() override;
    int SupplyDayTime() const{return m_daytime;};
    bool SupplyDayLight() const{return m_daylight;};

     int SupplyDisturbedNumber() const{return m_DisturbedNum;}
    bool IsDisturbed(int, int);
    void ResetSeedMap();
};

#endif //ALMASS_DISTURBERS_ALL_H