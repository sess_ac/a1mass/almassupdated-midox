//-------------------------
#ifndef Farmer_BaseH
#define Farmer_BaseH
//-------------------------

class SocialNodes;
class SocialNetworkSpatial;
class SocialNetworkAssociative;
class SocialNetworkVirtual;
class Landscape;

extern MapErrorMsg* g_msg;

using namespace std;

enum TypeOfFarmerState : int {
	tfs_InitialState = 0,
	tfs_DoAnnualAccounting,
	tfs_UpdateSocialBasedAttributes,
	tfs_OrdinaryManagement,
	tfs_UpdateManagement
};
enum TypeOfConsumatBehav : int {
	tcs_InitialState = 0,
	tcs_Repetition,
	tcs_Imitation,
	tcs_Socialcomparison,
	tcs_Optimization,
};

/** \brief A class to contain information on the farmer's farming style */
class FarmerStyle
{
	/**
	* A data class that holds the information on modification of probabilities of taking different types of farming action.
	* These are related to categories of farm management activities, intitially limited to pesticides and fertilizer application.
	* However, other characteristics could be added here, e.g. liklihood of using a no-tillage management plan, or replacing chemical with mechanical weed control.
	*/
public:
	/** \brief Returns the pesticide style */
	double GetPesticideStyle() { return m_FStyle_Pesticides; };
	/** \brief Returns the fertilizer style */
	double GetFertilizerStyle() { return m_FStyle_Fertilizer; };
	/** \brief Sets the pesticide style */
	void SetPesticideStyle(double a_style) { m_FStyle_Pesticides = a_style; };
	/** \brief Sets the fertlizer style */
	void SetFertilizerStyle(double a_style) { m_FStyle_Fertilizer = a_style; };
protected:
	double m_FStyle_Pesticides;
	double m_FStyle_Fertilizer;
};

class Farmer : public TAnimal
{
	/**
	* The Farmer class includes all behaviour assoicated with farming decisions, economics and social networks.
	* The Farmer can access all information on his farm, and can interact to alter farm management activities.
	* Any runtime errors caught by ALMaSS for the farmer should return 142 on exit.
	*/
protected:
	//**************** Social Networks *************************
	/** \brief The farmer's spatial social network */
	SocialNetworkSpatial* m_spatialNetwork;
	/** \brief The farmer's associative social network */
	SocialNetworkAssociative* m_associativeNetwork;
	/** \brief The farmer's virtual social network */
	SocialNetworkVirtual* m_virtualNetwork;
	/** \brief The farmer's farmer list */
	FarmerList* m_farmerlist;
	/** \brief The farmer's neighbours choosen for the interaction */
	vector<Farmer*>m_neighbours;

	static bool m_useonlyspatial;
	static int m_neighbours_spatial;
	static int m_neighbours_virtual;
	static int m_neighbours_associative;

	//**************** Decision making and social attributes *********************
	/** \brief A measure of the farmers tendency to risk aversion (range 0-1.0) */
	double m_envAttitude;
	/** \brief A measure of the farmers attitude to the environment (range 0-1.0) */
	double m_riskaversion;
	/** \brief A measure of the farmers uncertainty to risk aversion (range 0-1.0) */
	double m_riskspan;
	/** \brief A temporary version of the measure of the farmers tendency to risk aversion (range 0-1.0) */
	double m_envAttitude_temp;
	/** \brief A temporary version of the measure of the farmers attitude to the environment (range 0-1.0) */
	double m_riskaversion_temp;
	/** \brief The farmers age */
	int m_Age;
	/** \brief The farmers collective if any (-1 if not) */
	int m_CollectiveRef;
	/** \brief Holds the current farmer state */
	TypeOfFarmerState m_CurrentFarmerState;
	/** \brief Holds the current farmer CONSUMAT state */
	TypeOfConsumatBehav m_consumatbehave;
	/** \brief The farming style of the farmer */
	FarmerStyle m_MyFarmingStyle;

	/** \brief This holds the use of consumat flag */
	static bool m_useConsumat;

	//**************** Economics *******************************
	/** \brief The farmer's annual economic balance going back as far as the farmer existed */
	vector<double>  m_annualprofitloss;
	/** \brief The farmer economic balance since last annual audit */
	double m_runningprofit;
	/** \brief The farmer economic balance since last annual audit */
	double m_runningloss;
	/** \brief The farmer labour use since last annual audit */
	double m_runninglabourusage;
	/** \brief A pointer to the farmers farm */
	Farm* m_OurFarm;
	/** \brief A score for the current risk status */
	double m_RiskScore;
	/** \brief A score for the current environmental status */
	double m_EnvironmentScore;

	FarmManager* m_MyFarmManager;
	int m_FarmerRef;

public:
	/** \brief The constructor for the Farmer class */
	Farmer(int a_x, int a_y, Landscape* L, FarmerList* a_farmerlist, Farm* a_farm);
	/** \brief The destructor for the farmer class */
	~Farmer();
	/** \brief This is the Step method for farmers, where behaviour is done on a daily basis recursively */
	virtual void Step();
	/** \brief This is the EndStep method for farmers, where behaviour is done on a daily basis */
	virtual void EndStep();

	TypeOfFarmerState st_OrdinaryManagement();

	TypeOfFarmerState st_UpdateSocialBasedAttributes();

	TypeOfFarmerState st_DoAnnualAccounting();

	TypeOfFarmerState st_UpdateManagement();

	/** \brief Function to inizialize member variables when a farmer obj is created */
	void InitializeVariables();

	/** \brief Function set parameter values */
	static void SetStaticParameterValues();
	/** \brief Uses farmer attributes to set the farmer style values */
	void UpdateFarmerStyle();

	//********************** Economics  *******************************
	/** \brief Sets all the personal attributes for the farmer (and creates values where needed) */
	void SetPersonalAttributes(FarmerAttributes Attributes);
	/** \brief Get all the personal attributes for the farmer */
	FarmerAttributes GetPersonalAttributes();
	/** \brief Change score for the current risk status */
	void ChangeRiskScore(double a_change) { m_RiskScore += a_change; }
	/** \brief A score for the current environmental status */
	void ChangeEnvironmentScore(double a_change) { m_EnvironmentScore += a_change; }
	/** \brief Adds the amount to the current running profit for this year */
	void AddProfit(double a_value) { m_runningprofit += a_value; }
	/** \brief Adds the amount to the current running profit for this year */
	void AddCost(double a_value) { m_runningloss += a_value; }
	/** \brief Adds the amount of labour to the current running labour cost for this year */
	void AddLabourCost(double a_value) { m_runninglabourusage += a_value; }
	/** \brief Update the risk aversion after the interaction with neighbours*/
	void UpdateRiskaversion();
	/** \brief Update the risk aversion after the interaction with neighbour i */
	void UpdateRiskaversion(int i);
	/** \brief Records the management information for the farmer in terms of no of different categories of treatments performed */
	void CheckManagementImpacts(FarmEvent* ev);

	//**************** Management  *******************************
	void Imitate();
	void SocialCompare();
	void Optimize();
	void Repeat();


	//**************** Social Networks  *******************************
	/** \brief Calls the update method for the spatial social network */
	void UpdateSpatialNetwork();
	/** \brief Calls the update method for the associative social network */
	void UpdateAssociativeNetwork();
	/** \brief Calls the update method for the virtual social network */
	void UpdateVirtualNetwork();
	/** \brief Choose the neighbour to interact with, "a" is the number of neighbours choosen from the spatial network*/
	void ChooseNeighbours();
	/** \brief Swap temporary attitude variables to the actual ones */
	void SwapAttitudeTempReal() {
		m_envAttitude = m_envAttitude_temp;
		m_riskaversion = m_riskaversion_temp;
	}
	//************* General Get/Set methods ***************************
	/** \brief Gets the current risk aversion measure 0-1.0 */
	double Get_riskaversion() { return m_riskaversion; }
	/** \brief Gets the current risk aversion span 0-1.0 */
	double Get_riskspan() { return m_riskspan; }
	/** \brief Gets the current running loss for this year */
	double Get_runningprofit() { return m_runningprofit; }
	/** \brief Gets the current running loss for this year */
	double Get_runningloss() { return m_runningloss; }
	/** \brief Gets the annual profit loss for previous years. 0 = this year (always zero), 1 year before etc.. */
	double Get_annualprofitloss(int a_year) { 
		if (a_year < m_annualprofitloss.size())  return m_annualprofitloss[a_year];
		g_msg->Warn("Farmer annualprofitloss request out of bounds", a_year);
		exit(142);
	}
	/** \brief Sets the current risk aversion measure 0-1.0 */
	void Set_riskaversion(double a_value) { m_riskaversion = a_value; }
	/** \brief Sets the current risk span measure (0-1.0) */
	void Set_riskspan(double a_value) { m_riskspan = a_value; }
	/** \brief Sets the current running profit  */
	void Set_runningprofit(double a_value) { m_runningprofit = a_value; }
	/** \brief Sets the current running profit/loss  */
	void Set_runningloss(double a_value) { m_runningloss = a_value; }
	/** \brief Sets the annual profit or loss for last year */
	void Set_annualprofitloss(double a_value) {
		/** Puts the value at location 1, so grows the list historically from with the newest value at 1, then 2 etc.. */
		m_annualprofitloss.insert(m_annualprofitloss.begin() + 1, a_value);
	}
	/** \brief Gets the farm owned by the farmer*/
	Farm* GetFarm() { return m_OurFarm; }
	/** \brief Gets farm area */
	double GetFarmArea()
	{
		return m_OurFarm->GetAreaDouble();
	}
	
	void SetFarmerRef(int a_FarmerRef) { m_FarmerRef = a_FarmerRef; }
	
	int GetFarmerRef() { return m_FarmerRef; }
	
	/** \brief Gets the the Farm Manager */
	FarmManager* GetFarmManager() { return m_MyFarmManager; }
	/** \brief Returns the farmer style */
	FarmerStyle GetFarmerStyle() { return m_MyFarmingStyle;  }

	static void SetUseSocioEconomics(bool a_flag);
};

#endif