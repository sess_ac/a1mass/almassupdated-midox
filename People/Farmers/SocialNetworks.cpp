/*******************************************************************************************************
Copyright(c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.

Redistributionand use in sourceand binary forms, with or without modification, are permitted provided
that the following conditions are met :

Redistributions of source code must retain the above copyright notice, this list of conditionsand the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditionsand
the following disclaimer in the documentationand /or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*********************************************************************************************************/


#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../../Landscape/ls.h"
#include "../../BatchALMaSS/PopulationManager.h"
#include "SocialNetworks.h"
#include "Farmer.h"
#include <random>
extern std::default_random_engine g_std_rand_engine;

SocialNetwork::SocialNetwork(FarmerList* a_farmerlist, Farmer* a_owner)
{
	m_FarmerList = a_farmerlist;
	m_Owner = a_owner;
	UpdateNetwork();
}

void SocialNetworkSpatial::UpdateNetwork()
{
	/**
	* Updates the network by (re-)testing all links between all farmers
	* This version assumes all farmers need to be considered, but should be overridden in descendent classes
	* Uses the functionality of the FarmerList to generate the information that is used
	*/
	m_Nodes.resize(0);
	int NoFarmers = int(m_FarmerList->GetSize());
	vector<int>distances;
	APoint loc1, loc2;
	loc1 = m_Owner->SupplyPoint();
	for (int i = 0; i < NoFarmers; i++)
	{
		if (m_Owner != m_FarmerList->GetFarmer(i))
		{
			loc2 = m_FarmerList->GetLocation(i);
			int distx = abs(loc1.m_x - loc2.m_x);
			int disty = abs(loc1.m_y - loc2.m_y);
			distx = distx * distx; // Square it for x
			disty = disty * disty; // and for y
			// Will need to add code to skip comparing with self (could be based on zero distance)
			double distance = sqrt(distx + disty); // Calculate diagonal distance (could also just sum the orthoganal distance for less accuracy more speed)
			double strength;
			
			if (distance == 0) strength = 0; else {
				// strength = m_Owner->GetFarmArea() ;
				strength = m_FarmerList->GetFarmer(i)->GetFarmArea() / distance;
				//strength = 1.0; // add calculated strength
			}
			SocialNode node;
			node.SetStrength(strength);
			node.SetFarmer(m_FarmerList->GetFarmer(i));

			m_Nodes.push_back(node);
		}
		sort(m_Nodes.begin(), m_Nodes.end(), CompareStrength());

	}

}
Farmer* SocialNetworkSpatial::GetNeighbour(int i) {
	Farmer* a_neighbour;
	if (i == 0) {
		return m_Nodes.begin()->GetFarmer();
	}
	else
	{
		a_neighbour = m_Nodes[i].GetFarmer();
		return a_neighbour;
	}
	
}
Farmer* SocialNetworkSpatial::GetNeighbour() {
	return m_Nodes.begin()->GetFarmer();
}

void SocialNetworkSpatial::PrintNetworkSpatial_Strenght()
{
	/** Opens a stream file and output farmers' social network spatial (force between farmers) */
	string str1 = "SocioEconomic\\Social_Network_Spatial_Strenght.txt";
	if (g_date->OldDays() == 0) {
		ofstream ofile(str1.c_str(), ios::out);
		if (!ofile.is_open()) {
			g_msg->Warn("FarmManager::PrintNetworkSpatial_Strenght() Cannot open outpuit file ", "Social_Network_Spatial_Strenght.txt");
			exit(142);
		}
	}
	ofstream ofile(str1.c_str(), ios::app);
	if (!ofile.is_open()) {
		g_msg->Warn("FarmManager::PrintNetworkSpatial_Strenght() Cannot open outpuit file ", "Social_Network_Spatial_Strenght.txt");
		exit(142);
	}
	Farmer* farmer = m_Owner;
	ofile << farmer->GetFarmerRef();
	for (int i = 0; i < m_Nodes.size(); i++) {
		ofile << '\t' << m_Nodes[i].GetStrength();
	}
	ofile << endl;
	ofile.close();
}
void SocialNetworkSpatial::PrintNetworkSpatial(){

	/** Opens a stream file and output farmers' social network spatial (only farmer referencce) */
	string str2 = "SocioEconomic\\Social_Network_Spatial.txt";
	if (g_date->OldDays() == 0) {
		ofstream ofile(str2.c_str(), ios::out);
		if (!ofile.is_open()) {
			g_msg->Warn("FarmManager::PrintNetworkSpatial() Cannot open outpuit file ", "Social_Network_Spatial.txt");
			exit(142);
		}
	}
	ofstream ofile2(str2.c_str(), ios::app);
	Farmer* farmer = m_Owner;
	ofile2 << farmer->GetFarmerRef();;
	for (int i = 0; i < m_Nodes.size(); i++) {
		
		ofile2 << '\t' << m_Nodes[i].GetFarmer()->GetFarmerRef();
	}
	ofile2 << endl;
	ofile2.close();
}

void SocialNetworkAssociative::UpdateNetwork() {
	;
}

Farmer* SocialNetworkAssociative::GetNeighbour(int i)
{
	return m_Colleagues.at(i);
	
}

void SocialNetworkVirtual::UpdateNetwork() {
	
	m_Friends.resize(0);
	
	for (int i = 0; i < m_FarmerList->GetSize(); i++) {
		if (m_Owner != m_FarmerList->GetFarmer(i)) {
			m_Friends.push_back(m_FarmerList->GetFarmer(i));
		}
	}
	std::shuffle(m_Friends.begin(), m_Friends.end(), g_std_rand_engine);
}

Farmer* SocialNetworkVirtual::GetNeighbour(int i) { return m_Friends.at(i); }

Farmer* SocialNetworkVirtual::GetNeighbour() { return m_Friends.at(0); }
