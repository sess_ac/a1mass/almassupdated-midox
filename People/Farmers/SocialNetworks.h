//-------------------------
#ifndef SocialNetworksH
#define SocialNetworksH
//-------------------------

class Farmer;
class FarmerList;

using namespace std;


class SocialNode
{
protected:
	double m_strength;
	Farmer* m_farmer;
public:
	double GetStrength() { return m_strength; }
	void SetStrength(double a_value) { m_strength = a_value; }
	Farmer* GetFarmer() { return m_farmer; }
	void SetFarmer(Farmer* a_farmer) { m_farmer = a_farmer; }
};

/** \brief A base class for social networks */
class SocialNetwork
{
	/**
	* Contains the basics for all social networks. Derived classes implement these networks in different ways
	* m_Nodes is the list of network nodes these nodes are a list of objects of type SocialNode
	* m_TheFarmManager - pointer to give access to the FarmManager class (and therefore lists of farms and farmers).
	* Basic functions to set, get, and calculate node values
	*
	*/
public:
	/** The constructor for the SocialNetwork class */
	SocialNetwork(FarmerList* a_farmerlist, Farmer* a_owner);
	/** \brief A method to update the network */
	virtual void UpdateNetwork() {
	 	; // Does nothing in the base class
	};
protected:
	/** \brief The owner of the network */
	Farmer* m_Owner;
	/** \brief The list of nodes */
	vector<SocialNode> m_Nodes;
	/** \brief A pointer to the farmer list */
	FarmerList* m_FarmerList;
};


class SocialNetworkSpatial : public SocialNetwork
{
public:
	SocialNetworkSpatial(FarmerList* a_farmerlist, Farmer* a_owner) : SocialNetwork(a_farmerlist, a_owner)
	{
		UpdateNetwork();
	}
	/** \brief A method to update the network */
	virtual void UpdateNetwork();
	/** \brief Methods to pick a certain number (parameter) neighbours from the list of nodes */
	Farmer* GetNeighbour(int i);
	/** \brief Methods to pick neighbours from the list of nodes */
	Farmer* GetNeighbour();
	/** \brief Opens a stream file and output farmer's spatial neighbours, tab separated. */
	void PrintNetworkSpatial();
	/** \brief Opens a stream file and output farmer's spatial strenght with neighbours, tab separated. */
	void PrintNetworkSpatial_Strenght();

protected:
	;
};


class SocialNetworkAssociative : public SocialNetwork
{
public:
	SocialNetworkAssociative(FarmerList* a_farmerlist, Farmer* a_owner) : SocialNetwork(a_farmerlist, a_owner)
	{
		UpdateNetwork();
	}
	/** \brief A method to update the network */
	virtual void UpdateNetwork();
	Farmer* GetNeighbour(int i);
protected:
	vector<Farmer*>m_Colleagues;
};

class SocialNetworkVirtual : public SocialNetwork
{
public:
	SocialNetworkVirtual(FarmerList* a_farmerlist, Farmer* a_owner) : SocialNetwork(a_farmerlist, a_owner)
	{
		UpdateNetwork();;
	}
	/** \brief A method to update the network */
	virtual void UpdateNetwork();
	Farmer* GetNeighbour(int i);
	Farmer* GetNeighbour();
protected:
	vector<Farmer*>m_Friends;
};

typedef enum {
	sn_spatial = 1,
	sn_associative,
	sn_virtual,
}SocialNetworkEnum;

class CompareStrength {
public:
	bool operator() (SocialNode A1, SocialNode A2) const {
		return (A1.GetStrength() > A2.GetStrength());
	}
};


#endif
