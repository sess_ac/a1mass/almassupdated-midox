//
// Version of Feb, 12 2020
// Last modified by Andrey Chuhutin on 12/02/2020
/*
*******************************************************************************************************
Copyright (c) 2020, Andrey Chuhutin, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************************************
*/
//----------------------------------------------------------------------
// Created by Andrey Chuhutin on 12/02/2020.
//

//-------------------------
#ifndef Beetle_BaseH
#define Beetle_BaseH
//-------------------------
#include <Landscape/ls.h>
# include <unordered_set>




using namespace std;
#include <cstring>
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

//#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PositionMap.h"
#include "Beetle_toletov.h"
//#include "../Bembidion/Bembidion_All.h"

//#include <Landscape/ls.h>.0.0


#include "../Landscape/Configurator.h"
#include "BatchALMaSS/ALMaSS_Setup.h"

class Landscape;
class Beetle_Population_Manager;
//typedef vector < std::unique_ptr<TAnimal> > smartTListOfAnimals;
/**
*   \brief The enumeration lists all beetle behavioural states used by all the beetle species
*   <Longer description comes here>
*/
enum TTypesOfBeetleState: unsigned
{
  tobs_Initiation=0,
// Egg
  tobs_EDeveloping,
  tobs_Hatching,
  tobs_EDying,
// Larva
  tobs_LDeveloping,
  tobs_LMoving, // added for Ladybird
  tobs_Pupating,
  tobs_LDying,
// Pupa
  tobs_PDeveloping,
  tobs_Emerging,
  tobs_PDying,
// Adult
  tobs_Foraging,
  tobs_Aggregating,
  tobs_Hibernating,
  tobs_Dispersing,
  tobs_ADying,
// Destroy
  tobs_Destroy,
};


//------------------------------------------------------------------------------
/**
 *   \brief The enumeration lists all beetle developmental states,
 *
 *      Beetles are holometabolites (have complete metamorphosis)
*/
enum BeetleObjects: unsigned
{
	bob_Egg = 0,
	bob_Larva,
	bob_Pupa,
	bob_Adult
};
//------------------------------------------------------------------------------


class param_Point
{
public:
    int x;
    int y;
    unsigned direction;
};


// Initialize a Map that will be used for assignment of the stepDone after a particular stage has been assigned



// ------------------------------------------------------------------------
/**
 *   \brief This is a constants class that should be overridden (if needed) for each species
 *
 *      it will be included in the specific population manager and referred through getparameter
 *      functions of the target class. in the baseclass version only variables that will be referred by the general-beetle
 *      base functions are present.
*/

class BeetleConstantClass{
public:
    /**  \brief Day degree constant */
    double EggDevelConst2;
    /**  \brief Daily fixed mortality probability based on three larval stages and temperature */
    std::unique_ptr<std::vector<std::vector<double>>> LarvalDailyMort;

    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_Egg_SoilCultivationMortality;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_Larva_SoilCultivationMortality;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_Pupa_SoilCultivationMortality;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_Adult_SoilCultivationMortality;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_Egg_InsecticideApplication;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_Larva_InsecticideApplication;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_Pupa_InsecticideApplication;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_Adult_InsecticideApplication;

    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgInt> p_cfg_PesticideTrialEggTreatmentMort;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgInt> p_cfg_PesticideTrialLarvaeTreatmentMort;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgInt> p_cfg_PesticideTrialPupaeTreatmentMort;
    /** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgInt> p_cfg_PesticideTrialAdultTreatmentMort;

    /** brief  Farm Operation Mortality */
    const int PesticideTrialAdultToxicMort      = 0;
    /** brief  Farm Operation Mortality */
    const int PesticideTrialPupaeToxicMort      = 0;
    /** brief  Farm Operation Mortality */
    const int PesticideTrialLarvaeToxicMort     = 0;
    /** brief  Farm Operation Mortality */
    const int PesticideTrialEggToxicMort        = 0;
    /**  \brief Inflection point in larval day degree calculations */
    double DevelopmentInflectionPoint{12.};
/**  \brief Day degree threshold constant for all stages */
    double DevelConst1{5}; // Threshold for all development stages // Changed from 3 to fit Elzbieta's version
/**  \brief Day degree constant */
    std::unique_ptr<std::vector<double>> LDevelConst2;

/**  \brief Day degree constant */
    double PupaDevelConst2    = 147.7;
/**  \brief Day degree constant above inflection point */
    const double above12Egg 		= EggDevelConst2/124.9;
/**  \brief Day degree constant above inflection point */
    std::unique_ptr<std::vector<double>> above12Larvae;
/**  \brief Day degree constant above inflection point */
    const double above12Pupae 		= PupaDevelConst2/132.3;
/**  \brief Temperature threshold for egg laying */
    const double AdultEggLayingThreshold = 6; //was 3 -- changed to fit Elzbieta's version;
    const double EggProductionSlope	= 0.6;
/**  \brief The maximum number of eggs it is possible to produce per female */
    std::unique_ptr<const CfgInt> p_cfg_TotalNoEggs; // was 257 010904
/**  \brief Adult dispersal temp threshold for DD calcuation */
    const float DispersalThreshold 	= 6.0;
/** brief Day degrees required before dispersal */
    const int DipsersalDayDegrees = 8; // DK/PL = 8; NL = 48
/** brief Probability of hibernation in field */
    const int FieldHibernateChance 	= 25;    //25
/** \brief holds total number of entries for the dispersal: removed */
    //int g_TotalEntries;
/** \brief holds the max move distance for dispersal */
    int g_AdultMaxMoveDist{-1};
    /** \brief holds the temperature threshold for movement */
    const int AdultMovementTempThreshold = 1;
/** brief Turning rate of adults */
    const int AdultTurnRate         = 80;
/** brief Start of aggregation behaviour */
    const int StartAggregatingDay   = 270;
/** brief Chance of starting aggregating on StartAggregatingDay */
    const int StartAggregatingDayProb = 260;
/** brief Day at which hibernation can start if in the correct habitat type */
    const int StopAggregationDay	= 280;

/** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_BeetleStriglingMort;
/** brief  Farm Operation Mortality */
    std::unique_ptr<const CfgFloat> p_cfg_BeetleHarvestMort;
    /**  \brief Daily fixed mortality probability */
    const double DailyEggMort          =   0.007;
    /**  \brief Daily fixed mortality probability */
    const double DailyLarvaeMort       =   0.001;
    /**  \brief Daily fixed mortality probability */
    const double DailyPupaeMort        =   0.001;
    /**  \brief Daily fixed mortality probability */
    const double DailyAdultMort        =   0.001;
    std::unique_ptr<const CfgInt> p_cfg_beetlestartnos;
    /** \brief Adult/Larve density dependent range*/
    std::unique_ptr<const CfgInt> p_cfg_DDepRange;
    /** \brief Larval density dependent mortality */
    std::unique_ptr<const CfgInt> p_cfg_LDDepMort0; //1-7
    /** \brief Larval density dependent mortality */
    std::unique_ptr<const CfgInt> p_cfg_LDDepMort1; //0-100
    /** \brief Adult density dependent mortality constant*/
    std::unique_ptr<const CfgInt> p_cfg_ADDepMort0;
    /** \brief Chance of death if there is another adult within the density-dependent range square this beetle is in. */
    std::unique_ptr<const CfgInt> p_cfg_ADDepMort1;  //0-1000
    /** \brief Max daily movement in m */
    std::unique_ptr<const CfgInt> p_cfg_MaxDailyMovement;
    /** \brief Movement probability type and parameters */
    std::unique_ptr<CfgStr> p_cfg_MoveProbType;
    std::unique_ptr<CfgStr> p_cfg_MoveProbArgs;
    std::unique_ptr<probability_distribution> move_distribution ;
    const std::string SimulationName;
    /**\brief Number of Larval Stages: ground beetles have 3, but ladybugs have 4**/
    const int BeetleLarvalStagesNum{3};
    //Pesticide configs
/** \brief  Pesticide body burden is multiplied by this daily*/
    std::unique_ptr<const CfgFloat> p_cfg_BemLarvalPPPElimiationRate;
/** \brief  Adult PPP threshold for effect */
    std::unique_ptr<const CfgFloat> p_cfg_BemLarvalPPPThreshold;
/** \brief  The probability of daily effect per day on threshold excedence */
    std::unique_ptr<const CfgFloat> p_cfg_BemLarvalPPPEffectProb;
/** \brief  The probability of daily effect per day on threshold excedence */
    std::unique_ptr<const CfgFloat> p_cfg_BemLarvalPPPEffectProbDecay;
    /** \brief  Pesticide body burden is multiplied by this daily*/
    std::unique_ptr<const CfgFloat> p_cfg_BemPupalPPPElimiationRate;
/** \brief  Adult PPP threshold for effect */
    std::unique_ptr<const CfgFloat> p_cfg_BemPupalPPPThreshold;
/** \brief  The probability of daily effect per day on threshold excedence */
    std::unique_ptr<const CfgFloat> p_cfg_BemPupalPPPEffectProb;
/** \brief  The probability of daily effect per day on threshold excedence */
    std::unique_ptr<const CfgFloat> p_cfg_BemPupalPPPEffectProbDecay;
/** \brief  Pesticide body burden is multiplied by this daily*/
    std::unique_ptr<const CfgFloat> p_cfg_BemAdultPPPElimiationRate;
/** \brief  Adult PPP threshold for effect */
    std::unique_ptr<const CfgFloat> p_cfg_BemAdultPPPThreshold;
/** \brief  The probability of daily effect per day on threshold excedence */
    std::unique_ptr<const CfgFloat> p_cfg_BemAdultPPPEffectProb;
/** \brief  The probability of daily effect per day on threshold excedence */
    std::unique_ptr<const CfgFloat> p_cfg_BemAdultPPPEffectProbDecay;
/** \brief Controls whether pesticide mortality location should be recorded */
    std::unique_ptr<const CfgBool> p_cfg_SavePesticideMortLocation;
/** \brief Controls whether in-field off-field location should be recorded */
    std::unique_ptr<const CfgBool> p_cfg_SaveInfieldLocation;
/** \brief Controls whether in-field Probe is switched on */
    std::unique_ptr<const CfgBool> p_cfg_InfieldProbeIsOn;
/** \brief Controls whether per individual treatment of objects is on in probe */
    std::unique_ptr<const CfgBool> p_cfg_OldProbeCount;
/** \brief Defines the crop reference for in-crop if pest mort location or in-field locations are switched on */
    std::unique_ptr<const CfgInt> p_cfg_InCropRef; // 603 == tov_WWheatPTreatment
/** \brief Interval for recording in-crop locations - > 364 is ignored */
    std::unique_ptr<const CfgInt> p_cfg_SaveInfieldLocationInterval;
/** \brief Start day in year to record in-field locations */
    std::unique_ptr<const CfgInt> p_cfg_SaveInfieldLocationStartDay; // -2 will cause counting on 1st of every month
/** \brief Mortalities of beetles at extreme temperatures */
    std::unique_ptr<const CfgFloat> p_cfg_ExtremeTempMort;
    std::unique_ptr<const CfgFloat> p_cfg_AdultExtremeTempMort;
    std::unique_ptr<const CfgFloat> p_cfg_PupaExtremeTempMort;
    std::unique_ptr<const CfgFloat> p_cfg_LarvaExtremeTempMort;
    std::unique_ptr<const CfgFloat> p_cfg_EggExtremeTempMort;
    /** \brief Temperature lower to which it is regarded to be extreme temperature */
    std::unique_ptr<const CfgFloat> p_cfg_ExtremeTempMin;
    /** \brief Temperature higher to which it is regarded to be extreme temperature */
    std::unique_ptr<const CfgFloat> p_cfg_ExtremeTempMax;
    std::unique_ptr<const CfgBool> p_cfg_ExtremeTempIsOn;
    /** \brief parameter that defines whether the AOR will exclude all the cells that have tiles that return false for AOR filter function
     * can be "yes", "no" and "both": fobr the later both exclusive and non_exclusive probes will be created
     * */
    std::unique_ptr<const CfgStr> p_cfg_AORProbeIsExclusive;
    std::unique_ptr<std::map<TTypesOfBeetleState, bool>> BeetleMapOfStepDone;
    TToleList BeetleStartHabitats;
    TTovList BeetleSuitableForHibernation;
    TToleList BeetleReproductionLandscape;
    TToleList BeetleHalfReproductionLandscape;

    std::ofstream g_movementdistancesout;

    BeetleConstantClass();


};
//------------------------------------------------------------------------------

class param_List15
{
public:
    int BeenThereX[101];
    int BeenThereY[101];
    int nsteps;
};
//------------------------------------------------------------------------------

typedef vector<APoint> TListOfEggs;

//------------------------------------------------------------------------------


// Forward Declarations

class Landscape;
class Population_Manager;
class Beetle_Population_Manager;



//------------------------------------------------------------------------------
/** \brief A data class for Beetle data
 *
 * This class is used by Population Manager
 * */
class struct_Beetle
{
 public:
  //todo: check whether it could be removed
  //int DayDegrees;
  int x;
  int y;
  Landscape* L;
  Beetle_Population_Manager * BPM;
  int HowMany; /*andrey_note: Should remove it? It is used only once In reproduce function it receives 1 in Reproduce,
                  so it was supposed to be used to create more than one object but it is hardcoded to only one object
                  created at a time. So there is no point in that then?*/
                /*Upd. It seems this is the restriction of 1 beetle per tile*/
  //int HowManyFemales;
  /*andrey_note: Here I want to add an additional info about the sex of the Beetles in the concrete spot: for Bembidion it is all Females,
  however for the next animals it make sense. Not sure yet that it should go to just Bembidion_base */
  struct_Beetle(){
      x = -1;
      y = -1;
      L = nullptr;
      BPM = nullptr;
      HowMany = 0;
  }
};

class Beetle_Base :  public TAnimal // why do you complain here about lacking class? It is not included in h file originally
{
public:
	/** \brief Constructor */
	Beetle_Base(int x, int y, Landscape* L,
		Beetle_Population_Manager* BPM);
	~Beetle_Base() override= default;;
    //using TAnimal::TAnimal;
    virtual void ClearFromMap(int X, int Y){};
	/** \brief ReInit for object pool */
	virtual void ReInit(int x, int y, Landscape* L, Beetle_Population_Manager* BPM);
	/** \brief  BeginStep - pure virtual function: *has* to be implemented in the derived classes*/
    void BeginStep() override=0;
	/** \brief  Step - pure virtual function: *has* to be implemented in the derived classes*/
    void Step()override =0;

	/** \brief  Common state Die */
    virtual void st_Die(); //andrey_note: should it be virtual??? no class re-defines it-> no current need for runtime polymorphism
    /** Set body burden */
    //andrey_note: is it for debugging purposes? Sets the burden instantaneously,
    // instead of increase by events isn't it how it is supposed to work??
    // also never used
    void SetBodyBurden(double a_pcide) { m_body_burden = a_pcide; }
    bool OnFarmEvent(FarmToDo event) override;
 // Attributes
	/** \brief  Current behavioural state */
    TTypesOfBeetleState CurrentBState{tobs_Initiation};
	/** \brief  Pointer to the population manager */
    Beetle_Population_Manager * m_OurPopulation{nullptr};
	/** \brief  For experimental purposes */
	void CopyMyself(int a_beetle);
    [[nodiscard]] virtual int getLarvalStagesNum () const;
    virtual bool Cannibalisation()=0;


    void KillThis() override;
protected:

    /** \brief Init for a beetle object */
    void Init(Beetle_Population_Manager* BPM);
    /** \brief Basic movement for beetle */
    virtual void Move();

	/** \brief Current body burden of pesticide */
	double m_body_burden{-1.0};
	/** \brief Current effect probability */
	double m_currentPPPEffectProb{-1.0};
    //---------------------------------------------------------------------------
    [[nodiscard]] virtual TTypesOfBeetleState getDyingState() const =0;
    virtual  double getSoilCultivationMortality() const =0;
    virtual  double getInsecticideApplication() const =0;
    virtual  double getStriglingMortality() const;
    virtual  double getHarvestMortality() const;
    virtual  double getPesticideTrialTreatment() const =0;
    virtual  double getPesticideTrialToxic() const =0;
    virtual  double getExtremeTempMortality() const;
    void getHopProbabilities(float (& p)[8], int);
    virtual bool IsLocationAllowed(int x, int y){
        return true;
    };
    static int ShortHopDirection(float (&probabilities) [8]);
    virtual const int getMaxSAttempts();
    virtual double getShortRangeDistance();
    virtual int getMaxSensingDistance();
    bool IsLocationSamePoly(int x, int y);
    /**\brief function that calculates the factor that is taken into accounrt when considering the movement
     * of the individual
     * */
    virtual double MovementFactor(int, int, int, int);
	//bool Sex;




    virtual double getDailyMortalityRate() const;

    virtual bool DailyMortality();


    virtual double getStriglingHillMortality() const;

    virtual double getExtremeTempMin() const;

    virtual double getExtremeTempMax() const;
    virtual bool ExtremeTempIsOn() const;

    // There are two types of insects, those that live in the ground and those that live in the vegetation
    // in the ground the max/min temp is a soil temperature, the temp of the air is more variable
    virtual double getMaxTemp();

    virtual double getMinTemp();

    virtual double getCannibalisationChance() const;
};


//------------------------------------------------------------------------------
/**
\brief
The class describing the beetle Egg_List objects
*/
class Beetle_Egg_List : virtual public Beetle_Base
{

/** The egg list is an optimisation to reduce memory and time to run
   it means that there are only 365 of them possible and that each on contains
   all the eggs laid on that day. The only real problem is that there is no longer a link from parent to
   offspring - so this version cannot be used with genetics without adding the genes to the APoint struct.\n
*/
public:
    //using Beetle_Base::Beetle_Base;
    /** \brief Egg_List class constructor */
   Beetle_Egg_List(int today,Beetle_Population_Manager* BPM, Landscape* L);


    ~Beetle_Egg_List() override = default;;
    /** \brief Egg_List class Step code */
    void Step() override;
    /** \brief Egg_List class BeginStep code */
    void BeginStep() override;
    /** \brief Add an egg to the list */
    void AddEgg(int x, int y) {
        APoint E;
        E.m_x=x;
        E.m_y=y;
        EggList.push_back(E);
    };
    void ClearFromMap(int X, int Y) override{
        ;
    };
    int Supply_m_Location_x(int i){
      return   EggList[i].m_x;
    };
    int Supply_m_Location_y(int i){
        return   EggList[i].m_y;
    };
    /** \brief Remove an egg from the list: removed*/
    //void DeleteEgg(int x, int y); // Very slow do not use unless absolutely necessary
    /** \brief The list of eggs */
    TListOfEggs EggList;

    TTypesOfBeetleState getDyingState() const override;
    bool Cannibalisation() override{return false;};

protected:
    /** \brief Egg_List state development */
    virtual TTypesOfBeetleState st_Develop();
    /** \brief Egg_List state hatching */
    virtual TTypesOfBeetleState st_Hatch();
    /** \brief Egg_List non-temperature or density related mortality
     * It is the pure virtual function since the mortality rates are individual
     * for the different species. So it should be implemented on the species level*/
    bool DailyMortality() override;
    // Attributes
    /** Records the day hatched for development calculations */
    int m_DayMade;
    // The list of eggs is in a vector of type APoint
    void SortXR();
    //-----------------------------------------------
     /** to get  EggDevelConst2 **/
     virtual double getEggDevelConst2();

     double getSoilCultivationMortality () const override;
     double getInsecticideApplication() const override;

     virtual double getDailyEggMortality() const;
     double getPesticideTrialTreatment() const override;
     double getPesticideTrialToxic() const override;
    double getExtremeTempMortality() const override;
    /** Temp related mortality-- originally 0 for a standard beetle*/
    virtual double TempRelatedMortality(double temp, double maxtemp, double mintemp){return 0;};
};
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

/**
\brief
The class describing the beetle larvae objects
*/
class Beetle_Larvae :  virtual public Beetle_Base {
public:
    /** \brief Will use default constructor for Larvae  */
    //using Beetle_Base::Beetle_Base;
    Beetle_Larvae( int x, int y, Landscape* L, Beetle_Population_Manager* BPM );
    ~Beetle_Larvae() override = default;;
    /** \brief ReInit for object pool */
    void ReInit( int x, int y, Landscape* L, Beetle_Population_Manager* BPM ) override;
    /** \brief Larvae class BeginStep code */
    void BeginStep() override;
    /** \brief Larvae Step code */
    void Step() override;
    /** \brief Larvae EndStep code */
    void EndStep() override;
    /** \brief Kill this larva */
    //virtual void KillThis() override;
    /** Set body burden elimination rate for larva */
    static void SetLarvalPPPElimRate(double a_rate);
    /** Set body PPP threshold */
    static void SetLPPPThreshold(double a_thresh) { m_BeetleLPPPThreshold = a_thresh; }
    /** Set body PPP effect probability */
    static void SetLPPPEffectProb(double a_conc) { m_BeetleLPPPEffectProb = a_conc; }
    /** Set body PPP effect probability */
    static void SetLPPPEffectProbDecay(double a_decay) { m_BeetleLPPPEffectProbDecay = a_decay; }
    /** Get body burden elimination rate for adults */
    static double GetLarvalPPPElimRate() { return m_BeetleLarvalPPPElimRate ; }
    /** Get body PPP threshold */
    static double GetLPPPThreshold() { return m_BeetleLPPPThreshold; }
    /** Get body PPP effect probability */
    static double GetLPPPEffectProb() { return m_BeetleLPPPEffectProb; }
    /** Get body PPP effect probability */
    static double GetLPPPEffectProbDecay() { return m_BeetleLPPPEffectProbDecay; }
    bool Cannibalisation() override{return false;};
    void ClearFromMap(int X, int Y) override;
    virtual bool AddToMap(int X, int Y);
protected:

    /** \brief Larvae state development */
    virtual TTypesOfBeetleState  st_Develop();
    /** \brief Larvae state pupation */
    virtual TTypesOfBeetleState st_Pupate();
    /** \brief Larvae non-temperature or density related mortality
     * pure virtual will be implemented in the inherited classes */
    //virtual bool DailyMortality() override; not needed will use base class function
     /** \brief Larval pesticide response */
     virtual void InternalPesticideHandlingAndResponse();
    /** \brief Larvae reactions to management events */
    //virtual bool OnFarmEvent( FarmToDo event )=0;
     /** \brief Determine larval temperature related mortality */
    virtual double TempRelatedMortality(double temp2 , double maxtemp, double mintemp);
    // Attributes
    /** \brief Record larvae day degrees */
    double m_AgeDegrees={0};
    /** \brief Current larval growth stage (1-3) */
    int m_LarvalStage={0};
    /** Records the day hatched for development calculations */
    int m_DayMade=m_OurLandscape->SupplyDayInYear();
    /** \brief the daily elimination rate for pesticides */
    static double m_BeetleLarvalPPPElimRate;
    /** \brief PPP effects threshold */
    static double m_BeetleLPPPThreshold;
    /** \brief Effect probability on threshold excedence */
    static double m_BeetleLPPPEffectProb;
    /** \brief Effect probability on threshold excedence */
    static double m_BeetleLPPPEffectProbDecay;
    /** get LarvaMortality constant**/
    virtual double getLarvaTDailyMortality (int st, int t);

     double getSoilCultivationMortality() const override;
     double getInsecticideApplication() const override;

     double getPesticideTrialTreatment() const override;
     double getPesticideTrialToxic() const override;


    virtual double getLDevelConst2(int i) const;

    virtual double getDailyLarvaeMortality() const;
    TTypesOfBeetleState getDyingState() const override;

    virtual int getDDepthRange() const;

    double getDailyMortalityRate() const override;

    double getExtremeTempMortality() const override;

    double getHarvestMortality() const override;

    double getStriglingMortality() const override;
    int getLarvalStage() const{return m_LarvalStage;};

};
//------------------------------------------------------------------------------

/**
\brief
The class describing the beetle pupae objects
*/
class Beetle_Pupae :  virtual public Beetle_Base
{
public:
    /** \brief Pupae class constructor */
    Beetle_Pupae(int x, int y, Landscape* L,
                 Beetle_Population_Manager* BPM);
    ~Beetle_Pupae() override = default;

    /** \brief ReInit for object pool */
    void ReInit( int x, int y, Landscape* L, Beetle_Population_Manager* BPM ) override;
    /** \brief Pupae BeginStep code */
    void BeginStep() override;
    /** \brief Pupae Step code */
    void Step() override;
    /** \brief Pupae EndStep code */
    void EndStep() override;
    /** \brief Kill this pupa */
    //virtual void KillThis() override;
    /** Set body burden elimination rate for pupa */
    static void SetPupalPPPElimRate(double a_rate) { m_BeetlePupalPPPElimRate = a_rate; }
    /** Set body PPP threshold */
    static void SetPPPPThreshold(double a_thresh) { m_BeetlePPPPThreshold = a_thresh; }
    /** Set body PPP effect probability */
    static void SetPPPPEffectProb(double a_conc) { m_BeetlePPPPEffectProb = a_conc; }
    /** Set body PPP effect probability */
    static void SetPPPPEffectProbDecay(double a_decay) { m_BeetlePPPPEffectProbDecay = a_decay; }
    /** Get body burden elimination rate for pupa */
    static double GetPupalPPPElimRate() { return m_BeetlePupalPPPElimRate;}
    /** Get body PPP threshold */
    static double GetPPPPThreshold() { return m_BeetlePPPPThreshold; }
    /** Get body PPP effect probability */
    static double GetPPPPEffectProb() { return m_BeetlePPPPEffectProb ; }
    /** Get body PPP effect probability */
    static double GetPPPPEffectProbDecay() { return m_BeetlePPPPEffectProbDecay; }
    bool Cannibalisation() override{return false;};
protected:
    /** \brief Pupal state development */
    virtual TTypesOfBeetleState st_Develop();
    /** \brief Pupal state emergence */
    virtual TTypesOfBeetleState st_Emerge();
    /** \brief Pupae non-temperature or density related mortality
     * is pure virtual will be implemented in the inherited classes*/
    bool DailyMortality() override;
    /** \brief Pupal pesticicde response */
    void InternalPesticideHandlingAndResponse();
    /** \brief Pupal reactions to management events */
    //virtual bool OnFarmEvent(FarmToDo event);
    // Attributes
    /** \brief Record pupal day degrees */
    double m_AgeDegrees;
    /** Records the day hatched for development calculations */
    int m_DayMade;
    /** \brief the daily elimination rate for pesticides */
    static double m_BeetlePupalPPPElimRate;
    /** \brief PPP effects threshold */
    static double m_BeetlePPPPThreshold;
    /** \brief Effect probability on threshold excedence */
    static double m_BeetlePPPPEffectProb;
    /** \brief Effect probability on threshold excedence */
    static double m_BeetlePPPPEffectProbDecay;
    double getSoilCultivationMortality() const override;
    double getInsecticideApplication() const override;

    double getPesticideTrialTreatment() const override;
    double getPesticideTrialToxic() const override;

    virtual double getDevelConst2() const;
    /** \brief calculates the length of the pupal stage depending on the temperature */
    int calcLengthOfStageAtTemp(int temp2) const;

    virtual double calcDailyMortChance(int temp2, double LengthOfStageAtTemp) const;

    TTypesOfBeetleState getDyingState() const override;

    double getDailyMortalityRate() const override ;

    double getHarvestMortality() const override;

    double getStriglingMortality() const override;

    double getExtremeTempMortality() const override;
    virtual double TempRelatedMortality(double temp, double maxtemp, double mintemp){return 0.0;};
};
//------------------------------------------------------------------------------
/**
\brief
The class describing the adult (female) beetle objects
*/
class Beetle_Adult :  public Beetle_Larvae
{
public:
    /** Constructor */
    Beetle_Adult(int x, int y, Landscape* L,
                 Beetle_Population_Manager* BPM);
    /** \brief ReInit for object pool */
    void ReInit( int x, int y, Landscape* L, Beetle_Population_Manager* BPM ) override;
    /** Destructor */
    ~Beetle_Adult() override = default;
    /** \brief Adult Step */
    void Step() override;
    /** \brief Adult BeginStep */
    void BeginStep() override;
    /** \brief Adult EndStep */
    void EndStep() override;
    /** Set body burden elimination rate for adults */
    static void SetAdultPPPElimRate(double a_rate) { m_BeetleAdultPPPElimRate = a_rate; }
    /** Set body PPP threshold */
    static void SetAPPPThreshold(double a_thresh) { m_BeetleAPPPThreshold = a_thresh; }
    /** Set body PPP effect probability */
    static void SetAPPPEffectProb(double a_conc) { m_BeetleAPPPEffectProb = a_conc; }
    /** Set body PPP effect probability */
    static void SetAPPPEffectProbDecay(double a_decay) { m_BeetleAPPPEffectProbDecay = a_decay; }
    /** Get body burden elimination rate for adults */
    static double GetAdultPPPElimRate() { return m_BeetleAdultPPPElimRate; }
    /** Get body PPP threshold */
    static double GetAPPPThreshold() { return m_BeetleAPPPThreshold; }
    /** Get body PPP effect probability */
    static double GetAPPPEffectProb() { return m_BeetleAPPPEffectProb; }
    /** Get body PPP effect probability */
    static double GetAPPPEffectProbDecay() { return m_BeetleAPPPEffectProbDecay; }

    static probability_distribution m_move_distribution;
    void setMoveDistribution(std::string, std::string);
    bool Cannibalisation() override{return false;};
    void ClearFromMap(int X, int Y) override;
    /** Add the agent to the map, return true on success */
    bool AddToMap(int X, int Y) override;

    inline void setCanReproduce(bool param){m_CanReproduce=param;}

protected:
    /** \brief Intitialise attribute values */
    void Init();

    /** \brief Foraging behaviour */
    virtual TTypesOfBeetleState st_Forage();
    /** \brief Aggregation behaviour */
    virtual TTypesOfBeetleState st_Aggregate();
    /** \brief Hibernation behaviour */
    virtual TTypesOfBeetleState st_Hibernate();
    /** \brief Intiate Dispersal behaviour */
    virtual TTypesOfBeetleState st_Dispersal();
    /** \brief Daily ageing */
    virtual TTypesOfBeetleState st_Aging();
    /** \brief Density-independent winter mortality */
    bool WinterMort() const;
    /** \brief Density-dependent mortality */
    bool DDepMort();
    /** \brief Moves attempting egg laying under way */
    void MoveTo(int p_dist, unsigned p_direction, int p_turning);
    /** \brief Moves w.r.t. habitat quality only */
    inline int MoveTo_quality_assess();
    /** \brief Moves using a stopping rule for hibernation */
    TTypesOfBeetleState MoveToAggr(int p_dist, unsigned p_direction, int p_turning);
    /** \brief Produces the eggs */
    virtual void Reproduce(int p_x, int p_y);

    virtual /** \brief Does reproduction if possible */
    void CanReproduce();
    /** \brief Density-independent mortality */
    bool DailyMortality() override;
    /** \brief Adult reactions to management events */
    //virtual bool OnFarmEvent(FarmToDo event);
    /** \brief Hand pesticide events code for the beetle */
    void InternalPesticideHandlingAndResponse() override;
    // Attributes
    /** \brief The number of negative day degrees experienced */
    double m_negDegrees{-1};
    /** \brief The number of day degrees experienced in the spring which may trigger dispersal*/
    double m_HibernateDegrees{-1};
    //double OldBest;
    /** \brief The last direction moved */
    int OldDirection{-1};
    /** \brief The number of eggs produced */
    int m_EggCounter{-1};
    /** \brief Signal reproductive readiness */
    bool m_CanReproduce{false};
    /** \brief A helper attribute when simulating movement */
    static param_List15 pList;
    /** \brief A helper attribute when simulating movement */
    static param_Point pPoint;
    /** \brief the daily elimination rate for pesticides */
    static double m_BeetleAdultPPPElimRate;
    /** \brief PPP effects threshold */
    static double m_BeetleAPPPThreshold;
    /** \brief Effect probability on threshold excedence */
    static double m_BeetleAPPPEffectProb;
    /** \brief Effect probability on threshold excedence */
    static double m_BeetleAPPPEffectProbDecay;
    /** \brief Mortality by Strigling: similar for all forms-- should use base class method**/
     double getStriglingMortality() const override;
     /** Support function: Checking that the tole is suitable for hibernation**/
    virtual inline bool IsSuitableForHibernation(TTypesOfVegetation);
    /**Support function: Checking if it is the reproduction landscape**/
    virtual inline bool IsReproductionLandscape(TTypesOfLandscapeElement tole);
    /**Support function: Checking if it is the half-reproduction landscape**/
    virtual inline bool IsHalfReproductionLandscape(TTypesOfLandscapeElement tole);
    double getPesticideTrialTreatment() const override;
    double getSoilCultivationMortality() const override;
    double getInsecticideApplication() const override;
    double getPesticideTrialToxic() const override;
    bool getCanReproduce(){return m_CanReproduce;};;
    int getStartAggregationDay() const;

    int getStartAggregationDayProb() const;

    int getMaxMoveDist() const;

    double getDispersalThreshold() const;

    int getDispersalDayDegrees() const;

    bool checkForDispersal(int day, double temp);

    void IncNegDegrees();

    int getTurnRate() const;

    virtual int getMovementThreshold() const;

    virtual int getTotalNumberEggs() const;

    int getFieldHibernateChance() const;

    int getStopAggregationDay() const;

    double getDailyMortalityRate() const override;

    int getDDepthRange() const override;

    TTypesOfBeetleState getDyingState() const override;


    double getStriglingHillMortality() const override;
    double getHarvestMortality() const override;

    double getExtremeTempMortality() const override;
    /**The function that implements the short range movement
    *
    * the analogue of Bembidion's MoveTo in Poecilus and Ladybird */
    virtual void ShortRangeMovement();
};
//------------------------------------------------------------------------------




/** \brief The population manager class for beetles */
class Beetle_Population_Manager:  public Population_Manager
{
public:
    /**   \brief Method to add beetles to the population */
    virtual void CreateObjects(int ob_type, TAnimal *pvo,void* null ,
                               std::unique_ptr<struct_Beetle>,int number);
    /**   \brief Intialises the population manager */
    virtual void Init ();
    /**   \brief Does day degree development calculations here */
    void DoFirst () override;
    /**   \brief Replaces the Step function for the Egg_List */
    void DoBefore () override;
    /**   \brief Adds output adult locations to DoLast */
    void DoLast () override;
    /**   \brief Overides the Population_Manager::Probe method */
    long Probe(int ListIndex,probe_data* p_TheProbe ) override;
    /** \brief Constructor */
    explicit Beetle_Population_Manager(Landscape* p_L, int N);
    /** \brief Destructor */
    ~Beetle_Population_Manager() override;
    /** \brief Get adult population size */
    int SupplyAdPopSize() const{return m_AdPopSize;}
    /** \brief Get egg population size */
    int SupplyEggPopSize() const {return (int) m_EPopSize;}
    int SupplyDailyEggPopSize(int day) const {return (int)(*m_EList)[day]->EggList.size();}
    void SupplyEggLocXY(unsigned listindex, int j, int & x, int & y) const;
    /** \brief Get larval population size */
    int SupplyLarvaePopSize() const {return m_LPopSize;}
    /** \brief Get pupal population size */
    int SupplyPupaePopSize() const {return m_PPopSize;}

     /** \brief Get egg day degress for a specific day */
     virtual double SupplyEDayDeg(int day) { return m_EDayDeg[day];}

     /** \brief Get larval day degress for a specific dayand instar */
     virtual double SupplyLDayDeg(int L, int day) { return m_LDayDeg[L][day];}

     /** \brief Get pupal day degress for a specific day */
     virtual double SupplyPDayDeg(int day) { return m_PDayDeg[day];}
     unsigned GetPopulationSize(int) override;
     /** Get the size of the population on the particular location (uses the support map data structure) */
     virtual unsigned SupplyTilePopulation(unsigned x, unsigned y, int lifestage);
protected:
    /**   \brief Method to arbitrarily alter populations size */
    void Catastrophe() override;
    /**   \brief Method to arbitrarily alter populations size restricted spatially */
    void Catastrophe2();
    /**   \brief Special output functionality */
    void TheRipleysOutputProbe (FILE* a_prb) override;
    /**   \brief Special output functionality */
    void TheReallyBigOutputProbe() override;
    /**   \brief Special output functionality */
    void TheAOROutputProbe(  ) override;
    /** \brief Used to specify legal starting habitats for simulation start-up */
    bool IsStartHabitat(int a_x, int a_y);
    /** \brief Used to specify legal starting habitats for simulation start-up */
    inline bool IsStartHabitatAux(TTypesOfLandscapeElement tole) const;
    /** \brief has to be overridden until we not arrange the proper separate data storage in the beetle codes. */
    /**\ an alternative (in Field) probe when it is switched on*/
    std::unique_ptr<AOR_Probe_Base> m_AOR_Probe_inField;
    std::unique_ptr<AOR_Probe_Base> m_AOR_Probe_outField;
    std::unique_ptr<AOR_Probe_Base> m_AOR_Probe_inField_additional;
    std::unique_ptr<AOR_Probe_Base> m_AOR_Probe_outField_additional;
#ifdef __BEETLEPESTICIDE1
    /** \brief Locations output error handling */
    void LocOutputError( void );
	/** \brief Location output file open */
	void LocOutputOpen( void );
	/** \brief Records the locations of all beetles and classifies them as to infield, off-field or in-crop */
	void DoInFieldLocationOutput();
#endif
#ifdef __RECORD_RECOVERY_POLYGONS
    /** \brief Special pesticide recovery code */
   void RecordRecoveryPolygons();
   int m_RecoveryPolygons[101];
   int m_RecoveryPolygonsC[101];
#endif


//Attributes
protected:
    // let's see if we can live with that, we'll try to hide the original TheArray
    // substituting it by the smart pointer version of it
    //std::unique_ptr<vector < smartTListOfAnimals >> TheArray_new{nullptr}; // Creates an array of arrays of size 4
    /**   \brief Pointer to the landscape not needed, there is one in the base class*/
    //Landscape* The_Landscape;
    /**   \brief  To store the current population size */
    int m_AdPopSize{-1};

    /**   \brief  To store the current population size */
    int m_LPopSize{-1};
    /**   \brief  To store the current population size */
    int m_PPopSize{-1};
    /**   \brief  Storage for daily day degrees for eggs */
    std::array<double, 365> m_EDayDeg{};
    /**   \brief  Storage for daily day degrees for larvae */
    std::array<std::array<double,365>,3> m_LDayDeg{};
    /**   \brief  Storage for daily day degrees for pupae */
    std::array<double,365> m_PDayDeg{};
    //todo: try to make a single probe variable (maybe template)
public:
    /**   \brief  To store the current population size */
    long m_EPopSize{0};
    /**   \brief Map of suitability for movement */
    std::unique_ptr<MovementMap> m_MoveMap;
    /**   \brief  Optimised map of larval positions */
    std::unique_ptr<SimplePositionMap> m_LarvaePosMap;  //   Modified ***CJT*** 26-05-2009
    /**   \brief  Optimsied map of adult positions */
    std::unique_ptr<SimplePositionMap> m_AdultPosMap;  //   Modified ***CJT*** 26-05-2009
    /**   \brief  Storage for density-dependent mortality parameter */
    int LDDepMort0; // Must be range 1-8
    /**   \brief  Storage for density-dependent mortality parameter */
    double LDDepMort1; // 0-100
    /**   \brief  Storage for density-dependent mortality parameter */
    int ADDepMort0; //
    /**   \brief  Storage for density-dependent mortality parameter */
    double ADDepMort1; // 0-100
    /**   \brief  Daily temperature determined egg production
     *
     * not used by Ladybird, maybe to be moved to Bembidion class*/
    int TodaysEggProduction;
    std::unique_ptr<BeetleConstantClass> beetleconstantslist{nullptr};
    /**   \brief  Replacement for TheArray[0] */
    std::unique_ptr<std::vector <std::unique_ptr<Beetle_Egg_List>>> m_EList{nullptr};
    //Beetle_Egg_List* m_EList[365];
    // The problem is that the Population manager allocates 8 bytes too few so when the EList tries to write to the memory it encounters a problem.
// -----------------------------------
// Constants: try to implement the constants in here
// it can take long to refer to, we should check and otherwise turn them into const's


#ifdef __BEETLEPESTICIDE1
    protected:
	/**   \brief  In-field counter */
	int m_InFieldNo;
	/**   \brief  In-crop counter */
	int m_InCropNo;
	/**   \brief  Off-field counter */
	int m_OffFieldNo;
	/**   \brief  In crop tole reference */
	TTypesOfVegetation m_InCropRef;
	/**   \brief  Increments in field counter */
	void incInField() { m_InFieldNo++; }
	/**   \brief  Increments in crop counter */
	void incInCrop() { m_InCropNo++; }
	/**   \brief  Increments off field counter */
	void incOffField() { m_OffFieldNo++; }
	/** \brief Annual pesticide mortality output file open */
	void PestMortLocOutputOpen( void );
	/** \brief Annual pesticide mortality locations output */
	void PestMortLocOutput( void );
	/** \brief Pesticide mortality output error handling */
	void PestMortLocOutputError( void );
public:
	/**   \brief  Records the location of a beetle killed by a test pesticide */
	void RecordPesticideMortLoc(int a_x, int a_y)
	{
		// Need to get the element type, if a field need to check the crop
		TTypesOfLandscapeElement tole = m_TheLandscape->GetOwner_tole(a_x, a_y);
		if  (tole == tole_Orchard) incInCrop();
		else if  ((tole == tole_Field)  || (tole == tole_UnsprayedFieldMargin)) 
		{
			if (m_TheLandscape->SupplyVegType(a_x, a_y) == m_InCropRef) incInCrop();
		    else incInField();
		} else
		{
			incOffField();
		}
	}
#endif

    //void LOG(const char *fname);


//    void Run(int NoTSteps) override;

    bool StepFinished() override;







    //char *SpeciesSpecificReporting(int a_species, int a_time);

    //unsigned int FarmAnimalCensus(unsigned int a_farm, unsigned int a_typeofanimal);



    void Run(int NoTSteps) override;

    virtual void RunStepMethods();






   // void SupplyLocXY(unsigned int listindex, unsigned int j, int &x, int &y) override;

    void OpenTheAOROutputProbe(string a_AORFilename) override;
};
//------------------------------------------------------------------------------

/**
\brief
Function class to compare to Eggs X
*/
class CompareEggX {
public:
    bool operator() ( APoint A1, APoint A2 ) const {
        return (A1.m_x > A2.m_x);
    }
};
//------------------------------------------------------------------------------
/** \brief the function used by the beetle model to filter the beetles inField
 * The function is used in inField AOR probe (m_AOR_Probe_inField)
 * */
bool inField(int x, int y, Landscape *);
bool outField(int x, int y, Landscape *);

//-------------------------
#endif //Beetle_BaseH
