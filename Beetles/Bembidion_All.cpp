//
// Version of May, 31, 2020
// Last modified by Andrey Chuhutin on 12/02/2020
/*
*******************************************************************************************************
Copyright (c) 2020, Andrey Chuhutin, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************************************
*/
//----------------------------------------------------------------------
// Created by Andrey Chuhutin on 12/02/2020.
//
#include "Bembidion_All.h"
#include <cstring>
#include <iostream>


extern double g_SpeedyDivides[2001];
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_ReallyBigOutput_used;
extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;
//----------------------------------------------------------------------------------------------
//                                              BEMBIDION EGG
//----------------------------------------------------------------------------------------------

Bembidion_Egg_List::Bembidion_Egg_List(int today,Bembidion_Population_Manager* BPM, Landscape* L):Beetle_Egg_List(today,
                                                                                                                  static_cast<Beetle_Population_Manager *>(BPM), L),
                                                                                                  Beetle_Base(0, 0, L, static_cast<Beetle_Population_Manager *>(BPM)){
    ;
}
//----------------------------------------------------------------------------------------------
//                                              BEMBIDION LARVA
//----------------------------------------------------------------------------------------------
Bembidion_Larvae::Bembidion_Larvae( int x, int y, Landscape* L, Bembidion_Population_Manager* BPM ):Beetle_Larvae(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                                                                                                    Beetle_Base(x, y, L, static_cast<Beetle_Population_Manager *>(BPM)){
    ;
}

//----------------------------------------------------------------------------------------------
//                                              BEMBIDION PUPA
//----------------------------------------------------------------------------------------------
Bembidion_Pupae::Bembidion_Pupae( int x, int y, Landscape* L, Bembidion_Population_Manager* BPM ):Beetle_Pupae(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                                                                                                  Beetle_Base(x, y, L, static_cast<Beetle_Population_Manager *>(BPM)){
    ;
}
//----------------------------------------------------------------------------------------------
//                                              BEMBIDION ADULT
//----------------------------------------------------------------------------------------------
Bembidion_Adult::Bembidion_Adult( int x, int y, Landscape* L, Bembidion_Population_Manager* BPM ):Beetle_Adult(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                                                                                                  Beetle_Base(x, y, L, static_cast<Beetle_Population_Manager *>(BPM)){
    Init();
}

//----------------------------------------------------------------------------------------------
//                                              BEMBIDION POPULATION MANAGER
//----------------------------------------------------------------------------------------------
Bembidion_Population_Manager::Bembidion_Population_Manager(Landscape* p_L):Beetle_Population_Manager(p_L , 4){
   Init();
}
void Bembidion_Population_Manager::Init()
{

    // autom. called by constructor
    if (cfg_RipleysOutput_used.value()) {
        OpenTheRipleysOutputProbe("");
    }
    if ( cfg_ReallyBigOutput_used.value() ) {
        OpenTheReallyBigProbe();
    } else ReallyBigOutputPrb=nullptr;
    m_SimulationName = "Bembidion";
    // Create cfg_beetlestartnos adults
    m_EList = std::make_unique<std::vector <std::unique_ptr<Beetle_Egg_List>>>(365);
    // Create the egg lists

    for (int i=0; i<365; i++)
    {
        // std::cout << "DEBUG MESSAGE: m_EList creation: " << i << std::endl;
        (*m_EList)[i]=std::make_unique<Beetle_Egg_List>(i,this,m_TheLandscape);

    }
    for (int i=0; i<beetleconstantslist->p_cfg_beetlestartnos->value(); i++)
    {
        std::unique_ptr <struct_Beetle> aps(new struct_Beetle);
        aps->BPM = this;
        aps->L = m_TheLandscape;
        do
        {
            aps->x = random(m_TheLandscape->SupplySimAreaWidth());
            aps->y = random(m_TheLandscape->SupplySimAreaHeight());
        } while (!IsStartHabitat(aps->x, aps->y));
        CreateObjects(3,nullptr,nullptr,std::move(aps),1);
    }

    m_AdPopSize=beetleconstantslist->p_cfg_beetlestartnos->value();
    m_EPopSize=0;
    m_LPopSize=0;
    m_PPopSize=0;

// Load List of Animal Classes
    m_ListNames[0]="Egg";
    m_ListNames[1]="Larva";
    m_ListNames[2]="Pupa";
    m_ListNames[3]="Adult";
    m_ListNameLength = 4;
    m_population_type = TOP_Bembidion;

// Load State Names
    StateNames[tobs_Initiation] = "Initiation";
//Egg
    StateNames[tobs_EDeveloping] = "Developing";
    StateNames[tobs_Hatching] = "Hatching";
    StateNames[tobs_EDying] = "Dying";
//Larva
    StateNames[tobs_LDeveloping] = "Developing";
    StateNames[tobs_Pupating] = "Pupating";
    StateNames[tobs_LDying] = "Dying";
//Pupa
    StateNames[tobs_PDeveloping] = "Developing";
    StateNames[tobs_Emerging] = "Emerging";
    StateNames[tobs_PDying] = "Dying";
//Adult
    StateNames[tobs_Foraging] = "Foraging";
    StateNames[tobs_Aggregating] = "Aggregating";
    StateNames[tobs_Hibernating] = "Hibernating";
    StateNames[tobs_Dispersing] = "Dispersing";
    StateNames[tobs_ADying] = "Dying";

    // Ensure that larvae are sorted w.r.t. x position not shuffled
    BeforeStepActions[1]=1; // 1 = SortX

#ifdef __RECORD_RECOVERY_POLYGONS
    /* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::out);
	ofile << "This file records the number of females in each polygon each day" << endl;
	ofile.close();
	/* Open the polygon recovery file and read in polygons to m_RecoveryPolygons */
	ifstream ifile("RecoveryPolygonsList.txt",ios::in);
	int n;
	ifile >> n;
	m_RecoveryPolygons[0] = n;
	for (int i=0; i<n; i++) ifile >> m_RecoveryPolygons[1+i];
	for (int i=0; i<n; i++) m_RecoveryPolygonsC[1+i]=0;
	ifile.close();
#endif

#ifdef __BEETLEPESTICIDE1
    PestMortLocOutputOpen();
	m_InFieldNo = 0;
	m_OffFieldNo = 0;
	m_InCropNo = 0;
	m_InCropRef = m_TheLandscape->TranslateVegTypes(beetleconstantslist->p_cfg_InCropRef->value());
	if (beetleconstantslist->p_cfg_SaveInfieldLocation->value()) LocOutputOpen();
#endif

    //Beetle_Adult ba(0, 0, nullptr, NULL);
    // Initialise any static variables
    Beetle_Adult::SetAdultPPPElimRate(beetleconstantslist->p_cfg_BemAdultPPPElimiationRate->value());  // Initialize static variable
    Beetle_Adult::SetAPPPThreshold(beetleconstantslist->p_cfg_BemAdultPPPThreshold->value());  // Initialize static variable
    Beetle_Adult::SetAPPPEffectProb(beetleconstantslist->p_cfg_BemAdultPPPEffectProb->value());  // Initialize static variable
    Beetle_Adult::SetAPPPEffectProbDecay(beetleconstantslist->p_cfg_BemAdultPPPEffectProbDecay->value());  // Initialize static variable
    //Beetle_Pupae bp(0, 0, NULL, NULL);
    // Initialise any static variables
    Beetle_Pupae::SetPupalPPPElimRate(beetleconstantslist->p_cfg_BemPupalPPPElimiationRate->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPThreshold(beetleconstantslist->p_cfg_BemPupalPPPThreshold->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPEffectProb(beetleconstantslist->p_cfg_BemPupalPPPEffectProb->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPEffectProbDecay(beetleconstantslist->p_cfg_BemPupalPPPEffectProbDecay->value());  // Initialize static variable
    //Beetle_Larvae bl(0, 0, NULL, NULL);
    // Initialise any static variables
    Beetle_Larvae::SetLarvalPPPElimRate(beetleconstantslist->p_cfg_BemLarvalPPPElimiationRate->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPThreshold(beetleconstantslist->p_cfg_BemLarvalPPPThreshold->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPEffectProb(beetleconstantslist->p_cfg_BemLarvalPPPEffectProb->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPEffectProbDecay(beetleconstantslist->p_cfg_BemLarvalPPPEffectProbDecay->value());  // Initialize static variable
}
/**
All bembidion objects that are created must be created using this method. Data on the location and other attributes
are passed in data, and the number to create in number.\n
*/
void Bembidion_Population_Manager::CreateObjects(int ob_type,
                                                 TAnimal * /* pvo */ ,void* /* null */ ,std::unique_ptr<struct_Beetle> data,int number)
{



    for (int i=0; i<number; i++)
    {
        if (ob_type == 0)
        {
            (*m_EList)[data->L->SupplyDayInYear()]->AddEgg(data->x, data->y);
        }
        if (ob_type == 1) {
            // Will not create a new larva in a square already occupied
            if (m_LarvaePosMap->GetMapValue( data->x, data->y ) == 0) {
                m_LarvaePosMap->SetMapValue( data->x, data->y );
                if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type )) {
                    // We need to reuse an object
                    dynamic_pointer_cast <Beetle_Larvae> (SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))
                            ->ReInit(data->x, data->y, data->L, this);
                    IncLiveArraySize(ob_type);
                }
                else {
                    std::shared_ptr <Bembidion_Larvae> new_Larva(new Bembidion_Larvae(data->x, data->y, data->L, this ));
                    //new_Larva = new Bembidion_Larvae( data->x, data->y, data->L, this );
                    //(*TheArray_new)[ ob_type ].insert( (*TheArray_new)[ ob_type ].begin(), new_Larva );
                    PushIndividual(ob_type, std::move(new_Larva));
                    IncLiveArraySize(ob_type);
                }
            }
        }
        if (ob_type == 2)
        {
            if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type)) {
                // We need to reuse an object
                dynamic_pointer_cast<Bembidion_Pupae>  (SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
                IncLiveArraySize(ob_type);
            }
            else {
                std::shared_ptr <Bembidion_Pupae> new_Pupa(new Bembidion_Pupae(data->x, data->y, data->L, this ));
                //new_Pupa = new Bembidion_Pupae( data->x, data->y, data->L, this );
                PushIndividual(ob_type, std::move(new_Pupa));
                IncLiveArraySize(ob_type);
            }
        }
        if (ob_type == 3)
        {
            if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type)) {
                // We need to reuse an object
                dynamic_pointer_cast<Bembidion_Adult> (SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
                IncLiveArraySize(ob_type);
            }
            else {
                std::shared_ptr <Bembidion_Adult> new_Adult(new Bembidion_Adult(data->x, data->y, data->L, this ));
                PushIndividual(ob_type, std::move(new_Adult));
                IncLiveArraySize(ob_type);
            }
        }
    }
}

//-----------------------------------------------------------------------------
