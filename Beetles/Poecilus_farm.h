//
// Created by andrey on 11/15/22.
//

#ifndef ALMASS_POECILUS_FARM_H
#define ALMASS_POECILUS_FARM_H
#include <unordered_map>
#include <Landscape/Treatment.h>
#include <utility>
using namespace std;
using TTreatmentvsMortalityList=std::unordered_map<FarmToDo, int>;
class PoecilusTreatmentMortality{
public:
    TTreatmentvsMortalityList getList();
    explicit PoecilusTreatmentMortality(TTreatmentvsMortalityList);
private:
    TTreatmentvsMortalityList PoecilusTreatmentsList;
};
typedef struct PoecilusFarming_struct {
    PoecilusTreatmentMortality PoecilusLarvaPupaFertilizersTreatmentMortality{TTreatmentvsMortalityList {
            {fp_npks, 0.12},
            {fp_npk, 0.12},
            {fp_p, 0.12},
            {fp_k, 0.12},
            {fp_liquidNH3, 0.12},
            {fp_manganesesulphate, 0.12},
            {fp_ammoniumsulphate, 0.12},
            {fp_rsm,0.12},
            {fp_calcium,0.8},
            {fa_npks,0.12},
            {fa_npk,0.12},
            {fa_pk,0.12},
            {fa_p,0.12},
            {fa_k, 0.12},
            {fa_sk,0.12},
            {fa_manganesesulphate, 0.12},
            {fa_ammoniumsulphate, 0.12},
            {fa_rsm, 0.12},
            {fa_calcium, 0.8},
            {herbicide_treat, 0.09}, // 0.9 mortality and a low chance of using copper (10%)
            {fungicide_treat, 0.09}, // 0.9 mortality and a low chance of using copper (10%)
            {fa_cu, 0.9},
            {fp_cu, 0.9},
            {fa_boron, 0.12},
            {fp_boron, 0.12}

    }};
}TPoecilusFarmings;

#endif //ALMASS_POECILUS_FARM_H
