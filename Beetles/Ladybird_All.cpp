//
// Version of May, 31, 2020
// Last modified by Andrey Chuhutin on 12/02/2020
/*
*******************************************************************************************************
Copyright (c) 2020, Andrey Chuhutin, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************************************
*/
//----------------------------------------------------------------------
// Created by Andrey Chuhutin on 12/02/2020.
//
#include "Ladybird_toletov.h"
#include "Ladybird_All.h"
#include <cstring>
#include <iostream>
#include <algorithm>

#include <random>

// for debug only:
extern int g_time;
extern Landscape* g_landscape_p;

extern std::default_random_engine g_std_rand_engine;
extern double g_SpeedyDivides[2001];
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_ReallyBigOutput_used;
extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;
/*
std::shared_ptr<Ladybird_Population_Manager> Ladybird_Adult::m_OurLadybirdPopulation;
std::shared_ptr<Ladybird_Population_Manager> Ladybird_Pupae::m_OurLadybirdPopulation;
std::shared_ptr<Ladybird_Population_Manager> Ladybird_Larvae::m_OurLadybirdPopulation;
std::shared_ptr<Ladybird_Population_Manager> Ladybird_Egg_List::m_OurLadybirdPopulation;
 */
std::shared_ptr<Ladybird_Population_Manager> Ladybird_Base::m_OurLadybirdPopulation;

//std::map<int, bool> CanBeCannibalised { {0, true}, {1, true}, {2, true}, {3, true}, {4, true}, {5, false}, {6, false}, {7, false}};
//----------------------------------------------------------------------------------------------
//                                              Ladybird Base
//----------------------------------------------------------------------------------------------




int Ladybird_Base::EatAphids(int appetite) {
    long available=m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
    long diff=available - appetite;
    if (diff<0){
        // we want to eat everything
        incEatenToday((int) available);
        m_OurLadybirdPopulation->decAphids(m_Location_x, m_Location_y, (int)available);
        return (int) -diff;
    }
    else{
        incEatenToday(appetite);
        m_OurLadybirdPopulation->decAphids(m_Location_x, m_Location_y, (int) appetite);
        return 0;
    }

}
int Ladybird_Base::GetTempRange(double temp){
    double mintemp1 = m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdMortalityTempsMin;
    double maxtemp1 = m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdMortalityTempsMax;
    double steptemp = m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdMortalityTempsStep;


    if (temp<mintemp1){
        temp = mintemp1;
    }
    else{
        if (temp>maxtemp1){
            temp = maxtemp1;
        }
    }

     return (int) round(((float)temp-steptemp)/steptemp)-1;
}
/**
 * \brief eats the Larva/eggs
 *
 * the function checks the available amount of larvae
 * eats as much of the appetite as it available
 * returns the number of aphids that is still to be eaten
 *
 * */
int Ladybird_Base::Cannibalise(int appetite) {
    // we will first check if there are eggs (?)
    // we will eat them one by one until we ate enough
    // then that there is larva
    // eat what we can and return how much is left to be eaten
    int diff=appetite;
    int i = 0;
    // get the type of the object that could be cannibalised
    int max_cannibalised=getMaxCannibalise();
    while(diff>0&&(i<max_cannibalised)){
        int difference = m_OurLadybirdPopulation->getLadybirdDensity(m_Location_x, m_Location_y, i) - m_OurLadybirdPopulation->getCannibalisationValue(m_Location_x, m_Location_y, i);
        if(difference>0){
            for (int j = 0; j<difference&&diff>0; j++){
                if (g_rand_uni()<getCannibalisationChance()){
                    int Cannibalised_in_aphids=(int) m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdCannibalismCost[i];


                    m_OurLadybirdPopulation->setCannibalism(m_Location_x, m_Location_y, i, 1);
                    m_OurLadybirdPopulation->incCannibalisationsCount();
                    cout<<"DEBUG: Cannibalised someone at ("<<m_Location_x<<","<<m_Location_y<<")"<<endl;

                    incEatenToday(Cannibalised_in_aphids);
                    // removal from the map happens
                    //m_OurLadybirdPopulation->decLadybirdDensity(m_Location_x, m_Location_y, i);




                    diff-=Cannibalised_in_aphids;
                }


            }

        }

        i++;
    }
    return diff;


}
int Ladybird_Base::getMaxCannibalise(){
    return 0;
}
double Ladybird_Base::getCannibalisationChance() const{
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdCannibalisationChance->value();
}
bool Ladybird_Base::IsTooMuchAphids(long aphidsnum){
    if (aphidsnum>m_OurLadybirdPopulation->getMaxAphids()){
        return true;
    }
    else{
        return false;
    }
}
bool Ladybird_Base::IsLongRangeTarget(int x, int y){
    TTypesOfLandscapeElement tole=m_OurLandscape->SupplyElementType(x,y);
    TTypesOfVegetation tov = m_OurLandscape->SupplyVegType(x, y);
    if (CurrentBState==tobs_Foraging||CurrentBState==tobs_Dispersing){// If the state is foraging or dispersing we should check the toletovs for foraging
        if ((m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdLongRangeToles).find(tole) != (m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdLongRangeToles).end()||
                (m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdLongRangeTovs).find(tov) != (m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdLongRangeTovs).end()){
            return true;
        }
    }
    else if(CurrentBState==tobs_Aggregating){ // On aggregation we prefer different landscape elements
        if ((m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAggregationToles).find(tole) != (m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAggregationToles).end()||
            (m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAggregationTovs).find(tov) != (m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAggregationTovs).end()){
            return true;
        }

    }
    return false;
}




int Ladybird_Base::LongHopDirection(std::array<float,4> (&probabilities)){
    double number = g_rand_uni(); //between 0 and 1
    for (int i=0; i<4; i++){
        if (probabilities[i]>=number){
            return i;
        }
    }
    // random number max is 1, probabilities vector should have 1 at some place, if not, throw an error
    g_msg->Warn( WARN_BUG, " Ladybird_Base::LongHopDirection: ","Wrong probabilities array received");
    exit(-1);

}




const int Ladybird_Base::getMaxSAttempts(){
    return m_OurLadybirdPopulation->getMaxShortRangeAttempts();
}

/**
 * \brief this function estimates maximum number of steps the ladybird can run for
 *
 * \todo : can be that we will need to introduce the dependance in the temperature and the time of the year, simply, the days are longer in summer
 * */
int Ladybird_Base::getMaxDailyDistance(){
    return m_OurLadybirdPopulation->getMaxAdultDailyDistance();
}
int Ladybird_Base::getLongRangeDistance(double wind_speed, double travel_angle_rad, double wind_rad){
    //TODO Implement from coccinella_refs.docx
    int max_wind_speed{20};//TODO: move to the constants
    if (wind_speed>=max_wind_speed){
        return 0;
    }
    //int max_dist{getLongRangeDistance()};
    //float t_T{float(max_dist)/3};
    float t_T=m_OurLadybirdPopulation->getLongRangeDuration();
    float a=m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdBoundaryLayerFactor;
    float v_L=m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdFlyingSpeed; //TODO: move to the constants

    double wind_angle_rad{wind_rad-travel_angle_rad};
    return  static_cast<int>(t_T*(v_L+a*cos(wind_angle_rad)*wind_speed));
}
int Ladybird_Base::getLongRangeDistance(){
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdMaxLongRangeFlight->value();
}

int Ladybird_Base::getLongRangeSearchLength() {
    return m_OurLadybirdPopulation->ladybirdconstantslist->LongRangeMaxTries;
}
int Ladybird_Base::getDictLength() {
    return m_OurLadybirdPopulation->ladybirdconstantslist->LongRangeMinDict;
}
double Ladybird_Base::MovementFactor(int newx, int newy, int oldx, int oldy){
    return (double) (m_OurLadybirdPopulation->getAphidsNumber(newx, newy) - m_OurLadybirdPopulation->getAphidsNumber(oldx, oldy));
}
/**
 * \brief This function takes the wind direction as the input and returns the randomly drawn
 * direction of the ladybird long range travel
 *
 * */
int Ladybird_Base::getLongRangeDirection(int wind_direction, double wind_speed){
    /** The parameter that defines the increase in chances to travel with the wind
     * and decreases the chances to travel against the wind*/
     /**Currently wind direction is 0-3*/

    /* The threshold after which the wind influences the chances of the long range flights**/
    float windspeed_thresh = 1;

    /** So we will draw the numbers twice:
     * first time for the general 4 directions taking into account wind direction*/
     std::array<float,4> probabilities={0.25,0.5,0.75,1};
     if (wind_speed>windspeed_thresh){
         float winddirparam = 0.2; //TODO: change this parameter into something more meaningful, maybe put it where all the constants are
         int opposite_dir;
         switch (wind_direction) {
             case 0:
                 opposite_dir = 2;
                 break;
             case 1:
                 opposite_dir = 3;
                 break;
             case 2:
                 opposite_dir = 0;
                 break;
             case 3:
                 opposite_dir = 1;
                 break;
             default:
                 g_msg->Warn(WARN_BUG,"Ladybird_Base::getLongRangeDirection", "Unexpected wind direction");
                 exit (-1);
         }
         for (int i=wind_direction; i<4;i++)
            probabilities[i]-=winddirparam;
         for (int i=opposite_dir; i<4;i++)
            probabilities[i]+=winddirparam;

     }
     int inithopdir= LongHopDirection(probabilities);
     return PreciseLongHopDirection(inithopdir);


}
int Ladybird_Base::PreciseLongHopDirection(int inputdir) {
    int return_degrees=inputdir*90+m_OurLadybirdPopulation->m_LongHopRandom.geti();
    return return_degrees;
}
bool Ladybird_Base::ExtremeTempIsOn() const {
    return false;
}
Ladybird_Base::Ladybird_Base(int x, int y, Landscape *L, Ladybird_Population_Manager* LPM):Beetle_Base(x, y, L, static_cast<Beetle_Population_Manager *>(LPM)){
    ;
}
void Ladybird_Base::UpdateEggsToLay(int preydensitylevel, double temp){
    double f = m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdPDf.at(preydensitylevel);
    int ttemp = (int) ((temp-12.5)/5.);
    if (ttemp>6) ttemp = 6;
    if (ttemp<0) ttemp = 0;
    m_TotalEggsToLay+=(int)((m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdEPD.at(ttemp))*f);
}
double Ladybird_Base::getExtremeTempMin() const{
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdExtremeTempMin->value();
}
double Ladybird_Base::getExtremeTempMax() const{
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdExtremeTempMax->value();
}
double Ladybird_Base::getMinTemp(){

        return g_landscape_p->SupplyMinTemp();



};
double Ladybird_Base::getMaxTemp(){

        return g_landscape_p->SupplyMaxTemp();



};
//----------------------------------------------------------------------------------------------
//                                              Ladybird EGG
//----------------------------------------------------------------------------------------------

Ladybird_Egg_List::Ladybird_Egg_List(int today,Ladybird_Population_Manager* BPM, Landscape* L):
    Beetle_Egg_List(today,static_cast<Beetle_Population_Manager*>(BPM), L),
    Ladybird_Base(0, 0, L, BPM),
    Beetle_Base(0, 0, L, static_cast<Beetle_Population_Manager *>(BPM)){
    ;
}
double Ladybird_Egg_List::getDailyEggMortality() const {
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdDailyEggMortality->value();
}
TTypesOfBeetleState Ladybird_Egg_List::st_Develop()
{
    /* Commented out, since it is done by TempRelatedMortality in BeginStep
    double temp=m_OurLandscape->SupplyTemp();
    if (temp<getLowDyingTemp() || temp>getHighDyingTemp()){
        return getDyingState();
    }
     */
    if (m_OurPopulation->SupplyEDayDeg(m_DayMade)>getEggDevelConst2()) // Hatch
    {
        return tobs_Hatching; // go to hatch
    }
    else return tobs_EDeveloping; // carry on developing
}
double Ladybird_Egg_List::getSoilCultivationMortality() const {
    return m_OurPopulation->beetleconstantslist->p_cfg_Egg_SoilCultivationMortality->value();
}
double Ladybird_Egg_List::getEggDevelConst2() {
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdThresholdDD.at(0);
}
/**
 * Cannibalisation adjustment function
 * goes through the list and checks whether there should be eggs cannibalised at the location
 * returns true if there were cannibalisations in the list
 * */
bool Ladybird_Egg_List::Cannibalisation(){
    if (m_OurLadybirdPopulation->getCannibalisationsCount()==0)
    {
        return false;
    }
    int size=(int)EggList.size();
    bool Cannibalised{false};
    for (int i=0; i<size; i++){
        if (m_OurLadybirdPopulation->getCannibalisationValue(EggList[i].m_x, EggList[i].m_y, 0)>0){
            ClearFromMap(EggList[i].m_x, EggList[i].m_y);
            m_OurLadybirdPopulation->decCannibalisationValue(EggList[i].m_x, EggList[i].m_y, 0);
            m_OurLadybirdPopulation->decCannibalisationsCount();
            EggList[i].m_x = -999;
            Cannibalised= true;
        }
    }
    // Now sort the list based on x values
    if (Cannibalised==true){
        SortXR();
        // Then erase all the -999
        auto ritE=EggList.rend();
        auto ritB=EggList.rbegin();
        while (ritB!=ritE) {
            if ((*ritB).m_x<0) ritB++; else break;
        }
        EggList.erase((ritB).base(),EggList.end());
    }
    return Cannibalised;


}
void Ladybird_Egg_List::ClearFromMap(int X, int Y) {
    if (!m_OurLadybirdPopulation->decLadybirdDensity(X, Y, 0)){
        g_msg->Warn( WARN_BUG, " Ladybird_Egg_List::ClearFromMap "," Clearing non-existant agent from the map");
        exit(-1);
    }

}
double Ladybird_Egg_List::TempRelatedMortality(double temp, double maxtemp, double mintemp){
    double mortalitychance{0.0};
    if (((mintemp<getExtremeTempMin()) || (maxtemp>getExtremeTempMax()))&&ExtremeTempIsOn()){
        mortalitychance+=getExtremeTempMortality();
    }

    int t = GetTempRange(temp);
    return mortalitychance+m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdEggTMortality.at(t);
}


void Ladybird_Egg_List::BeginStep()
{
    DailyMortality(); // should be dealt with for each species separately
    // based on the constants injected in the inherited class
    // kill if the temp is too low or too high
    double temp = m_OurLandscape->SupplyTemp();
    int preylevel = 5; // we do not check the level of prey surrounding the eggs: assume maximum
    UpdateEggsToLay(preylevel, temp);

}

TTypesOfBeetleState Ladybird_Egg_List::st_Hatch()
{


    int size=(int)EggList.size();

    for (int i=0; i<size; i++)
    {
        std::unique_ptr <struct_Ladybird> aps(new struct_Ladybird);  //   instead of new struct_Beetle; using C++11
        aps->x   = EggList[i].m_x;
        aps->y   = EggList[i].m_y;
        aps->L   = m_OurLandscape;
        aps->eggs_developed = m_TotalEggsToLay;
        aps->can_Reproduce= false;
        ClearFromMap(aps->x, aps->y); // remove from map will add to map in CreateObjects

        m_OurLadybirdPopulation->CreateObjects(1, this, nullptr, std::move(aps), 1);
    }
    EggList.erase(EggList.begin(),EggList.end()); // empty the list

    return tobs_Initiation;
#ifdef __LAMBDA_RECORD
    m_OurPopulation->LamdaBirth(m_Location_x,m_Location_y,size);
#endif

}
//----------------------------------------------------------------------------------------------
//                                              Ladybird LARVA
//----------------------------------------------------------------------------------------------
Ladybird_Larvae::Ladybird_Larvae( int x, int y, Landscape* L, Ladybird_Population_Manager* BPM ):Beetle_Larvae(x,y,L,BPM),
                                                                                                 Ladybird_Base(0, 0, L, BPM),
                                                                                                 Beetle_Base(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)){//static_cast<Beetle_Population_Manager *>(BPM)
    m_DateMade= g_landscape_p->SupplyYearNumber()*365+ g_landscape_p->SupplyDayInYear();
}
int Ladybird_Larvae::getMaxCannibalise() {
    return m_LarvalStage+1;
}
double Ladybird_Larvae::getSoilCultivationMortality() const {
    return m_OurPopulation->beetleconstantslist->p_cfg_Larva_SoilCultivationMortality->value();
}
int Ladybird_Larvae::getLarvalStagesNum() const {
    /*
     * This way we access using cast
    return dynamic_cast<Ladybird_Population_Manager *>(m_OurPopulation)->ladybirdconstantslist->LadybirdLarvalStagesNum;
     */
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdLarvalStagesNum;
    // it works like that but it isn't nice, since all the reference to the members of the derived class are using cast
    // There are three alternatives-- only define m_OurPopulation in the last object (leaf), but then all the functionality that is going through the population manager will have to move
    // to the last derived class object
    // or to have two (or potentially n) pointers each one of the different type in each derived object, but then we will increase the memory footprint
    // or instead of accessing PM directly to access it through the getPopulationManager function that could be overloaded (but then still underneath it would need to either store two pointers or cast it each time)

        //return 0;
}
double Ladybird_Larvae::getLDevelConst2 (int i) const {
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdThresholdDD.at(1+i);
}



void Ladybird_Larvae::Step()
{

    if ( m_CurrentStateNo == -1||Cannibalisation()||m_StepDone) return;
    switch (CurrentBState)
    {
        case 0:    // Initial
            CurrentBState=tobs_LDeveloping;
            break;
        case tobs_LDeveloping: // Developing
        {
            CurrentBState=st_Develop();//   st_Develop() can return:
            if (CurrentBState!=tobs_LDeveloping){
                m_StepDone=true;
            }
            //     |
            //     |-> tobs_Pupating
            //     |-> tobs_LDying
            //     |-> tobs_LDeveloping

                //     |
                //     |-> false
                //     |-> false
                //     |-> true
        }
            break;

        case tobs_Pupating:
        {
            CurrentBState=st_Pupate();//   st_Pupate() can return:
            //     |
            //     |-> tobs_Destroy
            m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Pupate() stepDone is:
            //     |
            //     |-> true
        }
            break;

        case tobs_LDying:
            // Remove itself from the position map
            ClearFromMap(m_Location_x, m_Location_y);
            st_Die();
            m_StepDone=true;
            break;
        default:
            m_OurLandscape->Warn("Ladybird Larvae - Unknown State","");
            g_msg->Warn(WARN_BUG, "State attempted was: ", int(CurrentBState));
            exit(1);
    }
}
double Ladybird_Larvae::getLarvaDevelFactor(long AphidsNum){
    int i = PreyDensityLevel(AphidsNum);
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarAphidDevelFactor.at(m_LarvalStage).at(i);

}
int Ladybird_Larvae::PreyDensityLevel(long AphidsNum){
    int myinstar= m_LarvalStage;
    int divider;
    if (AphidsNum<m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMinAphidFactor[myinstar]){
        AphidsNum = m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMinAphidFactor[myinstar];
    }
    else if (AphidsNum>m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMaxAphidFactor[myinstar]){
        AphidsNum = m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMaxAphidFactor[myinstar];
    }
    if (myinstar <2) {
        divider = 5; // the number of aphids in experiment  were chosen with the space of 5: from 10 to 35 and from 15 to 40
        // lets check what is our factor
    }
    else if (myinstar==2){
        divider = 10; // the
    }
    else{ // then the instar is 3 (fourth instar)
        divider = 20;
    }
    return (int)(AphidsNum-m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMinAphidFactor[myinstar])/divider;
}
TTypesOfBeetleState Ladybird_Larvae::st_Develop()
{
/**
 *  \brief Function that controls daily development
    in addition controls feeding and movement.
    Larva only moves if there is not enough aphids or lower larvae to feed upon
	If it reaches the maximum daily distance and haven't got enough food-- it dies.

*/



    if (!CanMove()){
        return getDyingState(); // die
    }
    if (!(m_OurLandscape->SupplyTemp() < getMovementThreshold())) {
        long AphidsNum = m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
        if (getAphidsToEat()>0){ // is hungry?
            if (AphidsNum>0){
                EatAphids(getAphidsToEat());
                m_StepDone=false;
                return CurrentBState;
            }
            else{ // we will have to check if there are eggs/ younger larva to cannibalise
                int toeat = getAphidsToEat();
                int stilltoeat=Cannibalise(toeat);
                if (stilltoeat>0)
                {
                    int oldx{m_Location_x};
                    int oldy{m_Location_y};
                    Move();
                    if(AddToMap(m_Location_x, m_Location_y)){
                        ClearFromMap(oldx, oldy);
                        incMoveCounter();
                    }
                    else{
                        // cannot add to map, go back to the old location
                        m_Location_x=oldx;
                        m_Location_y=oldy;
                    }

                    m_StepDone=false;
                    return CurrentBState;
                }
                else {
                    m_StepDone=false; // false because we still want to check whether it will develop
                    return CurrentBState;
                }

            }

        }
        else{
            // Still alive and not hungry so now do the development bit
            if (m_OurLadybirdPopulation->SupplyLDayDeg(m_LarvalStage, m_DayMade) > getLDevelConst2(m_LarvalStage)*getLarvaDevelFactor(AphidsNum)) {
                ClearFromMap(m_Location_x, m_Location_y);
                m_LarvalStage++; // turn into next larval stage
                if(!AddToMap(m_Location_x, m_Location_y)){
                    m_LarvalStage--;
                    if (!AddToMap(m_Location_x, m_Location_y)){
                        g_msg->Warn(WARN_BUG, "Ladybird_Larvae::st_Develop(): ", "No place to write in the density map");
                        exit(-1);
                    }
                }


            }
            m_StepDone= true;
            if (m_LarvalStage == (getLarvalStagesNum()-1)) return tobs_Pupating; // become a pupa
            else return CurrentBState; // carry on developing
        }


    }
    else{
        m_StepDone = true;
        return tobs_LDeveloping; // skip the day
    }
}

bool Ladybird_Larvae::IsLocationAllowed(int x, int y) {
    return ((m_OurLadybirdPopulation->IsLocationAllowedForLarva(x, y))&&(m_OurLadybirdPopulation->getLadybirdDensity(x,y,m_LarvalStage+1)<254));
}
bool Ladybird_Larvae::Cannibalisation() {
    if (m_OurLadybirdPopulation->getCannibalisationsCount() == 0){
        return false;
    }
    bool Cannibalised{false};
    if (m_OurLadybirdPopulation->getCannibalisationValue(m_Location_x, m_Location_y, getLarvalStage() + 1) > 0) {
        m_OurLadybirdPopulation->decCannibalisationValue(m_Location_x, m_Location_y, getLarvalStage() + 1);
        m_OurLadybirdPopulation->decCannibalisationsCount();
        m_StepDone = true;
        ClearFromMap(m_Location_x, m_Location_y);
        st_Die();
        Cannibalised = true;
    }
    return Cannibalised;


}
/*
bool Ladybird_Larvae::TempRelatedLarvalMortality(int temp2){
    return TempRelatedLarvalMortality();
}
 */
/*
bool Ladybird_Larvae::TempRelatedLarvalMortality(){
    //todo: fill it up: it is supposed to ask for pre-calculated number from Population Manager
    double temp=m_OurLandscape->SupplyTemp();
    if ((temp<getLowDyingTemp())||(temp>getHighDyingTemp())){ // If it is too cold, we are going to die no matter what
        return true;
    }
    else{
        if (g_rand_uni()<(getLarvaTDailyMortality(m_LarvalStage, temp)+getDailyMortalityRate())){ // background mortality
            return false;
        }
        else{
            return true;
        }

    }

}
 */
void Ladybird_Larvae::ClearFromMap(int X, int Y) {
    if (!m_OurLadybirdPopulation->decLadybirdDensity(X, Y, 1+getLarvalStage())){

        g_msg->Warn( WARN_BUG, " Ladybird_Larvae::ClearFromMap"," Clearing non-existent agent from the map at location (X,Y)=("+std::to_string(X)+","+std::to_string(Y)+")");
        exit(-1);
    }

}

bool Ladybird_Larvae::AddToMap(int X, int Y) {
    return m_OurLadybirdPopulation->incLadybirdDensity(X, Y, 1+getLarvalStage());
}



double Ladybird_Larvae::getDailyMortalityRate() const {
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdDailyLarvaeMortality->value();
}

double Ladybird_Larvae::TempRelatedMortality(double temp, double maxtemp, double mintemp){
    double mortalitychance{0.0};
    if (((mintemp<getExtremeTempMin()) || (maxtemp>getExtremeTempMax()))&&ExtremeTempIsOn()){
        mortalitychance+=getExtremeTempMortality();
    }
    mortalitychance+=getLarvaTDailyMortality(getLarvalStage(), (int) temp);
    return mortalitychance;


}
void Ladybird_Larvae::BeginStep() {
    // kill if the temperatures too high or too low or if
    auto temp = g_landscape_p->SupplyTemp();
    auto maxtemp = getMaxTemp();
    auto mintemp = getMinTemp();
    double mortalitychance=TempRelatedMortality(temp, maxtemp, mintemp);
    if (!(mortalitychance>g_rand_uni())){

            long AphidsNum = m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
            int preylevel =  PreyDensityLevel(AphidsNum);
            UpdateEggsToLay(preylevel, temp);
            resetMoveCounter();
            CheckManagement();
            calcAphidsAppetite();


    }
    else{
        CurrentBState=getDyingState();

    }

}
/*
TTypesOfBeetleState Ladybird_Larvae::TempRelatedMortality() {
    if (TempRelatedLarvalMortality()){
        return getDyingState();
    }

}
 */
void Ladybird_Larvae::calcAphidsAppetite(){
    int today= g_landscape_p->SupplyYearNumber()*365+ g_landscape_p->SupplyDayInYear();
    m_AphidsAppetite= (int) ((today-m_DateMade)*m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAppetiteFactor[m_LarvalStage+1]);
}
double Ladybird_Larvae::getMovementThreshold(){
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdLarvaMovementThreshold;
}
int Ladybird_Larvae::getMaxDailyDistance(){
    return m_OurLadybirdPopulation->getMaxLarvaDailyDistance();
}


int Ladybird_Larvae::getMaxSensingDistance() {
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdLarvaAphidSensingDistance->value();
}
TTypesOfBeetleState Ladybird_Larvae::st_Pupate() {
    std::unique_ptr <struct_Ladybird> LS(new struct_Ladybird);


    LS->x   = m_Location_x;
    LS->y   = m_Location_y;
    LS->L   = m_OurLandscape;
    LS->BPM = m_OurPopulation;
    LS->can_Reproduce= false;
    // pupa resets the datemade
    LS->DateMade= g_landscape_p->SupplyYearNumber()*365+ g_landscape_p->SupplyDayInYear();
    LS->eggs_developed= m_TotalEggsToLay;
    // object destroyed by OwnerRTS--> C++11 unique_pointer + std::move
    ClearFromMap(LS->x, LS->y);// removing from map will add in CreateObjects

    m_OurLadybirdPopulation->CreateObjects(2,this,nullptr,std::move(LS),1);
    m_CurrentStateNo=-1; //Destroys object
    return tobs_Destroy;
}

double Ladybird_Larvae::getShortRangeDistance(){
    return m_OurLadybirdPopulation->m_ShortRangeDistanceLarva->get();//random(getMaxHopDistance()-1)+1;;
}
/**
 * \brief receives a real temperature and returns temp-related daily mortality rate
 * **/
double Ladybird_Larvae::getLarvaTDailyMortality (int st, int temp){
    int t = GetTempRange(temp);
    return (*m_OurPopulation->beetleconstantslist->LarvalDailyMort)[st][t];
}

//----------------------------------------------------------------------------------------------
//                                              Ladybird PUPA
//----------------------------------------------------------------------------------------------
Ladybird_Pupae::Ladybird_Pupae( int x, int y, Landscape* L, Ladybird_Population_Manager* BPM ):Beetle_Pupae(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                                                                                               Ladybird_Base(0, 0, L, BPM),
                                                                                                Beetle_Base(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)){
    ;
}
void Ladybird_Pupae::BeginStep(){
    double temp = g_landscape_p->SupplyTemp();
    int today = g_landscape_p->SupplyYearNumber()*365+g_landscape_p->SupplyDayInYear();
    auto maxtemp = getMaxTemp();
    auto mintemp = getMinTemp();

    if (TempRelatedMortality(temp, maxtemp, mintemp)<g_rand_uni()&& (today-m_DateMade<getMaxStageLength())){

        CheckManagement();
        temp = m_OurLandscape -> SupplyTemp();
        long AphidsNum = m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
        int preylevel =  PreyDensityLevel(AphidsNum);
        UpdateEggsToLay(preylevel, temp);
    }
    else{
        CurrentBState=getDyingState();
    }
}
int Ladybird_Pupae::getMaxStageLength(){
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdPupaStageMaxLength->value();
}
int Ladybird_Pupae::PreyDensityLevel(long AphidsNum) {
    int divider;
    int myinstar = 3; // pupa is like the 4th instar
    if (AphidsNum<m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMinAphidFactor[myinstar]){
        AphidsNum = m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMinAphidFactor[myinstar];
    }
    else if (AphidsNum>m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMaxAphidFactor[myinstar]){
        AphidsNum = m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMaxAphidFactor[myinstar];
    }
    divider = 20;
    return (int)(AphidsNum-m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdInstarMinAphidFactor[myinstar])/divider;
}
void Ladybird_Pupae::ClearFromMap(int X, int Y) {
    if (!m_OurLadybirdPopulation->decLadybirdDensity(X, Y, 5)){
        g_msg->Warn( WARN_BUG, " Ladybird_Pupae::ClearFromMap "," Clearing non-existant agent from the map");
        exit(-1);
    }

}

double Ladybird_Pupae::getSoilCultivationMortality() const {
    return m_OurPopulation->beetleconstantslist->p_cfg_Pupa_SoilCultivationMortality->value();
}
double Ladybird_Pupae::getDevelConst2() const{
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdThresholdDD.at(5);
}
double Ladybird_Pupae::getDailyMortalityRate() const {
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdDailyPupaeMortality->value();
}
TTypesOfBeetleState Ladybird_Pupae::st_Emerge()

{
    std::unique_ptr <struct_Ladybird> LS(new struct_Ladybird);

    LS->x   = m_Location_x;
    LS->y   = m_Location_y;
    LS->L   = m_OurLandscape;
    LS->BPM = m_OurPopulation;
    LS->can_Reproduce= false;
    // Adult resets the date made
    LS->DateMade=g_landscape_p->SupplyYearNumber()*365+g_landscape_p->SupplyDayInYear();
    LS->eggs_developed = m_TotalEggsToLay;
    // object destroyed by OwnerRTS--> C++11 unique_pointer + std::move
    ClearFromMap(LS->x, LS->y);// removing from map will add to map in CreateObjects

    m_OurLadybirdPopulation->CreateObjects(3,this,nullptr,std::move(LS),1);
    m_CurrentStateNo=-1; //Destroys object
    return tobs_Destroy;
}

double Ladybird_Pupae::TempRelatedMortality(double temp, double maxtemp, double mintemp){

    double mortalitychance{0.0};
    if (((mintemp<getExtremeTempMin()) || (maxtemp>getExtremeTempMax()))&&ExtremeTempIsOn()){
        mortalitychance+=getExtremeTempMortality();
    }

    int t = GetTempRange(temp);
    return mortalitychance+m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdPupaTMortality.at(t);
}

//----------------------------------------------------------------------------------------------
//                                              Ladybird ADULT
//----------------------------------------------------------------------------------------------
void Ladybird_Adult::BeginStep() {
    if (!DailyMortality()){
        CheckManagement();
        resetMoveCounter();

        //if (CurrentBState==tobs_ADying)
        //      std::cout<< "DD\t"<<m_OurPopulation->m_TheLandscape->SupplyGlobalDate()<<"\t"<<"Manage"<<'\n';
        if (CurrentBState!=getDyingState()){
            //IncNegDegrees(); // I cannot see a reason to do it in Ladybirds
            // Die if mortality occurs or is too old
            CurrentBState=st_Aging(); //todo: runs DailyMortality and kills after laying all the eggs should not run in Ladybird
        }
        if (CurrentBState == tobs_Hibernating){
            m_DaysInHibernation++;
        }
        /** calculate the day degrees*/
        double temp = m_OurLandscape->SupplyTemp();
        double LDT=getAdultDevelConst();
        double diff = temp - LDT;
        if (diff>0 && CurrentBState!=tobs_Hibernating){
            m_ADayDeg+=diff;
        }
        /** end of dd calculation */
        if (!getCanReproduce()){
            // update the number of eggs to lay in lifetime
            if (CurrentBState!=tobs_Hibernating){

                long AphidsNum = m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
                int preylevel =  PreyDensityLevel(AphidsNum);
                UpdateEggsToLay(preylevel, temp);
            }
            int Day=m_OurLandscape->SupplyDayInYear();
            if (IsReproductionReady() &&
                    (getOviPositionPeriod() && (((short int) Day - m_EmergenceDay)>m_OurLadybirdPopulation
                            ->ladybirdconstantslist
                            ->p_cfg_LadybirdMinMaturation
                            ->value()
            ))){
                setCanReproduce(true);


                cout<<"DEBUG: lifetime total eggs is: "<<m_TotalEggsToLay<<endl;
                setEggCounter(m_TotalEggsToLay);
                int dailyeggs=CalcMaxEggs();
                setDailyEggs(dailyeggs);

            }
        }
        else{
            int dailyeggs=CalcMaxEggs();
            setDailyEggs(dailyeggs);
        }

        //reset daily eggs numbers
        calcAphidsAppetite();
    }
    else{
        CurrentBState=getDyingState();
    }



}

int Ladybird_Adult::PreyDensityLevel(long AphidsNum) {
    // quite straightforwardly written, but for now will do
    if (AphidsNum<51){
        return 0;
    }
    else{
        if(AphidsNum<81) {
            return 1;
        }
        else{
            if (AphidsNum<111) {
                return 2;
            }
            else{
                 if (AphidsNum<141) {
                     return 3;
                 }
                 else{
                     if (AphidsNum<171) {
                         return 4;
                     }
                     else{
                         return 5;
                          }
                     }
                 }
             }
          }
}





TTypesOfBeetleState Ladybird_Adult::st_Aging()
{


    // Check should the beetle die today if not hibernating
    if (CurrentBState!=tobs_Hibernating)
    {
        if (DailyMortality()) {
            // std::cout<< "DD\t"<<m_OurPopulation->m_TheLandscape->SupplyGlobalDate()<<"\t"<<"Age"<<'\n';
            return tobs_ADying;
        } // go to die

    }
    return CurrentBState; // carry on
}

Ladybird_Adult::Ladybird_Adult( int x, int y, bool can_Reproduce, Landscape* L, Ladybird_Population_Manager* BPM ):Beetle_Adult(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                                                                                                                   Ladybird_Base(0, 0, L, BPM),
                                                                                                                   Beetle_Base(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)){
    Init();
    hasOverwintered=false; // probably not needed false is a default value
    m_CanReproduce = can_Reproduce;
    m_DateMade=g_landscape_p->SupplyYearNumber()*365+ g_landscape_p->SupplyDayInYear();
    m_DaysInHibernation = 0;
}
Ladybird_Adult::Ladybird_Adult( int x, int y, int DateMade, bool can_Reproduce, Landscape* L, Ladybird_Population_Manager* BPM ):Beetle_Adult(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                                                                                                                Ladybird_Base(0, 0, L, BPM),
                                                                                                                   Beetle_Base(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)){
    Init();
    hasOverwintered=false; // probably not needed false is a default value
    m_CanReproduce = can_Reproduce;
    m_DateMade=DateMade;
}
/**
*\brief
*Hibernation function (simple)
 *
 * The ladybirds exit the hibernation all at once at the April, 1st
 * Future improvements:
 * - gradual exit out hibernation: starts at the date, but not all the ladybirds come at once: LadybirdEmergingChance<1
 * - the date depends on the latitude (since it it is probably dependent on the day length): implemented in getLadybirdEmergenceDate()
*/
TTypesOfBeetleState Ladybird_Adult::st_Hibernate() {
    auto today =(short int) m_OurLandscape->SupplyDayInYear();
    if (today==0){
        setOverwintered(true);
        return tobs_Hibernating;
    }

    else{
        if ((m_OurLadybirdPopulation->isEmergenceDay())&&getOverwintered()){
            if (g_rand_uni()<m_OurLadybirdPopulation->getLadybirdEmergenceChance()){
                if (WinterMort()) return tobs_ADying; // die  - a once only test

                /** This will change the ladybird map, remove one hibernating ladybird add one active ladybird */
                if(DeregisterHibernation()){
                    /** this is needed to count the minimum timespan until oviposition */
                    m_EmergenceDay = today;

                    return tobs_Dispersing; // dispersing
                }
                else{
                    // cannot deregister: proceed hibernating
                    return tobs_Hibernating;
                }

            }
            return tobs_Hibernating;
        }
        else{
            return tobs_Hibernating;
        }
    }

}
/**\brief This is the Dispersal function
 *
 * Uses the longrange movement to disperse the ladybirds to the fields
 * */
TTypesOfBeetleState Ladybird_Adult::st_Dispersal()
{
    /**
    \brief the method that controls the Ladybird dispersion from the overwintering sites to the fields
    */
    //int p_distance=m_OurPopulation->beetleconstantslist->move_distribution->geti();
#ifdef BEMMOVETEST
    g_movementdistancesout << p_distance << '\n';
#endif
    // If its too cold then do not do any movement today
    if (m_OurLandscape->SupplyTemp() < getMovementThreshold()) {
        RecordNoLongFlight();
        return tobs_Dispersing;//originally it was return tobs_Foraging
    }



    if (m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdExhaustionInDispersal->value()&&(getLongFlightsNumber() >= getMaxLongRangeFlights())) {
        // too much long flights-- die of exhaustion
        cout<< "DEBUG: Dispersion: Dying of exhaustion"<<endl;
        m_StepDone = true;
        return tobs_ADying;
    }
    if (!LongRangeMovement()){ // the weather is wrong, could not take a flight
        return tobs_Dispersing;
    }


    if (((double)m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y)>g_landscape_p->SupplyLAGreen(m_Location_x, m_Location_y)*m_OurLadybirdPopulation->getAphidDensityToForage())){



        if (getCanReproduce()&&getOviPositionPeriod()&&(getDailyEggs()>0))
        // if by any chance we matured during the dispersion, we can also reproduce: normally it will happen while foraging
        {
            // the max eggs allowed to lay depends on the conditions on the day of the maturation

            CanReproduce();
        }
        m_StepDone = true;
        return tobs_Foraging;
    }
    else{

        /** If there is enough aphids to settle on the desired field we will continue searching
         * */
        return tobs_Dispersing;
    }
    //m_EggCounter=getTotalNumberEggs(); // start the egg production counter

}




/**
 * \brief The function returns Adults SET for oviposition
 *
 * */
double Ladybird_Adult::getDevelConst2() {
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdThresholdDD.at(6);
}
/**
 * \brief The function that performs long range movement
 * returns true on success
 * */
bool Ladybird_Adult::LongRangeMovement(){
    if (m_OurLadybirdPopulation->getFlyingWeather()){




        int wind_direction=m_OurLandscape->SupplyWindDirection();
        double wind_speed=m_OurLandscape->SupplyWind();
        int travel_angle;
        int max_distance;


        int min_dist{10000};
        float min_angle{0};
        bool is_hibernacula{false};
        int iter{0};
        int found_num{0};
        do{
            travel_angle= getLongRangeDirection(wind_direction, wind_speed);
            /** 180.0F stands for 180 in float, to avoid conversion */
            float angle_rad = ((float (travel_angle))/180.0F)*3.1415F;
            float wind_dir_rad = ((float (wind_direction))/180.0F)*3.1415F;
            max_distance=getLongRangeDistance(wind_speed, angle_rad, wind_dir_rad);

            int distance=LongRangeFind(angle_rad, max_distance, is_hibernacula);

            if (distance >0){
                found_num++;
                if ((distance<min_dist)||is_hibernacula){
                    min_dist=distance;
                    min_angle=angle_rad;
                }


                // If there is a hibernacula we disregard all the rest and stop the loop:
                //  we are going to  choose that point


            }

            iter++;
        }
        while((found_num<getDictLength())&&(iter<getLongRangeSearchLength())&&(!is_hibernacula));//todo: max_size of dict should be fit, move to cfg
        if (found_num>0){
            int min_distance = min_dist;


            /** Calculating the new location: static_casts are used to cast the types, since we are sure there will be no
             * overflood: first int to float, then the result to int from float after multiplication **/
            int x = m_Location_x;
            int y = m_Location_y;
            x +=  (int) round(static_cast<float>(min_distance)* cos(min_angle));
            y +=  (int) round(static_cast<float>(min_distance)* sin(min_angle));
            if (x>(m_OurLandscape->SupplySimAreaWidth()-1)){
                x-=(m_OurLandscape->SupplySimAreaWidth());
            }
            if(y>(m_OurLandscape->SupplySimAreaHeight()-1)){
                y-=(m_OurLandscape->SupplySimAreaHeight());
            }
            if(x<0){
                x+=(m_OurLandscape->SupplySimAreaWidth());
            }
            if (y<0){
                y+=(m_OurLandscape->SupplySimAreaHeight());
            }

            if(AddToMap(x, y)){
                ClearFromMap(m_Location_x, m_Location_y);
                m_Location_x = x;
                m_Location_y = y;
                RecordLongFlight();
                return true;
            }
            else{
                RecordNoLongFlight();
                return false;
            }



        }
        else{
            if (m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdDebugging->value()) {
                cout << "DEBUG: LongRangeMovement target not found. Location: (" << m_Location_x << "," << m_Location_y
                     << ")" << '\n';
            }
            RecordNoLongFlight();
            return false;
        }




    }
    else{
        RecordNoLongFlight();
        return false;
    }


}

/**
 * The function that checks that the ladybird could be transported into a new location
 * implements a long range flight behaviour. Returns a correct distance it could travel
 * if successful.
 * If unsuccesful returns 0
 * Upd (Aug 21): The new version of the function instead of choosing randomly out of the list of the
 * suitable locations now chooses the closest location.
 * */
int Ladybird_Adult::LongRangeFind(float angle_rad,int distance, bool &is_hibernacula){
    int minimal_flight_length{10};//TODO: move this parameter to a proper place

    int x;
    int y;

    //vector<APoint> points;
    // TODO: The following should work not in dispersion
    // in dispersion it should not look for the closest point, but look up to the maximum
    int i=minimal_flight_length-1;
    do{
        i++;
        x=(int) (round( static_cast<float>(i)*cos(angle_rad))+static_cast<float>(m_Location_x));
        y=(int) (round( static_cast<float>(i)*sin(angle_rad))+static_cast<float>(m_Location_y));
        // If we go out of the Area we wrap the animal in
        if (x>(m_OurLandscape->SupplySimAreaWidth()-1)){
            x-=(m_OurLandscape->SupplySimAreaWidth());
        }
        if(y>(m_OurLandscape->SupplySimAreaHeight()-1)){
            y-=(m_OurLandscape->SupplySimAreaHeight());
        }
        if(x<0){
            x+=(m_OurLandscape->SupplySimAreaWidth());
        }
        if (y<0){
            y+=(m_OurLandscape->SupplySimAreaHeight());
        }


    } while (i<distance&&!IsLongRangeTarget(x,y));
    if (i<distance){
        if(CurrentBState==tobs_Aggregating){
            /** If it is aggregation we choose the first point:
            * found a suitable spot to go to, return the distance to it
            * */
            int polyref = m_OurLandscape->SupplyPolyRef(x, y);
            is_hibernacula=m_OurLadybirdPopulation->IsHibernacula(polyref);
            return i;
        }
        else{
            int start = i;
            do{
                i++;
                x=(int) (round( static_cast<float>(i)*cos(angle_rad))+static_cast<float>(m_Location_x));
                y=(int) (round( static_cast<float>(i)*sin(angle_rad))+static_cast<float>(m_Location_y));
                // If we go out of the Area we wrap the animal in
                if (x>(m_OurLandscape->SupplySimAreaWidth()-1)){
                    x-=(m_OurLandscape->SupplySimAreaWidth());
                }
                if(y>(m_OurLandscape->SupplySimAreaHeight()-1)){
                    y-=(m_OurLandscape->SupplySimAreaHeight());
                }
                if(x<0){
                    x+=(m_OurLandscape->SupplySimAreaWidth());
                }
                if (y<0){
                    y+=(m_OurLandscape->SupplySimAreaHeight());
                }


            } while (i<distance&&IsLongRangeTarget(x,y));
            int end = i;
            int place_to_land=start+random(end-start);
            is_hibernacula=false;
            return place_to_land;
        }
    }
    else{
        /** No suitable place found*/
        return 0;
    }


    /*
    if (points.size()>0){
        int random = m_OurLadybirdPopulation->m_RegularRandom.geti() % points.size();
        m_Location_x = points[random].m_x;
        m_Location_x = points[random].m_y;
        return true;
    }
    else{
        return false;
    }
    */
}

/**\brief The foraging function
 *
 */
TTypesOfBeetleState Ladybird_Adult::st_Forage(){
    //int p_distance = m_OurPopulation->beetleconstantslist->move_distribution->get();
    //int p_distance = 1; // we will move one step at a time because we need to update what was eaten, etc
    //int p_distance = m_OurPopulation->beetleconstantslist->move_distribution->geti();
#ifdef BEMMOVETEST
    //g_movementdistancesout << p_distance << '\n';
#endif

    // If it is too cold for any function the LB can still die:
    /* No density dependent mortality
    if (DDepMort()) {
        //std::cout << "DD\t" << m_OurPopulation->m_TheLandscape->SupplyGlobalDate() << "\t"<<"Density" << '\n';
        return tobs_ADying;

    } // die
     */
    // If oviposition period is over or laid all the eggs-> Die
    if (getCanReproduce()){
        if ((getEggCounter()<=0)) {
            cout<<"DEBUG: Layed all eggs-> dying"<<endl;
            return tobs_ADying;
        }
    }
    int Day=m_OurLandscape->SupplyDayInYear();
    // If its too cold then do not do any movement today
    // Can there be eating? Here we assume that the temperature threshold is the same for all types of activity
    // eating/moving/mating
    if (!(m_OurLandscape->SupplyTemp() < getMovementThreshold())) {

        if (ShouldStartAggregating(Day)) {
            // it should start aggregating, if it has already overwintered and we are in a uniseasonal mode
            // then it is time to die
            if (getOverwintered() ) {
                if (m_OurLadybirdPopulation->IsUniSeasonal()){
                    m_StepDone = true;
                    cout<<"DEBUG: Is Uniseasonal -> dying"<<endl;
                    return tobs_ADying;
                } else{
                    if (g_rand_uni()<m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdYearlyMortality->value()){
                        cout<<"DEBUG: Seasonal mortality -> dying"<<endl;
                        m_StepDone = true;
                        return tobs_ADying;
                    }
                }

            }
            // otherwise we start aggregating
            m_StepDone = true;
            return tobs_Aggregating; // start aggregating


        }


        // base our decision upon that, but maybe it cannot know for sure whether there is enough aphids for all
        // if need to reproduce
        //int turning = getTurnRate();
        //DailyMovement(p_distance, true);// Die
        long AphidsNum = m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
        /** if there are too much aphids we move away to a random spot using short range movement (not aphids-gradient based)*/
        if (IsTooMuchAphids(AphidsNum)){
            RecordSuccesfulAphidSearch();
            if (CanMove()){
                RandomMovement();
            }
            m_StepDone =true;
            RecordNoLongFlight();
        }
//        if ((m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y)) >=
        //           m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdAphidDemand->value()) {//check if there are enough aphids to eat and reproduce
        if (AphidsNum>0) {//check if there are aphids //m_OurLadybirdPopulation->m_TheLandscape->SupplyLAGreen(m_Location_x, m_Location_y)*m_OurLadybirdPopulation->getAphidDensityToForage()
            // stay here eat

            EatAphids(getAphidsToEat());
            if (getCanReproduce()) {
      //          if (getOverwintered()) {
                    if (getOviPositionPeriod()  ){
                        if (AphidsNum >=
                            m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdAphidDemand->value()) {
                            if (getDailyEggs()>0&&CanMove()){ // we need to have energy to move in order to lay
                                CanReproduce();
                                // m_StepDone is defined inside the  CanReproduce()

                            }
                            else{
                                if (getAphidsToEat()>0){
                                    m_StepDone = false;
                                }
                                else {
                                    RecordSuccesfulAphidSearch();
                                    RecordNoLongFlight();
                                    m_StepDone = true;
                                }
                            }
                            return CurrentBState;
                        } else {
                            if (getDailyEggs()>0&&CanMove()) {
                                incMoveCounter();
                                ShortRangeMovement();
                                m_StepDone = false;
                                return CurrentBState;
                            } else {
                                m_StepDone = true;
                                RecordNoLongFlight();
                                return CurrentBState;
                            }

                        }

                    }

 //               }
            }
            if (getAphidsToEat()>0){
                // still hungry, cannot stop the step
                m_StepDone= false;
            }
            else{
                RecordSuccesfulAphidSearch();
                if (CanMove()){
                    RandomMovement();
                }
                m_StepDone =true;
                RecordNoLongFlight();
            }
            return CurrentBState;
        } else {
            // if there are no aphids to eat and reproduce--  try to cannibalise
            // don't try to reproduce
            //todo: here we can add a check of how many times it was unsuccessful, and maybe the ladybird will only start cannibalising when
            // it becomes more 'desperate'
            Cannibalise(getAphidsToEat()); // cannibalise the larvae
            // check if can still move
            if (!CanMove()) {
                // if hungry and cannot move
                // record unsuccessful move
                RecordUnsuccesfulAphidSearch();
                //stilltoeat = Cannibalise(AphidsToEat); // cannibalise the larvae: disabled because for now we implement the
                //                                           case when right in the beginning the ladybird tries to satisfy the appetite
                //                                          (case 3 in Try to Cannibalise)
                if (getUnsuccesfulAphidSearchNumber() > getLongRangeFlightThresh()) {


                    if (getLongFlightsNumber() >= getMaxLongRangeFlights()) {
                        cout<< "DEBUG: Foraging-> Dying of exhaustion"<<endl;
                        // too much long flights-- die of exhaustion
                        m_StepDone = true;
                        return tobs_ADying;
                    } else {
                        // if moved too much unsuccessfully-- fly far;

                        LongRangeMovement();

                        m_StepDone = true; // will end the day no matter what
                        return CurrentBState;
                    }
                } else {
                    // if not too much short movement still-- stay here and move short on next step
                    RecordNoLongFlight();
                    m_StepDone = true;
                    return CurrentBState;
                }
            } else {
                // can still move
                // will try a short distance movement again
                ShortRangeMovement();
                incMoveCounter();
                m_StepDone = false;
                return CurrentBState;
            }


        }


    }


                    // check the logic of the first scheme and figure out when should we move
                    // is daily activities are driven by laying eggs (daily) with getDailyEggs()>0
                    // by Eating
                    // or by movement
                    // I understand that we move only when there is not enough food. But according to the scheme
                    // we eat only outside the oviposiion



    else{ // if the temperature is too low for movement-- do nothing
        // todo: check taht the corresponding mortality is applied
        m_StepDone = true;
        return CurrentBState;
    }



}
//---
TTypesOfBeetleState Ladybird_Adult::st_Aggregate() {
    //Fly to find Hibernacula
    /** Debug message
    std::cout<< "DEBUG: Today is day # "<< m_OurLandscape->SupplyDayInYear()<< ". I am located in ("<< m_Location_x<<","
    << m_Location_y<<")"<< "aggregating to ";
     */
    if (!IsLongRangeTarget(m_Location_x, m_Location_y)){
        LongRangeMovement();

            /** We either moved or we couldn't fly: anyway let's either proceed or try another day */
        return tobs_Aggregating;

    }
    else{
        RecordNoLongFlight();
    }

    /** Another debug message
    std::cout<<"("<< m_Location_x<<","<< m_Location_y<<")"<<'\n';
     */
     if ((!(IsSomeoneHibernating()))&&IsHibernatingAlone()){
         /**Can hibernate alone */
         if(RegisterHibernation()) {
             m_EmergenceDay = 0;
             return tobs_Hibernating;
         }
         else{
             return tobs_Aggregating;
         }
     }
     else{
         if (FindClump()){
             if (RegisterHibernation()){
                 m_EmergenceDay = 0;
                 return tobs_Hibernating;
             }
             else{
                 return tobs_Aggregating;
             }

         }
         else{
             return tobs_Aggregating;
         }
     }


}
bool Ladybird_Adult::FindClump(){
    for (int i=0; i<getMaxHibernationTries();i++) {
        if (IsFitForHibernation() && IsSpaceToHibernate() && IsSomeoneHibernating()) {
            return true;
        } else {
            ShortRangeMovement();
        }
    }
    return false;
}

//----
/**\brief The reproduction function
 *
 * It checks that the requirements for the reproduction are met and toggle m_CanReproduce */
bool Ladybird_Adult::IsReproductionReady(){

    if ((double)(m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y))>g_landscape_p->SupplyLAGreen(m_Location_x, m_Location_y)*m_OurLadybirdPopulation->getAphidDensityToReproduce()
    && (m_ADayDeg>getDevelConst2())
    ){// TODO: here we are going to check that the number of the aphids is above the threshold
        return true;
    }
    else {
        return false;
    }
}
/** Returns the number of aphids needed so the Ladybird will stay awake */
int Ladybird_Adult::getAphidsToStayAwake(){return m_OurLadybirdPopulation->ladybirdconstantslist->AphidsToStayAwake;};
/** Returns the number of aphids needed so the Ladybird will start hibernation */
int Ladybird_Adult::getAphidsToHibernate(){return m_OurLadybirdPopulation->ladybirdconstantslist->AphidsToHibernate;};
int Ladybird_Adult::getMovementThreshold() const{
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdMovementTempThreshold;
};
bool Ladybird_Adult::IsLocationAllowed(int x, int y) {
    return ((m_OurLadybirdPopulation->IsLocationAllowedForAdult(x, y))&&(m_OurLadybirdPopulation->getLadybirdDensity(x,y,6)<254));
}
inline void Ladybird_Adult::RecordUnsuccesfulAphidSearch(){
    m_UnsuccessfulAphidSearch>>=1;
    m_UnsuccessfulAphidSearch.set(31, true);
}
inline void Ladybird_Adult::RecordSuccesfulAphidSearch(){
    m_UnsuccessfulAphidSearch>>=1;
}
inline int Ladybird_Adult::getLongRangeFlightThresh(){
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdMaxShortRangeAttempts->value();
}
inline int Ladybird_Adult::getMaxLongRangeFlights() {
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdMaxLongRangeAttempts->value();
}
inline void Ladybird_Adult::RecordLongFlight() {
    m_LongFlights>>=1;
    m_LongFlights.set(31, true);
}
inline int Ladybird_Adult::getLongFlightsNumber(){
    return (int) m_LongFlights.count();
}
inline int Ladybird_Adult::getUnsuccesfulAphidSearchNumber(){
    return (int) m_UnsuccessfulAphidSearch.count();
}
inline void Ladybird_Adult::RecordNoLongFlight() {
    m_LongFlights>>=1;
}
/**The function that checks whether the aggregation behaviour should be triggered */
inline bool Ladybird_Adult::ShouldStartAggregating(int a_day) {
    if (a_day>getLastDayOfAggregation()){ //
        return true;
    }
    else{
        if (a_day<getFirstDayOfAggregation()){
            return false;
        } else{
            long AphidsNum=m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
            if (AphidsNum>getAphidsToStayAwake()){
                return false;
            }
            else{
                if (AphidsNum<getAphidsToHibernate()){
                    return true;
                }
                else{
                    if(g_rand_uni()<getChanceToHibernate()){
                        return true;
                    }
                    else{
                        return false;
                    }
                }

            }

        }

    }

}
void Ladybird_Adult::CanReproduce() {
    //are we reproducing?
        TTypesOfLandscapeElement tole;
        tole = m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y);
        // First find out how many eggs to produce depending on temperature

        int NoEggs = 0;
        // Calculated in the pop man so it only needs calculating once
        if (IsReproductionLandscape(tole)) {
            int LNoEggs = getTodaysEggProduction(); // landscape driven number of eggs
            NoEggs = (LNoEggs>getDailyEggs()?getDailyEggs():LNoEggs);

        } else {
            if (IsHalfReproductionLandscape(tole)) {
                int LNoEggs = getTodaysEggProduction(); // landscape driven number of eggs
                NoEggs = ((LNoEggs>getDailyEggs()?getDailyEggs():LNoEggs))
                        >> 1; //todo: do we have a half reproduction landscape in Ladybirds?
            }
            else{
                /**We have eggs to lay, but the landscape type is wrong-- take a flight and end a day*/
                LongRangeMovement();
                m_StepDone = true;
            }
        }

// NoEggs is the daily amount of eggs, we should also divide them in clutches

        if (NoEggs > 0) {
            int EggsInClutch = getClutchSize();// Normaly distributed with the mean of 36
            EggsInClutch= (NoEggs<EggsInClutch? NoEggs: EggsInClutch);
            for (int i = 0; i < EggsInClutch; i++) {
                /**Old ay of reproduction choosing the place in beenThere list*/
                /**
                int place = random(pList.nsteps);
                Reproduce(pList.BeenThereX[place], pList.BeenThereY[place]);
                 */
                Reproduce(m_Location_x, m_Location_y);
            }
            decEggCounter(EggsInClutch);
            decDailyEggs(EggsInClutch);
            // _EggCounter will go negative when all eggs are used
            //- allows a small over-production
            ShortRangeMovement();
            m_StepDone = false;
        }
        else{
            RecordNoLongFlight();
            m_StepDone = true;
        }

}
int Ladybird_Adult::getClutchSize(){
    return (int) m_OurLadybirdPopulation->m_ClutchSizeRandom.get();
}
void    Ladybird_Adult::Reproduce(int p_x, int p_y){
    std::unique_ptr <struct_Ladybird> LS(new struct_Ladybird);
    LS->x   = p_x;
    LS->y   = p_y;
    LS->L   = m_OurLandscape;
    LS->BPM = m_OurPopulation;
    LS->can_Reproduce=false;
    // No need to register in density map: in CreateObjects
    // don't need delete because object destroyed by Population_Manager
    m_OurLadybirdPopulation->CreateObjects(0, nullptr,nullptr,std::move(LS),1);
}
/**
*\brief
*Step function (
 *
 * This is the basic function that defines the move of Adults between the stages
*/
void Ladybird_Adult::Step()
{
    if (m_StepDone || m_CurrentStateNo == -1) return;
    //std::cout<< "DEBUG MESSAGE: Day: <" << m_OurPopulation->m_TheLandscape->SupplyDayInYear() << "> in year "<< m_OurPopulation->m_TheLandscape->SupplyYearNumber()<<" BState: "<<CurrentBState<<" Can repr: "<<m_CanReproduce<<'\n';
    /* Debugging messages
    std::cout<< "DB\t"<<m_OurPopulation->m_TheLandscape->SupplyYear()<<"\t" <<m_OurPopulation->m_TheLandscape->SupplyGlobalDate()<<"\t"<< m_OurPopulation->m_TheLandscape->SupplyDayInYear() << "\t"<< m_OurPopulation->m_TheLandscape->SupplyYearNumber()<<"\t"<<CurrentBState<<"\t"<<m_CanReproduce<<'\n';
    */
    switch (CurrentBState)
    {
        case tobs_Initiation:  // nothing, same as 0
            CurrentBState=tobs_Foraging;
            break;
        case tobs_Foraging:  // Forage
        {
            CurrentBState=st_Forage();//   st_Forage() can return:
            //   |
            //   |-> tobs_Foraging
            //   |-> tobs_ADying
            //   |-> tobs_Aggregating

            //   st_Forage() stepDone is:
            //   |
            //   |-> true/false (is decided upon inside st_Forage())
            if (CurrentBState!=tobs_Foraging){
                m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone)) [CurrentBState];
            }

            //   |-> false
            //   |-> true

        }
            break;
        case tobs_Aggregating:  // Aggregate
        {
            CurrentBState=st_Aggregate();//   st_Aggregate() can return:
            //     |
            //     |-> tobs_Aggregating
            //     |-> tobs_Hibernating
            //     |-> tobs_ADying
            m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Aggregate() stepDone is:
            //     |
            //     |-> true
            //     |-> true
            //     |-> false

        }
            break;
        case tobs_Hibernating:  // Hibernate
        {
            CurrentBState= st_Hibernate();//   st_Aggregate() can return:
            //     |
            //     |-> tobs_Dispersing
            //     |-> tobs_ADying
            m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Hibernate() stepDone is:
            //     |
            //     |-> true
            //     |-> false
        }
            break;

        case tobs_Dispersing:  // Disperse
        {
            CurrentBState=st_Dispersal();//   st_Dispersal() can return:
            //     |
            //     |-> tobs_Dispersing (on very rare occasions: if the temp is low, it will try dispersing tomorrow)
            //     |-> tobs_Foraging (basically the same type of movement as in Dispersal)

            if (CurrentBState!=tobs_Foraging){
                m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone)) [CurrentBState];
            }
            //     |
            //     |-> true
            //     |-> true
        }
            break;

        case tobs_ADying:  // Die
            ClearFromMap(m_Location_x, m_Location_y);
            st_Die();
            m_StepDone=true;
            break;
        default:
            m_OurLandscape->Warn("Beetle Adult Unknown State","");
            g_msg->Warn(WARN_BUG, "Ladybird_Adult::Step State attempted was: ", int(CurrentBState));
            exit(1);
    }
}
double Ladybird_Adult::getAdultDevelConst(){
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdLDTs.at(6);
}
int Ladybird_Adult::getTodaysEggProduction() {
    int localEggNum=m_OurLadybirdPopulation->getLadybirdEggProductionXY(m_Location_x, m_Location_y);
    return localEggNum;
}
bool Ladybird_Adult::getOviPositionPeriod(){
    return m_OurLadybirdPopulation->getOvipositionPeriod();
}

/**
 * \brief calculates the number of Aphids to eat today
 *
 *
 * */

/**Amount of aphids that an individual consumes depends on its age*/
void Ladybird_Adult::calcAphidsAppetite() {
    int today= g_landscape_p->SupplyYearNumber()*365+ g_landscape_p->SupplyDayInYear();
    m_AphidsAppetite= (int) ((today-m_DateMade-m_DaysInHibernation)*m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAppetiteFactor[6]);
}
/**
 * \brief eats the Aphids
 *
 * the function checks the available amount of aphids
 * eats as much of the appetite as it available
 * returns the number of aphids that is still to be eaten
 *
 * */
int Ladybird_Adult::EatAphids(int appetite) {
    long available=m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
    int diff=(int) available -  appetite;
    if (diff<0){
        // we want to eat everything
        incEatenToday((int) available);
        m_OurLadybirdPopulation->decAphids(m_Location_x, m_Location_y, (int)available);
        return -diff;
    }
    else{
        incEatenToday(appetite);
        m_OurLadybirdPopulation->decAphids(m_Location_x, m_Location_y, (int)appetite);
        return 0;
    }

}



void Ladybird_Adult::ClearFromMap(int X, int Y) {
    if (!m_OurLadybirdPopulation->decLadybirdDensity(X, Y, 6+IsHibernating)){
        g_msg->Warn( WARN_BUG, " Ladybird_Adult::ClearFromMap "," Clearing non-existant agent from the map");
        exit(-1);
    }

}

bool Ladybird_Adult::AddToMap(int X, int Y) {
    return m_OurLadybirdPopulation->incLadybirdDensity(X, Y, 6+IsHibernating);
}
double Ladybird_Adult::getSoilCultivationMortality() const {
    return m_OurPopulation->beetleconstantslist->p_cfg_Adult_SoilCultivationMortality->value();
}
/**\brief Returns the max stage number that could be cannibalised*/
int Ladybird_Adult::getMaxCannibalise(){
    return 5;
}

int Ladybird_Adult::getMaxSensingDistance() {
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdAphidSensingDistance->value();
}

void Ladybird_Adult::RandomMovement() {
    float movementchance=m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdUnnecessaryMovementChance->value(); // This is the chance of random movement if not driven by the absence of food
                            //TODO: move it to the model parameters
    if (g_rand_uni()<movementchance){
        ShortRangeMovement();
    }

}
bool Ladybird_Adult::IsHibernatingAlone() {
    if (g_rand_uni()<getChanceToHibernateAlone()){
        return true;
    }
    else{
        return false;
    }

}
double Ladybird_Adult::getChanceToHibernateAlone() {
    int day = m_OurLandscape->SupplyDayInYear();
    int difference=day-getFirstDayOfAggregation();
    // In the first days after the aggregation the chance to hibernate alone is high
    if (difference<10){
        return (0.1-0.01*difference)*0.0001;// added *0.0001 for debugging
    }
    else{
        // then it is zero
        if (difference<30){
            return 0;
        }
        // finally we will have to do it no matter what
        else {
            return 1;
        }
    }
}
bool Ladybird_Adult::RegisterHibernation(){
    // we will remove the ladybird from the list of the active adults and
    ClearFromMap(m_Location_x, m_Location_y);
    IsHibernating = true;
    if (AddToMap(m_Location_x, m_Location_y))
        return true;
    else{
        // could not write to hibernated map: falling back
        IsHibernating = false;
        if (AddToMap(m_Location_x, m_Location_y))

            return false;
        else{
            // something's wrong, cannot write myself back there as well: fail the program
            g_msg->Warn( WARN_BUG, " Ladybird_Adult::RegisterHibernation() ","cannot write myself up both as hibernated and as a non-hibernated");
            exit(-1);
        }
    }


}

bool Ladybird_Adult::DeregisterHibernation(){
    // we will remove the ladybird from the list of the hibernating adults and
    ClearFromMap(m_Location_x, m_Location_y);
    IsHibernating = false;
    if(AddToMap(m_Location_x, m_Location_y)){
        return true;
    }
    else{
        IsHibernating = true;
        if(AddToMap(m_Location_x, m_Location_y)){
            return false;
        }
        else{
            // something's wrong, cannot write myself back there as well: fail the program
            g_msg->Warn( WARN_BUG, " Ladybird_Adult::DeregisterHibernation() ","cannot write myself up both as hibernated and as a non-hibernated");
            exit(-1);
        }
    }
}
/** returns the daily chance to hibernate */
double Ladybird_Adult::getChanceToHibernate(){return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdHibernationChance->value();};
int Ladybird_Adult::getMaxHibernationTries(){
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdMaxHibernationTries->value();
}
int Ladybird_Adult::getMaxClumpSize() {
    return m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdMaxClumpSize;
}
bool Ladybird_Adult::IsSpaceToHibernate(){
    return (m_OurLadybirdPopulation->getLadybirdDensity(m_Location_x, m_Location_y, 7)<getMaxClumpSize());
}
bool Ladybird_Adult::IsSomeoneHibernating() {
    return (m_OurLadybirdPopulation->getLadybirdDensity(m_Location_x, m_Location_y, 7) > 0);
}
bool Ladybird_Adult::IsFitForHibernation() {
    TTypesOfLandscapeElement tole = m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y);
    TTypesOfVegetation tov = m_OurLandscape->SupplyVegType(m_Location_x, m_Location_y);
    if (((m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAggregationToles).find(tole) != (m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAggregationToles).end())||
        ((m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAggregationTovs).find(tov) != (m_OurLadybirdPopulation->ladybirdconstantslist->LadybirdAggregationTovs).end()))
        return true;
    else
        return false;
}
double Ladybird_Adult::getDailyMortalityRate() const {
    return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdDailyAdultMortality->value();
}

double Ladybird_Adult::getShortRangeDistance(){
    return m_OurLadybirdPopulation->m_ShortRangeDistanceAdult->get();//random(getMaxHopDistance()-1)+1;;
}

double Ladybird_Adult::getExtremeTempMin() const{
    int month= m_OurLandscape->SupplyMonth()-1; // not forgetting -1 since this is calendar month and not a programmers months
    if (IsHibernating){
        return m_OurLadybirdPopulation->ladybirdconstantslist->Ladybird_SCP.at(month)*(1+m_OurLadybirdPopulation->
        ladybirdconstantslist->p_cfg_LadybirdExtremeTempMinHibernatingFactor->value());
    }
    else{
        return m_OurLadybirdPopulation->ladybirdconstantslist->Ladybird_SCP.at(month);
    }


}
double Ladybird_Adult::getMinTemp(){
    if (IsHibernating){
        return g_landscape_p->SupplySoilTemp();

    }
    else{
        return g_landscape_p->SupplyMinTemp();

    }

};
double Ladybird_Adult::getMaxTemp(){
    if (IsHibernating){
        return g_landscape_p->SupplySoilTemp();

    }
    else{
        return g_landscape_p->SupplyMaxTemp();

    }

};

int Ladybird_Adult::CalcMaxEggs() {
    int datenow = g_landscape_p->SupplyYearNumber()*365+g_landscape_p->SupplyDayInYear();
    int age = datenow - m_DateMade;
    int dailyeggs=(int)((int)(6.08*((double)age-2.97))/exp(0.075*((double)age-2.9706)));
    return (dailyeggs>0)?dailyeggs:1; //todo: put in the parameters-- there is always a chance to lay n=1 eggs
}

/** Returns the first day of aggregation */
//TODO: Translate to the day length: 263
int Ladybird_Adult::getFirstDayOfAggregation(){return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdAggregationStart->value();};
//TODO: Translate to the day length: 293
/** Returns the last day of aggregation */
int Ladybird_Adult::getLastDayOfAggregation(){return m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdAggregationEnd->value();};



//----------------------------------------------------------------------------------------------
//                                              Ladybird POPULATION MANAGER
//----------------------------------------------------------------------------------------------


Ladybird_Population_Manager::Ladybird_Population_Manager(Landscape* p_L):Beetle_Population_Manager(p_L, 4){
    ladybirdconstantslist = std::make_unique<LadybirdConstantClass>();
    /** Initializing day degrees array */
    m_DayDeg=make_unique<blitz::Array<double, 2>>(365, ladybirdconstantslist->LadybirdLarvalStagesNum+2); // corrected from +3, but now we calculate DD for adults individually
    beetleconstantslist->LarvalDailyMort->assign(
            {{0.054373716632444, 0.053186813186813, 0.057391304347826, 0.0735, 0.121428571428571, 0.217272727272727},
             {0.022869198312236, 0.024266666666667, 0.0321875, 0.049047619047619, 0.056666666666667, 0.100909090909091},
             {0.01071104387292, 0.013373493975904, 0.019111111111111, 0.0385, 0.07, 0.086666666666667},
             {0.010185664939551, 0.01151724137931, 0.016891891891892, 0.028888888888889, 0.046956521739131, 0.063181818181818}});
    beetleconstantslist->LDevelConst2->assign({101.4,107.4,190.4, 100}); //change to real numbers
    beetleconstantslist->above12Larvae->assign({(beetleconstantslist->LDevelConst2->at(0)/87.5),(beetleconstantslist->LDevelConst2->at(1)/95.2),(beetleconstantslist->LDevelConst2->at(2)/189.3),
                                                (beetleconstantslist->LDevelConst2->at(3)/189.3)}); //change to real numbers
    int adult_mean= ladybirdconstantslist->p_cfg_LadybirdMeanHoppingDistance->value();
    int adult_std= ladybirdconstantslist->p_cfg_LadybirdStdHoppingDistance->value();
    int larva_mean= ladybirdconstantslist->p_cfg_LadybirdLarvaMeanHoppingDistance->value();
    int larva_std= ladybirdconstantslist->p_cfg_LadybirdLarvaStdHoppingDistance->value();
    m_ShortRangeDistanceAdult=std::make_unique<probability_distribution>("NORMAL", std::to_string(adult_mean)+" "+std::to_string(adult_std));
    m_ShortRangeDistanceLarva=std::make_unique<probability_distribution>("NORMAL", std::to_string(larva_mean)+" "+std::to_string(larva_std));


}
void Ladybird_Population_Manager::Init()
{
    /*
     * creating the smart pointer to the Ladybird population manager as a static member of all sub-classes
     * potentially one can change it into having a base class Ladybird_unique_base that will include
     * only the things that are unique to ladybirds, so all the ladybird subclasses will inherit it
     * like class Ladybird_Egg_List : public  Beetle_Egg_List, public Ladybird_Base
     * */
    std::shared_ptr<Ladybird_Population_Manager> smart_ptr_to_this = shared_from_this();
    /*
    Ladybird_Adult::m_OurLadybirdPopulation = smart_ptr_to_this;

    Ladybird_Pupae::m_OurLadybirdPopulation = smart_ptr_to_this;

    Ladybird_Larvae::m_OurLadybirdPopulation = smart_ptr_to_this;

    Ladybird_Egg_List::m_OurLadybirdPopulation = smart_ptr_to_this;
     */
    Ladybird_Base::m_OurLadybirdPopulation = smart_ptr_to_this;
    for (int i=0; i<ladybirdconstantslist->p_cfg_LadybirdAphidSpecies->value().size(); i++ ){
        if (m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(ladybirdconstantslist->p_cfg_LadybirdAphidSpecies->value()[i])!= nullptr)
            Aphids_PM.emplace_back(static_pointer_cast<AphidsD_Population_Manager>(m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(
                    ladybirdconstantslist->p_cfg_LadybirdAphidSpecies->value()[i])));

    }
    if (Aphids_PM.empty()){
        std::cout<<"Aphids Population Manager is not Initialized: Exiting!";
        exit(-1);
    }


    int height=m_TheLandscape->SupplySimAreaHeight();
    int width=m_TheLandscape->SupplySimAreaWidth();
    m_AphidSpeciesNum=(int) ladybirdconstantslist->p_cfg_LadybirdAphidSpecies->value().size();

    m_LadybirdCannibalismMap=make_unique<blitz::Array<unsigned char, 3>>(width,height,5);
    (*m_LadybirdCannibalismMap)=0;
    // There are 8 dimensions in the map (egg, 4 larva stages, pupa, adult + inactive adult)
    m_LadybirdDensityMap=make_unique<blitz::Array<unsigned char, 3>>(width,height,LadybirdStagesNumberInDensityMap);
    (*m_LadybirdDensityMap)=0;
    if (ladybirdconstantslist->p_cfg_LadybirdDebugging->value()){
        //DEBUGGING
        unsigned int num = blitz::sum((*m_LadybirdDensityMap));
        cout<<"DEBUG: Number of agents:"<< num<<endl;
    }


    // autom. called by constructor
    if (cfg_RipleysOutput_used.value()) {
        OpenTheRipleysOutputProbe("");
    }
    if ( cfg_ReallyBigOutput_used.value() ) {
        OpenTheReallyBigProbe();
    } else ReallyBigOutputPrb=nullptr;
    m_SimulationName =  "Ladybird";
    m_EList = std::make_unique<std::vector <std::unique_ptr<Beetle_Egg_List>>>(365);
    // Create the egg lists

    for (int i=0; i<365; i++)
    {
        // std::cout << "DEBUG MESSAGE: m_EList creation: " << i << std::endl;
        (*m_EList)[i]=std::make_unique<Ladybird_Egg_List>(i,this,m_TheLandscape);

    }
    // Create cfg_beetlestartnos adults


    for (int i=0; i<ladybirdconstantslist->p_cfg_ladybirdstartnos->value(); i++)
    {
        std::unique_ptr <struct_Ladybird> aps(new struct_Ladybird);
        aps->BPM = this;
        aps->L = m_TheLandscape;
        aps->can_Reproduce= false;
        aps->DateMade=m_TheLandscape->SupplyYearNumber()*365+m_TheLandscape->SupplyDayInYear();
        aps->eggs_developed = ladybirdconstantslist->p_cfg_ladybirdstarteggsno->value();
        do
        {
            aps->x = random(m_TheLandscape->SupplySimAreaWidth());
            aps->y = random(m_TheLandscape->SupplySimAreaHeight());
        } while (!IsStartHabitat(aps->x, aps->y));

        CreateObjects(3,nullptr,nullptr,std::move(aps),1);


    }
    m_AdPopSize=ladybirdconstantslist->p_cfg_ladybirdstartnos->value();
    m_EPopSize=0;
    m_LPopSize=0;
    m_PPopSize=0;

// Load List of Animal Classes
    m_ListNames[0]="Egg";
    m_ListNames[1]="Larva";
    m_ListNames[2]="Pupa";
    m_ListNames[3]="Adult";
    m_ListNameLength = 4;
    m_population_type = TOP_Ladybird;

// Load State Names
    StateNames[tobs_Initiation] = "Initiation";
//Egg
    StateNames[tobs_EDeveloping] = "Developing";
    StateNames[tobs_Hatching] = "Hatching";
    StateNames[tobs_EDying] = "Dying";
//Larva
    StateNames[tobs_LDeveloping] = "Developing";
    StateNames[tobs_Pupating] = "Pupating";
    StateNames[tobs_LDying] = "Dying";
//Pupa
    StateNames[tobs_PDeveloping] = "Developing";
    StateNames[tobs_Emerging] = "Emerging";
    StateNames[tobs_PDying] = "Dying";
//Adult
    StateNames[tobs_Foraging] = "Foraging";
    StateNames[tobs_Aggregating] = "Aggregating";
    StateNames[tobs_Hibernating] = "Hibernating";
    StateNames[tobs_Dispersing] = "Dispersing";
    StateNames[tobs_ADying] = "Dying";

    // Ensure that larvae are sorted w.r.t. x position not shuffled
    BeforeStepActions[1]=1; // 1 = SortX

#ifdef __RECORD_RECOVERY_POLYGONS
    /* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::out);
	ofile << "This file records the number of females in each polygon each day" << endl;
	ofile.close();
	/* Open the polygon recovery file and read in polygons to m_RecoveryPolygons */
	ifstream ifile("RecoveryPolygonsList.txt",ios::in);
	int n;
	ifile >> n;
	m_RecoveryPolygons[0] = n;
	for (int i=0; i<n; i++) ifile >> m_RecoveryPolygons[1+i];
	for (int i=0; i<n; i++) m_RecoveryPolygonsC[1+i]=0;
	ifile.close();
#endif

#ifdef __LADYBIRDPESTICIDE1
    PestMortLocOutputOpen();
	m_InFieldNo = 0;
	m_OffFieldNo = 0;
	m_InCropNo = 0;
    m_InCropRef = m_TheLandscape->TranslateVegTypes(beetleconstantslist->p_cfg_InCropRef->value());
    if (beetleconstantslist->p_cfg_SaveInfieldLocation->value()) LocOutputOpen();
#endif
    //Beetle_Adult ba(0, 0, nullptr, nullptr);
    // Initialise any static variables
    Beetle_Adult::SetAdultPPPElimRate(beetleconstantslist->p_cfg_BemAdultPPPElimiationRate->value());  // Initialize static variable
    Beetle_Adult::SetAPPPThreshold(beetleconstantslist->p_cfg_BemAdultPPPThreshold->value());  // Initialize static variable
    Beetle_Adult::SetAPPPEffectProb(beetleconstantslist->p_cfg_BemAdultPPPEffectProb->value());  // Initialize static variable
    Beetle_Adult::SetAPPPEffectProbDecay(beetleconstantslist->p_cfg_BemAdultPPPEffectProbDecay->value());  // Initialize static variable
    //Beetle_Pupae bp(0, 0, nullptr, nullptr);
    // Initialise any static variables
    Beetle_Pupae::SetPupalPPPElimRate(beetleconstantslist->p_cfg_BemPupalPPPElimiationRate->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPThreshold(beetleconstantslist->p_cfg_BemPupalPPPThreshold->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPEffectProb(beetleconstantslist->p_cfg_BemPupalPPPEffectProb->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPEffectProbDecay(beetleconstantslist->p_cfg_BemPupalPPPEffectProbDecay->value());  // Initialize static variable
    //Beetle_Larvae bl(0, 0, nullptr, nullptr);
    // Initialise any static variables
    Beetle_Larvae::SetLarvalPPPElimRate(beetleconstantslist->p_cfg_BemLarvalPPPElimiationRate->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPThreshold(beetleconstantslist->p_cfg_BemLarvalPPPThreshold->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPEffectProb(beetleconstantslist->p_cfg_BemLarvalPPPEffectProb->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPEffectProbDecay(beetleconstantslist->p_cfg_BemLarvalPPPEffectProbDecay->value());  // Initialize static variable
    auto customDeleter = [](ofstream * po) {	po->close(); delete po; };
    m_LadybirdXYDumpFile= std::shared_ptr<ofstream>(new ofstream("LadybirdXYDump.txt", ios::out), customDeleter);
    //m_LadybirdXYDumpFile = std::make_shared<ofstream>("LadybirdXYDump.txt", ios::out);
    std::vector<std::string> xy_headers;
    xy_headers = { "day", "X", "Y", "poly_ref", "type" };
    WriteHeaders(m_LadybirdXYDumpFile, xy_headers);
    // Let's initialise hibernaculae
    InitialiseHibernaculae();
}
/**
All Ladybird objects that are created must be created using this method. Data on the location and other attributes
are passed in data, and the number to create in number.\n
*/
void Ladybird_Population_Manager::CreateObjects(int ob_type,
                                                 TAnimal * /* pvo */ ,void* /* null */ ,std::unique_ptr<struct_Ladybird> data,int number)
{


    for (int i=0; i<number; i++)
    {
        if (ob_type == 0)
        {

            if((*m_LadybirdDensityMap)(data->x,data->y,0)<blitz::huge((*m_LadybirdDensityMap)(data->x,data->y,0))){
                (*m_EList)[data->L->SupplyDayInYear()]->AddEgg(data->x, data->y);
                incLadybirdDensity(data->x, data->y, 0); //todo: move to the constructor
            }

        }
        if (ob_type == 1) {
            // Will not create a new larva in a square already occupied
            if((*m_LadybirdDensityMap)(data->x,data->y,1)<blitz::huge((*m_LadybirdDensityMap)(data->x,data->y,1))) {
                incLadybirdDensity( data->x, data->y, 1 ); //todo: move to the constructor
                if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type )) {
                    // We need to reuse an object
                    dynamic_pointer_cast<Ladybird_Larvae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
                    dynamic_pointer_cast<Ladybird_Larvae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setTotalEggsToLay(data->eggs_developed);
                    dynamic_pointer_cast<Ladybird_Larvae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setDateMade(data->DateMade);
                    dynamic_pointer_cast<Ladybird_Larvae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setEatenToday(0);
                    dynamic_pointer_cast<Ladybird_Larvae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->resetMoveCounter();
                    dynamic_pointer_cast<Ladybird_Larvae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->resetAphidsAppetite();
                    IncLiveArraySize(ob_type);
                }
                else {
                    std::unique_ptr <Ladybird_Larvae> new_Larva(new Ladybird_Larvae(data->x, data->y, data->L, this ));
                    new_Larva -> setTotalEggsToLay(data->eggs_developed);
                    //new_Larva = new Ladybird_Larvae( data->x, data->y, data->L, this );
                    //(*TheArray_new)[ ob_type ].insert( (*TheArray_new)[ ob_type ].begin(), new_Larva );
                    PushIndividual(ob_type, std::move(new_Larva));
                    IncLiveArraySize(ob_type);
                }
            }
        }
        if (ob_type == 2)
        {
            if((*m_LadybirdDensityMap)(data->x,data->y,5)<blitz::huge((*m_LadybirdDensityMap)(data->x,data->y,5))){
                incLadybirdDensity(data->x, data->y,5); //todo: move to the constructor
                if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type)) {
                    // We need to reuse an object
                    dynamic_pointer_cast<Ladybird_Pupae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
                    dynamic_pointer_cast<Ladybird_Pupae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setDateMade(data->DateMade);
                    dynamic_pointer_cast<Ladybird_Pupae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setTotalEggsToLay(data->eggs_developed);
                    IncLiveArraySize(ob_type);
                }
                else {
                    std::unique_ptr <Ladybird_Pupae> new_Pupa(new Ladybird_Pupae(data->x, data->y, data->L, this ));
                    new_Pupa -> setTotalEggsToLay(data->eggs_developed);
                    //new_Pupa = new Ladybird_Pupae( data->x, data->y, data->L, this );
                    PushIndividual(ob_type, std::move(new_Pupa));
                    IncLiveArraySize(ob_type);
                }
            }

        }
        if (ob_type == 3)
        {
            if((*m_LadybirdDensityMap)(data->x,data->y,6)<blitz::huge((*m_LadybirdDensityMap)(data->x,data->y,6))){
                incLadybirdDensity(data->x, data->y,6);//todo: move to the constructor
                if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type)) {
                    // We need to reuse an object
                    dynamic_pointer_cast<Ladybird_Adult>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
                    dynamic_pointer_cast<Ladybird_Adult>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setDateMade(data->DateMade);
                    dynamic_pointer_cast<Ladybird_Adult>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setCanReproduce(false);
                    dynamic_pointer_cast<Ladybird_Adult>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setOverwintered(false); //here we have to add setIsHibernating
                    dynamic_pointer_cast<Ladybird_Adult>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setIsHibernating(false);
                    dynamic_pointer_cast<Ladybird_Adult>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ResetAll();
                    dynamic_pointer_cast<Ladybird_Adult>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->setTotalEggsToLay(data->eggs_developed);
                    IncLiveArraySize(ob_type);
                }
                else {
                    std::unique_ptr <Ladybird_Adult> new_Adult(new Ladybird_Adult(data->x, data->y, data->DateMade,  data->can_Reproduce, data->L,  this ));
                    new_Adult -> setTotalEggsToLay(data->eggs_developed);
                    PushIndividual(ob_type, std::move(new_Adult));
                    IncLiveArraySize(ob_type);
                }
            }

        }
    }
}
int Ladybird_Population_Manager::getLadybirdEmergenceChance() const {
    return this->ladybirdconstantslist->LadybirdEmergingChance;
}
int Ladybird_Population_Manager::getLadybirdEmergenceDayLength() const {
    return this->ladybirdconstantslist->LadybirdEmergingDayLength;
}
inline bool Ladybird_Population_Manager::isEmergenceDay(){
    return getLadybirdEmergenceDayLength()<=m_TheLandscape->SupplyDaylength();
}
inline bool Ladybird_Population_Manager::getOvipositionPeriod() {
    auto date = m_TheLandscape->SupplyDayInYear();
    if ((date>getOviPositionPeriodStart())&&(date<getOviPositionPeriodEnd()))
        return true;
    return false;
}
/**
 * \brief The function that updates the daydegrees
 *
 * There are two possible implementations: in this way we update it in the appropriate vectors
 * In the other way we can also create the vector of daydegree for each object, and add vectorially
 * It may be as fast
 *
 * Note: THE FUNCTION UPDATES THE DD SUM ONLY FOR PRE_IMAGO STAGES
 * For adults we have to calculate individually, because the hibernation should not be taken into account
 * */
void Ladybird_Population_Manager::LadybirdUpdateDayDegrees(double temptoday, int today){

    auto DiffWithLTDS {ladybirdconstantslist->LadybirdLDTs};//
    //for (auto &i : DiffWithLTDS){
    for(auto it=DiffWithLTDS.begin();it!=DiffWithLTDS.end(); ++it){
        auto temp{temptoday};
        auto & value {*it};
        temp-=value;
        if (temp>=0){
            (*m_DayDeg)(blitz::Range(0,today),std::distance( DiffWithLTDS.begin(), it ))+=temp;
            (*m_DayDeg)(blitz::Range(today+2,364),std::distance( DiffWithLTDS.begin(), it ))+=temp;
            (*m_DayDeg)(blitz::Range(today+1,today+1),std::distance( DiffWithLTDS.begin(), it ))=0;
        }
    }


}
void Ladybird_Population_Manager::DoBefore()
{
    // This replaces the step function for the eggs
    for (int i=0; i<365; i++) dynamic_cast<Ladybird_Egg_List*>((*m_EList)[i].get())->SetStepDone(false);
    for (int i=0; i<365; i++) dynamic_cast<Ladybird_Egg_List*>((*m_EList)[i].get())->Step();

}

/** \brief The function that calculates the number of eggs that the Adult can lay in each square
 *
 * The function reads the number of Aphids in each square multiplies it by the constant
 * */
int Ladybird_Population_Manager::getLadybirdEggProductionXY(int x, int y){
    int num{0};
    for (int i=0; i<m_AphidSpeciesNum; i++){
        num+= SupplyAphidsNumber(x, y, i);
    }
    return (int) ((double)(num)*getTodaysEggProductionTempFactor()/(ladybirdconstantslist->p_cfg_LadybirdAphidToEggRatio->value()));
}

/*
void Ladybird_Population_Manager::LadybirdUpdateAphidsDensityMap() {
    //auto TodaysEggProductionAphidFactor=LadybirdCalcDailyAphidRelatedEggFactor(m_TheLandscape,Aphids_PM);
    // todo: performance bottleneck

    // debugging
    //cout << "p_aphidsMap = " << endl << (*p_aphidsMap)(blitz::Range(0,100), blitz::Range(0,100)) << endl;
    // end of debugging
    for (int iter3=0; iter3<m_AphidSpeciesNum; iter3++) {
        auto Amap = Aphids_PM[iter3]->getAphidsMap();
        int size1 = Amap->extent(0);
        int size2 = Amap->extent(1);
        int height{Aphids_PM[iter3]->getAphidsCellHeight()};
        int width{Aphids_PM[iter3]->getAphidsCellWidth()};

        for (int iter1 = 0; iter1 < size1; iter1++) {
            for (int iter2 = 0; iter2 < size2; iter2++) {

                (*m_LadybirdAphidsDensityMap)(blitz::Range(iter1 * height, (iter1 + 1) * (height) - 1),
                                              blitz::Range(iter2 * width, (iter2 + 1) * (width) - 1),
                                              blitz::Range(iter3, iter3)) = (
                        (*(Amap))(iter1, iter2) / height / width);




            }
        }

    }


}
*/
/*
void Ladybird_Population_Manager::AphidsUpdateAphidsDensityMap() {
    //auto TodaysEggProductionAphidFactor=LadybirdCalcDailyAphidRelatedEggFactor(m_TheLandscape,Aphids_PM);

    // debugging
    //cout << "p_aphidsMap = " << endl << (*p_aphidsMap)(blitz::Range(0,100), blitz::Range(0,100)) << endl;
    // end of debugging
    //todo: performance bottleneck

    for (int i = 0; i < ladybirdconstantslist->p_cfg_LadybirdAphidSpecies->value().size(); i++) {
        auto Amap = Aphids_PM[i]->getAphidsMap();
        int size1=Amap->extent(0);
        int size2=Amap->extent(1);
        int height {Aphids_PM[i]->getAphidsCellHeight()};
        int width {Aphids_PM[i]->getAphidsCellWidth()};

        for (int iter1=0; iter1<size1; iter1++){
            for (int iter2=0; iter2<size2; iter2++){
                //(*m_LadybirdAphidsDensityMap)(blitz::Range(iter1*height,(iter1+1)*(height)-1),blitz::Range(iter2*width,(iter2+1)*(width)-1))=((*(Amap))(iter1, iter2)/height/width);
                (*Amap)(iter1, iter2)=  (blitz::sum((*m_LadybirdAphidsDensityMap)(blitz::Range(iter1*height,(iter1+1)*(height)-1),blitz::Range(iter2*width,(iter2+1)*(width)-1))));
            }
        }
    }

}
*/

void Ladybird_Population_Manager::DoFirst() {
    //Beetle_Population_Manager::DoFirst();
    double temptoday= m_TheLandscape->SupplyTemp();
    double temp = m_TheLandscape->SupplyTemp();


    // Update the short range distance for today
    calcMaxAdultDailyDistance(temp);
    calcMaxLarvaDailyDistance(temp);

    int today=m_TheLandscape->SupplyDayInYear();
    long day=m_TheLandscape->SupplyGlobalDate();
    LadybirdUpdateDayDegrees(temptoday, today);
    //LadybirdUpdateAphidsDensityMap();
    TodaysEggProductionTempFactor=LadybirdCalcDailyTempRelatedEggFactor(temptoday);


    /** If we are late in the year and want to kill the remaining egg larvae pupae
     * todo: probably should be removed
     * */
    if ((today == 364)&&(this->ladybirdconstantslist->p_cfg_KillOnNyEve->value())) {
        KillOnNYEve();
    }
    else if(today == 0){
            ClearOnNYDay();
            if (day>366){
                // On the first day of the simulation (day 366 including the hidden year) let's not update the hibernaculae
                UpdateHibernaculae();
            }

    }
    /** Update the current population sizes */
    //m_EPopSize = (int)GetLiveArraySize( bob_Egg );
    m_EPopSize=0;
    for ( int i = 0; i < today; i++ ) {
        m_EPopSize += SupplyDailyEggPopSize(i);
    }
    m_LPopSize = (int)GetLiveArraySize( bob_Larva );
    m_PPopSize = (int)GetLiveArraySize( bob_Pupa );
    m_AdPopSize = (int)GetLiveArraySize( bob_Adult);
    // This replaces the BeginStep function for the eggs
    for (int i=0; i<365; i++)
    {
        dynamic_cast<Ladybird_Egg_List*>((*m_EList)[i].get())->BeginStep();
    }
}
void Ladybird_Population_Manager::DoLast(){
    //AphidsUpdateAphidsDensityMap();
    int day = m_TheLandscape->SupplyDayInYear();
    if ((day % 10 == 0)&&(ladybirdconstantslist->p_cfg_LadybirdDebugging->value())){

        XYDump();



    }
}
// DEBUGGING FUNCTION
/**
void Ladybird_Population_Manager::DebugOutputDoFirst(){
    long day = m_TheLandscape->SupplyGlobalDate();
    unsigned int sum{};
    for (int i=0; i<4; i++){ //Egg+Larva+Pupa+Adult=4
        unsigned int number=GetPopulationSize(i);
        int stages_map;
        if (i==0) stages_map = GetPopulationSizeFromDensityMap(0);
        if (i==1){
            stages_map=GetPopulationSizeFromDensityMap(1)+GetPopulationSizeFromDensityMap(2)+GetPopulationSizeFromDensityMap(3)+GetPopulationSizeFromDensityMap(4);
        }
        if (i==2){
            stages_map=GetPopulationSizeFromDensityMap(5);
        }
        if (i==3){
            stages_map=GetPopulationSizeFromDensityMap(6)+GetPopulationSizeFromDensityMap(7);
        }
        sum+=number;
        std::cout<<"DEBUG: day: "<<day<< ", number of type "<<i<<" in Density map: "<< stages_map<<", in TheArray: "<<number<<std::endl;
    }
    std::cout<<"DEBUG: day: Total Objects number in Density map: "<< blitz::sum((*m_LadybirdDensityMap))<<", TheArray: "<<sum<<std::endl;

};

void Ladybird_Population_Manager::DebugOutputDoFirstAdult(){
    long day = m_TheLandscape->SupplyGlobalDate();
    if (day>=662){
        unsigned int number=GetPopulationSize(3);
        int stages_map=GetPopulationSizeFromDensityMap(6)+GetPopulationSizeFromDensityMap(7);

        std::cout<<"DEBUG: day: "<<day<< ", number of Adults in Density map: "<< stages_map<<", in TheArray: "<<number<<std::endl;
    }


};
*/
int Ladybird_Population_Manager::SupplyAphidsNumber(int x, int y, int aphid_species){
    return Aphids_PM.at(aphid_species)->SupplyAphidsNumberLandscapeCO(x, y);
}

void Ladybird_Population_Manager::DeductAphidsNumber(int x, int y, int aphid_species, int number_to_deduct){
    Aphids_PM.at(aphid_species)->deductAphidsNumberLandscapeCO(x, y, number_to_deduct);
}

void Ladybird_Population_Manager::ResetAphidsNumber(int x, int y, int aphid_species){
    Aphids_PM.at(aphid_species)->zeroAphidsNumberLandscapeCO(x, y);
}
/**\brief Function that calculates daily temp-related factor of Egg production */
double Ladybird_Population_Manager::LadybirdCalcDailyTempRelatedEggFactor(double temp) {
    /**TODO: Here we will implement the function described in Xia et al 1999 */
    if (temp<14.2){ //Min. Temperature required to lay an egg is 10°C (Takahashi 1993) however the fitting function is negative for temp<14
        return 0.;
    }
    else{
        return -0.209*temp*temp+11.445*temp-133.93;
    }


}



void Ladybird_Population_Manager::KillOnNYEve(){
    for (unsigned j = 0; j<GetLiveArraySize( bob_Egg ); j++)
    {
        dynamic_pointer_cast <Beetle_Base>(SupplyAnimalSmartPtr(bob_Egg, j))->KillThis(); // Destroy this
    }
    for (unsigned j = 0; j<GetLiveArraySize( bob_Larva ); j++)
    {
        dynamic_pointer_cast <Beetle_Base>(SupplyAnimalSmartPtr(bob_Larva, j))->KillThis(); // Destroy this
    }
    for (unsigned j = 0; j<GetLiveArraySize( bob_Pupa ); j++)
    {
        dynamic_pointer_cast <Beetle_Base>(SupplyAnimalSmartPtr(bob_Pupa, j))->KillThis(); // Destroy this
    }
}
/**\brief To perform on the first day of the year*/
void Ladybird_Population_Manager::ClearOnNYDay(){
    /** Clear the Egg List*/
    for (int i=0; i<365; i++)
        (*m_EList)[i]->EggList.clear();
    /** Clear daydegrees m-x */
    *m_DayDeg = 0;
}

void Ladybird_Population_Manager::decAphids(int x, int y, int num) {
    std::vector<int> aphid_species{};
    aphid_species.reserve(m_AphidSpeciesNum);
for (int i=0; i<m_AphidSpeciesNum; i++){
        aphid_species.push_back(i);
        aphid_species.push_back(i);
    }
// if there is more than one species of the aphid at teh spot we will choose randomly the order of eating
    // the following is until we implemented shuffle correctly
    std::shuffle(aphid_species.begin(), aphid_species.end(), g_std_rand_engine);
    for (int i=0; (i<m_AphidSpeciesNum)&&(num>0); i++){
        if (SupplyAphidsNumber(x,y, aphid_species[i])>=num){
            DeductAphidsNumber(x,y, aphid_species[i], num);
        }

        else{
            num-=SupplyAphidsNumber(x,y, aphid_species[i]);
            ResetAphidsNumber(x,y, aphid_species[i]);

        }
    }


}
long Ladybird_Population_Manager::getAphidsNumber(int x, int y){
    long n{0};
    for (int i=0; i<m_AphidSpeciesNum; i++){
        n+=SupplyAphidsNumber(x, y, i);
    }

    return n;
};
int Ladybird_Population_Manager::getLadybirdDensity(int x, int y, int type) {
    return (*m_LadybirdDensityMap)(x,y,type);
}
bool Ladybird_Population_Manager::IsCanibalismMapNonZero() const{
    //return blitz::any((*m_LadybirdCannibalismMap)!=0); // this is a slower way to do it
    bool returnvalue=(getCannibalisationsCount()!=0);
    return returnvalue;
}
void Ladybird_Population_Manager::setCannibalism(int x, int y, int type, int num) {
    (*m_LadybirdCannibalismMap)(x,y,type)+=num;
}
/*
bool Ladybird_Population_Manager::StepFinished( ) {
    for ( int listindex = 0; listindex < SupplyListIndexSize(); listindex++ ) {
        if (ladybirdconstantslist->CanBeCannibalised[listindex]){
            for (int j = 0; j < GetLiveArraySize( listindex ); j++) {

                if (!(dynamic_pointer_cast <Beetle_Base> (SupplyAnimalSmartPtr(listindex, j))->Cannibalisation())){
                    if (SupplyAnimalSmartPtr(listindex, j)->GetStepDone() == false ) {
                        return false;
                    }
                }

            }

        }
        else{
            for (int j = 0; j < GetLiveArraySize( listindex ); j++) {
                if (SupplyAnimalSmartPtr(listindex, j)->GetStepDone() == false ) {
                    return false;
                }


            }


        }
    }
    return true;
}
*/
bool Ladybird_Population_Manager::StepFinished( ) {
    for ( int listindex = 0; listindex < SupplyListIndexSize(); listindex++ ) {
        for (unsigned j = 0; j < (unsigned)GetLiveArraySize( listindex ); j++) {
            if ( SupplyAnimalSmartPtr(listindex, j)->GetStepDone() == false ) {
                return false;
            }
        }
    }
    if (IsCanibalismMapNonZero()){
        cout<<"DEBUG: All species but eggs finished steps but Cannibalisms count is "<<getCannibalisationsCount()<<endl;
        for (int i=0; i<365; i++) dynamic_cast<Ladybird_Egg_List*>((*m_EList)[i].get())->Cannibalisation(); // running cannibalisation
        if (!IsCanibalismMapNonZero())
        {
            return true;
        }
        else{
            g_msg->Warn( WARN_BUG, " Ladybird_Population_Manager::StepFinished: ","Cannot finish the step due to cannibalisation inballance");
            exit(-1);
        }

    }
    return true;
}
inline bool Ladybird_Population_Manager::IsLocationAllowedForAdult(int x, int y){
    // here we just simply not allowing the ladybird to leave the simulation
    // there are smarter ways to deal with the problem, but for now let it be like that
   if ((x>(m_TheLandscape->SupplySimAreaWidth()-1))||(y>=(m_TheLandscape->SupplySimAreaHeight()-1))||
   x<0||y<0)
       return false;
   TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(x, y);
   TTypesOfVegetation tov = m_TheLandscape->SupplyVegType(x,y);
   if (((ladybirdconstantslist->LadybirdProhibitedTolesForMovement).find(tole) != (ladybirdconstantslist->LadybirdProhibitedTolesForMovement).end())||
    ((ladybirdconstantslist->LadybirdProhibitedTovsForMovement).find(tov) != (ladybirdconstantslist->LadybirdProhibitedTovsForMovement).end()))
       return false;
   else
       return true;
}


inline bool Ladybird_Population_Manager::IsLocationAllowedForLarva(int x, int y){
    // here we just simply not allowing the ladybird to leave the simulation
    // there are smarter ways to deal with the problem, but for now let it be like that
    if ((x>(m_TheLandscape->SupplySimAreaWidth()-1))||(y>=(m_TheLandscape->SupplySimAreaHeight()-1))||
        x<0||y<0)
        return false;
    TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(x, y);
    TTypesOfVegetation tov = m_TheLandscape->SupplyVegType(x,y);
    if (((ladybirdconstantslist->LadybirdProhibitedTolesForMovementLarva).find(tole) != (ladybirdconstantslist->LadybirdProhibitedTolesForMovementLarva).end())||
        ((ladybirdconstantslist->LadybirdProhibitedTovsForMovementLarva).find(tov) != (ladybirdconstantslist->LadybirdProhibitedTovsForMovementLarva).end()))
        return false;
    else
        return true;
}

/**
 * \brief the function that updates the objects numbers wtr to the Cannibalism
 *
 *
 * */

inline bool Ladybird_Population_Manager::IsUniSeasonal() const {
    return ladybirdconstantslist->p_cfg_isLadybirdUniSeasonal->value();
}
bool Ladybird_Population_Manager::getFlyingWeather() {
    if ((m_TheLandscape->SupplyTemp()>ladybirdconstantslist->LadybirdFlyingThreshTemp)
    &&(m_TheLandscape->SupplyWind()<ladybirdconstantslist->LadybirdFlyingThreshWind)){
        return true;
    }
    else {
        return false;
    }
}

void Ladybird_Population_Manager::InitialiseHibernaculae(){

    cout<<"Initialising Hibernaculae"<<'\n';


    int polyreflistsize {m_TheLandscape->SupplyLargestPolyNumUsed()};
    double hibernaculachance=((double) (ladybirdconstantslist->InitialHibernaculaeNumber))/((double) polyreflistsize);
    m_ListOfHibernaculae.resize(polyreflistsize);
    //int polyreflistsize = m_TheLandscape->SupplyNumberOfPolygons();
    for (int i = 0; i<polyreflistsize; i++){
        TTypesOfLandscapeElement tole= m_TheLandscape->SupplyElementType(i);
        TTypesOfVegetation tov = m_TheLandscape->SupplyVegType(i);
        // we set all the bits to nil
        m_ListOfHibernaculae[i].reset();
            if (((ladybirdconstantslist->LadybirdAggregationToles).find(tole) != (ladybirdconstantslist->LadybirdAggregationToles).end())||
                                                        ((ladybirdconstantslist->LadybirdAggregationTovs).find(tov) != (ladybirdconstantslist->LadybirdAggregationTovs).end()))
            {
                // Can potentially become a hibernacula let's throw a dice if it was used before the first year year
                if (g_rand_uni()<hibernaculachance){
                    m_ListOfHibernaculae[i].set(HIBERNACULA_MEMORY-1, true);
                    if (ladybirdconstantslist->p_cfg_LadybirdDebugging->value()) {
                        cout << "Polyref #" << i << ". Bitset is:" << m_ListOfHibernaculae[i] << '\n';
                    }
                }
            }
    }

}
void Ladybird_Population_Manager::UpdateHibernaculae(){
    int polyreflistsize {m_TheLandscape->SupplyLargestPolyNumUsed()};
    // creating a count vector and initializing it with 0
    std::vector<int> hibernacula_count(polyreflistsize,0);
    // First we will count the number of overwintering adults in polyrefs (we only need this step if we want to implement a threshold of a number of individuals overwintering needed,
    // for  a polyref to be regarded as a hibernacula)
    auto adult_size = (unsigned)GetLiveArraySize( 3 );
    for (int i = 0; i<adult_size; i++){
        int x{0},y{0};
        SupplyLocXY(3, i, x, y);
        int polynum=m_TheLandscape->SupplyPolyRef(x,y);
        // we count only the hibernating ones
        if (((dynamic_pointer_cast<Ladybird_Adult>(SupplyAnimalSmartPtr(3, i)))->CurrentBState)==tobs_Hibernating){
            hibernacula_count[polynum]++;
        }

    }
    // now we go through this vector and for the values higher than threshold we will
    for (int i = 0; i<polyreflistsize; i++){
        m_ListOfHibernaculae[i]>>=1;
        if (hibernacula_count[i]>=ladybirdconstantslist->HibernaculaThreshold){
            m_ListOfHibernaculae[i].set(HIBERNACULA_MEMORY-1, true);
            if (ladybirdconstantslist->p_cfg_LadybirdDebugging->value()) {
                cout << "DEBUG: Updating hibernaculae. Polyref #" << i << " has " << hibernacula_count[i]
                     << " beetles.";
                cout << "Bitset is:" << m_ListOfHibernaculae[i] << '\n';
            }
        }
    }
}
bool Ladybird_Population_Manager::IsHibernacula(int a_polyref) {
    return m_ListOfHibernaculae[a_polyref].any();
}
// FOR DEBUGGING ONLY _START
/***
 * The function that writes headers to the dump file
 *
 *
 * */
void Ladybird_Population_Manager::WriteHeaders(const shared_ptr<ofstream>& a_file, std::vector<std::string> a_headers)
{
    for (int i = 0; i < a_headers.size(); ++i)
    {
        if (i <= a_headers.size() - 2)
        {
            (*a_file) << a_headers[i];
            (*a_file) << '\t';
        }
        else
        {
            (*a_file) << a_headers[i];
            (*a_file) << endl;
        }
    }
}
void Ladybird_Population_Manager::XYDump() {
    // ob_types: 0-3
    long int day = m_TheLandscape->SupplyGlobalDate();
    for (int ob_type=0; ob_type<4; ob_type++){
        int total = int(GetLiveArraySize(ob_type));
        for (int j = 0; j<total; j++){
            int x{0},y{0};
            SupplyLocXY(ob_type, j, x, y);
            int poly = m_TheLandscape->SupplyPolyRef(x, y);
            (*m_LadybirdXYDumpFile) << day << '\t'<< x << '\t' << y << '\t' << poly << '\t'<< ob_type<< endl;
        }


    }


}
/** \brief the amount of objects of specific type according to the density map
 * (FOR DEBUGGING
 * */
int Ladybird_Population_Manager::GetPopulationSizeFromDensityMap(int type) {
    // will do the sum of the blitz array for the third dimension of type
    int temp = (int) blitz::sum((*m_LadybirdDensityMap)(blitz::Range::all(), blitz::Range::all(), blitz::Range(type, type)));
    return temp;

}

unsigned Ladybird_Population_Manager::SupplyTilePopulation(unsigned x, unsigned y, int lifestage) {
    if (lifestage == bob_Adult) {
        return getLadybirdDensity(x, y, 6);
    } else {
        if (lifestage == bob_Larva) {
            return getLadybirdDensity(x, y, 1)+getLadybirdDensity(x, y, 2)+getLadybirdDensity(x, y, 3)+getLadybirdDensity(x, y, 4);
        } else {
            if (lifestage == bob_Pupa){
                return getLadybirdDensity(x, y, 5);
            }
            else{
                if (lifestage == bob_Egg){
                    return getLadybirdDensity(x, y, 0);
                }
            }
        }
    }
}
void Ladybird_Population_Manager::calcMaxAdultDailyDistance(double temp) {
    int daylength = m_TheLandscape->SupplyDaylength(); // daylength in minutes
    if (temp<4){
        m_MaxAdultDistance = 0;
    }
    else{
        if (temp<10){
            m_MaxAdultDistance = (int) (((double) daylength) / 180);
        }
        else{
            m_MaxAdultDistance = (int) (0.0071*(25+(temp-14)*(115./20.))*daylength);
        }
    }
    m_MaxAdultDistance = m_MaxAdultDistance / (ladybirdconstantslist->p_cfg_LadybirdMeanHoppingDistance->value());
}
void Ladybird_Population_Manager::calcMaxLarvaDailyDistance(double temp)  {
    int daylength = m_TheLandscape->SupplyDaylength(); // daylength in minutes
    if (temp<4){
        m_MaxLarvaDistance = 0;
    }
    else{
        if (temp<10){
            m_MaxLarvaDistance = (int)(((double) daylength) / 360);
        }
        else{
            m_MaxLarvaDistance = (int) (0.0071*(25+(temp-14)*(115./20.))*daylength/2.);
        }
    }
    m_MaxLarvaDistance = m_MaxLarvaDistance /  (ladybirdconstantslist->p_cfg_LadybirdLarvaMeanHoppingDistance->value());
}
int Ladybird_Population_Manager::getMaxLarvaDailyDistance(){
    return m_MaxLarvaDistance;
}
int Ladybird_Population_Manager::getMaxAdultDailyDistance(){
    return m_MaxAdultDistance;
}
// FOR DEBUGGING ONLY _END
//-----------------------------------------------------------------------------
// LADYBIRDCONSTANTCLASS
//-----------------------------------------------------------------------------
LadybirdConstantClass::LadybirdConstantClass(): LadybirdLarvalStagesNum{4}, LadybirdEmergingDayLength{755},
                                                LadybirdEmergingChance{1}, LadybirdMaxClumpSize{20},
                                                p_cfg_KillOnNyEve{ new CfgBool{"LADYBIRD_KILL_ON_NY_EVE", CFG_CUSTOM, false}},
                                                p_cfg_isLadybirdUniSeasonal{ new CfgBool{"LADYBIRD_IS_UNISEASONAL", CFG_CUSTOM, true}},
                                                p_cfg_LadybirdExhaustionInDispersal{ new CfgBool{"LADYBIRD_EXHAUSTION_IN_DISPERSAL", CFG_CUSTOM, false}},
                                                p_cfg_LadybirdYearlyMortality{new CfgFloat{"LADYBIRD_YEARLY_MORT",CFG_CUSTOM, 1.0}},
                                                p_cfg_LadybirdOvipositionPeriodStart{ new CfgInt{"LADYBIRD_OVIPOSITION_START", CFG_CUSTOM, 185}},// Currently Jan 1 (since the oviposition is solely regulated by the aphids abundance), previously July 1st: 182
                                                p_cfg_LadybirdOvipositionPeriodEnd{ new CfgInt{"LADYBIRD_OVIPOSITION_END", CFG_CUSTOM, 244}},// Sept 1st
                                                p_cfg_ladybirdstartnos{new CfgInt{"LADYBIRD_STARTNO",CFG_CUSTOM, 5000}},
                                                p_cfg_ladybirdstarteggsno{new CfgInt{"LADYBIRD_STARTEGGSNO",CFG_CUSTOM, 100}},
                                                p_cfg_LadybirdAphidToEggRatio{new CfgFloat{"LADYBIRD_APHID_TO_EGG_RATIO",CFG_CUSTOM, 500.0}},
                                                p_cfg_LadybirdDailyEggMortality{new CfgFloat{"LADYBIRD_DAILY_EGG_MORT",CFG_CUSTOM, 0.0}}, //the value shouldn't be used we have a temperature related mortality, no backgrounfd mortality
                                                p_cfg_LadybirdDailyLarvaeMortality{new CfgFloat{"LADYBIRD_DAILY_LARVAE_MORT",CFG_CUSTOM, 0.00001}},
                                                p_cfg_LadybirdDailyPupaeMortality{new CfgFloat{"LADYBIRD_DAILY_PUPAE_MORT",CFG_CUSTOM, 0.00001}},
                                                p_cfg_LadybirdDailyAdultMortality{new CfgFloat{"LADYBIRD_DAILY_ADULT_MORT",CFG_CUSTOM, 0.00000001}},
                                                p_cfg_LadybirdExtremeTempMin{new CfgFloat{"LADYBIRD_EXTREME_MIN",CFG_CUSTOM, -20}},
                                                p_cfg_LadybirdExtremeTempMinHibernatingFactor{new CfgFloat{"LADYBIRD_EXTREME_MIN_HIB",CFG_CUSTOM, 0.2}},
                                                p_cfg_LadybirdExtremeTempMax{new CfgFloat{"LADYBIRD_EXTREME_MAX",CFG_CUSTOM, 40}},
                                                p_cfg_LadybirdUnnecessaryMovementChance{new CfgFloat{"LADYBIRD_MOVEMENT_CHANCE",CFG_CUSTOM, 1}},
                                                p_cfg_LadybirdCannibalisationChance{new CfgFloat{"LADYBIRD_CANNIBALISATION_CHANCE",CFG_CUSTOM, 1}},
                                                p_cfg_LadybirdDailyEggMax{new CfgInt{"LADYBIRD_DAILY_EGG_MAX",CFG_CUSTOM, 2}},//todo: unused-> remove
                                                p_cfg_LadybirdMaxDistance{new CfgInt{"LADYBIRD_MAX_DAILY_DISTANCE", CFG_CUSTOM, 40}},
                                                p_cfg_LadybirdMaxLongRangeFlight{new CfgInt{"LADYBIRD_MAX_LONG_RANGE_FLIGHT", CFG_CUSTOM, 1500}},
                                                p_cfg_LadybirdMaxLarvaDistance{new CfgInt{"LADYBIRD_MAX_LARVA_DAILY_DISTANCE", CFG_CUSTOM, 12}},
                                                p_cfg_LadybirdAphidDemand{new CfgInt{"LADYBIRD_APHID_DEMAND", CFG_CUSTOM, 30}},
                                                p_cfg_LadybirdAphidSensingDistance{new CfgInt{"LADYBIRD_APHID_SENSING_DISTANCE", CFG_CUSTOM, 150}},
                                                p_cfg_LadybirdLarvaAphidSensingDistance{new CfgInt{"LADYBIRD_LARVA_APHID_SENSING_DISTANCE", CFG_CUSTOM, 14}},
                                                p_cfg_LadybirdMeanHoppingDistance{new CfgInt{"LADYBIRD_MEAN_HOPPING_DISTANCE", CFG_CUSTOM, (p_cfg_LadybirdAphidSensingDistance->value())/10}},
                                                p_cfg_LadybirdStdHoppingDistance{new CfgInt{"LADYBIRD_STD_HOPPING_DISTANCE", CFG_CUSTOM, 5}},
                                                p_cfg_LadybirdLarvaMeanHoppingDistance{new CfgInt{"LADYBIRD_LARVA_MEAN_HOPPING_DISTANCE", CFG_CUSTOM, (p_cfg_LadybirdLarvaAphidSensingDistance->value())/10}},
                                                p_cfg_LadybirdLarvaStdHoppingDistance{new CfgInt{"LADYBIRD_LARVA_STD_HOPPING_DISTANCE", CFG_CUSTOM, 5}},
                                                p_cfg_LadybirdLarvaMaxHoppingDistance{new CfgInt{"LADYBIRD_LARVA_MAX_HOPPING_DISTANCE", CFG_CUSTOM, 2}},
                                                p_cfg_LadybirdMaxShortRangeAttempts{new CfgInt{"LADYBIRD_MAX_SHORT_RANGE_ATTEMPTS", CFG_CUSTOM, 8}},
                                                /** The assumption here is that the adult ladybird can survive approximately two weeks without aphid food */
                                                p_cfg_LadybirdMaxLongRangeAttempts{new CfgInt{"LADYBIRD_MAX_LONG_RANGE_ATTEMPTS", CFG_CUSTOM, 14}},
                                                p_cfg_LadybirdTotalEggsMax{new CfgInt{"LADYBIRD_MAX_TOTAL_EGGS", CFG_CUSTOM, 250}},
                                                p_cfg_LadybirdMaxHibernationTries{new CfgInt{"LADYBIRD_MAX_HIBERNATION_TRIES", CFG_CUSTOM, 500}},
                                                p_cfg_LadybirdMinMaturation{new CfgInt{"LADYBIRD_MIN_MATURATION_LENGTH", CFG_CUSTOM, 4}},
                                                p_cfg_LadybirdMaxAphids{new CfgInt{"LADYBIRD_MAX_APHIDS_NUMBER", CFG_CUSTOM, 100000}},
                                                p_cfg_LadybirdAggregationStart{new CfgInt{"LADYBIRD_AGGREGATION_START", CFG_CUSTOM, 263}},
                                                p_cfg_LadybirdPupaStageMaxLength{new CfgInt{"LADYBIRD_MAX_PUPA_STAGE_LENGTH", CFG_CUSTOM, 60}},
                                                p_cfg_LadybirdAggregationEnd{new CfgInt{"LADYBIRD_AGGREGATION_END", CFG_CUSTOM, 293}},
                                                p_cfg_LongRangeMaxTries{new CfgInt{"LADYBIRD_AGGREGATION_END", CFG_CUSTOM, 50}},
                                                p_cfg_LongRangeMinDict{new CfgInt{"LADYBIRD_AGGREGATION_END", CFG_CUSTOM, 30}},
                                                p_cfg_LadybirdAphidSpecies{new CfgArray_Int{"LADYBIRD_APHID_SPECIES", CFG_CUSTOM, 1, vector<int> { 25}}},
                                                p_cfg_LadybirdDebugging{ new CfgBool{"LADYBIRD_DEBUGGING", CFG_CUSTOM, false}},
                                                p_cfg_LadybirdHibernationChance{new CfgFloat{"LADYBIRD_HIBERNATION_CHANCE",CFG_CUSTOM, 0.1}},
                                                LadybirdMapOfStepDone{new std::map<TTypesOfBeetleState, bool>{
{tobs_Initiation,  true},
// Egg
{tobs_EDeveloping, true},
{tobs_Hatching,    false},
{tobs_EDying,      false},
// Larva
{tobs_LDeveloping, true},
{tobs_Pupating,    false},
{tobs_LDying,      false},
// Pupa
{tobs_PDeveloping, true},
{tobs_Emerging,    false},
{tobs_PDying,      false},
// Adult
{tobs_Foraging,    true},
{tobs_Aggregating, true},
{tobs_Hibernating, true},
{tobs_Dispersing,  true},
{tobs_ADying,      false},
// Destroy
{tobs_Destroy,     true},

}
}
{
    TLadybirdToleTovs LadybirdToleTovs;
    LadybirdProhibitedTolesForMovement=LadybirdToleTovs.LadybirdProhibitedToles.getList();
    LadybirdProhibitedTovsForMovement=LadybirdToleTovs.LadybirdProhibitedTovs.getList();
    LadybirdProhibitedTolesForMovementLarva=LadybirdToleTovs.LadybirdProhibitedTolesLarva.getList();
    LadybirdProhibitedTovsForMovementLarva=LadybirdToleTovs.LadybirdProhibitedTovsLarva.getList();
    LadybirdLongRangeTovs=LadybirdToleTovs.LadybirdLongRangeForagingTovs.getList();
    LadybirdLongRangeToles=LadybirdToleTovs.LadybirdLongRangeForagingToles.getList();
    LadybirdAggregationTovs=LadybirdToleTovs.LadybirdAggregationTovs.getList();
    LadybirdAggregationToles=LadybirdToleTovs.LadybirdAggregationToles.getList();
    // Now we will assign some const from config values. (where it is needed):
    LongRangeMaxTries=p_cfg_LongRangeMaxTries->value();
    LongRangeMinDict=p_cfg_LongRangeMinDict->value();
}
