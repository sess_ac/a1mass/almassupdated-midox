//
// Version of May, 31, 2020
// Last modified by Andrey Chuhutin on 12/02/2020
/*
*******************************************************************************************************
Copyright (c) 2020, Andrey Chuhutin, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************************************
*/
//----------------------------------------------------------------------
// Created by Andrey Chuhutin on 04/10/2021.
//
#include "Poecilus_All.h"
#include "Poecilus_toletov.h"
//#include <cstring>
#include <iostream>
#include <random>


extern double g_SpeedyDivides[2001];
extern std::default_random_engine g_std_rand_engine;
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_ReallyBigOutput_used;
extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;

std::shared_ptr<Poecilus_Population_Manager> Poecilus_Base::m_OurPoecilusPopulation;
//----------------------------------------------------------------------------------------------
//                                              Poecilus BASE
//----------------------------------------------------------------------------------------------
Poecilus_Base::Poecilus_Base(int x, int y, Landscape *L, Poecilus_Population_Manager* PPM):Beetle_Base(x, y, L, static_cast<Beetle_Population_Manager *>(PPM)){
    ;
}

bool Poecilus_Base::OnFarmEvent(FarmToDo event) {
    if (PoecilusFertilizerMortality(event)){
        TTypesOfBeetleState DyingState = getDyingState();
        CurrentBState=DyingState;
        return true;
    }
    else{
        return Beetle_Base::OnFarmEvent(event);
    }


}


double Poecilus_Base::MovementFactor(int newx, int newy, int oldx, int oldy){
    double newLA = m_OurLandscape->SupplyLATotal(newx, newy);
    double oldLA = m_OurLandscape->SupplyLATotal(oldx, oldy);
    double temp = m_OurLandscape->SupplyTemp();
    double tempthresh = m_OurPoecilusPopulation->SupplyTempThresh();
    return (newLA - oldLA)*(temp-tempthresh);
}

double Poecilus_Base::getPoecilusCountersThreshold(){
    return m_OurPoecilusPopulation->poecilusconstantslist->PoecilusCountersThreshold;
}
bool Poecilus_Base::PoecilusFertilizerMortality(FarmToDo event){
    return false;
}
double Poecilus_Base::getExtremeTempThreshold() const {
    return m_OurPoecilusPopulation->poecilusconstantslist->PoecilusHibernatedExtremeTempThreshold;
}
bool Poecilus_Base::ExtremeTempIsOn() const{
    return m_OurPoecilusPopulation->poecilusconstantslist->PoecilusExtremeTempIsOn;
}
double Poecilus_Base::getExtremeTempMin() const {
    return getExtremeTempThreshold();
}
double Poecilus_Base::getMinTemp() {
    return m_OurLandscape->SupplySoilTemp();
}
double Poecilus_Base::getMaxTemp() {
    return m_OurLandscape->SupplySoilTemp();
}
//----------------------------------------------------------------------------------------------
//                                              Poecilus EGG
//----------------------------------------------------------------------------------------------

Poecilus_Egg_List::Poecilus_Egg_List(int today,Poecilus_Population_Manager* BPM, Landscape* L):
                    Beetle_Egg_List(today,static_cast<Beetle_Population_Manager *>(BPM), L),
                    Poecilus_Base(0,0,L,BPM),
                    Beetle_Base(0, 0, L, static_cast<Beetle_Population_Manager *>(BPM)){
    m_DateMade = m_OurLandscape->SupplyGlobalDate();
}
void Poecilus_Egg_List::ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) {
    m_DateMade = m_OurLandscape->SupplyGlobalDate();
    Beetle_Egg_List::ReInit(x,y,L,BPM);
}
double Poecilus_Egg_List::getDailyEggMortality() const {
    return m_OurPoecilusPopulation->poecilusconstantslist->DailyEggMort;
}
void Poecilus_Egg_List::ClearFromMap(int X, int Y) {
    if (!m_OurPoecilusPopulation->decPoecilusDensity(X, Y, 0)){
        g_msg->Warn( WARN_BUG, " Poecilus_Egg_List::ClearFromMap "," Clearing non-existant agent from the map");
        exit(-1);
    }

}

bool Poecilus_Egg_List::AddToMap(int X, int Y) {
    return m_OurPoecilusPopulation->incPoecilusDensity(X, Y, 0);
}

TTypesOfBeetleState Poecilus_Egg_List::st_Hatch()
{


    int size=(int)EggList.size();

    for (int i=0; i<size; i++)
    {
        std::unique_ptr <struct_Beetle> aps(new struct_Beetle);  //   instead of new struct_Beetle; using C++11
        aps->x   = EggList[i].m_x;
        aps->y   = EggList[i].m_y;
        aps->L   = m_OurLandscape;


        ClearFromMap(aps->x, aps->y); // remove from map will add to map in CreateObjects

        m_OurPoecilusPopulation->CreateObjects(1, this, nullptr, std::move(aps), 1);
    }
    EggList.erase(EggList.begin(),EggList.end()); // empty the list

    return tobs_Initiation;
#ifdef __LAMBDA_RECORD
    m_OurPopulation->LamdaBirth(m_Location_x,m_Location_y,size);
#endif

}
//----------------------------------------------------------------------------------------------
//                                              Poecilus LARVA
//----------------------------------------------------------------------------------------------
Poecilus_Larvae::Poecilus_Larvae( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM ):
                Beetle_Larvae(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                Poecilus_Base(0,0,L,BPM),
                Beetle_Base(x, y, L, static_cast<Beetle_Population_Manager *>(BPM)){
    m_DateMade = m_OurLandscape->SupplyGlobalDate();
}
void Poecilus_Larvae::ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) {
    m_DateMade = m_OurLandscape->SupplyGlobalDate();
    Beetle_Larvae::ReInit(x,y,L,BPM);
}
double Poecilus_Larvae::getDailyLarvaeMortality() const{
    return m_OurPoecilusPopulation->poecilusconstantslist->DailyLarvaeMort;
}
double Poecilus_Larvae::getDailyMortalityRate() const{
    return getDailyLarvaeMortality();
}

bool Poecilus_Larvae::PoecilusFertilizerMortality(FarmToDo event){
    auto got = m_OurPoecilusPopulation->poecilusconstantslist-> TreatmentMortalities.find (event);
    if (got==m_OurPoecilusPopulation->poecilusconstantslist-> TreatmentMortalities.end()){
        return false;
    }
    else {
        double random = g_rand_uni();
        if (random<((got->second))){
            return true;
        }
        else{
            return false;
        }
    }
}

void Poecilus_Larvae::EndStep() {
    Do_Cannibalism();
    Beetle_Larvae::EndStep();
}
void Poecilus_Larvae::Do_Cannibalism(){
    // first we check how many larvae is there
    int x = m_Location_x;
    int y = m_Location_y;
    double mortality_chance{0};
    int popsize0 = m_OurPoecilusPopulation->getPoecilusDensity(x, y, 1);
    int popsize1 = m_OurPoecilusPopulation->getPoecilusDensity(x, y, 2);
    int popsize2 = m_OurPoecilusPopulation->getPoecilusDensity(x, y, 3);
    int popsize = popsize0 + popsize1 + popsize2;
    // if higher than threshold
    if (popsize>getCannibalismThreshold()){
        // depending on the instar calculate the dying chance
        int level = 0;
        if(popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(0)){
            level = 1;
        }
        if (popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(1)){
            level = 2;
        }
        if (popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(2)){
            level = 3;
        }
        if (popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(3)){
            level = 4;
        }
        if (popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(4)){
            level = 5;
        }
        double Cannibalism_L1 = m_OurPoecilusPopulation->poecilusconstantslist->Cannibalism_L1.at(level);
        double Cannibalism_L2 = m_OurPoecilusPopulation->poecilusconstantslist->Cannibalism_L2.at(level);
        double Cannibalism_L3 = m_OurPoecilusPopulation->poecilusconstantslist->Cannibalism_L3.at(level);
        if (getLarvalStage()==0){
             mortality_chance = Cannibalism_L1 + Cannibalism_L2 + Cannibalism_L3;
        }
        else {
            if (getLarvalStage()==1){
                mortality_chance = Cannibalism_L2 + Cannibalism_L3;
            }
            else{// the last instar can be eaten only by the last instar
                mortality_chance = Cannibalism_L3;
            }
        }
        // check whether it is dead
        if (g_rand_uni()<mortality_chance){
            // change the state into Dying
            CurrentBState=getDyingState();
        }

    }




}

void Poecilus_Larvae::ClearFromMap(int X, int Y) {
    if (!m_OurPoecilusPopulation->decPoecilusDensity(X, Y, 1+getLarvalStage())){
        g_msg->Warn( WARN_BUG, " Poecilus_Larvae::ClearFromMap "," Clearing non-existent agent from the map @"+std::to_string(X)+","+std::to_string(Y));
        exit(-1);
    }

}

bool Poecilus_Larvae::AddToMap(int X, int Y) {
    return m_OurPoecilusPopulation->incPoecilusDensity(X, Y, 1+getLarvalStage());
}
int Poecilus_Larvae::getCannibalismThreshold(){
    return m_OurPoecilusPopulation->poecilusconstantslist->CannibalismThreshold;
}
TTypesOfBeetleState Poecilus_Larvae::st_Pupate() {
    std::unique_ptr <struct_Beetle> LS(new struct_Beetle);


    LS->x   = m_Location_x;
    LS->y   = m_Location_y;
    LS->L   = m_OurLandscape;
    LS->BPM = m_OurPopulation;

    // pupa resets the datemade


    // object destroyed by OwnerRTS--> C++11 unique_pointer + std::move
    ClearFromMap(LS->x, LS->y);// removing from map will add in CreateObjects

    m_OurPoecilusPopulation->CreateObjects(2,this,nullptr,std::move(LS),1);
    m_CurrentStateNo=-1; //Destroys object
    return tobs_Destroy;
}

bool Poecilus_Larvae::PoecilusFertilizerMortality(FarmToDo event){
    auto got = m_OurPoecilusPopulation->poecilusconstantslist-> TreatmentMortalities.find (event);
    if (got==m_OurPoecilusPopulation->poecilusconstantslist-> TreatmentMortalities.end()){
        return false;
    }
    else {
        double random = g_rand_uni();
        if (random<got->second){
            return true;
        }
    }
}

void Poecilus_Larvae::EndStep() {
    Do_Cannibalism();
    Beetle_Larvae::EndStep();
}
void Poecilus_Larvae::Do_Cannibalism(){
    // first we check how many larvae is there
    int x = m_Location_x;
    int y = m_Location_y;
    double mortality_chance{0};
    int popsize0 = m_OurPoecilusPopulation->getPoecilusDensity(x, y, 1);
    int popsize1 = m_OurPoecilusPopulation->getPoecilusDensity(x, y, 2);
    int popsize2 = m_OurPoecilusPopulation->getPoecilusDensity(x, y, 3);
    int popsize = popsize0 + popsize1 + popsize2;
    // if higher than threshold
    if (popsize>getCannibalismThreshold()){
        // depending on the instar calculate the dying chance
        int level = 0;
        if(popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(0)){
            level = 1;
        }
        if (popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(1)){
            level = 2;
        }
        if (popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(2)){
            level = 3;
        }
        if (popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(3)){
            level = 4;
        }
        if (popsize>m_OurPoecilusPopulation->poecilusconstantslist->CannibalismLevelThresholds.at(4)){
            level = 5;
        }
        double Cannibalism_L1 = m_OurPoecilusPopulation->poecilusconstantslist->Cannibalism_L1.at(level);
        double Cannibalism_L2 = m_OurPoecilusPopulation->poecilusconstantslist->Cannibalism_L2.at(level);
        double Cannibalism_L3 = m_OurPoecilusPopulation->poecilusconstantslist->Cannibalism_L3.at(level);
        if (getLarvalStage()==0){
             mortality_chance = Cannibalism_L1 + Cannibalism_L2 + Cannibalism_L3;
        }
        else {
            if (getLarvalStage()==1){
                mortality_chance = Cannibalism_L2 + Cannibalism_L3;
            }
            else{// the last instar can be eaten only by the last instar
                mortality_chance = Cannibalism_L3;
            }
        }
        // check whether it is dead
        if (g_rand_uni()<mortality_chance){
            // change the state into Dying
            CurrentBState=getDyingState();
        }

    }




}

void Poecilus_Larvae::ClearFromMap(int X, int Y) {
    if (!m_OurPoecilusPopulation->decPoecilusDensity(X, Y, 1+getLarvalStage())){
        g_msg->Warn( WARN_BUG, " Poecilus_Larvae::ClearFromMap "," Clearing non-existent agent from the map");
        exit(-1);
    }

}

bool Poecilus_Larvae::AddToMap(int X, int Y) {
    return m_OurPoecilusPopulation->incPoecilusDensity(X, Y, 1+getLarvalStage());
}
int Poecilus_Larvae::getCannibalismThreshold(){
    return m_OurPoecilusPopulation->poecilusconstantslist->CannibalismThreshold;
}
//----------------------------------------------------------------------------------------------
//                                              Poecilus PUPA
//----------------------------------------------------------------------------------------------
Poecilus_Pupae::Poecilus_Pupae( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM ):
                Beetle_Pupae(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                Poecilus_Base(0,0,L,BPM),
                Beetle_Base(x, y, L, static_cast<Beetle_Population_Manager *>(BPM)){
    m_DateMade = m_OurLandscape->SupplyGlobalDate();
}
void Poecilus_Pupae::ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) {
    m_DateMade = m_OurLandscape->SupplyGlobalDate();
    Beetle_Pupae::ReInit(x,y,L,BPM);
}
double Poecilus_Pupae::getDailyPupaeMortality() const{
    return m_OurPoecilusPopulation->poecilusconstantslist->DailyPupaeMort;
}
double Poecilus_Pupae::getDailyMortalityRate() const{
    return getDailyPupaeMortality();
}

bool Poecilus_Pupae::PoecilusFertilizerMortality(FarmToDo event){
    auto got = m_OurPoecilusPopulation->poecilusconstantslist-> TreatmentMortalities.find (event);
    if (got==m_OurPoecilusPopulation->poecilusconstantslist-> TreatmentMortalities.end()){
        return false;
    }
    else {
        double random = g_rand_uni();
        if (random<got->second){
            return true;
        }
        else{
            return false;
        }
    }
}

void Poecilus_Pupae::ClearFromMap(int X, int Y) {
    if (!m_OurPoecilusPopulation->decPoecilusDensity(X, Y, 4)){
        g_msg->Warn( WARN_BUG, " Poecilus_Pupae::ClearFromMap "," Clearing non-existant agent from the map");
        exit(-1);
    }

}

bool Poecilus_Pupae::AddToMap(int X, int Y) {
    return m_OurPoecilusPopulation->incPoecilusDensity(X, Y, 4);
}
TTypesOfBeetleState Poecilus_Pupae::st_Emerge()

{
    std::unique_ptr <struct_Beetle> LS(new struct_Beetle);

    LS->x   = m_Location_x;
    LS->y   = m_Location_y;
    LS->L   = m_OurLandscape;
    LS->BPM = m_OurPopulation;

    // Adult resets the date made


    // object destroyed by OwnerRTS--> C++11 unique_pointer + std::move
    ClearFromMap(LS->x, LS->y);// removing from map will add to map in CreateObjects

    m_OurPoecilusPopulation->CreateObjects(3,this,nullptr,std::move(LS),1);
    m_CurrentStateNo=-1; //Destroys object
    return tobs_Destroy;
}
//----------------------------------------------------------------------------------------------
//                                              Poecilus ADULT
//----------------------------------------------------------------------------------------------
Poecilus_Adult::Poecilus_Adult( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM ):
                Beetle_Adult(x,y,L,static_cast<Beetle_Population_Manager *>(BPM)),
                Poecilus_Base(0,0,L,BPM),
                Beetle_Base(x, y, L, static_cast<Beetle_Population_Manager *>(BPM)){
    Init();
    m_DateMade = m_OurLandscape->SupplyGlobalDate();
    m_DayMade = m_OurLandscape->SupplyDayInYear();
    m_TodaysHoppingDistance = 0;
    m_MaxDailyDistance = 0;
    m_eggsLaid = 0;
}
void Poecilus_Adult::ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) {
    m_DateMade = m_OurLandscape->SupplyGlobalDate();
    m_DayMade = m_OurLandscape->SupplyDayInYear();
    m_TodaysHoppingDistance = 0;
    m_MaxDailyDistance = 0;
    m_eggsLaid = 0;
    m_IsMature = false;
    m_EggFormationCounter = 0;
    m_OvipositionPeriodCounter = 0;
    m_OvipositionFinished = false;
    hasOverwintered = false;
    Beetle_Adult::ReInit(x,y,L,BPM);
}
int Poecilus_Adult::getAge() const{
    long date = m_OurLandscape->SupplyGlobalDate();
    long birth = m_DateMade; //todo check m_DayMade is counting since the Imago Creation
    if (date-birth<238){
        return 0;
    }
    else {
        if ((date - birth) < (238 + 365)){
            return 1;
        }
        else {
            return 2;
        }
    }
};
double Poecilus_Adult::getDailyMortalityRate() const{
    int age = getAge();
    if (age<1){
        return m_OurPoecilusPopulation->poecilusconstantslist->DailyAdultMortality1;
    }
    else{
        if (age<2){
            return m_OurPoecilusPopulation->poecilusconstantslist->DailyAdultMortality2;
        }
        else{
            return m_OurPoecilusPopulation->poecilusconstantslist->DailyAdultMortality3;
        }
    }
}
void Poecilus_Adult::CheckMaturity(){
    int dateinyear = m_OurLandscape->SupplyDayInYear();
    int startmaturation = m_OurPoecilusPopulation->poecilusconstantslist->PoecilusStartMaturationDay;
    if ((!getIsMature())&&(dateinyear>startmaturation)&&(hasOverwintered)&&(CurrentBState!=tobs_Hibernating)){
        double number = g_rand_uni(); //between 0 and 1
        if (number<getMaturationChance()){
            setMaturity(true);
        }

    }
}

double Poecilus_Adult::getMaturationChance()
{
    return m_OurPoecilusPopulation->poecilusconstantslist->p_cfg_PoecilusDailyMaturationChance->value();
};
void Poecilus_Adult::CheckReproduction(){
    // only if mature and still cannot reproduce
    if (getIsMature()&&!getCanReproduce()&&(CurrentBState!=tobs_Hibernating)){
        double temp = m_OurLandscape->SupplyTemp();
        double deltaformation = getDailyEggFormationProgress(temp);
        UpdateEggFormationProgress(deltaformation);
        if (getCurrentEggFormationProgress()>getEggFormationThreshold()){
            setCanReproduce(true);
            ResetCurrentEggFormationProgress();
        }


    }
}
/** The function that returns the progress in egg formation time depending on the temperature */
/** The function returns the increment to teh counter, In the end, when counter reaches 1. the egg formation is complete*/

double Poecilus_Adult::getDailyEggFormationProgress(double temp){
    // todo: consider pre-compile the function or maybe just the division
    if (temp<=12){
        return m_OurPoecilusPopulation->poecilusconstantslist->PoecilusPreOviMin;
    }
    if (temp>27){
        return m_OurPoecilusPopulation->poecilusconstantslist->PoecilusPreOviMax;
    }
    double a = m_OurPoecilusPopulation->poecilusconstantslist->PoecilusPreOviFactorA;
    double b = m_OurPoecilusPopulation->poecilusconstantslist->PoecilusPreOviFactorB;
    double c = m_OurPoecilusPopulation->poecilusconstantslist->PoecilusPreOviFactorC;
    return 1./(b * temp + a * temp * temp + c);
}
void Poecilus_Adult::UpdateEggFormationProgress(double inc){
    m_EggFormationCounter+=inc;
}
double Poecilus_Adult::getEggFormationThreshold(){
    return getPoecilusCountersThreshold();
}
double Poecilus_Adult::getOvipositionThreshold(){
    return getPoecilusCountersThreshold();
}
TTypesOfBeetleState Poecilus_Adult::st_Forage(){
    CheckMaturity();
    CheckReproduction();

    if(CheckDormancy()){
        m_StepDone = true;
        if (getAge()<2){
            hasOverwintered = true;
            return tobs_Hibernating;
        }
        else{
            return tobs_ADying;
        }

    }

    if(getCanReproduce()){
        CheckOviposition();
        if (getCanReproduce()&&!getOvipositionFinshed()){

            int eggs_to_lay_today = getTodaysEggProduction()-getEggsLaid();
            int distance_to_move_today = getMaxDailyDistance() - getTodaysHoppingDistance();
            if (distance_to_move_today<1){
                for (int i = 0; i < eggs_to_lay_today; i++) {
                    Reproduce(m_Location_x, m_Location_y);
                    incEggsLaid();
                }
            }
            else{
                int eggs_to_lay = (int)(eggs_to_lay_today/distance_to_move_today);
                for (int i = 0; i < eggs_to_lay; i++) {
                    Reproduce(m_Location_x, m_Location_y);
                    incEggsLaid();
                }
            }

        }
    }
    if (getMaxDailyDistance() <= getTodaysHoppingDistance()){
        // returns true: can still move
        m_StepDone = false;
        ShortRangeMovement();
        incTodaysHoppingDistance();
    }
    else{
        int consumed_aphids = (int) m_OurPoecilusPopulation->SupplyAphidConsumption();
        m_OurPoecilusPopulation->decAphids(m_Location_x,m_Location_y, consumed_aphids);
        m_StepDone = true;
    }
    return tobs_Foraging;
}
double Poecilus_Adult::getExtremeTempThreshold() const{
    double threshtemp;
    if (CurrentBState==tobs_Hibernating){

        if(m_OurLandscape->SupplySnowDepth()<1){
            threshtemp = m_OurPoecilusPopulation->poecilusconstantslist->PoecilusHibernatedExtremeTempThreshold;
        }
        else{
            threshtemp = -99;
        }
    }
    else{
        threshtemp = m_OurPoecilusPopulation->poecilusconstantslist->PoecilusNonHibernatedExtremeTempThreshold;
    }
    return threshtemp;
}

double Poecilus_Adult::getMinTemp(){
    if (CurrentBState==tobs_Hibernating){
        return m_OurLandscape->SupplySoilTemp();
    }
    else{
        return m_OurLandscape->SupplyMinTemp(); // beetles are outside, so their min temp is min air temp, unless they are hibernating in the ground (then we use ground temp)
    }

}

double Poecilus_Adult::getMaxTemp(){
    if (CurrentBState==tobs_Hibernating){
        return m_OurLandscape->SupplySoilTemp();
    }
    else{
        return m_OurLandscape->SupplyMaxTemp(); // beetles are outside, so their min temp is min air temp, unless they are hibernating in the ground (then we use ground temp)
    }

}



void Poecilus_Adult::BeginStep(){

    resetTodaysHoppingDistance();
    resetEggsLaid();
    setMaxDailyDistance();
    if (DailyMortality()){
        CurrentBState = getDyingState();
    }



}
TTypesOfBeetleState Poecilus_Adult::st_Hibernate() {
    m_StepDone = true;
    int June1st = m_OurPoecilusPopulation->poecilusconstantslist->PoecilusLastDayToExitHibernation;
    int hiberstart = m_OurPoecilusPopulation->poecilusconstantslist->PoecilusLastDayToExitHibernation;
    if ((m_OurLandscape->SupplyDayInYear()>June1st)&&(m_OurLandscape->SupplyDayInYear()<hiberstart)){
        return tobs_ADying;
    }
    if(CheckDormancyExit()){
        return tobs_Foraging;
    }
    else{
        return tobs_Hibernating;
    }
}

bool Poecilus_Adult::CheckDormancy(){
    if ((CurrentBState==tobs_Foraging)&&(getTodaysHoppingDistance()==0)) {// we can only hibernate from the foraging state and it can only happen before the movement
        if (g_rand_uni()<m_OurPoecilusPopulation->SupplyDormancyChance()){
            return true;
        }

    }
    return false;
}
bool Poecilus_Adult::CheckDormancyExit(){
    if ((CurrentBState == tobs_Hibernating)){
        if (g_rand_uni()<m_OurPoecilusPopulation->SupplyDormancyExitChance()){
            return true;
        }
    }
    return false;
}
double Poecilus_Adult::getShortRangeDistance() {
    return m_OurPoecilusPopulation->poecilusconstantslist->PoecilusShortRangeDistance;
}
void Poecilus_Adult::setMaxDailyDistance() {
    //double temp = m_OurLandscape->SupplyTemp();

    m_MaxDailyDistance = (int) (m_OurPoecilusPopulation->SupplyMovementDistance());
}
/*
void Poecilus_Adult::CanReproduce(){ //todo: maybe delete


    CheckOviposition();
    if (getCanReproduce()&&!getOvipositionFinshed()){
        int eggs_to_lay_today = getTodaysEggProduction();
    }
}
 */
void Poecilus_Adult::CheckOviposition(){
    double delta = getDailyOvipositionProgress();
    double random_delta = getDailyOvipositionRandomProgress()*(2*g_rand_uni()-1);
    UpdateOvipositionProgress(delta+random_delta);
    if (getCurrentOvipositionProgress()>getOvipositionThreshold()){
        m_OvipositionFinished = true;
        m_CanReproduce = false;

    }
}
int Poecilus_Adult::getTodaysEggProduction(){
    int rand_factor = int (getDailyRandomEggFactor()*g_rand_uni());
    int eggsnum=getDailyEggProduction()+rand_factor;
    if (eggsnum>=0){
        return eggsnum;
    }
    else {
        return 0;
    }

}
double Poecilus_Adult::getDailyRandomEggFactor(){
    return m_OurPoecilusPopulation->m_DailyRandomEggFactor;
}
int Poecilus_Adult::getDailyEggProduction(){
    return m_OurPoecilusPopulation->m_DailyEggs;
}
double Poecilus_Adult::getDailyOvipositionProgress(){
    return m_OurPoecilusPopulation->m_DailyOvipositionFactor;
}

double Poecilus_Adult::getDailyOvipositionRandomProgress(){
    return m_OurPoecilusPopulation->m_DailyOvipositionRandomFactor;
}
void Poecilus_Adult::UpdateOvipositionProgress(double delta){
    m_OvipositionPeriodCounter+=delta;

}
double Poecilus_Adult::getExtremeTempMortality() const {
    return m_OurPoecilusPopulation->poecilusconstantslist->PoecilusExtremeTempMort;
}

void Poecilus_Adult::ClearFromMap(int X, int Y) {
    if (!m_OurPoecilusPopulation->decPoecilusDensity(X, Y, 5)){
        g_msg->Warn( WARN_BUG, " Poecilus_Adult::ClearFromMap "," Clearing non-existant agent from the map");
        exit(-1);
    }

}
bool Poecilus_Adult::AddToMap(int X, int Y) {
    return m_OurPoecilusPopulation->incPoecilusDensity(X, Y, 5);
}
void Poecilus_Adult::EndStep() {
    DoDensityDependentMortality();
    Beetle_Adult::EndStep();
}
 void Poecilus_Adult::DoDensityDependentMortality(){
     int x = m_Location_x;
     int y = m_Location_y;
     double mortality_chance{0};
     int popsize = m_OurPoecilusPopulation->getPoecilusDensity(x, y, 5);
     if (popsize>=m_OurPoecilusPopulation->poecilusconstantslist->PoecilusDensityDependentMortalityThreshold){
         if (g_rand_uni()<m_OurPoecilusPopulation->poecilusconstantslist->PoecilusDensityDependentMortality){
             // change the state into Dying
             CurrentBState=getDyingState();
         }
     }

 }
void Poecilus_Adult::Reproduce(int p_x, int p_y){
    std::unique_ptr <struct_Beetle> LS(new struct_Beetle);
    LS->x   = p_x;
    LS->y   = p_y;
    LS->L   = m_OurLandscape;
    LS->BPM = m_OurPopulation;

    // No need to register in density map: in CreateObjects
    // don't need delete because object destroyed by Population_Manager
    m_OurPoecilusPopulation->CreateObjects(0, nullptr,nullptr,std::move(LS),1);
}
//----------------------------------------------------------------------------------------------
//                                              Poecilus POPULATION MANAGER
//----------------------------------------------------------------------------------------------
Poecilus_Population_Manager::Poecilus_Population_Manager(Landscape* p_L):Beetle_Population_Manager(p_L , 4){
    poecilusconstantslist = std::make_unique<PoecilusConstantClass>();
    int height=m_TheLandscape->SupplySimAreaHeight();
    int width=m_TheLandscape->SupplySimAreaWidth();
    // There are 8 dimensions in the map (egg, 4 larva stages, pupa, adult + inactive adult)
    m_PoecilusDensityMap=make_unique<blitz::Array<unsigned char, 3>>(width,height,PoecilusStagesNumberInDensityMap);
    (*m_PoecilusDensityMap)=0;
}
// Updates DailyRandomEggFactor. Should be called daily
void Poecilus_Population_Manager::setDailyRandomEggFactor(double temp){
    double a = poecilusconstantslist->PoecilusDailyRandomEggFactorA;
    double b = poecilusconstantslist->PoecilusDailyRandomEggFactorB;
    double c = poecilusconstantslist->PoecilusDailyRandomEggFactorC;
    m_DailyRandomEggFactor = b * temp + a * temp * temp + c;
}
// Updates DailyEggProduction. Should be called daily
void Poecilus_Population_Manager::setDailyEggProduction(double temp){
    double a = poecilusconstantslist->PoecilusDailyEggFactorA;
    double b = poecilusconstantslist->PoecilusDailyEggFactorB;
    double c = poecilusconstantslist->PoecilusDailyEggFactorC;
    m_DailyEggs = (int) (b * temp + a * temp * temp + c);
}
void Poecilus_Population_Manager::DoFirst(){
    double temp = m_TheLandscape->SupplyTemp();
    double min_temp = m_TheLandscape->SupplyMinTemp();
    double max_temp = m_TheLandscape->SupplyMaxTemp();
    int year_day = m_TheLandscape->SupplyDayInYear();
    setDailyRandomEggFactor(temp);
    setDailyEggProduction(temp);
    setDailyOvipositionRandomFactor(temp);
    setDailyOvipositionFactor(temp);
    setDormancyChance(min_temp, year_day);
    setDormancyExitChance(max_temp, year_day);
    setMeanDistance(temp);
    setDistanceStd(temp);
    setMaxDistanceRNG();
    Beetle_Population_Manager::DoFirst();
    SetConsumeAphids(temp);
}
// Updates m_DormancyChance. Should be called daily temp is a MINIMUM temperature
void Poecilus_Population_Manager::setDormancyChance(double temp, int day){
    int Sept1st = poecilusconstantslist->PoecilusDormancyStartDate;
    int June1st = poecilusconstantslist->PoecilusDormancyStartDateEarly;
    if (day<June1st){ // no hibernation before the first of june
        m_DormancyChance = 0;
    }
    else{
        if ((day<Sept1st)&&(m_DormancyChance==0)){
            if ((temp<poecilusconstantslist->dormancy_threshold)){
                m_DormancyChance = 0;
            }
            else{
                m_DormancyChance = poecilusconstantslist->p_cfg_PoecilusStandardDormancyChance->value();
            }

        }
        else{
            if ((day==Sept1st)&&(m_DormancyChance==0)){
                m_DormancyChance = poecilusconstantslist->p_cfg_PoecilusStandardDormancyChance->value();
            }
            else{ // each day that has min temperature below 5 deg celsius increases dormancy chances
                if (temp<poecilusconstantslist->dormancy_threshold){
                    m_DormancyChance *= poecilusconstantslist->dormancy_multiplier;
                }
            }

        }
    }

}
// Updates m_DormancyExitChance. Should be called daily temp is a MAXIMUM temperature
void Poecilus_Population_Manager::setDormancyExitChance(double temp, int day){
    int March1st = poecilusconstantslist->PoecilusDormancyExitDate;
    int June1st = poecilusconstantslist->PoecilusDormancyStartDateEarly+1;
    if (day<March1st){
        m_DormancyExitChance = 0;
    }
    else{
        if (day>June1st){
            m_DormancyExitChance = 0;
        }
        else{
            if ((SupplyDormancyExitChance()==0)&&(temp> poecilusconstantslist->dormancy_exit_threshold)){
                m_DormancyExitChance = poecilusconstantslist->p_cfg_PoecilusStandardDormancyExitChance->value();
            }
            else{
                if(SupplyDormancyExitChance()!=0){
                    m_DormancyExitChance *= poecilusconstantslist->dormancy_exit_multiplier;
                }
                else{
                    m_DormancyExitChance = 0;
                }
            }
        }

    }
}
// Updates DailyOvipositionFactor. Should be called daily
void Poecilus_Population_Manager::setDailyOvipositionFactor(double temp){
    double value{0};
    if (temp<13.5)
        value = 56;
    else{
        if (temp<17)
            value = 29;
        else{
            if (temp<20.5)
                value = 34;
            else{
                if (temp<24.5)
                    value = 35;
                else{
                    value = 39.5;
                }
            }
        }
    }
    m_DailyOvipositionFactor = 1/value;
}

void Poecilus_Population_Manager::setDailyOvipositionRandomFactor(double temp){
    double value{0};
    if (temp<13.5)
        value = 40;
    else{
        if (temp<17)
            value = 16;
        else{
            if (temp<20.5)
                value = 30;
            else{
                if (temp<24.5)
                    value = 15;
                else{
                    value = 19;
                }
            }
        }
    }
    m_DailyOvipositionRandomFactor = 1/value;
}

void Poecilus_Population_Manager::Init()
{
    std::shared_ptr<Poecilus_Population_Manager> smart_ptr_to_this = shared_from_this();
    /*
    Ladybird_Adult::m_OurLadybirdPopulation = smart_ptr_to_this;

    Ladybird_Pupae::m_OurLadybirdPopulation = smart_ptr_to_this;

    Ladybird_Larvae::m_OurLadybirdPopulation = smart_ptr_to_this;

    Ladybird_Egg_List::m_OurLadybirdPopulation = smart_ptr_to_this;
     */
    Poecilus_Base::m_OurPoecilusPopulation = smart_ptr_to_this;

    // autom. called by constructor
    if (cfg_RipleysOutput_used.value()) {
        OpenTheRipleysOutputProbe("");
    }
    if ( cfg_ReallyBigOutput_used.value() ) {
        OpenTheReallyBigProbe();
    } else ReallyBigOutputPrb=nullptr;
    m_SimulationName = "Poecilus";
    // Create cfg_beetlestartnos adults
    for (int i=0; i<poecilusconstantslist->p_cfg_PoecilusAphidSpecies->value().size(); i++ ){
        if (m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(poecilusconstantslist->p_cfg_PoecilusAphidSpecies->value()[i])!= nullptr)
            Aphids_PM.emplace_back(static_pointer_cast<AphidsD_Population_Manager>(m_TheLandscape->SupplyThePopManagerList()->GetPopulation_smart(
                    poecilusconstantslist->p_cfg_PoecilusAphidSpecies->value()[i])));

    }
    m_AphidSpeciesNum=(int) poecilusconstantslist->p_cfg_PoecilusAphidSpecies->value().size();
    m_EList = std::make_unique<std::vector <std::unique_ptr<Beetle_Egg_List>>>(365);
    // Create the egg lists

    for (int i=0; i<365; i++)
    {
        // std::cout << "DEBUG MESSAGE: m_EList creation: " << i << std::endl;
        (*m_EList)[i]=std::make_unique<Poecilus_Egg_List>(i,this,m_TheLandscape);

    }
    for (int i=0; i<beetleconstantslist->p_cfg_beetlestartnos->value(); i++)
    {
        std::unique_ptr <struct_Beetle> aps(new struct_Beetle);
        aps->BPM = this;
        aps->L = m_TheLandscape;
        do
        {
            aps->x = random(m_TheLandscape->SupplySimAreaWidth());
            aps->y = random(m_TheLandscape->SupplySimAreaHeight());
        } while (!IsStartHabitat(aps->x, aps->y));
        CreateObjects(4,nullptr,nullptr,std::move(aps),1);
    }

    m_AdPopSize=beetleconstantslist->p_cfg_beetlestartnos->value();
    m_EPopSize=0;
    m_LPopSize=0;
    m_PPopSize=0;

// Load List of Animal Classes
    m_ListNames[0]="Egg";
    m_ListNames[1]="Larva";
    m_ListNames[2]="Pupa";
    m_ListNames[3]="Adult";
    m_ListNameLength = 4;
    m_population_type = TOP_Poecilus;

// Load State Names
    StateNames[tobs_Initiation] = "Initiation";
//Egg
    StateNames[tobs_EDeveloping] = "Developing";
    StateNames[tobs_Hatching] = "Hatching";
    StateNames[tobs_EDying] = "Dying";
//Larva
    StateNames[tobs_LDeveloping] = "Developing";
    StateNames[tobs_Pupating] = "Pupating";
    StateNames[tobs_LDying] = "Dying";
//Pupa
    StateNames[tobs_PDeveloping] = "Developing";
    StateNames[tobs_Emerging] = "Emerging";
    StateNames[tobs_PDying] = "Dying";
//Adult
    StateNames[tobs_Foraging] = "Foraging";
    StateNames[tobs_Aggregating] = "Aggregating";
    StateNames[tobs_Hibernating] = "Hibernating";
    StateNames[tobs_Dispersing] = "Dispersing";
    StateNames[tobs_ADying] = "Dying";

    // Ensure that larvae are sorted w.r.t. x position not shuffled
    BeforeStepActions[1]=1; // 1 = SortX

#ifdef __RECORD_RECOVERY_POLYGONS
    /* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::out);
	ofile << "This file records the number of females in each polygon each day" << endl;
	ofile.close();
	/* Open the polygon recovery file and read in polygons to m_RecoveryPolygons */
	ifstream ifile("RecoveryPolygonsList.txt",ios::in);
	int n;
	ifile >> n;
	m_RecoveryPolygons[0] = n;
	for (int i=0; i<n; i++) ifile >> m_RecoveryPolygons[1+i];
	for (int i=0; i<n; i++) m_RecoveryPolygonsC[1+i]=0;
	ifile.close();
#endif

#ifdef __BEETLEPESTICIDE1
    PestMortLocOutputOpen();
	m_InFieldNo = 0;
	m_OffFieldNo = 0;
	m_InCropNo = 0;
	m_InCropRef = m_TheLandscape->TranslateVegTypes(beetleconstantslist->p_cfg_InCropRef->value());
	if (beetleconstantslist->p_cfg_SaveInfieldLocation->value()) LocOutputOpen();
#endif


    // Initialise any static variables
    Beetle_Adult::SetAdultPPPElimRate(beetleconstantslist->p_cfg_BemAdultPPPElimiationRate->value());  // Initialize static variable
    Beetle_Adult::SetAPPPThreshold(beetleconstantslist->p_cfg_BemAdultPPPThreshold->value());  // Initialize static variable
    Beetle_Adult::SetAPPPEffectProb(beetleconstantslist->p_cfg_BemAdultPPPEffectProb->value());  // Initialize static variable
    Beetle_Adult::SetAPPPEffectProbDecay(beetleconstantslist->p_cfg_BemAdultPPPEffectProbDecay->value());  // Initialize static variable

    // Initialise any static variables
    Beetle_Pupae::SetPupalPPPElimRate(beetleconstantslist->p_cfg_BemPupalPPPElimiationRate->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPThreshold(beetleconstantslist->p_cfg_BemPupalPPPThreshold->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPEffectProb(beetleconstantslist->p_cfg_BemPupalPPPEffectProb->value());  // Initialize static variable
    Beetle_Pupae::SetPPPPEffectProbDecay(beetleconstantslist->p_cfg_BemPupalPPPEffectProbDecay->value());  // Initialize static variable

    // Initialise any static variables
    Beetle_Larvae::SetLarvalPPPElimRate(beetleconstantslist->p_cfg_BemLarvalPPPElimiationRate->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPThreshold(beetleconstantslist->p_cfg_BemLarvalPPPThreshold->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPEffectProb(beetleconstantslist->p_cfg_BemLarvalPPPEffectProb->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPEffectProbDecay(beetleconstantslist->p_cfg_BemLarvalPPPEffectProbDecay->value());  // Initialize static variable

    beetleconstantslist->DevelopmentInflectionPoint=100; // there is no inflection point in Poecilus
    beetleconstantslist->DevelConst1= 9;
    beetleconstantslist->LDevelConst2->assign({95.46,190.93,381.86});
    beetleconstantslist->PupaDevelConst2    = 147.7;
    beetleconstantslist->EggDevelConst2 = 88.7;
    LDDepMort1 = 0;
    poecilusconstantslist->PoecilusExtremeTempMort=poecilusconstantslist->p_cfg_PoecilusExtremeTempMortality->value();
    poecilusconstantslist->PoecilusHibernatedExtremeTempThreshold=poecilusconstantslist->p_cfg_PoecilusExtremeTempHibThreshold->value();
    poecilusconstantslist->DailyEggMort=poecilusconstantslist->p_cfg_PoecilusDailyEggMort->value();
    poecilusconstantslist->DailyLarvaeMort= poecilusconstantslist->p_cfg_PoecilusDailyLarvaeMort->value();
    poecilusconstantslist->DailyPupaeMort= poecilusconstantslist->p_cfg_PoecilusDailyPupaeMort->value();
    poecilusconstantslist->DailyAdultMortality1 = poecilusconstantslist->p_cfg_PoecilusDailyAdultMortality1->value();
    poecilusconstantslist->DailyAdultMortality2 = poecilusconstantslist->p_cfg_PoecilusDailyAdultMortality2->value();
    poecilusconstantslist->DailyAdultMortality3 = poecilusconstantslist->p_cfg_PoecilusDailyAdultMortality3->value();
    poecilusconstantslist->PoecilusDailyEggFactorA = poecilusconstantslist->p_cfg_PoecilusDailyEggFactorA->value();
    poecilusconstantslist->PoecilusDailyEggFactorB = poecilusconstantslist->p_cfg_PoecilusDailyEggFactorB->value();
    poecilusconstantslist->PoecilusDailyEggFactorC = poecilusconstantslist->p_cfg_PoecilusDailyEggFactorC->value();
}
/**
All Poecilus objects that are created must be created using this method. Data on the location and other attributes
are passed in data, and the number to create in number.\n
*/
void Poecilus_Population_Manager::CreateObjects(int ob_type,
                                                 TAnimal * /* pvo */ ,void* /* null */ ,std::unique_ptr<struct_Beetle> data,int number)
{


    for (int i=0; i<number; i++)
    {
        if (ob_type == 0)
        {
            if((*m_PoecilusDensityMap)(data->x,data->y,0)<blitz::huge((*m_PoecilusDensityMap)(data->x,data->y,0))) {
                (*m_EList)[data->L->SupplyDayInYear()]->AddEgg(data->x, data->y);
                incPoecilusDensity(data->x, data->y, 0); //todo: move to the constructor
            }
        }
        if (ob_type == 1) {
            // Will not create a new larva in a square already full
            if ((*m_PoecilusDensityMap)(data->x,data->y,1)<blitz::huge((*m_PoecilusDensityMap)(data->x,data->y,1))) {
                incPoecilusDensity( data->x, data->y, 1 );
                if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type )) {
                    // We need to reuse an object
                    dynamic_pointer_cast<Poecilus_Larvae>(SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
                    IncLiveArraySize(ob_type);
                }
                else {
                    std::unique_ptr <Poecilus_Larvae> new_Larva(new Poecilus_Larvae(data->x, data->y, data->L, this ));
                    //new_Larva = new Poecilus_Larvae( data->x, data->y, data->L, this );
                    //(*TheArray_new)[ ob_type ].insert( (*TheArray_new)[ ob_type ].begin(), new_Larva );
                    PushIndividual( ob_type, std::move(new_Larva));
                    IncLiveArraySize(ob_type);
                }
            }
        }
        if (ob_type == 2)
        {
            if((*m_PoecilusDensityMap)(data->x,data->y,4)<blitz::huge((*m_PoecilusDensityMap)(data->x,data->y,4))) {
                incPoecilusDensity(data->x, data->y, 4); //todo: move to the constructor
                if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
                    // We need to reuse an object
                    dynamic_pointer_cast<Poecilus_Pupae>(
                            SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L,
                                                                                              this);
                    IncLiveArraySize(ob_type);
                } else {
                    std::unique_ptr<Poecilus_Pupae> new_Pupa(new Poecilus_Pupae(data->x, data->y, data->L, this));
                    //new_Pupa = new Poecilus_Pupae( data->x, data->y, data->L, this );
                    PushIndividual(ob_type, std::move(new_Pupa));
                    IncLiveArraySize(ob_type);
                }
            }
        }
        if ((ob_type == 3)||(ob_type == 4)) // ob_type == 4 this is the same as ob_type==3, but the beetles created are hibernating.
            // this is the way the beetle objects should be created in the start of the simulation
            // to avoid the extremely high mortality right in the beginning
        {
            bool Is_hibernating = false;
            if (ob_type == 4){
                Is_hibernating = true;
                ob_type = 3;
            }
            if((*m_PoecilusDensityMap)(data->x,data->y,5)<blitz::huge((*m_PoecilusDensityMap)(data->x,data->y,5)))
            {
                incPoecilusDensity(data->x, data->y, 5); //todo: move to the constructor

                if (unsigned(SupplyListSize(ob_type)) > GetLiveArraySize(ob_type)) {
                    // We need to reuse an object
                    dynamic_pointer_cast<Poecilus_Adult>(
                            SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L,
                                                                                              this);
                    if (Is_hibernating==true){
                        dynamic_pointer_cast<Poecilus_Adult>(
                                SupplyAnimalSmartPtr(ob_type, GetLiveArraySize(ob_type)))->CurrentBState = tobs_Hibernating;
                    }
                    IncLiveArraySize(ob_type);
                } else {
                    std::unique_ptr<Poecilus_Adult> new_Adult(new Poecilus_Adult(data->x, data->y, data->L, this));
                    if (Is_hibernating == true){
                        new_Adult->CurrentBState=tobs_Hibernating;
                    }
                    PushIndividual(ob_type, std::move(new_Adult));
                    IncLiveArraySize(ob_type);
                }
            }
        }

    }
}
double Poecilus_Population_Manager::SupplyTempThresh() const{
    return poecilusconstantslist->p_cfg_PoecilusTempShadowThresh->value();
}
void Poecilus_Population_Manager::SetConsumeAphids(double temp){
    // here we cut teh angle a tiny bit. All the beetles consume the same amount of aphids at the same day
    // It is not very realistic, but spares a lot of computing power. When there is a big need to change it could be quite easily
    // altered. However, we need to see what is the best way to do it efficiency-wise
    int level=0;
    if (temp<=poecilusconstantslist->Aphid_consumption_threshs.at(0)){
        level = 0;
    }
    else{
        if (temp>=poecilusconstantslist->Aphid_consumption_threshs.at(3)){
            level = 3;
        }
        else{
            if (temp>poecilusconstantslist->Aphid_consumption_threshs.at(0)&&
                temp<=poecilusconstantslist->Aphid_consumption_threshs.at(1)){
                level = 0;
            }
            else{
                if (temp>poecilusconstantslist->Aphid_consumption_threshs.at(1)&&
                    temp<=poecilusconstantslist->Aphid_consumption_threshs.at(2)){
                    level = 1;
                }
                else{
                    if (temp>poecilusconstantslist->Aphid_consumption_threshs.at(2)&&
                        temp<poecilusconstantslist->Aphid_consumption_threshs.at(3)){
                        level = 2;
                    }
                }

            }

        }

    }
    double adult_mean = poecilusconstantslist->Aphid_consumption_means.at(level);
    double adult_std =  poecilusconstantslist->Aphid_consumption_stds.at(level);
    AphidConsumptionToday=std::make_unique<probability_distribution>("NORMAL", std::to_string(adult_mean)+" "+std::to_string(adult_std));

}

void Poecilus_Population_Manager::setMeanDistance(double temp) {
    //todo: to be run in the beginning of the day
    if (temp>(poecilusconstantslist->PoecilusMinMovementTemp)){
        m_DailyMeanDistance= 1.8*temp -15;
    }
    else {
        m_DailyMeanDistance=0;
    }
}
void Poecilus_Population_Manager::setDistanceStd(double temp){
    //todo: to be run in the beginning of the day
    m_DailyDistanceStd =poecilusconstantslist->p_cfg_PoecilusDistanceStd->value();
}
double Poecilus_Population_Manager::SupplyMeanDistance() const{
    return m_DailyMeanDistance;
}
double Poecilus_Population_Manager::SupplyDistanceStd() const{
    return m_DailyDistanceStd;
}
void Poecilus_Population_Manager::setMaxDistanceRNG(){
    double mean_dist = SupplyMeanDistance();
    double dist_std = SupplyDistanceStd();
    PoecilusMovementToday=std::make_unique<probability_distribution>("NORMAL", std::to_string(mean_dist)+" "+std::to_string(dist_std));

}
int Poecilus_Population_Manager::getPoecilusDensity(int x, int y, int type) {
    return (*m_PoecilusDensityMap)(x,y,type);
}

void Poecilus_Population_Manager::decAphids(int x, int y, int num) {
    std::vector<int> aphid_species{};
    aphid_species.reserve(m_AphidSpeciesNum);
for (int i=0; i<m_AphidSpeciesNum; i++){
        aphid_species.push_back(i);
        aphid_species.push_back(i);
    }
// if there is more than one species of the aphid at teh spot we will choose randomly the order of eating
    // the following is until we implemented shuffle correctly
    std::shuffle(aphid_species.begin(), aphid_species.end(), g_std_rand_engine);
    for (int i=0; (i<m_AphidSpeciesNum)&&(num>0); i++){
        if (SupplyAphidsNumber(x,y, aphid_species[i])>=num){
            DeductAphidsNumber(x,y, aphid_species[i], num);
        }

        else{
            num-=SupplyAphidsNumber(x,y, aphid_species[i]);
            ResetAphidsNumber(x,y, aphid_species[i]);

        }
    }


}
int Poecilus_Population_Manager::SupplyAphidsNumber(int x, int y, int aphid_species){
    return Aphids_PM.at(aphid_species)->SupplyAphidsNumberLandscapeCO(x, y);
}

void Poecilus_Population_Manager::DeductAphidsNumber(int x, int y, int aphid_species, int number_to_deduct){
    Aphids_PM.at(aphid_species)->deductAphidsNumberLandscapeCO(x, y, number_to_deduct);
}
void Poecilus_Population_Manager::ResetAphidsNumber(int x, int y, int aphid_species){
    Aphids_PM.at(aphid_species)->zeroAphidsNumberLandscapeCO(x, y);
}


//-----------------------------------------------------------------------------
PoecilusConstantClass::PoecilusConstantClass(): PoecilusStartMaturationDay{79},
                                                p_cfg_PoecilusDailyMaturationChance{new CfgFloat{"POECILUS_DAILY_MATURATION_CHANCE",CFG_CUSTOM, 0.03}},
                                                p_cfg_PoecilusTempShadowThresh{new CfgFloat{"POECILUS_TEMP_SHADOW_THRESH",CFG_CUSTOM, 18.0}},
                                                p_cfg_PoecilusDistanceStd{new CfgFloat{"POECILUS_DISTANCE_STD",CFG_CUSTOM, 5.0}},
                                                p_cfg_PoecilusStandardDormancyChance{new CfgFloat{"POECILUS_STANDARD_DORMANCY_CHANCE",CFG_CUSTOM, 0.03}},
                                                p_cfg_PoecilusStandardDormancyExitChance{new CfgFloat{"POECILUS_STANDARD_DORMANCY_EXITCHANCE",CFG_CUSTOM, 0.03}},
                                                p_cfg_PoecilusExtremeTempMortality{new CfgFloat{"POECILUS_EXTREME_TEMP_MORTALITY",CFG_CUSTOM, 0.2}},
                                                p_cfg_PoecilusExtremeTempHibThreshold{new CfgFloat{"POECILUS_EXTREME_TEMP_HIB_THRESHOLD",CFG_CUSTOM, -10.0}},
                                                p_cfg_PoecilusDailyEggMort{new CfgFloat{"POECILUS_DAILY_EGG_MORTALITY",CFG_CUSTOM, 0.021}},
                                                p_cfg_PoecilusDailyLarvaeMort{new CfgFloat{"POECILUS_DAILY_LARVAE_MORTALITY",CFG_CUSTOM, 0.005}},
                                                p_cfg_PoecilusDailyPupaeMort{new CfgFloat{"POECILUS_DAILY_PUPAE_MORTALITY",CFG_CUSTOM, 0.012}},
                                                p_cfg_PoecilusDailyAdultMortality1{new CfgFloat{"POECILUS_DAILY_ADULT_MORTALITY1",CFG_CUSTOM, 0.003}},
                                                p_cfg_PoecilusDailyAdultMortality2{new CfgFloat{"POECILUS_DAILY_ADULT_MORTALITY2",CFG_CUSTOM, 0.0011}},
                                                p_cfg_PoecilusDailyAdultMortality3{new CfgFloat{"POECILUS_DAILY_ADULT_MORTALITY3",CFG_CUSTOM, 0.00023}},
                                                p_cfg_PoecilusDailyEggFactorA{new CfgFloat{"POECILUS_DAILY_EGGFACTOR_A",CFG_CUSTOM, -0.02753}},
                                                p_cfg_PoecilusDailyEggFactorB{new CfgFloat{"POECILUS_DAILY_EGGFACTOR_B",CFG_CUSTOM, 1.72330}},
                                                p_cfg_PoecilusDailyEggFactorC{new CfgFloat{"POECILUS_DAILY_EGGFACTOR_C",CFG_CUSTOM, -16.70355}},
                                                p_cfg_PoecilusAphidSpecies{new CfgArray_Int{"LADYBIRD_APHID_SPECIES", CFG_CUSTOM, 1, vector<int> { }},}
{
    TPoecilusFarmings PoecilusFarming;
    TreatmentMortalities = PoecilusFarming.PoecilusLarvaPupaFertilizersTreatmentMortality.getList();
}
/*
 *     double PoecilusDailyEggFactorA{-0.02753};
 * \brief The polynomial coefficient for the  mean of daily egg number (a factor of temperature)
* double PoecilusDailyEggFactorB{1.72330};
* \brief The polynomial coefficient for the mean of daily egg number (a factor of temperature)
double PoecilusDailyEggFactorC{-16.70355};
 */