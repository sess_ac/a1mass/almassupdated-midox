/*
*******************************************************************************************************
Copyright (c) 2011, Andrey Chuhutin, Aarhus, 2020
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------

#ifndef Poecilus_allH
#define Poecilus_allH

//---------------------------------------------------------------------------

#include "Beetle_BaseClasses.h"
#include "Poecilus_farm.h"
#include <blitz/array.h>
#include <blitz/numinquire.h>
#include "Aphids_debug/AphidsD_all.h"
extern const char* SimulationName;

// #define __BEETLEPESTICIDE1

//------------------------------------------------------------------------------
// Forward Declarations

class Poecilus_Population_Manager;
class Landscape;
class MovementMap;
class SimplePositionMap;

class probability_distribution;



/** \brief
The class describing the constants specific to Poecilus
*/
class PoecilusConstantClass{
public:
    /** \brief Maturation day of the year (mean) */
    const int PoecilusStartMaturationDay{79};
    /** \brief generic counter threshold: normally we count until this value (could be 1 or 100) */
    const double PoecilusCountersThreshold{1.};
    /**\brief Daily chance to become sexually mature for reproduction*/
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyMaturationChance;
    /** \brief threshold temperature when the beetle switches between seeking for shadow and seeking for light */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusTempShadowThresh;
    /** \brief Standard deviation in daily covered distance */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDistanceStd;
    /** \brief Standard chance to start overwintering after the threshold date */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusStandardDormancyChance;
    /** \brief Standard chance to exit hibernation after the threshold date */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusStandardDormancyExitChance;
    /** \brief Mortality due to exposure to extremely low or extremely high temperature */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusExtremeTempMortality;
    /** \brief The threshold (decreased) for extremely low temeprature when hibernating. Here we assume that hibernation allows the beetles to lower the temperature it can bear */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusExtremeTempHibThreshold;
    /**\brief the list of aphid species that the  Poecilus can consume */
    std::unique_ptr<const CfgArray_Int> p_cfg_PoecilusAphidSpecies;
    /** \brief Background egg mortality configuration value*/

    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyEggMort;
    /** \brief Background larva mortality configuration value */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyLarvaeMort;
    /** \brief Background pupa mortality  configuration value */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyPupaeMort;
    /** \brief Value A of a linear fit for background adult mortality (function of temperature) configuration value */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyAdultMortality1;
    /** \brief Value B of a linear fit for background adult mortality (function of temperature)  configuration value*/
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyAdultMortality2;
    /** \brief Value C of a linear fit for background adult mortality (function of temperature)  configuration value*/
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyAdultMortality3;

    /** \brief The polynomial coefficient for the mean of daily egg number (a factor of temperature) */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyEggFactorA;
    /** \brief The polynomial coefficient for the  mean of daily egg number (a factor of temperature) */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyEggFactorB;
    /** \brief The polynomial coefficient for the mean of daily egg number (a factor of temperature) */
    std::unique_ptr<const CfgFloat> p_cfg_PoecilusDailyEggFactorC;


    /** \brief Threshold temeperature. eac h day below that temperature increases hibernation chances.*/
    double dormancy_threshold{5.};
    /** \brief The multiplier to apply if below the threshold dormancy_threshold */
    double dormancy_multiplier{1.2};
    /** \brief Threshold for dormancy exit temperature. each day above that temperature increases hibernation exit chances.*/
    double dormancy_exit_threshold{8.};
    /** \brief The multiplier to apply if above the threshold dormancy_exit_threshold */
    double dormancy_exit_multiplier{1.2};
    /** \brief Background egg mortality */
    double DailyEggMort {0.021};
    /** \brief Background larva mortality */
    double DailyLarvaeMort {0.005};
    /** \brief Background pupa mortality */
    double DailyPupaeMort {0.012};
    /** \brief Value A of a linear fit for background adult mortality (function of temperature) */
    double DailyAdultMortality1 {0.003};
    /** \brief Value B of a linear fit for background adult mortality (function of temperature) */
    double DailyAdultMortality2 {0.0011};
    /** \brief Value C of a linear fit for background adult mortality (function of temperature) */
    double DailyAdultMortality3 {0.00023};
    /** \brief The chances to die due to exposure to extreme temperature*/
    double PoecilusExtremeTempMort{0.2};
    /** \brief The polynomial coefficient for the length of the pre-oviposition period (a factor of temperature) */
    double PoecilusPreOviFactorA{0.229};
    /** \brief The polynomial coefficient for the length of the pre-oviposition period (a factor of temperature) */
    double PoecilusPreOviFactorB{-11.14};
    /** \brief The polynomial coefficient for the length of the pre-oviposition period (a factor of temperature) */
    double PoecilusPreOviFactorC{140.28};
    /** \brief The polynomial coefficient for the mean of daily egg number (a factor of temperature) */
    double PoecilusDailyEggFactorA{-0.02753};
    /** \brief The polynomial coefficient for the  mean of daily egg number (a factor of temperature) */
    double PoecilusDailyEggFactorB{1.72330};
    /** \brief The polynomial coefficient for the mean of daily egg number (a factor of temperature) */
    double PoecilusDailyEggFactorC{-16.70355};
    /** \brief The polynomial coefficient for the stochastic factor of daily egg number (function of temperature) */
    double PoecilusDailyRandomEggFactorA{-0.006};
    /** \brief The polynomial coefficient for the stochastic factor of daily egg number (function of temperature) */
    double PoecilusDailyRandomEggFactorB{0.27};
    /** \brief The polynomial coefficient for the stochastic factor of daily egg number (function of temperature) */
    double PoecilusDailyRandomEggFactorC{-2.32};
    /** \brief Minimum increase in daily progress in preoviposition */
    double PoecilusPreOviMin{1./41.2};
    /** \brief Maximum increase in daily progress in preoviposition */
    double PoecilusPreOviMax{1./5};
    /** \brief Step size of shornt range movement */
    double PoecilusShortRangeDistance{1.};
    /** \brief Minimum mean temperature that allows daily movement */
    double PoecilusMinMovementTemp{8.3};
    /** \brief a threshold after which non-hibernated individuals experience extreme freezing */
    double PoecilusNonHibernatedExtremeTempThreshold{0};
    /** \brief a threshold after which hibernated individuals experience extreme freezing */
    double PoecilusHibernatedExtremeTempThreshold{-10.};
    /** \brief a day the dormancy can start if it is cold: June 2nd */
    int PoecilusDormancyStartDateEarly{153};
    /** \brief a day the dormancy chance is normally higher than 0: Sept 1st */
    int PoecilusDormancyStartDate{244};
    /** \brief a day teh dormancy exit becomes possible: March 1st  */
    int PoecilusDormancyExitDate{60};
    /** \brief a date after which the beetle dies if not woken from hibernation: June 1st */
    int PoecilusLastDayToExitHibernation{152};
    /** \brief Minimum number of beetles per square that can trigger cannibalism behaviour */
    int CannibalismThreshold{4};
    /** \brief Currently not used */
    int CannibalismThresholdsNum{6};
    /** \brief threshold after which dd mortality happens */
    int PoecilusDensityDependentMortalityThreshold{4};
    /** \brief a chance to die due to DD mortality */
    double PoecilusDensityDependentMortality{0.5};
    /** \brief Extreme temp mortality is on for poecilus */
    bool PoecilusExtremeTempIsOn{true};
    /** \brief The number of beetles that define the thresholds between the different levels of cell occupancy that is used by the cannibalism implementation */
    std::array<int,6> CannibalismLevelThresholds{43,85,128,171,214,427};
    /** \brief The chances of cannibalism for different population sizes in the first instar */
    std::array<double,6> Cannibalism_L1{0.035,0.035,0.0475,0.08,0.079,0.075};
    /** \brief The chances of cannibalism for different population sizes in the second instar */
    std::array<double,6> Cannibalism_L2{0.035,0.035,0.0475,0.0714,0.0714,0.111};
    /** \brief The chances of cannibalism for different population sizes in the third instar */
    std::array<double,6> Cannibalism_L3{0.035,0.035,0.0714,0.0714,0.079,0.089};
    /** \brief The mean of the aphid consumption at different temperatures (step function values) */
    std::array<double,4> Aphid_consumption_means{23.6,29.2,32.6,37.0};
    /** \brief The standard deviation of the aphid consumption at different temperatures (step function values) */
    std::array<double,4> Aphid_consumption_stds{2.55,5.,3.42,2.76};
    /** \brief The temperatures at which different consumption of aphids is registered */
    std::array<double,4> Aphid_consumption_threshs{5.,10.,15.,20.};
    /** \brief The list that summarises farming events that involve fertilizer applications and the mortalities they cause */
    TTreatmentvsMortalityList TreatmentMortalities;
    /** \brief Constants class constructor */
    PoecilusConstantClass();
protected:
    ;
private:

};
//------------------------------------------------------------------------------
/**

Poecilus base class, here we define the Poecilus related methods and parameters that are used by more than
 one life stage
*/

class Poecilus_Base : virtual public Beetle_Base {
public:
    //methods:
    /** \brief Base class constructor */
    Poecilus_Base(int, int, Landscape *, Poecilus_Population_Manager*);
    //variables:
    /** \brief Pointer to the population manager already casted as Poecilus PM:
     * useful for speed, no need to cast each time     *
     * */
    static std::shared_ptr<Poecilus_Population_Manager> m_OurPoecilusPopulation;
    /** \brief The function returning the standard counter threshold */
    static double getPoecilusCountersThreshold();

protected:
    //methods:
    /** \brief The function that defines the movement factor that is evaluated to estimate the chances of the movement in various directions. Override from Beetle_Base */
    double MovementFactor(int, int, int, int) override;
    /** \brief Method dealing with the farming mortalities */
    bool OnFarmEvent(FarmToDo event) override;
    /** \brief The method that deals with the ferhtalizer mortality (should be overridden in descendant classes) */
    virtual bool PoecilusFertilizerMortality(FarmToDo event);
    /** \brief method that returns the threshold for extreme temp mortality (due to freezing) */
    [[nodiscard]] virtual double getExtremeTempThreshold() const;
    /** \brief the method returning true if extreme temp mortality is allowed */
    [[nodiscard]] bool ExtremeTempIsOn() const override;
    /** \brief return the minimum threshold for beetle mortality */
    [[nodiscard]] double getExtremeTempMin() const override;
    /** \brief The standard function to get the minimum temperature for most of the beetle stages: if the beetles are not adult they stay in the ground and we therefore use the ground temeperature */
    double getMinTemp() override;
    /** \brief The standard function to get the maximum temperature for most of the beetle stages: if the beetles are not adult they stay in the ground and we therefore use the ground temeperature */
    double getMaxTemp() override;
    //variables:
    /** \brief The date of birth at the current stage (e.g. for the larva is the date of emergence) */
    long m_DateMade{0};
private:
    //methods:
    //variables:
};

//------------------------------------------------------------------------------
/**
*\brief
*The class describing the beetle Egg_List objects
 *
 * In general case this class should have only a constructor
*/
class Poecilus_Egg_List : public  Beetle_Egg_List, public Poecilus_Base
{

/** The egg list is an optimisation to reduce memory and time to run
   it means that there are only 365 of them possible and that each on contains
   all the eggs laid on that day. The only real problem is that there is no longer a link from parent to
   offspring - so this version cannot be used with genetics without adding the genes to the APoint struct.\n


*/
protected:
    /** \brief The standard function that removes the agent from the tracking map */
    void ClearFromMap(int X, int Y) override;
    /** \brief The standard function that add the agent to the tracking map */
    static bool AddToMap(int X, int Y);
    /** \brief The method to rbun when the eggs in the list hatch */
    TTypesOfBeetleState st_Hatch() override;
public:
    /** \brief Egg_List class constructor */
    Poecilus_Egg_List(int today,Poecilus_Population_Manager* BPM, Landscape* L);
    /** \brief Get Daily egg mortality */
    [[nodiscard]] double getDailyEggMortality() const override;
    /**\brief The method reuses memory for a new object of the type Poecilus_Egg_list   * */
    void ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) override;

};



class Poecilus_Larvae : public Beetle_Larvae, public Poecilus_Base{
protected:
    /** \brief The standard function that removes the agent from the tracking map */
    void ClearFromMap(int X, int Y) override;
    /** \brief The standard function that add the agent to the tracking map */
    bool AddToMap(int X, int Y) override;
public:
    /** \brief Larva object contructor */
    Poecilus_Larvae( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM );
    /** \brief Larva object destructor */
    ~Poecilus_Larvae()override=default;
    /**\brief The method reuses memory for a new object of the type Poecilus_Larvae   * */
    void ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) override;
protected:
    /** \brief Method where the fertilizer mortality check is performed */
    bool PoecilusFertilizerMortality(FarmToDo event) override;
    /** \brief Gets the daily mortality cahnce for larvae */
    [[nodiscard]] double getDailyLarvaeMortality() const override;
    /** \brief A standard method tro be called to get the mortality chance */
    [[nodiscard]] double getDailyMortalityRate() const override;
    /** \brief Returns the threshold for cannibalism to occur */
    static int getCannibalismThreshold();
    /** \brief The method o be run in the end of the step */
    void EndStep() override;
    /** \brief Method where the cannibalism behaviour occur */
    void Do_Cannibalism();
    /** \brief The final stage of larval developmentL turning to pupa */
    TTypesOfBeetleState st_Pupate() override;
    //double TempRelatedMortality(double temp2, double maxtemp, double mintemp) override {return 0.0;};
    /** \brief T temp related mortality is not used in Poecilus, extreme temp mortality is estimated in begin step */

    double getLarvaTDailyMortality (int st, int t) override {return 0.0;};

};
class Poecilus_Pupae : public Beetle_Pupae, public Poecilus_Base{
public:
    /** \brief Pupae class constructor */
    Poecilus_Pupae( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM );
    /** \brief Pupae class destructor */
    ~Poecilus_Pupae()override =default;
    /**\brief The method reuses memory for a new object of the type Poecilus_Pupae   * */
    void ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) override;
protected:
    /** \brief The standard function that removes the agent from the tracking map */
    void ClearFromMap(int X, int Y) override;
    /** \brief The standard function that add the agent to the tracking map */
    static bool AddToMap(int X, int Y);
    /** \brief Mortality due to fertilizer application */
    bool PoecilusFertilizerMortality(FarmToDo event) override;
    /** \brief Background mortality for pupae */
    [[nodiscard]] double getDailyPupaeMortality() const;
    /** \brief A standard method tro be called to get the mortality chance */
    [[nodiscard]] double getDailyMortalityRate() const override;
    /** \brief The final step of pupal development: emergence into an adult */
    TTypesOfBeetleState st_Emerge() override;
    /** \brief There is no temperature related mortality calculation (stage length to mortality) in Poecilus pupae */
    [[nodiscard]] double calcDailyMortChance(int temp2, double LengthOfStageAtTemp) const override {return 0.0;};

};
class Poecilus_Adult : public Beetle_Adult, public Poecilus_Base{
protected:
     /** \brief Here we track the distance this adult has travelled today */
    int m_TodaysHoppingDistance;
    /** \brief Maximum distance to travel today */
    int m_MaxDailyDistance;
    /** \brief A counter for eggs laid today */
    int m_eggsLaid;
    /** \brief A state variable that flags that the individual is mature for reproduction */
    bool m_IsMature{false};
    /** \brief The counter that reflects the progress towards the end of egg formation */
    double m_EggFormationCounter{0};
    /** \brief The counter that reflects the progress towards the end of oviposition */
    double m_OvipositionPeriodCounter{0};
    /** \brief A state variable that flags that the individual has finished its oviposition  */
    bool m_OvipositionFinished{false};
    /** \brief A state variable that flags that the adult has overwintered */
    bool hasOverwintered{false};
/** \brief The function that returns the progress in egg formation time depending on the temperature */
/** The function returns the increment to teh counter, In the end, when counter reaches 1. the egg formation is complete*/
    static double getDailyEggFormationProgress(double temp);
    /** \brief The method returns the distance the beetle moved today */
    int getTodaysHoppingDistance() const{return m_TodaysHoppingDistance;};
    /** \brief The method increments the distance the beetle moved today */
    void incTodaysHoppingDistance(){m_TodaysHoppingDistance++;};
    /** \brief The method resets the distance the beetle moved today */
    void resetTodaysHoppingDistance(){m_TodaysHoppingDistance=0;};
    /** \brief The method increments the egg formation */
    void UpdateEggFormationProgress(double increment);
    /** \brief The method increments the oviposition progress  */
    void UpdateOvipositionProgress(double delta);
    /** \brief The method returns the egg formation threshold */
    static double getEggFormationThreshold();
    /** \brief The method returns the oviposition threshold */
    static double getOvipositionThreshold();
    /** \brief The method returns the progress in egg formation */
    double getCurrentEggFormationProgress() const{return m_EggFormationCounter;};
    /** \brief The method resets the progress in egg formation */
    void ResetCurrentEggFormationProgress(){m_EggFormationCounter=0;};
    /** \brief The method returns the progress in oviposition */
    double getCurrentOvipositionProgress() const{return m_OvipositionPeriodCounter;};
    /** \brief the method returns max daily distance */
    int getMaxDailyDistance() const{ return m_MaxDailyDistance;};
    /** \brief The method returns the number of eggs laid */
    int getEggsLaid() const{ return m_eggsLaid;};
    /** \brief The method increments the number of eggs laid */
    void incEggsLaid(){m_eggsLaid++;};
    /** \brief The method resets the number of eggs laid */
    void resetEggsLaid(){m_eggsLaid=0;};
    /** \brief The method uses defined on the population level relevant daily constants to generate max daily distance for this individual*/
    void setMaxDailyDistance();
    /** \brief The method returns maximum threshold on daily egg production */
    static int getTodaysEggProduction();
    /** \brief The method returns the random component of maximum threshold on daily egg production */
    static double getDailyRandomEggFactor();
    /** \brief The method returns the non-random component of maximum threshold on daily egg production */
    static int getDailyEggProduction();
    /** \brief The method checks for the progress in the reproductive development */
    void CheckReproduction();
    /** \brief The method checks for the progress in oviposition */
    void CheckOviposition();
    /** \brief the method returns true if the beetle finished oviposing */
    bool getOvipositionFinshed() const{return m_OvipositionFinished;};
    /** \brief the method returns the length of a single short-range hop */
    double getShortRangeDistance() override;
    /** \brief The method that is responsible for the daily foraging behaviour of an adult beetle */
    TTypesOfBeetleState st_Forage() override;
    /** \brief The method that is responsible for the hibernating behaviour of an adult beetle */
    TTypesOfBeetleState st_Hibernate() override;
    /** \brief The method to be deleted: not used in poecilus */
    //void CanReproduce() override;
    /** \brief returns daily progress towards the end of oviposition: deterministstatic ic part*/
    static double getDailyOvipositionProgress();
    /** \brief returns daily progress towards the end of oviposition: stochaststatic static ic partstatic */
    static double getDailyOvipositionRandomProgress();
    /** \brief The method applies density dependent mortality */
    void DoDensityDependentMortality();
    /**\brief The method checks whether the conditions for the start of Maturation are met and if
 * so toggles m_IsMature to true
 * */
    void CheckMaturity();
    /** \brief The method checks whether the individual can hibernate */
    bool CheckDormancy();
    /** \brief The method checks whether the individual can end hibernation */
    bool CheckDormancyExit();
    /** \brief The method sets the mature flag to be on */
    void setMaturity(bool IsMature){m_IsMature=IsMature;};
    /** \brief The method returns true if the beetle is mature for reproduction */
    [[nodiscard]] bool getIsMature() const{return m_IsMature;};
    /** \brief Rethurns maturation chance static */
    static double getMaturationChance();
    /** \brief A standard method tro be called to get the mortality chance */
    [[nodiscard]] double getDailyMortalityRate() const override;
    /** \brief returns the age of the individual */
    [[nodiscard]] int getAge() const;
    /** \brief returns the chance of dying due to extreme temperatures exposure */
    [[nodiscard]] double getExtremeTempMortality() const override;
    /** \brief Standard method to be run before the step **/
    void BeginStep() override;
    /** \brief Standard method to be run after the step **/
    void EndStep() override;
    /** \brief The standard function that removes the agent from the tracking map */
    void ClearFromMap(int X, int Y) override;
    /** \brief The standard function that add the agent to the tracking map */
    bool AddToMap(int X, int Y) override;
    /** \brief The method to be called when the beetle is reproducing */
    void Reproduce(int p_x, int p_y) override;
    /** \brief the method returns the minimum daily temperature: the value depends on whether the beetle is hibernating or not  */
    double getMinTemp() override;
    /** \brief the method returns the maximum daily temperature: the value depends on whether the beetle is hibernating or not  */
    double getMaxTemp() override;
public:
    /** \brief Adult poecilus beetle class constructor */
    Poecilus_Adult( int x, int y, Landscape* L, Poecilus_Population_Manager* BPM );
    /** \brief Adult poecilus beetle class destructor */
    ~Poecilus_Adult()override =default;
    /**\brief The method reuses memory for a new object of the type Poecilus_Adult   * */
    void ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) override;
    /** \brief calculation of minimum freezing threshold */
    [[nodiscard]] double getExtremeTempThreshold() const override;

private:


};
/** \brief The population manager class for Poecilus
 *
 * This should only include methods and attributes that are different from the generic beetle: e.g. move
 * */
class Poecilus_Population_Manager: public Beetle_Population_Manager, public std::enable_shared_from_this<Poecilus_Population_Manager>
{
protected:
    /** \brief The method that sets up population-wide temperature dependent factor of aphids consumption */
    void SetConsumeAphids(double temp);
    /** \brief Setting random generator for movement. It will be later used by all mobile individuals. */
    void setMaxDistanceRNG();
    /** \brief Connects to the aphid model(s) population manager(s) and asks for aphid numbers */
    int SupplyAphidsNumber(int x, int y, int aphid_species);
    /** \brief Connects to the aphid model(s) population manager(s) and reduces the aphids that were eaten  */
    void DeductAphidsNumber(int x, int y, int aphid_species, int number_to_deduct);
    /** \brief Connects to the aphid model(s) population manager(s) and resets the available aphids to zero  */
    void ResetAphidsNumber(int x, int y, int aphid_species);
public:
    /** \brief The method uses the additional data structure to fetch the local poecilus population of a specific stage (type)*/
    int getPoecilusDensity(int x, int y, int type);
    /** \brief The method decreases the number of poecilus beetles as accounted for in the data structure in this specific location by one */
    bool decPoecilusDensity(int x, int y, int type){
        if ((*m_PoecilusDensityMap)(x,y,type)==blitz::tiny((*m_PoecilusDensityMap)(x,y,type))){
            return false;
        }
        else{
            (*m_PoecilusDensityMap)(x,y,type)--;
            return true;
        }

    };
    /** \brief The method increases the number of poecilus beetles as accounted for in the data structure in this specific location by one */
    bool incPoecilusDensity(int x, int y, int type){
        if ((*m_PoecilusDensityMap)(x,y,type)==blitz::huge((*m_PoecilusDensityMap)(x,y,type))){
            return false;
        }
        else{
            (*m_PoecilusDensityMap)(x,y,type)++;
            return true;
        }

    };
    /**   \brief Method to add beetles to the population */
    void CreateObjects(int ob_type, TAnimal *pvo,void* null ,
                       std::unique_ptr<struct_Beetle>,int number) override;
    /** \brief The first method that is run every day on the population level */
    void DoFirst() override;
    /** \brief Constructor */
    explicit Poecilus_Population_Manager(Landscape* p_L);
    /** \brief Destructor: the same as in base class */
    ~Poecilus_Population_Manager() override= default;
    /** \brief Adjusts the stochastic part of daily egg production according to the mean daily temperature. */
    void setDailyRandomEggFactor(double temp);
    /** \brief Adjusts daily egg production according to the mean daily temperature. */
    void setDailyEggProduction(double temp);
    /** \brief Adjusts the stochastic part of daily chance to start oviposition according to the temperature */
    void setDailyOvipositionRandomFactor(double temp);
    /** \brief Adjusts the determenistic part of daily chance to start oviposition according to the temperature */
    void setDailyOvipositionFactor(double temp);
    /** \brief The method calculates and sets up the mean daily distance for non-stationary individuals */
    void setMeanDistance(double temp);
    /** \brief The method calculates and sets up the std of daily distance for non-stationary individuals */
    void setDistanceStd(double temp);
    /** \brief The method calculates and sets up the chance to start dormancy depending on the calendar day and the daily temperature */
    void setDormancyChance(double temp, int day);
    /** \brief The method calculates and sets up the chance to end dormancy depending on the calendar day and the daily temperature */
    void setDormancyExitChance(double temp, int day);
    /** \brief The method that decreases the number of aphids in aphid population model(s) */
    void decAphids(int x, int y, int num);
    /** \brief The method that returns the daily chance to start dormancy */
    double SupplyDormancyChance() const{return m_DormancyChance;};
    /** \brief The method that returns the daily chance to exit dormancy */
    double SupplyDormancyExitChance() const{return m_DormancyExitChance;};
    /** \brief The method that returns the pre-calculated daily mean distance to travel */
    double SupplyMeanDistance () const;
    /** \brief The method that returns the pre-calculated std of daily distance to travel */
    double SupplyDistanceStd () const;
    /** \brief The method that returns the temperature at which the beetle switches from seeking shadow to seeking light  */
    double SupplyTempThresh() const;
    /** \brief The method that uses random number generated to return daily aphid consumption */
    double SupplyAphidConsumption(){return AphidConsumptionToday->get();};
    /** \brief The method that uses random number generated to return daily beetle movement */
    int SupplyMovementDistance(){return (int) PoecilusMovementToday->get();};
    /** \brief Get adult population size */
    std::unique_ptr<PoecilusConstantClass> poecilusconstantslist{nullptr};
    /** \brief the pointer to the vector of aphid models population managers (can be empty if no aphid models are coupled for that run) */
    std::vector<std::shared_ptr<AphidsD_Population_Manager>> Aphids_PM{};
    /** \brief The std of the daily egg production */
    double m_DailyRandomEggFactor;
    /** \brief The mean of the daily egg production */
    int m_DailyEggs;
    /** \brief The mean of the daily progress towards the oviposition start */
    double m_DailyOvipositionFactor;
    /** \brief The std of the daily progress towards the oviposition start */
    double m_DailyOvipositionRandomFactor;
    /** \brief The mean of the daily movement distance */
    double m_DailyMeanDistance{0};
    /** \brief The std of the daily movement distance */
    double m_DailyDistanceStd{0};
    /** \brief The chance to start dormancy today */
    double m_DormancyChance;
    /** \brief The chance to exit dormancy today */
    double m_DormancyExitChance;
    /** \brief The number of aphid species to be run in parallel with taht model */
    int m_AphidSpeciesNum;

    /** \brief The init method of the Population Manager. Should be ran right after the constructor. */
    void Init() override;
private:
    /** \brief the density of poecilus lifeforms max 255 elements*/
    std::unique_ptr<blitz::Array<unsigned char, 3>> m_PoecilusDensityMap;
    /** \brief The number of stages to be reflected in the map: egg + 3 instars + pupa + adult*/
    int PoecilusStagesNumberInDensityMap = 6; //
    /** \brief The probability distribution that is set daily to reflect the daily aphid consumption */
    std::unique_ptr<probability_distribution> AphidConsumptionToday;
    /** \brief The probability distribution that is set daily to reflect the daily movement */
    std::unique_ptr<probability_distribution> PoecilusMovementToday;
};



#endif

