Ladybird MIDox                        {#Ladybird_page}
=========
<h1> MIDox for an ALMaSS Ladybird complex agent-based model </h1>
Chuhutin, A.<sup>*,1</sup>, Poulsen, T.<sup>1</sup>, Groom, G.<sup>1</sup> and Topping, C.J.<sup>1</sup>

1.	Department of Ecoscience, Aarhus University, C. F. Møllers Allé 8, 8000 Aarhus C, Denmark

*Corresponding author: Andrey Chuhutin, andrey@ecos.au.dk

## Introduction
This document will follow the Model Implementation Documentation with Doxygen (MIDox) format (\ref ref_topping_2023 "Topping et al., 2023"), the next step in designing, documenting and calibrating complicated models after the “Formal Model” format document (\ref ref_toppingetal_2022 "Topping et al., 2022"). Here we describe the implementation of the Ladybird formal model (\ref ref_chuhutin_2023 "Chuhutin et al., 2023").

The currently documented implementation has been calibrated (\ref ref_ziolkowska_2023 "Ziółkowska et al., 2023") using pattern-based calibration that was set up to replicate the basic ecological patterns of the ladybird population. In particular the phenological curves were used to fit the parameters of development, reproduction and mortality, the spatial distribution patterns, mean walking time and average movement speed measured in the field were used to fit the movement parameters. Information about the distribution and popularity of hibernaculae at different points in time was used to fit the parameters concerning the diapause and its timing.

This model was created within the ALMaSS  (Animal Landscape and Man Simulation System) framework (\ref ref_topping_2022 "Topping et al., 2022", \ref ref_topping_2003 "Topping et al., 2003") and therefore by design has the ability to connect with the other animal models that share the same framework (aphids as a food source, other polyphagous insect predators as potential competitors).

Designed in C++ using an object-oriented paradigm this model is by purpose made to be generic and easily expandable. If new behaviours of the ladybirds or new interconnection between the agents and the overall model will be discovered (e.g. in previously not modeled habitats) they could easily be introduced as a part of the model.

The division of the code into classes and methods was based on the overall model functionality and does not necessarily have a biological meaning. Thus, we combined the pre-pupa stage of development with the pupa stage since (due to its immobility) it shares the same basic behaviours. On the same note, the method that performs a long-range movement is shared independently of the movement purpose (whether it is search for food, dispersion or aggregation).

## Aims and Purposes
The purpose of this document is to both document the model source code, for readability and future maintenance, but also to record the decisions made when the formal model in its general algorithmic form (\ref ref_chuhutin_2023 "Chuhutin et al., 2023") was translated into code.

The seven-spot ladybird has complicated behaviours (\ref ref_hodek_2012 "Hodek et al., 2012") that are intricately connected to the landscape parameters, populations of other species inhabiting the same landscape and weather. In an effort to build an agent-based model one can only choose some of them for implementation. The behaviours that were chosen to be implemented in detail in our model are those that either:
- According to the literature and the experts we interviewed provide an important driver of ladybirds abundance or survival on the population level. (E.g. the cannibalism among the early developmental stages, the local dynamics at each cell, and the cannibalism of more developed stages on the less developed stages)
- Are an integral part of an essential processes that are well sampled and documented in literature and therefore provide a tool for the calibration of the model as a whole. (E.g. tracking the abundance of the ladybird in hibernaculae and the spread of the hibernating ladybirds in various landscape types are closely related to the distribution and mortality and are documented in literature (\ref ref_bianchi_2003 "Bianchi & Van der Werf, 2003"; \ref ref_honek_2007 "Honěk et al., 2007")).

The purpose of this model is to be used for complex scenarios landscape-based risk assessment together with a set of underlying prey models. However, it could be used as a standalone model (in the ALMaSS environment), provided presence of the phenology of the aphids, and therefore implementing only one-directional dependence between the predator and the prey.

## Model in Brief
The model aims to represent a population of ladybirds on a spatially-restricted landscape. While the model is running, agents (that represent various stages of ladybird development) are laid as eggs, develop, forage, move, reproduce, overwinter and die. During each one of these activities the agents interact with the landscape (through temporally-dynamic weather, crop rotations, farming events and vegetation growth models), the other species (the prey they are feeding on) and each other (cannibalism and collective hibernation).

The life-cycle of ladybird includes four main stages: eggs, larvae, pupae and adults (females), with each one of them implemented using a specific class (Ladybird_Egg_List, Ladybird_Larvae, Ladybird_Pupae and Ladybird_Adult, see Figure 1). The larva stage is composed of four instars, all of which utilize the same class Ladybird_Larvae. Our model is only implementing adult females, while adult males are ignored (the reasoning for that decision is provided by \ref ref_chuhutin_2023 "Chuhutin et al., 2023").

In addition to classes that represent various developmental stages three supplementary classes exist. The class Ladybird_Base includes the methods and variables which are used by more than one developmental stage (e.g. method Ladybird_Base::Cannibalise() is used both by Ladybird_Larvae and Ladybird_Adult). The class Ladybird_Population_Manager is a class that includes variables and implements methods that are utilised on the level of the ladybird population (e.g. Ladybird_Population_Manager::getMaxHopDistance() that returns maximum hop distance for a ladybird adult, which is the same for all the individuals on each day and is a function of mean daily temperature). The class LadybirdConstantClass includes all the constant variables that are defined in the start of the simulation, its instance is included as a member variable in Ladybird_Population_Manager and used by the rest of the ladybird classes.

Two important additional data structures were created inside Ladybird_Population_Manager for implementation purposes. Ladybird_Population_Manager::m_LadybirdDensityMap is a three-dimensional array implemented using the [Blitz++ library](https://github.com/blitzpp/blitz) that keeps track of the ladybirds of different developmental stages in the landscape. Note that hibernating ladybirds have a dedicated row in Ladybird_Population_Manager::m_LadybirdDensityMap despite not being implemented as a separate class. Each time, when the agent moves, dies or is created, Ladybird_Population_Manager::m_LadybirdDensityMap is updated. The variable type used in the array defines the maximum number of objects of each type so that if we use unsigned char the maximum number of objects is

\f[ N_\mathrm{max}=255\cdot \mathrm{Landscape}\_\mathrm{size}_x \cdot \mathrm{Landscape}\_\mathrm{size}_y \f]

which for a landscape of 10x10 km equals \f$255\cdot 10^8\f$. In practice the number is much lower, since not all parts of the landscape is used by the agents (e.g. ladybirds avoid locations occupied by water or unvegetated urban areas).
Ladybird_Population_Manager::m_LadybirdCannibalismMap is also a three-dimensional array which is updated at each cannibalism event and is used to remove from the simulation the individuals that have been eaten.
In the \ref implementation_ladybird "Implementation" we will provide a detailed account on the implementation of each one of the main behaviours, describing the data types and algorithms employed.

\image html LadybirdFig1.png "Figure 1: Overview of ladybird classes. The inheritance is denoted with an arrow directed from the inherited towards the base class."


## Scheduling {#scheduling_ladybird}
### Generic Scheduling Methods
In ALMaSS there are several basic step functions that are used throughout all the agent-based models. All of these methods are called from Beetle_Population_Manager::RunStepMethods() in the normal execution flow. Here we will mention those of them which are used in the ladybird model, while providing a short description of how each is used.

#### DoFirst()
This method is the first one to run before each day step, it runs on the level of the Population_Manager (i.e. it does not iterate between the agents). It provides an initial preparation for the run of the agents.
In the ladybird model the method calculates temperature-related constants, in this way all such calculations are performed only once a day with the results stored in the objects of the Population_Manager, which are accessible to all the living agents. In this way maximum daily distances for both adults and larvae are calculated (Ladybird_Population_Manager::calcMaxAdultDailyDistance() and Ladybird_Population_Manager::calcMaxLarvaDailyDistance()). In addition, it updates the day degrees for Larva and Adult development (Ladybird_Population_Manager::LadybirdUpdateDayDegrees()), temperature-related egg production factor (Ladybird_Population_Manager::LadybirdCalcDailyTempRelatedEggFactor(double temp)), and updates the hibernaculae list Ladybird_Population_Manager::UpdateHibernaculae() if it is the 1st of January.
Specifically for the ladybird eggs it is used to run Ladybird_Egg_List::BeginStep() for all the members of the Ladybird_Egg_List performing the daily mortality check and updating the total egg number.

#### BeginStep()
This method is executed per agent. It performs the mortality check  for most of the stages. In addition to that, it updates the number of eggs to be laid in the lifetime (based on the result of Ladybird_Population_Manager::LadybirdCalcDailyTempRelatedEggFactor(double temp) calculated in Ladybird_Population_Manager::DoFirst()) using Ladybird_Base::UpdateEggsToLay(), resets the movement counter (Ladybird_Base::resetMoveCounter()) and estimates the daily appetite for aphids (Ladybird_Base::calcAphidsAppetite()).

#### DoBefore()
This method is executed once for all the objects. In the ladybird model the primary use is to run Ladybird_Egg_List::Step() for each list instance.

#### Step()
This method is the main place where the ladybird behaviour is implemented. The method is responsible for moving between the developmental stages, movement in the landscape, initiating the foraging, reproductive and other behaviours. The execution of the step ends when there is no agent left with TALMaSSObject::m_StepDone false and when there is no more cannibalism events left to be dealt with (Cannibalisation() returns false) For more details see \ref behaviour_ladybird.

#### DoAfter()
This method is not used in this model.

#### EndStep()
This method is used to manage the pesticide mortality.

#### DoLast()
This method controls the output caused by the pesticide mortality and enables the catastrophe event (Beetle_Population_Manager::Catastrophe2()).

### Developmental and behavioural states {#behaviour_ladybird}

#### Developmental stages
The ladybird model has four basic developmental stages:
- Eggs: Implemented through Ladybird_Egg_List, where each object holds the list of the locations where the eggs have been laid at the same day. The eggs that are laid on the same day experience the same temperature and weather and therefore develop at the same pace. When this and all later developmental stages experience the environment, depending on the temperature and abundance of prey the individuals accumulate the number of eggs that they will lay in their lifetime.
- Larva: Implemented in Ladybird_Larvae. Each object is created when the egg hatches, it feeds on aphids, is able to move around the landscape and develops through 4 instar sub-stages. The development is a function of the temperature and aphid density. Older ladybird larvae are capable of cannibalising the younger individuals. If the agent survives all the developmental stages it is removed and at the same location pupa is created.
- Pupa: Implemented in Ladybird_Pupae. An immobile object that represents pupa and an immobile pre-pupa. The object develops as a function of temperature.
- Female adult: Implemented in Ladybird_Adult. A fully developed individual, that forages the aphids, is capable of moving by walking and flying, gathering in hibernaculae for overwintering and dispersing to the fields in spring and cannibalising the larvae if the amount of aphids is insufficient. Once maturity is reached and given a sufficient aphid population it can lay eggs. Once the adult has laid all its eggs, it dies.

#### Behavioural states

##### Eggs
The objects of Ladybird_Egg_List have two development stages:
- Ladybird_Egg_List::st_Develop() is in charge of the object development based on the daily temperatures.
- Ladybird_Egg_List::st_Hatch() is triggered in the end of the development and turns fully-developed eggs into larvae.

##### Larvae
The objects of Ladybird_Larvae implement two behavioural methods:
- Ladybird_Larvae::st_Develop() is a major behavioural state of the ladybird larvae. It controls the development and subsequent movement between the instar stages. In addition it is in charge of movement and foraging behaviour. The state triggers Ladybird_Larvae::st_Pupate().
- Ladybird_Larvae::st_Pupate() is the method which is responsible for turning the fully-developed larvae into pupae.

##### Pupae
The objects of Ladybird_Pupae use two developmental states:
- Ladybird_Pupae::st_Develop() controls the development as a function of mean daily temperatures. Once the development is finished the emerging is triggered.
- Ladybird_Pupae::st_Emerge() turns the pupae into adults.

##### Adults
The adults Ladybird_Adult have several developmental stages they are going through during their lifetime:
- Ladybird_Adult::st_Dispersal() is the method that controls the dispersal of the ladybirds from the hibernaculae to the fields. Taking account of the landscape and the aphid population the adults use short- and long-range movement to get to the places where they can feed on the aphids and reproduce. This state triggers the foraging state.
- Ladybird_Adult::st_Forage() is the method that controls the foraging state. During that stage the beetle forages on aphids, moves around using long- and short-range movement in search of food, cannibalises the larvae if no aphids are available, and lays eggs when the conditions for maturation and reproduction are met. This state can trigger either natural death (when all eggs have been laid) or (if the weather conditions and aphid abundance are suitable) aggregation behaviour that precedes winter hibernation.
- Ladybird_Adult::st_Aggregate() is the method that controls aggregation. The adult ladybirds use long-range flights to move to the areas suitable for hibernation and then use short range walks to find a hibernaculum, where they can overwinter in small groups. Once the ladybird joins the hibernaculum it moves to the hibernation state.
- Ladybird_Adult::st_Hibernate() is the method that controls a beetle’s behaviour while hibernating. During the hibernation the beetle remains stationary, while gaining an increased ability to withstand low temperatures. The end of hibernation comes triggered by the day length. At the end of it, the beetles that survived the winter move to the state of dispersal.

#### Start of the simulation
At the start of the simulation the adult ladybirds are randomly assigned to the types of landscape elements defined in BeetleConstantClass::BeetleStartHabitats. Since the simulation is normally starting on the 1st of January the beetles are initially in the hibernation state.
LadybirdConstantClass::InitialHibernaculaeNumber hibernaculae are randomly assigned to the types of landscape element mentioned in LadybirdConstantClass::LadybirdAggregationToles.

## Implementation {#implementation_ladybird}
### Oviposition {#oviposition_ladybird}
While the total number of eggs to be produced during the lifetime of a ladybird is determined throughout all the developmental stages, the actual oviposition is performed only by the adult (imago) developmental stage Ladybird_Adult. Ladybird_Adult's egg-laying behaviour is implemented as part of the foraging state: Ladybird_Adult::st_Forage(). See more under \ref scheduling_ladybird.

#### Cumulative number of eggs produced in the life cycle
In our model we assume that the number of eggs produced by the female defines the life span of the beetle, meaning that once the female lays all its eggs it dies (unless it is killed before as a result of a stochastic mortality event caused by weather or agricultural practice in e.g. Ladybird_Larvae::TempRelatedMortality() and Beetle_Base::OnFarmEvent()). This number (Ladybird_Base::m_TotalEggsToLay) is established during the development stages before maturation and depends on the environment it experiences. Ladybird_Base::m_TotalEggsToLay is re-evaluated every day (before maturation) using Ladybird_Base::UpdateEggsToLay(int preydensitylevel, double temp). It is estimated based on the current air temperature \f$T\f$ and encountered number of aphids \f$N_p\f$.

\f[ n_\mathrm{eggs}(T,N_p) = f_\mathrm{eggs}(T)\cdot f_\mathrm{eggs}(N_p) \f]
where \f$f_\mathrm{eggs}(T)\f$ and \f$f_\mathrm{eggs}(N_p)\f$ are temperature and prey factors respectively. Thus the total number of eggs the ladybird lays in its lifetime is:

\f[ N_\mathrm{eggs} = \sum_{d\in\text{days before maturity}}n_\mathrm{eggs}(T(d),N_p(d))  \f]

The functional temperature-dependent factor of daily egg production \f$f_\mathrm{eggs}(T)\f$ was estimated based on the total number of eggs in a lifetime as a function of temperature for Adalia bipunctata from (\ref ref_jalali_2009 "Jalali et al., 2009") which was factored to fit the mean values (n=1005.75) measured by \ref ref_kalushkov_2004 "Kalushkov and Hodek (2004)" (20 at T=25°C for Coccinella septempunctata and fit to a second degree polynomial. For details see the Formal Model paper (\ref ref_chihutin_2023 "Chuhutin et al., 2023").  The factored values are stored in LadybirdConstantClass::LadybirdEPD.

The functional prey-dependent factor \f$f_\mathrm{eggs}(N_p)\f$ was also measured by \ref ref_kalushkov_2004 "Kalushkov and Hodek (2004)". They provided food ad libitum, so we regard their results as maximums and multiply by a factor for taking into account the insufficient food supply. These factors, which we estimated from (\ref ref_xia_1999 "Xia et al., 1999") and fit to the second degree polynomial are available in LadybirdConstantClass::LadybirdPDf. The levels of prey density vary for different development stages and are defined (based on literature) using Ladybird_Base::PreyDensityLevel(long AphidsNum), which is overridden in classes that present different development stages.

#### Number of Eggs Laid Daily
In our model, the ladybird oviposition occurs in egg clutches which are distributed along the route of movement. The number of eggs in each clutch is randomly distributed from a Gaussian with mean \f$ \mu = 36 \f$ and standard deviation \f$ \sigma = 10 \f$ (\ref ref_honek_2008 "Honek et al., 2008") and is implemented in Ladybird_Adult::CalcMaxEggs(). The number of eggs laid by the ladybird in a day is given by the class variable Ladybird_Adult::DailyEggs which is set via Ladybird_Adult::setDailyEggs(int num) and accessed via Ladybird_Adult::getDailyEggs(). The estimation is based on the Bieri model (\ref ref_lanzoni_2004 "Lanzoni et al., 2004").

The oviposition process is related to the mechanism of dispersal and movement (see \ref movement_ladybird) and depends on the aphid density (\ref ref_honek_1980 "Honek, 1980"). Beetles disperse from the overwintering sites in the search for a field with a high enough aphid density to settle (implemented in Ladybird_Adult::st_Dispersal()). For the threshold, the minimum density in (\ref ref_honek_1980 "Honek, 1980") was chosen (LadybirdConstantClass::LadybirdAphidDensityToStartForaging defined to be \f$ 2550 \mathrm{cm}^2\f$ leaf area/aphid). Once the maturation of the ovarioles has finished (minimum 4 days according to literature, configurable through LadybirdConstantClass::p_cfg_LadybirdMinMaturation), the beetle is allowed to reproduce if the aphid density is sufficient (minimum value \f$ 335 \mathrm{cm}^2\f$ / aphid configured through LadybirdConstantClass::LadybirdAphidDensityToStartReproduction). If the aphid population does not support oviposition, the normal foraging will continue until a patch with a high enough population of prey is found. This simplified scheme of the oviposition behaviour is seen in Figure 2.
<br>
\image html LadybirdFig2.png "Figure 2: Part of the ladybird decision-making leading to oviposition or dispersal."
<br>
In addition, the number of eggs the ladybird is allowed to lay on the landscape is dependent on the abundance of prey. Thus, the number is given by Ladybird_Adult::getTodaysEggProduction() which refers to Ladybird_Population_Manager::getLadybirdEggProductionXY which is updated daily based on the number of aphids.

### Development
To model the development of the ladybird stages we use the linear day degree model based on the estimated values of LDT (lower development threshold) (\ref ref_honek_1990 "Honěk & Kocourek, 1990") and SET (sum of effective temperatures). The values come from fitting the data from (\ref ref_xia_1999 "Xia et al., 1999") and they are available in \ref table1_ladybird "Table 1" or in LadybirdConstantClass::LadybirdLDTs and  LadybirdConstantClass::LadybirdThresholdDD.

<table>
<td colspan=1>
     <caption id="table1_ladybird">Table 1: Estimated developmental constants. SET is calculated from the last transformation and therefore is not cumulative. The LDT and SET for the adult stage refer to the time needed to start oviposition.</caption>
</td>
<tr>
| Stage           | LDT (°C) | SET [dd]  |
| --------------- | -------- | --------- |
| Egg             | 11.5     | 42        |
| Larva: instar 1 | 13.8     | 22.8      |
| Larva: instar 2 | 13.6     | 20.4      |
| Larva: instar 3 | 13.6     | 23.1      |
| Larva: instar 4 | 13.9     | 38        |
| Pupa            | 12.9     | 63.6      |
| Adult           | 12.4     | 151.9     |
</tr>
</table>

In practice, the day degree estimation happens only once a day for all of the stages inside Ladybird_Population_Manager::LadybirdUpdateDayDegrees(double temptoday, int today). The function updates the variable Ladybird_Population_Manager::m_DayDeg that stores accumulated day degrees for all the individuals that started their current stage at a particular day. The variable is transparent to the objects that are managed by the Ladybird_Population_Manager and can thus be accessed by the individuals.

Apart from the temperature, the development of several important lifetime stages of the ladybird depends on:
- larva to immobile pre-pupa: in addition to the temperature, the transformation is affected by the presence of aphids
- pre-oviposition imago to hibernation (time length \f$T_h\f$): based on day length and presence of prey.
- from hibernation to dispersion (time length \f$T_d\f$): driven by the photoperiod (emergence from hibernation) plus weather, since weather suitable for flying is needed for movement to the fields).

Furthermore, the larva and imago development has a linear dependency on the prey density such that the development is faster, when the prey level is higher. This is implemented by introducing a dependency of prey density in the used SET value:	 

\f[ \text{SET}\left(N_\text{prey}\right)=\text{SET} \cdot f_{L}\left(N_\text{prey}\right) \f]

where \f$ f_{L}\left(N_\text{prey}\right) \f$ is multiplied by the SET value from \ref table1_ladybird "Table 1". The factors were estimated from literature and are available in LadybirdConstantClass::LadybirdInstarAphidDevelFactor and in \ref table2_ladybird "Table 2".  

<table>
<td colspan=1>
     <caption id="table2_ladybird">Table 2: The factor of the prey density in the ladybird larvae development.</caption>
</td>
<tr>
| Prey density | I     | II   | III   | IV   | V    | VI |
| ------------ | ----- | ---- | ----- | ---- | ---- | -- |
| Instar 1     | 1.615 | 1.46 | 1.269 | 1.12 | 1.04 | 1  |
| Instar 2     | 1.78  | 1.5  | 1.33  | 1.22 | 1.11 | 1  |
| Instar 3     | 1.8   | 1.55 | 1.3   | 1.1  | 1.05 | 1  |
| Instar 4     | 1.67  | 1.43 | 1.29  | 1.10 | 1.05 | 1  |
</tr>
</table>

### Mortality
The mortality of each individual is applied at each step of the simulation through stochastic variables drawn from a random number generator and compared to parameter values from the model. Farming activities can be mechanistically connected to increased mortality. The list of activities that have non-zero mortality chance is provided in \ref table3_ladybird "Table 3".

<table>
<td colspan=1>
     <caption id="table3_ladybird">Table 3: Agricultural activities that have non-zero mortality in ladybirds (note that pesticide mortality/treatment constitute a separate mechanism that will be described separately.</caption>
</td>
<tr>
| Activity                 | Egg mortality | Larva mortality | Pupa mortality | Adult mortality |
| ------------------------ | ------------- | --------------- | -------------- | --------------- |
| Harrowing                | 0.5           | 0.5             | 0.5            | 0.27            |
| Ploughing                | 0.5           | 0.5             | 0.5            | 0.27            |
| Using Stubble Cultivator | 0.5           | 0.5             | 0.5            | 0.27            |
| Autumn roll              | 0.5           | 0.5             | 0.5            | 0.27            |
| Autumn sowing            | 0.5           | 0.5             | 0.5            | 0.27            |
| Insecticide application  | 0.0           | 0.8             | 0.8            | 0.8             |
| Bed forming              | 0.5           | 0.5             | 0.5            | 0.27            |
| Bulb harvesting          | 0.5           | 0.5             | 0.5            | 0.27            |
| Harvest                  | 0             | 0               | 0              | 0.25            |
| Strigling                | 0             | 0               | 0              | 0.29            |
</tr>
</table>

In the source code the agricultural mortality is among a plethora of other ladybird behaviours that are implemented using polymorphism (\ref ref_stroustrup_1995 "Stroustrup, 1995"). The method Beetle_Base::OnFarmEvent() checks what type of agricultural activity the object has experienced and triggers a call to an activity-specific function if mortality should be applied e.g. getSoilCultivationMortality(). Such a function is returning different values of mortality (Table 3) depending on the type of the object (e.g. Ladybird_Larvae::getSoilCultivationMortality() or Ladybird_Adult::getSoilCultivationMortality()).

The mortality is implemented in the beginning of each step (e.g. Beetle_Larvae::BeginStep()) through TAnimal::CheckManagement().

Additionally, temperature-related mortality is implemented in pre-imago stages with values derived from laboratory data (\ref ref_xia_1999 "Xia et al., 1999") and are given in \ref table4_ladybird "Table 4". The temperature-related mortality is applied within the method TempRelatedMortality that is overridden in various Ladybird classes (e.g. Ladybird_Larvae::TempRelatedMortality(double temp, double maxtemp, double mintemp)).

<table>
<td colspan=1>
     <caption id="table4_ladybird">Table 4: Temperature related mortality in non-imago stages of ladybird.</caption>
</td>
<tr>
| Temperature range | 10     | 15      | 20       | 25       | 30      | 35       |
| ----------------- | ------ | ------- | -------- | -------- | ------- | -------- |
| Eggs              | 0.0342 | 0.0302  | 0.029655 | 0.039    | 0.0630  | 0.158125 |
| Pupa              | 0.0189 | 0.01829 | 0.014158 | 0.028596 | 0.05889 | 0.08526  |
| Larva Instar 1    | 0.05   | 0.05    | 0.06     | 0.07     | 0.12    | 0.22     |
| Larva Instar 2    | 0.02   | 0.02    | 0.03     | 0.05     | 0.06    | 0.1      |
| Larva Instar 3    | 0.01   | 0.01    | 0.02     | 0.04     | 0.07    | 0.09     |
| Larva Instar 4    | 0.01   | 0.01    | 0.02     | 0.03     | 0.05    | 0.06     |
</tr>
</table>

Extreme temperature mortality is also implemented using the same method (Ladybird_Larvae::TempRelatedMortality(), Ladybird_Pupae::TempRelatedMortality(), Ladybird_Adult::TempRelatedMortality(), Ladybird_Egg_List::TempRelatedMortality()) For pre-imago stages the threshold temperatures are defined in LadybirdConstantClass and are provided in \ref inputs_ladybird. The application of extreme temperature mortality increases the daily chance of dying by 25% (defined in BeetleConstantClass::p_cfg_ExtremeTempMort).

For adults, the lower threshold temperature is different for each month of the year (defined in LadybirdConstantClass::Ladybird_SCP). Because ladybirds hibernate in groups and hide in insulated places, the dormant ladybirds are assumed to have effectively lower SCP (by 20%, defined in LadybirdConstantClass::p_cfg_LadybirdExtremeTempMinHibernatingFactor).

In addition to that, all stages have background mortality due to predators and parasites which are adjusted during the calibration stage and are given by:
- LadybirdConstantClass::p_cfg_LadybirdDailyEggMortality
- LadybirdConstantClass::p_cfg_LadybirdDailyLarvaeMortality
- LadybirdConstantClass::p_cfg_LadybirdDailyPupaeMortality
- LadybirdConstantClass::p_cfg_LadybirdDailyAdultMortality

### Cannibalism ### {#cannibalism}
In the model, cannibalism has been implemented as ‘the last resort’ for a ladybird. Therefore, cannibalism is a part of the foraging behaviour (Ladybird_Larvae::st_Develop() and Ladybird_Adult::st_Forage()). Only if the ladybird cannot find any aphids to eat, it will try to cannibalise the younger individuals. Regardless of how satisfactory a cannibalisation attempt was, the adult or larva moves in search of a place with more aphids.

The cannibalism in terms of implementation can be subdivided into two processes:

- From the viewpoint of the cannibal (implemented in Ladybird_Base::Cannibalise) initiated during foraging as described above.
- From the viewpoint of the victim (implemented in Beetle_Base::Cannibalisation()). This method is called before each  Step() made by the agent that can be cannibalised. The method checks the cannibalism map Ladybird_Population_Manager::m_LadybirdCannibalismMap for an object that was cannibalised at the point where the current agent is located and if this object is of the same type, as the agent, the agent dies (Beetle_Base::st_Die()).

There is no cannibalisation of adults by adults, and no cannibalisation by pupae. Each stage can only eat the less advanced stage. E.g. adults can eat instars 1-4 and eggs, instar 4 can only eat instars 1-3 and eggs, and so on.

Since in this model we keep track of the ladybird appetite, there is a need to establish a nutritional value of the cannibalised individuals.  The nutritional value was estimated based on literature from the comparison between the mean weight of the ladybird stage and an aphid see LadybirdConstantClass::LadybirdCannibalismCost and \ref table5_ladybird "Table 5".

<table>
<td colspan=1>
     <caption id="table5_ladybird">Table 5: Sizes and assumed nutritional values (in aphids) of different life stages of seven-spot ladybird.</caption>
</td>
<tr>
| Life stage      | Nutritional value (in aphids) | Weight [mg] |
| --------------- | ----------------------------- | ----------- |
| Egg             | 1                             | 0.2         |
| Larva, instar 1 | 10                            | 2           |
| Larva, instar 2 | 30                            | 3           |
| Larva, instar 3 | 80                            | 8           |
| Larva, instar 4 | 120                           | 12          |
| Pupa            | 140                           | 14          |
| Adult4          | 160                           | 16          |
</tr>
</table>

### Movement {#movement_ladybird}
In our model implementation the ladybird methods can call three types of movement methods:
- Random movement (Ladybird_Adult::RandomMovement()). This movement is not driven by a need to search for higher densities of aphids and mostly occurs during aggregation or as a response to good or overly high aphid density. The adult or larva has a non-zero chance (LadybirdConstantClass::p_cfg_LadybirdUnnecessaryMovementChance) to perform an ordinary short-range movement.
- Short-range movement (Ladybird_Adult::ShortRangeMovement()). This movement is used to simulate intensive search behaviour when the ladybird or larva scans the local surroundings in search of prey.
- Long-range movement (Ladybird_Adult::LongRangeMovement()). The extensive search for a different type of landscape or vegetation. This movement could be either part of aggregation or dispersion behaviour or a response to insufficient resources at the current location.

At each step, the beetle cannot move more than the maximum movement distance depending on the temperature.

The mechanism of the movement is as follows. If there are enough aphids in the current location, the ladybird generally stays but has a chance (LadybirdConstantClass::p_cfg_LadybirdUnnecessaryMovementChance) to perform a random walk. However, if there are not enough aphids to sustain the individual, it will attempt cannibalisation (4.4). Then if there is still some movement left to do today, it will perform a short-range movement based on the gradient of prey abundance. The direction of such movement is chosen from 8 cardinal directions. Since the movement of ladybird is driven by the number of aphids, the directions that provide higher aphid populations are preferred:

\f[p\left(\hat{n}\right)=\frac{L\left(\hat{n},l_{\mathrm{sensing}}\right) -L\left(\hat{n},0\right)}{ \sum_{\hat{n} \in S_{\mathrm{directions}}} L\left(\hat{n},l_{\mathrm{sensing}}\right) - L\left(\hat{n},0\right)}\f]

where \f$S_{\mathrm{directions}}\f$ is a set of 8 cardinal directions, \f$l_{\mathrm{sensing}}\f$ is the sensing distance (see LadybirdConstantClass::p_cfg_LadybirdAphidSensingDistance and LadybirdConstantClass::p_cfg_LadybirdLarvaAphidSensingDistance) and \f$ L\left(\hat{n},l_{\mathrm{sensing}}\right)-L\left(\hat{n},0\right)\f$ is the difference in aphid population between the remote place and the current location.

After the movement direction is chosen, the beetle moves one step \f$ l\sim N \left(\frac{l_{\mathrm{sensing}}}{10},\frac{l_{\mathrm{sensing}}}{30}\right)\f$ (implemented as Ladybird_Population_Manager::m_ShortRangeDistanceAdult for adults and Ladybird_Population_Manager::m_ShortRangeDistanceLarva for larvae) in this direction. If a threshold number of short-range movements (LadybirdConstantClass::p_cfg_LadybirdMaxShortRangeAttempts) has been reached and the food has not been found (appetite was not satisfied), the individual attempts a long-range movement.

The distance the beetle covers in a day while walking depends on the temperature. From the mean ladybird time-budget, the ladybird is searching for prey approximately 71% of the daily active time. So the daily time of travel is \f$ t=0.71\cdot\text{daylight_time}\f$, where \f$\text{daylight_time}\f$ is measured in minutes. The temperature-dependent speed of movement is (based on \ref ref_elliott_2000 "Elliott et al., 2000"):

\f[v=25+\left(T-14\right)\cdot\frac{135-25}{34-14}\left[\text{cm}/\text{min}\right]=25+\left(T-14\right)\cdot\frac{115}{20}\left[\text{cm}/\text{min}\right]\f]

So, in total, the maximum daily distance in meters is

 \f[ S_d=0.0071\cdot\left(25+\left(T-14\right)\cdot\frac{115}{20}\right)\cdot\text{daylight_time} \f]

when T>10°C. For 4°C<T<10°C the distance covered is 1m per 3 hours of daylight. The daily distance is calculated in Ladybird_Population_Manager::calcMaxAdultDailyDistance() once a day at Ladybird_Population_Manager::DoFirst() using the mean daily temperature and the value is stored at Ladybird_Population_Manager::m_MaxAdultDistance from where the adult beetles access the information using Ladybird_Population_Manager::getMaxAdultDailyDistance each time they are about to perform the movement.

Based on the ratio between the speeds of adult and larva:

\f[S_{d,\mathrm{larva}}=\frac{S_d}{2}\f]

the maximum daily larva distance is calculated in Ladybird_Population_Manager::calcMaxLarvaDailyDistance() once a day at Ladybird_Population_Manager::DoFirst() using the mean daily temperature and the value is stored at Ladybird_Population_Manager::m_MaxLarvaDistance from where the larvae access the information using Ladybird_Population_Manager::getMaxLarvaDailyDistance each time they are about to perform the movement.

The long-range movement procedure starts by choosing the direction of the flight. The ladybird will generally prefer to fly along the direction of the wind. Based on the reduction due to travelling in the boundary layer (factor of \f$a=0.075\f$), the flight speed of the ladybird \f$ v_L = 1.5 \left[\frac{\mathrm{m}} {\mathrm{s}}\right]\f$ and maximum travelling distance (\f$500 m\f$) the maximum travel distance for the given day and travel direction is estimated:

\f[ S_L = T_\mathrm{max}\cdot\left(v_L-a\cdot v_w \cdot \cos\left(\alpha_w\right)\right) \f]

where \f$ T_\mathrm{max} \f$ is a max flying time, \f$v_w\f$ is the wind speed and \f$\alpha_w\f$ is an angle between the wind and flight directions; the method is implemented in Ladybird_Base::getLongRangeDistance(). Since the maximum possible distance depends on the flight direction, that is calculated inside the long-range movement method (Ladybird_Adult::LongRangeMovement()). Once the distance and the direction of the flight are established, we check potential target areas. Unless the ladybird is travelling to hibernaculum (which is prioritised no matter how far they are in the range up to the maximum flying distance \f$S_L\f$, the ladybird chooses the closest location of a suitable landscape vegetation type.

There is a minimum required temperature 15°C for flying, the wind speed should not exceed 20 m/s. This is implemented in Ladybird_Population_Manager::getFlyingWeather(), that in case the weather is not suitable for flying returns false.

The movement towards hibernacula (during Ladybird_Adult::st_Aggregate()) is implemented using the regular long-range movement method Ladybird_Adult::LongRangeMovement(). The difference in long-range movement between the foraging and aggregation states results from the different types of landscape features that the ladybirds are looking for. After finding the suitable landscape, the adult looks for a clump no bigger than 20 individuals while performing the short-range jumps. If the clump is full and the ladybird is not ready to hibernate alone, it performs another long-range jump.

The movement from the hibernacula (Ladybird_Adult::st_Dispersal()) is also a regular long-range movement. Based on the literature, we decided that the minimum required density of aphids corresponds to leaf area of \f$ 2550 \mathrm{cm}^2\f$ per aphid (see \ref oviposition_ladybird and \ref hibernation_ladybird).

### Hibernation {#hibernation_ladybird}
In the search for hibernation places, the ladybirds are looking for types of landscapes, which are related to rocks or forest/orchard edges. \ref table6_ladybird "Table 6" has a list of the relevant landscape types and the corresponding ALMaSS landscape elements, which are defined in LadybirdConstantClass::LadybirdAggregationToles.

<table>
<td colspan=1>
     <caption id="table6_ladybird">Table 6: Preferred hibernation places (types of landscape) and a corresponding ALMaSS type of landscape (#TTypesOfLandscapeElement).</caption>
</td>
<tr>
| Type of landscape | ALMaSS landscape element |
| ----------------- | ------------------------ |
| Chestnut Forest   | #tole_ChestnutForest      |
| Farm Forest       | #tole_FarmForest          |
| Fence             | #tole_Fence               |
| Mixed Forest      | #tole_MixedForest         |
| Invasive Forest   | #tole_InvasiveForest      |
| Individual Tree   | #tole_IndividualTree      |
| Young Forest      | #tole_YoungForest         |
| Coniferous Forest | #tole_ConiferousForest    |
| Deciduous Forest  | #tole_DeciduousForest     |
| Eucalyptus Forest | #tole_EucalyptusForest    |
| Cork Oak Forest   | #tole_CorkOakForest       |
| Holm Oak Forest   | #tole_HolmOakForest       |
| Other Oak Forest  | #tole_OtherOakForest      |
| Christmas Trees,  | #tole_ChristmasTrees      |
| Riverside Trees   | #tole_RiversideTrees      |
| Riverside Plants  | #tole_RiversidePlants     |
| Bare Rock         | #tole_BareRock            |
| Stone Pine Forest | #tole_StonePineForest     |
| Stone Wall        | #tole_StoneWall           |
| Roadside Slope    | #tole_RoadsideSlope       |
| Roadside Verge    | #tole_RoadsideVerge       |
</tr>
</table>

The aggregation is triggered during the foraging stage (Ladybird_Adult::st_Forage()) and is controlled by the method Ladybird_Adult::ShouldStartAggregating(int day). The decision can be taken between first and last day of aggregation (LadybirdConstantClass::p_cfg_LadybirdAggregationStart, \f$d_\mathrm{F}\f$ and LadybirdConstantClass::p_cfg_LadybirdAggregationEnd, \f$d_\mathrm{L}\f$), but the chance to start aggregation on a particular day (between \f$d_\mathrm{L}\f$ and \f$d_\mathrm{F}\f$) depends on the aphid density:

\f[
p(N_{\mathrm{aphids}}) = \begin{cases}
    1                 & \mathrm{if }\quad N_{\mathrm{aphids}} < N_{\mathrm{aphids,low}} \\
    p_{\mathrm{hib}}  & \mathrm{if }\quad N_{\mathrm{aphids,high}} > N_{\mathrm{aphids}} < N_{\mathrm{aphids,low}} \\
    0                 & \mathrm{if }\quad N_{\mathrm{aphids}} > N_{\mathrm{aphids,high}}
\end{cases}
\f]

The main triggers for the hibernation are the photoperiod (explicitly through \f$d_\mathrm{L}\f$ and \f$d_\mathrm{F}\f$) and the decrease in temperature and physiological ageing of the plants (implicitly through the aphid density). The ladybirds cannot stay active after the last day of aggregation.

The method Ladybird_Population_Manager::isEmergenceDay() controls the emergence from the hibernaculum whereas the photoperiod drives the end of overwintering. The first day when the day length \f$\tau_\mathrm{day}\f$ (method Landscape::SupplyDaylength()) is longer than a threshold \f$t_\mathrm{E}\f$ (set up as LadybirdConstantClass::LadybirdEmergingDayLength) is a first emergence day. Currently, all the ladybirds emerge simultaneously on the first emergence day.

To simulate the behaviour of ladybird beetles that allows them to mix and join the hibernaculae, we assumed variation in the willingness of the beetle to hibernate alone through the aggregation period. Thus, the probability of aggregating alone increases early in the period, causing some beetles to initiate clumps. Subsequently, this probability is reduced, causing the ladybirds to search for a hibernaculum that has space for that individual (\f$n_{\mathrm{beetles}}<N_{\mathrm{beetles,max}}\f$). Just before the end of the period, the chances to hibernate alone rise again, so each beetle eventually has a 100% chance to hibernate. This behaviour is implemented in Ladybird_Adult::getChanceToHibernateAlone().

We include differences in temperature-dependent mortality between the active and the hibernating individuals by applying different SCP (supercooling points) for inside and outside hibernaculae (20% difference, defined in LadybirdConstantClass::p_cfg_LadybirdExtremeTempMinHibernatingFactor). This difference represents the shelter that the hibernaculae provide to the overwintering beetles.

The fact that the location (1x1m resolution) was used as a hibernation site is recorded and is accessible to the ladybirds for a time period of \f$\tau_{\mathrm{hibernation\_clues}}=5 \mathrm{y}\f$. Since this value defines the size of the static data storage (array), we implemented it using the preprocessor variable #HIBERNACULA_MEMORY.

After performing a long-range jump into the landscape suitable for hibernation (listed in LadybirdConstantClass::LadybirdAggregationToles), the adult walks around in short-range movements, looking for an unoccupied hibernaculum. If the clump is not bigger than the maximum capacity (the number of hibernating individuals is smaller than LadybirdConstantClass::LadybirdMaxClumpSize), it joins. If the clump is too big, or there is no clump to be found, it continues searching. After several unsuccessful attempts, the individual can take another long flight to try searching in another location in the landscape. This heuristic algorithm is implemented in Ladybird_Adult::st_Aggregate().

During dispersal (implemented in Ladybird_Adult::st_Dispersal()), the beetles use long-range flights. There, the choice of the target depends on the landscape type (agricultural fields with a potential for aphid population and adjoining areas: landscape elements defined in LadybirdToleTovs_struct::LadybirdLongRangeForagingToles and vegetation types defined in LadybirdToleTovs_struct::LadybirdLongRangeForagingTovs). However the decision on settling is made based on the aphid density, which should be above the minimum acceptable density defined in LadybirdConstantClass::LadybirdAphidDensityToStartForaging.

### Foraging
The ladybird's consumption rate (appetite) is defined by its ages:

$$ A_\mathrm{aphids}=\mathrm{age} \cdot f_\mathrm{stage} $$

where the age is in days and \f$f_\mathrm{stage}\f$ is the linear appetite factor given in \ref table7_ladybird "Table 7".

<table>
<td colspan=1>
     <caption id="table7_ladybird">Table 7: Linear appetite factor for ladybird developmental stages.</caption>
</td>
<tr>
| Stage name      | \f$f_\mathrm{stage}\f$ |
| --------------- | - |
| Egg             | 0 |
| Larva, instar 1 | 5 |
| Larva, instar 2 | 5 |
| Larva, instar 3 | 7 |
| Larva, instar 4 | 7 |
| Pupa            | 0 |
| Adult           | 8 |
</tr>
</table>

When the ladybird is foraging (in stage Ladybird_Adult::st_Forage() or Ladybird_Larvae::st_Develop()) and there are enough aphids in the current location to satisfy their appetite, the adults only move randomly and larvae stay in the same 1x1 m square (they are stationary from the simulation perspective).

When there are not enough aphids at the location to satisfy their appetite, then cannibalism can occur and the adults perform short-range movement towards the direction of a higher concentration of aphids (implemented in Beetle_Base::Move()). Long-range flight is triggered if there are not enough aphids for several moves in a row (i.e. more than the short movement threshold LadybirdConstantClass::p_cfg_LadybirdMaxShortRangeAttempts). If out of the last \f$W_\mathrm{LRM}\f$ steps there were more than \f$N_\mathrm{LRM}\f$ attempted long flights, the beetle dies.

After each ovipositing, short-range movement is triggered so the adult won’t deplete the aphid resources of the young.

The ladybird model gets the current number of aphids in the location using Ladybird_Population_Manager::getAphidsNumber() that provides a connection between the ladybird model and the concurrently-running aphid model(s). Once the ladybird eats a number of aphids, the method Ladybird_Population_Manager::decAphids() decreases the number of aphids in one of the underlying aphid models. If there is more than one aphid model that has aphids at this concrete location, the aphids are subtracted from the model in a random order.

## Interconnections
In common with other ALMaSS models the model is connected to the Landscape, Farm and Crop classes.

Some of the Ladybird functionality is implemented in the Beetle classes (Beetle_Base, Beetle_Egg_List, Beetle_Larvae, Beetle_Pupae, Beetle_Adult, see Figure 1).

The ladybird model uses the ALMaSS random library (probability_distribution) for random numbers generation.

This model is naturally connected to the aphid models, as the aphids constitute the natural prey of the ladybirds and their presence and abundance regulate the transfer between behavioral stages of ladybirds. From the other side, by foraging the aphids the ladybirds regulate the aphid population. The connection between the the ladybirds and the aphids is established through the calls of the Ladybird_Population_Manager methods:  Ladybird_Population_Manager::getAphidsNumber() returns aphids at a given location and Ladybird_Population_Manager::decAphids(). The aphid species that could be used as prey by the ladybird are defined through the configuration, using LadybirdConstantClass::p_cfg_LadybirdAphidSpecies (configuration variable: LADYBIRD_APHID_SPECIES).

In the case where several models are running simultaneously, the other species that can consume aphid prey (e.g. Poecilus) could serve as competitors and therefore influence the population size and distribution of ladybirds.

## I/O, Variables & Scales

### Inputs {#inputs_ladybird}
Our model uses a landscape file set (that consists of lsb: a binary landscape file, farmref: a list of farm references connecting farms to the farmtypes and polyref: a list of polygon references connecting landscape polygons to type of landscape element, farm reference, soiltype, etc). In addition to that a standard input is a number of rotation files, that describe the common way of rotating the fields of a each farmtype.
The weather input to the model is provided in a separate file that includes the following variables:
- mean temperature
- mean wind
- precipitation
- minimum temperature
- maximum temperature
- soil temperature
- snow depth
- humidity
- soil temperature at twilight
- solar radiation

for each day of the simulation. The weather file is based on the real data in the given location and is used for the simulation in a circular manner.

Our model uses a configuration file that includes all the previously mentioned input files, output files as parameters along with the specific ecological parameters of the ladybird model. These parameters have predefined default values, but could be altered for each simulation. The list of the most essential parameters, their default values, name of the constants they are stored in and the variable name to be used in the config file is provided in Table 8.

<table>
<td colspan=1>
     <caption id="table8_ladybird">Table 8: Model parameters.</caption>
</td>
<tr>
| Parameter name  (including the symbol)                  | Value    | Units                          | Domain      | Constant name                                                  | Variable name in the configuration file |
| ------------------------------------------------------- | -------- | ------------------------------ | ----------- |--------------------------------------------------------------- | --------------------------------------- |
| Egg clutch size distribution                            | N(36,10) | eggs                           | Oviposition | Ladybird_Population_Manager::m_ClutchSizeRandom                | \-                                      |
| Minimum maturation period                               | 4        | days                           | Oviposition | LadybirdConstantClass::p_cfg_LadybirdMinMaturation             | LADYBIRD_MIN_MATURATION_LENGTH          |
| Sufficient aphid density for ovipositing                | 334      | cm<sup>2</sup>/aphid           | Oviposition | LadybirdConstantClass::LadybirdAphidDensityToStartReproduction | \-                                      |
| Extreme temperature mortality chance                    | 0.25     | \-                             | Mortality   | BeetleConstantClass::p_cfg_ExtremeTempMort                     | BEETLE_EXTREMETEMPMORT                  |
| Hibernation bonus (in SCP)                              | 0.2      | \-                             | Mortality   | LadybirdConstantClass::p_cfg_LadybirdExtremeTempMinHibernatingFactor | LADYBIRD_EXTREME_MIN_HIB          |
| Min temperature for non imago stages, \f$ t_\text{extreme, min} \f$| \-20     | °C                  | Mortality   | LadybirdConstantClass::p_cfg_LadybirdExtremeTempMin            | LADYBIRD_EXTREME_MIN                    |
| Max temperature for all stages, \f$ t_\text{extreme, max} \f$      | 40       | °C                  | Mortality   | LadybirdConstantClass::p_cfg_LadybirdExtremeTempMax            | LADYBIRD_EXTREME_MAX                    |
| Min temperature imago                                   | \-       | °C                             | Mortality   | LadybirdConstantClass::Ladybird_SCP                            | \-                                      |
| Egg daily background mortality chance                   | 0        | \-                             | Mortality   | LadybirdConstantClass::p_cfg_LadybirdDailyEggMortality         | LADYBIRD_DAILY_EGG_MORT                 |
| Larva daily background mortality chance                 | 0.00001  | \-                             | Mortality   | LadybirdConstantClass::p_cfg_LadybirdDailyLarvaeMortality      | LADYBIRD_DAILY_LARVAE_MORT              |
| Pupa daily background mortality chance                  | 0.00001  | \-                             | Mortality   | LadybirdConstantClass::p_cfg_LadybirdDailyPupaeMortality       | LADYBIRD_DAILY_PUPAE_MORT               |
| Adult daily background mortality chance                 | 0.00001  | \-                             | Mortality   | LadybirdConstantClass::p_cfg_LadybirdDailyAdultMortality       | LADYBIRD_DAILY_ADULT_MORT               |
| Ladybird speed, \f$ v_L \f$                             | 1.5      | m/s                            | Movement    | LadybirdConstantClass::LadybirdFlyingSpeed                     | \-                                      |
| Boundary layer factor, \f$ a \f$                        | 0.075    | \-                             | Movement    | LadybirdConstantClass::LadybirdBoundaryLayerFactor             | \-                                      |
| Adult Sensing distance, \f$ l_\text{sensing} \f$        | 150      | m                              | Movement    | LadybirdConstantClass::p_cfg_LadybirdAphidSensingDistance      | LADYBIRD_APHID_SENSING_DISTANCE         |
| Larva Sensing distance, \f$ l_\text{sensing} \f$        | 14       | m                              | Movement    | LadybirdConstantClass::p_cfg_LadybirdLarvaAphidSensingDistance | LADYBIRD_LARVA_APHID_SENSING_DISTANCE   |
| Adult Mean step size                                    | 15       | m                              | Movement    | (function of sensing distance)                                 | \-                                      |
| Adult Std of step size                                  | 5        | m                              | Movement    | (function of sensing distance)                                 | \-                                      |
| Larva Mean step size                                    | 1.4      | m                              | Movement    | (function of sensing distance)                                 | \-                                      |
| Larva Std of step size                                  | 0.46     | m                              | Movement    | (function of sensing distance)                                 | \-                                      |
| Minimum temperature for flying                          | 15       | °C                             | Movement    | LadybirdConstantClass::LadybirdFlyingThreshTemp                | \-                                      |
| Maximum wind speed for flying                           | 20       | m/s                            | Movement    | LadybirdConstantClass::LadybirdFlyingThreshWind                | \-                                      |
| Minimum temperature for walking                         | 4        | °C                             | Movement    | LadybirdConstantClass::LadybirdMovementTempThreshold           | \-                                      |
| Random walk chance when satiated                        | 0.1      | \-                             | Movement    | LadybirdConstantClass::p_cfg_LadybirdUnnecessaryMovementChance | \-                                      |
| \Aphid density threshold for settling                    | 2549     | cm<sup>2</sup>/aphid           | Movement    | LadybirdConstantClass::LadybirdAphidDensityToStartForaging     | \-                                      |
| The last day of aggregation, \f$ d_L \f$                | 293      | day of the year                | Hibernation | LadybirdConstantClass::p_cfg_LadybirdAggregationEnd            | LADYBIRD_AGGREGATION_END                |
| The first day of aggregation, \f$ d_F \f$               | 263      | day of the year                | Hibernation | LadybirdConstantClass::p_cfg_LadybirdAggregationStart          | LADYBIRD_AGGREGATION_START              |
| Emergence day length, \f$ t_E \f$                       | 755      | minutes                        | Hibernation | LadybirdConstantClass::LadybirdEmergingDayLength               | \-                                      |
| Daily chance of emerging after \f$ d_E \f$              | 1        | \-                             | Hibernation | LadybirdConstantClass::LadybirdEmergingChance                  | \-                                      |
| Minimum number not to hibernate after \f$ d_E \f$, \f$ N_\text{aphids, high} \f$                                                                                                                                                                                                                                                                                                                | 1000     | aphids/m<sup>2</sup>           | Hibernation | LadybirdConstantClass::AphidsToStayAwake                       | \-                                      |
| Minimum number of aphids to hibernate immediately after \f$ d_E \f$, \f$ N_\text{aphids, low} \f$                                                                                                                                                                                                                                                                                                                | 10       | aphids/m<sup>2</sup>           | Hibernation | LadybirdConstantClass::AphidsToHibernate                       | \-                                      |
| Chance to hibernate when \f$ N_\text{aphids, high} > N_\text{aphids} > N_\text{aphids, low}\f$, \f$ p_\text{hib} \f$                                                                                                                                                                                                                                                                                                                | 0.1      | \-                             | Hibernation | LadybirdConstantClass::p_cfg_LadybirdHibernationChance         | LADYBIRD_HIBERNATION_CHANCE             |
| Maximum number of beetles in hibernacula, \f$ N_\text{bettles, max} \f$| 20 | \-                    | Hibernation | LadybirdConstantClass::LadybirdMaxClumpSize                    | \-                                      |
| Long-range window, \f$ W_\text{LRM} \f$                 | 32       | steps                          | Foraging    | #FLIGHT_MEMORY_SIZE                                            | \-                                      |
| Maximum number of long range flights in running window, \f$N_\text{LRM} \f$ | 14 | \-               | Foraging    | LadybirdConstantClass::p_cfg_LadybirdMaxLongRangeAttempts      | LADYBIRD_MAX_LONG_RANGE_ATTEMPTS        |
| Short movement threshold, \f$ N_\text{SM} \f$           | 8        | moves                          | Foraging    | LadybirdConstantClass::p_cfg_LadybirdMaxShortRangeAttempts     | LADYBIRD_MAX_SHORT_RANGE_ATTEMPTS       |
| Linear appetite factor, \f$ f_\text{stage} \f$          | \ref table7_ladybird "Table 7" | \-       | Foraging    | LadybirdConstantClass::LadybirdAppetiteFactor                  |                                         |
| List of aphid species to use                            | 25       | lists                          | General     | LadybirdConstantClass::p_cfg_LadybirdAphidSpecies              | LADYBIRD_APHID_SPECIES                  |
</tr>
</table>

### Outputs
ALMaSS is a flexible simulation environment that is capable of providing different types of outputs when needed. Outside the calibration mode this model is configured to provide basic outputs common to the other models, namely the probe file, that allows to simulate the sampling procedure and returns the number of individuals in question at specific predefined locations or in general across the whole landscape. In addition this model provides the so called AOR probe, the probe that combines the abundance and occupancy metrics (\ref ref_hoye_2012 "Høye et al., 2012") at the given date of the year.

### State variables
In addition to the standard state variables (such as spatial location or alive/dead status) that are shared among all of the ALMaSS objects the objects belonging to the ladybird classes have some distinct state variables of special importance that are provided below.

#### General
-    Beetle_Base::CurrentBState the current state of the agent, can be one of:   tobs_Initiation=0, tobs_EDeveloping,  tobs_Hatching,  tobs_EDying, tobs_LDeveloping,  tobs_Pupating, tobs_LDying, tobs_PDeveloping,  tobs_Emerging, tobs_PDying,  tobs_Foraging, tobs_Aggregating,  tobs_Hibernating, tobs_Dispersing,  tobs_ADying, tobs_Destroy
-    Ladybird_Base::m_DateMade the day of the simulation the current agent was created (e.g. for pupa the day of pupating)
-	Ladybird_Base::eatenToday the number of aphids eaten today
-	Ladybird_Base::m_AphidsAppetite the number of aphids that the ladybird has to eat today
-	Ladybird_Base::m_MoveCounter number of short range moves made today
-	Ladybird_Base::m_TotalEggsToLay total number of eggs to be laid in a lifetime as acrued up to now
####	Larvae
-	Beetle_Larvae::m_AgeDegrees the day degrees accumulated by larva
-	Beetle_Larvae::m_LarvalStage the instar of this larva
####	Pupae
-	Beetle_Pupae::m_AgeDegrees the day degrees accumulated by pupa
####	Adult
-	Beetle_Adult::m_EggCounter the number of eggs produced
-	Beetle_Adult::m_CanReproduce the reproductive ability
-	Ladybird_Adult::m_EmergenceDay the day of the year when this individual emerged
-	Ladybird_Adult::IsHibernating a flag turned on if the beetle is hibernating
-	Ladybird_Adult::hasOverwintered a flag turned on if the beetle has overwintered
-	Ladybird_Adult::DailyEggs the number eggs that the individual can lay today
-	Ladybird_Adult::m_ADayDeg the accumulated day degrees
-	Ladybird_Adult::m_UnsuccessfulAphidSearch the variable holding the number of unsuccesful searches for aphids in the last #FLIGHT_MEMORY_SIZE days
-	Ladybird_Adult::m_LongFlights the variable holding the number of long-range flights in the last #FLIGHT_MEMORY_SIZE days

### Scales
The time-step of the model is one day. That means there is no possibility to schedule some actions, events or behaviours later or earlier on the same day. That means there is no limit on the number of steps each agent can perform during the day. The spatial resolution of ladybird model is 1x1m. One should note,  however, that the aphid model has a lower spatial resolution and because in its movement ladybirds rely on the aphid densities some of the ladybird movement could in fact be less fine.

There is no upper limit on the size of the landscape ALMaSS can use, but normally we use landscapes between 10x10km and 30x30km.

Weather patterns depend on the provided data, if the weather file is shorter than the length of the simulation, the weather recording is re-run.

## Discussion of implementation
Since the aphid models can have a bigger cell size than the ladybird model, individual ladybird agents have access to only a part of the cell population, proportional to ladybird_cell_size/aphid_cell_size. To reduce the computational time, the ladybird model does not save and does not resample the aphid population. This means that theoretically the aphid population of the whole aphid-size cell can leak through one ladybird-size cell. In other words a ladybird can consume the whole population of the aphid cell without moving in the landscape.

The cannibalism implementation was initially designed to be performed in such a way that the less developed agents (the first instar of the larvae) perform cannibalism steps first and they are followed by the second instars that cannot be cannibalised by the first group. This way of scheduling should lead to a situation where naturally there could be no cannibalisation of the same agent twice. This implementation, however, did not prove to be vital for several reasons. First, there was no way to predict the number of actual steps that the agent can perform in one timestep. And, second, if one schedules the behaviour of the population so that *all* the first instars finish *all* their  behaviours before the second instars start their behaviours, this won't yield the realistic population dynamics. Threfore the decision was made, to implement instead the counter of pending cannibalism operations. This counter is checked together with the check that all the agents finished their behaviours. If the counter is not equal to zero, an additional loop of the step iterations is triggered, which manages the cannibalisations (for more details see \ref cannibalism and the relevant source code).

## Calibration
The calibration output of the model is available in a [separate document](Doxygen_Pics/Ladybird_calibration.pdf).

## References
\anchor ref_bianchi_2003 Bianchi, F. J. J. A., & Van der Werf, W. (2003). The Effect of the Area and Configuration of Hibernation Sites on the Control of Aphids by Coccinella septempunctata (Coleoptera: Coccinellidae) in Agricultural Landscapes: A Simulation Study. Environmental Entomology, 32(6), 1290–1304. https://doi.org/10.1603/0046-225x-32.6.1290

\anchor ref_chuhutin_2023 Chuhutin, A., Frydyszczak, D., Ziółkowska, E., and Topping, C. J. (2023). Seven-spotted ladybird Formal Model. <em>In preparation for Food and Ecological Systems Modelling Journal</em>

\anchor ref_elliott_2000 Elliott, N. C., Kieckhefer, R. W., & Beck, D. A. (2000). Adult coccinellid activity and predation on aphids in spring cereals. Biological Control, 17(3), 218–226.

\anchor ref_hodek_2012 Hodek, I., van Emden, H. F., & Honěk, A. (2012). Ecology and behaviour of the ladybird beetles (Coccinellidae) (I. Hodek, H. F. van Emden, & A. Honěk, Eds.). Blackwell Publishing Ltd.

\anchor ref_honek_1980 Honek, A. (1980). Population density of aphids at the time of settling and ovariole maturation in Coccinella septempunctata. Entomophaga, 25(4), 427–430.

\anchor ref_honek_2008 Honek, A., Dixon, A. F. G., & Martinkova, Z. (2008). Body size, reproductive allocation, and maximum reproductive rate of two species of aphidophagous Coccinellidae exploiting the same resource. Entomologia Experimentalis et Applicata, 127(1), 1–9. https://doi.org/10.1111/j.1570-7458.2007.00663.x

\anchor ref_honek_1990 Honěk, A., & Kocourek, F. (1990). Temperature and development time in insects: A general relationship between thermal constants. Zoologische Jahrbücher, Abteilung Für Systematik, Ökologie Und Geographie Der Tiere, 117(4), 401–439.

\anchor ref_honek_2007 Honěk, A., Martinková, Z., & Pekár, S. (2007). Aggregation characteristics of three species of coccinellidae (Coleoptera) at hibernation sites. European Journal of Entomology, 104(1), 51–56. https://doi.org/10.14411/eje.2007.008

\anchor ref_hoye_2012 Høye, T. T., Skov, F., & Topping, C. J. (2012). Interpreting outputs of agent-based models using abundance–occupancy relationships. Ecological Indicators, 20, 221–227.

\anchor ref_jalali_2009 Jalali, M. A., Tirry, L., & De Clercq, P. (2009). Effects of food and temperature on development, fecundity and life-table parameters of Adalia bipunctata (Coleoptera: Coccinellidae). Journal of Applied Entomology, 133(8), 615–625. https://doi.org/10.1111/j.1439-0418.2009.01408.x

\anchor ref_kalushkov_2004 Kalushkov, P., & Hodek, I. (2004). The effects of thirteen species of aphids on some life history parameters of the ladybird Coccinella septempunctata. BioControl, 49(1), 21–32. https://doi.org/10.1023/B:BICO.0000009385.90333.b4

\anchor ref_lanzoni_2004 Lanzoni, A., Accinelli, G., Bazzocchi, G. G., & Burgio, G. (2004). Biological traits and life table of the exotic Harmonia axyridis compared with Hippodamia variegata, and Adalia bipunctata (Col., Coccinellidae). Journal of Applied Entomology, 128(4), 298–306. https://doi.org/10.1111/j.1439-0418.2004.00847.x

\anchor ref_stroustrup_1995 Stroustrup, B. (1995). Why C++ is not just an object-oriented programming language. Addendum to the Proceedings of the 10th Annual Conference on Object-Oriented Programming Systems, Languages, and Applications (Addendum), 1–13.

\anchor ref_topping_2022 Topping, C. J. (2022). The Animal \Landscape and Man Simulation System (ALMaSS): A history, design, and philosophy. Research Ideas and Outcomes, 8, e89919.

\anchor ref_topping_2003 Topping, C. J., Hansen, T. S., Jensen, T. S., Jepsen, J. U., Nikolajsen, F., & Odderskær, P. (2003). ALMaSS, an agent-based model for animals in temperate European landscapes. Ecological Modelling, 167(1), 65–82. https://doi.org/10.1016/S0304-3800(03)00173-X

\anchor ref_toppingetal_2022 Topping, C. J., Marcussen, L. K., Thomsen, P., & Chetcuti, J. (2022). The Formal Model article format: Justifying modelling intent and a critical review of data foundations through publication. Food and Ecological Systems Modelling Journal, 3, e91024.

\anchor ref_topping_2023 Topping, C. J., Chetcuti J., Ziółkowska E., and Marcussen, L. K. Model Implementation Documentation with doxygen (MIDox). Submitted to: Food and Ecological Systems Modelling Journal, 2023.

\anchor ref_xia_1999 Xia, J. Y., Van Der Werf, W., & Rabbinge, R. (1999). Temperature and prey density on bionomics of Coccinella septempunctata (Coleoptera: Coccinellidae) feeding on Aphis gossypii (Homoptera: Aphididae) on cotton. Environmental Entomology, 28(2), 307–314. https://doi.org/10.1093/ee/28.2.307

\anchor ref_ziolkowska_2023 Ziółkowska, E., Chuhutin, A., and Topping, C. J. (2023). Calibrating seven-spotted ladybird Formal Model. <em>In preparation for Food and Ecological Systems Modelling Journal</em>
