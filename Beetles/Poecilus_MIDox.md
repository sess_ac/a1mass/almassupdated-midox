Poecilus MIDox                        {#Poecilus_page}
=========
<h1> MIDox for an ALMaSS _Poecilus cupreus_ complex agent-based model </h1>
Chuhutin, A.<sup>*,1</sup>, Poulsen, T.<sup>1</sup>, Groom, G.<sup>1</sup>  and Topping, C.J.<sup>1</sup>

1.	Department of Ecoscience, Aarhus University, C. F. Møllers Allé 8, 8000 Aarhus C, Denmark

*Corresponding author: Andrey Chuhutin, andrey@ecos.au.dk

## Introduction
This document follows the Model Implementation Documentation with Doxygen (MIDox) format (\ref ref_topping_2023 "Topping et al., 2023"), and represents the next step in designing, documenting and calibrating complex agent-based ecological models after the “Formal Model” format document (\ref ref_toppingetal_2022 "Topping et al., 2022"). Here we describe the implementation of the Poecilus formal model (\ref ref_sowa_2023 "Sowa et al., 2023").

The currently documented implementation has been calibrated using pattern-based calibration that was set up to replicate the basic ecological patterns of the population of _P. cupreus_. In particular, the phenological curves were used to fit the parameters of development, reproduction and mortality. The data on total egg production, oviposition period length and change in these parameters as a function of temperature are used to fit the reproduction parameters. A survivorship curve is used to fit the mortality.

This model was created within the ALMaSS (Animal \Landscape and Man Simulation System) framework (\ref ref_topping_2022 "Topping et al., 2022", \ref ref_topping_2003 "Topping et al., 2003") and therefore by design has an ability to connect with the other animal models that share the same framework (aphids as a non-essential food source, other polyphagous insect predators as potential competitors).

Designed in C++ using object oriented paradigm this model is by purpose made to be generic and easily expandable. If new behaviours of the _P. cupreus_ or new interconnection between the agents and the overall model will be discovered (e.g. in previously not modelled habitats) they could be easily introduced as a part of the model.

The division of the code into classes and methods was based on the overall model functionality and does not necessarily have a biological meaning.

## Aims and Purposes
The purpose of this document is both to document the model source code, for readability and future maintenance, but also to record the decisions made when the formal model in its general algorithmic form (\ref ref_sowa_2023 "Sowa et al., 2023") was translated into code.

The group behaviour of _P. cupreus_ is intricately connected to the landscape parameters, populations of other species inhabiting the same landscape and weather. In an effort to build an agent-based model one can only choose some of the multitude of behaviours for implementation. The behaviours that were chosen to be implemented in detail in our model are those that either:
- According to the literature and the experts we interviewed provide an important driver of beetles abundance or survival on the population level. In this way, for example, due to a generalist nature of _P. cupreus_ we intentionally left out the beetles energy balance, but at the same time implemented a cannibalism behaviour.
- Being an integral part of essential processes that are well sampled and documented in the literature and therefore provide a tool for the calibration of the model as a whole. (E.g. the way the oviposition period is defined and is managed through the state variables was chosen to allow the tracking of oviposition period length versus temperature and comparison with the field and laboratory data).

The purpose of this model is to be used both for complex scenarios landscape-based risk assessment together with a set of underlying prey models and as a standalone model (in the ALMaSS environment).

## Model in Brief
The model aims to represent a population of _P. cupreus_ on a spatially-restricted landscape. While the model is running, agents (that represent various stages of beetle development) are laid as eggs, develop, forage, move, reproduce, overwinter and die. During each one of these activities the agents interact with the landscape (through temporally-dynamic weather, crop rotations, farming events and vegetation growing models), the other species (the prey they are consuming) and each other (cannibalism).

The life-cycle of _P. cupreus_ includes four main stages: eggs, larvae, pupae and adults (females), while each one of them is implemented using a specific class (Poecilus_Egg_List, Poecilus_Larvae, Poecilus_Pupae and Poecilus_Adult, see Figure 1). The larva stage is composed of 3 instars, all of which utilize the same class Poecilus_Larvae. Our model is only implementing adult females, while adult males are ignored (the reasoning for that decision is provided by \ref ref_sowa_2023 "Sowa et al."

In addition to classes that represent various developmental stages three supplementary classes exist. The class Poecilus_Base includes the methods and variables which are used by more than one developmental stage (e.g. method Poecilus_Base::PoecilusFertilizerMortality() is used both by Poecilus_Larvae and Poecilus_Adult to estimate the mortality due to the application of fertilizers). The class Poecilus_Population_Manager is a class that includes variables and implements methods that are utilised on the level of beetle population (e.g. Poecilus_Population_Manager::decAphids() is called by both Poecilus_Larvae and Poecilus_Adult to remove aphids from the concurrently running aphid models). The class PoecilusConstantClass includes all the constant variables that are defined in the start of the simulation, with its instance included as a member variable in Poecilus_Population_Manager and used by the rest of the poecilus classes.

One important additional data structure was created inside Poecilus_Population_Manager for implementation purposes. Poecilus_Population_Manager::m_PoecilusDensityMap is a three-dimensional array (implemented using [Blitz++ library](https://github.com/blitzpp/blitz)) that keeps track of the objects of specific type in the landscape. Each time, when the agent moves, dies or is created, Poecilus_Population_Manager::m_PoecilusDensityMap is updated. The variable type used in the array defines the maximum number of objects of each type so that if we use unsigned char the maximum number of objects is

\f[N_\mathrm{max}=255\cdot \mathrm{Landscape\_size}_x \cdot \mathrm{Landscape\_size}_y\f]

which for a landscape of 10x10 km equals \f$ 255\cdot10^8 \f$ . In practice the number is much lower, since not all parts of the landscape can be used by the agents (beetle cannot travel through locations occupied by water or urban areas).
In the [next chapter](#scheduling) we will describe how the different integral parts of the model call each other and are controlled by overall ALMaSS simulation. In [Implementation](#implementation) we will provide a detailed account on the implementation of each one of the main behaviours, describing the data types and algorithms employed.

\image html PoecilusFig1.png "Figure 1. \a P. \a cupreus model classes overview. The inheritance is denoted with an arrow directed from inherited towards a base class."

## Scheduling  {#scheduling}

### Generic Scheduling Methods
ALMaSS uses some basic step functions in a systematic way for all the agent-based models. In the beetle models these methods are called from Beetle_Population_Manager::RunStepMethods() in the normal execution flow. Here we will mention those of them which are used in the poecilus model, while providing a short usage description.

#### DoFirst()
This method is the first one to run before each day step, it runs on the level of the population (i.e. it does not iterate between the beetle agents). It provides an initial preparation for the run of the agents.
In the poecilus model the method calculates temperature-related constants, in this way all such calculations are performed only once a day with the results stored in the objects of Beetle_Population_Manager and Poecilus_Population_Manager, which are accessible to all the living agents. In this way maximum daily distances for both adults and larvae are calculated (using Poecilus_Population_Manager::setMeanDistance() and Poecilus_Population_Manager::setDistanceStd() and Poecilus_Population_Manager::setMaxDistanceRNG()). In addition to that it updates the day degrees for Larva and Adult development (by calling a Beetle_Population_Manager::DoFirst()), temperature-related egg production (Poecilus_Population_Manager::setDailyEggProduction()).

Specifically for the eggs, it is used to run Beetle_Egg_List::BeginStep() for all the members of the Poecilus_Egg_List performing the daily mortality check and updating the total egg number.

#### BeginStep()
This method is executed per agent. It performs the mortality check (background mortality, temperature-related mortality and fertilizers use) for most of the stages. In addition, it resets the daily egg counter Poecilus_Adult::m_eggsLaid, resets the movement counter (Poecilus_Adult::resetTodaysHoppingDistance()) and estimates the daily appetite for aphids. It also sets the maximum daily distance by drawing a random value from the updated probability density (Poecilus_Adult::setMaxDailyDistance()).

#### DoBefore()
The method is executed once for all the objects. The poecilus model uses the 'virtual' version of the method implemented in the base class, the primary function of which is to run Beetle_Egg_List::Step() for each list instance.

#### Step()
This method is the main place where the _P. cupreus_ behaviour is implemented. This method is responsible for moving between the developmental stages, going around the landscape, initiating the foraging, reproductive and cannibalistic behaviours. The execution of the step ends when there is no agent left with the StepDone property false.

#### DoAfter()
This method is not used in this model.

#### EndStep()
This method is used to manage the mortality due to pesticide application and cannibalism in larvae and density-dependent mortality in Poecilus_Adult.

#### DoLast()
This method controls the output caused by the pesticide mortality and enables the catastrophe event (Beetle_Population_Manager::Catastrophe2()).

### Developmental states and behavioural stages

#### Developmental stages
The poecilus model has four basic developmental stages:
- Eggs: Implemented through Poecilus_Egg_List, where each object holds the list of the locations where the eggs have been laid on the same day. The eggs that are laid on the same day experience the same temperatures and weather and therefore develop in the same pace. When this and all later developmental stages experience the environment, depending on the temperature and abundance of prey it accumulates the number of eggs that it will lay in its lifetime.
- Larva: Implemented in Poecilus_Larvae. Each object is created when the egg hatches, and only moves in the same 1m<sup>2</sup>. The larva develops through 3 instar sub-stages, where the development is a function of temperature. Cannibalism is common among the larva that share the same location. Older larvae are capable of cannibalising the younger individuals but not vice versa. If the agent survives all the developmental stages it is removed and at the same location a pupa is created.
- Pupa: Implemented in Poecilus_Pupae. An immobile object that represents pupa and an immobile pre-pupa. The object develops as a function of temperature.
- Female adult: Implemented in Poecilus_Adult. A fully developed individual, that is capable of moving by walking and overwintering and foraging the aphids. The movement of _P. cupreus_ is driven by the temperature, humidity and presence of shade (see \ref movement). Once maturity is reached it lays eggs. The chances of dying depend on the age, such that no individual lives more than 4 years.

#### Behavioural states

##### Eggs
The objects of Poecilus_Egg_List have two development stages:
- Beetle_Egg_List::st_Develop() is in charge of the object development based on the daily temperatures
- Poecilus_Egg_List::st_Hatch() is triggered in the end of the development and turns fully developed eggs into larvae.

##### Larvae
The objects of Poecilus_Larvae implement two behavioural methods:
- Beetle_Larvae::st_Develop() is a major behavioural state of the _P. cupreus_ larvae. It controls the development and subsequent movement between the instar stages. In addition, it is in charge of cannibalisation. The state triggers Poecilus_Larvae::st_Pupate().
- Poecilus_Larvae::st_Pupate() is the method which is responsible for turning the fully-developed larvae into pupae.

##### Pupae
The objects of Poecilus_Pupae use two developmental states:
- Beetle_Pupae::st_Develop() controls the development as a function of mean daily temperatures. Once the development is finished the emerging is triggered.
- Poecilus_Pupae::st_Emerge() turns the pupae into adults.

##### Adults
The adults (Poecilus_Adult) have several developmental stages they are going through during their lifetime:
- Poecilus_Adult::st_Forage() is the method that controls the foraging state. During that stage the beetle forages on aphids, walks around in search of shade or warmth, and when the conditions for maturation and reproduction are met lays eggs. This state can trigger either natural death (when all eggs have been laid) or (if the weather conditions and aphid abundance are suitable) winter hibernation.
- Poecilus_Adult::st_Hibernate() is a method that controls the beetle’s behaviour while hibernating. During the hibernation the beetle remains stationary, while gaining an increased ability to withstand low temperatures. The end of hibernation is triggered by the day length. At the end of it, the beetles that survived the winter move to the state of foraging.

#### Start of the simulation
At the start of the simulation the adult _P. cupreus_ are randomly assigned to the types of landscape elements defined in BeetleConstantClass::BeetleStartHabitats. Since the simulation is normally starting on the 1st of January the beetles are initially in the hibernation state.

\image html PoecilusFig2.png "Figure 2. State transition diagram for \a P. \a cupreus." width=800px

## Implementation {#implementation}

### Development
The parameters of the day-degree model for the basic stages of egg, larva, pupa and adult have been calculated based on a combination of studies. The details can be found in (\ref ref_sowa_2023 "Sowa et al., 2023") and the resulting values are provided in \ref Table1 "Table 1" (stored in BeetleConstantClass::LDevelConst2).

The objects accumulate day degrees only if the ambient temperature is above a value of \f$T_0=9\f$. For the egg, larval and pupal stages, the transition to the next developmental stage occurs when the accumulated degree-days attribute reaches the predefined threshold \f$T_\mathrm{sum}\f$ in \ref Table1 "Table 1" (stored in BeetleConstantClass::LDevelConst2, BeetleConstantClass::EggDevelConst2 BeetleConstantClass::PupaDevelConst2).

To calculate \f$T_\mathrm{sum}\f$ for the progressing through the instars the ratio between the relative lengths of the instars was assumed to be constant (as 1:1:2) and therefore the day-degrees for the instars could be calculated from the value in \ref Table1 "Table 1". The values for the instars are available in \ref Table2 "Table 2" (stored in BeetleConstantClass::LDevelConst2)

<table>
<td colspan=1>
     <caption id="Table1">Table 1: Parameters for temperature-driven development</caption>
</td>
<tr>
| P. cupreus life stage | Transition threshold in accumulated degree-days (\f$T_\mathrm{sum}\f$) |
|-----------------------|------------------------------------------------------------------------|
| Egg                   | 88.7                                                                   |
| Larva                 | 381.86                                                                 |
| Pupa                  | 74.81                                                                  |
</tr>
</table>

<table>
<td colspan=1>
     <caption id="Table2">Table 2: Parameters for temperature-driven development of larvae</caption>
</td>
<tr>
| Instar 1 | Instar 2 | Instar 3 |
|----------|----------|----------|
| 95.46    | 190.93   | 381.86   |
</tr>
</table>

### Oviposition
For the modelled _P. cupreus_ beetles the start of the reproduction behaviour is triggered by the increased daylight length (spring) after a prolonged period of cold temperatures (winter). Therefore, the chance of reaching maturation starting the reproduction is constant for a given calendar date. Implementation-wise it means that starting from the spring equinox (maturation start date, day 79, PoecilusConstantClass::PoecilusStartMaturationDay) the modelled beetles get 1/30 chance (daily maturation chance, PoecilusConstantClass::p_cfg_PoecilusDailyMaturationChance) of reaching maturation. Once the beetle is mature the start of oviposition depends on the length of the preoviposition period which is a function of temperature. The start of the oviposition is based on the literature data (for details see the formal model (\ref ref_sowa_2023 "Sowa et al., 2023")) and in particular, it is approximated by the polynomial fit \f$(r^2=0.98)\f$ of the data between the maximum and minimum data points (implemented in Poecilus_Adult::getDailyEggFormationProgress()):
		\f[ 0.229\cdot x^2-11.14 \cdot x +140.28\f]
The progress towards maturity is controlled by method Poecilus_Adult::UpdateEggFormationProgress() which is called by Poecilus_Adult::st_Forage() and updates the state variable Poecilus_Adult::m_EggFormationCounter.

Once the reproduction has started the length of the oviposition period and its variance are also temperature dependent. The progress towards the completion of the oviposition period is calculated daily and is composed of a temperature-dependent deterministic part and some stochastic parts which are dependent on the literature values where both types of data are approximated by step functions (estimated on the population level with Poecilus_Population_Manager::setDailyOvipositionFactor() and Poecilus_Population_Manager::setDailyOvipositionRandomFactor() called by Poecilus_Adult::getDailyOvipositionProgress() and Poecilus_Adult::getDailyOvipositionRandomProgress()). The controling state variable is Poecilus_Adult::m_OvipositionPeriodCounter which is updated by Poecilus_Adult::UpdateOvipositionProgress. The daily fecundity is also estimated daily for all the beetles, based on laboratory data and approximated by the polynomials the daily variance in egg production \f$(r^2=0.85)\f$:
		\f[
    y = -0.006\cdot x^2 + 0.27 \cdot x - 2.32
    \f]
and the mean in daily egg production \f$(r^2=0.99)\f$:
		\f[
    y = - 0.02753 \cdot x^2 + 1.72 \cdot x - 16.7
    \f]
The calculations of the daily egg factors are performed once per day by a call of Poecilus_Population_Manager::DoFirst(). The methods used are Poecilus_Population_Manager::setDailyRandomEggFactor() and Poecilus_Population_Manager::setDailyEggProduction(). The result is accessed by adults via Poecilus_Adult::getTodaysEggProduction().

The eggs are placed along the route of the beetles movement. After each successful movement the beetle lays a share of total daily eggs proportional to the movement length divided by a total daily movement:
     $$n_{\mathrm{eggs\ in\ clutch}} = \frac{l_{\mathrm{recent\ move\ length}}}{L_{\mathrm{total\ daily\ distance}}\left(t\right)}\cdot N_{\mathrm{eggs\ daily}}\left(t\right)$$
this ensures the equal distribution of eggs along the movement trajectory. This behaviour is implemented in Poecilus_Adult::st_Forage().

### Movement {#movement}
There is no directional movement to or from hibernation places implemented in the poecilus model. Even though some authors mention aggregation, there is no consensus as to increased densities of beetles in specific habitats during the overwintering period (see Formal model (\ref ref_sowa_2023 "Sowa et al., 2023")).

The extent of short-range movement of _P. cupreus_ is driven by the temperature, \f$T\f$. Since, according to \ref ref_chiverton_1988 "Chiverton", the number of shoots linearly correlate with the temperature \f$(r^2=0.98)\f$, we scale the movement range \f$d\f$, associating the maximum number of shoots with maximum average daily distance of 30m, and minimum number of shoots with minimum average distance of 3m:
$$d = 1.8\cdot T - 15 \ \ \ \forall T>8.3$$
The calculation is performed daily in Poecilus_Population_Manager::setMeanDistance() called by Poecilus_Population_Manager::DoFirst() and stores the value in Poecilus_Population_Manager::m_DailyMeanDistance.

The resulting distance that each beetle walks a particular day is a combination of the previously-discussed deterministic part (accessed through Poecilus_Population_Manager::SupplyMeanDistance()) and a stochastic part (defined in PoecilusConstantClass::p_cfg_PoecilusDistanceStd and accessed through Poecilus_Population_Manager::SupplyDistanceStd()). These two are combined in Poecilus_Adult::setMaxDailyDistance() which is called daily for each agent at Poecilus_Adult::BeginStep().

The mechanism of the movement is as follows. At each movement attempt the beetle validates that the maximum movement distance is not reached. If it is reached, the movement finishes for today. The control of the movement is performed in Poecilus_Adult::st_Forage().

If there is still some movement left to do today, the direction of the movement is decided upon out of 8 cardinal directions (implemented in Beetle_Base::Move()). Since the movement of _P. cupreus_ is driven by light intensity, humidity and temperature, the directions that provide more cover (simulated via leaf area index) are chosen in the warm days, and the directions where the vegetation has lower leaf area index are chosen on the cold days. The warmer the days are, the higher the preference of the shadow is:
$$p\left(\hat{n}\right)=\frac{f\left(L\left(\hat{n},l_{\mathrm{sensing}}\right) -L\left(\hat{n},0\right),\left(T-T_\mathrm{mean}\right)\right)}{ \sum_{\hat{n} \in S_{\mathrm{directions}}} f\left(L\left(\hat{n},l_{\mathrm{sensing}}\right) - L\left(\hat{n},0\right),\left(T-T_\mathrm{mean}\right)\right) }$$
where \f$S_{\mathrm{directions}}\f$ is a set of 8 cardinal directions, \f$l_{\mathrm{sensing}}\f$ is the sensing distance and, therefore, \f$L\left(\hat{n},l_{\mathrm{sensing}}\right) -L\left(\hat{n},0\right)\f$ is a difference in leaf area index between the remote place and the current location. \f$T\f$ is temperature and \f$T_\mathrm{mean}\f$ is a threshold temperature for which the effect of shadow becomes attractive instead of repulsive. We assume \f$T_\mathrm{mean}=18\mathrm{C}^\circ\f$ (PoecilusConstantClass::p_cfg_PoecilusTempShadowThresh) as the mean preferred temperature (\ref ref_thiele_1977 "Thiele, 1977"). The function of weighting between the leaf area index difference and the temperature \f$f\f$:
\f[
f(a,b) =
     \begin{cases}
       \ 0.01 &\quad\text{if }a =0\\
       \ a\cdot b+ 0.01 &\quad\text{otherwise}\\
     \end{cases}
\f]
The method Poecilus_Base::MovementFactor() (overriding virtual Beetle_Base::MovementFactor()) implements the function \f$f\f$ for \f$f\left(T-T_\mathrm{mean},\Delta_\mathrm{Leaf\ Area\ Index}\right)\f$. After the movement direction is chosen the beetle moves one step (\f$l_{\text{step}}=1 \text{m}\f$) (PoecilusConstantClass::PoecilusShortRangeDistance) in this direction and waits for its next turn to move. The beetle only moves at temperatures above \f$18\mathrm{C}^\circ\f$ (PoecilusConstantClass::PoecilusMinMovementTemp).

The larvae of _P. cupreus_ are stationary from the implementation perspective.

### Overwintering
The check for dormancy is performed by Poecilus_Adult::CheckDormancy() which is triggered by Poecilus_Adult::st_Forage(). The dormancy is triggered by daylight changes, so on the 1st of September (PoecilusConstantClass::PoecilusDormancyStartDate) the beetles start to get dormant (chance of hibernation is \f$p=0.03\f$, stored in  PoecilusConstantClass::p_cfg_PoecilusStandardDormancyChance). If the daily minimum temperature falls below 5°C (PoecilusConstantClass::dormancy_threshold) the beetles are allocated an hibernation chance of \f$p=0.03\f$ (PoecilusConstantClass::p_cfg_PoecilusStandardDormancyChance) even before the 1st of September (but not earlier than the 1st of June, PoecilusConstantClass::PoecilusDormancyStartDateEarly). Each day that the minimum temperature is below 5°C (PoecilusConstantClass::dormancy_threshold) increases the chances of hibernation by 20% (defined through PoecilusConstantClass::dormancy_multiplier). The mentioned calculations are performed only once a day on the population level and is implemented in Poecilus_Population_Manager::setDormancyChance() and triggered by Poecilus_Population_Manager::DoFirst().

As the days get longer (spring) the end of hibernation can be triggered. Normally this happens from the 1st of March (PoecilusConstantClass::PoecilusDormancyExitDate), but only from the day when the maximum temperature reaches above 8°C (PoecilusConstantClass:dormancy_exit_threshold), from which day the chance of exiting hibernation is \f$p=0.03\f$ (PoecilusConstantClass::p_cfg_PoecilusStandardDormancyExitChance). For each day where the maximum temperature is above 8°C, the chances of exiting hibernation increases by 20% (PoecilusConstantClass::dormancy_exit_multiplier). The check is performed by Poecilus_Population_Manager::SupplyDormancyExitChance() The beetles who did not exit the hibernation by the 1st of June (PoecilusConstantClass::PoecilusLastDayToExitHibernation) die.

### Mortality
The daily background mortality factors (in \ref Table3 "Table 3") are being generated based on the standard total stage mortality of _P. cupreus_ (due to e.g. predation or undefined environmental factors). The egg mortality is based on the average mortality for different foods weighted by their popularity (see the Formal model \ref ref_sowa_2023 "Sowa et al., 2023"). The daily mortality is generated, taking into account the average length of the stage (at 12°C):
<table>
<td colspan=1>
     <caption id="Table3">Table 3: Mortality of pre-imago developmental stages of \a P. \a cupreus</caption>
</td>
<tr>
| \a P. \a cupreus life stage | Stage length (days) | Mortality | Daily  mortality |
|-----------------------------|---------------------|-----------|------------------|
| Egg                         | 27                  | 0.57      | 0.021            |
| Larva                       | 81                  | 0.43      | 0.005            |
| Pupa                        | 19                  | 0.23      | 0.012            |
</tr>
</table>

Since the low temperature that has been chosen corresponds to a long stage length, the overall mortality numbers in \ref Table3 "Table 3" may be underestimated. However these numbers do not include mortality due to cannibalism (overpopulation) or agricultural practices.

These values are stored in PoecilusConstantClass (e.g. PoecilusConstantClass::DailyPupaeMort) and are used in Poecilus_Pupae::getDailyMortalityRate().

Yearly mortality of the imago is decided upon based on the yearly mortality data. The literature data (provided in detail in the Formal model \ref ref_sowa_2023 "Sowa et al., 2023") show total mortalities being 71.2% in the first season, from the first to the second reproductive period (year) 41%, and from the second to the third 8.5%. Under the assumption that a season has a length of 365 days (except for the first, which is assumed to have approximately 238 imago days) we get the following daily mortalities \ref Table4 "Table 4".

<table>
<td colspan=1>
     <caption id="Table4">Table 4: Daily mortality of imago in different seasons</caption>
</td>
<tr>
| Season | Daily Mortality |
|--------|-----------------|
| 1st    | 0.003           |
| 2nd    | 0.0011          |
| 3rd    | 0.00023         |
</tr>
</table>

The values are stored in PoecilusConstantClass::DailyAdultMortality1, PoecilusConstantClass::DailyAdultMortality2 and PoecilusConstantClass::DailyAdultMortality3 and are used in Poecilus_Adult::getDailyMortalityRate().
All beetles that survived through 3 seasons die at the start of their third hibernation.

According to the literature a temperature of -10 or below, with no snow cover will cause a 95% mortality per day (PoecilusConstantClass::PoecilusExtremeTempMort) for hibernating individuals. Non-hibernating individuals experience the same mortality at each day with temperature below freezing.

Exposure to some fertilizers causes high levels of mortality in _P. cupreus_ larvae and pupae. In our model we implement the overall mortality at the moment of the application that was calculated out of the daily mortalities post-application.
The mortality chances are summarised in \ref Table5 "Table 5".

<table>
<td colspan=1>
     <caption id="Table5">Table 5: Mortality in larva and pupa due to fertiliser salt application</caption>
</td>
<tr>
| Salt       | One time mortality upon application | Use in farming                                               |
|------------|-------------------------------------|--------------------------------------------------------------|
| 7.86 CuCl2 | 0.12                                | Rarely used                                                  |
| MgSO4      | 0.25                                | Epsom salt                                                   |
| CaCl2      | 0.8                                 | Fertiliser                                                   |
| CuSO4      | 0.9                                 | Fertiliser for orchards in DK, rarely herbicide or fungicide |
| 11.8 CuCl2 | 1                                   | Rarely used                                                  |
</tr>
</table>
For any other inorganic fertiliser the lowest found salt mortality of 0.12 was used. The mortality due to fertalizer application is implemented in Poecilus_Base::PoecilusFertilizerMortality() which is called from Poecilus_Base::OnFarmEvent(). The list of the mortalities corresponding to \ref Table5 "Table 5" is stored in a TTreatmentvsMortalityList accessible through PoecilusConstantClass::TreatmentMortalities.

### Nutrition
_P. cupreus_ beetles are polyphagous with a varying ratio of food sources depending on the season, satiation status and other factors. To provide the availability and nutritional values of all the sources in any given landscape demands both field data and subsequent development of formal models for a multitude of species. Until that is done, the model does not include a nutrition or foraging model or energy budget for individuals.

The only part of nutrition that is implemented in the model is aphid foraging and one-way damage to the aphid population. The number of aphids consumed by _P. cupreus_ is based on the literature (see details in \ref ref_sowa_2023 "Sowa et al., 2023") and corrected for the absence of male beetles in the model. The resulting values of mean and standard deviation (\ref Table6 "Table 6") are used to generate a stochastic number of aphids consumed by an individual. If the model temperature is below or over the tested range (5 - 20)°C, the maximum or minimum values are used correspondingly.

<table>
<td colspan=1>
     <caption id="Table6">Table 6:  Daily aphid consumption vs temperature by \a P. \a cupreus</caption>
</td>
<tr>
| Temperature  | Mean Daily aphid consumption  | Std  |
|--------------|-------------------------------|------|
| 5            | 23.6                          | 2.55 |
| 10           | 29.2                          | 5    |
| 15           | 32.6                          | 3.42 |
| 20           | 37                            | 2.76 |
</tr>
</table>

The values from \ref Table6 "Table 6" are stored in PoecilusConstantClass::Aphid_consumption_means and PoecilusConstantClass::Aphid_consumption_stds. Consumption parameters are estimated once a day depending on the temperature by Poecilus_Population_Manager::SetConsumeAphids where the relevant mean and standard deviation are used to set the random distribution Poecilus_Population_Manager::AphidConsumptionToday. Each agent draws a value from that distribution once a day. The agents consume aphids in Poecilus_Adult::st_Forage().

### Cannibalism
In our model all larva instars show cannibalistic behaviour. Since we do not model food intake, we have no need to track a particular individual that cannibalised another individual, but only to establish the ‘victims’ of cannibalisation. This procedure is performed once a day for each location in the landscape where poecilus larva are present.

The probability of a cannibalisation events among LX (for X=1,2,3) when only LX are present depends on the number of individuals in that square meter. The probability of cannibalism among the same age larvae \f$ p_{Li}\left(n_{Li}\right)\forall i=1..3 \f$ is provided in \ref Table7 "Table 7". The values are estimated from literature (see the Formal Model \ref ref_sowa_2023 "Sowa et al., 2023") and we work around the case of a single larva in the box.

<table>
<td colspan=1>
     <caption id="Table7">Table 7:  The daily chances of cannibalizing another larva of the same stage </caption>
</td>
<tr>
| Instar No\Density per sq m | 43    | 85    | 128    | 171    | 214    | 427   |
|----------------------------|-------|-------|--------|--------|--------|-------|
| L1                         | 0.035 | 0.035 | 0.0475 | 0.08   | 0.079  | 0.075 |
| L2                         | 0.035 | 0.035 | 0.0475 | 0.0714 | 0.0714 | 0.111 |
| L3                         | 0.035 | 0.035 | 0.0714 | 0.0714 | 0.079  | 0.089 |
</tr>
</table>

The \ref Table7 "Table 7" is implemented as a number of variables: the thresholds in density are PoecilusConstantClass::CannibalismLevelThresholds, the cannibalisation chances for L1: PoecilusConstantClass::Cannibalism_L1, for L2: PoecilusConstantClass::Cannibalism_L2 and for L3: PoecilusConstantClass::Cannibalism_L3.

While the third instar (L3) can eat the second instar (L2) but not vice versa, the chances to become a victim of cannibalisation are the highest among L1. If the total number of larva in the cell is \f$ N\f$ and number of the individuals in stage \f$i\f$ is \f$n_i\f$ then the number of individuals killed in cannibalism in each of the stages \f$\tilde{N}_{Li} \f$ is:
\f[
\begin{array}{c}  
\tilde{N}_{L1} = \left(   p_{L1}\left(    N \right)  +      p_{L2}\left(    N \right)   +  p_{L3}\left(    N \right)                \right)\cdot n_{L1}
\\
\tilde{N}_{L2} = \left(      p_{L2}\left(    N \right)   +  p_{L3}\left(    N \right)                \right)\cdot n_{L2}
\\  
\tilde{N}_{L3} =  p_{L3}\left(    N \right)                \cdot n_{L3}
\end{array}
\f]
The cannibalism behaviour only manifests itself if there is more than \f$ N_\text{cannibalism,larva}\f$ larvae per m<sup>2</sup> per day. This is a tunable parameter PoecilusConstantClass::CannibalismThreshold that is fitted during the calibration. The initial suggested value is 6 (the number of beetles in the same box when the first cannibalism event started to get registered in (\ref ref_heessen_1980 "Heeseen et al., 1980"). If the density of larvae is smaller, then the background mortality is just applied.
The described behaviour is implemented in Poecilus_Larvae::Do_Cannibalism().

Despite the fact that we suspect that eggs can be also cannibalized, we found no literature data to validate that and therefore the model does not include egg cannibalism.
For adults no cannibalism of the larvae is assumed. However the density-dependent mortality is applied to the imago stage if more than 4 adults stay within 1 m<sup>2</sup>.

## Interconnections
In common with other ALMaSS models the model is connected to Landscape, Farm and Crop classes.

In addition, much of the functionality of the poesilus model is implemented in the Beetle classes (Beetle_Base, Beetle_Egg_List, Beetle_Larvae, Beetle_Pupae, Beetle_Adult, see Figure 1).

The poecilus model uses ALMaSS random library (probability_distribution) for random numbers generation.

This model is one-way connected to the aphid models, as the aphids are a possible prey of _P. cupreus_. However, presence or absence of aphids doe not affect this model's behaviour.

The connection between the the beetles and the aphids is established through the calls of the Poecilus_Population_Manager methods:  Poecilus_Population_Manager::decAphids() removes the aphids from the underlying models. The aphid species that could be used as prey by the beetles are defined through the configuration, using PoecilusConstantList::p_cfg_PoecilusAphidSpecies (configuration variable: POECILUS_APHID_SPECIES).

## I/O, Variables & Scales

### Inputs
Our model uses a landscape file set (that consists of lsb: a binary landscape file, farmref: a list of farm references connecting farms to the farmtypes and polyref: a list of polygon references connecting landscape polygons to type of landscape element, farm reference, soiltype, etc). In addition to that a standard input is a number of rotation files, that describe the common way of rotating the fields of a each farmtype.
The weather input to the model is provided in a separate file that includes the following variables:
- mean temperature
- mean wind
- precipitation
- minimum temperature
- maximum temperature
- soil temperature
- snow depth
- humidity
- soil temperature at twilight
- solar radiation

for each day of the simulation. The weather file is based on the real data in the given location and is used for the simulation in a circular manner.

Our model uses a configuration file that includes all the previously mentioned input files, output files as parameters along with the specific ecological parameters of the model of _P. cupreus_. These parameters have their predefined default values, but could be altered for each simulation. The list of the most essential parameters, their default values, name of the constants they are stored in and the variable name to be used in the config file is provided in \ref Table8 "Table 8".

<table>
<td colspan=1>
     <caption id="Table8">Table 8: Model parameters.</caption>
</td>
<tr>
| Parameter name  (including the symbol)                                   | Value       | Units           | Domain        | Constant name                                                    | Variable name in the configuration file  |
|--------------------------------------------------------------------------|-------------|-----------------|---------------|------------------------------------------------------------------|------------------------------------------|
| Maturation start day                                                     | 79          | Day of the year | Oviposition   | PoecilusConstantClass::PoecilusStartMaturationDay                |                                          |
| Daily maturation chance                                                  | \f$\frac{1}{30}\f$ | -        | Oviposition   | PoecilusConstantClass::p_cfg_PoecilusDailyMaturationChance       | POECILUS_DAILY_MATURATION_CHANCE         |
| Preoviposition length factor a                                           | 0.229       | -               | Oviposition   | PoecilusConstantClass::PoecilusPreOviFactorA                     |                                          |
| Preoviposition length factor b                                           | -11.4       | -               | Oviposition   | PoecilusConstantClass::PoecilusPreOviFactorB                     |                                          |
| Preoviposition length factor c                                           | 140.28      | -               | Oviposition   | PoecilusConstantClass::PoecilusPreOviFactorC                     |                                          |
| Daily fecundity variance factor a                                        | -0.006      | -               | Oviposition   | PoecilusConstantClass::PoecilusDailyRandomEggFactorA             |                                          |
| Daily fecundity variance factor b                                        | 0.27        | -               | Oviposition   | PoecilusConstantClass::PoecilusDailyRandomEggFactorB             |                                          |
| Daily fecundity variance factor c                                        | -2.32       | -               | Oviposition   | PoecilusConstantClass::PoecilusDailyRandomEggFactorC             |                                          |
| Daily fecundity mean factor a                                            | -0.02753    | -               | Oviposition   | PoecilusConstantClass::PoecilusDailyEggFactorA                   |                                          |
| Daily fecundity mean factor b                                            | 1.72        | -               | Oviposition   | PoecilusConstantClass::PoecilusDailyEggFactorB                   |                                          |
| Daily fecundity mean factor c                                            | -16.7       | -               | Oviposition   | PoecilusConstantClass::PoecilusDailyEggFactorC                   |                                          |
| Step length                                                              | 1           | m               | Movement      | PoecilusConstantClass::PoecilusShortRangeDistance                |                                          |
| Minimum temeperature for movement                                        | 8.3         | °C              | Movement      | PoecilusConstantClass::PoecilusMinMovementTemp                   |                                          |
| Variance in daily movement distance                                      | 5           | -               | Movement      | PoecilusConstantClass::p_cfg_PoecilusDistanceStd                 | POECILUS_DISTANCE_STD                    |
| Threshold temperature \f$T_\mathrm{mean}\f$                              | 18          | °C              | Movement      | PoecilusConstantClass::p_cfg_PoecilusTempShadowThresh            | POECILUS_TEMP_SHADOW_THRESH              |
| First day with a non-zero chance to hibernate                            | 244         | Day of the year | Overwintering | PoecilusConstantClass::PoecilusDormancyStartDate                 |                                          |
| Daily hibernation chance                                                 | 0.03        | -               | Overwintering | PoecilusConstantClass::p_cfg_PoecilusStandardDormancyChance      | POECILUS_STANDARD_DORMANCY_CHANCE        |
| Min temperature hibernation  threshold                                   | 5           | °C              | Overwintering | PoecilusConstantClass::dormancy_threshold                        |                                          |
| Increased chance of hibernation due to temperature drop                  | 0.2         | -               | Overwintering | PoecilusConstantClass::dormancy_multiplier                       |                                          |
| First day with non-zero chance to stop hibernation                       | 60          | Day of the year | Overwintering | PoecilusConstantClass::PoecilusDormancyExitDate                  |                                          |
| Daily increase in chance of exiting the dormancy due to high temperature | 0.2         | -               | Overwintering | PoecilusConstantClass::dormancy_exit_multiplier                  |                                          |
| Max temperature exiting threshold                                        | 8           | °C              | Overwintering | PoecilusConstantClass::dormancy_exit_threshold                   |                                          |
| Earliest daye to start hibernating if it is cold                         | 152         | Day of the year | Overwintering | PoecilusConstantClass::PoecilusDormancyStartDateEarly            |                                          |
| Daily chance to exit hibernation                                         | 0.3         | -               | Overwintering | PoecilusConstantClass::p_cfg_PoecilusStandardDormancyExitChance  | POECILUS_STANDARD_DORMANCY_EXIT_CHANCE   |
| Imago daily mortality 1st season                                         | 0.003       | -               | Mortality     | PoecilusConstantClass::DailyAdultMortality1                      |                                          |
| Imago daily mortality 2nd season                                         | 0.003       | -               | Mortality     | PoecilusConstantClass::DailyAdultMortality2                      |                                          |
| Imago daily mortality 3rd season                                         | 0.003       | -               | Mortality     | PoecilusConstantClass::DailyAdultMortality3                      |                                          |
| Egg Daily mortality                                                      | 0.021       | -               | Mortality     | PoecilusConstantClass::DailyEggMort                              |                                          |
| Larva Daily mortality                                                    | 0.005       | -               | Mortality     | PoecilusConstantClass::DailyLarvaeMort                           |                                          |
| Pupa Daily mortality                                                     | 0.012       | -               | Mortality     | PoecilusConstantClass::DailyPupaeMort                            |                                          |
| Extreme temperature threshold (non-hibernated individuals)               | 0           | °C              | Mortality     | PoecilusConstantClass::PoecilusNonHibernatedExtremeTempThreshold |                                          |
| Extreme temperature threshold (hibernated individuals)                   | -10         | °C              | Mortality     | PoecilusConstantClass::PoecilusHibernatedExtremeTempThreshold    |                                          |
| Mortality at extreme temperatures                                        | 0.95        | -               | Mortality     | PoecilusConstantClass::PoecilusExtremeTempMort                   |                                          |
| Mean consumption vs temperature                                          | \ref Table6 "Table 6" |       | Nutrition     | PoecilusConstantClass::Aphid_consumption_means                   |                                          |
| Variance of consumption vs temperature                                   | \ref Table6 "Table 6" |       | Nutrition     | PoecilusConstantClass::Aphid_consumption_stds                    |                                          |
</tr>
</table>

### Outputs
ALMaSS is a flexible simulation environment that is capable of providing different types of outputs when needed. Outside the calibration mode this model is configured to provide basic outputs common to the other models, namely the probe file, that allows to simulate the sampling procedure and returns the number of individuals in question at specific predefined locations or in general across the whole landscape. In addition this model provides the so called AOR probe, the probe that combines the abundance and occupancy metrics (\ref ref_hoye_2012 "Høye et al., 2012") at the given date of the year.

### State variables
In addition to a standard state variables (such as spatial location and alive or dead status) that are shared among all of the ALMaSS objects, the class objects of this model have some distinct state variables of special importance that are provided below.

#### General
- Beetle_Base::CurrentBState the current state of the agent, can be one of: tobs_Initiation=0, tobs_EDeveloping,  tobs_Hatching,  tobs_EDying, tobs_LDeveloping,  tobs_Pupating, tobs_LDying, tobs_PDeveloping,  tobs_Emerging, tobs_PDying,  tobs_Foraging, tobs_Aggregating,  tobs_Hibernating, tobs_Dispersing,  tobs_ADying, tobs_Destroy

#### Larvae
- Beetle_Larvae::m_AgeDegrees the day degrees accumulated by larva
- Beetle_Larvae::m_LarvalStage the instar of this larva

#### Pupae
- Beetle_Pupae::m_AgeDegrees the day degrees accumulated by pupa

#### Adults
- Poecilus_Adult::m_TodaysHoppingDistance the distance the beetle has already travelled today
- Poecilus_Adult::m_MaxDailyDistance the maximum distance the beetle is allowed to travel today
- Poecilus_Adult::m_eggsLaid the number of eggs laid today
- Poecilus_Adult::m_IsMature the boolean reflecting whether the individual is mature
- Poecilus_Adult::m_EggFormationCounter the counter towards the end of the egg-formation satge that follows the maturation and preceeds oviposition
- Poecilus_Adult::m_OvipositionPeriodCounter the counter towards the end of the oviposition stage
- Poecilus_Adult::m_OvipositionFinished true if oviposition is over
- Beetle_Adult::m_CanReproduce reproductive ability

#### Population manager
- Poecilus_Population_Manager::m_DailyRandomEggFactor the stochastic part of the daily egg production
- Poecilus_Population_Manager::m_DailyEggs the deterministic part of the egg production
- Poecilus_Population_Manager::m_DailyOvipositionFactor the daily progress towards the end of oviposition
- Poecilus_Population_Manager::m_DailyOvipositionRandomFactor the stochastic part of the daily progress towards the end of oviposition
- Poecilus_Population_Manager::m_DailyMeanDistance daily mean distance of walking
- Poecilus_Population_Manager::m_DailyDistanceStd daily standard variation of walking distance
- Poecilus_Population_Manager::m_DormancyChance the chance to start overwintering
- Poecilus_Population_Manager::m_DormancyExitChance the chance to exit overwintering
- Poecilus_Population_Manager::m_AphidSpeciesNum number of aphid species to be consumed
- Poecilus_Population_Manager::AphidConsumptionToday random number generated which is set up to generate the aphid consumption for today
- Poecilus_Population_Manager::PoecilusMovementToday random number generated which is set to generate the movement distance of the adult today

### Scales
The time-step of the model is one day. That means there is no possibility to schedule some actions, events or behaviours later or earlier on the same day. That means there is no limit on the number of steps each agent has performed during the day. The spatial resolution of the model is 1m x 1m.

There is no upper limit on the size of the landscape ALMaSS can use, but normally we use landscapes between 10x10km and 30x30 km.

Weather patterns depend on the provided data, if the weather file is shorter than the length of the simulation, the weather recording is re-run.

## Discussion of implementation

The implementation of the poecilus model included among other things a high degree of overarching generalisation of other models. For this purpose, the source code of already existing models (e.g. model of _Bembidion lampros_ and model of _Coccinella septempunctata_) had to be refactored in such a way that the modules of interest could be exposed, packed in accessible form and moved to a parent class (Beetle_base or Beetle_Adult). This solution made it possible to keep the codebase of poecilus model proper relatively small, since it only includes the methods and variables that are used by poecilus model. Such refactoring could be observed e.g. in the movement mechanism of _P. cupreus_ which is re-using the short-range movement algorithm initially developed for the implementation of the ladybird model (up to the driver of movement which is unique and defined in Poecilus_Base::MovementFactor) or in day-degree based larva development which is almost exactly replicates the development of larvae of _B. lampros_ (up to the values of thresholds).

## Calibration
The calibration output of the model is available in a [separate document](Doxygen_Pics/Poecilus_calibration.pdf).

## References

\anchor ref_chiverton_1988 Chiverton, A. P. Searching behaviour and cereal aphid consumption by Bembidion lampros and Pterostichus cupreus, in relation to temperature and prey density. Entomologia experimentalis et applicata, 47(2):173–182, 1988. ISBN: 0013-8703 Publisher: Wiley Online Library.

\anchor ref_heessen_1980 Heessen H. J. L., and Brunsting, A. M. H.. Mortality of larvae of Pterostichus oblongopunctatus (Fabricius) col., Carabidae and Philonthus decorus (Gravenhorst)(col., Staphylinidae). Netherlands Journal of Zoology, 31(4):729–745, 1980. ISBN: 1568-542X Publisher: Brill.

\anchor ref_hoye_2012 Høye, T. T., Skov, F., & Topping, C. J. (2012). Interpreting outputs of agent-based models using abundance–occupancy relationships. Ecological Indicators, 20, 221–227.

\anchor ref_thiele_1977 Thiele, H. U. Carabid beetles in their environments. A study on habitat selection by adaptation in physiology and behaviour. Carabid beetles in their environments. A study on habitat selection by adaptation in physiology and behaviour., 1977. ISBN: 3540083065 Publisher: Springer-Verlag.

\anchor ref_topping_2022 Topping, C. J. (2022). The Animal \Landscape and Man Simulation System (ALMaSS): A history, design, and philosophy. Research Ideas and Outcomes, 8, e89919.

\anchor ref_topping_2003 Topping, C. J., Hansen, T. S., Jensen, T. S., Jepsen, J. U., Nikolajsen, F., & Odderskær, P. (2003). ALMaSS, an agent-based model for animals in temperate European landscapes. Ecological Modelling, 167(1), 65–82. https://doi.org/10.1016/S0304-3800(03)00173-X

\anchor ref_toppingetal_2022 Topping, C. J., Marcussen, L. K., Thomsen, P., & Chetcuti, J. (2022). The Formal Model article format: Justifying modelling intent and a critical review of data foundations through publication. Food and Ecological Systems Modelling Journal, 3, e91024.

\anchor ref_topping_2023 Topping, C. J., Chetcuti J., Ziółkowska E., and Marcussen, L. K. Model Implementation Documentation with doxygen (MIDox). Submitted to: Food and Ecological Systems Modelling Journal, 2023.

\anchor ref_sowa_2023 Sowa G., Chuhutin A., Ziółkowska E., and Topping, C. J. Model development for ground beetle Poecilus cupreus (Coleoptera: Carabidae), a general spring breading predator. Submitted to: Food and Ecological Systems Modelling Journal, 1(1), 2023.
