//
// Version of Feb, 12 2020
// Last modified by Andrey Chuhutin on 12/02/2020
/*
*******************************************************************************************************
Copyright (c) 2020, Andrey Chuhutin, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************************************
*/
//----------------------------------------------------------------------
// Created by Andrey Chuhutin on 12/02/2020.
//
// The following two are used in Population manager.h, maybe we should consider change it there


#include <memory>
#include <vector>

#include <cstring>
#include <iostream>
#include <random>
//#include <fstream>
using namespace std;
#include "../Landscape/ls.h" // included in h file
//#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PositionMap.h"
#include "Beetle_BaseClasses.h"



#define __CATASTROPHE2 // Kills the beetles not in field following the catastrophe control configs (values 0-99 only)
//#define BEMMOVETEST // For testing Bembidion daily movement distances
extern Landscape* g_landscape_p;
extern std::default_random_engine g_std_rand_engine;
extern double g_SpeedyDivides[2001];

extern CfgBool cfg_RipleysOutput_used;

extern CfgBool cfg_AOROutput_used;

extern CfgBool cfg_ReallyBigOutput_used;

extern CfgInt cfg_CatastropheEventStartYear;
extern CfgInt cfg_pm_eventday;
extern CfgBool cfg_RipleysOutputMonthly_used;
extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;

extern CfgInt cfg_AOROutput_interval;
extern CfgInt cfg_AOROutput_day;
extern CfgInt cfg_AOROutputFirstYear;
extern CfgInt cfg_RipleysOutput_interval;
extern CfgInt cfg_RipleysOutput_day;
extern CfgInt cfg_RipleysOutputFirstYear;
extern CfgInt cfg_ReallyBigOutput_interval;
extern CfgInt cfg_ReallyBigOutput_day1;
extern CfgInt cfg_ReallyBigOutput_day2;
extern CfgInt cfg_ReallyBigOutput_day3;
extern CfgInt cfg_ReallyBigOutput_day4;
extern CfgInt cfg_ReallyBigOutputFirstYear;


//---------------------------------------------------------------------------
// Definition of static class members (add the others here)
//---------------------------------------------------------------------------
double Beetle_Larvae::m_BeetleLarvalPPPElimRate=0;
double Beetle_Larvae::m_BeetleLPPPThreshold=0;
double Beetle_Larvae::m_BeetleLPPPEffectProb=0;
double Beetle_Larvae::m_BeetleLPPPEffectProbDecay=0;
double Beetle_Pupae::m_BeetlePupalPPPElimRate=0;
double Beetle_Pupae::m_BeetlePPPPThreshold=0;
double Beetle_Pupae::m_BeetlePPPPEffectProb=0;
double Beetle_Pupae::m_BeetlePPPPEffectProbDecay=0;
double Beetle_Adult::m_BeetleAdultPPPElimRate=0;
double Beetle_Adult::m_BeetleAPPPThreshold=0;
double Beetle_Adult::m_BeetleAPPPEffectProb=0;
double Beetle_Adult::m_BeetleAPPPEffectProbDecay=0;
param_Point Beetle_Adult::pPoint;
param_List15 Beetle_Adult::pList;
//probability_distribution Beetle_Adult::m_move_distribution = probability_distribution( p_cfg_MoveProbType->value(),  p_cfg_MoveProbArgs->value());
// DEBUG and testing



//---------------------------------------------------------------------------
//                          BEETLE_BASE
//---------------------------------------------------------------------------

Beetle_Base::Beetle_Base( int x, int y, Landscape* L,
                                Beetle_Population_Manager* BPM )
        :TAnimal(x, y, L)  {
    Init(BPM);
}



//---------------------------------------------------------------------------
void Beetle_Base::ReInit(int x, int y, Landscape* L, Beetle_Population_Manager * BPM) {
    ReinitialiseObject( x, y, L );
    Init(BPM);
}
//---------------------------------------------------------------------------
void Beetle_Base::Init(Beetle_Population_Manager * BPM) {
    m_OurPopulation = BPM;
    CurrentBState = TTypesOfBeetleState::tobs_Initiation;
}

double Beetle_Base::getExtremeTempMortality() const{
    return m_OurPopulation->beetleconstantslist->p_cfg_ExtremeTempMort->value();
}

double Beetle_Base::getExtremeTempMin() const{
    return m_OurPopulation->beetleconstantslist->p_cfg_ExtremeTempMin->value();
}

double Beetle_Base::getExtremeTempMax() const{
    return m_OurPopulation->beetleconstantslist->p_cfg_ExtremeTempMax->value();
}
bool Beetle_Base::ExtremeTempIsOn() const{
    return m_OurPopulation->beetleconstantslist->p_cfg_ExtremeTempIsOn->value();
}

double Beetle_Base::getMaxTemp(){
    return m_OurLandscape->SupplySoilTemp();
}
double Beetle_Base::getMinTemp(){
    return m_OurLandscape->SupplySoilTemp();
}
/** \brief Chance to cannibalise a form if it is in the same 1sq m**/
double Beetle_Base::getCannibalisationChance() const{
    return 1;
}
//---------------------------------------------------------------------------
/**
 * ! \brief OnFarmEvent is essentially similar for all classes apart from Eggs
 * therefore it will be inherited by Larva, Adult and Pupa
 * Egg will have a separate implementation
 *
 * **/
bool Beetle_Base::OnFarmEvent(FarmToDo event){
    TTypesOfBeetleState DyingState = getDyingState();
    switch(event)
     {
        //todo: check that the switch is correctly gone through
     case  sleep_all_day:
      break;
     case  autumn_plough:
     case  stubble_plough: // assumed to be the same as for autumn plough
     case  stubble_cultivator_heavy: // assumed to be the same as for autumn plough
     case  heavy_cultivator_aggregate: // assumed to be the same as for autumn plough
     case  autumn_harrow:
     case  preseeding_cultivator: // assumed to be the same as for harrow
     case  preseeding_cultivator_sow: // assumed to be the same as for harrow
     case  autumn_roll:
     case  autumn_sow:
     case  winter_plough:
     case  deep_ploughing:
     case  spring_plough:
     case  spring_harrow:
     case  shallow_harrow:
     case  autumn_or_spring_plough:
     case  strigling_sow:
     case  bed_forming:
     case  bulb_harvest:
         if (g_rand_uni()<getSoilCultivationMortality()) CurrentBState = DyingState;
         break;
     case  harvest:
     case green_harvest:
         if (g_rand_uni() < getHarvestMortality()) CurrentBState = DyingState;
         break;
     case strigling:
     case strigling_hill:
         if (g_rand_uni() < getStriglingMortality()) CurrentBState = DyingState;
         break;
     case  insecticide_treat:
         if (g_rand_uni() < getInsecticideApplication()) CurrentBState = DyingState;
         break;
     case trial_toxiccontrol:
         if (random(1000) < getPesticideTrialToxic()) CurrentBState = DyingState;
         break;
     case syninsecticide_treat:
     case trial_insecticidetreat:
         if (random(1000) < getPesticideTrialTreatment()) CurrentBState = DyingState;
         break;
// Below here are all the no mortality operations
     case  spring_roll:
     case  spring_sow:
     case  spring_sow_with_ferti:
    case  fp_npks:
    case  fp_npk:
    case  fp_pk:
    case  fp_sk:
	case  fp_k:
	case  fp_p:
    case  fp_liquidNH3:
    case  fp_slurry:
    case  fp_manganesesulphate:
    case  fp_ammoniumsulphate:
    case  fp_manure:
    case  fp_greenmanure:
    case  fp_sludge:
    case  fp_rsm:
    case  fp_calcium:
    case  fp_boron:
    case  fa_npks:
    case  fa_boron:
    case  fa_npk:
    case  fa_cu:
    case  fa_nk:
    case  harvestshoots:
    case manual_weeding:
    case  fa_pk:
	case  fa_p:
	case  fa_k:
    case  fa_sk:
    case  fa_slurry:
    case  fa_ammoniumsulphate:
    case  fa_manganesesulphate:
    case  fa_manure:
    case  fa_greenmanure:
    case  fa_sludge:
    case  fa_rsm:
    case  fa_calcium:
    case  herbicide_treat:
    case  growth_regulator:
    case  fungicide_treat:
	case  org_insecticide:
    case  org_herbicide:
    case  org_fungicide:
    case fp_pks:
    case fa_pks:
    case fp_n:
    case fa_n:
    ///
    case pruning:
    case shredding:
    case fiber_covering:
    case fiber_removal:
    case fp_nk:
    case fp_ns:
    case fp_nc:
    case autumn_sow_with_ferti:
    case harvest_bushfruit:
    case fp_cu:
    case  hilling_up:
    case  water:
    case  swathing:
    case flower_cutting:
    case straw_covering:
    case straw_removal:
    case  cattle_out:
    case  cattle_out_low:
    case  cut_to_hay:
    case  cut_to_silage:
    case  straw_chopping:
    case  hay_turning:
    case  hay_bailing:
    case  flammebehandling:
    case  stubble_harrowing:
    case  burn_straw_stubble:
    case burn_top: //AHA
    case mow:
    case cut_weeds:
    case pigs_out:
    case  molluscicide:
    case  row_cultivation:
    case trial_control:
    case product_treat:
    case glyphosate:
    case biocide:
        break;
     default:
       m_OurLandscape->Warn("Beetle Adult - Unknown Treatment in Daily Mortality:",std::to_string((int)event));
       exit(1);
     }
     // Must incorporate a test here in case the animal is dead - killing it twice
     // can be a bad idea
     if (CurrentBState==DyingState) return true;
     else return false;
}
void Beetle_Base::KillThis()
{
    CurrentBState=getDyingState();
}
//---------------------------------------------------------------------------
/**
Do the housekeeping necessary to die as an object in the system.\n
*/
void Beetle_Base::st_Die()
{
    CurrentBState=tobs_Destroy;
    m_CurrentStateNo=-1;
    m_StepDone=true;
#ifdef __LAMBDA_RECORD
    m_OurPopulation->LamdaDeath(m_Location_x, m_Location_y);
#endif
}
int Beetle_Base::getLarvalStagesNum () const{
    return m_OurPopulation->beetleconstantslist->BeetleLarvalStagesNum;
}
double Beetle_Base::getDailyMortalityRate() const{
    /**
     * \brief This is a virtual function that will be overriden by Larva, Egg, pupa and Adult class
     *
     * ***/
    return 0;
}
bool Beetle_Base::DailyMortality()
{
    /**
     * This is the function that would be used by Larva, Pupa nad Adult classes as is (Egg will be overloaded)
     *
     *
     * ***/
    if (g_rand_uni()<getDailyMortalityRate())
    {
        return true;
    }
    return false;
}
double Beetle_Base::getStriglingMortality() const {
    /**
 * \brief Method taht returns Strigling mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return m_OurPopulation->beetleconstantslist-> p_cfg_BeetleStriglingMort->value();
}
double Beetle_Base::getStriglingHillMortality() const {
    /**
 * \brief Method taht returns Strigling mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return getSoilCultivationMortality();
}
double Beetle_Base::getHarvestMortality() const {
    /**
 * \brief Method that returns harvest mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return m_OurPopulation->beetleconstantslist-> p_cfg_BeetleHarvestMort->value();
}
void Beetle_Base::CopyMyself(int a_beetle)
{
    std::unique_ptr <struct_Beetle> BS(new struct_Beetle);  //   instead of new struct_Beetle; using C++11
    BS->x   = m_Location_x;
    BS->y   = m_Location_y;
    BS->L   = m_OurLandscape;
    BS->BPM = m_OurPopulation;
    m_OurPopulation->CreateObjects(a_beetle,this,nullptr,std::move(BS),1);

}
/** \brief The function that calculates the cumulative probability for the movement direction
 * */
void Beetle_Base::getHopProbabilities(float (&probabilities)[8], int checking_distance) {
    double differences[8]{0};
    //int checking_distance=m_OurLadybirdPopulation->ladybirdconstantslist->p_cfg_LadybirdAphidSensingDistance->value();
    /*** small number that would be added to all the directions, so even if the aphid density is lower than in the starting point
     * there is still a small chance to hop to the direction
     * */
    if (m_Location_x==2659&&m_Location_y==6249){
        cout<<"DEBUG:"<<endl;
    }
    float delta = 0.01;
    float delta_div_by = delta;
    double delta_mult_by = delta * 8;
    double max = -1e8;
    double min = 1e8;
    double dif_sum = 0;
    int inside_directions = 8;
    for (int i = 0; i < inside_directions; i++) {
        int x = m_Location_x + (Vector_x[i]) * checking_distance;
        int y = m_Location_y + (Vector_y[i]) * checking_distance;
        if (x > (m_OurLandscape->SupplySimAreaWidth() - 1)) {
            x -= (m_OurLandscape->SupplySimAreaWidth());
        }
        if (y > (m_OurLandscape->SupplySimAreaHeight() - 1)) {
            y -= (m_OurLandscape->SupplySimAreaHeight());
        }
        if (x < 0) {
            x += (m_OurLandscape->SupplySimAreaWidth());
        }
        if (y < 0) {
            y += (m_OurLandscape->SupplySimAreaHeight());
        }
        //if (x>0 && y>0 && x<m_OurLandscape->SupplySimAreaWidth() && y<m_OurLandscape->SupplySimAreaHeight()){
        //differences[i]= m_OurLadybirdPopulation->getAphidsNumber(x, y) - m_OurLadybirdPopulation->getAphidsNumber(m_Location_x, m_Location_y);
        differences[i] = MovementFactor(x, y, m_Location_x, m_Location_y);
        if (differences[i] > max) {
            max = (double) differences[i];
        }
        if (differences[i] < min) {
            min = (double) differences[i];
        }
        dif_sum += (double) differences[i];
        //}
        //else{
        /* The following solution was good if we want them to avoid crossing the borders
         * However in ALMaSS in general the species "wrap" the landscape, so the check was re-implemented to comply with that
        // if we are outside let us assign a very low number, so the probability to hop there will be very low
        // anyway the situation that there will be hop there will be very low
        differences[i]=-200000;
        min=differences[i];
        dif_sum +=differences[i];
         */
        //}
    }
    // we will compensate for using the minimum number as a lower threshold
    dif_sum -= (double) inside_directions * min;
    // because we need a cumulative probability
    if (min == max || dif_sum <1e-3) {
        float acc = 0;
        for (int i = 0; i < inside_directions; i++) {
            probabilities[i] = 1.0f / (float) inside_directions + acc;
            acc = probabilities[i];
        }
    } else {
        float acc = 0;
        for (int i = 0; i < inside_directions; i++) {
            probabilities[i] =
                    (((double) (differences[i] - min) / (double) dif_sum) * (1 - delta_mult_by)) + delta_div_by + acc;
            acc = probabilities[i];
        }
    }
    if (probabilities[inside_directions - 1] < 1.0) {
        if (probabilities[inside_directions - 1] > 0.99){
            //cout << " probability is marginally incorrect, assuming the sum of 1\n";
            probabilities[inside_directions - 1] = 1.;
        }
        else{
            //cout << " probability is very incorrect:"<<probabilities[inside_directions - 1]<<endl;
        }

    }
}
// The function returns the array with cumulative probabilities to be hit, so the only thing needed is to check that
// i-1>0?prob[i-1]:0<x<prob[i] for each i
double Beetle_Base::MovementFactor(int x, int y, int x1, int y1){
    return 0.;
}
//-------------------------------------------------
const int Beetle_Base::getMaxSAttempts(){
    return 1;
}
double Beetle_Base::getShortRangeDistance(){
    return 1.;
}
int Beetle_Base::getMaxSensingDistance(){
    return 1.;
}
//--------------------------------------
/** \brief The function that draws the random number and checks in which interval in the probabilities vector are we
 * This would be the direction the Ladybird will take */
int Beetle_Base::ShortHopDirection(float (&probabilities) [8]){
    double number = g_rand_uni(); //between 0 and 1
    for (int i=0; i<8; i++){
        if (probabilities[i]>=number){
            return i;
        }
    }
    // random number max is 1, probabilities vector should have 1 at some place, if not, throw an error
    g_msg->Warn( WARN_BUG, " Beetle_Base::ShortHopDirection: ","Wrong probabilities array received");
    exit(-1);

}

//---------------------------------------------------------------------------
void Beetle_Base::Move(){
    int maxShortRangeAttempts=getMaxSAttempts();
    bool IsAllowed = false;
    int i = 0;
    float probabilities[8]{0};
    float same_probabilities[8]{0.125,0.250,0.375,0.5,0.625,0.75,0.875,1.0};
    int new_x, new_y;
    while (!IsAllowed && (i<maxShortRangeAttempts)){
        /** Hopping distance cannot be 0 . Or can it be?*/
        double hopping_distance= getShortRangeDistance();
        int hop_direction;
        /** If we are hibernating then the chance to hop is equal in all directions
         * During hibernation short range hopping occurs only if a spot is occupied and we cannot hibernate alone
         * In this case we will just randomly hop very close and try again */
        if (CurrentBState != tobs_Aggregating){
            /**Get the probabilities of hopping directions based on the aphids availability*/
            getHopProbabilities(probabilities, getMaxSensingDistance());
            hop_direction=ShortHopDirection(probabilities);

        }
        else{
            /** Use the array of equal probabilities */
            hop_direction=ShortHopDirection(same_probabilities);

        }

        /**Choose the direction and return it  */
        /** Check if the position is ok for moving in*/
        new_x = m_Location_x+(Vector_x[hop_direction])* (int) hopping_distance;
        new_y = m_Location_y+(Vector_y[hop_direction])* (int) hopping_distance;
        IsAllowed = IsLocationAllowed(new_x, new_y);
        // Wrapping around:
        if (new_x>(m_OurLandscape->SupplySimAreaWidth()-1)){
            new_x-=(m_OurLandscape->SupplySimAreaWidth());
        }
        if(new_y>(m_OurLandscape->SupplySimAreaHeight()-1)){
            new_y-=(m_OurLandscape->SupplySimAreaHeight());
        }
        if(new_x<0){
            new_x+=(m_OurLandscape->SupplySimAreaWidth());
        }
        if (new_y<0){
            new_y+=(m_OurLandscape->SupplySimAreaHeight());
        }
        /** If we are aggregating, we cannot exit the current poly*/
        if (CurrentBState == tobs_Aggregating) {
            bool isSamePoly = IsLocationSamePoly(new_x, new_y);
            IsAllowed = IsAllowed && isSamePoly;
        }
        i++;
    }
    if (IsAllowed){
        m_Location_x = new_x;
        m_Location_y = new_y;
    }

}
//---------------------------------------------------------------------------
bool Beetle_Base::IsLocationSamePoly(int x, int y){
    int polyind_old=m_OurLandscape->SupplyPolyRef(m_Location_x, m_Location_y);
    int polyind_new=m_OurLandscape->SupplyPolyRef(x, y);
    return polyind_old== polyind_new;
}
//---------------------------------------------------------------------------
//                           BEETLE_EGG
//---------------------------------------------------------------------------
/*! \brief Support function returning EggDevelConst2
 *         ..
 *  In this way we workaround the inability to overload the static parameters.
 *  One should note that in the current implementation we directly refer to the population manager
 *  to supply us with the parameter. However this may be CPU-costly. As an alternative the additional
 *  static variable in the descendant class can be introduced and the overloaded function
 *  could refer to it instead.
 *  This function may be overwritten by the descending classes to return another parameter
 */
double Beetle_Egg_List::getEggDevelConst2()
{
    return m_OurPopulation->beetleconstantslist->EggDevelConst2;
}
double Beetle_Egg_List::getSoilCultivationMortality() const {
    return m_OurPopulation->beetleconstantslist->p_cfg_Egg_SoilCultivationMortality->value();
}
double Beetle_Egg_List::getInsecticideApplication() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_Egg_InsecticideApplication->value();
}
double Beetle_Egg_List::getDailyEggMortality() const {
    return m_OurPopulation->beetleconstantslist->DailyEggMort;
}
double Beetle_Egg_List::getPesticideTrialTreatment() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_PesticideTrialEggTreatmentMort->value();
}
double Beetle_Egg_List::getPesticideTrialToxic() const {
    return m_OurPopulation->beetleconstantslist->PesticideTrialEggToxicMort;
}
TTypesOfBeetleState Beetle_Egg_List::getDyingState() const{
    return tobs_EDying;
}
double Beetle_Egg_List::getExtremeTempMortality() const{
    return m_OurPopulation->beetleconstantslist->p_cfg_EggExtremeTempMort->value();
}
/*! \brief Beetle egg list constructor: will use default constructor
 * here as well C++11 should be smart enough
 *         ..
 *
 *  Beetle egg list constructor function

Beetle_Egg_List::Beetle_Egg_List(int today, Beetle_Population_Manager * BPM,
                                        Landscape* L): Beetle_Base(0,0,L,BPM)
// All the beetles eggs have the same constructor
{
    m_DayMade=today;
}
*/
Beetle_Egg_List::Beetle_Egg_List(int today,Beetle_Population_Manager* BPM, Landscape* L):Beetle_Base(0,0,L,BPM)
{


    m_DayMade=today;
}
//---------------------------------------------------------------------------
TTypesOfBeetleState Beetle_Egg_List::st_Develop()
{
    if (m_OurPopulation->SupplyEDayDeg(m_DayMade)>getEggDevelConst2()) // Hatch
    {
      return tobs_Hatching; // go to hatch
    }
    else return tobs_EDeveloping; // carry on developing
}



// All the beetle eggs have basically the same states
// so the Begin step and Step functions are essentially the same for all of them
//---------------------------------------------------------------------------

void Beetle_Egg_List::BeginStep()
{
    DailyMortality(); // should be dealt with for each species separately
                        // based on the constants injected in the inherited class
}


//---------------------------------------------------------------------------

void Beetle_Egg_List::Step()
{
    if ( m_CurrentStateNo == -1||Cannibalisation() || m_StepDone) return;
    switch (CurrentBState)
    {
        case tobs_Initiation:    // Initial
            CurrentBState=tobs_EDeveloping;
            break;
        case tobs_EDeveloping: // Developing
        {
            CurrentBState=st_Develop();//   st_Develop() can return:
                                        //     |
                                        //     |-> tobs_Hatching
                                        //     |-> tobs_EDeveloping
            m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Develop() stepDone is:
                                                                //     |
                                                                //     |-> false
                                                                //     |-> true

        }
        break;

        case tobs_Hatching:{
            CurrentBState=st_Hatch();//   st_Hatch() can return:
                                    //     |
                                    //     |-> tobs_Initiation
            m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Hatch() stepDone is:
                                                            //     |-> true
        }
         break;
        default:
            m_OurLandscape->Warn("Beetle_Egg_List::Step()","Beetle Egg - Unknown State");
            g_msg->Warn(WARN_BUG, "State attempted was: ", int(CurrentBState));
            exit(1);
    }
}
//---------------------------------------------------------------------------

// Hatching is the same for all the beetles
TTypesOfBeetleState Beetle_Egg_List::st_Hatch()
{


    int size=(int)EggList.size();

    for (int i=0; i<size; i++)
    {
        std::unique_ptr <struct_Beetle> BS(new struct_Beetle);  //   instead of new struct_Beetle; using C++11
        BS->x   = EggList[i].m_x;
        BS->y   = EggList[i].m_y;
        BS->L   = m_OurLandscape;
        BS->BPM = m_OurPopulation;
        m_OurPopulation->CreateObjects(1, this, nullptr, std::move(BS), 1);
    }
    EggList.erase(EggList.begin(),EggList.end()); // empty the list

    return tobs_Initiation;
#ifdef __LAMBDA_RECORD
    m_OurPopulation->LamdaBirth(m_Location_x,m_Location_y,size);
#endif

}
//---------------------------------------------------------------------------

bool Beetle_Egg_List::DailyMortality()
{
    // Needs to go through all eggs in the list and test for background mortality
    // and while we are here for OnFarmEvent stuff
    auto mortality = getDailyEggMortality();
    auto mintemp = getMinTemp();
    auto maxtemp = getMaxTemp();
    auto temp = m_OurLandscape->SupplyTemp();
    if (((mintemp < (getExtremeTempMin())) || (maxtemp > getExtremeTempMax())) && ExtremeTempIsOn()) mortality += getExtremeTempMortality();
    mortality += TempRelatedMortality(temp, maxtemp, mintemp);
    int size = (int)EggList.size();
    for (int i = size - 1; i >= 0; i--)
    {
        if (g_rand_uni() < mortality)
        {
            // Remove the Egg it is dead
            //EggList.erase(EggList.begin()+i);
            ClearFromMap(EggList[i].m_x, EggList[i].m_y);
            EggList[i].m_x = -999; // Code for kill it
        }
        else
        {
            int res = 0;
            FarmToDo event;
            if ((event = (FarmToDo)m_OurLandscape->SupplyLastTreatment(m_Location_x,
                m_Location_y, &res)) != sleep_all_day)
            {
                switch (event)
                {
                case  sleep_all_day:
                    break;
                case  autumn_plough:
                case  stubble_plough:
                case  stubble_cultivator_heavy:
                case  heavy_cultivator_aggregate:
                case  autumn_harrow:
                case  preseeding_cultivator:
                case  preseeding_cultivator_sow:
                case  autumn_roll:
                case  autumn_sow:
                case  winter_plough:
                case  deep_ploughing:
                case  spring_plough:
                case  spring_harrow:
                case  shallow_harrow:
                    if (g_rand_uni() < getSoilCultivationMortality()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }

                    break;
                case  spring_roll:
                case  spring_sow:
                case  spring_sow_with_ferti:
                case  fp_npks:
                case  fp_npk:
                case  fp_pk:
                case  fp_sk:
                case  fp_p:
                case  fp_k:
                case  fp_liquidNH3:
                case  fp_slurry:
                case  fp_ammoniumsulphate:
                case  fp_manganesesulphate:
                case  fp_manure:
                case  fp_greenmanure:
                case  fp_sludge:
                case  fp_rsm:
                case  fp_calcium:
                case  fa_npks:
                case  fa_npk:
                case  fa_pk:
                case  fa_p:
                case  fa_k:
                case  fa_sk:
                case  fa_slurry:
                case  fa_ammoniumsulphate:
                case  fa_manganesesulphate:
                case  fa_manure:
                case  fa_greenmanure:
                case  fa_sludge:
                case  fa_rsm:
                case  fa_calcium:
                case  fa_boron:
                case  herbicide_treat:
                case  growth_regulator:
                case  fungicide_treat:
                case  org_insecticide:
                case  org_herbicide:
                case  org_fungicide:
                case  molluscicide:
                case  row_cultivation:
                case  strigling:
                case  hilling_up:
                case  water:
                case  swathing:
                case  harvest:
                case  cattle_out:
                case  cattle_out_low:
                case  cut_to_hay:
                case  cut_to_silage:
                case  straw_chopping:
                case  hay_turning:
                case  hay_bailing:
                case  burn_straw_stubble:
                case  stubble_harrowing:
                ///
                case pruning:
                case shredding:
                case green_harvest:
                case fiber_covering:
                case fiber_removal:
                case fp_boron:
                case fp_n:
                case fp_nk:
                case fp_ns:
                case fp_nc:
                case autumn_sow_with_ferti:
                case harvest_bushfruit:
                case fp_cu:

                ///
                    break;
                case  insecticide_treat:
                    if (g_rand_uni() < getInsecticideApplication()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }
                    break;

                case  autumn_or_spring_plough:
                    if (g_rand_uni() < getSoilCultivationMortality()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }

                    break;
                case flammebehandling:    // 50
                case mow:
                case cut_weeds:
                case pigs_out:
                    break;
                case strigling_sow:
                case  strigling_hill:
                    if (g_rand_uni() < getSoilCultivationMortality()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }

                    break;
                case trial_insecticidetreat:
                    if (random(1000) < getPesticideTrialTreatment()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }

                    break;
                case trial_toxiccontrol:
                    if (random(1000) < getPesticideTrialToxic()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }

                    break;
                case trial_control:
                case product_treat:
                case glyphosate:
                    break;
                case syninsecticide_treat:
                    if (random(1000) < getInsecticideApplication()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }

                    break;
                case biocide:
                    break;
                case  bed_forming:
                    if (g_rand_uni() < getSoilCultivationMortality()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }

                    break;
                case flower_cutting:
                    break;
                case  bulb_harvest:
                    if (g_rand_uni() < getSoilCultivationMortality()) {
                        //EggList.erase(EggList.begin()+i);
                        ClearFromMap(EggList[i].m_x, EggList[i].m_y);
                        EggList[i].m_x = -999; // Code for kill it
                    }

                    break;
                case straw_covering:
                case straw_removal:
                case  fa_n:
                case  fa_cu:
                case  fa_nk:
                case  burn_top:
                case  fa_pks:
                case  fp_pks:
                case  harvestshoots:
                case manual_weeding:
                    break;
                default:
                    m_OurLandscape->Warn("Beetle_Egg_List::DailyMortality() Beetle Egg - Treatment in Daily Mortality", std::to_string((int)event));
                    exit(1);
                }
            }
        }
    }
        // Now sort the list based on x values
        SortXR();
        // Then erase all the -999
        auto ritE = EggList.rend();
        auto ritB = EggList.rbegin();
        while (ritB != ritE) {
            if ((*ritB).m_x < 0) ritB++; else break;
        }
        EggList.erase((ritB).base(), EggList.end());
        return true; // always returns true

}

/**
Sort TheArray w.r.t. the current X in reverse order
*/
// The function will be used in all the descendent classes
void Beetle_Egg_List::SortXR() {
    sort( EggList.begin(), EggList.end(), CompareEggX() );
}

//-----------------------------------------------------------------------------

//---------------------------------------------------------------------------
//                           BEETLE_LARVAE
//---------------------------------------------------------------------------
Beetle_Larvae::Beetle_Larvae( int x, int y, Landscape* L, Beetle_Population_Manager* BPM ):Beetle_Base(x,y,L,BPM){
    m_DayMade = L->SupplyDayInYear();
}


double Beetle_Larvae::getSoilCultivationMortality() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_Larva_SoilCultivationMortality->value();
}
double Beetle_Larvae::getInsecticideApplication() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_Larva_InsecticideApplication->value();
}
TTypesOfBeetleState Beetle_Larvae::getDyingState() const{
    return tobs_LDying;
}

double Beetle_Larvae::getPesticideTrialTreatment() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_PesticideTrialLarvaeTreatmentMort->value();
}
double Beetle_Larvae::getPesticideTrialToxic() const{
    return m_OurPopulation->beetleconstantslist->PesticideTrialLarvaeToxicMort;
}
double Beetle_Larvae::getStriglingMortality() const {
    /**
 * \brief Method taht returns Strigling mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return 0;
}
void Beetle_Larvae::SetLarvalPPPElimRate(double a_rate) {
    Beetle_Larvae::m_BeetleLarvalPPPElimRate = a_rate;
}
//---------------------------------------------------------------------------

void Beetle_Larvae::ReInit( int x, int y, Landscape* L,	Beetle_Population_Manager * BPM ) {
    Beetle_Base::ReInit( x, y, L, BPM );
    m_LarvalStage = 0;
    m_AgeDegrees = 0;
    m_DayMade = L->SupplyDayInYear();
}

//--------------------------------------------------------------------------
double Beetle_Larvae::getLarvaTDailyMortality (int st, int t){
    /**
     * \brief returns temp-related daily mortality rate
     * **/
    return (*m_OurPopulation->beetleconstantslist->LarvalDailyMort)[st][t];
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
int Beetle_Larvae::getDDepthRange () const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_DDepRange->value();
}
double Beetle_Larvae::getHarvestMortality() const {
    /**
 * \brief Method that returns harvest mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return 0;
}
//--------------------------------------------------------------------------
double Beetle_Larvae::getLDevelConst2 (int i) const {
    return m_OurPopulation->beetleconstantslist->LDevelConst2->at(i);
}
double Beetle_Larvae::getDailyLarvaeMortality() const{
    return m_OurPopulation->beetleconstantslist->DailyLarvaeMort;
}
double Beetle_Larvae::getDailyMortalityRate() const{
    return getDailyLarvaeMortality();
}
double Beetle_Larvae::getExtremeTempMortality() const{
    return m_OurPopulation->beetleconstantslist->p_cfg_LarvaExtremeTempMort->value();
}
//---------------------------------------------------------------------------
void Beetle_Larvae::ClearFromMap(int X, int Y){
    m_OurPopulation->m_LarvaePosMap->ClearMapValue(X,Y);
}
bool Beetle_Larvae::AddToMap(int X, int Y){
    m_OurPopulation->m_LarvaePosMap->SetMapValue( X, Y);
    return true;
}

double Beetle_Larvae::TempRelatedMortality(double temp2, double maxtemp, double mintemp)
{
/**
	The idea here is that we calculate how long a larvae would expect to be
	at a temperature (based on today's temp)
	We also calculate the background mortality expected at that temperature
	then divide the total mortality by the number of days
	then apply that mortality today
	Problem is that we are dealing with percentages so this is not straight
	forward.
	The solution used here is to calculate a set of development and daily
	mort for 5 degree intervals then choose the closest.
*/
    /**
     * We check and if the temperature s extreme one we have a high mortality  rate
     * */
    double mortality {0.0};
    if (((mintemp<getExtremeTempMin()) || (maxtemp>getExtremeTempMax()))&&ExtremeTempIsOn()){
       mortality+=getExtremeTempMortality();
    }
    if (temp2<0) temp2=0; else if (temp2>25) temp2=25;
    temp2=(int)(floor((float)temp2+2.0)*0.2); // same as (temp2+2)/5
    // take a mortality test here we add a background mortality rate that was previously dealt with by DailyMortality()
   // if (g_rand_uni()<(getLarvaTDailyMortality(m_LarvalStage, temp2))+0) return true;// die
    mortality+=getLarvaTDailyMortality(m_LarvalStage, (int)temp2)+getDailyMortalityRate();
    return mortality;

}
TTypesOfBeetleState Beetle_Larvae::st_Develop()
{
/**
	Determines temperature related mortality.
	If the larvae survives this then a density-dependent mortality is applied.
	If the the larvae survives the second mortality then development occurs based on the temperature experienced since the day before. Development rate depends on the larval stage. The code is speeded by these calculations being carried out by the Beetle_Population_Manager for all eggs, larvae and adults extant each day, hence only one calculation needs to be done for each life-stage instead of potentially millions.
*/
   double temp2= floor(m_OurLandscape->SupplyTemp()+0.5);
   auto maxtemp = getMaxTemp();
   auto mintemp = getMinTemp();
   if (TempRelatedMortality(temp2, maxtemp, mintemp)>g_rand_uni())
   {
     // To ensure that we don't influence anyone else clear the position
     return getDyingState(); // die
   }
   // Find out if there is any danger of using wrap round
   if ((m_Location_x==0)||(m_Location_y==0)||(m_Location_x>=
       m_OurPopulation->SupplySimW() - 2) || (m_Location_y >= m_OurPopulation->SupplySimH() - 2))
   {
     // No density dependent mortality at the edge (this is a fudge, but a small one with large landscapes)
   }
   else
   {
     // Do density dependent mortality based on the +/- 1 m in all directions.
     if (((m_OurPopulation->LDDepMort1)>0)&&(m_OurPopulation->m_LarvaePosMap->GetMapDensity(m_Location_x-1,m_Location_y-1,getDDepthRange())>=m_OurPopulation->LDDepMort0)) {// ((m_OurPopulation->LDDepMort1)>0&&)
       if (g_rand_uni()<m_OurPopulation->LDDepMort1)
       {
        // To ensure that we don't influence anyone else clear the position
        return getDyingState(); // die
       }
	 }
   }
   // Still alive so now do the development bit
   if (m_OurPopulation->SupplyLDayDeg(m_LarvalStage, m_DayMade )>getLDevelConst2(m_LarvalStage))
   {
     ClearFromMap(m_Location_x, m_Location_y); // we have to clear and add again, because sometimes different instars are stored in different place
     m_LarvalStage++; // turn into next larval stage
     AddToMap(m_Location_x, m_Location_y);
   }
   if (m_LarvalStage==getLarvalStagesNum()) return tobs_Pupating; // become a pupa
   else return tobs_LDeveloping; // carry on developing
}
//---------------------------------------------------------------------------


TTypesOfBeetleState Beetle_Larvae::st_Pupate()
{
   // Must clear the map position
   ClearFromMap(m_Location_x, m_Location_y);
   //struct_Beetle * BS;
   std::unique_ptr <struct_Beetle> BS(new struct_Beetle);  //   instead of new struct_Beetle; using C++11
   BS->x   = m_Location_x;
   BS->y   = m_Location_y;
   BS->L   = m_OurLandscape;
   BS->BPM = m_OurPopulation;
   //BS->DayDegrees=(int)m_AgeDegrees;
   // carry the extra degrees over to next stage
   m_OurPopulation->CreateObjects(2,this,nullptr,std::move(BS),1);
   m_CurrentStateNo=-1;
   return tobs_Destroy;
   //delete BS;
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------


void Beetle_Larvae::BeginStep()
{
  CheckManagement();
  /**
   * The call for background mortality estimation has been moved to be together with the temperature
   * related mortality (st_Develop) to speed up things
   *
   * */
 // if (DailyMortality())CurrentBState=getDyingState();
}
//---------------------------------------------------------------------------


void Beetle_Larvae::Step()
{
 if (m_StepDone || m_CurrentStateNo == -1) return;
 switch (CurrentBState)
 {
  case 0:    // Initial
   CurrentBState=tobs_LDeveloping;
   break;
  case tobs_LDeveloping: // Developing
  {
      CurrentBState=st_Develop();//   st_Develop() can return:
                                  //     |
                                  //     |-> tobs_Pupating
                                  //     |-> tobs_LDying
                                  //     |-> tobs_LDeveloping
      m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Develop() stepDone is:
                                                      //     |
                                                      //     |-> false
                                                      //     |-> false
                                                      //     |-> true
  }
  break;

  case tobs_Pupating:
  {
       CurrentBState=st_Pupate();//   st_Pupate() can return:
                                  //     |
                                  //     |-> tobs_Destroy
        m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Pupate() stepDone is:
                                                      //     |
                                                      //     |-> true
  }
  break;

  case tobs_LDying:
   // Remove itself from the position map
   ClearFromMap(m_Location_x, m_Location_y);
   st_Die();
   m_StepDone=true;
   break;
  default:
    m_OurLandscape->Warn("Beetle_Larvae::Step()", "Beetle Larvae - Unknown State");
	g_msg->Warn(WARN_BUG, "State attempted was: ", int(CurrentBState));
    exit(1);
  }
}
//---------------------------------------------------------------------------

void Beetle_Larvae::EndStep()
{
#ifdef __BEETLEPESTICIDE1
	m_body_burden *= m_BeetleLarvalPPPElimRate;
	// Pick up the pesticide to add to the body burden
	m_body_burden += m_OurLandscape->SupplyPesticide(m_Location_x, m_Location_y, ppp_1);
	InternalPesticideHandlingAndResponse();
	if (m_currentPPPEffectProb != 0)
	{
		// Kill the beetle following a fixed probability of death. The test is taken each day until it dies.
		if (g_rand_uni() < m_currentPPPEffectProb) {
            ClearFromMap(m_Location_x, m_Location_y);
            st_Die();
        }
	}
#endif
}

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//                           BEETLE_PUPAE
//---------------------------------------------------------------------------
double Beetle_Pupae::getSoilCultivationMortality() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_Pupa_SoilCultivationMortality->value();
}
double Beetle_Pupae::getInsecticideApplication() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_Pupa_InsecticideApplication->value();
}
double Beetle_Pupae::getPesticideTrialTreatment() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_PesticideTrialPupaeTreatmentMort->value();
}
double Beetle_Pupae::getPesticideTrialToxic() const {
    return m_OurPopulation->beetleconstantslist->PesticideTrialPupaeToxicMort;

}
double Beetle_Pupae::getHarvestMortality() const {
    /**
 * \brief Method that returns harvest mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return 0;
}

Beetle_Pupae::Beetle_Pupae(int x, int y,Landscape* L, Beetle_Population_Manager * BPM): Beetle_Base(x,y,L,BPM)
{
    m_AgeDegrees=0;
    m_DayMade=L->SupplyDayInYear();
}

void Beetle_Pupae::ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) {
	Beetle_Base::ReInit( x, y, L, BPM );
	m_AgeDegrees = 0;
	m_DayMade = L->SupplyDayInYear();
}
double Beetle_Pupae::getDevelConst2() const{
    return m_OurPopulation->beetleconstantslist->PupaDevelConst2;
}
double Beetle_Pupae::getExtremeTempMortality() const{
    return m_OurPopulation->beetleconstantslist->p_cfg_PupaExtremeTempMort->value();
}
//-------------------------------------------------------------------------------------
/**
* \brief Method that returns daily mortality
 * Note that the mortality is out of 100%
 * */
double Beetle_Pupae::calcDailyMortChance (int temp2, double LengthOfStageAtTemp) const{
  if (((temp2<getExtremeTempMin())||(temp2>getExtremeTempMax()))&&ExtremeTempIsOn()) return getExtremeTempMortality()*100;//
  if (temp2 <1) temp2=1;
  if (temp2<13)
  {

        // Get the mortality per day
        // Equation modified from Boye Jensen 1990
        return (100-(-7.1429+1.42857*temp2))*
               g_SpeedyDivides[(int) LengthOfStageAtTemp];
    }
  else if (temp2<23)
    {

        // Get the mortality per day
        // Equation modified from Boye Jensen 1990
        return (100-(-74.623+7.26415*temp2))
               *g_SpeedyDivides[(int) LengthOfStageAtTemp];
    }
  else // temp>=23
    {


        // Get the mortality per day
        // Equation modified from Boye Jensen 1990
        return (20.0)*g_SpeedyDivides[(int) LengthOfStageAtTemp];
    }
};


int Beetle_Pupae::calcLengthOfStageAtTemp (int temp2) const{
    if (temp2 <1) temp2=1;

    // How long to develop at this temp
    return (int)(getDevelConst2()*g_SpeedyDivides[temp2]);

};

TTypesOfBeetleState Beetle_Pupae::getDyingState() const{
    return tobs_PDying;
}
double Beetle_Pupae::getDailyMortalityRate() const{
    return m_OurPopulation->beetleconstantslist->DailyPupaeMort;
}
double Beetle_Pupae::getStriglingMortality() const {
    /**
 * \brief Method taht returns Strigling mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return 0;
}
TTypesOfBeetleState Beetle_Pupae::st_Develop()
/**
 * /brief This is a general develop function that  makes use of functions that calculate daily mortality rates
 * and length of the Pupa stage as a function of temperature.
 *
 * **/
{

   int temp2=(int) floor(m_OurLandscape->SupplyTemp()+0.5);
    /*
     * We call the function that calculates daily mortality chance, it takes into account background mortality
     * which is the one that previously was dealt by DailyMortality() called from BeginStep()
     * */
   double DailyMortChance=calcDailyMortChance(temp2, calcLengthOfStageAtTemp(temp2))*0.01;




    // take a mortality test (temperature related mortality + daily background mortality)
    //if (random(1000) < ((DailyMortChance+0)*10)) return getDyingState();   //if (random(1000) < ((DailyMortChance+getDailyMortalityRate())*10)) return getDyingState();   // we will fall back to this after DEBUG
    if (g_rand_uni() < ((DailyMortChance+getDailyMortalityRate()))) return getDyingState();   // we will fall back to this after DEBUG
    // Still alive so now do the development bit
    if (temp2>0) m_AgeDegrees+=temp2;
    if (m_OurPopulation->SupplyPDayDeg(m_DayMade )>getDevelConst2())
    {
      return tobs_Emerging; // go to emerge
    }
    else return tobs_PDeveloping; // carry on developing
}

TTypesOfBeetleState Beetle_Pupae::st_Emerge()
/**
 * This is a common process among beetles so it should be here
 *
 *
 * **/
{
   std::unique_ptr <struct_Beetle> BS(new struct_Beetle);

   int NoToMake=1;
   BS->x   = m_Location_x;
   BS->y   = m_Location_y;
   BS->L   = m_OurLandscape;
   BS->BPM = m_OurPopulation;
   // object destroyed by OwnerRTS--> C++11 unique_pointer + std::move

   m_OurPopulation->CreateObjects(3,this,nullptr,std::move(BS),NoToMake);
    m_CurrentStateNo=-1; //Destroys object
    return tobs_Destroy;
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------


void Beetle_Pupae::BeginStep()
{
  CheckManagement();
  /**
   * The call for dailyMortality and an associated
   * assignment of dying state is moved to the temperature related mortality to fasten the things up
   * */
  //if (DailyMortality())CurrentBState=getDyingState(); // comment out after DEBUG
}
//---------------------------------------------------------------------------


void Beetle_Pupae::Step()
/**
 * \brief Standard step function for a beetle  base class
 *
 *
 * **/
{
 if ( m_CurrentStateNo == -1||Cannibalisation()||m_StepDone) return;
 switch (CurrentBState)
 {
  case 0:    // Initial
   CurrentBState=tobs_PDeveloping;
   break;
  case tobs_PDeveloping: // Developing
  {
      CurrentBState=st_Develop();//   st_Develop() can return:
                              //     |
                              //     |-> tobs_Emerging
                              //     |-> tobs_PDying
                              //     |-> tobs_PDeveloping
      m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Develop() stepDone is:
                                                  //     |
                                                  //     |-> false
                                                  //     |-> false
                                                  //     |-> true
  }
  break;
  case tobs_Emerging:
  {
      CurrentBState=st_Emerge();//   st_Emerge() can return:
      //     |
      //     |-> tobs_Destroy
      m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Emerge() stepDone is:
      //         |
      //         |-> false
  }
  break;
  case tobs_PDying: {
      ClearFromMap(m_Location_x, m_Location_y);
      st_Die();
      m_StepDone = true;
      break;
  }
  default:
    m_OurLandscape->Warn("Beetle_Pupae::Step()", "Beetle Pupae - Unknown State");
    exit(1);
  }
}
//---------------------------------------------------------------------------

void Beetle_Pupae::EndStep()
{
#ifdef __BEETLEPESTICIDE1
	m_body_burden *= m_BeetlePupalPPPElimRate;
	// Pick up the pesticide to add to the body burden
	m_body_burden += m_OurLandscape->SupplyPesticide(m_Location_x, m_Location_y, ppp_1);
	InternalPesticideHandlingAndResponse();
	if (m_currentPPPEffectProb != 0)
	{
		// Kill the beetle following a fixed probability of death. The test is taken each day until it dies.
		if (g_rand_uni() < m_currentPPPEffectProb) {
            ClearFromMap(m_Location_x, m_Location_y);
            st_Die();
        }
	}
#endif
}

bool Beetle_Pupae::DailyMortality()
{
  if (g_rand_uni()<getDailyMortalityRate())
  {
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------




//------------------------------------------------------------------------------

//---------------------------------------------------------------------------
//                           BEETLE_ADULT
//---------------------------------------------------------------------------
// should use a base class implementation
// double Beetle_Adult::getStriglingMortality() const {
//    return m_OurPopulation->beetleconstantslist-> p_cfg_BeetleStriglingMort->value();
//}
double Beetle_Adult::getStriglingMortality() const {
    /**
 * \brief Method taht returns Strigling mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return m_OurPopulation->beetleconstantslist-> p_cfg_BeetleStriglingMort->value();
}

double Beetle_Adult::getPesticideTrialTreatment() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_PesticideTrialAdultTreatmentMort->value();
}

double Beetle_Adult::getPesticideTrialToxic() const {
    return m_OurPopulation->beetleconstantslist->PesticideTrialAdultToxicMort;

}
double Beetle_Adult::getSoilCultivationMortality() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_Adult_SoilCultivationMortality->value();
}
double Beetle_Adult::getInsecticideApplication() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_Adult_InsecticideApplication->value();
}
TTypesOfBeetleState Beetle_Adult::getDyingState() const{
    return tobs_ADying;
}
Beetle_Adult::Beetle_Adult( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) :
Beetle_Larvae( x, y, L, BPM ), Beetle_Base(x, y, L, BPM) {
	;
}


void Beetle_Adult::ClearFromMap(int X, int Y){
    m_OurPopulation->m_AdultPosMap->ClearMapValue(X,Y);
}
bool Beetle_Adult::AddToMap(int X, int Y){
    m_OurPopulation->m_AdultPosMap->SetMapValue( X, Y);
    return true;
}
//---------------------------------------------------------------------------

void Beetle_Adult::ReInit( int x, int y, Landscape* L, Beetle_Population_Manager * BPM ) {
	Beetle_Larvae::ReInit( x, y, L, BPM );
	Init();
}
int Beetle_Adult::getStartAggregationDay() const {
    return m_OurPopulation->beetleconstantslist->StartAggregatingDay;
}
int Beetle_Adult::getStartAggregationDayProb() const{
    return m_OurPopulation->beetleconstantslist->StartAggregatingDayProb;
}
int Beetle_Adult::getMaxMoveDist() const{
    return m_OurPopulation->beetleconstantslist->g_AdultMaxMoveDist;
}

double Beetle_Adult::getDispersalThreshold() const{
    return m_OurPopulation->beetleconstantslist->DispersalThreshold;
}
int Beetle_Adult::getDispersalDayDegrees() const{
    return m_OurPopulation->beetleconstantslist->DipsersalDayDegrees;
}
int Beetle_Adult::getTurnRate() const{
    return m_OurPopulation->beetleconstantslist->AdultTurnRate;
}
int Beetle_Adult::getMovementThreshold() const{
    return m_OurPopulation->beetleconstantslist->AdultMovementTempThreshold;
};
int Beetle_Adult::getTotalNumberEggs() const{
    return m_OurPopulation->beetleconstantslist-> p_cfg_TotalNoEggs->value();
};
int Beetle_Adult::getFieldHibernateChance() const{
    return m_OurPopulation->beetleconstantslist->FieldHibernateChance;
}
int Beetle_Adult::getStopAggregationDay() const{
    return m_OurPopulation->beetleconstantslist->StopAggregationDay;
}
double Beetle_Adult::getDailyMortalityRate() const {
    return m_OurPopulation->beetleconstantslist->DailyAdultMort;
}
int Beetle_Adult::getDDepthRange () const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_DDepRange->value();
}
double Beetle_Adult::getHarvestMortality() const {
    return m_OurPopulation->beetleconstantslist-> p_cfg_BeetleHarvestMort->value();
}
double Beetle_Adult::getExtremeTempMortality() const{
    return m_OurPopulation->beetleconstantslist->p_cfg_AdultExtremeTempMort->value();
}
double Beetle_Adult::getStriglingHillMortality() const {
    /**
 * \brief Method that returns Strigling Hill mortality
     *
     * This is a function that can be overriden by Larva, Egg, pupa and Adult class.
     * However here we base it on the assumption that such mortality is similar for all the beetle forms
    *
    * ***/
    return getStriglingMortality();
}
//---------------------------------------------------------------------------

void Beetle_Adult::Init()
{
    m_negDegrees=0; //No negative degrees to sum
    // don't want to start accumulating Hibernate degrees until Jan 1st
    m_HibernateDegrees=-99999;
    OldDirection=random(8);
    m_EggCounter=0;
    m_CanReproduce=false;
	m_body_burden = 0;
	m_currentPPPEffectProb = 0;
}
//---------------------------------------------------------------------------



/**
If it is not yet time to start aggregating then the beetle does its daily movement and returns a zero value,
indicating it will stay in this state. If date has exceeded StartAggregatingDay, then the beetle has a increasing
probability with time of changing to aggregative behaviour.\n
If the beetle starts aggregating and it is from last season, then it is assumed that it will die.\n
*/
TTypesOfBeetleState Beetle_Adult::st_Forage()
{
    int p_distance = (int) m_OurPopulation->beetleconstantslist->move_distribution->get();
#ifdef BEMMOVETEST
    m_OurPopulation->beetleconstantslist->g_movementdistancesout << p_distance << '\n';
#endif
    // If its too cold then do not do any movement today
    if (!(m_OurLandscape->SupplyTemp() < getMovementThreshold())) {
        int turning = getTurnRate();
        //DailyMovement(p_distance, true);// Die
        MoveTo(p_distance, OldDirection, turning);
        CanReproduce();
    }


  if (DDepMort()) {
      //std::cout << "DD\t" << m_OurPopulation->m_TheLandscape->SupplyGlobalDate() << "\t"<<"Density" << '\n';
      return tobs_ADying;

  } // die
  int Day=m_OurLandscape->SupplyDayInYear();
  if (Day<getStartAggregationDay()) return CurrentBState;
  else
  {
    if (random(100)<Day-getStartAggregationDayProb())
    {
     // Stop laying eggs now
     if (m_CanReproduce){ // must be from last year so kill it
        // std::cout << "DD\t" << m_OurPopulation->m_TheLandscape->SupplyGlobalDate() << "\t"<<"LastYear" << '\n';
         return tobs_ADying;
    }
     return tobs_Aggregating; // start aggregating
    }
  }
  return CurrentBState;
}
//-----------------------------------------------------------------------------
/**
 * \brief The function that performs short range movement
 * */
void Beetle_Adult::ShortRangeMovement() {
    int oldx{m_Location_x};
    int oldy{m_Location_y};

    Move();
    if(AddToMap(m_Location_x, m_Location_y)){
        ClearFromMap(oldx, oldy);
    }
    else{
        m_Location_x=oldx;
        m_Location_y=oldy;
    }



}
//----------------------------------------------------------------------------
//--------------------------------------------------------------------------
TTypesOfBeetleState Beetle_Adult::st_Aggregate()
{
    int p_distance=random(getMaxMoveDist())+1;
#ifdef BEMMOVETEST
    m_OurPopulation->beetleconstantslist->g_movementdistancesout << p_distance << '\n';
#endif
    // If its too cold then do not do any movement today


    int hab=m_OurPopulation->m_MoveMap->GetMapValue(m_Location_x, m_Location_y);
    if (m_OurLandscape->SupplyDayInYear()>363)
    {
        // Has reached January so must decide if can hibernate here or die
        if ((hab==1)||(random(100)<getFieldHibernateChance())) return tobs_Hibernating;
        else return tobs_ADying; // die
    }
    else
    if (m_OurLandscape->SupplyDayInYear()>getStopAggregationDay())
    {

        TTypesOfVegetation tov = m_OurLandscape->SupplyVegType(m_Location_x, m_Location_y);
        if (IsSuitableForHibernation(tov)) {
            return tobs_Hibernating;
        }

    }
   if (m_OurLandscape->SupplyTemp() < getMovementThreshold()) {
       return CurrentBState;
   }
   else{
       int turning = getTurnRate();
       MoveToAggr(p_distance, OldDirection, turning);// DailyMovement(p_distance, false);
       return CurrentBState; // Carry on aggregating
   }



}
//-----------------------------------------------------------------------------
bool Beetle_Adult::checkForDispersal(int day, double temp) {

    if (day>120) return false; // don't disperse if after spring //Changed to 120 from 100 , to fit with Elzbieta's version TODO: put it to the variable


    if ((day >= 60) && (m_HibernateDegrees >= getDispersalDayDegrees()))
    {
        //If we don't start dispersing then ensure we enter code next time
        m_HibernateDegrees=getDispersalDayDegrees();

        // test against the chance of dispersal
        // assume that it takes about one month to get 120 DDs
        // 30*(10-6)=120 - ALSO ASSUMES MEAN TEMP OF 10 DEGREES C
        // Therefore the probability of dispersing per day is that prob when
        // summed culmulatively gives 0.92 after 30 days (0.92 is the observed
        // dispersal % at 120DDs above 6 degrees C
        // this value is 0.081
        if (random(1000)<81)
        {

            return true; // dispersing
        }
        if (day>90)
        {


            return true; // dispersing
        }

    }
    return false;// don't disperse
}

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    

/**
Accumulates day degrees from the beginning of the year and starts dispersal when
the sum exceeds a threshold or at the beginning of March.\n
At the end of hibernation the beetle is allowed to reproduce via the m_CanReproduce attribute.\n
*/
TTypesOfBeetleState Beetle_Adult::st_Hibernate()
{
    /**
     * \brief This is the function that describes possibilities of moving to other stage after the hibernation.
     * Currently (as in Bembidion) the only possibilities are:
     * 0 Hibernation
     * 1 Dispersal
     * 2 Death
     * However generally it can include checks for different types of behaviour.
     *
     * **/
    int day=m_OurLandscape->SupplyDayInYear();
    double temp=m_OurLandscape->SupplyTemp();
    if (day==0) m_HibernateDegrees=0;


    if (temp>getDispersalThreshold()) m_HibernateDegrees+=temp-getDispersalThreshold();
    if (checkForDispersal(day,temp)){
        m_CanReproduce=true;//Can only reproduce after hibernating else too young
        if (WinterMort()) return tobs_ADying; // die  - a once only test
        return tobs_Dispersing; // dispersing
    }else{

        return CurrentBState; // still hibernating
    }
    /** Auld code: to be removed
    if (day==0) m_HibernateDegrees=0;
    else if (day>100) return 0; // don't disperse if after spring

    if (temp>getDispersalThreshold()) m_HibernateDegrees+=temp-getDispersalThreshold();
	if ((day >= 60) || (m_HibernateDegrees >= getDispersalDayDegrees()))
    {
      //If we don't start dispersing then ensure we enter this code next time
      m_HibernateDegrees=getDispersalDayDegrees();
      // test against the chance of dispersal
      // assume that it takes about one month to get 120 DDs
      // 30*(10-6)=120 - ALSO ASSUMES MEAN TEMP OF 10 DEGREES C
      // Therefore the probability of dispersing per day is that prob when
      // summed culmulatively gives 0.92 after 30 days (0.92 is the observed
      // dispersal % at 120DDs above 6 degrees C
      // this value is 0.081
      if (random(1000)<81)
      {
        m_CanReproduce=true;//Can only repoduce after hibernating else too young
		if (WinterMort()) return 2; // die  - a once only test
		return 1; // dispersing
      }
      if (day>90)
      {
        m_CanReproduce=true;//Can only repoduce after hibernating else too young
		if (WinterMort()) return 2; // die  - a once only test
		return 1; // dispersing
      }
    }
    return 0; // still hibernating
     **/
}
//-----------------------------------------------------------------------------


TTypesOfBeetleState Beetle_Adult::st_Dispersal()
{
   /**
   Will now move in a direction away from high densities of beetles and towards good habitat
   */
    int p_distance=(int) m_OurPopulation->beetleconstantslist->move_distribution->get();
#ifdef BEMMOVETEST
    m_OurPopulation->beetleconstantslist->g_movementdistancesout << p_distance << '\n';
#endif
    // If its too cold then do not do any movement today
    if (m_OurLandscape->SupplyTemp() < getMovementThreshold()) return tobs_Dispersing;//originally it was return tobs_Foraging
                                                                                    // added it here just for the sake of future hypothetical beetle that exhibits
                                                                                    // a special dispersing behaviour
    // are we aggregating or dispersing?
    int turning = getTurnRate();
   //DailyMovement(p_distance, true);
   MoveTo(p_distance, OldDirection, turning);
   m_EggCounter=getTotalNumberEggs(); // start the egg production counter
   CanReproduce();
   return tobs_Foraging;
}
//---------------------------------------------------------------------------
void Beetle_Adult::IncNegDegrees(){
    double temp = m_OurLandscape->SupplySoilTemp();
    if (temp<0.0) m_negDegrees+=temp;
}
// This version has no ageing whilst hibernating
TTypesOfBeetleState Beetle_Adult::st_Aging()
{


    // Check should the beetle die today if not hibernating
    if (CurrentBState!=tobs_Hibernating)
    {
      if (DailyMortality()) {
         // std::cout<< "DD\t"<<m_OurPopulation->m_TheLandscape->SupplyGlobalDate()<<"\t"<<"Age"<<'\n';
          return tobs_ADying;} // go to die
      if (m_CanReproduce)
      {
         if (m_EggCounter<0) {
           //  std::cout<< "DD\t"<<m_OurPopulation->m_TheLandscape->SupplyGlobalDate()<<"\t"<<"EggsLaid"<<'\n';
             return tobs_ADying;} // All eggs laid so die
       }
    }
    return CurrentBState; // carry on
}
//---------------------------------------------------------------------------

bool Beetle_Adult::WinterMort() const
{
   // Beetles are immobile during winter so only call this once at the
   // end of overwintering
   double mortchance=0.1;// max 90% mortality
   if (m_negDegrees>-40)
   {
      mortchance = 0.94925+(m_negDegrees*0.00426);
   }
   else if (m_negDegrees>-80)
   {
      mortchance = 1.16913+(m_negDegrees*0.01149);
   }
   else if (m_negDegrees>-107)
   {
      mortchance = 0.6665+(m_negDegrees*0.00528);
   }

   //mortchance = 0.6; // mortality fixed to 40%
   if (random(1000) < ((1-mortchance)*1000)) return true; // die
   else return false;
}



//---------------------------------------------------------------------------


void Beetle_Adult::MoveTo(int p_distance,unsigned p_direction,int p_turning)
{
  /**
  Move p_distance steps in p_direction with p_turning out of 100 chance of
  changing p_direction. This function will alter m_Location_x & m_Location_y.\n
  p_direction gives the preferred direction (0-7)\n
  p_distance is the number of steps\n
  The beetles will stop moving in suitable hibernatio habitat late in the year.\n
  \n
  This method is very similar to Beetle_Adult::MoveToAggr but differs in the direction of movement
  relative to habitat types and the end of movement behaviour. In this case the beetl tries to lay eggs
  in MoveToAggre it test for hibernation conditions.\n
  */
  int vx = m_Location_x;
  int vy = m_Location_y;
  pPoint.direction=p_direction;
  pList.nsteps=0;
  if (random(100)<p_turning)
  {
     // change direction
     if (random(2)==1) pPoint.direction=((1+pPoint.direction) & 7U);
     else pPoint.direction=((7+pPoint.direction) & 0x07); //same as p_direction-1 but +ve
  }

  /*
    Improved speed here by checking if the max distance goes out of bounds
    i.e. x+p_distance, y+p_distance & x-p_distance,y-p_distance
    if not then there is no need to check for out of bounds later at all
  */

  if ((vx-p_distance<0) || (vy-p_distance<0) || (vx+p_distance>=m_OurPopulation->SupplySimW())
          || (vy+p_distance>=m_OurPopulation->SupplySimH()))
  {
    //---- Duplicated with out of bounds check for speed
    for (int i=0; i<p_distance; i++)
    {
      // test the squares at Vector, Vector+1+2, Vector-1-2
      // if they are inaccessible (water,buildings,forest) try another square
      //
      int qual=3;
      int tries=0;
      while ((qual==3)&&(tries++<10))
      {
        // first watch out for out of bounds
        pPoint.x=vx+Vector_x[pPoint.direction];
        pPoint.y=vy+Vector_y[pPoint.direction];
        if (pPoint.x<0) pPoint.x=
                       (pPoint.x+m_OurPopulation->SupplySimW())%m_OurPopulation->SupplySimW();
        else if (pPoint.x>=m_OurPopulation->SupplySimW())
                                      pPoint.x=pPoint.x%m_OurPopulation->SupplySimW();
        if (pPoint.y<0)
        {
          pPoint.y=(pPoint.y+m_OurPopulation->SupplySimH())%m_OurPopulation->SupplySimH();
        }
        else if (pPoint.y>=m_OurPopulation->SupplySimH())
             {
               pPoint.y=pPoint.y%m_OurPopulation->SupplySimH();
             }
        qual=MoveTo_quality_assess();
      }
      if (qual!=3)
      {
        pList.BeenThereX[pList.nsteps]=pPoint.x;
        pList.BeenThereY[pList.nsteps++]=pPoint.y;
        vx=pPoint.x;
        vy=pPoint.y;
        OldDirection=(int) pPoint.direction;
      }
      else
      {
        pList.BeenThereX[pList.nsteps]=vx;
        pList.BeenThereY[pList.nsteps++]=vy;
      }
    }
  }
  //---- Duplicated for speed
  else // No Out of bounds possible
  {
    for (int i=0; i<p_distance; i++)
    {
      // test the squares at Vector, Vector+1+2, Vector-1-2
      // if they are inaccessible (water,buildings,forest) try another square
      //
      int qual=3;
      int tries=0;
      while ((qual==3)&&(tries++<10))
      {
        pPoint.x=vx+Vector_x[pPoint.direction];
        pPoint.y=vy+Vector_y[pPoint.direction];
        qual=MoveTo_quality_assess();
      }
      if (qual!=3)
      {
        pList.BeenThereX[pList.nsteps]=pPoint.x;
        pList.BeenThereY[pList.nsteps++]=pPoint.y;
        vx=pPoint.x;
        vy=pPoint.y;
        OldDirection=(int) pPoint.direction;
      }
      else
      {
        pList.BeenThereX[pList.nsteps]=vx;
        pList.BeenThereY[pList.nsteps++]=vy;
      }
    }
  }
  // alter the location
  ClearFromMap(m_Location_x,m_Location_y);
  m_Location_x=vx;
  m_Location_y=vy;
  AddToMap(m_Location_x,m_Location_y);
}
//---------------------------------------------------------------------------

void Beetle_Adult::CanReproduce() { //todo: let's rename this function, the name is misleading
    //are we reproducing?
    if (m_CanReproduce) {
        TTypesOfLandscapeElement tole;
        tole = m_OurLandscape->GetOwner_tole(m_Location_x, m_Location_y);
        // First find out how many eggs to produce depending on temperature
        int NoEggs = 0;
        // Calcuated in the pop man so it only needs calculating once
        if (IsReproductionLandscape(tole)) {
            NoEggs = m_OurPopulation->TodaysEggProduction;
        } else {
            if (IsHalfReproductionLandscape(tole)) {
                NoEggs = (m_OurPopulation->TodaysEggProduction) >> 1;
            }
        }


        if (NoEggs > 0) {
            for (int i = 0; i < NoEggs; i++) {
                int place = random(pList.nsteps);
                Reproduce(pList.BeenThereX[place], pList.BeenThereY[place]);
            }
            m_EggCounter -= NoEggs;
            // _EggCounter will go negative when all eggs are used
            //- allows a small over-production
        }
    }
}
//---------------------------------------------------------------------------

void Beetle_Adult::Reproduce(int p_x, int p_y)
{

   std::unique_ptr <struct_Beetle> BS(new struct_Beetle);
   BS->x   = p_x;
   BS->y   = p_y;
   BS->L   = m_OurLandscape;
   BS->BPM = m_OurPopulation;
   //BS->DayDegrees=0;
   BS->HowMany=1;
   // don't need delete because object destroyed by Population_Manager
   m_OurPopulation->CreateObjects(0,this,nullptr,std::move(BS),1);

}
//---------------------------------------------------------------------------



/**
A version of MoveTo where movement is determined by the suitability of the habitat moved to.
*/
int Beetle_Adult::MoveTo_quality_assess()
{
   int qual;
   // get the quality of the test area
   // if it is 3 then don't go here
   // 2 can walk there but it is non-habitat
   // 1 linear features e.g. hedges, roadside verges
   // 1 grassland
   // 0 crop field
   if (!m_OurPopulation->m_AdultPosMap->GetMapValue(pPoint.x,pPoint.y))
   {
     qual = m_OurPopulation->m_MoveMap->GetMapValue(pPoint.x,pPoint.y);
   }
   else qual=3;
   // if it is 3 then don't go here
   switch (qual)
   {
     case 3:  // Cannot go
      // try another direction
      if (random(2)==1) pPoint.direction=((1+pPoint.direction) & 0x07);
      else pPoint.direction=((7+pPoint.direction)& 0x07);
      break;
     case 2:
      // Have a 40% chance of moving on to it
      if (random(100)>=40)
      {
        // try another direction
        if (random(2)==1) pPoint.direction=((1+pPoint.direction) & 0x07);
        else pPoint.direction=((7+pPoint.direction) & 0x07); // Reverse
        break;
      }
     case 1:
     case 0:
      break;
     default:
      // Should never happen
      m_OurLandscape->Warn("Beetle_Adult::MoveTo_quality_assess()", "Beetle Adult:: MoveTo QA Bad Direction");
      exit(1);
   }
   return qual;
}
//---------------------------------------------------------------------------

TTypesOfBeetleState Beetle_Adult::MoveToAggr(int p_distance,unsigned p_direction,int p_turning)
{
  /**
  Move p_distance steps in p_direction with p_turning out of 100 chance of
  changing p_direction. This function will alter m_Location_x & m_Location_y.\n
  p_direction gives the preferred direction (0-7)\n
  p_distance is the number of steps\n
  This method does not alter CurrentBState
  */
  int vx = m_Location_x;
  int vy = m_Location_y;
  int tvx=0;
  int tvy=0;
  if (random(100)<p_turning)
  {
     // change direction
     if (random(2)==1) p_direction=((1+p_direction) & 0x07);
     else p_direction=((7+p_direction) & 0x07);
  }

  /*
    Improved speed here by checking if the max distance goes out of bounds
    i.e. x+p_distance, y+p_distance & x-p_distance,y-p_distance
    if not then there is no need to check for out of bounds later at all
  */
  if ((vx-p_distance<0) || (vy-p_distance<0)
                                      || (vx+p_distance>=m_OurPopulation->SupplySimW())
                                      || (vy+p_distance>=m_OurPopulation->SupplySimH()))
  {
  //---- Duplicated with out of bounds check for speed
  for (int i=0; i<p_distance; i++)
  {
    // test the squares at Vector, Vector+1+2, Vector-1-2
    // if they are inaccessible (water,buildings,forest) try another square
    //
    int qual=3;
    int tries=0;
    while ((qual==3)&&(tries++<10))
    {
      // first watch out for out of bounds
      tvx=vx+Vector_x[p_direction];
      tvy=vy+Vector_y[p_direction];
      if (tvx<0) tvx=(tvx+m_OurPopulation->SupplySimW())%m_OurPopulation->SupplySimW();
      else if (tvx>=m_OurPopulation->SupplySimW()) tvx=tvx%m_OurPopulation->SupplySimW();
      if (tvy<0) {tvy=(tvy+m_OurPopulation->SupplySimH())%m_OurPopulation->SupplySimH();}
      else if (tvy>=m_OurPopulation->SupplySimH()) {tvy=tvy%m_OurPopulation->SupplySimH();}
      // get the quality of the test area
      qual = m_OurPopulation->m_MoveMap->GetMapValue(tvx,tvy);
      // if it is 3 then don't go here
      switch (qual)
      {
        case 3:  // Cannot go
         // try another direction
         if (random(2)==1) p_direction=((1+p_direction)  & 0x07);
         else p_direction=((7+p_direction) & 0x07);
         break;
        case 2:
         // Have a 40% chance of moving on to it
         if (random(100)>=40)
         {
           // try another direction
           if (random(2)==1) p_direction=((1+p_direction) & 0x07);
           else p_direction=((7+p_direction) & 0x07);
           break;
         }
        case 1:
          // This is where we want to be
          i=p_distance;
        case 0:
         break;
        default:
        // Should never happen
        m_OurLandscape->Warn("Beetle_Adult::MoveToAggr", "Beetle Adult:: MoveTo Aggr Bad Direction");
        exit(1);
      }
    }
    if (qual!=3)
    {
      vx=tvx;
      vy=tvy;
      OldDirection=(int) p_direction;
    }
  }
  }
  //---- Duplicated for speed
  else // No Out of bounds possible
  {
  for (int i=0; i<p_distance; i++)
  {
    tvx=vx+Vector_x[p_direction];
    tvy=vy+Vector_y[p_direction];
    // if they are inaccessible (water,buildings,forest) try another square
    //
    int qual=3;
    int tries=0;
    while ((qual==3)&(tries++<10))
    {
      // get the quality of the test area
      // if it is 3 then don't go here
      // 2 can walk there but it is non-habitat
      // 1 linear features e.g. hedges, roadside verges
      // 1 grassland
      // 0 crop field
      qual = m_OurPopulation->m_MoveMap->GetMapValue(tvx,tvy);
      // if it is 3 then don't go here
      switch (qual)
      {
        case 3:  // Cannot go
         // try another direction
         if (random(2)==1) p_direction=((1+p_direction) & 0x07);
         else p_direction=((7+p_direction) & 0x07);
         break;
        case 2:
         // Have a 40% chance of moving on to it
         if (random(100)>=40)
         {
           // try another direction
           if (random(2)==1) p_direction=((1+p_direction) & 0x07);
           else p_direction=((7+p_direction) & 0x07);
           break;
         }
        case 1:
          // where we want to be so stop for a while
          i=p_distance;
        case 0:
         break;
        default:
        // Should never happen
        m_OurLandscape->Warn("Beetle_Adult::MoveToAggr", "Beetle Adult:: MoveTo Aggr 2 Bad Direction");
        exit(1);
      }
    }
    if (qual!=3)
    {
      vx=tvx;
      vy=tvy;
      OldDirection=(int) p_direction;
    }
  }
  }
  // alter the location
  ClearFromMap(m_Location_x,m_Location_y);
  m_Location_x=vx;
  m_Location_y=vy;
  AddToMap(m_Location_x,m_Location_y);
return CurrentBState;// this method does not alter the CurrentBState
}
//---------------------------------------------------------------------------

void Beetle_Adult::BeginStep()
{
  CheckManagement();
  //if (CurrentBState==tobs_ADying)
        //      std::cout<< "DD\t"<<m_OurPopulation->m_TheLandscape->SupplyGlobalDate()<<"\t"<<"Manage"<<'\n';
  if (CurrentBState!=getDyingState()){
      IncNegDegrees();
      // Die if mortality occurs or is too old
      CurrentBState=st_Aging();
  }

}
//---------------------------------------------------------------------------

void Beetle_Adult::EndStep()
{
#ifdef __BEETLEPESTICIDE1
	m_body_burden *= m_BeetleAdultPPPElimRate;
	// Pick up the pesticide to add to the body burden
	m_body_burden += m_OurLandscape->SupplyPesticide(m_Location_x, m_Location_y, ppp_1);
	InternalPesticideHandlingAndResponse();
	if (m_currentPPPEffectProb != 0)
	{
		// Kill the beetle following a fixed probability of death. The test is taken each day until it dies.
		if (g_rand_uni() < m_currentPPPEffectProb) {
            ClearFromMap(m_Location_x, m_Location_y);
            st_Die();
        }
	}
#endif
}
//---------------------------------------------------------------------------

void Beetle_Adult::Step()
{
  if (m_StepDone || m_CurrentStateNo == -1) return;
  //std::cout<< "DEBUG MESSAGE: Day: <" << m_OurPopulation->m_TheLandscape->SupplyDayInYear() << "> in year "<< m_OurPopulation->m_TheLandscape->SupplyYearNumber()<<" BState: "<<CurrentBState<<" Can repr: "<<m_CanReproduce<<'\n';
  /* Debugging messages
  std::cout<< "DB\t"<<m_OurPopulation->m_TheLandscape->SupplyYear()<<"\t" <<m_OurPopulation->m_TheLandscape->SupplyGlobalDate()<<"\t"<< m_OurPopulation->m_TheLandscape->SupplyDayInYear() << "\t"<< m_OurPopulation->m_TheLandscape->SupplyYearNumber()<<"\t"<<CurrentBState<<"\t"<<m_CanReproduce<<'\n';
  */
    switch (CurrentBState)
  {
  case tobs_Initiation:  // nothing, same as 0
    CurrentBState=tobs_Foraging;
    break;
  case tobs_Foraging:  // Forage
  {
      CurrentBState=st_Forage();//   st_Forage() can return:
                                //   |
                                //   |-> tobs_Foraging
                                //   |-> tobs_ADying
                                //   |-> tobs_Aggregating

      m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone)) [CurrentBState];//   st_Forage() stepDone is:
                                                      //   |
                                                      //   |-> true
                                                      //   |-> false
                                                      //   |-> true

  }
  break;
  case tobs_Aggregating:  // Aggregate
  {
      CurrentBState=st_Aggregate();//   st_Aggregate() can return:
                                   //     |
                                   //     |-> tobs_Aggregating
                                   //     |-> tobs_Hibernating
                                   //     |-> tobs_ADying
      m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Aggregate() stepDone is:
                                                      //     |
                                                      //     |-> true
                                                      //     |-> true
                                                      //     |-> false

  }
  break;
  case tobs_Hibernating:  // Hibernate
  {
      CurrentBState= st_Hibernate();//   st_Aggregate() can return:
                                  //     |
                                  //     |-> tobs_Dispersing
                                  //     |-> tobs_ADying
      m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Hibernate() stepDone is:
                                                      //     |
                                                      //     |-> true
                                                      //     |-> false
  }
  break;

  case tobs_Dispersing:  // Disperse
  {
      CurrentBState=st_Dispersal();//   st_Dispersal() can return:
                                  //     |
                                  //     |-> tobs_Dispersing (on very rare occasions: if the temp is low, it will try dispersing tomorrow)
                                  //     |-> tobs_Foraging (basically the same type of movement as in Dispersal)
      m_StepDone=(*(m_OurPopulation->beetleconstantslist->BeetleMapOfStepDone))[CurrentBState];//   st_Hibernate() stepDone is:
                                                      //     |
                                                      //     |-> true
                                                      //     |-> true
  }
  break;

  case tobs_ADying:  // Die
    ClearFromMap(m_Location_x, m_Location_y);
    st_Die();
    m_StepDone=true;
    break;
  default:
  m_OurLandscape->Warn("Beetle_Adult::Step()", "Beetle Adult Unknown State");
  g_msg->Warn(WARN_BUG, "State attempted was: ", int(CurrentBState));
  exit(1);
  }
}
//---------------------------------------------------------------------------

/**
Determines whether mortality has occured due to density-independen factors. Uses DailyAdultMort as a probability of
dying.\n
*/
bool Beetle_Adult::DailyMortality()
{
    auto mortality = getDailyMortalityRate();
    // by default we model a ground beetle, so the soil temperature is the one to be considered and maxtemp = mintemp = soiltemp
    auto maxtemp = getMaxTemp();
    auto mintemp = getMinTemp();
    if (((mintemp<getExtremeTempMin())||(maxtemp>getExtremeTempMax()))&&ExtremeTempIsOn()) mortality+=getExtremeTempMortality();
	if (g_rand_uni()<mortality)
	{
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------


/**
Determines whether mortality has occured due to density-dependence. Uses ADDepMort1 as a probability of
dying if adult beetles are found by GetMapDensity.\n
*/
bool Beetle_Adult::DDepMort()
{
//   Modified ***CJT*** 26-05-2009
   int range=getDDepthRange();
   int x=m_Location_x;
   int y=m_Location_y;
   // Find out if there is any danger of using wrap round
   if ((x<0)||(y<0)|| (x>=m_OurPopulation->SupplySimW() -range) ||(y>=m_OurPopulation->SupplySimW() -range))
   {
	   int diff = 0;
	   if (x<0) {
		   diff = abs(x);
	   } else if (x>=m_OurPopulation->SupplySimW() -range) diff = (x+range)-m_OurPopulation->SupplySimW();
	   if (y<0) {
		   if (diff< abs(y)) diff = abs(y);
	   } else if (y>=m_OurPopulation->SupplySimH() -range){
		   int diff2=(y+range)-m_OurPopulation->SupplySimH();
		   if (diff2>diff) diff=diff2;
	   }
	   range-=diff;
   }
   if (m_OurPopulation->m_AdultPosMap->GetMapDensity(m_Location_x-range,m_Location_y-range,range*2)>m_OurPopulation->ADDepMort0) {
		   if (g_rand_uni()<m_OurPopulation->ADDepMort1) return true; // die
	   }
   return false;
}
inline bool Beetle_Adult::IsSuitableForHibernation(TTypesOfVegetation tov) {
    if ((m_OurPopulation->beetleconstantslist->BeetleSuitableForHibernation).find(tov) != (m_OurPopulation->beetleconstantslist->BeetleSuitableForHibernation).end()){
        return true;
    }
    else
        return false;
}
inline bool Beetle_Adult::IsReproductionLandscape(TTypesOfLandscapeElement tole) {
    if ((m_OurPopulation->beetleconstantslist->BeetleReproductionLandscape).count(tole))
        return true;
    else
        return false;
}
inline bool Beetle_Adult::IsHalfReproductionLandscape(TTypesOfLandscapeElement tole) {
    if ((m_OurPopulation->beetleconstantslist->BeetleHalfReproductionLandscape).count(tole))
        return true;
    else
        return false;
}

//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//                      BEETLE_POPULATION_MANAGER
//---------------------------------------------------------------------------


void Beetle_Population_Manager::CreateObjects(int ob_type,
           TAnimal * /* pvo */ ,void* /* null */ ,std::unique_ptr<struct_Beetle> data,int number)
{
//   Beetle_Adult*  new_Adult;
//   Beetle_Pupae*  new_Pupa;
//  Beetle_Larvae* new_Larva;

   for (int i=0; i<number; i++)
   {
    if (ob_type == 0)
    {
        (*m_EList)[data->L->SupplyDayInYear()]->AddEgg(data->x, data->y);
    }
	if (ob_type == 1) {
		// Will not create a new larva in a square already occupied
		if (m_LarvaePosMap->GetMapValue( data->x, data->y ) == 0) {
			m_LarvaePosMap->SetMapValue( data->x, data->y );
			if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type )) {
				// We need to reuse an object
				dynamic_cast<Beetle_Larvae*>((SupplyAnimalPtr(ob_type,GetLiveArraySize(ob_type))))->ReInit(data->x, data->y, data->L, this);
				IncLiveArraySize(ob_type);
			}
			else {
                std::unique_ptr <Beetle_Larvae> new_Larva (new Beetle_Larvae(data->x, data->y, data->L, this));

				//(*TheArray_new)[ ob_type ].insert( (*TheArray_new)[ ob_type ].begin(), new_Larva );
				PushIndividual(ob_type, std::move(new_Larva));
				IncLiveArraySize(ob_type);
			}
		}
	}
    if (ob_type == 2)
    {
		if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type)) {
			// We need to reuse an object
			dynamic_cast<Beetle_Pupae*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
			IncLiveArraySize(ob_type);
		}
		else {
            std::unique_ptr <Beetle_Pupae> new_Pupa (new Beetle_Pupae(data->x, data->y, data->L, this));
			//new_Pupa = new Beetle_Pupae( data->x, data->y, data->L, this );
			PushIndividual(ob_type, std::move(new_Pupa));
			IncLiveArraySize(ob_type);
		}
    }
    if (ob_type == 3)
    {
		if (unsigned(SupplyListSize(ob_type))>GetLiveArraySize(ob_type)) {
			// We need to reuse an object
			dynamic_cast<Beetle_Adult*>(SupplyAnimalPtr(ob_type, GetLiveArraySize(ob_type)))->ReInit(data->x, data->y, data->L, this);
			IncLiveArraySize(ob_type);
		}
		else {
            std::unique_ptr <Beetle_Adult> new_Adult (new Beetle_Adult(data->x, data->y, data->L, this));
			//new_Adult = new Beetle_Adult( data->x, data->y, data->L, this );
			PushIndividual(ob_type, std::move(new_Adult));
			IncLiveArraySize(ob_type);
		}
    }
   }
}
//---------------------------------------------------------------------------

Beetle_Population_Manager::Beetle_Population_Manager(Landscape* p_L, int N)
//something after this line screws p_L
   : Population_Manager(p_L, N)

{



    beetleconstantslist = std::make_unique<BeetleConstantClass>();
  // Four lists are needed so need to remove 6 of the ten default arrays
  // Eggs, Larvae, Pupae & Adults

  //smartTListOfAnimals alist;
  //(*TheArray_new).insert( (*TheArray_new).end(), 12, alist );




   // something before this line screws the memory pointed by p_L <basically it happens after the exit of Population Manager and this line>
   // so that the allocation of the memory for the beetle_population_manager class happens on this memory
   // the constructor decides to put the Beetle_Population variables in this memory chunk
   // but why??? doesn't it know that this memory is taken.
  m_MoveMap=std::make_unique<MovementMap>(p_L, 0); // 0 for beetles
  m_LarvaePosMap=std::make_unique<SimplePositionMap>(p_L);
  m_AdultPosMap=std::make_unique<SimplePositionMap>(p_L);

  // Clear the m_DayDeg array
  m_EDayDeg.fill(0.);
  m_PDayDeg.fill(0.);
  /* not needed we initialize with {} in declaration
  for (int i=0; i<365; i++)
  {

    m_LDayDeg[i][0]=0;
    m_LDayDeg[i][1]=0;
    m_LDayDeg[i][2]=0;

  }
   */
  //beetleconstantslist->g_TotalEntries = 0;
  beetleconstantslist->g_AdultMaxMoveDist = beetleconstantslist-> p_cfg_MaxDailyMovement->value();
  /*
  vector<double> dist1;
  dist1.resize(g_AdultMaxMoveDist + 1); // +1 for zero distance
  dist1[1] = 2;
  for (int i = 2; i<= g_AdultMaxMoveDist; i++)
  {
	  dist1[i] = pow(dist1[i - 1],  p_cfg_BeetleDispersalSlope->value());
  }
  for (int i = 1; i <= g_AdultMaxMoveDist; i++) {
	  dist1[i] = (floor(dist1[i] + 0.5));
	  g_TotalEntries += dist1[i];
  }
  g_MoveDistribution.resize(g_TotalEntries);
  int count = 0;
  int dist = 1;
  for (int i = g_AdultMaxMoveDist; i>0; i--)
  {
	  for (int j = 0; j < dist1[i]; j++)
	  {
		  g_MoveDistribution[count++] = dist;
	  }
	  dist++;
  }
  */

  LDDepMort0=beetleconstantslist-> p_cfg_LDDepMort0->value();
  LDDepMort1=beetleconstantslist-> p_cfg_LDDepMort1->value()/100.0;
  ADDepMort0=beetleconstantslist-> p_cfg_ADDepMort0->value();
  ADDepMort1=beetleconstantslist-> p_cfg_ADDepMort1->value()/100.0;
  TodaysEggProduction=0;

 }
//---------------------------------------------------------------------------


Beetle_Population_Manager::~Beetle_Population_Manager()
{
  //for (int i=0; i<365; i++) delete m_EList[i];
 // delete m_MoveMap;
 // delete m_LarvaePosMap;
 // delete m_AdultPosMap;
  // DEBUG and testing
#ifdef BEMMOVETEST
    beetleconstantslist->g_movementdistancesout.close();
#endif
}
//---------------------------------------------------------------------------

/**
Initialises the population manager and creates the initial beetle population.\n
Housekeeping actions such as setting up probe files and recording life-stage and state names are done here.\n
An optimisation is to sort the larval list by x-coordinate to speed searching later. This removes the default
randomising function for larvae.\n
*/
void Beetle_Population_Manager::Init()
{
    m_EList = std::make_unique<std::vector <std::unique_ptr<Beetle_Egg_List>>>(365);
    // Create the egg lists

    for (int i=0; i<365; i++)
    {
        // std::cout << "DEBUG MESSAGE: m_EList creation: " << i << std::endl;
        (*m_EList)[i]=std::make_unique<Beetle_Egg_List>(i,this,m_TheLandscape);

    }
 // autom. called by constructor
	if ( cfg_RipleysOutput_used.value()) {
		OpenTheRipleysOutputProbe("");
	}
	if (  cfg_ReallyBigOutput_used.value() ) {
    OpenTheReallyBigProbe();
  } else ReallyBigOutputPrb=nullptr;
 m_SimulationName ="Beetle";
 // Create cfg_beetlestartnos adults

 for (int i=0; i<beetleconstantslist-> p_cfg_beetlestartnos->value(); i++)
 {
     std::unique_ptr <struct_Beetle> aps(new struct_Beetle);
     aps->BPM = this;
     aps->L = m_TheLandscape;
   do
   {

     aps->x = random(m_TheLandscape->SupplySimAreaWidth());
     aps->y = random(m_TheLandscape->SupplySimAreaHeight());
   } while (!IsStartHabitat(aps->x, aps->y));
   CreateObjects(3,nullptr,nullptr,std::move(aps),1);
 }
 m_AdPopSize=beetleconstantslist-> p_cfg_beetlestartnos->value();
 m_EPopSize=0;
 m_LPopSize=0;
 m_PPopSize=0;

// Load List of Animal Classes
  m_ListNames[0]="Egg";
  m_ListNames[1]="Larva";
  m_ListNames[2]="Pupa";
  m_ListNames[3]="Adult";
  m_ListNameLength = 4;
  m_population_type = TOP_Beetle;

// Load State Names
  StateNames[tobs_Initiation] = "Initiation";
//Egg
  StateNames[tobs_EDeveloping] = "Developing";
  StateNames[tobs_Hatching] = "Hatching";
  StateNames[tobs_EDying] = "Dying";
//Larva
  StateNames[tobs_LDeveloping] = "Developing";
  StateNames[tobs_Pupating] = "Pupating";
  StateNames[tobs_LDying] = "Dying";
//Pupa
  StateNames[tobs_PDeveloping] = "Developing";
  StateNames[tobs_Emerging] = "Emerging";
  StateNames[tobs_PDying] = "Dying";
//Adult
  StateNames[tobs_Foraging] = "Foraging";
  StateNames[tobs_Aggregating] = "Aggregating";
  StateNames[tobs_Hibernating] = "Hibernating";
  StateNames[tobs_Dispersing] = "Dispersing";
  StateNames[tobs_ADying] = "Dying";

  // Ensure that larvae are sorted w.r.t. x position not shuffled
  BeforeStepActions[1]=1; // 1 = SortX

#ifdef __RECORD_RECOVERY_POLYGONS
	/* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::out);
	ofile << "This file records the number of females in each polygon each day" << endl;
	ofile.close();
	/* Open the polygon recovery file and read in polygons to m_RecoveryPolygons */
	ifstream ifile("RecoveryPolygonsList.txt",ios::in);
	int n;
	ifile >> n;
	m_RecoveryPolygons[0] = n;
	for (int i=0; i<n; i++) ifile >> m_RecoveryPolygons[1+i];
	for (int i=0; i<n; i++) m_RecoveryPolygonsC[1+i]=0;
	ifile.close();
#endif

#ifdef __BEETLEPESTICIDE1
	PestMortLocOutputOpen();
	m_InFieldNo = 0;
	m_OffFieldNo = 0;
	m_InCropNo = 0;
	m_InCropRef = m_TheLandscape->TranslateVegTypes(beetleconstantslist->p_cfg_InCropRef->value());
	if (beetleconstantslist->p_cfg_SaveInfieldLocation->value()) LocOutputOpen();
#endif
	//Beetle_Adult ba(0, 0, nullptr, nullptr);
	// Initialise any static variables
	Beetle_Adult::SetAdultPPPElimRate(beetleconstantslist-> p_cfg_BemAdultPPPElimiationRate->value());  // Initialize static variable
	Beetle_Adult::SetAPPPThreshold(beetleconstantslist-> p_cfg_BemAdultPPPThreshold->value());  // Initialize static variable
	Beetle_Adult::SetAPPPEffectProb(beetleconstantslist-> p_cfg_BemAdultPPPEffectProb->value());  // Initialize static variable
	Beetle_Adult::SetAPPPEffectProbDecay(beetleconstantslist-> p_cfg_BemAdultPPPEffectProbDecay->value());  // Initialize static variable

	//Beetle_Pupae bp(0, 0, nullptr, nullptr);
	// Initialise any static variables
	Beetle_Pupae::SetPupalPPPElimRate(beetleconstantslist-> p_cfg_BemPupalPPPElimiationRate->value());  // Initialize static variable
	Beetle_Pupae::SetPPPPThreshold(beetleconstantslist-> p_cfg_BemPupalPPPThreshold->value());  // Initialize static variable
	Beetle_Pupae::SetPPPPEffectProb(beetleconstantslist-> p_cfg_BemPupalPPPEffectProb->value());  // Initialize static variable
	Beetle_Pupae::SetPPPPEffectProbDecay(beetleconstantslist-> p_cfg_BemPupalPPPEffectProbDecay->value());  // Initialize static variable
	//Beetle_Larvae bl(0, 0, nullptr, nullptr);
	// Initialise any static variables
    Beetle_Larvae::SetLarvalPPPElimRate(beetleconstantslist-> p_cfg_BemLarvalPPPElimiationRate->value());  // Initialize static variable
	Beetle_Larvae::SetLPPPThreshold(beetleconstantslist-> p_cfg_BemLarvalPPPThreshold->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPEffectProb(beetleconstantslist-> p_cfg_BemLarvalPPPEffectProb->value());  // Initialize static variable
    Beetle_Larvae::SetLPPPEffectProbDecay(beetleconstantslist-> p_cfg_BemLarvalPPPEffectProbDecay->value());  // Initialize static variable
}
//---------------------------------------------------------------------------
#ifdef __RECORD_RECOVERY_POLYGONS
void Beetle_Population_Manager::RecordRecoveryPolygons()
{
	/**
	Only used for recovery pesticide experiment.
	Loops through all beetle and records the numbers in any polygon identified as needing to be recorded in m_RecoveryPolygons[]
	*/
	int sz = (int) GetLiveArraySize[bob_Adult];
    for (int j=0; j<sz; j++)
    {
      int p = (*TheArray_new)[bob_Adult][j]->SupplyPolygonRef();
	  for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	  {
		  if (p == m_RecoveryPolygons[i]) {
			  m_RecoveryPolygonsC[i]++;
			  break;
		  }
	  }
    }
	/* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::app);
	for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	{
		ofile << m_RecoveryPolygonsC[i] << '\t';
		m_RecoveryPolygonsC[i] = 0;
	}
	ofile << endl;
	ofile.close();
}
//---------------------------------------------------------------------------
#endif


bool Beetle_Population_Manager::IsStartHabitat(int a_x, int a_y)
{
    TTypesOfLandscapeElement tole;
    tole=m_TheLandscape->GetOwner_tole(a_x, a_y);
    return IsStartHabitatAux(tole);
}

void Beetle_Population_Manager::DoFirst()
{
	/**
	This method removes any young stages that have survived until winter.\n
	It subsequently calculates day degree development for each day for each beetle stage. This is an optimising
	strategy to prevent each beetle individual calculating these.\n
	Finally it replaces the Egg_List BeginStep functionality. This is also an optimisation since it only requires
	365 Egg_lists, one for each day of the year instead of millions of individual eggs.\n
	NB this breaks the traditional ALMaSS protocol for handling animal individuals.\n
	*/
#ifdef __RECORD_RECOVERY_POLYGONS
	/**
	There is also a special bit of code that is used for pesticide tests
	*/
	RecordRecoveryPolygons();
#endif
  int today=m_TheLandscape->SupplyDayInYear();
  // WHOOAA!! This is really dangerous if these objects are registered anywhere
  // else in the system
  if (today==364)
  {
	  for (unsigned j = 0; j<GetLiveArraySize( bob_Egg ); j++)
    {
      SupplyAnimalPtr(bob_Egg, j)->KillThis(); // Destroy this
    }
	  for (unsigned j = 0; j<GetLiveArraySize( bob_Larva ); j++)
    {
      SupplyAnimalPtr(bob_Larva, j)->KillThis(); // Destroy this
    }
	  for (unsigned j = 0; j<GetLiveArraySize( bob_Pupa ); j++)
    {
      SupplyAnimalPtr(bob_Pupa, j)->KillThis(); // Destroy this
    }
  }
  else if (today==0)
  {
      m_EDayDeg = {};
      m_LDayDeg ={};
      m_PDayDeg ={};
   for (int i=0; i<365; i++)
   {
       (*m_EList)[i]->EggList.clear();
       /*
        m_EDayDeg[i]=0;
        m_LDayDeg[i][0]=0;
        m_LDayDeg[i][1]=0;
        m_LDayDeg[i][2]=0;
        m_PDayDeg[i]=0;
        */
   }
  }

  double temptoday=m_TheLandscape->SupplyTemp();
  double soiltemptoday= m_TheLandscape->SupplySoilTemp();
  // Calculate the number of eggs laid today
  if (temptoday>beetleconstantslist->AdultEggLayingThreshold)
    TodaysEggProduction=(int) floor(0.5+(((double)temptoday-
                          (double)beetleconstantslist->AdultEggLayingThreshold)*beetleconstantslist->EggProductionSlope));
  else TodaysEggProduction=0;
  if (TodaysEggProduction>10)TodaysEggProduction=10; // ensure no bigger than 10
  // Calculated the DayDegrees for the different stages today
  if (soiltemptoday>beetleconstantslist->DevelConst1)
  {
   soiltemptoday-=beetleconstantslist->DevelConst1; // Subtract the threshold
   if (soiltemptoday>beetleconstantslist->DevelopmentInflectionPoint)
   {
    for(int i=0; i<=today; i++)
    {
      double et,l1t,l2t,l3t,pt;
      et=soiltemptoday*beetleconstantslist->above12Egg;
      l1t=soiltemptoday*beetleconstantslist->above12Larvae->at(0);
      l2t=soiltemptoday*beetleconstantslist->above12Larvae->at(1);
      l3t=soiltemptoday*beetleconstantslist->above12Larvae->at(2);
      pt=soiltemptoday*beetleconstantslist->above12Pupae;
      m_EDayDeg[i]+=et;
      m_LDayDeg[0][i]+=l1t;
      m_LDayDeg[1][i]+=l2t;
      m_LDayDeg[2][i]+=l3t;
      m_PDayDeg[i] += pt;
    }
   }
   else
   {
    for(int i=0; i<=today; i++)
    {
      m_EDayDeg[i]+=soiltemptoday;
      m_LDayDeg[0][i]+=soiltemptoday;
      m_LDayDeg[1][i]+=soiltemptoday;
      m_LDayDeg[2][i]+=soiltemptoday;
      m_PDayDeg[i]+=soiltemptoday;
    }
   }
  }
  // Store the current population sizes
  m_EPopSize=0;
    for ( int i = 0; i < today; i++ ) {
        m_EPopSize += SupplyDailyEggPopSize(i);
    }
  //m_EPopSize = (int)GetLiveArraySize( bob_Egg );
  m_LPopSize = (int)GetLiveArraySize( bob_Larva );
  m_PPopSize = (int)GetLiveArraySize( bob_Pupa );
  m_AdPopSize = (int)GetLiveArraySize( bob_Adult);

#ifdef __LAMBDA_RECORD
  if (today==March) {
	  LamdaDumpOutput();
	  LamdaClear();
  }
#endif

#ifdef __BEETLEPESTICIDE1
  if (today==0) PestMortLocOutput();
#endif

  // This replaces the BeginStep function for the eggs
  for (int i=0; i<365; i++)
  {
      (*m_EList)[i]->BeginStep();
  }
}
//---------------------------------------------------------------------------

void Beetle_Population_Manager::DoBefore()
{
  // This replaces the step function for the eggs
  for (int i=0; i<365; i++) (*m_EList)[i]->SetStepDone(false);
  for (int i=0; i<365; i++) (*m_EList)[i]->Step();

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

/*
class ReturnLessThanX
{
  public:
    bool operator()(TAnimal* A1, int x) const
    {
      return (A1->Supply_m_Location_x()<x);
    }
};
*/
//---------------------------------------------------------------------------
/*
class ReturnMoreThanX
{
  public:
    bool operator()(int x,TAnimal* A1) const
    {
      return (A1->Supply_m_Location_x()>x);
    }
};
*/
//---------------------------------------------------------------------------

/**
 Outputs the adult locations if needed \n
*/

//---------------------------------------------------------------------------

/**
The parent class Probe method needs to be overridden because of the use of
the Egg_List class. Otherwise functionality is the same as parent class.\n
*/
long Beetle_Population_Manager::Probe(int ListIndex,probe_data* p_TheProbe)
{
  // Counts through the list and goes through each area to see if the animal
  // is standing there and if the farm, veg or element conditions are met
  AnimalPosition Sp{};
  long NumberSk = 0;
  if (ListIndex==0)
  {
     int X;
     int Y;
     // Four possibilites
     // either NoVegTypes or NoElementTypes or NoFarmTypes is >0 or all==0
     if (p_TheProbe->m_NoFarms!=0)
     {
       for (int n=0; n<365; n++)
       {
        for (auto & j : (*m_EList)[n]->EggList)
        {
         Y=j.m_y;
         X=j.m_x;
         unsigned Farm=m_TheLandscape->SupplyFarmOwner(X,Y);
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((X>=p_TheProbe->m_Rect[i].m_x1)
            && (Y>=p_TheProbe->m_Rect[i].m_y1)
            && (X<=p_TheProbe->m_Rect[i].m_x2)
            && (Y<=p_TheProbe->m_Rect[i].m_y2))
              for (unsigned k=0; k<p_TheProbe->m_NoFarms; k++)
              {
                if (p_TheProbe->m_RefFarms[k]==Farm)
                NumberSk++; // it is in the square so increment number
              }
         }
        }
       }
     }
     else if (p_TheProbe->m_NoEleTypes!=0)
     {
       for (int n=0; n<365; n++)
       {
        for (auto & j : (*m_EList)[n]->EggList)
        {
         Y=j.m_y;
         X=j.m_x;
         int EleType = (int)m_TheLandscape->GetOwner_tole(X,Y);
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((X>=p_TheProbe->m_Rect[i].m_x1)
            && (Y>=p_TheProbe->m_Rect[i].m_y1)
            && (X<=p_TheProbe->m_Rect[i].m_x2)
            && (Y<=p_TheProbe->m_Rect[i].m_y2))
              for (unsigned k=0; k<p_TheProbe->m_NoEleTypes; k++)
              {
                if (p_TheProbe->m_RefEle[k]==EleType)
                NumberSk++; // it is in the square so increment number
              }
         }
        }
       }
     }
     else if (p_TheProbe->m_NoVegTypes!=0)
     {
       for (int n=0; n<365; n++)
       {
        for (auto & j : (*m_EList)[n]->EggList)
        {
         Y=j.m_y;
         X=j.m_x;
         int VegType = (int)m_TheLandscape->SupplyVegType(X,Y);
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((X>=p_TheProbe->m_Rect[i].m_x1)
            && (Y>=p_TheProbe->m_Rect[i].m_y1)
            && (X<=p_TheProbe->m_Rect[i].m_x2)
            && (Y<=p_TheProbe->m_Rect[i].m_y2))
               for (unsigned k=0; k<p_TheProbe->m_NoVegTypes; k++)
               {
                 if (p_TheProbe->m_RefVeg[k]==VegType)
                    NumberSk++; // it is in the square so increment number
               }
             }
           }
         }
     }
     else // both must be zero
     {
         if (p_TheProbe->m_FullLandscapeProbe){
             NumberSk = m_EPopSize;
         }
         for (int n=0; n<365; n++)
         {
           for (auto & j : (*m_EList)[n]->EggList)
           {
             Y=j.m_y;
             X=j.m_x;
             for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
             {
               if ((X>=p_TheProbe->m_Rect[i].m_x1)
                && (Y>=p_TheProbe->m_Rect[i].m_y1)
                && (X<=p_TheProbe->m_Rect[i].m_x2)
                && (Y<=p_TheProbe->m_Rect[i].m_y2))
                  NumberSk++; // it is in the square so increment number

             }
           }
         }
      }
  }
  else
  {
     // Four possibilites
     // either NoVegTypes or NoElementTypes or NoFarmTypes is >0 or all==0
     if (p_TheProbe->m_NoFarms!=0)
     {
		 for (unsigned j = 0; j<GetLiveArraySize( ListIndex ); j++)
       {
         Sp=SupplyAnimalPtr(ListIndex, j)->SupplyPosition();
         unsigned Farm=SupplyAnimalPtr(ListIndex, j)->SupplyFarmOwnerRef();
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((Sp.m_x>=p_TheProbe->m_Rect[i].m_x1)
            && (Sp.m_y>=p_TheProbe->m_Rect[i].m_y1)
            && (Sp.m_x<=p_TheProbe->m_Rect[i].m_x2)
            && (Sp.m_y<=p_TheProbe->m_Rect[i].m_y2))
              for (unsigned k=0; k<p_TheProbe->m_NoFarms; k++)
              {
                if (p_TheProbe->m_RefFarms[k]==Farm)
                NumberSk++; // it is in the square so increment number
              }
         }
       }
     }
     else if (p_TheProbe->m_NoEleTypes!=0)
     {
		 for (unsigned j = 0; j < (unsigned) GetLiveArraySize( ListIndex ); j++)
       {
         Sp=SupplyAnimalPtr(ListIndex, j)->SupplyPosition();
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((Sp.m_x>=p_TheProbe->m_Rect[i].m_x1)
            && (Sp.m_y>=p_TheProbe->m_Rect[i].m_y1)
            && (Sp.m_x<=p_TheProbe->m_Rect[i].m_x2)
            && (Sp.m_y<=p_TheProbe->m_Rect[i].m_y2))
              for (unsigned k=0; k<p_TheProbe->m_NoEleTypes; k++)
              {
                if (p_TheProbe->m_RefEle[k]==Sp.m_EleType)
                NumberSk++; // it is in the square so increment number
              }
         }
       }
     }
     else
     {
       if (p_TheProbe->m_NoVegTypes!=0)
       {
		   for (unsigned j = 0; j<(unsigned)GetLiveArraySize( ListIndex ); j++)
         {
           Sp=SupplyAnimalPtr(ListIndex, j)->SupplyPosition();
           for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
           {
             if ((Sp.m_x>=p_TheProbe->m_Rect[i].m_x1)
                 && (Sp.m_y>=p_TheProbe->m_Rect[i].m_y1)
                  && (Sp.m_x<=p_TheProbe->m_Rect[i].m_x2)
                   && (Sp.m_y<=p_TheProbe->m_Rect[i].m_y2))

             {
               for (unsigned k=0; k<p_TheProbe->m_NoVegTypes; k++)
               {
                 if (p_TheProbe->m_RefVeg[k]==Sp.m_VegType)
                    NumberSk++; // it is in the square so increment number
               }
             }
           }
         }
       }
       else // both must be zero
       {
           if (p_TheProbe->m_FullLandscapeProbe){
                NumberSk = GetLiveArraySize( ListIndex );
           }
           else{
               for (unsigned j = 0; j<(unsigned)GetLiveArraySize( ListIndex ); j++)
               {

                   Sp=SupplyAnimalPtr(ListIndex, j)->SupplyPosition();
                   for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
                   {
                       if ((Sp.m_x>=p_TheProbe->m_Rect[i].m_x1)
                           && (Sp.m_y>=p_TheProbe->m_Rect[i].m_y1)
                           && (Sp.m_x<=p_TheProbe->m_Rect[i].m_x2)
                           && (Sp.m_y<=p_TheProbe->m_Rect[i].m_y2))
                           NumberSk++; // it is in the square so increment number
                   }
               }
           }

       }
     }
  }
     return NumberSk;
}
//---------------------------------------------------------------------------




/**
A spatial output. This dumps the x,y co-ordinates for all adults to a text file.\n
*/
void Beetle_Population_Manager::TheRipleysOutputProbe( FILE* a_prb ) {
  //Beetle_Adult* FS;//should use shared pointer instead

  auto totalF = (unsigned)GetLiveArraySize( bob_Adult );
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
		  if (SupplyAnimalPtr(bob_Adult, j)->GetCurrentStateNo() != -1)
          {
              //todo: don't use SupplyAnimalPointer or move to the base class (SupplyAnimalPointer makes use of the real pointer under the smart pointer--> unsafe)
			  x = SupplyAnimalPtr(bob_Adult, j)->Supply_m_Location_x();
			  y = SupplyAnimalPtr(bob_Adult, j)->Supply_m_Location_y();
			  fprintf(a_prb, "%d\t%d\n", x, y);
		  }
  }
  fflush(a_prb);
}
//-----------------------------------------------------------------------------

/**
Another spatial output method. This time with extra information over and above the x,y, coord found in the Ripley Probe.\n
*/
void Beetle_Population_Manager::TheReallyBigOutputProbe(  ) {
  //Beetle_Adult* FS;
  auto totalF = (unsigned)GetLiveArraySize( bob_Adult );// previously here was a dynamic cast to Beetle_Adult
    // However we do not use any beetle functionality here

  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(ReallyBigOutputPrb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
      std::shared_ptr<TAnimal> FS={SupplyAnimalSmartPtr(3, j)};
	  if (FS->GetCurrentStateNo() != -1) {
          SupplyLocXY(3, j, x, y);
		  int poly = m_TheLandscape->SupplyPolyRef(x, y);
		  fprintf(ReallyBigOutputPrb, "%d\t%d\t%d\n", x, y, poly);
	  }
  }
  fflush(ReallyBigOutputPrb);
}
void Beetle_Population_Manager::TheAOROutputProbe()
{
    std::string IsExclusiveStr{beetleconstantslist->p_cfg_AORProbeIsExclusive->value()};
    m_AOR_Probe->DoProbe(bob_Adult);
    if (beetleconstantslist->p_cfg_InfieldProbeIsOn->value()){
        if (IsExclusiveStr=="yes"||
                (IsExclusiveStr=="no")) {
            m_AOR_Probe_inField->DoProbe(bob_Adult);
            m_AOR_Probe_outField->DoProbe(bob_Adult);
        }
        if(IsExclusiveStr=="both"){
                m_AOR_Probe_inField->DoProbe(bob_Adult);
                m_AOR_Probe_outField->DoProbe(bob_Adult);
                m_AOR_Probe_inField_additional->DoProbe(bob_Adult);
                m_AOR_Probe_outField_additional->DoProbe(bob_Adult);
        }

    }
}
void Beetle_Population_Manager::OpenTheAOROutputProbe(string a_AORFilename) {
    m_AORProbeFileName= a_AORFilename;
    m_AOR_Probe = new AOR_Probe_Beetle(this, m_TheLandscape, a_AORFilename);
    if (beetleconstantslist->p_cfg_InfieldProbeIsOn->value()){
        CfgStr cfg_AORProbeNameInField("BEETLE_INFIELD_AOR_PROBE_NAME", CFG_CUSTOM, "InField_AOR_Probe.txt");
        CfgStr cfg_AORProbeNameOutField("BEETLE_OUTFIELD_AOR_PROBE_NAME", CFG_CUSTOM, "OutField_AOR_Probe.txt");

        CfgStr cfg_AORProbeNameInFieldEx("BEETLE_EX_INFIELD_AOR_PROBE_NAME", CFG_CUSTOM, "Exclusive_InField_AOR_Probe.txt");
        CfgStr cfg_AORProbeNameOutFieldEx("BEETLE_EX_OUTFIELD_AOR_PROBE_NAME", CFG_CUSTOM, "Exclusive_OutField_AOR_Probe.txt");
        std::string probefilename1;
        std::string probefilename2;
        std::string IsExclusiveStr{beetleconstantslist->p_cfg_AORProbeIsExclusive->value()};
        bool IsExclusive;

        if (IsExclusiveStr =="no" ){
            IsExclusive = false;
            probefilename1 = cfg_AORProbeNameInField.value();
            probefilename2 = cfg_AORProbeNameOutField.value();
        }
        else{
            if (IsExclusiveStr == "yes" ){
                IsExclusive = true;
                probefilename1 = cfg_AORProbeNameInFieldEx.value();
                probefilename2 = cfg_AORProbeNameOutFieldEx.value();
            }
            else{
                IsExclusive = true;
                m_AOR_Probe_inField_additional = std::make_unique<AOR_Probe_Beetle>(this, m_TheLandscape, cfg_AORProbeNameInFieldEx.value(), inField, IsExclusive);
                m_AOR_Probe_outField_additional = std::make_unique<AOR_Probe_Beetle>(this, m_TheLandscape, cfg_AORProbeNameOutFieldEx.value(), outField, IsExclusive);
                IsExclusive = false;
                probefilename1 = cfg_AORProbeNameInField.value();
                probefilename2 = cfg_AORProbeNameOutField.value();
            }

        }

        m_AOR_Probe_inField = std::make_unique<AOR_Probe_Beetle>(this, m_TheLandscape, probefilename1, inField, IsExclusive);
        m_AOR_Probe_outField = std::make_unique<AOR_Probe_Beetle>(this, m_TheLandscape, probefilename2, outField, IsExclusive);


    }

}
unsigned Beetle_Population_Manager::SupplyTilePopulation(unsigned x, unsigned y, int lifestage) {
    if (lifestage == bob_Adult) {
        return m_AdultPosMap->GetMapValue(x, y);
    } else {
        if (lifestage == bob_Larva) {
            return m_LarvaePosMap->GetMapValue(x, y);
        } else {
            g_msg->Warn("Beetle_Population_Manager::SupplyTilePopulation ", "Non-mapped BeetleObject");
            exit(-1);
        }
    }
}

//-----------------------------------------------------------------------------

/**
	This method simply alters populations on 1st January - either by increasing or decreasing them. Increases are by
	'cloning', decrease by pro-rata mortality. The effect is under the control of configuration variables cfg_pm_eventfrequency
	and cfg_pm_eventsize.\n
*/
void Beetle_Population_Manager::Catastrophe( ) {
    int year = m_TheLandscape->SupplyYearNumber();
    if (year < cfg_CatastropheEventStartYear.value()) return;

	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=1) return;
	// First do we have the right year?
	year = m_TheLandscape->SupplyYearNumber()- cfg_CatastropheEventStartYear.value();
	if (year%cfg_pm_eventfrequency.value()!=0) return;

	Beetle_Adult* BA;
	// Now if the % decrease is higher or lower than 100 we need to do different things
	int esize=cfg_pm_eventsize.value();
	if (esize<100) {
		unsigned size2;
		size2 = (unsigned)GetLiveArraySize( bob_Adult );
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) > esize) {
				    // This is rightmost ugly! But I cannot find any nice way to cast an
                        BA = dynamic_cast < Beetle_Adult * > ( SupplyAnimalPtr(bob_Adult, j));
					    BA->CurrentBState=tobs_ADying; // Kill it
				}
			}
		}
	else if (esize>100) {
		// This also requires a copy method in the target animals
		// esize also needs translating  120 = 20%, 200 = 100%
		if (esize<200) {
			esize-=100;
			unsigned size2;
			size2 = (unsigned)GetLiveArraySize( bob_Adult );
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) < esize) {
                        BA = dynamic_cast < Beetle_Adult * > ( SupplyAnimalPtr(bob_Adult, j) );
					    BA->CopyMyself(bob_Adult); // Duplicate it
					}
				}
		} else {
			esize-=100;
			esize/=100; // this will throw away fractional parts so will get 1, 2, 3  from 200, 350 400
				unsigned size2;
				size2 = (unsigned)GetLiveArraySize( bob_Adult );
				for ( unsigned j = 0; j < size2; j++ ) {
					for ( int e=0; e<esize; e++) {
						BA = dynamic_cast < Beetle_Adult * > (SupplyAnimalPtr(bob_Adult, j));
						BA->CopyMyself(bob_Adult); // Duplicate it
						}
				}
			}
		}
	else return; // No change so do nothing
}

void Beetle_Population_Manager::Catastrophe2()
{
    // First do we have the right day?
    int today = m_TheLandscape->SupplyDayInYear();
    if (today != cfg_pm_eventday.value()) return;
    // First do we have the right year?
    int year = m_TheLandscape->SupplyYearNumber();
    if (year < cfg_CatastropheEventStartYear.value()) return;
    if (year % cfg_pm_eventfrequency.value() != 0) return;

    Beetle_Adult* BA;
    // Now if the % decrease is higher or lower than 100 we need to do different things
    int esize = cfg_pm_eventsize.value();
    if (esize < 100) {
        unsigned size2;
        size2 = (unsigned)GetLiveArraySize(bob_Adult);
        for (unsigned j = 0; j < size2; j++) {
            if (random(100) >= esize) {
                // This is rightmost ugly! But I cannot find any nice way to cast an
                BA = dynamic_cast <Beetle_Adult*> (SupplyAnimalPtr(bob_Adult, j));
                if (BA->SupplyPolygonType()!=tole_Field)  BA->CurrentBState = tobs_ADying; // Kill it
            }
        }
    }
    else return; // No change so do nothing
}

/*
void Beetle_Population_Manager::LOG( const char* fname ) {
  FILE * PFile = fopen(fname, "w" );
  if (PFile) {
	  m_TheLandscape->Warn("PopulationManager::LOG - Could not open file ",fname);
	  exit(0);
  }
  AnimalPosition AP;
  for ( unsigned listindex = 0; listindex < (*TheArray_new).size(); listindex++ ) {
#ifndef __BCB__
    fprintf( PFile, "%s :\n", m_ListNames[ listindex ] );
#else
    fprintf( PFile, "%s :\n", m_ListNames[ listindex ].c_str() );
#endif
    for ( unsigned j = 0; j < (unsigned) m_LiveArraySize[listindex]; j++ ) {
      AP = (*TheArray_new)[ listindex ] [ j ]->SupplyPosition();
      fprintf( PFile, "%i %i %i\n", j, AP.m_x, AP.m_y );
    }
  }
  fclose( PFile );
}
 */


void Beetle_Population_Manager::Run( int NoTSteps ) {

  for ( int TSteps = 0; TSteps < NoTSteps; TSteps++ )
  {
	  unsigned size2;
	  auto size1 =  (unsigned) SupplyListIndexSize();

	  for ( unsigned listindex = 0; listindex < size1; listindex++ )
	  {
		  // Must check each object in the list for m_CurrentStateNo==-1
		  m_LiveArraySize[listindex] = PartitionLiveDead(listindex);
	  }
#ifdef __ALMASS_VISUAL
	  if (m_MainForm!=NULL)
	  {
		  int n = 0;
		  for ( unsigned listindex = 0; listindex < size1; listindex++ ) n += (int) m_LiveArraySize[ listindex ];

		  //if (n>0) DisplayLocations();
	  }
#endif
	  // begin step actions ...
	  // set all stepdone to false.... is this really necessary??
	  for ( int listindex = 0; listindex < size1; listindex++ )
	  {
		  size2 = (unsigned) GetLiveArraySize(listindex);
		  for ( unsigned j = 0; j < size2; j++ )
		  {
			  SupplyAnimalSmartPtr(listindex, j)->SetStepDone( false );
		  }
	  }

	  for ( unsigned listindex = 0; listindex < size1; listindex++ ) {
		  // Call the Shuffle/Sort procedures
		  Shuffle_or_Sort( listindex );
	  }
	  // Need to check if Ripleys Statistic needs to be saved
	  if (cfg_RipleysOutput_used.value()) {
		  int Year = m_TheLandscape->SupplyYearNumber();
		  if (Year >= cfg_RipleysOutputFirstYear.value()) {
			  if (Year % cfg_RipleysOutput_interval.value() == 0) {
				  int day = m_TheLandscape->SupplyDayInYear();
				  if (cfg_RipleysOutput_day.value() == day) {
					  // Do the Ripley Probe
					  TheRipleysOutputProbe(RipleysOutputPrb);
				  }
			  }
		  }
	  }
	  // Need to check if AOR output needs to be saved
	  if (cfg_AOROutput_used.value()) {
		  int Year = m_TheLandscape->SupplyYearNumber();
		  if (Year >= cfg_AOROutputFirstYear.value()) {
			  if (Year % cfg_AOROutput_interval.value() == 0) {
				  int day = m_TheLandscape->SupplyDayInYear();
				  if (cfg_AOROutput_day.value() == day) {
					  // Do the AOR Probe
					  TheAOROutputProbe();
				  }
			  }
		  }
	  }
	  // Need to check if Monthly Ripleys Statistic needs to be saved
    if ( cfg_RipleysOutputMonthly_used.value() ) {
		if (m_TheLandscape->SupplyDayInMonth()==1) {
			int Year = m_TheLandscape->SupplyYearNumber();
			if (Year>=cfg_RipleysOutputFirstYear.value()) {
				if ( Year % cfg_RipleysOutput_interval.value() == 0 ) {
					int month = m_TheLandscape->SupplyMonth();
					// Do the Ripley Probe
					switch (month) {
						case 1: TheRipleysOutputProbe( RipleysOutputPrb1 );
							break;
						case 2: TheRipleysOutputProbe( RipleysOutputPrb2 );
							break;
						case 3: TheRipleysOutputProbe( RipleysOutputPrb3 );
							break;
						case 4: TheRipleysOutputProbe( RipleysOutputPrb4 );
							break;
						case 5: TheRipleysOutputProbe( RipleysOutputPrb5 );
							break;
						case 6: TheRipleysOutputProbe( RipleysOutputPrb6 );
							break;
						case 7: TheRipleysOutputProbe( RipleysOutputPrb7 );
							break;
						case 8: TheRipleysOutputProbe( RipleysOutputPrb8 );
							break;
						case 9: TheRipleysOutputProbe( RipleysOutputPrb9 );
							break;
						case 10: TheRipleysOutputProbe( RipleysOutputPrb10 );
							break;
						case 11: TheRipleysOutputProbe( RipleysOutputPrb11 );
							break;
						case 12: TheRipleysOutputProbe( RipleysOutputPrb12 );
							break;
                        default:
                            g_msg->Warn("Beetle_Population_Manager::Run ","Wrong month");
                            exit(-1);
					}
				}
			}
		}
	}
    // Need to check if Really Big Probe needs to be saved
    if ( cfg_ReallyBigOutput_used.value() ) {
      int Year = m_TheLandscape->SupplyYearNumber();
	  if (Year>=cfg_ReallyBigOutputFirstYear.value()) {
		  if ( Year % cfg_ReallyBigOutput_interval.value() == 0 ) {
			int day = m_TheLandscape->SupplyDayInYear();
			if (( cfg_ReallyBigOutput_day1.value() == day )|| ( cfg_ReallyBigOutput_day2.value() == day )|| ( cfg_ReallyBigOutput_day3.value() == day )|| ( cfg_ReallyBigOutput_day4.value() == day )||( cfg_ReallyBigOutput_day1.value() == -1 ))
			{
			  // Do the Ripley Probe
			  TheReallyBigOutputProbe();
			}
		  }
		}
	}
	int yr=m_TheLandscape->SupplyYearNumber();
	/* Position debugging messages
	std::cout <<"\n";
	std::cout << "BB\t" << m_TheLandscape->SupplyGlobalDate() << "\t"<< (*(*TheArray_new.get())[3][7].get()).Supply_m_Location_y() << "\t"<< (*(*TheArray_new.get())[3][15].get()).Supply_m_Location_y() <<"\t"<< (*(*TheArray_new.get())[3][48].get()).Supply_m_Location_y() << "\n";
    */
	RunStepMethods();

  } // End of time step loop
}
/**
 * \brief Function that includes all step-methods
 *
 * Has been introduced in order to reduce code replication
 * */
void Beetle_Population_Manager::RunStepMethods() {
    unsigned size2;
    auto size1 =  (unsigned) SupplyListIndexSize();
    DoFirst();
    // call the begin-step-method of all objects
    for ( int listindex = 0; listindex < size1; listindex++ ) {
        size2 = (unsigned)GetLiveArraySize( listindex );
        for (unsigned j = 0; j < size2; j++)
            SupplyAnimalSmartPtr(listindex, j)->BeginStep();
    }
    DoBefore();
    // call the step-method of all objects
    do {
        for ( int listindex = 0; listindex < size1; listindex++ ) {
            size2 = (unsigned)GetLiveArraySize( listindex );
            for (unsigned j = 0; j < size2; j++) {
                SupplyAnimalSmartPtr(listindex, j)->Step();
            }
        } // for listindex
    } while ( !StepFinished() );
    DoAfter();
    // call the end-step-method of all objects
    for ( int listindex = 0; listindex < size1; listindex++ ) {
        size2 = (unsigned)GetLiveArraySize( listindex );
        for (unsigned j = 0; j < size2; j++) {
            SupplyAnimalSmartPtr(listindex, j)->EndStep();
        }
    }
    // ----------------
    // end of this step actions
    // For each animal list
    
    DoLast();
}
bool Beetle_Population_Manager::StepFinished( ) {
  for ( int listindex = 0; listindex < SupplyListIndexSize(); listindex++ ) {
          for (unsigned j = 0; j < (unsigned)GetLiveArraySize( listindex ); j++) {
      if ( SupplyAnimalSmartPtr(listindex, j)->GetStepDone() == false ) {
        return false;
      }
    }
  }
  return true;
}

void Beetle_Population_Manager::DoLast() {

/*
#ifdef __UNIX__
  for ( unsigned i = 0; i < 100; i++ ) {
    StateList.n[ i ] = 0;
  }
#else
  // Zero results
  for ( unsigned listindex = 0; listindex < m_ListNameLength; listindex++ ) {
    for ( unsigned i = 0; i < 100; i++ ) {
      Counts[ listindex ] [ i ] = 0;
    }
  }
#endif
  // How many animals in each state?
  for ( unsigned listindex = 0; listindex < (*TheArray_new).size(); listindex++ ) {
    unsigned size = (unsigned) (*TheArray_new)[ listindex ].size();
    for ( unsigned j = 0; j < size; j++ ) {
#ifdef __UNIX__
      StateList.n[ (*TheArray_new)[ listindex ] [ j ]->WhatState() ] ++;
#else
      Counts[ listindex ] [ (*TheArray_new)[ listindex ] [ j ]->WhatState() ] ++;
#endif
    }
  }
*/
#ifdef __BEETLEPESTICIDE1
    int today=m_TheLandscape->SupplyDayInYear();
	if (beetleconstantslist->p_cfg_SaveInfieldLocation->value())
	{
		if (today>= beetleconstantslist->p_cfg_SaveInfieldLocationStartDay->value())
		{
			if (beetleconstantslist->p_cfg_SaveInfieldLocationStartDay->value() == -2)
			{
				if (m_TheLandscape->SupplyDayInMonth() == 1) DoInFieldLocationOutput();
			}
			else
			{
				if ( (today - beetleconstantslist->p_cfg_SaveInfieldLocationStartDay->value()) % beetleconstantslist->p_cfg_SaveInfieldLocationInterval->value() == 0) DoInFieldLocationOutput();
			}
		}
	}
#endif
#ifdef __CATASTROPHE2
        Catastrophe2();
#else
        Catastrophe(); // This method must be overidden in descendent classes
#endif
}


//-----------------------------------------------------------------------------
//                    (*TheArray_new) handling functions
//----------------------

//------------------------------------------------------------------------------



//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------



/*
void Beetle_Population_Manager::Shuffle_or_Sort( unsigned Type ) {
    switch ( BeforeStepActions[ Type ] ) {
        case 0:
            Debug_Shuffle( Type );
            break;
        case 1:
            SortX( Type );
            break;
        case 2:
            SortY( Type );
            break;
        case 3:
            SortXIndex( Type );
            break;
        case 4: // Do nothing
            break;
        case 5:
            if (g_rand_uni() < double (1./500)) Debug_Shuffle( Type );
            break;
        default:
            m_TheLandscape->Warn( "Population_Manager::Shuffle_or_Sort", "- BeforeStepAction Unknown");
            exit( 1 );
    }
}
 */
/*
void Beetle_Population_Manager::SupplyLocXY(unsigned listindex, unsigned j, int & x, int & y) {
    x = (*TheArray_new)[listindex][j].get()->Supply_m_Location_x();
    y = (*TheArray_new)[listindex][j].get()->Supply_m_Location_y();
}
*/
void Beetle_Population_Manager::SupplyEggLocXY(unsigned listindex, int j, int & x, int & y) const {
    x = (*m_EList)[listindex].get()->Supply_m_Location_x(j);
    y = (*m_EList)[listindex].get()->Supply_m_Location_y(j);
}

inline bool Beetle_Population_Manager::IsStartHabitatAux(TTypesOfLandscapeElement tole) const{
    if ((beetleconstantslist->BeetleStartHabitats).find(tole) != (beetleconstantslist->BeetleStartHabitats).end())
        return true;
    else
        return false;
}
unsigned  Beetle_Population_Manager::GetPopulationSize(int poptype) {


    if (poptype == 0) {
        return SupplyEggPopSize();
    }
    else {
        return GetLiveArraySize(poptype);
    }
}
//-----------------------------------------------------------------------------
#ifdef __BEETLEPESTICIDE1

void Beetle_Population_Manager::PestMortLocOutputError( void )
{
	cout << "Cannot open output file for Beetle pesticide location output " << "Bem_PesticideLocationMort.txt" << endl;
	char ch;
	cin >> ch;
	exit(1);
}

void Beetle_Population_Manager::PestMortLocOutputOpen( void )
{
	ofstream ofile("Bem_PesticideLocationMort.txt",ios::out);
	if ( !ofile.is_open() ) PestMortLocOutputError();
	ofile << "Year" << '\t' << "inCrop" << '\t' << "inField" << '\t' << "offField" << endl;
	ofile.close();
}

void Beetle_Population_Manager::PestMortLocOutput( void )
{
	ofstream ofile("Bem_PesticideLocationMort.txt",ios::app);
	if ( !ofile.is_open() ) PestMortLocOutputError();
	ofile << m_TheLandscape->SupplyYearNumber()-1 << '\t' << m_InCropNo << '\t' << m_InFieldNo << '\t' << m_OffFieldNo << endl;
	ofile.close();
	m_InCropNo = 0;
	m_InFieldNo = 0;
	m_OffFieldNo = 0;
}

void Beetle_Population_Manager::LocOutputOpen( void )
{
	ofstream ofile("Bem_Locations.txt",ios::out);
	if ( !ofile.is_open() ) PestMortLocOutputError();
	ofile << "Year" << '\t'  << "Day" << '\t' ;
	for (int i=0; i< 100; i++)
	{
		ofile << "inCrop" << i << '\t' << "inField" << i << '\t' << "offField" << i << '\t';
	}
	ofile << "inCropTotal" << '\t' << "inFieldTotal" << '\t' << "offFieldTotal" << endl;
	ofile.close();
}


void Beetle_Population_Manager::DoInFieldLocationOutput()
{
	// Records only adults
	unsigned no = (unsigned)GetLiveArraySize( bob_Adult );
	int incrop[100];
	int infield[100];
	int offfield[100];
	int mw10 = (m_TheLandscape->SupplySimAreaWidth()/10);
	int mh10 = (m_TheLandscape->SupplySimAreaHeight()/10);
	// Zero the counts
	for (int i=0; i<100; i++)
	{
		incrop[i] = 0;
		infield[i] = 0;
		offfield[i] = 0;
	}
	for (unsigned i=0; i<no; i++)
	{
		// Need to get the element type, if a field need to check the crop
		APoint p = SupplyAnimalPtr(bob_Adult,i)->SupplyPoint(); //old line: APoint p = (*TheArray_new)[bob_Adult][i]->SupplyPoint();
		// Find the right grid cell of our 100.
		int cell = (p.m_x / mw10) + ((p.m_y / mh10) * 10);
		TTypesOfLandscapeElement tole = m_TheLandscape->GetOwner_tole(p.m_x, p.m_y);
		if  (tole == tole_Orchard) incrop[cell]++;
		else if ((tole == tole_Field) || ((tole == tole_UnsprayedFieldMargin)))
		{
			if (m_TheLandscape->SupplyVegType(p.m_x, p.m_y) == m_InCropRef) incrop[cell]++;
		    else infield[cell]++;
		} else offfield[cell]++;
	}
	ofstream ofile("Bem_Locations.txt",ios::app);
	if ( !ofile.is_open() ) LocOutputError();
	ofile << m_TheLandscape->SupplyYearNumber() << '\t' <<  m_TheLandscape->SupplyDayInYear() << '\t';
	int incr=0,offc=0,offf=0;
	for (int i=0; i< 100; i++)
	{
		ofile << incrop[i] << '\t' << infield[i] << '\t' << offfield[i] << '\t';
		incr += incrop[i];
		offc += infield[i];
		offf += offfield[i];
	}
	ofile << incr << '\t' << offc << '\t' << offf << endl;
	ofile.close();
}

void Beetle_Population_Manager::LocOutputError( void )
{
	cout << "Cannot open output file for Beetle location output " << "Bem_Locations.txt" << endl;
	char ch;
	cin >> ch;
	exit(1);
}


#endif

//-------------------------------------------------------------------------------------
//*************************  PESTICIDE HANDLINE CODE **********************************
//-------------------------------------------------------------------------------------
void Beetle_Adult::InternalPesticideHandlingAndResponse() {
	/**
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for adult to determine if an effect needs to be tested for
	if (m_body_burden > GetAPPPThreshold()) {
		// We are above the PPP body burden threshold, so make a test for effect
		TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
		switch (tp) {
		case ttop_NoPesticide:
		case ttop_ReproductiveEffects: // Reproductive effects
									   // TODO
			break;
		case ttop_AcuteEffects: // Acute mortality
		case ttop_AcuteEffectsDoseResponse: // Acute mortality
			if (g_rand_uni() < GetAPPPEffectProb()) {
                ClearFromMap(m_Location_x, m_Location_y);
				st_Die(); // Kill this one now
			}
			break;
		case ttop_AcuteDelayedEffects:
			if (g_rand_uni() < (GetAPPPEffectProb() * m_body_burden)) {
				m_currentPPPEffectProb = GetAPPPEffectProbDecay();
			}
			break;
		case ttop_MultipleEffects:
			break;
		default:
			g_msg->Warn("Unknown pesticide type used in Beetle_Adult::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
			exit(47);
		}
	}
}


//---------------------------------------------------------------------------


/*
unsigned int Beetle_Population_Manager::FarmAnimalCensus(unsigned int a_farm, unsigned int a_typeofanimal)
{
	unsigned int No = 0;
	unsigned sz = (unsigned)GetLiveArraySize( a_typeofanimal );
	for ( unsigned j = 0; j < sz; j++ )
	{
		if (a_farm == (*TheArray_new)[ a_typeofanimal ] [ j ]->SupplyFarmOwnerRef()) No++;
	}
	return No;
}
 */
//-----------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
void Beetle_Pupae::InternalPesticideHandlingAndResponse() {
	/**
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for pupae to determine if an effect needs to be tested for
	if (m_body_burden > GetPPPPThreshold()) {
		// We are above the PPP body burden threshold, so make a test for effect
		TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
		switch (tp) {
		case ttop_NoPesticide:
		case ttop_ReproductiveEffects: // Reproductive effects
									   // TODO
			break;
		case ttop_AcuteEffects: // Acute mortality
		case ttop_AcuteEffectsDoseResponse: // Acute mortality
			if (g_rand_uni() < GetPPPPEffectProb()) {
                ClearFromMap(m_Location_x, m_Location_y);
				st_Die(); // Kill this one now
			}
			break;
		case ttop_AcuteDelayedEffects:
			if (g_rand_uni() < (GetPPPPEffectProb() * m_body_burden)) {
				m_currentPPPEffectProb = GetPPPPEffectProbDecay();
			}
			break;
		case ttop_MultipleEffects:
			break;
		default:
			g_msg->Warn("Unknown pesticide type used in Beetle_Adult::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
			exit(47);
		}

	}
}
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
void Beetle_Larvae::InternalPesticideHandlingAndResponse() {
	/**
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for larvae to determine if an effect needs to be tested for
	if (m_body_burden > GetLPPPThreshold()) {
		// We are above the PPP body burden threshold, so make a test for effect
		TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
		switch (tp) {
		case ttop_NoPesticide:
		case ttop_ReproductiveEffects: // Reproductive effects
									   // TODO
			break;
		case ttop_AcuteEffects: // Acute mortality
		case ttop_AcuteEffectsDoseResponse: // Acute mortality
			if (g_rand_uni() < GetLPPPEffectProb()) {
                ClearFromMap(m_Location_x, m_Location_y);
				st_Die(); // Kill this one now
			}
			break;
		case ttop_AcuteDelayedEffects:
			if (g_rand_uni() < (GetLPPPEffectProb() * m_body_burden)) {
				m_currentPPPEffectProb = GetLPPPEffectProbDecay();
			}
			break;
		case ttop_MultipleEffects:
			break;
		default:
			g_msg->Warn("Unknown pesticide type used in Beetle_Adult::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
			exit(47);
		}
	}

}


BeetleConstantClass::BeetleConstantClass():
EggDevelConst2{178.58},
p_cfg_Egg_SoilCultivationMortality{new CfgFloat{"BEETLE_EGGSOILMORT",CFG_CUSTOM,0.500}},
p_cfg_Larva_SoilCultivationMortality{new CfgFloat{"BEETLE_LARVAESOILMORT",CFG_CUSTOM,0.500}},
p_cfg_Pupa_SoilCultivationMortality{new CfgFloat{"BEETLE_PUPAESOILMORT",CFG_CUSTOM,0.500}},
p_cfg_Adult_SoilCultivationMortality{new CfgFloat{"BEETLE_ADULTSOILMORT",CFG_CUSTOM,0.270}},
p_cfg_Egg_InsecticideApplication{new CfgFloat{"BEM_EGGINSECTICIDEMORT", CFG_CUSTOM,0}},
p_cfg_Larva_InsecticideApplication{new CfgFloat{"BEM_LARVAEINSECTICIDEMORT", CFG_CUSTOM,0.800}},
p_cfg_Pupa_InsecticideApplication{new CfgFloat{"BEM_PUPAEINSECTICIDEMORT", CFG_CUSTOM,0.800}},
p_cfg_Adult_InsecticideApplication{new CfgFloat{"BEM_ADULTINSECTICIDEMORT", CFG_CUSTOM,0.800}},
p_cfg_PesticideTrialEggTreatmentMort{new CfgInt{"BEM_PTRIALEGGMORT",CFG_CUSTOM,0}},
above12Egg{EggDevelConst2/124.9},
p_cfg_PesticideTrialLarvaeTreatmentMort{new CfgInt{"BEM_PTRIALLARVAEMORT",CFG_CUSTOM,0}},
p_cfg_PesticideTrialPupaeTreatmentMort{new CfgInt{"BEM_PTRIALPUPAEMORT",CFG_CUSTOM,0}},
p_cfg_PesticideTrialAdultTreatmentMort{new CfgInt{"BEM_PTRIALADULTMORT",CFG_CUSTOM,0}},
p_cfg_TotalNoEggs{new CfgInt{"BEM_TOTALEGGS",CFG_CUSTOM,256}},
p_cfg_BeetleStriglingMort{new CfgFloat{"BEM_STRIGLINGMORT", CFG_CUSTOM, 0.250}},
p_cfg_BeetleHarvestMort{new CfgFloat{"BEM_HARVESTMORT", CFG_CUSTOM, 0.290}},
p_cfg_beetlestartnos{new CfgInt{"BEM_STARTNO",CFG_CUSTOM,10000}},
p_cfg_DDepRange{new CfgInt{"BEM_DDEPRANGE",CFG_CUSTOM,3}},
p_cfg_LDDepMort0{new CfgInt{"BEM_LDDEPMORTZERO",CFG_CUSTOM,3}},
p_cfg_LDDepMort1{new CfgInt{"BEM_LDDEPMORTONE",CFG_CUSTOM,0}},
p_cfg_ADDepMort0{new CfgInt{"BEM_ADDEPMORTZERO",CFG_CUSTOM,2}},
p_cfg_ADDepMort1{new CfgInt{"BEM_ADDEPMORTONE",CFG_CUSTOM,10}},
p_cfg_MaxDailyMovement{new CfgInt{"BEM_MAXDAILYMOVEMENT",CFG_CUSTOM,14}},
p_cfg_MoveProbType{new CfgStr{"BEM_MOVEPROBTYPE",CFG_CUSTOM,"BETABINOMIAL"}},
p_cfg_MoveProbArgs{new CfgStr{"BEM_MOVEPROBARGS", CFG_CUSTOM, "14 1 2 0"}},
move_distribution {new probability_distribution{(p_cfg_MoveProbType->value()), (p_cfg_MoveProbArgs->value())}},
SimulationName{"Generic Beetle"},
p_cfg_BemLarvalPPPElimiationRate{new CfgFloat{"BEM_LARVALPPPELIMIATIONRATE", CFG_CUSTOM, 0.0, 0.0, 1.0}},
p_cfg_BemLarvalPPPThreshold{new CfgFloat{"BEM_LARVALPPPTHRESHOLD", CFG_CUSTOM, 99999.9, 0.0, 100000}},
p_cfg_BemLarvalPPPEffectProb{new CfgFloat{"BEM_LARVALPPPEFFECTPROB", CFG_CUSTOM, 0, 0.0, 1.0}},
p_cfg_BemLarvalPPPEffectProbDecay{new CfgFloat{"BEM_LARVALPPPEFFECTPROBDECAY", CFG_CUSTOM, 0, 0.0, 1.0}},
p_cfg_BemPupalPPPElimiationRate{new CfgFloat{"BEM_PUPALPPPELIMIATIONRATE", CFG_CUSTOM, 0.0, 0.0, 1.0}},
p_cfg_BemPupalPPPThreshold{new CfgFloat{"BEM_PUPALPPPTHRESHOLD", CFG_CUSTOM, 99999.9, 0.0, 100000}},
p_cfg_BemPupalPPPEffectProb{new CfgFloat{"BEM_PUPALPPPEFFECTPROB", CFG_CUSTOM, 0, 0.0, 1.0}},
p_cfg_BemPupalPPPEffectProbDecay{new CfgFloat{"BEM_PUPALPPPEFFECTPROBDECAY", CFG_CUSTOM, 0, 0.0, 1.0}},
p_cfg_BemAdultPPPElimiationRate{new CfgFloat{"BEM_ADPPPELIMIATIONRATE", CFG_CUSTOM, 0.0, 0.0, 1.0}},
p_cfg_BemAdultPPPThreshold{new CfgFloat{"BEM_ADPPPTHRESHOLD", CFG_CUSTOM, 99999.9, 0.0, 100000}},
p_cfg_BemAdultPPPEffectProb{new CfgFloat{"BEM_ADPPPEFFECTPROB", CFG_CUSTOM, 0, 0.0, 1.0}},
p_cfg_BemAdultPPPEffectProbDecay{new CfgFloat{"BEM_ADPPPEFFECTPROBDECAY", CFG_CUSTOM, 0, 0.0, 1.0}},
p_cfg_SavePesticideMortLocation{new CfgBool{"BEM_SAVEPESTMORTLOC", CFG_CUSTOM, false}},
p_cfg_SaveInfieldLocation{new CfgBool{"BEM_SAVEINFIELDLOC", CFG_CUSTOM, false}},
p_cfg_InfieldProbeIsOn{new CfgBool{"BEETLE_INFIELDAORISON", CFG_CUSTOM, false}},
p_cfg_OldProbeCount{new CfgBool{"BEETLE_OLDPROBECOUNT", CFG_CUSTOM, false}},
p_cfg_InCropRef{new CfgInt{"BEM_INCROPREF", CFG_CUSTOM, 603}}, // 603 == tov_WWheatPTreatment
p_cfg_SaveInfieldLocationInterval{new CfgInt{"BEM_INCROPREFINTERVAL", CFG_CUSTOM, 10000}},
p_cfg_SaveInfieldLocationStartDay{new CfgInt{"BEM_INCROPREFSTARTDAY", CFG_CUSTOM, -2}},// -2 will cause counting on 1st of every month
p_cfg_ExtremeTempMort{ new CfgFloat{"BEETLE_EXTREMETEMPMORT", CFG_CUSTOM, 0.25, 0, 1}},
p_cfg_AdultExtremeTempMort{ new CfgFloat{"BEETLE_ADULTEXTREMETEMPMORT", CFG_CUSTOM, 0.9, 0, 1}},
p_cfg_EggExtremeTempMort{ new CfgFloat{"BEETLE_EGGEXTREMETEMPMORT", CFG_CUSTOM, p_cfg_ExtremeTempMort->value(), 0, 1}},
p_cfg_PupaExtremeTempMort{ new CfgFloat{"BEETLE_PUPAEXTREMETEMPMORT", CFG_CUSTOM, p_cfg_ExtremeTempMort->value(), 0, 1}},
p_cfg_LarvaExtremeTempMort{ new CfgFloat{"BEETLE_LARVAEXTREMETEMPMORT", CFG_CUSTOM, p_cfg_ExtremeTempMort->value(), 0, 1}},
p_cfg_ExtremeTempMin{ new CfgFloat{"BEETLE_EXTREMETEMPMIN", CFG_CUSTOM, 7, -100, 100}},
p_cfg_ExtremeTempMax{ new CfgFloat{"BEETLE_EXTREMETEMPMAX", CFG_CUSTOM, 30, -100, 100}},
p_cfg_ExtremeTempIsOn{ new CfgBool{"BEETLE_EXTREMETEMPISON", CFG_CUSTOM, false}},
p_cfg_AORProbeIsExclusive{new CfgStr{"BEETLE_AOR_PROBE_ISEXCLUSIVE", CFG_CUSTOM, "no"}},
BeetleMapOfStepDone{new std::map<TTypesOfBeetleState, bool>{
        {tobs_Initiation,  true},
// Egg
        {tobs_EDeveloping, true},
        {tobs_Hatching,    false},
        {tobs_EDying,      false},
// Larva
        {tobs_LDeveloping, true},
        {tobs_LMoving,     false},
        {tobs_Pupating,    false},
        {tobs_LDying,      false},
// Pupa
        {tobs_PDeveloping, true},
        {tobs_Emerging,    false},
        {tobs_PDying,      false},
// Adult
        {tobs_Foraging,    true},
        {tobs_Aggregating, true},
        {tobs_Hibernating, true},
        {tobs_Dispersing,  true},
        {tobs_ADying,      false},
// Destroy
        {tobs_Destroy,     true},

}
},
g_movementdistancesout{"MovementDistances.txt", ios_base::out}


{

    TBeetleToleTovs BeetleToleTovs;
    BeetleSuitableForHibernation=BeetleToleTovs.BeetleSuitableForHibernation.getList();
    BeetleReproductionLandscape=BeetleToleTovs.BeetleReproductionLandscape.getList();
    BeetleHalfReproductionLandscape=BeetleToleTovs.BeetleHalfReproductionLandscape.getList();
    BeetleStartHabitats=BeetleToleTovs.BeetleStartHabitats.getList();
    LarvalDailyMort = std::make_unique<std::vector<std::vector<double>>>();
    LarvalDailyMort->assign(
            {{0.0855,0.0855,0.1036,0.0563,0.0515,0.0657},
                    {0.0626,0.0626,0.0940,0.0529,0.0492,0.0633},
                  {0.0631,0.0631,0.0545,0.0268,0.0236,0.0295}});
    LDevelConst2 = std::make_unique<std::vector<double>>();
    LDevelConst2->assign({101.4,107.4,190.4});
    above12Larvae = std::make_unique<std::vector<double>>();
    above12Larvae->assign({(LDevelConst2->at(0)/87.5),(LDevelConst2->at(1)/95.2),(LDevelConst2->at(2)/189.3)});
    DevelopmentInflectionPoint=12.0;
    DevelConst1=5.0;





}

bool inField(int x, int y, Landscape *LS){
    TTypesOfLandscapeElement tole = LS->GetOwner_tole(x, y);
    if ((tole ==tole_Field || ((tole == tole_UnsprayedFieldMargin)))){
        return true;
    }
    return false;
}

bool outField(int x, int y, Landscape *LS){
    TTypesOfLandscapeElement tole = LS->GetOwner_tole(x, y);
    if ((tole == tole_Field || ((tole == tole_UnsprayedFieldMargin)))) {
        return false;
    }
    return true;
}