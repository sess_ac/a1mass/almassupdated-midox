//
// Created by andrey on 2/5/21.
//

#ifndef ALMASS_BEETLE_TOLETOV_H
#define ALMASS_BEETLE_TOLETOV_H
#include <Landscape/ls.h>
# include <unordered_set>
#include <utility>
using namespace std;
using TToleList=std::unordered_set<TTypesOfLandscapeElement>;
using TTovList=std::unordered_set<TTypesOfVegetation> ;
class BeetleTovParams{
public:
    TTovList getList();
    explicit BeetleTovParams(TTovList);
private:
    TTovList BeetleTovList;
};
class BeetleToleParams{
public:
    TToleList getList();
    explicit BeetleToleParams(TToleList);
private:
    TToleList BeetleToleList;
};
typedef struct BeetleToleTovs{
    BeetleToleParams BeetleStartHabitats{TToleList{
            tole_Field,
            tole_Orchard,
            tole_Vineyard,
            tole_PermPasture,
            tole_PermPastureLowYield,
            tole_PermPastureTussocky,
            tole_RoadsideVerge,
            tole_NaturalGrassDry,
            tole_NaturalGrassWet,
            tole_FieldBoundary,
            tole_UnsprayedFieldMargin,
            tole_YoungForest,
            tole_WaterBufferZone,
            tole_OPermPasture,
            tole_OPermPastureLowYield,
            tole_OPermPasturePigs,
            tole_OBushFruit,
            tole_OOrchard, 
            tole_BushFruit, 
            tole_Wasteland, 
            tole_Garden, 
            tole_PermanentSetaside
  }};
    BeetleTovParams BeetleSuitableForHibernation{TTovList{
            tov_Wasteland,
            tov_PermanentGrassGrazed,
            tov_PermanentGrassLowYield, // 35
            tov_PermanentGrassTussocky, // 35
            tov_OPermanentGrassGrazed, // 35
            tov_NaturalGrass, // 110
            tov_Heath,
            tov_PermanentSetAside,
            tov_OSetAside,
            tov_WaterBufferZone,
            tov_NLPermanentGrassGrazed,
			tov_DEPermanentGrassGrazed,
			tov_DEPermanentGrassLowYield,
            tov_DEOPermanentGrassGrazed,
            tov_DEOPermanentGrassLowYield

    }};
    BeetleToleParams BeetleReproductionLandscape{TToleList{
            tole_Field,
            tole_Orchard,
            tole_Vineyard,
            tole_OBushFruit,
            tole_OOrchard,
            tole_BushFruit,
            tole_Wasteland
 //           tole_UnsprayedFieldMargin
    }};
    BeetleToleParams BeetleHalfReproductionLandscape{TToleList{
            tole_PermPasture,
            tole_PermPastureLowYield,
            tole_PermPastureTussocky,
            tole_RoadsideVerge,
            tole_NaturalGrassDry, // 110
            tole_NaturalGrassWet,
            tole_YoungForest,
            tole_MownGrass,
            tole_BeetleBank,
            tole_WaterBufferZone,
            tole_FieldBoundary,
            tole_OPermPasture,
            tole_OPermPastureLowYield,
            tole_OPermPasturePigs,
            tole_Garden
    }};
} TBeetleToleTovs;

int beetle_tole_movemap_init(Landscape* m_OurLandscape, int x, int y);

#endif //ALMASS_BEETLE_TOLETOV_H
