//
// Created by andrey on 2/8/21.
//
# include "Beetle_toletov.h"

#include <utility>

BeetleTovParams::BeetleTovParams(TTovList inp){
    BeetleTovList=std::move(inp);
}
BeetleToleParams::BeetleToleParams(TToleList inp){
    BeetleToleList=std::move(inp);
}
TTovList BeetleTovParams::getList(){
    return BeetleTovList;
}
TToleList BeetleToleParams::getList(){
    return BeetleToleList;
}

int beetle_tole_movemap_init(Landscape* m_OurLandscape, int x, int y)
{
    TTypesOfLandscapeElement tole = m_OurLandscape->GetOwner_tole(x, y);
	int colour;
	// Beetle
	switch (tole)
	{
	case tole_Hedges: // 130
	case tole_RoadsideVerge: // 13
	case tole_FieldBoundary: // 160
	case tole_HedgeBank:
	case tole_BeetleBank:
	case tole_NaturalGrassWet:
	case tole_RoadsideSlope:
	case tole_WaterBufferZone:
	case tole_PermanentSetaside: // 33
	case tole_RiversidePlants: // 98
	case tole_Vildtager:
	case tole_PermPasture: // 35
	case tole_PermPastureLowYield: // 35
	case tole_PermPastureTussocky: // 26
	case tole_PermPastureTussockyWet:
	case tole_Heath:
	case tole_NaturalGrassDry: // 110
	case tole_PermPasturePigs:
	case tole_OPermPasturePigs:
	case tole_OPermPasture:
	case tole_OPermPastureLowYield:
	case tole_FarmBufferZone:
	case tole_NaturalFarmGrass:
	case tole_GreenFallow:
	case tole_FarmFeedingGround:
		colour = 1;
		break;
	case tole_Marsh: // 95
	case tole_Scrub: // 70
	case tole_Railway: // 118
	case tole_PitDisused: // 75
	case tole_Track:  // 123
	case tole_SmallRoad:  // 122
	case tole_LargeRoad:  // 121
	case tole_MetalledPath:
	case tole_Carpark:
	case tole_Churchyard:
	case tole_Saltmarsh:
	case tole_PlantNursery:
	case tole_HeritageSite:
	case tole_Copse:
	case tole_WoodyEnergyCrop:
	case tole_WoodlandMargin:
	case tole_IndividualTree:
	case tole_RiversideTrees: // 97
	case tole_DeciduousForest: // 40
	case tole_MixedForest:     // 60
	case tole_ConiferousForest: // 50
	case tole_YoungForest:
	case tole_StoneWall: // 15
	case tole_ActivePit: // 115
	case tole_Fence: // 225
	case tole_RefuseSite:	// 224
	case tole_SwampForest: // 400
	//needs to be re-think for PT forest types
	case tole_MontadoCorkOak:
	case tole_MontadoHolmOak:
	case tole_MontadoMixed:
	case tole_AgroForestrySystem:
	case tole_CorkOakForest:
	case tole_HolmOakForest:
	case tole_OtherOakForest:
	case tole_ChestnutForest:
	case tole_EucalyptusForest:
	//
	case tole_InvasiveForest:
	case tole_MaritimePineForest:
	case tole_StonePineForest:
	case tole_ChristmasTrees:
	case tole_OChristmasTrees:
	case tole_EnergyCrop:
	case tole_OEnergyCrop:
	case tole_FarmForest:
	case tole_OFarmForest:
	case tole_FarmYoungForest:
	case tole_OFarmYoungForest:
		colour = 2;
		break;
	case tole_Field:  // 20 & 30
	case tole_UnsprayedFieldMargin:
	case tole_AmenityGrass:
	case tole_Parkland:
	case tole_Orchard:
	case tole_OrchardBand:
	case tole_MownGrass:
	case tole_Wasteland: // 209
	case tole_UnknownGrass:
	case tole_Garden: //11
	case tole_OOrchard:
	case tole_ForestAisle: //413
	case tole_BushFruit:
	case tole_OBushFruit:
	case tole_Vineyard:
	case tole_OliveGrove:
	case tole_AlmondPlantation:
	case tole_WalnutPlantation:
	case tole_SolarPanel:
		colour = 0;
		break;
	case tole_Building: // 5
	case tole_Freshwater: // 90
	case tole_FishFarm: // 220
	case tole_Pond:
	case tole_River: // 96
	case tole_Saltwater:  // 80
	case tole_Coast: // 100
	case tole_BareRock: // 59
	case tole_UrbanNoVeg:
	case tole_UrbanVeg:
	case tole_UrbanPark:
	case tole_BuiltUpWithParkland:
	case tole_SandDune:
	case tole_Stream:
	case tole_Pylon:
	case tole_WindTurbine:
	case tole_DrainageDitch:
	case tole_Canal:
	case tole_Airport:
	case tole_Portarea:
	case tole_Saltpans:
	case tole_Pipeline:
	case tole_RiceField:
	colour = 3;
		break;
	case tole_Foobar: // 999   !! type unknown - should not happen
	default:
		g_msg->Warn(WARN_FILE,
			"MovementMap::Init(): Unknown landscape element type:", int(tole));
		exit(1);
	}

	return colour;
}