//
// Version of May, 31, 2020
// Last modified by Andrey Chuhutin on 12/02/2020
/*
*******************************************************************************************************
Copyright (c) 2020, Andrey Chuhutin, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************************************
*/
//----------------------------------------------------------------------
// Created by Andrey Chuhutin on 29/09/2020.
//
//---------------------------------------------------------------------------

#ifndef Ladybird_allH
#define Ladybird_allH

//---------------------------------------------------------------------------
#include <blitz/array.h>
#include <blitz/numinquire.h>
#include "Beetle_BaseClasses.h"
#include "Aphids_debug/AphidsD_all.h"
#include <bitset>
#include "BatchALMaSS/ALMaSS_Random.h"
/** The size of the "memory" of hibernaculae in years*/
#define HIBERNACULA_MEMORY 5
#define FLIGHT_MEMORY_SIZE 32
extern const char* SimulationName;


//------------------------------------------------------------------------------
// Forward Declarations

class Ladybird_Population_Manager;
class Landscape;
class MovementMap;
class SimplePositionMap;

class probability_distribution;


//--------------------------------------------------------------------------


//------------------------------------------------------------------------------

/** \brief A data class for Ladybird data */

 class  struct_Ladybird : public struct_Beetle
{
 public:
  bool can_Reproduce{false};
  int DateMade{0};
  int eggs_developed{0};
  struct_Ladybird()= default;
};
const int LadybirdStagesNumberInDensityMap{8};
//------------------------------------------------------------------------------
/**
\brief The class describing the constants specific to Ladybird

 Some of these constants will be moved to use the configurator. When this happens both the definition and the initialisation will change.
*/
class LadybirdConstantClass{
public:
    /** \brief
    * The map that defines which types of Ladybird can be cannibalised
    * Eggs and larva can be cannibalised, but not pupa or adult
    * */
    std::map<int, bool> CanBeCannibalised { {0, true}, {1, true}, {2, false}, {3, false}};
    /** \brief Ladybird number of stages */
    const int LadybirdLarvalStagesNum;
    /** \brief the length of the earliest day of emergence*/
    const int LadybirdEmergingDayLength;
    /** \brief The chance of emergence in particular day after the earliest date*/
    const int LadybirdEmergingChance;
    /** \brief The maximum size of overwintering clump which is allowed */
    const int LadybirdMaxClumpSize;
    /** Ladybird's Lower Developmental Thresholds*/
    const std::array<double,7> LadybirdLDTs{
            11.5, /** Egg */
            13.8, /** 1st instar */
            13.6, /** 2nd instar */
            13.6, /** 3rd instar */
            13.9, /** 4th instar */
            12.9, /** Pupa */
            12.4 /** Adult */

    };
    /** \brief Prey density factors The variable that connects with prey density and amount of maximum eggs developed */
    const std::array<double,6> LadybirdPDf{
            0, /** Prey level 1*/
            0.5,
            0.82,
            0.89,
            0.98,
            1 /** Prey level 6 */
    };
    /** \brief Temperature-related eggs per day */
    const std::array<double,7> LadybirdEPD{
        0, /** T<12.5 */
        7, /** 12.5<T<17.5 */
        21, /** 17.5<T<22.5 */
        39, /** 22.5<T<27.5 */
        28, /** 27.5<T<32.5 */
        20, /** 32.5<T<37.5 */
        0 /** T> 37.5 */
    };
    /** \brief The minimum number of Aphids ranges for development length factorisation */
    const std::array<int, 4> LadybirdInstarMinAphidFactor{
        10, /**1st instar*/
        15,
        20,
        40 /**4th instar*/
    };
    /** \brief The maximum number of Aphids ranges for development length factorisation */
    const std::array<int, 4> LadybirdInstarMaxAphidFactor{
            35, /**1st instar*/
            40,
            70, // originally in paper 80
            140 /**4th instar*/ // originally in paper 150
    };
    /** \brief The factor of the instar development which is a function of aphid abundance */
    const std::array<std::array <double, 6>,4> LadybirdInstarAphidDevelFactor{
            {{1.61538461538462, 1.46153846153846, 1.26923076923077, 1.11538461538462, 1.03846153846154, 1}, // 1st instar
             {1.77777777777778, 1.5, 1.33333333333333, 1.22222222222222, 1.11111111111111, 1}, // 2 instar
             {1.8, 1.55, 1.3, 1.1, 1.05, 1},
             {1.66666666666667, 1.42857142857143, 1.28571428571429, 1.0952380952381, 1.04761904761905, 1}} // 4th instar
    };
    /** \brief The aphid-related factor that is used for estimation of eggs to be laid in a lifetime  */
    const std::array<double,6> LadybirdAphidNumberEggFactor{
        0, 0.5, 0.82, 0.89, 0.98, 1
    };
    /** \brief the density of aphids that when first encountered will trigger moving to the reproduction state
    * NOTE: the density is not absolute but in sq cm of leaf cover per aphid  */
    const int LadybirdAphidDensityToStartReproduction{335};
    /** \brief the density of aphids that when first encountered will trigger moving to the reproduction state
    * NOTE: the density is not absolute but in sq cm of leaf cover per aphid  */
    const int LadybirdAphidDensityToStartForaging{2550};
    /** \brief minimum temperature that larva can move at */
    const double LadybirdLarvaMovementThreshold{10};
    /**\brief Background egg mortality */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdDailyEggMortality;
   /**\brief Background larvae mortality */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdDailyLarvaeMortality;
    /**\brief Background pupae mortality */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdDailyPupaeMortality;
    /**\brief Background adult mortality */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdDailyAdultMortality;
    /** \brief Maximum Temperatures that each stage of ladybird can survive*/
    const std::array<double,7> LadybirdMaxTemps{
            40., /** Egg */
            40., /** 1st instar */
            40., /** 2nd instar */
            40., /** 3rd instar */
            40., /** 4th instar */
            40., /** Pupa */
            40. /** Adult */

    };
    /** \brief The Minimum Temperatures that each stage of ladybird can survive*/
    const std::array<double,7> LadybirdMinTemps{
            -27., /** Egg */
            -20., /** 1st instar */
            -20., /** 2nd instar */
            -18., /** 3rd instar */
            -9., /** 4th instar */
            -19., /** Pupa */
            -20. /** Adult */

    };
    /** \brief Threshold value of Daydegrees that triggers the transformation */
    const std::array<double, 7> LadybirdThresholdDD{
        42,  /** Egg */
        22.8,  /** 1st instar */
        20.4, /** 2nd instar */
        23.1, /** 3rd instar */
        38, /** 4th instar */
        63.6, /** Pupa */
        151.9 /** Adult */
    };
    /** \brief The currency conversion: cannibalism in aphids */
    const std::array<double, 7>LadybirdCannibalismCost{
        1, /** Egg price in Aphids */
        10, /** L1 price in Aphids */
        30, /** L2 price in Aphids */
        80, /** L3 price in Aphids */
        120, /** L4 price in Aphids */
        140, /** Pupa price in Aphids: never used-- pupae are edible */
        160  /** Adult price in Aphids: never used-- Adults are not edible */
    };
    /** \brief The base daily appetite factor for different stages in aphids
     * to get the number of aphids it should be multiplied by age in days
     * */
    const std::array<double, 7>LadybirdAppetiteFactor{
            0, /** Egg does not eat */
            5, /** L1 appetite factor in Aphids */
            5, /** L2 appetite factor in Aphids */
            7, /** L3 appetite factor in Aphids */
            7, /** L4 appetite factor in Aphids */
            0, /** Pupa does not eat */
            8  /** Adult appetite factor  */
    };
    /** \brief Minimum tolerated temperature at given month of the year */
    const std::array<double, 12> Ladybird_SCP{
        -15, // January
        -15,
        -10,
        -9,
        -5,
        -5,
        -5,
        -5,
        -5,
        -9,
        -12,
        -17 // December
    };
    /** Threshold temperatures for mortalities: Min **/
    const double LadybirdMortalityTempsMin{10};
    /** Threshold temperatures for mortalities: Max **/
    const double LadybirdMortalityTempsMax{35};
    /** Threshold temperatures for mortalities: Step **/
    const double LadybirdMortalityTempsStep{5.0};
    /** \brief The temperature at which the flight is possible */
    const double LadybirdFlyingThreshTemp{15.0};
    /** \brief The temperature at which the movement is possible for adults */
    const int LadybirdMovementTempThreshold{4};
    /** \brief The maximum wind speed that still allows flying (m/s) */
    const double LadybirdFlyingThreshWind{20.0};
    /** \brief The number of hiberniculae when the simulation has started */
    const int InitialHibernaculaeNumber{500};
    /**\brief max length of pupa stage (in days) */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdPupaStageMaxLength;
    /** \brief The temperature related mortality of Ladybird eggs */
    const std::array<double, 7>LadybirdEggTMortality{0.034231321839081,0.030215053763441,0.029655172413793,0.039,0.06304347826087, 0.158125};
    /** \brief The temperature related mortality of Ladybird pupae */
    const std::array<double, 7>LadybirdPupaTMortality{0.018996015936255, 0.018292682926829, 0.014158415841584, 0.02859649122807, 0.058888888888889, 0.085263157894737};
    /** \brief Boolean flag that signals whether the undeveloped stages of ladybird should be killed in mid-winter (at NY)*/
    std::unique_ptr<const CfgBool> p_cfg_KillOnNyEve;
    /** \brief the map that defines whether the step is finished
     * after we changed state into a specific beetle state */
    std::unique_ptr<std::map<TTypesOfBeetleState, bool>> LadybirdMapOfStepDone;
    /**\brief the number of ladybirds to start the simulation with */
    std::unique_ptr<const CfgInt> p_cfg_ladybirdstartnos;
    /**\brief the number of eggs each one of the starting ladybirds has */
    std::unique_ptr<const CfgInt> p_cfg_ladybirdstarteggsno;
    /**\brief Oviposition period start */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdOvipositionPeriodStart;
    /**\brief Oviposition period end */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdOvipositionPeriodEnd;
    /**\brief the coeffitient of local aphid number factor that contributes to egg production */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdAphidToEggRatio;
    /**\brief Max daily eggs: unused */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdDailyEggMax;//todo: unused-> remove
    /** \brief The variable that is true if the ladybirds can die due to exhaustion during dispersal */
    std::unique_ptr<const CfgBool> p_cfg_LadybirdExhaustionInDispersal;
    /**\brief Max distance to travel in one day (in short jumps)*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxDistance;
    /**\brief Max length of a long range flight*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxLongRangeFlight;
    /** \brief Max distance to travel in one day for larva (in moves)*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxLarvaDistance;
    /** \brief The number of aphids needed to ovipose at the location */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdAphidDemand;
    /** \brief distance at which a ladybird senses aphids
    * this value is used to guide the movement of the ladybird
    * from the low concentration to the high concentration */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdAphidSensingDistance;
    /** \brief distance at which a ladybird larva senses aphids*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdLarvaAphidSensingDistance;
    /** \brief Mean distance of a single hop*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMeanHoppingDistance;
    /** \brief Std distance of a single hop*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdStdHoppingDistance;
    /** \brief Mean distance of a single hop: Larva*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdLarvaMeanHoppingDistance;
    /** \brief Std distance of a single hop: Larva*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdLarvaStdHoppingDistance;
    /** \brief Max distance of a single hop*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdLarvaMaxHoppingDistance;
    /** \brief Max number of attempts to hop short distance before hopping long*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxShortRangeAttempts;
    /**\brief Max number of attempts to hop long distance before dying*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxLongRangeAttempts;
    /**\brief Max Total number of laid eggs*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdTotalEggsMax;
    /** \brief defines whether the ladybird is uniseasonal*/
    std::unique_ptr<const CfgBool> p_cfg_isLadybirdUniSeasonal;
    /** \brief for non-uniseasonal the chance to die each year */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdYearlyMortality; //todo: remove me
    /** \brief min Extreme temperature. Min temperature below that causes mass mortality */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdExtremeTempMin;
    /** \brief min Extreme temperature. Min temperature below that causes mass mortality while hibernating (factor on top of a regular SCP) */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdExtremeTempMinHibernatingFactor;
    /** \brief max Extreme temperature. Max temperture above that causes mass mortality */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdExtremeTempMax;
    /** \brief Chance to perform random movement if not caused by absence of food */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdUnnecessaryMovementChance  ;
    /** \brief Maximum number of short range moves in an attempt to hibernate*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxHibernationTries;
    /** \brief Maturation period: minimum period between the emergence and oviposition (in days)*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMinMaturation;
    /** \brief Max Aphids number that the Ladybird tolerates without fleeing*/
    std::unique_ptr<const CfgInt> p_cfg_LadybirdMaxAphids;
    /** \brief Max Aphids number that the Ladybird tolerates without fleeing*/
    std::unique_ptr<const CfgBool> p_cfg_LadybirdDebugging;
    /** \brief Chance to catch the cannibalisation victim */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdCannibalisationChance;
    /** \brief Config parameter: Max number of tries to search for LongRangeDirection */
    std::unique_ptr<const CfgInt> p_cfg_LongRangeMaxTries;
    /** \brief Config parameter: Min size of the long range targets dictionary **/
    std::unique_ptr<const CfgInt> p_cfg_LongRangeMinDict;
    /** \brief Max number of tries to search for LongRangeDirection */
    int LongRangeMaxTries{50};
    /** \brief Min size of the long range targets dictionary **/
    int LongRangeMinDict{30};
    /** \brief The numbers (TOPs) of the aphid species that the ladybird can interact with */
    std::unique_ptr<const CfgArray_Int> p_cfg_LadybirdAphidSpecies;
    /** \brief A number of ladybirds that need to overwinter in a particular polygon for it to be regarded as a hibernacula*/
    int HibernaculaThreshold{10};
    /**\brief the types of vegetation that are prohibited for ladybirds */
    TTovList LadybirdProhibitedTovsForMovement;
    /**\brief the types of landscape that are prohibited for ladybirds */
    TToleList  LadybirdProhibitedTolesForMovement;
    /**\brief the types of vegetation that are prohibited for ladybirds larva */
    TTovList LadybirdProhibitedTovsForMovementLarva;
    /**\brief the types of landscape that are prohibited for ladybirds larva */
    TToleList  LadybirdProhibitedTolesForMovementLarva;
    /**\brief the types of vegetation that are targets of long range movement */
    TTovList LadybirdLongRangeTovs;
    /**\brief the types of landscape that are targets of long range movement */
    TToleList LadybirdLongRangeToles;
    /**\brief the types of vegetation that are aggregation targets */
    TTovList LadybirdAggregationTovs;
    /**\brief the types of landscape that are aggregation targets */
    TToleList LadybirdAggregationToles;
    /** \brief Ladybird lab flying speed */
    const float LadybirdFlyingSpeed{1.5};
    /** \brief Boundary layer reduction in wind speed */
    const float LadybirdBoundaryLayerFactor{0.075};
    /**\brief Num of aphids needed to stay awake */
    const int AphidsToStayAwake{1000};
    /**\brief Num of aphids that triggers hibernation */
    const int AphidsToHibernate{10};
    /** \brief The date the aggregation starts */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdAggregationStart;
    /** \brief The date the aggregation ends */
    std::unique_ptr<const CfgInt> p_cfg_LadybirdAggregationEnd;
    /** \brief Daily hibernation chance between the start and end dates */
    std::unique_ptr<const CfgFloat> p_cfg_LadybirdHibernationChance;
    /** \brief Constructor of constants class */
    LadybirdConstantClass();
protected:
    ;
private:

};
//------------------------------------------------------------------------------
/**

Ladybird base class, includes all basic methods and variables used by more than one stage
*/
class Ladybird_Base : virtual public Beetle_Base{
public:
    /** \brief Class constructor */
    Ladybird_Base(int, int, Landscape *, Ladybird_Population_Manager*);
    /**\brief pointer to the population manager as a <Ladybird_Population_Manager> */
    static std::shared_ptr<Ladybird_Population_Manager> m_OurLadybirdPopulation;
    /**\brief the global date the stage has been formed (e.g. emergence  date for adult )*/
    int m_DateMade{0};
    /**\brief number of aphids or aphid equivalents eaten today */
    int eatenToday{0};
    /**\brief the today's appetite */
    int m_AphidsAppetite{0};
    /**\brief the counter of the steps made today */
    int m_MoveCounter{0};
    /**\brief the method that is responsible for aphid foraging behaviour (removes the aphids from the aphid model)*/
    virtual int EatAphids(int);
    /** \brief increment the counter of eaten aphids */
    void incEatenToday(int add){eatenToday+=add;};
    /**\brief return the value of daily aphid consumption counter */
    [[nodiscard]] virtual int getEatenToday() const{return eatenToday;};
    /** \brief set the value of the counter of eaten aphids */
    virtual void setEatenToday(int a){eatenToday=a;};
    /** \brief return how many aphids are left to be eaten */
    inline int getAphidsToEat(){return getAphidsAppetite()-getEatenToday();};
    /**\brief The function returns true if there are too much aphids in the location*/
    static bool IsTooMuchAphids(long num);
    /**\brief The function returns  the aphids appetite (virtual function stub: to be overridden) */
    virtual inline int getAphidsAppetite(){return 0;};
    /**\brief reset the appetite  to zero */
    virtual inline void resetAphidsAppetite(){m_AphidsAppetite=0;};
    /**\brief Perform the cannibalisation behaviour from the perspective of a _cannibal_ : receives the appetite to fulfill, returns how much is left to be eaten after an attempt to cannibalise */
    int Cannibalise(int appetite);
    /**\brief returns the highest number of stage that can still be cannibalised. Uses the same notation as m_LadybirdCannibalismMap */
    virtual int getMaxCannibalise();
    /** \brief the length of one short range jump */
    double getShortRangeDistance() override{return 0.;};
    /**\brief the method that based on the vector of hopping probabilities that correspond to 4 cardinal directions chooses the direction that the ladybird will hop to in a longrange attempt */
    static int LongHopDirection(std::array<float,4> (&probabilities));
    /** \brief  The function takes the direction in 0-3
     * and gives out the random direction in degrees inside this sector*/
    static int PreciseLongHopDirection(int inputdir);
    /**\brief returns true if the coordinates are a valid target for ladybird movement */
    bool IsLocationAllowed (int x, int y) override{
      return true;
    };
    /**\brief returns the value of the movement counter */
    [[nodiscard]] inline int getMoveCounter() const{return m_MoveCounter;};
    /**\brief resets the movement counter */
    inline void resetMoveCounter(){m_MoveCounter=0;};
    /**\brief increments the movement counter */
    inline void incMoveCounter(){m_MoveCounter++;};
    /**\brief returns todays maximum movement distance (the function of the temperature) */
    virtual int getMaxDailyDistance();
    /**\brief Calculates the appetite of aphids based on the age (Arshad et al, 2017)*/
    virtual inline void calcAphidsAppetite(){m_AphidsAppetite=0;};
    /**\brief returns true if the beetle can still move today */
    inline bool CanMove(){
        auto movecounter = getMoveCounter();
        auto maxdistance = getMaxDailyDistance();
        bool returnvalue=(movecounter < maxdistance);
        return returnvalue;
    };

    /**\brief returns the maximum sensing distance. It will be used to estimate the movement direction */
    int getMaxSensingDistance() override{return 0;};
    /**\brief returns the number of attempts to perform a short-range hop */
    const int getMaxSAttempts() override;
    /**\brief judging by the wind speed and the desired travel direction here we estimate how far can the beetle fly*/
    static int getLongRangeDistance(double wind_speed, double travel_angle_rad, double wind_rad);
    /** not in use */
    static int getLongRangeDistance();
    /**\brief how long we will try to search for a long range target */
    static int getLongRangeSearchLength();
    /**\brief return the length of the dictionary (number of potential targets)*/
    static int getDictLength();
    /**\brief returns the direction of the long range jump in degrees based on the wind direction  */
    static int getLongRangeDirection(int wind_direction, double wind_speed);
    /**\brief returns true if the coordinates point to a legitimate long range target */
    bool IsLongRangeTarget(int x, int y);
    /**\brief sets the date when this particular stage is created to a given number (day since the simulation start)*/
    void setDateMade(int date){
        m_DateMade=date;
    };
    /**\brief return true if the extreme temp mortality is switched on */
    [[nodiscard]] bool ExtremeTempIsOn() const override;
    /**\brief sets the number of total eggs to lay in the lifetime to a given value */
    void setTotalEggsToLay(int no){m_TotalEggsToLay=no;};
private:
protected:
    /** the number of eggs to lay in the lifetime: updated until the maturity*/
    int m_TotalEggsToLay{0};
    /**\brief returns the chance to perform the cannibalism */
    [[nodiscard]] double getCannibalisationChance() const override;
    /**\brief the function that defines the factor that drive the movement in these particular species (aphids number) */
    double MovementFactor(int, int, int, int) override;
    /**\brief returns minimum temperature below which extreme temperature mortality happens */
    [[nodiscard]] double getExtremeTempMin() const override;
    /**\brief returns maximum temperature above which extreme temperature mortality happens */
    [[nodiscard]] double getExtremeTempMax() const override;
    /**\brief returns the daily min temperature (it could be either min air temperature or soil temperature, depending on where that stage spends its time) */
    double getMinTemp() override;
    /**\brief returns the daily max temperature (it could be either max air temperature or soil temperature, depending on where that stage spends its time) */
    double getMaxTemp() override;
    /**\brief gets a number of aphids at the locations and returns what prey density level this number corresponds to */
    virtual int PreyDensityLevel(long AphidsNum)=0;
    /**\brief based on the number of aphids met and the temperature, the future number of offspring is adjusted */
    virtual void UpdateEggsToLay(int preydensitylevel, double temp);
    /**\brief returns the mortality range that corresponds to the temperature */
    static int GetTempRange(double temp);
    /**\brief returns the maximum length of the stage in days (currently used only in pupae)*/
    virtual int getMaxStageLength(){return 1000;};

};
//------------------------------------------------------------------------------
/**
*\brief
*The class describing the beetle Egg_List objects
 *
 * In general case this class should have only a constructor
*/
class Ladybird_Egg_List : public  Beetle_Egg_List, public Ladybird_Base
{

/** The egg list is an optimisation to reduce memory and time to run
   it means that there are only 365 of them possible and that each on contains
   all the eggs laid on that day. The only real problem is that there is no longer a link from parent to
   offspring - so this version cannot be used with genetics without adding the genes to the APoint struct.\n


*/
public:
	/** \brief Egg_List class constructor */
    Ladybird_Egg_List(int today,Ladybird_Population_Manager* BPM, Landscape* L);
    /**\brief returns the threshold in dd that triggers the hatching */
    double getEggDevelConst2() override;
    /**\brief the method that performs the _victim_'s side of cannibalisation event */
    bool Cannibalisation() override;
    /**\brief return the mortality due to soil cultivation events*/
    [[nodiscard]] double getSoilCultivationMortality() const override;
    /**\brief basic development behaviour*/
    TTypesOfBeetleState st_Develop() override;
    /**\brief hatching behaviour: in the end of development */
    TTypesOfBeetleState st_Hatch() override;
    /**\brief remove from map: standard function overriden*/
    void ClearFromMap(int X, int Y) override;
    /**\brief standard beginning of the step overriden for ladybird eggs*/
    void BeginStep() override;
protected:
    /**\brief the function that returns the prey density level that depends on the aphids number. We assume that eggs don't feel the presence of the eggs. Therefore function returns the max value always.*/
    int PreyDensityLevel(long AphidsNum) override{return 5;};
    /**\brief returns background egg mortality*/
    [[nodiscard]] double getDailyEggMortality() const override;
    /**\brief returns mortality that is a function of temperature */
    double TempRelatedMortality (double temp, double maxtemp, double mintemp) override;

};

/** \brief The population manager class for Ladybird
 *
 * This should only include methods and attributes that are different from the generic beetle
 * */

class Ladybird_Larvae : public Beetle_Larvae, public Ladybird_Base{
public:
    /**\brief Constructor of larva object */
    Ladybird_Larvae( int x, int y, Landscape* L, Ladybird_Population_Manager* BPM );
    /**\brief Destructor of larva object */
    ~Ladybird_Larvae()override=default;
protected:
    // member functions
    /**\brief Returns the development factor that depends on aphid number*/
    double getLarvaDevelFactor(long AphidsNum);
    /**\brief Returns the number of instars in ladybird larvae (overrides the base class)  */
    [[nodiscard]] int getLarvalStagesNum() const override;
    /**\brief The method that clears the object from the map */
    void ClearFromMap(int X, int Y) override;
    /**\brief The method that adds the object to the map */
    bool AddToMap(int X, int Y) override;
    /**\brief returns the aphids appetite */
    inline int getAphidsAppetite() override{ return m_AphidsAppetite;   };
    /**\brief Calculates the appetite in aphids. The method run daily through BeginStep().*/
    void calcAphidsAppetite() override;
    /**\brief The override of the standard method that returns the daydegrees constant */
    [[nodiscard]] double getLDevelConst2(int i) const override;
    /**\brief The _victim_'s side of cannibalisation process. Run by a step function. */
    bool Cannibalisation() override;
    /**\brief The mortality due to agricultural practises related to soil cultivation. */
    [[nodiscard]] double getSoilCultivationMortality() const override;
    /**\brief The step function. */
    void Step() override;
    /**\brief The method that returns the short range jump length as generated by the static random numbers generator */
    double getShortRangeDistance() override;
    /**\brief The main behaviour method that is responsible for the moving between the development sub-stages and eventually leading to pupation.*/
    TTypesOfBeetleState st_Develop() override;
    /**\brief The method that is run on the individual level before the Step() function*/
    void BeginStep() override;
    /**\brief The method that returns temperature threshold below which the larvae cannot move */
    static inline double getMovementThreshold();
    /**\brief Depending on what is the stage (instar) of the larva the method returns the maximum number (related to m_LadybirdDensityMap) of stage that could be cannibalised */
    int getMaxCannibalise() override;
    /**\brief returns true if the location is allowed for larvae (override) */
    bool IsLocationAllowed(int x, int y) override;
    /**\brief The override of the standard beetle method that returns the maximum daily distance. In the case of ladybird larvae it returns the Ladybird_Population_Manager::m_MaxLarvaDistance which is calculated daily.*/
    int getMaxDailyDistance() override;
    /** \brief Returns the maximum distance the beetle senses the aphid presence. The method is used to evaluate the movement trajectory. */
    int getMaxSensingDistance() override;
    /**\brief translates the aphid numbers to the level of prey density. Note: the translation map is different in different stages. */
    int PreyDensityLevel(long AphidsNum) override;
    /**\brief the method that removes the larva object and substitutes it with a pupa object.*/
    TTypesOfBeetleState st_Pupate() override;
    /**\brief returns the background mortality for ladybird larvae */
    [[nodiscard]] double getDailyMortalityRate() const  override;
    /**\brief returns the larva temperature dependent daily mortality from the constants*/
    double getLarvaTDailyMortality(int st, int t) override;
    /**\brief checks for extreme temperature mortality and calls getLarvaTDailyMortality */
    double TempRelatedMortality(double temp, double maxtemp, double mintemp) override;
};
class Ladybird_Pupae : public Beetle_Pupae, public Ladybird_Base{
public:
    Ladybird_Pupae( int x, int y, Landscape* L, Ladybird_Population_Manager* BPM );
    ~Ladybird_Pupae()override =default;
    void ClearFromMap(int X, int Y) override;
   // static std::shared_ptr<Ladybird_Population_Manager> m_OurLadybirdPopulation;

    [[nodiscard]] double getDevelConst2() const override;

    [[nodiscard]] double getSoilCultivationMortality() const override;

    TTypesOfBeetleState st_Emerge() override;
    void BeginStep () override;
protected:
    int PreyDensityLevel(long AphidsNum) override;
    [[nodiscard]] double getDailyMortalityRate() const  override;

    double TempRelatedMortality(double temp, double maxtemp, double mintemp) override;
    /** \brief There is no temperature related mortality calculation (stage length to mortality) in Ladybird pupae */
    [[nodiscard]] double calcDailyMortChance(int temp2, double LengthOfStageAtTemp) const override {return 0.0;};
    int getMaxStageLength() override;
};
class Ladybird_Adult : public Beetle_Adult, public Ladybird_Base{
public:
    // member functions
    Ladybird_Adult( int x, int y, bool can_Reproduce, Landscape* L, Ladybird_Population_Manager* BPM );
    Ladybird_Adult( int x, int y, int DateMade, bool can_Reproduce, Landscape* L, Ladybird_Population_Manager* BPM );
    ~Ladybird_Adult()override =default;
    bool IsLocationAllowed(int x, int y) override;
    void setOverwintered(bool value) {hasOverwintered=value;};


    void setIsHibernating(bool val){
        IsHibernating = val;
    }
    void ResetAll(){
        DailyEggs=0;
        m_EmergenceDay=0;
        m_UnsuccessfulAphidSearch=0;
        m_LongFlights=0;
        m_ADayDeg=0;
        eatenToday =0;
        m_AphidsAppetite = 0;
        m_MoveCounter = 0;
        m_DaysInHibernation = 0;
    }
    // member variables
   // static std::shared_ptr<Ladybird_Population_Manager> m_OurLadybirdPopulation;
protected:
    // member functions
    int PreyDensityLevel(long AphidsNum) override;
    /** supplies the daydegrees collected */
    double SupplyADayDeg() const { return m_ADayDeg;}
    /**The function that transports the ladybird to a new position it returns true if a new position was found*/
    int LongRangeFind(float, int, bool&);
    void calcAphidsAppetite() override;
    void Reproduce(int p_x, int p_y) override;
    bool ShouldStartAggregating(int day);
    inline int getAphidsAppetite() override{  return m_AphidsAppetite;   };
    [[nodiscard]] double getDailyMortalityRate() const  override;
    /**
 * \brief Hibernate function for the Adult is used only in the extreme case: 1st year
 *
 * In the rest of the code the Ladybird will turn into Immobile_Adult during hibernation
 * */
    TTypesOfBeetleState st_Hibernate() override;
    TTypesOfBeetleState st_Forage() override;
    void Step() override;
    static int getClutchSize();

    int EatAphids(int) override;
    //int Cannibalise(int);
    //todo: check if the method is needed
    int getEggsLayedToday();
    //todo: check if the method is needed
    int getEggsLayedTotal();
    int getTodaysEggProduction();

    [[nodiscard]] bool getOverwintered() const{return hasOverwintered;};

    static bool getOviPositionPeriod();

    void ClearFromMap(int X, int Y) override;
    bool AddToMap(int X, int Y) override;

    /** The functions that read and write the number of Eggs per day and in total
 * */

    int getMaxSensingDistance() override;
    void setEggCounter(int num){m_EggCounter=num;};
    void decEggCounter(int num){m_EggCounter-=num;};
    int getEggCounter(){return m_EggCounter;};
    void setDailyEggs(int num){DailyEggs=num;};
    void decDailyEggs(int num){DailyEggs-=num;};
    [[nodiscard]] int getDailyEggs() const{return DailyEggs;};

    double getShortRangeDistance() override;

    void RecordUnsuccesfulAphidSearch();
    void RecordSuccesfulAphidSearch();

    int getUnsuccesfulAphidSearchNumber();
    static int getLongRangeFlightThresh();
    static int getMaxLongRangeFlights();

    void RecordLongFlight();
    void RecordNoLongFlight();
    int getLongFlightsNumber();
    TTypesOfBeetleState st_Dispersal() override;
    TTypesOfBeetleState st_Aggregate() override;
    TTypesOfBeetleState st_Aging() override;
    /**Looking for clump to hibernate*/
    bool FindClump();
    /**Checking if ready to hibernate alone*/
    bool IsHibernatingAlone();
    /**Register the beetle as being in hibernation: true on success*/
    bool RegisterHibernation();
    /**Deregister the beetle as being in hibernation: true on success*/
    bool DeregisterHibernation();
    /**Returns the number of cells to check before giving up the idea to hibernate today*/
    static int getMaxHibernationTries();
    /** Check if the clump is not too big to hibernate*/
    bool IsSpaceToHibernate();
    /** Check if someone hibernating in the point */
    bool IsSomeoneHibernating();
    /** Check if the landscape is fit for hibernation*/
    bool IsFitForHibernation();
    /** Depending on the date returns the chance to hibernate alone */
    double getChanceToHibernateAlone();
    /** Returns the first day of aggregation */
    //TODO: translate to the day length: 263
    static int getFirstDayOfAggregation();
    //TODO: translate to the day length: 293
    /** Returns the last day of aggregation */
    static int getLastDayOfAggregation();

    /** Returns the number of aphids needed so the Ladybird won't start hibernation */
    static int getAphidsToStayAwake();

    /** Returns the number of aphids needed so the Ladybird will start hibernation */
    static int getAphidsToHibernate();
    //TODO: Move the parameters to config variables
    /** returns the daily chance to hibernate */
    static double getChanceToHibernate();
    //TODO: Move the parameters to config variables
    /** Get the maximum size of the overwintering clump allowed*/
    static int getMaxClumpSize();
    void CanReproduce() override;


    bool IsReproductionReady();
    //todo: check if the method is needed
    static double getAdultDevelConst();

    void BeginStep() override;


    /**The function that implements the long range movement*/
    bool LongRangeMovement();

    [[nodiscard]] double getSoilCultivationMortality() const override;

    int getMaxCannibalise() override;

    void RandomMovement();


    // member variables
    /**The day of teh year when this particular individual emerged,
     * to be used to track minimum delay time between the emergence and oviposition*/
    unsigned short int m_EmergenceDay{0};
    bool IsHibernating{false};
    bool hasOverwintered{false};
    //todo: check if the method is needed
    short m_DailyDistance{0};
    /** The variables defining the amount of eggs laid for each animal
     * after checking we can move it to a separate vector (eigen/blitz)
     * */
    int DailyEggs{0};
    /** day degrees sum for an individual*/
    double m_ADayDeg{0};
    /** \brief days spent in hibernation */
    int m_DaysInHibernation{0};

    //int eatenToday{0};

    /** \brief The variable that holds the unsuccesfull searches for aphids
     *
     * It remembers 32 last days. Therefore the threshold for a long run would be maximum 32 (around a month)
     *
     * */
    std::bitset<FLIGHT_MEMORY_SIZE> m_UnsuccessfulAphidSearch {0};
    std::bitset<FLIGHT_MEMORY_SIZE> m_LongFlights {0};

    [[nodiscard]] double getExtremeTempMin() const override;
    

    double getMinTemp() override;

    double getMaxTemp() override;
    static double getDevelConst2() ;


    int CalcMaxEggs();

    int getMovementThreshold() const override;
};

class Ladybird_Population_Manager: public Beetle_Population_Manager, public std::enable_shared_from_this<Ladybird_Population_Manager>
{
 public:
    // member functions
    explicit Ladybird_Population_Manager(Landscape *p_L);
    /**\brief Method to check that the location is allowed for ladybird*/
    bool IsLocationAllowedForAdult(int x, int y);
    bool IsLocationAllowedForLarva(int x, int y);

/**   \brief Method to add beetles to the population */
   virtual void CreateObjects(int ob_type, TAnimal *pvo,void* null ,
                              std::unique_ptr<struct_Ladybird> data,int number);


   /** \brief Destructor: the same as in base class */
   ~Ladybird_Population_Manager() override= default;
    //virtual void Init(void) override;
    void Init() override;

    inline bool isEmergenceDay();
    int getLadybirdEmergenceDayLength() const;
    int getLadybirdEmergenceChance() const;
    /**\brief the function returns true if the weather is suitable for flying */
    bool getFlyingWeather();
    inline int getOviPositionPeriodStart() const{return ladybirdconstantslist->p_cfg_LadybirdOvipositionPeriodStart->value();};
    inline int getOviPositionPeriodEnd() const{return ladybirdconstantslist->p_cfg_LadybirdOvipositionPeriodEnd->value();};
    ;
    //todo: check if the method is needed
    inline int getMeanHopDistance() const{return ladybirdconstantslist->p_cfg_LadybirdMeanHoppingDistance->value();};
    //todo: check if the method is needed
    inline int getStdHopDistance() const {return ladybirdconstantslist->p_cfg_LadybirdStdHoppingDistance->value();};
    inline int getMaxShortRangeAttempts() const{return ladybirdconstantslist->p_cfg_LadybirdMaxShortRangeAttempts->value();};
    inline int getAphidDensityToReproduce() const{return ladybirdconstantslist->LadybirdAphidDensityToStartReproduction;};
    inline int getAphidDensityToForage() const{return ladybirdconstantslist->LadybirdAphidDensityToStartForaging;};
    inline int getMaxAphids() const{return ladybirdconstantslist->p_cfg_LadybirdMaxAphids->value();};

    // todo: move parameters to constants class in the following
    /** \brief long flight duration */
    static inline double getLongRangeDuration(){return 3000.;} // 50 min * 60 sec/min
    bool getOvipositionPeriod();
    bool IsUniSeasonal() const;
    void decAphids(int x, int y, int num);
    int getCannibalisationValue(int x, int y, int type){return (*m_LadybirdCannibalismMap)(x, y, type);};
    void decCannibalisationValue(int x, int y, int type){(*m_LadybirdCannibalismMap)(x, y, type)--;};
   // member variables
    std::unique_ptr<LadybirdConstantClass> ladybirdconstantslist{nullptr};
    probability_distribution m_LongHopRandom{"UNIINT", "0 89"};
    //todo: check if the method is needed
    probability_distribution m_RegularRandom{"UNIINT", "0 2147483647"};
    probability_distribution m_ClutchSizeRandom{"NORMAL", "36.0 10.0"};
    std::unique_ptr<probability_distribution> m_ShortRangeDistanceAdult;
    std::unique_ptr<probability_distribution> m_ShortRangeDistanceLarva;
    double SupplyEDayDeg(int day) override { return (*m_DayDeg)(day,0);}
    /** \brief Get larval day degress for a specific dayand instar */
    double SupplyLDayDeg(int L, int day) override { return (*m_DayDeg)(day,1+L);}
    /** \brief Get pupal day degress for a specific day */
    double SupplyPDayDeg(int day) override{ return (*m_DayDeg)(day,5);}
    unsigned SupplyTilePopulation(unsigned x, unsigned y, int lifestage) override;
    int getLadybirdEggProductionXY(int x, int y);
    long getAphidsNumber(int x, int y);
    void calcMaxAdultDailyDistance(double temp);
    void calcMaxLarvaDailyDistance(double temp);
    int getMaxLarvaDailyDistance();
    int getMaxAdultDailyDistance();
    /**
     * returns the number of eggs/larva/pupa/adult
     * type is:m_LadybirdDensityMap
     * 0: eggs
     * 1: L1
     * 2: L2
     * 3: L3
     * 4: L4
     * 5: Pu
     * 6: Ad
     * * */
    int getLadybirdDensity(int x, int y, int type);
    bool decLadybirdDensity(int x, int y, int type){
        if ((*m_LadybirdDensityMap)(x,y,type)==blitz::tiny((*m_LadybirdDensityMap)(x,y,type))){
            return false;
        }
        else{
            (*m_LadybirdDensityMap)(x,y,type)--;
            return true;
        }

    };
    bool incLadybirdDensity(int x, int y, int type){
        if ((*m_LadybirdDensityMap)(x,y,type)==blitz::huge((*m_LadybirdDensityMap)(x,y,type))){
            return false;
        }
        else{
            (*m_LadybirdDensityMap)(x,y,type)++;
            return true;
        }

    };
    void setCannibalism(int x, int y, int type, int num);
    /** \brief The function that for each polyref returns whether this polygon was a hibernacula or not
 * **/
    bool IsHibernacula(int a_polyref);
    int getTotalNumberEggsMax() const;
    bool IsCanibalismMapNonZero() const;
    void incCannibalisationsCount(){
        m_CannibalisationsCount++;
        cout<<"DEBUG: Increasing Cannibalisation count:"<<m_CannibalisationsCount<<endl;
    };
    void decCannibalisationsCount(){m_CannibalisationsCount--;
        cout<<"DEBUG: Decreasing Cannibalisation count:"<<m_CannibalisationsCount<<endl;
    };
    int getCannibalisationsCount()const {return m_CannibalisationsCount;};
   //Ladybird_Egg_List* m_EList[365];
protected:
    /*************************/
    /** variables */
    /*************************/
    /** \brief Max distance the larva can travel today (in mean moves) */
    int m_MaxLarvaDistance;
    /** \brief Max distance the adult can travel today (in mean moves) */
    int m_MaxAdultDistance;
    /** \brief variable that stores the open cannibalisations */
    int m_CannibalisationsCount{0};
    /** \brief
    Pointer to an output file for ladybird x y data
    */
    std::shared_ptr<ofstream> m_LadybirdXYDumpFile;
    std::vector<std::shared_ptr<AphidsD_Population_Manager>> Aphids_PM{};
    /** I am not sure about the implementation of teh next ones. */
    /** Maybe it would be better to have a proper vector here (from blitz)
     * With blitz (or better even Eigen) you can do Vector+=scalar instead of loops
     */
    /**   \brief  Storage for daily day degrees for eggs implemented in base class*/
  //  std::array<double,365> m_EDayDeg;
    /**   \brief  Storage for daily day degrees for larvae implemented in base class */
   // std::array<std::array<double,365>,4> m_LDayDeg;
    /**   \brief  Storage for daily day degrees for pupae implemented in base class */
   // std::array<double, 365> m_PDayDeg;
    /**   \brief  Storage for daily day degrees for adults implemented in base class */
   // std::array<double, 365> m_ADayDeg;
    /** \brief Storage for daily day degrees */
    std::unique_ptr<blitz::Array<double, 2>> m_DayDeg;
    /** \brief the dependence between temperature and the clutch size: updated daily */
    double TodaysEggProductionTempFactor{0.0};
    /** \brief the density of the Aphids: received daily from the Aphid model: removed, all the operations will be performed directly in the aphid data structure through the supplied methods */
    //std::unique_ptr<blitz::Array<long, 3>> m_LadybirdAphidsDensityMap;
    /** \brief the density of ladybird lifeforms max 255 elements*/
    std::unique_ptr<blitz::Array<unsigned char, 3>> m_LadybirdDensityMap;
    /** \brief the map of Cannibalism-related adjustments */
    std::unique_ptr<blitz::Array<unsigned char, 3>> m_LadybirdCannibalismMap;
    /** The list of all the polygons where for each polygon the bits correspond to whether it was used for hibernation in a particular year
    * The bitset is initialised randomly on the first year and then runs itself over. The size of the bitset corresponds to how long we are going to
    * "remember" the hibernaculae.
    * */
    std::vector<std::bitset<HIBERNACULA_MEMORY>> m_ListOfHibernaculae;
    /** \brief The number of different aphid species on the landscape: to be updated on the start and to reflect to the
     * size of m_LadybirdAphidsDensityMap*/
    int m_AphidSpeciesNum{-1};
    /*************************/
    /**  methods */
    /*************************/
    /** the function that calculates total egg temperature related factor called daily in DoFirst()*/
    static double LadybirdTotalEggTempFactor(double temp);
    void DebugOutputDoFirst();


    inline double getTodaysEggProductionTempFactor() const{return TodaysEggProductionTempFactor;};
    //void RunStepMethods() override;
    /**\brief Function that updates daydegrees vector */
    void LadybirdUpdateDayDegrees(double temptoday, int today);
    /**\brief Function that calculates daily temp-related factor of Egg production */
    static double LadybirdCalcDailyTempRelatedEggFactor(double temp);
    void DoBefore () override;
    void DoFirst() override;
    void KillOnNYEve();

    void ClearOnNYDay();




   // void LadybirdUpdateAphidsDensityMap();
   int SupplyAphidsNumber(int x, int y, int aphid_species);
   void DeductAphidsNumber(int x, int y, int aphid_species, int number_to_deduct);
   void ResetAphidsNumber(int x, int y, int aphid_species);
    //void AphidsUpdateAphidsDensityMap();

    void DoLast() override;



    bool StepFinished() override;

    static void WriteHeaders(const std::shared_ptr<ofstream>& a_file, vector<string> a_headers);

    void XYDump();
    /** \brief The function that initialises hibernaculae
 *
 * It runs only one in the simulation on the first day of the first year
 * */
    void InitialiseHibernaculae();
    /** \brief The function that updates the list of hibernaculae
     *
     * Runs each year Jan 1st*/
    void UpdateHibernaculae();
    /** \brief the amount of objects of specific type according to the density map
     * (FOR DEBUGGING
    * */

    int GetPopulationSizeFromDensityMap(int spec);

    void DebugOutputDoFirstAdult();
};



#endif

