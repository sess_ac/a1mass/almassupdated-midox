//
// Created by andrey on 1/27/21.
//
/* TESTING APHIDS MODEL
 * This is a stub model of Aphids_debug which is designed to run together with ladybird model for the development/debugging purposes
 * The distribution of Aphids_debug is stationary and defined in the start of the simulation
 *
 *
 * */
#ifndef ALMASS_APHIDSD_ALL_H
#define ALMASS_APHIDSD_ALL_H
#include <Landscape/ls.h>
#include "../BatchALMaSS/PopulationManager.h"
using namespace std;
#include <string.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
# include <unordered_set>
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PositionMap.h"
#include "../Landscape/Configurator.h"
#include "BatchALMaSS/ALMaSS_Setup.h"
#include "blitz/array.h"
#include "AphidsD_toletov.h"
class AphidsD_Population_Manager:  public Population_Manager {
public:
    /** \brief Constructor */
    explicit AphidsD_Population_Manager(Landscape* p_L);
    /** \brief Destructor */
    ~AphidsD_Population_Manager() override;
    void Init();
    void initConstantAphidsDistribution(int value); //set ups the number of aphids to be monotonous equal to value
    void setAphidsNumber(int x, int y, int number); //sets number of Aphids_debug on the specific square
    std::shared_ptr<blitz::Array<long,2>> getAphidsMap ();
    int getAphidsNumber(int x, int y);
    int deductAphidsNumber(int x, int y, int number_to_deduct);
    int getAphidsCellWidth();
    int getAphidsCellHeight();
    virtual void Run(int NoTSteps);
    int SupplyAphidsNumberLandscapeCO(int x, int y);
    void deductAphidsNumberLandscapeCO(int x, int y, int number_to_deduct);
    void zeroAphidsNumberLandscapeCO(int x, int y);
    bool SupportsAphids(int x,int y);
protected:
    std::unique_ptr<blitz::Array<long,2>> p_AphidsMap;
    CfgInt cfg_AphidCellWidth{"APHID_CELL_WIDTH", CFG_CUSTOM, 10};
    CfgInt cfg_AphidCellHeight{"APHID_CELL_HEIGHT", CFG_CUSTOM, 10};
    CfgBool cfg_ConstantAphidsDistribution{"APHID_CONST_DIST", CFG_CUSTOM, true};
    CfgInt cfg_ConstantAphidsDistributionValue{"APHID_CONST_DIST_VAL", CFG_CUSTOM, 100000};
    TTovList TovSupport;
    TToleList ToleSupport;

};





#endif //ALMASS_APHIDSD_ALL_H
