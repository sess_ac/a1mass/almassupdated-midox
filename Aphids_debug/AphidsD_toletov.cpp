//
// Created by andrey on 2/14/23.
//
# include "AphidsD_toletov.h"

AphidDTovParams::AphidDTovParams(TTovList inp){
    AphidDTovList=inp;
}
AphidDToleParams::AphidDToleParams(TToleList inp){
    AphidDToleList=inp;
}
TTovList AphidDTovParams::getList(){
    return AphidDTovList;
}
TToleList AphidDToleParams::getList(){
    return AphidDToleList;
}