//
// Created by andrey on 2/14/23.
//

#ifndef ALMASS_APHIDSD_TOLETOV_H
#define ALMASS_APHIDSD_TOLETOV_H
//
// Created by andrey on 5/4/21.
//

#ifndef ALMASS_LADYBIRD_TOLETOV_H
#define ALMASS_LADYBIRD_TOLETOV_H
#include <Landscape/ls.h>
#include <unordered_set>
#include <utility>
#include "../Beetles/Beetle_toletov.h"
using namespace std;
/*
using ATToleList=std::unordered_set<TTypesOfLandscapeElement>;
using ATTovList=std::unordered_set<TTypesOfVegetation> ;
*/
class AphidDTovParams{
public:
    TTovList getList();
    explicit AphidDTovParams(TTovList);
private:
    TTovList AphidDTovList;
};
class AphidDToleParams{
public:
    TToleList getList();
    explicit AphidDToleParams(TToleList);
private:
    TToleList AphidDToleList;
};


typedef struct AphidsToleTovs_struct{

    AphidDTovParams AphidTovs{TTovList {
            tov_Carrots,
            tov_Potatoes,
            tov_BroadBeans,
            tov_OCarrots,
            tov_DKCabbages,
            tov_DKCerealLegume,
            tov_DKLegume_Peas,
            tov_DKLegume_Beans,
            tov_DKCarrots,
            tov_DKOCabbages,
            tov_DKOCerealLegume,
            tov_SpringWheat,
            tov_WinterWheat,
            tov_OWinterWheat,
            tov_OSpringBarley,
            tov_DKOSpringWheat,
            tov_DKOWinterWheat,
            tov_DKOCarrots,
            tov_DKOSpringOats,
            tov_DKSpringOats

    }};
    AphidDToleParams AphidToles{TToleList {
            tole_PlantNursery,
            tole_Field
    }};

} TAphidsToleTovs;
#endif //ALMASS_LADYBIRD_TOLETOV_H
#endif //ALMASS_APHIDSD_TOLETOV_H
