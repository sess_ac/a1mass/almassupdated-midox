/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Partridge_Population_Manager.h This file contains the headers for the partridge population manager class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 15th October 2008 \n
 \n
 Doxygen formatted comments in October 2008 \n
 Code by Chris Topping\n
*/
//---------------------------------------------------------------------------
#ifndef Partridge_Population_ManagerH
  #define Partridge_Population_ManagerH
//---------------------------------------------------------------------------



  #include <cmath>
  #include <stdio.h>
  #include <vector>

#include "../BatchALMaSS/ALMaSS_Setup.h"

#ifdef __UNIX
  #include "../BatchALMaSS/PopulationManager.h"
  #include "Partridge_Covey.h"
  #include "Partridge_All.h"
  #include "Partridge_Communication.h"
#else
  #include "../BatchALMaSS/PopulationManager.h"
  #include "Partridge_Covey.h"
  #include "Partridge_All.h"
  #include "Partridge_Communication.h"
#endif

class Partridge_struct;
class Partridge_Covey;
struct PartridgeCommunicationData;
class Partridge_Communication;

  /** \brief For storing assessed habitat quality */
class CoverTempMap {
  public:
    ~CoverTempMap() { delete qualmap; }
	CoverTempMap(int a_lx, int a_ly) { 
		m_maxx = (a_lx / 10); m_maxy = (a_ly / 10); 
		qualmap = new double[m_maxx*m_maxy]; 
	};
	double* qualmap;
	int m_maxx, m_maxy;
	void ZeroQual() { for (int i=0; i< m_maxx; i++) for (int j=0; j<m_maxy; j++) qualmap[i + j*m_maxx] = 0; }
	double GetQual(int a_x, int a_y) { return qualmap[(a_x / 10) + m_maxx*(a_y / 10)]; }
	void SetQual(int a_x, int a_y, double a_qual) { qualmap[(a_x / 10) + m_maxx*(a_y / 10)] = a_qual; }
	void AddQual(int a_x, int a_y, double a_qual) { qualmap[(a_x / 10) + m_maxx*(a_y / 10)] += a_qual; }
	double GetQualIndexed(int a_x, int a_y) { return qualmap[a_x + a_y * m_maxx]; }
	void SetQualIndexed(int a_x, int a_y, double a_qual) {qualmap[a_x + a_y * m_maxx] = a_qual; }
};

  /** \brief  The population manager for partridge objects */
class Partridge_Population_Manager : public Population_Manager {
  //  double m_FoodNeed[200];
  //  double m_move_veg_structure[4][4];
  /** \brief Utility method called before BeginStep */
  virtual void DoFirst();
  /** \brief Utility method called before Step */
  virtual void DoBefore();
  /** \brief Utility method called before EndStep */
  virtual void DoAfter();
  /** \brief Ultility method called at the end of the time-step */
  virtual void DoLast();
  /** \brief Should the birds start to flock? */
  void TestShouldFlock();
  /** \brief Global hunting */
  void Hunting(int p_chance);
  /** \brief Density grid-based hunting */
  void HuntingGrid(int p_chance);
  /** \brief Differentiated hunting in beetlebank areas */
  void HuntingDifferentiatedBeetleBankArea( int a_pct );
  /** \brief Intitialises the nesting cover map */
  void CreateNestingCoverDensityMap( );


  // For looping through bobjects of a certain type.
  /** \brief  */
  unsigned int m_loop_index;
  /** \brief  */
  unsigned int m_loop_limit;
  /** \brief  */
  int m_loop_ob_type;
  /** \brief  */
  unsigned int m_Partridge_IDs;
  /** \brief Opens the partridge output files */
  bool OpenParOutputFiles();
  /** \brief Closes the partridge output files */
  bool CloseParOutputFiles();
  // Attributes
  /** \brief Flag for flocking or not */
  bool m_ShouldFlock;
  /** \brief  Precalculated food values with age */
  void FillInFoodArray();
  /** \brief No hatch success */
  int m_HatchSuccess;
  /** \brief internal variables */
  int m_maxx, m_maxy, m_halfwidth;
  // Output files
  /** \brief Adult mortality record */
  FILE * ParAdMort; //
  /** \brief Juvenile mortaltiy record */
  FILE * ParJuvMort; //
  /** \brief No of breeding Pairs */
  FILE * ParNoB; //
  /** \brief For each clutch, clutch no. & size */
  FILE * ParClutches; //
  /** \brief Covey size 1st oct, + no. chicks */
  FILE * ParFlocks; //
  /** \brief No unparied males 1st Jun */
  FILE * ParUnpairedMale; //
  /** \brief No. successful hatches per year */
  FILE * NoHatchedPerYear; //

  /** \brief  Part of habitat quality map evaluation*/
  int HabitatEvalPolyField( int a_field );
  /** \brief  Part of habitat quality map evaluation*/
  double EvalHabitatQual(TTypesOfLandscapeElement a_cet, int a_poly);
  /** \brief  Part of habitat quality map evaluation*/
  double m_HabitatQuality[300];
  /** \brief Kill/clone a configuragle proportion of population*/
  virtual void Catastrophe();
  /** \brief If male immigration is needed - Unused */
  void MaleImmigration( void );

public:
  /** \brief Debug */
  Partridge_Male* bad_guys[500];
  /** \brief Message data */
  PartridgeCommunicationData* m_comms_data;
  /** \brief Message class pointer */
  Partridge_Communication m_messagecentre;
  /** \brief Pointer to territory quality map */
  CoverTempMap* m_territoryqualmap;
  /** \brief Pointer to nesting cover map */
  CoverTempMap* m_nestingcovermap;
  /** \brief Pointer to kfactors object */
  k_factors * m_Ourkfactors;
  /** \brief No starvation events */
  int m_Starved;
/*
  int m_Matured;
  int m_Eaten;
  int m_MaturedE;
  int m_EatenE;
*/
  /** \brief Get a list of neighbour covies */
  bool FillCoveyNeigbourList(Partridge_Covey* a_covey, int a_distance, int a_x, int a_y);
  /** \brief Habitat evaluation */
  double TerrEvalPoly( TTypesOfLandscapeElement a_cet, int a_poly );
  /** \brief Returns clutch density at x,y with radius radius */
  int ClutchDensity( int x, int y, int radius);

  /** \brief Record starvation event */
 void AddStarved() {
    m_Starved++;
  }

 /* void AddMatured() {
    m_Matured++;
  }

  void AddEaten() {
    m_Eaten++;
  }

  void AddMaturedE() {
    m_MaturedE++;
  }

  void AddEatenE() {
    m_EatenE++;
  }
*/

  //  void SetFoodNeed(int i, double value) {m_FoodNeed[i]=value;}
  //  double GetFoodNeed(int i) {return m_FoodNeed[i];}
  //  void SetMoveVegStructure(int i, int j, double value) {m_move_veg_structure[i][j]=value;}
  //  double GetMoveVegStructure(int i, int j) {return m_move_veg_structure[i][j];}

	/** \brief Output method */
  void WriteParAdMort( int a_min, int a_age, int a_sex, int a_cause );
	/** \brief Output method */
  void WriteParJuvMort( int yr, int a_min, int a_age, int a_cause );
	/** \brief Output method */
  void WriteParNoB( int a_min, int a_nob );
	/** \brief Output method */
  void WriteParUnpairedMale( int a_min, int a_num );
	/** \brief Output method */
  void WriteParClutches( int a_min, int a_num1, int a_num2 );
	/** \brief Output method */
  void WriteParFlocks( int a_min, int a_size, int a_young );
	/** \brief Output method */
  void WriteNoHatchedPerYear( int a_min, int a_num );

  /** \brief  */
  void AddHatchSuccess( int sz ) {
    m_HatchSuccess += sz;
  }

  /** \brief  */
  int SupplyPegPosx( int j ) {
    return dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey,j) )->X();
  };

  /** \brief  */
  int SupplyPegPosy( int j ) {
    return dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey,j) )->Y();
  };

  /** \brief  */
  int SupplyCovPosx( int j ) {
    return dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey,j) )->XCenter();
  };

  /** \brief  */
  int SupplyCovPosy( int j ) {
    return dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey,j) )->YCenter();
  };


  /** \brief  */
  void ObjectLoopInit( int ob_type );
  /** \brief  */
  TAnimal * ObjectLoopFetch( void );

  /** \brief  */
  void CreateObjects( int ob_type, Partridge_struct * data, int number );
  /** \brief  */
  void CreateInitialObjects( int ob_type, int number );
  /** \brief  */
  void CreateCloneObjects( int ob_type, AdultPartridge_struct * as );
  /** \brief  */
  void AddObject( int ob_type, TAnimal * pTAo );

  /** \brief  */
  virtual void Init( void );
  /** \brief  */
  Partridge_Population_Manager( Landscape * a_map );
  /** \brief  */
  virtual ~Partridge_Population_Manager( void );
  // Interface
  /** \brief  */
  void UpdateNestingCoverMap();
  /** \brief  */
  double GetNestingCoverDensity( int x, int y );
  /** \brief  */
  double GetTerrQual( int x, int y );
  /** \brief  */
  unsigned int GetNewID() {
    return ++m_Partridge_IDs;
  }

  /** \brief  */
  bool SupplyShouldFlock() {
    return m_ShouldFlock;
  };

  /** \brief  */
  void DissolveCovey( Partridge_Covey * a_covey );
  /** \brief  */
  bool CoveyDissolveWeather();
	/** \brief Debug only */
  void DoSanityCheck(); // Is a de-bug method and can be removed at some point
	/** \brief Debug only */
  void DoMaleSanityCheck(); // ditto
	/** \brief Output method */
    virtual void TheAOROutputProbe();
	/** \brief Output method */
    virtual void TheRipleysOutputProbe(FILE* a_prb);

  /** \brief  */
  double GetHabitatQuality (int a_day) {return m_HabitatQuality[a_day];}

};

#endif

