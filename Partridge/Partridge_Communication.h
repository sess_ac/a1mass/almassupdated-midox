/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Partridge_Communication.h This file contains the headers for the partridge communication class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of 15th October 2008 \n
 \n
 Doxygen formatted comments in October 2008 \n
 Code by Chris Topping\n
*/
//---------------------------------------------------------------------------
#ifndef partridge_communicationH
  #define partridge_communicationH
//---------------------------------------------------------------------------

class Partridge_Covey;
class Partridge_Clutch;
class Partridge_Chick;
class Partridge_Chick2;
class Partridge_Male;
class Partridge_Female;

/**
\brief
Possible messages to pass
*/
typedef enum {
  pcomm_KillChick,
  pcomm_MaleDying,
  pcomm_ChicksMature,
  pcomm_Mating,
  pcomm_MatingM,
  pcomm_ClutchDead,
  pcomm_AllInfertile,
  pcomm_EggsHatch,
  pcomm_SetClutch,
  pcomm_AllChicksDead,
  pcomm_WaitForMale,
  pcomm_ClutchMown,
  pcomm_MumDeadC,
  pcomm_FemaleGivingUp,
  pcomm_ClutchEaten,
  pcomm_StoppingBreeding,
  pcomm_MovingHome
}
TypeOfPartridge_Communication;

/**
\brief
Data structure of a message
*/
struct PartridgeCommunicationData {
  // Attributes
  Partridge_Clutch* m_clutch;
  Partridge_Chick* m_chick;
  Partridge_Chick2* m_chick2;
  Partridge_Male* m_male;
  Partridge_Female* m_female;
  Partridge_Covey* m_covey;
  bool m_HaveTerritory; // Special bool - used a lot
  bool m_boolean; // used for transfer of boolean values
  int m_int; // used for transfer of int values
  double m_float; // used for transfer of double values
};

/**
\brief
Used for messaging
*/
/**
Messages are passed between birds, and this a common cause of bugs if the information passed in inconsistent. This class therefore has two
purposes - collecting messaging code together for ease of overview, and as a debug function. \n
*/
class Partridge_Communication {
public:
  // Methods
/**
\brief
Void data
*/
  void ClearData(PartridgeCommunicationData* pc_data);
/**
\brief
Pass a message
*/
  bool PassMessage(PartridgeCommunicationData* pc_data,TypeOfPartridge_Communication pc);
};

#endif
