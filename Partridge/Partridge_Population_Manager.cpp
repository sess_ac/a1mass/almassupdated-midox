/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Partridge_Population_Manager.cpp This file contains the code for the partridge population manager class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 15th October 2008 \n
 All rights reserved. \n
 \n
 Doxygen formatted comments in October 2008 \n
 Code by Chris Topping\n
*/

#include <cmath>
#include <string.h>
#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Partridge/Partridge_Communication.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Partridge/Partridge_Covey.h"
#include "../Partridge/Partridge_toletov.h"
#include "../BatchALMaSS/AOR_Probe.h"


#ifdef __NESTPOSITIONS_OUT
	static CfgInt cfg_ParNestPosOutStart( "PAR_NESPOSTOUTSTART", CFG_CUSTOM, 20 );
	static CfgInt cfg_ParNestPosOutEnd( "PAR_NESPOSTOUTEND", CFG_CUSTOM, 30 );
	FILE* NestPositions;
#endif

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//extern void FloatToDouble(double&,float);

double g_par_rainfactor;
//double g_move_veg_structure[4] [4];
double g_FoodNeed[200];
int g_MaxWalk[200];


// The data below is the culmulative probabability of covey dissolution
// if it used to determine when the covey dissovles by the partridge_pop_man
const int DissolveChance[40] =
{
  0, 381, 1123, 2205, 3609, 5314, 7300, 9548, 12036, 14746, 17657, 20749, 24003, 27399, 30916, 3453, 38235, 41997, 45801,
       49626, 53454, 57263, 61035, 64748, 68383, 71921, 75341, 78623, 81747, 84694, 87443, 89974, 92268, 94305, 96064, 97525,
       98670, 99477, 99927, 100000
};

extern CfgInt cfg_CatastropheEventStartYear;
extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;

extern CfgInt cfg_par_start_dissolve;
extern CfgInt cfg_par_last_brood_date;
extern CfgInt cfg_par_max_terr_qual;
extern CfgInt cfg_par_min_terr_qual;

extern CfgBool cfg_ReallyBigOutputMonthly_used;
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;
//extern CfgInt cfg_par_female_mortality_alone;
extern CfgInt cfg_nest_hedgebank0;
extern CfgInt cfg_nest_hedgebank1;
extern CfgInt cfg_nest_hedgebank2;
extern CfgInt cfg_par_male_gives_up;
extern CfgInt cfg_par_start_dissolve;
extern CfgInt cfg_par_mature_threshold;
extern CfgBool cfg_ReallyBigOutput_used;

extern CfgBool cfg_BeetleBankInvert;
extern CfgInt cfg_BeetleBankMinX;
extern CfgInt cfg_BeetleBankMaxX;
extern CfgInt cfg_BeetleBankMinY;
extern CfgInt cfg_BeetleBankMaxY;


/** \brief Increase in energy requirement with rainfall */
static CfgFloat cfg_par_rainfactor("PAR_RAINFACTOR",CFG_CUSTOM,0.2);
/** \brief Correction for using rainfall penalty */
static CfgFloat cfg_rainscaling("PAR_RAINSCALING",CFG_CUSTOM,0.767);
/** \brief Probe date */
static CfgInt cfg_springcensusdate("PAR_SPRINGCENSUSDATE",CFG_CUSTOM,March+21);
/** \brief Loss due to emmigration (out of 10000) */
static CfgInt cfg_Emigration( "PAR_EMIGRATION", CFG_CUSTOM, 1 ); //  0 upwards, e.g. 3000 = 30% of current population size emigrates
/** \brief Emigration rate of alone individuals (of 10000) */
CfgInt cfg_IndividualEmigration( "PAR_INDIVIDUALEMIGRATION", CFG_CUSTOM, 3500 ); //  0 upwards, e.g. 30 = 30% of current population size immigrates
/** \brief unused */
static CfgInt cfg_MaleImmigration( "PAR_MALEIMMIGRATION", CFG_CUSTOM, 0 ); //  0 upwards, e.g. 30 = 30% of current population size immigrates
/** \brief Number of birds at which hunting starts */
static CfgInt cfg_ParHuntingThreshold("PAR_HUNTINGTHRESHOLD",CFG_CUSTOM,1);
/** \brief Size if hunting grid (m) */
static CfgInt cfg_ParHuntingGridSize("PAR_HUNTINGGRIDSIZE",CFG_CUSTOM,1000);
/** \brief 0 if global hunting, else threshold */
static CfgInt cfg_ParHuntingType("PAR_HUNTINGTYPE",CFG_CUSTOM,1); // 1==Grid & threshold, 2 == beetlband differentiated, otherwise global
/** \brief First year with hunting */
static CfgInt cfg_ParShootStartYear( "PAR_SHOOTSTARTYEAR", CFG_CUSTOM, 1 );
/** \brief Minimum% shot */
static CfgInt cfg_ParShotMin( "PAR_SHOTPERCENTAGE_MIN", CFG_CUSTOM, 1500 ); // out of 10000
/** \brief Maximum% shot */
static CfgInt cfg_ParShotMax( "PAR_SHOTPERCENTAGE_MAX", CFG_CUSTOM, 1500 );
/** \brief Used to scale biomass hindrance */
static CfgFloat cfg_HindranceScalingFactor( "PAR_HINDRANCE_SCALING", CFG_CUSTOM, 1000 );
/** \brief Start no of male partridges */
static CfgInt cfg_StartingNumberMalePartridges( "PAR_STARTING_NO_MALES", CFG_CUSTOM, 10000);
/** \brief Start no of female partridges */
static CfgInt cfg_StartingNumberFemalePartridges( "PAR_STARTING_NO_FEMALES", CFG_CUSTOM, 10000 );
/** \brief Point at which the simulation is terminated due to excessive population size */
static CfgInt cfg_FemalePopSizeMaxCutoff("PAR_FEMMAXPOPSIZECUTOFF",CFG_CUSTOM,7000);
/** \brief Slope of linear increase in energy needs of chicks */
static CfgFloat cfg_par_energyslope_perday( "PAR_ENERGYSLOPE_PERDAY", CFG_CUSTOM, 1.06 );
/** \brief Date when birds start to flock */
static CfgInt cfg_flockdata_date( "PAR_FLOCKDATA_DATE", CFG_CUSTOM, 270 );
/** \brief Probe date */
static CfgInt cfg_breedingpairs_date( "PAR_BREEDINGPAIRS_DATE", CFG_CUSTOM, 90 );
/** \brief Probe date */
static CfgInt cfg_unpaireddata_date( "PAR_UNPAIREDDATA_DATE", CFG_CUSTOM, 150 );
/** \brief Probe date */
static CfgInt cfg_kfactoroutput_date( "PAR_KFACTOROUTPUT_DATE", CFG_CUSTOM, 90 );

/** \brief The hunting rate in beetle bank areas */
CfgInt cfg_ParHuntingBeetleBankArea("PAR_HUNTING_BEETLEBANKAREA",CFG_CUSTOM,500 ); // out of 10000



//******* Below are essential methods needed by all Population_Managers ********
//

Partridge_Population_Manager::Partridge_Population_Manager(Landscape* L) : Population_Manager(L, 6)
{
    // Load List of Animal Classes
    m_ListNames[0] = "Clutch";
    m_ListNames[1] = "Chick";
    m_ListNames[2] = "Chick2";
    m_ListNames[3] = "Male";
    m_ListNames[4] = "Female";
    m_ListNames[5] = "Covey";
    m_ListNameLength = 6;
    m_population_type = TOP_Partridge;
    Init();
    m_Starved = 0;
    /*  m_Matured = 0; m_Eaten = 0; */
    m_comms_data = new PartridgeCommunicationData;
}

//-----------------------------------------------------------------------------

Partridge_Population_Manager::~Partridge_Population_Manager()
{
  CloseParOutputFiles();
  delete m_Ourkfactors;
  delete m_territoryqualmap;
  delete m_nestingcovermap;
  delete m_comms_data;
}

//-----------------------------------------------------------------------------
void Partridge_Population_Manager::ObjectLoopInit( int ob_type )
{
  m_loop_index = 0;
  m_loop_ob_type = ob_type;
  m_loop_limit = (int) SupplyListSize(ob_type);
}

//-----------------------------------------------------------------------------

TAnimal * Partridge_Population_Manager::ObjectLoopFetch( void )
{

  if ( m_loop_index >= m_loop_limit )
    return NULL;

  return SupplyAnimalPtr(m_loop_ob_type,m_loop_index++);
}

//-----------------------------------------------------------------------------

void Partridge_Population_Manager::Init( void )
{
  if ( cfg_RipleysOutput_used.value() ) {
    OpenTheRipleysOutputProbe("");
  }
  if ( cfg_ReallyBigOutput_used.value() ) {
	OpenTheReallyBigProbe();
  } else ReallyBigOutputPrb=0;
  // autom. called by constructor
  m_SimulationName = "Partridge";
  m_Partridge_IDs = 0;
  // Create some male and female Partridges
  int num = cfg_StartingNumberMalePartridges.value();
  for ( int i = 0; i < num; i++ )
  {
    CreateInitialObjects( pob_Male, 1 );
  }
  for ( int i = 0; i < cfg_StartingNumberFemalePartridges.value(); i++ )
  {
    CreateInitialObjects( pob_Female, 1 );
  }


  // Load State Names
  StateNames[pars_Initiation] = "ALL Initiation";
  StateNames[pars_CMoving] = "Clutch Moving";
  StateNames[pars_ClDeveloping] = "Clutch Developing";
  StateNames[pars_ClHatching] = "Clutch Hatching";
  StateNames[pars_ClDying] = "Clutch Dying";
  StateNames[pars_ChDeveloping] = "Chick_Developing";
  StateNames[pars_ChMaturing] = "Chick_Maturing";
  StateNames[pars_ChDying] = "Chick_Dying";
  StateNames[pars_MFlocking] = "Male Flocking";
  StateNames[pars_MFindingMate] = "Male Finding Mate";
  StateNames[pars_MFollowingMate] = "Male Following Mate";
  StateNames[pars_MPairing] = "Male Pairing";
  StateNames[pars_MGuardingMate] = "Male Guarding Mate";
  StateNames[pars_MCaringForYoung] = "Male Caring For Young";
  StateNames[pars_MDying] = "Male Dying";
  StateNames[pars_FFlocking] = "Female Flocking";
  StateNames[pars_FFindingTerritory] = "Female Finding Territory";
  StateNames[pars_FAttractingMate] = "Female Attracting A Mate";
  StateNames[pars_FBuildingUpResources] = "Female Building Up Resources";
  StateNames[pars_FMakingNest] = "Female Making Nest";
  StateNames[pars_FLaying] = "Female Laying";
  StateNames[pars_FStartingNewBrood] = "Female Starting New Brood";
  StateNames[pars_FIncubating] = "Female Incubating";
  StateNames[pars_FCaringForYoung] = "Female Caring For Young";
  StateNames[pars_FDying] = "Female Dying";
  StateNames[pars_CoveyDissolve] = "Covey Dissolve";
  StateNames[pars_CoveyBeing] = "Covey Exists";
  StateNames[pars_Destroy] = "ALL Destroy";
  StateNamesLength = 28;
  // determine whether we should shuffle, or sort or do nothing to Partridges
  // after each time step.
  BeforeStepActions[0] = 3; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[1] = 3; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing

  BeforeStepActions[2] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[3] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[4] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[5] = 0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing

  // They all start flocking
  m_ShouldFlock = true;

  // OPen the output files
  m_HatchSuccess = 0;
  if ( !OpenParOutputFiles() )
  {
    m_TheLandscape->Warn( "Partridge_Pop_Man::Init(): ""Problem opening output files", "" );
    exit( 1 );
  }
#ifdef __NESTPOSITIONS_OUT
NestPositions=fopen("NestPositions.txt","w");
  if ( !NestPositions )
  {
    g_msg->Warn( WARN_BUG, "Partridge_Population_Manager::Init(): Cannot open NestPositions.txt file", "" );
    exit( 1 );
  }
#endif
  // [Height][Biomass]
/*
FILE * HBdata = fopen("HB_impedence_18072010.txt", "r" );
  if ( !HBdata )
  {
    g_msg->Warn( WARN_BUG, "Partridge_Population_Manager::Init(): Cannot open HBdata file", "" );
    exit( 1 );
  }
  double d;
  float f;
  double sc = cfg_HindranceScalingFactor.value();
  for ( int h = 0; h < 4; h++ )
    for ( int b = 0; b < 4; b++ )
    {
      fscanf( HBdata, "%g\n", & f );
	  FloatToDouble(d,f);
      //SetMoveVegStructure(h,b,d);
      d += ( 1.0 - d ) / sc;
      g_move_veg_structure[h] [b] = d;
    }
  fclose( HBdata );
*/
  FillInFoodArray();
  // Create analysis objects
  m_Ourkfactors = new k_factors;
  m_Ourkfactors->m_ThisYear->setNoMalesSept( 100000000 ); // this is just to avoid log error in the first year
  m_Ourkfactors->CreateInfoDatabaseFile();
  // Need to create and fill in the density maps
  int lx = m_TheLandscape->SupplySimAreaWidth();
  int ly = m_TheLandscape->SupplySimAreaHeight();
  m_maxx = lx / 10;
  m_maxy = ly / 10;
  m_halfwidth = lx / 2;
  m_territoryqualmap = new CoverTempMap(lx,ly);
  m_nestingcovermap = new CoverTempMap(lx,ly);
  CreateNestingCoverDensityMap();
  g_par_rainfactor = cfg_par_rainfactor.value();

  // Added by Krishnan Sudharsan March 22 2007 and March 23 2007
  m_HabitatQuality[0]=0;
  int daydiff = cfg_par_last_brood_date.value() - cfg_par_start_dissolve.value();
  double alpha_max = 4.00000; // Maximum power value
  double alpha_min = 0.00000; // Minimum power value
  double alpha_medium = (alpha_max + alpha_min) / 2; // Changing value of mid point
  double m_medium_tq = cfg_par_max_terr_qual.value() - pow(daydiff, alpha_medium); // Territory quality when power is intermediate
  // the range of territory quality within which our target has to be
  double range_min = cfg_par_min_terr_qual.value()- cfg_par_min_terr_qual.value()* 0.01;
  double range_max = cfg_par_min_terr_qual.value()+ cfg_par_min_terr_qual.value()* 0.01;

  // Search value has to be equal to territory minimum value at end
  // Binary search to find the power that decreases TQ from max to min in given days
  // Continue searching till the search value equals the minimum territory value.
  // Stop when the search value = t_min. Time at this point = daydiff.
  while (m_medium_tq < range_min || m_medium_tq > range_max)
  {
	  if (m_medium_tq < range_min){
		  alpha_max = alpha_medium ;
		  alpha_medium = (alpha_min + alpha_max) / 2;}
	  else {
		  alpha_min = alpha_medium;
		  alpha_medium = (alpha_min + alpha_max) / 2;}
	  m_medium_tq = cfg_par_max_terr_qual.value() - pow(daydiff, alpha_medium);
  }
  // For sake of safety I added 1 extra day for the territoryquality assessment
  for (int i = 0; i < daydiff+1; i++){
	  m_HabitatQuality[i] = cfg_par_max_terr_qual.value() - pow(i,alpha_medium);}

}

//-----------------------------------------------------------------------------

void Partridge_Population_Manager::CreateObjects( int ob_type, Partridge_struct * data, int number )
{
  Partridge_Clutch * New_Clutch;
  Partridge_Chick * New_Chick;
  Partridge_Chick2 * New_Chick2;
  Partridge_Male * New_Male;
  Partridge_Female * New_Female;
  Partridge_Covey * New_Flock;

  Clutch_struct * cs;
  Chick_struct * pcs;
  AdultPartridge_struct * as;
  Covey_struct * covs;

  // Register the event with k_factors
  if ( ob_type == pob_Chick ) m_Ourkfactors->m_ThisYear->incNoClutchesHatched();

  for ( int i = 0; i < number; i++ )
  {
    if ( ob_type == 0 ) // Clutch creation
    {

      cs = dynamic_cast < Clutch_struct * > ( data );
      New_Clutch = new Partridge_Clutch( cs->x, cs->y, cs->Mum, cs->m_covey, m_TheLandscape, cs->No,
           cs->family_counter, this );
        PushIndividual(ob_type,New_Clutch);
      // Give mum the pointer to the clutch
      m_comms_data->m_clutch = New_Clutch;
      m_comms_data->m_female = cs->Mum;
      m_messagecentre.PassMessage( m_comms_data, pcomm_SetClutch );
      //      cs->Mum->OnSetMyClutch( New_Clutch );
      // Register the event with k_factors
      m_Ourkfactors->m_ThisYear->incNoClutches();
#ifdef __NESTPOSITIONS_OUT
	  int y=m_TheLandscape->SupplyYearNumber();
	  if ((y>=cfg_ParNestPosOutStart.value()) &&(y<cfg_ParNestPosOutEnd.value())) {
		  int veg=int(m_TheLandscape->SupplyVegType(cs->x, cs->y));
		  int ele=int(m_TheLandscape->SupplyElementType(cs->x, cs->y));
		fprintf(NestPositions,"%d\t%d\t%d\t%d\t%d\n",y,cs->x, cs->y,veg,ele);
	  }
#endif
	}
    if ( ob_type == 1 ) // Chick
    {
      pcs = dynamic_cast < Chick_struct * > ( data );
      bool sex = false;
      // if ( random( 2 ) == 1 ) sex = true; Sex ratio not 50:50
	  if ( random( 211 ) > 100 ) sex = true; // sex ratio 111/100 males/females Dammerham
      New_Chick = new Partridge_Chick( pcs->x, pcs->y, pcs->Mum, pcs->m_covey, m_TheLandscape, sex,
           pcs->family_counter, this );
      // Add the new chick to the population managers arrays
        PushIndividual(ob_type,New_Chick);
      m_Ourkfactors->m_ThisYear->incNoChicksHatched();
    }
    if ( ob_type == 2 ) // Chick2
    {
      pcs = dynamic_cast < Chick_struct * > ( data );
      New_Chick2 = new Partridge_Chick2( pcs->x, pcs->y, pcs->Mum, pcs->m_covey, m_TheLandscape, pcs->sex,
           pcs->family_counter, this );
      // Add the new chick to the population managers arrays
        PushIndividual(ob_type,New_Chick2);
      // Record the fact that we got this old
      m_Ourkfactors->m_ThisYear->incNoChicksSixWeeks();
    }
    if ( ob_type == 3 ) // Male
    {
      as = dynamic_cast < AdultPartridge_struct * > ( data );
      // Male
      New_Male = new Partridge_Male( as->bx, as->by, as->x, as->y, as->m_covey, m_TheLandscape, as->family_counter, this );
        PushIndividual(ob_type,New_Male);
    }
    if ( ob_type == 4 ) // Female
    {
      as = dynamic_cast < AdultPartridge_struct * > ( data );
      // Female
      New_Female = new Partridge_Female( as->bx, as->by, as->x, as->y, as->m_covey, m_TheLandscape, as->family_counter, this );
        PushIndividual(ob_type,New_Female);
    }
    if ( ob_type == 5 ) // Covey
    {
      covs = dynamic_cast < Covey_struct * > ( data );
      New_Flock = new Partridge_Covey( covs->first_member, this, covs->x, covs->y, m_TheLandscape );
        PushIndividual(ob_type,New_Flock);
    }
  }
}

//-----------------------------------------------------------------------------

void Partridge_Population_Manager::AddObject( int ob_type, TAnimal * pTAo )
{
    PushIndividual(ob_type,pTAo);
}

//-----------------------------------------------------------------------------


void Partridge_Population_Manager::CreateInitialObjects( int ob_type, int number )
{
  Partridge_Male * New_Male;
  Partridge_Female * New_Female;
  Partridge_Covey * New_Flock;

  int extentx = m_TheLandscape->SupplySimAreaWidth();
  int extenty = m_TheLandscape->SupplySimAreaHeight();

  for ( int i = 0; i < number; i++ )
  {
		unsigned x = 0;
		unsigned y = 0;
		bool OK=false;
		while (!OK) {
			x = random( extentx );
			y = random( extenty );
            
            OK = partridge_tole_initOK(m_TheLandscape, x, y);

		}
    if ( ob_type == pob_Male ) // Male
    {
      // Male
      New_Male = new Partridge_Male( 0, 0, x , y, NULL, m_TheLandscape, random( 1000000 ), this );
        PushIndividual(ob_type,New_Male);

      New_Flock = new Partridge_Covey( New_Male, this, New_Male->Supply_m_Location_x(), New_Male->Supply_m_Location_y(), m_TheLandscape );
      New_Male->SetCovey( New_Flock );
    }
    if ( ob_type == pob_Female ) // Female
    {
      New_Female = new Partridge_Female( 0, 0, x , y, NULL, m_TheLandscape, random( 1000000 ), this );
        PushIndividual(ob_type,New_Female);
      New_Flock = new Partridge_Covey( New_Female, this, New_Female->Supply_m_Location_x(), New_Female->Supply_m_Location_y(), m_TheLandscape );
      New_Female->SetCovey( New_Flock );
    }
  }
}

//-----------------------------------------------------------------------------


void Partridge_Population_Manager::CreateCloneObjects( int ob_type, AdultPartridge_struct * as )
{
	Partridge_Male * New_Male;
	Partridge_Female * New_Female;
	Partridge_Covey * New_Flock;

	if ( as->sex ) // Male
    {
		// Male
		New_Male = new Partridge_Male( as->bx, as->by, as->x, as->y, NULL, m_TheLandscape, as->family_counter, this );
        PushIndividual(ob_type,New_Male);
		New_Flock = new Partridge_Covey( New_Male, this, New_Male->Supply_m_Location_x(), New_Male->Supply_m_Location_y(), m_TheLandscape );
		New_Male->SetCovey( New_Flock );
		// Must now set age.
		New_Male->SetAge(as->age);
    }
	else // Female
    {
		// Female
		New_Female = new Partridge_Female( as->bx, as->by, as->x, as->y, NULL, m_TheLandscape, as->family_counter, this );
        PushIndividual(ob_type,New_Female);
		New_Flock = new Partridge_Covey( New_Female, this, New_Female->Supply_m_Location_x(), New_Female->Supply_m_Location_y(), m_TheLandscape );
		New_Female->SetCovey( New_Flock );
		// Must now set age.
		New_Female->SetAge(as->age);
    }
}

//-----------------------------------------------------------------------------

//*************** End of essential methods ***************

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int Partridge_Population_Manager::ClutchDensity( int x, int y, int radius )
{
  // Counts the number of clutches that are within 500m of x,y
  int result = 0;
  AnimalPosition ap;
  int sz = (int) SupplyListSize(pob_Clutch);
  for ( int c = 0; c < sz; c++ )
  {

    int X{0},Y{0};
    SupplyLocXY(pob_Clutch,c,X,Y);
    if ( ( ( abs( ( int )( X - x ) ) % m_halfwidth ) < radius )
         && ( ( abs( ( int )( Y - y ) ) % m_halfwidth ) < radius ) )result++;
  }
  return result;
}

//---------------------------------------------------------------------------


/**
One of the primary methods where output and control activities can occur. Since this is the first method called before BeginStep there are no internal inconsistencies and it is certain that all extant objects will be counted/informed.\n
*/
void Partridge_Population_Manager::DoFirst()
{
  /* if (g_date->Date()<730) { m_female_mortality_alone=0; if (g_date->Date()==729) {
  m_female_mortality_alone=cfg_par_female_mortality_alone.value(); } } */
  int today = m_TheLandscape->SupplyDayInYear();

  if (today==March+22) {
	  if (m_TheLandscape->SupplyYearNumber()>5) {
		Partridge_Male* PM = NULL; // assignment to get rid of warning
		Partridge_Female* PF = NULL;
		  if ((int) SupplyListSize(pob_Female) > cfg_FemalePopSizeMaxCutoff.value() ){
	  			unsigned size2 = (unsigned) SupplyListSize(pob_Male);
				for ( unsigned j = 0; j < size2; j++ ) {
					PM = dynamic_cast < Partridge_Male * > ( SupplyAnimalPtr(pob_Male,j) );
					PM->KillThis();
				}
				size2 = (unsigned) SupplyListSize(pob_Female);
				for ( unsigned j = 0; j < size2; j++ ) {
					PF = dynamic_cast < Partridge_Female * > ( SupplyAnimalPtr(pob_Female, j) );
					PF->KillThis();
				}
		  }
	  }
  }

  // Need to UpdateNestingCoverMap every so often in the breeding season
  if ( ( today >= cfg_par_start_dissolve.value() ) && ( today < cfg_par_male_gives_up.value() )  && (SupplyListSize(pob_Female)>0))
  {
	  int daysfromstart=today-cfg_par_start_dissolve.value();
	  // for now do it every 7 days
	  if ( daysfromstart % 7 == 0 ) UpdateNestingCoverMap();
  }
  if ( today == (cfg_springcensusdate.value()-1) )
  {
	/*
	// Before we do anything we may need to have emigration
	if (cfg_Emigration.value() >0) {
		unsigned int sz = (int)TheArray[pob_Covey].size();
		int testvalue;
		if (sz>1) testvalue=cfg_Emigration.value(); else testvalue=cfg_IndividualEmigration.value();
		for (unsigned int c=0; c<sz; c++) {
			if (random(10000) < testvalue) {
				dynamic_cast < Partridge_Covey * > ( TheArray[pob_Covey] [c] )->CoveyEmigrate();
			}
		}
	}
	*/
	  // Testing 15/04/08
	// Before we do anything we may need to have emigration
	if (cfg_Emigration.value() >0) {
		unsigned int sz = (int) SupplyListSize(pob_Covey);
		int testvalue=cfg_Emigration.value();
		for (unsigned int c=0; c<sz; c++) {
			if (random(10000) < testvalue) {
				dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey, c) )->CoveyEmigrate();
			}
			else {
				dynamic_cast < Partridge_Covey * > (SupplyAnimalPtr(pob_Covey,c))->CoveyIndividualEmigrate();
			}
		}
	}
  }
  if ( today == cfg_springcensusdate.value() )
  {
	// before 1st clutch is possible 80
    int birds = (int) SupplyListSize(pob_Male) + (int) SupplyListSize(pob_Female);
    // No Breeding Pairs
    // Calculate and write output to flock data file
    int si = (int) SupplyListSize(pob_Female);
    int nobs = 0;
    int nts = 0;
    for ( int i = 0; i < si; i++ )
    {
      // Generate a date to dissolve
      if ( dynamic_cast < Partridge_Female * > ( SupplyAnimalPtr(pob_Female,i) )->GetMate() ) nobs++;
      if ( dynamic_cast < Partridge_Female * > ( SupplyAnimalPtr(pob_Female,i) )->HaveTerritory() ) nts++;
    }
    m_Ourkfactors->m_ThisYear->SetPairsInApril( nobs );
    m_Ourkfactors->m_ThisYear->SetTerritorialFemalesInApril( nts );
    m_Ourkfactors->m_ThisYear->SetFemalesInApril( si );
    si = (int) SupplyListSize(pob_Male);
    m_Ourkfactors->m_ThisYear->SetMalesInApril( si );
    m_Ourkfactors->m_ThisYear->SetBirdsInApril( birds );
    m_Ourkfactors->calcreal_3();
    m_Ourkfactors->Output_kfactors();
  }

  // Added by Krishnan Sudharsan on May 15, 2007
  // Needed to count territorial/non-territorial females in May
  if ( today == May+15 )
  { int num = (int) SupplyListSize(pob_Female);
	int nobs = 0;
	int difference = 0;
	for ( int i = 0; i < num; i++ )
    {
      // Find the number of territorial females on May 15th
      if ( dynamic_cast < Partridge_Female * > ( SupplyAnimalPtr(pob_Female,i) )->HaveTerritory() ) nobs++;
    }
	difference = num - nobs;
	m_Ourkfactors->m_ThisYear->SetTerritorialFemalesInMay( nobs );
    m_Ourkfactors->m_ThisYear->SetNonTerritorialFemalesInMay( difference );
  }

  if ( today == August+4  )
  { // Aug 5th
    m_Ourkfactors->m_ThisYear->setNoChick1sAug((int) SupplyListSize(pob_Chick));
    m_Ourkfactors->m_ThisYear->setNoChick2sAug((int) SupplyListSize(pob_Chick2));
	m_Ourkfactors->m_ThisYear->setNoFemsAug((int) SupplyListSize(pob_Male));
	m_Ourkfactors->m_ThisYear->setNoMalesAug((int) SupplyListSize(pob_Female));
  }

  if ( today == September )
  { // 1st Sept
    // add 50% of chicks to males and females - assumes a 1:1 ratio at hatch
    int nochicks = (int) SupplyListSize(pob_Chick);
    nochicks += (int) SupplyListSize(pob_Chick2);
    m_Ourkfactors->m_ThisYear->setNoFemalesSept( ( nochicks / 2 ) + (int) SupplyListSize(pob_Female) );
    m_Ourkfactors->m_ThisYear->setNoMalesSept( ( nochicks / 2 ) + (int) SupplyListSize(pob_Male) );
    // Need to set old birds values too
    Partridge_Base * pf;
    unsigned sz = (unsigned) SupplyListSize(pob_Female);
    int oldbirds = 0;
    for ( unsigned j = 0; j < sz; j++ )
    {
      pf = dynamic_cast < Partridge_Base * > ( SupplyAnimalPtr(pob_Female,j) );
      if ( pf->GetAge() > 244 ) oldbirds++;
    }
    nochicks += ( sz - oldbirds );
    m_Ourkfactors->m_ThisYear->setNoOldFemales( oldbirds );
    oldbirds = 0;
    sz = (unsigned) SupplyListSize(pob_Male);
    for ( unsigned j = 0; j < sz; j++ )
    {
      pf = dynamic_cast < Partridge_Base * > ( SupplyAnimalPtr(pob_Male,j) );
      if ( pf->GetAge() > 244 ) oldbirds++;
    }
    m_Ourkfactors->m_ThisYear->setNoOldMales( oldbirds );
    nochicks += ( sz - oldbirds );
    m_Ourkfactors->m_ThisYear->setNoChicksSept( nochicks );
    m_Ourkfactors->m_ThisYear->SetBroodGeoMean( g_covey_manager->BroodGeoMean() );
    // Here we have a problem that not all chicks have reached 6 weeks - so add
    // the little ones now
    m_Ourkfactors->m_ThisYear->AddToChicks6wks( (int) SupplyListSize(pob_Chick) );
    // Do those calculations we can now
    m_Ourkfactors->calcreal_1();
  }
  else if ( today == November )
  { // after shoot is what is important
    m_Ourkfactors->calcreal_2();
    m_Ourkfactors->calcDickPottsk();
  }
  else if ( today == December+14 ) {
	  // Mid December counts of males and females
	  m_Ourkfactors->m_ThisYear->setNoFemalesDec((int) SupplyListSize(pob_Female));
	  m_Ourkfactors->m_ThisYear->setNoMalesDec((int) SupplyListSize(pob_Male));
  }
  g_covey_manager->Tick();
  //DoSanityCheck();
  //MaleImmigration(); // unused at present
  TestShouldFlock();
}

//---------------------------------------------------------------------------

double Partridge_Population_Manager::GetNestingCoverDensity( int a_x, int a_y )
{
/*
  x /= 10;
  y /= 10;
  return m_nestingcovermap->qualmap[x] [y];
*/
	return m_nestingcovermap->GetQual(a_x,a_y);
}

//---------------------------------------------------------------------------

double Partridge_Population_Manager::GetTerrQual( int a_x, int a_y )
{
/*
  x /= 10;
  y /= 10;
  return m_territoryqualmap->qualmap[x] [y];
*/
  return m_territoryqualmap->GetQual(a_x,a_y);
}

//---------------------------------------------------------------------------

void Partridge_Population_Manager::CreateNestingCoverDensityMap()
{
  // Doing this by brute force takes forever, so this is done by creating a
  // temporary map of covers first
  //
  int lx = m_TheLandscape->SupplySimAreaWidth();
  int ly = m_TheLandscape->SupplySimAreaHeight();
  CoverTempMap * tm;
  tm = new CoverTempMap(lx, ly);
  tm->ZeroQual();
  m_territoryqualmap->ZeroQual();
  double area;
  TTypesOfLandscapeElement tole;
  for ( int i = 0; i < lx; i++ )
  {
    for ( int j = 0; j < ly; j++ )
    {
      tole = m_TheLandscape->SupplyElementType( i, j );
      // Add the value of this cell to the qualmap
      m_territoryqualmap->AddQual(i,j,EvalHabitatQual( tole, m_TheLandscape->SupplyPolyRef( i, j ) ));
      switch ( tole )
      {
        //        case tole_Hedges:
        //        case tole_FieldBoundary:
        case tole_HedgeBank:
          // No good if it is sub-type 0
          if ( m_TheLandscape->SupplyElementSubType( i, j ) > 0 )
		  {
				  tm->AddQual(i,j,1);
		  }
        break;
        default:
        break;
      }
    }
  }
  // Now we can do the real job
  // Create a grid of 10x10 squares (default for CoverTempMap)
  // loop through the grid
  for ( int i = 0; i < m_maxx; i++ )
  {
    for ( int j = 0; j < m_maxy; j++ )
    {
      // Needs the values for the 1km square we are in. (1000/8 = 125, so need to look at 125*125 squares)
      area = 0;
      for ( int ii = -50; ii < 50; ii++ )
      {
        for ( int jj = -50; jj < 50; jj++ )
          area += tm->GetQualIndexed(( m_maxx + i + ii ) % m_maxx,( m_maxy + j + jj ) % m_maxy);
      }
      // Assume the mean width of useful habitat is 2.44m and per km (excl. trees)
      m_nestingcovermap->SetQualIndexed(i,j,area/3132);
    }
  }
  delete tm;
}

//---------------------------------------------------------------------------

double Partridge_Population_Manager::EvalHabitatQual( TTypesOfLandscapeElement a_cet, int a_poly )
{
  
    return partridge_tole_habitat_eval2(m_TheLandscape, a_poly);

}

//---------------------------------------------------------------------------
int Partridge_Population_Manager::HabitatEvalPolyField( int a_field )
{
  
	return partridge_tov_habitat_eval_field2(m_TheLandscape, a_field);
  
}

//---------------------------------------------------------------------------
/*
void Partridge_Population_Manager::UpdateNestingCoverMap()
{
  int lx = m_TheLandscape->SupplySimAreaHeight();
  int ly = m_TheLandscape->SupplySimAreaHeight();
  for ( int i = 0; i < lx / 10; i++ )
  {
    for ( int j = 0; j < ly / 10; j++ )
    {
      m_territoryqualmap->qualmap[i] [j] = 0;
    }
  }
  TTypesOfLandscapeElement tole;
  int oldref=-9999;
  double q=0;
  for ( int i = 0; i < lx; i++ )
  {
    for ( int j = 0; j < ly; j++ )
    {
		int nref = m_TheLandscape->SupplyPolyRef( i, j );
		if (nref==oldref) {
			 m_territoryqualmap->qualmap[i / 10] [j / 10] += q;
		}
		else {
			tole = m_TheLandscape->SupplyElementType( i, j );
			// Add the value of this cell to the qualmap
			q = EvalHabitatQual( tole, m_TheLandscape->SupplyPolyRef( i, j ) ); // Uses the covey function in Partridge_Tuning.cpp via m_OurSpecialCovey;
			m_territoryqualmap->qualmap[i / 10] [j / 10] += q;
			oldref=nref;
		}
    }
  }
}
*/
void Partridge_Population_Manager::UpdateNestingCoverMap()
{
  int lx = m_TheLandscape->SupplySimAreaWidth();
  int ly = m_TheLandscape->SupplySimAreaHeight();
  m_territoryqualmap->ZeroQual();
  TTypesOfLandscapeElement tole;
  int oldref=-9999;
  double q=0;
  for ( int i = 0; i < lx; i++ )
  {
    for ( int j = 0; j < ly; j++ )
    {
		int nref = m_TheLandscape->SupplyPolyRef( i, j );
		if (nref==oldref) {
			 m_territoryqualmap->AddQual(i,j,q);
		}
		else {
			tole = m_TheLandscape->SupplyElementType( i, j );
			// Add the value of this cell to the qualmap
			q = EvalHabitatQual( tole, m_TheLandscape->SupplyPolyRef( i, j ) ); // Uses the covey function in Partridge_Tuning.cpp via m_OurSpecialCovey;
			m_territoryqualmap->AddQual(i,j,q);
			oldref=nref;
		}
    }
  }
}

//---------------------------------------------------------------------------

/**
Implements a global mortality probability per bird on a single day of the year. Only adults and Chick2 objects are affected.\n
*/
void Partridge_Population_Manager::Hunting( int p_chance )
{
  unsigned size2 = (unsigned) SupplyListSize(pob_Female);
  for ( unsigned j = 0; j < size2; j++ )
  {
    if ( random( 10000 ) < p_chance )
    {
		if (SupplyAnimalPtr(pob_Female,j)->GetCurrentStateNo() != -1) {
            SupplyAnimalPtr(pob_Female,j)->Dying();
		  m_Ourkfactors->m_ThisYear->incNoShotBirds();
		  m_Ourkfactors->m_ThisYear->incNoShotFemales();
		}
    }
  }
  size2 = (unsigned) SupplyListSize(pob_Male);
  for ( unsigned j = 0; j < size2; j++ )
  {
    if ( random( 10000 ) < p_chance )
    {
		if (SupplyAnimalPtr(pob_Male,j)->GetCurrentStateNo() != -1) {
            SupplyAnimalPtr(pob_Male,j)->Dying();
			m_Ourkfactors->m_ThisYear->incNoShotBirds();
		}
	}
  }
  size2 = (unsigned) SupplyListSize(pob_Chick2);
  for ( unsigned j = 0; j < size2; j++ )
  { if (size2>0) {
	      if ( random( 10000 ) < p_chance )
    {
		if (SupplyAnimalPtr(pob_Chick2,j)->GetCurrentStateNo() != -1) {
              SupplyAnimalPtr(pob_Chick2,j)->Dying();
			  m_Ourkfactors->m_ThisYear->incNoShotBirds();
		}
    }
  }
  }
}


/**
The hunting pressure is applied in a grid of variable size and only when densities in the grid are above a specified threshold.\n
*/
void Partridge_Population_Manager::HuntingGrid( int p_chance )
{
	// First creates a density map of e.g. 1km squares
	unsigned GridSize=cfg_ParHuntingGridSize.value();
	unsigned Grids = SimW/GridSize;
	unsigned* densities = new unsigned[10000]; // This will allow up to 100x100 squares
	for (unsigned d=0; d<10000; d++) densities[d]=0;
	for (int pob=(int)pob_Chick2; pob<=(int)pob_Female; pob++) {
		unsigned size2 = (unsigned) SupplyListSize(pob);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (SupplyAnimalPtr(pob,j)->GetCurrentStateNo() != -1) {
                int x{0},y{0};
                SupplyLocXY(pob,j,x,y);

				unsigned sqx=x/GridSize;
				unsigned sqy=y/GridSize;
				densities[(sqy*Grids)+sqx]++;
			}
		}
	}
	// Now go through and apply hunting
	unsigned ParHuntingThreshold=cfg_ParHuntingThreshold.value();
	for (int pob=(int)pob_Chick2; pob<=(int)pob_Female; pob++) {
		unsigned size2 = (unsigned) SupplyListSize(pob);
		for ( unsigned j = 0; j < size2; j++ ) {
			if ( random( 10000 ) < p_chance )    {
				if (SupplyAnimalPtr(pob,j)->GetCurrentStateNo() != -1) {
                    int x{0},y{0};
                    SupplyLocXY(pob,j,x,y);

					unsigned sqx=x/GridSize;
					unsigned sqy=y/GridSize;
					if (densities[(sqy*Grids)+sqx]>ParHuntingThreshold) {
						if ( random( 10000 ) < p_chance ) {
                                SupplyAnimalPtr(pob,j)->Dying();
								m_Ourkfactors->m_ThisYear->incNoShotBirds();
						}
					}
				}
			}
		}
	}
	delete[] densities;
}

/**
*	Differential hunting inside a specific area designated as beetlebank addition area. 
*	Takes a fixed proportion of the population (input variable for inside and outside), in the middle of the hunting season
*/
void Partridge_Population_Manager::HuntingDifferentiatedBeetleBankArea( int a_pct ) 
{
	int tx1 = cfg_BeetleBankMinX.value();
	int tx2 = cfg_BeetleBankMaxX.value();
	int ty1 = cfg_BeetleBankMinY.value();
	int ty2 = cfg_BeetleBankMaxY.value();
	double huntpercent_out, huntpercent_in;
	if (!cfg_BeetleBankInvert.value())  
	{
		huntpercent_out = a_pct;
		huntpercent_in =cfg_ParHuntingBeetleBankArea.value();
	}
	else
	{
		huntpercent_in = a_pct;
		huntpercent_out =cfg_ParHuntingBeetleBankArea.value();
	}
	for (int pob=(int)pob_Chick2; pob<=(int)pob_Female; pob++) 
	{
		unsigned size2 = (unsigned) SupplyListSize(pob);
		for ( unsigned j = 0; j < size2; j++ ) 
		{
			if (SupplyAnimalPtr(pob,j)->GetCurrentStateNo() != -1)
			{
                int x{0},y{0};
                SupplyLocXY(pob,j,x,y);

				if ((x >= tx1) && (y >= ty1) && (x <= tx2) && (y <= ty2))
				{
						if ( random( 10000 ) < huntpercent_in ) 
						{
                                SupplyAnimalPtr(pob,j)->Dying();
								m_Ourkfactors->m_ThisYear->incNoShotBirds();
						}
				}
				else
						if ( random( 10000 ) < huntpercent_out ) 
						{
                                SupplyAnimalPtr(pob,j)->Dying();
								m_Ourkfactors->m_ThisYear->incNoShotBirds();
						}
			}
		}
	}
}


/**
A debug function.\n
*/
void Partridge_Population_Manager::DoSanityCheck()
{
  int today = m_TheLandscape->SupplyDayInYear();
  //if ( today == 60 )
  {
    for ( unsigned j = 0; j < SupplyListSize(pob_Covey); j++ )
    {
      dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey,j) )->SanityCheck();
    }
  }
  if (( today < 61 ) || (today>270))
  {
    for ( unsigned j = 0; j < SupplyListSize(pob_Covey); j++ )
    {
      dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey,j) )->SanityCheck4();
    }
  }
}

/**
A debug function.\n
*/
void Partridge_Population_Manager::DoMaleSanityCheck()
{
  // THIS IS A DEBUG FUNCTION
  int counter = 0;
  for ( unsigned j = 0; j < SupplyListSize(pob_Male); j++ )
  {
    Partridge_Male * pm = dynamic_cast < Partridge_Male * > ( SupplyAnimalPtr(pob_Male,j) );
    if ( pm->WhatState() != pars_Destroy )
    {
      int res = pm->AmIaMember();
      if ( res == 1 ) bad_guys[counter++] = pm;
      if ( res == 2 )
      {
        m_TheLandscape->Warn( "Partridge_Pop_Man::DoMaleSanityCheck(): ""Someone is not a member of their covey", "" );
        exit( 0 );
      }
    }
  }
}

//---------------------------------------------------------------------------


void Partridge_Population_Manager::DoBefore()
{
#ifdef __PARDEBUG4
  DoSanityCheck();
#endif
}

//---------------------------------------------------------------------------


void Partridge_Population_Manager::DoAfter()
{
  int today = m_TheLandscape->SupplyDayInYear();
  // Shooting after cfg_ParShootStartYear years
  if (cfg_ParShootStartYear.value() <= m_TheLandscape->SupplyYearNumber()) {
	if ( today == September+4 )
    {
	  // 5th September is shooting day
	  int pct= random(cfg_ParShotMax.value()-cfg_ParShotMin.value());
	  pct+=cfg_ParShotMin.value();
      if (cfg_ParHuntingType.value()==1) HuntingGrid( pct );
		else if (cfg_ParHuntingType.value()==1) HuntingDifferentiatedBeetleBankArea( pct );
			else Hunting( pct );
    }
  }
}

//---------------------------------------------------------------------------

void Partridge_Population_Manager::DoLast()
{
	if (m_TheLandscape->SupplyDayInYear() == cfg_flockdata_date.value())
	{
		// Calculate and write output to flock data file
		int si = (int) SupplyListSize(pob_Covey);
		int size, chi;
		for (int i = 0; i < si; i++)
		{
			// Generate a date to dissolve
			size = (int) dynamic_cast <Partridge_Covey*> (SupplyAnimalPtr(pob_Covey,i))->GetCoveySize();
			chi = dynamic_cast <Partridge_Covey*> (SupplyAnimalPtr(pob_Covey,i))->GetOurChicks();
			WriteParFlocks(m_TheLandscape->SupplyGlobalDate(), size, chi);
		}
}
	// Unpaired Males
	if (m_TheLandscape->SupplyDayInYear() == cfg_unpaireddata_date.value())
	{
		// Calculate and write output to flock data file
		int si = (int) SupplyListSize(pob_Male);
		int upm = 0;
		for (int i = 0; i < si; i++)
		{
			// Generate a date to dissolve
			if (!dynamic_cast <Partridge_Male*> (SupplyAnimalPtr(pob_Male,i))->GetMate()) upm++;
		}
		WriteParUnpairedMale(m_TheLandscape->SupplyGlobalDate(), upm);
	}
	// No Breeding Pairs
	if (m_TheLandscape->SupplyDayInYear() == cfg_breedingpairs_date.value())
	{
		// Calculate and write output to flock data file
		int si = (int) SupplyListSize(pob_Female);
		int nobs = 0;
		for (int i = 0; i < si; i++)
		{
			// Generate a date to dissolve
			if (dynamic_cast <Partridge_Female*> (SupplyAnimalPtr(pob_Female,i))->GetMate()) nobs++;
		}
		WriteParNoB(m_TheLandscape->SupplyGlobalDate(), nobs);
	}
	if (m_TheLandscape->SupplyDayInYear() == 364)
	{
		WriteNoHatchedPerYear(m_TheLandscape->SupplyGlobalDate(), m_HatchSuccess);
		m_HatchSuccess = 0;
	}
#ifdef __PARDEBUG4
  DoSanityCheck();
#endif
}
//---------------------------------------------------------------------------

/**
Rather than calculate it every time we need it, this method fills in the values required for each age in days.\n
The rain scaling factor decreases the energy needed if there is no rain which is compensated for by increased demands when there is rain -
overall we maintain our fit to Damerham data.\n
*/
void Partridge_Population_Manager::FillInFoodArray()
{
  double mult;
  for ( int i = 0; i < 200; i++ )
  {
    // using the raw data will provide the result in Kcal per day
    // These then need to be scaled to the expectation of extraction
    // by multiplication by the extraction rate
    if ( i > cfg_par_mature_threshold.value() ) mult = cfg_par_mature_threshold.value();
    else
      mult = i;
    //    SetFoodNeed(i,(double) cfg_par_energyslope_perday.value()*mult);
#ifdef __RAIN_HURTS
	// NB 0.8 is just a guess - we would need a special study on this to firm up this factor
	g_FoodNeed[i] = ( double )cfg_par_energyslope_perday.value() * mult * cfg_rainscaling.value();
#else
	g_FoodNeed[i] = ( double )cfg_par_energyslope_perday.value() * mult;
#endif
  }
  int half_grown = cfg_par_mature_threshold.value() / 2;
  for ( int i = 0; i < 200; i++ )
  {
    if ( i > half_grown ) mult = half_grown;
    else
      mult = i;
    g_MaxWalk[i] = ( int )( 50 + ( ( ( 500 - 50 ) / half_grown ) * mult ) );
  }
}
//---------------------------------------------------------------------------
/**
Unused at present.
*/
bool Partridge_Population_Manager::CoveyDissolveWeather()
{
  /* // The aim here is to get covey dissolve after 31st Jan, when we have a 5 day
  // period with temp above 5 (mean) and rainfall <2mm long int d1 = m_TheLandscape->SupplyGlobalDate();
  for ( int i = 0; i < 5; i++ ) { if ( m_TheLandscape->SupplyTemp( d1 - i ) < 2.5 ) return false;
  //    if ( m_TheLandscape->SupplyRain( d1 - i ) >= 2.0 ) return false; } // No wet or cold days so all OK */
  return true;
}
//---------------------------------------------------------------------------

/**
Called every day. The aim here is to get covey dissolve after a spedified date, and reflocking after a later date.\n
*/
void Partridge_Population_Manager::TestShouldFlock()
{
  int today = m_TheLandscape->SupplyDayInYear();
  if ( ( m_ShouldFlock ) && ( today > cfg_par_start_dissolve.value() ) && ( today < 150 ) && ( CoveyDissolveWeather() ) )
  {
    m_ShouldFlock = false;
    // Now need to tell the coveys to pair the birds
    // and break up
    //
    int si = (int) SupplyListSize(pob_Covey);
    for ( int i = 0; i < si; i++ )
    {
      // Generate a date to dissolve
      // Just make sure it is not too late
      if ( today > 80 ) today = 80;
      dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey,i) )->OnDissolve( today );
    }
  }
  if ( today >= cfg_par_male_gives_up.value() ) m_ShouldFlock = true;
}

//---------------------------------------------------------------------------

/**  21/03/06 New information makes the males obtain visiting permission from another
  covey close to them. So that they are not alone. They can do this if
  the covey is not one formed from their parent covey. Movement from one
  covey to the next is not subject to alone mortality. \n
  \n
  17/02/06 It seems that our behaviour here was not quite right. We need females to stay
  in the covey until found by a male.\n

  So the sequence of events is:\n
\n
  1) Covery break up message received\n
  2) Pairing in the covey where possible\n
  3) Pairs leave and start repro\n
  4) Single males leave and start to look for females\n
  5) Females wait until a male comes along then they leave\n
  6) When the last females dies or is paired this process is ended.
*/
void Partridge_Population_Manager::DissolveCovey( Partridge_Covey * a_covey )
{
#ifdef __PAR_DEBUG2
  a_covey->SanityCheck();
  if ( !a_covey->AllFlocking2() )
  {
    m_TheLandscape->Warn( "Partridge_Pop_Man::DissolveCovey(): someone not flocking", "" );
    exit( 1 );
  }
  if ( !a_covey->ArePaired() )
  {
    m_TheLandscape->Warn( "Partridge_Pop_Man::DissolveCovey(): someone still paired", "" );
    exit( 1 );
  }
#endif
  Partridge_Female * pf;
  Partridge_Male * pm;
  Partridge_Base * pb;
  int remaining = a_covey->GetCoveySize();
  do
  {
    // First loop through the females and see if they have old mates in the covey
    pf = a_covey->GetUnpairedFemale();
    if ( pf ) // There is a female and she has an oldmate in the covey
    {
      pm = pf->GetOldMate();
      // Take them both out of the covey
      remaining = a_covey->RemoveMember( pm );
	  pm->SetUncleStatus( false );
      //remaining = a_covey->RemoveMember( pf );
	  if (pf->GetUncleStatus()) {
		  pf->SetUncleStatus( false );
	  }
      // The Male must now make his own covey
      pm->MakeCovey(); // this will update his m_covey
      m_comms_data->m_female = pf;
      m_comms_data->m_male = pm;
      m_comms_data->m_covey = pm->GetCovey(); // synchronise coveys
      m_messagecentre.PassMessage( m_comms_data, pcomm_Mating );
      //remaining--; // The female gets removed from the covey by the above line
#ifdef __PAR_DEBUG2
			if ( pm->GetObjectType() != pob_Male )	{
				m_TheLandscape->Warn( "Partridge_Pop_Man::DissolveCovey(): pm not male", "" );
			    exit( 1 );
			}
			if ( pf->GetObjectType() != pob_Female )	{
				m_TheLandscape->Warn( "Partridge_Pop_Man::DissolveCovey(): pf not female", "" );
			    exit( 1 );
			}

#endif
      m_messagecentre.PassMessage( m_comms_data, pcomm_MatingM );
	  remaining = a_covey->GetCoveySize();
    }
  }
  while ( ( pf ) && ( remaining > 0 ) );
	// Send single males off
	pm=NULL;
	do {
		pm=a_covey->GetMaleInCovey();
		// The Male must now make his own covey
		if (pm!=NULL) {
			pm->MakeCovey(); // this will update his m_covey
			a_covey->RemoveMember(pm);
			remaining = a_covey->GetCoveySize();
			pm->StartBreedingBehaviour();
		}
	} while ( ( pm!=NULL ) && ( remaining > 0 ) && (a_covey->GetCurrentStateNo() > -1));

	if ( remaining > 0 )
	{
	  // Should now only be females left
		pf = NULL;
	    // At this point remaining has the remaining number of birds
		for ( int bird = 0; bird < remaining; bird++ )
		{
			pb = a_covey->GetMember( bird );
			pb->SetUncleStatus(false);
#ifdef __PAR_DEBUG2
			if ( pb->GetObjectType() == pob_Male )	{
				m_TheLandscape->Warn( "Partridge_Pop_Man::DissolveCovey(): Male left in covey on dissolve", "" );
			    exit( 1 );
			}
#endif
			pf = dynamic_cast < Partridge_Female * > ( pb );
			m_comms_data->m_female = pf;
			m_messagecentre.PassMessage( m_comms_data, pcomm_WaitForMale ); // tells her to wait here for a male
		}
	}
#ifdef __PAR_DEBUG2
  a_covey->SanityCheck();
#endif
}

//---------------------------------------------------------------------------

bool Partridge_Population_Manager::FillCoveyNeigbourList( Partridge_Covey * a_covey, int a_distance, int a_x, int a_y )
{
  // This list loops through the coveys and checks whether they are within
  // a_distance. If so add them to the covey neighbour list
  //
  // Start by emptying the covey neighbour list
  a_covey->m_neighbourlist_size = 0;
  // Now loop
  int size2 = (int) SupplyListSize(pob_Covey);
  for ( int j = 0; j < size2; j++ )
  {
    int x{0},y{0};
    SupplyLocXY(pob_Covey,j, x, y);

    int dx = abs( x - a_x );
    int dy = abs( y - a_y );
    // OK OK, this is not the real distance, but it is a reasonable approximation (max x0.3 out)
    int dist = dx + dy;
    // There are only 25 spaces in the neighbour list so don't risk an overflow
    if ( ( a_covey->m_neighbourlist_size < 25 ) && ( dist <= a_distance ) &&
      (SupplyAnimalPtr(pob_Covey,j) != a_covey ) && (SupplyAnimalPtr(pob_Covey,j)->GetCurrentStateNo() != -1) )
    {
      Partridge_Covey * cov = dynamic_cast < Partridge_Covey * > ( SupplyAnimalPtr(pob_Covey,j) );
      a_covey->m_neighbourlist[a_covey->m_neighbourlist_size++] = cov;
    }
  }
  if ( a_covey->m_neighbourlist_size == 0 ) return false;
  return true;
}

//---------------------------------------------------------------------------

// Special Partridge Output Section

//ParAdMort;    // Adult mortality record
//ParJuvMort;   // Juvenile mortaltiy record
//ParNoB;       // No of breeding Pairs
//ParClutches;  // For each clutch, clutch no. & size
//ParFlocks;    // Covey size 1st oct, + no. chicks
//ParUnpairedMale;  // No unparied males 1st Jun
//NoHatchedPerYear; // No. successful hatches per year

bool Partridge_Population_Manager::OpenParOutputFiles()
{
  ParAdMort = fopen("ParAdMort.txt", "w" );
  if ( !ParAdMort ) return false;
  ParJuvMort = fopen("ParJuvMort.txt", "w" );
  if ( !ParJuvMort ) return false;
  ParNoB = fopen("ParNoB.txt", "w" );
  if ( !ParNoB ) return false;
  ParClutches = fopen("ParClutches.txt", "w" );
  if ( !ParClutches ) return false;
  ParFlocks = fopen("ParFlocks.txt", "w" );
  if ( !ParFlocks ) return false;
  ParUnpairedMale = fopen("ParUnpairedMale.txt", "w" );
  if ( !ParUnpairedMale ) return false;
  NoHatchedPerYear = fopen("NoHatchedPerYear.txt", "w" );
  if ( !NoHatchedPerYear ) return false;
  return true;
}

bool Partridge_Population_Manager::CloseParOutputFiles()
{
  /*  FILE * fff = fopen( "MeanFoodPerday.txt", "w" );

  fprintf( fff, "%g\n", debug_totalfood / debug_totaldays ); fprintf( fff, "Starved Chicks %d\n", m_Starved );
  fprintf( fff, "Eaten Chicks %d\n", m_Eaten ); fprintf( fff, "Matured Chicks %d\n", m_Matured );
  fprintf( fff, "Eaten Clutches %d\n", m_EatenE ); fprintf( fff, "Matured Clutches %d\n", m_MaturedE ); fclose( fff ); */
  fclose( ParAdMort );
  fclose( ParJuvMort );
  fclose( ParNoB );
  fclose( ParClutches );
  fclose( ParFlocks );
  fclose( ParUnpairedMale );
  fclose( NoHatchedPerYear );
  FILE * idb = fopen("k_facInfoBase.txt", "a" );
  if (!idb) {
      g_msg->Warn( "k_factors::DumpInfoDatabase","Cannot open k_facInfoBase.txt" );
      exit( 1 );
  }
  fprintf( idb, "0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n");
  fclose( idb );
#ifdef __NESTPOSITIONS_OUT
	fclose(NestPositions);
#endif
  return true;
}

void Partridge_Population_Manager::WriteParAdMort( int a_min, int a_age, int a_sex, int a_cause )
{
  fprintf( ParAdMort, "%d %d %d %d\n", a_min, a_age, a_sex, a_cause );
  //  fflush(ParAdMort);
}

void Partridge_Population_Manager::WriteParJuvMort( int a_yr, int a_min, int a_age, int a_cause )
{
  fprintf( ParJuvMort, "%d\t%d\t%d\t%d\n", a_yr, a_min, a_age, a_cause );
  //  fflush(ParJuvMort);
}

void Partridge_Population_Manager::WriteParNoB( int a_min, int a_nob )
{
  fprintf( ParNoB, "%d %d\n", a_min, a_nob );
  fflush( ParNoB );
}

void Partridge_Population_Manager::WriteParUnpairedMale( int a_min, int a_num )
{
  fprintf( ParUnpairedMale, "%d %d\n", a_min, a_num );
  fflush( ParUnpairedMale );
}

void Partridge_Population_Manager::WriteParClutches( int a_min, int a_num1, int a_num2 )
{
  fprintf( ParClutches, "%d\t%d\t%d\n", a_min, a_num1, a_num2 );
  //  fflush(ParClutches);
}

void Partridge_Population_Manager::WriteParFlocks( int a_min, int a_size, int a_young )
{
  fprintf( ParFlocks, "%d %d %d\n", a_min, a_size, a_young );
  fflush( ParFlocks );
}

/**
*/
void Partridge_Population_Manager::WriteNoHatchedPerYear( int a_min, int a_num )
{
  fprintf( NoHatchedPerYear, "%d %d\n", a_min, a_num );
  fflush( NoHatchedPerYear );
}




void Partridge_Population_Manager::TheAOROutputProbe() {
	m_AOR_Probe->DoProbe(pob_Female);
}
//-----------------------------------------------------------------------------

/**
An output designed to be used as an input to calculating Ripley's k in R.\n
*/
void Partridge_Population_Manager::TheRipleysOutputProbe( FILE* a_prb ) {
  Partridge_Female* FS;
  unsigned totalF=(unsigned) SupplyListSize(pob_Female);
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
	  FS=dynamic_cast<Partridge_Female*>(SupplyAnimalPtr(pob_Female,j));
	  x=FS->Supply_m_Location_x();
	  y=FS->Supply_m_Location_y();
	  fprintf(a_prb,"%d\t%d\n", x,y);
  }
  fflush(a_prb);
}
//-------------------------------------------------------------------------------------------------

void Partridge_Population_Manager::Catastrophe( void ) {
	/** This version simply alters populations on 1st January - it is very dangerous to
	add individuals in many of the models so beware!!!!\n
    4th October 2007 - Implemented only to work with ADULT Males and Females
	*/
	//
	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=1) return;
	// First do we have the right year?
	int year = m_TheLandscape->SupplyYearNumber()- cfg_CatastropheEventStartYear.value();
	;
	if (year%cfg_pm_eventfrequency.value()!=0) return;

	Partridge_Male* PM = NULL; // assignment to get rid of warning
	Partridge_Female* PF = NULL;
	Partridge_Base* PB = NULL;
	// Now if the % decrease is higher or lower than 100 we need to do different things
	int esize=cfg_pm_eventsize.value();
	if (esize<100) {
		unsigned size2 = (unsigned) SupplyListSize(pob_Male);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (random(100) > esize) {
                   PM = dynamic_cast < Partridge_Male * > ( SupplyAnimalPtr(pob_Male,j));
					PM->SetState(pars_MDying); // Kill it
			}
		}
		size2 = (unsigned) SupplyListSize(pob_Female);
		for ( unsigned j = 0; j < size2; j++ ) {
			if (random(100) > esize) {
                    PF = dynamic_cast < Partridge_Female * > ( SupplyAnimalPtr(pob_Female,j) );
					PF->SetState(pars_FDying); // Kill it
			}
		}
	} else if (esize>100) {
		// This is a tricky thing to do because we need to duplicate birds, but dare not mess
		// mate pointers etc up.
		// This also requires a copy method in the target birds
		// esize also needs translating  120 = 20%, 200 = 100%
		if (esize<200) {
			esize-=100;
			unsigned size2;
			size2 = (unsigned) SupplyListSize(pob_Male);
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) < esize) {
                        PB = dynamic_cast < Partridge_Base * > ( SupplyAnimalPtr(pob_Male,j));
					    PB->CopyMyself(pob_Male); // Duplicate it
				}
			}
			size2 = (unsigned) SupplyListSize(pob_Female);
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) < esize) {
                        PB = dynamic_cast < Partridge_Base * > ( SupplyAnimalPtr( pob_Female,j) );
					    PB->CopyMyself(pob_Female); // Duplicate it
				}
			}
		} else {
			esize-=100;
			esize/=100; // this will throw away fractional parts so will get 1, 2, 3  from 200, 350 400
			unsigned size2;
			size2 = (unsigned) SupplyListSize(pob_Male);
			for ( unsigned j = 0; j < size2; j++ ) {
					for ( int e=0; e<esize; e++) {
                        PB = dynamic_cast < Partridge_Base * > ( SupplyAnimalPtr(pob_Male,j));
					    PB->CopyMyself(pob_Male); // Duplicate it
				}
			}
			size2 = (unsigned) SupplyListSize(pob_Female);
			for ( unsigned j = 0; j < size2; j++ ) {
					for ( int e=0; e<esize; e++) {
                        PB = dynamic_cast < Partridge_Base * > ( SupplyAnimalPtr(pob_Female,j));
					    PB->CopyMyself(pob_Female); // Duplicate it
				}
			}
		}
	}
	else return; // No change so do nothing
}
//-----------------------------------------------------------------------------

/**
Unused at present
*/
void Partridge_Population_Manager::MaleImmigration( void ) {
	//
	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=212) return;

	// Now if the % decrease is higher or lower than 100 we need to do different things
	int esize=cfg_MaleImmigration.value();
	if (esize<1) return; // why waste time if there is nothing to do
	Partridge_Base* PB = NULL;
	// This is a tricky thing to do because we need to duplicate birds, but dare not mess
	// mate pointers etc up.
	// This also requires a copy method in the target birds
	// esize also needs translating  120 = 20%, 200 = 100%
	unsigned size2;
	size2 = (unsigned) SupplyListSize(pob_Male);
	for ( unsigned j = 0; j < size2; j++ ) {
		if (random(100) < esize) {
			PB = dynamic_cast < Partridge_Base * > ( SupplyAnimalPtr(pob_Male, j) );
			PB->CopyMyself(pob_Male); // Duplicate it
		}
	}
}
//-----------------------------------------------------------------------------
