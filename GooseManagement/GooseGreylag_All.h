/* 
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#ifndef Goose_Greylag_Base_BaseH
#define Goose_Greylag_Base_BaseH

/**
\file
\brief
<B>GreylagGoose_All.h This is the header file for the greylag goose classes</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of 8th February 2013 \n
*/
//---------------------------------------------------------------------------

class Goose_Base;
class Landscape;
class Goose_Population_Manager;

/** \brief
* A class to describe the Greylag base
*/
class Goose_Greylag_Base : public Goose_Base
{
public:
	/** \brief Goose_Greylag_Base constructor */
	Goose_Greylag_Base(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief Intitialise object */
	void Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief ReInit for object pool */
	void ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief Goose_Greylag_Base destructor */
	virtual ~Goose_Greylag_Base();
protected:
   /** \brief 
   Return to roost and assess whether to forage again that day 
   */
	TTypeOfGoose_BaseState st_ToRoost();
    /** \brief 
    Find the closest roost
    */
    virtual void ChangeRoost() { m_OurPopulationManager->FindClosestRoost(m_Location_x, m_Location_y, (unsigned) gs_Greylag); }
	/** \brief
	Pick a hop location point within a_dist meters - must be overridden by descendent classes
	*/
	virtual APoint ChooseHopLoc();

};

/** \brief
* A class to describe the Greylag family group
*/
class Goose_Greylag_FamilyGroup final: public Goose_Greylag_Base
{
public:
   /** \brief Goose_Greylag_FamilyGroup constructor */
	Goose_Greylag_FamilyGroup(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, int a_groupsize, APoint a_roost);
	/** \brief Intitialise object */
	void Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, int a_groupsize, APoint a_roost);
	/** \brief ReInit for object pool */
	void ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, int a_groupsize, APoint a_roost);
	/** \brief Goose_Greylag_FamilyGroup destructor */
	~Goose_Greylag_FamilyGroup() final;
   /** \brief The Goose_Greylag_FamilyGroup Step*/
   virtual void Step( );
  /** \brief The Goose_Greylag_FamilyGroup EndStep*/
    virtual void EndStep() { ; }
  /** \brief The FamilyGroup KillThis must be overridden for families */
    virtual void KillThis();
};

/** \brief
* A class to describe the Greylag non-breeder
*/
class Goose_Greylag_NonBreeder final: public Goose_Greylag_Base
{
public:
   /** \brief Goose_Greylag_NonBreeder constructor */
	Goose_Greylag_NonBreeder(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief Intitialise object */
	void Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief ReInit for object pool */
	void ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
   /** \brief Goose_Greylag_NonBreeder destructor */
	~Goose_Greylag_NonBreeder() final;
   /** \brief The Goose_Greylag_NonBreeder Step*/
   virtual void Step( );
};
#endif
