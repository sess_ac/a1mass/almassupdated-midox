/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Goose_Population_Manager.cpp
\brief <B>The source code for the goose population manager and associated classes</B>
*/
/**  \file Goose_Population_Manager.cpp
Version of  8th February 2013 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <cstring>
#include <cmath>
#include <iostream>
//#include <fstream>
#include <vector>
#include <list>
//#include "../BatchALMaSS/ALMaSS_Random.h"
//#include <random>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../People/Hunters/Hunters_all.h"
#include "GoosePinkFooted_All.h"
#include "GooseGreylag_All.h"
#include "GooseBarnacle_All.h"

#include <boost/io/ios_state.hpp>


/** \brief This pointer provides access the to landscape module */
extern Landscape* g_land;
/** \brief This variable provides access the to the internal ALMaSS math functions */
extern ALMaSS_MathFuncs g_AlmassMathFuncs;
/** \brief This variable controls how long the simulation will run for */
extern int g_torun;
/** \brief This config variable controls whether we should make AOR output */
extern CfgBool cfg_AOROutput_used;
extern CfgBool cfg_WriteCurve;

/** \brief Scales the growth of vegetation - max value */
extern CfgFloat cfg_PermanentVegGrowthMaxScaler;
/** \brief Scales the growth of vegetation - min value */
extern CfgFloat cfg_PermanentVegGrowthMinScaler;
/** \brief Config to control if the Ripley probe is used or not */
extern CfgBool cfg_ReallyBigOutput_used;
/** \brief Should git version info be printed to file and console?*/
extern CfgBool l_map_print_git_version_info;

//---------------------------------------------------------------------------------------






 //---------------------------------------------------------------------------

 /** \brief Daily inflow and outflow % for geese per month (Pinkfeet, Barnies, Greylag) */
 double inflowoutflowgeese[36] = {
	 /** 
	 * This works as a daily % of the current population, and just randomly removes objects and then adds the same number of objects of the same 
	 * type to a random roost - so population size will only be affected by chance variation in family sizes on object creation. 0.0033 gives 10% turnover per month
	 */
 0.0033,0.0033,0.0033,0.0033,0.0033,0.0033,0.0033,0.0033,0.0033,0.0033,0.0033,0.0033,
 0,0,0,0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,0,0,0
 };
 /*
probability_distribution father_pf_d("NORMAL", std::to_string(cfg_goose_pf_baseweight.value())+" "+std::to_string(cfg_goose_pf_baseweight_std.value()));
probability_distribution mother_pf_d("NORMAL", std::to_string(cfg_goose_pf_baseweight_fem.value())+" "+std::to_string(cfg_goose_pf_baseweight_fem_std.value()));
probability_distribution young_pf_d("NORMAL", std::to_string(cfg_goose_pf_baseweight_juv.value())+" "+std::to_string(cfg_goose_pf_baseweight_juv_std.value()));

probability_distribution father_bn_d("NORMAL", std::to_string(cfg_goose_bn_baseweight.value())+" "+std::to_string(cfg_goose_bn_baseweight_std.value()));
probability_distribution mother_bn_d("NORMAL", std::to_string(cfg_goose_bn_baseweight_fem.value())+" "+std::to_string(cfg_goose_bn_baseweight_fem_std.value()));
probability_distribution young_bn_d("NORMAL", std::to_string(cfg_goose_bn_baseweight_juv.value())+" "+std::to_string(cfg_goose_bn_baseweight_juv_std.value()));

probability_distribution father_gl_d("NORMAL", std::to_string(cfg_goose_gl_baseweight.value())+" "+std::to_string(cfg_goose_gl_baseweight_std.value()));
probability_distribution mother_gl_d("NORMAL", std::to_string(cfg_goose_gl_baseweight_fem.value())+" "+std::to_string(cfg_goose_gl_baseweight_fem_std.value()));
probability_distribution young_gl_d("NORMAL", std::to_string(cfg_goose_gl_baseweight_juv.value())+" "+std::to_string(cfg_goose_gl_baseweight_juv_std.value()));
*/





GooseConfigs::GooseConfigs() = default;

 GooseActiveForageLocation::GooseActiveForageLocation(GooseSpeciesType a_type, int a_number, int a_polyref,
													  int a_area, double a_graindensity, double a_maizedensity, double *a_grazing,
													  Goose_Population_Manager *p_NPM)
 {
	 m_HabitatType = tomis_foobar;
	 SetPolygonref(a_polyref);
	 SetArea(a_area);
	 SetGrainDensity(a_graindensity); // grain/m2
	 SetMaizeDensity(a_maizedensity); // kJ/m2
	 /**
	* The grain density is measured in number of grains. To go from grains to grams 
	* we use the weight of a grain being 0.04
	*/
	 m_grainKJ_total = a_area * a_graindensity * 17.67 * 0.04; /** 17.67 kJ/g dw grain & 0.04 grain/g */
	 m_maizeKJ_total = a_area * a_maizedensity;				   // Already in kJ/m2
	 for (int i = 0; i < gs_foobar; i++)
	 {
		 SetGrazing(i, a_grazing[i]);
	 }
	 for (int i = 0; i < gst_foobar; i++)
	 {
		 m_BirdsPresent[i] = 0;
		 m_MaxBirdsPresent[i] = 0;
	 }
	 UpdateKJ(); // Scales m_xxxxKJ_total to area. Used also when recalculating after consumption by geese.
	 AddGeese(a_type, a_number);
	 m_OurPopulationManager = p_NPM;
}
//---------------------------------------------------------------------------

void GooseActiveForageLocation::ClearBirds()
{
	for (int i = 0; i < gst_foobar; i++)
	{
		if (m_BirdsPresent[i] != 0)
		{
			g_land->Warn("GooseActiveForageLocation::ClearBirds() - still birds on field", "");
			std::exit(1);
		}
		m_MaxBirdsPresent[i] = 0;
	}
}
//---------------------------------------------------------------------------

int GooseActiveForageLocation::GetHuntables()
{
	int huntables = 0;
	if (m_OurPopulationManager->InPinkfootSeason()) {
		huntables += m_BirdsPresent[gst_PinkfootFamilyGroup] + m_BirdsPresent[gst_PinkfootNonBreeder];
	}
	if (m_OurPopulationManager->InGreylagSeason()) {
		huntables += m_BirdsPresent[gst_GreylagFamilyGroup] + m_BirdsPresent[gst_GreylagNonBreeder];
	}
	return huntables;
}
//---------------------------------------------------------------------------

double Goose_Population_Manager::GetDistToClosestRoost(int a_x, int a_y, unsigned a_type)
{
	int rx = m_roosts[a_type][0].m_x;
	int ry = m_roosts[a_type][0].m_y;
	int dist = g_AlmassMathFuncs.CalcDistPythagoras(rx, ry, a_x, a_y);
	for (unsigned i = 1; i<m_roosts[a_type].size(); i++)
	{
		rx = m_roosts[a_type][i].m_x;
		ry = m_roosts[a_type][i].m_y;
		int di = g_AlmassMathFuncs.CalcDistPythagoras(rx, ry, a_x, a_y);
		if (di < dist) {
			dist = di;
		}
	}
	return dist;
}


Goose_Population_Manager::~Goose_Population_Manager()
{
	/** Close files and delete pointers to them */
	m_GoosePopDataFile->close();
	delete m_GoosePopDataFile;
	m_GooseFieldForageDataFile->close();
	delete m_GooseFieldForageDataFile;
	m_GooseEnergeticsDataFile->close();
	delete m_GooseEnergeticsDataFile;
	m_GooseIndLocCountFile->close();
	delete m_GooseIndLocCountFile;
	m_GooseHabitatUseFile->close();
	delete m_GooseHabitatUseFile;
	m_GooseHabitatUseFieldObsFile->close();
	delete m_GooseHabitatUseFieldObsFile;
	m_GooseWeightStatsFile->close();
	delete m_GooseWeightStatsFile;
	/** Delete any memory constructs created by new */
	delete m_IntakeRateVSGrainDensity_PF;
	delete m_IntakeRateVSMaizeDensity_BN;
	delete m_ForageRateVSGooseDensity;
	delete m_variate_generator;
	if (cfg_AOROutput_used.value()) {
		delete m_AOR_Pinkfeet;
		delete m_AOR_Greylags;
		delete m_AOR_Barnacles;
	}
	m_GooseXYDumpFile->close();
	delete m_GooseXYDumpFile;
#ifdef __GITVERSION
	if (l_map_print_git_version_info.value()) {
		m_GooseGitVersionFile ->close();
		delete m_GooseGitVersionFile;
	}
#endif
}
//---------------------------------------------------------------------------

Goose_Population_Manager::Goose_Population_Manager(Landscape* L) : Population_Manager(L, 6)
{

    /** \brief The distance the alarm call of the goose is effective*/
    CfgInt cfg_goose_AlarmCallDistance("GOOSE_ALARM_CALL_DISTANCE", CFG_CUSTOM, 100, -1, 10000);

    // g_torun = cfg_goose_ModelExitDay.value(); // watchout GUI/Pybind etc not guranteed to use this
	// Initialise member variables
	m_thermalcosts[0] = 0.0;
	m_thermalcosts[1] = 0.0;
	m_thermalcosts[2] = 0.0;
	// Set start of day - which is at sunrise. 10 is added to m_daytime in DoFirst. Therfore we start out with -10.
    m_daytime = -10;
	// Load List of Animal Classes
	m_ListNames[0] = "Pinkfoot Family";
	m_ListNames[1] = "Pinkfoot Nonbreeder";
	m_ListNames[2] = "Barnacle Family";
	m_ListNames[3] = "Barnacle Nonbreeder";
	m_ListNames[4] = "Greylag Family";
	m_ListNames[5] = "Greylag Nonbreeder";
	m_ListNameLength = 6;
	m_population_type = TOP_Goose;
	m_SimulationName = "Goose";
	/** Set up before step action sorts
	This determines how we handle the arrays between steps
	*/
	BeforeStepActions[0]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	BeforeStepActions[1]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	BeforeStepActions[2]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	BeforeStepActions[3]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	BeforeStepActions[4]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	/**
	* There is a mandatory file for goose populations that describe the roost locations for each goose type in the landscape - GooseRoosts.txt
	* For each type of goose the x,y locations for roosts are listed
	*/
	ifstream ifile("GooseRoosts.txt");
	if (!ifile.is_open())
	{
		m_TheLandscape->Warn("Goose_Population_Manager::Goose_Population_Manager(Landscape* L) ","GooseRoosts.txt missing");
		exit(1);
	}
	int no_entries;
	ifile >> no_entries;
	roostlist r;
	for (unsigned i = 0; i < gs_foobar; i++) m_roosts.push_back(r);
	for (int i=0; i< no_entries; i++)
	{
		int type;
		APoint ap;
		ifile >> type >> ap.m_x >> ap.m_y;
		m_roosts[type].push_back(ap);
	}

	/**
	Read in a file with the distribution of the brood sizes as observed from the field.
	We only have data from Pinkfeet, so this distribution is recycled for the other species
	*/
	ifstream jfile( "PfYoungDist.txt" );
	if (!jfile.is_open()) {
		m_TheLandscape->Warn( "Goose_Population_Manager::Goose_Population_Manager(Landscape* L) ", "PfYoungDist.txt missing" );
		exit( 1 );
	}
    jfile >> no_entries;
	for (int i = 0; i< no_entries; i++) {
		int young;
		jfile >> young;
		m_youngdist.push_back( young );
	}
	// We dont start in the hunting season
	m_PinkfootSeason = false;
	m_GreylagSeason = false;
	// We make one goose base just to access its static members
	APoint pt;
	pt.m_x = 0;
	pt.m_y = 0;
	//Goose_Base gb(nullptr, nullptr,1,true, pt);
	Goose_Base::SetFlightCost(GooseCfg.cfg_goose_flightcost.value());
    Goose_Base::Set_mingooseforagerate(0.0, gs_Pinkfoot);
    Goose_Base::Set_mingooseforagerate(0.0, gs_Barnacle);
    Goose_Base::Set_mingooseforagerate(0.0, gs_Greylag);
	//gb.Set_Indivmingooseforagerate( 0.0 );
    Goose_Base::Set_GooseMaxAppetiteScaler(GooseCfg.cfg_goose_MaxAppetiteScaler.value());
    Goose_Base::Set_GooseMaxEnergyReserveProportion( GooseCfg.cfg_goose_MaxEnergyReserveProportion.value() );
	Goose_Base::Set_GooseKJtoFatConversion( 1.0/(GooseCfg.cfg_goose_EnergyContentOfFat.value() + GooseCfg.cfg_goose_MetabolicConversionCosts.value()));
	Goose_Base::Set_GooseFattoKJConversion(1.0 / (GooseCfg.cfg_goose_EnergyContentOfFat.value() - GooseCfg.cfg_goose_MetabolicConversionCosts.value()));
	Goose_Base::Set_GooseMinForageOpenness( GooseCfg.cfg_goose_MinForageOpenness.value() );
	Goose_Base::Set_GooseLeavingThreshold( GooseCfg.cfg_goose_LeavingThreshold.value() * 5.0); // 5 because we use a 5-day running average
	Goose_Base::Set_GooseForageDist(GooseCfg.cfg_goose_pf_ForageDist.value(), GooseCfg.cfg_goose_bn_ForageDist.value(), GooseCfg.cfg_goose_gl_ForageDist.value());
    Goose_Base::Set_GooseFieldForageDist(GooseCfg.cfg_goose_pf_FieldForageDist.value(), GooseCfg.cfg_goose_bn_FieldForageDist.value(), GooseCfg.cfg_goose_gl_FieldForageDist.value());

	Goose_Base::Set_GooseFollowingLikelyhood(GooseCfg.cfg_goose_bn_followinglikelyhood.value(), gst_BarnacleFamilyGroup);
	Goose_Base::Set_GooseFollowingLikelyhood(GooseCfg.cfg_goose_pf_followinglikelyhood.value(), gst_PinkfootFamilyGroup);
	Goose_Base::Set_GooseFollowingLikelyhood(GooseCfg.cfg_goose_gl_followinglikelyhood.value(), gst_GreylagFamilyGroup);
	// We use the value for families for NB also. Currently not possible to quantify a difference. 
	// At a later stage we might want to explore this more, so we keep the cfg for later.
	Goose_Base::Set_GooseFollowingLikelyhood(GooseCfg.cfg_goose_bn_followinglikelyhood.value(), gst_BarnacleNonBreeder);
	Goose_Base::Set_GooseFollowingLikelyhood(GooseCfg.cfg_goose_pf_followinglikelyhood.value(), gst_PinkfootNonBreeder);
	Goose_Base::Set_GooseFollowingLikelyhood(GooseCfg.cfg_goose_gl_followinglikelyhood.value(), gst_GreylagNonBreeder);

	//gb.Set_GooseLeavingRoost( true );
	/** Set teh alarm call distance */
	m_AlarmCallDistance=cfg_goose_AlarmCallDistance.value();
	/** Create curves for food intake rates and forage rates */
	m_ForageRateVSGooseDensity = new PettiforFeedingTimeCurveClass(GooseCfg.cfg_Petti1A.value(), GooseCfg.cfg_Petti1B.value(), GooseCfg.cfg_Petti1C.value(), GooseCfg.cfg_Petti1D.value(), GooseCfg.cfg_Petti1E.value(), GooseCfg.cfg_Petti1F.value(), GooseCfg.cfg_Petti1G.value());
	m_ForageRateVSGooseDensity_BN = new PettiforFeedingTimeCurveClass(GooseCfg.cfg_Petti_A_BN.value(), GooseCfg.cfg_Petti1B.value(), GooseCfg.cfg_Petti_C_BN.value(), GooseCfg.cfg_Petti1D.value(), GooseCfg.cfg_Petti1E.value(), GooseCfg.cfg_Petti1F.value(), GooseCfg.cfg_Petti_G_BN.value());
	m_IntakeRateVSGrainDensity_PF = new HollingsDiscCurveClass(GooseCfg.cfg_H1A.value(), GooseCfg.cfg_H1B.value(), GooseCfg.cfg_H1C.value(), GooseCfg.cfg_H1D.value(), GooseCfg.cfg_H1E.value(), GooseCfg.cfg_H1F.value());
	m_IntakeRateVSMaizeDensity_BN = new HollingsDiscCurveClass( GooseCfg.cfg_H2A.value(), GooseCfg.cfg_H2B.value(), GooseCfg.cfg_H2C.value(), GooseCfg.cfg_H2D.value(), GooseCfg.cfg_H2E.value(), GooseCfg.cfg_H2F.value() );
	if (cfg_WriteCurve.value()) {
		m_ForageRateVSGooseDensity->WriteDataFile( 10 );
		m_ForageRateVSGooseDensity_BN->WriteDataFile( 10 );
		m_IntakeRateVSGrainDensity_PF->WriteDataFile( 10 );
		m_IntakeRateVSMaizeDensity_BN->WriteDataFile( 10 );
	}
	// Set up a normal distribution which we can then draw from at random
	m_variate_generator = new probability_distribution("NORMAL", std::to_string(GooseCfg.cfg_goose_RoostLeaveDistMean.value()) + " " + std::to_string(GooseCfg.cfg_goose_RoostLeaveDistSD.value()) );  // mean and std
    int size_x = m_TheLandscape->SupplySimAreaWidth();
    int size_y = m_TheLandscape->SupplySimAreaHeight();

    m_AlarmedBirdsMap= make_unique<blitz::Array<bool, 2>>(size_x,size_y);
    (*m_AlarmedBirdsMap)= false;
	Goose_Population_Manager::Init();
}

//---------------------------------------------------------------------------

void Goose_Population_Manager::Init()
{
	/**
	*Finally opens any output files needed
	*/
	if (cfg_ReallyBigOutput_used.value()) {
		OpenTheReallyBigProbe();
	}
	else ReallyBigOutputPrb = nullptr;
	if (cfg_AOROutput_used.value()) {
		m_AOR_Pinkfeet = new AOR_Probe_Goose(this, m_TheLandscape, "AOR_pinkfoot.txt");
		m_AOR_Barnacles = new AOR_Probe_Goose(this, m_TheLandscape, "AOR_barnacle.txt");
		m_AOR_Greylags = new AOR_Probe_Goose(this, m_TheLandscape, "AOR_greylag.txt");
	}
	m_GooseXYDumpFile = new ofstream("GooseXYDump.txt", ios::out);
	std::vector<std::string> xy_headers;
	xy_headers = { "X", "Y", "poly_ref", "fl_x", "fl_y", "fl_poly" };
	WriteHeaders(m_GooseXYDumpFile, xy_headers);

	m_GoosePopDataFile = new ofstream("GoosePopulationData.txt", ios::out);
	std::vector<std::string> pd_headers;
	pd_headers = { "season", "day", "pf_families", "pf_non_breeders", "bn_families",
					"bn_non_breeders", "gl_families", "gl_non_breeders", "snow_depth" };
	WriteHeaders(m_GoosePopDataFile, pd_headers);

	m_GooseWeightStatsFile = new ofstream("GooseWeightStats.txt", ios::out);
	std::vector<std::string> ws_headers = { "season", "day", "day_in_year", "species", "mean_weight", "mean_weight_se", "n" , "mean_weight_sd"};
	WriteHeaders(m_GooseWeightStatsFile, ws_headers);

	m_GooseIndLocCountFile = new ofstream("GooseIndLocCountStats.txt", ios::out);
	std::vector<std::string> ilc_headers = { "season", "day", "day_in_year", "species",
											"n_forage_locs", "n_forage_locs_se", "n", "n_forage_locs_sd" };
	WriteHeaders(m_GooseIndLocCountFile, ilc_headers);

	m_GooseHabitatUseFile = new ofstream("GooseHabitatUseStats.txt", ios::out);
	std::vector<std::string> ghu_headers = { "season", "day", "day_in_year", "species" };
	for (int h = 0; h < tomis_foobar; h++)
	{
		string str1 = IntakeSourceToString((TTypeOfMaxIntakeSource)h);
		ghu_headers.push_back(str1);
	}
	ghu_headers.emplace_back("count");
	WriteHeaders(m_GooseHabitatUseFile, ghu_headers);
	ClearGooseHabitatUseStats();

	m_GooseHabitatUseFieldObsFile = new ofstream("GooseHabitatUseFieldObsStats.txt", ios::out);
	std::vector<std::string> ghufo_headers = { "season", "day", "day_in_year", "species" };
	for (int h = 0; h < tomis_foobar; h++)
	{
		string str1 = IntakeSourceToString((TTypeOfMaxIntakeSource)h);
		ghufo_headers.push_back(str1);
	}
	ghufo_headers.emplace_back("count");
	WriteHeaders(m_GooseHabitatUseFieldObsFile, ghufo_headers);
	ClearGooseHabitatUseFieldObsStats();

	m_GooseLeaveReasonStatsFile = new ofstream("GooseLeaveReasonStats.txt", ios::out);
	std::vector<std::string> lr_headers = { "season", "day", "day_in_year", "species_type", "leave_reason", "n" };
	WriteHeaders(m_GooseLeaveReasonStatsFile, lr_headers);

	m_StateStatsFile = new ofstream("GooseStateStats.txt", ios::out);
	std::vector<std::string> ss_headers = { "season", "day", "n" };
	WriteHeaders(m_StateStatsFile, ss_headers);

	m_GrainDistFile = new ofstream("GooseGrainDist.txt", ios::out);
	std::vector<std::string> gd_headers = { "season", "grain_dist" };
	WriteHeaders(m_GrainDistFile, gd_headers);

	m_GooseEnergeticsDataFile = new ofstream("GooseEnergeticsData.txt", ios::out);
	std::vector<std::string> ed_headers;
	ed_headers = { "season", "day", "species", "foraging_time", "foraging_time_se",
				  "flight_distance", "flight_distance_se", "daily_energy_budget",
				  "daily_energy_budget_se", "daily_energy_balance",
				  "daily_energy_balance_se", "day_length" ,"foraging_time_sd","flight_distance_sd","daily_energy_budget_sd",
                   "daily_energy_balance_sd", "rest_time", "rest_time_sd"};
	WriteHeaders(m_GooseEnergeticsDataFile, ed_headers);
	m_GooseFieldForageDataFile = new ofstream("GooseFieldForageData.txt", ios::out);
	// We use two different versions dependent on whether we are testing of just running
	if (GooseCfg.cfg_goose_runtime_reporting.value())
	{
		std::vector<std::string> fd_headers;
		fd_headers = {
			"season",
			"day",
			"geese",
			"pinkfoot",
			"pinkfoot_timed",
			"roost_dist_pinkfoot",
			"barnacle",
			"barnacle_timed",
			"roost_dist_barnacle",
			"greylag",
			"greylag_timed",
			"roost_dist_greylag",
			"grain",
			"maize",
			"polyref"
		};
		WriteHeaders(m_GooseFieldForageDataFile, fd_headers);
	}
	if (!GooseCfg.cfg_goose_runtime_reporting.value())
	{
		std::vector<std::string> fd_headers;
		fd_headers = {"season", "day", "polyref",  "utm_x", "utm_y", "geese",
					  "geese_timed", "openness", "pinkfoot", "pinkfoot_timed",
					  "roost_dist_pinkfoot", "grass_pinkfoot", "barnacle",
					  "barnacle_timed", "roost_dist_barnacle", "grass_barnacle",
					  "greylag", "greylag_timed", "roost_dist_greylag", "grass_greylag",
					  "grain", "maize", "veg_type_chr", "veg_height", "digestability",
					  "veg_phase", "previous_crop", "last_sown_veg" };
		WriteHeaders(m_GooseFieldForageDataFile, fd_headers);
	}
	if (GooseCfg.cfg_goose_ObservedOpennessQuery.value())
	{
		ObservedOpennessQuery();
	}
	if (GooseCfg.cfg_goose_WriteConfig.value()) {
		WriteConfig();
	}
#ifdef __GITVERSION
	if (l_map_print_git_version_info.value())
	{
		m_GooseGitVersionFile = new ofstream("almass-version.txt", ios::out);
		(*m_GooseGitVersionFile) << "hash=" << GIT_HASH << std::endl;
		(*m_GooseGitVersionFile) << "time=" << COMPILE_TIME << std::endl;
		(*m_GooseGitVersionFile) << "branch=" << GIT_BRANCH << std::endl;
	}
#endif
}
/** 
Here locations of geese observed in the field are read in from a file and the
openness score for those locations are written to a file.
*/
void Goose_Population_Manager::ObservedOpennessQuery()  
{
	ofstream ofile("GooseObservedOpenness.txt", ios::out);
	ofile << "Polyref" << '\t' << "Openness" << '\t' << "ElementType" << '\t'<< "CentroidX" << '\t' << "CentroidY" << endl;

	ifstream ifile(GooseCfg.cfg_goose_ObservedOpennessQuery_file.value());
	if (!ifile.is_open())
	{
		m_TheLandscape->Warn( "Goose_Population_Manager::Goose_Population_Manager(Landscape* L) ", "File with goose observations missing" );
		exit( 1 );
	}

	int no_entries;
	ifile >> no_entries;  // Read in the first line (number of lines)

	for (int i = 0; i < no_entries; i++)
	{
		int x;
		int y;
	    string thetype;
		int centroidx;
		int centroidy;
		int thepolyref;
		int theopenness;

		ifile >> x >> y;  // Read in the first and second column as x and y
		thetype = m_TheLandscape->PolytypeToString(m_TheLandscape->SupplyElementType(x, y));  /**@todo The string is padded by blank spaces - can this be fixed? */
		centroidx = m_TheLandscape->SupplyCentroidX(x, y);
		centroidy = m_TheLandscape->SupplyCentroidY(x, y);
		thepolyref = m_TheLandscape->SupplyPolyRef( x, y );
		theopenness = m_TheLandscape->SupplyOpenness( x, y );

		//Write out the stuff to a file.
		ofile << thepolyref << '\t' << theopenness << '\t' << thetype << '\t' << centroidx << '\t' << centroidy << endl;  
	}
	ifile.close();
	ofile.close();
}

/**
Write out the values of the configs used
Handy when compiling the result documents
*/
void Goose_Population_Manager::WriteConfig() const {
	ofstream ofile( "GooseConfig.txt", ios::out);
	boost::io::ios_flags_saver  ifs( ofile );  // Only use fixed within the scope of WriteConfig
	ofile << fixed;
	ofile << "Variable" << '\t' << "Value" << endl;
	ofile << "GOOSE_PINKFOOTWEIGHT" << '\t' << GooseCfg.cfg_goose_pf_baseweight.value() << endl;
	ofile << "GOOSE_BARNACLEWEIGHT" << '\t' << GooseCfg.cfg_goose_bn_baseweight.value() << endl;
	ofile << "GOOSE_GREYLAGWEIGHT" << '\t' << GooseCfg.cfg_goose_gl_baseweight.value() << endl;
	ofile << "GOOSE_FLIGHTCOST" << '\t' << GooseCfg.cfg_goose_flightcost.value() << endl;
	ofile << "GOOSE_FORAGEDIST_PF" << '\t' << GooseCfg.cfg_goose_pf_ForageDist.value() << endl;
	ofile << "GOOSE_FORAGEDIST_BN" << '\t' << GooseCfg.cfg_goose_bn_ForageDist.value() << endl;
	ofile << "GOOSE_FORAGEDIST_GL" << '\t' << GooseCfg.cfg_goose_gl_ForageDist.value() << endl;
	ofile << "GOOSE_BMRCONSTANTA" << '\t' << GooseCfg.cfg_goose_BMRconstant1.value() << endl;
	ofile << "GOOSE_BMRCONSTANTB" << '\t' << GooseCfg.cfg_goose_BMRconstant2.value() << endl;
	ofile << "GOOSE_THERMALCONSTANTA_PF" << '\t' << GooseCfg.cfg_goose_pf_Thermalconstant.value() << endl;
	ofile << "GOOSE_THERMALCONSTANTA_BN" << '\t' << GooseCfg.cfg_goose_bn_Thermalconstant.value() << endl;
	ofile << "GOOSE_THERMALCONSTANTA_GL" << '\t' << GooseCfg.cfg_goose_gl_Thermalconstant.value() << endl;
	ofile << "GOOSE_THERMALCONSTANTB" << '\t' << GooseCfg.cfg_goose_Thermalconstantb.value() << endl;
	ofile << "GOOSE_MAXAPPETITESCALER" << '\t' << GooseCfg.cfg_goose_MaxAppetiteScaler.value() << endl;
	ofile << "GOOSE_MAXENERGYRESERVEPROPORTION" << '\t' << GooseCfg.cfg_goose_MaxEnergyReserveProportion.value() << endl;
	ofile << "GOOSE_ENERGYCONTENTOFFAT" << '\t' << GooseCfg.cfg_goose_EnergyContentOfFat.value() << endl;
	ofile << "GOOSE_METABOLICCONVCOSTS" << '\t' << GooseCfg.cfg_goose_MetabolicConversionCosts.value() << endl;
	ofile << "GOOSE_MINFORAGEOPENNESS" << '\t' << GooseCfg.cfg_goose_MinForageOpenness.value() << endl;
	ofile << "GOOSE_PF_SEXRATIO" << '\t' << GooseCfg.cfg_goose_pf_sexratio.value() << endl;
	ofile << "GOOSE_BN_SEXRATIO" << '\t' << GooseCfg.cfg_goose_bn_sexratio.value() << endl;
	ofile << "GOOSE_GL_SEXRATIO" << '\t' << GooseCfg.cfg_goose_gl_sexratio.value() << endl;
	ofile << "GOOSE_PF_ARRIVEDATESTART" << '\t' << GooseCfg.cfg_goose_pf_arrivedatestart.value() << endl;
	ofile << "GOOSE_PF_ARRIVEDATEEND" << '\t' << GooseCfg.cfg_goose_pf_arrivedateend.value() << endl;
	ofile << "GOOSE_BN_ARRIVEDATESTART" << '\t' << GooseCfg.cfg_goose_bn_arrivedatestart.value() << endl;
	ofile << "GOOSE_BN_ARRIVEDATEEND" << '\t' << GooseCfg.cfg_goose_bn_arrivedateend.value() << endl;
	ofile << "GOOSE_GL_ARRIVEDATESTART" << '\t' << GooseCfg.cfg_goose_gl_arrivedatestart.value() << endl;
	ofile << "GOOSE_GL_ARRIVEDATEEND" << '\t' << GooseCfg.cfg_goose_gl_arrivedateend.value() << endl;
	ofile << "GOOSE_PF_LEAVINGDATESTART" << '\t' << GooseCfg.cfg_goose_pf_leavingdatestart.value() << endl;
	ofile << "GOOSE_PF_LEAVINGDATEEND" << '\t' << GooseCfg.cfg_goose_pf_leavingdateend.value() << endl;
	ofile << "GOOSE_BN_LEAVINGDATESTART" << '\t' << GooseCfg.cfg_goose_bn_leavingdatestart.value() << endl;
	ofile << "GOOSE_BN_LEAVINGDATEEND" << '\t' << GooseCfg.cfg_goose_bn_leavingdateend.value() << endl;
	ofile << "GOOSE_GL_LEAVINGDATESTART" << '\t' << GooseCfg.cfg_goose_gl_leavingdatestart.value() << endl;
	ofile << "GOOSE_GL_LEAVINGDATEEND" << '\t' << GooseCfg.cfg_goose_gl_leavingdateend.value() << endl;
	//ofile << "GOOSE_GRAINDECAYRATEWINTER" << '\t' << GooseCfg.cfg_goose_GrainDecayRateWinter.value() << endl;
	//ofile << "GOOSE_GRAINDECAYRATESPRING" << '\t' << GooseCfg.cfg_goose_GrainDecayRateSpring.value() << endl;
	ofile << "GOOSE_MAXSCAREDIST" << '\t' << GooseCfg.cfg_goose_MaxScareDistance.value() << endl;
	ofile << "GOOSE_FOLLOWINGLIKELYHOOD_BN" << '\t' << GooseCfg.cfg_goose_bn_followinglikelyhood.value() << endl;
	ofile << "GOOSE_FOLLOWINGLIKELYHOOD_PF" << '\t' << GooseCfg.cfg_goose_pf_followinglikelyhood.value() << endl;
	ofile << "GOOSE_FOLLOWINGLIKELYHOOD_GL" << '\t' << GooseCfg.cfg_goose_gl_followinglikelyhood.value() << endl;
	ofile << "GOOSE_ROOSTLEAVEDISTSD" << '\t' << GooseCfg.cfg_goose_RoostLeaveDistSD.value() << endl;
	ofile << "GOOSE_ROOSTLEAVEDISTMEAN" << '\t' << GooseCfg.cfg_goose_RoostLeaveDistMean.value() << endl;
	ofile << "GOOSE_INITIALENERGYRESERVEPROPORTION" << '\t' << GooseCfg.cfg_goose_InitialEnergyReserveProportion.value() << endl;
	ofile << "GOOSE_ROOSTCHANGECHANCE" << '\t' << GooseCfg.cfg_goose_roostchangechance.value() << endl;
	ofile << "GOOSE_LEAVEREASONSTATS" << '\t' << GooseCfg.cfg_goose_LeaveReasonStats.value() << endl;
	ofile << "GOOSE_DIST_WEIGHTING_POWER" << '\t' << GooseCfg.cfg_goose_dist_weight_power.value() << endl;
	//ofile << "GOOSE_GRASS_TO_WINTER_CEREAL_SCALER" << '\t' << GooseCfg.cfg_goose_grass_to_winter_cereal_scaler.value() << endl;
	ofile << "GOOSE_DAYTIMEBMRMULTIPLIER" << '\t' << GooseCfg.cfg_goose_daytime_BMR_multiplier.value() << endl;
	ofile << "GOOSE_NIGHTTIMEBMRMULTIPLIER" << '\t' << GooseCfg.cfg_goose_nighttime_BMR_multiplier.value() << endl;
	ofile << "GOOSE_SNOW_SCALER" << '\t' << GooseCfg.cfg_goose_snow_scaler.value() << endl;
	ofile << "VEG_GROWTHSCALERMAX" << '\t' << cfg_PermanentVegGrowthMaxScaler.value() << endl;
	ofile << "VEG_GROWTHSCALERMIN" << '\t' << cfg_PermanentVegGrowthMinScaler.value() << endl;
	ofile.close();
}

void Goose_Population_Manager::CreateObjects(int a_ob_type, TAnimal *, struct_Goose * a_data, int a_number)
{
	for (int i = 0; i<a_number; i++)
	{
		switch (a_ob_type)
		{
		case gst_PinkfootFamilyGroup:
			Goose_Pinkfoot_FamilyGroup* new_PFFamilyGoose;
			if (unsigned(SupplyListSize(a_ob_type))>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_PFFamilyGoose = dynamic_cast<Goose_Pinkfoot_FamilyGroup*>(SupplyAnimalPtr(a_ob_type,GetLiveArraySize(a_ob_type)));
				new_PFFamilyGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_PFFamilyGoose = new Goose_Pinkfoot_FamilyGroup(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
                PushIndividual(a_ob_type, new_PFFamilyGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_PinkfootNonBreeder:
			Goose_Pinkfoot_NonBreeder* new_PFNonbreederGoose;
			if (unsigned(SupplyListSize(a_ob_type))>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_PFNonbreederGoose = dynamic_cast<Goose_Pinkfoot_NonBreeder*>(SupplyAnimalPtr(a_ob_type, GetLiveArraySize(a_ob_type)));
				new_PFNonbreederGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_PFNonbreederGoose = new Goose_Pinkfoot_NonBreeder(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				PushIndividual(a_ob_type, new_PFNonbreederGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_BarnacleFamilyGroup:
			Goose_Barnacle_FamilyGroup* new_BFamilyGoose;
			if (unsigned(SupplyListSize(a_ob_type))>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_BFamilyGoose = dynamic_cast<Goose_Barnacle_FamilyGroup*>(SupplyAnimalPtr(a_ob_type,GetLiveArraySize(a_ob_type)));
				new_BFamilyGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_BFamilyGoose = new Goose_Barnacle_FamilyGroup(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
                PushIndividual(a_ob_type,new_BFamilyGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_BarnacleNonBreeder:
			Goose_Barnacle_NonBreeder* new_BNonbreederGoose;
			if (unsigned(SupplyListSize(a_ob_type))>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_BNonbreederGoose = dynamic_cast<Goose_Barnacle_NonBreeder*>(SupplyAnimalPtr(a_ob_type,GetLiveArraySize(a_ob_type)));
				new_BNonbreederGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_BNonbreederGoose = new Goose_Barnacle_NonBreeder(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
                PushIndividual(a_ob_type, new_BNonbreederGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_GreylagNonBreeder:
			Goose_Greylag_NonBreeder* new_GNonbreederGoose;
			if (unsigned(SupplyListSize(a_ob_type))>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_GNonbreederGoose = dynamic_cast<Goose_Greylag_NonBreeder*>(SupplyAnimalPtr(a_ob_type,GetLiveArraySize(a_ob_type)));
				new_GNonbreederGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_GNonbreederGoose = new Goose_Greylag_NonBreeder(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				PushIndividual(a_ob_type,new_GNonbreederGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_GreylagFamilyGroup:
			Goose_Greylag_FamilyGroup* new_GFamilyGoose;
			if (unsigned(SupplyListSize(a_ob_type))>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_GFamilyGoose = dynamic_cast<Goose_Greylag_FamilyGroup*>(SupplyAnimalPtr(a_ob_type,GetLiveArraySize(a_ob_type)));
				new_GFamilyGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
				}
			else {
				new_GFamilyGoose = new Goose_Greylag_FamilyGroup(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
                PushIndividual(a_ob_type,new_GFamilyGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		default:
			char ob[255];
			sprintf(ob, "%d", (int)a_ob_type);
			m_TheLandscape->Warn("Goose_Population_Manager::CreateObjects() unknown object type - ", ob);
			exit(1);
			break;
		}
	}
}

//---------------------------------------------------------------------------

void Goose_Population_Manager::FindClosestRoost(int &a_x, int &a_y, unsigned a_type)
{
	unsigned int answer = 0;
	int rx = m_roosts[a_type][0].m_x;
	int ry = m_roosts[a_type][0].m_y;
	int dist = g_AlmassMathFuncs.CalcDistPythagoras(rx, ry, a_x, a_y);
	for (unsigned i=1; i<m_roosts[a_type].size(); i++)
	{
		rx = m_roosts[a_type][i].m_x;
		ry = m_roosts[a_type][i].m_y;
		int di = g_AlmassMathFuncs.CalcDistPythagoras( rx, ry, a_x, a_y );
		if (di<dist) answer = i;
	}
	a_x = m_roosts[a_type][answer].m_x;
	a_y = m_roosts[a_type][answer].m_y;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoFirst()
{
	/**
	* The Goose_Population_Manager DoFirst has three main functions. The first is to control the variables related to the goose timestep of 10 minutes.
	* Here we assume the daylight hours start at time 0 and go on until the end of the daylength at any given date. \n
	*
	* Then, if we are at start of day, then max goose numbers are relayed to the field elements. \n
	* Next checks the date and determines if we need to either migrate out of the area or into the area. If so calls the appropriate immigration or emigration code.
	* Otherwise if we are in normal Danish simulation mode then at the start of each day the list of active forage locations is emptied ready to be refilled
	* as the geese start foraging and some global energetic constants are calculated (thermal regulation costs).\n
	*/
	int dlt = m_TheLandscape->SupplyDaylength();
	m_daytime = ( m_daytime + 10 ) % 1440; // We have 10 minutes timesteps. This resets m_daytime to 0 when it hits 1440.
	if (m_daytime < dlt ) m_daylight = true; else m_daylight = false;
	m_daylightleft = dlt - m_daytime;
    int today = m_TheLandscape->SupplyDayInYear();
	/*
	* Next checks the date and determines if we need to either migrate out of the area or into the area. If so calls the appropriate immigration or emigration code.
	* Otherwise if we are in normal Danish simulation mode then at the start of each day the list of active forage locations is emptied ready to be refilled
	* as the geese start foraging.
	*/
	if (m_daytime == 0)
	{
        if (today>GooseCfg.cfg_goose_MaxEnergyReserveProportionChangeDate.value()&&today<GooseCfg.cfg_goose_MaxEnergyReserveProportionEndChangeDate.value()){
            Goose_Base::Set_GooseMaxEnergyReserveProportion( GooseCfg.cfg_goose_MaxEnergyReserveProportionSpring.value());
        }
        else{
            Goose_Base::Set_GooseMaxEnergyReserveProportion( GooseCfg.cfg_goose_MaxEnergyReserveProportion.value());
        }

        DoDailyImmigrationEmigration();
		DoImmigration();
		DoEmigration();

		m_GooseForageLocations.clear();
		// Calculate energetic constants for each species
		double temp = m_TheLandscape->SupplyTemp();
		if (temp < GooseCfg.cfg_goose_pf_Thermalconstant.value())
		{
			double dt = GooseCfg.cfg_goose_pf_Thermalconstant.value() - temp;
			m_thermalcosts[gs_Pinkfoot] = GooseCfg.cfg_goose_Thermalconstantb.value() * dt;
		}
		if (temp < GooseCfg.cfg_goose_bn_Thermalconstant.value())
		{
			double dt = GooseCfg.cfg_goose_bn_Thermalconstant.value() - temp;
			m_thermalcosts[ gs_Barnacle ] = GooseCfg.cfg_goose_Thermalconstantb.value() * dt;
		}
		if (temp < GooseCfg.cfg_goose_gl_Thermalconstant.value())
		{
			double dt = GooseCfg.cfg_goose_gl_Thermalconstant.value() - temp;
			m_thermalcosts[ gs_Greylag ] = GooseCfg.cfg_goose_Thermalconstantb.value() * dt;
		}

	}
	/*
	* Then we check if we are in the hunting season of any of the legal quarry species
	*/

	if (InHuntingSeason(today, gs_Pinkfoot))
	{
		m_PinkfootSeason = true;
	}
	else m_PinkfootSeason = false;
	if (InHuntingSeason(today, gs_Greylag))
	{
		m_GreylagSeason = true;
	}
	else m_GreylagSeason = false;
	/*
	* By March first we reset all grain and maize resources to make sure we don't get 
	* problems in the following season
	*/
	if (today == GooseCfg.cfg_goose_grain_and_maize_reset_day.value()) {
		m_TheLandscape->ResetGrainAndMaize();
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoLast() {
	/**
	* The first job of DoLast is to update grain status in the forage locations.\n
	* If it is end of day, then max goose numbers are relayed to the field elements, then to inform the landscape
	* of changes in grain and maize densities as a result of goose feeding. This is done here rather than have the geese interact directly with the landscape elements for efficiency, since landscape elements will only
	* be updated once a day, and after the geese are finished. This also means fewer calculations and indirections.\n
	* If there is no grain or maize to deplete this does nothing. \n @todo Where is the grazing resource depleted?
	* \n
	* Once that is done then we need to produce output - we produce three output files here - the first with the goose
	* information at individual level. The second is field information about where the geese forage. In both cases the
	* data is written to these files when between start and end year, and start and end day (inclusive).\n
	* The third output is the a description of the population sizes of each type of goose we
	* simulate (these are numbers extant in the area being simulated).
	*/
	for (auto & m_GooseForageLocation : m_GooseForageLocations) {
		m_GooseForageLocation.UpdateKJ();
	}
	int daylength = m_TheLandscape->SupplyDaylength();
	// Since the daylight starts at m_daytime == 0 and the daylength varies, we are recording geese at a timepoint by dividing the day by a specified value. E.g. to get noon, we divide by 2
	int rectime = int(round( daylength / GooseCfg.cfg_goose_TimedCounts.value() ));
	rectime -= rectime % 10;  // To get a timestep point
	if (rectime == m_daytime) {
		int sz = (int)m_GooseForageLocations.size();
		for (int i = 0; i < sz; i++) {
			int pref = m_GooseForageLocations[i].GetPolygonref();
			int goose_numbers = m_GooseForageLocations[i].GetGooseNumbers();
			m_TheLandscape->RecordGooseNumbersTimed(pref, goose_numbers);
			for (unsigned j = 0; j < gs_foobar; j++) {
				int goose_numbers_timed = m_GooseForageLocations[i].GetBirds((GooseSpecies)j);
				m_TheLandscape->RecordGooseSpNumbersTimed(pref, goose_numbers_timed, (GooseSpecies)j);
				int roost_dist = int( m_GooseForageLocations[i].GetRoostDist((GooseSpecies)j));
				m_TheLandscape->RecordGooseRoostDist(pref, roost_dist, (GooseSpecies)j);
			}
		}
	}
	int day = m_TheLandscape->SupplyDayInYear();
	if (day == 300)
	{
		rectime = int(round(daylength / GooseCfg.cfg_goose_TimedCounts.value()));
		rectime -= rectime % 10;  // To get a timestep point
		if (rectime == m_daytime) 
		{
			XYDump();
		}
	}
	if (m_daytime == 1430) // Last chance before sunrise (start of next day)
	{
		day = m_TheLandscape->SupplyDayInYear(); // **CJT** This looks to be superfluous
		if (day == 183) m_SeasonNumber++;

		int sz = (int)m_GooseForageLocations.size();
		for (int i = 0; i < sz; i++) {
			m_TheLandscape->RecordGooseNumbers( m_GooseForageLocations[ i ].GetPolygonref(), m_GooseForageLocations[ i ].GetMaxBirdsPresent() );
			for (unsigned j = 0; j < gs_foobar; j++) {
				m_TheLandscape->RecordGooseSpNumbers( m_GooseForageLocations[ i ].GetPolygonref(), m_GooseForageLocations[ i ].GetMaxSpBirdsPresent( (GooseSpecies)j ), (GooseSpecies)j );
			}
			// Remove bird records from the location and do a debug check on any birds remaining.
			m_GooseForageLocations[ i ].ClearBirds();
		}
		// If there are any geese and the output is required we record output:
		int anygeese = 0;
		for (int i = 0; i < gst_foobar; ++i) {
			anygeese = (int) GetLiveArraySize( i );
			if (anygeese > 0) {
				break;
			}
		}
		if (anygeese > 0) {
			if (GooseCfg.cfg_goose_FieldForageInfo.value()) {
				GooseFieldForageInfoOutput();
			}
			if (GooseCfg.cfg_goose_EnergyRecord.value()) {
				GooseEnergyRecordOutput();
				ClearGooseForagingTimeStats();
				ClearGooseFlightDistanceStats();
				ClearGooseDailyEnergyBudgetStats();
				ClearGooseDailyEnergyBalanceStats();
			}
			if (GooseCfg.cfg_goose_WeightStats.value()) {
				if ((day + 1) % 7 == 0)  // Only do weekly mean of the weights. +1 to avoid day 0 problems
				{
					GooseWeightStatOutput();  // Here we record the weight stats for the day
                      // and here we reset them. The recording done weekly, thus the weight reset has to be done weekly on the same day.
                      // Consider moving all of it to Goose_Base::StartDay() In order to avoid having half code here half code there.
                    ClearGooseWeightStats();

                }
			}
			if (GooseCfg.cfg_goose_IndLocCounts.value()) {
				GooseIndLocCountOutput();  // Here we record the individual forage location counts stats for the day
				ClearIndLocCountStats();  // and here we reset them.
			}
			if (GooseCfg.cfg_goose_StateStats.value())
			{
				StateStatOutput();
				ClearStateStats();
			}
			if (GooseCfg.cfg_goose_PopulationDescriptionON.value()) {
				GoosePopulationDescriptionOutput();
			}
			if (GooseCfg.cfg_goose_LeaveReasonStats.value()) {
				GooseLeaveReasonStatOutput();
				ClearGooseLeaveReasonStats();
			}
		}
		// Update forage:
		for (int i = 0; i < sz; i++) {
			int TheForageLocationPolyref = m_GooseForageLocations[ i ].GetPolygonref();
			double poly_grain = m_TheLandscape->SupplyBirdSeedForage( TheForageLocationPolyref );
			if (poly_grain > 0) {
				m_TheLandscape->SetBirdSeedForage( TheForageLocationPolyref, m_GooseForageLocations[ i ].GetGrainDensity() );
			}
			double poly_maize = m_TheLandscape->SupplyBirdMaizeForage( TheForageLocationPolyref );
			if (poly_maize > 0) {
				m_TheLandscape->SetBirdMaizeForage( TheForageLocationPolyref, m_GooseForageLocations[ i ].GetMaizeDensity() );
			}
			if (m_GooseForageLocations[ i ].GetGrazedBiomass() > 0) {
				m_TheLandscape->GrazeVegetationTotal( TheForageLocationPolyref, m_GooseForageLocations[ i ].GetGrazedBiomass() );
				m_GooseForageLocations[ i ].ResetGrazing();
			}
		}
	}
	// Record habitat use
	if (GetDayTime() == GooseCfg.cfg_gooseHabitatUsetime.value())
	{
		// Need to cycle through all the geese and ask where they foraged last
		for (int i = 0; i < gst_foobar; i++)
		{
			Goose_Base* gb;
			int sz = int(GetLiveArraySize(i));
			for (int j = 0; j < sz; j++)
			{
				// This records what the geese fed on - provides the habitat use output
				gb = (dynamic_cast<Goose_Base*>(SupplyAnimalPtr(i,j)));
				TMaxIntakeSource src = gb->GetMaxIntakeSource();
				if (src.m_maxintakesource != tomis_foobar)
				{
					RecordHabitatUse(src.m_maxintakesource, gb->GetSpecies(), gb->GetGroupsize());
					// Below the same kind of output, but now mimicing the field observations
					TTypeOfMaxIntakeSource res = tomis_foobar;
					// If it is maize it is maize if in stubble
					if (src.m_maxintakesource == tomis_sowncrop) res = tomis_sowncrop;
					else if ((((src.m_prevsowncrop == tov_Maize) || (src.m_prevsowncrop == tov_MaizeSilage))) && (src.m_instubble)) res = tomis_maize;
					else if (src.m_instubble) {
						if (src.m_iscereal) res = tomis_grain;
						else if (m_TheLandscape->SupplyIsGrass2(src.m_veg)) res = tomis_grass; // post silage cut
					}
					else res = tomis_grass;
					RecordHabitatUseFieldObs(res, gb->GetSpecies(), gb->GetGroupsize());
				}
			}
		}
		// Write the record
		if (g_date->GetDayInMonth() == 1)
		{
			GooseHabitatUseOutput();
			GooseHabitatUseFieldObsOutput();
		}
		if (g_date->JanFirst() && m_SeasonNumber > 0)
		{
			GrainDistOutput();
		}
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseHabitatUseOutput()
{
	for (int i = 0; i < gs_foobar; ++i)
	{
		double pct[tomis_foobar];
		int count = 0;
		for (int h = 0; h < tomis_foobar; h++) {
			count += m_HabitatUseStats[i* tomis_foobar + h];
			pct[h] = 0;
		}
		for (int h = 0; h < tomis_foobar; h++) {
			if (count > 0) pct[h] = m_HabitatUseStats[i* tomis_foobar + h] / double(count); else pct[h] = 0;
		}

		std::string gs = GooseToString((GooseSpecies)i);
		(*m_GooseHabitatUseFile)
			<< m_SeasonNumber << '\t'
			<< m_TheLandscape->SupplyGlobalDate() << '\t'
			<< m_TheLandscape->SupplyDayInYear() << '\t'
			<< gs << '\t';
		for (double h : pct) {
			(*m_GooseHabitatUseFile) << h << '\t';
		}
		(*m_GooseHabitatUseFile) << count << endl;
	}
	ClearGooseHabitatUseStats();
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseHabitatUseFieldObsOutput()
{
	for (int i = 0; i < gs_foobar; ++i)
	{
		double pct[tomis_foobar];
		int count = 0;
		for (int h = 0; h < tomis_foobar; h++) {
			count += m_HabitatUseFieldObsStats[i* tomis_foobar + h];
			pct[h] = 0;
		}
		for (int h = 0; h < tomis_foobar; h++) {
			if (count > 0) pct[h] = m_HabitatUseFieldObsStats[i* tomis_foobar + h] / double(count); else pct[h] = 0;
		}

		std::string gs = GooseToString((GooseSpecies)i);
		(*m_GooseHabitatUseFieldObsFile)
			<< m_SeasonNumber << '\t'
			<< m_TheLandscape->SupplyGlobalDate() << '\t'
			<< m_TheLandscape->SupplyDayInYear() << '\t'
			<< gs << '\t';
		for (double h : pct) {
			(*m_GooseHabitatUseFieldObsFile) << h << '\t';
		}
		(*m_GooseHabitatUseFieldObsFile) << count << endl;
	}
	ClearGooseHabitatUseFieldObsStats();
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseFieldForageInfoOutput()
{
	/** 
	* The information needed here is in the landscape. 
	* We need to trawl through all the fields and record those that have suitable openness scores and food for geese 
	*/
	long Day = m_TheLandscape->SupplyGlobalDate();

	GooseFieldList* afieldlist = m_TheLandscape->GetGooseFields(GooseCfg.cfg_goose_MinForageOpenness.value());
	auto sz = (unsigned) afieldlist->size();
	if (GooseCfg.cfg_goose_runtime_reporting.value())
	{
		for (unsigned f = 0; f < sz; f++)
		{
			// Only write if there were any geese
			if  ((*afieldlist)[f].vegtypechr == "WinterWheat"){//if ((*afieldlist)[f].geese > 0) {
				(*m_GooseFieldForageDataFile)
					<< m_SeasonNumber << '\t'
					<< Day << '\t'
					<< (*afieldlist)[f].geese << '\t';
					for (int i = 0; i < gs_foobar; ++i)
					{
						(*m_GooseFieldForageDataFile) 
							<< (*afieldlist)[f].geesesp[i] << '\t'
							<< (*afieldlist)[f].geesespTimed[i] << '\t'
							<< (*afieldlist)[f].roostdists[i] << '\t';
					}
					(*m_GooseFieldForageDataFile)
						<< (*afieldlist)[f].grain << '\t'
						<< (*afieldlist)[f].maize << '\t' 
						<< (*afieldlist)[f].polyref << endl;
			}
		}
	}
	if (!GooseCfg.cfg_goose_runtime_reporting.value()) {
		for (unsigned f = 0; f < sz; f++)
		{
			int pref = (*afieldlist)[f].polyref;
			int utm_x = 484378 + m_TheLandscape->SupplyCentroidX(pref);
			int utm_y = 6335161 - m_TheLandscape-> SupplyCentroidY(pref);
			(*m_GooseFieldForageDataFile)
				<< m_SeasonNumber << '\t'
				<< Day << '\t'
				<< pref << '\t'
				<< utm_x << '\t'
				<< utm_y << '\t'
				<< (*afieldlist)[f].geese << '\t'
				<< (*afieldlist)[f].geeseTimed << '\t'
				<< (*afieldlist)[f].openness << '\t';
			for (int i = 0; i < gs_foobar; ++i)
			{
				(*m_GooseFieldForageDataFile)
					<< (*afieldlist)[f].geesesp[i] << '\t'
					<< (*afieldlist)[f].geesespTimed[i] << '\t'
					<< (*afieldlist)[f].roostdists[i] << '\t'
					<< (*afieldlist)[f].grass[i] << '\t';
			}
			(*m_GooseFieldForageDataFile)
				<< (*afieldlist)[f].grain << '\t'
				<< (*afieldlist)[f].maize << '\t'
				<< (*afieldlist)[f].vegtypechr << '\t'
				<< (*afieldlist)[f].vegheight << '\t'
				<< (*afieldlist)[f].digestability << '\t'
				<< (*afieldlist)[f].vegphase << '\t'
				<< (*afieldlist)[f].previouscrop << '\t'
				<< (*afieldlist)[f].lastsownveg << endl;
		}
	}
	delete afieldlist;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseEnergyRecordOutput()
{
	long day = m_TheLandscape->SupplyGlobalDate();
	int day_length = m_TheLandscape->SupplyDaylength();
		for (int i = 0; i < gs_foobar; ++i)
		{
			std::string gs = GooseToString((GooseSpecies)i);
			(*m_GooseEnergeticsDataFile)
				<< m_SeasonNumber << '\t'
				<< day << '\t'
				<< gs << '\t'
				<< m_ForagingTimeStats[i].get_meanvalue() << '\t'
				<< m_ForagingTimeStats[i].get_SE() << '\t'
				<< m_FlightDistanceStats[i].get_meanvalue() << '\t'
				<< m_FlightDistanceStats[i].get_SE() << '\t'
				<< m_DailyEnergyBudgetStats[i].get_meanvalue() << '\t'
				<< m_DailyEnergyBudgetStats[i].get_SE() << '\t'
				<< m_DailyEnergyBalanceStats[i].get_meanvalue() << '\t'
				<< m_DailyEnergyBalanceStats[i].get_SE() << '\t'
				<< day_length<< '\t'
				<< m_ForagingTimeStats[i].get_SD() << '\t'
				<< m_FlightDistanceStats[i].get_SD() << '\t'
				<< m_DailyEnergyBudgetStats[i].get_SD() << '\t'
				<< m_DailyEnergyBalanceStats[i].get_SD() << '\t'
                << m_RestTimeStats[i].get_meanvalue() << '\t'
                << m_RestTimeStats[i].get_SD()
				 << endl;
		}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseWeightStatOutput()
{
	for (int i = 0; i < gs_foobar; ++i)
	{
		std::string gs = GooseToString((GooseSpecies)i);
		(*m_GooseWeightStatsFile)
			<< m_SeasonNumber << '\t'
			<< m_TheLandscape->SupplyGlobalDate() << '\t'
			<< m_TheLandscape->SupplyDayInYear() << '\t'
			<< gs << '\t'
			<< m_WeightStats[i].get_meanvalue() << '\t'
			<< m_WeightStats[i].get_SE() << '\t'
			<< m_WeightStats[i].get_N() <<'\t'<< m_WeightStats[i].get_SD() << endl;
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseIndLocCountOutput()
{
	for (int i = 0; i < gs_foobar; ++i)
	{
		std::string gs = GooseToString((GooseSpecies)i);
		(*m_GooseIndLocCountFile)
			<< m_SeasonNumber << '\t'
			<< m_TheLandscape->SupplyGlobalDate() << '\t'
			<< m_TheLandscape->SupplyDayInYear() << '\t'
			<< gs << '\t'
			<< m_IndividualForageLocationData[i].get_meanvalue() << '\t'
			<< m_IndividualForageLocationData[i].get_SE() << '\t'
			<< m_IndividualForageLocationData[i].get_N() << '\t'<< m_IndividualForageLocationData[i].get_SD() << endl;
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::
StateStatOutput() {
	long Day = m_TheLandscape->SupplyGlobalDate();
	(*m_StateStatsFile) << m_SeasonNumber << '\t' << Day << '\t' << m_StateStats.get_N() << endl;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::
GrainDistOutput() {
	std::string grain_dist = "unknown-grain-dist";
	if(m_TheLandscape->SupplyGrainDist() == 0) grain_dist = "high";
	if(m_TheLandscape->SupplyGrainDist() == 1) grain_dist = "low";
	(*m_GrainDistFile) << m_SeasonNumber << '\t' << grain_dist << endl;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseLeaveReasonStatOutput()
{
	long day = m_TheLandscape->SupplyGlobalDate();
	int day_in_year = m_TheLandscape->SupplyDayInYear();
	for (int i = 0; i < gst_foobar; ++i)
	{
		std::string gst = GooseTypeToString((GooseSpeciesType)i);
		for (int j = 0; j < tolr_foobar; ++j)
		{
			(*m_GooseLeaveReasonStatsFile)
				<< m_SeasonNumber << '\t'
				<< day << '\t'
				<< day_in_year << '\t'
				<< gst << '\t'
				<< LeaveReasonToString((TTypeOfLeaveReason)j) << '\t'
				<< m_LeaveReasonStats[i][j].get_N() << endl;
		}
	}
}

//---------------------------------------------------------------------------
void Goose_Population_Manager::GoosePopulationDescriptionOutput()
{
	/**
	* Here the first job is to correct the standard output and produce a more detailed
	* description of the population, ie not counting families as one.
	*/
	int sz = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
	int pff = 0;
	for (int j = 0; j<sz; j++)
	{
		pff += dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_PinkfootFamilyGroup,j))->GetGroupsize();
	}
	sz = int(GetLiveArraySize(gst_BarnacleFamilyGroup));
	int bf = 0;
	for (int j = 0; j<sz; j++)
	{
		bf += dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_BarnacleFamilyGroup,j))->GetGroupsize();
	}
	sz = int(GetLiveArraySize(gst_GreylagFamilyGroup));
	int gf = 0;
	for (int j = 0; j<sz; j++)
	{
		gf += dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_GreylagFamilyGroup,j))->GetGroupsize();
	}
	(*m_GoosePopDataFile)
	  << m_SeasonNumber << '\t'
	  << m_TheLandscape->SupplyGlobalDate() << '\t'
	  << pff << '\t'
	  << int(GetLiveArraySize(gst_PinkfootNonBreeder)) << '\t'
	  << bf << '\t'
	  << int(GetLiveArraySize(gst_BarnacleNonBreeder)) << '\t'
	  << gf << '\t'
	  << int(GetLiveArraySize(gst_GreylagNonBreeder)) << '\t' 
	  << m_TheLandscape->SupplySnowDepth() << endl;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoImmigration()
{
	/**
	* Here we need to determine the inputs in terms of birds to the Danish simulation area and create all the necessary objects and object relationships.
	* This is a tiny bit tricky since it is important that all birds start the simulation with a sensible and complete characteristics.
	*
	* The first step is to determine whether there are birds of each type to immigrate today, and if so how many. We have the numbers to immigrate and the 
	* range of days over which this happens. For sake of simplicity and because we don't have other information we will assume an even distribution of birds 
	* over this period.
	*/
	
	for (auto & m_migrationnumber : m_migrationnumbers)
	{
		m_migrationnumber[0] = 0;
		m_migrationnumber[1] = 0;
	}

	// Get the date as day in year
	int today = m_TheLandscape->SupplyDayInYear();
	// Pink foot immigration?
	if ((today >= GooseCfg.cfg_goose_pf_arrivedatestart.value()) && (today <= GooseCfg.cfg_goose_pf_arrivedateend.value()))
	{
		// there are some birds to come in today
		GetImmigrationNumbers(gs_Pinkfoot, true);  // true is fall immigration
	}
	/** Pinkfoot spring migration*/
	if (GooseCfg.cfg_goose_pf_springmigrate.value()) {
		if (GooseCfg.cfg_goose_pf_leavingdateend.value() > today) {
			if ((today >= GooseCfg.cfg_goose_pf_springmigdatestart.value()) && (today <= GooseCfg.cfg_goose_pf_springmigdateend.value()) && m_SeasonNumber > 0) {
				// there are some birds to come in today
				GetImmigrationNumbers(gs_Pinkfoot, false);  // false is spring immigration
			}
		}
	}
	// Barnacle immigration?
	if ((today >= GooseCfg.cfg_goose_bn_arrivedatestart.value()) && (today <= GooseCfg.cfg_goose_bn_arrivedateend.value()))
	{
		// there are some birds to come in today
		GetImmigrationNumbers(gs_Barnacle, true);
	}
	/** Barnacle spring migration*/
	if (GooseCfg.cfg_goose_bn_springmigrate.value()) {
		if (GooseCfg.cfg_goose_bn_leavingdateend.value() > today) {
			if ((today >= GooseCfg.cfg_goose_bn_springmigdatestart.value()) && (today <= GooseCfg.cfg_goose_bn_springmigdateend.value()) && m_SeasonNumber > 0) {
				// there are some birds to come in today
				GetImmigrationNumbers(gs_Barnacle, false);
			}
		}
	}
	// Greylag immigration?
	if ((today >= GooseCfg.cfg_goose_gl_arrivedatestart.value()) && (today <= GooseCfg.cfg_goose_gl_arrivedateend.value()))
	{
		// there are some birds to come in today
		GetImmigrationNumbers(gs_Greylag, true);
	}
	/** Greylag spring migration*/
	if (GooseCfg.cfg_goose_gl_springmigrate.value()) {
		if (GooseCfg.cfg_goose_gl_leavingdateend.value() > today) {
			if ((today >= GooseCfg.cfg_goose_gl_springmigdatestart.value()) && (today <= GooseCfg.cfg_goose_gl_springmigdateend.value()) && m_SeasonNumber > 0) {
				// there are some birds to come in today
				GetImmigrationNumbers(gs_Greylag, false);
			}
		}
	}

	struct_Goose* gs;
	gs = new struct_Goose;
	gs->m_GPM = this;
	gs->m_L = m_TheLandscape;
    double group_weight{0};
	int youngno = (int) m_youngdist.size(); // The length of the vector with brood sizes

    // Create the pinkfooted families
	for (int i=0; i<m_migrationnumbers[gs_Pinkfoot][0]; i++)
	{

		// Find the roost location
		int index = random((int) m_roosts[gs_Pinkfoot].size());
		gs->m_roost = m_roosts[gs_Pinkfoot][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		gs->m_sex = true;
		gs->m_grpsize = 2 + m_youngdist[ random(youngno) ];

        group_weight+=GooseCfg.father_pf_d(gen);
        group_weight+=GooseCfg.mother_pf_d(gen);
        for (int j=2;j<(gs->m_grpsize); j++){
            group_weight+=GooseCfg.young_pf_d(gen);
        }

        gs->m_weight = group_weight/(gs->m_grpsize); // The weight of the group object is mean weight of the family members
		CreateObjects(gst_PinkfootFamilyGroup,nullptr,gs,1);
	}
	// Create the pink footed non-breeders
	for (int i = 0; i<m_migrationnumbers[gs_Pinkfoot][1]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Pinkfoot].size());
		gs->m_roost = m_roosts[gs_Pinkfoot][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		if (g_rand_uni() < GooseCfg.cfg_goose_pf_sexratio.value()) {
		    gs->m_sex = true; // true = male, false = female
            gs->m_weight = GooseCfg.father_pf_d(gen);

		}
		else {
            gs->m_sex = false; // true = male, false = female
            gs->m_weight = GooseCfg.mother_pf_d(gen);
        }




		CreateObjects(gst_PinkfootNonBreeder,nullptr,gs,1);
	}
	// Create the barnacle families
	for (int i = 0; i<m_migrationnumbers[gs_Barnacle][0]; i++)
	{
	    group_weight=0;
		// Find the roost location
		int index = random((int) m_roosts[gs_Barnacle].size());
		gs->m_roost = m_roosts[gs_Barnacle][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		gs->m_sex = true;


		gs->m_grpsize = 2 + m_youngdist[ random( youngno) ];  // Values for Pinkfeet in lack of better data

        group_weight+=GooseCfg.father_bn_d(gen);
        group_weight+=GooseCfg.mother_bn_d(gen);
        for (int j=2;j<(gs->m_grpsize); j++){
            group_weight+=GooseCfg.young_bn_d(gen);
        }

        gs->m_weight = group_weight/(gs->m_grpsize); // The weight of the group object is mean weight of the family members
		CreateObjects(gst_BarnacleFamilyGroup, nullptr, gs, 1);
	}
	// Create the barnacle non-breeders
	for (int i = 0; i<m_migrationnumbers[gs_Barnacle][1]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Barnacle].size());
		gs->m_roost = m_roosts[gs_Barnacle][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		if (g_rand_uni() < GooseCfg.cfg_goose_bn_sexratio.value()) {
		    gs->m_sex = true;
            gs->m_weight = GooseCfg.father_bn_d(gen);
		}
		else{
		    gs->m_sex = false;
            gs->m_weight = GooseCfg.mother_bn_d(gen);
		} // true = male, false = female


		CreateObjects(gst_BarnacleNonBreeder,nullptr,gs,1);
	}
	// Create the greylag families
	for (int i = 0; i<m_migrationnumbers[gs_Greylag][0]; i++)
	{
        group_weight=0;
		// Find the roost location
		int index = random((int) m_roosts[gs_Greylag].size());
		gs->m_roost = m_roosts[gs_Greylag][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		gs->m_sex = true;



        group_weight+=GooseCfg.father_gl_d(gen);
        group_weight+=GooseCfg.mother_gl_d(gen);
        gs->m_grpsize = 2 + m_youngdist[ random( youngno) ];  // Values for Pinkfeet in lack of better data
        for (int j=2;j<(gs->m_grpsize); j++){
            group_weight+=GooseCfg.young_gl_d(gen);
        }

        gs->m_weight = group_weight/(gs->m_grpsize); // The weight of the group object is mean weight of the family members

		CreateObjects(gst_GreylagFamilyGroup, nullptr, gs, 1);
	}
	// Create the greylag non-breeders
	for (int i = 0; i<m_migrationnumbers[gs_Greylag][1]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Greylag].size());
		gs->m_roost = m_roosts[gs_Greylag][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		if (g_rand_uni() < GooseCfg.cfg_goose_gl_sexratio.value()) {
		    gs->m_sex = true;
            gs->m_weight = GooseCfg.father_gl_d(gen);
		} else{
		    gs->m_sex = false;
            gs->m_weight = GooseCfg.mother_gl_d(gen);
		}  // true = male, false = female


		CreateObjects(gst_GreylagNonBreeder,nullptr,gs,1);
	}
	delete gs;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoEmigration()
{
	double emigrationchance;
	// Get the date as day in year
	int today = m_TheLandscape->SupplyDayInYear();
	// Pink foot emigration?
	if ((today >= GooseCfg.cfg_goose_pf_leavingdatestart.value()) && (today <= GooseCfg.cfg_goose_pf_leavingdateend.value()))
	{
		// Should do some emigration. The chance of this is determined by the date. This is a linearly scaling probability
		// equal to 1/(1+lastdate-today)
		emigrationchance = 1.0 / (1 + GooseCfg.cfg_goose_pf_leavingdateend.value() - today);
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_PinkfootFamilyGroup,j))->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_PinkfootNonBreeder));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_PinkfootNonBreeder,j))->On_Emigrate();
		}
	}
	// Pinkfoot fall migration?
	if (GooseCfg.cfg_goose_pf_fallmigrate.value() && (today >= GooseCfg.cfg_goose_pf_fallmigdatestart.value()) && (today <= GooseCfg.cfg_goose_pf_fallmigdateend.value())) {
		emigrationchance = GooseCfg.cfg_goose_pf_fallmig_probability.value();
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_PinkfootFamilyGroup,j))->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_PinkfootNonBreeder));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_PinkfootNonBreeder,j))->On_Emigrate();
		}
	}
	// Barnacle emigration?
	if ((today >= GooseCfg.cfg_goose_bn_leavingdatestart.value()) && (today <= GooseCfg.cfg_goose_bn_leavingdateend.value()))
	{
		// Should do some emigration. The chance of this is determined by the date. This is a linearly scaling probability
		// equal to 1/(1+lastdate-today)
		emigrationchance = 1.0 / (1 + GooseCfg.cfg_goose_bn_leavingdateend.value() - today);
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_BarnacleFamilyGroup));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_BarnacleFamilyGroup,j))->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_BarnacleNonBreeder));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_BarnacleNonBreeder,j))->On_Emigrate();
		}
	}
	// Barnacle fall migration?
	if (GooseCfg.cfg_goose_bn_fallmigrate.value() && (today >= GooseCfg.cfg_goose_bn_fallmigdatestart.value()) && (today <= GooseCfg.cfg_goose_bn_fallmigdateend.value())) {
		emigrationchance = GooseCfg.cfg_goose_bn_fallmig_probability.value();
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_BarnacleFamilyGroup));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_BarnacleFamilyGroup,j))->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_BarnacleNonBreeder));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_BarnacleNonBreeder,j))->On_Emigrate();
		}
	}
	// Greylag emigration?
	if ((today >= GooseCfg.cfg_goose_gl_leavingdatestart.value()) && (today <= GooseCfg.cfg_goose_gl_leavingdateend.value()))
	{
		// Should do some emigration. The chance of this is determined by the date. This is a linearly scaling probability
		// equal to 1/(1+lastdate-today)
		emigrationchance = 1.0 / (1 + GooseCfg.cfg_goose_gl_leavingdateend.value() - today);
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_GreylagFamilyGroup));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_GreylagFamilyGroup,j))->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_GreylagNonBreeder));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_GreylagNonBreeder,j))->On_Emigrate();
		}
	}
	// Greylag fall migration?
	if (GooseCfg.cfg_goose_gl_fallmigrate.value() && (today >= GooseCfg.cfg_goose_gl_fallmigdatestart.value()) && (today <= GooseCfg.cfg_goose_gl_fallmigdateend.value())) {
		emigrationchance = GooseCfg.cfg_goose_gl_fallmig_probability.value();
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_GreylagFamilyGroup));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_GreylagFamilyGroup,j))->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_GreylagNonBreeder));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_GreylagNonBreeder,j))->On_Emigrate();
		}
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoDailyImmigrationEmigration()
{
	/**
	Runs through the daily exchange rates for each species and removes and adds the same number of objects
	*/
	struct_Goose* gs;
	gs = new struct_Goose;
	gs->m_GPM = this;
	gs->m_L = m_TheLandscape;
	double baseweights[gs_foobar];
	baseweights[gs_Pinkfoot] = GooseCfg.cfg_goose_pf_baseweight.value();
	baseweights[gs_Barnacle] = GooseCfg.cfg_goose_bn_baseweight.value();
	baseweights[gs_Greylag] = GooseCfg.cfg_goose_gl_baseweight.value();
	int youngno = (int)m_youngdist.size(); // The length of the vector with brood sizes
	int month = m_TheLandscape->SupplyMonth() - 1; // minus one to get to zero base
    double group_weight{0};
    double sexratios[3]{GooseCfg.cfg_goose_pf_sexratio.value(),GooseCfg.cfg_goose_bn_sexratio.value(),GooseCfg.cfg_goose_gl_sexratio.value()};
	for (int sp = 0; sp < gs_foobar; sp++)
	{
		int sts = sp*2;
		// For each species get the turnover %
		double turnover = inflowoutflowgeese[(sp * 12) + month];
		// for each object test for trigger emigration/immigration
		int sz = int(GetLiveArraySize(sts));
		for (int j = 0; j < sz; j++) {
			if (g_rand_uni() <= turnover) {
				dynamic_cast<Goose_Base*>(SupplyAnimalPtr(sts,j))->On_Emigrate();
				// Now immigrate one
				int index = random((int)m_roosts[sp].size());
				gs->m_roost = m_roosts[sp][index];
				gs->m_x = gs->m_roost.m_x;
				gs->m_y = gs->m_roost.m_y;
				gs->m_sex = true;


				gs->m_grpsize = 2 + m_youngdist[random(youngno)];  // Values for Pinkfeet in lack of better data
                group_weight+=GooseCfg.fathers[sp](gen);
                group_weight+=GooseCfg.mothers[sp](gen);
                for (int i=2;i<(gs->m_grpsize); i++){
                    group_weight+=GooseCfg.youngs[sp](gen);
                }

                gs->m_weight = group_weight/(gs->m_grpsize);
				CreateObjects(sts, nullptr, gs, 1);
			}
		}
		sts++;
		sz = int(GetLiveArraySize(sts));
		for (int j = 0; j < sz; j++) {
			if (g_rand_uni() <= turnover) {
				dynamic_cast<Goose_Base*>(SupplyAnimalPtr(sts,j))->On_Emigrate();
				// Now immigrate one
				int index = random((int)m_roosts[sp].size());
				gs->m_roost = m_roosts[sp][index];
				gs->m_x = gs->m_roost.m_x;
				gs->m_y = gs->m_roost.m_y;
				gs->m_sex = true;
				//gs->m_weight = baseweights[sp];

                if (g_rand_uni() <sexratios[sp]) {
                    gs->m_sex = true; // true = male, false = female
                    gs->m_weight = GooseCfg.fathers[sp](gen);

                }
                else {
                    gs->m_sex = false; // true = male, false = female
                    gs->m_weight = GooseCfg.mothers[sp](gen);
                }

				CreateObjects(sts, nullptr, gs, 1);
			}
		}
	}
	delete gs;
}
//---------------------------------------------------------------------------
int Goose_Population_Manager::ForageLocationInUse(int a_polyref)
{
	/**
	* \param [in] a_polyref is the polygon reference number that identifies the field for foraging
	* \return Returns and integer which is the index in the vector if the polyref is found, otherwise returns -1
	* Searches the whole m_GooseForageLocations vector for a match for a_polyref. Stops looking if one is found.
	*/
	int index = -1;
	int sz = (int) m_GooseForageLocations.size();
	for (int i=0; i< sz; i++)
	{
		if (m_GooseForageLocations[i].GetPolygonref() == a_polyref )
		{
			index = i;
			break;
		}
	}
	return index;
}
//---------------------------------------------------------------------------

int Goose_Population_Manager::NewForageLocation(GooseSpeciesType a_type, int a_number, int a_polyref )
{
	/**
	* \param [in] a_type is type of goose species to forage here in this first forage group
	* \param [in] a_number is the number of geese in this first forarge group
	* \param [in] a_polyref is the polygon reference number that identifies the field for foraging
	*
	* Adds the new forage location to the m_GooseForageLocations vector and adds the starting number of 
	* geese of the type specified. Calculates the distance to the closest roost for each species.
	*/
	double grain = m_TheLandscape->SupplyBirdSeedForage( a_polyref );  // grain/m2
	double maize = m_TheLandscape->SupplyBirdMaizeForage( a_polyref );  // kJ/m2
	double grazing[gs_foobar];
	// NB GooseSpeciesType >> 1 becomes a GooseSpecies
	for (int g = 0; g < gs_foobar; g++)
	{
		grazing[g] = m_TheLandscape->GetActualGooseGrazingForage(a_polyref, (GooseSpecies) g);
	}
	GooseActiveForageLocation aFL(a_type, a_number, a_polyref, (int) m_TheLandscape->SupplyPolygonArea(a_polyref), grain, maize, grazing, this);
	aFL.ResetGrazing();
	// Calculate the distance to the closest roost for each species
	int x = m_TheLandscape->SupplyCentroidX(a_polyref);
	int y = m_TheLandscape->SupplyCentroidY(a_polyref);
	for (int i = 0; i < gs_foobar; i++) {
		double dist = GetDistToClosestRoost(x, y, i);
		aFL.SetDistToClosestRoost(i, dist);
	}
	m_GooseForageLocations.push_back(aFL);
	int ind = (int)m_GooseForageLocations.size() - 1;
	return ind;
}
//---------------------------------------------------------------------------

 void Goose_Population_Manager::AddGeeseToForageLocation( GooseSpeciesType a_type, int a_index, int a_number )
 {
	 m_GooseForageLocations[a_index].AddGeese(a_type, a_number);
 }
 //---------------------------------------------------------------------------

 void Goose_Population_Manager::RemoveGeeseFromForageLocation(GooseSpeciesType a_type, int a_index, int a_number)
 {
	 m_GooseForageLocations[a_index].RemoveGeese(a_type, a_number);
 }
 //---------------------------------------------------------------------------


 void Goose_Population_Manager::AddGeeseToForageLocationP(GooseSpeciesType a_type, int a_polyref, int a_number)
 {
	/**f
	* First step is to get the forage location - then add the number of birds of a_type to it.
	*/
	int index = ForageLocationInUse( a_polyref );
	if (index == -1 )
	{
		NewForageLocation(a_type,a_number, a_polyref);
	}
	else m_GooseForageLocations[index].AddGeese(a_type, a_number);
 }
 //---------------------------------------------------------------------------

	 int Goose_Population_Manager::BirdsToShootAtPoly(int a_polyref /*, double &a_protectedpct */)
	 {
		 /**
		 * Assumes that a_protectedpct is initialised to 0.0 on entry. First step is to get the forage location - the find out how many birds are there.
		 */
		 int index = ForageLocationInUse(a_polyref);
		 if (index == -1) return 0;
		 // There are birds here - so how many and what type?
		 int geese = 0;
		 if (InPinkfootSeason())
		 {
			 geese += m_GooseForageLocations[index].GetBirds(gst_PinkfootFamilyGroup) + m_GooseForageLocations[index].GetBirds(gst_PinkfootNonBreeder);
		 }
		 if (InGreylagSeason())
		 {
			 geese += m_GooseForageLocations[index].GetBirds(gst_GreylagFamilyGroup) + m_GooseForageLocations[index].GetBirds(gst_GreylagNonBreeder);
		 }

		 //int protectedgeese = m_GooseForageLocations[index].GetBirds(gst_BarnacleFamilyGroup) + m_GooseForageLocations[index].GetBirds(gst_BarnacleNonBreeder);
		 //int birds = geese+protectedgeese;
		 //if (protectedgeese> 0) a_protectedpct = protectedgeese / (double)(birds); // else a_protectedpct = 0.0; This is not necessary since a_protectedpct should be 0.0 on entry
		 if (geese < 0) {
			 g_msg->Warn("Goose_Population_Manager::BirdsToShootAtPoly 0 or less then 0 geese to shoot.", geese);
			 exit(0);
		 }
		 return geese;
	 }
//---------------------------------------------------------------------------

void Goose_Population_Manager::BirdsShot(int a_polyref, int a_numbershot, GooseHunter* a_hunter)
 {
	 /**
	 * First step is to get the forage location - the we pick a_numbershot birds to kill.
	 * This is a bit problematic since we know the type of birds but not exactly which ones are here. To speed things up as much as possible we first determine
	 * which birds will be shot by picking randomly from the birds here.
	 * Next we need to make some 'BANG's and scare the birds away from this location and nearby locations.
	 * Note that barnacle geese cannot be shot and are excluded by the loop below.
	 * @todo Add input parameter to docu
	 */
	 int index = ForageLocationInUse(a_polyref);
	 if (index == -1)
	 {
		 g_msg->Warn("Goose_Population_Manager::BirdsShot - No birds at this location to shoot. Tried to kill ", a_numbershot);
		 exit(0);
	 }
	 int birds = (int)m_GooseForageLocations[index].GetHuntables(); // This is the number of geese not number of objects!
	 if (birds < 1) return; // Other hunters have shot all the birds already
	 GooseSpeciesType gst = gst_foobar;
	 int found = 0;
	 int counter = 0;
	 while (a_numbershot > 0)
	 {
		 counter++;
		 if (counter > 25)
		 {
			 int pffg = m_GooseForageLocations[index].GetBirds(gst_PinkfootFamilyGroup);
			 int pfnb = m_GooseForageLocations[index].GetBirds(gst_PinkfootNonBreeder);
			 int glfg = m_GooseForageLocations[index].GetBirds(gst_GreylagFamilyGroup);
			 int glnb = m_GooseForageLocations[index].GetBirds(gst_GreylagNonBreeder);
			 //cout << "Goose_Population_Manager::BirdsShot() Been shooting like crazy all morning - something is not quite right here...";
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - where are those damn birds?. Tried to kill ", a_numbershot);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - there where supposed to this many of them:", birds);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - consisting of pinkfoot familygroups:", pffg);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - consisting of pinkfoot nonbreeders:", pfnb);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - consisting of greylag familygroups:", glfg);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - consisting of greylag nonbreeders:", glnb);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - Today is day in year:", g_land->SupplyDayInYear());
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - in year:", g_land->SupplyYear());
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - Pinkfoot season?", m_PinkfootSeason);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - Greylag season?", m_GreylagSeason);
			 exit(0);
		 }
		 found = 0;
		 int deadbird = (int)(g_rand_uni() * birds);
		 {
			 found = m_GooseForageLocations[index].GetBirds(gst_PinkfootFamilyGroup);
			 if (found > deadbird && InPinkfootSeason()) gst = gst_PinkfootFamilyGroup;
			 else {
				 deadbird -= found;
				 found = m_GooseForageLocations[index].GetBirds(gst_PinkfootNonBreeder);
				 if (found > deadbird && InPinkfootSeason()) gst = gst_PinkfootNonBreeder;
				 else {
					 deadbird -= found;
					 found = m_GooseForageLocations[index].GetBirds(gst_GreylagNonBreeder);
					 if (found > deadbird && InGreylagSeason()) gst = gst_GreylagNonBreeder;
					 else {
						 deadbird -= found;
						 found = m_GooseForageLocations[index].GetBirds(gst_GreylagFamilyGroup);
						 if (found > deadbird && InGreylagSeason()) gst = gst_GreylagFamilyGroup;
					 }
				 }
			 }
		 }
		 // gst has the enum value identifying the type of shot bird.
		 int sz = int(GetLiveArraySize(gst));
		 for (int i = 0; i < sz; i++)
		 {
			 if (SupplyAnimalPtr(gst,i)->SupplyPolygonRef() == a_polyref)
			 {
				 if (SupplyAnimalPtr(gst,i)->GetCurrentStateNo() != -1)
				 {
					 // Kill the bird
                     SupplyAnimalPtr(gst,i)->KillThis();
					 // Tell the hunter
					 a_hunter->OnShotABird(gst, a_polyref);
					 a_numbershot--;
					 break;
				 }
			 }
		 }
	 }
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::BangAtPoly(int a_polyref)
{
	/**
	* Here we have to find all the geese that are near to this polygon - then tell them they are scared of this location.
	* Unfortunately there is no easy way to do this except trawl through all geese and test the distance they are from the bang
	* at the moment this method is called.
	* We assume the bang occurs at the centroid since we do not have a particular location for this within the polygon.
	* Centroid distance to the location of each bird is checked and if under the maximum threat distance then we tell the bird they are scared.
	* An alternative approach would be to link the size of scare to the distance. This is provided as an alternative assuming __LINKGOOSESCARETODISTANCE is defined.
	*/
	int polyX = m_TheLandscape->SupplyCentroidX(a_polyref);
	int polyY = m_TheLandscape->SupplyCentroidY(a_polyref);
	int maxscaredist = GooseCfg.cfg_goose_MaxScareDistance.value();
	for (unsigned i=0; i<(m_ListNameLength); i++)
	{
		auto sz = (unsigned) (GetLiveArraySize((int) i));
		for (unsigned j=0; j<sz; j++)
		{
			APoint ap = SupplyAnimalPtr(i,j)->SupplyPoint();
			int dist = g_AlmassMathFuncs.CalcDistPythagorasApprox( polyX, polyY, ap.m_x, ap.m_y );
			if (dist < maxscaredist)
			{
				auto* gb = dynamic_cast<Goose_Base*>(SupplyAnimalPtr(i,j));
#ifdef __LINKGOOSESCARETODISTANCE

				gb->On_Bang(a_polyref, 1.0-(double) (dist/maxscaredist));
#else
				gb->On_Bang(a_polyref);
#endif
			}
		}
	}
}
//---------------------------------------------------------------------------
void Goose_Population_Manager::DisturberBangAtCoords(const shared_ptr<Disturber_Population_Manager>& DPM) {
    /**
     * We look for all the geese that are affected by the map of Bangs that is given.
     * This will improve the performance. We basically have to go through the array of the birds
     * twice. First time we go through the birds and check if any is affected by disturbers.
     * After that all the birds that were disturbed give the alarm call that has some effective distance.
     * During the second go, all the birds that are within the effective distance of the alarm call
     * are regarded scared and fly away. The effective distance of the alarm call is defined
     * in the Config file
     * */
     // we need a polyref for the scared location so that the birds will remember it
     int a_polyref;
     int radius=m_AlarmCallDistance;

     int size_x = m_TheLandscape->SupplySimAreaWidth();
     int size_y = m_TheLandscape->SupplySimAreaHeight();
     //map with the alarmed birds SHOULD BE MOVED TO CONSTRUCTOR, SO THE ALLOCATION HAPPENS ONLY ONCE
     //blitz::Array<bool, 2> alarmed_birds(size_x, size_y);
     // it is initially empty
     int tmp_x;
     int tmp_y;
     /**if alarm distance is 0 or negative, then we only need one loop*/
     if (radius>0){
         (*m_AlarmedBirdsMap)=false; //@todo: This line-- creation (and destruction) of the alarmed birds map slows up things severely. Should be changed into a map that is created once and always re-used
         bool birds_alarmed=false;
         // the initial loop, going through all the birds, looking for the ones disturbed
         typedef blitz::TinyVector<int,2> coord;
         list<blitz::RectDomain<2> > circle;
         for (unsigned i=0; i<(m_ListNameLength); i++) {
             auto sz = (unsigned) GetLiveArraySize((int) i);
             int sum_disturbed=0;
             for (unsigned j = 0; j < sz; j++) {
                 APoint ap = SupplyAnimalPtr(i,j)->SupplyPoint();
                 int centre_i = ap.m_x;
                 int centre_j = ap.m_y;
                 // if the goose is affected by a disturber
                 if(DPM->IsDisturbed(ap.m_x, ap.m_y)){
                     sum_disturbed++;
                     (*m_AlarmedBirdsMap)(ap.m_x,ap.m_y)=true;
                     birds_alarmed=true;
                     tmp_x=ap.m_x;
                     tmp_y=ap.m_y;
                     //cout<<"DEBUG: bird sounded alarm"<<'\n';

                     // now we will create a circle of "true"s around the bird and the
                     // radius of the circle is a parameter m_AlarmCallDistance
                     // we use something which is called strips in blitz++
                     // we use vertical strips
                     int hor_begin = (centre_i-radius<0)?0:(centre_i-radius<0);
                     int hor_end= centre_i+radius+1>size_x?size_x:centre_i+radius+1;
                     for (int iter=hor_begin; iter < hor_end; ++iter)
                     {
                         // squared distance to the centre
                         double jdist2 = blitz::pow2(radius) - blitz::pow2(iter - centre_i);
                         if (jdist2 < 0.0)
                             continue;
                         int jdist = int(sqrt(jdist2));
                         int vert_begin = centre_j - jdist<0?0:centre_j - jdist;
                         int vert_end = centre_j + jdist>size_y?size_y:centre_j + jdist;
                         coord startPos(iter, int(vert_begin));
                         circle.push_back(strip(startPos, blitz::secondDim, int(vert_end)));
                     }
                     // now we will assign the full circle to "true"s
                     (*m_AlarmedBirdsMap)[circle]=true;
                 }
             }
             if (sum_disturbed>0){
 //                cout<<"DEBUG: "<< m_TheLandscape->SupplyGlobalDate()<<": "<<sum_disturbed<<" " <<GooseSpeciesType (i) <<" sounded alarm"<<'\n';
             }

         }
         /** Debugging output
         if (birds_alarmed){
             cout<<"DB:disturbers_map"<<(*bangmap)(blitz::Range(tmp_x-151,tmp_x+149),blitz::Range(tmp_y-151,tmp_y+149))<<'\n';
             cout<<"DB:alarmed_map"<<alarmed_birds(blitz::Range(tmp_x-151,tmp_x+149),blitz::Range(tmp_y-151,tmp_y+149))<<'\n';
         }
          **/
         // the second loop: all the birds that heard the alarm call will depart
         // only if there was at least one alarmed bird
         if (birds_alarmed){
             for (unsigned i=0; i<(m_ListNameLength); i++) {
                 auto sz = (unsigned) GetLiveArraySize((int)i);
                 int sum_disturbed=0;
                 for (unsigned j = 0; j < sz; j++) {

                     APoint ap = SupplyAnimalPtr(i,j)->SupplyPoint();
                     if ((*m_AlarmedBirdsMap)(ap.m_x,ap.m_y)){
                         sum_disturbed++;
                         //cout<<"DEBUG: bird alarmed"<<'\n';
                         a_polyref = m_TheLandscape->SupplyPolyRef(ap.m_x, ap.m_y);
                         auto* gb = dynamic_cast<Goose_Base*>(SupplyAnimalPtr(i,j));
                         // fly away while remembering the polyref, NOTE!!! here the scare factor is 1
                         // maybe there will be disturbers that scare more or less but we will tweak it later on
                         // gb->On_Bang(a_polyref, scarefactor);
                         gb->On_Bang(a_polyref);
                     }
                 }
//                 if (sum_disturbed>0) {
//                     cout << "DEBUG: " << m_TheLandscape->SupplyGlobalDate() << ": " << sum_disturbed << " "
//                          << GooseSpeciesType(i) << " heard alarm" << '\n';
 //                }
             }
         }
     }
     else {
         for (unsigned i = 0; i < (m_ListNameLength); i++) {
             auto sz = (unsigned) GetLiveArraySize((int) i);
             int sum_disturbed = 0;
             for (unsigned j = 0; j < sz; j++) {
                 APoint ap = SupplyAnimalPtr(i,j)->SupplyPoint();
                 if (DPM->IsDisturbed(ap.m_x, ap.m_y)) {
                     sum_disturbed++;
                     a_polyref = m_TheLandscape->SupplyPolyRef(ap.m_x, ap.m_y);
                     auto *gb = dynamic_cast<Goose_Base *>(SupplyAnimalPtr(i,j));
                     gb->On_Bang(a_polyref);
                 }
             }
//            if (sum_disturbed>0) {
//                 cout << "DEBUG: " << m_TheLandscape->SupplyGlobalDate() << ": " << sum_disturbed << " "
//                      << GooseSpeciesType(i) << " were disturbed" << '\n';
//             }

         }

     }
}
//---------------------------------------------------------------------------
/**
* When leaving the roost in the morning we have the option to either follow another bird or go exploring on our own./n
* If we follow another bird this function will find a bird to follow (a leader). It is a prerequisite that the leader
* is from the same roost as the bird following and that the leader have found a suitable forage location.
* \param [in] a_roost The roost of the follower as a point.
* \param [in] a_species The species of the follower.
* \return A pointer to the leader.
*/
Goose_Base* Goose_Population_Manager::GetLeader(APoint a_roost, GooseSpecies a_species)
{
	// Need to start a random point in our list of birds
	// We know the total number of bird objects of the species, so assume they are one circular long list 
	// 
	Goose_Base* GBp;
	int sz1 = 0;
	int sz2 = 0;
	int base = (int)a_species * 2;  // To get the position of the speciesFamilyGroup in TheArray
	int base1 = 1 + ((int)a_species * 2);  // the speciesNonBreeder is always the next one
	switch (a_species) {
	case gs_Pinkfoot:
		sz1 = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
		sz2 = int(GetLiveArraySize(gst_PinkfootNonBreeder));
		break;
	case gs_Barnacle:
		sz1 = int(GetLiveArraySize(gst_BarnacleFamilyGroup));
		sz2 = int(GetLiveArraySize(gst_BarnacleNonBreeder));
		break;
	case gs_Greylag:
		sz1 = int(GetLiveArraySize(gst_GreylagFamilyGroup));
		sz2 = int(GetLiveArraySize(gst_GreylagNonBreeder));
		break;
    case gs_foobar:
        g_msg->Warn("Goose_Population_Manager::GetLeader tried to get a leader for gs_foobar", "");
        exit(-1);
        break;
    }
	int total = sz1 + sz2;
	int start = random(total);
	for (int g = 0; g < total; g++)  // Loops until a goose which does not return -1 for the forage location index is found
	{
		int index;
		if (start == total) start = 0;
		if (start < sz1) {
			index = start;
			GBp = dynamic_cast<Goose_Base*>(SupplyAnimalPtr(base,index));
			if ((GBp->GetRoost().m_x == a_roost.m_x) && (GBp->GetRoost().m_y == a_roost.m_y) && (GBp->GetForageLocIndex() != -1)) {
				return GBp;
			}
		}
		else {
			index = start - sz1;
			GBp = dynamic_cast<Goose_Base*>(SupplyAnimalPtr(base1, index));
			if ((GBp->GetRoost().m_x == a_roost.m_x) && (GBp->GetRoost().m_y == a_roost.m_y) && (GBp->GetForageLocIndex() != -1)) {
				return GBp;
			}
		}
		start++;
	}
	return nullptr;
}

/** @todo add documentation */
int Goose_Population_Manager::GetForageLocIndex( GooseSpecies a_species, int a_x, int a_y ) {
	int forageindex = -1;
	std::vector<int> fields;
	int sz = (int)m_GooseForageLocations.size();
	if (sz > 0) {
		for (int i = 0; i < sz; i++) {
			int birds = m_GooseForageLocations[ i ].GetBirds( a_species );
			if (birds > 0) {
				int index = m_GooseForageLocations[ i ].GetPolygonref();
				fields.push_back( index );
			}
		}
		if (!fields.empty()) {
			// Find the distances to those forage locations
			int answer = random(int(fields.size()));
			int max_dist = 43266;  // diagonal of Vejlerne
			int fx, fy;
			for (int i = 0; i<fields.size(); i++) {
				fx = m_TheLandscape->SupplyCentroidX( fields[ i ] );
				fy = m_TheLandscape->SupplyCentroidY( fields[ i ] );
				int di = g_AlmassMathFuncs.CalcDistPythagoras( fx, fy, a_x, a_y );
				if (g_rand_uni() < exp(-((double) di / max_dist) * GooseCfg.cfg_goose_dist_weight_power.value()))
				{
					answer = i;
				} 
			}
			int polyref = fields[ answer ];
			forageindex = ForageLocationInUse( polyref );
		}
	}
	return forageindex;
}


void Goose_Population_Manager::RemoveMaxForageKj( double forage, TTypeOfMaxIntakeSource a_maxintakesource, int m_myForageIndex ) {
	switch (a_maxintakesource) {
		case tomis_grain:
			RemoveGrainKJ( forage, m_myForageIndex );
			break;
		case tomis_maize:
			RemoveMaizeKJ( forage, m_myForageIndex );
			break;
		case tomis_grass:
			Graze( forage, m_myForageIndex );
			break;
		case tomis_foobar:
			m_TheLandscape->Warn( "Goose_Population_Manager::RemoveMaxForageKj", " - Somebody visited the foobar. Not allowed!" );
			exit( 0 );
		default:
			;
	}
}

void Goose_Population_Manager::GetImmigrationNumbers(GooseSpecies a_goose, bool fall)
{
	double scaler = GooseCfg.cfg_goose_startno_scaler.value();
	double scaler_pf = GooseCfg.cfg_goose_pf_startno_scaler.value() * scaler;
	double scaler_bn = GooseCfg.cfg_goose_bn_startno_scaler.value() * scaler;
	double scaler_gl = GooseCfg.cfg_goose_gl_startno_scaler.value() * scaler;
	if (a_goose == gs_Pinkfoot && fall)
	{
		int pfnum = (int) round(GooseCfg.cfg_goose_pf_startnos.value() * scaler_pf);
		int pfyoung = (int)floor(pfnum * GooseCfg.cfg_goose_pf_young_proportion.value());
		int	pffam = (int)floor(pfyoung / 4); // On average the families have 4 young
		int	pfnb = pfnum - (pffam + pfyoung);  // The familes are then two adults plus their young

		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (pffam / (double) ((1 + GooseCfg.cfg_goose_pf_arrivedateend.value()) - GooseCfg.cfg_goose_pf_arrivedatestart.value())));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (pfnb / (double) ((1 + GooseCfg.cfg_goose_pf_arrivedateend.value()) - GooseCfg.cfg_goose_pf_arrivedatestart.value())));
	}
	if (a_goose == gs_Pinkfoot && !fall)
	{
		int pfnum = (int) round(GooseCfg.cfg_goose_pf_springmignos.value() * scaler_pf);
		int pfyoung = (int)floor(pfnum * GooseCfg.cfg_goose_pf_young_proportion.value());
		int	pffam = (int)floor(pfyoung / 4); // On average the families have 4 young
		int	pfnb = pfnum - (pffam + pfyoung);  // The familes are then two adults plus their young

		int start = GooseCfg.cfg_goose_pf_springmigdatestart.value();
		int end = GooseCfg.cfg_goose_pf_springmigdateend.value();
		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (pffam / (double) ((1 + end) - start)));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (pfnb / (double) ((1 + end) - start)));
	}
	if (a_goose == gs_Barnacle && fall)
	{
		int bnnum = (int) round(GooseCfg.cfg_goose_bn_startnos.value() * scaler_bn);
		int bnyoung = (int)floor(bnnum * GooseCfg.cfg_goose_bn_young_proportion.value());
		int	bnfam = (int)floor(bnyoung / 4); // On average the families have 4 young
		int	bnnb = bnnum - (bnfam + bnyoung);  // The familes are then two adults plus their young

		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (bnfam / (double) ((1 + GooseCfg.cfg_goose_bn_arrivedateend.value()) - GooseCfg.cfg_goose_bn_arrivedatestart.value())));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (bnnb / (double) ((1 + GooseCfg.cfg_goose_bn_arrivedateend.value()) - GooseCfg.cfg_goose_bn_arrivedatestart.value())));
	}
	if (a_goose == gs_Barnacle && !fall)
	{
		int bnnum = (int) round(GooseCfg.cfg_goose_bn_springmignos.value() * scaler_bn);
		int bnyoung = (int)floor(bnnum * GooseCfg.cfg_goose_bn_young_proportion.value());
		int	bnfam = (int)floor(bnyoung / 4); // On average the families have 4 young
		int	bnnb = bnnum - (bnfam + bnyoung);  // The familes are then two adults plus their young

		int start = GooseCfg.cfg_goose_bn_springmigdatestart.value();
		int end = GooseCfg.cfg_goose_bn_springmigdateend.value();
		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (bnfam /(double) ((1 + end) - start)));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (bnnb / (double) ((1 + end) - start)));
	}
	if (a_goose == gs_Greylag && fall)
	{
		int glnum = (int) round(GooseCfg.cfg_goose_gl_startnos.value() * scaler_gl);
		int glyoung = (int)floor(glnum * GooseCfg.cfg_goose_gl_young_proportion.value());
		int	glfam = (int)floor(glyoung / 4); // On average the families have 4 young
		int	glnb = glnum - (glfam + glyoung);  // The familes are then two adults plus their young

		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (glfam /(double) ((1 + GooseCfg.cfg_goose_gl_arrivedateend.value()) - GooseCfg.cfg_goose_gl_arrivedatestart.value())));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (glnb /(double) ((1 + GooseCfg.cfg_goose_gl_arrivedateend.value()) - GooseCfg.cfg_goose_gl_arrivedatestart.value())));
	}
	if (a_goose == gs_Greylag && !fall)
	{
		int glnum = (int) round(GooseCfg.cfg_goose_gl_springmignos.value() * scaler_gl);
		int glyoung = (int)floor(glnum * GooseCfg.cfg_goose_gl_young_proportion.value());
		int	glfam = (int)floor(glyoung / 4); // On average the families have 4 young
		int	glnb = glnum - (glfam + glyoung);  // The familes are then two adults plus their young

		int start = GooseCfg.cfg_goose_gl_springmigdatestart.value();
		int end = GooseCfg.cfg_goose_gl_springmigdateend.value();
		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (glfam /(double) ((1 + end) - start)));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (glnb /(double) ((1 + end) - start)));
	}
}


// Weight stats
void Goose_Population_Manager::RecordWeight(double a_weight, GooseSpecies a_species)
{
	m_WeightStats[a_species].add_variable(a_weight);
}

void Goose_Population_Manager::ClearGooseWeightStats() {
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_WeightStats[(GooseSpecies)i].ClearData();
	}
}

// Habitat use stats
void Goose_Population_Manager::RecordHabitatUse(int a_habitatype, GooseSpecies a_species, int a_count)
{
	m_HabitatUseStats[a_species * tomis_foobar + a_habitatype] += a_count;
}

// Habitat use stats
void Goose_Population_Manager::RecordHabitatUseFieldObs(int a_habitatype, GooseSpecies a_species, int a_count)
{
	m_HabitatUseFieldObsStats[a_species * tomis_foobar + a_habitatype] += a_count;
}

void Goose_Population_Manager::ClearGooseHabitatUseStats() {
	for (int & m_HabitatUseStat_it : m_HabitatUseStats)
	{
		m_HabitatUseStat_it = 0;
	}
}

void Goose_Population_Manager::ClearGooseHabitatUseFieldObsStats() {
	for (int & m_HabitatUseFieldObsStat_it : m_HabitatUseFieldObsStats)
	{
		m_HabitatUseFieldObsStat_it = 0;
	}
}

// Foraging time stats
void Goose_Population_Manager::RecordForagingTime(int a_time, GooseSpecies a_species)
{
	m_ForagingTimeStats[a_species].add_variable(a_time);
}
// Resting time stats
void Goose_Population_Manager::RecordRestingTime(int a_time, GooseSpecies a_species) {
    m_RestTimeStats[a_species].add_variable(a_time);
}

void Goose_Population_Manager::ClearGooseForagingTimeStats() 
{
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_ForagingTimeStats[(GooseSpecies)i].ClearData();
        m_RestTimeStats[(GooseSpecies)i].ClearData();
	}
}

// Flight distance stats
void Goose_Population_Manager::RecordFlightDistance(int a_distance, GooseSpecies a_species)
{
	m_FlightDistanceStats[a_species].add_variable(a_distance);
}

void Goose_Population_Manager::ClearGooseFlightDistanceStats() 
{
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_FlightDistanceStats[(GooseSpecies)i].ClearData();
	}
}

// Daily energy budget stats
void Goose_Population_Manager::RecordDailyEnergyBudget(double a_deb, GooseSpecies a_species)
{
	m_DailyEnergyBudgetStats[a_species].add_variable(a_deb);
}

void Goose_Population_Manager::ClearGooseDailyEnergyBudgetStats() 
{
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_DailyEnergyBudgetStats[(GooseSpecies)i].ClearData();
	}
}

// Daily energy balance stats
void Goose_Population_Manager::RecordDailyEnergyBalance(double a_balance, GooseSpecies a_species)
{
	m_DailyEnergyBalanceStats[a_species].add_variable(a_balance);
}

void Goose_Population_Manager::ClearGooseDailyEnergyBalanceStats() 
{
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_DailyEnergyBalanceStats[(GooseSpecies)i].ClearData();
	}
}

// Forage location stats
void Goose_Population_Manager::RecordIndForageLoc(double a_count, int a_groupsize, GooseSpecies a_species)
{
	for (int i=0; i<a_groupsize; i++) m_IndividualForageLocationData[a_species].add_variable(a_count);
}

void Goose_Population_Manager::ClearIndLocCountStats() {
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_IndividualForageLocationData[(GooseSpecies)i].ClearData();
	}
}

// State stats
void Goose_Population_Manager::RecordState() {
	m_StateStats.add_variable(1);
}

void Goose_Population_Manager::ClearStateStats() {
		m_StateStats.ClearData();
}

void Goose_Population_Manager::RecordLeaveReason(TTypeOfLeaveReason a_leavereason, GooseSpeciesType a_speciestype) {
	m_LeaveReasonStats[a_speciestype][a_leavereason].add_variable(1);
}

void Goose_Population_Manager::ClearGooseLeaveReasonStats() {
	for (unsigned i = 0; i < gst_foobar; i++)
	{
		for (unsigned j = 0; j < tolr_foobar; j++)
		{
			m_LeaveReasonStats[(GooseSpeciesType)i][(TTypeOfLeaveReason)j].ClearData();
		}
	}
}

bool Goose_Population_Manager::InHuntingSeason(int a_day, GooseSpecies a_species) {
	/**
	*  The goose hunting season is complicated because it runs over a year boundary (or it can)
	* Particularly the first year of the simulation is tricky, because we start out in January
	* which can be legal hunting season, but the geese don't arrive until the fall.
	*/
	if (GetSeasonNumber() == 0) {

		return false;  // We don't want them to start in January in the first year of the sim.
	}
	if (a_species == gs_Pinkfoot)
	{
		int pfstart = GooseCfg.cfg_goose_pinkfootopenseasonstart.value();
		int pfend = GooseCfg.cfg_goose_pinkfootopenseasonend.value();
		if (pfstart > pfend) {
			if (a_day < pfstart && a_day > pfend) return false;
			else {
				return true;  // must be in the first year after the start of season
			}
		}
		else {
			// Season all in one year
			if (a_day < pfstart || a_day > pfend) {
				return false;
			}
			else {
				return true;
			}
		}
	}
	else if (a_species == gs_Greylag)
	{
		int glstart = GooseCfg.cfg_goose_greylagopenseasonstart.value();
		int glend = GooseCfg.cfg_goose_greylagopenseasonend.value();
		if (glstart > glend) {
			if (a_day < glstart && a_day > glend) return false;
			else {
				return true;  // must be in the first year after the start of season
			}
		}
		else {
			// Season all in one year
			if (a_day < glstart || a_day > glend) {
				return false;
			}
			else {
				return true;
			}
		}
	}
	else
	{
		g_msg->Warn(WARN_FILE, "Goose_Population_Manager::InHuntingSeason: ""Species error", "");
		exit(1);
	}
}


void Goose_Population_Manager::WriteHeaders(ofstream *a_file, std::vector<std::string> a_headers)
{
	for (int i = 0; i < a_headers.size(); ++i)
	{
		if (i <= a_headers.size() - 2)
		{
			(*a_file) << a_headers[i];
			(*a_file) << '\t';
		}
		else
		{
			(*a_file) << a_headers[i];
			(*a_file) << endl;
		}
	}
}


void Goose_Population_Manager::TheAOROutputProbe() {
	/** 
	* Here the problem is that we can't use the standard outputs because of the special
	* goose population structures. So we need some special adaptions to the probe.
	* This is done by extending the AOR_Probe class and using this, one for each species
	* We also need to do this only once per day, so since the main calling loops use days
	* as triggers we need to return doing nothing if the time is wrong.
	*/
	if (GetDayTime() == GooseCfg.cfg_gooseAORtime.value())
	{
		m_AOR_Pinkfeet->DoProbe(gs_Pinkfoot);
		m_AOR_Barnacles->DoProbe(gs_Barnacle);
		m_AOR_Greylags->DoProbe(gs_Greylag);
	}
}

void Goose_Population_Manager::XYDump() {
	// Families
	int total = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
	int x, y, poly, index, gaflx, gafly, gaflpoly;
	//int w = m_TheLandscape->SupplySimAreaWidth();
	//int h = m_TheLandscape->SupplySimAreaHeight();
	
	for (int j = 0; j<total; j++)      
	{
		int group_size = dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_PinkfootFamilyGroup,j))->GetGroupsize();
		for (int k = 0; k < group_size; k++)
		{

            Population_Manager::SupplyLocXY(gst_PinkfootFamilyGroup,j,x,y);
			poly = m_TheLandscape->SupplyPolyRef(x, y);
			index = dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_PinkfootFamilyGroup,j))->GetForageLocIndex();
			if (index != -1) {
				GooseActiveForageLocation* gafl = GetForageLocation(index);
				gaflpoly = gafl->GetPolygonref();
				gaflx = m_TheLandscape->SupplyCentroidX(gaflpoly);
				gafly = m_TheLandscape->SupplyCentroidY(gaflpoly);
			}
			else {
				gaflpoly = -1;
				gaflx = -1;
				gafly = -1;
			}
			(*m_GooseXYDumpFile) << x << '\t' << y << '\t' << poly << '\t' <<
				gaflx << '\t' << gafly << '\t' << gaflpoly <<  endl;
		}
	}
	// Non-breeders
	total = int(GetLiveArraySize(gst_PinkfootNonBreeder));
	
	for (int j = 0; j<total; j++)
	{
        Population_Manager::SupplyLocXY(gst_PinkfootNonBreeder,j,x,y);

		poly = m_TheLandscape->SupplyPolyRef(x, y);
		index = dynamic_cast<Goose_Base*>(SupplyAnimalPtr(gst_PinkfootNonBreeder,j))->GetForageLocIndex();
		if (index != -1) {
			GooseActiveForageLocation* gafl = GetForageLocation(index);
			gaflpoly = gafl->GetPolygonref();
			gaflx = m_TheLandscape->SupplyCentroidX(gaflpoly);
			gafly = m_TheLandscape->SupplyCentroidY(gaflpoly);
		}
		else {
			gaflpoly = -1;
			gaflx = -1;
			gafly = -1;
		}
		(*m_GooseXYDumpFile) << x << '\t' << y << '\t' << poly << '\t' <<
			gaflx << '\t' << gafly << '\t' << gaflpoly << endl;
	}
}

std::string Goose_Population_Manager::GooseTypeToString(GooseSpeciesType a_gst)
{
	switch (a_gst)
	{
	case gst_PinkfootFamilyGroup:
		return "pinkfoot_family";
	case gst_PinkfootNonBreeder:
		return "pinkfoot_nonbreeder";
	case gst_BarnacleFamilyGroup:
		return "barnacle_family";
	case gst_BarnacleNonBreeder:
		return "barnacle_nonbreeder";
	case gst_GreylagFamilyGroup:
		return "greylag_family";
	case gst_GreylagNonBreeder:
		return "greylag_nonbreeder";
	default:
		return "gst_foobar";
	}
}

std::string Goose_Population_Manager::GooseToString(GooseSpecies a_gs)
{
	switch (a_gs)
	{
	case gs_Pinkfoot:
		return "pinkfoot";
	case gs_Barnacle:
		return "barnacle";
	case gs_Greylag:
		return "greylag";
	default:
		return "gs_foobar";
	}
}

std::string Goose_Population_Manager::IntakeSourceToString(TTypeOfMaxIntakeSource a_intake_source)
{
	switch (a_intake_source)
	{
	case tomis_grass:
		return "grass";
	case tomis_sowncrop:
		return "winter_cereal";
	case tomis_maize:
		return "maize";
	case tomis_grain:
		return "grain";
	default:
		return "tomis_foobar";
	}
}

std::string Goose_Population_Manager::LeaveReasonToString(TTypeOfLeaveReason a_leave_reason)
{
	switch (a_leave_reason)
	{
	case tolr_migration:
		return "migration";
	case tolr_bodycondition:
		return "body_condition";
	case tolr_leanweight:
		return "lean_weight";
	default:
		return "tolr_foobar";
	}
}

