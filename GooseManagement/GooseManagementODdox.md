ALMaSS Goose Management ODdox Documentation                        {#Goose_page}
=========

# The goose management model description following the ODdox protocol {#label-Title}

## 1. PURPOSE {#label-Purpose}

To predict the spatial behaviour and habitat use of pink-footed geese during their over-wintering stop in Denmark.

## 2. STATE VARIABLES AND SCALES {#label-StateVarAndScales}

Model goose populations consist of adults and juveniles, some of which are members of family groups, the others are considered as members of loose flocks of non-breeders. 
The model considers three species of geese: 
1. Pink-footed geese _Anser brachyrhynchus_ 
2. Greylag geese _Anser anser_
3. Barnacle geese _Branta leucopsis_

Each follows the same basic modelling approach differing only in the way the models are parameterised. All individuals are classified into types but are all descended from the base class Goose_Base. Hence Goose_Base has all the functionality common to all geese of all species. Each possible goose type is listed by the enumeration ::GooseSpecies. For each species, two types are possible, family groups or non-breeders. This information is held in the enumeration ::GooseSpeciesType.

The spatial scale considered depends on the maps used as input variables, but ideally would cover all main pinkfooted goose roosting and feeding locations in Denmark. Spatial resolution is the standard ALMaSS resolution of 1m<sup>2</sup>.

### Individual goose attributes {#label-IndivGooseAttr}
The format here describes the type of the variable, a link to the program code declaration and a brief description.

Goose_Base is the class from which all other geese are descended. All attributes and behaviour present in Goose_Base are also part of the descendent classes, although behaviours can be modified such that different types of geese will do the same thing in different ways (e.g. family flocks can have different behaviour than individual nonbreeders).

Goose_Base attributes: 

These variables are static, i.e. considered to be the same value for all Goose_Base descendents:

- static double Goose_Base::m_flightcost - The cost of flight per kg per *m* in kJ.
- static double Goose_Base::m_mingooseforagerate[gs_foobar] - The species specific minimum tolerable forage rate in kJ/minute.
- static double Goose_Base::m_GooseMaxAppetiteScaler - Scaler to determine the max kJ possible to eat per day based on non-energy reserve weight (no units).
- static double Goose_Base::m_GooseMaxEnergyReserveProportion - Max proportion of weight that can be energy reserve. 
- static double Goose_Base::m_GooseKJtoFatConversion - Conversion rate kJ to fat (no units). 
- static double Goose_Base::m_GooseFattoKJConversion - Conversion rate fat to kJ (no units).
- static double Goose_Base::m_GooseMinForageOpenness - The minimum openness value a goose will tolerate for forage (no units, calculated by Landscape::CalculateOpenness).
- static double Goose_Base::m_GooseLeavingThreshold - The trigger for leaving as an 5 day average of body condition. 
- static double Goose_Base::m_GooseForageDist[gs_foobar] - The maximum distance travelled to forage sites for each species of goose (*m*).
- static double Goose_Base::m_GooseForageDistX2[gs_foobar] - The maximum distance travelled to forage sites for each species of goose (*m*) - this is a speed optimisation.
- static int Goose_Base::m_followinglikelyhood[gs_foobar] - An attribute used to determine the chance of following or exploring when foraging. Expressed as m_followinglikelyhood out of 10000 (no units).
- static double Goose_Base::m_flightcost[gs_foobar] - The cost of flight per kg per m in kJ.
- static double Goose_Base::m_SnowDepth - The current snow depth. Held here for speed optimization.
 

These varibles are individually set:

- CurrentGState Goose_Base::CurrentGState - Holds the current goose behavioural state. 
- bool Goose_Base::m_sex - Sex of bird, *true* = male *false* = female. 
- GooseSpecies Goose_Base::m_myspecies - Holds the type of goose species the goose belongs to. 
- GooseSpeciesType Goose_Base::m_myGooseSpeciesType - Holds the type of goose. 
- APoint Goose_Base::m_MyRoost - The x,y location of the current roost. 
- int Goose_Base::m_groupsize - The size of the group this bird represents (families will be > 1, otherwise this will be 1). 
- double Goose_Base::m_weight - The weight of the bird in *g* excluding fat reserve.
- double Goose_Base::m_weightTotal - The weight of the bird in *g* including fat reserve.
- int Goose_Base::m_forageLocCount - The number of forage locations visited today.
- double Goose_Base::m_BMR - The daily BMR.
- double Goose_Base::m_DEB - The daily energy budget excluding flying.
- double Goose_Base::m_LeaveRoostTime - Controls the time when the goose will leave the roost.
- double Goose_Base::m_energyToday - The bird's daily energy account (energy input - energy use).
- bool Goose_Base::m_LeavingRoost - Indicate that we are coming from the roost. Used to control code for "following"
- double Goose_Base::m_energyReserve - The bird's energy reserves in grams.
- double Goose_Base::m_BodyCondition[5] - A variable describing body condition over the past 5 days (no units). This is expressed as (m_weightTotal)/m_weight).
- double Goose_Base::m_TheBodyCondition - A variable describing current body condition (no units). This is expressed as (m_weightTotal)/m_weight).
- unsigned Goose_Base::m_BodyConditionCounter - A counter for the daily body condition.
- TTypeOfMaxIntakeSource Goose_Base::m_MaxIntakeSource - Temporary storage for information about the food source which gives max intake rate on the field.
- double Goose_Base::m_Indivmingooseforagerate - The minimum tolerable forage rate in kJ/minute for the individual.
- int Goose_Base::m_FlightNumber - Counter for the number of flights per day (no units).
- int Goose_Base::m_FlightDistance - Storage for the total distance flown per day (meters).

These varibles are programmatic structures and do not represent biological entities or inputs:

- int Goose_Base::m_myForageIndex - Temporary storage for a forage location index - this is a speed optimisation (no units).
- Goose_Population_Manager*  Goose_Base::m_OurPopulationManager provides programatic access to the controlling population manager.
- int Goose_Base::m_myMemPolyID - Temporary storage for a memory location polygon id (no units).
- GooseMemoryMap Goose_Base::m_MyMemory - The Goose Memory - pointer to access the memory (see GooseMemoryMap below):

Decendent goose classes:
- Goose_Pinkfoot_Base is descended from Goose_Base but has no unique attributes.
- Goose_Pinkfoot_FamilyGroup is descended from Goose_Pinkfoot_Base but has no unique attributes.
- Goose_Pinkfoot_NonBreeder is descended from Goose_Pinkfoot_Base but has no unique attributes.

- Goose_Barnacle_Base is descended from Goose_Base but has no unique attributes.
- Goose_Barnacle_FamilyGroup is descended from Goose_Barnacle_Base but has no unique attributes.
- Goose_Barnacle_NonBreeder is descended from Goose_Barnacle_Base but has no unique attributes.

- Goose_Greylag_Base is descended from Goose_Base but has no unique attributes.
- Goose_Greylag_FamilyGroup is descended from Goose_Greylag_Base but has no unique attributes.
- Goose_Greylag_NonBreeder is descended from Goose_Greylag_Base but has no unique attributes.

Other classes declared in Goose_Base:

The TMaxIntakeSource class is a construct to hold information required to classify goose habitat use. TMaxIntakeSource attributes:
- TTypeOfMaxIntakeSource TMaxIntakeSource::m_maxintakesource - The intake source which gave the maximum intake
- bool TMaxIntakeSource::m_iscereal - Flag for whether the intake source is a cereal
- TTypesOfVegetation TMaxIntakeSource::m_veg - The vegetation type
- bool TMaxIntakeSource::m_instubble - Flag to indicate if the intake source was in stubble
- TTypesOfVegetation TMaxIntakeSource::m_prevsowncrop - The previous sown crop on the forage location

### Goose memory

The GooseMemoryMap class (see @subpage Goose_Memory_page) is a construct representing the memory functions of all geese. The structure and processes are common but information held there is specific to each individual goose. The memory concept is based on locations that the goose has experienced and remembers. Decay rates describe the rate at which the memory of a threat or food resource fades. Memories decay if not refreshed and updated.

Each location is stored in a GooseMemoryLocation structure. GooseMemoryMap attributes:
- double GooseMemoryMap::m_threatdecayrate - The decay rate per day for threats (no units).
- int GooseMemoryMap::m_expectedforagingtime - The time in minutes a goose expects to be able to forage when evaluating cost benefit of flying to a location.
- std::list<GooseMemoryLocation> GooseMemoryMap::m_memorylocations - the storage location for the list of memories. See GooseMemoryLocation below:

GooseMemoryLocation holds the data for a single goose memory. 
GooseMemoryLocation attributes:
- int GooseMemoryLocation::m_age - Age of the memory (days)
- int GooseMemoryLocation::m_x - x-coordinate in the landscape.
- int GooseMemoryLocation::m_y - y-coordinate in the landscape.
- int GooseMemoryLocation::m_polygonid - The unique polygon identification number. 
- double GooseMemoryLocation::m_grain - The grain resource at the location. 
- double GooseMemoryLocation::m_maize - The maize resource at the location. 
- double GooseMemoryLocation::m_grazing - The grazing resource at the location. 
- double GooseMemoryLocation::m_foodresource - The max food resource. 
- double GooseMemoryLocation::m_threat - The threat-level memory strength (no units). 
- double GooseMemoryLocation::m_score A score used to assess the location, created by combining distance, threat and resource scores by GooseMemoryMap::CalcScore.
- static int GooseMemoryLocation::m_infinitememorystop - The minimum memory score remembered(no units).


### Goose population manager

Goose_Population_Manager is descended from the standard Population_Manager class. It manages the lists of geese.
Goose_Population_Manager attributes:
- int Goose_Population_Manager::m_daytime - Holds the time of day (minutes).
- bool Goose_Population_Manager::m_daylight - Flag for in daylight hours.
- int Goose_Population_Manager::m_daylightleft - Number of daylight minutes left. 
- double Goose_Population_Manager::m_thermalcosts[3] - Temporary storage for daily goose energetic thermal costs constant for each species.
- std::vector<int> Goose_Population_Manager::m_youngdist - The observed distribution of young for pinkfeet.
- int Goose_Population_Manager::m_migrationnumbers[3][2] - Storage for goose numbers. Used when birds are immigrating.
- std::vector<roostlist> Goose_Population_Manager::m_roosts - The list of roosts.
- std::vector<GooseActiveForageLocation> Goose_Population_Manager::m_GooseForageLocations - Is the list of active goose forage locations where we have geese at any point in time. 
- PettiforFeedingTimeCurveClass* Goose_Population_Manager::m_ForageRateVSGooseDensity - Speed optimisation to hold all density dependent feeding times (proportion time spent feeding).
- HollingsDiscCurveClass* Goose_Population_Manager::m_IntakeRateVSGrainDensity_PF - Speed optimisation to hold all potential forage rates in the range 0-9999 grain/m<sup>2</sup> (no units ?).
- HollingsDiscCurveClass* Goose_Population_Manager::m_IntakeRateVSMaizeDensity_BN - Speed optimisation to hold all potential forage rates in the range 0-9999 maize/m<sup>2</sup> (no units ?).
- ofstream* Goose_Population_Manager::m_GoosePopDataFile - Pointer to an output file for goose population data.
- ofstream* Goose_Population_Manager::m_GooseEnergeticsDataFile - Pointer to an output file for goose energetics data
- ofstream* Goose_Population_Manager::m_GooseHabitatUseFile - Pointer to an output file for goose habitat use data
- ofstream* Goose_Population_Manager::m_GooseHabitatUseFieldObsFile - Pointer to an output file for goose habitat use data, field observation mimic version
- ofstream* Goose_Population_Manager::m_GooseIndLocCountFile - Pointer to an output file for goose individual forage location count data
- ofstream* Goose_Population_Manager::m_GooseWeightStatsFile - Pointer to an output file for goose weight stats data
- ofstream* Goose_Population_Manager::m_GooseLeaveReasonStatsFile - Pointer to an output file for goose leave reason stats data
- ofstream* Goose_Population_Manager::m_GooseFieldForageDataFile - Pointer to an output file for goose field forage data
- int Goose_Population_Manager::m_HabitatUseStats[gs_foobar * tomis_foobar] - Data for the habitat use
- int Goose_Population_Manager::m_HabitatUseFieldObsStats[gs_foobar * tomis_foobar] - Data for the habitat use, field observation mimic version
- SimpleStatistics Goose_Population_Manager::m_IndividualForageLocationData[gs_foobar] - Statistics for the number of forage locations visited per goose of the population
- SimpleStatistics Goose_Population_Manager::m_WeightStats[gs_foobar] - Statistics for the weights of the population
- SimpleStatistics Goose_Population_Manager::m_ForagingTimeStats[gs_foobar] - Statistics for the time spent foraging for the population
- SimpleStatistics Goose_Population_Manager::m_FlightDistanceStats[gs_foobar] - Statistics for the flight distances in the population
- SimpleStatistics Goose_Population_Manager::m_DailyEnergyBudgetStats[gs_foobar] - Statistics for the daily energy budget in the population
- SimpleStatistics Goose_Population_Manager::m_DailyEnergyBalanceStats[gs_foobar] - Statistics for the daily energy balance in the population
- ofstream* Goose_Population_Manager::m_StateStatsFile - Pointer to an output file for state stats data
- SimpleStatistics Goose_Population_Manager::m_StateStats - Debugging code. Statistics for the number of times a state method is called
- SimpleStatistics Goose_Population_Manager::m_LeaveReasonStats[gst_foobar][tolr_foobar] - Statistics for reasons for leaving the simulation
- AOR_Probe_Goose* Goose_Population_Manager::m_AOR_Pinkfeet - AOR Probe for pinkfeet
- AOR_Probe_Goose* Goose_Population_Manager::m_AOR_Barnacles - AOR Probe for barnacles
- AOR_Probe_Goose* Goose_Population_Manager::m_AOR_Greylags - AOR Probe for greylags
- ofstream* Goose_Population_Manager::m_GooseXYDumpFile -	Pointer to an output file for goose x y data




 
## 3. PROCESS OVERVIEW AND SCHEDULING {#label-ProcessOverAndSchedu}

The basic time-step of the model is 10 minutes. Geese will update their status based on integrating the previous days behaviour, experiences and activities. 

Food resource dynamics are considered per landscape element polygon. This entails inputs from harvesting (crop dependent), background loss rates (assumed
to be degredation, germination and consumption by non-goose species), management input losses e.g. tillage, and goose feeding depletion rates.  
The degredation rate is controlled by the input variables #cfg_goose_GrainDecayRateWinter and #cfg_goose_GrainDecayRateSpring whereas the spilled seed concentration is determined by field observations of spillage (see section [4.G STOCHASTICITY](@ref label-Stochasticity)). 
 
Food dynamics are updated on a daily basis in terms of inputs from spilled grain/maize and resource depletion by non-goose sources. Grain and maize resource depletion by geese is integrated throughout the feeding day (i.e. updated in each time step) since within day food dynamics can affect the goose responses. Growth of grass and cereals is updated daily as is the depletion of this resource by geese (and other animals like cattle).

Perceived predator threat or _openness_ is calculated by Landscape::CalculateOpenness. It follows the observations of [Madsen 1985][Madsen, J. 1985] on disturbance of pink-footed geese in Jutland, Denmark. The idea is that each open space (e.g. a field) gets a score based on the maximum distance to nearest objects which hinders free view for the goose if the goose stands anywhere on the field and rotates 360&deg;. The distance is curtailed by trees or any tall objects, but also roads. For speed optimization the starting point for the openness calculation is incremented in 10 m steps and the direction is incremented 90&deg; giving 4 assessments per point, see Fig. 1.

![](GooseOpenness.png)
*Fig 1. Openness calculation. In this example the shortest distance at point P1 is d, in point P2 b. Between P1 and P2, P2 has got the largest disance (P2-b &gt; P1-d), hence the P2-b is the openness score assigned to Field A. The point in Field B illustrates the fact that distances can span field boundaries and cross water.*

The goose's within season memory operates on the basis that as it explores the environment it learns about the forage quality of different locations and remembers these. These memories decay with time if not reinforced by new visits. 

### Major simulation control is described by the following diagram: {#label-MajorSimControl}
![](GoosePopManControl.jpg)
*Fig 2. Simulation control*

This control is done by Goose_Population_Manager::DoFirst which calls Goose_Population_Manager::DoImmigration and Goose_Population_Manager::DoEmigration to handle the addtion and removal of geese from the DK simulation area.

Timing of major events (here we only give links to the config variables for pinkfoot): 
- **Arrival**: The geese arrive gradually to the area. The timing and duration is for each species determined by input variables for arrive start and end (#cfg_goose_pf_arrivedatestart & #cfg_goose_pf_arrivedateend). Numbers and population composition (i.e. family flock/non-breeder ratio) is also controlled by species specific input variables (#cfg_goose_pf_startnos & #cfg_goose_pf_young_proportion). 
- **Fall migration** (out of the simulation area): At a certain point during the fall birds continue further down the flyway (and out of the area simulated here). Whether birds should do fall migration as well as the timing and duration of this if they should, is controlled by species specific config variables (#cfg_goose_pf_fallmigrate, #cfg_goose_pf_fallmigdatestart & #cfg_goose_pf_fallmigdateend). The rate at which birds leave the simulation during fall migration is controlled by #cfg_goose_pf_fallmig_probability which gives the daily probability for leaving.
- **Spring migration** (into the area): This is a second wave of birds coming into the area which mimicks spring migration. Controlled by species specific config variables (#cfg_goose_pf_springmigrate, #cfg_goose_pf_springmigdatestart & #cfg_goose_pf_springmigdateend). We assume the that the population composition is the same as during arrival.
- **Leaving**: Geese depart gradually from the area at the onset of spring migration (out of the simulation area) or the birds depart when it is no longer energetically sensible to stay. The timing and duration is for each species determined by input variables for leave start and end (#cfg_goose_pf_leavingdatestart & #cfg_goose_pf_leavingdateend). 


![](Goose_STDiagram.png)
*Fig 3. Goose state diagram. Boxes shows possible states, blue arrows possible transitions between states and red arrows indicate external events*

#### GOOSE ENERGETICS {#label-GooseEnergetics}
Goose energetics uses a simple metabolic model. A daily energy budget excluding flight is assumed based on the weight of the bird. Flight costs are then added to this energy budget based on distances moved each day. Food intake results in energy intake up to a daily maximum allowed input (see #cfg_goose_MaxAppetiteScaler). Excess energy is converted to energy reserves up to a maximum energy reserve set by a proportion of body weight (see #cfg_goose_MaxEnergyReserveProportion). Daily energy deficits are taken from the energy reserves, and if these fall below a fixed proportion of the body weight the goose will leave the area for better foraging (see #cfg_goose_LeavingThreshold).

We use the same principles for energetics calculations for all three species considered in the model. When available we use species specific parameters in these  equations, but will in the absence of species specific information scale proportional to body weight. 

### General Energetic Calculations {#label-GeneralEnergeticCalc}

#### Flight {#label-Flight}

We calculate flight costs based on the body weight of the individual goose and its BMR. Specifically we calculate flight cost as 12 times BMR divided by 24 hours to get the hourly cost of flight. We divide this by an average flight speed of 57.6 km/hour (measured on migrating brent geese: [Green & Alerstam 2000]). This yields a cost in kJ/km which we further divide by 1000 to get cost per meter, and then by 1000 to get the cost per  gram. This yields the following a flight cost of 0.000002079 kJ per gram per meter.

#### BMR {#label-BMR}
Goose_Base::m_BMR in kJ per day is calculated by: m_BMR = 4.19 x 78.3 x m_weightTotal<sup>0.72</sup>, where Goose_Base::m_weightTotal is the attribute that holds the weight of the goose. Here 4.19 x 78.3 is held in #cfg_goose_BMRconstant1 and the exponent in #cfg_goose_BMRconstant2.

#### Thermal costs {#label-ThermalCosts}

Lower critical temperatuer (LCT) is based on the allometric relationship from Kendeigh et al. (1977). Again we use species-specific approach, the LCT (based on body masses of barnacle 1880, pinkfoot 2524 and 3454 g respectively) will be:
- Barnacle: 12.1 &deg;C
- Pinkfoot: 11.1 &deg;C
- Greylag: 10.8 &deg;C

When ambient temperatures fall below LCT, thermal costs add to the daily energy budget (DEB). Thermal costs are calculated by a linear function of the difference between the actual temperature and the lower critical temperature (#cfg_goose_pf_Thermalconstant for pinkfooted goose) multiplied by a constant (#cfg_goose_Thermalconstantb in kJ/day), see e.g. [Clausen et al. 2012]. So for pink-footed geese we have:
`cfg_goose_Thermalconstantb * (cfg_goose_Thermalconstant_pf - temp)` (see Goose_Population_Manager::DoFirst())
	
The #cfg_goose_Thermalconstantb is set at 1.272 kJ/day per hour per degree below LCT [Lefebvre & Raveling 1967].

#### Daily Energy Budget (DEB) {#label-DEB}
For each goose we calculate a daily energy budget based upon its sum of BMR and thermal costs. The daily energy budget for the goose is held in the attribute 
Goose_Base::m_DEB as total kJ required. Other activity costs (flying) are added to this budget as they accrue throughout the day.
At the end of each day the total forage intake is used to calculate the net energy gain or loss for the day. This is simply by subtraction of m_DEB from the daily 
energy intake which is saved recorded in Goose_Base::m_energyToday.
For other activity costs we use a multiplier of the BMR. Enery expenditure in multiples of BMR for the daytime period spent on foraging sites is 1.65 based on [Madsen 1985][Madsen, J. 1985b] (see #cfg_goose_daytime_BMR_multiplier).
[Therkildsen & Madsen 2000b] assumes that night time roosting equals an energy expenditure of 1.3 x BMR (see #cfg_goose_nighttime_BMR_multiplier)
Hence, values of 1.65 and 1.3 BMR  are used and the proportion of time spent on the roost is assumed to be the proportion of the day when it is dark. See Goose_Base::StartDay

#### Energy Reserves Calculation {#label-EnergyReserveCalc}
After m_DEB is subtracted from Goose_Base::m_energyToday, if the result is positive then we have a net gain in energy. This is translated into fat and added to the fat 
stores held in Goose_Base::m_energyReserve, if negative then the energy reserves are reduced by the equivalent kJ fat required to produce the kJ of deficit. 
Calculations to and from kJ and fat cost energy, thus the fat reserve energy is less than the kJ raw energy used to create them. 
Likewise the energy obtained from mobilizing fat is less than its pure calorific value. Both translation rates between kJ and fat, and fat and kJ are 
determined by the input variables #cfg_goose_EnergyContentOfFat and #cfg_goose_MetabolicConversionCosts. The corresponding values are stored in Goose_Base::m_GooseKJtoFatConversion and Goose_Base::m_GooseFattoKJConversion 
The energy reserve calculations are done at the start of every new day before any activity occurs, in Goose_Base::StartDay().

### FORAGING {#label-Foraging}
Foraging is the most important and complicated part of the energetics. It can be separated into two main parts: the first is finding and moving to a forage site, the second the forage intake rate calculations.

#### Forage Site Location {#label-ForageSiteLoc}
The geese will move to a forage site that is remembered as a good one, a new one found when exploring, or one found by following another goose. Flight costs involved with all movements from the roost to forage locations are calculated as they accrue during the day as costs per m travelled. Exploration flight costs are also added to the m_DEB for the day. Choices made by the geese in determining whether to return to a forage location, seek a new one or follow are made in #Goose_Base::st_ChooseForageLocation and controlled primarily by the goose's memory, i.e. it's knowledge of the landscape, and a probability of following another goose.

#### Forage Intake {#label-ForageIntake}
Forage intake by the geese is dependent upon a number of factors. The amount of forage available as grain, maize or grazing and the species specific functional response to these resources, the number of geese present and the amount of snow in the landscape. Below we provide more details on the implemenation.

- When the birds are feeding on grain then the functional reponse is based on [Amano et al. (2004)][Amano et al. 2004] (converted to kJ from g/dw by multiplying by 17.67) with assumption that white-fronted geese _Anser albifrons_ and pinkfooted geese have a comparable response and that the energy content of grain vs. rice follows the weight difference (grain weighs app. the double of rice). For greylags and barnacle geese the response is scaled based on the relationship between body mass and intake rate from [Durant et al. (2003)][Durant et al. 2003]. For all three species the digestibility is assumed to be 0.695. 

The amount of grain present on a field depends on the time since harvest - with a decay rate assumed to represent loss to other organisms and spoiling (the daily decay rate is controlled by the configs cfg_goose_GrainDecayRateWinter and cfg_goose_GrainDecayRateSpring in Elements.cpp). The amount present on the day after harvest is randomly drawn from a distribution obtained from field data from 2013 & 2014 from cereal fields in Jutland, Dk (see FarmManager::GetSpilledGrain()). The amount of forage available to a goose depends on the density of grains (expressed as grains per m<sup>2</sup>) and on the species specific response (Fig. 3). 

- The situation when feeding on maize is similar to grain, but here the functional response is based on field observations of barnacle geese made with wildlife cameras in Jutland, Dk in 2015. The digestibility of maize is 0.87 (Value found for Bewick's swans from Nolet, B. unpubl). Again we scale to the two remaining species using the relationship between body mass and intake rate from [Durant et al. (2003)][Durant et al. 2003]. The amount present on the day after harvest is randomly drawn from a distribution obtained from field data from 2015, 2016 & 2017 see [Clausen et al. 2018][Clausen et al. 2018] ![Fig. 3. Functional response for geese on grain and maize](maize-grain-intake_2018-07-04.png)
*Fig. 3. Functional response for geese on maize and grain*	


- The situation with grazing differs from grain and maize.
	This is primarily dependent upon the height of the vegetation and we make an assumption that there is no lower limit of food quality, but that food quality is constant at 19.8 kJ per g dry weight of plant material. Figure 4 shows the kJ obtained per minute as a function of grazing height and is for pinkfeet based on [Therkildsen & Madsen 2000a], and for barnacle and greylag a parabolic relationship fitted to the data from [Durant et al. (2003)][Durant et al. 2003]. This function returns a measure of grazing intake rate (grazingKJ).  	
![](grass-intake_2018-07-04.png)
*Fig. 4. Functional response for geese on grass*

- The actual amount of forage obtained also depends on goose density. The time spent foraging depends on the flock size at the foraging location. The intake rate is a capped-linear relationship following [Pettifor et al. (2000)][Pettifor et al. 2000] (Fig. 5). The result of this function can be called the density adjusted feeding rate (densFR) ![](feeding-time_2018-07-04.png)
*Fig. 5. Functional response for density of geese*
- Snow cover reduces the intake by 5% per 1 cm of snow (see #cfg_goose_snow_scaler). 

The amount forage obtained in a 10 minute timestep of the model is therefore given by:
`kJ = intake per min * 10 * densFR * (1 - snowdepth*0.05)` where  snowdepth is measured in cm.

#### Limits to forage intake {#label-LimitForageIntake}
The individual with the higest intake in [Therkildsen & Madsen (2000a)][Therkildsen & Madsen 2000a] had 404 g AFDW assuming an energy contet of 19.8 kJ/g and assimilation of 0.404 this gives = 3232 kJ. This can be scaled relative to BMR of 670.75 kJ for a 2.7 kg goose to give a scaling against BMR of 4.8x (4.82). In the absence of other data we have used this scaling factor for barnacles and greylags. This value forms the default for the configuration variable #cfg_goose_MaxAppetiteScaler

Lower limits to foraging are defined as the intake rate that will allow the geese to obtain positive energy balance (value stored in Goose_Base::m_mingooseforagerate). This value changes with the daylength and temperature. However, geese will not continue to search for forage locations that will meet their minimum forage rate. Instead their minimum acceptable forage rate is reduced with each unsuccessful attempt to find food. Once they have found food, the minimum forage rate is reset. This is controlled on the individual level and is controlled in Goose_Base::m_Indivmingooseforagerate.

## 4. DESIGN CONCEPTS {#label-DesignConcepts}

###  4.A EMERGENCE {#label-Emergence}
The pattern of goose usage of the landscape in time and space is an emergent property of the goose management model. 

1. Geese aggregate in flocks as a consequence of their foraging decisions as well as their likelyhood for following other geese, thus the flock size distribution is an emergent property. 
2. The spatial distribution of geese in the landscape is determined by food availability in the landscape and foraging decisions by the geese and is therefore also an emergent property.
3. Similarly, the distances from the roosts to the locations where geese are foraging are a consequense of foraging decisions and the efficiency with which the geese explore the landscape surrounding the roosts.
4. The foraging habitat selection of the geese is determined by the energetics related to the amount and type of forage (see the section [GOOSE ENERGETICS](@ref label-GooseEnergetics)), which in turn is an emergent property of the dynamic landscape. The distance to the foraging locations also affect the habitat selection of the geese by favoring the locations that are closer (the geese are minimizing energy expenditure).
5. The time spent foraging depends on the available food resources, the flock sizes and the weather (snow cover reduces the intake). 
6. Upon initiation the geese have species specific, but equal weight. As the simulation runs the energetics determine the weight development for the indervidual. The distribution of weights within the population is an emergent property.

###  4.B ADAPTATION {#label-Adaptation}
The geese will adopt a foraging strategy which will adapt to the environmental conditions created by food resource distribution and competition both within and between species.

###  4.C FITNESS {#label-Fitness}
Fitness is measured in body condition of the goose, but has no consequences for mortality. Geese will leave the simulation when experiencing prolonged decline in body condition.

###  4.D PREDICTION {#label-Prediction}
Each individual will over time build up a memory map of previously visited forage locations. This allows the individual to make predictions about conditions at a number of locations and act on these predictions.

###  4.E SENSING {#label-Sensing}
Sensing happens in several different ways. Geese sense:
- The food resources present at locations they visit.
- The density of other geese (both conspecifics and other species) at locations they visit.
- Preceived predator threat (via the openness score).
- Weather conditions (temperature and snow cover).

###  4.F INTERACTION {#label-Interaction}
There are two modes of goose-goose interaction:
- When leaving the roost and during within day movements, the individual goose may choose to follow other geese.
- When foraging the density of geese on the same forage location affects the intake rate for the individual.

### 4.G STOCHASTICITY {#label-Stochasticity}
Stochasticity is used extensively in the model. Primarily to make foraging decisions based on weighted probabilities, but also for events not directly modelled mechanistically, e.g. crop management.
- Emmigration events not triggered by prolonged decline in body condition are stochastic. 
- The birds do not leave their roost all at once, but are staggered around a mean leave time which is set by the input variable #cfg_goose_RoostLeaveDistMean, the variation around the mean leave time is determined by the input #cfg_goose_RoostLeaveDistSD. From this distribution each individual is stochastically assigned a value daily that gives the bird is leave time.
- When a bird leave the roost in the morning it will make a decisions based on a probability whether to follow another bird or go exploring on its own. Similarly during day a bird makes decisions based on probabilities whether to go foraging locations where other birds are foraging, or whether to set off on it own.
- Distribution of spilled grain and maize is determined by random draw from distributions of spillage collected in the field in Jutland, Denmark in 2013 & 2014.
- When exploring the landscape a goose will randomly pick the direction and distance in which to explore.

###  4.H COLLECTIVES {#label-Collectives}
Goose family groups and non-breeding flocks are collectives used in the model. 

###  4.I OBSERVATION {#label-Observation}
- Flock sizes: Species specific flock sizes are recorded daily in the ouput file GooseFieldForageData.txt. From these flock size distribution are derived.
- Habitat selection: All vegetated polygons have a type and the types of vegetation and their state constitute a goose potential goose habitat. The habitat type is recorded in the output file GooseHabitatUseFieldObsStats.txt.
- Distance from roost to forage location: The polygons where geese forage are recorded in the output file GooseFieldForageData.txt. From these the distance to the nearest roost is calculated for each polygon/species combination (i.e. nearest roost is not neccessarily the some for all species). 
- Weights: Individual weights are recorded daily in GooseEnergeticsData.txt.
- Forage locations: The number of forage locations that an individual goose uses per day is stored in GooseIndLocCountStats.txt.
- Reasons for leaving: When a goose leaves the simulation (e.g. due to poor body condition) the reason for leaving is stored in GooseLeaveReasonStats.txt.
- Energetics: The daily energy budget and balance in addition to the standard errors are stored in the file GooseEnergeticsData.txt
- Daily flight distance: The mean daily flight distance and standard error is stored in the file GooseEnergeticsData.txt
- Foraging time: The mean daily time spent foraging and standard error is stored in the file GooseEnergeticsData.txt

## 5. INITIALISATION {#label-Initialisation}
A year of landscape simulation is necessary before populating the model with geese. Initial starting points are the current estimated numbers of geese arriving at the simulated location.

## 6. INPUTS {#label-Inputs}
We require maps for the area to be simulated and weather as well as the parameters described below. Maps and weather are standard inputs.

**Input parameters:**
- From GooseMemoryMap.cpp
	- GOOSE_MEM_THREATDECAYRATE #cfg_goose_mem_threatdecayrate The rate of daily decay of threat memories. Default 0.1
	- GOOSE_MEM_MINMEMVALUE #cfg_goose_mem_minmemoryvalue The number of days a memory is kept. Default 3
	- GOOSE_MEM_EXPECTEDFORAGINGTIME #cfg_goose_mem_expectedforagingtime The expected foraging time (minutes) when evaluating cost benefit of flying to a location. Default 225

- From Goose_Population_Manager.cpp
	- GOOSE_FOLLOWINGLIKELYHOOD_BN #cfg_goose_bn_followinglikelyhood Probability of following another goose to a forage location (9882 of 10000).
	- GOOSE_FOLLOWINGLIKELYHOOD_PF #cfg_goose_pf_followinglikelyhood Probability of following another goose to a forage location (9970 of 10000).
	- GOOSE_FOLLOWINGLIKELYHOOD_GL #cfg_goose_gl_followinglikelyhood Probability of following another goose to a forage location (6000 of 10000).
	- GOOSE_MAXAPPETITESCALER #cfg_goose_MaxAppetiteScaler The maximum daily intake as a scaling of DEB without flying (2.94). 
	- GOOSE_FLIGHTCOST #cfg_goose_flightcost The cost of flight per gram per m (0.000002079 kJ). 
	- GOOSE_PINKFOOTWEIGHT #cfg_goose_pf_baseweight Lean weight of pinkfoot geese (2307 g).
	- GOOSE_BARNACLEWEIGHT #cfg_goose_bn_baseweight Lean weight of barnacle geese (1708 g).
	- GOOSE_GREYLAGWEIGHT #cfg_goose_gl_baseweight Lean weight of greylag geese (2795 g).
	- GOOSE_MAXENERGYRESERVEPROPORTION #cfg_goose_MaxEnergyReserveProportion The maximum proportion by weight that can to be stored as energy reserves (0.2).
	- GOOSE_METABOLICCONVCOSTS #cfg_goose_MetabolicConversionCosts The metabolic costs of converting tissue to energy or vice versa. Assumed 30% efficiency after Sibly & Calow (1986 p. 54-55) (11.4 kJ/g).
	- GOOSE_MINFORAGEOPENNESS #cfg_goose_MinForageOpenness The minimum openness score that a goose will tolerate for foraging (70).
	- GOOSE_MAXSCAREDIST #cfg_goose_MaxScareDistance The maximum distance in m that a goose can hear a scary bang. From Jensen, G unpubl. (500).
	- GOOSE_POPULATIONDESCRIPTIONON #cfg_goose_PopulationDescriptionON Control for whether to print goose population stats (true).
	- GOOSE_LEAVINGTHRESHOLD #cfg_goose_LeavingThreshold The 5-day mean of body condition which triggers the bird to leave  value (1.0).
	- GOOSE_PF_STARTNOS #cfg_goose_pf_startnos Pink-footed goose start numbers (13440).
	- GOOSE_PF_ARRIVEDATESTART #cfg_goose_pf_arrivedatestart The initial arrival day for pink-foots (258).
	- GOOSE_PF_ARRIVEDATEEND #cfg_goose_pf_arrivedateend The last date for arrival for pink-foots (273).
	- GOOSE_PF_LEAVINGDATESTART #cfg_goose_pf_leavingdatestart The initial departure date for pink-foots (78).
	- GOOSE_PF_LEAVINGDATEEND #cfg_goose_pf_leavingdateend The last date of departure for pink-foots (104).
	- GOOSE_STARTNO_SCALER #cfg_goose_startno_scaler Scaler for easy adjustment of goose input numbers.

	- GOOSE_PF_SPRING_MIGRATE #cfg_goose_pf_springmigrate Should pinkfeet spring migrate? (true).
	- GOOSE_PF_SPRING_MIG_NOS #cfg_goose_pf_springmignos Number of birds spring migrating (5600).
	- GOOSE_PF_SPRING_MIG_START #cfg_goose_pf_springmigdatestart Onset of spring migration (7).
	- GOOSE_PF_SPRING_MIG_END #cfg_goose_pf_springmigdateend End of spring migration (59).
	- GOOSE_PF_FALL_MIGRATE #cfg_goose_pf_fallmigrate Should pinkfeet fall migrate (true).
	- GOOSE_PF_FALL_MIGRATION_START #cfg_goose_pf_fallmigdatestart Onset of fall migration (274). 
	- GOOSE_PF_FALL_MIGRATION_END #cfg_goose_pf_fallmigdateend End of fall migration (304). 
	- GOOSE_PF_FALLMIG_PROBABILITY #cfg_goose_pf_fallmig_probability Probability of a goose fall migrating (0.03).

	- GOOSE_BN_SPRING_MIGRATE #cfg_goose_bn_springmigrate Should barnacles spring migrate? (true).
	- GOOSE_BN_SPRING_MIG_NOS #cfg_goose_bn_springmignos Number of birds spring migrating (8960).
	- GOOSE_BN_SPRING_MIG_START #cfg_goose_bn_springmigdatestart Onset of spring migration (15).
	- GOOSE_BN_SPRING_MIG_END #cfg_goose_bn_springmigdateend End of spring migration (75).
	- GOOSE_BN_FALL_MIGRATE #cfg_goose_bn_fallmigrate Should barnacles fall migrate  (true).
	- GOOSE_BN_FALL_MIGRATION_START #cfg_goose_bn_fallmigdatestart Onset of fall migration (348).
	- GOOSE_BN_FALL_MIGRATION_END #cfg_goose_bn_fallmigdateend End of fall migration (318).
	- GOOSE_BN_FALLMIG_PROBABILITY #cfg_goose_bn_fallmig_probability Probability of a goose fall migrating (0.0083).

	- GOOSE_GL_SPRING_MIGRATE #cfg_goose_gl_springmigrate Should greylags spring migrate? (true).
	- GOOSE_GL_SPRING_MIG_NOS #cfg_goose_gl_springmignos Number of birds spring migrating (2240).
	- GOOSE_GL_SPRING_MIG_START #cfg_goose_gl_springmigdatestart Onset of spring migration (15).
	- GOOSE_GL_SPRING_MIG_END #cfg_goose_gl_springmigdateend End of spring migration (75).
	- GOOSE_GL_FALL_MIGRATE #cfg_goose_gl_fallmigrate Should greylags fall migrate (true).
	- GOOSE_GL_FALL_MIGRATION_START #cfg_goose_gl_fallmigdatestart Onset of fall migration (232).
	- GOOSE_GL_FALL_MIGRATION_END #cfg_goose_gl_fallmigdateend End of fall migration (334).
	- GOOSE_GL_FALLMIG_PROBABILITY #cfg_goose_gl_fallmig_probability Probability of a goose fall migrating (0.0083).
	
	- GOOSE_BN_STARTNOS #cfg_goose_bn_startnos Barnacle goose start numbers (5600).
	- GOOSE_BN_ARRIVEDATESTART #cfg_goose_bn_arrivedatestart The initial arrival day for barnacles (277).
	- GOOSE_BN_ARRIVEDATEEND #cfg_goose_bn_arrivedateend The last date for arrival for barnacles (298).
	- GOOSE_BN_LEAVINGDATESTART #cfg_goose_bn_leavingdatestart The initial departure date for barnacles (90).
	- GOOSE_BN_LEAVINGDATEEND #cfg_goose_bn_leavingdateend The last date of departure for barnacles (134).
	
	- GOOSE_GL_STARTNOS #cfg_goose_gl_startnos Greylag goose start numbers (8960).
	- GOOSE_GL_ARRIVEDATESTART #cfg_goose_gl_arrivedatestart The initial arrival day for greylags (212).
	- GOOSE_GL_ARRIVEDATEEND #cfg_goose_gl_arrivedateend The last date for arrival for greylags. (231).
	- GOOSE_GL_LEAVINGDATESTART #cfg_goose_gl_leavingdatestart The initial depature date for greylags (76).
	- GOOSE_GL_LEAVINGDATEEND #cfg_goose_gl_leavingdateend The last date of departure for greylags (86).

	- GOOSE_FORAGEDIST_PF #cfg_goose_pf_ForageDist The maximum forage range in m for pink-footed geese (35000).
	- GOOSE_FORAGEDIST_BN #cfg_goose_bn_ForageDist The maximum forage range in m for barnacle geese (35000).
	- GOOSE_FORAGEDIST_GL #cfg_goose_gl_ForageDist The maximum forage range in m for greylag geese (35000).
	- GOOSE_GRAINDECAYRATE #cfg_goose_GrainDecayRateWinter The decay rate for spilled grain from harvest to spring (0.9985). 
	- GOOSE_GRAINDECAYRATE #cfg_goose_GrainDecayRateSpring The decay rate for spilled grain from spring until 1st July (0.95).
	- GOOSE_MINFORAGEDECAYRATE #cfg_goose_MinForageRateDecayRate The decay rate for the minimum acceptable forage rate (0.72).
	- GOOSE_TIMEDCOUNTS #cfg_goose_TimedCounts Time at which the geese are counted on fields. Daylight hours divided by this cfg (2).
	- GOOSE_RUNTIMEREPORTING #cfg_goose_runtime_reporting Should we use a stripped down reporting to optimize run time? (true).

- Curve parameters for the CurveClasses (from Goose_Population_Manager.cpp and Landscape.cpp)
	- The P1 curve replicates a curve fitted by [Therkildsen & Madsen 2000a] and the P2 & P3 curves are fitted based on [Durant et al. 2003]. Y-values are kJ per minute, x-values are grass height. The formula is a simple 2nd order polynomial which is the then scaled to get to kJ.  See Landscape::m_GooseIntakeRateVSVegetationHeight_PF, Landscape::m_GooseIntakeRateVSVegetationHeight_GL & Landscape::m_GooseIntakeRateVSVegetationHeight_BN
		- 	POLYNOMIALTWO_ONE_A #cfg_P1A	Coefficient A in a second order polynomial function
		- 	POLYNOMIALTWO_ONE_B #cfg_P1B	Coefficient B in a second order polynomial function 
		- 	POLYNOMIALTWO_ONE_C #cfg_P1C	The constant C in a second order polynomial function 
		- 	POLYNOMIALTWO_ONE_D #cfg_P1D	Scaler for assimilation of energy from grass for pinkfeet
		- 	POLYNOMIALTWO_ONE_E #cfg_P1E	Should we reverse axis values?
		- 	POLYNOMIALTWO_ONE_F #cfg_P1F	Maximum x-value. At this point the curve tends to 0, must stop here to avoid negative values.
		- 	POLYNOMIALTWO_ONE_G #cfg_P1G	Minimum x-value. 
		- 	POLYNOMIALTWO_ONE_H #cfg_P1H	The name of the curve and its output file name. 
		
		-  POLYNOMIALTWO_TWO_A #cfg_G6A		Coefficient A in a second order polynomial function
		-  POLYNOMIALTWO_TWO_B #cfg_G6B		Coefficient B in a second order polynomial function
		-  POLYNOMIALTWO_TWO_C #cfg_G6C		The constant C in a second order polynomial function
		-  POLYNOMIALTWO_TWO_D #cfg_G6D		Scaler for assimilation of energy from grass
		-  POLYNOMIALTWO_TWO_E #cfg_G6E		Should we reverse axis values? 
		-  POLYNOMIALTWO_TWO_F #cfg_G6F		Maximum x-value. At this point the curve tends to 0, must stop here to avoid negative values.
		-  POLYNOMIALTWO_TWO_G #cfg_G6G		Minimum x-value.
		-  POLYNOMIALTWO_TWO_H #cfg_G6H		The name of the curve and its output file name.

		-  POLYNOMIALTWO_THREE_A #cfg_B6A	Coefficient A in a second order polynomial function
		-  POLYNOMIALTWO_THREE_B #cfg_B6B	Coefficient B in a second order polynomial function 
		-  POLYNOMIALTWO_THREE_C #cfg_B6C	The constant C in a second order polynomial function 
		-  POLYNOMIALTWO_THREE_D #cfg_B6D	Scaler for assimilation of energy from grass 
		-  POLYNOMIALTWO_THREE_E #cfg_B6E	Should we reverse axis values?
		-  POLYNOMIALTWO_THREE_F #cfg_B6F	Maximum x-value. At this point the curve tends to 0, must stop here to avoid negative values.
		-  POLYNOMIALTWO_THREE_B #cfg_B6G	Minimum x-value. 
		-  POLYNOMIALTWO_THREE_H #cfg_B6H	The name of the curve and its output file name. 
	- The H1 curve replicates a curve fitted by [Amano et al. 2004] and the H2 curve is based on [Clausen et al. 2018]. Y-values are kJ per minute, x-values are either densities, grain in grain per m<sup>2</sup> and maize in kJ/m<sup>2</sup>. The formula is a Hollings disc curve. See Goose_Population_Manager::m_IntakeRateVSGrainDensity_PF and Goose_Population_Manager::m_IntakeRateVSMaizeDensity_BN
		- HOLLINGS_ONE_A #cfg_H1A	Attack rate in a type II functional response curve (Hollings disc equation) 
		- HOLLINGS_ONE_B #cfg_H1B	Handling time in a type II functional response curve (Hollings disc equation) 
		- HOLLINGS_ONE_C #cfg_H1C	Should we reverse axis values?
		- HOLLINGS_ONE_D #cfg_H1D	Maximum x-value. At this point the curve tends to 0, must stop here to avoid negative values.
		- HOLLINGS_ONE_E #cfg_H1E	Minimum x-value. 
		- HOLLINGS_ONE_G #cfg_H1F	The name of the curve and its output file name. 

		- HOLLINGS_TWO_A #cfg_H2A	Attack rate in a type II functional response curve (Hollings disc equation)
		- HOLLINGS_TWO_B #cfg_H2B	Handling time in a type II functional response curve (Hollings disc equation) 
		- HOLLINGS_TWO_C #cfg_H2C	Should we reverse axis values?
		- HOLLINGS_TWO_D #cfg_H2D	Maximum x-value. At this point the curve tends to 0, must stop here to avoid negative values.
		- HOLLINGS_TWO_E #cfg_H2E	Minimum x-value. 
		- HOLLINGS_TWO_G #cfg_H2F	The name of the curve and its output file name. 


## 7. INTERCONNECTIONS {#label-Interconnections}
The goose management model depends on the Weather and Landscape classes. The landscape class provides details of the farming activity, preceived threat level (openness) and food resource. The latter two being extensions to be added to the landscape for this project. The weather class provides information on temperatures which has consequnces for metabolic rates and snow depths which can prevent goose foraging. 
The Goose_Population_Manager is descended from Population_Manager and performs the role of an auditor in the simulation. Many of the functions and behaviours needed to execute and
ALMaSS model are maintained by the Population_Manager.

## 8. REFERENCES {#label-References}
Amano, T., K. Ushiyama, G. Fujita and H. Higuchi (2004). Alleviating grazing damage by white-fronted geese: an optimal foraging approach. Journal of Applied Ecology 41(4): 675-688.

Clausen, K. K., P. Clausen, C. C. Fælled and K. N. Mouritsen (2012). Energetic consequences of a major change in habitat use: endangered Brent Geese Branta bernicla hrota losing their main food resource. Ibis 154(4): 803-814.

Clausen, K. K., Madsen, J., Nolet, B. A. and Haugaard, L. (2018). Maize stubble as foraging habitat for wintering geese and swans in northern Europe. Agriculture, Ecosystems & Environment 259: 72-76.

Durant, D., et al. (2003). The functional response in three species of herbivorous Anatidae: effects of sward height, body mass and bill size. Journal of Animal Ecology 72(2): 220-231.

Duriez, O., et al. 2009. What decision rules might pink-footed geese use to depart on migration? An individual-based model. Behavioral Ecology 20(3): 560-569.

Madsen, J. (1985). Impact of Disturbance on Field Utilization of Pink-Footed Geese in West Jutland, Denmark. Biological Conservation 33(1): 53-63.

Madsen, J. (1985). Relations between Change in Spring Habitat Selection and Daily Energetics of Pink-Footed Geese Anser brachyrhynchus

Kendeigh, S. C., et al. (1977). Avian energetics. Granivorous birds in ecosystems. J. Pinowski and S. C. Kendeigh. Cambridge, UK, Cambridge University Press: 127-204.

Lefebvre, E. A. and D. G. Raveling (1967). Distribution of Canada Geese in Winter as Related to Heat Loss at Varying Environmental Temperatures. Journal of Wildlife Management 31(3): 538-546

Sibly, R. M. and P. Calow (1986). Physiological Ecology of Animals. Oxford, Blackwell Scientific Publications. pp 54-55.

Therkildsen & Madsen 2000a: Assessment of food intake rates in pinkfooted geese Anser brachyrhynchus based on examination of oesophagus contents. - Wildlife Biology 6: 167-172.

Therkildsen & Madsen 2000b: Energetics of feeding on winter wheat versus pasture grasses: a window of opportunity for winter range expansion in the pink-footed goose Anser brachyrhynchus. Wildlife Biology 6, 65-74.

[Amano et al. 2004]:http://onlinelibrary.wiley.com/doi/10.1111/j.0021-8901.2004.00923.x/full "Amano et al. 2004"
[Clausen et al. 2012]:http://onlinelibrary.wiley.com/doi/10.1111/j.1474-919X.2012.01265.x/full "Clausen et al. 2012"
[Clausen et al. 2018]:https://www.sciencedirect.com/science/article/pii/S0167880918301117 "Clausen et al. 2018"
[Durant et al. 2003]:http://onlinelibrary.wiley.com/doi/10.1046/j.1365-2656.2003.00689.x/full "Durant et al. 2003"
[Duriez, O. et al. 2009]:http://beheco.oxfordjournals.org/content/early/2009/03/17/beheco.arp032.abstract "Duriez, O. et al. 2009"
[Madsen, J. 1985]: http://www.sciencedirect.com/science/article/pii/0006320785900047        "Madsen 1985"
[Madsen, J. 1985b]: https://www.researchgate.net/publication/271877716_Relations_between_Change_in_Spring_Habitat_Selection_and_Daily_Energetics_of_Pink-Footed_Geese_Anser_brachyrhynchus        "Madsen 1985b"
[Madsen & Klassen 2006]: http://onlinelibrary.wiley.com/doi/10.1111/j.2006.0908-8857.03555.x/full        "Madsen & Klassen 2006"
[Green & Alerstam 2000]:  http://onlinelibrary.wiley.com/doi/10.1034/j.1600-048X.2000.310213.x/pdf  "Green & Alerstam 2000"
[Lefebvre & Raveling 1967]:  http://www.jstor.org/stable/3798137?seq=1#page_scan_tab_contents  "Lefebvre & Raveling 1967"
[Pettifor et al. 2000]:  https://besjournals.onlinelibrary.wiley.com/doi/abs/10.1046/j.1365-2664.2000.00536.x  "Pettifor et al. 2000"
[Therkildsen & Madsen 2000a]:    https://www.researchgate.net/profile/Ole_Therkildsen/publication/237748799_Assessment_of_food_intake_rates_in_pink-footed_geese_Anser_brachyrhynchus_based_on_examination_of_oesophagus_contents/links/53e9c1bc0cf28f342f414640.pdf    "Therkildsen & Madsen 2000a"
[Therkildsen & Madsen 2000b]: http://eurekamag.com/research/003/432/003432746.php "Therkildsen & Madsen 2000b"
