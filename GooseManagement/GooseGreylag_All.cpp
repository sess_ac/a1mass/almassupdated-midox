/* 
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>GreylagGoose_All.cpp This is the c++ code file for the greylag goose classes</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of 8th February 2013 \n
*/

//#include <iostream>
//#include <fstream>
#include <list>
//#include <vector>
#include <string>
#include "../Landscape/ls.h"
//#include "../BatchALMaSS/PopulationManager.h"
#include "GooseMemoryMap.h"
#include "Goose_Base.h"
//#include "../BatchALMaSS/CurveClasses.h"
#include "../People/Hunters/Hunters_all.h"
#include "Goose_Population_Manager.h"
#include "GooseGreylag_All.h"

/** \brief This pointer provides access the to the internal ALMaSS error message system */
//extern MapErrorMsg *g_msg;

using namespace std;

/** \brief To calculate daily energy budget. This is the multiple of BMR spent during daytime */
//extern CfgFloat cfg_goose_daytime_BMR_multiplier;
/** \brief To calculate daily energy budget. This is the multiple of BMR spent during nighttime */
//extern CfgFloat cfg_goose_nighttime_BMR_multiplier;
/** \brief Chance of changing roost */
//extern CfgFloat cfg_goose_roostchangechance;

//*******************************************************************************************************
//**********************************Goose_Greylag_Base**************************************************
//*******************************************************************************************************

Goose_Greylag_Base::Goose_Greylag_Base(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) : Goose_Base(p_L, p_NPM, a_weight, a_sex, a_roost)
{
	Init(p_NPM, a_weight, a_sex, a_roost);
}
void Goose_Greylag_Base::ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) {
	Goose_Base::ReInit(p_L, p_NPM, a_weight, a_sex, a_roost);
	Init(p_NPM, a_weight, a_sex, a_roost);
}

void Goose_Greylag_Base::Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) {
	Goose_Base::Init(p_NPM, a_weight, a_sex, a_roost);
	/**
	* Here we deal with intialisation that is common to all greylags but different from other geese species
	* Calculates the maximum daily intake possible
	*/
	m_myspecies = gs_Greylag;
	double daylightprop = m_OurLandscape->SupplyDaylightProp();
	double nighttimeprop = m_OurLandscape->SupplyNightProp();
	double multiplier = m_OurPopulationManager->GooseCfg.cfg_goose_daytime_BMR_multiplier.value() * daylightprop + m_OurPopulationManager->GooseCfg.cfg_goose_nighttime_BMR_multiplier.value() * nighttimeprop;
	SetDailyEnergyBudget(m_BMR * multiplier + m_OurPopulationManager->GetThermalCosts(m_myspecies));
}
//-------------------------------------------------------------------------------------------------------

Goose_Greylag_Base::~Goose_Greylag_Base() 
{
	;
}
//-------------------------------------------------------------------------------------------------------

TTypeOfGoose_BaseState Goose_Greylag_Base::st_ToRoost()
{
	/**
	* If we have arrived in this method while at a forage location then the correct number of geese need to be removed from that location.
	*/
	if (m_myForageIndex != -1)
	{
		m_OurPopulationManager->RemoveGeeseFromForageLocation(m_myGooseSpeciesType, m_myForageIndex, m_groupsize);
	}
	m_myForageIndex = -1;
	/**
	* There may be multiple roosts possible, and the chance that we change roost if there is a nearer one is given by #cfg_goose_roostchangechance 
	* This is first tested, if the test is passed then the goose will move to the nearest roost.
	*/
	if (g_rand_uni() < m_OurPopulationManager->GooseCfg.cfg_goose_roostchangechance.value())
	{
		ChangeRoost();
	}
	FlyTo(m_MyRoost.m_x, m_MyRoost.m_y);
	return togs_Roost;
}
//--------------------------------------------------------------------------------------------------------

APoint Goose_Greylag_Base::ChooseHopLoc()
{
	/** Chooses a location to hop to within greylag foraging distance.
	* The foraging distance is different depending on where this method is called from.
	* The point is returned after ensuring that the co-ordinates are valid.
	*/
	APoint pt;
	pt.m_x = (int)((g_rand_uni() * m_GooseForageDistX2[gs_Greylag]) - m_GooseForageDist[gs_Greylag] + m_MyRoost.m_x);
	pt.m_y = (int)((g_rand_uni() * m_GooseForageDistX2[gs_Greylag]) - m_GooseForageDist[gs_Greylag] + m_MyRoost.m_y);
	m_OurLandscape->CorrectCoordsPointNoWrap(pt);
	/** Then automatically calls #EvaluateForageToHopLoc to fill in the information between here and the hop location. */
	EvaluateForageToHopLoc(pt);
	return pt;
}
//--------------------------------------------------------------------------------------------------------

//*******************************************************************************************************
//**********************************Goose_Greylag_FamilyGroup*********************************************
//*******************************************************************************************************
void Goose_Greylag_FamilyGroup::Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, int a_groupsize, APoint a_roost)
{
	Goose_Greylag_Base::Init(p_NPM, a_weight, a_sex, a_roost);
	/**
	Here we deal with intialisation that is only for family greylags
	*/
	m_groupsize = a_groupsize;
	m_myGooseSpeciesType = gst_GreylagFamilyGroup;
}
Goose_Greylag_FamilyGroup::Goose_Greylag_FamilyGroup(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, 
	int a_groupsize, APoint a_roost) : Goose_Greylag_Base(p_L, p_NPM, a_weight, a_sex, a_roost)
{
	Init(p_NPM, a_weight, a_sex, a_groupsize, a_roost);
}
void Goose_Greylag_FamilyGroup::ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, int a_groupsize, APoint a_roost)
{
	Goose_Greylag_Base::ReInit(p_L, p_NPM, a_weight, a_sex, a_roost);
	Init(p_NPM, a_weight, a_sex, a_groupsize, a_roost);
}
//-------------------------------------------------------------------------------------------------------

Goose_Greylag_FamilyGroup::~Goose_Greylag_FamilyGroup() 
{
	;
}
//-------------------------------------------------------------------------------------------------------

void Goose_Greylag_FamilyGroup::Step( )
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (CurrentGState)
	{
	case togs_InitialState: // Initial state always starts by setting the behaviour to togs_Roost
		CurrentGState = togs_Roost;
		break;
	case togs_ChooseForageLocation: /** In state ChooseForageLocation, if the bird is the group leader then he will determine the forage location. Otherwise does nothing, all thinking is done by the group leader. */
		CurrentGState = st_ChooseForageLocation();
		m_StepDone = true;
		break;
	case togs_Forage:
		CurrentGState = st_Forage(); /** In state Forage the bird must assess competitive status, and determine forage intake. If acceptable feed, otherwise move */
		m_StepDone = true;
		break;
    case togs_Rest:
        CurrentGState = st_Rest(); /** In state Rest the goose has eaten enough and is just resting while waiting for the food to be processed and start foraging again */
        m_StepDone = true;
        break;
	case togs_ToRoost:
		CurrentGState = st_ToRoost();
		m_StepDone = true;
		break;
	case togs_Roost:
		CurrentGState = st_Roost();
		m_StepDone = true;
		break;
	case togs_Emigrate:
		On_Migrate(tolr_migration);
		break;
	case togs_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Goose_Greylag_FamilyGroup::Step()","unknown state - default");
		exit(1);
	}
}
//-------------------------------------------------------------------------------------------------------

void Goose_Greylag_FamilyGroup::KillThis( )
{
	/** 
	* Kill this for single animal agents is straightforward, but for families it is a single bird that dies.
	* The bird is randomly picked from the family group, and if the family group is empty the group 'dies'
	* In all cases we need to remove the bird from the forage area if any.
	* The birds do not fly away at this point (if there are any left), this happens when the get the On_Bang message.
	*/
	if (m_myForageIndex != -1)
	{
		m_OurPopulationManager->RemoveGeeseFromForageLocation(m_myGooseSpeciesType, m_myForageIndex, 1);
	}
	if (--m_groupsize < 1)
	{
		delete m_MyMemory; // this is reinitisalised in the object pool.
		TAnimal::KillThis();
	}
	
}
//-------------------------------------------------------------------------------------------------------
//*******************************************************************************************************

//*******************************************************************************************************
//**********************************Goose_Greylag_NonBreeder**************************************************
//*******************************************************************************************************

Goose_Greylag_NonBreeder::Goose_Greylag_NonBreeder(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) : Goose_Greylag_Base(p_L, p_NPM, a_weight, a_sex, a_roost)
{
	Init(p_NPM, a_weight, a_sex, a_roost);
}
//-------------------------------------------------------------------------------------------------------

void Goose_Greylag_NonBreeder::Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) {
	Goose_Greylag_Base::Init(p_NPM, a_weight, a_sex, a_roost);
	/**
	* Here we deal with intialisation that is only for Goose_Greylag_NonBreeder birds. This is the only difference between family birds and non-breeders.
	*/
	m_groupsize = 1;
	m_myGooseSpeciesType = gst_GreylagNonBreeder;
}
//-------------------------------------------------------------------------------------------------------

void Goose_Greylag_NonBreeder::ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) {
	Goose_Greylag_Base::ReInit(p_L, p_NPM, a_weight, a_sex, a_roost);
	Init(p_NPM, a_weight, a_sex, a_roost);
}
//-------------------------------------------------------------------------------------------------------

Goose_Greylag_NonBreeder::~Goose_Greylag_NonBreeder() 
{
	;
}
//-------------------------------------------------------------------------------------------------------

void Goose_Greylag_NonBreeder::Step( )
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (CurrentGState)
	{
	case togs_InitialState: // Initial state always starts by setting the behaviour to togs_Roost
		CurrentGState = togs_Roost;
		break;
	case togs_ChooseForageLocation: /** In state ChooseForageLocation, if the bird is the group leader then he will determine the forage location. Otherwise does nothing, all thinking is done by the group leader. */
		CurrentGState = st_ChooseForageLocation();
		m_StepDone = true;
		break;
	case togs_Forage:
		CurrentGState = st_Forage(); /** In state Forage the bird must assess competitive status, and determine forage intake. If acceptable feed, otherwise move */
		m_StepDone = true;
		break;
    case togs_Rest:
        CurrentGState = st_Rest(); /** In state Rest the goose has eaten enough and is just resting while waiting for the food to be processed and start foraging again */
        m_StepDone = true;
        break;
	case togs_ToRoost:
		CurrentGState = st_ToRoost();
		m_StepDone = true;
		break;
	case togs_Roost:
		CurrentGState = st_Roost();
		m_StepDone = true;
		break;
	case togs_Emigrate:
		On_Migrate(tolr_migration);
		break;
	case togs_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone=true;
		break;
	default:
		m_OurLandscape->Warn("Goose_Greylag_NonBreeder::Step()","unknown state - default");
		exit(1);
	}
}
//-------------------------------------------------------------------------------------------------------
//*******************************************************************************************************


