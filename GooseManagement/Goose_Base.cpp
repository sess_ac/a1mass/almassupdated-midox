/* 
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Goose_Base.h This is the c++ code file for the goose base class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of 8th February 2013 \n
*/

#include <iostream>
#include <list>
#include "../Landscape/ls.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../People/Hunters/Hunters_all.h"

/** \brief This pointer provides access the to the internal ALMaSS error message system <removed, redundant>*/
//extern MapErrorMsg *g_msg;
/** \brief This variable provides access the to the internal ALMaSS math functions */
extern ALMaSS_MathFuncs g_AlmassMathFuncs;

using namespace std;

//*******************************************************************************************************

//*******************************************************************************************************

//*******************************************************************************************************
// Initialise static members.
// Note that it is possible to do this only here - but the config variables have not been read in - so these need to be done again
// afterwards. This is done in the population manager by creating an instance of the Goose_Base and assinging these variables again
/// but with correct values.
double Goose_Base::m_flightcost = -1;
double Goose_Base::m_mingooseforagerate[ 3 ] = { -1, -1, -1 };
double Goose_Base::m_GooseMaxAppetiteScaler = -1;
double Goose_Base::m_GooseMaxEnergyReserveProportion = -1;
double Goose_Base::m_GooseKJtoFatConversion = -1;
double Goose_Base::m_GooseFattoKJConversion = -1;
double Goose_Base::m_GooseMinForageOpenness = -1;
double Goose_Base::m_GooseLeavingThreshold = -1;
double Goose_Base::m_GooseForageDistX2[3] = { -1, -1, -1 };
double Goose_Base::m_GooseForageDist[3] = { -1, -1, -1 };
double Goose_Base::m_GooseFieldForageDistX2[3] = { -1, -1, -1 };
double Goose_Base::m_GooseFieldForageDist[3] = { -1, -1, -1 };
int Goose_Base::m_followinglikelyhood [ gst_foobar ] = { -1, -1, -1 , -1, -1, -1 };
double Goose_Base::m_SnowDepth = 0.0;
//*******************************************************************************************************
//************************************* Goose_Base ******************************************************
//*******************************************************************************************************

void Goose_Base::Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) {
	/** Assigns the pointer to the population manager. */
	m_OurPopulationManager = p_NPM;
	/** Sets the initial state. */
	CurrentGState = togs_InitialState;
	/** Sets the sex. */
	SetSex(a_sex);
	/** Sets the individual variation in forage rate acceptance */
	m_individualforagevariation = ((m_OurPopulationManager->GooseCfg.cfg_gooseindividualforagechoicevariation.value() * g_rand_uni()) - (m_OurPopulationManager->GooseCfg.cfg_gooseindividualforagechoicevariation.value()/2)) + 1.0;
	/** Sets the weight. */
	m_weight = a_weight;
    m_Rest_counter = 0;
    int today = m_OurLandscape->SupplyDayInYear();
    int EnergyReserveDaystoStartBefore = 30;
    double EnergyReserveProportion;
    int beforeNY{0};
    double InitialEnergyReserveProportionSpring = 0.22;
    if ( m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdatestart.value() - EnergyReserveDaystoStartBefore<0){
        // If the start of the migration is close to beginning of Jan
        // check how many days in the previous year we should prepare for it
        beforeNY=365+m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdatestart.value() - EnergyReserveDaystoStartBefore;
    }
    if (EnergyReserveDaystoStartBefore>(m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdateend.value()-m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdatestart.value())){
        // The migration itself should last longer than the preparation for the migration (building the slopes)
        cout << "InitialEnergyReserveProportion: The length of the slope is bigger than the Spring length\n";
        exit(-1);
    }
    // Date when we will start dialing back to pre-migration weights
    int StartEndingSlope=m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdateend.value()-EnergyReserveDaystoStartBefore;
    // If we are inside the spring migration, the weight is as it is supposed to be
    if ((today >= m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdatestart.value()) && (today <= StartEndingSlope))
    {
        EnergyReserveProportion = InitialEnergyReserveProportionSpring;
	}
    else
    {
        // If we are waiting for migration to start the weights should already rise a bit (before the NY)
        if(beforeNY!=0&&today>beforeNY)
        {
            int n=(365-today+m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdatestart.value()+1); //+1?
            EnergyReserveProportion=m_OurPopulationManager->GooseCfg.cfg_goose_InitialEnergyReserveProportion.value()+(EnergyReserveDaystoStartBefore-n)*(InitialEnergyReserveProportionSpring-m_OurPopulationManager->GooseCfg.cfg_goose_InitialEnergyReserveProportion.value())/EnergyReserveDaystoStartBefore;
        }
        else
        {
            // If we are waiting for migration to start the weights should already rise a bit (after the NY)
            if((today<m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdatestart.value())&&(today>(m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdatestart.value()-EnergyReserveDaystoStartBefore)))
            {
                int n=m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdatestart.value()-today;
                EnergyReserveProportion=m_OurPopulationManager->GooseCfg.cfg_goose_InitialEnergyReserveProportion.value()+(EnergyReserveDaystoStartBefore-n)*(InitialEnergyReserveProportionSpring-m_OurPopulationManager->GooseCfg.cfg_goose_InitialEnergyReserveProportion.value())/EnergyReserveDaystoStartBefore;
            }
            else
            {
                    // After the migration is finished we bring the weights back
                    if(today>StartEndingSlope&&today<m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdateend.value())
                    {
                        int n=m_OurPopulationManager->GooseCfg.cfg_goose_pf_springmigdateend.value()-today;
                        EnergyReserveProportion=InitialEnergyReserveProportionSpring-(EnergyReserveDaystoStartBefore-n)*(InitialEnergyReserveProportionSpring-m_OurPopulationManager->GooseCfg.cfg_goose_InitialEnergyReserveProportion.value())/EnergyReserveDaystoStartBefore;
                    }
                    else
                    {
                        // If we are here we are just in regular times not within the migration period
                        EnergyReserveProportion=m_OurPopulationManager->GooseCfg.cfg_goose_InitialEnergyReserveProportion.value();
                    }
            }


        }


    }
    //cout<<"DEBUG:" <<today<<" Energy Reserve is "<<EnergyReserveProportion<<'\n';
    m_energyReserve = m_weight * EnergyReserveProportion;
	m_weightTotal = m_weight + m_energyReserve;
	/** Calculates BMR */
	m_BMR = pow(GetTotalWeight() / 1000, m_OurPopulationManager->GooseCfg.cfg_goose_BMRconstant2.value())*m_OurPopulationManager->GooseCfg.cfg_goose_BMRconstant1.value(); // BMR is used to scale other energetic costs on a daily basis
	m_energyToday = 0.0;
	m_BodyConditionCounter = 0;
	m_TheBodyCondition = 0.0;
	for (unsigned i = 0; i < 5; i++)
	{
		double condition = GetTotalWeight() / m_weight; // total weight divided by lean weight.
		SetBodyCondition(condition, i);
		AddBodyCondition(m_BodyCondition[i]);
	}
	m_LeaveRoostTime = 0; // intialisation
	m_myForageIndex = -1; // -1 signals not currently foraging
	m_MaxIntakeSource.m_maxintakesource = tomis_foobar;  // The source of the max forage intake rate has not been determined yet.
	// Assume a default groupsize of 1, if otherwise it needs setting in descendent classes
	m_groupsize = 1;
	// Set the roost and location
	m_Location_x = a_roost.m_x;
	m_Location_y = a_roost.m_y;
	m_MyRoost.m_x = a_roost.m_x;
	m_MyRoost.m_y = a_roost.m_y;

	// Any other intialisation will be done by descendent class constructors
	m_MyMemory = new GooseMemoryMap(this);
	// the species must be reset by the goose constructor
	m_myspecies = gs_foobar;
	// Must calculate the DEB now or risk use of an undefined m_DEB in StartDay
    SetDailyEnergyBudget(0);
	// Set the individual minimum forage rate acceptable. Overwritten later in StartDay
	Set_Indivmingooseforagerate(0.0);
	m_FlightNumber = 0;
	m_FlightDistance = 0;
	m_forageLocCount = 0;
	m_FoodMassInCrop=0;
    m_MaxCropSize=(float) (m_OurPopulationManager->GooseCfg.cfg_goosebasecropsize.value()*m_weight);

}

Goose_Base::Goose_Base(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) : TAnimal(0, 0, p_L)
{
	Goose_Base::Init(p_NPM, a_weight, a_sex, a_roost);
}

void Goose_Base::ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost) {
	TAnimal::ReinitialiseObject(0, 0, p_L);
	// Assign the pointer to the population manager
	Init(p_NPM, a_weight, a_sex, a_roost);
}

//-------------------------------------------------------------------------------------------------------

Goose_Base::~Goose_Base()
{
	if ((m_MyMemory!=nullptr)&&(m_CurrentStateNo!=-1)) delete m_MyMemory; // So here lies the problem: The allocation m_Memory is done by the
}
//-------------------------------------------------------------------------------------------------------
void Goose_Base::StartDay()
{ 
	// Adjust the energy reserves
	if (m_OurPopulationManager->GetDayTime() == 0)
	{

		// Remove our DEB from our net energy collection for the day
		m_energyToday -= GetDailyEnergyBudget();
		if (GetDailyEnergyExpenditure() > 0) //m_energyToday > 0
		{
			m_energyReserve += GetDailyEnergyExpenditure() * m_GooseKJtoFatConversion;
		}
		else m_energyReserve += GetDailyEnergyExpenditure() * m_GooseFattoKJConversion;
        if (m_energyReserve / m_weight > m_GooseMaxEnergyReserveProportion)	{
            m_energyReserve = m_GooseMaxEnergyReserveProportion *  m_weight;
        }
		m_weightTotal = m_weight + m_energyReserve;
        m_BMR = pow(GetTotalWeight() / 1000, m_OurPopulationManager->GooseCfg.cfg_goose_BMRconstant2.value())*m_OurPopulationManager->GooseCfg.cfg_goose_BMRconstant1.value(); // BMR is used to scale other energetic costs on a daily basis
		if (GetTotalWeight() < m_weight) {
			On_Migrate(tolr_leanweight);
		}
		// Some goose output recording
		int foraging_time = GetForagingTime(GetRoostLeaveTime());
        int resting_time = GetRestTime();
		m_OurPopulationManager->RecordForagingTime(foraging_time, m_myspecies);
        m_OurPopulationManager->RecordRestingTime(resting_time, m_myspecies);

		m_OurPopulationManager->RecordFlightDistance(GetFlightDistance(), m_myspecies);
		m_OurPopulationManager->RecordDailyEnergyBudget(GetDailyEnergyBudget(), m_myspecies);
		m_OurPopulationManager->RecordDailyEnergyBalance(GetDailyEnergyExpenditure(), m_myspecies);
        if (((m_OurLandscape->SupplyDayInYear() + 1) % 7 == 0)&&(m_groupsize==1)){ // we record weight only for non-breeders, because otherwise the statistics is
            //affected by the group size and the std of weights inside the family making the results incomparable with the field data
            m_OurPopulationManager->RecordWeight(GetTotalWeight(), m_myspecies);// we run it only once a week since
            // the recording is performed only once a week
        }

		m_OurPopulationManager->RecordIndForageLoc(double(m_forageLocCount), m_groupsize, m_myspecies);
		m_forageLocCount = 0;

		// Set the body condition as % by weight of fat. This is a running sum over the last 5 days.
		m_BodyConditionCounter = ++m_BodyConditionCounter & 5;
		SubtractBodyCondition(m_BodyCondition[m_BodyConditionCounter]);
		double condition = GetTotalWeight() / m_weight;
		SetBodyCondition( condition, m_BodyConditionCounter );
		AddBodyCondition( m_BodyCondition[m_BodyConditionCounter]);
		// Here we limit the maximum growth possible

		if (GetBodyCondition() < m_GooseLeavingThreshold)
		{
			On_Migrate(tolr_bodycondition);
		}
		// Reset energy intake
		m_energyToday = 0;
		m_FlightNumber = 0;
		m_FlightDistance = 0;
		// Reset the roost leaving time
		m_LeaveRoostTime = (*m_OurPopulationManager->m_variate_generator).get();

		// Decay all memories before adding new information today
		m_MyMemory->DecayAllMemory();
		// Calculate the new DEB
		double daylightprop = m_OurLandscape->SupplyDaylightProp();
		double nighttimeprop = m_OurLandscape->SupplyNightProp();
		double multiplier = m_OurPopulationManager->GooseCfg.cfg_goose_daytime_BMR_multiplier.value() * daylightprop + m_OurPopulationManager->GooseCfg.cfg_goose_nighttime_BMR_multiplier.value() * nighttimeprop;
		SetDailyEnergyBudget(m_BMR * multiplier + m_OurPopulationManager->GetThermalCosts( m_myspecies ));
		double minforagerate = (GetDailyEnergyBudget() / (1440 * daylightprop));  // They accept forage rates so they can obtain positive energy balance minus the rest time from the previous day: is it correct?
        if (minforagerate<0){
            minforagerate=0; // if for whatever reason minforrate is negative make it zero
        }
		Set_mingooseforagerate( minforagerate, m_myspecies );
        ResetRestTime(); // we only reset it here, we need the rest time from the previous day to set minforagerate
		Set_Indivmingooseforagerate( minforagerate);  // At the start of the day, give the individual the MFR for the species
		m_SnowDepth = m_OurLandscape->SupplySnowDepth();  // We store the current snow depth here for speed optimization.
		UpdateMaxCropSize();
        ResetLastEmptyCrop();
		ResetCrop();
  //      /** If the date is correct let us switch to the spring fattening*/
//        if (today==cfg_goose_MaxEnergyReserveProportionChangeDate.value()){
  //          Set_GooseMaxEnergyReserveProportion( cfg_goose_MaxEnergyReserveProportionSpring.value() );
    //    }
	}
} 

void Goose_Base::Step()
{
    m_OurLandscape->Warn("Goose_Base::Step()","attempt to instantiate and use the base class!");
    exit(1);
}

//-------------------------------------------------------------------------------------------------------

void Goose_Base::st_Dying( )
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_StepDone = true;
	delete m_MyMemory; // this is reinitisalised in the object pool.
}
//-------------------------------------------------------------------------------------------------------
TTypeOfGoose_BaseState Goose_Base::st_ChooseForageLocation( )
{
	// Debugging
	m_OurPopulationManager->RecordState();

	int dtime = m_OurPopulationManager->GetDayTime();
	int daylength = m_OurLandscape->SupplyDaylength();
	if (dtime >= daylength + m_OurPopulationManager->GooseCfg.cfg_goose_AfterDarkTime.value())
	{
#ifdef __DEBUG_GOOSE
		if (m_myForageIndex != -1)
		{
			if (m_OurPopulationManager->GetBirdsAtForageLoc(m_myForageIndex, m_myGooseSpeciesType) < m_groupsize) {
				g_msg->Warn("Goose_Base::st_ChooseForageLocation - Removing negative geese from forage location", (double)(m_OurPopulationManager->GetBirdsAtForageLoc(m_myForageIndex, m_myGooseSpeciesType) - m_groupsize));
				exit(0);
			}
		}
#endif
		return togs_ToRoost;
	}

	if (g_rand_uni2() <= m_followinglikelyhood[m_myGooseSpeciesType])
	{
		/**
		* 2. If we are following then find a good location from the main list of birds held by the population manager.
		* If no such location is available then default to explore.
		*/
		/**
		* Depending on the rules in force we probably only want to follow geese from our own species,
		* and maybe from our own roost
		*/
#ifdef __FOLLOW_FIELD
		int index = -1;
			int forageindex = m_OurPopulationManager->GetForageLocIndex( m_myspecies, m_Location_x, m_Location_y );
			if (forageindex > -1) {
				index = forageindex;
			}
#endif

#ifdef __FOLLOW_BIRD  
		Goose_Base* leader = m_OurPopulationManager->GetLeader( m_MyRoost, m_myspecies );
		if (leader != NULL) {
			int index = leader->GetForageLocIndex();
	}
#endif

			if (index > -1) 
			{

				GooseActiveForageLocation* gafl = m_OurPopulationManager->GetForageLocation( index );
				FlyTo( m_OurLandscape->SupplyCentroid( gafl->GetPolygonref() ) );
				Set_GooseLeavingRoost(false);  // We've left the roost now, so reset
				GooseMemoryLocation aThought;
				int poly = gafl->GetPolygonref();
				double grain = gafl->GetGrainDensity();  // grain/m2
				double maize = gafl->GetMaizeDensity();  // kJ/m2
				aThought.m_grain = m_OurPopulationManager->GetFeedingRate(grain, m_myspecies);  // kJ/min
				aThought.m_maize = m_OurPopulationManager->GetMaizeFeedingRate(maize, m_myspecies);  // kJ/min
				aThought.m_grazing = gafl->GetGrazing( (int)m_myspecies );  // kJ/min
				aThought.m_foodresource = GetMaxIntakeRate(aThought.m_grain, aThought.m_maize, aThought.m_grazing);
				aThought.m_polygonid = poly;
				aThought.m_x = m_OurLandscape->SupplyCentroidX( poly );
				aThought.m_y = m_OurLandscape->SupplyCentroidY( poly );
				aThought.m_threat = 0;
				int dist = g_AlmassMathFuncs.CalcDistPythagorasApprox( m_MyRoost.m_x, m_MyRoost.m_y, aThought.m_x, aThought.m_y );
				aThought.m_score = m_MyMemory->CalcScore(dist, aThought.m_foodresource, aThought.m_threat);
				if (!m_MyMemory->IsKnownArea( gafl->GetPolygonref() )) m_MyMemory->MemAdd( aThought );
				// Once the goose arrives at a forage location it needs to add itself to the geese already there, or initiate a forage location if one does not exist 
				m_myForageIndex = m_OurPopulationManager->ForageLocationInUse( aThought.m_polygonid );
				if (m_myForageIndex != -1) m_OurPopulationManager->AddGeeseToForageLocation( m_myGooseSpeciesType, m_myForageIndex, m_groupsize );
				else {
					g_msg->Warn( "TTypeOfGoose_BaseState Goose_Base::st_ChooseForageLocation() : No geese at foraging location when following", "" );
					exit( 0 );
				}
				// some stats recording
				m_forageLocCount++;
				m_myMemPolyID = aThought.m_polygonid;
				return togs_Forage;
			}
	}
	/** 
	* First searches memory for the best forage location remembered. If it finds one then it flies there and calculates
	* the energetic cost. If no suitable memory is found then initiates an exploration.
	*
	* Once the goose arrives at a forage location it needs to add itself to the geese already there, 
	* or initiate a forage location if one does not exist. 
	*/
	if (m_LeavingRoost) Explore();  // Go for an exploration trip to initialise the memory.
	GooseMemoryLocation aThought = m_MyMemory->GetBestFeedingScore();
	if (aThought.m_score<=0)
	{
		Explore();  // Go for an exploration trip
		aThought = m_MyMemory->GetBestFeedingScore();
		// Did we find anything suitable?:
		if (aThought.m_score <= 0) {
			// if not, start this method over again.
			return togs_ChooseForageLocation;
		}
	}
	m_Location_x = aThought.m_x;
	m_Location_y = aThought.m_y;
	int polyref = m_OurLandscape->SupplyPolyRef(m_Location_x, m_Location_y);
	double grazing = m_OurLandscape->GetActualGooseGrazingForage(polyref, m_myspecies);  // kJ/min
	double grain = m_OurLandscape->SupplyBirdSeedForage(polyref);  // seeds/m2
	double maize = m_OurLandscape->SupplyBirdMaizeForage(polyref);  // seds/m2
	//bool instubble = m_OurLandscape->SupplyInStubble(polyref);
	//TTypesOfVegetation veg = m_OurLandscape->SupplyVegType(polyref);
	//TTypesOfVegetation lastsowncrop = m_OurLandscape->SupplyLastSownVeg(polyref);
	//bool iscereal = m_OurLandscape->SupplyIsCereal2(lastsowncrop);
    // Here we make a test to see if the forage location is in use before going further
	m_myForageIndex = m_OurPopulationManager->ForageLocationInUse(aThought.m_polygonid);
	// Here we do the forage calculation.The intake rate depends on :
	// -1 The grain or grazing resource available in grain and grazing
	double grain_intake = m_OurPopulationManager->GetFeedingRate(grain, m_myspecies); // kJ/min
	double maize_intake = m_OurPopulationManager->GetMaizeFeedingRate(maize, m_myspecies); // kJ/min
	m_MyMemory->ChangeSetFoodRes(aThought.m_polygonid, grain_intake, maize_intake, grazing);  // Remember these
// -2 The relationship between resource and intake rate.This is dependent on the species and family or non - breeder. It also depends on the 
	// type of resource, i.e. the response for grain is different than for grazing.
	// -3 The number of geese which will determine an interference competition factor, which reduces intake rate
	double geese = m_groupsize;  // if m_myForageIndex = -1, nobody else is there.
	if ( m_myForageIndex != -1) geese = m_OurPopulationManager->GetForageGooseDensity(m_myForageIndex);
	// -4 The interference factor is a look-up from a pre-calculated curve in the goose population manager
	double interferenceComp = m_OurPopulationManager->GetForageRateDensity(geese, m_myspecies);
	// A simplification here is to assume that they either eat grain or graze but not both
	double myforagerate;
	// We need to determine if myforagerate is ok, before calling GetMaxForageRate as 
	// this sets the max intake rate up for recording. This might cause recording of a 
	// forage rate that was too low.
	myforagerate = GetMaxIntakeRate(grain_intake, maize_intake, grazing);
	myforagerate *= interferenceComp;
	myforagerate = AdjustIntakeRateToSnowDepth(myforagerate) * m_individualforagevariation;
	// Now decide if this is OK or not. The intake rate is compared to the current minimum intake rate for the individual.
	if (myforagerate < m_Indivmingooseforagerate)
	{
		m_myForageIndex = -1; // If we don't do this we will get a negative number of geese on the forage location later unless they find somewhere else to go.
		return togs_ChooseForageLocation;
	}
	//myforagerate = GetMaxForageRate(grain_intake, maize_intake, grazing, interferenceComp, iscereal, veg, instubble, lastsowncrop) * m_individualforagevariation; // GetMaxForageRate determines the source of max intake rate and adjust intake for snow cover if needed.
	// OK we are staying
	m_forageLocCount++;
	if (m_myForageIndex != -1){
		m_OurPopulationManager->AddGeeseToForageLocation(m_myGooseSpeciesType, m_myForageIndex, m_groupsize);
	}
	else {
		m_myForageIndex = m_OurPopulationManager->NewForageLocation(m_myGooseSpeciesType, m_groupsize, aThought.m_polygonid);
	}
	m_myMemPolyID = aThought.m_polygonid;
	return togs_Forage;
}
//-------------------------------------------------------------------------------------------------------

void Goose_Base::FlyTo(APoint a_pt )
{
	FlyTo(a_pt.m_x,a_pt.m_y);
}
//-------------------------------------------------------------------------------------------------------

void Goose_Base::FlyTo(int a_x, int a_y )
{
	/** 
	* Moves the bird from m_Location_x,m_Location_y to a_x,a_y. Assumes that the cost of flight is 
	* directly proportional to the distance x bird weight. The cost is calcuated and removed from the 
	* energy reserve. To save some calculation costs (sqrt is expensive in time) we use an approximation
	* to Pythagorus accurate to around 6%.
	* m_weightTotal is in grams and m_flightcost the cost of moving 1 gram per m, all distances in meters.
	* NB all flying trips are assumed to take 10 minutes but the energy used is determined by the distance.
	*/
	if ((m_Location_x == a_x) && (m_Location_y == a_y)) return;
	int dist = g_AlmassMathFuncs.CalcDistPythagorasApprox( m_Location_x, m_Location_y, a_x, a_y );
	double cost = dist * GetFlightCost();
	m_energyToday-=cost;
	/** and sets the new location */
	m_Location_x = a_x;
	m_Location_y = a_y;
	/** and finally store the information about flight number and distance */
	m_FlightNumber++;
	m_FlightDistance += dist;

}
//-------------------------------------------------------------------------------------------------------

TTypeOfGoose_BaseState Goose_Base::st_Forage()
{
	/**
	* Must assess the competition status here (must be at a forage location to call this state).
	* If the forage rate resulting from competition and forage availability is too low then the bird will move 
	* the flock/family, otherwise it will signal feeding at a certain rate. The forage loss will need to be calculated
	* and sent to the LE polygon (foragae location) in question. This is done immediately since it will influence the
	* forage assessment of birds not yet considered in this step (assumed to come later).
	*
	* The first things is to check if we are near the end of daylight, if so then fly to roost.
	*
	* If the goose has eaten all it can in a day, then it just rests on the field until evening.\n
	*
	* Get the grain and forage density - NB is in KJ, which needs to be to needs to be remembered.\n
	* Here we do the forage calculation.The intake rate depends on :
	* -1 The grain or grazing resource available
	* -2 The relationship between resource and intake rate.This is dependent on the species and family or non - breeder.
	*    It also depends on the type of resource, i.e. the response for grain is different than for grazing.
	* -3 The number of geese which will determine an interference competition factor, which reduces intake rate
	*
	* Then decide if this is OK or not. The simplest here is to compare to a standard intake rate threshold for
	* the species/type combination.
	*/
	int dtime = m_OurPopulationManager->GetDayTime();
	int daylength = m_OurLandscape->SupplyDaylength();
	if (dtime >= daylength + m_OurPopulationManager->GooseCfg.cfg_goose_AfterDarkTime.value())
	{
#ifdef __DEBUG_GOOSE
		if (m_myForageIndex != -1)
		{
			if (m_OurPopulationManager->GetBirdsAtForageLoc(m_myForageIndex, m_myGooseSpeciesType) < m_groupsize) {
				g_msg->Warn("Goose_Base::st_Forage - Removing negative geese from forage location", (double)(m_OurPopulationManager->GetBirdsAtForageLoc(m_myForageIndex, m_myGooseSpeciesType) - m_groupsize));
				exit(0);
			}
		}
#endif
		return togs_ToRoost;
	}

    //cout<<"DEBUG: Checking crop: Filled "<<GetCropSize()<< " out of "<< GetMaxCropSize()<<endl;
	// If the crop is full we will stop eating and start resting
	if (GetCropSize()>=GetMaxCropSize()){

	    return togs_Rest;
	}
	//
	// Get the grain and forage density - NB grain/m2 and maize in kJ/m2, which needs to be to needs to be remembered.
	//
	int polyref = m_OurPopulationManager->m_GooseForageLocations[m_myForageIndex].GetPolygonref();
	double grazing = m_OurLandscape->GetActualGooseGrazingForage(polyref, m_myspecies);  // kJ/min
	double grain = m_OurLandscape->SupplyBirdSeedForage(polyref);  // kJ/m2
	double maize = m_OurLandscape->SupplyBirdMaizeForage(polyref);  // kJ/m2
	bool instubble = m_OurLandscape->SupplyInStubble(polyref);
	TTypesOfVegetation veg = m_OurLandscape->SupplyVegType(polyref);
	TTypesOfVegetation lastsowncrop = m_OurLandscape->SupplyLastSownVeg(polyref);
	bool iscereal = m_OurLandscape->SupplyIsCereal2(lastsowncrop);
	// Here we do the forage calculation.The intake rate depends on :
	// -1 The grain, maize or grazing resource available in grain, maize and grazing
	double grain_intake = m_OurPopulationManager->GetFeedingRate( grain, m_myspecies ); // KJ/min
	double maize_intake = m_OurPopulationManager->GetMaizeFeedingRate(maize, m_myspecies); // KJ/min
	m_MyMemory->ChangeSetFoodRes(m_myMemPolyID, grain_intake, maize_intake, grazing);  // Remember these
	// -2 The relationship between resource and intake rate. This is dependent on the species and family or non-breeder. It also depends on the 
	// type of resource, i.e. the response for grain or maize is different than for grazing.
    // -3 The number of geese which will determine an interference competition factor, which reduces intake rate
	double geese = m_OurPopulationManager->GetForageGooseDensity(m_myForageIndex);
	// -4 The interfence factor is a look-up from a pre-calculated curve in the goose population manager
	double interferenceComp = m_OurPopulationManager->GetForageRateDensity(geese, m_myspecies);
	// A simplification here is to assume that they either eat grain, maize or graze but any combination
	double myforagerate;
	myforagerate = GetMaxForageRate( grain_intake, maize_intake, grazing, interferenceComp, iscereal, veg, instubble, lastsowncrop) * m_individualforagevariation;  // GetMaxForageRate determines the source of max intake rate and adjust intake for snow cover if needed.
	// Now decide if this is OK or not. The simplest here is to compare to a standard intake rate threshold for the species/type combination.
	if (myforagerate <= m_Indivmingooseforagerate)
	{
		/** 
		* If can't get our required forage rate then try to find somewhere that we can, 
		* and if we find a place that cannot meet our requirements then we want to forget this place fast.
		*/
		m_MyMemory->MemDel(m_myMemPolyID);
		/**No food here, so lower the acceptable level of MFR */
		Set_Indivmingooseforagerate( m_Indivmingooseforagerate * m_OurPopulationManager->GooseCfg.cfg_goose_MinForageRateDecayRate.value());
		
		/** 
		* If the goose decides to move on immediately then it must deregister itself or group with the
		* population manager at this forage location. This is done by using RemoveGeeseFromForageLocation  
		*/
		m_OurPopulationManager->RemoveGeeseFromForageLocation(m_myGooseSpeciesType, m_myForageIndex, m_groupsize);
		m_myForageIndex = -1; 
		return togs_ChooseForageLocation;
	}
	UpdateCropAtForage(myforagerate);
    if(GetCropSize()>0){
        IncLastEmptyCrop();
    }
    else{
        ResetLastEmptyCrop();
    }
    /**
    * If OK then how long should we feed? \n
    * There are three constraints: \n
    * - End of day
    * - Crop full
    * - Energy reserves at maximum
    */
	// We have lots of food, stay and eat until full.
	double forage = myforagerate * 10; // 10 for 10 minutes
	m_energyToday += forage; 
	// Scale the amount of forage removed from the field to the size of the group (1 for non-breeders)
	forage = ScaleForageToGroupsize(forage);
	m_OurPopulationManager->RemoveMaxForageKj( forage, m_MaxIntakeSource.m_maxintakesource, m_myForageIndex );  // Removes kJ from the field

	if (dtime >= daylength + m_OurPopulationManager->GooseCfg.cfg_goose_AfterDarkTime.value())
	{
		Set_Indivmingooseforagerate( GetGooseMinForageRate(m_myspecies));  // Okay, we found food today, so reset the minimum acceptable MFR.
	    return togs_ToRoost;
	}

	return togs_Forage;
}
//--------------------------------------------------------------------------------------------------------
TTypeOfGoose_BaseState Goose_Base::st_Rest(){
    IncRestTime();
    UpdateCropAtRest();
    if(GetCropSize()>0){
        IncLastEmptyCrop();
    }
    else{
        ResetLastEmptyCrop();
    }
    int dtime = m_OurPopulationManager->GetDayTime();
    int daylength = m_OurLandscape->SupplyDaylength();
    // If is ten menutes before the end of the day move to foraging, so it will move to roosting in teh next timestep
    if (dtime >= daylength + m_OurPopulationManager->GooseCfg.cfg_goose_AfterDarkTime.value())
	{
        Set_Indivmingooseforagerate( GetGooseMinForageRate(m_myspecies));  // Okay, we found food today, so reset the minimum acceptable MFR.
        return togs_ToRoost;
	}
    double cur_reserve_proportion = (m_energyReserve / m_weight);
    double x=(m_GooseMaxEnergyReserveProportion-cur_reserve_proportion)/(m_GooseMaxEnergyReserveProportion-m_OurPopulationManager->GooseCfg.cfg_goose_InitialEnergyReserveProportion.value()); // further we are from the maxEnergyReserveProportion--> faster we start eating
    double threshold = GetDynamicThreshold(x);
    if (GetCropSize()<( threshold*GetMaxCropSize())){
        // check if start eating
        //float running_chance=GetCropSize()/GetMaxCropSize();

        double probability=GetForagingChance(x);
        // m_energyReserve is the mass of the fat
        // m_weight is a lean weight
        // cout<< "DEBUG: probability to start eating is "<< probability<< " scaling param is "<<1-m_energyReserve / m_weight<< " running chance is "<<running_chance;
        //cout<< " crop size is "<< GetCropSize()<< " maxcrop is: "<< GetMaxCropSize()<< " energy reserve: "<<m_energyReserve<< " weight: "<< m_weight;
        if (g_rand_uni()<probability){
            //  cout<<" Finishes Resting"<<endl;
            return togs_Forage;
        }

    }
    // cout<<" Proceeds Resting"<<endl;
    return togs_Rest;


}

//--------------------------------------------------------------------------------------------------------
double Goose_Base::GetForagingChance(double x) {

    // m_energyReserve is the mass of the fat
    // m_weight is a lean weight
    // m_GooseMaxEnergyReserveProportion is the maximum proportion of m_energyReserve to m_weight is possible for goose under the circumstances
    //todo: check if it is too slow and precalculate
    double foraging_chance=1/(1 + exp(-(x*10-5)));
    return foraging_chance;

}
//--------------------------------------------------------------------------------------------------------
double Goose_Base::GetDynamicThreshold(double ratio){
    // The dynamic threshold is the steps function
    // So we just multiply by 10 do round and divide back
    return std::round(ratio*10)/10;
}
//--------------------------------------------------------------------------------------------------------
int Goose_Base::GetThrouputTime() const{
    return m_OurPopulationManager->GooseCfg.cfg_goose_throuput_time.value();
}
//--------------------------------------------------------------------------------------------------------
void Goose_Base::RemoveFoodFromCrop() {
    // Here we will remove each time 1/12=1/(2 hours *6 timepoints per hour) one twelfth of maximum crop size 1/12 = 0.083
    if (GetLastEmptyCrop()>GetThrouputTime()){
        m_FoodMassInCrop-=(float)(GetWeight()*m_OurPopulationManager->GooseCfg.cfg_goosedigestionparam.value());
    }

}
//--------------------------------------------------------------------------------------------------------
void Goose_Base::UpdateCropAtForage(double foragerate ){

    

    // Important: In the landscape the grain and maize weight is dry, the grass weight is wet
    /** forage rate is measured in kJ/min so we should also multiply by 10*/
    /** to get to the wet weight we should take into account dry weight per kJ, digestability, and (for the grain and maize) the water content
     * the grass in ALMaSS is already given as a wet weight
     * */
    /** For grass 3.5 kJ/gram ww; Robbins 1993 (1/3.5 = 0.286) (0.286/0.4=.714) (.714*10=7.14 gr consumed per kJ in 10 mins) */
    /** For maize: (1/(14.8*0.7*0.35))*10=2.758 */
    /** For grain: (1/(16*0.695*0.35))*10=2.57 */

    //float grain_water_content = 0.35;
    double grass_coeff = m_OurPopulationManager->GooseCfg.cfg_goose_grass_energy_density.value();// (1/(20*0.4))*10*
    double maize_coeff = m_OurPopulationManager->GooseCfg.cfg_goose_maize_energy_density.value(); // (1/(14.8*0.7))*0.35=2.758*0.35=3.72330
    double grain_coeff = m_OurPopulationManager->GooseCfg.cfg_goose_grain_energy_density.value(); // (1/(16*0.695))*0.35=2.57*0.35=3.46
    switch (GetMaxIntakeSource().m_maxintakesource) {
        case tomis_grain: m_FoodMassInCrop+=(float)(foragerate*grain_coeff);
        case tomis_grass: m_FoodMassInCrop+=(float)(foragerate*grass_coeff);
        case tomis_maize: m_FoodMassInCrop+=(float)(foragerate*maize_coeff);
        case tomis_sowncrop: m_FoodMassInCrop+=(float)(foragerate*grass_coeff); //Sown crop is like grass
        case tomis_foobar:;
        
    }
    m_FoodMassInCrop=m_FoodMassInCrop>GetMaxCropSize()?GetMaxCropSize():m_FoodMassInCrop; // cannot eat more than the size of the crop
    //Let's also remove the nominal amount
    RemoveFoodFromCrop();
    if (m_FoodMassInCrop<0){ // cannot have a negative mass of food in the crop
        m_FoodMassInCrop=0;
    }

}
//-------------------------
double Goose_Base::GetMaxForageRate(double a_grain, double a_maize, double a_grass, double a_interferenceComp, bool a_iscereal, TTypesOfVegetation a_veg, bool a_stubble, TTypesOfVegetation a_prevcrop) {
    double MaxRate = 0.0;
    m_MaxIntakeSource.m_maxintakesource = tomis_foobar;
    if (a_grain > MaxRate) {
        MaxRate = a_grain;
        m_MaxIntakeSource.m_maxintakesource = tomis_grain;
    }
    if (a_maize > MaxRate) {
        MaxRate = a_maize;
        m_MaxIntakeSource.m_maxintakesource = tomis_maize;
    }
    if (a_grass > MaxRate) {

        if (a_iscereal == false) {
            m_MaxIntakeSource.m_maxintakesource = tomis_grass;
            MaxRate = a_grass;
        }
        else {
            // we allow the crop to be eaten only if it is not in stubble or not oilseedrape that could grow in stubble
            // this is needed to ensure that geese are not eating the stubble under any circumstances
            if ((a_stubble == false)||(m_OurPopulationManager->GooseCfg.GooseToleTovs.GooseCanGrowInStubbleCereals.getList().find(a_veg) != m_OurPopulationManager->GooseCfg.GooseToleTovs.GooseCanGrowInStubbleCereals.getList().end())){
                m_MaxIntakeSource.m_maxintakesource = tomis_sowncrop;
                MaxRate = a_grass;
            }

        }
    }

    MaxRate *= a_interferenceComp;
    MaxRate = AdjustIntakeRateToSnowDepth(MaxRate);
    m_MaxIntakeSource.m_iscereal = a_iscereal;
    m_MaxIntakeSource.m_instubble = a_stubble;
    m_MaxIntakeSource.m_veg = a_veg;
    m_MaxIntakeSource.m_prevsowncrop = a_prevcrop;
    /**** CJT DEBUG ****
    if ((a_stubble) && (!a_iscereal) && (m_MaxIntakeSource.m_maxintakesource == tomis_grain))
    {
        g_msg->WarnAddInfo(WARN_BUG, "Day ", m_OurLandscape->SupplyDayInYear());
        g_msg->WarnAddInfo(WARN_BUG, "Veg ", a_veg);
        g_msg->WarnAddInfo(WARN_BUG, "Prev Crop ", a_prevcrop);
        g_msg->WarnAddInfo(WARN_BUG, "Grain ", a_grain);
        g_msg->WarnAddInfo(WARN_BUG, "******** ", 0);
    }
    **** END ****/


    return MaxRate;
}

//-------------------------------------------------------m_FoodMassInCrop-------------------------------------------------
void Goose_Base::UpdateCropAtRest() {

    RemoveFoodFromCrop();
    if (m_FoodMassInCrop<0){ // cannot have a negative mass of food in the crop
        m_FoodMassInCrop=0;
    }
}
//--------------------------------------------------------------------------------------------------------
float Goose_Base::GetMaxCropSize() const{
    return m_MaxCropSize;
}
//--------------------------------------------------------------------------------------------------------
inline void Goose_Base::UpdateMaxCropSize(){
  double base_crop_size=m_OurPopulationManager->GooseCfg.cfg_goosebasecropsize.value(); //wet gr of food per gr weight
  m_MaxCropSize=(float) (base_crop_size*m_weight);
}
//--------------------------------------------------------------------------------------------------------
TTypeOfGoose_BaseState Goose_Base::st_ToRoost()
{
	return togs_foobar;
}
//--------------------------------------------------------------------------------------------------------

TTypeOfGoose_BaseState Goose_Base::st_Roost()
{
	/**
	* Assesses whether it is daylight and time to leave the roost. If not it continues to roost.
	*/
	if ( !m_OurPopulationManager->GetIsDaylight() )
	{
		return togs_Roost;
	}
	/**
	* m_LeaveRoostTime is a way to stagger the birds leaving the roost. Each birds has its own leave time set each day.
	*/
	if (GetRoostLeaveTime() > m_OurPopulationManager->GetDayTime())
	{
		return togs_Roost;
	}
	Set_GooseLeavingRoost( true );  // Indicate that we are coming from the roost. Used to trigger memory initialisation via explore.
	return togs_ChooseForageLocation;
}
//--------------------------------------------------------------------------------------------------------

void Goose_Base::Explore()
{
	/** 
	* This behaviour can be invoked either from the roost or from another location. If from another location then there
	* is an initial move followed by an exploration of the area directly on route to the chosen location.
	* If from the roost then a distance and direction is chosen for the explore. In both cases the movement has
	* energetic costs associated with the flight.
	*/
	APoint HopLoc;
	for (int i = 0; i < 1; i++)
	{
		HopLoc = ChooseHopLoc(); // The distance moved is dependent on the species and this is controlled by the inherited ChooseHopLoc code.
	}
	FlyTo( HopLoc );
}
//--------------------------------------------------------------------------------------------------------

/**
* Moves along the vector between the current location and a_HopLoc testing forage for each polygon
* encountered along the way. This will build up the goose' memory. 
* \param [in] a_HopLoc A point in the landscape.
*/
void Goose_Base::EvaluateForageToHopLoc( APoint a_HopLoc )
{
	double dx = a_HopLoc.m_x - m_Location_x;
	double dy = a_HopLoc.m_y - m_Location_y;
	int dist = g_AlmassMathFuncs.CalcDistPythagoras( a_HopLoc.m_x, a_HopLoc.m_y, m_Location_x, m_Location_y );
	double tx = m_Location_x;
	double ty = m_Location_y;
	// We are looking for big things, so no need to move at 1m steps
	double ddx = (dx/dist)*100;
	double ddy = (dy/dist)*100;
	int lastpoly = m_OurLandscape->SupplyPolyRef(m_Location_x, m_Location_y);
    for (int d=0; d<dist/100; d++)
	{
		tx += ddx;
		ty += ddy;
			if (m_OurLandscape->SupplyPolyRef((int)tx, (int)ty) != lastpoly)
			{
				lastpoly = m_OurLandscape->SupplyPolyRef((int)tx, (int)ty);
				int openness = (int)m_OurLandscape->SupplyOpenness(lastpoly); // Score calculated by Landscape::CaclulateOpenness
				if (openness >= m_GooseMinForageOpenness)
				{
					GooseMemoryLocation aThought;
					double grain = m_OurLandscape->SupplyBirdSeedForage(lastpoly); // grain/m2
					double maize = m_OurLandscape->SupplyBirdMaizeForage(lastpoly); // kJ/m2
					aThought.m_grain = m_OurPopulationManager->GetFeedingRate(grain, m_myspecies); // kJ/min
					aThought.m_maize = m_OurPopulationManager->GetMaizeFeedingRate(maize, m_myspecies); // kJ/min
					aThought.m_grazing = m_OurLandscape->GetActualGooseGrazingForage(lastpoly, m_myspecies); // kJ/min
					aThought.m_foodresource = GetMaxIntakeRate(aThought.m_grain, aThought.m_maize, aThought.m_grazing);
					if (aThought.m_foodresource > 0)  // don't remember poor food locations
					{
						aThought.m_polygonid = lastpoly;
						aThought.m_x = m_OurLandscape->SupplyCentroidX(lastpoly);
						aThought.m_y = m_OurLandscape->SupplyCentroidY(lastpoly);
						aThought.m_threat = 0;
						int the_dist = g_AlmassMathFuncs.CalcDistPythagorasApprox(m_MyRoost.m_x, m_MyRoost.m_y, aThought.m_x, aThought.m_y);
						aThought.m_score = m_MyMemory->CalcScore(the_dist, aThought.m_foodresource, aThought.m_threat);
						m_MyMemory->MemAdd(aThought);
					}
				}
			}
		}

}
//--------------------------------------------------------------------------------------------------------

void Goose_Base::On_Bang(int a_polyid ) 
{ 
	/**
	* The goose has heard a shot. It remembers the location the shot came from as bad and flies away
	* to find another place to forage. This version assumes the scare value is a constant 1.0.
	* The first test is because the shot might have killed it already!
	* \param [in] a_polyid The polygon id from where the shot was fired.
	*/
	if (m_CurrentStateNo == -1) return;
	/** For now we only allow bang response while foraging, because otherwise the code should be altered,
	 * in particular we should correct the calculation of the Roost Leaving Time (for now it is estimated in advance)*/
	if (CurrentGState!=togs_Forage) return;
	if (!m_MyMemory->ChangeAddThreat(a_polyid, 1.0))
	{
		GooseMemoryLocation aThought;
		aThought.m_polygonid = a_polyid;
		aThought.m_x = m_OurLandscape->SupplyCentroidX(a_polyid);
		aThought.m_y = m_OurLandscape->SupplyCentroidY(a_polyid);
		aThought.m_threat = 1;
		int dist = g_AlmassMathFuncs.CalcDistPythagorasApprox( m_MyRoost.m_x, m_MyRoost.m_y, aThought.m_x, aThought.m_y );
		aThought.m_score = m_MyMemory->CalcScore( dist, aThought.m_foodresource, aThought.m_threat );
		m_MyMemory->MemAdd(aThought);
	}
	/**
	* We have been scared off, so we the correct number of geese at that location before we fly off to find a new forage location
	*/
	if (m_myForageIndex != -1)
	{
		m_OurPopulationManager->RemoveGeeseFromForageLocation(m_myGooseSpeciesType, m_myForageIndex, m_groupsize);
	}
	m_myForageIndex = -1;
	CurrentGState = st_ChooseForageLocation();
}
//--------------------------------------------------------------------------------------------------------

void Goose_Base::On_Bang(int a_polyid, double a_scare ) 
{ 
	/**
	* The goose has heard a shot. It remembers the location the shot came from as bad and flies away
	* to find another place to forage. This version allows a variable scare value to be remembered.
	* The first test is because the shot might have killed it already!
	* \param [in] a_polyid The polygon id from where the shot was fired.
	* \param [in] a_scare The scare value.
	*/
	if (m_CurrentStateNo == -1) return;
	if (!m_MyMemory->ChangeAddThreat(a_polyid, a_scare))
	{
		GooseMemoryLocation aThought;
		aThought.m_polygonid = a_polyid;
		aThought.m_x = m_OurLandscape->SupplyCentroidX(a_polyid);
		aThought.m_y = m_OurLandscape->SupplyCentroidY(a_polyid);
		aThought.m_threat = a_scare;
		int dist = g_AlmassMathFuncs.CalcDistPythagorasApprox( m_MyRoost.m_x, m_MyRoost.m_y, aThought.m_x, aThought.m_y );
		aThought.m_score = m_MyMemory->CalcScore( dist, aThought.m_foodresource, aThought.m_threat );
		m_MyMemory->MemAdd(aThought);
	}
	/**
	* We have been scared off, so we the correct number of geese at that location before we fly off to find a new forage location
	*/
	if (m_myForageIndex != -1)
	{
		m_OurPopulationManager->RemoveGeeseFromForageLocation(m_myGooseSpeciesType, m_myForageIndex, m_groupsize);
	}
	m_myForageIndex = -1;
	CurrentGState = st_ChooseForageLocation();
}
//--------------------------------------------------------------------------------------------------------

void Goose_Base::KillThis()
{
	/** 
	* Kill this for single animal agents is straightforward. We need to remove the bird from the forage area if any.
	*/
	if (m_myForageIndex != -1)
	{
		m_OurPopulationManager->RemoveGeeseFromForageLocation(m_myGooseSpeciesType, m_myForageIndex, 1);
	}
    delete m_MyMemory; // this part is relevant for the Nonbreeeders which otherwise do not have their memory recycled
	TAnimal::KillThis(); // Sets CurrentStateNo to -1, and StepDone to true
}
//--------------------------------------------------------------------------------------------------------

/** 
* We need to migrate out of the simulation and record the reason for leaving. This is currently done by
* killing the bird. No birds are migrating back into the simulation. 
* \param [in] a_leavereason The reason for leaving. One of the values in the enum TTypeOfLeaveReason.
*/

void Goose_Base::On_Migrate(TTypeOfLeaveReason a_leavereason)
{
	CurrentGState = togs_Die;
	m_OurPopulationManager->RecordLeaveReason(a_leavereason, m_myGooseSpeciesType);
}
//-------------------------


double Goose_Base::AdjustIntakeRateToSnowDepth(double a_intakerate) {
	if (m_SnowDepth > 0.0) {
		a_intakerate = a_intakerate * (1 - (m_SnowDepth * m_OurPopulationManager->GooseCfg.cfg_goose_snow_scaler.value()));
	}
	return a_intakerate;
}
//-------------------------

int Goose_Base::GetForagingTime(double a_RoostLeaveTime)
{
    //param [in] a_EndForagingTime The time of day where a goose hits it's daily max intake <- removed after dailymaxappetite gone obsolete
	int foraging_time;
	int day_length = m_OurLandscape->SupplyDaylength();
	int roost_leave_time = (int)a_RoostLeaveTime;

//	cout<< "Roost Leave: "<<roost_leave_time;
//  cout<< " End Foraging: "<<a_EndForagingTime<<'\n';
	if (roost_leave_time < 0) {
		roost_leave_time = 0;
	}

	foraging_time = (day_length + m_OurPopulationManager->GooseCfg.cfg_goose_AfterDarkTime.value()) - roost_leave_time;



	if (foraging_time == -14000)
	{
		m_OurLandscape->Warn("Goose_Base::CalcForagingTime()","error in foraging time calculation");
		exit(0);
	}
	if (foraging_time<-10){
        m_OurLandscape->Warn("Goose_Base::CalcForagingTime()"," error foraging time is bigger than tick (10 mins)");
        exit (0);
	}

	if (foraging_time<0)
	    // If we get here, it is probably because the LeaveNest time is just a tiny bit bigger than the EndForaging time,
	    // so probably no problem here
	    foraging_time=0;
    int rest_time=GetRestTime();

    if (rest_time>day_length+ m_OurPopulationManager->GooseCfg.cfg_goose_AfterDarkTime.value())
        cout<<"DEBUG: rest is longer than a day"<<endl;
    else{
        if (foraging_time>day_length+ m_OurPopulationManager->GooseCfg.cfg_goose_AfterDarkTime.value())
            cout<<"DEBUG: forage+rest is longer than a day"<<endl;
    }
	return foraging_time-rest_time;
}

int Goose_Base::GetRestTime() const{
    return m_Rest_counter*10;
}

void Goose_Base::ResetRestTime(){
    m_Rest_counter=0;
}

void Goose_Base::IncRestTime(){
    m_Rest_counter++;
}
/*********************************************************************************************************/
