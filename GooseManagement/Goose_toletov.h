//
// Created by andrey on 10/19/22.
//

#ifndef ALMASS_GOOSE_TOLETOV_H
#define ALMASS_GOOSE_TOLETOV_H
//
// Created by andrey on 2/5/21.
//


#include <Landscape/ls.h>
# include <unordered_set>
#include <utility>
using namespace std;
//using TToleList=std::unordered_set<TTypesOfLandscapeElement>;
using TTovList=std::unordered_set<TTypesOfVegetation> ;
class GooseTovParams{
public:
    TTovList getList();
    explicit GooseTovParams(TTovList);
private:
    TTovList GooseTovList;
};
/*
class BeetleToleParams{
public:
    TToleList getList();
    explicit BeetleToleParams(TToleList);
private:
    TToleList BeetleToleList;
};
 */

typedef struct GooseToleTovs{
/*
BeetleToleParams BeetleStartHabitats{TToleList{
        tole_Field,
        tole_Orchard,
        tole_Vineyard,
        tole_PermPasture,
        tole_PermPastureLowYield,
        tole_PermPastureTussocky,
        tole_RoadsideVerge,
        tole_NaturalGrassDry,
        tole_NaturalGrassWet,
        tole_FieldBoundary,
        tole_UnsprayedFieldMargin,
        tole_YoungForest,
        tole_WaterBufferZone
}};
 */
// The list of cereals that could grow in stubble.
// 
GooseTovParams GooseCanGrowInStubbleCereals{TTovList{
        tov_DKOWinterRape,
        tov_DKWinterRape
}};

} TGooseToleTovs;




#endif //ALMASS_GOOSE_TOLETOV_H
