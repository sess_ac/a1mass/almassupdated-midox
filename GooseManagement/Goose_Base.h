/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Goose_Base.h This is the header file for the goose base class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 8th February 2013 \n
*/
//---------------------------------------------------------------------------

#ifndef Goose_BaseH
#define Goose_BaseH
//#include "Landscape/LandscapeFarmingEnums.h"
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

#include "Landscape/LandscapeFarmingEnums.h"
#include "BatchALMaSS/ALMaSS_Setup.h"
#include "ALMaSSDefines.h"
#include "Landscape/Landscape.h"
#include "BatchALMaSS/PopulationManager.h"

class APoint;
class TAnimal;
class GooseMemoryMap;
class Goose_Base;
class Goose_Population_Manager;


//------------------------------------------------------------------------------
/**
Used for the population manager's list of Goose_Base
*/
//typedef vector<Goose_Base*> TListOfGoose_Base;
//---------------------------------------------------------------------------

/**
The enumeration lists all possible goose types. 
*/
typedef enum
{
   gst_PinkfootFamilyGroup = 0, /*!< Pinkfoot family groups */
   gst_PinkfootNonBreeder, /*!< Pinkfoot non-breeders */
   gst_BarnacleFamilyGroup, /*!< Barnacle family groups */
   gst_BarnacleNonBreeder, /*!< Barnacle non-breeders */
   gst_GreylagFamilyGroup, /*!< Greylag family groups */
   gst_GreylagNonBreeder, /*!< Greylag non-breeders */
   gst_foobar /*!< Default */
} GooseSpeciesType;


/**
Goose_Base like other ALMaSS animals work using a state/transition concept.
These are the Goose_Base behavioural states, these need to be altered, but some are here just to show how they should look.
*/
typedef enum
{
	togs_InitialState=0, /*!< The initial goose state */
	togs_ChooseForageLocation, /*!< The choose forage location state */
	togs_Forage, /*!< The forage state */
	togs_Rest, /*!< The resting state */
	togs_ToRoost, /*!< The state when the goose is heading back to the roost */
	togs_Roost, /*!< The state when the goose is roosting */
	togs_Die, /*!< The state when the goose is dying */
	togs_Emigrate, /*!< The state when a goose is leaving the simulation */
	togs_foobar /*!< Default */
} TTypeOfGoose_BaseState;

/**
This enumeration holds the different food sources.
Handy when max intake rate needs to be determined and remembered.
*/
typedef enum
{
	tomis_grass = 0, /*!< Grass was the intake source which gave the max intake */
	tomis_sowncrop, /*!< The max intake source was a sowncrop (e.g. cereal) */
	tomis_maize, /*!< Maize was the intake source which gave the max intake */
	tomis_grain, /*!< Grain was the intake source which gave the max intake */
	tomis_foobar /*!< Default */
} TTypeOfMaxIntakeSource;

/**
This enumeration holds the different reasons for leaving the simulation.
*/
typedef enum
{
	tolr_migration = 0, /*!< Bird left as a result of migration */
	tolr_bodycondition, /*!< Bird left as a result of poor body condition */
	tolr_leanweight, /*!< Bird left as it hit lean weight */
	tolr_foobar /*!< Default */
} TTypeOfLeaveReason;

/** 
A class to hold data required to classify the habitat usage of geese
*/
class TMaxIntakeSource
{
public:
  /** \brief The intake source which gave the maximum intake */
  TTypeOfMaxIntakeSource m_maxintakesource{tomis_foobar};
  /** \brief Flag for whether the intake source is a cereal */
  bool m_iscereal{false};
  /** \brief The vegetation type */
  TTypesOfVegetation m_veg{tov_None};
  /** \brief Flag to indicate if the intake source was in stubble */
  bool m_instubble{false};
  /** \brief The previous sown crop on the forage location */
  TTypesOfVegetation m_prevsowncrop{tov_None};
};

/** \brief
* A class to describe the goose base
*/
class Goose_Base : public TAnimal
{
   /**
   A Goose_Base must have some simple functionality:
   Inititation
   Movement
   Dying

   Inherits m_Location_x, m_Location_y, m_OurLandscape from TAnimal
   NB All areas are squares of size length X length
   */

protected:
   /** \brief
   Variable to record current behavioural state
   */
   TTypeOfGoose_BaseState CurrentGState{togs_foobar};
   /** \brief
   This is a time saving pointer to the correct population manager object
   */
   Goose_Population_Manager*  m_OurPopulationManager{};
   /** \brief
   Sex of bird, true = male false = female
   */
   bool m_sex{};
   /** \brief
   Holds goose species.
   */
   GooseSpecies m_myspecies{gs_foobar};
   /** \brief
   Holds the goose species type.
   */
   GooseSpeciesType m_myGooseSpeciesType{gst_foobar};
   /** \brief
   The current roost location
   */
   APoint m_MyRoost;
   /** \brief
   The size of the group this bird represents
   */
   int m_groupsize{};
   /** \brief
   The weight of the bird in gram excluding fat reserve
   */
   double m_weight{};
   /** \brief
   The weight of the bird in g including fat reserve
   */
   double m_weightTotal{};
   /** \brief
   The number of forage locations visited today.
   */
   int m_forageLocCount{};
   /** \brief
   The daily BMR
   */
   double m_BMR{};
   /** \brief
   The daily energy budget excluding flying (kJ)
   */
   double m_DEB{};

   /** \brief
   The current snow depth. Held here for speed optimization.
   */
   static double m_SnowDepth;
   /** \brief
   The bird's daily energy account (kJ)
   */
   double m_energyToday{};
   /** \brief
   Holds information about the food source which gave max intake rate
   */
   TMaxIntakeSource m_MaxIntakeSource;
    /** \brief
     Holds information on how full the crop is; It will raise when the geese are foraging, decrease when the
     geese are resting
    */
    float m_FoodMassInCrop{0};
    /** \brief
    Holds information on maximum crop size: varies among the different species and potentially among the different individuals
    */
    float m_MaxCropSize{};
   /** \brief
   Controls the time when the goose will leave the roost
   */
   double m_LeaveRoostTime{};
   /** \brief
   Flag to indicate that we are coming from the roost. Used to control code for "following"
   */
   bool m_LeavingRoost{};
   /** \brief
   The bird's energy reserves in grams
   */
   double m_energyReserve{};
   /** \brief
   The cost of flight per g per m in kJ
   */
   static double m_flightcost;
   /** \brief
   The minimum tolerable forage rate in kJ/minute for the individual
   */
   double m_Indivmingooseforagerate{0.0};
   /** \brief Number of rest bouts experienced during the day
    */
   int m_Rest_counter{0};
   /** \brief
   The individual forage variability between birds is used to give a bird specific forage rate variability.
   */
   double m_individualforagevariation{};
   /** \brief
   The minimum tolerable forage rate in kJ/minute for the species
   */
   static double m_mingooseforagerate[ gs_foobar ];
   /** \brief
   The maximum distance travelled from roost to forage sites for each species of goose (m)
   */
   static double m_GooseForageDist[gs_foobar];
   /** \brief
   Double the maximum distance travelled from roost to forage sites for each species of goose (m) - this is a speed optimisation
   */
   static double m_GooseForageDistX2[gs_foobar];
   /** \brief
   The maximum distance travelled from roost to forage sites for each species of goose (m)
   */
   static double m_GooseFieldForageDist[gs_foobar];
   /** \brief
   Double the maximum distance travelled from roost to forage sites for each species of goose (m) - this is a speed optimisation
   */
   static double m_GooseFieldForageDistX2[gs_foobar];
   /** \brief
   The trigger for leaving as a 5 day average of body condition
   */
   static double m_GooseLeavingThreshold;
   /** \brief
   Scaler to determine the max kJ possible to eat per day based on non-energy reserve weight
   */
   static double m_GooseMaxAppetiteScaler;
   /** \brief
   Max proportion of weight that can be energy reserve
   */
   static double m_GooseMaxEnergyReserveProportion;
   /** \brief
   Conversion rate kJ to fat
   */
   static double m_GooseKJtoFatConversion;
   /** \brief
   Conversion rate fat to kJ
   */
   static double m_GooseFattoKJConversion;
   /** \brief
   The minimum openness value a goose will tolerate for forage.
   */
   static double m_GooseMinForageOpenness;
   /** \brief
   A variable describing current body condition over the past 5 days
   */
   double m_BodyCondition[5]{};
   /** \brief
   A variable describing current body condition averaged over 5 days
   */
   double m_TheBodyCondition{};
   /** \brief
   A counter for the daily body condition
   */
   unsigned m_BodyConditionCounter{};
   /** \brief
   Temporary storage for a forage location index - this is a speed optimisation
   */
   int m_myForageIndex{};
   /** \brief
   Temporary storage for a memory location polygon id
   */
   int m_myMemPolyID{};
   /** \brief
   The Goose Memory
   */
   GooseMemoryMap* m_MyMemory{};
	/** \brief
	An attribute used to determine the chance of following or exploring when foraging.
	*/
   static int m_followinglikelyhood[ (int)gst_foobar ];
   /** \brief
   Counter for the number of flights per day
   */
   int m_FlightNumber{};
   /** \brief
   Storage for the total distance flown per day
   */
   int m_FlightDistance{};
   /** \brief
   How many timesteps since last empty crop
   */
   int m_LastEmptyCrop{0};

    /** \brief
    Supply max size of crop
    */
    float GetMaxCropSize() const ;
    /** \brief
    Supply the size of crop
    */
    float GetCropSize() const {return m_FoodMassInCrop;};
    /** \brief
Supply how many steps ago the crop was empty
*/
    int GetLastEmptyCrop() const {return m_LastEmptyCrop;};
    /** \brief Increment last empty crop */
    void IncLastEmptyCrop()  {m_LastEmptyCrop++;};
    /** \brief Reset last empty crop */
    void ResetLastEmptyCrop()  {m_LastEmptyCrop=0;};
    void ResetCrop(){m_FoodMassInCrop=0;};
    void RemoveFoodFromCrop();
public:
   /** \brief
   Goose_Base constructor
   */
	Goose_Base(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);

    virtual /** \brief Intitialise object */
	void Init(Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);

    virtual /** \brief ReInit for object pool */
	void ReInit(Landscape* p_L, Goose_Population_Manager* p_NPM, double a_weight, bool a_sex, APoint a_roost);
	/** \brief
   Goose_Base destructor
   */
	~Goose_Base() override;

    /** \brief
    Update max size of crop
    */
    virtual inline void UpdateMaxCropSize();
    /** \brief
    Get chance of foraging initiation when in rest
    */
    static double GetForagingChance(double ratio) ;
   /** \brief
   Behavioural state dying
   */
   void st_Dying( );
   /** \brief
   Behavioural exploration of the area to find forage.
   */
   void Explore();
    /** \brief
    Behavioural state of Resting in the Field

     It occurs when the consumed too much food to proceed with foraging, but still not enough for today
    */
   TTypeOfGoose_BaseState st_Rest();
   /** \brief
   Behavioural forage
   */

   TTypeOfGoose_BaseState st_Forage();
   /** \brief
   Return to roost and assess whether to forage again that day
   */
   virtual TTypeOfGoose_BaseState st_ToRoost();
   /** \brief
   Supply roost coords
   */
   APoint GetRoost() { return m_MyRoost; }
   /** \brief
   Supply the current max intake source
   */
   TMaxIntakeSource GetMaxIntakeSource() {
	   return m_MaxIntakeSource;
   }
   /** \brief
   Find the closest roost
   */
   virtual void ChangeRoost() { ; }
   /** \brief
   Roosting until next forage or next morning
   */
   TTypeOfGoose_BaseState st_Roost();
   /** \brief
   Pick a hop location point within a_dist meters - must be overridden by descendent classes
   */
   virtual APoint ChooseHopLoc() { return m_MyRoost; }  
   /** \brief
   Do a forage evaluation from current location to a_HopLoc
   */
   void EvaluateForageToHopLoc( APoint a_HopLoc );
   /** \brief
   Do any 'housekeeping' associated with the start of day
   */
   virtual void StartDay();
   /** \brief
   The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep (10 minutes).
   */
   void BeginStep() override { StartDay(); }
   /** \brief
   The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'.
   */
   void Step() override;
   /** \brief
   The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep.
   */
   void EndStep() override{} // NB this is not used in the Goose_Base code
   /** \brief 
   Cause the bird to immediately transition to Emigrate
   */
   virtual void On_Emigrate() 
   {
	   CurrentGState = togs_Emigrate; 
   }
   /** \brief
   Returns the current body condition
   */
   double GetBodyCondition() const { return m_TheBodyCondition; }
    /** \brief
 Updates the crop fullness when Foraging: can be potentially overriden by a specific goose model
 */
   virtual void UpdateCropAtForage(double foragerate);
    /** \brief
Updates the crop fullness when resting: can be potentially overriden by a specific goose model
*/
    virtual void UpdateCropAtRest();
   /** \brief
   Sets the current body condition
   */
   void SetBodyCondition(double a_condition, unsigned a_index) { m_BodyCondition[a_index] = a_condition; }
   /** \brief
   Alters body conditon by addition of a double value
   */
   void AddBodyCondition(double a_condition) { m_TheBodyCondition += a_condition; }
   /** \brief
   Alters body conditon by subtraction of a double value
   */
   void SubtractBodyCondition( double a_condition) { m_TheBodyCondition -= a_condition; }
   /** \brief
   Returns the groupsize
   */
   int GetGroupsize() const { return m_groupsize; }
   /** \brief
   Scales forage to groupsize
   */
   double ScaleForageToGroupsize(double a_forage) const { return m_groupsize * a_forage; }
   /** \brief
   Gets for the forage location index
   */
   int GetForageLocIndex() const { return m_myForageIndex; }
   /** \brief
   Returns the species
   */
   GooseSpecies GetSpecies() {
	   return m_myspecies;
   }
   /** \brief
   Returns the sex
   */
    bool GetSex() const { return m_sex; }

   /** \brief
   Returns the total daily number of flights
   */
    int GetFlightNumber() const {
	   return m_FlightNumber;
   }
   /** \brief
   Returns the total daily distance flown
   */
   int GetFlightDistance() const {
	   return m_FlightDistance;
   }
   /** \brief
   Returns the daily energy budget
   */
   double GetDailyEnergyBudget() const {
	   return m_DEB;
   }
    /** \brief
 Sets the daily energy budget
 */
   void SetDailyEnergyBudget(double a_DEB)  {
        m_DEB=a_DEB;
    }
   /** \brief
   Returns the daily energy expenditure
   */
   double GetDailyEnergyExpenditure() const {
	   return m_energyToday;
   }
   /** \brief
   Returns the time spent foraging
   \param [in] a_RoostLeaveTime The time of day where a goose leaves the roost
   \return The difference between roost leave time and end foraging time
   */
   int GetForagingTime(double a_RoostLeaveTime);
   /** \brief
   Returns the minimum forage rate for the species
   \param [in] a_species The type of goose species
   \return The minimum forage rate the goose will accept.
   */
   static double GetGooseMinForageRate(GooseSpecies a_species)
   {
       return m_mingooseforagerate[a_species];
   }
   /** \brief
   Returns the max forage rate of the three different types of forage
   The max rate is adjusted according to feeding time in the time budget, according to the interference competition value
   and to the current snow depth in the landscape. 
   \param [in] a_grain The intake rate from the grain resource
   \param [in] a_maize The intake rate from the maize resource
   \param [in] a_grass The intake rate from the grazing resource
   \param [in] a_interferenceComp The scaler for interference competition
   \param [in] a_iscereal Flag for a cereal crop or not (false)
   \param [in] a_veg The vegetation type
   \param [in] a_stubble Flag for a crop in stubble or not (false)
   \param [in] a_prevcrop The previously sown crop
   \return The max forage rate scaled to time budget and interference competetion
   */
   double GetMaxForageRate(double a_grain, double a_maize, double a_grass, double a_interferenceComp, bool a_iscereal, TTypesOfVegetation a_veg, bool a_stubble, TTypesOfVegetation a_prevcrop);

   /** \brief
   Returns the max intake rate of the three different types of intake
   \param [in] a_grain The intake rate from the grain resource kJ/min
   \param [in] a_maize The intake rate from the maize resource kJ/min
   \param [in] a_grass The intake rate from the grazing resource kJ/min
   \return The max intake rate kJ/min
   */
   static double GetMaxIntakeRate( double a_grain, double a_maize, double a_grass) {
	   double MaxRate = 0.0;
	   if (a_grain > MaxRate) {
		   MaxRate = a_grain;
	   }
	   if (a_maize > MaxRate) {
		   MaxRate = a_maize;
	   }
	   if (a_grass > MaxRate) {
		   MaxRate = a_grass;
	   }
	   return MaxRate;
   }
   /** \brief
   Sets the sex
   */
   void SetSex( bool a_sex ) {
	   m_sex = a_sex;
   }
   /** \brief
   Returns the total weight of the bird
   */
   double GetTotalWeight() const {return m_weightTotal;}
    /** \brief
 Returns the weight of the bird
 */
    double GetWeight() const {return m_weight;}

    /** \brief Returns the time when the goose leaves the roost */
   double GetRoostLeaveTime() const { return m_LeaveRoostTime; }
   /** \brief
   Set the flight cost per m per g
   */
   static void SetFlightCost(double a_cost) { m_flightcost = a_cost; }
   /** \brief
   Get the flight costs per m per g
   */
   double GetFlightCost() const {
	   return m_flightcost * GetTotalWeight();
   }
   /** \brief
   Set the mimimum tolerated forage rate for all geese
   */
   static void Set_mingooseforagerate(double a_cost, GooseSpecies a_species) { m_mingooseforagerate[a_species] = a_cost; }
   /** \brief
   Set the mimimum tolerated forage rate for the individual goose
   */
   void Set_Indivmingooseforagerate( double a_cost) {
	   m_Indivmingooseforagerate = a_cost;
   }
   /** \brief
   Set the body condition threshold for leaving for all geese 
   */
   static void Set_GooseLeavingThreshold(double a_threshold) { m_GooseLeavingThreshold = a_threshold; }
   /** \brief
   Set the goose appetite scale used to calculate max intake for all geese
   */
   static void Set_GooseMaxAppetiteScaler(double a_cost) { m_GooseMaxAppetiteScaler = a_cost; }
   /** \brief
   Set the maximum energy reserve proportion allowed for all geese
   */
   static void Set_GooseMaxEnergyReserveProportion(double a_prop) { m_GooseMaxEnergyReserveProportion = a_prop; }
   /** \brief
   Set the kJ to fat conversion constant for all geese
   */
   static void Set_GooseKJtoFatConversion(double a_cost) { m_GooseKJtoFatConversion = a_cost; }
   /** \brief
   Set the fat to kJ conversion constant for all geese
   */
   static void Set_GooseFattoKJConversion(double a_cost) { m_GooseFattoKJConversion = a_cost; }
   /** \brief
   Set the min forage openess for all geese
   */
   static void Set_GooseMinForageOpenness(double a_cost) { m_GooseMinForageOpenness = a_cost; }
   /** \brief
   Set the max forage distance from roost for all geese
   */
   static void Set_GooseForageDist(double sp1, double sp2, double sp3)
   { 
	   //Dist
	   m_GooseForageDist[gs_Pinkfoot] = sp1;
	   m_GooseForageDist[gs_Barnacle] = sp2;
	   m_GooseForageDist[gs_Greylag] = sp3;
	   // DistX2
	   m_GooseForageDistX2[gs_Pinkfoot] = sp1 * 2.0;
	   m_GooseForageDistX2[gs_Barnacle] = sp2 * 2.0;
	   m_GooseForageDistX2[gs_Greylag] = sp3 * 2.0;
   }
   /** \brief
   Set the max forage distance from a field for all geese
   */
   static void Set_GooseFieldForageDist(double sp1, double sp2, double sp3)
   {
	   //Dist
	   m_GooseFieldForageDist[gs_Pinkfoot] = sp1;
	   m_GooseFieldForageDist[gs_Barnacle] = sp2;
	   m_GooseFieldForageDist[gs_Greylag] = sp3;
	   // DistX2
	   m_GooseFieldForageDistX2[gs_Pinkfoot] = sp1 * 2.0;
	   m_GooseFieldForageDistX2[gs_Barnacle] = sp2 * 2.0;
	   m_GooseFieldForageDistX2[gs_Greylag] = sp3 * 2.0;
   }
   /** \brief Set the followinglikelyhood*/
   static void Set_GooseFollowingLikelyhood( int a_likelyhood, GooseSpeciesType a_speciestype) { m_followinglikelyhood[a_speciestype] = a_likelyhood;}
   /** \brief Set the flag to indicate if we are coming from the roost */
   void Set_GooseLeavingRoost( bool a_leaving) {m_LeavingRoost = a_leaving;}
   /** \brief
   The goose is moved to the location specified by a_x, a_y - caused by group decision
   */
    void On_MoveTo(int a_x, int a_y ) { FlyTo( a_x, a_y); }
   /** \brief
   The bird is dead of some external cause
   */
   void KillThis() override;
   /** \brief
   The goose is scared by a bang at location
   */
   void On_Bang(int a_polyid );
   /** \brief
   The goose is scared by a bang at location
   */
   void On_Bang(int a_polyid, double a_scare );
   /** \brief
   The goose is told to leave the simulation area (poss due to bad weather)
   */
   void On_Migrate(TTypeOfLeaveReason a_leavereason);
   /** \brief When there is snow, the intake rate is decreased by 10 % per cm of snow */
   double AdjustIntakeRateToSnowDepth(double a_intakerate);
protected:
	/** \brief
	Selects a forage location based on past memory, or initiates an exploration
	*/
	TTypeOfGoose_BaseState st_ChooseForageLocation( );
	/** \brief
	The goose flys to the location specified by a_x, a_y
	*/
	void FlyTo(int a_x, int a_y );
	/** \brief
	The goose flys to the location specified by a_pt
	*/
	void FlyTo( APoint a_pt );

    int GetRestTime() const;

    void ResetRestTime();

    void IncRestTime();

    int GetThrouputTime() const;

    static double GetDynamicThreshold(double ratio);
};

#endif
