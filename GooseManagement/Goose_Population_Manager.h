/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Goose_Population_Manager.h This is the header file for the goose population manager class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 21st February 2013 \n
*/
//---------------------------------------------------------------------------

#ifndef Goose_Population_ManagerH
#define Goose_Population_ManagerH
#include "../People/Hunters/Hunters_all.h"
#include "../People/Hunters/Disturbers_All.h"
#include "Goose_toletov.h"
#include <random>
#include <blitz/array.h>
//---------------------------------------------------------------------------

class Goose;
class Goose_Population_Manager;
class AOR_Probe_Goose;
class probability_distribution;
class GooseHunter;

//------------------------------------------------------------------------------
/** \brief a list of goose roosts as points */
typedef std::vector<APoint> roostlist;

/**
\brief
Used for creation of a new Goose object
*/
class struct_Goose
{
 public:
  /**
  \brief the sex
  */
  bool m_sex{true};
  /**
  \brief is part of a family: discontinued-- unused
  */
  //bool m_family;
  /**
  \brief Size of family unit if any
  */
  int m_grpsize{1};
  /**
  \brief x-coord
  */
  int m_x{-1};
  /**
  \brief y-coord
  */
  int m_y{-1};
  /**
  \brief species ID
  */
  int m_species{0};
  /**
  \brief
  The weight
  */
  double m_weight{-1};
  /**
  \brief Landscape pointer
  */
  Landscape* m_L{nullptr};
  /** \brief
  Goose_Population_Manager pointer
  */
  Goose_Population_Manager * m_GPM{nullptr};
  /** \brief
  Roost location
  */
  APoint m_roost;
};

/**
\brief
A class to hold an active goose foraging location and the number of birds of different types there
*/
class GooseActiveForageLocation
{
protected:
	/**
	\brief
	A landscape element ( LE ) reference number used by the Landscape class to identify this location.
	*/
	int m_polygonref{-1};
	/**
	\brief
	The area of feeding location.
	*/
	double m_area{0};
	/**
	\brief
	The grain density in kJ/m2
	*/
	double m_graindensity{0};
	/**
	\brief
	The maize density in kJ/m2
	*/
	double m_maizedensity{0};
	/**
	\brief
	The grazing intake rate in kJ/min
	*/
	double m_grazingrate[gs_foobar]{0};
	/** 
	\brief 
	Contains the total amount of grazing eaten today 
	*/
	double m_grazedbiomass{0};
	/**
	\brief
	The total grain kJ
	*/
	double m_grainKJ_total;
	/**
	\brief
	The total maize kJ
	*/
	double m_maizeKJ_total;
	/**
	\brief
	Whether it is a cereal crop, grass etc
	*/
	TTypeOfMaxIntakeSource m_HabitatType;
	/**
	\brief
	An array holding the number of geese of different type i.e. Pinkfoot families, juvs, Barnacle families, juvs, Greylag familes, juvs
	*/
	int m_BirdsPresent[gst_foobar]{0};
	/**
	\brief
	An array holding the maximum number of geese of different types and total i.e. Pinkfoot families, juvs, Barnacle families, juvs, Greylag familes, juvs present on the field at one time 
	for each day.
	*/
	int m_MaxBirdsPresent[gst_foobar]{0};
	/** \brief
	An array holding the distance to the closest roost.
	*/
	double m_dist_to_closest_roost[gs_foobar]{0};
	/** \brief
	This is a time saving pointer to the correct population manager object
	*/
	Goose_Population_Manager*  m_OurPopulationManager;
public:
	/**
	\brief
	Constructor for GooseActiveForageLocation
	*/
	GooseActiveForageLocation(GooseSpeciesType a_type, int a_number, int a_polyref, int a_area, double a_graindensity, double a_maizedensity, double * a_grazing, Goose_Population_Manager* p_NPM);
	/**
	\brief
	Set the area in m
	*/
	void SetArea( int a_area) { m_area = a_area; }
	/**
	\brief
	Get the area in m
	*/
	double GetArea() const { return m_area; }
	/**
	\brief
	Get the landscape element ( LE ) reference number used by the Landscape class to identify this location.
	*/
	int GetPolygonref() const { return m_polygonref; }
	/**
	\brief
	Sum of the maximum of all geese present (this may not be the same as the real maximum)
	*/
	int GetMaxBirdsPresent()
	{ 		
		int sum=0; 
		for (int i = (int) 0; i<gst_foobar;i++) 
			sum+=m_MaxBirdsPresent[i]; 
		return sum; 
	}
	/**
	\brief
	Sum of the maximum of each goose species present (this may not be the same as the real maximum)
	*/
	int GetMaxSpBirdsPresent(GooseSpecies a_goose) {
		int sum = 0;
			switch (a_goose) {
				case gs_Pinkfoot:
					sum += (m_MaxBirdsPresent[ gst_PinkfootFamilyGroup ] + m_MaxBirdsPresent[ gst_PinkfootNonBreeder ]);
					break;
				case gs_Barnacle:
					sum += (m_MaxBirdsPresent[ gst_BarnacleFamilyGroup ] + m_MaxBirdsPresent[ gst_BarnacleNonBreeder ]);
					break;
				case gs_Greylag:
					sum += (m_MaxBirdsPresent[ gst_GreylagFamilyGroup ] + m_MaxBirdsPresent[ gst_GreylagNonBreeder ]);
					break;
				default:
					return -1;
			}
			return sum;
		}
		
	/**	\brief	Set the landscape element ( LE ) reference number used by the Landscape class to identify this location.*/
	void SetPolygonref(int a_polyref) { m_polygonref = a_polyref; }
	/**	\brief	Returns the number of huntable birds at the location */
	int GetHuntables();
	/**
	\brief
	Returns the total number of geese at the location.
	*/
	int GetGooseNumbers( ) {
		int sum = 0;
		for (int i : m_BirdsPresent) {
			sum += i;
		}
		return sum;
	}
	/**
	\brief
	Returns the density of geese at the location.
	*/
	double GetGooseDensity( ) {
		//return GetGooseNumbers() / m_area;
		return GetGooseNumbers();  /** @todo Testing a new implementation which uses flock size and not density.*/
	}
	/**
	\brief
	Get how many birds of a type
	*/
	int GetBirds(GooseSpeciesType a_type) { return m_BirdsPresent[(int)a_type]; }
	/**
	\brief
	Get how many birds of a species
	*/
	int GetBirds( GooseSpecies a_goose ) {
				int sum = 0;
		switch (a_goose) {
			case gs_Pinkfoot:
				sum += (GetBirds(gst_PinkfootFamilyGroup) + GetBirds(gst_PinkfootNonBreeder));
				break;
			case gs_Barnacle:
				sum += (GetBirds(gst_BarnacleFamilyGroup) + GetBirds(gst_BarnacleNonBreeder));
				break;
			case gs_Greylag:
				sum += (GetBirds(gst_GreylagFamilyGroup) + GetBirds(gst_GreylagNonBreeder));
				break;
			default:
				return -1;
		}
		return sum;
	}
	
	/**\brief
	Adds geese to the location.
	*/
	void AddGeese(GooseSpeciesType a_type, int a_number) 
	{
		m_BirdsPresent[(int)a_type] += a_number; 
		if (m_BirdsPresent[(int)a_type] > m_MaxBirdsPresent[(int)a_type])
		{
			m_MaxBirdsPresent[(int)a_type] = m_BirdsPresent[(int)a_type];
		}
	}
	/**
	\brief
	Removes geese to the location.
	*/
	void RemoveGeese(GooseSpeciesType a_type, int a_number) { 
		m_BirdsPresent[a_type] -= a_number; 
#ifdef __DEBUG_GOOSE
		if (m_BirdsPresent[a_type]<0) 
		{
			g_msg->Warn("GooseActiveForageLocation::RemoveGeese - Negative birds at forage location", (double)m_BirdsPresent[a_type]);
			exit(0);
		}
#endif
	}
	/**
	\brief
	Returns the current forage density (grain/m2)
	*/
	double GetGrainDensity() const { return m_graindensity; }
	/**
	\brief
	Sets forage density (grain/m2)
	*/
	void SetGrainDensity(double a_density) { m_graindensity = a_density; }
	/**
	\brief
	Returns the current maize forage density (kJ/m2)
	*/
	double GetMaizeDensity() const {
		return m_maizedensity;
	}
	/**
	\brief
	Returns whether the current crop is a cereal
	*/
     bool GetHabitatType() {
		return m_HabitatType;
	}
	/**
	\brief
	Sets maize forage density (kJ/m2)
	*/
	void SetMaizeDensity( double a_density ) {
		m_maizedensity = a_density;
	}
	/**
	\brief
	Returns the current forage rate kJ/min assuming no other geese affect this
	*/
	double GetGrazing(int gs) { return m_grazingrate[gs]; }
	/**
	\brief
	Returns the distance to the closest roost for gs
	*/
	double GetRoostDist(int gs) { return m_dist_to_closest_roost[gs]; }
	/**
	\brief
	Returns the current grazing forage eaten in g
	*/
	double GetGrazedBiomass() const { return m_grazedbiomass; }
	/**
	\brief
	Sets forage density (kJ/m2)
	*/
	void SetGrazing(int gs, double a_density) { m_grazingrate[gs] = a_density; }
	/** \brief	Sets the distance to the closest roost in meters */
	void SetDistToClosestRoost(int gs, double a_dist) { m_dist_to_closest_roost[gs] = a_dist; }
	/**
	\brief
	Sets grazed biomass to zero
	*/
	void ResetGrazing() { m_grazedbiomass = 0.0; }
	
	/**	\brief Removes grain from the field as kJ.  */
	/** Assimilation index is 0.404 1/0.404=2.4752 */
	/** Digestibility of grain assumed to be 0.695 1/0.695=1.4388 */
	/** 2.4752*1.4388=3.5613 */
	void RemoveGrainKJ(double a_kJ) { m_grainKJ_total -= (a_kJ*1.4388); }
	
	/**
	\brief
	Removes maize from the field as kJ
	*/
    /** Assimilation index is 0.404 1/0.404=2.4752 */
    /** Digestibility of maize assumed to be 0.81 (according to Kevin via email, 0.87 is a typo) 1/0.81=1.2346 */
    /** 1.1494*2.4752= 2.8450*/
	void RemoveMaizeKJ( double a_kJ ) {
		m_maizeKJ_total -= (a_kJ*1.2346);
	}
	/**
	\brief
	Records forage removed as kJ from the field as grams
	*/
    /** Assimilation index is 0.404 1/0.404=2.4752 */
    /** Digestibility of grass is m_digestibility and
     * it will be taken into account in the later stage (GrazeVegetationTotal(poly, weight))*/
    /** 0.2857*(2.4752)=0.70716 */
	void Graze(double a_kJ) {
		m_grazedbiomass += a_kJ * 0.2857;  /**3.5 kJ/gram ww; Robbins 1993*/
	}
	/**
	\brief
	Empties the bird recording arrays
	*/
	void ClearBirds();
	
	/**	\brief Updates the grain or maize density based on the current total grain or maize amount */
	void UpdateKJ() {
		m_graindensity = (m_grainKJ_total / m_area) / 17.67 / 0.04;  /** 17.67 kJ/g dw grain & 0.04 g/grain*/
		m_maizedensity = m_maizeKJ_total / m_area;
	}
};
/**
 * \brief
 *  The container class that includes al the configuration values used in Goose model
 * **/
class GooseConfigs{
public:
    /**\brief Constructor of the container class for the configs*/
    GooseConfigs();
    // values
    /** \brief The likelyhood that a goose will follow another goose when going foraging - barnacle goose */
    CfgInt cfg_goose_bn_followinglikelyhood{"GOOSE_FOLLOWINGLIKELYHOOD_BN", CFG_CUSTOM, 0, 0, 10000};
    /** \brief The likelyhood that a goose will follow another goose when going foraging - pinkfooted goose */
    CfgInt cfg_goose_pf_followinglikelyhood{"GOOSE_FOLLOWINGLIKELYHOOD_PF", CFG_CUSTOM, 0, 0, 10000};
    /** \brief The likelyhood that a goose will follow another goose when going foraging - graylag goose */
    CfgInt cfg_goose_gl_followinglikelyhood{"GOOSE_FOLLOWINGLIKELYHOOD_GL", CFG_CUSTOM, 0, 0, 10000};
    /** \brief Lean weight of pinkfoot geese */
/** Based on male weights from Norway (Trøndelag) Gundersen et al Submitted 2016.
    assuming a lean of 90% of the measured mean weight on day 265. */
    CfgFloat cfg_goose_pf_baseweight{"GOOSE_PINKFOOTWEIGHT", CFG_CUSTOM, 2378, 2000, 3000};  /**@todo Currently male weight. Should Male, Female, Juvs not have different weights? */
    CfgFloat cfg_goose_pf_baseweight_fem{"GOOSE_FEMALE_PINKFOOTWEIGHT", CFG_CUSTOM, 2188, 1900, 2500};
    CfgFloat cfg_goose_pf_baseweight_juv{"GOOSE_JUVENILE_PINKFOOTWEIGHT", CFG_CUSTOM, 1911, 1600, 2000};
    CfgFloat cfg_goose_pf_baseweight_std{"GOOSE_PINKFOOTWEIGHT_STD", CFG_CUSTOM, 283, 200, 300};
    CfgFloat cfg_goose_pf_baseweight_fem_std{"GOOSE_FEMALE_PINKFOOTWEIGHT_STD", CFG_CUSTOM, 215, 200, 300};
    CfgFloat cfg_goose_pf_baseweight_juv_std{"GOOSE_JUVENILE_PINKFOOTWEIGHT_STD", CFG_CUSTOM, 277, 200, 300};

    /** \brief Lean weight of barnacle geese */
    CfgFloat cfg_goose_bn_baseweight{"GOOSE_BARNACLEWEIGHT", CFG_CUSTOM, 1760, 1500, 2000};  /**@todo Male*/
    CfgFloat cfg_goose_bn_baseweight_fem{"GOOSE_FEMALE_BARNACLEWEIGHT", CFG_CUSTOM, 1620, 1400, 1900};
    CfgFloat cfg_goose_bn_baseweight_juv{"GOOSE_JUVENILE_BARNACLEWEIGHT", CFG_CUSTOM, 1415, 1200, 1500};
    CfgFloat cfg_goose_bn_baseweight_std{"GOOSE_BARNACLEWEIGHT_STD", CFG_CUSTOM, 210, 180, 220};
    CfgFloat cfg_goose_bn_baseweight_fem_std{"GOOSE_FEMALE_BARNACLEWEIGHT_STD", CFG_CUSTOM, 159, 100, 200};
    CfgFloat cfg_goose_bn_baseweight_juv_std{"GOOSE_JUVENILE_BARNACLEWEIGHT_STD", CFG_CUSTOM, 205, 100, 200};

/** \brief Lean weight of greylag geese */
/** @todo From Leif Nilsson (male weight) */
    CfgFloat cfg_goose_gl_baseweight{"GOOSE_GREYLAGWEIGHT", CFG_CUSTOM, 2880, 2500, 3000};
    CfgFloat cfg_goose_gl_baseweight_fem{"GOOSE_FEMALE_GREYLAGWEIGHT", CFG_CUSTOM, 2651, 2300, 3000};
    CfgFloat cfg_goose_gl_baseweight_juv{"GOOSE_JUVENILE_GREYLAGWEIGHT", CFG_CUSTOM, 2315, 2100, 2500};
    CfgFloat cfg_goose_gl_baseweight_std{"GOOSE_GREYLAGWEIGHT_STD", CFG_CUSTOM, 343.84, 300, 400};
    CfgFloat cfg_goose_gl_baseweight_fem_std{"GOOSE_FEMALE_GREYLAGWEIGHT_STD", CFG_CUSTOM, 260.7, 200, 350};
    CfgFloat cfg_goose_gl_baseweight_juv_std{"GOOSE_JUVENILE_GREYLAGWEIGHT_STD", CFG_CUSTOM, 335.7, 200, 450};

    std::normal_distribution<> father_pf_d { cfg_goose_pf_baseweight.value(),cfg_goose_pf_baseweight_std.value()};
    std::normal_distribution<> mother_pf_d {cfg_goose_pf_baseweight_fem.value(),cfg_goose_pf_baseweight_fem_std.value()};
    std::normal_distribution<> young_pf_d {cfg_goose_pf_baseweight_juv.value(),cfg_goose_pf_baseweight_juv_std.value()};

    std::normal_distribution<> father_bn_d{cfg_goose_bn_baseweight.value(),cfg_goose_bn_baseweight_std.value()};
    std::normal_distribution<> mother_bn_d{cfg_goose_bn_baseweight_fem.value(),cfg_goose_bn_baseweight_fem_std.value()};
    std::normal_distribution<> young_bn_d{cfg_goose_bn_baseweight_juv.value(),cfg_goose_bn_baseweight_juv_std.value()};

    std::normal_distribution<> father_gl_d{cfg_goose_gl_baseweight.value(),cfg_goose_gl_baseweight_std.value()};
    std::normal_distribution<> mother_gl_d{cfg_goose_gl_baseweight_fem.value(),cfg_goose_gl_baseweight_fem_std.value()};
    std::normal_distribution<> young_gl_d{cfg_goose_gl_baseweight_juv.value(),cfg_goose_gl_baseweight_juv_std.value()};

    std::normal_distribution<> fathers[3]{father_pf_d,father_bn_d,father_gl_d};
    std::normal_distribution<> mothers[3]{mother_pf_d,mother_bn_d,mother_gl_d};
    std::normal_distribution<> youngs[3]{young_pf_d,young_bn_d,young_gl_d};
    /** \brief the cost of flight in kJ per m per gram */
/** Calculated as 12 * BMR to get cost of flight. We devide this by 24 to per hour.
* this is then devided by an average flight speed of 57.6 km/h (Green & Alerstam 2000 J Avian Biol 31:215-225)
* This gives a cost in kJ/km which is divided by 1000 to meters and mass to get to per gram. */
    CfgFloat cfg_goose_flightcost{"GOOSE_FLIGHTCOST", CFG_CUSTOM, 0.000002079, 0, 1};
/** \brief The maximum forage range in m for pink-footed geese */
    CfgFloat cfg_goose_pf_ForageDist{"GOOSE_FORAGEDIST_PF", CFG_CUSTOM, 35000, 1000, 35000};
/** \brief The maximum forage range in m for barnacle geese */
    CfgFloat cfg_goose_bn_ForageDist{"GOOSE_FORAGEDIST_BN", CFG_CUSTOM, 35000, 1000, 35000};
/** \brief The maximum forage range when already at a forage location for pink-footed geese (m) */
    CfgFloat cfg_goose_pf_FieldForageDist{"GOOSE_FIELDFORAGEDIST_PF", CFG_CUSTOM, 5000, 1000, 35000};
/** \brief The maximum forage range when already at a forage location for barnacle geese (m) */
    CfgFloat cfg_goose_bn_FieldForageDist{"GOOSE_FIELDFORAGEDIST_BN", CFG_CUSTOM, 5000, 1000, 35000};
/** \brief The maximum forage range when already at a forage location for greylag geese (m) */
    CfgFloat cfg_goose_gl_FieldForageDist{"GOOSE_FIELDFORAGEDIST_GL", CFG_CUSTOM, 5000, 1000, 35000};
/** \brief The maximum forage range in m for greylag geese */
    CfgFloat cfg_goose_gl_ForageDist{"GOOSE_FORAGEDIST_GL", CFG_CUSTOM, 35000, 1000, 35000};
/** \brief The cost of BMR per kg goose - constant 1*/
    CfgFloat cfg_goose_BMRconstant1{"GOOSE_BMRCONSTANTA", CFG_CUSTOM, 4.19 * 78.30, 1, 500};
/** \brief The cost of BMR per kg goose - constant 2 */
    CfgFloat cfg_goose_BMRconstant2{"GOOSE_BMRCONSTANTB", CFG_CUSTOM, 0.72, 0, 1};
/** \brief Pinkfooted goose lower critical temperature for calculating thermal regulation cost. Constant 1 */
/** Calculated from Kendeigh 1977 based on body weight 2524g */
    CfgFloat cfg_goose_pf_Thermalconstant{"GOOSE_THERMALCONSTANTA_PF", CFG_CUSTOM, 11.4, 1, 25};
/** \brief Barnacle goose lower critical temperature for calculating thermal regulation cost. Constant 1 */
/** Calculated from Kendeigh 1977 based on body weight 1880g */
    CfgFloat cfg_goose_bn_Thermalconstant{"GOOSE_THERMALCONSTANTA_BN", CFG_CUSTOM, 12.1, 1, 25};
/** \brief Greylag goose lower critical temperature for calculating thermal regulation cost. Constant 1 */
/** Calculated from Kendeigh 1977 based on body weight 3454g */
    CfgFloat cfg_goose_gl_Thermalconstant{"GOOSE_THERMALCONSTANTA_GL", CFG_CUSTOM, 10.8, 1, 25};
/** \brief Thermal regulation cost Constant b */
/** The 1.272 is from Lefebvre & Raveling 1967. Journal of Wildlife Management 31(3): 538-546*/
    CfgFloat cfg_goose_Thermalconstantb{"GOOSE_THERMALCONSTANTB", CFG_CUSTOM, 1.272 * 24, 1, 100}; // kJ per day
/** \brief The maximum a goose is allowed to eat in a day as a scaling of BMR */
/** Calculated from maximum daily food intake and adjusted for energy assimilation and content in winter wheat.
Therkildsen & Madsen 2000, Wildlife Biology 6: 167-172.*/
    CfgFloat cfg_goose_MaxAppetiteScaler{"GOOSE_MAXAPPETITESCALER", CFG_CUSTOM, 3.42, 1, 10};
/** \brief The maximum proportion of weight that can be stored as energy reserves (autumn)*/
/** Calculated based on observed API scores weights and lean weight*/
    CfgFloat cfg_goose_MaxEnergyReserveProportion{"GOOSE_MAXENERGYRESERVEPROPORTION", CFG_CUSTOM, 0.27, 0.1, 1};
/** \brief The maximum proportion of weight in spring */
/** Initially the same as in autumn */
    CfgFloat cfg_goose_MaxEnergyReserveProportionSpring{"GOOSE_MAXENERGYRESERVEPROPORTION_SPRING", CFG_CUSTOM, 0.27,
                                                        0.1, 1};
/** \brief The day when the ene rgy reserve is switched into a spring value */
/** Initially March, 1st (according to Kevin, issue #123)*/
    CfgInt cfg_goose_MaxEnergyReserveProportionChangeDate{"GOOSE_MAXENERGYRESERVEPROPORTION_CHANGEDATE", CFG_CUSTOM, 2,
                                                          0, 365};
/** \brief The day when the ene rgy reserve is switched into a autumn value */
/** Initially June, 1st (according to Kevin, issue #123)*/
    CfgInt cfg_goose_MaxEnergyReserveProportionEndChangeDate{"GOOSE_MAXENERGYRESERVEPROPORTION_ENDCHANGEDATE",
                                                             CFG_CUSTOM, 0, 151, 365};
/** \brief The initial proportion of weight that can be stored as energy reserves */
    CfgFloat cfg_goose_InitialEnergyReserveProportion{"GOOSE_INITIALENERGYRESERVEPROPORTION", CFG_CUSTOM, 0.1, 0.0, 1};
/** \brief The energy content of fat*/
    CfgFloat cfg_goose_EnergyContentOfFat{"GOOSE_ENERGYCONTENTOFFAT", CFG_CUSTOM, 39.8, 1, 100};
/** \brief The metabolic costs of converting tissue to energy or vice versa  */
    CfgFloat cfg_goose_MetabolicConversionCosts{"GOOSE_METABOLICCONVCOSTS", CFG_CUSTOM, 11.4, 1, 100};
/** \brief The minimum openness score that a goose will tolerate for foraging */
/** Distance to closests landscape feature avoided by geese measured from the centre of the polygon. */
    CfgFloat cfg_goose_MinForageOpenness{"GOOSE_MINFORAGEOPENNESS", CFG_CUSTOM, 100.0, 10, 10000};
/** \brief The maximum distance in m that a goose can hear a scary bang */
/** From Gitte Jensen unpubl. */
    CfgInt cfg_goose_MaxScareDistance{"GOOSE_MAXSCAREDIST", CFG_CUSTOM, 500, 1, 5000};
/** \brief The 5-day mean of body condition which triggers the bird to leave */
/** Bodycondition = total weight/lean weight */
    CfgFloat cfg_goose_LeavingThreshold{"GOOSE_LEAVINGTHRESHOLD", CFG_CUSTOM, 1.0, 1.0, 5.0};
/** \brief The number of minutes that geese will be foraging after sunset */
    CfgInt cfg_goose_AfterDarkTime{"GOOSE_AFTERDARKTIME", CFG_CUSTOM, 30, 0, 180};
/**\brief The threshold of the crop fullness (ratio [0,1]) that if the crop is smaller than that we will start checking whether we should start foraging again */
    CfgFloat cfg_goose_CropThresholdToForage{"GOOSE_CROPTHRESHOLDTOFORAGE", CFG_CUSTOM, 0.5, 0, 1.0};
/** \brief To calculate daily energy budget. This is the multiple of BMR spent during daytime
* Default is based on Madsen 1985 ("Relations between change in spring habitat selection...")
*/
    CfgFloat cfg_goose_daytime_BMR_multiplier{"GOOSE_DAYTIMEBMRMULTIPLIER", CFG_CUSTOM, 1.65, 1.0, 10.0};
/** \brief To calculate daily energy budget. This is the multiple of BMR spent during nighttime */
    CfgFloat cfg_goose_nighttime_BMR_multiplier{"GOOSE_NIGHTTIMEBMRMULTIPLIER", CFG_CUSTOM, 1.3, 1.0, 10.0};
/** \brief Chance of changing roost */
    CfgFloat cfg_goose_roostchangechance{"GOOSE_ROOSTCHANGECHANCE", CFG_CUSTOM, 0.0, 0.0, 1.0}; // Default is no change of roost
/** \brief variation in individual forage rates - 0.1 means +/- 5% */
    CfgFloat cfg_gooseindividualforagechoicevariation{"GOOSE_INDIVIDFORAGERATEVAR", CFG_CUSTOM, 0.1, 0.0, 1.0};
    /** \brief A size of the birds crop as a proportion of its weight */
    CfgFloat cfg_goosebasecropsize{"GOOSE_BASECROPSIZE", CFG_CUSTOM, 0.116, 0.0, 1.0};
    /**\brief thconstant that describes the amount of food in g that is swallowed per gr of the total weight in the crop of the goose */
    CfgFloat cfg_goosedigestionparam{"GOOSE_DIGESTIONCONST", CFG_CUSTOM, 0.0072625, 0, 1.0};
/** \brief Input variable to control exit of the goose model
The geese leave the simulation in spring, so this variable is used to hard exit the
model once they have all left. */
     CfgInt cfg_goose_ModelExitDay{"GOOSE_MODELEXITDAY", CFG_CUSTOM, 9999, 1, 100000};
/** \brief The day where all grain across the landscape is reset. */
    CfgInt cfg_goose_grain_and_maize_reset_day{"GOOSE_GRAINANDMAIZERESETDAY", CFG_CUSTOM, 89, 0, 364};

/** \brief The time in minutes (multiple of ten) from sunrise when the habitat use is recorded for geese */
    CfgInt cfg_gooseHabitatUsetime{"GOOSE_HABUSERECORDTIME", CFG_CUSTOM, 120, 0, 12 * 60};
/** \brief The time in minutes (multiple of ten) from sunrise when the AOR probe is recorded for geese */
    CfgInt cfg_gooseAORtime{"GOOSE_AORRECORDTIME", CFG_CUSTOM, 120, 0, 12 * 60};
/** \brief The cfg variable determining whether to print goose population stats */
    CfgBool cfg_goose_PopulationDescriptionON{"GOOSE_POPULATIONDESCRIPTIONON", CFG_CUSTOM, true};
/** \brief Should we record goose energetics? */
    CfgBool cfg_goose_EnergyRecord{"GOOSE_ENERGYRECORD", CFG_CUSTOM, false};
/** \brief Should we record gooseindividual forage location count statistics? */
    CfgBool cfg_goose_IndLocCounts{"GOOSE_INDLOCCOUNTSTATS", CFG_CUSTOM, false};
/** \brief Should we record goose weight statistics? */
    CfgBool cfg_goose_WeightStats{"GOOSE_WEIGHTSTATS", CFG_CUSTOM, false};
/** \brief  Should we record state statistics? */
    CfgBool cfg_goose_StateStats{"GOOSE_STATESTATS", CFG_CUSTOM, false};
/** \brief Should we record goose leaving reason statistics? */
    CfgBool cfg_goose_LeaveReasonStats{"GOOSE_LEAVEREASONSTATS", CFG_CUSTOM, false};
/** \brief Should we record goose field forage information? */
    CfgBool cfg_goose_FieldForageInfo{"GOOSE_FIELDFORAGEINFO", CFG_CUSTOM, false};
/** \brief Should we query the map for openness scores on locations where geese have been observed in the field? */
    CfgBool cfg_goose_ObservedOpennessQuery{"GOOSE_OBSERVEDOPENNESS", CFG_CUSTOM, false};
/** \brief Should we query the map for openness scores on locations where geese have been observed in the field? */
    CfgStr cfg_goose_ObservedOpennessQuery_file{"GOOSE_OBSERVEDOPENNESS_FILE", CFG_CUSTOM, "GooseObservations.txt"};
/** \brief Should we write the values of the configs to a file? */
    CfgBool cfg_goose_WriteConfig{"GOOSE_WRITECONFIG", CFG_CUSTOM, true};
/** \brief Should we use stripped down output to optimize run time? */
    CfgBool cfg_goose_runtime_reporting{"GOOSE_RUNTIMEREPORTING", CFG_CUSTOM, true};

/** \brief Input: Pink-footed goose start numbers */
    CfgInt cfg_goose_pf_startnos{ "GOOSE_PF_STARTNOS", CFG_CUSTOM, 13440 };
/** \brief Input: Pink-footed goose proportion of young at start */
    CfgFloat cfg_goose_pf_young_proportion{ "GOOSE_PF_YOUNG_PROPORTION", CFG_CUSTOM, 0.21, 0.0, 1.0 };
/** \brief Input: Barnacle goose start numbers */
    CfgInt cfg_goose_bn_startnos{ "GOOSE_BN_STARTNOS", CFG_CUSTOM, 5600 };
/** \brief Input: Barnacle goose proportion of young at start */
    CfgFloat cfg_goose_bn_young_proportion{"GOOSE_BN_YOUNG_PROPORTION", CFG_CUSTOM, 0.21, 0.0, 1.0};
/** \brief Input: Greylag geese start numbers */
    CfgInt cfg_goose_gl_startnos{"GOOSE_GL_STARTNOS", CFG_CUSTOM, 8960};
/** \brief Input: Greylag goose proportion of young at start */
    CfgFloat cfg_goose_gl_young_proportion{"GOOSE_GL_YOUNG_PROPORTION", CFG_CUSTOM, 0.21, 0.0, 1.0};
/** \brief Input: Goose number scaler */
    CfgFloat cfg_goose_startno_scaler{"GOOSE_STARTNO_SCALER", CFG_CUSTOM, 1, 0.0, 100};
/** \brief Input: Pinkfoot number scaler */
    CfgFloat cfg_goose_pf_startno_scaler{"GOOSE_PF_STARTNO_SCALER", CFG_CUSTOM, 1, 0.0, 100};
/** \brief Input: Barnacle number scaler */
    CfgFloat cfg_goose_bn_startno_scaler{"GOOSE_BN_STARTNO_SCALER", CFG_CUSTOM, 1, 0.0, 100};
/** \brief Input: Greylag number scaler */
    CfgFloat cfg_goose_gl_startno_scaler{"GOOSE_GL_STARTNO_SCALER", CFG_CUSTOM, 1, 0.0, 100};

/** \brief Input: Should we simulate a spring migration? */
    CfgBool cfg_goose_pf_springmigrate{"GOOSE_PF_SPRING_MIGRATE", CFG_CUSTOM, true};
/** \brief Input: Date for onset of spring migration */
    CfgInt cfg_goose_pf_springmigdatestart{"GOOSE_PF_SPRING_MIG_START", CFG_CUSTOM, 7, 1, 365};
/** \brief Input: Date for end of spring migration */
    CfgInt cfg_goose_pf_springmigdateend{"GOOSE_PF_SPRING_MIG_END", CFG_CUSTOM, 59, 1, 365};
/** \brief Input: Number of pinkfeet to immigrate in spring */
    CfgInt cfg_goose_pf_springmignos{"GOOSE_PF_SPRING_MIG_NOS", CFG_CUSTOM, 5600};
/** \brief Input: Should we simulate a pinkfoot fall migration? */
    CfgBool cfg_goose_pf_fallmigrate{"GOOSE_PF_FALL_MIGRATE", CFG_CUSTOM, true};
/** \brief Input: Date for onset of pinkfoot fall migration */
    CfgInt cfg_goose_pf_fallmigdatestart{"GOOSE_PF_FALL_MIGRATION_START", CFG_CUSTOM, 274, 1, 365};
/** \brief Input: Date for end of pinkfoot fall migration */
    CfgInt cfg_goose_pf_fallmigdateend{"GOOSE_PF_FALL_MIGRATION_END", CFG_CUSTOM, 304, 1, 365};
/** \brief Input: The pinkfoot fall migration probability */
    CfgFloat cfg_goose_pf_fallmig_probability{"GOOSE_PF_FALLMIG_PROBABILITY", CFG_CUSTOM, 0.03, 0.0, 1.0};

/** \brief Input: Should we simulate greylag a spring migration? */
    CfgBool cfg_goose_gl_springmigrate{"GOOSE_GL_SPRING_MIGRATE", CFG_CUSTOM, true};
/** \brief Input: Date for onset of greylag spring migration */
    CfgInt cfg_goose_gl_springmigdatestart{"GOOSE_GL_SPRING_MIG_START", CFG_CUSTOM, 15, 1, 365};
/** \brief Input: Date for end of greylag spring migration */
    CfgInt cfg_goose_gl_springmigdateend{"GOOSE_GL_SPRING_MIG_END", CFG_CUSTOM, 75, 1, 365};
/** \brief Input: Number of greylag to immigrate in spring */
    CfgInt cfg_goose_gl_springmignos{"GOOSE_GL_SPRING_MIG_NOS", CFG_CUSTOM, 2240};
/** \brief Input: Should we simulate a greylag fall migration? */
    CfgBool cfg_goose_gl_fallmigrate{"GOOSE_GL_FALL_MIGRATE", CFG_CUSTOM, true};
/** \brief Input: Date for onset of greylag fall migration */
    CfgInt cfg_goose_gl_fallmigdatestart{"GOOSE_GL_FALL_MIGRATION_START", CFG_CUSTOM, 232, 1, 365};
/** \brief Input: Date for end of greylag fall migration */
    CfgInt cfg_goose_gl_fallmigdateend{"GOOSE_GL_FALL_MIGRATION_END", CFG_CUSTOM, 334, 1, 365};
/** \brief Input: The greylag fall migration probability */
    CfgFloat cfg_goose_gl_fallmig_probability{"GOOSE_GL_FALLMIG_PROBABILITY", CFG_CUSTOM, 0.0083, 0.0, 1.0};

/** \brief Input: Should we simulate barnacle a spring migration? */
    CfgBool cfg_goose_bn_springmigrate{"GOOSE_BN_SPRING_MIGRATE", CFG_CUSTOM, true};
/** \brief Input: Date for onset of barnacle spring migration */
    CfgInt cfg_goose_bn_springmigdatestart{"GOOSE_BN_SPRING_MIG_START", CFG_CUSTOM, 15, 1, 365};
/** \brief Input: Date for end of barnacle spring migration */
    CfgInt cfg_goose_bn_springmigdateend{"GOOSE_BN_SPRING_MIG_END", CFG_CUSTOM, 75, 1, 365};
/** \brief Input: Number of barnacle to immigrate in spring */
    CfgInt cfg_goose_bn_springmignos{"GOOSE_BN_SPRING_MIG_NOS", CFG_CUSTOM, 8960};
/** \brief Input: Should we simulate a barnacle fall migration? */
    CfgBool cfg_goose_bn_fallmigrate{"GOOSE_BN_FALL_MIGRATE", CFG_CUSTOM, true};
/** \brief Input: Date for onset of barnacle fall migration */
    CfgInt cfg_goose_bn_fallmigdatestart{"GOOSE_BN_FALL_MIGRATION_START", CFG_CUSTOM, 318, 1, 365};
/** \brief Input: Date for end of barnacle fall migration */
    CfgInt cfg_goose_bn_fallmigdateend{"GOOSE_BN_FALL_MIGRATION_END", CFG_CUSTOM, 348, 1, 365};
/** \brief Input: The barnacle fall migration probability */
    CfgFloat cfg_goose_bn_fallmig_probability{"GOOSE_BN_FALLMIG_PROBABILITY", CFG_CUSTOM, 0.0083, 0.0, 1.0};

/** \brief The sex ratio of pinkfoot non-breeders */
    CfgFloat cfg_goose_pf_sexratio{"GOOSE_PF_SEXRATIO", CFG_CUSTOM, 0.5, 0.0, 1.0};
/** \brief The sex ratio of barnacle non-breeders */
    CfgFloat cfg_goose_bn_sexratio{"GOOSE_BN_SEXRATIO", CFG_CUSTOM, 0.5, 0.0, 1.0};
/** \brief The sex ratio of greylag non-breeders */
    CfgFloat cfg_goose_gl_sexratio{"GOOSE_GL_SEXRATIO", CFG_CUSTOM, 0.5, 0.0, 1.0};

/** \brief Input: The initial starting date for arrival for pink-foots to the simulation area */
    CfgInt cfg_goose_pf_arrivedatestart{"GOOSE_PF_ARRIVEDATESTART", CFG_CUSTOM, 265};  // 23/09
/** \brief Input: The last date for arrival for pink-foots to the simulation area */
    CfgInt cfg_goose_pf_arrivedateend{"GOOSE_PF_ARRIVEDATEEND", CFG_CUSTOM, 280}; // 08/10
/** \brief Input: The initial starting date for arrival for barnacles to the simulation area */
    CfgInt cfg_goose_bn_arrivedatestart{"GOOSE_BN_ARRIVEDATESTART", CFG_CUSTOM, 277}; // 05/10
/** \brief Input: The last date for arrival for barnacles to the simulation area */
    CfgInt cfg_goose_bn_arrivedateend{"GOOSE_BN_ARRIVEDATEEND", CFG_CUSTOM, 298}; // 26/10
/** \brief Input: The initial starting date for arrival for greylags to the simulation area */
    CfgInt cfg_goose_gl_arrivedatestart{"GOOSE_GL_ARRIVEDATESTART", CFG_CUSTOM, 212}; // 01/08
/** \brief Input: The last date for arrival for greylags to the simulation area */
    CfgInt cfg_goose_gl_arrivedateend{"GOOSE_GL_ARRIVEDATEEND", CFG_CUSTOM, 231};  // 20/08
/** \brief Input: The initial starting date for pink-foots to the leave simulation area */
    CfgInt cfg_goose_pf_leavingdatestart{"GOOSE_PF_LEAVINGDATESTART", CFG_CUSTOM, 78}; // 20/03
/** \brief Input: The last date for pink-foots to leave the simulation area */
    CfgInt cfg_goose_pf_leavingdateend{"GOOSE_PF_LEAVINGDATEEND", CFG_CUSTOM, 104};  // 15/04
/** \brief Input: The initial starting date for barnacles to leave the simulation area */
    CfgInt cfg_goose_bn_leavingdatestart{"GOOSE_BN_LEAVINGDATESTART", CFG_CUSTOM, 90}; // 01/04
/** \brief Input: The last date for barnacles to leave the simulation area */
    CfgInt cfg_goose_bn_leavingdateend{"GOOSE_BN_LEAVINGDATEEND", CFG_CUSTOM, 134}; // 15/05
/** \brief Input: The initial starting date for greylags to leave the simulation area */
    CfgInt cfg_goose_gl_leavingdatestart{"GOOSE_GL_LEAVINGDATESTART", CFG_CUSTOM, 58}; // 28/02
/** \brief Input: The last date for greylags to leave the simulation area */
    CfgInt cfg_goose_gl_leavingdateend{"GOOSE_GL_LEAVINGDATEEND", CFG_CUSTOM, 68}; // 10/03

/** \brief The decay rate for the minimum forage rate*/
    CfgFloat cfg_goose_MinForageRateDecayRate{"GOOSE_MINFORAGEDECAYRATE", CFG_CUSTOM, 0.72, 0.0, 1.0};
/** \brief The mean for the normal distribution which determines roost leave time */
    CfgInt cfg_goose_RoostLeaveDistMean{"GOOSE_ROOSTLEAVEDISTMEAN", CFG_CUSTOM, 30};
/** \brief The standard deviation for the normal distribution which determines roost leave time */
    CfgInt cfg_goose_RoostLeaveDistSD{"GOOSE_ROOSTLEAVEDISTSD", CFG_CUSTOM, 10};

/** \brief The time when the geese are counted on fields.
We divide the daylight hours by this cfg and add that to the sun up time to get the timing for counting. E.g. a value of 2 will give noon. */
    CfgInt cfg_goose_TimedCounts{"GOOSE_TIMEDCOUNTS", CFG_CUSTOM, 2, 1, 12};

/** \brief The start of the pinkfoot hunting season */
    CfgInt cfg_goose_pinkfootopenseasonstart{"GOOSE_PF_OPENSEASONSTART", CFG_CUSTOM, 243};
/** \brief The end of the pinkfoot hunting season */
    CfgInt cfg_goose_pinkfootopenseasonend{"GOOSE_PF_OPENSEASONEND", CFG_CUSTOM, 364};
/** \brief The start of the greylag hunting season */
    CfgInt cfg_goose_greylagopenseasonstart{"GOOSE_GL_OPENSEASONSTART", CFG_CUSTOM, 243};
/** \brief The end of the greylag hunting season */
    CfgInt cfg_goose_greylagopenseasonend{"GOOSE_GL_OPENSEASONEND", CFG_CUSTOM, 31};
    /** \brief The exponent for the distance weighting curve */
    CfgFloat cfg_goose_dist_weight_power{"GOOSE_DIST_WEIGHTING_POWER", CFG_CUSTOM, 1.0, -10.0, 10.0};
    /** \brief The decrease in intake rate resulting from snow */
    CfgFloat cfg_goose_snow_scaler{"GOOSE_SNOW_SCALER", CFG_CUSTOM, 0.1, 0.0, 1.0};


    // ---- Grain functional response calculated by Kevin on 13/10/2017 ---- //
    /** \brief Attack rate in a type II functional response curve (Hollings disc equation) */
    CfgFloat cfg_H1A{"HOLLINGS_ONE_A", CFG_CUSTOM, 0.04217666};
    /** \brief Handling time in a type II functional response curve (Hollings disc equation) */
    CfgFloat cfg_H1B{"HOLLINGS_ONE_B", CFG_CUSTOM, 0.0840075};
    /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
    CfgBool cfg_H1C{"HOLLINGS_ONE_C", CFG_CUSTOM, false};
    /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
    CfgFloat cfg_H1D{"HOLLINGS_ONE_D", CFG_CUSTOM, 2500};
    /** \brief Min x-value */
    CfgFloat cfg_H1E{"HOLLINGS_ONE_E", CFG_CUSTOM, 0};
    /** \brief File name for functional response on grain for pinkfeet */
    CfgStr cfg_H1F{"HOLLINGS_ONE_G", CFG_CUSTOM, "KJIntakeAtDiffGrainDensities_PF"};

    // ---- Maize functional response Calculated by Kevin on 29/06/2018 ---- //
    /** \brief Attack rate in a type II functional response curve (Hollings disc equation) */
    CfgFloat cfg_H2A{"HOLLINGS_TWO_A", CFG_CUSTOM, 0.04294186};
    /** \brief Handling time in a type II functional response curve (Hollings disc equation) */
    CfgFloat cfg_H2B{"HOLLINGS_TWO_B", CFG_CUSTOM, 0.05844966};
    /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
    CfgBool cfg_H2C{"HOLLINGS_TWO_C", CFG_CUSTOM, false};
    /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
    CfgFloat cfg_H2D{"HOLLINGS_TWO_D", CFG_CUSTOM, 2100};
    /** \brief Min x-value */
    CfgFloat cfg_H2E{"HOLLINGS_TWO_E", CFG_CUSTOM, 0};
    /** \brief File name for functional response maize for barnacle */
    CfgStr cfg_H2F{"HOLLINGS_TWO_G", CFG_CUSTOM, "KJIntakeAtDiffMaizeDensities_BN"};

    // ---- Pettifor feeding time curve Pinkfoot & Greylag ---- //
    /** \brief  Maximum feeding time */
    CfgFloat cfg_Petti1A{"PETTIFOR_A_PF", CFG_CUSTOM, 0.88};
    /** \brief  Minimum feeding time */
    CfgFloat cfg_Petti1B{"PETTIFOR_B_PF", CFG_CUSTOM, 0.57};
    /** \brief  Threshold flock size above which feeding time no longer increases */
    CfgFloat cfg_Petti1C{"PETTIFOR_C_PF", CFG_CUSTOM, 280};
    /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
    CfgBool cfg_Petti1D{"PETTIFOR_D_PF", CFG_CUSTOM, false};
    /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
    CfgFloat cfg_Petti1E{"PETTIFOR_E_PF", CFG_CUSTOM, 1000};
    /** \brief Min x-value */
    CfgFloat cfg_Petti1F{"PETTIFOR_F_PF", CFG_CUSTOM, 0};
    /** \brief File name for density dependent functional response for pinkfeet */
    CfgStr cfg_Petti1G{"PETTIFOR_G_PF", CFG_CUSTOM, "FeedingTimePettifor_PF"};

    // ---- Pettifor feeding time curve Barnacle ---- //
    /** \brief  Threshold flock size above which feeding time no longer increases */
    CfgFloat cfg_Petti_C_BN{"PETTIFOR_C_BN", CFG_CUSTOM, 280};
    /** \brief File name for density dependent functional response for barnacle geese */
    CfgStr cfg_Petti_G_BN{"PETTIFOR_G_BN", CFG_CUSTOM, "FeedingTimePettifor_BN"};
    /** \brief  Maximum feeding time for barnacle geese */
    CfgFloat cfg_Petti_A_BN{"PETTIFOR_A_BN", CFG_CUSTOM, 0.88};

    /** \brief  Grass energy density: a coeefitient needed to estimate the fullness of the crop
     *
     * (1/(20*0.4))*10**/
    CfgFloat cfg_goose_grass_energy_density{"GOOSE_GRASS_ENERDENSITY", CFG_CUSTOM, 7.14};
    /** \brief  Grain energy density: a coeefitient needed to estimate the fullness of the crop
     *
     * For grains the moisture content is assumed static 35% ( even though, probably with the season the water accumulates in the grain more and more)
     * (1/(14.8*0.7))*0.35=2.758*0.35=3.72330*/
    CfgFloat cfg_goose_grain_energy_density{"GOOSE_GRAIN_ENERDENSITY", CFG_CUSTOM, 3.46};
    /** \brief  Maize energy density: a coeefitient needed to estimate the fullness of the crop
     *
     * For grains the moisture content is assumed static 35% ( even though, probably with the season the water accumulates in the grain more and more)
     * (1/(16*0.695))*0.35=2.57*0.35=3.46*/
    CfgFloat cfg_goose_maize_energy_density{"GOOSE_MAIZE_ENERDENSITY", CFG_CUSTOM, 3.723};

    CfgInt cfg_dump_grain_each_x_days{"MAP_DUMP_GRAIN_EACH_X_DAYS", CFG_CUSTOM, 1};

    CfgInt cfg_goose_throuput_time{"GOOSE_THROUPUT_TIME", CFG_CUSTOM, 12};
    TGooseToleTovs  GooseToleTovs;
};
/**
\brief
The class to handle all goose population related matters
*/
class Goose_Population_Manager : public Population_Manager
{
public:
	// Methods
		/** \brief
		Goose_Population_Manager Constructor
		*/
	explicit Goose_Population_Manager(Landscape* L);
	/** \brief
	Goose_Population_Manager Destructor
	*/
	~Goose_Population_Manager() override;
	/** \brief
	* Get the time of day (in minutes).
	For the goose model, sunrise is defined as m_daytime == 0. Hence this function returns the number of minutes since sunrise.
	*/
	int GetDayTime() const { return m_daytime; }
	/** \brief
	* Get the daylight minutes left today
	*/
     int GetDaylightLeft() const { return m_daylightleft; }
	/** \brief
	* Is it daylight hours? Daylight starts at m_daytime == 0.
	*/
	int GetIsDaylight() const { return m_daylight; }
	/** \brief Are we in the pinkfoot hunting season? */
	bool InPinkfootSeason() const { return m_PinkfootSeason; }
	/** \brief Are we in the greylag hunting season? */
	bool InGreylagSeason() const { return m_GreylagSeason; }
	/** \brief
	Method for creating a new individual Goose
	*/
	void CreateObjects(int ob_type, TAnimal *pvo, struct_Goose* data, int number);
	/** \brief
	Tests if a forage location is currently in use, if so returns the index to it
	*/
	int ForageLocationInUse(int a_polyref);
	/** \brief
	Adds a goose or geese to the forage location - uses polygon number as reference
	*/
      void AddGeeseToForageLocationP(GooseSpeciesType a_type, int a_polyref, int a_number);
	/** \brief
	Adds a goose or geese to the forage location - requires an index
	*/
	void AddGeeseToForageLocation(GooseSpeciesType a_type, int a_index, int a_number);
	/** \brief
	Removes a goose or geese to the forage location - requires an index
	*/
	void RemoveGeeseFromForageLocation(GooseSpeciesType a_type, int a_index, int a_number);
	/** \brief
	Creates a new forage location and adds a goose or geese to the forage location. Returns an index to the forage location list.
	*/
	int NewForageLocation(GooseSpeciesType a_type, int a_number, int a_polyref);
	/** \brief
	Returns a pointer to the forage location indexed by index.
	*/
	GooseActiveForageLocation* GetForageLocation(unsigned int a_index) {
		if ((m_GooseForageLocations.empty()) || (m_GooseForageLocations.size() <= a_index)) return nullptr; else return &m_GooseForageLocations[a_index];
	}
	/** \brief
	Get the forage rate based on the grain density
	\param [in] a_graindensity The grain density (grain/m2) on the forage location
	\param [in] a_species The type of goose species
	\return The species specific intake rate (kJ/min) when feeding on grain
	*/
	double GetFeedingRate(double a_graindensity, GooseSpecies a_species)
	{
		/**	Unpublished curve from Nolet.
		* o:/ST_GooseProject/Field data/Fugledata/Functional response pink-feet.xlsx
		* This curve has already taken digestibility, energy content and assimilation
		* into account.
		*/
		if (a_species == gs_Greylag) return m_IntakeRateVSGrainDensity_PF->GetY(a_graindensity) * 1.21;
		else if (a_species == gs_Pinkfoot) return m_IntakeRateVSGrainDensity_PF->GetY(a_graindensity);
		else return m_IntakeRateVSGrainDensity_PF->GetY(a_graindensity) * 0.74;
	}
	/** \brief
	Get the forage rate when feeding on maize
	\param [in] a_maizedensity The maize density in kJ/m2
	\param [in] a_species The type of goose species
	\return The species specific intake rate when feeding on maize
	*/
	double GetMaizeFeedingRate(double a_maizedensity, GooseSpecies a_species) {
		/**	Functional response for geese feeding on maize is currently lacking.
		The curve used here is based on field observations of barnacle geese 
		made with wildlife cameras in Jutland in 2015. Assimilation for energy
		is accounted for in the functional response curce. Value found for Bewick's swans
		from Nolet, B. unpubl. For greylag and pinkfeet the value is scaled based
		on the relationship between */
		if (a_species == gs_Greylag) return m_IntakeRateVSMaizeDensity_BN->GetY(a_maizedensity) * 1.64;
		else if (a_species == gs_Pinkfoot) return m_IntakeRateVSMaizeDensity_BN->GetY(a_maizedensity) * 1.35;
		else return m_IntakeRateVSMaizeDensity_BN->GetY(a_maizedensity);
	}
	/** \brief
	Get the forage intake rate for a forage density.
	\param [in] a_foragedensity The density of geese at the forage location
	\param [in] a_species The goose species
	\return The species specific intake rate dependent on the goose density
	*/
	double GetForageRateDensity(double a_foragedensity, GooseSpecies a_species)
	{
		if (a_species == gs_Barnacle)
		{
			return m_ForageRateVSGooseDensity_BN->GetY(a_foragedensity);
		}
		else
			return m_ForageRateVSGooseDensity->GetY(a_foragedensity);
	};
	/** \brief
	Returns the total goose density for a forage location.
	*/
	double GetForageGooseDensity(int a_index) { return m_GooseForageLocations[a_index].GetGooseDensity(); }
	/** \brief
	Returns the forage density for a forage location and goose type. This is species specific
	*/
     double GetForageGrazing(int a_index, int gs) { return m_GooseForageLocations[a_index].GetGrazing(gs); }
	/** \brief
	Returns the forage density for a forage location.
	*/
	double GetGrainDensity(int a_index) { return m_GooseForageLocations[a_index].GetGrainDensity(); }
	/** \brief
	Returns the maize forage density for a forage location.
	*/
     double GetMaizeDensity(int a_index) {
		return m_GooseForageLocations[a_index].GetMaizeDensity();
	}
	/** \brief Removes kJ eaten as grains from a forage area.*/
	void RemoveGrainKJ(double a_kJ, int a_index) { m_GooseForageLocations[a_index].RemoveGrainKJ(a_kJ); }

	/** \brief
	Removes KJ eaten as maize from a forage area.
	*/
	void RemoveMaizeKJ(double a_kJ, int a_index) {
		m_GooseForageLocations[a_index].RemoveMaizeKJ(a_kJ);
	}
	/** \brief
	Removes KJ as grams veg biomass from a forage area.
	*/
	void Graze(double a_kJ, int a_index) { m_GooseForageLocations[a_index].Graze(a_kJ); }
	/** \brief
	Removes the forage eaten from the field
	\param [in] a_forage The forage to be removed in Kj
	\param [in] a_maxintakesource The source of the max intake
	\param [in] m_myForageIndex Temporary storage for a forage location index
	*/
	void RemoveMaxForageKj(double a_forage, TTypeOfMaxIntakeSource a_maxintakesource, int m_myForageIndex);
	/** \brief
	Returns the number of birds at a forage location - given by a poly ref
	*/
	int GetBirdsAtForageLoc(int a_index, GooseSpeciesType a_type) {
		return m_GooseForageLocations[a_index].GetBirds(a_type);
	}
	/** \brief
	Returns the number of birds at a forage location - given by a poly ref
	*/
	int BirdsToShootAtPoly(int a_poly /*, double & a_protectedpct */);
	/** \brief
	Passes the message to shoot a number of birds at a forage location.
	*/
	void BirdsShot(int a_polyref, int a_numbershot, GooseHunter* a_Hunter);
	/** \brief
	Passes a 'Bang' message to birds near to the location specified by the polygon reference.
	*/
	void BangAtPoly(int a_polyref);

	/** \brief Asks for a pointer to a goose that can be followed. */
	Goose_Base* GetLeader(APoint a_homeloc, GooseSpecies a_species);

	/** \brief
	Changes a_x & a_y to the location of the nearest roost of a_type to a_x, a_y
	*/
	void FindClosestRoost(int &a_x, int &a_y, unsigned a_type);
	/** \brief Returns the distance in to the nearest roost of a_type in meters	*/
	double GetDistToClosestRoost(int a_x, int a_y, unsigned a_type);
	/** \brief Get daily thermal costs const */
	double GetThermalCosts(GooseSpecies a_goose) { return m_thermalcosts[a_goose]; }
	/** \brief Is a list of active goose forage locations where we have geese */
	std::vector<GooseActiveForageLocation>m_GooseForageLocations;
	/** \brief Get the number of forage locations*/
     int GetNumberOfForageLocs() const { return int(m_GooseForageLocations.size()); }
	/** \brief Get a forage location for my species (picked randomly among the active locations)*/
	int GetForageLocIndex(GooseSpecies a_species, int a_x, int a_y);
	/** \brief Function to be able to draw randomly from predefined distributions */
	probability_distribution * m_variate_generator;
	/** \brief 	Record a forage location count */
	void RecordIndForageLoc(double a_count, int a_groupsize, GooseSpecies a_species);
	/** \brief 	Record the weight */
	void RecordWeight(double a_weight, GooseSpecies a_species);
	/** \brief 	Record the habitat use */
	void RecordHabitatUse(int a_habitatype, GooseSpecies a_species, int a_count);
	/** \brief 	Record the habitat use */
	void RecordHabitatUseFieldObs(int a_habitatype, GooseSpecies a_species, int a_count);
	/** \brief 	Record the time spent foraging */
	void RecordForagingTime(int a_time, GooseSpecies a_species);
    /** \brief 	Record the time spent resting */
    void RecordRestingTime(int a_time, GooseSpecies a_species);
	/** \brief 	Record the flight distance */
	void RecordFlightDistance(int a_distance, GooseSpecies a_species);
	/** \brief 	Record the daily energy budget */
	void RecordDailyEnergyBudget(double a_deb, GooseSpecies a_species);
	/** \brief 	Record the daily energy balance */
	void RecordDailyEnergyBalance(double a_balance, GooseSpecies a_species);
	/** \brief  Record the state */
	void RecordState();
	/** \brief 	Record the reason for leaving */
	void RecordLeaveReason(TTypeOfLeaveReason a_leavereason, GooseSpeciesType a_speciestype);
	/** \brief   BangAtCoords*/
	void DisturberBangAtCoords(const shared_ptr<Disturber_Population_Manager>&);
    /** \brief   gosse config instance */
    GooseConfigs GooseCfg;
protected:
	// Attributes
    std::random_device rd{};
    std::mt19937 gen{rd()};
    /** \brief The binary map that holds the locations where the birds are alarmed.*/
	 unique_ptr<blitz::Array<bool, 2>> m_AlarmedBirdsMap;
		/** \brief Holds the time of day. Note that sunrise is at m_daytime == 0.*/
	int m_daytime;
	/** \brief Flag for in daylight hours. Sunrise is always at m_daytime == 0. */
	bool m_daylight{true};
	/** \brief Number of daylight minutes left */
	int m_daylightleft{-1};
	/** \brief Temporary storage for daily goose energetic thermal costs constant for each species */
	double m_thermalcosts[3]{0,0,0};
	/** \brief The observed distribution of young for pink feet*/
	std::vector<int> m_youngdist;
	/** \brief Storage for goose numbers. Used when birds are immigrating */
	int m_migrationnumbers[3][2]{0,0,0,0,0,0};
	/** \brief Flag to indicate if we are in the pinkfoot hunting season? */
	bool m_PinkfootSeason;
	/** \brief Flag to indicate if we are in the greylag hunting season*/
	bool m_GreylagSeason;
	/**	\brief	The list of roosts	*/
	std::vector<roostlist> m_roosts;
	/** \brief
	Speed optimisation to hold all potential forage rates in the range 0-X grain density per m2
	*/
	HollingsDiscCurveClass* m_IntakeRateVSGrainDensity_PF;
	/** \brief
	Speed optimisation to hold all potential forage rates in the range 0-X maize density per m2 @todo fix
	*/
	HollingsDiscCurveClass* m_IntakeRateVSMaizeDensity_BN;
	/** \brief
	Speed optimisation to hold all potential competition reductions in the range 0 - 1 goose/m2
	*/
	//GompertzCurveClass* m_ForageRateVSGooseDensity;
	/** \brief Speed optimisation to hold all potential feeding time increases as a function of flock size - pinkfoot and greylag*/
	PettiforFeedingTimeCurveClass* m_ForageRateVSGooseDensity;
	/** \brief Speed optimisation to hold all potential feeding time increases as a function of flock size - barnacle*/
	PettiforFeedingTimeCurveClass* m_ForageRateVSGooseDensity_BN;
	/** \brief
	Pointer to an output file for goose population data
	*/
	ofstream* m_GoosePopDataFile{nullptr};
	/** \brief
	Pointer to an output file for goose energetics data
	*/
	ofstream* m_GooseEnergeticsDataFile{nullptr};
	/** \brief
	Pointer to an output file for goose habitat use data
	*/
	ofstream* m_GooseHabitatUseFile{nullptr};
	/** \brief
	Pointer to an output file for goose habitat use data, field observation mimic version
	*/
	ofstream* m_GooseHabitatUseFieldObsFile{nullptr};
	/** \brief
	Pointer to an output file for goose individual forage location count data
	*/
	ofstream* m_GooseIndLocCountFile{nullptr};
	/** \brief
	Pointer to an output file for goose weight stats data
	*/
	ofstream* m_GooseWeightStatsFile{nullptr};
	/** \brief
	Pointer to an output file for goose leave reason stats data
	*/
	ofstream* m_GooseLeaveReasonStatsFile{nullptr};
	/**	\brief
	Pointer to an output file for goose field forage data
	*/
	ofstream* m_GooseFieldForageDataFile{nullptr};
	/** \brief Pointer to an output file for almass version */
	ofstream* m_GooseGitVersionFile{nullptr};
	/**	\brief
	Data for the habitat use
	*/
	int m_HabitatUseStats[gs_foobar * tomis_foobar]{0};
	/**	\brief
	Data for the habitat use, field observation mimic version
	*/
	int m_HabitatUseFieldObsStats[gs_foobar * tomis_foobar]{0};
	/**	\brief
	Statistics for the number of forage locations visited per goose of the population
	*/
	SimpleStatistics m_IndividualForageLocationData[gs_foobar];
	/**	\brief
	Statistics for the weights of the population
	*/
	SimpleStatistics m_WeightStats[gs_foobar];
    /**	\brief
    Statistics for the time of rests during the day
    */
    SimpleStatistics m_RestTimeStats[gs_foobar];
	/**	\brief
	Statistics for the time spent foraging for the population
	*/
	SimpleStatistics m_ForagingTimeStats[gs_foobar];
	/**	\brief
	Statistics for the flight distances in the population
	*/
	SimpleStatistics m_FlightDistanceStats[gs_foobar];
	/**	\brief
	Statistics for the daily energy budget in the population
	*/
	SimpleStatistics m_DailyEnergyBudgetStats[gs_foobar];
	/**	\brief
	Statistics for the daily energy balance in the population
	*/
	SimpleStatistics m_DailyEnergyBalanceStats[gs_foobar];
	/** \brief	Pointer to an output file for state stats data */
	ofstream* m_StateStatsFile{nullptr};
	/** \brief	Pointer to an output file for grain dist data */
	ofstream *m_GrainDistFile{nullptr};
	/**	\brief Debugging code. Statistics for the number of times a state method is called */
	SimpleStatistics m_StateStats;
	/**	\brief
	Statistics for reasons for leaving the simulation
	*/
	SimpleStatistics m_LeaveReasonStats[gst_foobar][tolr_foobar];
	/**	\brief
	AOR Probe for pinkfeet
	*/
	AOR_Probe_Goose * m_AOR_Pinkfeet{nullptr};
	/**	\brief
	AOR Probe for barnacles
	*/
	AOR_Probe_Goose * m_AOR_Barnacles{nullptr};
	/**	\brief
	AOR Probe for greylags
	*/
	AOR_Probe_Goose * m_AOR_Greylags{nullptr};
	/** \brief
	Pointer to an output file for goose x y data
	*/
	ofstream* m_GooseXYDumpFile{nullptr};
	/** \brief
	 * how far the alarm call is effective
	 * */
    int m_AlarmCallDistance;
	// Methods
		/** \brief Called upon initialization of the simulation. Sets up output files. */
	virtual void Init();
	/** \brief  Things to do before anything else at the start of a timestep
	*/
	void DoFirst() override;
	/** \brief Things to do before the Step
	*/
	void DoBefore() override { ; }
	/** \brief
	Things to do before the EndStep
	*/
	void DoAfter() override { ; }
	/** \brief
	Things to do after the EndStep
	*/
	void DoLast() override;
	/** \brief
	Controls main immigration to the Danish simulation area
	*/
	virtual void DoImmigration();
	/** \brief
	Controls main emigration from the Danish simulation area
	*/
	virtual void DoEmigration();
	/** \brief
	Does some movement of birds in and out each day from the Danish simulation area
	*/
	virtual void DoDailyImmigrationEmigration();
	/** \brief
	Produces output to a standard file describing the state of the goose populations.
	*/
	void GoosePopulationDescriptionOutput();
	/** \brief
	Produces output to a standard file describing the energetic state of all individuals of the goose populations.
	*/
	void GooseFieldForageInfoOutput();
	/** \brief
	Produces output to a standard file describing the number of birds foraging at each field and the field conditions
	*/
	void GooseEnergyRecordOutput();
	/** \brief
	Outputs simple stats for the weights in the population
	*/
	void GooseWeightStatOutput();
	/** \brief
	Outputs simple stats for the number of forage locations visited per goose
	*/
	void GooseIndLocCountOutput();
	/** \brief
	Outputs simple stats for the goose habitat use
	*/
	void GooseHabitatUseOutput();
	/** \brief
	Outputs simple stats for the goose habitat use but using rules to mimic field observations
	*/
	void GooseHabitatUseFieldObsOutput();
	/** \brief
	Clear simple stats for forage location counts
	*/
	void ClearIndLocCountStats();
	/** \brief
	Clear simple stats for the weights in the population
	*/
	void ClearGooseWeightStats();
	/** \brief
	Clear simple stats for habitat use
	*/
	void ClearGooseHabitatUseStats();
	/** \brief
	Clear simple stats for field obs habitat use
	*/
	void ClearGooseHabitatUseFieldObsStats();
	/** \brief
	Clear simple stats for foraging times in the population
	*/
	void ClearGooseForagingTimeStats();
	/** \brief
	Clear simple stats for flight distances in the population
	*/
	void ClearGooseFlightDistanceStats();
	/** \brief
	Clear simple stats for daily energy budget in the population
	*/
	void ClearGooseDailyEnergyBudgetStats();
	/** \brief
	Clear simple stats for daily energy balance in the population
	*/
	void ClearGooseDailyEnergyBalanceStats();
	/** \brief  Clear simple stats for the states */
	void ClearStateStats();
	/** \brief  Write simple stats for the states */
	void StateStatOutput();
	/** \brief  Write grain dist output */
	void GrainDistOutput();
	/** \brief
	Outputs simple stats for the reasons for leaving the simulation
	*/
	void GooseLeaveReasonStatOutput();
	/** \brief
	Clear simple stats for the reasons for leaving the simulation
	*/
	void ClearGooseLeaveReasonStats();
	/** \brief
	Writes a file with the openness scores on places where geese have been observed in the field
	*/
	void ObservedOpennessQuery();
	/** \brief
	Writes a file the values of the config variables used
	*/
	void WriteConfig() const;
	/** \brief
	Get the numbers to immigrate.
	*/
	void GetImmigrationNumbers(GooseSpecies a_goose, bool a_season);
	/** \brief 	Are we in the hunting season? */
	bool InHuntingSeason(int a_day, GooseSpecies a_species);
	/** \brief 	Handy function for writing headers */
	static void WriteHeaders(ofstream *a_file, std::vector<std::string> a_headers);
	/** \brief 	The modified goose version of the standard output for creating AOR statistics */
	void TheAOROutputProbe() override;
	/** \brief 	Does nothing, but we need it here because it automatically called by the Population_Manager if the NWordOutput is used */
	void TheRipleysOutputProbe(FILE* /*a_prb*/) override{ ; };
	/** \brief 	Does nothing, but we need it here because it automatically called by the Population_Manager if the NWordOutput is used */
	void CloseTheRipleysOutputProbe() override { ; }
	/** \brief 	Does nothing, but we need it here because it automatically called by the Population_Manager if the NWordOutput is used */
	void CloseTheReallyBigOutputProbe() override{ ; }
	/** \brief
	Outputs x y data 
	*/
	void XYDump();
	/** \brief 	Translates gst enum to string  */
	static std::string GooseTypeToString(GooseSpeciesType a_gst);
	/** \brief 	Translates gs enum to string  */
	static std::string GooseToString(GooseSpecies a_gs);
	/** \brief 	Translates tomis enum to string  */
	static std::string IntakeSourceToString(TTypeOfMaxIntakeSource a_intake_source);
	/** \brief 	Translates tolr enum to string  */
	static std::string LeaveReasonToString(TTypeOfLeaveReason a_leave_reason);

};

#endif
