#ifndef HABITATMAP_H
#define HABITATMAP_H
#include <vector>

class Landscape;
class QString;
class MainWindow;

struct palette
{
    palette();
    std::vector<char> red;
    std::vector<char> green;
    std::vector<char> blue;
};

class HabitatMap
{
public:
    HabitatMap(const unsigned SIZE);
    HabitatMap();
    ~HabitatMap();
    QString getMapText(const int x, const int y);
    QString getAnimalText(const int x, const int y);

    void DrawLandscape();
    void ChangeZoom( int X, int Y );
    void SetScale( int Width, int Height );
    void SetSimExtent( int Width, int Height );
    void SetUpMapImage();
    void SetLandscape(Landscape* l) {m_ALandscape=l;}
    void ZoomIn( int X, int Y);
    void ZoomOut( int X, int Y);
    int mapSize() {return __MAPSIZE1;}
    unsigned char* imageData() {return idata;}
    void Spot( int cref, int x, int y );
    int currentView;
    void ConvertForZoom(int &d_x, int &d_y, int s_x, int s_y);
    MainWindow* mainWindow;

protected:
    unsigned __MAPSIZE1;
    Landscape* m_ALandscape;
    unsigned char* idata;
    bool m_TrackOn;
    float m_scalingW, m_scalingH;
    int m_TLx,m_TLy,m_BRx,m_BRy,m_Width,m_Height,m_Zoom;
    int m_SimW;
    int m_SimH;
    double m_ddegs;
    palette colours;
};

#endif // HABITATMAP_H
