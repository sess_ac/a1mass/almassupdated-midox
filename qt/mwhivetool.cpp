#include "mwhivetool.h"
//#include "ui_mwhivetool.h"
//#include "../BatchALMaSS/PopulationManager.h"
#include "mainwindow.h"
#include <math.h>
#include <QDialog>
#include <QWidget>
#include <QButtonGroup>
#include <QGraphicsPolygonItem>
#include <QBrush>
#include <QPen>
#include <QGraphicsScene>
#include <QActionGroup>
#include "honeycombcell.h"
#include <blitz/array.h>

#include "../ALMaSSDefines.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"

#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../HoneyBee/HoneyBee_Colony.h"
#include "../HoneyBee/Hive.h"

MWHiveTool::MWHiveTool(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MWHiveTool)
{
    temp_scene = NULL;
    ui->setupUi(this);
    MainWindow * mw = dynamic_cast<MainWindow*>(parent);
    pm =(HoneyBee_Colony*)mw->getPopManager();


    // Note - this is temporary. Should be in PM.
    hive = pm->getHive();
    //hive->clearCellType();
    //hive->setCellType(1,1,1,2);
    //std::cout << hive->getCellTypeArray() << std::endl;

    //hive=pm->getHive();

    seeGroup = new QActionGroup(this);
    seeGroup->addAction(ui->actionBee_count);
    seeGroup->addAction(ui->actionCell_Types);
    seeGroup->addAction(ui->actionTemperature);

    viewType=3;

    zoom=ui->zoomDial->value();
    zoom=1500;
    ui->zoomLCD->display(zoom);

    scene=new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    //genTemperature();
    //hive->makeSomeHoney();
//    doDraw();
}


MWHiveTool::~MWHiveTool()
{
    delete ui;
    delete seeGroup;
    delete scene;

    // Note - this is temporary. Should be in PM.
    //delete hive;
}

void MWHiveTool::paintEvent(QPaintEvent * p) {
    doDraw();
}

void MWHiveTool::doDraw()
{
    if (!ui->updateCB->isChecked())
        return;

    //genTemperature();
    //hive->makeSomeHoney();
    int frame=ui->frameSB->value()-1;
    int box=ui->boxSB->value()-1;
    if(frame<0) frame=0;
    if(frame>=(hive->supplyYrange())/2-1){
        frame=(hive->supplyYrange())/2-1;
    }

    //std::cout << "View Type: " << viewType << std::endl;

    double min=ui->minTempSB->value();
    double max=ui->maxTempSB->value();
    if(temp_scene!=NULL){
        delete temp_scene;
    }
    switch (viewType) {
    case 1: // Cell types
    {
        blitz::Array<int,2> f2=hive->getFrameCellType(box, frame,FrameSide::Front);
        blitz::Array<int,2> b2=hive->getFrameCellType(box, frame,FrameSide::Back);
        //drawFrameContinuous<int>(f2,b2,0,6);
        temp_scene=drawFrameCategorical<int>(f2,b2);
        break;
    }
    case 2: // Bee numbers
    {
        blitz::Array<int,2> f2=hive->getFrameBeeNums(box, frame,FrameSide::Front);
        blitz::Array<int,2> b2=hive->getFrameBeeNums(box, frame,FrameSide::Back);
        //std::cout << "Front Side Array" << std::endl;
        //std::cout << f2;
        temp_scene=drawFrameContinuous<int>(f2,b2,0,3);
        break;
    }
    case 3: // Temp
    {
        blitz::Array<double,2> f2=hive->getFrameTemp(frame,FrameSide::Front);
        blitz::Array<double,2> b2=hive->getFrameTemp(frame,FrameSide::Back);
        temp_scene=drawFrameContinuous<double>(f2,b2,min,max);
        break;
    }
    case 4: // Honey
    {
        blitz::Array<double,2> f2=(hive->getFrameHoney(box, frame,FrameSide::Front))(blitz::Range::all(), blitz::Range::all());
        blitz::Array<double,2> b2=(hive->getFrameHoney(box, frame,FrameSide::Back))(blitz::Range::all(), blitz::Range::all());
        temp_scene=drawFrameContinuous<double>(f2,b2,0,50);
        break;
    }
    case 5: // Pollen
    {
        blitz::Array<double,2> f2=hive->getFramePollen(box, frame,FrameSide::Front)(blitz::Range::all(), blitz::Range::all());
        blitz::Array<double,2> b2=hive->getFramePollen(box, frame,FrameSide::Back)(blitz::Range::all(), blitz::Range::all());
        temp_scene=drawFrameContinuous<double>(f2,b2,0,50);
        break;
    }


    default:
        break;
    }

    ui->graphicsView->fitInView(0,0,zoom,zoom,Qt::KeepAspectRatio);
}

void MWHiveTool::on_zoomDial_sliderMoved(int position)
{
    zoom=position;
    ui->zoomLCD->display(zoom);
    doDraw();
}

void MWHiveTool::on_boxSB_valueChanged(int arg1)
{
    doDraw();
}

void MWHiveTool::on_frameSB_valueChanged(int arg1)
{
    doDraw();
}

void MWHiveTool::on_actionCell_Types_toggled(bool arg1)
{
}

void MWHiveTool::on_actionBee_count_toggled(bool arg1)
{
}

void MWHiveTool::on_actionCell_Types_triggered()
{
    viewType=1;
    doDraw();

}

void MWHiveTool::on_actionBee_count_triggered()
{
    viewType=2;
    doDraw();
}

void MWHiveTool::on_actionTemperature_triggered()
{
    viewType=3;
    doDraw();
}

void MWHiveTool::on_actionHoney_triggered()
{
    viewType=4;
    doDraw();
}

void MWHiveTool::on_actionPollen_triggered()
{
    viewType=5;
    doDraw();
}

