

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mwhivetool.h"


#include <QMessageBox>
#include <QDebug>
#include <QMouseEvent>

#include "stdio.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <string>

#include "qcustomplot.h"

#include "../ALMaSSDefines.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"

#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Vole/GeneticMaterial.h"
#include "../Skylark/skylarks_all.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Vole/vole_all.h"
#include "../Vole/VolePopulationManager.h"
#include "../Vole/Predators.h"
#include "../Beetles/Bembidion_All.h"
#include "../Hare/Hare_All.h"
#include "../Spiders/Spider_BaseClasses.h"
#include "../Spiders/Erigone.h"
#include "../Spiders/Erigone_Population_Manager.h"
#include "../Spiders/Haplodrassus.h"
#include "../Spiders/Haplodrassus_Population_Manager.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../People/Hunters/Hunters_all.h"
#include "../People/Hunters/Disturbers_All.h"
#include "../People/Farmers/Farmer.h"
#include "../People/Farmers/SocialNetworks.h"
#include "../GooseManagement/Goose_Population_Manager.h"
#include "../RodenticideModelling/RodenticidePredators.h"
#include "../RoeDeer/Roe_all.h"
#include "../RoeDeer/Roe_pop_manager.h"
#include "../Rabbit/Rabbit.h"
#include "../Rabbit/Rabbit_Population_Manager.h"
#include "../Newt/Newt.h"
#include "../Newt/Newt_Population_Manager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"
#include "../OliveMoth/OliveMoth.h"
#include "../OliveMoth/OliveMoth_Population_Manager.h"
#include "../OliveFly/OliveFly.h"
#include "../OliveFly/OliveFly_Population_Manager.h"
#include "../Lacewing/Lacewing.h"
#include "../Lacewing/Lacewing_Population_Manager.h"
#include "../Aphid/Aphid.h"
#include "../Aphid/Aphid_Population_Manager.h"
#include "../Osmia/Osmia.h"
#include "../Osmia/Osmia_Population_Manager.h"
#include "../Beetles/Ladybird_All.h"
#include "../Beetles/Poecilus_All.h"
#include "../Aphids_debug/AphidsD_all.h"
#include "habitatmap.h"
#include "../HoneyBee/HoneyBee.h"
#include "../HoneyBee/HoneyBee_Colony.h"
#include "../HoneyBee/Hive.h"
#include "../Bombus/Bombus.h"
#include "../Bombus/Bombus_Population_Manager.h"
#include "../Theoretical1/Theoretical1.h"
#include "../Theoretical1/Theoretical1_Population_Manager.h"
//#include "../Aphid/Aphid.h"
//#include "../Aphid/Aphid_Population_Manager.h"

extern CfgBool l_pest_enable_pesticide_engine;
extern CfgInt l_pest_NoPPPs;
extern CfgBool cfg_rodenticide_enable;
static CfgBool cfg_animate( "G_ANIMATE", CFG_CUSTOM, true );
static CfgBool cfg_dumpmap( "G_DUMPMAP", CFG_CUSTOM, true ); // Produces a set of bitmaps using the next three cfgs to determine start, range and interval
static CfgInt cfg_dumpmapstart( "G_DUMPMAPSTART", CFG_CUSTOM, 366  );
static CfgInt cfg_dumpmapend( "G_DUMPMAPEND", CFG_CUSTOM, 730 );
static CfgInt cfg_dumpmapstep( "G_DUMPMAPSTEP", CFG_CUSTOM, 5 );
static CfgInt cfg_tracelevel("G_TRACELEVEL", CFG_CUSTOM, 4);

extern PopulationManagerList g_PopulationManagerList;
extern TTypesOfPopulation g_Species;

std::unordered_set<TTypesOfPopulation> EggListPopulation{
        TOP_Beetle,
        TOP_Bembidion,
        TOP_Ladybird
};

ALMaSS_MathFuncs g_AlmassMathFuncs;

std::shared_ptr<HoneyBee_Colony> g_honeybee_population_manager;
int g_torun; // because used in the goose, fine .... ( but not really used in the gui, so it works maybe for goose only from batch)

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    habMap.mainWindow=this;

    // Must come first. Used by the configurator below.
    g_msg = CreateErrorMsg();
    g_msg->SetFilename("ErrorFile.txt");
    g_msg->SetWarnLevel(WARN_MSG);

    //hivetool=new MWHiveTool(this);

    g_cfg = CreateConfigurator();
    g_cfg->ReadSymbols("TIALMaSSConfig.cfg");
    m_currentSpecies=-1; // not initialized yet 

    m_PopulationManagerList = new PopulationManagerList;


    //g_cfg->DumpAllSymbolsAndExit( "allsymbols.cfg" );
    ui->setupUi(this);
    //showFullScreen();
    setWindowState(Qt::WindowMaximized);
    viewGroup = new QActionGroup(this);
    viewGroup->addAction(ui->actionMap);
    viewGroup->addAction(ui->actionVeg_Type);
    viewGroup->addAction(ui->actionBiomass);
    viewGroup->addAction(ui->actionFarm_ownership);
    viewGroup->addAction(ui->actionPesticide_Load);
    viewGroup->addAction(ui->actionGoose_Numbers);
    viewGroup->addAction(ui->actionGoose_Food_Resource);
    viewGroup->addAction(ui->actionGoose_Grain_Resource);
    viewGroup->addAction(ui->actionGoose_Grazing_Resource);
    viewGroup->addAction(ui->actionSoil_Type_Rabbits);
    viewGroup->addAction(ui->actionHunters_Locations);
    viewGroup->addAction(ui->actionPollen);
    viewGroup->addAction(ui->actionNectar);

    // This is how to set background colour of pop labels
    //ui->popLab1->setStyleSheet("background:rgb(46,204,113);");


    connect(ui->map,SIGNAL(clicked()),this,SLOT(on_map_clicked2()));
    connect(ui->map,SIGNAL(clicked(QMouseEvent*)),this,SLOT(on_map_clicked(QMouseEvent*)));


    clickGroup = new QActionGroup(this);
    clickGroup->addAction(ui->actionZoom);
    clickGroup->addAction(ui->actionInfo);
    m_Paused=false;
    drawingMap=true;
    hivetool=NULL;
    DataInit();

    ui->map->setScaledContents(true);


    // m_chart1->legend()->hide();
    // Customize the title font
    QFont font;
    font.setPixelSize(12);

    m_lifestagelists = 0; // Initiatlise this to zero until we know how many sub populations we need to track
}

MainWindow::~MainWindow()
{
    delete ui;
    delete viewGroup;
    if (hivetool)
        delete hivetool;
    //delete popBar;

    delete m_PopulationManagerList;
}

void MainWindow::setUpForm()
{
    // Give the species checkboxes proper names, taken from SupplyListName() and enable disable form widgets as needed
    QCheckBox* spboxes[14]={ui->spBox1,ui->spBox2,ui->spBox3,ui->spBox4,ui->spBox5,ui->spBox6,ui->spBox7,
                           ui->spBox8,ui->spBox9,ui->spBox10,ui->spBox11,ui->spBox12,ui->spBox13,ui->spBox14};

    QLabel* splabs[14]={ui->popLab1,ui->popLab2,ui->popLab3,ui->popLab4,ui->popLab5,ui->popLab6,ui->popLab7,
                       ui->popLab8,ui->popLab9,ui->popLab10,ui->popLab11,ui->popLab12,ui->popLab13,ui->popLab14};
    QLineEdit* popNums[14] = { ui->popNum1,ui->popNum2,ui->popNum3,ui->popNum4,ui->popNum5,ui->popNum6,ui->popNum7, ui->popNum8,ui->popNum9,ui->popNum10,ui->popNum11,ui->popNum12,ui->popNum13,ui->popNum14 };
    m_lifestagelists = m_AManager->SupplyListNameLength();
    if (m_lifestagelists > 0)
    {
        for (int i=0; i < m_lifestagelists;++i)
        {
            spboxes[i]->setEnabled(true);
            spboxes[i]->setChecked(true);
            spboxes[i]->setText(m_AManager->SupplyListName(i));
            splabs[i]->setText(m_AManager->SupplyListName(i));
        }
        for (int ac = m_lifestagelists; ac < 14; ++ac)
        {
            popNums[ac]->setDisabled(true);
        }
        // Create chart but don't show it yet
        MakePlots(m_lifestagelists);
    }
}

void MainWindow::MakePlots(int a_no_series)
{
    // Line colours adjust as you like
    QColor colours[14] =
    {
        QColor(0,0,255), //blue
        QColor(211,211,211), //lightgrey
        QColor(0,128,0), //green
        QColor(220,20,60), //crimson
        QColor(244,164,96), //sandybrown
        QColor(255,0,255), // magenta
        QColor(102, 205, 170), //mediumaquamarine
        QColor(69, 169, 169), //dargrey
        QColor(0,255,0), //lime
        QColor(255, 192, 203), // pink
        QColor(0,0,128),  // navy
        QColor(0, 128, 128), // teal
        QColor(255,255,0), // yellow
        QColor(160, 82, 45) // sienna
    };
    
    m_plot_y.resize(a_no_series);
    // create graph and assign data to it:
    for (int s = 0; s < a_no_series; s++)
    {
        ui->customPlot1->addGraph();
        ui->customPlot1->graph(s)->setData(m_plot_x, m_plot_y[s]);
        ui->customPlot1->graph(s)->setPen(QPen(colours[s]));
    }
    // give the axes some labels:
    ui->customPlot1->xAxis->setLabel("day");
    ui->customPlot1->yAxis->setLabel("number");
    m_plotmax_x = -1;
    m_plotmax_y = -1;
    ui->customPlot1->setInteraction(QCP::iRangeDrag, true);
        //ui->customPlot1->legend->setVisible(true);

}

void MainWindow::Replot1(int a_x)
{
    // add the data
    m_plot_x.append(a_x);
    // rescale axes if needed
    if (m_plotmax_x < a_x)
    {
        m_plotmax_x = a_x + 10; // jump in steps of 10 days to avoid doing this every step
        ui->customPlot1->xAxis->setRange(0, m_plotmax_x);
    }
    if (m_reploty)
    {
        ui->customPlot1->yAxis->setRange(0, m_plotmax_y);
    }
    // plot the chart:
    for (int s = 0; s < m_lifestagelists; s++)
    {
        ui->customPlot1->graph(s)->setData(m_plot_x, m_plot_y[s]);
    }
    ui->customPlot1->replot();
    m_reploty = false;

}
void MainWindow::DataInit()
{
  // Application flags and data
  m_ProbesSet = false;
  m_NoProbes = 0;
  m_time = 0;
  m_Year = 0;
  m_Steps = 0;
  m_SimInitiated = false;
  // End application data & flags
}

int MainWindow::getSubValue() {
    return ui->subSpinBox->value();
}

int MainWindow::getCurrentSpecies() {
    return m_currentSpecies;
}

Population_Manager_Base* MainWindow::getPopManager() {
    return m_AManager.get();
}

std::shared_ptr<Population_Manager_Base> MainWindow::getPopManager_smart() {
    return m_AManager;
}
// To get these functions, right click in action editor in
// embedded designer (click mainwindow.ui file in projects browser)
// Then pick go to slot. Creator will add a function stub.
void MainWindow::on_actionStart_triggered()
{
    m_BatchRun = false;
    ui->animalComboBox->setEnabled(false);
    m_currentSpecies = ui->animalComboBox->currentIndex() - 1; // Added option for "None"
    /*
    ui->stepSizeCB->setEnabled(false);
    ui->stepSpinBox->setEnabled(false);
    ui->itComboBox->setEnabled(false);
    */
    if (m_currentSpecies==TOP_ApisRAM)
          ui->actionHive_Tool->setEnabled(true);
    Go();
}

void MainWindow::on_actionPhoto_triggered()
{
    QString  current_msg = ui->statusBar->currentMessage();
    ui->statusBar->showMessage("Taking a Landscape photo");
    QCoreApplication::processEvents();

    HabitatMap habMapPhoto(5000);
    habMapPhoto.mainWindow = this;
    habMapPhoto.SetSimExtent(m_ALandscape->SupplySimAreaWidth(), m_ALandscape->SupplySimAreaHeight());
    habMapPhoto.SetLandscape(m_ALandscape);
   // habMapPhoto.SetUpMapImage();
    habMapPhoto.currentView = habMap.currentView;
    habMapPhoto.DrawLandscape();
    ShowAnimals(&habMapPhoto);
    QImage img(habMapPhoto.imageData(), habMapPhoto.mapSize(), habMapPhoto.mapSize(), QImage::Format_RGB888);
    QVariant var((int)time(NULL));
    img.save("ALMaSSMap_photo_"+var.toString()+".png", "png", 100);

    ui->statusBar->showMessage(current_msg);
}

void MainWindow::Go()
{
  std::cout << "start triggered\n";
  // Go figures out whether the simulation is already running, and if not
  // starts a new one, otherwise it runs for however long the form says
  if ( !m_SimInitiated )
  {
    // Randomize the random generator.
    init_random_seed();

    m_torun = GetTimeToRun();
    if ( m_torun > 0 )
    {
      if ( !ReadBatchINI("TI_inifile.ini") )
      {
	return;
      }
      ui->statusBar->showMessage("Creating the landscape. This may take a while...");
      QCoreApplication::processEvents();

      g_date = new Calendar;
      cout << "Created Calendar Object" << endl;

      g_weather = new Weather;
      cout << "Created Weather Object " << endl;

      g_letype = new LE_TypeClass;
      cout << "Created LE_TypeClass Object" << endl;

      g_crops = new PlantGrowthData;
      cout << "Created PlantGrowthData Object " << endl;

      g_nectarpollen = CreatePollenNectarDevelopmentData();
      cout << "Created PollenNectarDevelopmentData Object" << endl;

      printf("Creating Landscape Object ... \n");
      CreateLandscape();
      printf("Landscape Object Done\n");

      /* Now latitude and longitude given, or using DK as default. Calculate daylength */
      g_date->CreateDaylength(m_ALandscape->SupplyLatitude(), m_ALandscape->SupplyLongitude(), m_ALandscape->SupplyTimezone());

      g_pest = new Pesticide(m_ALandscape);
      cout << "Created Pesticide Object" << endl;
      m_ALandscape->SetThePopManagerList(m_PopulationManagerList);
      m_ALandscape->RunHiddenYear();

      ui->statusBar->showMessage("Creating the Population Manager.");
      QCoreApplication::processEvents();
      cout << "Creating the Population Manager" << endl;
      if (!CreatePopulationManager())
      {
        g_msg->Warn(  "Population Manager Not Created! ", m_currentSpecies);
        exit(0);
      }
      
      setUpForm();
      // Now got to get the probe files read in
      GetProbeInput_ini();
      // Ready to go
      m_time = 0;
      m_SimInitiated = true;
      m_StopSignal = false;
      //habMap.DrawLandscape();
    }
  }
  else
  {
    m_torun = GetTimeToRun();
  }
  if ( m_SimInitiated )
  {
    RunTheSim();
  }
}

bool MainWindow::ReadBatchINI(const char *filename) {
  // Must read the TIBatch.INI
  // Read the INI file
  ifstream Fi;
  //int answer = wxYES;
  //wxString filestr = m_IniFileEdit->GetLineText( 0 );
  //Fi.open(filestr.char_str());
  Fi.open(filename);
  while ( !Fi.is_open() )
  {
      g_msg->Warn( filename, "file not found");
      // Issue and error warning
      //wxString amsg = _T("INI File Missing: TI_inifile.ini");
      //amsg += filestr;
      //answer = wxMessageBox( amsg, _T("Try Again?"), wxYES_NO | wxCANCEL, this );
      QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Try Again", "INI File Missing - Quit?",
                                      QMessageBox::Yes|QMessageBox::No);
    if ( reply != QMessageBox::Yes ) return false;
//	Fi.open(filestr.char_str());
    Fi.open(filename);
  }
  char Data[ 255 ];
  Fi >> m_NoProbes;
  Fi.getline(Data, 255);
  for ( int i = 0; i < m_NoProbes; i++ ) {
    m_files[ i ] = new char[ 255 ];
    Fi.getline(m_files[ i ], 255);
  }
  Fi.getline(m_ResultsDir, 255);


  if ( m_currentSpecies == TOP_Vole )
  {
    // Read the PredBatch.INI file
    ifstream Fi2;
    QMessageBox::StandardButton reply;
    while ( !Fi2.is_open() )
    {
        Fi2.open( "VoleToxPreds.ini");
        if ( !Fi2.is_open() ) 
        {
            // Issue and error warning
            QString amsg = ("Predator Batch File Missing: ");
            // dww need to add the following line in qt too.
            // amsg += wxString::Format( _T("%s"), _T("VoleToxPreds.ini") );
            //answer = wxMessageBox( amsg, _T("Try Again?"), wxYES_NO | wxCANCEL, this );
            reply = QMessageBox::question(this, "Try Again", amsg+"Quit?",
                                      QMessageBox::Yes|QMessageBox::No);
            //}
            if (reply != QMessageBox::Yes ) return false;
        }
    }
    Fi2 >> m_NoPredProbes;
    Fi2.getline(Data, 255);
    for ( int i = 0; i < m_NoPredProbes; i++ ) 
    {
        m_Predfiles[ i ] = new char[ 255 ];
        Fi2.getline(m_Predfiles[ i ], 255);
    }
    Fi2.getline(m_PredResultsDir, 255 );
    Fi2.close();
  }

  if ( m_BatchRun ) {
      Fi >> m_torun;
      m_torun *= 365; // the number of years and multiplied by 365 to get days
  }
  Fi.close();
  return true;
}

void MainWindow::GetProbeInput_ini() {
  char Nme[ 511 ];
  ofstream* AFile = NULL;
  for ( int NProbes = 0; NProbes < m_NoProbes; NProbes++ ) {
    // Must read the probe from a file
    m_AManager->TheProbe[ NProbes ] = new probe_data;
    m_AManager->ProbeFileInput( ( char * ) m_files[ NProbes ], NProbes );
    char NoProbesString[ 512 ];
    sprintf( NoProbesString, "%sProbe%d.res", m_ResultsDir, NProbes );
    if ( NProbes == 0 ) {
      AFile = m_AManager->TheProbe[ NProbes ]->OpenFile( NoProbesString );
      if ( !AFile->is_open() ) {
        m_ALandscape->Warn( "MainWindow::GetProbeInput_ini() - cannot open Probe File", "" );
        exit( 1 );
      }
    } else m_AManager->TheProbe[ NProbes ]->SetFile( AFile );
  }

  if ( m_currentSpecies == TOP_Vole ) {
    for ( int NProbes = 0; NProbes < m_NoPredProbes; NProbes++ ) {
      m_PredatorManager->TheProbe[ NProbes ] = new probe_data;
      m_PredatorManager->ProbeFileInput( ( char * ) m_Predfiles[ NProbes ], NProbes );
      sprintf( Nme, "%sPredProbe%d.res", m_ResultsDir, NProbes + 1 );
      //      strcpy( Nme, m_PredResultsDir );
      if ( NProbes == 0 ) {
        AFile = m_PredatorManager->TheProbe[ NProbes ]->OpenFile( Nme );
        if ( !AFile ) {
          m_ALandscape->Warn( "BatchALMSS - cannot open Probe File", Nme );
          exit( 1 );
        }
      } else m_PredatorManager->TheProbe[ NProbes ]->SetFile( AFile );
    }
  }
}


void MainWindow::setUIMapSize()
{
    ui->map->setFixedHeight(__MAPSIZE);
    ui->map->setFixedWidth(__MAPSIZE);
}

void MainWindow::CreateLandscape()
{
  // Create the landscape
  m_ALandscape = new Landscape();
  // dww need to set these in a qt way

  habMap.SetSimExtent( m_ALandscape->SupplySimAreaWidth(), m_ALandscape->SupplySimAreaHeight() );
  habMap.SetLandscape( m_ALandscape );
  habMap.SetUpMapImage();
  setUIMapSize();
}


bool MainWindow::CreatePopulationManager() {
  // SET UP THE ANIMAL POPULATION
  // THE LANDSCAPE MUST BE SETUP BEFORE THE CALL HERE
  if ( !m_ALandscape ) {
      QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Landscape not created", "Quit?",
                                      QMessageBox::Yes|QMessageBox::No);
    //wxMessageBox( _T("ALMaSSGUI::CreatePopulationManager: Landscape Not Created"), _T("ERROR"), wxOK, this );
    return false;
  }
  g_Species = TTypesOfPopulation(m_currentSpecies);

  m_ALandscape->SetThePopManagerList(m_PopulationManagerList);
  if (m_currentSpecies == -1) // None 
  {
      m_AManager = std::make_shared<None_Population_Manager> (m_ALandscape);
      // Do not add it to the g_PopulationManagerList
  }

  if ( m_currentSpecies == TOP_Skylark) {
    m_AManager = std::make_shared<Skylark_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_Skylark);
  }

  if ( m_currentSpecies == TOP_Vole) {
    m_AManager = std::make_shared<Vole_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_Vole);
    m_PredatorManager = std::make_shared<TPredator_Population_Manager> ( m_ALandscape, static_cast<Vole_Population_Manager*>(m_AManager.get()) );
    m_PopulationManagerList->SetPopulation(m_PredatorManager, TOP_Predators);
  }

  if (m_currentSpecies == TOP_Erigone) {
      m_AManager = std::make_shared<Erigone_Population_Manager>(m_ALandscape);
      m_PopulationManagerList->SetPopulation(m_AManager, TOP_Erigone);
  }

  if (m_currentSpecies == TOP_Haplodrassus) {
      m_AManager = std::make_shared<Haplodrassus_Population_Manager>(m_ALandscape);
      m_PopulationManagerList->SetPopulation(m_AManager, TOP_Haplodrassus);
  }

  if ( m_currentSpecies == TOP_Bembidion) {
    m_AManager = std::make_shared<Bembidion_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_Bembidion);
  }

  if (m_currentSpecies == TOP_Hare) { // Hare
      m_AManager = std::make_shared<THare_Population_Manager>(m_ALandscape);
	  m_PopulationManagerList->SetPopulation(m_AManager, TOP_Hare);
  }

  if ( m_currentSpecies == TOP_Partridge ) {
    m_AManager = std::make_shared<Partridge_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_Partridge);
  }

  if ( m_currentSpecies == TOP_Goose) {
    m_AManager = std::make_shared<Goose_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_Goose); // sets the population to null
    m_Hunter_Population_Manager = std::make_shared<Hunter_Population_Manager>(m_ALandscape);
    CfgInt cfg_DisturbersSetNo( "DISTURBERS_SETS_NO", CFG_CUSTOM, 1  );
    if (cfg_DisturbersSetNo.value()>0){
        cout<<"Disturbers are ON. Initiating.\n";
        m_Disturber_Population_Manager = std::make_shared<Disturber_Population_Manager>(cfg_DisturbersSetNo.value(), m_ALandscape, TOP_Goose);
        m_PopulationManagerList->SetPopulation(m_Disturber_Population_Manager, TOP_Disturbers);
        m_Disturber_Population_Manager->Init();
    }

    m_PopulationManagerList->SetPopulation(m_Hunter_Population_Manager, TOP_Hunters);

  }

  if (m_currentSpecies == TOP_RoeDeer)
  {
    m_AManager = std::make_shared<RoeDeer_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation( m_AManager, TOP_RoeDeer );
  }

  if (m_currentSpecies == TOP_Rabbit)
  {
    m_AManager = std::make_shared<Rabbit_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation( m_AManager, TOP_Rabbit );
  }

  if (m_currentSpecies == TOP_Newt)
  {
      m_AManager = std::make_shared<Newt_Population_Manager>(m_ALandscape);
      m_PopulationManagerList->SetPopulation(m_AManager, TOP_Newt);
  }

  if (m_currentSpecies == TOP_Osmia)
  {
    m_AManager = std::make_shared<Osmia_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_Osmia);
  }

  if (m_currentSpecies == TOP_ApisRAM)
  {
      g_honeybee_population_manager = std::make_shared<HoneyBee_Colony>(m_ALandscape);
      m_AManager=g_honeybee_population_manager;
      m_PopulationManagerList->SetPopulation(m_AManager, TOP_ApisRAM);
  }

  if (m_currentSpecies == TOP_OliveMoth)
  {
    cout << "Making Olive Moth<n";
    m_AManager = std::make_shared<OliveMoth_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_OliveMoth);
  }

  if (m_currentSpecies == TOP_OliveFly)
  {
    cout << "Making Olive Fly<n";
    m_AManager = std::make_shared<OliveFly_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_OliveFly);
  }

  if (m_currentSpecies == TOP_Lacewing)
  {
    cout << "Making SubPopulation<n";
    m_AManager = std::make_shared<SubPopulation_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_Lacewing);
  }

  if (m_currentSpecies == TOP_Aphid)
  {
    cout << "Making Aphid<n";
    m_AManager = std::make_shared<Aphid_Population_Manager>(m_ALandscape);
    m_PopulationManagerList->SetPopulation(m_AManager, TOP_Aphid);
  }

  if (m_currentSpecies== TOP_Ladybird)
  {
      cout<< "Making Ladybird\n";
      std::shared_ptr<AphidsD_Population_Manager> apMan ( new AphidsD_Population_Manager(m_ALandscape));
      m_PopulationManagerList->SetPopulation(apMan, TOP_AphidsD);
      std::shared_ptr<Ladybird_Population_Manager> lbMan(new Ladybird_Population_Manager(m_ALandscape)) ;
      lbMan->Init();
      m_AManager = lbMan;

      // here we will add a new PM for
      m_PopulationManagerList->SetPopulation(m_AManager, TOP_Ladybird);
  }
  if ( m_currentSpecies == TOP_Poecilus) {
        cout<< "Making Poecilus\n";
        m_AManager = std::make_shared<Poecilus_Population_Manager>(m_ALandscape);
        m_PopulationManagerList->SetPopulation(m_AManager, TOP_Poecilus);
  }

  if (m_currentSpecies == TOP_Bombus)
  {
      m_AManager = std::make_shared<Bombus_Population_Manager>(m_ALandscape);
      m_PopulationManagerList->SetPopulation(m_AManager, TOP_Bombus);
  }
  if (m_currentSpecies == TOP_Theoretical_1)
  {
      m_AManager = std::make_shared<Theoretical1_Population_Manager>(m_ALandscape);
      m_PopulationManagerList->SetPopulation(m_AManager, TOP_Theoretical_1);
  }
  /*
  if (currentSpecies == TOP_Aphid) { // Aphid
	  Aphid_Population_Manager * hMan = new Aphid_Population_Manager(m_ALandscape);
	  m_AManager = hMan;
	  m_PopulationManagerList->SetPopulation(m_AManager, TOP_Aphid);
  }
*/

  m_AManager->SetNoProbesAndSpeciesSpecificFunctions(m_NoProbes);

  #ifdef __FLOWERTESTING
    m_ALandscape->SetSpeciesFunctions(TOP_ApisRAM);
  #endif

  return true;
}




/*
   cout << "Making Olive Moth<n";
   OliveMoth_Population_Manager * omMan = new OliveMoth_Population_Manager(m_ALandscape);
   m_AManager = omMan;
   g_PopulationManagerList.SetPopulation(m_AManager, TOP_OliveMoth);


  m_AManager->SetNoProbesAndSpeciesSpecificFunctions(m_NoProbes);
  return true;
}
*/

//------------------------------------------------------------------------------

int MainWindow::GetTimeToRun() {
  // Get the time to run in days
  const int daysinmonths[ 13 ] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
  int torun = 0;
  int month;
  // First the number of whatever
//  int anum = item7->GetValue();
  int anum = ui->stepSpinBox->value();
  // Now figure out whatever
//  switch ( item8->GetSelection() ) {
  switch (ui->itComboBox->currentIndex()) {
    case 0: // Year
      torun = anum * 365;
    break;
    case 1: // Month
      if ( m_SimInitiated ) month = m_ALandscape->SupplyMonth(); else
        month = 1; // if we have not started it must be January
      for ( int i = 0; i < anum; i++ ) {
        torun += daysinmonths[ month ];
        if ( ++month == 13 ) month = 1;
      }
    break;
    case 2: // Day
      torun = anum;
    break;
    case 3: // TimeStep
      torun = anum;
    break;
    case - 1:
      // No selection
    break;
  }
  return torun;
}

void MainWindow::CloseDownSim()
{
  if ( m_SimInitiated )
  {
    m_ALandscape->SimulationClosingActions();
    if ( m_AManager ) {
      // Close the probe file
      if ( m_AManager->TheProbe[ 0 ] != NULL ) m_AManager->TheProbe[ 0 ]->CloseFile();
      // delete all probes
      for ( int i = 0; i < m_NoProbes; i++ ) delete m_AManager->TheProbe[ i ];
    }
    delete m_ALandscape;
    m_time = 0;
    //item9->Enable( true );
  }
  m_Year = 0;
  m_SimInitiated = false; // so we can run another if we want to later
  //m_StartButton->Enable( true );
  //m_BatchButton->Enable( true );
}


void MainWindow::RunTheSim() {
    QLineEdit* popNums[14] = { ui->popNum1,ui->popNum2,ui->popNum3,ui->popNum4,ui->popNum5,ui->popNum6,ui->popNum7, ui->popNum8,ui->popNum9,ui->popNum10,ui->popNum11,ui->popNum12,ui->popNum13,ui->popNum14 };
    ui->statusBar->showMessage("Running the simulation.");
    QCoreApplication::processEvents();
    //cout << "Running Sim. Species number:  " << currentSpecies << std::endl;
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(m_torun);
    // m_bmpout = (_T("ALMaSSbmpOut_0000.bmp"));

    // Add species with non-1 day timestep below
    int stepsize = 1;
    if (m_currentSpecies == TOP_RoeDeer) stepsize = 144;
    if (m_currentSpecies == TOP_Goose) stepsize = 144;
    if (m_currentSpecies == TOP_ApisRAM) stepsize = 144;
    if (m_currentSpecies == TOP_Bombus) stepsize = 144;


    for (int days = 0; days < m_torun; days++) {
        QCoreApplication::processEvents();
        if (m_Paused) {
            QCoreApplication::processEvents();
            days--;
            ui->statusBar->showMessage("The simulation is paused.");
        }
        else
        {
            QCoreApplication::processEvents();
            ui->statusBar->showMessage("Turning the world.");

            m_ALandscape->TurnTheWorld();

            QCoreApplication::processEvents();

            m_time++;
            QString day = QString::number(m_ALandscape->SupplyDayInMonth()).rightJustified(2, '0') += '-';
            QString month = QString::number(m_ALandscape->SupplyMonth()).rightJustified(2, '0') += '-';
            QString year = QString::number(m_ALandscape->SupplyYearNumber()).rightJustified(4, '0'); // Can also ask for year in which case the weather simulation year is used, but this does not seem useful information because this is usually looped.
            day.append(month);
            day.append(year);
            ui->dateDisplay->setText(day);

            int stepsize = 1;
            /*  ***CJT*** removed because the stepsize must be fixed for each species, it cannot be changed at will
            if (ui->stepSizeCB->isEnabled())
            {
                if (ui->stepSizeCB->currentIndex() == 1)
                    stepsize = 24;

                if (ui->stepSizeCB->currentIndex() == 2)
                    stepsize = 144;
            }
            */

            // Add species with non-1 day timestep below
            if (m_currentSpecies == TOP_RoeDeer) stepsize = 144;
            if (m_currentSpecies == TOP_Goose) stepsize = 144;
            if (m_currentSpecies == TOP_ApisRAM) stepsize = 144;
            if (m_currentSpecies == TOP_Bombus) stepsize = 144;

            ui->statusBar->showMessage("Running the main model.");
            QCoreApplication::processEvents();

            for (int withindaystep = 0; withindaystep < stepsize; ++withindaystep)
            {
                QCoreApplication::processEvents();

                for (int p = 0; p < TOP_foobar; p++)
                {
                    if (m_PopulationManagerList->GetPopulation(p) != nullptr){
                        m_PopulationManagerList->GetPopulation(p)->Run(1);

                    }
                }

                UpdateGraphics(withindaystep);
                unsigned int nAnimals;
                // If it is beetle we use eggLists for eggs:


                for (int ac = 0; ac < m_lifestagelists; ++ac)
                {
                    nAnimals = m_AManager->GetPopulationSize(ac);


                    QString number = QString::number(nAnimals);
                    popNums[ac]->setText(number);
                }
                ui->stepLCD->display((days * stepsize) + withindaystep);

                /* ? */
                if (g_date->TickMinute10())
                    g_date->TickHour();

            }

            QCoreApplication::processEvents();

            auto ssr = m_AManager->SpeciesSpecificReporting(m_currentSpecies, m_time);

            ui->progressBar->setValue(days + 1);
            for (int ac = 0; ac < m_lifestagelists; ++ac)
            {
                int nAnimals = m_AManager->GetLiveArraySize(ac);
                m_plot_y[ac].append(nAnimals);
                if (nAnimals > m_plotmax_y) {
                    m_reploty = true;
                    m_plotmax_y = round(nAnimals * 1.05);
                }
            }
            if (ui->checkBoxPlot1->isChecked()) Replot1(m_ALandscape->SupplyGlobalDate()-364);

            // QCoreApplication::processEvents();

            // NEED THESE TWO !!!!!!!!!!!!!!!!
        //    if (cfg_animate.value()) habMap.DrawLandscape();
         //   ShowAnimals();
            // If we want to dump the bmp map to a file then we need to do it here.


            //if (cfg_dumpmap.value()) DumpMap(i);

          //  while ( wxTheApp->Pending() ) wxTheApp->Dispatch();

            /*habMap.ShowBitmap2();*/
            if (m_currentSpecies == TOP_Vole) m_PredatorManager->ProbeReport(m_time);

            /*
            wxString astr(m_AManager->SpeciesSpecificReporting(currentSpecies, m_time), wxConvUTF8);
            if (astr.length()>0)
            m_ProbesBox->InsertItems( 1, &astr, 0 );
                    */

            m_ALandscape->DumpVegAreaData(m_time);
            
            // QCoreApplication::processEvents();

            //  Update();
            
            if (m_StopSignal)
            {
                m_torun = -1;
                CloseDownSim();
                ui->map->clear();
                ui->progressBar->setValue(0);
                ui->statusBar->showMessage("The simulation is stopped.");
            }

        }
    }
}

void MainWindow::UpdateGraphics(int withinadaystep)
{
    if (drawingMap)
    {
        // Only if the first time in the day 
        if (withinadaystep==0){
            habMap.DrawLandscape();
        }
        
        ShowAnimals(&habMap);
        
        QImage img(habMap.imageData(), habMap.mapSize(), habMap.mapSize(), QImage::Format_RGB888);

        //This is the special code for drawing hive and foragers for apisram model
        if (m_currentSpecies==TOP_ApisRAM)
        {
          QPainter painter(&img);
          QPen pen;
          pen.setWidth(20);
          pen.setColor(Qt::green);
          painter.setPen(pen);
          int d_x = -1;
          int d_y = -1;
          habMap.ConvertForZoom(d_x, d_y, g_honeybee_population_manager->supplyHivePositionX(), g_honeybee_population_manager->supplyHivePositionY());
          if (d_x>-1 && d_y>-1){
             painter.drawPoint(d_x, d_y);
          }
          
          int i = 3;//we only display the worker adult bees in the landscape when they are out
          if (isAnimalChecked(i))
          {
            int x, y;
            unsigned noInList = m_AManager->GetLiveArraySize(i);
            pen.setWidth(5);
            pen.setColor(Qt::red);
            painter.setPen(pen);
            for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
            {
                m_AManager->SupplyLocXY( i, anAnimalRef, x, y );
                if (!(g_honeybee_population_manager->isInHive<HoneyBee_Worker>(i,anAnimalRef)))
                {
                  int d_x = -1;
                  int d_y = -1;
                  habMap.ConvertForZoom(d_x, d_y, x, y);
                  if (d_x>10 && d_y>10){
                    painter.drawPoint(d_x, d_y);
                    }
                }
            }
          }
          painter.end();
        }
        ui->map->setPixmap(QPixmap::fromImage(img));
        //int min_size = min(ui->map->height(), ui->map->width());
        //ui->map->setPixmap(QPixmap::fromImage(img).scaled(min_size, min_size, Qt::KeepAspectRatio, Qt::SmoothTransformation));        
        
    }
    if (hivetool)
    {
        hivetool->doDraw();
    }
}


void MainWindow::ShowAnimals(HabitatMap *hm ) {
    
  int x, y;
  if (m_currentSpecies == TOP_Vole)
  {
    /*
    // Special code for genetic demo
    */
    if (ui->spBox6->isChecked() )
    {
        std::shared_ptr<Vole_Population_Manager> VPM;
        VPM = dynamic_pointer_cast<Vole_Population_Manager>(m_AManager);
        for ( int lstages=0; lstages<4; lstages++)
        {
            unsigned noInList = m_AManager->GetLiveArraySize(lstages);
            for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
            {
                VPM->SupplyLocXY( lstages, anAnimalRef, x, y );
                // Get the genetic colour
                unsigned gcolour = 3;
                unsigned gcolour1 = VPM->GetVoleAllele(lstages, anAnimalRef, 3, 0);
                unsigned gcolour2 = VPM->GetVoleAllele(lstages, anAnimalRef, 3, 1);
                if ((gcolour1 == 1) && (gcolour2 ==1))  gcolour=4; else if ((gcolour1 == 1) || (gcolour2 ==1)) gcolour = 2;
                hm->Spot( gcolour, x, y );
            }
        }
        return;
    }
  }
  /*
  else
  {
      if (ui->spBox6->isChecked())
      {
          unsigned noInList = m_AManager->GetLiveArraySize(5);
        for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
        {
          m_AManager->SupplyLocXY( 5, anAnimalRef, x, y );
          hm->Spot( 5, x, y );
        }
      }
      //return;
  }*/
 
 /*
  if (m_currentSpecies==TOP_ApisRAM)
  {
    hm->Spot(2, g_honeybee_population_manager->supplyHivePositionX(), g_honeybee_population_manager->supplyHivePositionY());//draw the hive
    int i = 3;//we only display the worker adult bees in the landscape when they are out
    if (isAnimalChecked(i))
    {
      unsigned noInList = m_AManager->GetLiveArraySize(i);
      for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
      {
          m_AManager->SupplyLocXY( i, anAnimalRef, x, y );
          if (!(g_honeybee_population_manager->isInHive<HoneyBee_Worker>(i,anAnimalRef)))
          {
             hm->Spot( 4, x, y );
          }
      }
    }
    return;
  }
  */
    if (EggListPopulation.find(static_cast <TTypesOfPopulation> (m_currentSpecies)) != EggListPopulation.end()){
        if (isAnimalChecked(0)){
            for ( unsigned anAnimalRef = 0; anAnimalRef < unsigned(m_ALandscape->SupplyDayInYear()); anAnimalRef++ )
            {
                auto eggnum= static_pointer_cast<Beetle_Population_Manager>(m_AManager)->SupplyDailyEggPopSize(anAnimalRef);
                for (int j=0;j<eggnum;j++ ){
                    static_pointer_cast<Beetle_Population_Manager>(m_AManager)->SupplyEggLocXY( anAnimalRef, j, x, y );
                    hm->Spot( 0, x, y );
                }
            }
        }
    }
    
    //display a dot when there is a population in the cell
    if (m_currentSpecies==TOP_OliveMoth || m_currentSpecies==TOP_OliveFly || m_currentSpecies==TOP_Lacewing || m_currentSpecies==TOP_Aphid){
      std::shared_ptr<SubPopulation_Population_Manager> temp_sub = static_pointer_cast<SubPopulation_Population_Manager>(m_AManager);
      int x_step = temp_sub->supplyCellWidth();
      int y_step = temp_sub->supplyCellHeight();
      for (unsigned x_index = 0; x_index < temp_sub->supplyCellNumX(); x_index++){
        for (unsigned y_index = 0; y_index < temp_sub->supplyCellNumY(); y_index++){
          if (temp_sub->getTotalSubpopulationInCell(x_index, y_index)>0){
          //if (temp_sub->getSubpopulationInCellLifeStage(x_index, y_index, 5)>0){
          //if (temp_sub->getSuitabilityInCell(x_index, y_index)>0){
            temp_sub->SupplyLocXY(x_index, y_index, x, y);
            hm->Spot( 0, x, y); 
          }
        }
      }
      return;
    }
    else if(m_currentSpecies==TOP_Bombus){
        int j = 1;

        for (int i = 0; i < 12; ++i)
        {
            if (isAnimalChecked(i) && (i == 0 || i ==  2 || i ==  3  || i ==  7))
            {
                unsigned noInList = m_AManager->GetLiveArraySize(i);
                for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
                {
                    m_AManager->SupplyLocXY( i, anAnimalRef, x, y );
                    hm->Spot( j, x, y );
                }
                j++;
            }
        }
    }

    else{
        for (int i = 0; i < 12; ++i)
        {
            if (isAnimalChecked(i))
            {
                unsigned noInList = m_AManager->GetLiveArraySize(i);
                for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
                {
                    m_AManager->SupplyLocXY( i, anAnimalRef, x, y );
                    hm->Spot( i, x, y );
                }
            }
        }
    }
}

bool MainWindow::isAnimalChecked(const unsigned i) {
    QCheckBox* spboxes[14]={ui->spBox1,ui->spBox2,ui->spBox3,ui->spBox4,ui->spBox5,ui->spBox6,ui->spBox7,
                                 ui->spBox8,ui->spBox9,ui->spBox10,ui->spBox11,ui->spBox12,ui->spBox13,ui->spBox14};
    return spboxes[i]->isChecked();
}

// ////////////////////////// ACTIONS //////////////////////////

void MainWindow::on_actionPause_triggered()
{
//    std::cout << "pause triggered\n";
}

void MainWindow::on_actionContinue_triggered()
{
    std::cout << "Continue triggered\n";
}

void MainWindow::on_actionStop_triggered()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Stop The Simulation", "Really stop? You won't be able to restart.",
                                          QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        std::cout << "Stop triggered\n";
        m_StopSignal=true;
        ui->animalComboBox->setEnabled(true);
        ui->stepSpinBox->setEnabled(true);
        ui->itComboBox->setEnabled(true);
    }
}

void MainWindow::on_stepSpinBox_valueChanged(int arg1)
{
 m_torun=arg1*144;
}

void MainWindow::on_stepSpinBox_valueChanged(const QString &arg1)
{

}

void MainWindow::on_actionMap_toggled(bool arg1)
{
    if (arg1) {
        habMap.currentView=0;
        habMap.DrawLandscape();
        ShowAnimals(&habMap);
        UpdateGraphics();
    }
}

void MainWindow::on_actionVeg_Type_toggled(bool arg1)
{
    if (arg1) {
        habMap.currentView=1;
        habMap.DrawLandscape();
        ShowAnimals(&habMap);
        UpdateGraphics();
    }
}

void MainWindow::on_actionBiomass_toggled(bool arg1)
{
    habMap.currentView=2;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionFarm_ownership_toggled(bool arg1)
{
    habMap.currentView=3;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionPesticide_Load_toggled(bool arg1)
{
    habMap.currentView=4;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionGoose_Numbers_toggled(bool arg1)
{
    habMap.currentView=5;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionGoose_Food_Resource_toggled(bool arg1)
{
    habMap.currentView=6;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionGoose_Grain_Resource_toggled(bool arg1)
{
    habMap.currentView=7;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionGoose_Grazing_Resource_toggled(bool arg1)
{
    habMap.currentView=8;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionSoil_Type_Rabbits_toggled(bool arg1)
{
    habMap.currentView=9;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionHunters_Locations_toggled(bool arg1)
{
    habMap.currentView=10;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionPollen_toggled(bool arg1)
{
    habMap.currentView=11;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_actionNectar_toggled(bool arg1)
{
    habMap.currentView=12;
    habMap.DrawLandscape();
    ShowAnimals(&habMap);
    UpdateGraphics();
}

void MainWindow::on_animalComboBox_currentIndexChanged(int index)
{
}

void MainWindow::on_actionMap_triggered()
{

}

void MainWindow::on_map_clicked(QMouseEvent * e)
{
  if (!GetSimInitiated()) return;
  QPoint p = e->pos();
  //std::cout << "Zoom: " << p.x() << " " << p.y() << std::endl;
  if (ui->actionZoom->isChecked()) {  
    if (e->buttons()==Qt::LeftButton) {
        habMap.ZoomIn(p.x(),p.y());
    }
    else {
        habMap.ZoomOut(p.x(),p.y());
    }
  }
  if (ui->actionInfo->isChecked()) {
    if (e->buttons()==Qt::LeftButton) {
        DisplayMapInfo(p.x(),p.y());
    }
    else {
        FindAnimal(p.x(),p.y());
    }
  }

  UpdateGraphics();

}

void MainWindow::DisplayMapInfo(const unsigned x, const unsigned y) {
    ui->textEdit->clear();
    ui->textEdit->moveCursor(QTextCursor::End);
    ui->textEdit->insertPlainText(habMap.getMapText(x,y));
    ui->textEdit->moveCursor(QTextCursor::End);
}

void MainWindow::FindAnimal(const unsigned x, const unsigned y) {
   ui->textEdit->clear();
   ui->textEdit->moveCursor(QTextCursor::End);
   ui->textEdit->insertPlainText(habMap.getAnimalText(x,y));
   ui->textEdit->moveCursor(QTextCursor::End);
}

void MainWindow::on_map_clicked2()
{
}


void MainWindow::on_actionHive_Tool_triggered()
{
    hivetool=new MWHiveTool(this);
    hivetool->show();
}

void MainWindow::on_actionPause_toggled(bool arg1)
{
    m_Paused=arg1;
}

void MainWindow::on_actionMap_Update_toggled(bool arg1)
{
    drawingMap=arg1;
}

void MainWindow::on_actionExit_triggered()
{
   QMessageBox::StandardButton reply;
   reply = QMessageBox::question(this, "Exit QtALMaSS", "Are you sure you want to quit?",
                                                QMessageBox::Yes|QMessageBox::No);
          if (reply == QMessageBox::Yes) {
          exit(0);
          }
}

void MainWindow::on_actionExit_triggered(bool checked)
{

}
