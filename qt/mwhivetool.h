#ifndef MWHIVETOOL_H
#define MWHIVETOOL_H

#include <QMainWindow>
//#include <honeycombcell.h>
#include <blitz/array.h>
#include "mwhivetool.h"
#include "ui_mwhivetool.h"
//#include "mainwindow.h"
#include <math.h>
#include <QDialog>
#include <QWidget>
#include <QButtonGroup>
#include <QGraphicsPolygonItem>
#include <QBrush>
#include <QPen>
#include <QGraphicsScene>
#include <QActionGroup>
#include "honeycombcell.h"
#include <blitz/array.h>

#include "../ALMaSSDefines.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"

#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../HoneyBee/HoneyBee_Colony.h"
#include <QPainter>
#include <QGraphicsItem>

class QGraphicsScene;
class QActionGroup;
class MainWindow;

namespace Ui {
class MWHiveTool;
}

class MWHiveTool : public QMainWindow
{
    Q_OBJECT
protected:
    QMainWindow*  parent;
    QGraphicsScene *scene;
    QGraphicsScene *temp_scene;
    QActionGroup* seeGroup;
    HoneyBee_Colony * pm;
    Hive * hive;
    int zoom;
    int viewType;
    //Population_Manager * pop_manager;
public:
    explicit MWHiveTool(QWidget *parent = 0);
    ~MWHiveTool();

    void paintEvent(QPaintEvent * p);
    //void drawFrame(blitz::Array<int,2> dataf, blitz::Array<int,2> datab);

    void genTemperature();

    template <class T>
    QGraphicsScene * drawFrameContinuous(blitz::Array<T,2> dataf, blitz::Array<T,2> datab, const T min, const T max)
    {
        scene->clear();
        QGraphicsScene * myscene;
        QColor c;
        float val;
        float dspan;
        //HoneyCombCell *cell;
        auto shp=dataf.shape();
        int XN=shp(0);
        int YN=shp(1);
        const int R=20;
        float XS=sqrt(R*R-pow((R/2),2));
        float YS=R-XS/4.0;
        QBrush brush(Qt::green);
        QPen blackpen(Qt::black);
        //QBrush brush;
        //QPen blackpen;
        blackpen.setWidth(4);
        //scene->clear();
        float x,y;
        float off=YS*YN*.6;
        blackpen.setWidth(1);
        float r = R/2;
        float r2 = r / 2.0;
        float v = sqrt(pow(r,2) - pow(r2,2));

        myscene = NULL;//new QGraphicsScene(this);

        for (int X=0;X<XN;++X) {
            for (int Y=0;Y<YN;++Y) {
                //(x,y) are polygon centers.
                y = Y*YS;
                if ((Y%2) == 0)
                    x = X*XS;
                else
                    x = X*XS+XS/2.0;
                float yy=y;

                dspan=max-min;
                val=(dataf(X,Y)-min)/(dspan)*255;
                if (val > 255) val=255;
                if (val < 0) val=0;
                c = {128,0,(int)val};


                brush.setColor(c);

                y=y-off;
                y=-y;
                QPolygonF polyf;// = new QPolygonF();
                polyf << QPointF(x,y+r)
                      << QPointF(x+v,y+r2)
                      << QPointF(x+v,y-r2)
                      << QPointF(x,y-r)
                      << QPointF(x-v,y-r2)
                      << QPointF(x-v,y+r2);

                scene->addPolygon(polyf,blackpen,brush);

                val=(datab(X,Y)-min)/(dspan)*255;
                if (val > 255) val=255;
                if (val < 0) val=0;
                c = {128,0,(int)val};

                brush.setColor(c);

                y=yy;
                y=y+off;
                y=-y;
                QPolygonF polyf2; //= new QPolygonF();
                polyf2 << QPointF(x,y+r)
                      << QPointF(x+v,y+r2)
                      << QPointF(x+v,y-r2)
                      << QPointF(x,y-r)
                      << QPointF(x-v,y-r2)
                      << QPointF(x-v,y+r2);

                scene->addPolygon(polyf2,blackpen,brush);
            }
        }
        ui->graphicsView->setScene(scene);
        return myscene;
    }

    template <class T>
    QGraphicsScene * drawFrameCategorical(blitz::Array<T,2> dataf, blitz::Array<T,2> datab)
    {
        scene->clear();
        QGraphicsScene * myscene;

        QColor c;
        T val;
        //float dspan;
        //HoneyCombCell *cell;
        auto shp=dataf.shape();
        int XN=shp(0);
        int YN=shp(1);
        const int R=20;
        float XS=sqrt(R*R-pow((R/2),2));
        float YS=R-XS/4.0;

        myscene = NULL;//new QGraphicsScene(this);

        QBrush brush(Qt::green);
        QPen blackpen(Qt::black);
        blackpen.setWidth(4);
        //scene->clear();
        float x,y;
        float off=YS*YN*.6;
        blackpen.setWidth(1);
        float r = R/2;
        float r2 = r / 2.0;
        float v = sqrt(pow(r,2) - pow(r2,2));


        QColor colours[19] = {Qt::lightGray,        // 0 NotPresent
                             Qt::lightGray,        // 1 Empty
                             Qt::darkYellow,       // 2 Nectar
                             Qt::yellow,           // 3 Honey
                             Qt::darkGreen,        // 4 Pollen
                             Qt::darkRed,  // 5 Marked Nectar
                             Qt::red,  // 6 Marked Pollen
                             Qt::blue,         // 7 Marked Egg
                             Qt::white,            // 8 Egg layed
                             Qt::darkMagenta,  // 9 Larva
                             Qt::magenta,          // 10 Pupa
                             Qt::darkBlue,         // 11 Worker
                             Qt::blue,         // 12 D Marked Egg
                             Qt::white,            // 13 D Egg layed
                             Qt::darkMagenta,  // 14 D Larva
                             Qt::magenta,          // 15 D Pupa
                             Qt::darkBlue,         // 16 Drone
                             Qt::darkBlue,         // 17 Queen
                             Qt::black};           // 18 DeadBody
        QString annotations[19] = {QString(""),
                                   QString(""),
                                   QString("N"),
                                   QString("H"),
                                   QString("P"),
                                   QString("M"),
                                   QString("M"),
                                   QString("M"),
                                   QString("E"),
                                   QString("L"),
                                   QString("P"),
                                   QString("A"),
                                   QString("M"),
                                   QString("E"),
                                   QString("L"),
                                   QString("P"),
                                   QString("A"),
                                   QString("A"),
                                   QString("D"),
                                  };
        for (int X=0;X<XN;++X) {
            for (int Y=0;Y<YN;++Y) {
                //(x,y) are polygon centers.
                y = Y*YS;
                if ((Y%2) == 0)
                    x = X*XS;
                else
                    x = X*XS+XS/2.0;

                float yy=y;

                val=dataf(X,Y);
                c = colours[val];
                brush.setColor(c);

                y=y-off;
                y=-y;
                QPolygonF polyf;
                polyf << QPointF(x,y+r)
                      << QPointF(x+v,y+r2)
                      << QPointF(x+v,y-r2)
                      << QPointF(x,y-r)
                      << QPointF(x-v,y-r2)
                      << QPointF(x-v,y+r2);
                scene->addPolygon(polyf,blackpen,brush);

                if (ui->annotationsCB->isChecked())
                {
                    //QGraphicsTextItem * an = new QGraphicsTextItem;
                    //an->setPos(x-r,y-r);
                    //an->setPlainText(annotations[val]);
                    //an->setDefaultTextColor(Qt::black);
                    //an->setZValue(1);
                    //myscene->addItem(an);
                }

                val=datab(X,Y);
                c = colours[val];
                brush.setColor(c);

                y=yy;
                y=y+off;
                y=-y;
                QPolygonF polyf2;
                polyf2 << QPointF(x,y+r)
                      << QPointF(x+v,y+r2)
                      << QPointF(x+v,y-r2)
                      << QPointF(x,y-r)
                      << QPointF(x-v,y-r2)
                      << QPointF(x-v,y+r2);

                scene->addPolygon(polyf2,blackpen,brush);

                if (ui->annotationsCB->isChecked())
                {
                    //QGraphicsTextItem * an2 = new QGraphicsTextItem;
                    //an2->setPos(x-r,y-r);
                    //an2->setPlainText(annotations[val]);
                    //an2->setDefaultTextColor(Qt::black);
                    //an2->setZValue(1);
                    //scene->addItem(an2);
                }
            }
        }
        ui->graphicsView->setScene(scene);
        //scene->setSceneRect(0,0,XN*XS,YN*YS);
        return myscene;
    }


    void doDraw();

private slots:
    void on_zoomDial_sliderMoved(int position);

    void on_boxSB_valueChanged(int arg1);

    void on_frameSB_valueChanged(int arg1);

    void on_actionCell_Types_toggled(bool arg1);

    void on_actionBee_count_toggled(bool arg1);

    void on_actionCell_Types_triggered();

    void on_actionBee_count_triggered();

    void on_actionTemperature_triggered();

    void on_actionHoney_triggered();

    void on_actionPollen_triggered();

private:
    Ui::MWHiveTool *ui;
    HoneyCombCell *cell;
};

#endif // MWHIVETOOL_H

