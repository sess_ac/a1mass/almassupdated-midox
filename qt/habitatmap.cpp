#include <QString>

#include "habitatmap.h"
#include "../Landscape/ls.h"

#include "../RodenticideModelling/RodenticidePredators.h"
#include "../BatchALMaSS/PopulationManager.h"

// This is horrible. Just used to access sub box in debug pane
// Try and get rid.
#include "mainwindow.h"
#include "colmap.h"

extern CfgBool l_pest_enable_pesticide_engine;
extern CfgInt l_pest_NoPPPs;
extern CfgBool cfg_rodenticide_enable;
static CfgBool cfg_animate( "G_ANIMATE", CFG_CUSTOM, true );
static CfgBool cfg_dumpmap( "G_DUMPMAP", CFG_CUSTOM, true ); // Produces a set of bitmaps using the next three cfgs to determine start, range and interval
static CfgInt cfg_dumpmapstart( "G_DUMPMAPSTART", CFG_CUSTOM, 366  );
static CfgInt cfg_dumpmapend( "G_DUMPMAPEND", CFG_CUSTOM, 730 );
static CfgInt cfg_dumpmapstep( "G_DUMPMAPSTEP", CFG_CUSTOM, 5 );


palette::palette()
{
    for (int i=0; i < 400; ++i)
    {

        /*
        red.push_back(random(255));
        green.push_back(random(255));
        blue.push_back(random(255));
    */

      red.push_back(char(255));
      green.push_back(char(255));
      blue.push_back(char(255));

    }


    almasscmap::colourmap colours = almasscmap::makeColourMap();
    almasscmap::rgbarray colour;

    colour=colours["CORAL"];
    red[tole_Hedges]=colour[0];
    green[tole_Hedges]=colour[1];
    blue[tole_Hedges]=colour[2];

    colour=colours["GOLD"];
    red[tole_RoadsideVerge]=colour[0];
    green[tole_RoadsideVerge]=colour[1];
    blue[tole_RoadsideVerge]=colour[2];

    colour=colours["DARK ORCHID"];
    red[tole_Railway]=colour[0];
    green[tole_Railway]=colour[1];
    blue[tole_Railway]=colour[2];

    colour=colours["PALE GREEN"];
    red[tole_FieldBoundary]=colour[0];
    green[tole_FieldBoundary]=colour[1];
    blue[tole_FieldBoundary]=colour[2];

    colour=colours["SIENNA"];
    red[tole_Marsh]=colour[0];
    green[tole_Marsh]=colour[1];
    blue[tole_Marsh]=colour[2];

    colour=colours["THISTLE"];
    red[tole_Scrub]=colour[0];
    green[tole_Scrub]=colour[1];
    blue[tole_Scrub]=colour[2];

    colour=colours["WHEAT"];
    red[tole_Field]=colour[0];
    green[tole_Field]=colour[1];
    blue[tole_Field]=colour[2];

    colour=colours["MEDIUM SEA GREEN"];
    red[tole_PermPastureLowYield]=colour[0];
    green[tole_PermPastureLowYield]=colour[1];
    blue[tole_PermPastureLowYield]=colour[2];

    colour=colours["SPRING GREEN"];
    red[tole_PermPastureTussocky]=colour[0];
    green[tole_PermPastureTussocky]=colour[1];
    blue[tole_PermPastureTussocky]=colour[2];

    colour=colours["KHAKI"];
    red[tole_PermanentSetaside]=colour[0];
    green[tole_PermanentSetaside]=colour[1];
    blue[tole_PermanentSetaside]=colour[2];

    colour=colours["MEDIUM FOREST GREEN"];
    red[tole_PermPasture]=colour[0];
    green[tole_PermPasture]=colour[1];
    blue[tole_PermPasture]=colour[2];

    colour=colours["YELLOW"];
    red[tole_NaturalGrassDry]=colour[0];
    green[tole_NaturalGrassDry]=colour[1];
    blue[tole_NaturalGrassDry]=colour[2];

    colour=colours["YELLOW GREEN"];
    red[tole_RiversidePlants]=colour[0];
    green[tole_RiversidePlants]=colour[1];
    blue[tole_RiversidePlants]=colour[2];

    colour=colours["FIREBRICK"];
    red[tole_PitDisused]=colour[0];
    green[tole_PitDisused]=colour[1];
    blue[tole_PitDisused]=colour[2];

    colour=colours["SEA GREEN"];
    red[tole_RiversideTrees]=colour[0];
    green[tole_RiversideTrees]=colour[1];
    blue[tole_RiversideTrees]=colour[2];

    colour=colours["FOREST GREEN"];
    red[tole_DeciduousForest]=colour[0];
    green[tole_DeciduousForest]=colour[1];
    blue[tole_DeciduousForest]=colour[2];

    colour=colours["DARK OLIVE GREEN"];
    red[tole_ConiferousForest]=colour[0];
    green[tole_ConiferousForest]=colour[1];
    blue[tole_ConiferousForest]=colour[2];

    colour=colours["DARK GREEN"];
    red[tole_MixedForest]=colour[0];
    green[tole_MixedForest]=colour[1];
    blue[tole_MixedForest]=colour[2];

    colour=colours["GREEN YELLOW"];
    red[tole_YoungForest]=colour[0];
    green[tole_YoungForest]=colour[1];
    blue[tole_YoungForest]=colour[2];

    colour=colours["GREY"];
    red[tole_StoneWall]=colour[0];
    green[tole_StoneWall]=colour[1];
    blue[tole_StoneWall]=colour[2];

    colour=colours["MEDIUM ORCHID"];
    red[tole_Garden]=colour[0];
    green[tole_Garden]=colour[1];
    blue[tole_Garden]=colour[2];

    colour=colours["ORCHID"];
    red[tole_Track]=colour[0];
    green[tole_Track]=colour[1];
    blue[tole_Track]=colour[2];

    colour=colours["INDIAN RED"];
    red[tole_SmallRoad]=colour[0];
    green[tole_SmallRoad]=colour[1];
    blue[tole_SmallRoad]=colour[2];

    colour=colours["RED"];
    red[tole_LargeRoad]=colour[0];
    green[tole_LargeRoad]=colour[1];
    blue[tole_LargeRoad]=colour[2];

    colour=colours["BLACK"];
    red[tole_Building]=colour[0];
    green[tole_Building]=colour[1];
    blue[tole_Building]=colour[2];

    colour=colours["BROWN"];
    red[tole_Saltmarsh]=colour[0];
    green[tole_Saltmarsh]=colour[1];
    blue[tole_Saltmarsh]=colour[2];

    colour=colours["DARK GREY"];
    red[tole_ActivePit]=colour[0];
    green[tole_ActivePit]=colour[1];
    blue[tole_ActivePit]=colour[2];

    colour=colours["BLUE"];
    red[tole_Freshwater]=colour[0];
    green[tole_Freshwater]=colour[1];
    blue[tole_Freshwater]=colour[2];

    colour=colours["CORNFLOWER BLUE"];
    red[tole_River]=colour[0];
    green[tole_River]=colour[1];
    blue[tole_River]=colour[2];

    colour=colours["GOLDENROD"];
    red[tole_Coast]=colour[0];
    green[tole_Coast]=colour[1];
    blue[tole_Coast]=colour[2];

    colour=colours["AQUAMARINE"];
    red[tole_Saltwater]=colour[0];
    green[tole_Saltwater]=colour[1];
    blue[tole_Saltwater]=colour[2];

    colour=colours["GREEN"];
    red[tole_HedgeBank]=colour[0];
    green[tole_HedgeBank]=colour[1];
    blue[tole_HedgeBank]=colour[2];

    colour=colours["BLUE VIOLET"];
    red[tole_Heath]=colour[0];
    green[tole_Heath]=colour[1];
    blue[tole_Heath]=colour[2];

    colour=colours["PURPLE"];
    red[tole_Orchard]=colour[0];
    green[tole_Orchard]=colour[1];
    blue[tole_Orchard]=colour[2];

    colour=colours["DARK SLATE BLUE"];
    red[tole_OrchardBand]=colour[0];
    green[tole_OrchardBand]=colour[1];
    blue[tole_OrchardBand]=colour[2];

    colour=colours["MAROON"];
    red[tole_MownGrass]=colour[0];
    green[tole_MownGrass]=colour[1];
    blue[tole_MownGrass]=colour[2];

    colour=colours["MEDIUM SPRING GREEN"];
    red[tole_NaturalGrassWet]=colour[0];
    green[tole_NaturalGrassWet]=colour[1];
    blue[tole_NaturalGrassWet]=colour[2];

    colour=colours["LIGHT GREY"];
    red[tole_MetalledPath]=colour[0];
    green[tole_MetalledPath]=colour[1];
    blue[tole_MetalledPath]=colour[2];

    colour=colours["DARK SLATE GRAY"];
    red[tole_RoadsideSlope]=colour[0];
    green[tole_RoadsideSlope]=colour[1];
    blue[tole_RoadsideSlope]=colour[2];

    colour=colours["DIM GREY"];
    red[tole_HeritageSite]=colour[0];
    green[tole_HeritageSite]=colour[1];
    blue[tole_HeritageSite]=colour[2];

    colour=colours["CADET BLUE"];
    red[tole_Stream]=colour[0];
    green[tole_Stream]=colour[1];
    blue[tole_Stream]=colour[2];

    colour=colours["DARK TURQUOISE"];
    red[tole_Carpark]=colour[0];
    green[tole_Carpark]=colour[1];
    blue[tole_Carpark]=colour[2];

    colour=colours["MEDIUM AQUAMARINE"];
    red[tole_Churchyard]=colour[0];
    green[tole_Churchyard]=colour[1];
    blue[tole_Churchyard]=colour[2];

    colour=colours["ORANGE RED"];
    red[tole_Wasteland]=colour[0];
    green[tole_Wasteland]=colour[1];
    blue[tole_Wasteland]=colour[2];

    colour=colours["MEDIUM SEA GREEN"];
    red[tole_IndividualTree]=colour[0];
    green[tole_IndividualTree]=colour[1];
    blue[tole_IndividualTree]=colour[2];

    colour=colours["LIGHT BLUE"];
    red[tole_WindTurbine]=colour[0];
    green[tole_WindTurbine]=colour[1];
    blue[tole_WindTurbine]=colour[2];

    colour=colours["LIGHT STEEL BLUE"];
    red[tole_Vildtager]=colour[0];
    green[tole_Vildtager]=colour[1];
    blue[tole_Vildtager]=colour[2];

    colour=colours["MAGENTA"];
    red[tole_PlantNursery]=colour[0];
    green[tole_PlantNursery]=colour[1];
    blue[tole_PlantNursery]=colour[2];

    colour=colours["PINK"];
    red[tole_WoodyEnergyCrop]=colour[0];
    green[tole_WoodyEnergyCrop]=colour[1];
    blue[tole_WoodyEnergyCrop]=colour[2];

    colour=colours["PLUM"];
    red[tole_WoodlandMargin]=colour[0];
    green[tole_WoodlandMargin]=colour[1];
    blue[tole_WoodlandMargin]=colour[2];

    colour=colours["MAROON"];
    red[tole_Pylon]=colour[0];
    green[tole_Pylon]=colour[1];
    blue[tole_Pylon]=colour[2];

    colour=colours["MEDIUM BLUE"];
    red[tole_Pond]=colour[0];
    green[tole_Pond]=colour[1];
    blue[tole_Pond]=colour[2];

    colour=colours["STEEL BLUE"];
    red[tole_FishFarm]=colour[0];
    green[tole_FishFarm]=colour[1];
    blue[tole_FishFarm]=colour[2];

    colour = colours["DARKORCHID3"];
    red[tole_Vineyard] = colour[0];
    green[tole_Vineyard] = colour[1];
    blue[tole_Vineyard] = colour[2];

    colour = colours["DARKOLIVEGREEN3"];
    red[tole_OliveGrove] = colour[0];
    green[tole_OliveGrove] = colour[1];
    blue[tole_OliveGrove] = colour[2];

    colour = colours["YELLOWGREEN"];
    red[tole_RiceField] = colour[0];
    green[tole_RiceField] = colour[1];
    blue[tole_RiceField] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_MontadoCorkOak] = colour[0];
    green[tole_MontadoCorkOak] = colour[1];
    blue[tole_MontadoCorkOak] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_MontadoHolmOak] = colour[0];
    green[tole_MontadoHolmOak] = colour[1];
    blue[tole_MontadoHolmOak] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_MontadoMixed] = colour[0];
    green[tole_MontadoMixed] = colour[1];
    blue[tole_MontadoMixed] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_AgroForestrySystem] = colour[0];
    green[tole_AgroForestrySystem] = colour[1];
    blue[tole_AgroForestrySystem] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_CorkOakForest] = colour[0];
    green[tole_CorkOakForest] = colour[1];
    blue[tole_CorkOakForest] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_HolmOakForest] = colour[0];
    green[tole_HolmOakForest] = colour[1];
    blue[tole_HolmOakForest] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_OtherOakForest] = colour[0];
    green[tole_OtherOakForest] = colour[1];
    blue[tole_OtherOakForest] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_ChestnutForest] = colour[0];
    green[tole_ChestnutForest] = colour[1];
    blue[tole_ChestnutForest] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_EucalyptusForest] = colour[0];
    green[tole_EucalyptusForest] = colour[1];
    blue[tole_EucalyptusForest] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_MaritimePineForest] = colour[0];
    green[tole_MaritimePineForest] = colour[1];
    blue[tole_MaritimePineForest] = colour[2];

    colour = colours["SPRINGGREEN4"];
    red[tole_StonePineForest] = colour[0];
    green[tole_StonePineForest] = colour[1];
    blue[tole_StonePineForest] = colour[2];

    colour = colours["PALEGREEN4"];
    red[tole_InvasiveForest] = colour[0];
    green[tole_InvasiveForest] = colour[1];
    blue[tole_InvasiveForest] = colour[2];

    colour = colours["WHEAT2"];
    red[tole_Portarea] = colour[0];
    green[tole_Portarea] = colour[1];
    blue[tole_Portarea] = colour[2];

    colour = colours["SNOW3"];
    red[tole_Airport] = colour[0];
    green[tole_Airport] = colour[1];
    blue[tole_Airport] = colour[2];

    colour = colours["WHITESMOKE"];
    red[tole_Saltpans] = colour[0];
    green[tole_Saltpans] = colour[1];
    blue[tole_Saltpans] = colour[2];

    colour = colours["SEAGREEN4"];
    red[tole_SwampForest] = colour[0];
    green[tole_SwampForest] = colour[1];
    blue[tole_SwampForest] = colour[2];

    colour = colours["SLATEGRAY"];
    red[tole_Pipeline] = colour[0];
    green[tole_Pipeline] = colour[1];
    blue[tole_Pipeline] = colour[2];

    colour = colours["DARKSLATEGRAY4"];
    red[tole_SolarPanel] = colour[0];
    green[tole_SolarPanel] = colour[1];
    blue[tole_SolarPanel] = colour[2];

    colour = colours["PALEGREEN"];
    red[tole_ForestAisle] = colour[0];
    green[tole_ForestAisle] = colour[1];
    blue[tole_ForestAisle] = colour[2];
    
    //
    
    colour = colours["PURPLE"];
    red[tole_OOrchard] = colour[0];
    green[tole_OOrchard] = colour[1];
    blue[tole_OOrchard] = colour[2];

    colour = colours["MEDIUMORCHID"];
    red[tole_BushFruit] = colour[0];
    green[tole_BushFruit] = colour[1];
    blue[tole_BushFruit] = colour[2];

    colour = colours["MEDIUMORCHID"];
    red[tole_OBushFruit] = colour[0];
    green[tole_OBushFruit] = colour[1];
    blue[tole_OBushFruit] = colour[2];

    colour = colours["FORESTGREEN"];
    red[tole_ChristmasTrees] = colour[0];
    green[tole_ChristmasTrees] = colour[1];
    blue[tole_ChristmasTrees] = colour[2];

    colour = colours["FORESTGREEN"];
    red[tole_OChristmasTrees] = colour[0];
    green[tole_OChristmasTrees] = colour[1];
    blue[tole_OChristmasTrees] = colour[2];

    colour = colours["TURQUOISE1"];
    red[tole_EnergyCrop] = colour[0];
    green[tole_EnergyCrop] = colour[1];
    blue[tole_EnergyCrop] = colour[2];

    colour = colours["TURQUOISE1"];
    red[tole_OEnergyCrop] = colour[0];
    green[tole_OEnergyCrop] = colour[1];
    blue[tole_OEnergyCrop] = colour[2];

    colour = colours["GREEN2"];
    red[tole_FarmForest] = colour[0];
    green[tole_FarmForest] = colour[1];
    blue[tole_FarmForest] = colour[2];

    colour = colours["GREEN2"];
    red[tole_OFarmForest] = colour[0];
    green[tole_OFarmForest] = colour[1];
    blue[tole_OFarmForest] = colour[2];

    colour = colours["MEDIUM FOREST GREEN"];
    red[tole_PermPasturePigs] = colour[0];
    green[tole_PermPasturePigs] = colour[1];
    blue[tole_PermPasturePigs] = colour[2];

    colour = colours["MEDIUM FOREST GREEN"];
    red[tole_OPermPasturePigs] = colour[0];
    green[tole_OPermPasturePigs] = colour[1];
    blue[tole_OPermPasturePigs] = colour[2];

    colour = colours["MEDIUM FOREST GREEN"];
    red[tole_OPermPasture] = colour[0];
    green[tole_OPermPasture] = colour[1];
    blue[tole_OPermPasture] = colour[2];

    colour = colours["MEDIUM FOREST GREEN"];
    red[tole_OPermPastureLowYield] = colour[0];
    green[tole_OPermPastureLowYield] = colour[1];
    blue[tole_OPermPastureLowYield] = colour[2];

    colour = colours["MEDIUM FOREST GREEN"];
    red[tole_OPermPastureLowYield] = colour[0];
    green[tole_OPermPastureLowYield] = colour[1];
    blue[tole_OPermPastureLowYield] = colour[2];

    colour = colours["GREEN1"];
    red[tole_FarmYoungForest] = colour[0];
    green[tole_FarmYoungForest] = colour[1];
    blue[tole_FarmYoungForest] = colour[2];

    colour = colours["GREEN1"];
    red[tole_OFarmYoungForest] = colour[0];
    green[tole_OFarmYoungForest] = colour[1];
    blue[tole_OFarmYoungForest] = colour[2];

	colour = colours["PURPLE"];
	red[tole_AlmondPlantation] = colour[0];
	green[tole_AlmondPlantation] = colour[1];
	blue[tole_AlmondPlantation] = colour[2];

	colour = colours["PURPLE"];
	red[tole_WalnutPlantation] = colour[0];
	green[tole_WalnutPlantation] = colour[1];
	blue[tole_WalnutPlantation] = colour[2];

	colour = colours["MEDIUM FOREST GREEN"];
	red[tole_FarmBufferZone] = colour[0];
	green[tole_FarmBufferZone] = colour[1];
	blue[tole_FarmBufferZone] = colour[2];

	colour = colours["MEDIUM FOREST GREEN"];
	red[tole_NaturalFarmGrass] = colour[0];
	green[tole_NaturalFarmGrass] = colour[1];
	blue[tole_NaturalFarmGrass] = colour[2];

	colour = colours["MEDIUM FOREST GREEN"];
	red[tole_GreenFallow] = colour[0];
	green[tole_GreenFallow] = colour[1];
	blue[tole_GreenFallow] = colour[2];

	colour = colours["MEDIUM FOREST GREEN"];
	red[tole_FarmFeedingGround] = colour[0];
	green[tole_FarmFeedingGround] = colour[1];
	blue[tole_FarmFeedingGround] = colour[2];

}

HabitatMap::HabitatMap(const unsigned SIZE)
{
    idata = new unsigned char[SIZE*SIZE*3];
    __MAPSIZE1=SIZE;
}

HabitatMap::HabitatMap()
{
    unsigned SIZE=948;
    idata = new unsigned char[SIZE*SIZE*3];
    __MAPSIZE1=SIZE;
    m_scalingW = 0;
    m_scalingH = 0;
    m_Zoom = 1;
    currentView=0;
}

HabitatMap::~HabitatMap()
{
    delete(idata);
}

void HabitatMap::DrawLandscape()
{
    float x, y;
    // show pesticide load ?
    
    int yy;
    int k = 0;
    y = m_TLy;

    RodenticidePredators_Population_Manager* RPM = NULL;
    if (cfg_rodenticide_enable.value())
        RPM = m_ALandscape->SupplyRodenticidePredatoryManager();

    // for code in the for loops below is especially time sensitive

    if (currentView == 4) // show pesticides
    {
        //PlantProtectionProducts ppp = PlantProtectionProducts(m_MainForm->m_subselectionspin->GetValue()-1);
        PlantProtectionProducts ppp = PlantProtectionProducts(0);
        for (unsigned j = 0; j < __MAPSIZE1; j++)
        {   
            yy = int(y); 
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++)
            {
                if (l_pest_enable_pesticide_engine.value())
                {
                    int col = (int)(m_ALandscape->SupplyPesticide(int(x), yy, ppp) * 250);
                    if (col > 255) col = 255;
                    idata[k++] = col;
                    idata[k++] = 0;
                    idata[k++] = 0;
                }
                else if (cfg_rodenticide_enable.value()) // For rodenticide
                {
                    int col = (int)(m_ALandscape->SupplyRodenticide(int(x), yy) * 250);
                    if (col > 255) col = 255;
                    idata[k++] = col;
                    idata[k++] = 0;
                    idata[k++] = col;
                    // Draw predator territories
                    if (idata[k-3] < 10)
                    {
                        col = RPM->HowManyTerritoriesHere(int(x), yy);
                        idata[k-3] += col;
                        idata[k-2] += 64 * col;
                        idata[k-1] += 24 * col;
                    }
                }
                else
                {
                    idata[k++] = 0;
                    idata[k++] = 0;
                    idata[k++] = 0;
                }
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else  if (currentView == 2) { // Show Biomass
        int col;
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = int(y);
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
                col = (int)m_ALandscape->SupplyVegBiomass(int(x), yy);
                if (col > 255) col = 255;
                idata[k++] = 64;
                idata[k++] = col;
                idata[k++] = 64;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else if (currentView == 3) { // Show farm ownership
        int owner;
        int red, green, blue;
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = int(y);
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
                owner = m_ALandscape->SupplyFarmOwnerIndex(int(x), yy);
                if (owner != -1) {
                    red = ((owner * 17) % 255);
                    green = (red + (owner * 19) % 255);
                    blue = (green + (owner * 29) % 255);
                    idata[k++] = red;
                    idata[k++] = green;
                    idata[k++] = blue;
                }
                else {
                    idata[k++] = 0;
                    idata[k++] = 0;
                    idata[k++] = 0;
                }
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else if (currentView == 1) { // tov show
        int red, green, blue;
        unsigned ref;
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = int(y);
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
                ref = m_ALandscape->SupplyVegType(int(x), yy);
                red = ((ref * 17) % 255);
                green = (red + (ref * 19) % 255);
                blue = (green + (ref * 29) % 255);
                idata[k++] = red;
                idata[k++] = green;
                idata[k++] = blue;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else if (currentView == 5)  // Goose Numbers
    {
        int col;
        for (unsigned j = 0; j < __MAPSIZE1; j++)
        {
            yy = int(y);
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++)
            {
                col = m_ALandscape->GetGooseNumbers(int(x), yy);
                if (col > 255) col = 255;
                idata[k++] = 32;
                idata[k++] = 32;
                idata[k++] = col;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else if (currentView == 6)  // Goose Food Density
    {
        double food;
        for (unsigned j = 0; j < __MAPSIZE1; j++)
        {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++)
            {
                food = m_ALandscape->SupplyBirdSeedForage(int(x), yy) * 17.7
                        + m_ALandscape->GetActualGooseGrazingForage(int(x), yy, gs_Pinkfoot);
                if (food > 255) food = 255;
                idata[k++] = food;
                idata[k++] = food;
                idata[k++] = food;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else if (currentView == 7)  // Goose GrainFood Density
    {
        double food;
        for (unsigned j = 0; j < __MAPSIZE1; j++)
        {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++)
            {
                food = m_ALandscape->SupplyBirdSeedForage(int(x), yy) * 2;
                if (food > 255) food = 255;
                idata[k++] = 32;
                idata[k++] = food;
                idata[k++] = food;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else if (currentView == 8)  // Goose Grazing
    {
        double food;
        for (unsigned j = 0; j < __MAPSIZE1; j++)
        {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++)
            {
                food = m_ALandscape->GetActualGooseGrazingForage(int(x), yy, gs_Pinkfoot) * 4;
                if (food > 255) food = 255;
                idata[k++] = 64;
                idata[k++] = food;
                idata[k++] = 32;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else if (currentView == 9)  // Soils
    {
        double soil;
        for (unsigned j = 0; j < __MAPSIZE1; j++)
        {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++)
            {
                soil = m_ALandscape->SupplySoilTypeR(int(x), yy) * 16;
                if (soil > 255) soil = 255;
                idata[k++] = 64;
                idata[k++] = soil;
                idata[k++] = soil;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else if (currentView == 10)  // Hunter mapping
    {
        unsigned ref;
        // First we draw the standard landscape
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
                // Note that because of the way the data is stored, we have to reverse
                // the x,y co-ords
                ref = m_ALandscape->SupplyElementType(int(x), yy);
                if (ref == tole_Building)  // 24 is tole_Building
                {
                    if (m_ALandscape->SupplyCountryDesig(int(x), yy) == 1) // Country residence
                    {
                        idata[k++] = 255;
                        idata[k++] = 0;
                        idata[k++] = 0;
                    }
                    else
                    {
                        idata[k++] = 0;
                        idata[k++] = 0;
                        idata[k++] = 0;
                    }
                }
                else
                {
                    if (ref > 400) ref = 399; // The max number of colours
                    idata[k++] = colours.red[ref];
                    idata[k++] = colours.green[ref];
                    idata[k++] = colours.blue[ref];
                }
                x += m_scalingW;
            }
            y += m_scalingH;
        }
        // Next we add the hunter dots

 /*       unsigned noInList = m_MainForm->m_Hunter_Population_Manager->SupplyListSize(0);
        for (unsigned aHunter = 0; aHunter < noInList; aHunter++)
        {
            APoint pt = m_MainForm->m_Hunter_Population_Manager->GetHunterHome(aHunter, 0);
            if (pt.m_x < 0 || pt.m_y < 0 || pt.m_x >= m_ALandscape->SupplySimAreaWidth() || pt.m_y >= m_ALandscape->SupplySimAreaHeight())
            {
                pt.m_x = 0;
                pt.m_y = 0;
            }
            Spot(0, pt.m_x, pt.m_y);
            // Here we only show the first hunter hunting location
            pt = m_MainForm->m_Hunter_Population_Manager->GetHunterHuntLoc(aHunter, 0,0);
            Spot(1, pt.m_x, pt.m_y);
        }
        */
    }
    else  if (currentView == 11) { // Pollen 
        int Pollen;
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
				Pollen = (int)(m_ALandscape->SupplyPollen(int(x), yy).m_quality * 1000);
                if (Pollen > 255) Pollen = 255;
                idata[k++] = 0;
                idata[k++] = Pollen;
                idata[k++] = Pollen;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else  if (currentView == 12) { // Nectar
        int Nectar;
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
				Nectar = (int)(m_ALandscape->SupplyNectar(int(x), yy).m_quality * 1000);
                if (Nectar > 255) Nectar = 255;
                idata[k++] = Nectar;
                idata[k++] = Nectar;
                idata[k++] = 0;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    else {    // default
        unsigned ref;
        int col;
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
                // Note that because of the way the data is stored, we have to reverse
                // the x,y co-ords
                ref = m_ALandscape->SupplyElementType(int(x), yy);
                if (ref == tole_Building)  // 24 is tole_Building
                {
                    if (m_ALandscape->SupplyCountryDesig(int(x), yy) == 1) // Country residence
                    {
                        idata[k++] = 255;
                        idata[k++] = 0;
                        idata[k++] = 0;
                    }
                    else
                    {
                        idata[k++] = 0;
                        idata[k++] = 0;
                        idata[k++] = 0;
                    }
                }
                else
                {
                    if (ref > 400) ref = 399; // The max number of colours
                    idata[k++] = colours.red[ref];
                    idata[k++] = colours.green[ref];
                    idata[k++] = colours.blue[ref];
                }
                if (cfg_rodenticide_enable.value()) // For rodenticide
                {
                    // Draw predator territories
                    col = RPM->HowManyTerritoriesHere(int(x), yy);
                    idata[k++] += 64 * col;
                    idata[k++] += col;
                    idata[k++] += col;
                }
                x += m_scalingW;
            }
            y += m_scalingH;
        }
    }
    #ifdef __FLOWERTESTING
    if(m_ALandscape->SupplyDayInMonth()==15){
        int month_in_num = m_ALandscape->SupplyMonth();
        QVariant var(month_in_num);

        int Pollen;
        k = 0;
        y = m_TLy;
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
				Pollen = (int)(m_ALandscape->SupplyPollen(int(x), yy).m_quality * 1000);
                if (Pollen > 255) Pollen = 255;
                idata[k++] = 0;
                idata[k++] = Pollen;
                idata[k++] = Pollen;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
        QImage img(idata, __MAPSIZE1, __MAPSIZE1, QImage::Format_RGB888);
        img.save("pollen"+var.toString()+".png", "png", 100);

        int Nectar;
        k = 0;
        y = m_TLy;
        for (unsigned j = 0; j < __MAPSIZE1; j++) {
            yy = (int)y;
            x = m_TLx;
            for (unsigned i = 0; i < __MAPSIZE1; i++) {
				Nectar = (int)(m_ALandscape->SupplyNectar(int(x), yy).m_quality * 1000);
                if (Nectar > 255) Nectar = 255;
                idata[k++] = Nectar;
                idata[k++] = Nectar;
                idata[k++] = 0;
                x += m_scalingW;
            }
            y += m_scalingH;
        }
        QImage img1(idata, __MAPSIZE1, __MAPSIZE1, QImage::Format_RGB888);
        img1.save("nectar"+var.toString()+".png", "png", 100);
    }
    #endif
}

void HabitatMap::ChangeZoom( int X, int Y ) {
  // Now place the TL corner
  m_TLx = X - ( m_Width / 2 );
  if ( m_TLx > m_SimW - m_Width ) m_TLx = m_SimW - m_Width;
  if ( m_TLx < 0 ) m_TLx = 0;
  m_TLy = Y - ( m_Height / 2 );
  if ( m_TLy > m_SimH - m_Height ) m_TLy = m_SimH - m_Height;
  if ( m_TLy < 0 ) m_TLy = 0;
  // Place the BL corner
  m_BRx = m_TLx + m_Width;
  m_BRy = m_TLy + m_Height;
  SetScale( m_Width, m_Height );
  //  DrawLandscape();
  //  m_AManager->DisplayLocations();
  //  if (RadioButton1->Checked)  {
  //    PredatorManager->DisplayLocations();
  //  }

}

void HabitatMap::SetSimExtent( int Width, int Height ) {
  // This function must be called before the form can be used
  m_SimW = Width;
  m_SimH = Height;
  m_TLx = 0;
  m_TLy = 0;
  m_BRx = m_SimW;
  m_BRy = m_SimH;
  m_Width = m_SimW;
  m_Height = m_SimH;
  SetScale( m_SimW, m_SimH );
}

void HabitatMap::SetScale( int Width, int Height ) {
  // This function must be called before the form can be used
  // It determines the multiplier for the display i.e how many pixels map to
  // one unit of the track area
  m_scalingW = Width / ( float )__MAPSIZE1;
  m_scalingH = Height / ( float )__MAPSIZE1;
}

void HabitatMap::SetUpMapImage() {
  //wxIm->LoadFile( _T( __TITLEBMP ), wxBITMAP_TYPE_BMP );
  //idata = wxIm->GetData();
}

void HabitatMap::Spot( int cref, int x, int y ) {
  if ( ( x >= m_TLx ) && ( x < m_BRx ) && ( y >= m_TLy ) && ( y < m_BRy ) ) {
    int i = (int) (( x - m_TLx ) / m_scalingW);
    int j = (int) (( y - m_TLy ) / m_scalingH);
    int row3 = j * __MAPSIZE1;
    int row2 = row3 - __MAPSIZE1;
    int row1 = row2 - __MAPSIZE1;
    int row4 = row3 + __MAPSIZE1;
    int row5 = row4 + __MAPSIZE1;

    if (row1 < 1 ) return;
    if (row2 < 0 ) return;
    if (row5 > int(__MAPSIZE1 * (__MAPSIZE1-1) )) return;
    if (row4 > int(__MAPSIZE1 * __MAPSIZE1 )) return;

    switch ( cref ) {
      case 0:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) + 1 ]        = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 255;

            idata[   3 * (( row1 + i +1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 255;

            //idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 255;

            //idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) + 1 ]        = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i -1) ) + 1 ]        = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i -2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row3 + i +0) ) + 1 ]        = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i +1) ) + 1 ]        = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i +2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i -1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i -2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row4 + i +0) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 245;

            idata[   3 * (( row4 + i +1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 250;

            idata[   3 * (( row4 + i +2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row4 + i -1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 245;

            idata[   3 * (( row4 + i -2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 240;


            idata[   3 * (( row5 + i +0) ) + 1 ]        = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 220;

            idata[   3 * (( row5 + i +1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 240;

            //idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) & (__MAPSIZE  *  __MAPSIZE) ) + 1 ]        = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 200;

            //idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 1:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 255;

            idata[   3 * (( row1 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 255;

            //idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 255;

            //idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i -2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row3 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i -2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row4 + i +0) ) + 1 ]        = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 245;

            idata[   3 * (( row4 + i +1) ) + 1 ]        = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row4 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row4 + i -1) ) + 1 ]        = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row4 + i -2) ) + 1 ]        = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row5 + i +0) ) + 1 ]        = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i +1) ) + 1 ]        = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 255;

            //idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) ) + 1 ]        = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 255;

            //idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 2:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 0;

            idata[   3 * (( row1 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 0;

            //idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 0;

            //idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

            idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row2 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 64;

            idata[   3 * (( row2 + i -2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row3 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 32;

            idata[   3 * (( row3 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row3 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 0;

            idata[   3 * (( row3 + i -2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row4 + i +0) ) + 1 ]        = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i +1) ) + 1 ]        = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i -1) ) + 1 ]        = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i -2) ) + 1 ]        = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row5 + i +0) ) + 1 ]        = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 0;

            idata[   3 * (( row5 + i +1) ) + 1 ]        = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 0;

            //idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) ) + 1 ]        = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 0;

            //idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 3:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) + 2 ]        = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 0;

            idata[   3 * (( row1 + i +1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 0;

            //idata[   3 * (( row1 + i +2) ) + 2 ]        = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;

            idata[   3 * (( row1 + i -1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 0;

            //idata[   3 * (( row1 + i -2) ) + 2 ]        = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;

            idata[   3 * (( row2 + i +0) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;

            idata[   3 * (( row2 + i +1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;

            idata[   3 * (( row2 + i +2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 0;

            idata[   3 * (( row2 + i -1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 64;

            idata[   3 * (( row2 + i -2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 0;


            idata[   3 * (( row3 + i +0) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 32;

            idata[   3 * (( row3 + i +1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 128;

            idata[   3 * (( row3 + i +2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 0;

            idata[   3 * (( row3 + i -1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 0;

            idata[   3 * (( row3 + i -2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 0;


            idata[   3 * (( row4 + i +0) ) + 2 ]        = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 0;

            idata[   3 * (( row4 + i +1) ) + 2 ]        = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 0;

            idata[   3 * (( row4 + i +2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 0;

            idata[   3 * (( row4 + i -1) ) + 2 ]        = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 0;

            idata[   3 * (( row4 + i -2) ) + 2 ]        = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 0;


            idata[   3 * (( row5 + i +0) ) + 2 ]        = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 0;

            idata[   3 * (( row5 + i +1) ) + 2 ]        = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 0;

            //idata[   3 * (( row5 + i +2) ) + 2 ]        = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;

            idata[   3 * (( row5 + i -1) ) + 2 ]        = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 0;

            //idata[   3 * (( row5 + i -2) ) + 2 ]        = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
            break;

      case 4:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 0;

            idata[   3 * (( row1 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 0;

            //idata[   3 * (( row1 + i +2) ) ]       = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 0;

            //idata[   3 * (( row1 + i -2) ) ]       = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

            idata[   3 * (( row2 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row2 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 64;

            idata[   3 * (( row2 + i -2) ) ]       = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row3 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 32;

            idata[   3 * (( row3 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row3 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 0;

            idata[   3 * (( row3 + i -2) ) ]       = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row4 + i +0) ) ]       = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i +1) ) ]       = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i -1) ) ]       = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i -2) ) ]       = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row5 + i +0) ) ]       = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 0;

            idata[   3 * (( row5 + i +1) ) ]       = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 0;

            //idata[   3 * (( row5 + i +2) ) ]       = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) ) ]       = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 0;

            //idata[   3 * (( row5 + i -2) ) ]       = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
        break;
      case 5:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 172;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 128;

            idata[   3 * (( row1 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 128;

            //idata[   3 * (( row1 + i +2) ) ]       = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 128;

            //idata[   3 * (( row1 + i -2) ) ]       = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

            idata[   3 * (( row2 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 128;

            idata[   3 * (( row2 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 128;

            idata[   3 * (( row2 + i -2) ) ]       = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 128;


            idata[   3 * (( row3 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i -2) ) ]       = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 128;


            idata[   3 * (( row4 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 128;

            idata[   3 * (( row4 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 128;

            idata[   3 * (( row4 + i +2) ) ]       = 245;
            idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 128;

            idata[   3 * (( row4 + i -1) ) ]       = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 128;

            idata[   3 * (( row4 + i -2) ) ]       = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 128;


            idata[   3 * (( row5 + i +0) ) ]       = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 128;

            idata[   3 * (( row5 + i +1) ) ]       = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 128;

            //idata[   3 * (( row5 + i +2) ) ]       = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) ) ]       = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 128;

            //idata[   3 * (( row5 + i -2) ) ]       = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
        break;
      default:
        idata[ 3 * ( row1 + i ) ] = 200;
        idata[ ( 3 * ( row1 + i ) ) + 1 ] = 200;
        idata[ ( 3 * ( row1 + i ) ) + 2 ] = 200;
    }
  }
}

void HabitatMap::ZoomIn( int X, int Y ) {
  // First back transform X & Y by the scaling factors
  X = (int) (m_TLx + X * m_scalingW);
  Y = (int) (m_TLy + Y * m_scalingH);
  // Zoom in
  int NewZoom = m_Zoom + m_Zoom;
  m_Width = m_SimW / NewZoom;
  m_Height = m_SimH / NewZoom;
  if (m_Width < 100) {
     m_Width = 100; // Don't zoom in smaller than 100m
  } else
     m_Zoom = NewZoom;
  ChangeZoom( X, Y );
}

void HabitatMap::ZoomOut(int X, int Y) {
    // First back transform X & Y by the scaling factors
    X = (int)(m_TLx + X * m_scalingW);
    Y = (int)(m_TLy + Y * m_scalingH);
    // Zoom out
    m_Zoom /= 2;
    if (m_Zoom < 1) m_Zoom = 1; // don't go bigger than SimW
    m_Width = m_SimW / m_Zoom;
    m_Height = m_SimH / m_Zoom;
    ChangeZoom(X, Y);
}

QString HabitatMap::getMapText(const int x, const int y) {
    QString mytext;
    int xx, yy;
    xx = (int) (m_TLx + x * m_scalingW);
    yy = (int) (m_TLy + y * m_scalingH);
    int apolyref = m_ALandscape->SupplyPolyRef(xx, yy);
    int aUMref = m_ALandscape->SupplyUMRef(xx, yy);
    int a_ownertole = m_ALandscape->SupplyOwner_tole(xx, yy);

    mytext += "USED FOR DEBUGGING\n";
    mytext += "YOU CAN GET WHATEVER YOU\n";
    mytext += "WANT IN TERMS OF INFORMATION\n";
    mytext += "PRESENTED HERE\n";
    mytext += "x: " + QString::fromStdString(std::to_string(xx)) + "\n";
    mytext += "y: " + QString::fromStdString(std::to_string(yy)) + "\n";
    mytext += "Polygon Ref: : " + QString::fromStdString(std::to_string(apolyref)) + "\n";
    mytext += "UM Ref: " + QString::fromStdString(std::to_string(aUMref)) + "\n";
    mytext += "Owner_tole: " + QString::fromStdString(std::to_string(a_ownertole)) + "\n";
    std::string astr = m_ALandscape->PolytypeToString( m_ALandscape->SupplyElementType( apolyref ));
    mytext += "Element Type: " + QString::fromStdString(astr) + "\n";
    double polyarea = m_ALandscape->SupplyPolygonArea(apolyref)/10000.0;
    mytext += "Polygon Area (ha): " + QString::fromStdString(std::to_string(polyarea)) + "\n";
    astr = m_ALandscape->VegtypeToString( m_ALandscape->SupplyVegType( apolyref )).c_str();
    mytext += "Veg Type: " + QString::fromStdString(astr) + "\n";
    mytext += "Veg. Total Biomass: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegBiomass(apolyref))) + "\n";
    mytext += "LAI Green: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyLAGreen(apolyref))) + "\n";
    mytext += "Veg. Dead Biomass: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegBiomass(apolyref)-m_ALandscape->SupplyGreenBiomass(apolyref))) + "\n";
    mytext += "Veg Height: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegHeight(apolyref))) + "\n";
    mytext += "Veg. Digestibility: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegDigestibility(apolyref))) + "\n";
    mytext += "Veg Density: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegDensity(apolyref))) + "\n";
    mytext += "Veg Cover: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegCover(apolyref))) + "\n";
    mytext += "Weed Biomass: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyWeedBiomass(apolyref))) + "\n";
    mytext += "Insect Biomass: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyInsects(apolyref))) + "\n";
    mytext += "Is Grazed: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyGrazingPressure(apolyref))) + "\n";
    mytext += "Is Patchy: " ;
    if ( m_ALandscape->SupplyVegPatchy( apolyref ) )
        mytext += "True";
    else
        mytext += "False";
    mytext += "Centroid x: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyCentroidX(apolyref))) + "\n";
    mytext += "Centroid y: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyCentroidY(apolyref))) + "\n";
    mytext += "Veg Phase: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegPhase(apolyref))) + "\n";
    //mytext += "Last Treatment: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyLastTreatment(apolyref,0))) + "\n";
    mytext += "Latest Sown Crop: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyLastSownVeg(apolyref))) + "\n";
    mytext += "Valid x: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyValidX(apolyref))) + "\n";
    mytext += "Valid y: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyValidY(apolyref))) + "\n";
    int fref = m_ALandscape->SupplyFarmOwner( apolyref );
    mytext += "Farm Owner Ref: " + QString::fromStdString(std::to_string(fref)) + "\n"; // lct: this might be renumbered, cannot be trusted to matched with the input file
    if (fref!=-1)
    {
        mytext += "Farm Area Total (ha): " + QString::fromStdString(std::to_string(m_ALandscape->SupplyFarmArea(apolyref)/10000.0)) + "\n";
        mytext += "Farm Type: " + QString::fromStdString(m_ALandscape->FarmtypeToString(m_ALandscape->SupplyFarmType(apolyref))) + "\n";
        mytext += "Farm Rotation: " + QString::fromStdString(m_ALandscape->SupplyFarmRotFilename(apolyref)) + "\n";
    }
    else
        mytext += "Not belonging to a Farm Owner\n";

 
    mytext += "Pollen g/m2: " + QString::fromStdString(std::to_string(round((m_ALandscape->SupplyPollen(apolyref).m_quantity *10000))/10000.0)) + "\n";
    mytext += "Total Pollen : " + QString::fromStdString(std::to_string(round((m_ALandscape->SupplyTotalPollen(apolyref)*10000))/10000.0)) + "\n";
    mytext += "Nectar g/m2: " + QString::fromStdString(std::to_string(round((m_ALandscape->SupplyNectar(apolyref).m_quantity *10000))/10000.0)) + "\n";
    mytext += "Total Nectar : " + QString::fromStdString(std::to_string(round((m_ALandscape->SupplyTotalNectar(apolyref)*10000))/10000.0)) + "\n";

    mytext += "NEXT LINE ONLY WORKS WITH\n";
    mytext += "THE PESTICIDE ENGINE\n";
    if (l_pest_enable_pesticide_engine.value())
    {
        PlantProtectionProducts ppp;
        if (mainWindow->getCurrentSpecies() == 4)
            ppp = PlantProtectionProducts(mainWindow->getSubValue()-1);
        else
            ppp = ppp_1;

        mytext += "Pesticide Load: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyPesticide(xx,yy,ppp))) + "\n";
    }
    else
    {
        mytext += "Pesticide Load: Temporarily Unavailable";
    }

    mytext += "Currenty Day Degrees: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyDayDegrees(apolyref))) + "\n";
    mytext += "Openness: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyOpenness(apolyref))) + "\n";
    double goosefood = m_ALandscape->SupplyBirdSeedForage(apolyref);
    mytext += "Grain food density: " + QString::fromStdString(std::to_string(goosefood)) + "\n";

    mytext += "Grazing food density in Kj m2: " + QString::fromStdString(std::to_string(m_ALandscape->GetActualGooseGrazingForage(apolyref,gs_Pinkfoot))) + "\n";
    mytext += "Maximum goose nnumbers: " + QString::fromStdString(std::to_string(m_ALandscape->GetGooseNumbers(apolyref))) + "\n";
    int goosenums = m_ALandscape->GetGooseNumbers(apolyref);
    mytext += "Max. goose density in geese/ha: " + QString::fromStdString(std::to_string((double)goosenums / polyarea)) + "\n";

    mytext += "Hare Forage: " + QString::fromStdString(std::to_string(m_ALandscape->GetHareFoodQuality(apolyref))) + "\n";

    mytext += "\n";
    mytext += "\n";
    return mytext;
}

QString HabitatMap::getAnimalText(const int x, const int y) {
    QString mytext;
    int X = (int)(m_TLx + x * m_scalingW);
    int Y = (int)(m_TLy + y * m_scalingH);

    TAnimal* animal = NULL;
    TAnimal* animal_closest = NULL;
    double dist, dist2 = 99999999999999999999.0;

    for (int i=1; i <= 6; ++i) {
        if (mainWindow->isAnimalChecked(i)) {
            animal = NULL;
            animal = mainWindow->getPopManager()->FindClosest(X, Y, i-1);
            if (animal) {
                dist = sqrt(((animal->Supply_m_Location_x() - X)* (animal->Supply_m_Location_x() - X)) + ((animal->Supply_m_Location_y() - Y)* (animal->Supply_m_Location_y() - Y)));
                if (dist < dist2)
                {
                    dist2 = dist;
                    animal_closest = animal;
                }
            }
        }
    }
    if (animal_closest != NULL) {
        mytext += "ANIMAL INFORMATION AVAILABLE FROM THE TAnimal*\n";
        mytext += "x: " + QString::fromStdString(std::to_string(animal_closest->Supply_m_Location_x())) + "\n";
        mytext += "y: " + QString::fromStdString(std::to_string(animal_closest->Supply_m_Location_y())) + "\n";
        mytext += "Polygon ref: " + QString::fromStdString(std::to_string(animal_closest->SupplyPolygonRef())) + "\n";
        mytext += "Current State No: " + QString::fromStdString(std::to_string(animal_closest->GetCurrentStateNo())) + "\n";
        mytext += "WhatState returns: " + QString::fromStdString(std::to_string(animal_closest->WhatState())) + "\n";
    }
    return mytext;
}


void HabitatMap::ConvertForZoom(int &d_x, int &d_y, int x, int y)
{
    if ( ( x >= m_TLx ) && ( x < m_BRx ) && ( y >= m_TLy ) && ( y < m_BRy ) ) 
    {
        d_x = (int) (( x - m_TLx ) / m_scalingW);
        d_y = (int) (( y - m_TLy ) / m_scalingH);
    }
}
