#ifndef HONEYCOMBCELL_H
#define HONEYCOMBCELL_H

#include <QPainter>
#include <QGraphicsItem>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>

class HoneyCombCell : public QGraphicsItem
{
public:
    HoneyCombCell(const int x, const int y, const int size, QColor fillColour, QString label);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    bool Pressed;
protected:
    int xval,yval,sizeval;
    QString clabel;
    QColor myFillColour;
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
};

#endif // HONEYCOMBCELL_H
