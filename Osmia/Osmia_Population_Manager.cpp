/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Osmia_Population_Manager.cpp
Version of  September 2019 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include<vector>
#include <chrono>
#pragma warning( push )
#pragma warning( disable : 4100)
#pragma warning( disable : 4127)
#pragma warning( disable : 4244)
#pragma warning( disable : 4267)
#include <blitz/array.h>
#pragma warning( pop ) 
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Osmia/Osmia.h"
#include "../Osmia/Osmia_Population_Manager.h"

//---------------------------------------------------------------------------------------
/** \brief Monthly pollen thresholds, 12 quantity then 12 quality. Units mg/m2  and unitless */
static CfgArray_Double cfg_OsmiaPollenThresholds("OSMIA_POLLEN_THRESHOLDS", CFG_CUSTOM, 24, vector<double> {
	1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
		1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
});

/** \brief Monthly nectar thresholds, 12 quantity then 12 quality. Units mj/m2  and mg/l */
static CfgArray_Double cfg_OsmiaNectarThresholds("OSMIA_NECTAR_THRESHOLDS", CFG_CUSTOM, 24, vector<double> {
	1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
		1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
});


/** \brief Montly mortalities for TTypeOfOsmiaParasitoids parasitoids. Each set of 12 are the mortalities matching the order in the enum class TTypeOfOsmiaParasitoids */
static CfgArray_Double cfg_OsmiaParasDailyMort("OSMIA_PARAS_DAILYMORT", CFG_CUSTOM, 24, vector<double> {
	1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
		1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
});
/** \brief This array is the daily rate of development of prepupa with temperature rounded to the nearest degree (0-41 degrees*/
static CfgArray_Double cfg_OsmiaPrepupalDevelRates("OSMIA_PREPUPALDEVELRATES", CFG_CUSTOM, 42, vector<double> {
	0.118180491, 0.128062924, 0.139167698, 0.151690375, 0.165863251, 0.181962547, 0.200316654, 0.221315209, 
		0.245418359, 0.273164807, 0.305175879, 0.342150483, 0.384842052, 0.434002716, 0.490272059, 0.553979475, 0.62482638,
		0.701432201, 0.780791977, 0.857828943, 0.925409524, 0.97526899, 1, 0.995492173, 0.96251684, 0.90641791, 0.835121012, 
		0.756712977, 0.677752358, 0.602659522, 0.53389011, 0.472441557, 0.418380352, 0.371255655, 0.330377543, 0.294984821,
		0.264336547, 0.237755941, 0.214646732, 0.194494708,	0.176862031, 0.161378614
});
/** \brief The starting number of Osmia InCocoons */
static CfgInt cfg_OsmiaStartNo("OSMIA_STARTNOS", CFG_CUSTOM, 50000);
/** \brief Dispersal rates for TTypeOfOsmiaParasitoids parasitoids. Each entry is the dispersal rate for the parasitoid type matching the order in the enum class TTypeOfOsmiaParasitoids */
static CfgArray_Double cfg_OsmiaParasDispersal("OSMIA_PARAS_DISPERSAL", CFG_CUSTOM, static_cast<unsigned>(TTypeOfOsmiaParasitoids::topara_foobar) - 1, vector<double> { 0.001, 0.0001 });
/** \brief Starting numbers for TTypeOfOsmiaParasitoids parasitoids. Each entry is the max and min starting numbers per sub-population for the parasitoid type matching the order in the enum class TTypeOfOsmiaParasitoids */
static CfgArray_Double cfg_OsmiaParasStartHighLow("OSMIA_PARAS_STARTHIGHLOW", CFG_CUSTOM, 2*(static_cast<unsigned>(TTypeOfOsmiaParasitoids::topara_foobar) - 1), vector<double> { 2.0, 1.0, 2.0, 1.0});
/** \brief The size class step for Osmia female mass */
CfgFloat cfg_OsmiaAdultMassCategoryStep("OSMIA_ADULTMASSCLASSSTEP", CFG_CUSTOM, 10.0);
/** \brief Input file for Osmia nest density per LE */
static CfgStr cfg_OsmiaNestByLE_Datafile("OSMIA_NESTBYLEDATAFILE", CFG_CUSTOM, "OsmiaNestsByHabitat.txt");
/** \brief The daily background mortality rate for females */
static CfgFloat cfg_OsmiaFemaleBckMort("OSMIA_FEMALEBACKMORT", CFG_CUSTOM, 0.02);
/** \brief The mimimum number of eggs planned for a nest */
static CfgInt cfg_OsmiaMinNoEggsInNest("OSMIA_MINNOEGGSINNEST", CFG_CUSTOM, 3);
/** \brief The maximum number of eggs planned for a nest  */
static CfgInt cfg_OsmiaMaxNoEggsInNest("OSMIA_MAXNOEGGSINNEST", CFG_CUSTOM, 30);
/** \brief Array of parameters for the Osmia sex ratio vs mothers age logistic equation  */
static CfgArray_Double cfg_OsmiaSexRatioVsMotherAgeLogistic("OSMIA_SEXRATIOVSMOTHERSAGELOGISTIC", CFG_CUSTOM, 4, vector<double> { 14.90257909, 0.09141286, 0.6031729, -0.39213001 });
/** \brief Array of parameters for the Osmia female cocoon mass vs mothers age logistic equation  */
static CfgArray_Double Cfg_OsmiaFemaleCocoonMassVsMotherAgeLogistic("OSMIA_FEMALECOCOONMASSVSMOTHERSAGELOGISTIC", CFG_CUSTOM, 4, vector<double> { 18.04087868, 104.19820591, 133.74150303, -0.17686981});
/** \brief Array of parameters for the Osmia sex ratio vs mothers mass linear equation  */
static CfgArray_Double cfg_OsmiaSexRatioVsMotherMassLinear("OSMIA_SEXRATIOVSMOTHERSMASSLINEAR", CFG_CUSTOM, 2, vector<double> { 0.0055, -0.1025 });
/** \brief Array of parameters for the Osmia female first cocoon mass vs mothers mass linear equation  */
static CfgArray_Double Cfg_OsmiaFemaleCocoonMassVsMotherMassLinear("OSMIA_FEMALECOCOONMASSVSMOTHERSMASSLINEAR", CFG_CUSTOM, 2, vector<double> { 0.3, 65.1 });//{ 0.46, 63.85 });
/** \brief Assumed (fitted) value for the total difference in cocoon mass from first to last cocoon  */
static CfgFloat cfg_Osmia_LifetimeCocoonMassLoss("OSMIA_LIFETIMECOCOONMASSLOSS", CFG_CUSTOM, 30.0); // ***CJT*** TO BE CHECKED ON CALIBRATION
/** \brief The relationhip between cocoon massand provisioning mass is linear */
CfgFloat cfg_OsmiaCocoonMassFromProvMass("OSMIAS_COCOONTOPROVISIONING", CFG_CUSTOM, 1.0 / 3.247);
/** \brief The relationhip between cocoon mass and provisioning mass is linear */
CfgFloat cfg_OsmiaProvMassFromCocoonMass("OSMIAS_PROVISIONINGTOCOCOON", CFG_CUSTOM, 3.247);
/** \brief The minimum amount of pollen needed to provision a male cell */
static CfgFloat  cfg_MaleMinTargetProvisionMass("OSMIA_MALEMINTARGETPROVISIONMASS", CFG_CUSTOM, 10.0);
/** \brief The minimum time to construct a cell in days */
static CfgInt cfg_MinimumCellConstructionTime("OSMIA_MINCELLCONSTRUCTTIME", CFG_CUSTOM, 1);
/** \brief The maximum time allowed to construct a cell in days */
static CfgInt cfg_MaximumCellConstructionTime("OSMIA_MAXCELLCONSTRUCTTIME", CFG_CUSTOM, 4); //is defined as the time associated with risk of provisioning at the level of 0.5, i.e., according to (Seidelmann, 2006) it is equal to 0.5/0.022 = 22.7 h (~ 4 days)
/** \brief The maximum number of nests possible for a bee */
static CfgInt cfg_TotalNestsPossible("OSMIA_TOTALNESTSPOSSIBLE",CFG_CUSTOM, 5); // WIP find value
/** \brief The flag for using mechanistic (true) or statistical (false) parasitoid assumptions */
static CfgBool cfg_UsingMechanisticParasitoids("OSMIA_USEMECHANISTICPARASITOIDS", CFG_CUSTOM, false);
/** \brief The probability that a parasitoid is Bombylid - statistical parasitoids only */
static CfgFloat cfg_OsmiaBombylidProb("OSMIA_BOMBYLIDPROB", CFG_CUSTOM, 0.5); // WIP Find sensible value
/** \brief The conversion rate from timethe cell is open to open cell parasitism */
static CfgFloat cfg_OsmiaParasitismProbToTimeCellOpen("OSMIA_PARASITISMPROBTOTIMECELLOPEN", CFG_CUSTOM, 0.0075);
/** \brief Array of parameters for per capita parasitoid probability for each type of parasitoid in the order based on TTypeOfOsmiaParasitoids */
static CfgArray_Double cfg_OsmiaPerCapitaParasationChance("OSMIA_PERCAPITAPARASITATIONCHANCE", CFG_CUSTOM, static_cast<int>(TTypeOfOsmiaParasitoids::topara_foobar) - 1, vector<double> { 0.00001, 0.00002 }); // WIP find sensible values or replace methods
/** \brief Parameter to set the numbe of nest finding attempts */
static CfgInt cfg_OsmiaFemaleFindNestAttemptNo("OSMIA_FEMALEFINDNESTATTEMPTNO", CFG_CUSTOM, 20);
/** \brief The change in proportion pollen before a new patch is selected */
static CfgFloat cfg_OsmiaPollenGiveUpThreshold("OSMIA_POLLENGIVEUPTHRESHOLD", CFG_CUSTOM, 0.75, 0, 1.0);
/** \brief The change in proportion pollen before a new patch is selected */
static CfgFloat cfg_OsmiaPollenGiveUpReturn("OSMIA_POLLENGIVEUPRETURN", CFG_CUSTOM, 0.75, 0, 50.0);
/** \brief Parameter to set the proportion of pollen removed by other bees */
static CfgFloat cfg_OsmiaDensityDependentPollenRemovalConst("OSMIADENSITYDENPENDENTPOLLENREMOVALCONST", CFG_CUSTOM, 1); // EZ: no competition assumed as default
/** \brief The conversion rate from pollen availability score to mg pollen provisioned per day per bee  - a fitting parameter*/
static CfgFloat  cfg_PollenScoreToMg("OSMIA_POLLENSCORETOMG", CFG_CUSTOM,  0.8);
/** \brief A distribution to create an exponential decay from zero to one */
probability_distribution Osmia_Population_Manager::m_exp_ZeroTo1 = probability_distribution("BETA", "0.75,2.5");
/** \brief The probability of death per day if the pesticide body burden threshold is exceeded */
static CfgFloat cfg_OsmiaPesticideProbability("OSMIA_PPP_PROB", CFG_CUSTOM, 0.0);
/** \brief The pesticide body burden threshold for ppp effects */
static CfgFloat cfg_OsmiaPesticideThreshold("OSMIA_PPP_THRESHOLD", CFG_CUSTOM, 10000.0);

extern CfgFloat cfg_OsmiaInCocoonOverwinteringTempThreshold;
extern CfgFloat cfg_OsmiaInCocoonEmergenceTempThreshold;
extern CfgFloat cfg_OsmiaFemaleMassMin;
extern CfgFloat cfg_OsmiaFemaleMassMax;
extern CfgInt	cfg_OsmiaTypicalHomingDistance;
extern CfgInt cfg_OsmiaMaxHomingDistance;
extern Landscape* g_landscape_p;
extern CfgInt cfg_OsmiaForageSteps;
extern CfgInt cfg_OsmiaDetailedMaskStep;

// Assign default static member values (these will be changed later).
double Osmia_Base::m_DailyDevelopmentMortEggs = 0;
double Osmia_Base::m_DailyDevelopmentMortLarvae = 0;
double Osmia_Base::m_DailyDevelopmentMortPrepupae = 0;
double Osmia_Base::m_DailyDevelopmentMortPupae = 0;
double Osmia_Base::m_OsmiaEggDevelTotalDD = 0;
double Osmia_Base::m_OsmiaEggDevelThreshold = 0;
double Osmia_Base::m_OsmiaLarvaDevelTotalDD = 0;
double Osmia_Base::m_OsmiaLarvaDevelThreshold = 0;
double Osmia_Base::m_OsmiaPupaDevelTotalDD = 0;
double Osmia_Base::m_OsmiaPupaDevelThreshold = 0;
double Osmia_Base::m_OsmiaPrepupalDevelTotalDays = 0;
double Osmia_Base::m_OsmiaPrepupalDevelTotalDays10pct = 0;
double Osmia_Base::m_OsmiaInCocoonOverwinteringTempThreshold = 0;
double Osmia_Base::m_OsmiaInCocoonEmergenceTempThreshold = 0;
double Osmia_Base::m_OsmiaInCocoonPrewinteringTempThreshold = 0;
double Osmia_Base::m_OsmiaInCocoonWinterMortConst = 0.0;
double Osmia_Base::m_OsmiaInCocoonWinterMortSlope = 0.0;
double Osmia_Base::m_OsmiaInCocoonEmergCountConst = 0.0;
double Osmia_Base::m_OsmiaInCocoonEmergCountSlope = 0.0;
double Osmia_Base::m_OsmiaFemaleMassFromProvMassConst = 0.0;
double Osmia_Base::m_OsmiaFemaleMassFromProvMassSlope = 0.0;
double Osmia_Base::m_TempToday = -9999;
int Osmia_Base::m_TempTodayInt = -9999;
OsmiaParasitoid_Population_Manager* Osmia_Base::m_OurParasitoidPopulationManager = NULL;
double Osmia_InCocoon::m_OverwinteringTempThreshold = 0.0;
double Osmia_Base::m_OsmiaFemaleBckMort = 0.0;
int Osmia_Base::m_OsmiaFindNestAttemptNo = 0;
int Osmia_Base::m_OsmiaFemaleMinEggsPerNest = 0;
int Osmia_Base::m_OsmiaFemaleMaxEggsPerNest = 0;
double  Osmia_Base::m_CocoonToProvisionMass = 0.0;
double  Osmia_Base::m_ProvisionToCocoonMass = 0.0;
double  Osmia_Base::m_TotalProvisioningMassLoss = 0.0;
double  Osmia_Base::m_TotalProvisioningMassLossRange = 0.0;
double  Osmia_Base::m_TotalProvisioningMassLossRangeX2 = 0.0;
bool 	Osmia_Base::m_UsingMechanisticParasitoids = false;
double Osmia_Base::m_PollenScoreToMg = 0.0;
double Osmia_Base::m_DensityDependentPollenRemovalConst = 0.0;
double Osmia_Base::m_MaleMinTargetProvisionMass = 0.0;
double Osmia_Base::m_MaleMaxTargetProvisionMass = 0.0;
double Osmia_Base::m_FemaleMinTargetProvisionMass = 0.0;
double Osmia_Base::m_FemaleMaxTargetProvisionMass = 0.0;
double Osmia_Base::m_MaleMaxMass = 0.0;
double Osmia_Base::m_FemaleMinMass = 0.0;
double Osmia_Base::m_FemaleMaxMass = 0.0;
double Osmia_Base::m_MinimumCellConstructionTime = 0.0;
double Osmia_Base::m_MaximumCellConstructionTime = 0.0;
int Osmia_Base::m_TotalNestsPossible = 0;
double Osmia_Base::m_BombylidProbability = 0.0;
double Osmia_Base::m_ParasitismProbToTimeCellOpen = 0.0;
double Osmia_Base::m_OsmiaFemaleR50distance = 0.0;
double Osmia_Base::m_OsmiaFemaleR90distance = 0.0;
int Osmia_Base::m_OsmiaFemaleLifespan = 0;
int Osmia_Base::m_OsmiaFemalePrenesting = 0;
vector<double>  Osmia_Base::m_ParasitoidAttackChance = {};
Osmia_Nest_Manager* Osmia_Nest::m_OurManager = NULL;
array<double,12> OsmiaParasitoidSubPopulation::m_MortalityPerMonth = { 0,0,0,0,0,0,0,0,0,0,0,0 };
int OsmiaParasitoidSubPopulation::m_ThisMonth = -1;
vector<double> Osmia_Female::m_FemaleForageEfficiency = {};
double Osmia_Female::m_pollengiveupthreshold = 0.0;
double Osmia_Female::m_pollengiveupreturn = 0.0;
OsmiaForageMask Osmia_Female::m_foragemask;
OsmiaForageMaskDetailed Osmia_Female::m_foragemaskdetailed(1,600);
int Osmia_Female::m_ForageSteps = 20;
#ifdef __OSMIARECORDFORAGE
double Osmia_Female::m_foragesum = 0.0;
int Osmia_Female::m_foragecount = 0;
#endif
#ifdef __OSMIA_PESTICIDE
double Osmia_Female::m_OsmiaPPPEffectProb = 0.0;
double Osmia_Female::m_OsmiaPPPThreshold = 0.0;
#endif

//---------------------------------------------------------------------------

Osmia_Population_Manager::~Osmia_Population_Manager (void)
{
//	delete m_PollenNectarLists;
#ifdef __OSMIATESTING
	m_eggsfirstnest.close();
	ofstream ofile("EggsDistributions.txt", ios::out);
	for (int i = 0; i < 30; i++)
	{
		ofile << m_egghistogram[0][i] << '\t' << m_egghistogram[1][i] << '\t' << m_egghistogram[2][i] << '\t' << m_egghistogram[3][i] << endl;
	}
	ofile.close();
#endif // __OSMIATESTING
}
//---------------------------------------------------------------------------

Osmia_Population_Manager::Osmia_Population_Manager(Landscape* L) : Population_Manager(L, 6)
{
	/** Loads the list of Animal Classes. */
	m_ListNames[0] = "Egg";		// from egg laying to the beginning of feeding
	m_ListNames[1] = "Larva";	// larval development + cocoon spinning
	m_ListNames[2] = "Prepupa";	// the summer diapause period
	m_ListNames[3] = "Pupa";
	m_ListNames[4] = "In Cocoon";	// fully developed adults remaining within cocoons (includes overwintering period)
	m_ListNames[5] = "Female";
	m_ListNameLength = 6;
	m_SimulationName = "Osmia";
	// Init performs intialisation of the data to run the Osmia model
	Init();
	// Set up neccessary conditions for mid-lifecycle start
	m_PreWinteringEndFlag = true;
	m_OverWinterEndFlag = false;
	// Create some animals
	struct_Osmia* sp;
	sp = new struct_Osmia;
	sp->OPM = this;
	sp->L = m_TheLandscape;
	double minmass =(cfg_OsmiaFemaleMassMin.value() - 4) / 0.25;
	double maxmass = (cfg_OsmiaFemaleMassMax.value() - 4) / 0.25;
	sp->mass = minmass + (maxmass - minmass) * g_rand_uni();
	sp->parasitised = TTypeOfOsmiaParasitoids::topara_Unparasitised;
	sp->sex = true;
	for (int i = 0; i < cfg_OsmiaStartNo.value(); i++) // This will need to be an input variable (config)
	{
		sp->x = random(SimW);
		sp->y = random(SimH);
		int pindex = m_TheLandscape->SupplyPolyRefIndex(sp->x, sp->y);
		sp->nest = CreateNest(sp->x, sp->y, pindex);
		sp->nest->AddEgg(NULL);
		CreateObjects(TTypeOfOsmiaLifeStages::to_OsmiaInCocoon, NULL, sp, 1);
	}
	for (unsigned co = 0; co<unsigned(SupplyListSize(int(TTypeOfOsmiaLifeStages::to_OsmiaInCocoon))); co++)
	{
		dynamic_cast<Osmia_InCocoon*>(SupplyAnimalPtr(int(TTypeOfOsmiaLifeStages::to_OsmiaInCocoon),co))->SetAgeDegrees(2000);
	}
	// Store the competition scaler for faster access
	m_PollenCompetitionsReductionScaler = cfg_OsmiaDensityDependentPollenRemovalConst.value();
	// Store prepal development rates for faster access
	m_PrePupalDevelRates.resize(42);
	for (int i = 0; i < 42; i++) m_PrePupalDevelRates[i] = cfg_OsmiaPrepupalDevelRates.value(i);
}

void Osmia_Population_Manager::Init()
{
	/**
	* Initiates the Osmia nests on each landscape element
	* The temperature related mortality needs to be calculated for in nest development and stored in the static variable in the Osmia_Base

	*/
	struct_Osmia o_data;
	o_data.age = 0;
	o_data.L = m_TheLandscape;
	o_data.OPM = this;
	o_data.x = 0;
	o_data.y = 0;
	o_data.nest = NULL;
	o_data.parasitised = TTypeOfOsmiaParasitoids::topara_Unparasitised;
	o_data.mass = 100;
	o_data.sex = true;
	m_PreWinteringEndFlag = true;
	m_OurOsmiaNestManager.InitOsmiaBeeNesting();
	// Set the values of egg/base static variables
	Osmia_Egg egg(&o_data);
	egg.SetParameterValues();
	/*temporarily uses .get()'ed version of GetPopulation to get the raw pointer, will move to smart pointers: issue opened*/
	egg.SetParasitoidManager(static_cast<OsmiaParasitoid_Population_Manager*>(this->m_TheLandscape->SupplyThePopManagerList()->GetPopulation(TOP_OsmiaParasitoids)));
	// Set the values of InCocoon static variables
	Osmia_InCocoon ic(&o_data);
	ic.SetOverwinteringTempThreshold(cfg_OsmiaInCocoonOverwinteringTempThreshold.value());
	// Set the values of female static variables
	Osmia_Female female(&o_data);
	female.SetDailyMort(cfg_OsmiaFemaleBckMort.value());
	female.SetMinEggsPerNest(cfg_OsmiaMinNoEggsInNest.value());
	female.SetMaxEggsPerNest(cfg_OsmiaMaxNoEggsInNest.value());
	female.SetCocoonToProvisionMass(cfg_OsmiaProvMassFromCocoonMass.value());// Also sets some other derived static members
	female.SetProvisionToCocoonMass(cfg_OsmiaCocoonMassFromProvMass.value());
	female.SetPollenScoreToMg(cfg_PollenScoreToMg.value());
	female.SetMinimumCellConstructionTime(cfg_MinimumCellConstructionTime.value());
	female.SetMaximumCellConstructionTime(cfg_MaximumCellConstructionTime.value());
	female.SetTotalNestsPossible(cfg_TotalNestsPossible.value());
	female.SetBombylidProbability(cfg_OsmiaBombylidProb.value());
	female.SetParasitismProbToTimeCellOpen(cfg_OsmiaParasitismProbToTimeCellOpen.value());
	female.SetUsingMechanisticParasitoids(cfg_UsingMechanisticParasitoids.value());
	female.SetNestFindAttempts(cfg_OsmiaFemaleFindNestAttemptNo.value());
	female.SetForageSteps(cfg_OsmiaForageSteps.value());
	female.SetForageMaskDetailed(cfg_OsmiaDetailedMaskStep.value(), cfg_OsmiaTypicalHomingDistance.value());
	female.SetPollenGiveUpThreshold(cfg_OsmiaPollenGiveUpThreshold.value());
	female.SetPollenGiveUpReturn(cfg_OsmiaPollenGiveUpReturn.value());
#ifdef __OSMIARECORDFORAGE
	female.m_foragesum = 0.0;
	female.m_foragecount = 0;
#endif
#ifdef __OSMIA_PESTICIDE
	female.m_OsmiaPPPEffectProb = cfg_OsmiaPesticideProbability.value();
	female.m_OsmiaPPPThreshold = cfg_OsmiaPesticideThreshold.value();
#endif

	// Read the pollen and nectar thresholds
	OsmiaPollenNectarThresholds pnt;
	for (int m = 0; m < 12; m++)
	{
		pnt.m_pollenTquan = cfg_OsmiaPollenThresholds.value(m);
		pnt.m_pollenTqual = cfg_OsmiaPollenThresholds.value(m + 12);
		pnt.m_nectarTquan = cfg_OsmiaNectarThresholds.value(m);
		pnt.m_nectarTqual = cfg_OsmiaNectarThresholds.value(m + 12);
		m_PN_thresholds.push_back(pnt);
	}

	// Calculates sexratio for egg lookups

	// Create a data curve for 0 to 59 days for the sex ratio
	// Logistic(x | x0, min, max, k) = min + (max - min) / (1 + exp(-k * (x - x0)))
	vector<double> params_logistic, params_lin, params_logistic2, params_lin2; // these are X0,min,max,k, and linear a,b respectively
	params_logistic = cfg_OsmiaSexRatioVsMotherAgeLogistic.value();
	params_lin = cfg_OsmiaSexRatioVsMotherMassLinear.value();
	eggsexratiovsagelogisticcurvedata curve1;
	params_lin2 = Cfg_OsmiaFemaleCocoonMassVsMotherMassLinear.value();
	params_logistic2 = Cfg_OsmiaFemaleCocoonMassVsMotherAgeLogistic.value();
	femalecocoonmassvsagelogisticcurvedata curve2;
	for (double mass = cfg_OsmiaFemaleMassMin.value(); mass <= cfg_OsmiaFemaleMassMax.value(); mass += cfg_OsmiaAdultMassCategoryStep.value())
	{
		for (unsigned age = 0; age <= 60; age++)
		{
			// Calculate the data for the egg sex ration curve
			double adjustedmax = params_lin[0] * mass + params_lin[1];
			curve1.push_back(params_logistic[1] + (adjustedmax - params_logistic[1]) / (1 + exp(-params_logistic[3] * (age - params_logistic[0]))));
			// Calculate the data for the cocoon mass vs female mass curve
			double avg_female_cocoon_mass = params_lin2[0] * mass + params_lin2[1];
			double first_female_cocoon_mass = avg_female_cocoon_mass + cfg_Osmia_LifetimeCocoonMassLoss.value() / 2.0; // this lifetime cocoon mass loss nedds to have +/- 5 mg
			// NB this is calculated as provisioning mass
			curve2.push_back( 40.0+ (cfg_OsmiaProvMassFromCocoonMass.value() * (params_logistic2[1] + (first_female_cocoon_mass - params_logistic2[1]) / (1 + exp(-params_logistic2[3] * (age - params_logistic2[0]))))));
		}
		m_EggSexRatioEqns.push_back(curve1);
		m_FemaleCocoonMassEqns.push_back(curve2);
		curve1.clear();
		curve2.clear();
	}
	// Calculate nest provisioning time parameters
	for (int d = 0; d < 365; d++) //here max 60 is needed because then bee will die anyway
	{
		/** Calculate number of hours needed for provisioning of one cell depending on Osmia age [based on Seidelmann 2006]  */
		/***CJT** Changed from daily to hours */
		double eff = 21.643 / (1 + pow(exp(1.0), (log(d) - log(18.888)) * 3.571)); // provisioning efficiency [mg/h]
		double constructime = (2.576 * eff + 56.17) / eff;   // cell construction time [h]
		m_NestProvisioningParameters[d] = int(constructime );	// days needed for 1 cell construction
	}
	/**
	* Sets the parasitoid parameters for per capitata likelihood of parasitism
	*/
	female.SetParasitoidParameters(cfg_OsmiaPerCapitaParasationChance.value());
	/**
	* Creates the data structures to hold Osmia density measures
	*/
	m_GridExtent = SimW / 1000;
	int GEy = SimH / 1000;
	m_FemaleDensityGrid.resize(m_GridExtent * GEy);
	ClearDensityGrid();
#ifdef __OSMIATESTING

	for (int i = 0; i < 30; i++)
	{
		m_egghistogram[0][i] = 0;
		m_egghistogram[1][i] = 0;
		m_egghistogram[2][i] = 0;
		m_egghistogram[3][i] = 0;
	}
	m_eggsfirstnest.open("eggsfirstnest.txt", ios_base::out);
	ofstream ofile("OsmiaFemaleWeights.txt", ios::out);
#endif

	/**
	* Calculate the foraging efficiency with age mg/hr
	*/
	Osmia_Female::AddForageEfficiency(0);
	for (int i = 1; i <= 100; i++) Osmia_Female::AddForageEfficiency(21.643 / (1 + exp((log(i) - log(18.888)) * 3.571)));
	/**
	* Resets the OsmiaStageLengths output file.
	*/
	ofstream file1("OsmiaStageLengths.txt", ios::out);
	file1.close();

}
//---------------------------------------------------------------------------

void Osmia_Population_Manager::CreateObjects(TTypeOfOsmiaLifeStages os_type, TAnimal * a_caller, struct_Osmia * data, int number) {
	Osmia_Egg*  new_Osmia_Egg;
	Osmia_Larva*  new_Osmia_Larva;
	Osmia_Prepupa* new_Osmia_Prepupa;
	Osmia_Pupa*  new_Osmia_Pupa;
	Osmia_InCocoon*  new_Osmia_InCocoon;
	Osmia_Female*  new_Osmia_Female;
#ifdef __RECORDOSMIAEGGPRODUCTION
	if (os_type == TTypeOfOsmiaLifeStages::to_OsmiaEgg) RecordEggProduction(number);
#endif
	for (int i = 0; i < number; i++) {
		switch (os_type) {
		case TTypeOfOsmiaLifeStages::to_OsmiaEgg:
			if (unsigned(SupplyListSize(int(os_type))) > GetLiveArraySize(int(os_type))) {
				// We need to reuse an object
				new_Osmia_Egg = dynamic_cast<Osmia_Egg*>(SupplyAnimalPtr(int(os_type),GetLiveArraySize(int(os_type))));
				new_Osmia_Egg->ReInit(data);
			}
			else {
				new_Osmia_Egg = new Osmia_Egg(data);
                PushIndividual(int(os_type),new_Osmia_Egg);
			}
			IncLiveArraySize(int(os_type));
			data->nest->AddEgg(new_Osmia_Egg);

			break;
		case TTypeOfOsmiaLifeStages::to_OsmiaLarva:
			if (unsigned(SupplyListSize(int(os_type))) > GetLiveArraySize(int(os_type))) {
				// We need to reuse an object
				new_Osmia_Larva = dynamic_cast<Osmia_Larva*>(SupplyAnimalPtr(int(os_type),GetLiveArraySize(int(os_type))));
				new_Osmia_Larva->ReInit(data);
			}
			else {
				new_Osmia_Larva = new Osmia_Larva(data);
                PushIndividual(int(os_type),new_Osmia_Larva);
			}
			IncLiveArraySize(int(os_type));
			// Need to replace the pointer held by the nest (egg->larvae)
			data->nest->ReplaceNestPointer(a_caller,new_Osmia_Larva);
			break;
		case TTypeOfOsmiaLifeStages::to_OsmiaPrepupa:
			if (unsigned(SupplyListSize(int(os_type))) > GetLiveArraySize(int(os_type))) {
				// We need to reuse an object
				new_Osmia_Prepupa = dynamic_cast<Osmia_Prepupa*>(SupplyAnimalPtr(int(os_type),GetLiveArraySize(int(os_type))));
				new_Osmia_Prepupa->ReInit(data);
			}
			else {
				new_Osmia_Prepupa = new Osmia_Prepupa(data);
                PushIndividual(int(os_type),new_Osmia_Prepupa);
			}
			IncLiveArraySize(int(os_type));
			data->nest->ReplaceNestPointer(a_caller, new_Osmia_Prepupa);
			break;
		case TTypeOfOsmiaLifeStages::to_OsmiaPupa:
			if (unsigned(SupplyListSize(int(os_type))) > GetLiveArraySize(int(os_type))) {
				// We need to reuse an object
				new_Osmia_Pupa = dynamic_cast<Osmia_Pupa*>(SupplyAnimalPtr(int(os_type),GetLiveArraySize(int(os_type))));
				new_Osmia_Pupa->ReInit(data);
			}
			else {
				new_Osmia_Pupa = new Osmia_Pupa(data);
                PushIndividual(int(os_type),new_Osmia_Pupa);
			}
			IncLiveArraySize(int(os_type));
			data->nest->ReplaceNestPointer(a_caller, new_Osmia_Pupa);
			break;
		case TTypeOfOsmiaLifeStages::to_OsmiaInCocoon:
			if (unsigned(SupplyListSize(int(os_type))) > GetLiveArraySize(int(os_type))) {
				// We need to reuse an object
				new_Osmia_InCocoon = dynamic_cast<Osmia_InCocoon*>(SupplyAnimalPtr(int(os_type),GetLiveArraySize(int(os_type))));
				new_Osmia_InCocoon->ReInit(data);
			}
			else {
				new_Osmia_InCocoon = new Osmia_InCocoon(data);
                PushIndividual(int(os_type),new_Osmia_InCocoon);
			}
			IncLiveArraySize(int(os_type));
			data->nest->ReplaceNestPointer(a_caller, new_Osmia_InCocoon);
			break;
		case TTypeOfOsmiaLifeStages::to_OsmiaFemale:
				if (unsigned(SupplyListSize(int(os_type))) > GetLiveArraySize(int(os_type))) {
					// We need to reuse an object
					new_Osmia_Female = dynamic_cast<Osmia_Female*>(SupplyAnimalPtr(int(os_type),GetLiveArraySize(int(os_type))));
					new_Osmia_Female->ReInit(data);
				}
				else {
					new_Osmia_Female = new Osmia_Female(data);
                    PushIndividual(int(os_type),new_Osmia_Female);
				}
				IncLiveArraySize(int(os_type));
				data->nest->RemoveCell(a_caller);
				break;
		}
	}
}
//---------------------------------------------------------------------------
void Osmia_Population_Manager::DoFirst() {
	struct_Osmia o_data;
	o_data.age = 0;
	o_data.L = m_TheLandscape;
	o_data.OPM = this;
	o_data.x = 0;
	o_data.y = 0;
	o_data.nest = NULL;
	o_data.parasitised = TTypeOfOsmiaParasitoids::topara_Unparasitised;
	o_data.mass = 100;
	double temp = m_TheLandscape->SupplyTemp();
	Osmia_Base ob(&o_data);
	ob.SetTemp(temp); // Sets the static variable for temperature for all Osmia (speed optimisation)
	//if ((!g_weather->Raining()) && (temp > 10.0) && (g_weather->GetWind() < 8.0))  m_FlyingWeather = true; else m_FlyingWeather = false;
	//if (temp > 10.0 && g_weather->GetWind() < 8.0)  m_FlyingWeather = true; else m_FlyingWeather = false;
	m_FlyingWeather = g_weather->GetFlyingHours();
	m_OurOsmiaNestManager.UpdateOsmiaNesting(); // Updates nest status for all nests
	ClearDensityGrid(); // Clears this before all the bees get going
	int temp_i = int(floor(temp + 0.5));
	if (temp_i < 0) temp_i = 0;
	m_PrePupalDevelDaysToday = m_PrePupalDevelRates[temp_i];

}

void Osmia_Population_Manager::RecordEggProduction(int a_eggs) {
	// A Osmia has become larva, we need to record this for the statistics
	m_OsmiaEggProdStats.add_variable(a_eggs);
}

void Osmia_Population_Manager::RecordEggLength(int a_length) {
	m_EggStageLength.add_variable(a_length);
}

void Osmia_Population_Manager::RecordLarvalLength(int a_length) {	
	m_LarvalStageLength.add_variable(a_length);
}

void Osmia_Population_Manager::RecordPrePupaLength(int a_length) {
	m_PrePupaStageLength.add_variable(a_length);
}

void Osmia_Population_Manager::RecordPupaLength(int a_length) {
	m_PupaStageLength.add_variable(a_length);
}

void Osmia_Population_Manager::RecordInCocoonLength(int a_length) {
	m_InCocoonStageLength.add_variable(a_length);
}


#ifdef __OSMIATESTING 
void Osmia_Population_Manager::WriteNestTestData(OsmiaNestData a_target, OsmiaNestData a_achieved)
{
	m_eggsfirstnest << a_target.m_no_eggs << '\t' << a_target.m_no_females << '\t';
	for (int i=0; i < a_target.m_cell_provision.size(); i++) {
		m_eggsfirstnest << a_target.m_cell_provision[i] << '\t';
	}
	m_eggsfirstnest << '\n';

	m_eggsfirstnest << a_achieved.m_no_eggs << '\t' << a_achieved.m_no_females << '\t';
	for (int i = 0; i < a_achieved.m_cell_provision.size(); i++) {
		m_eggsfirstnest << a_achieved.m_cell_provision[i] << '\t';
	}
	m_eggsfirstnest << '\n';
}
#endif

void Osmia_Population_Manager::DoBefore()
{
	/** \brief Things to do before the Step */
	/** To save time we only refil the pollen map if any adults are around to care about this. */
	/** If some stages survive to winter due to slow development we kill these here */
	int today = m_TheLandscape->SupplyDayInYear();
	if (today == 0)
	{
		unsigned size2 = (unsigned)GetLiveArraySize(int(TTypeOfOsmiaLifeStages::to_OsmiaLarva));
		for (unsigned j = 0; j < size2; j++) {
			SupplyAnimalPtr(int(TTypeOfOsmiaLifeStages::to_OsmiaLarva), j)->KillThis();
		}
		size2 = (unsigned)GetLiveArraySize(int(TTypeOfOsmiaLifeStages::to_OsmiaPrepupa));
		for (unsigned j = 0; j < size2; j++) {
			SupplyAnimalPtr(int(TTypeOfOsmiaLifeStages::to_OsmiaPrepupa), j)->KillThis();
		}
		size2 = (unsigned)GetLiveArraySize(int(TTypeOfOsmiaLifeStages::to_OsmiaPupa));
		for (unsigned j = 0; j < size2; j++) {
			SupplyAnimalPtr(int(TTypeOfOsmiaLifeStages::to_OsmiaPupa), j)->KillThis();
		}
#ifdef __OSMIATESTING
		// Record the Osmia weights if needed
		ofstream ofile("OsmiaFemaleWeights.txt", ios::app);
		int sz = m_FemaleWeights.size();
		vector<int> histogram;
		for (int h = 0; h <= 20; h++) histogram.push_back(0);
		for (int i = 0; i < sz; i++) {
			// Create histogram values
			int col = int(m_FemaleWeights[i] / 10);
			histogram[col]++;
		}
		for (int h = 0; h <= 20; h++) {
			ofile << histogram[h] << '\t';
		}
		ofile << endl;
		ofile.close();
		m_FemaleWeights.clear();
#endif

	}
	// Debug code to ensure all nests have some occupied cells.
	if (GetLiveArraySize(int(TTypeOfOsmiaLifeStages::to_OsmiaFemale)) == 0)
	{
		// Only if no females around to mess this up.
		m_OurOsmiaNestManager.CheckZeroNests();
	}
}
//---------------------------------------------------------------------------

OsmiaParasitoid_Population_Manager::OsmiaParasitoid_Population_Manager(Landscape * L, int a_cellsize) : Population_Manager(L)
{
	/**
	* Intialises each subpopulation of Osmia parasitoids. Creates subpopulation maps and intialises each population
	* Parasitoid populations are described by the enum TTypeOfOsmiaParasitoids
	*/
	// Calculate the number of cells
	m_CellSize = a_cellsize;
	m_Wide = (1 + (m_TheLandscape->SupplySimAreaWidth() / m_CellSize));
	m_High = (1 + (m_TheLandscape->SupplySimAreaHeight() / m_CellSize));
	m_Size = m_Wide*m_High;
	unsigned notypes = static_cast<int>(TTypeOfOsmiaParasitoids::topara_foobar) - 1;
	std::vector<OsmiaParasitoidSubPopulation*>::size_type index = notypes * m_Size;;
	m_SubPopulations.resize(index);
	for (unsigned ps = 0; ps < notypes; ps++)
	{
		double range = cfg_OsmiaParasStartHighLow.value(2 * ps) - cfg_OsmiaParasStartHighLow.value(2 * ps + 1);
		for (unsigned int y = 0; y < m_High; y++)
			for (unsigned int x = 0; x < m_Wide; x++)
			{
				OsmiaParasitoidSubPopulation* OPsP = new OsmiaParasitoidSubPopulation(cfg_OsmiaParasDispersal.value(ps), (g_rand_uni() * range) + cfg_OsmiaParasStartHighLow.value(2 * ps + 1), x, y, m_Wide, m_High, this);
				index = ps* (x + y * m_Wide);
				m_SubPopulations[index] = OPsP;
			}
		array<double, 12> morts;
		for (int m=0; m<12; m++) morts[m]= cfg_OsmiaParasDailyMort.value(ps*12+m);
		m_SubPopulations[0]->SetMortalities(morts); // This is a static member so only the one intialisation is needed, therefore outside the for y loop
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

OsmiaParasitoid_Population_Manager::~OsmiaParasitoid_Population_Manager()
{
	for (int p=0; p< int(m_SubPopulations.size()); p++)
	{
		delete m_SubPopulations[p];
	}
}
//---------------------------------------------------------------------------------------------------------------------------------

array<double, static_cast<unsigned>(TTypeOfOsmiaParasitoids::topara_foobar)> OsmiaParasitoid_Population_Manager::GetParasitoidNumbers(int a_x, int a_y)
{
	array<double, static_cast<unsigned>(TTypeOfOsmiaParasitoids::topara_foobar)> parasitoiddensitylist;
	// First find the right sub-population
	int subpop = (a_x / m_CellSize) + (a_y / m_CellSize) * m_Wide;
	for (unsigned ps = 1; ps < static_cast<int>(TTypeOfOsmiaParasitoids::topara_foobar); ps++)
	{
		parasitoiddensitylist[ps] = m_SubPopulations[subpop]->GetSubPopnSize();
		subpop += m_Size;
	}
	return parasitoiddensitylist;
}
//---------------------------------------------------------------------------------------------------------------------------------

OsmiaParasitoidSubPopulation::OsmiaParasitoidSubPopulation(double a_dispersalfraction, double a_startno, int a_x, int a_y, int a_wide, int a_high, OsmiaParasitoid_Population_Manager* a_popman)
{
	m_NoParasitoids = a_startno;
	m_x = a_x;
	m_y = a_y;
	m_OurPopulationManager = a_popman;
	m_DiffusionRate = a_dispersalfraction;
	
	/**
	* Calculate neighbour cells for dispersal.
	* There will be 8 cells for dispersal
	* We need also to consider the bounds for wrap around
	* m_CellIndexArray is an array containing numbers which represent the cell position in a spatial array (1 dimension representing the matrix). 
	* The actual cells are stored in the population manager for the parasitoids.
	*/
	int count = 0;
	int dx = -1;
	int dy = 0;
	int dxx = 1;
	int dyy = 0;
	for (int x = dx; x <= dxx; x++)
	{
		int actualx = m_x + dx;
		if (actualx < 0) actualx += a_wide;
		if (actualx >= a_wide) actualx -= a_wide;
		int actualy1 = m_y - dx;
		if (actualy1 < 0) actualy1 += a_high;
		int actualy2 = m_y + dx;
		if (actualy2 > a_high) actualy2 -= a_high;
		m_CellIndexArray[count] = actualx + actualy1 * a_wide;
		count++;
		m_CellIndexArray[count] = actualx + actualy2 * a_wide;
		count++;
		// Now do the y
	}
	int actualx1 = m_x + dx;
	int actualx2 = m_x - dx;
	if (actualx1 < 0) actualx1 += a_wide;
	if (actualx2 >= a_wide) actualx2 -= a_wide;
	for (int y = dy; y <= dyy; y++)
	{
		int actualy = m_y + dy;
		if (actualy < 0) actualy += a_high;
		if (actualy > a_high) actualy -= a_high;
		m_CellIndexArray[count] = actualx1 + actualy * a_wide;
		count++;
		m_CellIndexArray[count] = actualx2 + actualy * a_wide;
		count++;
	}

	/**
	* Calculate the diffusion rates.
	* This is done per row by dividing the total diffusion rate by the number of rows,
	* then dividing out result by the number of squares in the row.  Currently we assume dispersal only to the next square (8 round this current population)
	*/
	m_DiffusionConstant = m_DiffusionRate;
}

void OsmiaParasitoidSubPopulation::DailyMortality()
{
	/**
	* Applies a daily probability of mortality for adults. This is provided as a monthly figure, which allows some seaonality to be included.
	* It is a static member so only one copy exists per subpopulation type.
	*/
	m_NoParasitoids *= m_MortalityPerMonth[m_ThisMonth];
}

void OsmiaParasitoidSubPopulation::Dispersal()
{
	/**
	Calculates the number of dispersers per cell to the 8 surrounding cells and removes them from the present cell
	*/
	int count = 0;
	double dispersers = m_NoParasitoids * m_DiffusionConstant;
	m_NoParasitoids-=dispersers;
	double disperserspercell = dispersers / 8.0;
	m_NoParasitoids -= dispersers;
	for (int c = 0; c < 8; c++)
	{
		m_OurPopulationManager->AddDispersers(m_CellIndexArray[count++], dispersers);
	}
}

void OsmiaParasitoidSubPopulation::Reproduce()
{
	/**
	* This links the chance of a cell being parasitised to the local population size.
	* The probability of attack depends on factors under control of the bee therefore currently there is nothing needed here.
	* When an egg is produced the local conditions will determine its fate wrt parasitoids. 
	* Therefore parasitoid attack density related probability curves are a property of the bee.
	*/
}

OsmiaParasitoidSubPopulation::~OsmiaParasitoidSubPopulation()
{
}

void Osmia_Nest_Manager::InitOsmiaBeeNesting()
{
	/** Reads in an input file **Ela** and provides a max nest number to each instance of LE* in the m_elems vector */
	array<TTypesOfLandscapeElement, tole_Foobar> tole_ref;
	array<double, tole_Foobar> maxOsmiaNests;
	array<double, tole_Foobar> minOsmiaNests;
	fstream ifile(cfg_OsmiaNestByLE_Datafile.value(), ios::in);
	if (!ifile.is_open()) {
		g_msg->Warn("Cannot open file: ", cfg_OsmiaNestByLE_Datafile.value());
		exit(1);
	}
	// Read the file tole type by tole type - here we can't rely on the order but need the tole number
	int length;
	ifile >> length;
	if (length != tole_Foobar) {
		g_msg->Warn("Inconsistent file length with tole_Foobar: ", int(tole_Foobar));
		exit(1);
	}
	// read the file
	for (int i = 0; i < length; i++)
	{
		int toleref;
		ifile >> toleref >> minOsmiaNests[i] >> maxOsmiaNests[i];
		tole_ref[i] = g_landscape_p->TranslateEleTypes(toleref);
		if (minOsmiaNests[i] > 0) m_PossibleNestType[tole_ref[i]] = true; else m_PossibleNestType[tole_ref[i]] = false;
	}
	ifile.close();
	unsigned nopolys = g_landscape_p->SupplyNumberOfPolygons();
	OsmiaPolygonEntry ope(0, 0);
	m_PolyList.resize(nopolys);
	double totalnests = 0;
	for (unsigned int e = 0; e < nopolys; e++) {
		ope.SetAreaAttribute( int(floor(g_landscape_p->SupplyPolygonAreaVector(e)+0.5)));
		ope.SetIndexAttribute(e);
		m_PolyList[e] = ope;
		TTypesOfLandscapeElement eletype = g_landscape_p->SupplyElementTypeFromVector(e);
		// first find the eletype
		int found = -1;
		for (int j = 0; j < length; j++)
		{
			if (tole_ref[j] == eletype) {
				found = j;
				break;
			}
		}
		if (found == -1) {
			g_msg->Warn("Inconsistent file data, missing tole type ref: ", eletype);
			exit(1);
		}
		// We have the ref type, so now calculate the number of nests and set it
		double n = (minOsmiaNests[found] + double(g_rand_uni()  * (maxOsmiaNests[found] - minOsmiaNests[found]))) * 0.0001;
		m_PolyList[e].SetMaxOsmiaNests(n); // ***CJT*** Added 0.001 scaler here to reduce densities for debug
		totalnests += m_PolyList[e].GetMaxNoNests(); // Just to have a record of the max possible nests
	}
}
