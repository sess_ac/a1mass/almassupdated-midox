/*
*******************************************************************************************************
Copyright (c) 2019, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Osmia.h
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file Osmia.h
Version of  2 August 2019 \n
By Chris J. Topping \n \n
*/
#ifndef __CJT_OSMIAFIXEDRESOURCE  // For debug only
  //#define __CJT_OSMIAFIXEDRESOURCE
#endif

#ifndef __OSMIARECORDFORAGE
//#define __OSMIARECORDFORAGE
#endif

#ifndef __OSMIA_PESTICIDE
#define __OSMIA_PESTICIDE
#endif

#ifndef __OSMIATESTING
#define __OSMIATESTING
#endif

//---------------------------------------------------------------------------
#ifndef OsmiaH
#define OsmiaH
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class Osmia_Population_Manager;
class OsmiaParasitoid_Population_Manager;
class Osmia_Nest_Manager;
class PollenMap_centroidbased;
class probability_distribution;
class Osmia_Egg;
class Osmia_Female;
class struct_Osmia;
class Osmia_Base;

//------------------------------------------------------------------------------
/**
Used for the population manager's list of Osmia
*/
//typedef vector<Osmia*> TListOfOsmia;
//---------------------------------------------------------------------------

#define __OSMIA_DIST_SIZE 10000

/** \brief The assumed total mass loss first cocoon to last per nest in terms of female cocoon mass */
static CfgFloat cfg_OsmiaTotalCocoonMassLoss("OSMIATOTALCOCOONMASSLOSS", CFG_CUSTOM, 15.0);
/** \brief The range aroudn the assumed total mass loss first cocoon to last per nest in terms of female cocoon mass */
static CfgFloat cfg_OsmiaTotalCocoonMassLossRange("OSMIATOTALCOCOONMASSLOSSRANGE", CFG_CUSTOM, 5.0);

/**
\brief The possible behavioural states for Osmia classes 
*/
enum TTypeOfOsmiaState
{
	toOsmias_InitialState = 0,
	toOsmias_Develop,
	toOsmias_NextStage,
	toOsmias_Disperse,
	toOsmias_NestProvisioning,
	toOsmias_ReproductiveBehaviour,
	toOsmias_Emerged,
	toOsmias_Die
};



/**
\brief The types of parasitism possible for Osmia eggs/larvae
*/
enum class TTypeOfOsmiaParasitoids : unsigned // unsigned is used because this may be used as an index to an array
{
	topara_Unparasitised = 0, 
	topara_Bombylid,
	topara_Cleptoparasite,
	topara_foobar
};

class OsmiaForageMask
{
	/**
	* This is a way to speed up searches from a centre point. The predefined mask can be iterated through without having to calculate offsets.
	* It could also be modified to alter distances between locations if needed by altering the step size.
	*/
public:
	/** \brief Holds 20 distances and 8 directions with offsets to x,y */
	int m_mask[20][8][2];
	int m_step;
	int m_step2;
	OsmiaForageMask();
};

class OsmiaForageMaskDetailed
{
	/**
	* This is a way to speed up searches from a centre point. The predefined mask can be iterated through without having to calculate offsets.
	* It could also be modified to alter distances between locations if needed by altering the step size.
	*/
public:
	/** \brief Holds 20 distances and 8 directions with offsets to x,y */
	vector<APoint> m_mask;
	int m_step;
	int m_maxdistance;
	OsmiaForageMaskDetailed(int a_step, int a_maxdistance);
};

class OsmiaNestData
{
public:
	int m_no_eggs;
	int m_no_females;
	vector<double> m_cell_provision;
};

/** \brief The Osmia nest is assumed to be a linear element with egg cells added one by one.*/
class Osmia_Nest : public TAnimal
{
	/**
	* The Osmia_Nest class contains a list of Osmia_Egg pointers to objects it contains.
	* It is descended from TAnimal which means it has a location and has access to the TALMaSSObject Step code if needed..however, right now this is not implemented in the population manager.
	* All information about the egg state is held by the egg itself e.g. sex, parasitoids, so this is simply a list of eggs in order of them being added to the nest
	* The nest can however supply the number of current eggs it holds
	*/
protected:
	/** \brief x-location */
	int m_x;
	/** \brief y-location */
	int m_y;
	/** \brief polygon reference to where the nest is located */
	int m_PolyRef;
	/** \brief list of egg objects */
	vector<TAnimal*>m_cells;
	/** \brief A pointer to the one and only nest population manager */
	static Osmia_Nest_Manager* m_OurManager;
	/** \brief Signals that the nest is closed or open for adding new cells */
	bool m_isOpen;
	/** \brief Simulates the natural variation assumed per nest location based on aspect, exposure etc.. */
	int m_aspectdelay;
public:
	Osmia_Nest(int a_x, int a_y, int a_polyref, Osmia_Nest_Manager* a_manager);
	/** \brief Adds an egg to the nest */
	void AddEgg(TAnimal* a_egg) { m_cells.push_back(a_egg); }
	void ReplaceNestPointer(TAnimal* a_oldpointer, TAnimal* a_newpointer) {
		std::replace(m_cells.begin(), m_cells.end(), a_oldpointer, a_newpointer); // replaces the old life stage pointer with the new one in-place
	}
	void RemoveCell(TAnimal* a_oldpointer);
	/** \brief Debug function - Is this osmia present? */
	bool Find(TAnimal* a_osmia)
	{
		if (std::find(m_cells.begin(), m_cells.end(), a_osmia) != m_cells.end()) return true;
		else {
			return false;
		}
	}
	/** \brief Removes all Osmia from the nest from a target to the end of the tube */
	void KillAllSubsequentCells(TAnimal* a_osmia);
	/** \brief Get the polyref */
	int GetPolyRef() { return m_PolyRef; }
	/** \brief For debug - get the number of nests currently for this polygon */
	int GetNoNests();
	/** \brief Get the number of cells for this nest */
	int GetNoCells() { return int (m_cells.size()); };
	/** \brief Removes the nest from the polygon list */
	void ReleaseNest();
	/** \brief Debug - to check if any nests have zero cells */
	bool ZeroCells() {
		if (m_cells.size() < 1) return false; else return true;
	}
	int GetAspectDelay() { return m_aspectdelay; }
	/** \brief Tells us whether the nest is finished for additions = false or can be added too = true */
	bool GetIsOpen() { return m_isOpen; }
	/** \brief Signals that the nest is closed */
	void CloseNest() { m_isOpen = false; }
	// Debug
	Osmia_Female* m_owner;
};

class Osmia_Base : public TAnimal
{
	/**
	A Osmia must have some simple functionality:
	Inititation and development
	And some simple characteristics, herein age.
	Inherits m_Location_x, m_Location_y, m_OurLandscape from TAnimal
	NB All areas are squares of size length X length
	*/

protected:
	/** \brief Variable to record current behavioural state */
	TTypeOfOsmiaState m_CurrentOState;
	/** \brief A typical member variable - this one is the age in days */
	int m_Age;
	/** \brief This is a time saving pointer to the correct population manager object */
	Osmia_Population_Manager* m_OurPopulationManager;
	/** \brief This is a time saving pointer to the parasitoid population manager object */
	static OsmiaParasitoid_Population_Manager* m_OurParasitoidPopulationManager;
	/** \brief The temperature today. This is static because there is only one temperature today. */
	static double m_TempToday;
	/** \brief The temperature today to the nearest degree. This is static because there is only one temperature today. This is mostly for use in temperature indexed arrays (if used) */
	static int m_TempTodayInt;
	/** \brief This holds the daily mortality for eggs */
	static double m_DailyDevelopmentMortEggs;
	/** \brief This holds the daily mortality for larvae */
	static double m_DailyDevelopmentMortLarvae;
	/** \brief This holds the daily mortality for pre-pupae */
	static double m_DailyDevelopmentMortPrepupae;
	/** \brief This holds the daily mortality for pupae */
	static double  m_DailyDevelopmentMortPupae;
	/** \brief Is the number of day degrees needed for egg development to hatch */
	static double m_OsmiaEggDevelTotalDD;
	/** \brief Is temperature developmental threshold for egg development */
	static double m_OsmiaEggDevelThreshold;
	/** \brief Is the number of day degrees needed for larval hatch above the developmental threshold for larvae */
	static double m_OsmiaLarvaDevelTotalDD;
	/** \brief Is temperature developmental threshold for larval development */
	static double m_OsmiaLarvaDevelThreshold;
	/** \brief Is the number of day degrees needed for pupal hatch above the developmental threshold for pupae */
	static double m_OsmiaPupaDevelTotalDD;
	/** \brief Is temperature developmental threshold for pupal development */
	static double m_OsmiaPupaDevelThreshold;
	/** \brief Number of days for prepupal development */
	static double m_OsmiaPrepupalDevelTotalDays;
	/** \brief 10% of the number of days for prepupal development - just for speed */
	static double m_OsmiaPrepupalDevelTotalDays10pct;
	/** \brief holds the value for the InCocoon overwintering temperature threshold */
	static double m_OsmiaInCocoonOverwinteringTempThreshold;
	/** \brief holds the value for the InCocoon emergence temperature threshold */
	static double m_OsmiaInCocoonEmergenceTempThreshold;
	/** \brief holds the value for the InCocoon prewintering temperature threshold */
	static double m_OsmiaInCocoonPrewinteringTempThreshold;
	/** \brief holds the constant term value for the InCocoon winter mortality calculation */
	static double m_OsmiaInCocoonWinterMortConst;
	/** \brief holds the coefficient value for the InCocoon winter mortality calculation */
	static double m_OsmiaInCocoonWinterMortSlope;
	/** \brief holds the constant term value for the InCocoon emergence counter calculation */
	static double m_OsmiaInCocoonEmergCountConst;
	/** \brief holds the coefficient value for the InCocoon emergence counter calculation */
	static double m_OsmiaInCocoonEmergCountSlope;
	/** \brief holds the constant term value for the female mass calculation from provision mass */
	static double m_OsmiaFemaleMassFromProvMassConst;
	/** \brief holds the coefficient value for the female mass calculation from provision mass */
	static double m_OsmiaFemaleMassFromProvMassSlope;
	/** \brief The minimum target provisioning for a male cell */
	static double m_MaleMinTargetProvisionMass;
	/** \brief The maximum target provisioning for a male cell */
	static double m_MaleMaxTargetProvisionMass;
	/** \brief The minimum target provisioning for a female cell */
	static double m_FemaleMinTargetProvisionMass;
	/** \brief The maximum target provisioning for a female cell */
	static double m_FemaleMaxTargetProvisionMass;
	/** \brief The maximum female mass */
	static double m_FemaleMaxMass;
	/** \brief The minimum female mass */
	static double m_FemaleMinMass;
	/** \brief The minimum Male mass */
	static double m_MaleMaxMass;
	/** \brief The conversion rate from pollen availability score to mg pollen provisioned per day */
	static double m_PollenScoreToMg;
	/** \brief A parameter to link linear reduction in pollen availability to Osmia numbers per 1km2 */
	static double m_DensityDependentPollenRemovalConst;
	/** \brief The shortest possible construction time for a cell - normally 1 day */
	static double m_MinimumCellConstructionTime;
	/** \brief The longest possible construction time for a cell */
	static double m_MaximumCellConstructionTime;
	/** \brief The maximum number of nests possible for a bee */
	static int m_TotalNestsPossible;
	/** \brief holds the probability of bombylid fly parasitism if open nest parasitoid */
	static double m_BombylidProbability;
	/** \brief holds the ratio of open cell parasitism to time cell is open */
	static double m_ParasitismProbToTimeCellOpen;
	/** \brief Holds the probability per capita of parasitoid attack for a subpopulation sized cell */
	static vector<double> m_ParasitoidAttackChance;
	/** \brief holds the value for the female typical homing distance */
	static double m_OsmiaFemaleR50distance;
	/** \brief holds the value for the female max homing distance */
	static double m_OsmiaFemaleR90distance;
	/** \brief holds the value for the duration of prenesting */
	static int m_OsmiaFemalePrenesting;
	/** \brief holds the value for max female lifespan */
	static int m_OsmiaFemaleLifespan;
	/** \brief Static instance of the probability_distribution class of distance probablilties for nest searching and foraging */
	static probability_distribution  m_generalmovementdistances;
	/** \brief Static instance of the probability_distribution class of distance probablilties for dispersal */
	static probability_distribution  m_dispersalmovementdistances;
	/** \brief Static instance of the probability_distribution class of distance probablilties for numbers of eggs per nest */
	static probability_distribution  m_eggspernestdistribution;
	/** \brief Static instance of the probability_distribution class of distance probablilties for approx exponential probs zero to 1.0 */
	static probability_distribution  m_exp_ZeroToOne;
	/** \brief The ratio of cocoon to provision mass */
	static double m_CocoonToProvisionMass;
	/** \brief The ratio of provison to cocoon mass */
	static double m_ProvisionToCocoonMass;
	/** \brief The total provision mass loss expected first cocoon to last */
	static double m_TotalProvisioningMassLoss;
	/** \brief The variability around the expected total mass loss */
	static double m_TotalProvisioningMassLossRange;
	/** \brief Twice the variability around the expected total mass loss */
	static double m_TotalProvisioningMassLossRangeX2;
	/** \brief Flag for switching between ways of calculating parasitism */
	static bool m_UsingMechanisticParasitoids;
	/** \brief Holds the parameter for unspecified female daily mortality */
	static double m_OsmiaFemaleBckMort;
	/** \brief Holds the parameter for minimum planned eggs per nest */
	static int m_OsmiaFemaleMinEggsPerNest;
	/** \brief Holds the parameter for number of find nest tries */
	static int m_OsmiaFindNestAttemptNo;
	/** \brief Holds the parameter for maximum planned eggs per nest */
	static int m_OsmiaFemaleMaxEggsPerNest;
	/** \brief Static instance of the probability_distribution class of emergence day */
	static probability_distribution m_emergenceday;
	/** \brief Holds the parasitoid status - only one is allowed */
	TTypeOfOsmiaParasitoids m_ParasitoidStatus;
	/** A useful pointer to the current nest with double use, for up to InCocoon its the nest where they are, for females its the current being provisioned */
	Osmia_Nest* m_OurNest;
	/** An attribute to record the mass of the bee or provisioned pollen depending on life-stage*/
	double m_Mass;
	/** \brief A variable to hold the number of available forage hours left in a day */
	int m_foragehours;

public:
	/** \brief Osmia constructor */
	Osmia_Base(struct_Osmia* data);
	/** \brief Osmia reinitialise object methods */
	void ReInit(struct_Osmia* data);
	/** \brief Osmia destructor */
	virtual ~Osmia_Base();
	/** \brief Behavioural state dying */
	virtual void st_Dying(void);
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void BeginStep(void) { ; } // NB this is not used in the Osmia_Base code
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void) { ; } // NB this is not used in the Osmia_Base code
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void EndStep(void) { ; } // NB this is not used in the Osmia_Base code
	/** \brief A typical interface function - this one returns the age */
	int GetAge() { return m_Age; }
	/** \brief A typical interface function - this one returns the age */
	void SetAge(int a_age) { m_Age = a_age; }
	/** \brief Returns the bee's mass */
	double GetMass() { return m_Mass; }
	/** \brief Sets the bee's mass */
	void SetMass(double a_mass) { m_Mass = a_mass; }
	/** \brief Set the parasitised status */
	void SetParasitised(TTypeOfOsmiaParasitoids a_status) { 
		m_ParasitoidStatus = a_status; 
	}
	/** \brief Set the parasitised status */
	TTypeOfOsmiaParasitoids GetParasitised( void ) { return m_ParasitoidStatus; }
	Osmia_Nest* GetNest() { return m_OurNest; }
	/** \brief Used to populate the static members holding mortality and development parameters */
	void SetParameterValues();
			 /* Not currently using temperature related mortality
		 void SetNestMortality(double a_MortsByTemp[3][80]) {
			for (int i = 0; i < 80; i++) {
			m_DailyDevelopmentMortEggToCocoon[i] = a_MortsByTemp[0][i];
			m_DailyDevelopmentMortPupa[i] = a_MortsByTemp[1][i];
			m_DailyDevelopmentMortOverWinter[i] = a_MortsByTemp[2][i];
			*/
	void SetTemp(double a_temperature) { 
		m_TempToday = a_temperature;
		m_TempTodayInt = int(floor(a_temperature + 0.5)); 
	}
	/** Set method for m_OsmiaParasitoid_Population_Manager */
	void SetParasitoidManager(OsmiaParasitoid_Population_Manager* a_popman) 
	{ 
		m_OurParasitoidPopulationManager = a_popman; 
	}
};

class Osmia_Egg : public Osmia_Base
{
protected:
	/** \brief This contains the age in degrees for development */
	double m_AgeDegrees;
	/** \brief Holds the sex of the egg. Female = fertilized = true */
	bool m_Sex;
	/** \brief Holds the age when the stage was initiated */
	int m_StageAge;
public:
	/** \brief Osmia_Egg constructor */
	Osmia_Egg(struct_Osmia* data);
	/** \brief Osmia_Egg ReInit for object pool */
	virtual void ReInit(struct_Osmia* data);
	/** \brief Osmia_Egg destructor */
	virtual ~Osmia_Egg();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** \brief A typical interface function - this one returns the agedegrees */
	double GetAgeDegrees() { return m_AgeDegrees; }
	/** \brief A typical interface function - this one returns the agedegrees */
	void SetAgeDegrees(unsigned a_agedegrees) { m_AgeDegrees = a_agedegrees; }
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state hatch */
	virtual TTypeOfOsmiaState st_Hatch(void);
	/** \brief Daily mortality test for eggs */
	virtual bool DailyMortality() { if (g_rand_uni() < m_DailyDevelopmentMortEggs) return true; else return false; }
};

class Osmia_Larva : public Osmia_Egg
{
protected:
public:
	/** \brief Osmia_Larva constructor */
	Osmia_Larva(struct_Osmia* data);
	/** \brief Osmia_Larva ReInit for object pool */
	virtual void ReInit(struct_Osmia* data);
	virtual ~Osmia_Larva();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state pupate */
	virtual TTypeOfOsmiaState st_Prepupate(void);
	/** \brief Daily mortality test for larvae */
	virtual bool DailyMortality() { if (g_rand_uni() < m_DailyDevelopmentMortLarvae) return true; else return false; }
};

class Osmia_Prepupa : public Osmia_Larva
{
public:
	/** \brief Osmia_Prepupa constructor */
	Osmia_Prepupa(struct_Osmia* data);
	/** \brief Osmia_Prepupa ReInit for object pool */
	virtual void ReInit(struct_Osmia* data);
	virtual ~Osmia_Prepupa();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state for emerging from the pupa */
	virtual TTypeOfOsmiaState st_Pupate(void);
	/** \brief Daily mortality test for prepupae */
	virtual bool DailyMortality() { if (g_rand_uni() < m_DailyDevelopmentMortPrepupae) return true; else return false; }
	/** /brief A target for day/temp related development */
	double m_myOsmiaPrepupaDevelTotalDays;
};

class Osmia_Pupa : public Osmia_Prepupa
{
public:
	/** \brief Osmia_Pupa constructor */
	Osmia_Pupa(struct_Osmia* data);
	/** \brief Osmia_Pupa ReInit for object pool */
	virtual void ReInit(struct_Osmia* data);
	virtual ~Osmia_Pupa();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state for emerging from the pupa */
	virtual TTypeOfOsmiaState st_Emerge(void);
	/** \brief Daily mortality test for pupae */
	virtual bool DailyMortality() { if (g_rand_uni() < m_DailyDevelopmentMortPupae) return true; else return false; }

};

class Osmia_InCocoon : public Osmia_Pupa
{
protected:
	int m_emergencecounter;

	/** An attribute to record the number of day degrees calculated for the prewintering period (from start of Osmia_InCocoon stage till the m_PreWinteringEndFlag is set to true) */
	double m_DDPrewinter;
public:
	/** \brief Osmia_Adult constructor */
	Osmia_InCocoon(struct_Osmia* data);
	/** \brief Osmia_Adult ReInit for object pool */
	virtual void ReInit(struct_Osmia* data);
	virtual ~Osmia_InCocoon();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** \brief Set method for m_OverwinteringTempThreshold */
	void SetOverwinteringTempThreshold(double a_temp) { m_OverwinteringTempThreshold = a_temp; }
	/** \brief Returns the number of day degrees during prewinter above 15.0 degrees */
	double GetDDPreWinter() { return m_DDPrewinter; }
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state for emerging from the InCocoon */
	virtual TTypeOfOsmiaState st_Emerge(void);
	/** Calculates pupal mortality as a result of prewinter day degrees */
	bool WinterMortality();
	/** \brief Parameter for overwintering day degrees threshold */
	static double m_OverwinteringTempThreshold;
};

class Osmia_Female : public Osmia_InCocoon
{
public:
#ifdef __OSMIARECORDFORAGE
	static double m_foragesum;
	static int m_foragecount;
#endif
protected:
	//Attributes
	static OsmiaForageMask m_foragemask;
	static OsmiaForageMaskDetailed m_foragemaskdetailed;
	double m_currentpollenlevel;
	/** \brief The change in pollen return that triggers a new search */
	static double m_pollengiveupthreshold;
	/** \brief The minimum pollen return below which a new forage search is triggered */
	static double m_pollengiveupreturn;
	/** \brief records the current density grid index for fast access */
	//int m_DensityIndex;
	/** \brief Keeps track of the number of days a cell is open */
	int m_CellOpenDays;
	/** \brief Keeps track of any part time cell construction hours */
	double m_CellCarryOver;
	/** \brief Contains the number of all eggs yet to lay */
	int m_EggsToLay;
	/** \brief Keeps a track of the planned number of eggs for this nest */
	int m_EggsThisNest;
	/** \brief a flag determining if dispersal is necessary */
	bool m_ToDisperse;
	/** \brief The number of days of post emergence life */
	int m_EmergeAge;
	/** \brief The location of the current nest, holds -1 in m_x when no nest */
	APoint m_CurrentNestLoc;
	/** \brief  The number of days needed for 1 cell construction */
	int m_ProvisioningTime;
	/** \brief  A counter to keep track of the flying days during nest cell construction*/
	int m_FlyingCounter;
	/** \brief Records the amount of pollen currently provisioned in a cell */
	double m_CurrentProvisioning;
	/** \brief  Used to put the bees into a size class - 0,1,2,3 very small, small, medium, big*/
	int m_BeeSizeScore1;
	/** \brief  Used to put the bees into a smaller size classes than used for m_BeeSizeScore1, size class controlled by cfg_OsmaiAdultMassCategoryStep */
	int m_BeeSizeScore2;
	/** \brief A list of female nest targets */
	deque<double>m_NestProvisioningPlan;
	/** \brief A list of eggs male/female */
	deque<bool>m_NestProvisioningPlanSex;
	/** \brief Flag to indicate whether we have a foraging location (=true) */
	bool m_ForageLoc;
	/** \brief Index to the resource providing polygon lists in the Osmia_Population_Manager */
	int m_ForageLocPoly;
	/** \brief The number of distance steps from nest to max forage range */
	static int m_ForageSteps;
	/** \brief A vector holding the age related efficiency of Osmia foraging indexed by day (from Seidelmann 2006) */
	static vector<double> m_FemaleForageEfficiency;
#ifdef __OSMIA_PESTICIDE
public:
	/** \brief The current pesticide death probability after trigger excedence */
	static double m_OsmiaPPPEffectProb;
	/** \brief Pesticide trigger level */
	static double m_OsmiaPPPThreshold;
	/** \brief Respond to pesticide load */
protected:
	/** \brief The current pesticide body burden */
	double m_pppbodyburden;
	void InternalPesticideHandlingAndResponse();
	/** \brief Get body PPP threshold */
	static double GetPPPThreshold() { return m_OsmiaPPPThreshold; }
	/** \brief Get body PPP death chance */
	static double GetPPPEffectProb() { return m_OsmiaPPPEffectProb; }
#endif

#ifdef __OSMIATESTING
	OsmiaNestData m_target;
	OsmiaNestData m_achieved;
	bool m_firstnestflag;
#endif // __OSMIATESTING


	//Methods
	virtual void st_Dying(void)
	{
		// this one is needed because all other stages free a nest space when they die, so this is different
		if (m_OurNest != nullptr)
		{
			m_OurNest->CloseNest();
			if (m_OurNest->GetNoCells() < 1)
			{
				m_OurNest->ReleaseNest();
			}
		}
		KillThis(); // this will kill the animal object and free up space
	}
	//--------------------------------------------------------------------------------------------------------------------------------
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Find a suitable location for a nest */
	virtual bool FindNestLocation(void);
	/** \brief This checks for the need to dispese and does it if necessary. */
	virtual TTypeOfOsmiaState st_Dispersal(void);
	/** \brief The foraging algorithm for Osmia */
	double Forage(void);
	/** \brief This checks for the need to do something regarding reproduction and does it if necessary. */
	virtual TTypeOfOsmiaState st_ReproductiveBehaviour(void);
	/** \brief Calculates the planned number of eggs for the next nest. */
	int PlanEggsPerNest();
	/** \brief This calculates the number of eggs the female should lay. */
	void CalculateEggLoad() {
		/**
		* According to Seidelmann (2010) female of a mass m_mass can produce on average the following number of eggs per nest: \n
		* no_eggs_nest = 0.0371 * m_mass + 2.8399 (+/- 3 eggs)\n
		* If we assume that a bee can colonize a maximum of N nests (onfig variable) in her lifetime, the maximum total number of eggs to lay is defined as:\n
		* total_no_eggs = N * no_eggs_nest
		*/
		m_EggsToLay = int((m_TotalNestsPossible * (0.0371 * m_Mass + 2.8399)) + (g_rand_uni() * 6) - 3);
		m_EggsThisNest = PlanEggsPerNest() + 2; // 2 is added because it will be removed at the start of each nest
	}
	/** \brief Determines the type of parasitoid if any */
	TTypeOfOsmiaParasitoids CalcParaistised(double a_daysopen);
	/** \brief Produces an egg */
	void LayEgg();

public:
	/** \brief Osmia_Female constructor */
	Osmia_Female(struct_Osmia* data);
	/** \brief Osmia_Female ReInit for object pool */
	virtual void ReInit(struct_Osmia* data);
	/** \brief Osmia_Female destructor */
	virtual ~Osmia_Female();
	/** \brief Osmia_Femae initialisation code for Constructor and ReInit */
	virtual void Init(double a_mass);
	/** BeginStep sets up conditions before the Step code */
	virtual void BeginStep(void);
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	void SetForageSteps(int a_sz) { m_ForageSteps = a_sz; }
	/** \brief Initialise the detailed forage mask */
	void SetForageMaskDetailed(int a_step, int a_max) {
		OsmiaForageMaskDetailed fmd(a_step, a_max);
		m_foragemaskdetailed = fmd;
	}
	/** \brief Record the give up level for pollen from a forage patch we are foraging from */
	void SetPollenGiveUpThreshold(double a_prop) { m_pollengiveupthreshold = a_prop; }
	/** \brief Record the give up level for pollen from a forage patch we are foraging from */
	void SetPollenGiveUpReturn(double a_value) { m_pollengiveupreturn = a_value; }
	/** \brief Record the daily mortality parameter values */
	void SetDailyMort(double a_prob) { m_OsmiaFemaleBckMort = a_prob; }
	/** \brief Record the daily mortality parameter values */
	void SetNestFindAttempts(int a_no) { m_OsmiaFindNestAttemptNo = a_no; }
	/** \brief Set the min eggs parameter value */
	void SetMinEggsPerNest(int a_eggs) { m_OsmiaFemaleMinEggsPerNest = a_eggs; }
	/** \brief Set the min eggs parameter value */
	void SetMaxEggsPerNest(int a_eggs) { m_OsmiaFemaleMaxEggsPerNest = a_eggs; }
	/** \brief Set the conversion ratio cocoon to provisioning */
	void SetCocoonToProvisionMass(double a_ratio) {
		m_CocoonToProvisionMass = a_ratio;
		m_TotalProvisioningMassLoss = cfg_OsmiaTotalCocoonMassLoss.value() * a_ratio;
		m_TotalProvisioningMassLossRange = cfg_OsmiaTotalCocoonMassLossRange.value() * a_ratio;
		m_TotalProvisioningMassLossRangeX2 = m_TotalProvisioningMassLossRange * 2.0;
	}
	/** \brief Set the conversion ratio provisioning to cocoon */
	void SetProvisionToCocoonMass(double a_ratio) { m_ProvisionToCocoonMass = a_ratio; }
	/** \brief Set pollen score to provisioned mg value */
	void SetPollenScoreToMg(double a_ratio) { m_PollenScoreToMg = a_ratio; }
	/** \brief Set the target mass for male cell provisioning - this is a minimum */
	void SetMaleMinTargetProvisionMass(double a_mass) { m_MaleMinTargetProvisionMass = a_mass; }
	/** \brief Set the target mass for female cell provisioning - this is a minimum */
	void SetFemaleMinTargetProvisionMass(double a_mass) { m_FemaleMinTargetProvisionMass = a_mass; }
	/** \brief Set the max target mass for female cell provisioning */
	void SetFemaleMaxTargetProvisionMass(double a_mass) { m_FemaleMaxTargetProvisionMass = a_mass; }
	/** \brief Set minimum cell construction time */
	void SetMinimumCellConstructionTime(double a_time) { m_MinimumCellConstructionTime = a_time; }
	/** \brief Set maximum cell construction time */
	void SetMaximumCellConstructionTime(double a_time) { m_MaximumCellConstructionTime = a_time; }
	/** \brief Set the maximum number of nests possible for a bee */
	void SetTotalNestsPossible(int a_total) { m_TotalNestsPossible = a_total; }
	/** \brief Sets Bombylid probability */
	void SetBombylidProbability(double a_prob) { m_BombylidProbability = a_prob; }
	/** \brief Set the conversion ratio time cell open to open cell parasitism */
	void SetParasitismProbToTimeCellOpen(double a_ratio) { m_ParasitismProbToTimeCellOpen = a_ratio; }
	/** \brief Sets the UsingMechanisticParasitoids flag */
	void SetUsingMechanisticParasitoids(bool a_flag) { m_UsingMechanisticParasitoids = a_flag; }
	void SetParasitoidParameters(vector<double> a_params) { m_ParasitoidAttackChance = a_params; }
	/** \brief sets the m_DensityDependentPollenRemovalConst value */
	void SetDensityDependentPollenRemovalConst(double a_value) { m_DensityDependentPollenRemovalConst = a_value; }
	/** \brief Save a forage efficiency value */
	static void AddForageEfficiency(double a_eff) { m_FemaleForageEfficiency.push_back(a_eff); }
};
#endif