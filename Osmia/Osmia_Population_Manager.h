/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Osmia_Population_Manager.h 
Version of  May 2017 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef Osmia_Population_ManagerH
#define Osmia_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Osmia;
class OsmiaParasitoid_Population_Manager;

/**
\brief Osmia life stages modelled
*/
enum class TTypeOfOsmiaLifeStages : int // int is used because this may be used as an index to an array passed in CreateObjects
{
	to_OsmiaEgg = 0,
	to_OsmiaLarva,
	to_OsmiaPrepupa,
	to_OsmiaPupa,
	to_OsmiaInCocoon,
	to_OsmiaFemale
};

typedef vector<double>  eggsexratiovsagelogisticcurvedata;
typedef vector<double>  femalecocoonmassvsagelogisticcurvedata;
//------------------------------------------------------------------------------
/** \brief A data class to hold each pair of pollen and nectar thresholds */
class OsmiaPollenNectarThresholds
{
public:
	double m_pollenTqual = 0.0;
	double m_nectarTqual = 0.0;
	double m_pollenTquan = 0.0;
	double m_nectarTquan = 0.0;
};
//------------------------------------------------------------------------------

class OsmiaParasitoidSubPopulation
{
protected:
	// Attributes
	/** \brief The total number of parasitoids in this population */
	double m_NoParasitoids;
	/** \brief The time-step related total dispersal loss */
	double m_DiffusionRate;
	/** \brief The distance related dispersal loss constants */
	double m_DiffusionConstant; 
	/** \brief The index of the surrounding cells, a speed optimisation */
	int m_CellIndexArray[8]; 
	/** \brief The array storing the sub-populations of parasitoids */
	blitz::Array<double, 2> m_SubPopulations;
	/** \brief cell_index x */
	int m_x;
	/** \brief cell_index y */
	int m_y;
	/** \brief A pointer to the owning population manager */
	OsmiaParasitoid_Population_Manager* m_OurPopulationManager;
	/** \brief Daily mortality rates each month */
	static array<double,12> m_MortalityPerMonth;
	/** \brief records the month (optimsation) */
	static int m_ThisMonth;
	// Methods
public:
	OsmiaParasitoidSubPopulation(double a_dispersalfraction, double a_startno, int a_x, int a_y, int a_wide, int a_high, OsmiaParasitoid_Population_Manager* a_popman);
	~OsmiaParasitoidSubPopulation();
	void Add(double a_change) { m_NoParasitoids += a_change; }
	void Remove(double a_change) { m_NoParasitoids -= a_change; }
	double GetSubPopnSize() { return m_NoParasitoids; }
	/** \brief Removes parasitoids killed by daily mortality */
	void DailyMortality();
	/** \brief Moves parasitoids that move by dispersal */
	void Dispersal();
	/** \brief Carries out any reproduction possible */
	void Reproduce();
	virtual void DoFirst() {
		/** Removes parasitoids killed by daily mortality */
		DailyMortality();
		/** Moves parasitoids that move by dispersal */
		Dispersal();
		/** Carries out any reproduction possible */
		Reproduce();
	}
	void SetThisMonth(int a_month) { m_ThisMonth = a_month;  }
	void SetMortalities(array<double, 12> a_morts) {
		m_MortalityPerMonth = a_morts;
	}
};
//------------------------------------------------------------------------------

class OsmiaParasitoid_Population_Manager : public Population_Manager
{
protected:
	// Attributes
	/** \brief The array storing the sub-populations of parasitoids */
	vector<OsmiaParasitoidSubPopulation*>m_SubPopulations;
	/** \brief A useful pointer to the lanscape object */
	Landscape* m_TheLandscape;
	/** \brief the number of subpopulation cells wide */
	unsigned m_Wide;
	/** \brief the number of subpopulation cells high */
	unsigned m_High;
	/** \brief Width of the cell in m (they are square) */
	unsigned m_CellSize;
	/** \brief Total cells */
	unsigned m_Size;
	// Methods

public:
	OsmiaParasitoid_Population_Manager(Landscape* a_landscape, int a_cellsize);
	~OsmiaParasitoid_Population_Manager();
	void AddDispersers(int a_ref, double a_dispersers) {
		m_SubPopulations[a_ref]->Add(a_dispersers);
	}
	void RemoveParasitoids(int a_ref, double a_dispersers) {
		m_SubPopulations[a_ref]->Remove(a_dispersers);
	}
	double GetSize(int a_ref) { return m_SubPopulations[a_ref]->GetSubPopnSize(); }
	double GetSize(int a_x, int a_y) { return m_SubPopulations[a_x+a_y*m_Wide]->GetSubPopnSize(); }
	/** \brief returns an array with the parasitoid numbers in the cell at the location given by a_x, a_y */
	array<double, static_cast<unsigned>(TTypeOfOsmiaParasitoids::topara_foobar)> GetParasitoidNumbers(int a_x, int a_y);
	void AddParasitoid(TTypeOfOsmiaParasitoids a_type, int a_x, int a_y) 
	{
		int subpop = ((a_x / m_CellSize) + (a_y / m_CellSize) * m_Wide) + (static_cast<unsigned>(a_type)-1) * m_Size;
		m_SubPopulations[subpop]->Add(1);
	}
};
//------------------------------------------------------------------------------
/**
\brief
Used for creation of a new Osmia object
*/
class struct_Osmia
{
 public:
  /** \brief x-coord */
  int x;
  /** \brief y-coord */
  int y;
  /** \brief age */
  int age;
  /** \brief the sex of the Osmia */
  bool sex;
  /** \brief Landscape pointer */
  Landscape* L;
  /** \brief Osmia_Population_Manager pointer */
  Osmia_Population_Manager * OPM;
  /** \brief a pointer to a nest */
  Osmia_Nest* nest;
  /** \brief Are we parasitised */
  TTypeOfOsmiaParasitoids parasitised;
  /** \brief The mass of the Osmia in mg */
  double mass;
};

/** \brief Is the list of nests held by a polygon and associated density controls*/
class OsmiaPolygonEntry
{
protected:
    vector<Osmia_Nest*> m_NestList;
	/** \brief to record the chance of osmia nesting */
	double m_OsmiaNestProb;
	/** \brief to record the number of possible osmia nests */
	double m_MaxOsmiaNests; // Needs to be double because its used to divide later
	/** \brief to record the number of actual osmia nests (only used for speed - it is the same as the size of the vector m_NestList */
	int m_CurrentOsmiaNests;
	/** \brief The polygon area */
	int m_Area;
	/** \brief The polygon reference used by the Landscape and Osmia_Nest_Manager */
	int m_Polyindex;
public:
	/** \brief The constructor for OsmiaPolygonEntry */
	OsmiaPolygonEntry()
	{
		m_CurrentOsmiaNests = 0;
		m_MaxOsmiaNests = 0;
		m_OsmiaNestProb = 0;
		m_Polyindex = -1;
		m_Area = -1;
	}
	OsmiaPolygonEntry(int a_index, int a_area)
	{
		m_CurrentOsmiaNests = 0;
		m_MaxOsmiaNests = 0;
		m_OsmiaNestProb = 0;
		m_Polyindex = a_index;
		m_Area = a_area;
	}

	/** \brief The destructor for OsmiaPolygonEntry */
	~OsmiaPolygonEntry()
	{
		for (std::vector<Osmia_Nest*>::iterator it = m_NestList.begin(); it != m_NestList.end(); ++it) {
			delete *it;
		}
		m_NestList.clear();
	}
	/** \brief Test to see if a nest is found */
	bool IsOsmiaNestPossible() {
		if ((m_CurrentOsmiaNests < m_MaxOsmiaNests) && (g_rand_uni() < m_OsmiaNestProb)) return true;
		return false;
	}
	/** \brief Test release an Osmia nest that is no longer used */
	void ReleaseOsmiaNest(Osmia_Nest* a_nest) {
		m_CurrentOsmiaNests--;
		std::vector<Osmia_Nest*>::iterator toErase;

		toErase = std::find(m_NestList.begin(), m_NestList.end(), a_nest);

		// And then erase if found
		if (toErase != m_NestList.end())
		{
			/* Since container element is a pointer to the nest
			   we need to destroy it too ! */
			delete* toErase;
			m_NestList.erase(toErase);
		}
	}
	/** \brief Recalculate the nest finding probability */
	void UpdateOsmiaNesting()
	{
		if (m_MaxOsmiaNests <= 0) m_OsmiaNestProb = 0.0;
		else {
			m_OsmiaNestProb = 1.0 - (m_CurrentOsmiaNests / m_MaxOsmiaNests);
		}
	}
	/** \brief Add an occupied nest */
	void IncOsmiaNesting(Osmia_Nest* a_nest)
	{
		m_NestList.push_back(a_nest);
		m_CurrentOsmiaNests++;
	}
	/** \brief Sets the max number of Osmia nests for this LE */
	void SetMaxOsmiaNests(double a_noNests)
	{
		double maxnests = a_noNests * m_Area;
		if (maxnests> 2147000000 ) maxnests= 2147000000;
		m_MaxOsmiaNests = int(maxnests);
		if (m_MaxOsmiaNests < 1) m_MaxOsmiaNests = 0;
	}
	/** \brief Sets the area  attrribute */
	void SetAreaAttribute(int a_area) { m_Area = a_area; }
	/** \brief Sets the area  attrribute */
	void SetIndexAttribute(int a_index) { m_Polyindex = a_index; }
	/** \brief Returns the number of current nests */
	int GetNoNests() { return m_CurrentOsmiaNests; }
	/** \brief Returns the max number of nests */
	int GetMaxNoNests() { return m_MaxOsmiaNests; }
	/** \brief Returns the number of current nests */
	bool SanityCheck() { return m_CurrentOsmiaNests == int(m_NestList.size()); }
	/** \brief Checks for zero cell nests */
	bool SanityCheck2() {  
		int sz = int(m_NestList.size()); 
		for (int i = 0; i < sz; i++)
		{
			if (!m_NestList[i]->ZeroCells())
			{
				return false;
			}
		}
		return true;
	}
};

class Osmia_Nest_Manager
{
public:
	/** \brief Osmia nest manager constructor */
	Osmia_Nest_Manager()
	{
		;
	}
	/** \brief Osmia nest manager denstructor */
	~Osmia_Nest_Manager()
	{
		m_PolyList.clear();
	}
	/** \brief Read in the Osmia nest density files and allocate to each LE object */
	void InitOsmiaBeeNesting();
	/** \brief Tell all LE objects to update their osmia nest status */
	void UpdateOsmiaNesting() {
		/**
		Loops through all landscape element objects and updates their Osmia nesting status
		*/
		for (unsigned int s = 0; s < m_PolyList.size(); s++) {
			m_PolyList[s].UpdateOsmiaNesting();
		}
	}
	/** \brief Find out whether an osmia nest can be made here */
	bool IsOsmiaNestPossible(int a_polyindex)
	{
		return m_PolyList[a_polyindex].IsOsmiaNestPossible();
	}
	/** \brief Create the osmia nest here  */
	Osmia_Nest* CreateNest(int a_x, int a_y, int a_polyindex)
	{
		Osmia_Nest* a_nest = new Osmia_Nest(a_x, a_y,a_polyindex, this);
		m_PolyList[a_polyindex].IncOsmiaNesting(a_nest); 

		return a_nest;
	}
	/** \brief Reopen the osmia nest here  */
	void ReleaseOsmiaNest(int a_polyindex, Osmia_Nest* a_nest)
	{
		m_PolyList[a_polyindex].ReleaseOsmiaNest(a_nest);
	}
	/** \brief Is a nest possible in this polytype */
	bool GetNestPossible(TTypesOfLandscapeElement index) { return m_PossibleNestType[int(index)]; }

	int GetNoNests(int a_polyindex) {
		return m_PolyList[a_polyindex].GetNoNests(); 
	}

	bool SanityCheck(int a_polyindex) {	 
		return m_PolyList[a_polyindex].SanityCheck();
	}

	bool CheckZeroNests() {
		for (unsigned int s = 0; s < m_PolyList.size(); s++) {
			if (!m_PolyList[s].SanityCheck2()) return false;
		}
		return true;
	}
protected:
	vector<OsmiaPolygonEntry> m_PolyList;
	/** \brief Holds a set of flags indicating whether an Osmia nest is possible (true) or not (false) */
	bool m_PossibleNestType[tole_Foobar];
};

/**
\brief
The class to handle all Osmia bee population related matters
*/
class Osmia_Population_Manager : public Population_Manager
{
public:
	// Methods
	   /** \brief Osmia_Population_Manager Constructor */
	Osmia_Population_Manager(Landscape* L);
	/** \brief Used to collect data initialisation together */
	void Init();
	/** \brief Osmia_Population_Manager Destructor */
	virtual ~Osmia_Population_Manager(void);
	/** \brief Method for creating a new individual Osmia */
	void CreateObjects(TTypeOfOsmiaLifeStages ob_type, TAnimal* pvo, struct_Osmia* data, int number);
	/** \brief Add a new egg production to the stats record */
	void RecordEggProduction(int a_eggs);
	void RecordEggLength(int a_length);
	void RecordLarvalLength(int a_length);
	void RecordPrePupaLength(int a_length);
	void RecordPupaLength(int a_length);
	void RecordInCocoonLength(int a_length);
	void RecordFemaleWeight(double a_mass) { m_FemaleWeights.push_back(a_mass); };
	/** \brief Returns flag to denore the end of prewintering, if ended it is set to true */
	bool IsEndPreWinter() { return m_PreWinteringEndFlag; }
	/** \brief Returns flag to denore the end of overwintering, if ended it is set to true */
	bool IsOverWinterEnd() { return m_OverWinterEndFlag; }
	/** \brief Checks whether a nest is possible here */
	bool IsOsmiaNestPossible(int a_polyindex) { return m_OurOsmiaNestManager.IsOsmiaNestPossible(a_polyindex); }
	/** \brief Checks whether a nest is possible here */
	bool SantityCheck(int a_polyindex) { return m_OurOsmiaNestManager.SanityCheck(a_polyindex); }
	/** \brief Creates an Osmia_Nest in the polygon referenced by the index polyindex */
	Osmia_Nest* CreateNest(int a_x, int a_y, int a_polyindex) { return m_OurOsmiaNestManager.CreateNest(a_x, a_y, a_polyindex); }
	/** \brief Return the Osmia flying weather hours */
	int GetForageHours() { return m_FlyingWeather;	}
	
	/** \brief Returns the values of provisioning parameters for a adult age */
	double GetProvisioningParams(int a_age) {
		return m_NestProvisioningParameters[a_age];
	}
	/** \brief Return the first cocoon mass for a nest give an age and female mass */
	double GetFirstCocoonProvisioningMass(int a_age, int a_massclass)
	{
		return m_FemaleCocoonMassEqns[a_massclass][a_age]-(m_exp_ZeroTo1.get() * m_FemaleCocoonMassEqns[a_massclass][a_age] * 0.6);
	}
	/** \brief Return the sex ratio for a nest give an age and female mass */
	double GetSexRatioEggsAgeMass(int a_massclass, int a_age)
	{
		return m_EggSexRatioEqns[a_massclass][a_age];
	}
	/** \brief Adds a bee to the density grid at a location */
	int AddToDensityGrid(APoint a_loc)
	{
		int index = (a_loc.m_x / 1000) + (a_loc.m_y / 1000) * m_GridExtent;
		m_FemaleDensityGrid[index]++;
		return index;
	}
	/** \brief Adds a bee to the density grid using the grid index value */
	void AddToDensityGrid(int a_index)
	{
		m_FemaleDensityGrid[a_index]++;
	}
	/** \brief Adds a bee to the density grid using the grid index value */
	void RemoveFromDensityGrid(int a_index)
	{
		m_FemaleDensityGrid[a_index]--;
	}
	/** \brief Gets the number of bees at a location */
	int GetDensity(APoint a_loc)
	{
		int index = (a_loc.m_x / 1000) + (a_loc.m_y / 1000) * m_GridExtent;
		return m_FemaleDensityGrid[index];
	}
	/** \brief Gets the number of bees at a location based on the grid index value */
	int GetDensity(int a_index)
	{
		return m_FemaleDensityGrid[a_index];
	}
	/** \brief Empties the bee density grid */
	void ClearDensityGrid()
	{
		for (int i=0; i< m_FemaleDensityGrid.size(); i++) m_FemaleDensityGrid[i] = 0;
	}
	/** \brief Returns the amount of development a pre-pupal bee will get today based on temperature */
	double GetPrePupalDevelDays() {
		return m_PrePupalDevelDaysToday;
	}
	//DEBUG//
#ifdef __OSMIATESTING
public:
	ofstream m_eggsfirstnest;
	double m_egghistogram[4][30];
	void WriteNestTestData(OsmiaNestData a_target, OsmiaNestData a_achieved);
#endif // __OSMIATESTING
protected:
	// Attributes
	/** \brief A class for holding the stats on Osmia egg production */
	SimpleStatistics m_OsmiaEggProdStats;
	/** \brief A class for holding the stats on Osmia egg stage length */
	SimpleStatistics m_EggStageLength;
	/** \brief A class for holding the stats on Osmia larval stage length*/
	SimpleStatistics m_LarvalStageLength;
	/** \brief A class for holding the stats on Osmia pupal stage length*/
	SimpleStatistics m_PrePupaStageLength;
	SimpleStatistics m_PupaStageLength;
	SimpleStatistics m_InCocoonStageLength;
	/** \brief Static instance of the probability_distribution class of distance probablilties for approx exponential probs zero to 1.0 */
	static probability_distribution  m_exp_ZeroTo1;
	/** \brief A vector for holding female emergence weights */
	vector<double> m_FemaleWeights;
	/** \brief A pointer to the pollen map object */
	PollenMap_centroidbased* m_ThePollenMap;
	/** \brief A daily value  to indicate the hours the weather is OK for Osmia adult activity */
	int m_FlyingWeather;
	/** A flag to signal the prewinter phase is over */
	bool m_PreWinteringEndFlag;
	/** A flag to signal the period between onset of wintering and March 1st */
	bool m_OverWinterEndFlag;
	/** \brief This provides the interface to the Osmia_Nests linked to the polgons 
	* - it duplicates some functionality of the Landscape but is held here to prevent bloating of landscape code/footprint for other models
	*/
	Osmia_Nest_Manager m_OurOsmiaNestManager;
	/** \brief A lookup table used to store pre-calculated nest provisioning parameters in advance since they are CPU intensive */
	double m_NestProvisioningParameters[365];
	/** \brief Holds logistic equation values per age/mass of female for egg sex ratios */
	vector<eggsexratiovsagelogisticcurvedata> m_EggSexRatioEqns;
	/** \brief Holds logistic equation values per age/mass of female for first female cocoon mass */
	vector<femalecocoonmassvsagelogisticcurvedata>  m_FemaleCocoonMassEqns;
	/** \brief holds numbers of females per 1km2 */
	vector<int> m_FemaleDensityGrid;
	/** \brief holds numbers of grid cells per row */
	int m_GridExtent;
	/** \brief An attribute used to scale the available pollen based on assumed competetion from other bee species */
	double m_PollenCompetitionsReductionScaler;
	/** \brief Holds the prepupal development rates for fast access */
	vector<double> m_PrePupalDevelRates;
	/** \brief Holds the prepupal development rates for today for fast access */
	double m_PrePupalDevelDaysToday;
	/** \brief Holds a list of pollen and nectar thresholds, one for each month */
	vector< OsmiaPollenNectarThresholds> m_PN_thresholds;

	// Methods
    /** \brief  Things to do before anything else at the start of a timestep  */
	virtual void DoFirst();
	/** \brief Things to do before the Step */
	virtual void DoBefore();
	/** \brief Things to do before the EndStep */
	virtual void DoAfter() {}
	/** \brief Things to do after the EndStep - here calculations related to summing global day degrees */
	virtual void DoLast()
	{
		int today = m_TheLandscape->SupplyDayInYear();
		if (today > September)
		{
			int day = g_date->OldDays() + g_date->DayInYear();
			double t0 = m_TheLandscape->SupplyTempPeriod(day, 1);
			/**
			* If the PostPreWinteringFlag is not set and its after Sept 1st then we need to test for the end of pre-wintering
			*/
			if (!m_PreWinteringEndFlag)
			{
				double t1 = m_TheLandscape->SupplyTempPeriod(day - 1, 1);
				double t2 = m_TheLandscape->SupplyTempPeriod(day - 2, 1);
				double t3 = m_TheLandscape->SupplyTempPeriod(day - 3, 1);
				double t4 = m_TheLandscape->SupplyTempPeriod(day - 4, 1);
				double t5 = m_TheLandscape->SupplyTempPeriod(day - 5, 1);
				/** Based on checking for a sustained and stable drop in autumn temperature */
				if (((t2 < 13.0) && (t1 < 13.0) && (t0 < 13.0)) && (((t5 - t4 > 1.0) && (t4 - t3 > 1.0)) || ((t3 < 13.0) && (t5 - t4 >= 3.0))))
				{
					m_PreWinteringEndFlag = true;
				}
			}
		}
		if (today == March)
		{
			m_OverWinterEndFlag = true;
		}
		if (today == June)
		{
			// No emergence after April so we can safely reset this flag here.
			m_PreWinteringEndFlag = false;
			m_OverWinterEndFlag = false;
		}
#ifdef __OSMIARECORDFORAGE
		double meanforage = 0.0;
		if (Osmia_Female::m_foragecount > 0) meanforage = Osmia_Female::m_foragesum / Osmia_Female::m_foragecount;
		cout << meanforage << endl;
		Osmia_Female::m_foragesum = 0.0;
		Osmia_Female::m_foragecount = 0.0;
#endif
		if (today == 364) {
			ofstream file1("OsmiaStageLengths.txt", ios::app);
			file1 << "Year: " << g_date->GetYear() << endl;
			file1 << "Mean egg stage days is:" << '\t'  << m_EggStageLength.get_meanvalue() << endl;
			file1 << "Mean larval stage days is:" << '\t' << m_LarvalStageLength.get_meanvalue() << endl;
			file1 << "Mean prepupal stage days is:" << '\t' << m_PrePupaStageLength.get_meanvalue() << endl;
			file1 << "Mean pupal stage days is:" << '\t' << m_PupaStageLength.get_meanvalue() << endl;
			file1 << "Mean incocoon stage days is:" << '\t' << m_InCocoonStageLength.get_meanvalue() << endl;
			m_EggStageLength.ClearData();
			m_LarvalStageLength.ClearData();
			m_PrePupaStageLength.ClearData();
			m_PupaStageLength.ClearData();
			m_InCocoonStageLength.ClearData();
			file1.close();
		}
	}
};

#endif