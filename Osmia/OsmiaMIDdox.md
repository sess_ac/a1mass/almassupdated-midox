Osmia MIDox                       {#Osmia_page}
=========
<h1> The ALMaSS <i>Osmia bicornis</i> Model MIDox Documentation </h1>   
Ziółkowska, E.<sup>*,1,2</sup> & Topping, C. J. <sup>2</sup> 

1.  Institute of Environmental Sciences, Jagiellonian University in Kraków, Poland
2.  Social-Ecological Systems Simulation centre, Department of Ecoscience, Aarhus University, Denmark <br>

*Corresponding author: Elżbieta Ziółkowska, e.ziolkowska@uj.edu.pl


\anchor ss1
## Introduction

The spatially-explicit agent-based model of the solitary bee _Osmia bicornis_ L. aims to provide a realistic and detailed representation of  _O. bicornis_ populations in space and time in European agricultural landscapes. Pollinator populations are declining worldwide (e.g., \ref r3 "Potts et al. 2010"; \ref r4 "Powney et al. 2019"). Various co-existing factors contribute to that decline including intensification of agricultural practices and habitat degradation, agrochemical use, but also parasites and pathogens, invasive species, poor nutrition, and climate change (\ref r1 "Goulson et al. 2015"; \ref r5 "Sánchez-Bayo and Wyckhuys 2019"). Therefore, there is a need to better understand the functioning of pollinator populations under multiple stressors at larger spatial and temporal scales. This can be achieved by a modelling approach coupling a detailed spatiotemporal landscape model with an agent-based population modelling. Solitary bees are very important for providing pollinating services in agricultural landscapes, and among them, the _Osmia_ spp. are one of the best known, and reared on a commercial scale to be used as managed alternative pollinators.

The _O. bicornis_ model was created within the ALMaSS (Animal Landscape and Man Simulation System) framework (\ref r7 "Topping et al., 2003"; \ref r8 "Topping et al. 2022 a"). Here we describe details of the ALMaSS _O. bicornis_ model implementation, following the Model Implementation Documentation with Doxygen (MIDox) format. This is the next step in designing, documenting and calibrating complicated models after the “Formal Model” format document (\ref r8 "Topping et al. 2022 b").


\anchor ss2
## Aims and Purposes

The purpose of this document is both to document the model source code, for readability and future maintenance, and also to record the decisions made when the formal model in its general algorithmic form (\ref r9 "Ziółkowska et al. 2023") was translated into code.

We first describe the model in brief and explain the scheduling of processes during the simulation. As the availability of nesting and food resources are main drivers of _O. bicornis_ population dynamics, a dynamic ALMaSS landscape model is an important part of the simulation, and behaviour of bees is intricately connected to the landscape parameters. We further provide a detailed description of implementation of _O. bicornis_ behavioural states and activities during bee’s lifetime. The documentation also discusses the important interconnections with other elements of the ALMaSS code, describes deviations from the assumptions proposed in the formal model (\ref r9 "Ziółkowska et al. 2023"), and explains model’s inputs, outputs and parameters.

The ALMaSS _O. bicornis_ model is an important step in building future landscape risk assessments for pollinators and, together with the honey-bee colony model and the _Bombus_ models (both being developed within the ALMaSS modelling framework), aims to better understand the impact of various stressors on pollinators and the services they provide.


\anchor ss3
## Model in brief

ALMaSS is implemented in C++ using object orientation. The _O. bicornis_ model is built into the existing ALMaSS implementation alongside multiple other species models (\ref r7 "Topping et al., 2003"). The model aims to represent a population of _O. bicornis_ and simulate its dynamics in a spatially-restricted landscape. During the simulation, each bee (at various stages of development) is represented as an agent in a certain behavioural state (e.g., developing, dispersing, reproducing). During its activities, an agent interacts with the landscape through e.g., temporally-dynamic weather, vegetation or floral resources.

The model considers _O. bicornis_ behaviour on a daily basis. The life-cycle of _O. bicornis_ includes six main stages: egg, larva, pre-pupa, pupa, cocooned adult and adult, and each one of them is implemented using a specific class (Osmia_Egg, Osmia_Larva, Osmia_Prepupa, Osmia_Pupa, Osmia_InCocoon, and Osmia_Female). We assume that all adult females are being fertilized; therefore, males, are not included in the model, except for the nesting and provisioning activities of the mother (see \ref ss506 "Nesting" and see \ref ss508 "Provisioning" for more details). Where possible common functions and attributes were collected together in a base class Osmia_Base, which is the ancestor class for all of the above. Osmia_Base class inherits properties from TAnimal::TAnimal of type TALMaSSObject::TALMaSSObject. For economy of memory usage, TALMaSSObject::TALMaSSObject is recycled once created, reusing dead life stages when they are available instead of creating new objects in the memory.

In common with all other ALMaSS species models, the _O. bicornis_ classes are administered by a population manager class, in this case Osmia_Population_Manager. The Osmia_Population_Manager is descended from the Population_Manager and performs the role of an auditor in the simulation. The Osmia_Population_Manager keeps track of individual bee objects (egg, larva, pre-pupa, pupa, cocooned adult, and adult female) in the model and initiates their daily behaviour via management of lists.

In addition to bee objects, also nests are spatially modelled. The _Osmia_ nests, which are implemented using the Osmia_Nest class, are administered by another population manager class, Osmia_Nest_Manager. Osmia_Nest class inherits properties from TAnimal::TAnimal of type TALMaSSObject::TALMaSSObject. As _O. bicornis_ is restricted by the availability of nesting sites, there is a maximum number of nesting sites assigned to each polygon (habitat patch) in a landscape at the beginning of each simulation. The polygon’s occupancy by nests is being tracked during the simulation and managed by the OsmiaPolygonEntry class (see \ref ss511 "Use of resources" for more details).

Two additional data classes, OsmiaForageMask and OsmiaForageMaskDetailed, are generated to speed up retrieving daily information on pollen available to a bee around its nest. This is important for the foraging algorithm managed by the Osmia_Female::Forage method (see \ref ss508 "Provisioning" for more details).


\anchor ss4

## Scheduling

When the simulation starts, the ALMaSS landscape is set up first and then passed to the Osmia_Population_Manager. The Osmia_Population_Manager::Init method is called to perform initialization of the data to run the model. This includes setting all the static variables for each of the life stages, and running the Osmia_Nest_Manager::InitOsmiaBeeNesting method to assign nest density and nesting probability to each of the polygons in a landscape (see \ref ss511 "Use of resources" for more details).

As the simulation starts on January 1st, the model is initiated with a starting population of unparasitized cocooned adult females (Osmia_InCocoon), each randomly placed inside a nest located within the landscape following density set up for each polygon. The size of initial population is controlled by the configuration variable #cfg_OsmiaStartNo.

The starting cocooned adult females are the first individuals added to the list of living and dead individuals (Osmia_Population_Manager::TheArray). Dead individuals are recycled within the model to save the cost of creating a new individual. All new individuals are added to or recycle from the list. The first cocooned adult females are the only ones randomly placed into the landscape and each of them is given information on the nest in which it is located, as well as provision mass gathered by a mother bee for its development. This provision mass is a value randomly driven in between provisions needed to generate the minimum and maximum possible bee masses in the model (defined by #cfg_OsmiaFemaleMassMin and #cfg_OsmiaFemaleMassMax configuration variables, respectively). The starting cocooned adult females then follow a normal development, emerging from the nest on spring to start interacting with the landscape as adult females (see \ref ss402 "Developmental and behavioural stages" for more details).


\anchor ss401
### Generic scheduling methods

In ALMaSS there are several basic step functions that are used throughout all the agent-based models. All of these methods are called from the Population_Manager::RunStepMethods in the normal execution flow. Below we mention those of them which are used in the _O. bicornis_ model, and provide a short description of how they are used.

#### DoFirst()

This method is the first one to run before each day step, and it runs on the level of the population manager (does not iterate between the agents). It provides an initial preparation for the run of the agents.

In the _O. bicornis_ model, the Osmia_Population_Manager::DoFirst method supplies daily mean temperature and number of possible flying hours between sunrise and sunset (see \ref ss505 "Movement" for more details), as well as applies Osmia_Nest_Manager::UpdateOsmiaNesting function, i.e., loops through all the polygons in a landscape and updates information on nesting probability. It also clears the density grid Osmia_Population_Manager::m_FemaleDensityGrid recording number of female bees per 1 km<sup>2</sup> of a landscape with Osmia_Population_Manager::ClearDensityGrid function. Finally, it assigns (for a fast access) a daily rate of development of pre-pupa Osmia_Population_Manager::m_PrePupalDevelDaysToday.

#### DoBefore()

The method is executed once for all the objects before the Step() method is initiated. In the _O. bicornis_ model the primary use of Osmia_Population_Manager::DoBefore is to remove, at the beginning of each simulation year, all the bees which survived the overwintering in larva, pre-pupa or pupa stages due to slow development.

#### Step()

This method is run iteratively for all agents and is the main place where the _O. bicornis_ behaviour is implemented. This method is responsible for moving between the developmental stages, initiating reproduction and other bee’s behaviors (see \ref ss402 "Developmental and behavioural stages" for more details). _Step_ can be called repeatedly until the step is specified as being over (TALMaSSObject::m_StepDone is set to _True_).

#### DoAfter()

The method is not used in the _O. bicornis_ model.

#### DoLast()

This method is the last one to run at the end of each day step, and it runs on the level of the population manager (does not iterate between the agents).
The Osmia_Population_Manager::DoLast method sets/resets the flags for the end of pre-wintering (Osmia_Population_Manager::m_PreWinteringEndFlag) and overwintering  (Osmia_Population_Manager::m_OverWinterEndFlag) periods depending on the time in the year and temperature conditions (see \ref ss502 "Overwintering" for more details). It also calculates, at the end of each simulation year, the mean lengths of bee’s developmental stages for the whole population in the last simulation year, based on information stored in data classes Osmia_Population_Manager::m_EggStageLength, Osmia_Population_Manager::m_LarvalStageLength, Osmia_Population_Manager::m_PrePupaStageLength, Osmia_Population_Manager::m_PupaStageLength, and Osmia_Population_Manager::m_InCocoonStageLength. After that, it resets all these data classes, so the new entries can be added for the population in the next simulation year.



\anchor ss402
### Developmental stages and behavioural states

As with all ALMaSS models the _O. bicornis_ model is based on a state machine which can be described by a state-transition diagram. Bees are in a behavioural state and due to decisions or external events move via transitions to another state where they will exhibit different behaviour.

#### Life in the nest

The following developmental stages occur in the nest:

- Eggs: implemented through Osmia_Egg. The egg could be fertilized (female-diploid progeny) or nonfertilized (male-haploid progeny). The duration of the egg stage is temperature-dependent, so it is shorter at higher temperatures (see \ref ss501 “Development in the nest”). There is a constant daily mortality rate assumed for this stage (see \ref ss509 “Mortality”). The egg develops into a larva.

  Objects of Osmia_Egg have two behavioural states:  Osmia_Egg::st_Develop and Osmia_Egg::st_Hatch.

- Larva: implemented through Osmia_Larva. Larva eats from the provision available in the cell (and defecates) up to the start of cocoon formation (spinning larva). Apart from eating, the movements of larvae are limited, thus not included in the model. The duration of the larva stage is temperature-dependent, shorter at higher temperatures (see \ref ss501 “Development in the nest”). There is a constant daily mortality rate assumed for this stage (see \ref ss509 “Mortality”). The larva develops into a prepupa.

  Objects of Osmia_Larva have two behavioural states:  Osmia_Larva::st_Develop and Osmia_Larva::st_Prepupate.

- Prepupa: implemented through Osmia_Prepupa. The cocooned larva enters prepupal dormancy more or less in synchrony with early summer. The duration of the prepupal stage is temperature-dependent (but non-linearly; see \ref ss501 “Development in the nest”). During this stage, respiration rate drops significantly, and body weight loss is minimal. There is a constant daily mortality rate assumed for this stage (see \ref ss509 “Mortality”). The prepupa develops into a pupa.

  Objects of Osmia_Prepupa have two behavioural states:  Osmia_Prepupa::st_Develop and Osmia_Prepupa::st_Pupate.

- Pupa: implemented through Osmia_Pupa. The duration of the pupa stage is temperature-dependent, shorter at higher temperatures (see \ref ss501 “Development in the nest”). There is a constant daily mortality rate assumed for this stage (see \ref ss509 “Mortality”). The pupa develops into a cocooned adult in late summer / early autumn.

  Objects of Osmia_Pupa have two behavioural states:  Osmia_Pupa::st_Develop and Osmia_Pupa::st_Emerge.

- Cocooned adult: implemented through Osmia_InCocoon. The adult overwinters in the cocoon (see \ref ss502 "Overwintering") and emerges from the nest the following spring (see \ref ss503 "Emergence from the nest"). There is an overwintering mortality applied only once, at the end of the overwintering period (before emergence; see \ref ss509 “Mortality”).

  Objects of Osmia_InCocoon have two behavioral states:  Osmia_InCocoon::st_Develop and Osmia_InCocoon::st_Emerge.

  

  \image html OsmiaFig1.png "Figure 1. State transition diagram for _O. bicornis_ showing development within the nest" width=800px

  

#### Adult females

The female adults (Osmia_Female) have several behavioral states they are going through during their lifetime:

- Osmia_Female::st_Develop - this is a general method controlling day-to-day adult female behavior. There is applied a daily background mortality rate for an adult female outside the nest (see \ref ss509 “Mortality”). There is a control mechanism for the length of a bee life. After the pre-nesting period, bee is able to switch to the reproductive behavior.
- Osmia_Female::st_ReproductiveBehaviour - this is a method controlling bee's reproduction  (see \ref ss507 "Reproduction" for more details). Bee is able to reproduce if she already has a nest (Osmia_Female::FindNestLocation; see \ref ss506 "Nesting"). If nest cannot be find in the closest vicinity of the current bee's location, a bee switches to the dispersal behavior. Having a nest, bee is first developing a 'plan' for the number of offspring, their sex ratio and amount of provisioning needed for each of the planned brood cells. The ‘plan’ may be only achieved if there are optimal (favorable) environmental conditions, i.e., if there are no restrictions in floral (pollen) resources around the nest and weather conditions allow for constant provisioning. The achieved provisions are controlled by the Osmia_Female::Forage method  (see \ref ss508 "Provisioning" for more details).
- Osmia_Female::st_Dispersal - this is a method controlling long-range movement of a bee (see \ref ss505 "Movement" for more details).



\anchor ss5

## Implementation details

\anchor ss501

### Development in the nest

Class Osmia_Egg represents the egg stage of the bee. Each egg has a sex (set to _True_ if its fertilized) and location and position in a nest in a given landscape polygon. The egg starts life at zero days old (Osmia_Egg::m_StageAge = 0) with zero day degrees (Osmia_Egg::m_AgeDegrees = 0). The development of an egg is managed by the Osmia_Egg::st_Develop method. The egg ages each day and the temperature experiences is summed each day to create a cumulative degree-day sum (Osmia_Egg::m_AgeDegrees) above a threshold temperature for development (Osmia_Egg::m_OsmiaEggDevelThreshold defined using #cfg_OsmiaEggDevelThreshold configuration variable). When this sum reaches a pre-defined number of degree-days (Osmia_Egg::m_OsmiaEggDevelTotalDD defined using #cfg_OsmiaEggDevelTotalDD configuration variable), the egg hatches to produce a larva (Osmia_Egg::st_Hatch). The Osmia_Egg::st_Hatch method passes to Osmia_Larva class information on the egg’s location in a nest, its sex, parasitoid status and mass of provisions gathered for this egg.

Class Osmia_Larva represents the larval stage of the bee in a nest. The larva eats from the provision available in the cell (and defecates) up to the start of cocoon formation. Apart from eating, the movements of larvae are limited. Functionally of Osmia_Larva class is similar to the egg. The development of a larva is managed by the Osmia_Larva::st_Develop method. The larva ages each day and the temperature experiences is summed each day to create a cumulative degree-day sum (Osmia_Larva::m_AgeDegrees) above a threshold temperature for development (Osmia_Larva::m_OsmiaLarvaDevelThreshold defined using #cfg_OsmiaLarvaDevelThreshold configuration variable). When this sum reaches a pre-defined number of degree-days (Osmia_Larva::m_OsmiaLarvaDevelTotalDD defined using #cfg_OsmiaLarvaDevelTotalDD configuration variable), the larva develops into pre-pupa (Osmia_Larva::st_Prepupate). The Osmia_Larva::st_Prepupate method passes to Osmia_Prepupa class information on the larva’s location in a nest, its sex, parasitoid status and mass of provisions gathered for that brood cell.

Class Osmia_Prepupa represents the prepupal stage of the bee in a nest. During this stage, cocooned larva is in a prepupal dormancy, i.e., respiration rate drops significantly, and body weight loss is minimal. Functionally of Osmia_Prepupa class is similar to the egg and larva. The development of a prepupa is managed by the Osmia_Prepupa::st_Develop method. The prepupa ages each day with a rate Osmia_Population_Manager::m_PrePupalDevelDaysToday depending on temperature experiences (calculated with Osmia_Population_Manager::GetPrePupalDevelDays function). When the Osmia_Prepupa::m_AgeDegrees sum reaches a pre-defined number of degree-days (Osmia_Prepupa::m_myOsmiaPrepupaDevelTotalDays), the prepupa develops into pupa (Osmia_Prepupa::st_Pupate). Osmia_Prepupa::m_myOsmiaPrepupaDevelTotalDays adds an individual variation (+/- 10%) in around a maximal developmental speed (Osmia_Base::m_OsmiaPrepupalDevelTotalDays defined using #cfg_OsmiaPrepupaDevelTotalDays configuration variable). The Osmia_Prepupa::st_Pupate method passes to the Osmia_Pupa class information on the prepupa’s location in a nest, its sex, parasitoid status and mass of provisions gathered for that brood cell.

Class Osmia_Pupa represents the pupal stage of the bee in a nest. The development of a pupa is managed by the Osmia_Pupa::st_Develop method. The pupa ages each day and the temperature experiences are summed each day to create a cumulative degree-day sum (Osmia_Pupa::m_AgeDegrees) above a threshold temperature for development (Osmia_Pupa::m_OsmiaPupaDevelThreshold defined using #cfg_OsmiaPupaDevelThreshold configuration variable). When this sum reaches a pre-defined number of degree-days (Osmia_Pupa::m_OsmiaPupaDevelTotalDD defined using #cfg_OsmiaPupaDevelTotalDD configuration variable), the pupa develops into a cocooned adult (Osmia_Pupa::st_Emerge). The Osmia_Pupa::st_Emerge method passes to the Osmia_InCocoon class information on the pupa’s location in a nest, its sex, parasitoid status and mass of provisions gathered for that brood cell.

There is a constant daily mortality rate assumed for each of the egg-to-pupa stages (see \ref ss509 "Mortality"). The cocooned adult overwinters in the nest (see \ref ss502 "Overwintering") and emerges from the nest the following spring (see \ref ss503 "Emergence").


\anchor ss502

### Overwintering

Overwintering of cocooned adult bees in the nest is managed by the Osmia_InCocoon::st_Develop method.

The overwintering of _O. bicornis_ is divided into three parts: pre-wintering, diapause, and post-diapause quiescence (Ziólkowska et al. 2023). The pre-wintering period is defined as the period from adult eclosion (end of the pupa stage) to the time threshold \f%D\f%, determined based on temperature regime. Applied rules refer to temperatures currently experienced by bees (so no predictions are required) and allow to capture sudden temperature drops below 15°C followed by 5 days with low enough temperatures (see Ziólkowska et al. 2023). Once the threshold is reached, Osmia_Population_Manager::m_PreWinteringEndFlag is set to _True_ indicating the end of pre-wintering and the start of diapause. The diapause ends on March 1st, starting the post-diapause.

The temperature conditions during pre-wintering influences the bee’s overwintering mortality (see \ref ss509 "Mortality"). The temperature conditions during the diapause influences the length of the post-diapause, i.e., timing of the bee’s emergence from the nest in spring (see \ref ss503 "Emergence from the nest").

\anchor ss503
### Emergence from the nest

Emergence from the nest is managed by the Osmia_InCocoon::st_Develop method.

The length of post-diapause, i.e., the number of days from March 1st to the beginning of emergence of bees from the nest is set as number of days with temperature above Osmia_Base::m_OsmiaInCocoonEmergenceTempThreshold defined by the following equation (Fründ et al. 2013):

_noDaysToEmergenceStart_ = int(Osmia_Base::m_OsmiaInCocoonEmergCountConst + Osmia_Base::m_OsmiaInCocoonEmergCountSlope * Osmia_InCocoon::m_AgeDegrees)

where Osmia_InCocoon::m_AgeDegrees are the accumulated degree days during the diapause, and Osmia_Base::m_OsmiaInCocoonEmergCountConst and Osmia_Base::m_OsmiaInCocoonEmergCountSlope are defined using the #cfg_OsmiaInCocoonEmergCountConst and #cfg_OsmiaInCocoonEmergCountSlope configuration variables, respectively. Osmia_Base::m_OsmiaInCocoonEmergenceTempThreshold is defined by the #cfg_OsmiaInCocoonEmergenceTempThreshold configuration variable.

The degree-days accumulation over the diapause period Osmia_InCocoon::m_AgeDegrees is calculated using a baseline temperature Osmia_Base::m_OsmiaInCocoonOverwinteringTempThreshold defined using the #cfg_OsmiaInCocoonOverwinteringTempThreshold configuration variable. The definition of the diapause period is provided in \ref ss502 "Overwintering".

The emergence distribution Osmia_Base::m_emergenceday is implemented according to data provided by A. Bednarska (personal communication; see Ziólkowska et al. 2023), as distribution of #cfg_OsmiaEmergenceProbType type and with parameters defined by #cfg_OsmiaEmergenceProbArgs. Therefore, for each cocooned adult female, a value driven from the emergence distribution is added to _noDaysToEmergenceStart_ together with a random factor Osmia_Nest::m_aspectdelay (defined separately for each nest) to obtain the number of days from March 1st to the emergence day Osmia_InCocoon::m_emergencecounter. Osmia_Nest::m_aspectdelay is a random value between 0 and 15, simulating differences in local nesting conditions (e.g., exposition to sun) which may affect the bee’s emergence time.

\anchor ss504
### Osmia mass

In the code, after emergence from the nest (see Osmia_InCocoon::st_Emerge method) each bee gets a mass property Osmia_Female::m_Mass as a direct function of the mass of provision available in the nest cell from which the bee originates. This function is a combination of two linear relationships, one predicting the mass of the cocooned adult from the mass of provision, and the second predicting the mass of adult bee from the mass of cocooned adult (see \ref r9 "Ziółkowska et al. 2023"):

*femaleAdultMass =* Osmia_Base::m_OsmiaFemaleMassFromProvMassSlope * _provisionMass_ + Osmia_Base::m_OsmiaFemaleMassFromProvMassConst

Osmia_Base::m_OsmiaFemaleMassFromProvMassSlope and Osmia_Base::m_OsmiaFemaleMassFromProvMassConst are defined using the #cfg_OsmiaFemaleMassFromProvMassConst and #cfg_OsmiaFemaleMassFromProvMassSlope configuration variables, respectively.

In the model, two classifications of adult female bees are used based on their mass: Osmia_Female::m_BeeSizeScore1 and Osmia_Female::m_BeeSizeScore2.

Osmia_Female::m_BeeSizeScore1 uses four size classes: very small (0), small (1), medium (2), and big (3) calculated according to the following equation:

Osmia_Female::m_BeeSizeScore1 = int(floor((Osmia_Female::m_Mass – Osmia_Female::m_FemaleMinMass)/((Osmia_Female::m_FemaleMaxMass – Osmia_Female::m_FemaleMinMass) / 3.0)+0.5))

where Osmia_Female::m_FemaleMinMass and Osmia_Female::m_FemaleMaxMass are the minimum and maximum possible female masses in the model. They are defined by #cfg_OsmiaFemaleMassMin and #cfg_OsmiaFemaleMassMax configuration variables, respectively. Osmia_Female::m_BeeSizeScore1 is used to calculate the number of eggs in the first nest built by a bee (see \ref ss507 "Reproduction").

Osmia_Female::m_BeeSizeScore2 uses smaller classes of bee sizes with size class controlled by the #cfg_OsmiaAdultMassCategoryStep configuration variable and is calculated according to the following equation:

Osmia_Female::m_BeeSizeScore2 = int(floor((Osmia_Female::m_Mass – Osmia_Female::m_FemaleMinMass) / #cfg_OsmiaAdultMassCategoryStep + 0.5))

Osmia_Female::m_BeeSizeScore2 is used in the calculation of the foraging and dispersal distances (see \ref ss505 "Foraging and dispersal"), the planned sex-ratio of female vs male progeny in a given nest (see \ref ss507 "Reproduction"), and the planned mass of provision needed for each egg (see \ref ss508 "Provisioning").



\anchor ss505
### Movement

In the model, two different types of movement are considered: long-range (dispersal) and short-range.

#### Dispersal

Long-range movement, i.e., dispersal, is controlled by the Osmia_Female::st_Dispersal method. Dispersal occurs when a bee is looking for a new nesting site if there are no nesting sites available in the surroundings of the previous nest or the resources available around the nest are too low to allow for nesting.

The maximum possible dispersal distance Osmia_Base::m_OsmiaFemaleR90distance is defined by the #cfg_OsmiaMaxHomingDistance configuration variable and relates to the so-called maximum homing distance _r90_. The maximum homing distance _r90_ is a distance at which 10% of individuals are able to return to the nest in translocation (‘homing’) experiments, i.e. experimental release and recapture of bees at different distances from the nest.
Each time a bee needs to disperse, a translocation distance is calculated in such a way that a value in range [0,1] is driven from a dispersal movement distribution Osmia_Base::m_dispersalmovementdistances and multiply by the maximum possible dispersal distance Osmia_Base::m_OsmiaFemaleR90distance. The dispersal movement distribution is of #cfg_OsmiaDispersalMovementProbType type, and with parameters defined by the #cfg_OsmiaDispersalMovementProbArgs configuration variable.

#### Short-range movements

Short range movements are used to evaluate nesting sites around the natal nest or current bee’s location after dispersal. They are managed by the Osmia_Female::FindNestLocation method (see \ref ss506 "Nesting" for more details). The maximum possible short-range movement distance Osmia_Base::m_OsmiaFemaleR50distance is defined by the #cfg_OsmiaTypicalHomingDistance configuration variable and relates to the so-called typical homing distance _r50_. The typical homing distance _r50_ is a distance at which 50% of individuals are able to return to the nest in translocation (‘homing’) experiments, i.e. experimental release and recapture of bees at different distances from the nest. Osmia_Base::m_OsmiaFemaleR50distance also defines the distance from the nest at which bees can forage and provision brood cells with pollen (see \ref ss508 "Provisioning" for more details).

A short-range movement is a translocation distance calculated in such a way that a value in range [0,1] is driven from a general movement distribution Osmia_Base::m_generalmovementdistances and multiplied by the maximum possible short-range movement distance Osmia_Base::m_OsmiaFemaleR50distance. The general movement distribution is of #cfg_OsmiaGeneralMovementProbType type, and with parameters defined by the #cfg_OsmiaGenerallMovementProbArgs configuration variable.

#### Flying weather conditions

We assume that _O. bicornis_ can be active outside the nest if the temperature > 13°C, and there is almost no rain (precipitation < 0.1 mm) and no strong wind (wind speed < 8 m/s) (Ziólkowska et al. 2023). We assume that flying conditions are assessed hourly, and the daily sum of hours in between sunrise and sunset fulfilling these conditions (Osmia_Population_Manager::m_FlyingWeather) is provided per each day of simulation being read from the weather input file (see \ref ss701 "Inputs and model parameters" for details).


\anchor ss506
### Nesting

#### Pre-nesting

After emergence but before reproduction, female bees leave their natal nesting sites and enter the so-called pre-nesting period. They use this time to feed and complete ovary maturation (\ref r9 "Ziółkowska et al. 2023"). In the model, the pre-nesting period is counted as a time in between emergence and start of reproduction which the bee spends outside the nest and which duration Osmia_Base::m_OsmiaFemalePrenesting is set by the #cfg_OsmiaFemalePrenestingDuration configuration variable. The bee can spend time outside the nest only if the weather conditions are favourable (see \ref ss505 "Foraging and dispersal" for details). Therefore, if there is at least one hour of favourable weather conditions after the bee’s emergence, this day is counted as a pre-nesting day (because the bee is able to leave the natal nest and feed) using Osmia_Female::m_FlyingCounter. Once Osmia_Female::m_FlyingCounter > Osmia_Base::m_OsmiaFemalePrenesting, a bee enters the reproductive behaviour (controlled by the Osmia_Female::st_ReproductiveBehaviour method).

#### Searching for a nest

Searching for a nesting site is managed by the Osmia_Female::FindNestLocation method. From its current location the bee is able to perform a maximum of Osmia_Base::m_OsmiaFindNestAttemptNo number of attempts to find a nesting location. Each attempt is a short-range movement (see \ref ss506 "Movement" for more details) in one of eight cardinal/ordinal directions (randomly chosen).

After each attempt, a bee checks if nesting is possible in the polygon (habitat patch) of its current location using OsmiaPolygonEntry::IsOsmiaNestPossible function. This function checks if the maximum number of nests for that given polygon OsmiaPolygonEntry::m_MaxOsmiaNests has not been reached yet, and if not it applies the probability of nesting OsmiaPolygonEntry::m_OsmiaNestProb (see \ref ss511 "Use of resources" for details).

If after performing Osmia_Base::m_OsmiaFindNestAttemptNo number of attempts the nesting site is still not found, a bee starts to disperse on longer distances (i.e., by switching to dispersal behaviour controlled by the Osmia_Female::st_Dispersal method). If the nesting site is found, a nest Osmia_Base::m_OurNest is being created (Osmia_Nest_Manager::CreateNest) and its location stored in the Osmia_Nest_Manager (see details below).

#### Structure of the nest

When building their nests, females of _O. bicornis_ use pre-existing cavities, which are usually tube-shaped (such as dry stems of hollow plants, or dead wood). The cells in the nest are arranged linearly in series with transverse partitions in between. In each nest, fertilized eggs (daughters) are always laid first, followed by unfertilized ones (sons) (\ref r9 "Ziółkowska et al. 2023").

Osmia nests are managed by Osmia_Nest_Manager population manager class. Each nest is defined as a linear structure of suitable diameter to allow the construction of _O. bicornis_ brood cells. The Osmia_Nest class contains a list of egg objects laid by a bee in a nest (Osmia_Nest::m_cells), as well as stores information on the nest location in a landscape (Osmia_Nest::m_x and Osmia_Nest::m_y), and reference to a polygon in which the nest is located (Osmia_Nest::m_PolyRef). As long as the nest is open (Osmia_Nest::m_isOpen), new eggs can be laid in that nest (Osmia_Nest::AddEgg). As development of egg-to-pupa stages, as well as overwintering of cocooned adults take place inside the nest, the Osmia_Nest class stores the information on objects in a nest until the emergence of adults from the nest in spring.

Osmia_Nest::RemoveCell is called each time an adult bee emerges from the nest, or a bee is killed during development before the emergence (see \ref ss509 "Mortality" for details). Osmia_Nest::KillAllSubsequentCells is called when, due to parasitoid attack, all bees from several cells starting from a given one until the entrance of the nest are killed (see \ref ss510 "Parasitism" for details). After removal of all objects from the nest, Osmia_Nest_Manager::ReleaseOsmiaNest is called to remove the nest from simulation (which entails an increase in available nesting sites in the polygon where the nest was located).

There is a minimum (Osmia_Base::m_OsmiaFemaleMinEggsPerNest) and maximum (Osmia_Base::m_OsmiaFemaleMaxEggsPerNest) number of eggs a bee can lay in a nest, and they are defined using #cfg_OsmiaMinNoEggsInNest and #cfg_OsmiaMaxNoEggsInNest configuration variables, respectively.


\anchor ss507
### Reproduction

The reproductive behaviour of a female bee is managed by the Osmia_Female::st_ReproductiveBehaviour method. A female bee enters reproductive behaviour once she finds a suitable nesting location (see \ref ss506 "Nesting" for details). For each nest to be created, a female bee first develops a ‘plan’ for laying eggs, which includes determining number of eggs in a nest, their sex ratio (i.e., number of female and male eggs), and target provisions for each of the planned eggs. The ‘plan’ may only be achieved if there are optimal (favourable) environmental conditions, i.e., if there are no restrictions in floral (pollen) resources around the nest and weather conditions allow for constant provisioning. Details on provisioning (including provisioning plan, its implementation, and foraging behaviour) are described in \ref ss508 "Provisioning".

#### Total reproductive potential

The total reproductive potential of a female bee is defined as the total number of eggs the bee can produce in her lifetime (property given to an adult female after emergence) and it depends on the bee mass. The total reproductive potential of a female bee is calculated with the Osmia_Female::CalculateEggLoad function (as a part of the Osmia_Female::Init method) as:

_totalEggsPossible_ = Osmia_Base::m_TotalNestsPossible * (0.0371 * Osmia_Base::m_Mass + 2.8399) (+/- 3 eggs)

where Osmia_Base::m_TotalNestsPossible is the maximum number of nests a female bee can produce in her lifetime (defined by the #cfg_TotalNestsPossible configuration variable); and Osmia_Base::m_Mass is the bee mass.

The number of eggs yet to be laid by a female bee Osmia_Female::m_EggsToLay is set to total reproductive potential when the bee emerges from a nest, and is reduced by one each time a female bee lays an egg. If there is no more eggs to lay (i.e., Osmia_Female::m_EggsToLay < 1), the bee dies.

#### Planned number of eggs in a nest

The planned number of eggs in the first nest built by a given bee is calculated with  Osmia_Female::PlanEggsPerNest function (as a part of the Osmia_Female::Init method) as:

_noEggsInFirstNest_ = Osmia_Base::m_OsmiaFemaleMinEggsPerNest + int(floor((0.5 + Osmia_Base::m_OsmiaFemaleMaxEggsPerNest + Osmia_Female::m_BeeSizeScore1 - Osmia_Base::m_OsmiaFemaleMinEggsPerNest)* probValue)) (+/- 2 eggs)

where Osmia_Base::m_OsmiaFemaleMinEggsPerNest and Osmia_Base::m_OsmiaFemaleMaxEggsPerNest are minimum and maximum number of eggs possible to lay in a nest (defined using #cfg_OsmiaMinNoEggsInNest and #cfg_OsmiaMaxNoEggsInNest configuration variables, respectively); Osmia_Female::m_BeeSizeScore1 is a class of bee mass (very small (0), small (1), medium (2), or big (3); see \ref ss504 "Osmia mass" for more details); and probValue is a value driven from the Osmia_Base::m_eggspernestdistribution probability distribution of #cfg_OsmiaEggsPerNestProbType type, and with parameters defined by the #cfg_OsmiaEggsPerNestProbArgs configuration variable.

The number of eggs to be laid by a female bee in a given nest Osmia_Female::m_EggsThisNest is set to number of eggs planned for the first nest when the bee emerges from a nest. The number of cells planned for consecutive nests is decreasing by a constant number of cells per nest Osmia_Base::m_OsmiaDecreaseStepNestSize defined using #cfg_OsmiaDecreaseStepNestSize configuration variable. If the number of cells planned for a nest (Osmia_Female::m_EggsThisNest) is smaller than the minimum number of eggs possible to lay in a nest (Osmia_Base::m_OsmiaFemaleMinEggsPerNest), then it will be reset to this value.

#### Planned sex ratio in a nest

The sex ratio for a given nest depends on the bee’s reproductive age (i.e. counted after the end of pre-nesting period) and the bee’s mass (\ref r9 "Ziółkowska et al. 2023"); and is calculated with the Osmia_Population_Manager::GetSexRatioEggsAgeMass function. The Osmia_Population_Manager::GetSexRatioEggsAgeMass function retrieves a value from a two-dimensional vector Osmia_Population_Manager::m_EggSexRatioEqns holding pre-calculated sex-ratio values for each of the bee mass classes defined by Osmia_Female::m_BeeSizeScore2 and for each day of a bee’s life (starting from reproductive age = 0 until the bee’s maximum lifespan Osmia_Female::m_OsmiaFemaleLifespan). For each bee mass class (mass), a sex ratio in a nest started at a given bee’s reproductive age (age) is calculated as follows.

First the sex ratio at reproductive age = 0 is calculated as:

_sexRatio (age = 0, mass)_ = _a_ * _mass_ – _b_

where slope _a_, and intercept _b_ are defined using the \#cfg_OsmiaSexRatioVsMotherMassLinear configuration variable.

Then a family of logistic curves is created, parameterized so that their maximum (which is reached at reproductive age zero) matches the mass dependence:

_sexRatio (age, mass)_ = _min_ + ((_sexRatio (age = 0, mass)_) - _min_)/(1+ exp(-_k_*(_age_-_x0_)))

where parameters _x0_, _min_, _max_, and _k_ of the logistic curve are defined using the \#cfg_OsmiaSexRatioVsMotherAgeLogistic configuration variable.

Knowing the total number of eggs planned in a given nest (Osmia_Female::m_EggsThisNest) and the sex ratio for that nest, the number of female eggs in a nest can be calculated.

#### Laying an egg

If the bee gathers enough pollen for a given brood cell and the duration of foraging activity (i.e., number of foraging hours spent) exceeds the assumed threshold (see \ref ss508 "Provisioning" for details), the bee lays an egg (Osmia_Female::LayEgg). The egg could be fertilized (female; Osmia_Egg::m_Sex is set to *True*) or non-fertilized (male; Osmia_Egg::m_Sex is set to *False*) depending on the nesting ‘plan’ and amount of provisions gathered by a bee (see \ref ss508 "Provisioning" for details).

Each newly generated egg's location in a nest and amount of provisions in a brood cell are recorded. The probability of brood cell being parasitized depends on the time the brood cell is open (Osmia_Female::m_CellOpenDays) and is calculated with Osmia_Female::CalcParaistised function (see \ref ss510 "Parasitism" for details).




\anchor ss508
### Provisioning

Provisioning of a female bee is managed by the Osmia_Female::st_ReproductiveBehaviour method. Provisioning can be only performed when weather conditions are favorable (see \ref ss505 "Foraging and dispersal" for details). The potential number of foraging hours per day is read from the weather file with Osmia_Population_Manager::GetForageHours function. Osmia_Base::m_foragehours is a variable holding the number of available foraging hours left in a day. It is set to the potential number of foraging hours per day when a bee switches to reproductive behavior for the first time that day. Osmia_Female::m_CellCarryOver variable keeps track of any part-time cell construction hours. It is assumed that a certain, constant time of 4 h and 30 min is needed for finishing a brood cell (this includes 3 h and 40 min for provisioning and 50 min for building; \ref r10 "Raw 1972"). Each time a bee lays an egg (Osmia_female::LayEgg), Osmia_Female::m_CellCarryOver is reduced by this time, so at the end of the day it is known if the bee already started building a new brood cell and needs to finish it the next day.

#### Provisioning plan for a nest

After finding a nesting location, bee creates a nesting ‘plan’, part of which is a provisioning plan for each egg planned in that nest. The list of planned (target) provisions for a nest is stored in a list Osmia_Female::m_NestProvisioningPlan. The amount of provisions planned for a given egg in a nest depends on the egg’s sex (Osmia_Egg::m_Sex), and more provisions are required to lay a fertilized than a non-fertilized egg. There is a minimum amount of provisions required to lay a fertilized (Osmia_Base::m_FemaleMinTargetProvisionMass) and non-fertilized (Osmia_Base::m_MaleMinTargetProvisionMass) egg, as well as maximum allowed provisions for a fertilized (Osmia_Base::m_FemaleMaxTargetProvisionMass) and non-fertilized (Osmia_Base::m_MaleMaxTargetProvisionMass) egg.

The planned amount of provisions for fertilized eggs (female offspring) is calculated as follows. The amount of provisions needed for the first female cell in the first nest of a bee depends on the bee’s reproductive age (i.e. counted after the end of pre-nesting period) and the bee’s mass (\ref r9 "Ziółkowska et al. 2023"); and is calculated with the Osmia_Population_Manager::GetFirstCocoonProvisioningMass function.

The Osmia_Population_Manager::GetFirstCocoonProvisioningMass function first retrieves a value from a two-dimensional vector Osmia_Population_Manager::m_FemaleCocoonMassEqns and reduces it by the inclusion of an exponentially falling distribution of probability (Osmia_Population_Manager::m_exp_ZeroTo1). Osmia_Population_Manager::m_FemaleCocoonMassEqns vector holds pre-calculated target provisions for the first female cell in the first nest for each of the bee mass classes defined by Osmia_Female::m_BeeSizeScore2 and for each day of bee’s life (starting from reproductive age = 0 until the bee’s maximum lifespan Osmia_Female::m_OsmiaFemaleLifespan). For each bee mass class (*mass*), target provisions for the first female cell in the first nest started at a given bee’s reproductive age (*age*) are calculated as follows (\ref r9 "Ziółkowska et al. 2023").

First the target mass for first cocooned adult female in the first nest at bee’s reproductive age = 0 is calculated as:

_firstFemaleCocoonedMass (age=0, mass)_= _avgFemaleCocoonedMass_ + #cfg_Osmia_LifetimeCocoonMassLoss / 2

where #cfg_Osmia_LifetimeCocoonMassLoss is a configuration variable defining the total difference in cocoon mass from first to last cocoon produced in a bee’s lifetime; and *avgFemaleCocoonedMass* is defined as:

_avgFemaleCocoonedMass (mass)_ = _a_ * _mass_ + _b_

where slope _a_, and intercept _b_ are defined using the #Cfg_OsmiaFemaleCocoonMassVsMotherMassLinear configuration variable.

Then a family of logistic curves is created, parameterized so that their maximum (which is reached at reproductive age zero) matches the mass dependence:

_firstFemaleCocoonedMass (age, mass)_ = _min_+ ((_firstFemaleCocoonedMass (age = 0, mass)_) - _min_)/(1+ exp(-_k_*(_age_-_x0_)))

where parameters _x0_, _min_, _max_ and _k_ of the logistic curve are defined using the #Cfg_OsmiaFemaleCocoonMassVsMotherAgeLogistic configuration variable.

Then target provisioning masses (returned by Osmia_Population_Manager::m_FemaleCocoonMassEqns) are calculated from masses of cocooned adults as follows:

_firstFemaleProvisioningMass (age, mass)_ = 40 + (#cfg_OsmiaProvMassFromCocoonMass *_firstFemaleCocoonedMass (age, mass)_)

where #cfg_OsmiaProvMassFromCocoonMass is a configuration value defining the linear relationship between mass of cocooned adult and mass of provisions. A rescaling factor of 40 was added to the above equation in the process of calibration (see \ref ss9 "Calibration" for details).

The mass of provisions needed for the consecutive female cells is decreasing at a fixed rate Osmia_Base::m_TotalProvisioningMassLoss (+/- Osmia_Base::m_TotalProvisioningMassLossRange). The target provisions for all the male eggs planned in a nest are set to the minimum amount of provisions required to lay a non-fertilized (Osmia_Base::m_MaleMinTargetProvisionMass) egg.

#### Execution of the nesting 'plan' 

The maximum possible amount of pollen which bee is able to retrieve from the landscape (_provisioning_mg_) is calculated with Osmia_Female::Forage() algorithm (see \ref ss511 "Use of resources" for details). It is further rescaled to obtain the amount of pollen currently provisioned in a brood cell (Osmia_Female::m_CurrentProvisioning):

Osmia_Female::m_CurrentProvisioning += _provisioning_mg_ * Osmia_Female::m_foragehours * 0.815

where Osmia_Female::m_foragehours is the number of available forage hours left in a day, and 0.815 is a rescaling factor indicating the proportion of foraging hours spend on provisioning (i.e., according to \ref r10 “Raw 1972”, bee is spending on average 3h and 40 min on provisioning out of 4 h and 30 min needed for building a brood cell).

There are two stopping rules for the egg provisioning: (1) maximum time the brood cell is open (Osmia_Base::m_MaximumCellConstructionTime) is reached, or (2) target provisioning is achieved. There is also a test for giving up a nest location if pollen availability is too low. After one of the stopping rules is triggered, the bee checks the amount of gathered provisions and decides about the sex of the egg to be laid.

The egg is fertilized (female; Osmia_Egg::m_Sex is set to *True*) if bee was targeting to lay a fertilized egg (according to the nesting ‘plan’) and gathered provisions (Osmia_Female::m_CurrentProvisioning) are bigger than the minimum needed for a female (Osmia_Base::m_FemaleMinTargetProvisionMass). If bee was targeting to lay a fertilized egg but was not able to gather enough provisions, the non-fertilized egg (male; Osmia_Egg::m_Sex is set to *False*) is produced. However, because mass distributions for *O. bicornis* females and males could partially overlap, the provisioning for that brood cell is truncated to maximum allowed for a male (Osmia_Base::m_MaleMaxTargetProvisionMass). Otherwise, the non-fertilized egg (male; Osmia_Egg::m_Sex is set to *False*) is produced if gathered provisions are bigger than the minimum required for a male (Osmia_Base::m_MaleMinTargetProvisionMass).



\anchor ss509
### Mortality

In the simulation, the bees die because they have finished laying all the eggs, reached the end of their life span, or from external events. The number of eggs left to lay is controlled by the Osmia_Female::st_ReproductiveBehaviour method. The bee’s lifespan Osmia_Female::m_OsmiaFemaleLifespan is defined by the #cfg_OsmiaFemaleLifespan configuration variable, and the number of days left to live is controlled by the Osmia_Female::st_Develop method. The mortality caused by external, unknown factors is defined separately for the egg-to-pupa stages, overwintering period, and adult bees outside the nest (see below). In addition, bees may be killed by parasitoids that live in their nests, and this is described in more details in \ref ss510 "Parasitism".

#### Mortality of egg to pupa stages

There is a constant daily mortality rate assumed for each of the egg-to-pupa stages (Osmia_Base::m_DailyDevelopmentMortEggs, Osmia_Base::m_DailyDevelopmentMortLarvae, Osmia_Base::m_DailyDevelopmentMortPrepupae, Osmia_Base::m_DailyDevelopmentMortPupae), defined using the following configuration variables: #cfg_OsmiaEggDailyMORT, #cfg_OsmiaLarvaDailyMORT, #cfg_OsmiaPrepupaDailyMORT, #cfg_OsmiaPupaDailyMORT.

#### Overwintering mortality

The overwintering mortality is defined in relation to degree-days accumulation over the pre-wintering period Osmia_InCocoon::m_DDPrewinter based on results obtained for *O. lignaria* males reared in natural conditions in North Ogden, Utah, USA, flying in April – May (Sgolastra et al., 2011). As no other relevant field data was available for females, we used data for males, assuming that it is applicable to both sexes:

<br>*overwintering_mortality =* Osmia_InCocoon::m_OsmiaInCocoonWinterMortSlope * Osmia_InCocoon::m_DDPrewinter *–* Osmia_InCocoon::m_OsmiaInCocoonWinterMortConst<br>

Osmia_InCocoon::m_OsmiaInCocoonWinterMortConst and Osmia_InCocoon::m_OsmiaInCocoonWinterMortSlope are defined using #cfg_OsmiaInCocoonWinterMortConst and #cfg_OsmiaInCocoonWinterMortSlope configuration variables, respectively.

The degree-days accumulation over the pre-wintering period Osmia_InCocoon::m_DDPrewinter is calculated using a baseline temperature Osmia_InCocoon::m_OsmiaInCocoonPrewinteringTempThreshold defined using #cfg_OsmiaInCocoonPrewinteringTempThreshold configuration variable. The definition of pre-wintering period is provided in \ref ss502 "Overwintering".

The overwintering mortality is applied only once, at the end of the overwintering period (before emergence) using the Osmia_InCocoon::WinterMortality function (applied within the Osmia_InCocoon::st_Develop method).

#### Background mortality outside the nest

There is a daily background mortality rate Osmia_Female::m_OsmiaFemaleBckMort for an adult Osmia female outside the nest defined using #cfg_OsmiaFemaleBckMort. Osmia_Female::m_OsmiaFemaleBckMort is applied within the Osmia_Female::st_Develop method.


\anchor ss510
### Parasitism

Parasitism is managed by the Osmia_Female::CalcParaistised method. Only the open-cell parasitism is included. The probability that a brood cell is being parasitized depends on the time the cell is open :

_open_cell_parasitism_prob_ = Osmia_Base::*m_ParasitismProbToTimeCellOpen * (Osmia_Female::m_CellOpenDays * 24)

If the cell is parasitized, it can be in the form of cleptoparasitism by flies and wasps (e.g., *C. indagator* drosophilid fly and Sapygid wasps; TTypeOfOsmiaParasitoids::topara_Cleptoparasite) or parasitism by flies (e.g., *A. anthrax* bombylid fly; TTypeOfOsmiaParasitoids::topara_Bombylid), with a given probability defined by Osmia_Base::m_BombylidProbability defined using the #cfg_OsmiaBombylidProb configuration variable.

Parasitism by flies is influencing the offspring in two ways: (1) directly, by parasitising on the bee larva that causes its death (Osmia_Nest::RemoveCell), and (2) indirectly, when the parasite is moving towards the nest entrance killing all bee larvae as it leaves the nest (Osmia_Nest::KillAllSubsequentCells). Cleptoparasitism by flies and wasps causes death of a larva only in the cell being parasitized (Osmia_Nest::RemoveCell).




\anchor ss511
### Use of resources

#### Evaluation of nesting resources

The list of nests located in a given polygon in a landscape is managed by the OsmiaPolygonEntry class. The class also holds information on associated polygon’s properties and nest density control variables, i.e., polygon area (OsmiaPolygonEntry::m_Area), reference index (OsmiaPolygonEntry::m_Polyindex) used by the Landscape::Landscape and Osmia_Nest_Manager, current number of nests in that polygon (OsmiaPolygonEntry::m_CurrentOsmiaNests), maximum number of nests possible in that polygon (OsmiaPolygonEntry::m_MaxOsmiaNests), and probability of building a nest in that polygon (OsmiaPolygonEntry::m_OsmiaNestProb). 

At the beginning of each simulation, Osmia_Nest_Manager::InitOsmiaBeeNesting reads the input text file #cfg_OsmiaNestByLE_Datafile describing minimum and maximum density of _O. bicornis_ nests per m<sup>2</sup> of a given type of landscape element (TOLE). Then for each individual polygon in a landscape, a polygon nest density value is driven from an uniform distribution with parameters set by minimum and maximum nest density possible for a TOLE type of that polygon. This polygon nest density is further multiplied by the polygon’s area using OsmiaPolygonEntry::SetMaxOsmiaNests function to calculate a maximum number of _O. bicornis_ nests OsmiaPolygonEntry::m_MaxOsmiaNests possible to be generated in that polygon.

After removal of all objects from the nest, Osmia_Nest::ReleaseOsmiaNest is called to remove the nest from simulation (which entails an increase in available nesting sites in the polygon where the nest was located).

#### Evaluation of food resources

We assume that adult bees are not limited by their food requirements; therefore, the _O. bicornis_ model only evaluates pollen resources for offspring. The _O. bicornis_ model uses the ALMaSS nectar and pollen model, i.e., has daily access to the information on nectar and pollen produced by different landscape element (l_map_nectarpolen) and vegetation (l_map_tov_nectarpolen) types. Thus, on each day of the simulation, a bee is be able to sample the landscape to get information on the amount of pollen produced in a given resource patch (in mg per m<sup>2</sup>). The competition from other pollinators will be modelled as a global decrease in floral resources available to _O. bicornis_ (Osmia_Population_Manager::m_PollenCompetitionsReductionScaler defined using #\#cfg_OsmiaDensityDependentPollenRemovalConst configuration variable).

We use a radial-spoke pattern of searching from the nest location. The foraging is managed by the Osmia_Female::Forage method. When the bee needs to find a forage location, it will search in eight directions (N, NE, E, SE, S; SW, W, NW) and select the best forage location encountered within the maximum forage range (OsmiaForageMaskDetailed::m_maxdistance). Since the pattern is radial with the spokes evenly distributed, the chance of encountering a patch of pollen decreases with distance from the nest, and the likelihood of encounters near the nest is very high.

The bee will continue to forage from the patch found as long as the pollen return rate is high enough (threshold parameter Osmia_Female::m_pollengiveupthreshold defined using #cfg_OsmiaPollenGiveUpThreshold configuration variable), and a new nest is not started. If either condition is triggered, a new search will be initiated. This algorithm does not map to any measurable biological parameters except forage distance; hence the step size (OsmiaForageMaskDetailed::m_step), acceptance level (Osmia_Female::m_pollengiveupthreshold), and frequency of search (Osmia_Female::m_ForageSteps defined using the #cfg_OsmiaForageSteps configuration variable) are fitted parameters.




\anchor ss6
## Interconnections

The _O. bicornis_ model connects to various established elements of the ALMaSS code through the Landscape. Landscape gives access to all the polygons (LE) within the landscape. These polygons give access to \ref TTypesOfLandscapeElement, Calendar, PollenNectarData and Landscape::SupplyPesticide models.




\anchor ss7
## I/O, variables and scales

\anchor ss701

### Inputs

ALMaSS models need a BatchALMaSS.ini, TI_inifile.ini, TIALMaSSConfig.cfg, maps of the area simulated, weather, farm rotation files for each farm type. The model also can be set to have a number of probe files, one for each life stage (six for the _O. bicornis_ model). The two .ini files specify which probe files are used, the duration of the model run and in the case of the BatchALMaSS.ini the species model that is being run (_O. bicornis_ is species 10).

The _O. bicornis_ model uses the ALMaSS nectar and pollen model and needs two files that define how nectar and pollen change for the LE (l_map_nectarpolen) and vegetation (l_map_tov_nectarpolen). To control the number of nests per patch of habitat, the _O. bicornis_ model needs a file defining the minimum and maximum number of nests per square meter for each habitat type (#cfg_OsmiaNestByLE_Datafile).

Our model uses a configuration file that includes all the previously mentioned input files, output files as parameters along with the specific ecological parameters of the *O. bicornis* model. These parameters have their predefined values, but could be altered for each simulation. The list of the most essential parameters, their default values, name of the constants they are stored in and the variable name to be used in the config file is provided in \ref _t2 "Table 2".



\anchor _t2 Table 2. Model parameters grouped per behavioral type

| **Configuration variable**                    | **Description**                                              | **Default value**                                     | **Units**  | **Variable  name in the configuration file** | **Source**                                                |
| :-------------------------------------------- | ------------------------------------------------------------ | ----------------------------------------------------- | ---------- | -------------------------------------------- | --------------------------------------------------------- |
| #cfg_OsmiaStartNo                             | Starting number of cocooned adults                           | 50000                                                 | -          | OSMIA_STARTNOS                               | Assumed                                                   |
| #cfg_OsmiaFemaleLifespan                      | Maximum  lifespan                                            | 60                                                    | day        | OSMIA_LIFESPAN                               | Assumed                                                   |
| \ref ss501 "Development in the nest"          |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaEggDevelTotalDD                     | Number of degree days (above the developmental threshold)  needed for egg to hatch | 86                                                    | degree day | OSMIA_EGGDEVELDD                             | Changed in  calibration                                   |
| #cfg_OsmiaEggDevelThreshold                   | Temperature developmental threshold for egg development      | 0                                                     | °C         | OSMIA_EGGDEVELTHRESHOLD                      | Changed in  calibration                                   |
| #cfg_OsmiaLarvaDevelTotalDD                   | Number of degree days (above the developmental threshold)  needed for larva to develop into prepupa | 422                                                   | degree day | OSMIA_LARVADEVELDD                           | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaLarvaDevelThreshold                 | Temperature developmental threshold for larva  development   | 4.5                                                   | °C         | OSMIA_LARVADEVELTHRESHOLD                    | Changed in  calibration                                   |
| #cfg_OsmiaPrepupaDevelTotalDays               | Maximal (reached at optimal temperature) developmental  speed (in days) for prepupal stage | 45                                                    | day        | OSMIA_PREPUPADEVELDAYS                       | Changed in  calibration                                   |
| #cfg_OsmiaPupaDevelTotalDD                    | Number of degree days (above the developmental threshold)  needed for pupa to develop into cocooned adult | 570                                                   | degree day | OSMIA_PUPADEVELDD                            | Changed in  calibration                                   |
| #cfg_OsmiaPupaDevelThreshold                  | Temperature developmental threshold for pupa development     | 1.1                                                   | °C         | OSMIA_PUPADEVELTHRESHOLD                     | Changed in  calibration                                   |
| \ref ss502 "Overwintering"                    |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaInCocoonOverwinteringTempThreshold  | Temperature developmental threshold for overwintering  development | 0                                                     | °C         | OSMIA_INCOCOONOVERWINTERINGTEMPTHRESHOLD     | Assumed                                                   |
| #cfg_OsmiaInCocoonPrewinteringTempThreshold   | Temperature threshold for end of pre-wintering / onset  of overwintering | 15                                                    | °C         | OSMIA_INCOCOONPREWINTERINGTEMPTHRESHOLD      | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| \ref ss503 "Emergence from the nest"          |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaInCocoonEmergCountConst             | Constant term in emergence counter (counting days left  to emergence) equation for cocooned adult | 35.4819                                               | -          | OSMIA_INCOCOONEMERGENCECOUNTERCONST          | Changed in calibration                                    |
| #cfg_OsmiaInCocoonEmergCountSlope             | Coefficient in emergence counter (counting days left to  emergence) equation for cocooned adult | -0.0147                                               | -          | OSMIA_INCOCOONEMERGENCECOUNTERSLOPE          | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaInCocoonEmergenceTempThreshold      | Temperature threshold for counting days left to  emergence   | 12                                                    | °C         | OSMIA_INCOCOONEMERGENCETEMPTHRESHOLD         | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaEmergenceProbType                   | Type of the emergence distribution                           | DISCRETE                                              | -          | OSMIA_EMERGENCEPROBTYPE                      | Based on field data, see \ref r9 "Ziółkowska et al. 2023" |
| #cfg_OsmiaEmergenceProbArgs                   | Parameters for the mergence distribution                     | 8 7 9 24 20 8  6 5 5 4 4                              | -          | OSMIA_EMERGENCEPROBARGS                      | Based on field data, see \ref r9 "Ziółkowska et al. 2023" |
| \ref ss504 "Osmia mass"                       |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaMaleMassMin                         | Minimum possible mass for an adult male                      | 88                                                    | mg         | OSMIA_MINMALEMASS                            | Based on field data, see \ref r9 "Ziółkowska et al. 2023" |
| #cfg_OsmiaMaleMassMax                         | Maximum possible mass for an adult male                      | 105                                                   | mg         | OSMIA_MAXMALEMASS                            | Based on field data, see \ref r9 "Ziółkowska et al. 2023" |
| #cfg_OsmiaFemaleMassMin                       | Minimum possible mass for an adult female                    | 25                                                    | mg         | OSMIA_MINFEMALEMASS                          | Based on field data, see \ref r9 "Ziółkowska et al. 2023" |
| #cfg_OsmiaFemaleMassMax                       | Maximum possible mass for an adult female                    | 200                                                   | mg         | OSMIA_MAXFEMALEMASS                          | Assumed                                                   |
| #cfg_OsmiaAdultMassCategoryStep               | The size class step for female mass                          | 10                                                    | mg         | OSMIA_ADULTMASSCLASSSTEP                     | Assumed                                                   |
| #cfg_OsmiaCocoonMassFromProvMass              | Scaling parameter to recalculate cocoon mass from  provision mass | 1.0 / 3.247                                           | -          | OSMIAS_COCOONTOPROVISIONING                  | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaProvMassFromCocoonMass              | Scaling parameter to recalculate provision mass from target  cocoon mass | 3.247                                                 | -          | OSMIAS_PROVISIONINGTOCOCOON                  | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaFemaleMassFromProvMassConst         | Constant term in equation for calculation of adult mass  from mass of provisions | 4.00                                                  | -          | OSMIA_FEMALEMASSFROMPROVMASSCONST            | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaFemaleMassFromProvMassSlope         | Coefficient in equation for calculation of adult mass  from mass of provisions | 0.25                                                  | -          | OSMIA_FEMALEMASSFROMPROVMASSSLOPE            | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| \ref ss505 "Movement"                         |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaTypicalHomingDistance               | Maximum distance for short-range movements                   | 660                                                   | m          | OSMIA_TYPICALHOMINGDISTANCE                  | \ref r9 "Ziółkowska et al. 2023"                          |
| #cfg_OsmiaMaxHomingDistance                   | Maximum  distance for dispersal                              | 1430                                                  | m          | OSMIA_MAXHOMINGDISTANCE                      | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaDispersalMovementProbType           | Type of dispersal movement distribution                      | BETA                                                  | -          | OSMIA_DISPMOVPROBTYPE                        | Assumed                                                   |
| #cfg_OsmiaDispersalMovementProbArgs           | Parameters for dispersal movement distribution               | 10, 5                                                 | -          | OSMIA_DISPMOVPROBARGS                        | Assumed                                                   |
| #cfg_OsmiaGeneralMovementProbType             | Type of general movement distribution                        | BETA                                                  | -          | OSMIA_GENMOVPROBTYPE                         | Assumed                                                   |
| #cfg_OsmiaGenerallMovementProbArgs            | Parameters for general movement distribution                 | 10, 5                                                 | -          | OSMIA_GENMOVPROBARGS                         | Assumed                                                   |
| \ref ss506 "Nesting"                          |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaFemalePrenestingDuration            | Duration of prenesting                                       | 2                                                     | day        | OSMIA_PRENESTINGDURATION                     | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaMinNoEggsInNest                     | Mimimum number of eggs planned for a nest                    | 3                                                     | -          | OSMIA_MINNOEGGSINNEST                        | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaMaxNoEggsInNest                     | Maximum number of eggs planned for a nest                    | 30                                                    | -          | OSMIA_MAXNOEGGSINNEST                        | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaDecreaseStepNestSize                | Decrease step in number of eggs planned for consecutive  nests | 2                                                     | -          | OSMIA_DECREASESTEPNESTSIZE                   | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_TotalNestsPossible                       | Maximum number of nests possible for a bee                   | 5                                                     | -          | OSMIA_TOTALNESTSPOSSIBLE                     | Assumed                                                   |
| #cfg_OsmiaFemaleFindNestAttemptNo             | Number of nest finding attempts                              | 20                                                    | -          | OSMIA_FEMALEFINDNESTATTEMPTNO                | Assumed                                                   |
| \ref ss507 "Reproduction"                     |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaSexRatioVsMotherMassLinear          | Array of parameters for the Osmia sex ratio vs mothers  mass linear equation | 0.0055,  -0.1025                                      | -          | OSMIA_SEXRATIOVSMOTHERSMASSLINEAR            | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaSexRatioVsMotherAgeLogistic         | Array of parameters for the Osmia sex ratio vs mothers  age logistic equation | 14.90257909, 0.09141286, 0.6031729, -0.39213001       | -          | OSMIA_SEXRATIOVSMOTHERSAGELOGISTIC           | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #Cfg_OsmiaFemaleCocoonMassVsMotherMassLinear  | Array of parameters for the Osmia female first cocoon  mass vs mothers mass linear equation | 0.3, 65.1                                             | -          | OSMIA_FEMALECOCOONMASSVSMOTHERSMASSLINEAR    | Changed in  calibration                                   |
| #Cfg_OsmiaFemaleCocoonMassVsMotherAgeLogistic | Array of parameters for the Osmia female cocoon mass vs  mothers age logistic equation | 18.04087868,  104.19820591, 133.74150303, -0.17686981 | -          | OSMIA_FEMALECOCOONMASSVSMOTHERSAGELOGISTIC   | \ref r9 "Ziółkowska et al. 2023"                          |
| #cfg_Osmia_LifetimeCocoonMassLoss             | Total difference in cocoon mass from first to last  cocoon   | 30                                                    | -          | OSMIA_LIFETIMECOCOONMASSLOSS                 | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaEggsPerNestProbType                 | Type of the probability distribution for number of eggs  in the first nest | BETA                                                  | -          | OSMIA_EGGSPERNESTPROBYPE                     | Assumed                                                   |
| #cfg_OsmiaEggsPerNestProbArgs                 | Parameters for the probability distribution for number  of eggs in the first nest | 1, 4                                                  | -          | OSMIA_EGGSPERNESTPROBARGS                    | Assumed                                                   |
| \ref ss508 "Provisioning"                     |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaForageSteps                         | Number of steps between nest and maximum foraging  distance  | 20                                                    | -          | OSMIA_FORAGESTEPS                            | Assumed                                                   |
| #cfg_MaleMinTargetProvisionMass               | Minimum amount of pollen needed to provision a male cell     | 10                                                    | mg         | OSMIA_MALEMINTARGETPROVISIONMASS             | Based on field data, see \ref r9 "Ziółkowska et al. 2023" |
| #cfg_MinimumCellConstructionTime              | Minimum time to construct a cell                             | 1                                                     | day        | OSMIA_MINCELLCONSTRUCTTIME                   | Assumed                                                   |
| #cfg_MaximumCellConstructionTime              | Maximum time allowed to construct a cell                     | 4                                                     | day        | OSMIA_MAXCELLCONSTRUCTTIME                   | Assumed                                                   |
| #cfg_OsmiaPollenGiveUpThreshold               | Change in proportion pollen before a new patch is  selected  | 0.75                                                  | -          | OSMIA_POLLENGIVEUPTHRESHOLD                  | Assumed                                                   |
| #cfg_OsmiaPollenGiveUpReturn                  |                                                              | 0.75                                                  | -          | OSMIA_POLLENGIVEUPRETURN                     | Assumed                                                   |
| #cfg_PollenScoreToMg                          | Conversion rate from pollen availability score to mg  pollen provisioned per day per bee | 0.8                                                   | -          | OSMIA_POLLENSCORETOMG                        | Calibrated                                                |
| #cfg_OsmiaMaxPollen                           | Cap on the amount of pollen provision possible to bring  back to the nest (per foraging hour) | 2.5                                                   | mg         | OSMIA_MAXPOLLEN                              | Calibrated                                                |
| \ref ss509 "Mortality"                        |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaFemaleBckMort                       | Daily probability of mortality for an adult female           | 0.02                                                  | -          | OSMIA_FEMALEBACKMORT                         | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaEggDailyMORT                        | Daily unspecified mortality for eggs                         | 0.0014                                                | -          | OSMIA_EGGDAILYMORT                           | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaLarvaDailyMORT                      | Daily unspecified mortality for larvae                       | 0.0014                                                | -          | OSMIA_LARVADAILYMORT                         | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaPrepupaDailyMORT                    | Daily unspecified mortality for prepupae                     | 0.003                                                 | -          | OSMIA_PREPUPADAILYMORT                       | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaPupaDailyMORT                       | Daily unspecified mortality for pupae                        | 0.003                                                 | -          | OSMIA_PUPADAILYMORT                          | \ref r9 "Ziółkowska et al. 2023"                          |
| #cfg_OsmiaInCocoonWinterMortConst             | Constant term in winter mortality equation for cocooned adult | -4.63                                                 | -          | OSMIA_INCOCOONWINTERMORTCONST                | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaInCocoonWinterMortSlope             | Coefficient in winter mortality equation for cocooned  adult | 0.05                                                  | -          | OSMIA_INCOCOONWINTERMORTSLOPE                | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| \ref ss510 "Parasitism"                       |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaBombylidProb                        | Probability that a parasitoid is Bombylid                    | 0.5                                                   |            | OSMIA_BOMBYLIDPROB                           | Assumed                                                   |
| #cfg_OsmiaParasitismProbToTimeCellOpen        | Conversion rate from time the cell is open to open cell  parasitism probability | 0.0075                                                |            | OSMIA_PARASITISMPROBTOTIMECELLOPEN           | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| \ref ss511 "Use of resources"                 |                                                              |                                                       |            |                                              |                                                           |
| #cfg_OsmiaNestByLE_Datafile                   | Input file for Osmia nest density per landscape element  type (TOLE) | OsmiaNestsByHabitat.txt                               | -          | OSMIA_NESTBYLEDATAFILE                       | Literature based, see \ref r9 "Ziółkowska et al. 2023"    |
| #cfg_OsmiaDensityDependentPollenRemovalConst  | Proportion of pollen available to Osmia after use of  resources by other pollinators; 1 means no competition | 1                                                     | -          | OSMIADENSITYDENPENDENTPOLLENREMOVALCONST     | Assumed                                                   |



\anchor ss702

### Outputs

ALMaSS is a flexible simulation environment that is capable of providing different types of outputs when needed. Outside the calibration mode this model is configured to provide basic outputs common to the other models, namely the probe file, that allows to simulate the sampling procedure and returns the number of individuals in question at specific predefined locations or in general across the whole landscape. In addition this model provides the so called AOR probe, the probe that combines the abundance and occupancy metrics (/ref r2 "Høye et al. 2012") at the given date of the year.



\anchor ss703

### State variables

In addition to a standard state variables (such as spatial location and alive or dead status) that are shared among all of the ALMaSS objects, the class objects of this model have some distinct state variables of special importance that are provided in \ref _t2 “Table 3”.



\anchor _t3 Table 3. Most important state variables that change throughout the model

| **Variable name**                                            | **Units**          | **Description**                                              |
| ------------------------------------------------------------ | ------------------ | ------------------------------------------------------------ |
| _General_                                                    |                    |                                                              |
| Osmia_Base::m_Age                                            | day                | Age in days                                                  |
| Osmia_Base::m_Mass                                           | mg                 | Depending on life-stage, mass of a bee or provisioned  pollen |
| Osmia_Base::m_CurrentOState                                  | -                  | Current  behavioural state                                   |
| Osmia_Base::m_foragehours                                    | hour               | Number of available forage hours left in a day               |
| _Egg-to-pupa_                                                |                    |                                                              |
| Osmia_Egg::m_AgeDegrees<br>Osmia_Larva::m_AgeDegrees<br>Osmia_Prepupa::m_AgeDegrees<br>Osmia_Pupa::m_AgeDegrees | degree day         | Age in degree days for development                           |
| Osmia_Egg::m_Sex<br>Osmia_Larva::m_Sex<br>Osmia_Prepupa::m_Sex<br>Osmia_Pupa::m_Sex | binary             | Sex; fertilized egg = female = true                          |
| Osmia_Egg::m_StageAge<br>Osmia_Larva::m_StageAge<br>Osmia_Prepupa::m_StageAge<br>Osmia_Pupa::m_StageAge | day                | Age when the stage was initiated                             |
| _Cocooned adult_                                             |                    |                                                              |
| Osmia_InCocoon::m_AgeDegrees                                 | degree day         | Age in degree days for development                           |
| Osmia_InCocoon::m_Sex                                        | binary             | Sex; fertilized egg = female = true                          |
| Osmia_InCocoon::m_StageAge                                   | day                | Age when the stage was initiated                             |
| Osmia_InCocoon::m_DDPrewinter                                | degree day         | Number of degrees days calculated for the prewintering  period |
| _Female adult_                                               |                    |                                                              |
| Osmia_Female::m_EmergeAge                                    | day                | Number of days of post emergence life                        |
| Osmia_Female::m_CurrentNestLoc                               | -                  | The location of the current nest, holds -1 in m_x when  no nest |
| Osmia_Female::m_EggsToLay                                    | -                  | Number of all eggs yet to lay by a female                    |
| Osmia_Female::m_EggsThisNest                                 | -                  | The planned number of eggs for this nest                     |
| Osmia_Female::m_CellOpenDays                                 | day                | The number of days a cell is open                            |
| Osmia_Female::m_FlyingCounter                                | day                | The number of flying days (i.e., days with at least one  hour of flying weather) during nest cell construction |
| Osmia_Female::m_CurrentProvisioning                          | mg                 | The amount of pollen currently provisioned in a cell         |
| Osmia_Female::m_CellCarryOver                                | hour               | Keeps track of any part time cell construction hours         |
| Osmia_Female::m_NestProvisioningPlan                         | -                  | List of female nest targets in terms of provisioning         |
| Osmia_Female::m_NestProvisioningPlanSex                      | -                  | List of female nest targets in terms of sex allocation       |
| _Osmia population manager_                                   |                    |                                                              |
| Osmia_Population_Manager::m_PreWinteringEndFlag              | -                  | Returns flag to denore the end of prewintering, if ended  it is set to true |
| Osmia_Population_Manager::m_OverWinterEndFlag                | -                  | Returns flag to denore the end of overwintering, if  ended it is set to true |
| Osmia_Population_Manager::m_FlyingWeather                    | -                  | Number of flying weather hours                               |
| Osmia_Population_Manager::m_FemaleDensityGrid                | per km<sup>2</sup> | Actual density of adult females                              |
| Osmia_Population_Manager::m_FemaleWeights                    | -                  | Vector for holding female emergence weights                  |
| _Osmia nest manager_                                         |                    |                                                              |
| Osmia_Nest_Manager::m_PossibleNestType                       | -                  | Set of flags indicating whether an Osmia nest is  possible (true) or not (false) |
| _Osmia polygon entry_                                        |                    |                                                              |
| OsmiaPolygonEntry::m_NestList                                | -                  | List of nests in a polygon                                   |
| OsmiaPolygonEntry::m_OsmiaNestProb                           | -                  | Probability of finding a nesting place in a polygon          |
| OsmiaPolygonEntry::m_MaxOsmiaNests                           | -                  | Maximum number of nests in a polygon                         |
| OsmiaPolygonEntry::m_CurrentOsmiaNests                       | -                  | Actual number of nests in a polygon                          |
| OsmiaPolygonEntry::m_Area                                    | m<sup>2</sup>      | Area of a  polygon                                           |
| OsmiaPolygonEntry::m_Polyindex                               | -                  | Polygon reference used by the Landscape and  Osmia_Nest_Manager |



\anchor ss704

### Scales

The _O. bicornis_ model and all landscape functions take place on a daily time scale. The landscape model is a one-meter resolution and ALMaSS species models are commonly run on landscapes of 10 by 10 km.

Weather patterns depend on the provided data, if the weather file is shorter than the length of the simulation, the weather recording is re-run.




\anchor ss8
## Discussion of implementation

There were several changes we included in relation to planned model implementation from the Formal Model (\ref r9 “Ziółkowska et al. 2023”). For example, parameters related to the development of bees in the nest were changed in response to calibration. This was expected as the developmental times measured under constant laboratory conditions (which were reported in the Formal Model) are very different from those experienced by the bees in the wild. In the field, temperatures fluctuate and are experienced differently by different nests with different locations (aspect, height, exposure). Although we implemented a simple degree-day model, calibration showed that it allows to successfully reflect the bee’s phenology observed in the natural conditions.

We were able to calibrate the nesting parameters in such a way that the emergent distribution of eggs per nest reflects well distribution observed in the field by \ref r12 “Ivanov (2006)”. The distribution of masses of adult bees at emergence was more difficult to fit into the pattern observed from the field data (provided by A. Bednarska, personal communication). It required the inclusion of an exponentially falling distribution of the probability of using excess pollen forageable to provision a nest, and a similar distribution to reduce the initial target provisioning mass, as well as calibration of the forage mg per hour scaling parameter, and the addition of a maximum pollen mass per hour that was allowable as a collection limit.

We observed that the year-to-year variation in _O. bicornis_ population numbers (i.e., numbers of produced female eggs versus male eggs) are resulting mainly from the differences in the pattern of available foraging hours per day. We also observed that although population numbers differed considerably from year to year during the simulation, the distribution of female masses did not. This seems to be in line with the optimal body size theory (\ref r11 “Seidelmann et al. 2010”).



\anchor ss9

## Calibration

The calibration output of the model is available in a [separate document](Doxygen_Pics/Osmia_calibration.pdf).




\anchor ss10

## References

\anchor r1 Goulson, D., Nicholls, E., Botias, C., Rotheray, E. L. (2015) Bee declines driven by combined stress from parasites, pesticides, and lack of flowers. Science 347, 6229.

\anchor r2 Høye, T. T., Skov, F., & Topping, C. J. (2012). Interpreting outputs of agent-based models using abundance–occupancy relationships. Ecological Indicators, 20, 221–227.

\anchor r3 Potts, S.G., Biesmeijer, J.C., Kremen, C., et al (2010) Global pollinator declines: trends, impacts and drivers. Trends Ecol Evol 25:345–353.

\anchor r4 Powney, G.D., Carvell, C., Edwards, M., et al (2019) Widespread losses of pollinating insects in Britain. Nat Commun 2019 101 10:1–6.

\anchor _r10 Raw, A. (1972) The biology of the solitary bee _Osmia rufa_ (L.) (Megachilidae). Trans. R. Entomol. Soc. London 124:213–229.

\anchor r5 Sánchez-Bayo, F., Wyckhuys, K.A.G. (2019) Worldwide decline of the entomofauna: A review of its drivers. Biol Conserv 232:8–27.

\anchor r6 Topping, C. J. (2022). The Animal \Landscape and Man Simulation System (ALMaSS): A history, design, and philosophy. Research Ideas and Outcomes, 8, e89919.

\anchor r7 Topping, C. J., Hansen, T. S., Jensen, T. S., Jepsen, J. U., Nikolajsen, F., & Odderskær, P. (2003). ALMaSS, an agent-based model for animals in temperate European landscapes. Ecological Modelling, 167(1), 65–82.

\anchor r8 Topping, C. J., Marcussen, L. K., Thomsen, P., & Chetcuti, J. (2022). The Formal Model article format: Justifying modelling intent and a critical review of data foundations through publication. Food and Ecological Systems Modelling Journal, 3, e91024.

\anchor r9 Ziółkowska, E., Bednarska, A. J., Laskowski, R., Topping C. J. (2023) The Formal Model for a spatially-explicit agent based model of the solitary bee *Osmia bicornis* L. Submitted to: Food and Ecological Systems Modelling Journal.
