// ============================================================================
// Declarations
// ============================================================================

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------

//#include "vld.h"  // INCLUDE FOR DEBUGGING MEMORY LEAKS

#include "stdio.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <wx/wx.h>
#include "wx/spinctrl.h"
#include <string>
#include <blitz/array.h>

#include "../ALMaSSDefines.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Vole/GeneticMaterial.h"
#include "../Skylark/skylarks_all.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Vole/vole_all.h"
#include "../Vole/VolePopulationManager.h"
#include "../Vole/Predators.h"
#include "../Bembidion/Bembidion_All.h"
#include "../Hare/Hare_All.h"
#include "../Spider/spider_all.h"
#include "../Spider/SpiderPopulationManager.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../Hunters/Hunters_all.h"
#include "../GooseManagement/Goose_Population_Manager.h"
#include "../RodenticideModelling/RodenticidePredators.h"
#include "../RoeDeer/Roe_all.h"
#include "../RoeDeer/Roe_pop_manager.h"
#include "../Rabbit/Rabbit.h"
#include "../Rabbit/Rabbit_Population_Manager.h"
#include "../Newt/Newt.h"
#include "../Newt/Newt_Population_Manager.h"
#include "../Osmia/Osmia.h"
#include "../Osmia/Osmia_Population_Manager.h"
#include "../OliveMoth/olivemoth.h"

using namespace std;

extern TTypesOfPopulation g_Species;

#include "ALMaSS_GUI.h"
//globals
int g_torun;


// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

// ----------------------------------------------------------------------------
// resources
// ----------------------------------------------------------------------------

// the application icon (under Windows and OS/2 it is in resources and even
// though we could still include the XPM here it would be unused)
#ifndef __UNIX
#if !defined(__WXMSW__) && !defined(__WXPM__)
    #include "../sample.xpm"
#endif
#endif

#include "wx/event.h"

//------------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// event tables and other macros for wxWidgets
// ----------------------------------------------------------------------------

// the event tables connect the wxWidgets events with the functions (event
// handlers) which process them. It can be also done at run-time, but for the
// simple menu events like this the static method is much simpler.
BEGIN_EVENT_TABLE( MyMap, wxPanel )
     ////@begin MyMap event table entries
	EVT_LEFT_DOWN( MyMap::LeftMouseDown )
	EVT_RIGHT_DOWN( MyMap::RightMouseDown )
	EVT_PAINT( MyMap::ShowBitmap )
     ////@end MyMap event table entries
END_EVENT_TABLE()

BEGIN_EVENT_TABLE( ALMaSSGUI, wxFrame )
	EVT_BUTTON( ID_BUTTON,		ALMaSSGUI::OnButton0Click )
	EVT_BUTTON( ID_BUTTON1,		ALMaSSGUI::OnButton1Click )
	EVT_BUTTON( ID_BUTTON2,		ALMaSSGUI::OnButton2Click )
	//EVT_BUTTON( ID_BUTTON3,		ALMaSSGUI::OnButton3Click )
	EVT_CHOICE(ID_CHOICE3, ALMaSSGUI::ChooseDisplay)
END_EVENT_TABLE();

// Create a new application object: this macro will allow wxWidgets to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also implements the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MyApp and
// not wxApp)
IMPLEMENT_APP(MyApp) ;

// ============================================================================
// implementation
// ============================================================================

//#define __SpecificPesticideEffectsTRINEDALKVIST__

extern CfgBool l_pest_enable_pesticide_engine;
extern CfgInt l_pest_NoPPPs;
extern CfgBool cfg_rodenticide_enable;
extern CfgBool cfg_fixed_random_sequence;
static CfgBool cfg_animate( "G_ANIMATE", CFG_CUSTOM, true );
static CfgBool cfg_dumpmap( "G_DUMPMAP", CFG_CUSTOM, false ); // Produces a set of bitmaps using the next three cfgs to determine start, range and interval
static CfgInt cfg_dumpmapstart( "G_DUMPMAPSTART", CFG_CUSTOM, 366  );
static CfgInt cfg_dumpmapend( "G_DUMPMAPEND", CFG_CUSTOM, 730 );
static CfgInt cfg_dumpmapstep( "G_DUMPMAPSTEP", CFG_CUSTOM, 5 );


//---------------------------------------------------------------------------
const wxString TheMonths[ 13 ] = {_T(""),
_T("  January"),
_T(" February"),
_T("    March"),
_T("    April"),
_T("      May"),
_T("     June"),
_T("     July"),
_T("   August"),
_T("September"),
_T("  October"),
_T(" November"),
_T(" December")};

// ----------------------------------------------------------------------------
const double randmaxp= RAND_MAX +1;
// ----------------------------------------------------------------------------

// Globals
	ALMaSSGUI* g_MainForm;
	ALMaSS_MathFuncs g_AlmassMathFuncs;

	PopulationManagerList g_PopulationManagerList;




//------------------------------------------------------------------------------
/*
#ifdef __UNIX
void FloatToDouble(double &d, float f) {
	char * num = 0;
	num = (char*) malloc(128);
	gcvt(f,8,num);
	d = atof(num);
	delete num;
}
#else
void FloatToDouble(double &d, float f) {
    char * num = 0;
    num = (char*) malloc(_CVTBUFSIZE);
    errno_t err = _gcvt_s(num, _CVTBUFSIZE,f,8);
    if (err!=0) {
      assert(0);
    }
    d = atof(num);
	delete num;
}
#endif
*/

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

// 'Main program' equivalent: the program execution "starts" here
bool MyApp::OnInit()
{
    // create the main application window
    m_frame = new ALMaSSGUI( wxT( "ALMaSS" ), wxPoint(50,50), wxSize(-1,-1));

    // and show it (the frames, unlike simple controls, are not shown when
    // created initially)
	m_frame->Show(TRUE);
	SetTopWindow(m_frame);
    // success: wxApp::OnRun() will be called which will enter the main message
    // loop and the application will run. If we returned false here, the
    // application would exit immediately.
    return true;
}

int MyApp::OnRun()
{
    // see the comment in ctor: if the initial value hasn't been changed, use
    // the default Yes from now on
    if ( m_exitOnFrameDelete == Later )
    {
        m_exitOnFrameDelete = Yes;
    }
    //else: it has been changed, assume the user knows what he is doing
    return MainLoop();
}


// ----------------------------------------------------------------------------
// main frame
// ----------------------------------------------------------------------------

// frame constructor
ALMaSSGUI::ALMaSSGUI(const wxString& title, const wxPoint& pos, const wxSize& size )
	: wxFrame((wxFrame *)NULL, -1, title, pos, size)
{
	// Must come first. Used by the configurator below.
	g_msg = new MapErrorMsg("ErrorFile.txt");
	g_msg->SetWarnLevel(WARN_ALL);

	// Configurator instantiation is automatic.
	g_cfg->ReadSymbols("TIALMaSSConfig.cfg");
	//g_cfg->DumpAllSymbolsAndExit( "allsymbols.cfg" );
	Create();
	g_MainForm=this;
}

ALMaSSGUI::~ALMaSSGUI() {
  CloseDownSim();
  for ( int i = 0; i < m_NoProbes; i++ ) {
    delete m_files[ i ];
  }
  for ( int i = 0; i < m_NoPredProbes; i++ ) {
    delete m_Predfiles[ i ];
  }
  delete g_cfg;
  delete m_aMap;
  //_CrtDumpMemoryLeaks();

}

bool ALMaSSGUI::Create() {

  ////@begin ALMaSSGUI member initialisation
  ////@end ALMaSSGUI member initialisation

  ////@begin ALMaSSGUI creation
  //    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
  //    wxDialog::Create( parent, id, caption, pos, size, style );
  CreateControls();
  GetSizer()->Fit( this );
  GetSizer()->SetSizeHints( this );
  Centre();
  DataInit();
  m_aMap->SetUpColours();
  ////@end ALMaSSGUI creation
  return TRUE;
}

void ALMaSSGUI::DataInit() {
  // Application flags and data
  m_ProbesSet = false;
  m_NoProbes = 0;
  m_time = 0;
  m_Year = 0;
  m_Steps = 0;
  m_SimInitiated = false;
  // End application data & flags
}

//------------------------------------------------------------------------------
bool ALMaSSGUI::IsAnimalChecked(int a_index)
{
	switch (a_index)
	{
	case 1:
		if (m_CheckBox1->IsChecked()) return true; else return false;
		break;
	case 2:
		if (m_CheckBox2->IsChecked()) return true; else return false;
		break;
	case 3:
		if (m_CheckBox3->IsChecked()) return true; else return false;
		break;
	case 4:
		if (m_CheckBox4->IsChecked()) return true; else return false;
		break;
	case 5:
		if (m_CheckBox5->IsChecked()) return true; else return false;
		break;
	case 6:
		if (m_CheckBox6->IsChecked()) return true; else return false;
		break;
	case 7:
		if (m_CheckBox7->IsChecked()) return true; else return false;
		break;
	case 8:
		if (m_CheckBox8->IsChecked()) return true; else return false;
		break;
	case 9:
		if (m_CheckBox9->IsChecked()) return true; else return false;
		break;
	case 10:
		if (m_CheckBox10->IsChecked()) return true; else return false;
		break;
	case 11:
		if (m_CheckBox11->IsChecked()) return true; else return false;
		break;
	case 12:
		if (m_CheckBox12->IsChecked()) return true; else return false;
		break;
	default:
		// No such check box raise and error
		wxMessageBox(_T("ALMaSSGUI::IsAnimalChecked: No such checkbox"), _T("Error"), wxOK, this);
		exit(1);
	}
}

/* ! * Control creation for ALMaSSGUI */

void ALMaSSGUI::CreateControls() {
  ////@begin ALMaSSGUI content construction

  item1 = this;

  wxBoxSizer * item2 = new wxBoxSizer( wxVERTICAL );
  item1->SetSizer( item2 );
  item1->SetAutoLayout( TRUE );

  wxBoxSizer * item3 = new wxBoxSizer( wxHORIZONTAL );
  item2->Add( item3, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_StartButton = new wxButton( item1, ID_BUTTON, _( "Start" ), wxDefaultPosition, wxSize( 50, -1 ), 0 );
  m_StartButton->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  item3->Add( m_StartButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  m_PauseButton = new wxButton( item1, ID_BUTTON1, _( "Pause" ), wxDefaultPosition, wxSize( 50, -1 ), 0 );
  m_PauseButton->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  item3->Add( m_PauseButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  m_HaltButton = new wxButton( item1, ID_BUTTON2, _( "Halt" ), wxDefaultPosition, wxSize( 50, -1 ), 0 );
  m_HaltButton->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_HaltButton->SetHelpText( _( "Pressing here will end the current simulation" ) );
  if ( ShowToolTips() )
    m_HaltButton->SetToolTip( _( "Pressing here will end the current simulation" ) );
  item3->Add( m_HaltButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  item7 = new wxSpinCtrl( item1, ID_SPINCTRL, _T( "" ), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 1000, 1 );
  item3->Add( item7, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  wxString item8Strings[] = {
        _("Year"),
        _("Month"),
        _("Day"),
        _("Step")
    };
  item8 = new wxChoice( item1, ID_CHOICE, wxDefaultPosition, wxDefaultSize, 4, item8Strings, 0 );
  item8->SetStringSelection( _( "Year" ) );
  item3->Add( item8, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

#define __noMenuSpecies 13

  wxString item9Strings[__noMenuSpecies] = {
        _("Skylark"),
        _("Vole"),
        _("Spider"),
        _("Beetle"),
        _("Hare"),
        _("Partridge"),
	_("Goose Management"),
	_("Roe Deer"),
	_("Rabbit"),
	_("Newt"),
	_("Osmia rufa"),
	_("ApisRAM"),
	_("Olive Moth")

  item9 = new wxChoice( item1, ID_CHOICE2, wxDefaultPosition, wxDefaultSize, __noMenuSpecies, item9Strings, 0 );
  item9->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  //item9->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  //item9->SetForegroundColour( wxColour( 192, 192, 192 ) );
  item9->SetSelection( 12 ); // default
  item3->Add( item9, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  wxBoxSizer * item10 = new wxBoxSizer( wxHORIZONTAL );
  item2->Add( item10, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_aMap = new MyMap( item1, ID_PANEL, wxDefaultPosition, wxSize( __MAPSIZE, __MAPSIZE ), wxDOUBLE_BORDER | wxTAB_TRAVERSAL );
  m_aMap->m_MainForm = item1;
  item10->Add( m_aMap, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  wxString * item13Strings = NULL;
  m_ListPad = new wxListBox( item1, ID_LISTBOX, wxDefaultPosition, wxSize( 250, __MAPSIZE ), 0, item13Strings, wxLB_SINGLE );
  item10->Add( m_ListPad, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  wxBoxSizer * sizer4 = new wxBoxSizer( wxVERTICAL );
  item10->Add( sizer4, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );


  /*
wxBitmap LogoBitmap( _T( "aLogo.bmp" ), wxBITMAP_TYPE_BMP );
  wxStaticBitmap * Logo = new wxStaticBitmap( item1, wxID_STATIC, LogoBitmap, wxDefaultPosition, wxSize( 98, 46 ), 0 );
  sizer4->Add( Logo, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );
*/
  wxStaticText * item16 = new wxStaticText(item1, wxID_STATIC, _("Current Simulation Date: "), wxDefaultPosition, wxDefaultSize, 0);
  item16->SetForegroundColour(wxColour(192, 192, 192));
  item16->SetBackgroundColour(wxColour(7, 86, 40));
  item16->SetFont(wxFont(9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T("Tahoma")));
  sizer4->Add(item16, 0, wxALIGN_CENTER_HORIZONTAL | wxALL | 5, 5);
  m_DateBox = new wxTextCtrl(item1, ID_TEXTCTRL, _(" 1 January    0"), wxDefaultPosition, wxSize(150, -1), wxTE_READONLY);
  m_DateBox->SetFont(wxFont(10, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T("Tahoma")));
  sizer4->Add(m_DateBox, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);

  wxString * ProbeBoxStrings = NULL;
  m_ProbesBox = new wxListBox( item1, ID_LISTBOX1, wxDefaultPosition, wxSize( 175, 300 ), 0, ProbeBoxStrings, wxLB_HSCROLL );
  sizer4->Add( m_ProbesBox, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  wxStaticText * label_0 = new wxStaticText( item1, wxID_STATIC, _( "Display Animal Positions" ), wxDefaultPosition, wxDefaultSize, 0 );
  label_0->SetForegroundColour( wxColour( 192, 192, 192 ) );
  label_0->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  label_0->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  sizer4->Add( label_0, 0, wxALIGN_CENTER_HORIZONTAL | wxALL | 5, 5 );

  m_CheckBox1 = new wxCheckBox( item1, ID_CHECKBOX, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox1->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox1->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox1->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox1->SetValue( FALSE );
  sizer4->Add( m_CheckBox1, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox2 = new wxCheckBox( item1, ID_CHECKBOX1, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox2->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox2->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox2->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox2->SetValue( FALSE );
  sizer4->Add( m_CheckBox2, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox3 = new wxCheckBox( item1, ID_CHECKBOX2, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox3->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox3->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox3->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox3->SetValue( FALSE );
  sizer4->Add( m_CheckBox3, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox4 = new wxCheckBox( item1, ID_CHECKBOX3, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox4->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox4->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox4->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox4->SetValue( FALSE );
  sizer4->Add( m_CheckBox4, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox5 = new wxCheckBox( item1, ID_CHECKBOX4, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox5->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox5->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox5->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox5->SetValue( FALSE );
  sizer4->Add( m_CheckBox5, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox6 = new wxCheckBox( item1, ID_CHECKBOX5, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox6->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox6->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox6->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox6->SetValue( FALSE );
  sizer4->Add( m_CheckBox6, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox7 = new wxCheckBox( item1, ID_CHECKBOX, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox7->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox7->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox7->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox7->SetValue( FALSE );
  sizer4->Add( m_CheckBox7, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox8 = new wxCheckBox( item1, ID_CHECKBOX1, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox8->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox8->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox8->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox8->SetValue( FALSE );
  sizer4->Add( m_CheckBox8, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox9 = new wxCheckBox( item1, ID_CHECKBOX2, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox9->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox9->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox9->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox9->SetValue( FALSE );
  sizer4->Add( m_CheckBox9, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox10 = new wxCheckBox( item1, ID_CHECKBOX3, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox10->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox10->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox10->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox10->SetValue( FALSE );
  sizer4->Add( m_CheckBox10, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox11 = new wxCheckBox( item1, ID_CHECKBOX4, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox11->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox11->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox11->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox11->SetValue( FALSE );
  sizer4->Add( m_CheckBox11, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  m_CheckBox12 = new wxCheckBox( item1, ID_CHECKBOX5, _( "Unused" ), wxDefaultPosition, wxSize( 100, -1 ), 0 );
  m_CheckBox12->SetForegroundColour( wxColour( 192, 192, 192 ) );
  m_CheckBox12->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  m_CheckBox12->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  m_CheckBox12->SetValue( FALSE );
  sizer4->Add( m_CheckBox12, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  wxBoxSizer * item14 = new wxBoxSizer( wxHORIZONTAL );
  item2->Add( item14, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  wxString item15Strings[] = {
        _("Standard View"),
        _("Veg. Type"),
        _("Veg. Biomass"),
        _("Farm Ownership"),
		_("Pesticide Load"),
		_("Goose Numbers"),
		_("Goose Food Resource"),
		_("Goose Grain Resource"),
		_("Goose Grazing Resource"),
		_("Soil Type (rabbits)"),
		_("Hunters Locations"),
		_("Pollen"),
		_("Nectar")
  };
  item15 = new  wxChoice(item1, ID_CHOICE3, wxDefaultPosition, wxDefaultSize, 13, item15Strings, 0);
  item15->SetSelection(0); // default is Standard View

	  
	  //wxRadioBox( item1, ID_RADIOBOX1, _( "Display" ), wxDefaultPosition, wxSize( 700, -1 ), 7, item15Strings, 1, wxRA_SPECIFY_ROWS );
  //item15->SetForegroundColour( wxColour( 192, 192, 192 ) );
  //item15->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  item15->SetFont( wxFont( 9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, FALSE, _T( "Tahoma" ) ) );
  item3->Add( item15, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  // Sub-selection of display types (e.g. pesticides)
  m_subselectionspin = new wxSpinCtrl(item1, ID_SPINCTRL2, _T("Sub-selection"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 1000, 1);
  m_subselectionspin->Disable();
  item3->Add(m_subselectionspin, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);


  wxBoxSizer * item17 = new wxBoxSizer( wxHORIZONTAL );
  item2->Add( item17, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

  /*
  m_BatchButton = new wxButton( item1, ID_BUTTON3, _( "Run In Batch Mode" ), wxDefaultPosition, wxSize( 110, -1 ), 0 );
  item17->Add( m_BatchButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  wxStaticText * item19 = new wxStaticText( item1, wxID_STATIC, _( "Batch File:" ), wxDefaultPosition, wxDefaultSize, 0 );
  item19->SetForegroundColour( wxColour( 255, 255, 255 ) );
  item19->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  item19->SetFont( wxFont( 9, wxSWISS, wxNORMAL, wxNORMAL, FALSE, _T( "Tahoma" ) ) );
  item17->Add( item19, 0, wxALIGN_CENTER_VERTICAL | wxALL | wxADJUST_MINSIZE, 5 );

  m_BatchFileEdit = new wxTextCtrl( item1, ID_TEXTCTRL1, _( "TIBatch.bat" ), wxDefaultPosition, wxSize( 200, -1 ), 0 );
  item17->Add( m_BatchFileEdit, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );

  wxStaticText * item31 = new wxStaticText( item1, wxID_STATIC, _( "    INI File:" ), wxDefaultPosition, wxDefaultSize, 0 );
  item31->SetForegroundColour( wxColour( 255, 255, 255 ) );
  item31->SetBackgroundColour( wxColour( 7, 86, 40 ) );
  item31->SetFont( wxFont( 9, wxSWISS, wxNORMAL, wxNORMAL, FALSE, _T( "Tahoma" ) ) );
  item17->Add( item31, 0, wxALIGN_CENTER_VERTICAL | wxALL | wxADJUST_MINSIZE, 5 );

  m_IniFileEdit = new wxTextCtrl( item1, ID_TEXTCTRL1, _( "TI_inifile.ini" ), wxDefaultPosition, wxSize( 200, -1 ), 0 );
  item17->Add( m_IniFileEdit, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5 );
  */

  this->SetBackgroundColour( wxColour( 7, 86, 40 ) );

  m_Paused = false;

  ////@end ALMaSSGUI content construction
}

//------------------------------------------------------------------------------

/* ! * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON */
void ALMaSSGUI::OnButton0Click(wxCommandEvent& /* event */) {
  // The Start Button has been pressed
  m_BatchRun = false;
  m_StartButton->Enable( false );
  /* Modified dww. For some reason I don't have SetMax, but do have SetRange */
  m_subselectionspin->SetRange(1,l_pest_NoPPPs.value());
  // Call the GO method
  Go();
  m_StartButton->Enable( true );
}

//------------------------------------------------------------------------------
/* ! * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON1 */
void ALMaSSGUI::OnButton1Click(wxCommandEvent& /* event */) {
  if ( m_Paused ) {
    m_Paused = false;
    m_PauseButton->SetLabel( _T("Pause") );
  } else {
    m_Paused = true;
    m_PauseButton->SetLabel( _T("Continue") );
  }
  while ( m_Paused ) {
    while ( wxTheApp->Pending() ) wxTheApp->Dispatch();
  }

}

//------------------------------------------------------------------------------
/* ! * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON2 */
void ALMaSSGUI::OnButton2Click(wxCommandEvent& /* event */) {
  // Halt Button Press
  m_torun = 0;
  m_StopSignal = true;
  // This will stop the current sim without causing any other nasty problems
  m_DateBox->Clear();
}

//------------------------------------------------------------------------------
/* ! * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON3 */
void ALMaSSGUI::OnButton3Click(wxCommandEvent& /* event */) {
  m_StartButton->Enable( false );
  //m_BatchButton->Enable( false );
  // RunBatch Button Press
  //RunBatch1Click();
  m_StartButton->Enable( true );
  //m_BatchButton->Enable( true );
}

/** \brief handles subselection menu if needed */
void ALMaSSGUI::ChooseDisplay(wxCommandEvent& /* event */)
{
	if (item15->GetSelection() == 4)
	{
		m_subselectionspin->Enable();
		
		/* Modified dww. For some reason I don't have SetMax, but do have SetRange */
		m_subselectionspin->SetRange(1,l_pest_NoPPPs.value());
		m_subselectionspin->SetValue(1);
	}
	else
	{
		m_subselectionspin->Disable();
		m_subselectionspin->SetValue("Sub-selection");
	}
}

//------------------------------------------------------------------------------

/* ! * Should we show tooltips? */

bool ALMaSSGUI::ShowToolTips() {
  return TRUE;
}

//------------------------------------------------------------------------------

void ALMaSSGUI::Go() {
	// Go figures out whether the simulation is already running, and if not
	// starts a new one, otherwise it runs for however long the form says
	if ( !m_SimInitiated ) {
		// Randomize the random generator.
		if (cfg_fixed_random_sequence.value()) {
			srand(0); // Use of rand() discouraged. use internal ALMaSS_Random.h functionality
			g_generator_fixed();;
		}
		else
		{
			srand((int)time(NULL)); // Use of rand() discouraged. use internal ALMaSS_Random.h functionality
		}
		m_torun = GetTimeToRun();
		if ( m_torun > 0 ) {
			if ( !ReadBatchINI() ) return;
			CreateLandscape();
			if (!CreatePopulationManager()) return; // Let out if for some reason the population manager was not created
			m_AManager->m_MainForm = this;
			m_ALandscape->SetThePopManager(m_AManager);
			SetUpForm();
			// Now got to get the probe files read in
			GetProbeInput_ini();
			// Ready to go
			m_time = 0;
			m_SimInitiated = true;
			m_StopSignal = false;
			m_aMap->DrawLandscape();
		}
	} else {
		m_torun = GetTimeToRun();
	}
	if ( m_SimInitiated ) RunTheSim();
}

//------------------------------------------------------------------------------

void ALMaSSGUI::RunTheSim() {
	m_bmpout = (_T("ALMaSSbmpOut_0000.bmp"));
  for ( int i = 0; i < m_torun; i++ ) {
    // Check for windows messages
    // OK _ looks like wxTheApp is something declared somewhere....!"!
    while ( wxTheApp->Pending() ) wxTheApp->Dispatch();
    m_ALandscape->TurnTheWorld();
    while ( wxTheApp->Pending() ) wxTheApp->Dispatch();
    // Update the Date
    m_time++;
    int day = m_ALandscape->SupplyDayInMonth();
    int month = m_ALandscape->SupplyMonth();
    if ( ( day == 1 ) && ( month == 1 ) ) m_Year++;
    wxString str = wxString::Format( _T("%d "), day );
    str += TheMonths[ month ];
    str += wxString::Format( _T(" %4d "), m_Year );
    while ( wxTheApp->Pending() ) wxTheApp->Dispatch();
    m_DateBox->Clear();
    m_DateBox->AppendText( str );
    // Check for windows messages
    while ( wxTheApp->Pending() ) wxTheApp->Dispatch();
    if ( item9->GetSelection() == TOP_Vole ) m_PredatorManager->Run( 1 );
    if ( item9->GetSelection() == TOP_Goose)
	{
		for (int tenmin=0; tenmin<144; tenmin++)
		{
			m_AManager->Run( 1 ); // Goose Model
			m_Hunter_Population_Manager->Run(1);
		}
	}
    else {
        for (int p = 0; p < TOP_foobar; p++)
        {
            if (g_PopulationManagerList.GetPopulation(p) != NULL) g_PopulationManagerList.GetPopulation(p)->Run(1);
        }
    }
    while ( wxTheApp->Pending() ) wxTheApp->Dispatch();
    // Do the probe output
    // Drawing functions here

    if (cfg_animate.value()) m_aMap->DrawLandscape();
    ShowAnimals();
    // If we want to dump the bmp map to a file then we need to do it here.
	if (cfg_dumpmap.value()) DumpMap(i);

	while ( wxTheApp->Pending() ) wxTheApp->Dispatch();
    m_aMap->ShowBitmap2();
    if ( item9->GetSelection() == 1 ) m_PredatorManager->ProbeReport( m_time );
	wxString astr(m_AManager->SpeciesSpecificReporting(item9->GetSelection(), m_time), wxConvUTF8);
    if (astr.length()>0)
	m_ProbesBox->InsertItems( 1, &astr, 0 );
	m_ALandscape->DumpVegAreaData( m_time );
	Update();
    if ( m_StopSignal ) {
      m_torun = -1;
      CloseDownSim();
    }
  }
  }
//---------------------------------------------------------------------------

bool ALMaSSGUI::DumpMap(int day)
{
  if ((cfg_dumpmapstart.value()<=day) && (cfg_dumpmapend.value()>=day) && ((day-cfg_dumpmapstart.value())%cfg_dumpmapstep.value()==0))
    {
      m_aMap->wxIm->SaveFile(m_bmpout,wxBITMAP_TYPE_BMP );
    }
  wxString fnum=m_bmpout.BeforeFirst('.');
  fnum=fnum.AfterLast('_');
  long thisfilenum;
  fnum.ToLong(&thisfilenum,10);
  thisfilenum++;
  m_bmpout = _T("ALMaSSbmpOut_")+wxString::Format( _T("%04d"), (int)thisfilenum )+_T(".bmp");
  return true;
}

//---------------------------------------------------------------------------
bool ALMaSSGUI::ImmediateRun(int a_species) {
		item9->SetSelection( a_species );
		m_StartButton->Enable( false );
		//m_BatchButton->Enable( false );
		m_BatchRun = true;
		// Read in the ini file
		if ( !ReadBatchINI() ) {
		  cout <<"No batch file present or a problem with it so giving up" << "\n";
		  return false;
		}
		// Make a Landscape
		printf( "Making Landscape\n" );
		CreateLandscape();
		printf( "Landscape Created\n" ); // So now set up a Population Manager
		if ( !CreatePopulationManager() ) {
			cout << "Failed to create the population manager";
			return false;
		} else
		  printf( "Population Created\n" ); // Now got to get the probe files read in
		GetProbeInput_ini();
		// Ready to go
		m_SimInitiated = true;
		m_StopSignal=false;
		Refresh();
		Update();
		RunTheSim();
		CloseDownSim();
		return true;
}
//-------------------------------------------------------------------------------------

void ALMaSSGUI::UpdateGraphics()
{
	m_aMap->DrawLandscape();
	ShowAnimals();
    m_aMap->ShowBitmap2();
	Update();
}
//-------------------------------------------------------------------------------------

void ALMaSSGUI::ShowAnimals() {
  int x, y;
  if (item9->GetSelection() == 1) 
  {
	/*
	// Special code for genetic demo
	*/
    if ( m_CheckBox6->IsChecked() )
	{
		Vole_Population_Manager * VPM;
		VPM = dynamic_cast<Vole_Population_Manager*>(m_AManager);
		for ( int lstages=0; lstages<4; lstages++)
		{
			unsigned noInList = m_AManager->GetLiveArraySize(lstages);
			for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) 
			{
				VPM->SupplyLocXY( lstages, anAnimalRef, x, y );
				// Get the genetic colour
				unsigned gcolour = 3;
				unsigned gcolour1 = VPM->GetVoleAllele(lstages, anAnimalRef, 3, 0);
				unsigned gcolour2 = VPM->GetVoleAllele(lstages, anAnimalRef, 3, 1);
				if ((gcolour1 == 1) && (gcolour2 ==1))  gcolour=4; else if ((gcolour1 == 1) || (gcolour2 ==1)) gcolour = 2;
				m_aMap->Spot( gcolour, x, y );
			}
		}
		return;
	}
  }
  else 
  {
	  if ( m_CheckBox6->IsChecked() ) 
	  {
		  unsigned noInList = m_AManager->GetLiveArraySize(5);
		for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) 
		{
		  m_AManager->SupplyLocXY( 5, anAnimalRef, x, y );
		  m_aMap->Spot( 5, x, y );
		}
	  }
  }
  // unsigned anAnimalRef;
  if ( m_CheckBox1->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(0);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 0, anAnimalRef, x, y );
      m_aMap->Spot( 0, x, y );
    }
  }
  if ( m_CheckBox2->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(1);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 1, anAnimalRef, x, y );
      m_aMap->Spot( 1, x, y );
    }
  }
  if ( m_CheckBox3->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(2);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 2, anAnimalRef, x, y );
      m_aMap->Spot( 2, x, y );
    }
  }
  if ( m_CheckBox4->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(3);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 3, anAnimalRef, x, y );
      m_aMap->Spot( 3, x, y );
    }
  }
  
  if ( m_CheckBox5->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(4);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 4, anAnimalRef, x, y );
      m_aMap->Spot( 4, x, y );
    }
  }

  if ( m_CheckBox6->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(5);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 5, anAnimalRef, x, y );
      m_aMap->Spot( 5, x, y );
    }
  }

  if ( m_CheckBox7->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(6);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 6, anAnimalRef, x, y );
      m_aMap->Spot( 6, x, y );
    }
  }

  if ( m_CheckBox8->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(7);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 7, anAnimalRef, x, y );
      m_aMap->Spot( 7, x, y );
    }
  }

  if ( m_CheckBox9->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(8);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 8, anAnimalRef, x, y );
      m_aMap->Spot( 8, x, y );
    }
  }

  if ( m_CheckBox10->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(9);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 9, anAnimalRef, x, y );
      m_aMap->Spot( 9, x, y );
    }
  }

  if ( m_CheckBox11->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(10);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 10, anAnimalRef, x, y );
      m_aMap->Spot( 10, x, y );
    }
  }

  if ( m_CheckBox12->IsChecked() ) {
	  unsigned noInList = m_AManager->GetLiveArraySize(11);
    for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ ) {
      m_AManager->SupplyLocXY( 11, anAnimalRef, x, y );
      m_aMap->Spot( 11, x, y );
    }
  }



}

//---------------------------------------------------------------------------

void ALMaSSGUI::CloseDownSim() 
{
  if ( m_SimInitiated ) 
  {
	m_ALandscape->SimulationClosingActions();
  	if ( m_AManager ) {
      // Close the probe file
      if ( m_AManager->TheProbe[ 0 ] != NULL ) m_AManager->TheProbe[ 0 ]->CloseFile();
      // delete all probes
      for ( int i = 0; i < m_NoProbes; i++ ) delete m_AManager->TheProbe[ i ];
      delete m_AManager;
      if ( item9->GetSelection() == 1 ) {
        delete m_PredatorManager;
      }
    }
	delete m_ALandscape;
    m_time = 0;
    item9->Enable( true );
  }
  m_Year = 0;
  m_SimInitiated = false; // so we can run another if we want to later
  m_StartButton->Enable( true );
  //m_BatchButton->Enable( true );
}

//------------------------------------------------------------------------------

void ALMaSSGUI::SetUpForm() {
  // Must be called before running and after m_AManager is initialised
  if ( !m_AManager ) return;
  m_CheckBox1->SetLabel( _T("Unused" ));
  m_CheckBox1->Enable( false );
  m_CheckBox2->SetLabel( _T("Unused" ));
  m_CheckBox2->Enable( false );
  m_CheckBox3->SetLabel( _T("Unused" ));
  m_CheckBox3->Enable( false );
  m_CheckBox4->SetLabel( _T("Unused" ));
  m_CheckBox4->Enable( false );
  m_CheckBox5->SetLabel( _T("Unused" ));
  m_CheckBox5->Enable( false );
  m_CheckBox6->SetLabel( _T("Unused" ));
  m_CheckBox6->Enable( false );
  m_CheckBox7->SetLabel( _T("Unused" ));
  m_CheckBox7->Enable( false );
  m_CheckBox8->SetLabel( _T("Unused" ));
  m_CheckBox8->Enable( false );
  m_CheckBox9->SetLabel( _T("Unused" ));
  m_CheckBox9->Enable( false );
  m_CheckBox10->SetLabel( _T("Unused" ));
  m_CheckBox10->Enable( false );
  m_CheckBox11->SetLabel( _T("Unused" ));
  m_CheckBox11->Enable( false );
  m_CheckBox12->SetLabel( _T("Unused" ));
  m_CheckBox12->Enable( false );
  int NoChecks = m_AManager->SupplyListNameLength();
  cout << "NoChecks: " << NoChecks << "\n";
  wxString line1(m_AManager->SupplyListName( 0 ),wxConvUTF8);
  wxString line2(m_AManager->SupplyListName( 1 ),wxConvUTF8);
  wxString line3(m_AManager->SupplyListName( 2 ),wxConvUTF8);
  wxString line4(m_AManager->SupplyListName( 3 ),wxConvUTF8);
  wxString line5(m_AManager->SupplyListName( 4 ),wxConvUTF8);
  wxString line6(m_AManager->SupplyListName( 5 ),wxConvUTF8);
  wxString line7(m_AManager->SupplyListName( 6 ),wxConvUTF8);
  wxString line8(m_AManager->SupplyListName( 7 ),wxConvUTF8);
  wxString line9(m_AManager->SupplyListName( 8 ),wxConvUTF8);
  wxString line10(m_AManager->SupplyListName( 9 ),wxConvUTF8);
  wxString line11(m_AManager->SupplyListName( 10 ),wxConvUTF8);
  wxString line12(m_AManager->SupplyListName( 11 ),wxConvUTF8);

  switch ( NoChecks ) {
    case 12:
      m_CheckBox12->SetLabel( line12);
      m_CheckBox12->Enable( true );
      m_CheckBox12->SetValue(true);
    case 11:
      m_CheckBox11->SetLabel( line11);
      m_CheckBox11->Enable( true );
      m_CheckBox11->SetValue(true);
    case 10:
      m_CheckBox10->SetLabel( line10 );
      m_CheckBox10->Enable( true );
      m_CheckBox10->SetValue(true);
    case 9:
      m_CheckBox9->SetLabel( line9 );
      m_CheckBox9->Enable( true );
      m_CheckBox9->SetValue(true);
    case 8:
      m_CheckBox8->SetLabel( line8 );
      m_CheckBox8->Enable( true );
      m_CheckBox8->SetValue(true);
    case 7:
      m_CheckBox7->SetLabel( line7 );
      m_CheckBox7->Enable( true );
      m_CheckBox7->SetValue(true);
    case 6:
      m_CheckBox6->SetLabel( line6);
      m_CheckBox6->Enable( true );
      m_CheckBox6->SetValue(true);
    case 5:
      m_CheckBox5->SetLabel( line5);
      m_CheckBox5->Enable( true );
      m_CheckBox5->SetValue(true);
    case 4:
      m_CheckBox4->SetLabel( line4 );
      m_CheckBox4->Enable( true );
      m_CheckBox4->SetValue(true);
    case 3:
      m_CheckBox3->SetLabel( line3 );
      m_CheckBox3->Enable( true );
      m_CheckBox3->SetValue(true);
    case 2:
      m_CheckBox2->SetLabel( line2 );
      m_CheckBox2->Enable( true );
      m_CheckBox2->SetValue(true);
    case 1:
      m_CheckBox1->SetLabel( line1 );
      m_CheckBox1->Enable( true );
      m_CheckBox1->SetValue(true);
    break;
  default:
    g_msg->Warn("Unknown Species Type in GUI SetUpForm ", 1);
    exit(1);

  }
}

//------------------------------------------------------------------------------

void ALMaSSGUI::CreateLandscape() {
  // Create the landscape
  m_ALandscape = new Landscape();
  m_aMap->SetSimExtent( m_ALandscape->SupplySimAreaWidth(), m_ALandscape->SupplySimAreaHeight() );
  m_aMap->SetLandscape( m_ALandscape );
  m_aMap->SetUpMapImage();
}

//------------------------------------------------------------------------------

bool ALMaSSGUI::CreatePopulationManager() {
  // SET UP THE ANIMAL POPULATION
  // THE LANDSCAPE MUST BE SETUP BEFORE THE CALL HERE
  if ( !m_ALandscape ) {
    wxMessageBox( _T("ALMaSSGUI::CreatePopulationManager: Landscape Not Created"), _T("ERROR"), wxOK, this );
    return false;
  }
  g_Species = item9->GetSelection();

    if ( item9->GetSelection() == TOP_Skylark) {
    Skylark_Population_Manager * skMan = new Skylark_Population_Manager( m_ALandscape );
    m_AManager = skMan;
    m_AManager->OpenTheBreedingPairsProbe();
    m_AManager->OpenTheBreedingSuccessProbe();
    m_AManager->OpenTheFledgelingProbe();
	g_PopulationManagerList.SetPopulation(m_AManager, TOP_Skylark);
  }

    if ( item9->GetSelection() == TOP_Vole) {
    Vole_Population_Manager * vMan = new Vole_Population_Manager( m_ALandscape );
    m_AManager = vMan;
	g_PopulationManagerList.SetPopulation(m_AManager, TOP_Vole);
    m_PredatorManager = new TPredator_Population_Manager( m_ALandscape, vMan );
    m_PredatorManager->SetNoProbesAndSpeciesSpecificFunctions(m_NoPredProbes);
	g_PopulationManagerList.SetPopulation(m_PredatorManager, TOP_Predators);

  }

	if ( item9->GetSelection() == TOP_Spider) {
		Erigone_Population_Manager * spMan = new Erigone_Population_Manager( m_ALandscape );
		m_AManager = spMan;
		g_PopulationManagerList.SetPopulation(m_AManager, TOP_Spider);
	}
	
	if ( item9->GetSelection() == TOP_Bembidion) {
		Bembidion_Population_Manager * bMan = new Bembidion_Population_Manager( m_ALandscape );
		m_AManager = bMan;
		g_PopulationManagerList.SetPopulation(m_AManager, TOP_Bembidion);
	}

	if ( item9->GetSelection() == TOP_Hare) { // Hare
    THare_Population_Manager * hMan = new THare_Population_Manager( m_ALandscape );
    m_AManager = hMan;
	g_PopulationManagerList.SetPopulation(m_AManager, TOP_Hare);
  }

  if ( item9->GetSelection() == 5 ) {
    Partridge_Population_Manager * pMan = new Partridge_Population_Manager( m_ALandscape );
    m_AManager = pMan;
	g_PopulationManagerList.SetPopulation(m_AManager, TOP_Partridge);
  }

  if ( item9->GetSelection() == TOP_Goose) {
    Goose_Population_Manager * gMan = new Goose_Population_Manager( m_ALandscape );
    m_AManager = gMan;
	g_PopulationManagerList.SetPopulation(m_AManager, TOP_Goose);
	m_Hunter_Population_Manager = new Hunter_Population_Manager( m_ALandscape );
	g_PopulationManagerList.SetPopulation(m_Hunter_Population_Manager, TOP_Hunters);
  }

  if (item9->GetSelection( ) == TOP_RoeDeer)
  {
	  RoeDeer_Population_Manager * rMan = new RoeDeer_Population_Manager( m_ALandscape );
	  m_AManager = rMan;
	  g_PopulationManagerList.SetPopulation( m_AManager, TOP_RoeDeer );
  }
  if (item9->GetSelection( ) == TOP_Rabbit)
  {
	  Rabbit_Population_Manager * rbMan = new Rabbit_Population_Manager( m_ALandscape );
	  m_AManager = rbMan;
	  g_PopulationManagerList.SetPopulation( m_AManager, TOP_Rabbit );
  }

  if (item9->GetSelection() == TOP_Newt)
  {
	  Newt_Population_Manager * nMan = new Newt_Population_Manager(m_ALandscape);
	  m_AManager = nMan;
	  g_PopulationManagerList.SetPopulation(m_AManager, TOP_Newt);
  }

  if (item9->GetSelection() == TOP_Osmia)
  {
      Osmia_Population_Manager* osMan = new Osmia_Population_Manager(m_ALandscape);
      OsmiaParasitoid_Population_Manager* osPMan = new Osmia_Population_Manager(m_ALandscape);
      m_AManager = osMan;
      g_PopulationManagerList.SetPopulation(m_AManager, TOP_Osmia);
      g_PopulationManagerList.SetPopulation(osPMan, TOP_OsmiaParasitoid);
  }

  if (item9->GetSelection() == TOP_ApisRAM)
  {
	  // ApisRAM here
	  return false;
  }

  cout << item9->GetSelection() << "<n";
  if (item9->GetSelection() == TOP_OliveMoth)
  {
    cout << "Making Olive Moth<n";
    OliveMoth_Population_Manager * omMan = new OliveMoth_Population_Manager(m_ALandscape);
    m_AManager = omMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_OliveMoth);
  }

  m_AManager->SetNoProbesAndSpeciesSpecificFunctions(m_NoProbes);
  return true;
}

//------------------------------------------------------------------------------

int ALMaSSGUI::GetTimeToRun() {
  // Get the time to run in days
  const int daysinmonths[ 13 ] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
  int torun = 0;
  int month;
  // First the number of whatever
  int anum = item7->GetValue();
  // Now figure out whatever
  switch ( item8->GetSelection() ) {
    case 0: // Year
      torun = anum * 365;
    break;
    case 1: // Month
      if ( m_SimInitiated ) month = m_ALandscape->SupplyMonth(); else
        month = 1; // if we have not started it must be January
      for ( int i = 0; i < anum; i++ ) {
        torun += daysinmonths[ month ];
        if ( ++month == 13 ) month = 1;
      }
    break;
    case 2: // Day
      torun = anum;
    break;
    case 3: // TimeStep
      torun = anum;
    break;
    case - 1:
      // No selection
    break;
  }
  return torun;
}

//______________________________________________________________________________
//_________________________________MyMap________________________________________
//______________________________________________________________________________

MyMap::MyMap( wxWindow * parent, wxWindowID id, const wxPoint & pos, const wxSize & size, long style ) :
     wxPanel( parent, id, pos, size, style, _T("Panel1") ) {
       wxIm = new wxImage();
       bool OK = wxIm->LoadFile( _T(__TITLEBMP), wxBITMAP_TYPE_BMP );
       if ( !OK ) {
        ofstream errorfile("ErrorFile.txt",ios::app);
        errorfile <<  "Title BMP missing" << endl;
        errorfile.close();
       }
       idata = wxIm->GetData();
       m_scalingW = 0;
       m_scalingH = 0;
       m_Zoom = 1;
       m_TrackOn = false;
}

//------------------------------------------------------------------------------

MyMap::~MyMap() {
	//delete wxIm;
}
//------------------------------------------------------------------------------


void MyMap::LeftMouseDown( wxMouseEvent & event ) 
{
	// If so simulation running then we get a crash if we try this, so exit gracefully
	if (!g_MainForm->GetSimInitiated()) return;
	if (event.ShiftDown())
	{
		// The shift key was pressed so zoom out
		ZoomOut(event.m_x, event.m_y);
	}
	else if (event.ControlDown())
	{
		// The control key was pressed so we want information about an animal
		FindAnimal(event.m_x, event.m_y);
	}
	else Centre(event.m_x, event.m_y);
	DrawLandscape();
	ShowBitmap2();
}
//------------------------------------------------------------------------------

void MyMap::RightMouseDown( wxMouseEvent & event ) 
{
	// If so simulation running then we get a crash if we try this, so exit gracefully
	if (!g_MainForm->GetSimInitiated()) return;
	if (event.ShiftDown())
	{
		// The shift key was pressed so zoom in
		ZoomIn( event.m_x, event.m_y );
		DrawLandscape();
		ShowBitmap2();
	} else DisplayMapInfo( event.m_x, event.m_y );
}
//------------------------------------------------------------------------------

void MyMap::DisplayMapInfo( int x, int y ) {
  // Must collect as much info as we can and display it on the memo pad
  // First calculate the actual x,y on the map
  int xx, yy;
  xx = (int) (m_TLx + x * m_scalingW);
  yy = (int) (m_TLy + y * m_scalingH);
  // Now clear the memo pad.
  m_MainForm->ClearPad();
  // The pad is written to using strings
  int apolyref = m_ALandscape->SupplyPolyRef(xx, yy);
  int aUMref = m_ALandscape->SupplyUMRef();
  wxString str = _T("USED FOR DEBUGGING");
  m_MainForm->AddToPad( str );
  str = _T("YOU CAN GET WHATEVER YOU");
  m_MainForm->AddToPad( str );
  str = _T("WANT IN TERMS OF INFORMATION");
  m_MainForm->AddToPad( str );
  str = _T("PRESENTED HERE");
  m_MainForm->AddToPad( str );
  str = _T("x: ");
  str += wxString::Format( _T("%d"), xx );
  m_MainForm->AddToPad( str );
  str = _T("y: ");
  str += wxString::Format( _T("%d"), yy );
  m_MainForm->AddToPad( str );
  str = _T("Polygon ref: ");
  str += wxString::Format(_T("%d"), apolyref);
  m_MainForm->AddToPad(str);
  str = _T("Unsprayed M. ref: ");
  str += wxString::Format(_T("%d"), aUMref);
  m_MainForm->AddToPad(str);
  str = _T("Element Type: ");
  std::string astr = m_ALandscape->PolytypeToString( m_ALandscape->SupplyElementType( apolyref ));
  wxString str2( astr.c_str(), wxConvUTF8);
  str.Append( str2 );
  m_MainForm->AddToPad(str);
  str = _T("Polygon area (ha): ");
  double polyarea = m_ALandscape->SupplyPolygonArea(apolyref)/10000.0;
  str += wxString::Format(_T("%.4f"), polyarea);
  m_MainForm->AddToPad(str);
  str = _T("Veg. Type: ");
  astr = m_ALandscape->VegtypeToString( m_ALandscape->SupplyVegType( apolyref )).c_str();
  str2.Clear();
  str2 = wxString::FromUTF8(astr.c_str());
  str.Append( str2  );
  m_MainForm->AddToPad( str );
  str = _T("Veg. Total Biomass: ");
  str += wxString::Format( _T("%g"), m_ALandscape->SupplyVegBiomass( apolyref ) );
  m_MainForm->AddToPad( str );
  str = _T("LAI Green: ");
  str += wxString::Format( _T("%g"), m_ALandscape->SupplyLAGreen( apolyref ) );
  m_MainForm->AddToPad( str );
  str = _T("Veg. Dead Biomass: ");
  str += wxString::Format( _T("%g"), m_ALandscape->SupplyVegBiomass( apolyref ) - m_ALandscape->SupplyGreenBiomass( apolyref ) );
  m_MainForm->AddToPad( str );
  str = _T("Veg. Height: ");
  str += wxString::Format(_T("%g"), m_ALandscape->SupplyVegHeight(apolyref));
  m_MainForm->AddToPad(str);
  str = _T("Veg. Digestibility: ");
  str += wxString::Format(_T("%g"), m_ALandscape  ->SupplyVegDigestibility(apolyref));
  m_MainForm->AddToPad(str);
  str = _T("Veg. Density: ");
  str += wxString::Format( _T("%d"), m_ALandscape->SupplyVegDensity( apolyref ) );
  m_MainForm->AddToPad( str );
  str = _T("Veg. Cover: ");
  str += wxString::Format( _T("%g"), m_ALandscape->SupplyVegCover( apolyref ) );
  m_MainForm->AddToPad( str );
  str = _T("Weed Biomass: ");
  str += wxString::Format( _T("%g"), m_ALandscape->SupplyWeedBiomass( apolyref ) );
  m_MainForm->AddToPad( str );
  str = _T("Insect Biomass: ");
  str += wxString::Format( _T("%g"), m_ALandscape->SupplyInsects( apolyref ) );
  m_MainForm->AddToPad( str );
  str = _T("Is Grazed: ");
  int graze = m_ALandscape->SupplyGrazingPressure(apolyref);
  str += wxString::Format(_T("%d"), graze);
  m_MainForm->AddToPad(str);
  str = _T("Is Patchy: ");
  if ( m_ALandscape->SupplyVegPatchy( apolyref ) ) str.Append( _T("True") ); else
    str.Append( _T("False" ));
  m_MainForm->AddToPad(str);
  str = _T("Centroid x ");
  str += wxString::Format(_T("%d"), m_ALandscape->SupplyCentroidX(apolyref));
  m_MainForm->AddToPad(str);
  str = _T("Centroid y: ");
  str += wxString::Format(_T("%d"), m_ALandscape->SupplyCentroidY(apolyref));
  m_MainForm->AddToPad(str);

  str = _T("Veg Phase ");
  str += wxString::Format(_T("%d"), m_ALandscape->SupplyVegPhase(apolyref));
  m_MainForm->AddToPad(str);

  str = _T("Last Treatment ");
  int temp = 0;
  str += wxString::Format(_T("%d"), m_ALandscape->SupplyLastTreatment(apolyref, &temp));
  m_MainForm->AddToPad(str);

  str = _T("Latest sown crop ");
  str += wxString::Format(_T("%d"), m_ALandscape->SupplyLastSownVeg(apolyref));
  m_MainForm->AddToPad(str);


  str = _T("Valid x ");
  str += wxString::Format(_T("%d"), m_ALandscape->SupplyValidX(apolyref));
  m_MainForm->AddToPad(str);
  str = _T("Valid y: ");
  str += wxString::Format(_T("%d"), m_ALandscape->SupplyValidY(apolyref));
  m_MainForm->AddToPad(str);

  str = _T("Farm Owner Ref: ");
  int fref = m_ALandscape->SupplyFarmOwner( apolyref );
  str += wxString::Format( _T("%d"), fref );
  m_MainForm->AddToPad( str );
  str = _T("Farm Area: ");
  if (fref!=-1) {
	  str += wxString::Format( _T("%.1f"), m_ALandscape->SupplyFarmArea( apolyref )/10000.0 );
  }
  else str +=  _T(" N/A");
  m_MainForm->AddToPad( str );
  m_MainForm->AddToPad( _T("") );

 /*
  // Pollen & nectar data
  str = _T("Pollen g/m2  ");
  str += wxString::Format(_T("%6.2g"), round((m_ALandscape->SupplyPollen(apolyref).m_quality*10000)) / 10000.0);
  m_MainForm->AddToPad(str);
  str = _T("Total Pollen ");
  str += wxString::Format(_T("%6.2g"), round((m_ALandscape->SupplyTotalPollen(apolyref) * 10000)) / 10000.0);
  m_MainForm->AddToPad(str);
  str = _T("Nectar g/m2 ");
  str += wxString::Format(_T("%6.2g"), round((m_ALandscape->SupplyNectar(apolyref).m_quality * 10000)) / 10000.0);
  m_MainForm->AddToPad(str);
  str = _T("Total Necta ");
  str += wxString::Format(_T("%6.2g"), round((m_ALandscape->SupplyTotalNectar(apolyref) * 10000)) / 10000.0);
  m_MainForm->AddToPad(str);



  m_MainForm->AddToPad(_T(""));
  str = _T("NEXT LINE ONLY WORKS WITH");
  m_MainForm->AddToPad( str );
  str = _T("THE PESTICIDE ENGINE");
  m_MainForm->AddToPad( str );
  if (l_pest_enable_pesticide_engine.value())
  {
	  str = _T("Pesticide Load: ");
	  PlantProtectionProducts ppp;
	  if (m_MainForm->item15->GetSelection() == 4)  ppp = PlantProtectionProducts(m_MainForm->m_subselectionspin->GetValue()-1);
	  else ppp = ppp_1;
	  str += wxString::Format(_T("%g"), m_ALandscape->SupplyPesticide(xx, yy, ppp ));
  }
  else str += _T("Temporarily unavailable");
  m_MainForm->AddToPad( str );
  // Make a new line
  m_MainForm->AddToPad( (wxChar*)"" );

  m_ddegs = m_ALandscape->SupplyDayDegrees(apolyref);
  str = _T("Current Day Degrees");
  m_MainForm->AddToPad( str );
  str = wxString::Format( _T("%.1f"), m_ddegs);
  m_MainForm->AddToPad( str );
  // Make a new line
  str.Clear();
  m_MainForm->AddToPad( str );
  // Some info for goose management modelling
  str = _T("Openness: ");
  str += wxString::Format(_T("%d"), m_ALandscape->SupplyOpenness(apolyref));
  m_MainForm->AddToPad(str);

  str = _T("Grain food density in Kj per m2: ");
  double goosefood = m_ALandscape->SupplyBirdSeedForage(apolyref);
  str += wxString::Format(_T("%.3f"), goosefood);
  m_MainForm->AddToPad(str);

  str = _T("Grazing food density in Kj per m2: ");
  goosefood = m_ALandscape->GetActualGooseGrazingForage(apolyref,gs_Pinkfoot);
  str += wxString::Format(_T("%.3f"), goosefood);
  m_MainForm->AddToPad(str);

  str = _T("Maximum goose numbers (all geese): ");
  int goosenums = m_ALandscape->GetGooseNumbers(apolyref);
  str += wxString::Format(_T("%d"), goosenums);
  m_MainForm->AddToPad(str);
  
  str = _T("Max. goose density in geese/ha: ");
  str += wxString::Format(_T("%.4f"), (double)goosenums / polyarea);
  m_MainForm->AddToPad(str);

  m_MainForm->AddToPad(_T(""));
  str = _T("Hare Forage: ");
  double forage = m_ALandscape->GetHareFoodQuality(apolyref);
  str += wxString::Format(_T("%.4f"), forage);
  m_MainForm->AddToPad(str);
  */
}

//------------------------------------------------------------------------------

void MyMap::DrawLandscape() {
	float x, y;
	RodenticidePredators_Population_Manager* RPM = NULL;
	int xx, yy;
	x = m_TLx;
	if (cfg_rodenticide_enable.value()) RPM = m_ALandscape->SupplyRodenticidePredatoryManager();
	// show pesticide load ?
	if (m_MainForm->item15->GetSelection() == 4)
	{
		PlantProtectionProducts ppp = PlantProtectionProducts(m_MainForm->m_subselectionspin->GetValue()-1);
		for (int i = 0; i < __MAPSIZE; i++)
		{
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++)
			{
				yy = (int)y;
				if (l_pest_enable_pesticide_engine.value())
				{
					int col = (int)(m_ALandscape->SupplyPesticide(xx, yy, ppp) * 250);
					if (col > 250) col = 250;
					idata[3 * (j * __MAPSIZE + i)]       = col;
					idata[(3 * (j * __MAPSIZE + i)) + 1] = 0;
					idata[(3 * (j * __MAPSIZE + i)) + 2] = 0;
				}
				else if (cfg_rodenticide_enable.value()) // For rodenticide
				{
					int rodenticide = rodenticide = (int)(m_ALandscape->SupplyRodenticide(xx, yy) * 250);
					if (rodenticide > 255) rodenticide = 255;
					int col = rodenticide;
					idata[3 * (j * __MAPSIZE + i)] = col;
					idata[(3 * (j * __MAPSIZE + i)) + 1] = 0;
					idata[(3 * (j * __MAPSIZE + i)) + 2] = col;
					// Draw predator territories				
					yy = (int)y;
					if (idata[3 * (j * __MAPSIZE + i)] < 10)
					{
						col = RPM->HowManyTerritoriesHere(xx, yy);
						idata[3 * (j * __MAPSIZE + i)] += col;
						idata[(3 * (j * __MAPSIZE + i)) + 1] += 64 * col;
						idata[(3 * (j * __MAPSIZE + i)) + 2] += 24 * col;
					}
				}
				else
				{
					idata[3 * (j * __MAPSIZE + i)] = 0;
					idata[(3 * (j * __MAPSIZE + i)) + 1] = 0;
					idata[(3 * (j * __MAPSIZE + i)) + 2] = 0;
				}
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else  if (m_MainForm->item15->GetSelection() == 2) {
		for (int i = 0; i < __MAPSIZE; i++) {
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++) {
				yy = (int)y;
				int Biomass = (int)m_ALandscape->SupplyVegBiomass(xx, yy);
				if (Biomass > 250) Biomass = 250;
				int col = Biomass >> 0;
				idata[3 * (j * __MAPSIZE + i)] = 64;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = col;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = 64;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else if (m_MainForm->item15->GetSelection() == 3) {
		// Show farm ownership
		for (int i = 0; i < __MAPSIZE; i++) {
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++) {
				yy = (int)y;
				int owner;
				owner = m_ALandscape->SupplyFarmOwnerIndex(xx, yy);
				if (owner != -1) {
					int red, green, blue;
					red = ((owner * 17) % 255);
					green = (red + (owner * 19) % 255);
					blue = (green + (owner * 29) % 255);
					idata[3 * (j * __MAPSIZE + i)] = red;
					idata[(3 * (j * __MAPSIZE + i)) + 1] = green;
					idata[(3 * (j * __MAPSIZE + i)) + 2] = blue;
				}
				else {
					idata[3 * (j * __MAPSIZE + i)] = 0;
					idata[(3 * (j * __MAPSIZE + i)) + 1] = 0;
					idata[(3 * (j * __MAPSIZE + i)) + 2] = 0;
				}
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else if (m_MainForm->item15->GetSelection() == 1) {
		// tov show
		for (int i = 0; i < __MAPSIZE; i++) {
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++) {
				yy = (int)y;
				unsigned ref = m_ALandscape->SupplyVegType(xx, yy);
				int red = ((ref * 17) % 255);
				int green = (red + (ref * 19) % 255);
				int blue = (green + (ref * 29) % 255);
				idata[3 * (j * __MAPSIZE + i)] = red;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = green;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = blue;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else if (m_MainForm->item15->GetSelection() == 5)  // Goose Numbers
	{
		for (int i = 0; i < __MAPSIZE; i++)
		{
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++)
			{
				yy = (int)y;
				int goosenumbers = m_ALandscape->GetGooseNumbers(xx, yy);
				if (goosenumbers > 255) goosenumbers = 255;
				idata[3 * (j * __MAPSIZE + i)] = 32;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = 32;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = goosenumbers;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else if (m_MainForm->item15->GetSelection() == 6)  // Goose Food Density
	{
		for (int i = 0; i < __MAPSIZE; i++)
		{
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++)
			{
				yy = (int)y;
				double food = m_ALandscape->SupplyBirdSeedForage(xx, yy) * 17.7 + m_ALandscape->GetActualGooseGrazingForage(xx, yy, gs_Pinkfoot);
				if (food > 255) food = 255;
				idata[3 * (j * __MAPSIZE + i)] = food;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = food;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = food;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else if (m_MainForm->item15->GetSelection() == 7)  // Goose GrainFood Density
	{
		for (int i = 0; i < __MAPSIZE; i++)
		{
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++)
			{
				yy = (int)y;
				double food = m_ALandscape->SupplyBirdSeedForage(xx, yy) * 2;
				if (food > 255) food = 255;
				idata[3 * (j * __MAPSIZE + i)] = 32;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = food;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = food;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else if (m_MainForm->item15->GetSelection() == 8)  // Goose Grazing
	{
		for (int i = 0; i < __MAPSIZE; i++)
		{
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++)
			{
				yy = (int)y;
				double food = m_ALandscape->GetActualGooseGrazingForage(xx, yy, gs_Pinkfoot) * 4;
				if (food > 255) food = 255;
				idata[3 * (j * __MAPSIZE + i)] = 64;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = food;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = 32;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else if (m_MainForm->item15->GetSelection() == 9)  // Soils
	{
		for (int i = 0; i < __MAPSIZE; i++)
		{
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++)
			{
				yy = (int)y;
				double soil = m_ALandscape->SupplySoilTypeR(xx, yy) * 16;
				if (soil > 255) soil = 255;
				idata[3 * (j * __MAPSIZE + i)] = 64;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = soil;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = soil;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else if (m_MainForm->item15->GetSelection() == 10)  // Hunter mapping
	{
		// First we draw the standard landscape
		for (int i = 0; i < __MAPSIZE; i++) {
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++) {
				yy = (int)y;
				// Note that because of the way the data is stored, we have to reverse
				// the x,y co-ords
				unsigned ref = m_ALandscape->SupplyElementType(xx, yy);
				if (ref == 24)  // 24 is tole_Building
				{
					if (m_ALandscape->SupplyCountryDesig(xx, yy) == 1) // Country residence
					{
						idata[3 * (j * __MAPSIZE + i)] = 255;
						idata[(3 * (j * __MAPSIZE + i)) + 1] = 0;
						idata[(3 * (j * __MAPSIZE + i)) + 2] = 0;
					}
					else
					{
						idata[3 * (j * __MAPSIZE + i)] = 0;
						idata[(3 * (j * __MAPSIZE + i)) + 1] = 0;
						idata[(3 * (j * __MAPSIZE + i)) + 2] = 0;
					}
				}
				else
				{
					if (ref > 67) ref = 67; // The max number of colours
					idata[3 * (j * __MAPSIZE + i)] = m_MainForm->ColoursRed[ref];
					idata[(3 * (j * __MAPSIZE + i)) + 1] = m_MainForm->ColoursGreen[ref];
					idata[(3 * (j * __MAPSIZE + i)) + 2] = m_MainForm->ColoursBlue[ref];
				}
				y += m_scalingH;
			}
			x += m_scalingW;
		}
		// Next we add the hunter dots
		unsigned noInList = m_MainForm->m_Hunter_Population_Manager->SupplyListSize(0);
		for (unsigned aHunter = 0; aHunter < noInList; aHunter++)
		{
			APoint pt = m_MainForm->m_Hunter_Population_Manager->GetHunterHome(aHunter, 0);
			if (pt.m_x < 0 || pt.m_y < 0 || pt.m_x >= m_ALandscape->SupplySimAreaWidth() || pt.m_y >= m_ALandscape->SupplySimAreaHeight())
			{
				pt.m_x = 0;
				pt.m_y = 0;
			}
			Spot(0, pt.m_x, pt.m_y);
			// Here we only show the first hunter hunting location
			pt = m_MainForm->m_Hunter_Population_Manager->GetHunterHuntLoc(aHunter, 0,0);
			Spot(1, pt.m_x, pt.m_y);
		}
	}
	else  if (m_MainForm->item15->GetSelection() == 11) {
		for (int i = 0; i < __MAPSIZE; i++) {
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++) {
				yy = (int)y;
				int Pollen = int(m_ALandscape->SupplyPollen(xx, yy).m_quantity*1000);
				if (Pollen > 250) Pollen = 250; 
				int col = Pollen >> 0;
				idata[3 * (j * __MAPSIZE + i)] = 0;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = col;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = col;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else  if (m_MainForm->item15->GetSelection() == 12) {
		for (int i = 0; i < __MAPSIZE; i++) {
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++) {
				yy = (int)y;
				int Nectar = (int)m_ALandscape->SupplyNectar(xx, yy).m_quantity * 1000;
				if (Nectar > 250) Nectar = 250;
				int col = Nectar >> 0;
				idata[3 * (j * __MAPSIZE + i)] = col;
				idata[(3 * (j * __MAPSIZE + i)) + 1] = col;
				idata[(3 * (j * __MAPSIZE + i)) + 2] = 0;
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
	else {
		// default
		for (int i = 0; i < __MAPSIZE; i++) {
			y = m_TLy;
			xx = (int)x;
			for (int j = 0; j < __MAPSIZE; j++) {
				yy = (int)y;
				// Note that because of the way the data is stored, we have to reverse
				// the x,y co-ords
				unsigned ref = m_ALandscape->SupplyElementType(xx, yy);
				if (ref == 24)  // 24 is tole_Building
				{
					if (m_ALandscape->SupplyCountryDesig(xx, yy) == 1) // Country residence
					{
						idata[3 * (j * __MAPSIZE + i)] = 255;
						idata[(3 * (j * __MAPSIZE + i)) + 1] = 0;
						idata[(3 * (j * __MAPSIZE + i)) + 2] = 0;
					}
					else
					{
						idata[3 * (j * __MAPSIZE + i)] = 0;
						idata[(3 * (j * __MAPSIZE + i)) + 1] = 0;
						idata[(3 * (j * __MAPSIZE + i)) + 2] = 0;
					}
				}
				else
				{
					if (ref > 67) ref = 67; // The max number of colours
					idata[3 * (j * __MAPSIZE + i)] = m_MainForm->ColoursRed[ref];
					idata[(3 * (j * __MAPSIZE + i)) + 1] = m_MainForm->ColoursGreen[ref];
					idata[(3 * (j * __MAPSIZE + i)) + 2] = m_MainForm->ColoursBlue[ref];
				}
				if (cfg_rodenticide_enable.value()) // For rodenticide
				{
					// Draw predator territories				
					yy = (int)y;
					int col = RPM->HowManyTerritoriesHere(xx, yy);
					idata[3 * (j * __MAPSIZE + i)] += 64 * col;
					idata[(3 * (j * __MAPSIZE + i)) + 1] += col;
					idata[(3 * (j * __MAPSIZE + i)) + 2] += col;
				}
				y += m_scalingH;
			}
			x += m_scalingW;
		}
	}
}

//---------------------------------------------------------------------------

void MyMap::SetSimExtent( int Width, int Height ) {
  // This function must be called before the form can be used
  m_SimW = Width;
  m_SimH = Height;
  m_TLx = 0;
  m_TLy = 0;
  m_BRx = m_SimW;
  m_BRy = m_SimH;
  m_Width = m_SimW;
  m_Height = m_SimH;
  SetScale( m_SimW, m_SimH );
}

//---------------------------------------------------------------------------

void MyMap::SetScale( int Width, int Height ) {
  // This function must be called before the form can be used
  // It determines the multiplier for the display i.e how many pixels map to
  // one unit of the track area
  m_scalingW = Width / ( float )__MAPSIZE;
  m_scalingH = Height / ( float )__MAPSIZE;
}

//---------------------------------------------------------------------------

void MyMap::SetUpMapImage() {
  wxIm->LoadFile( _T( __TITLEBMP ), wxBITMAP_TYPE_BMP );
  idata = wxIm->GetData();
}

//---------------------------------------------------------------------------

void MyMap::ShowBitmap( wxPaintEvent & event ) {
  wxWindowDC mapDC( this );
  wxBitmap b2( *(wxIm), -1 );
  mapDC.DrawBitmap( b2, 0, 0, false );
  event.Skip();
}

//---------------------------------------------------------------------------

void MyMap::ShowBitmap2() {
  wxWindowDC mapDC( this );
  wxBitmap b2( *(wxIm), -1 );
  mapDC.DrawBitmap( b2, 0, 0, false );
}

//---------------------------------------------------------------------------

void MyMap::SetUpColours() {
  wxColour aColour;

  aColour = wxColour( _T("CORAL") );
  m_MainForm->ColoursRed[ tole_Hedges ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Hedges ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Hedges ] = aColour.Blue();
  aColour = wxColour( _T("GOLD") );
  m_MainForm->ColoursRed[ tole_RoadsideVerge ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_RoadsideVerge ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_RoadsideVerge ] = aColour.Blue();
  aColour = wxColour( _T("DARK ORCHID") );
  m_MainForm->ColoursRed[ tole_Railway ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Railway ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Railway ] = aColour.Blue();
  aColour = wxColour( _T("PALE GREEN" ));
  m_MainForm->ColoursRed[ tole_FieldBoundary ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_FieldBoundary ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_FieldBoundary ] = aColour.Blue();
  aColour = wxColour( _T("SIENNA" ));
  m_MainForm->ColoursRed[ tole_Marsh ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Marsh ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Marsh ] = aColour.Blue();
  aColour = wxColour( _T("THISTLE") );
  m_MainForm->ColoursRed[ tole_Scrub ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Scrub ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Scrub ] = aColour.Blue();
  aColour = wxColour( _T("WHEAT") );
  m_MainForm->ColoursRed[ tole_Field ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Field ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Field ] = aColour.Blue();
  aColour = wxColour( _T("MEDIUM SEA GREEN" ));
  m_MainForm->ColoursRed[ tole_PermPastureLowYield ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_PermPastureLowYield ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_PermPastureLowYield ] = aColour.Blue();
  aColour = wxColour( _T("SPRING GREEN") );
  m_MainForm->ColoursRed[ tole_PermPastureTussocky ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_PermPastureTussocky ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_PermPastureTussocky ] = aColour.Blue();
  aColour = wxColour( _T("KHAKI") );
  m_MainForm->ColoursRed[ tole_PermanentSetaside ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_PermanentSetaside ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_PermanentSetaside ] = aColour.Blue();
  aColour = wxColour( _T("MEDIUM FOREST GREEN") );
  m_MainForm->ColoursRed[ tole_PermPasture ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_PermPasture ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_PermPasture ] = aColour.Blue();
  aColour = wxColour( _T("YELLOW" ));
  m_MainForm->ColoursRed[ tole_NaturalGrassDry ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_NaturalGrassDry ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_NaturalGrassDry ] = aColour.Blue();
  aColour = wxColour( _T("YELLOW GREEN") );
  m_MainForm->ColoursRed[ tole_RiversidePlants ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_RiversidePlants ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_RiversidePlants ] = aColour.Blue();
  aColour = wxColour( _T("FIREBRICK" ));
  m_MainForm->ColoursRed[ tole_PitDisused ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_PitDisused ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_PitDisused ] = aColour.Blue();
  aColour = wxColour( _T("SEA GREEN" ));
  m_MainForm->ColoursRed[ tole_RiversideTrees ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_RiversideTrees ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_RiversideTrees ] = aColour.Blue();
  aColour = wxColour( _T("FOREST GREEN") );
  m_MainForm->ColoursRed[ tole_DeciduousForest ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_DeciduousForest ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_DeciduousForest ] = aColour.Blue();
  aColour = wxColour( _T("DARK OLIVE GREEN") );
  m_MainForm->ColoursRed[ tole_ConiferousForest ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_ConiferousForest ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_ConiferousForest ] = aColour.Blue();
  aColour = wxColour( _T("DARK GREEN") );
  m_MainForm->ColoursRed[ tole_MixedForest ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_MixedForest ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_MixedForest ] = aColour.Blue();
  aColour = wxColour( _T("GREEN YELLOW") );
  m_MainForm->ColoursRed[ tole_YoungForest ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_YoungForest ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_YoungForest ] = aColour.Blue();
  aColour = wxColour( _T("GREY") );
  m_MainForm->ColoursRed[ tole_StoneWall ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_StoneWall ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_StoneWall ] = aColour.Blue();
  aColour = wxColour( _T("MEDIUM ORCHID") );
  m_MainForm->ColoursRed[ tole_Garden ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Garden ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Garden ] = aColour.Blue();
  aColour = wxColour( _T("ORCHID" ));
  m_MainForm->ColoursRed[ tole_Track ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Track ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Track ] = aColour.Blue();
  aColour = wxColour( _T("INDIAN RED") );
  m_MainForm->ColoursRed[ tole_SmallRoad ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_SmallRoad ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_SmallRoad ] = aColour.Blue();
  aColour = wxColour( _T("RED" ));
  m_MainForm->ColoursRed[ tole_LargeRoad ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_LargeRoad ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_LargeRoad ] = aColour.Blue();
  aColour = wxColour( _T("BLACK") );
  m_MainForm->ColoursRed[ tole_Building ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Building ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Building ] = aColour.Blue();
  aColour = wxColour( _T("BROWN") );
  m_MainForm->ColoursRed[ tole_Saltmarsh ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Saltmarsh ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Saltmarsh ] = aColour.Blue();
  aColour = wxColour( _T("DARK GREY") );
  m_MainForm->ColoursRed[ tole_ActivePit ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_ActivePit ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_ActivePit ] = aColour.Blue();
  aColour = wxColour( _T("BLUE") );
  m_MainForm->ColoursRed[ tole_Freshwater ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Freshwater ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Freshwater ] = aColour.Blue();
  aColour = wxColour( _T("CORNFLOWER BLUE") );
  m_MainForm->ColoursRed[ tole_River ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_River ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_River ] = aColour.Blue();
  aColour = wxColour( _T("GOLDENROD") );
  m_MainForm->ColoursRed[ tole_Coast ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Coast ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Coast ] = aColour.Blue();
  aColour = wxColour( _T("AQUAMARINE") );
  m_MainForm->ColoursRed[ tole_Saltwater ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Saltwater ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Saltwater ] = aColour.Blue();
  aColour = wxColour( _T("GREEN") );
  m_MainForm->ColoursRed[ tole_HedgeBank ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_HedgeBank ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_HedgeBank ] = aColour.Blue();
  aColour = wxColour( _T("BLUE VIOLET") );
  m_MainForm->ColoursRed[ tole_Heath ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Heath ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Heath ] = aColour.Blue();
  aColour = wxColour( _T("PURPLE") );
  m_MainForm->ColoursRed[ tole_Orchard ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Orchard ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Orchard ] = aColour.Blue();
  aColour = wxColour( _T("DARK SLATE BLUE" ));
  m_MainForm->ColoursRed[ tole_OrchardBand ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_OrchardBand ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_OrchardBand ] = aColour.Blue();
  aColour = wxColour( _T("MAROON" ));
  m_MainForm->ColoursRed[ tole_MownGrass ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_MownGrass ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_MownGrass ] = aColour.Blue();
  aColour = wxColour( _T("MEDIUM SPRING GREEN" ));
  m_MainForm->ColoursRed[ tole_NaturalGrassWet ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_NaturalGrassWet ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_NaturalGrassWet ] = aColour.Blue();
  aColour = wxColour( _T("LIGHT GREY" ));
  m_MainForm->ColoursRed[ tole_MetalledPath ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_MetalledPath ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_MetalledPath ] = aColour.Blue();
  aColour = wxColour( _T("DARK SLATE GREY" ));
  m_MainForm->ColoursRed[ tole_RoadsideSlope ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_RoadsideSlope] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_RoadsideSlope ] = aColour.Blue();
  aColour = wxColour( _T("DIM GREY" ));
  m_MainForm->ColoursRed[ tole_HeritageSite ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_HeritageSite ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_HeritageSite ] = aColour.Blue();
  aColour = wxColour( _T("CADET BLUE") );
  m_MainForm->ColoursRed[ tole_Stream ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Stream ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Stream ] = aColour.Blue();
  aColour = wxColour( _T("DARK TURQUOISE") );
  m_MainForm->ColoursRed[ tole_Carpark ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Carpark ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Carpark ] = aColour.Blue();
  aColour = wxColour( _T("MEDIUM AQUAMARINE" ));
  m_MainForm->ColoursRed[ tole_Churchyard ] = aColour.Red();
  m_MainForm->ColoursGreen[ tole_Churchyard ] = aColour.Green();
  m_MainForm->ColoursBlue[ tole_Churchyard ] = aColour.Blue();
  aColour = wxColour( _T( "ORANGE RED" ) );
  m_MainForm->ColoursRed[tole_Wasteland] = aColour.Red();
  m_MainForm->ColoursGreen[tole_Wasteland] = aColour.Green();
  m_MainForm->ColoursBlue[tole_Wasteland] = aColour.Blue();
  aColour = wxColour(_T("MEDIUM SEA GREEN"));
  m_MainForm->ColoursRed[tole_IndividualTree] = aColour.Red();
  m_MainForm->ColoursGreen[tole_IndividualTree] = aColour.Green();
  m_MainForm->ColoursBlue[tole_IndividualTree] = aColour.Blue();
  aColour = wxColour( _T("LIGHT BLUE" ));
  m_MainForm->ColoursRed[tole_WindTurbine] = aColour.Red();
  m_MainForm->ColoursGreen[tole_WindTurbine] = aColour.Green();
  m_MainForm->ColoursBlue[tole_WindTurbine] = aColour.Blue();
  aColour = wxColour( _T("LIGHT STEEL BLUE" ));
  m_MainForm->ColoursRed[tole_Vildtager] = aColour.Red();
  m_MainForm->ColoursGreen[tole_Vildtager] = aColour.Green();
  m_MainForm->ColoursBlue[tole_Vildtager] = aColour.Blue();
  aColour = wxColour( _T("MAGENTA") );
  m_MainForm->ColoursRed[tole_PlantNursery] = aColour.Red();
  m_MainForm->ColoursGreen[tole_PlantNursery] = aColour.Green();
  m_MainForm->ColoursBlue[tole_PlantNursery] = aColour.Blue();
  aColour = wxColour(_T("PINK"));
  m_MainForm->ColoursRed[tole_WoodyEnergyCrop] = aColour.Red();
  m_MainForm->ColoursGreen[tole_WoodyEnergyCrop] = aColour.Green();
  m_MainForm->ColoursBlue[tole_WoodyEnergyCrop] = aColour.Blue();
  aColour = wxColour(_T("PLUM"));
  m_MainForm->ColoursRed[tole_WoodlandMargin] = aColour.Red();
  m_MainForm->ColoursGreen[tole_WoodlandMargin] = aColour.Green();
  m_MainForm->ColoursBlue[tole_WoodlandMargin] = aColour.Blue();
  aColour = wxColour( _T("MAROON" ));
  m_MainForm->ColoursRed[tole_Pylon] = aColour.Red();
  m_MainForm->ColoursGreen[tole_Pylon] = aColour.Green();
  m_MainForm->ColoursBlue[tole_Pylon] = aColour.Blue();
  aColour = wxColour(_T("MEDIUM BLUE"));
  m_MainForm->ColoursRed[tole_Pond] = aColour.Red();
  m_MainForm->ColoursGreen[tole_Pond] = aColour.Green();
  m_MainForm->ColoursBlue[tole_Pond] = aColour.Blue();
  aColour = wxColour(_T("STEEL BLUE"));
  m_MainForm->ColoursRed[tole_FishFarm] = aColour.Red();
  m_MainForm->ColoursGreen[tole_FishFarm] = aColour.Green();
  m_MainForm->ColoursBlue[tole_FishFarm] = aColour.Blue();
  /*
  aColour = wxColour(_T("MEDIUM TURQUOISE"));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("MEDIUM SLATE BLUE" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("MEDIUM VIOLET RED") );
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("NAVY" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("ORANGE") );
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("ORANGE RED" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("MIDNIGHT BLUE") );
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("RED" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("SALMON" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("SKY BLUE" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("SLATE BLUE" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("STEEL BLUE" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("TAN" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("TURQUOISE") );
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("VIOLET" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  aColour = wxColour( _T("WHITE" ));
  m_MainForm->ColoursRed[ 99 ] = aColour.Red();
  m_MainForm->ColoursGreen[ 99 ] = aColour.Green();
  m_MainForm->ColoursBlue[ 99 ] = aColour.Blue();
  */
}

//---------------------------------------------------------------------------

void MyMap::Centre( int X, int Y ) {
  // First back transform X & Y by the scaling factors
  X = (int) (m_TLx + X * m_scalingW);
  Y = (int) (m_TLy + Y * m_scalingH);
  ChangeZoom( X, Y );
}

//---------------------------------------------------------------------------


void MyMap::ZoomIn( int X, int Y ) {
  // First back transform X & Y by the scaling factors
  X = (int) (m_TLx + X * m_scalingW);
  Y = (int) (m_TLy + Y * m_scalingH);
  // Zoom in
  int NewZoom = m_Zoom + m_Zoom;
  m_Width = m_SimW / NewZoom;
  m_Height = m_SimH / NewZoom;
  if (m_Width < 100) {
    m_Width = 100; // Don't zoom in smaller than 100m
  } else
    m_Zoom = NewZoom;
  ChangeZoom( X, Y );
}

//---------------------------------------------------------------------------


void MyMap::ZoomOut(int X, int Y) {
	// First back transform X & Y by the scaling factors
	X = (int)(m_TLx + X * m_scalingW);
	Y = (int)(m_TLy + Y * m_scalingH);
	// Zoom out
	m_Zoom /= 2;
	if (m_Zoom < 1) m_Zoom = 1; // don't go bigger than SimW
	m_Width = m_SimW / m_Zoom;
	m_Height = m_SimH / m_Zoom;
	ChangeZoom(X, Y);
}

//---------------------------------------------------------------------------

void MyMap::FindAnimal(int X, int Y) {
	// First back transform X & Y by the scaling factors
	X = (int)(m_TLx + X * m_scalingW);
	Y = (int)(m_TLy + Y * m_scalingH);
	/** 
	* We need to find the animal dot closest to the point where the mouse was left-clicked with Ctrl.
	* The first thing we can do is to eliminate anything that is not displayed.
	* After that we just need to trawl through the invidividuals to find one that is closest to this location.
	* All the heavy work is done by the population manager
	*/
	TAnimal* animal = NULL;
	TAnimal* animal_closest = NULL;
	double dist, dist2 = 99999999999999999999.0;
	if (m_MainForm->IsAnimalChecked(1)) {
		animal = m_MainForm->m_AManager->FindClosest(X, Y, 0);
		dist = sqrt(((animal->Supply_m_Location_x() - X)* (animal->Supply_m_Location_x() - X)) + ((animal->Supply_m_Location_y() - Y)* (animal->Supply_m_Location_y() - Y)));
		if (dist < dist2)
		{
			dist2 = dist;
			animal_closest = animal;
		}
	}
	if (m_MainForm->IsAnimalChecked(2)) {
		animal = m_MainForm->m_AManager->FindClosest(X, Y, 1);
		dist = sqrt(((animal->Supply_m_Location_x() - X)* (animal->Supply_m_Location_x() - X)) + ((animal->Supply_m_Location_y() - Y)* (animal->Supply_m_Location_y() - Y)));
		if (dist < dist2)
		{
			dist2 = dist;
			animal_closest = animal;
		}
	}
	if (m_MainForm->IsAnimalChecked(4)) {
		animal = m_MainForm->m_AManager->FindClosest(X, Y, 2);
		dist = sqrt(((animal->Supply_m_Location_x() - X)* (animal->Supply_m_Location_x() - X)) + ((animal->Supply_m_Location_y() - Y)* (animal->Supply_m_Location_y() - Y)));
		if (dist < dist2)
		{
			dist2 = dist;
			animal_closest = animal;
		}
	}
	if (m_MainForm->IsAnimalChecked(5)) {
		animal = m_MainForm->m_AManager->FindClosest(X, Y, 3);
		dist = sqrt(((animal->Supply_m_Location_x() - X)* (animal->Supply_m_Location_x() - X)) + ((animal->Supply_m_Location_y() - Y)* (animal->Supply_m_Location_y() - Y)));
		if (dist < dist2)
		{
			dist2 = dist;
			animal_closest = animal;
		}
	}
	if (m_MainForm->IsAnimalChecked(5)) {
		animal = m_MainForm->m_AManager->FindClosest(X, Y, 4);
		dist = sqrt(((animal->Supply_m_Location_x() - X)* (animal->Supply_m_Location_x() - X)) + ((animal->Supply_m_Location_y() - Y)* (animal->Supply_m_Location_y() - Y)));
		if (dist < dist2)
		{
			dist2 = dist;
			animal_closest = animal;
		}
	}
	if (animal_closest != NULL)
	{
		m_MainForm->ClearPad();
		// The pad is written to using strings
		
		wxString str = _T("ANIMAL INFORMATION AVAILABLE FROM THE TAnimal*");
		m_MainForm->AddToPad(str);
		str = _T("x: ");
		str += wxString::Format(_T("%d"), animal_closest->Supply_m_Location_x());
		m_MainForm->AddToPad(str);
		str = _T("y: ");
		str += wxString::Format(_T("%d"), animal_closest->Supply_m_Location_y());
		m_MainForm->AddToPad(str);
		str = _T("Polygon ref: ");
		str += wxString::Format(_T("%d"), animal_closest->SupplyPolygonRef());
		m_MainForm->AddToPad(str);
		str = _T("Current State No: ");
		str += wxString::Format(_T("%d"), animal_closest->GetCurrentStateNo());
		m_MainForm->AddToPad(str);
		str = _T("WhatState returns: ");
		str += wxString::Format(_T("%d"), animal_closest->WhatState());
		m_MainForm->AddToPad(str);
	}
}

//---------------------------------------------------------------------------

void MyMap::ChangeZoom( int X, int Y ) {
  // Now place the TL corner
  m_TLx = X - ( m_Width / 2 );
  if ( m_TLx > m_SimW - m_Width ) m_TLx = m_SimW - m_Width;
  if ( m_TLx < 0 ) m_TLx = 0;
  m_TLy = Y - ( m_Height / 2 );
  if ( m_TLy > m_SimH - m_Height ) m_TLy = m_SimH - m_Height;
  if ( m_TLy < 0 ) m_TLy = 0;
  // Place the BL corner
  m_BRx = m_TLx + m_Width;
  m_BRy = m_TLy + m_Height;
  SetScale( m_Width, m_Height );
  //  DrawLandscape();
  //  m_AManager->DisplayLocations();
  //  if (RadioButton1->Checked)  {
  //    PredatorManager->DisplayLocations();
  //  }
}

//---------------------------------------------------------------------------

void MyMap::Spot( int cref, int x, int y ) {
  if ( ( x >= m_TLx ) && ( x < m_BRx ) && ( y >= m_TLy ) && ( y < m_BRy ) ) {
    int i = (int) (( x - m_TLx ) / m_scalingW);
    int j = (int) (( y - m_TLy ) / m_scalingH);
	int row3 = j * __MAPSIZE;
	int row2 = row3 - __MAPSIZE;
	int row1 = row2 - __MAPSIZE;
	int row4 = row3 + __MAPSIZE;
	int row5 = row4 + __MAPSIZE;

	if (row1 < 1 ) return;
	if (row2 < 0 ) return;
	if (row5 > __MAPSIZE * (__MAPSIZE-1) ) return;
	if (row4 > __MAPSIZE * __MAPSIZE ) return;

    switch ( cref ) {
      case 0:
			// rather than loop I'm defining each pixel so I can draw shapes

  			idata[   3 * (( row1 + i +0) ) + 1 ]        = 0;
			idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 255;
			idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 255;

			idata[   3 * (( row1 + i +1) ) + 1 ]        = 0;
			idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 255;

			//idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row1 + i -1) ) + 1 ]        = 0;
			idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 255;

			//idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +0) ) + 1 ]        = 172;
			idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +2) ) + 1 ]        = 0;
			idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i -1) ) + 1 ]        = 64;
			idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i -2) ) + 1 ]        = 0;
			idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 255;


			idata[   3 * (( row3 + i +0) ) + 1 ]        = 32;
			idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 255;

			idata[   3 * (( row3 + i +1) ) + 1 ]        = 128;
			idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 255;

			idata[   3 * (( row3 + i +2) ) + 1 ]        = 0;
			idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row3 + i -1) ) + 1 ]        = 0;
			idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 255;

			idata[   3 * (( row3 + i -2) ) + 1 ]        = 0;
			idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 255;


			idata[   3 * (( row4 + i +0) ) + 1 ]        = 0;
			idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 245;
			idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 245;

			idata[   3 * (( row4 + i +1) ) + 1 ]        = 0;
			idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 250;
			idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 250;

			idata[   3 * (( row4 + i +2) ) + 1 ]        = 0;
			idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row4 + i -1) ) + 1 ]        = 0;
			idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 245;
			idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 245;

			idata[   3 * (( row4 + i -2) ) + 1 ]        = 0;
			idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 240;
			idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 240;


			idata[   3 * (( row5 + i +0) ) + 1 ]        = 0;
			idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 220;
			idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 220;

			idata[   3 * (( row5 + i +1) ) + 1 ]        = 0;
			idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 240;
			idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 240;

			//idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row5 + i -1) & (__MAPSIZE  *  __MAPSIZE) ) + 1 ]        = 0;
			idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 200;
			idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 200;

			//idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 1:
			// rather than loop I'm defining each pixel so I can draw shapes

  			idata[   3 * (( row1 + i +0) ) + 1 ]        = 255;
			idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
			idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 255;

			idata[   3 * (( row1 + i +1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 255;

			//idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row1 + i -1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 255;

			//idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +0) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
			idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i -1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
			idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i -2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 255;


			idata[   3 * (( row3 + i +0) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
			idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 255;

			idata[   3 * (( row3 + i +1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
			idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 255;

			idata[   3 * (( row3 + i +2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row3 + i -1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 255;

			idata[   3 * (( row3 + i -2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 255;


			idata[   3 * (( row4 + i +0) ) + 1 ]        = 245;
			idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 245;

			idata[   3 * (( row4 + i +1) ) + 1 ]        = 250;
			idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 255;

			idata[   3 * (( row4 + i +2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row4 + i -1) ) + 1 ]        = 245;
			idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 255;

			idata[   3 * (( row4 + i -2) ) + 1 ]        = 240;
			idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 255;


			idata[   3 * (( row5 + i +0) ) + 1 ]        = 220;
			idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 255;

			idata[   3 * (( row5 + i +1) ) + 1 ]        = 240;
			idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 255;

			//idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row5 + i -1) ) + 1 ]        = 200;
			idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 255;

			//idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 2:
			// rather than loop I'm defining each pixel so I can draw shapes

  			idata[   3 * (( row1 + i +0) ) + 1 ]        = 255;
			idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
			idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 0;

			idata[   3 * (( row1 + i +1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 0;

			//idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row1 + i -1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 0;

			//idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +0) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
			idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

			idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 0;

			idata[   3 * (( row2 + i -1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
			idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 64;

			idata[   3 * (( row2 + i -2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 0;


			idata[   3 * (( row3 + i +0) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
			idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 32;

			idata[   3 * (( row3 + i +1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
			idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

			idata[   3 * (( row3 + i +2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 0;

			idata[   3 * (( row3 + i -1) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 0;

			idata[   3 * (( row3 + i -2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 0;


			idata[   3 * (( row4 + i +0) ) + 1 ]        = 245;
			idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 0;

			idata[   3 * (( row4 + i +1) ) + 1 ]        = 250;
			idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 0;

			idata[   3 * (( row4 + i +2) ) + 1 ]        = 255;
			idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 0;

			idata[   3 * (( row4 + i -1) ) + 1 ]        = 245;
			idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 0;

			idata[   3 * (( row4 + i -2) ) + 1 ]        = 240;
			idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 0;


			idata[   3 * (( row5 + i +0) ) + 1 ]        = 220;
			idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 0;

			idata[   3 * (( row5 + i +1) ) + 1 ]        = 240;
			idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 0;

			//idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row5 + i -1) ) + 1 ]        = 200;
			idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 0;

			//idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 3:
			// rather than loop I'm defining each pixel so I can draw shapes

  			idata[   3 * (( row1 + i +0) ) + 2 ]        = 255;
			idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
			idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 0;

			idata[   3 * (( row1 + i +1) ) + 2 ]        = 255;
			idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 0;

			//idata[   3 * (( row1 + i +2) ) + 2 ]        = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;

			idata[   3 * (( row1 + i -1) ) + 2 ]        = 255;
			idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 0;

			//idata[   3 * (( row1 + i -2) ) + 2 ]        = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;

			idata[   3 * (( row2 + i +0) ) + 2 ]        = 255;
			idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
			idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;

			idata[   3 * (( row2 + i +1) ) + 2 ]        = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;

			idata[   3 * (( row2 + i +2) ) + 2 ]        = 255;
			idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 0;

			idata[   3 * (( row2 + i -1) ) + 2 ]        = 255;
			idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
			idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 64;

			idata[   3 * (( row2 + i -2) ) + 2 ]        = 255;
			idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 0;


			idata[   3 * (( row3 + i +0) ) + 2 ]        = 255;
			idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
			idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 32;

			idata[   3 * (( row3 + i +1) ) + 2 ]        = 255;
			idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
			idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 128;

			idata[   3 * (( row3 + i +2) ) + 2 ]        = 255;
			idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 0;

			idata[   3 * (( row3 + i -1) ) + 2 ]        = 255;
			idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 0;

			idata[   3 * (( row3 + i -2) ) + 2 ]        = 255;
			idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 0;


			idata[   3 * (( row4 + i +0) ) + 2 ]        = 245;
			idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 0;

			idata[   3 * (( row4 + i +1) ) + 2 ]        = 250;
			idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 0;

			idata[   3 * (( row4 + i +2) ) + 2 ]        = 255;
			idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 0;

			idata[   3 * (( row4 + i -1) ) + 2 ]        = 245;
			idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 0;

			idata[   3 * (( row4 + i -2) ) + 2 ]        = 240;
			idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 0;


			idata[   3 * (( row5 + i +0) ) + 2 ]        = 220;
			idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 0;

			idata[   3 * (( row5 + i +1) ) + 2 ]        = 240;
			idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 0;

			//idata[   3 * (( row5 + i +2) ) + 2 ]        = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;

			idata[   3 * (( row5 + i -1) ) + 2 ]        = 200;
			idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
			idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 0;

			//idata[   3 * (( row5 + i -2) ) + 2 ]        = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
			break;

      case 4:
			// rather than loop I'm defining each pixel so I can draw shapes

  			idata[   3 * (( row1 + i +0) ) ]       = 255;
			idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 0;
			idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 0;

			idata[   3 * (( row1 + i +1) ) ]       = 255;
			idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 0;

			//idata[   3 * (( row1 + i +2) ) ]       = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row1 + i -1) ) ]       = 255;
			idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 0;

			//idata[   3 * (( row1 + i -2) ) ]       = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +0) ) ]       = 255;
			idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

			idata[   3 * (( row2 + i +1) ) ]       = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +2) ) ]       = 255;
			idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 0;

			idata[   3 * (( row2 + i -1) ) ]       = 255;
			idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 64;
			idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 64;

			idata[   3 * (( row2 + i -2) ) ]       = 255;
			idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 0;


			idata[   3 * (( row3 + i +0) ) ]       = 255;
			idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 32;
			idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 32;

			idata[   3 * (( row3 + i +1) ) ]       = 255;
			idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 128;
			idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

			idata[   3 * (( row3 + i +2) ) ]       = 255;
			idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 0;

			idata[   3 * (( row3 + i -1) ) ]       = 255;
			idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 0;

			idata[   3 * (( row3 + i -2) ) ]       = 255;
			idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 0;


			idata[   3 * (( row4 + i +0) ) ]       = 245;
			idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 0;

			idata[   3 * (( row4 + i +1) ) ]       = 250;
			idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 0;

			idata[   3 * (( row4 + i +2) ) ]       = 255;
			idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 0;

			idata[   3 * (( row4 + i -1) ) ]       = 245;
			idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 0;

			idata[   3 * (( row4 + i -2) ) ]       = 240;
			idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 0;


			idata[   3 * (( row5 + i +0) ) ]       = 220;
			idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 0;

			idata[   3 * (( row5 + i +1) ) ]       = 240;
			idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 0;

			//idata[   3 * (( row5 + i +2) ) ]       = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row5 + i -1) ) ]       = 200;
			idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 0;
			idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 0;

			//idata[   3 * (( row5 + i -2) ) ]       = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
		break;
      case 5:
			// rather than loop I'm defining each pixel so I can draw shapes

  			idata[   3 * (( row1 + i +0) ) ]       = 255;
			idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 172;
			idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 128;

			idata[   3 * (( row1 + i +1) ) ]       = 255;
			idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 128;

			//idata[   3 * (( row1 + i +2) ) ]       = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;
			//idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row1 + i -1) ) ]       = 255;
			idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 128;

			//idata[   3 * (( row1 + i -2) ) ]       = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;
			//idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +0) ) ]       = 255;
			idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

			idata[   3 * (( row2 + i +1) ) ]       = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;
			idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

			idata[   3 * (( row2 + i +2) ) ]       = 255;
			idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 128;

			idata[   3 * (( row2 + i -1) ) ]       = 255;
			idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 128;

			idata[   3 * (( row2 + i -2) ) ]       = 255;
			idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 128;


			idata[   3 * (( row3 + i +0) ) ]       = 255;
			idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 128;

			idata[   3 * (( row3 + i +1) ) ]       = 255;
			idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

			idata[   3 * (( row3 + i +2) ) ]       = 255;
			idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 128;

			idata[   3 * (( row3 + i -1) ) ]       = 255;
			idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 128;

			idata[   3 * (( row3 + i -2) ) ]       = 255;
			idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 128;


			idata[   3 * (( row4 + i +0) ) ]       = 255;
			idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 128;

			idata[   3 * (( row4 + i +1) ) ]       = 255;
			idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 128;

			idata[   3 * (( row4 + i +2) ) ]       = 245;
			idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 128;

			idata[   3 * (( row4 + i -1) ) ]       = 245;
			idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 128;

			idata[   3 * (( row4 + i -2) ) ]       = 240;
			idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 128;


			idata[   3 * (( row5 + i +0) ) ]       = 220;
			idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 128;

			idata[   3 * (( row5 + i +1) ) ]       = 240;
			idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 128;

			//idata[   3 * (( row5 + i +2) ) ]       = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;
			//idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

			idata[   3 * (( row5 + i -1) ) ]       = 200;
			idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 172;
			idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 128;

			//idata[   3 * (( row5 + i -2) ) ]       = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
			//idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
		break;
      default:
        idata[ 3 * ( row1 + i ) ] = 200;
        idata[ ( 3 * ( row1 + i ) ) + 1 ] = 200;
        idata[ ( 3 * ( row1 + i ) ) + 2 ] = 200;
    }
  }
}

//---------------------------------------------------------------------------

/*
void ALMaSSGUI::RunBatch1Click() {
  // First make sure that any current sim is closed
  CloseDownSim();
  // Do any form greying out that is necessary
  //
  //
  // This method causes the program to run as in batch mode
  // Requires the use of DOS commands
  // e.g. int res=system("copy c:\\tocopy.txt c:\\copied.txt");
  //
  // 1. open the batch process file
  // 2. read each entry
  // 3. is it a copy. delete or run entry
  // 4. echo it to the memo
  // 5. carry out the command
  // 6. when no more commands close the process and return done
  //
  int answer = wxYES;
  //wxString filestr = m_BatchFileEdit->GetLineText( 0 );
  ifstream BatchFile((char*) filestr.char_str());
  while (! BatchFile.is_open())
  {
      // Issue and error warning
      wxString amsg = _T("TI Batch File Missing: ");
	  amsg.Append((filestr.c_str()));
      answer = wxMessageBox( amsg, _T("Try Again?"), wxYES_NO | wxCANCEL, this );
	  if ( answer != wxYES ) return;
	  wxString filestr = m_BatchFileEdit->GetLineText( 0 );
	  BatchFile.open((char*)filestr.char_str());
  }
  char batchline[ __MAPSIZE ];
  // Do some form stuff
  //Run1->Enabled=false;
  //Memo1->Clear();
  if ( BatchFile.is_open() ) {
    ClearPad();
	while ( !BatchFile.eof()) 
	{ 
	  BatchFile.getline(batchline,__MAPSIZE);
      //        batchline[ __MAPSIZE - 1 ] = '\0';
      // The pad is written to using strings
      AddToPad( (wxChar*) batchline );
      if ( !ParseBatchLine( batchline ) ) {
        // Issue and error warning
        wxString amsg = _T("Error in: ");
        amsg += wxString::Format( _T("%s"), batchline );
        // Issue and error warning
        wxMessageBox( amsg, _T("Try Again?"), wxOK, this );
      }
    }
    BatchFile.close();
  } else {
    // Print an error
  }
  // Do some more form things
  //Run1->Enabled=true;
  //Edit2->Enabled=true;
  //ComboBoxEx1->Enabled=true;
  //Label3->Enabled=true;
}
*/
//---------------------------------------------------------------------------

bool ALMaSSGUI::ParseBatchLine( char * a_line ) {
  wxString line((const wxChar*) a_line, strlen(a_line));
  if ( ( line.find( _T("REM") ) == 0 ) || ( line.find( _T("#") ) == 0 ) ) {
    // Comment lines
    return true;
  }
  if ( line.find( _T("SPECIES ") ) == 0 ) {
    // Set the species
    // 0,1,2,3 are legal by 30/03/04
    if ( line.find( _T(" 0\n" )) == 7 ) item9->SetSelection( 0 ); // Skylark
    if ( line.find( _T(" 1\n" )) == 7 ) {
      item9->SetSelection( 1 ); // Vole
    }
    if ( line.find( _T(" 2\n" )) == 7 ) {
      item9->SetSelection( 2 ); // Spider
    }
    if ( line.find( _T(" 3\n" )) == 7 ) {
      item9->SetSelection( 3 ); // Beetle
    }
    return true;
  }
  if ( line.find( _T("copy ") ) == 0 ) {
    // copy line.
    if ( system( a_line ) == 0 ) return true;
    return false;
  }
  if ( line.find( _T("del ") ) == 0 ) {
    // delete line.
    if ( system( a_line ) == 0 ) return true;
    return false;
  }
  if ( line.find( _T("ToxImpact") ) == 0 ) {
    // run the sim line.
    m_BatchRun = true;
    // Read in the ini file
    if ( !ReadBatchINI() ) {
      // No batch file present or a problem with it so give up
      return false;
    }
    // Make a Landscape
    printf( "Making Landscape\n" );
	CreateLandscape();
    printf( "Landscape Created\n" ); // So now set up a Population Manager
    if ( !CreatePopulationManager() ) return false; else
      printf( "Population Created\n" ); // Now got to get the probe files read in
    GetProbeInput_ini();
    // Ready to go
    m_SimInitiated = true;
	m_StopSignal=false;
    RunTheSim();
    CloseDownSim();
    m_BatchRun = false;
    m_time = 0;
    m_SimInitiated = false;
    return true;
  }
  return false;
}

//---------------------------------------------------------------------------

bool ALMaSSGUI::ReadBatchINI() {
  // Must read the TIBatch.INI
  // Read the INI file
  ifstream Fi;
  int answer = wxYES;
  //wxString filestr = m_IniFileEdit->GetLineText( 0 );
  //Fi.open(filestr.char_str());
  Fi.open("TI_inifile.ini");
  while ( !Fi.is_open() ) 
  {
	  g_msg->Warn( "TI_inifile.ini file not found","" );
	  // Issue and error warning
	  wxString amsg = _T("INI File Missing: TI_inifile.ini");
	  //amsg += filestr;
	  answer = wxMessageBox( amsg, _T("Try Again?"), wxYES_NO | wxCANCEL, this );
    if ( answer != wxYES ) return false;
//	Fi.open(filestr.char_str());
	Fi.open("TI_inifile.ini");
  }
  char Data[ 255 ];
  Fi >> m_NoProbes;
  Fi.getline(Data, 255);
  for ( int i = 0; i < m_NoProbes; i++ ) {
    m_files[ i ] = new char[ 255 ];
	Fi.getline(m_files[ i ], 255);
  }
  Fi.getline(m_ResultsDir, 255);
  // Read the PredBatch.INI file
  ifstream Fi2;
  answer = wxYES;
  while ( !Fi2.is_open() ) 
  {
    Fi2.open( "VoleToxPreds.ini");
    if ( !Fi2.is_open() ) {
      // Issue and error warning
      wxString amsg = _T("Predator Batch File Missing: ");
      amsg += wxString::Format( _T("%s"), _T("VoleToxPreds.ini") );
      answer = wxMessageBox( amsg, _T("Try Again?"), wxYES_NO | wxCANCEL, this );
    }
    if ( answer != wxYES ) return false;
  }
  Fi2 >> m_NoPredProbes;
  Fi2.getline(Data, 255);
  for ( int i = 0; i < m_NoPredProbes; i++ ) {
    m_Predfiles[ i ] = new char[ 255 ];
	Fi2.getline(m_Predfiles[ i ], 255);
  }
  Fi2.getline(m_PredResultsDir, 255 );
  Fi2.close();
  if ( m_BatchRun ) {
	  Fi >> m_torun;
	  m_torun *= 365; // the number of years and multiplied by 365 to get days
  }
  Fi.close();
  return true;
}

//-----------------------------------------------------------------------------

void ALMaSSGUI::GetProbeInput_ini() {
  char Nme[ 511 ];
  ofstream* AFile = NULL;
  for ( int NProbes = 0; NProbes < m_NoProbes; NProbes++ ) {
    // Must read the probe from a file
    m_AManager->TheProbe[ NProbes ] = new probe_data;
    m_AManager->ProbeFileInput( ( char * ) m_files[ NProbes ], NProbes );
    char NoProbesString[ 32 ];
    sprintf( NoProbesString, "%sProbe%d.res", m_ResultsDir, NProbes );
    if ( NProbes == 0 ) {
      AFile = m_AManager->TheProbe[ NProbes ]->OpenFile( NoProbesString );
	  if ( !AFile->is_open() ) {
        m_ALandscape->Warn( "BatchALMSS - cannot open Probe File", NULL );
        exit( 1 );
      }
	} else m_AManager->TheProbe[ NProbes ]->SetFile( AFile );
  }
  if ( item9->GetSelection() == 1 ) {
    for ( int NProbes = 0; NProbes < m_NoPredProbes; NProbes++ ) {
      m_PredatorManager->TheProbe[ NProbes ] = new probe_data;
      m_PredatorManager->ProbeFileInput( ( char * ) m_Predfiles[ NProbes ], NProbes );
      sprintf( Nme, "%sPredProbe%d.res", m_ResultsDir, NProbes + 1 );
      //      strcpy( Nme, m_PredResultsDir );
      if ( NProbes == 0 ) {
        AFile = m_PredatorManager->TheProbe[ NProbes ]->OpenFile( Nme );
        if ( !AFile ) {
          m_ALandscape->Warn( "BatchALMSS - cannot open Probe File", Nme );
          exit( 1 );
        }
      } else m_PredatorManager->TheProbe[ NProbes ]->SetFile( AFile );
    }
  }
}

//-----------------------------------------------------------------------------

/*
void ALMaSSGUI::ProbeReport( int Time ) {

wxString str = wxString::Format( _T("%d - "), Time );
  int No;
  bool found = false;
  for ( int ProbeNo = 0; ProbeNo < m_NoProbes; ProbeNo++ ) {
    No = 0;
    // See if we need to record/update this one
    // if time/months/years ==0 or every time
    if ( ( m_AManager->TheProbe[ ProbeNo ]->m_ReportInterval == 3 )
         || ( ( m_AManager->TheProbe[ ProbeNo ]->m_ReportInterval == 2 ) && ( BeginningOfMonth() ) )
         || ( ( m_AManager->TheProbe[ ProbeNo ]->m_ReportInterval == 1 ) && ( Time % 365 == 0 ) ) ) {
           // Goes through each area and sends a value to OutputForm
           unsigned Index = m_AManager->SupplyListIndexSize();
           for ( unsigned listindex = 0; listindex < Index; listindex++ ) {
             if ( m_AManager->TheProbe[ ProbeNo ]->m_TargetTypes[ listindex ] )
               No += m_AManager->Probe( listindex, m_AManager->TheProbe[ ProbeNo ] );
           }
           m_AManager->TheProbe[ ProbeNo ]->FileOutput( No, Time, ProbeNo );
           str += wxString::Format( _T(" %d "), No );
           found = true;
    }
  }
  if ( found ) m_ProbesBox->InsertItems( 1, & str, 0 );
}

//-----------------------------------------------------------------------------

void ALMaSSGUI::ProbeReportNow( int Time ) {
  wxString str = wxString::Format( _T("%d - "), Time );
  int No;
  bool found = false;
  for ( int ProbeNo = 0; ProbeNo < m_NoProbes; ProbeNo++ ) {
    No = 0;
    // See if we need to record/update this one
    // if time/months/years ==0 or every time
    // Goes through each area and sends a value to OutputForm
    unsigned Index = m_AManager->SupplyListIndexSize();
    for ( unsigned listindex = 0; listindex < Index; listindex++ ) {
      if ( m_AManager->TheProbe[ ProbeNo ]->m_TargetTypes[ listindex ] )
        No += m_AManager->Probe( listindex, m_AManager->TheProbe[ ProbeNo ] );
    }
    m_AManager->TheProbe[ ProbeNo ]->FileOutput( No, Time, ProbeNo );
    str += wxString::Format( _T(" %d "), No );
    found = true;
  }
  if ( found ) m_ProbesBox->InsertItems( 1, & str, 0 );

}

//-----------------------------------------------------------------------------
void ALMaSSGUI::ProbeReport135( int Time ) {

wxString str = wxString::Format( _T("%d - "), Time );
  int No = 0;
  int Total = 0;
  bool found = false;
  if ( ( item9->GetSelection() == 0 ) || ( item9->GetSelection() == 4 ) ) {
    for ( int ProbeNo = 0; ProbeNo < m_NoProbes; ProbeNo++ ) {
      // See if we need to record/update this one
      // if time/months/years ==0 or every time
      if ( Time % 365 == 150 ) {
        // Goes through each area and sends a value to OutputForm
        No = 0;
        unsigned Index = m_AManager->SupplyListIndexSize();
        for ( unsigned listindex = 0; listindex < Index; listindex++ ) {
          if ( m_AManager->TheProbe[ ProbeNo ]->m_TargetTypes[ listindex ] )
            No += m_AManager->Probe( listindex, m_AManager->TheProbe[ ProbeNo ] );
        }
        m_AManager->TheProbe[ ProbeNo ]->FileOutput( No, Time, ProbeNo );
        str += wxString::Format( _T(" %d "), No );
        found = true;
      }

      Total = m_AManager->TheBreedingFemalesProbe( ProbeNo );
      m_AManager->BreedingPairsOutput( Total, No, Time );
    }
  }
  if ( found ) m_ProbesBox->InsertItems( 1, & str, 0 );

}

//-----------------------------------------------------------------------------
*/

 
