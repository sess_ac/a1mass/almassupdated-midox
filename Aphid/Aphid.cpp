/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Aphid.cpp
\brief <B>The main source code for Aphid class</B>
*/
/**  \file Aphid.cpp
Version of  Feb 2021 \n
By Xiaodong Duan \n \n
*/

#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/ALMaSS_Random.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"
#include "../Aphid/Aphid.h"
#include "../Aphid/Aphid_Population_Manager.h"

extern MapErrorMsg *g_msg;
using namespace std;
using namespace blitz;

static CfgFloat cfg_AphidSuitabilityWeight("APHID_SUITABILITY_WEIGHT", CFG_CUSTOM, 10);
/** \brief The number of nymphs that adults can reproduce. */
static CfgFloat cfg_AphidReproductionNymphNum("APHID_REPRODUCTION_NYMPH_NUM", CFG_CUSTOM, 2);
static CfgFloat cfg_AphidEggNumPerDay("APHID_EGG_NUM_PER_DAY", CFG_CUSTOM, 3);

CfgFloat  cfg_AphidParasitoidHatchRate("APHID_PARASITOID_HATCH_RATE", CFG_CUSTOM, 0.9);
CfgFloat  cfg_AphidParasitoidKillAge("APHID_PARASITOID_KILL_AGE", CFG_CUSTOM, 7);
CfgFloat  cfg_AphidUncropDensityFactor("APHID_UNCROP_DENSITY_FACTOR", CFG_CUSTOM, 8);
CfgFloat  cfg_AphidCropDensityDownFactor("APHID_CROP_DENSITY_DOWN_FACTOR", CFG_CUSTOM, 100);
CfgFloat  cfg_AphidUncropMortalityFactor("APHID_UNCROP_MORTALITY_FACTOR", CFG_CUSTOM, 1.2);
CfgFloat  cfg_AphidLandingMortalityFactor("APHID_LANDING_MORTALITY_FACTOR", CFG_CUSTOM, 0.8);
CfgFloat  cfg_AphidReproductionGreenBiomassPropWeight("APHID_REPRODUCTION_GREEN_BIOMASS_PROP_WEIGHT", CFG_CUSTOM, 1);
CfgInt cfg_AphidDensityHistoryDayNum("APHID_DENSITY_HISTORY_DAY_NUM", CFG_CUSTOM, 4);
CfgFloat cfg_AphidDensityMortalityStrength("APHID_DENSITY_MORTALITY_STRENGTH", CFG_CUSTOM, 0.45);
CfgFloat cfg_APhidIndependentSurvivor("APHID_INDEPENDENT_SURVIVOR", CFG_CUSTOM, 0.95);
CfgFloat  cfg_AphidNoncropBiomassWeight("APHID_NONCROP_BIOMASS_WEIGHT", CFG_CUSTOM, 0.05);

extern CfgStr cfg_AphidSpeciesName;
extern CfgFloat cfg_AphidPredatorsMortality;
extern CfgFloat cfg_AphidMaxWindSpeed;
extern CfgFloat cfg_AphidParasitoidStartNum;

Aphid::Aphid(int p_x, int p_y, int p_w, int p_h, Landscape* p_L, Aphid_Population_Manager* p_NPM, bool a_empty_flag, double* p_suitability, double* p_weight_density, int number, int a_index_x, int a_index_y, int a_SpeciesID, TTypesOfLandscapeElement p_winter_landscape_host, TTypesOfLandscapeElement p_summer_landscape_host, bool p_farm_flag) : SubPopulation(p_x,  p_y, p_L, p_NPM,  a_empty_flag, a_index_x, a_index_y, p_suitability, p_weight_density, -1, a_SpeciesID, p_winter_landscape_host,  p_summer_landscape_host,  p_farm_flag)
{
	if(number>0){
		addAnimalNumGivenStageColumn(0,0,number); //always start with eggs.
	}

	m_cal_suitability_func_pointer = &Aphid::calSuitabilityShared;
	m_cal_biotic_mortality_func_pointer = &Aphid::calBioticMortalityRateShared;

	/*
	if (strcmp(cfg_AphidSpeciesName.value(), "Pea aphid")==0){
		m_cal_suitability_func_pointer = &Aphid::calSuitabilityPeaAphid;
		m_cal_biotic_mortality_func_pointer = &Aphid::calBioticMortalityRatePeaAphid;
	}

	if (strcmp(cfg_AphidSpeciesName.value(), "English grain aphid")==0){
		m_cal_suitability_func_pointer = &Aphid::calSuitabilityPeaAphid;
		m_cal_biotic_mortality_func_pointer = &Aphid::calBioticMortalityRatePeaAphid;
	}
	*/

	m_parasitoid_num = 0;
	m_parasitoid_egg_num = 0;
	m_history_density_vec.resize(cfg_AphidDensityHistoryDayNum.value());
	for (int i=0; i<cfg_AphidDensityHistoryDayNum.value(); i++){
		m_history_density_vec.at(i)=0;
	}
}

void Aphid::calPopuDensity(void){
	*m_popu_density = -1;
	double current_biomass = m_OurLandscape->SupplyVegBiomass(m_Location_x, m_Location_y);
	//double current_biomass = m_OurLandscape->SupplyGreenBiomass(m_Location_x, m_Location_y);
	if (!m_farm_flag){
		current_biomass *= cfg_AphidNoncropBiomassWeight.value();
	}
	if(current_biomass > 0)
	*m_popu_density = m_whole_population_in_cell/current_biomass/m_OurPopulationManager->supplySizeSubpopulationCell();

	//copy the densities first
	for (int i=0; i<cfg_AphidDensityHistoryDayNum.value()-1; i++){
		m_history_density_vec.at(i)=m_history_density_vec.at(i+1);
	}
	m_history_density_vec.at(cfg_AphidDensityHistoryDayNum.value()-1) = *m_popu_density;
}


void Aphid::calSuitabilityShared(void){
	*m_suitability = 0;
	//first summer host
	if(m_OurPopulationManager->supplySummerHostOn() &&(m_summer_landscape_host!=tole_Foobar)){
		*m_suitability = 1;
	}
	if(m_farm_flag && m_OurPopulationManager->supplySummerHostOn()){
		TTypesOfVegetation current_veg_type = m_OurLandscape->SupplyVegType(m_Location_x, m_Location_y);
		//if(m_OurPopulationManager->isSummerHostTov(current_veg_type) && (m_OurLandscape->SupplyGreenBiomassProp(m_Location_x, m_Location_y)>0)){
		if(m_OurPopulationManager->isSummerHostTov(current_veg_type)){
			*m_suitability = 1;
		}
	}


	//check winter host
	if(m_OurPopulationManager->supplyWinterHostOn() &&(m_winter_landscape_host!=tole_Foobar)){
		*m_suitability = 1;
	}
	if(m_farm_flag && m_OurPopulationManager->supplyWinterHostOn()){
		TTypesOfVegetation current_veg_type = m_OurLandscape->SupplyVegType(m_Location_x, m_Location_y);
		//if(m_OurPopulationManager->isWinterHostTov(current_veg_type) && (m_OurLandscape->SupplyGreenBiomassProp(m_Location_x, m_Location_y)>0)){
		if(m_OurPopulationManager->isWinterHostTov(current_veg_type)){
			*m_suitability = 1;
		}
	}

	/*
	*m_suitability = 1;
	if(m_summer_landscape_host==tole_Foobar){
		if(!m_farm_flag){
			*m_suitability = 0;
		}
		else{
			TTypesOfVegetation current_veg_type = m_OurLandscape->SupplyVegType(m_Location_x, m_Location_y);
			if(!(m_OurPopulationManager->isSummerHostTov(current_veg_type))){
				*m_suitability = 0;
			}
		}
		
	}
	else{
		int current_month = m_OurLandscape->SupplyMonth();
		vector<int> temp_period = m_OurPopulationManager->supplySummerTolePeriod(m_summer_landscape_host);
		if (current_month <  temp_period.at(0) || current_month > temp_period.at(1)){
			*m_suitability = 0;
		}
	}
	*/

	//no biomass, no suitability
	if (m_OurLandscape->SupplyVegBiomass(m_Location_x, m_Location_y)<=0){
		*m_suitability = 0;
	}
}

double Aphid::calBioticMortalityRate(int a_life_stage){
	return (this->*m_cal_biotic_mortality_func_pointer)(a_life_stage);
}

double Aphid::calBioticMortalityRateShared(int a_life_stage){
	if (a_life_stage == toa_Egg){
		return 0;
	}

	else{
		return cfg_AphidPredatorsMortality.value();
	}
}


void Aphid::calSuitability(void){
	(this->*m_cal_suitability_func_pointer)();
	 //The suibability is not change during the simulation. This needs to be cahanged!

	
	/*
	TTypesOfLandscapeElement current_le = m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y);
	TTypesOfAphidDevelopmentSeason current_development_season = TTypesOfAphidDevelopmentSeason(m_OurPopulationManager->supplyDevelopmentSeason());
	//winter
	if (m_OurPopulationManager->isWinterHostTole(current_le)){		
		if (current_development_season == toAphidFallDev || current_development_season == toAphidSpringDev){
			*m_suitability = 1.0;
		}
		if (current_development_season ==  toAphidSummerDev
		 || current_development_season == toAphidHibernate){
			 *m_suitability = 0.0;
		 }
		if (current_development_season == toAphidLateSpringDev){
			*m_suitability = 0.5;
		}
	}

	//summer
	if (m_OurPopulationManager->isSummerHostTole(current_le)){
		if (current_development_season == toAphidSummerDev || current_development_season == toAphidLateSpringDev){
			*m_suitability = 1.0;
		}
		if (current_development_season ==  toAphidFallDev
		 || current_development_season == toAphidSpringDev){
			 *m_suitability = 0.5;
		 }
		if (current_development_season == toAphidHibernate){
			*m_suitability = 0.0;
		}
	}
	*/

	/*
	//for now, I assume the biomass is average
	double biomasscount = m_OurPopulationManager->m_TheLandscape->SupplyVegBiomass(m_Location_x,m_Location_y) * m_OurPopulationManager->supplySizeSubpopulationCell();
	if (isnan(biomasscount)){
		biomasscount=0;
	}
	
	m_plant_juice += biomasscount * 0.01;

	//biomasscount -= AphidIntake();
	double aphid_num = getTotalSubpopulation();
	if (isnan(aphid_num)){
		cout<<"NaN"<<endl;
	}
	double temp_suitabilty = cfg_AphidSuitabilityWeight.value()*m_plant_juice/(aphid_num+1);
	*m_suitability = 2*((1+exp(-1*temp_suitabilty))-0.5);
	if((*m_suitability)>1) *m_suitability=1;
	if((*m_suitability)<0) *m_suitability=0;
	//if (aphid_num <= 0) aphid_num = 1000; //no aphid in this area, try if we can put 1000 here
    //double ave_biomass = biomasscount/aphid_num;
    //if (ave_biomass < cfg_AphidMinBiomassPerAphid.value()) {m_suitability=0;return 0.0;}
	//if (ave_biomass > cfg_AphidMaxBiomassPerAphid.value()) {m_suitability=1.0;return 1.0;}
    //m_suitability = (ave_biomass-cfg_AphidMinBiomassPerAphid.value())/(cfg_AphidMaxBiomassPerAphid.value()-cfg_AphidMinBiomassPerAphid.value());
	*/
}

void Aphid::doReproduction(void){
	int temp_max_num_col = m_OurPopulationManager->supplyMaxColNum();
	for (int i = toa_Aptera; i<m_OurPopulationManager->supplyLifeStageNum(); i++){
		if(i==toa_Male) continue; //male doesn't produce anything
		if (i==toa_Aptera || i == toa_Alate){
			double temp_re_number_total_wing = 0.0;
			double temp_re_number_total_unwing = 0.0;
			double temp_re_number = 0.0;
			double temp_re_prop = 0.0;

			/** \todo get the growth stage and the density. */
			double a_growth_stage = m_OurLandscape->SupplyVegGrowthStage(m_OurLandscape->SupplyPolyRef(m_Location_x, m_Location_y));
			//if(!m_farm_flag) a_growth_stage = 50;
			//double a_density = m_whole_population_in_cell/m_OurLandscape->SupplyVegCover(m_Location_x, m_Location_y)/m_OurPopulationManager->supplySizeSubpopulationCell();
			double current_biomass = m_OurLandscape->SupplyVegBiomass(m_Location_x, m_Location_y);
			//double current_biomass = m_OurLandscape->SupplyGreenBiomassProp(m_Location_x, m_Location_y);
			double a_density = *m_popu_density;
			if (current_biomass>0){
				//a_density = m_whole_population_in_cell/current_biomass/m_OurPopulationManager->supplySizeSubpopulationCell();
				if(!m_farm_flag){
					a_density *= cfg_AphidUncropDensityFactor.value();
				}
				else{
					a_density /= cfg_AphidCropDensityDownFactor.value();
				}
			} 
			
			//int temp_re_number = cfg_AphidReproductionNymphNum.value() * getNumforOneLifeStage(toa_Aptera);
			for(int j=0; j<temp_max_num_col; j++){
				double temp_mother_num = m_animal_num_array(i,j);
				if(temp_mother_num<1) continue;
				int next_life = m_OurPopulationManager->calOffspringStage(i, &temp_re_number,m_OurPopulationManager->supplyAgeInDayDegree(i,j), a_density, a_growth_stage, &temp_re_prop,( m_winter_landscape_host != tole_Foobar));
				//temp_re_number *= m_OurLandscape->SupplyGreenBiomassProp(m_Location_x, m_Location_y)*cfg_AphidReproductionGreenBiomassPropWeight.value();
				if (m_OurLandscape->SupplyGreenBiomassProp(m_Location_x, m_Location_y)<=0) temp_re_number = 0.0;
				if (temp_re_number > 0)
				{
					//sexual ones
					if (next_life == toa_Nymph_M || next_life == toa_Nymph_F){
						addAnimalNumGivenStageColumn(next_life, m_OurPopulationManager->supplyNewestIndex(next_life), temp_mother_num*temp_re_number);
						//m_OurPopulationManager->updateWholePopulationArray(toa_Nymph, temp_re_number);
						//check if it is the first time
						if (m_OurPopulationManager->supplyFirstFlagLifeStage(next_life)){
							m_OurPopulationManager->setFirstFlagLifeStage(next_life, false);
							m_OurPopulationManager->setOldIndex(next_life, m_OurPopulationManager->supplyNewestIndex(next_life));
						}
					}

					if (next_life == toa_Nymph || next_life == toa_Nymph_W){
						//winged ones asexaual
						if(temp_re_prop > 0){
							addAnimalNumGivenStageColumn(toa_Nymph_W, m_OurPopulationManager->supplyNewestIndex(int(toa_Nymph_W)), temp_mother_num*temp_re_number*temp_re_prop);
							//m_OurPopulationManager->updateWholePopulationArray(toa_Nymph, temp_re_number);
							//check if it is the first time
							if (m_OurPopulationManager->supplyFirstFlagLifeStage(toa_Nymph_W)){
								m_OurPopulationManager->setFirstFlagLifeStage(toa_Nymph_W, false);
								m_OurPopulationManager->setOldIndex(toa_Nymph_W, m_OurPopulationManager->supplyNewestIndex(toa_Nymph_W));
							}
						}
						if(temp_re_prop>=0 && temp_re_prop<=1){
							//if(temp_re_prop ==0 && m_landing_local_move_flag){
							//	temp_re_number *=10;
							//}
							addAnimalNumGivenStageColumn(toa_Nymph, m_OurPopulationManager->supplyNewestIndex(int(toa_Nymph)), temp_mother_num*temp_re_number*(1-temp_re_prop));
							//m_OurPopulationManager->updateWholePopulationArray(toa_Nymph, temp_re_number);
							//check if it is the first time
							if (m_OurPopulationManager->supplyFirstFlagLifeStage(toa_Nymph)){
								m_OurPopulationManager->setFirstFlagLifeStage(toa_Nymph, false);
								m_OurPopulationManager->setOldIndex(toa_Nymph, m_OurPopulationManager->supplyNewestIndex(toa_Nymph));
							}
						}
						else{
							cout<<"NEGATIVE: "<<temp_re_prop<<endl;
						}
					}
				}
			}
		}
		//laying eggs
		if (i==toa_Female){
			//int temp_egg_number = cfg_AphidEggNumPerDay.value() * getNumforOneLifeStage(toa_Female);
			double temp_egg_number = 0;
			double temp_egg_number_total = 0;
			for(int j=0; j<temp_max_num_col; j++){
				double temp_mother_num = m_animal_num_array(i,j);
				if(temp_mother_num<1) continue;
				int temp_age= m_OurPopulationManager->supplyAgeInDay(i,j)-1;
				if (temp_age<0) temp_age = 0;
				int next_life = m_OurPopulationManager->calOffspringStage(toa_Female, &temp_egg_number, temp_age);
				if(temp_egg_number>0){
					temp_egg_number_total += temp_mother_num*temp_egg_number;
				}
			}
			
			if (temp_egg_number_total > 0){
				addAnimalNumGivenStageColumn(toa_Egg, 0, temp_egg_number_total);
				//m_OurPopulationManager->updateWholePopulationArray(toa_Egg, temp_egg_number);
			}
		}
	}
}

void Aphid::doMovement(void){
	#ifdef APHID_DEBUG
	m_OurPopulationManager->m_num_parasitoid += m_parasitoid_num;
	m_OurPopulationManager->m_num_parasitoid_egg += m_parasitoid_egg_num;
	#endif

	//return;
	//double current_wind_speed = m_OurLandscape->SupplyWind();
	//if (current_wind_speed >= cfg_AphidMaxWindSpeed.value()) return; //too windy, no flying
	vector<int> temp_flying_stages = m_OurPopulationManager->supplyVecFlyingLifeStages();
	for( int i = 0; i<m_OurPopulationManager->supplyNumFlyingLifeStages();i++){
		//flying for the winged ones		
		if(m_population_each_life_stage(temp_flying_stages.at(i))>0){
			m_flying_array(i, blitz::Range::all()) = m_animal_num_array(temp_flying_stages.at(i), blitz::Range::all())*cfg_AphidLandingMortalityFactor.value();
		}
		else{
			m_flying_array(i, blitz::Range::all()) = 0;
		}		
	}
}

void Aphid::doDropingWings(void){
	;
	/*
	int current_winged_num = getNumforOneLifeStage(toa_WingedFemale);
	if(current_winged_num>0){
		m_temp_droping_wings_array = m_animal_num_array(toa_WingedFemale, blitz::Range::all());
		//std::cout<<"++++"<<current_winged_num<<"++++"<<m_temp_droping_wings_array<<std::endl;
		//std::cout<<m_animal_num_array<<std::endl;
		addAnimalNumGivenStage(toa_Female, &m_temp_droping_wings_array);
		m_temp_droping_wings_array = -1*m_temp_droping_wings_array;
		//std::cout<<"----"<<m_temp_droping_wings_array<<std::endl;
		addAnimalNumGivenStage(toa_WingedFemale, &m_temp_droping_wings_array);
	}
	*/
}



void Aphid::giveBirthParasitoidEgg(double a_egg_num){
	if(m_parasitoid_num > 0){
		m_parasitoid_egg_num = m_parasitoid_num * a_egg_num;
		m_parasitoid_num = 0;
	}

	else{
		m_parasitoid_egg_num = cfg_AphidParasitoidStartNum.value() * a_egg_num;
	}
	#ifdef APHID_DEBUG
	//m_OurPopulationManager->m_num_new_parasitoid_egg_daily += m_parasitoid_egg_num;
	#endif
}

void Aphid::killByParasitoid(){
	if(m_parasitoid_egg_num > 0){
		double killed_num = 0;
		double egg_num = m_parasitoid_egg_num;
		for(int i = toa_Aptera; i<=toa_Male; i++){
			//int index_new = m_OurPopulationManager->supplyNewestIndex(i) - 1;
			//if (index_new < 0) index_new = m_OurPopulationManager->supplyMaxColNum();
			//int kill_index = index_new;
			for(int kill_index=0; kill_index<m_OurPopulationManager->supplyMaxColNum(); kill_index++){ //kill 7-days old
				//kill_index = index_new - h;
				if(m_OurPopulationManager->supplyAgeInDay(i, kill_index)>cfg_AphidParasitoidKillAge.value()){
					continue;
				}
				if(m_animal_num_array(i, kill_index)>0){
					double adult_num = m_animal_num_array(i, kill_index);
					//enough to kill
					if (adult_num >= egg_num){
						addAnimalNumGivenStageColumn(i, kill_index, -1*egg_num);
						killed_num += egg_num;
						egg_num = 0;
						break;
					}
					//still need to kill more
					else {
						addAnimalNumGivenStageColumn(i, kill_index, -1*adult_num);
						killed_num += adult_num;
						egg_num -= adult_num;
					}
				}
			}
			if (egg_num<=0){
				break;
			}
		}
		m_parasitoid_egg_num = killed_num;
		#ifdef APHID_DEBUG
		m_OurPopulationManager->m_num_killed_by_parasitoid+=killed_num;
		#endif
	}
}

void Aphid::hatchParasitoidEggs(){
	if(m_parasitoid_egg_num > 0){
		m_parasitoid_num = m_parasitoid_egg_num * cfg_AphidParasitoidHatchRate.value();
		m_parasitoid_egg_num = 0;
	}
}

double Aphid::calCellRelatedMortalityWeight(void){
	//if (m_farm_flag){
	//	return 0;
	//}
	//return cfg_AphidUncropMortalityFactor.value();
	return (1- 1/(cfg_APhidIndependentSurvivor.value()*(1+cfg_AphidDensityMortalityStrength.value()*m_history_density_vec.at(0))));
}

bool Aphid::OnFarmEvent(FarmToDo event){
	//cout<<"Farm event: "<<event<<endl;
	//if(event==insecticide_treat){
	if(event == harvest){
		killAllPopulation();
	}
	return false;
}

void Aphid::killAllPopulation(){
	//cout<<"Killling allllllllllll"<<endl;
	if(m_whole_population_in_cell>0){
		#ifdef APHID_DEBUG
		m_OurPopulationManager->m_num_new_parasitoid_egg_daily += m_whole_population_in_cell;
		#endif
		m_animal_num_array = 0;
		m_whole_population_in_cell = 0;
		for(int i = toa_Egg; i<= toa_Male; i++){
			if (m_population_each_life_stage(i) <=0) continue;
			double kill_num = floor(-1*m_population_each_life_stage(i));
			m_OurPopulationManager->updateWholePopulationArray(i, kill_num);		
		}
		m_population_each_life_stage = 0;
	}
}

void Aphid::BeginStep(){
	CheckManagement();
}


