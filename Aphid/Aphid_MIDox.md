Aphid MIDox             {#Aphid_page}
=========
<h1> MIDox for Four Aphid Species using ALMaSS Subpopulation Model </h1>
Duan, X.*, Thomsen, P., Andersen, A. H. and Topping, C.J.

Department of Ecoscience, Aarhus University, C. F. Møllers Allé 8, 8000 Aarhus C, Denmark

*Corresponding author: Xiaodong Duan, xd@ecos.au.dk

# Introduction
Aphids are a group of sap-sucking insects comprising 4000 species of which 100 species colonise agriculturally important crops \ref emden2007aphids1 "(Emden & Harrington, 2007)". Due to their rapid reproduction rate and good dispersal ability, aphids can cause considerable economic damage to agriculture \ref hulle2010aphids2 "(Hulle et al., 2010)". Therefore it is essential to find ways of evaluating their impact and control. This document describes the implementation of an ALMaSS subpopulation model for four aphid species: <em>Aphis fabae</em> (Black bean aphid), <em>Myzus persicae</em> (Peach potato aphid), <em>Sitobion avenae</em> (English Grain aphid) and <em>Acyrthosiphon pisum</em> (Pea aphid), wihtin the ALMaSS  (Animal Landscape and Man Simulation System) framework \ref topping2003 "(Topping et al., 2003)". The four species are grouped into two categories based on whether they use different plant hosts in winter and summer or use the same plant host through the year. Black bean and peach potato aphids use woody habitats to overwinter while they fly to mainly crop areas for summer reproduction and thus fall within the first category while the other two species can use the same host plants for the entire year. The implementation of the aphid model is the first model in ALMaSS using a subpopulation approach and thus forms the test case for a new method. The documentation follows the Model Implementation Documentation with Doxygen (MIDox) format, the next step in designing, documenting and calibrating complex models after the “Formal Model” format document \ref Topping2022aphids4 "(Topping et al., 2022)".

# Aims & Purposes
The model is implemented to represent aphid population distribution and abundance in dynamic landscapes. The model responds to changes in crop management and will be used to assess the effectiveness of control measures and to help understand the spatiotemporal dynamics of aphid populations.

The aim of the aphid model is to couple it with models of aphid predators (e.g. Ladybirds) within the same system. We aim to use this coupled system to assess management impacts on natural enemies while also evaluating aphid control. As such, the aphid model may form an important tool in evaluating integrated pest management at regional scales.

A single model is implemented to be as general as possible to handle four aphid species, while the variations among the four species are dealt with model parameters and functions setting.

# Model in brief
The aphid model differs from the other species models developed within the ALMaSS framework because it is not an agent-based model. Instead, the landscape is firstly segmented into grid, in which the model aphids with the same age are grouped into cohorts in each cell. We refer to this as the ALMaSS Subpopulation Model. The model implements two classes: the Aphid and Aphid_Population_Manager (referred as the aphid manager) derived from the base classes: SubPopulation and SubPopulation_Population_Manager (referred as the base manager) respectively. Further, the aphid model is integrated within ALMaSS which allows the model aphids to interact with the spatiotemporally dynamic landscape model in ALMaSS.

In the aphid manager, the landscape is split into equally sized, relatively small squares forming a continuous grid with a default size of 10 x 10m. Each square, representing a grid cell, is used to model a single subpopulation of aphids that describes the reproduction and mortality of the aphids within that grid cell per time step (one day). The suitability for aphids within each grid cell are defined based on its context in the ALMaSS landscape (e.g. host plant availability). In each grid cell, an instance of the Aphid class is created where the model aphids are divided into a stage-structured population representation where individuals of the same age form cohorts. Each day the cohorts go trough development, mortality, movement and reproduction if they are capable of them at that life stage. If the cohorts accumulated enough development time when a day has passed they will move to the next life stage or die. Death can occur at any stage or age due to weather, host plant and predator conditions of the location where they are. Reproduction is controlled through the environmental temperature and the mothers' age. Daylight length determines the production of eggs (product of sexual reproduction) and nymphs (product of asexual reproduction).

All subpopulations can, in theory, interact through dispersal (i.e. grid cells are connected through the flying activities of alates). Unlike the model developed by researchers \ref duffy2017improved3 "(Duffy et al., 2017)", our model is designed from a mechanistic perspective. This approach also allows for interactions between the aphids and other organisms (e.g., parasitoids) explicitly within the model. And, it can also be coupled with other models in the ALMaSS framework to generate external interactions such as aphid predator ladybird.

# Scheduling
To start the simulation of the aphid model an instance of the Aphid_Population_Manager class is created. The flowchart of the aphid manager can be found in Figure 1. In the initialisation phase the species to be simulated is set with a parameter followed by the setting of necessary model input parameters for the corresponding species. Function pointers are also set for the selected species. Once the initialisation is done, the simulation loop starts and runs until a predefined simulation time is reached. The model runs with a time step of one day. Each day the development season is updated, which is used to make the summer and winter host plants available or unavailable for the host switching species (Black bean and Peach potato aphids). The availability of summer and winter host plants is calculated based on daylight length. However, if it is not a host switching species being simulated, summer host plants are set to appear for the whole year without the setting of any winter host plants. The mortality array is updated after the development season at the beginning of a new day. This is used to calculate the temperature dependent mortality of the simulated aphid species. Afterwards, the aphid manager will loop all the Aphid objects to let the cohorts to do mortality, reproduction, development and preparation for dispersal. After the loop is finished, the aphid manager will relocate the flying aphid to land based on the wind direction. By the end of each day the accumulated day degrees (see Implementation details: Development) will be updated based on the average environmental temperature and each life stage's base development temperature and new day will start.

\image html aphid_manager_flow_chart.png "Figure 1. Scheduling flowchart for the aphid manager class."

# Implementation details
## Function pointers
Since four aphid species are modelled using the same C++ classes, several function pointers are defined to point to different functions that will be used by different species. These pointers are initialised based on the chosen aphid species.
- Aphid_Population_Manager::m_next_life_func_pointer, this pointer points to a function which calculates the next life stage for a given life stage.
- Aphid::m_cal_biotic_mortality_func_pointer, this pointer points to a function which calculates the biotic mortality for a given grid cell.
- Aphid_Population_Manager::m_update_mortality_array_pointer, this pointer points to a function which calculates the temperature and age dependent mortality for all the life stages.
- Aphid_Population_Manager::m_offspring_func_pointer, this pointer points to a function which calculates the life stage and number of the offspring for a given model aphid mother.
- Aphid_Population_Manager::m_is_ready_next_liff_func_pointer, this pointer points to a function which tests whether a given model aphid cohort is ready to move to the next life stage.
- Aphid::m_cal_suitability_func_pointer, this pointer points to a function which calculates the suitability of a given grid cell.

  
## Lifestages
Nine life stages are defined for an aphid population to implement the full life cycle of aphids which is illustrated in Figure 2. These are eggs, four different nymph variations, adults without wings, adults with wings, females and males.
- #toa_Egg, this is the product of sexual reproduction. Females lay their eggs in late autumn.
- #toa_Nymph, this nymph variation will develop into aptera.
- #toa_Nymph_W, this nymph variation will develop into alate which will disperse (fly) to other grid cells.
- #toa_Nymph_F, this nymph variation will develop into females.
- #toa_Nymph_M, this nymph variation  will develop into males.
- #toa_Aptera, this represents an aphid adult without wings. These produce nymphs for reproduction.
- #toa_Alate, this represents an aphid adult with wings. These can both fly and produce nymphs for reproduction.
- #toa_Female, this is the aphid adult female. Females can produce eggs.
- #toa_Male, this is the aphid adult male.

\image html aphid_life_circle.png "Figure 2. The life cycle of aphids. Blue Arrows represent development, red arrows reproduction and green arrows represent egg laying. The development in the red dash rectangle is the sexual development and the one in the green dash rectangle is the asexual development."

## Development
The development from one lifestage to another in the aphid life cycle is based on day degrees (see below). Each day the accumulated day degrees for all lifestages are updated in SubPopulation_Population_Manager::doDevelopment function after which the Aphid_Population_Manager::m_is_ready_next_liff_func_pointer is called. This function checks whether each age cohort is ready for the next lifestage and if so calculates the next lifestage. The next-stage-ready cohorts will be used by the SubPopulation::doDevelopment function which will move the next-stage-ready cohorts to the next lifestage.

### Degree day calculation
A simple linear model is used to calculate the day degrees determining aphid development. Each lifestage has its own lower developmental thresholds given by \f$T_l\f$, where \f$l\f$ represents the lifestage. Suppose the daily average environmental temperature is \f$t_a\f$ for one day \f$a\f$, the corresponding day degrees \f$DD_l\f$ for lifestage $l$ is calculated by:

$$DD_l = t_a - T_l$$ 

### Next lifestage calculation
All four aphid species use the function Aphid_Population_Manager::calNextStageShared to calculate the next lifestage as shown in Table 1. For the adult stage, the next lifestage is -1 which means death.

Table 1: Aphid input parameters.
Current lifestage  | Next lifestage
------------- | -------------
#toa_Egg  | #toa_Nymph
#toa_Nymph   |  #toa_Aptera
#toa_Nymph_W   |  #toa_Alate
#toa_Nymph_F |   #toa_Female
#toa_Nymph_M |   #toa_Male
#toa_Aptera  |   -1
#toa_Alate   |   -1
#toa_Female  |   -1
#toa_Male    |   -1

## Reproduction
Aphids have two types of reproductive morphs: eggs (sexual reproduction) and nymphs (asexual reproduction). Eggs are laid by female adults while nymphs are produced by aptera and alates. Two functions: the Aphid_Population_Manager::calOffspringStagePea and Aphid_Population_Manager::calOffspringStageAphis are implemented for the calculation of the type and number of offspring for a given mother aphid depending on her type and age. The first function is used by species without host switching behaviour while the latter is used by species with a host switching behaviour. Both of these functions will be set to point by the Aphid_Population_Manager::m_offspring_func_pointer, which is called by Aphid::doReproduction, to perform reproduction for each reproductive aphid object in every grid cell.

In Aphid::doReproduction all the reproductive adults are looped to produce offspring either as eggs or nymphs. For sexual reproduction the number of eggs is calculated from the number of females at every age (in days) and the number of eggs per female at that age. The number of eggs is returned by the function of Aphid_Population_Manager::m_offspring_func_pointer. The egg number per female age is calculated by: #cfg_AphidEggReproductionParaA * exp(#cfg_AphidEggReproductionParaB *i ) * (#cfg_AphidEggReproductionParaC + (g_rand_uni()-0.5)*2* #cfg_AphidEggReproductionParaSTD), where i is the age of the female.

For asexual reproduction the nymph number per day depends on the development age of the mother aphid which is calculated using accumulated day degrees (ADD). When the mother's ADD is less than #cfg_AphidNymphReproInfectionADD the number of nymphs per day is calculated by:
#cfg_AphidNymphReproParaA1 * ADD + #cfg_AphidNymphReproParaB1, otherwise #cfg_AphidNymphReproParaA2 * ADD + #cfg_AphidNymphReproParaB2

For Pea aphids and English grain aphids (plant host switching behaviour) the day length is used to decide which reproductive morph to produce. Specifically, when the day length is between #cfg_PeaAphidDayLengthMale and #cfg_PeaAphidDayLengthFemale, #toa_Nymph_M is produced. When the day length is less than #cfg_PeaAphidDayLengthFemale, #toa_Nymph_F is produced. Otherwise, asexual morphs (#toa_Nymph or #toa_Nymph_W) are produced. For Peach potato and Black bean aphids (without plant host switching behaviour), #toa_Nymph_F is produced when the mothers are located on a winter host plant and it is later than August. Otherwise, #toa_Nymph or #toa_Nymph_W are produced.

For the asexual nymphs the vegetation growth stage (Landscape::SupplyVegGrowthStage) and the population density (Aphid::calPopuDensity) are used to calculate the ratio between #toa_Nymph and #toa_Nymph_W. Specifically, the proportion of #toa_Nymph_W is calculated by: #cfg_AphidAlatePropParaA * population density + #cfg_AphidAlatePropParaC * vegetation growth stage + #cfg_AphidAlatePropParaB.

## Mortality
Mortality describes how many individuals die daily due to biotic and abiotic factors. Each aphid cohort at a given lifestage has a mortality rate that is given based on their age, the environmental temperature, population density and pressure from parasitods and predators. The proportion of aphids to be killed every day, given by the mortality rate, is performed by the SubPopulation::doMorality function.

### Age and temperature
The age and temperature dependent mortality is calculated using the function Aphid_Population_Manager::updateMortalityArrayShared pointed to by the Aphid_Population_Manager::m_update_mortality_array_pointer. This is used for all species.

#### Egg
The mortality of eggs is modelled to not be dependent on the age and temperature but rather a constant that is calculated by two input parameters using the formula: #cfg_AphidEggMortalityRate+(g_rand_uni()-0.5)*2*#cfg_AphidEggMortalityRateStd.

#### Nymph
The mortality of nymphs is assumed to depend on the environmental temperature and is calculated using three model parameters given in the formula: #cfg_AphidNymphMortalityParaA*current_temperature*current_temperature + #cfg_AphidNymphMortalityParaB*current_temperature+#cfg_AphidNymphMortalityParaC, where current_temperature is the environmental temperature.

#### Adults
When the daily average environmental temperature is above 34.5 or below -9 all the adult aphids will be killed, i.e. the mortality rate is 1. When the temperature is between -9 and 34.5 temperature will not cause any mortality to adult aphids. The mortality in adults caused by age is calculated using the accumulated day degrees (ADD) based on aphid longevity using the formula: 1.0 - (ADD*#cfg_AphidLongevityParaA-#cfg_AphidLongevityParaB).  

### Parasitoids
<em>Aphidius smithi</em>, a parasitoid, is modelled using two morphs; egg and adult. Each grid cell contains 10 <em>A. smithi</em> eggs as a starting point. When aphids enter a new cell <em>Aphidius smithi</em> development starts. <em>Aphidius smithi</em> adults have a fixed lifespan of 7 days. Eggs are laid based on the ratio between parasitoids and aphids. The number of eggs in each cell is limited by the number of suitable aphids as only 1 <em>A. smithi</em> egg can develop in each aphid. When parasitoid eggs reach the mummification stage at 73 day degrees (DD) adult aphids (all morphs hereof) with an age of 64+/-15 DD are killed within the cell. It is assumed that each morph is equally susceptible to mortality by parasitoids. When <em>A. smithi</em> eggs have accumulated 180 day degrees above 6.1°C they become adults. <em>Aphidius smithi</em> adult's fecundity is assumed to only be influenced by the ratio between aphids and parasitoids. The number of killed aphids by <em>A. smithi</em> is calculated in Aphid::killByParasitoid().

### Density based mortality
Density-dependent mortality happens due to disease and a decrease in host quality due to crowding. This is calculated through the function Aphid::calCellRelatedMortalityWeight using the formula: 1 - 1/(#cfg_APhidIndependentSurvivor*(1+#cfg_AphidDensityMortalityStrength*SubPopulation::m_history_density_vec.at(0))), where SubPopulation::m_history_density_vec.at(0) is the density at a number of days before given by #cfg_AphidDensityHistoryDayNum.

### Predator
As a response to predation a constant mortality is added to all aphid morphs except eggs. This is done by the function Aphid::calBioticMortalityRateShared pointed to by the m_cal_biotic_mortality_func_pointer. Here, the model parameter #cfg_AphidPredatorsMortality is used to add a constant mortality value. Note that after the aphid model is connected with ladybird, this value will be adjusted.

## Movement
In the model only alates can perform long-distance movement through flying. Alates can only fly once in their lifetime and flying behaviour is driven by the wind direction and speed. Only when the wind speed is smaller than #cfg_AphidMaxWindSpeed will the alate take off and fly to another location given by the wind direction. After alates land they will loose their wings and become aptera. Landing is controlled by the function SubPopulation_Population_Manager::doFlying and the landing masks calculated by the function SubPopulation_Population_Manager::calLongMovementMask().

# Interconnections
The aphid model is connected to the landscape model in ALMaSS. This means that the aphids read and respond to temperature, landscape type, vegetation type, vegetation growth stage, total biomass and green biomass. Later it is our aim that aphids will also be connected with Ladybird as a predator.

- Landscape::SupplyGreenBiomass: Get the green biomass.
- Landscape::SupplyVegBiomass: Get the total biomass.
- Landscape::SupplyVegGrowthStage: Get the vegetation growth stage.
- Landscape::SupplyTemp: Get the average daily temperature.
- Landscape::SupplyPolyRef: Get the polygon ID.
- Landscape::SupplyVegType: Get the vegetation type.
- Landscape::SupplyGreenBiomassProp: Get the green biomass proportion.

# I/O, Variables & Scales
## Inputs
All the input parameters are listed in Table 2.
Table 2: Aphid input parameters.
Name  | Description
------------- | -------------
#cfg_AphidSpeciesName  | This defines which species will be simulated.
#cfg_AphidDevelopmentFile   |  This defines the file containing the base development temperature.
#cfg_AphidCellWidth | The width of the grid cells.
#cfg_AphidCellHeight    | The height of the grid cells.
#cfg_AphidPredatorsMortality | The constant mortality value caused by predators.
#cfg_AphidParasitoidHatchRate | The successful rate of parasitoid egg hatchment.
#cfg_AphidParasitoidKillAge | The adult aphids above this age will be killed by parasitoids.
#cfg_AphidParasitoidStartDdeg | The starting day degrees for parasitoid development.
#cfg_AphidParasitoidStartNum    | The number of parasitoids in each grid cell at simulation start.
#cfg_AphidParasitoidBaseDevTemp | The base development temperature for parasitoids.
#cfg_AphidParasitoidLifeTime    | The life time of parasitoids in calendar days.
#cfg_AphidParasitoidEggPerDay   | The number of eggs that parasitoids can lay per day.
#cfg_AphidParasitoidKillDdeg    | The accumulated day degrees it takes for the parasitoids to kill its host aphid.
#cfg_AphidParasitoidHatchDdeg   | The required number of accumulated day degrees before a parasitoid hatches.
#cfg_AphidDensityHistoryDayNum  | Parameter for density dependent mortality.
#cfg_AphidDensityMortalityStrength  |   Parameter for density dependent mortality.
#cfg_APhidIndependentSurvivor  |   Parameter for density dependent mortality.
#cfg_AphidEggStartingNum    | Number of aphid eggs per grid cell at simulation start.
#cfg_AphidScaleWindSpeed    |   This is used to sample the wind speed to discrete ranges. Each range has eight landing 2D masks in eight wind directions. 
#cfg_AphidMaxLongDistance   |   This is the longest distance at which the alates can land along the wind direction when the wind speed is in the first range.
#cfg_AphidPeakLongDistance  |   This is the distance where the majority of alates can land along the wind direction when the wind speed is in the first range. 
#cfg_AphidMaxWindSpeed  |   When the wind speed is larger than w_M alates will not attempt to fly. 
#cfg_AphidMovMaskWindDirectionNum   | This is the wind direction number. Can be 4 or 8.
#cfg_AphidMovMaskWindSpeedStepSize  |   This is the scale used to increase the peak and longest distance when wind speed increases to one range from the previous range. 
#cfg_AphidEggMortalityRate  | Parameter for the calculation of egg mortality.
#cfg_AphidEggMortalityRateStd  | Parameter for the calculation of egg mortality (standard deviation).
#cfg_PeaAphidDayLengthMale  | Maximum day length in minutes for male emergence for aphids without host switching behaviour.
#cfg_PeaAphidDayLengthMale  | Maximum day length in minutes for female emergence for aphids without host switching behaviour.
#cfg_AphidEggReproductionParaA  | Parameter for calculation of age dependent daily nymph number for asexual reproduction.
#cfg_AphidEggReproductionParaB  | Parameter for calculation of age dependent daily nymph number for asexual reproduction.
#cfg_AphidEggReproductionParaC  | Parameter for calculation of age dependent daily nymph number for asexual reproduction.
#cfg_AphidEggReproductionParaSTD  | Parameter for calculation of age dependent daily nymph number for asexual reproduction.
#cfg_AphidNymphReproParaA1  | Parameter for calculation of age dependent daily egg number for sexual reproduction.
#cfg_AphidNymphReproParaB1  | Parameter for calculation of age dependent daily egg number for sexual reproduction.
#cfg_AphidNymphReproParaA2  | Parameter for calculation of age dependent daily egg number for sexual reproduction.
#cfg_AphidNymphReproParaB2  | Parameter for calculation of age dependent daily egg number for sexual reproduction.
#cfg_AphidNymphReproInfectionADD | Inflection point parameter for calculation of daily egg numbers for sexual reproduction.
#cfg_AphidNymphDeveDayDegWing   | The day degrees required by nymphs to develop into alates.
#cfg_AphidNymphDeveDayDegWingStd   | The standard deviation of day degrees required by nymphs to develop into alates.
#cfg_AphidNymphDeveDayDegUnwing   | The day degrees required by nymphs to develop into aptera.
#cfg_AphidNymphDeveDayDegUnwingStd   | The standard deviation of day degrees required by nymphs to develop into aptera.
#cfg_AphidLongevityParaA    | Parameter to calculate the longevity of aphid adults.
#cfg_AphidLongevityParaB    | Parameter to calculate the longevity of aphid adults.
#cfg_PeaAPhidSexualDeveRequiredDDeg | The day degrees required for sexual production in species without host switching behaviour.
#cfg_PeaAPhidSexualDeveRequiredDDegStd | The standard deviation of day degrees required for sexual reproduction in species without host switching behaviour.
#cfg_AphidNymphMortalityParaA   | Parameter to calculate the temperature dependent mortality rate for nymphs.
#cfg_AphidNymphMortalityParaB   | Parameter to calculate the temperature dependent mortality rate for nymphs.
#cfg_AphidNymphMortalityParaC   | Parameter to calculate the temperature dependent mortality rate for nymphs.
#cfg_AphidEggHatchChanceParaA   | Parameter to calculate the hatching chance for eggs based on their accumulated day degrees.
#cfg_AphidEggHatchChanceParaB   | Parameter to calculate the hatching chance for eggs based on their accumulated day degrees.
#cfg_AphisAphidSummerHostDayLength  | The minimum day length, in minutes, to make summer host plants available for aphids with host switching behaviour.
#cfg_AphisAphidWinterHostDayLength  | The maximum day length, in minutes, to make winter host plants available for aphids with host switching behaviour.

## Outputs
The output of aphid simulation can be reimplemented per request. Currently, there are three output files to store daily aphid population dynamics, one for the whole landscape, one for a non-crop polygon and the last for a corp polygon.

## State variables
Here several key state variables are listed. These variables vary throughout the simulation and represent the varying states of the model.

Table 3: Key state variables in aphid model.
Name  | Description
------------- | -------------
SubPopulation_Population_Manager::m_development_degree_day  | The array to store the accumulated degree days per day for all the aphid cohorts.
SubPopulation::m_animal_num_array   | The array to store the number of aphid in each aphid cohorts for all aphid life stages.

## Scales
The grid square size is 10 by 10 meters, but this can be changed by editing #cfg_AphidCellWidth and #cfg_AphidCellHeight. The step size is 1-day for both aphid and base landscape model.

# Calibration
The calibration output of the model is available in a [separate document](Doxygen_Pics/Aphid_calibration.pdf).


# Reference

\anchor emden2007aphids1 H.F. van Emden & R. Harrington (Eds.) (2007). Aphids as crop pests. <em>CAB International</em>.<br>

\anchor hulle2010aphids2 M. Hulle, A. Coeur d’Acier, S. Bankhead-Dronnet, & R. Harrington (2010). Aphids in the face of global changes. <em>Comptes Rendus Biologies</em>, 333(6-7):497–503.<br>

\anchor duffy2017improved3 C. Duffy, R. Fealy, & R. M. Fealy (2017). An improved simulation model to describe the temperature-dependent population dynamics of the grain aphid, <em>Sitobion avenae</em>. <em>Ecological Modelling</em>, 354:140–171.

\anchor Topping2022aphids3 C. J. Topping, L. Kondrup Marcussen, P. Thomsen & J. Checuti (2022). The Formal Model article format: justifying modelling intent and a critical review of data foundations through publication. *\Food and Ecological Systems Modelling Journal*, 3.<br>

\anchor topping2003aphids3 C. J. Topping, T. S. Hansen, T. S. Jensen, J. U. Jepsen, F. Nikolajsen & P. Odderskær (2003). ALMaSS, an agent-based model for animals in temperate European landscapes. <em>Ecological Modelling</em>, 167:65-82.