/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Aphid_Population_Manager.h 
\brief <B>The main header code for Aphid population manager</B>
*/
/**  \file Aphid_Population_Manager.h
Version of  Feb. 2021 \n
By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------
#ifndef Aphid_Population_ManagerH
#define Aphid_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


class Aphid;
class Aphid_Population_Manager;

/**
\brief
Used for creation of a new Aphid object
*/
class struct_Aphid
{
 public:
  /** \brief x-coord */
  int x;
  /** \brief y-coord */
  int y;
  /** \brief area width */
  int w;
  /** \brief area height */
  int h;
  /** \brief species ID */
  int species;
  /** \brief Landscape pointer */
  Landscape* L;
  /** \brief Aphid_Population_Manager pointer */
  Aphid_Population_Manager * NPM;
  /** \brief Indicator show whether it is an empty subpopulation. */
  bool empty_flag;
  /** \brief Starting suitability for the subpopulation. */
  double* starting_suitability;
  /** \brief Starting weighted population density for the subpopulation. */
  double* starting_popu_density;
  int index_x;
  int index_y;
  TTypesOfLandscapeElement winter_landscape_host;
  TTypesOfLandscapeElement summer_landscape_host;
  bool farm_flag;
};

enum TTypesOfAphidDevelopmentSeason: unsigned
{
  toAphidSpringDev=0,
  toAphidLateSpringDev,
  toAphidSummerDev,
  toAphidFallDev,
  toAphidHibernate
};

/**
\brief
The class to handle all Aphid population related matters in the whole landscape
*/
class Aphid_Population_Manager : public SubPopulation_Population_Manager
{
   /** \brief The variable to show the development season. */
   TTypesOfAphidDevelopmentSeason m_current_developtype;
   /** \brief The function pointer for calculation of next life stage. */
   int (Aphid_Population_Manager::*m_next_life_func_pointer)(int current_stage);
   /** \brief The function pointer for calculation of the shared base mortality rate for the whole landscape. */
   void (Aphid_Population_Manager::*m_update_mortality_array_pointer)();
   /** \brief The function pointer for calculation of the offsrping stage number. */
   int (Aphid_Population_Manager::*m_offspring_func_pointer)(int current_stage, double *offspring_num, double a_age, double a_density, double a_growth_stage, double* a_propotion, bool winter_host_flag);
   /** \brief THe function pointer for testing whether the given life stage is ready for the next one. */
   bool (Aphid_Population_Manager::*m_is_ready_next_liff_func_pointer)(int a_life_stage);
   /** \brief THe variable to store the maximum number of degree days for aphid. */
   int m_max_adult_dd;
   /** \brief The vector to store the age (in degree days) related reproduction offsping nymph. */
   vector <double> m_nymph_per_day;
   /** \brief The vector to store the age (in degree days) related reproduction offspring egg. */
   vector <double> m_egg_per_day; 
   /** \brief The vector to store the chance for an egg to hatch. */
   vector <double> m_egg_hatach_chances;
   /** \brief The weight to regulate the number of nymph produced per day based on temperature. */
   double m_nymph_produced_num_weight;
   /** \brief The flag to enable sexaul development. */
   bool m_egg_reproduction_flag;
   /** \brief The accumulated day degrees to enable sexaul development. */
   double m_accu_ddeg_egg_reproduction;
   /** \brief The flag to show the first hatch of egg. */
   bool m_first_egg_hatch_flag;
   /** \brief The flag to enable parasitoid development. */
   bool m_parasitoid_development_flag;
   /** \brief The accumulated degree days to enable parasitoid development. */
   double m_accu_ddeg_parasitoid;
   /** \brief The number of eggs that parasitoid can lay for life time. */
   double m_parasitoid_egg_lifetime;
   /** \brief The accumulated degree days for parasitoid eggs. */
   double m_parasitoid_egg_accu_ddeg;
   /** \brief The flag to show whether parasitoid eggs fininsh killing. */
   bool m_parasitoid_egg_kill_flag;
   /** \brief The longest day degrees that a adult can have. */
   double m_max_day_degree_adult;
   #ifdef __APHID_CALIBRATION
      FILE* m_aphid_calib_crop_output;
      FILE* m_aphid_calib_tole_output;
      FILE* m_aphid_calib_land_output;
   #endif
   
public:
   Aphid_Population_Manager(Landscape* L);
   virtual ~Aphid_Population_Manager();
   Aphid* CreateObjects(TAnimal *pvo, struct_Aphid* data, int number);
   virtual int calNextStage(int current_stage, double density = 1);
   /** \brief The function to update the development season. */
   virtual void updateDevelopmentSeason();
   /** \brief The function to supply the development season. */
   virtual unsigned supplyDevelopmentSeason () {return m_current_developtype;}
   /** \brief The function to initialise the population when starting aphid simulation, this function is called in the subpopulation_mananger constructor. */
   virtual void initialisePopulation();
   /** \brief The function to calculate the next life stage for pea aphid. */
   int calNextStageShared(int current_stage);
   /** \brief The function to calculate the next life stage for black bean aphid. */
   int calNextStageBlackBean(int current_sgate);
   /** \brief The function to initilise the simulation with eggs. */
   void initialiseSimWithEggs(void);
   /** \brief The fucntion to update the base mortality rate, it will call the corresponding function pointer. */
   virtual void updateMortalityArray(void);
   /** \brief The function to calculate the base mortality rate for pea aphid. */
   void updateMortalityArrayPeaAphid(void);
   /** \brief The function to calculate the base mortality rate for English grain aphid. */
   void updateMortalityArrayShared(void);
   /** \brief The function to calculate the offspring sage. */
   virtual int calOffspringStage(int current_stage, double *offspring_num=NULL, double a_age=1, double a_density=-1, double a_growth_stage=0, double* a_propotion=NULL, bool winter_host_flag=false);
   int calOffspringStagePea(int current_stage, double *offspring_num=NULL, double a_age=1, double a_density=-1, double a_growth_stage=0, double* a_propotion=NULL, bool winter_host_flag=false);
   int calOffspringStageAphis(int current_stage, double *offspring_num=NULL, double a_age=1, double a_density=-1, double a_growth_stage=0, double* a_propotion=NULL, bool winter_host_flag=false);
   /** \breif The functions to test whether it is ready for the next life stages for aphid.*/
   virtual bool isEnoughNextLifeStage(int a_life_stage);
   bool isEnoughNextLifeStagePeaAphid(int a_life_stage);
   bool isEnoughNextLifeStageEnglishGrainAphid(int a_life_stage);
   /** \brief The function to return the daily nymph number weight. */
   double supplyNymphNumWeight(void) {return m_nymph_produced_num_weight;}
   virtual void doParasitoidDevelopment();
   virtual void doSpecicesLastThing();
   virtual void writeCalibrationFiles(void);
   #ifdef __APHID_CALIBRATION
      bool openAphidCalibFiles(void);
      void closeAphidCalibFiles(void);
      void writeAphidCalibData(void);
      void writeRowAphidCalibData(FILE* target_file, CfgArray_Int poly_id_list);
   #endif   
};

#endif
