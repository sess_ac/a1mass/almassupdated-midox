/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Aphid.h
\brief <B>The main header code for Aphid class.</B>
*/
/**  \file Aphid.h
Version of  Feb. 2021 \n
By Xiaodong \n \n
*/

//---------------------------------------------------------------------------
#ifndef AphidH
#define AphidH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class Aphid_Population_Manager;
//------------------------------------------------------------------------------

/**
 * \brief
Enumerator for aphid object types
*/
enum Aphid_Object : int
{
   toa_Egg = 0,
   toa_Nymph,
   toa_Nymph_W,
   toa_Nymph_F,
   toa_Nymph_M,
   toa_Aptera,
   toa_Alate,
   toa_Female,
   toa_Male,
   toa_WingedFemale,
   toa_WingedMale,
   toa_count
};

/**
\brief
The class for base Aphid using subpopulation method.
*/
class Aphid : public SubPopulation
{
private:
   /** \brief Variable to hold the plant juice in the cell. */
   double m_plant_juice;
   /** \brief The function pointer for suitablility calculations. */
   void (Aphid::*m_cal_suitability_func_pointer)(void);
   /** \brief The function pointer for biotic mortality rate calculation. */
   double (Aphid::*m_cal_biotic_mortality_func_pointer)(int);
   /** \brief The array to store the previous density in the past days */
   std::vector<double> m_history_density_vec;

public:
   /** \brief Aphid constructor */
   Aphid(int p_x, int p_y, int p_w, int p_h, Landscape *p_L, Aphid_Population_Manager *p_NPM, bool a_empty_flag, double *p_suitability, double *p_weight_density, int number, int a_index_x, int a_index_y, int a_SpeciesID = 919, TTypesOfLandscapeElement p_winter_landscape_host = tole_Foobar, TTypesOfLandscapeElement p_summer_landscape_host = tole_Foobar, bool p_farm_flag = false);
   virtual bool OnFarmEvent(FarmToDo event);

   virtual void calPopuDensity(void);
   virtual void calSuitability(void);
   void calSuitabilityShared(void);
   virtual void doReproduction(void);
   virtual void doMovement(void);
   virtual void doDropingWings(void);
   virtual double calCellRelatedMortalityWeight(void);
   virtual double calBioticMortalityRate(int a_life_stage);
   double calBioticMortalityRateShared(int a_life_stage);
   /** \brief The function to set the parasitoid number. */
   virtual void setParasitoidNum(double a_num) { m_parasitoid_num = a_num; }
   /** \brief The function to set the parasitoid egg number. */
   virtual void setParasitoidEggNum(double a_num) { m_parasitoid_egg_num = a_num; }
   /** \brief The function to give birth of parasitoid eggs. */
   virtual void giveBirthParasitoidEgg(double a_egg_num);
   /** \brief The function to kill aphids by parasitoid. */
   virtual void killByParasitoid();
   /** \brief The function to hatch parasitoid eggs. */
   virtual void hatchParasitoidEggs();
   /** \brief The function to kill all aphids in the cell due to e.g. pesticide applicaiont. */
   void killAllPopulation();
   virtual void BeginStep(void);
};

#endif