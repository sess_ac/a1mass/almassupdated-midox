/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file SubPopulationPopulationManager.cpp
\brief <B>The main source code for subpopulation population manager class</B>
*/
/**  \file SubPopulationPopulationManager.cpp
Version of  Feb. 2021 \n
By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"
#include "../Aphid/Aphid.h"
#include "../Aphid/Aphid_Population_Manager.h"


using namespace std;
using namespace blitz;


extern Landscape* g_landscape_p;
extern FarmManager* g_farmmanager;

CfgStr cfg_AphidDevelopmentFile("APHID_DEVELOPMENT_FILE", CFG_CUSTOM, "Subpopulation/aphid_info.txt");
static CfgInt cfg_AphidEggStartingNum("APHID_EGG_STARINGNUM", CFG_CUSTOM, 1000);
/** \brief The longest distance that an aphid with wings can fly. */
CfgFloat cfg_AphidMaxShortDistance("APHID_MAX_SHORT_DISTANCE", CFG_CUSTOM, 1000);
/** \brief The longest distance that an aphid WITHOUT wings can fly. (meters)*/
CfgFloat cfg_AphidMaxLongDistance("APHID_MAX_LONG_DISTANCE", CFG_CUSTOM, 1000);
/** \brief The distance for the peak amount when the winged adults fly. (meters) */
CfgFloat cfg_AphidPeakLongDistance("APHID_PEAK_LONG_DISTANCE", CFG_CUSTOM, 100);
/** \brief The scale for the wind speed. */
CfgFloat cfg_AphidScaleWindSpeed("APHID_SCALE_WIND_SPEED", CFG_CUSTOM, 1.01);
/** \brief The maximum speed that allows an aphid to fly. */
CfgFloat cfg_AphidMaxWindSpeed("APHID_MAX_WIND_SPEED", CFG_CUSTOM, 10);
/** \brief The number of wind directions used to calculate the movement masks. */
CfgFloat cfg_AphidMovMaskWindDirectionNum("APHID_MOV_MASK_WIND_DIRECTION_NUM", CFG_CUSTOM, 4);
/** \brief The sampling number of wind speed used to calculate the movement masks. */
CfgFloat cfg_AphidMovMaskWindSpeedStepSize("APHID_MOV_MASK_WIND_SPEED_STEP_SIZE", CFG_CUSTOM, 2);
/** \brief The maximum days for all the lifestages. */
CfgInt cfg_AphidMaxAliveDay("APHID_MAX_ALIVE_DAY", CFG_CUSTOM, 200);
/** \brief The number of life stages for the aphids. */
CfgInt cfg_AphidLifeStageNum("APHID_LIFE_STAGE_NUM", CFG_CUSTOM, 9);
/** \brief The width of the cell on the landscape for the aphids. -x */
CfgInt cfg_AphidCellWidth("APHID_CELL_WIDTH", CFG_CUSTOM, 100);
/** \brief The height of the cell on the landscape for the aphids. -y*/
CfgInt cfg_AphidCellHeight("APHID_CELL_HEIGHT", CFG_CUSTOM, 100);
/** \brief This variable decdices which aphid species to run. */
//CfgStr cfg_AphidSpeciesName("APHID_SPECIES_NAME", CFG_CUSTOM, "Aphis fabae");
CfgStr cfg_AphidSpeciesName("APHID_SPECIES_NAME", CFG_CUSTOM, "Pea aphid");
/** \brief The file for storing hosts for pea aphid. The file contains five rows, host switch flag (1 no switch, 2 switch between winter and summer), summer tole, summer tov, winter tole, winter tov (-1 means there is no host). When the aphids dont't change hosts between summer and winter, only the summer host arrays is used.*/
CfgStr cfg_PeaAphidHostFile("PEA_APHID_HOST_FILE", CFG_CUSTOM, "Subpopulation/pea_aphid_host.txt");
CfgFloat cfg_AphidEggMortalityRate("APHID_EGG_MORALITY_RATE", CFG_CUSTOM, 0.0119);
CfgFloat cfg_AphidEggMortalityRateStd("APHID_EGG_MORALITY_RATE_STD", CFG_CUSTOM, 0.00077);
//CfgFloat cfg_PeaAphidNymphBaseDeveTempe("PEA_APHID_NYMPH_BASE_DEVE_TEMPE", CFG_CUSTOM, 4.0);
CfgFloat cfg_PeaAphidDayLengthMale("PEA_APHID_DAY_LENGTH_MALE", CFG_CUSTOM, 13.5);
CfgFloat cfg_PeaAphidDayLengthFemale("PEA_APHID_DAY_LENGTH_FEMALE", CFG_CUSTOM, 13.0);
CfgFloat cfg_AphidNymphReproParaA1("APHID_NYMPH_REPRO_PARA_A1", CFG_CUSTOM, 0.1199);
CfgFloat cfg_AphidNymphReproParaB1("APHID_NYMPH_REPRO_PARA_B1", CFG_CUSTOM, 2.9174);
CfgFloat cfg_AphidNymphReproParaA2("APHID_NYMPH_REPRO_PARA_A2", CFG_CUSTOM, -0.0332);
CfgFloat cfg_AphidNymphReproParaB2("APHID_NYMPH_REPRO_PARA_B2", CFG_CUSTOM, 9.9696);
CfgFloat cfg_AphidNymphReproInfectionADD("APHID_NYMPH_RERPR_INFLECTION_ADD", CFG_CUSTOM, 66.5);
CfgFloat cfg_AphidNymphReproTempWeightParaA("APHID_NYMPH_REPRO_TEMP_WEIGHT_PARA_A", CFG_CUSTOM, -0.006);
CfgFloat cfg_AphidNymphReproTempWeightParaB("APHID_NYMPH_REPRO_TEMP_WEIGHT_PARA_B", CFG_CUSTOM, 0.204);
CfgFloat cfg_AphidNymphReproTempWeightParaC("APHID_NYMPH_REPRO_TEMP_WEIGHT_PARA_C", CFG_CUSTOM, -0.76);
CfgFloat cfg_AphidAlatePropParaA("APHID_ALATE_PROP_PARA_A", CFG_CUSTOM, 2.603);
CfgFloat cfg_AphidAlatePropParaB("APHID_ALATE_PROP_PARA_B", CFG_CUSTOM, -27.189);
CfgFloat cfg_AphidAlatePropParaC("APHID_ALATE_PROP_PARA_C", CFG_CUSTOM, 0.847);
CfgFloat cfg_AphidNymphDeveDayDegWing("APHID_NYMPH_DEVE_DAY_DEG_WING", CFG_CUSTOM, 148.59);
CfgFloat cfg_AphidNymphDeveDayDegWingStd("APHID_NYMPH_DEVE_DAY_DEG_WING_STD", CFG_CUSTOM, 4.48);
CfgFloat cfg_AphidNymphDeveDayDegUnwing("APHID_NYMPH_DEVE_DAY_DEG_UNWING", CFG_CUSTOM, 140.25);
CfgFloat cfg_AphidNymphDeveDayDegUnwingStd("APHID_NYMPH_DEVE_DAY_DEG_UNWING_STD", CFG_CUSTOM, 2.62);
CfgFloat cfg_AphidLongevityParaA("APHID_ADULT_LONGEVITY_PARA_A", CFG_CUSTOM, -0.0047);
CfgFloat cfg_AphidLongevityParaB("APHID_ADULT_LONGEVITY_PARA_B", CFG_CUSTOM, 1.892);
CfgFloat cfg_PeaAPhidSexualDeveRequiredDDeg("CFG_PEA_APHID_SEXUAL_DEVE_REQUIRED_DDEG", CFG_CUSTOM, 1157);
CfgFloat cfg_PeaAPhidSexualDeveRequiredDDegStd("CFG_PEA_APHID_SEXUAL_DEVE_REQUIRED_DDEG_STD", CFG_CUSTOM, 73);
CfgFloat cfg_AphidPredatorsMortality("APHID_PREDATORS_MORTALITY", CFG_CUSTOM, 0.05);
CfgFloat cfg_AphidParasitoidBaseDevTemp("APHID_PARASITOID_BASE_DEV_TEMP", CFG_CUSTOM, 6.1);
CfgFloat cfg_AphidParasitoidStartDdeg("APHID_PARASITOID_START_DDEG", CFG_CUSTOM, 30);
CfgFloat cfg_AphidParasitoidStartNum("APHID_PARASITOID_START_NUM", CFG_CUSTOM, 10);
CfgFloat cfg_AphidParasitoidLifeTime("APHID_PARASITOID_LIFE_TIME", CFG_CUSTOM, 7);
CfgFloat cfg_AphidParasitoidEggPerDay("APHID_PARASITOID_EGG_PER_DAY", CFG_CUSTOM, 5.28);
CfgFloat cfg_AphidParasitoidKillDdeg("APHID_PARASITOID_KILL_DDEG", CFG_CUSTOM, 73);
CfgFloat cfg_AphidParasitoidHatchDdeg("APHID_PARASITOID_HATCH_DDEG", CFG_CUSTOM, 180);
CfgFloat cfg_AphidEggMortalityIncre("PHID_EGG_MORTALITY_INCRE", CFG_CUSTOM, 3);
CfgFloat cfg_AphidEggReproductionParaA("APHID_EGG_REPRODUCTION_PARA_A", CFG_CUSTOM, 0.2469);
CfgFloat cfg_AphidEggReproductionParaB("APHID_EGG_REPRODUCTION_PARA_B", CFG_CUSTOM, -0.266);
CfgFloat cfg_AphidEggReproductionParaC("APHID_EGG_REPRODUCTION_PARA_C", CFG_CUSTOM, 18.9);
CfgFloat cfg_AphidEggReproductionParaSTD("APHID_EGG_REPRODUCTION_PARA_STD", CFG_CUSTOM, 8.3);
CfgArray_Int cfg_AphidCalibrationPolygonToStoreTOLE("APHID_CALIBRATION_POLYGON_TO_STORE_TOLE", CFG_CUSTOM, 1, vector<int>{384});
CfgArray_Int cfg_AphidCalibrationPolygonToStoreCROP("APHID_CALIBRATION_POLYGON_TO_STORE_CROP", CFG_CUSTOM, 1, vector<int>{14});

CfgStr cfg_EnglishGrainAphidHostFile("ENGLISH_GRAIN_APHID_HOST_FILE", CFG_CUSTOM, "Subpopulation/english_grain_aphid_host.txt");
CfgFloat cfg_AphidNymphMortalityParaA("APHID_NYMPH_MORTALITY_PARA_A", CFG_CUSTOM, 0.0025);
CfgFloat cfg_AphidNymphMortalityParaB("APHID_NYMPH_MORTALITY_PARA_B", CFG_CUSTOM, -0.059);
CfgFloat cfg_AphidNymphMortalityParaC("APHID_NYMPH_MORTALITY_PARA_C", CFG_CUSTOM, 0.3081);


CfgFloat cfg_AphisAphidSummerHostDayLength("APHIS_APHID_SUMMER_HOST_DAY_LENGTH", CFG_CUSTOM, 600);
CfgFloat cfg_AphisAphidWinterHostDayLength("APHIS_APHID_WINTER_HOST_DAY_LENGTH", CFG_CUSTOM, 1000);

CfgStr cfg_BlackBeanAphidHostFile("BLACK_BEAN_APHID_HOST_FILE", CFG_CUSTOM, "Subpopulation/black_bean_aphid_host.txt");
CfgStr cfg_PeachPotatoAphidHostFile("PEACH_POTATO_APHID_HOST_FILE", CFG_CUSTOM, "Subpopulation/peach_potato_aphid_host.txt");
CfgFloat cfg_AphidEggStartingADD("APHID_EGG_STARTING_ADD", CFG_CUSTOM, 70);
CfgFloat cfg_AphidEggHatchChanceParaA("APHID_EGG_HATCH_CHANCE_PARA_A", CFG_CUSTOM, 1.1126);
CfgFloat cfg_AphidEggHatchChanceParaB("APHID_EGG_HATCH_CHANCE_PARA_B", CFG_CUSTOM, -141.01);
//---------------------------------------------------------------------------

Aphid_Population_Manager::Aphid_Population_Manager(Landscape* L) : SubPopulation_Population_Manager(L, cfg_AphidDevelopmentFile.value(), cfg_AphidCellWidth.value(), cfg_AphidCellHeight.value(), cfg_AphidLifeStageNum.value(), cfg_AphidMaxLongDistance.value(), cfg_AphidPeakLongDistance.value(), cfg_AphidScaleWindSpeed.value(), cfg_AphidMaxWindSpeed.value(), cfg_AphidMovMaskWindDirectionNum.value(), cfg_AphidMovMaskWindSpeedStepSize.value(), cfg_AphidMaxAliveDay.value())
{
	/**
	 * Construct a new Aphid_Population_Manger object
	 * 
	 */
	initialisePopulation();	
	#ifdef __APHID_CALIBRATION
		openAphidCalibFiles();
	#endif

	//initialisation for common computation
	m_egg_reproduction_flag = false;
	m_first_egg_hatch_flag = false;
	m_accu_ddeg_egg_reproduction = 0;
	m_parasitoid_development_flag = false;
	m_accu_ddeg_egg_reproduction = 0;
	m_parasitoid_egg_lifetime= cfg_AphidParasitoidEggPerDay.value() * cfg_AphidParasitoidLifeTime.value();
	m_parasitoid_egg_accu_ddeg = -1;
	
	if (strcmp(cfg_AphidSpeciesName.value(), "Pea aphid")==0){
		//maximum length of degree days for adult aphid
		m_max_adult_dd = ceil(-1.0*cfg_AphidLongevityParaB.value()/cfg_AphidLongevityParaA.value());
		m_nymph_per_day.resize(m_max_adult_dd+1);
		m_egg_per_day.resize(m_max_adult_dd+1);
		m_egg_hatach_chances.resize(m_max_adult_dd+1);
		//initialise the look up vectors for offpring
		for (int i=0; i<=m_max_adult_dd; i++){
			if (i<=cfg_AphidNymphReproInfectionADD.value()){
				m_nymph_per_day.at(i) = cfg_AphidNymphReproParaA1.value()*i + cfg_AphidNymphReproParaB1.value();
				
			}
			else {
				m_nymph_per_day.at(i)= cfg_AphidNymphReproParaA2.value()*i + cfg_AphidNymphReproParaB2.value();
			}
			if (m_nymph_per_day.at(i)<0) m_nymph_per_day.at(i) = 0;
			m_egg_per_day.at(i) = cfg_AphidEggReproductionParaA.value()*exp(cfg_AphidEggReproductionParaB.value()*i)*(cfg_AphidEggReproductionParaC.value() + (g_rand_uni()-0.5)*2*cfg_AphidEggReproductionParaSTD.value());
			if (m_egg_per_day.at(i)<0) m_egg_per_day.at(i) =0;
			m_egg_hatach_chances.at(i) = cfg_AphidEggHatchChanceParaA.value()*i+cfg_AphidEggHatchChanceParaB.value();
			if (m_egg_hatach_chances.at(i)<0) m_egg_hatach_chances.at(i) = 0;
			if (m_egg_hatach_chances.at(i)>1) m_egg_hatach_chances.at(i) = 1;
		}
	}

	//English grain aphid
	if (strcmp(cfg_AphidSpeciesName.value(), "English grain aphid")==0){
		//maximum length of degree days for adult aphid
		m_max_adult_dd = ceil(-1.0*cfg_AphidLongevityParaB.value()/cfg_AphidLongevityParaA.value());
		m_nymph_per_day.resize(m_max_adult_dd+1);
		m_egg_per_day.resize(m_max_adult_dd+1);
		m_egg_hatach_chances.resize(m_max_adult_dd+1);
		//initialise the look up vectors for offpring
		for (int i=0; i<=m_max_adult_dd; i++){
			if (i<=cfg_AphidNymphReproInfectionADD.value()){
				m_nymph_per_day.at(i) = cfg_AphidNymphReproParaA1.value()*i + cfg_AphidNymphReproParaB1.value();
				
			}
			else {
				m_nymph_per_day.at(i)= cfg_AphidNymphReproParaA2.value()*i + cfg_AphidNymphReproParaB2.value();
			}
			if (m_nymph_per_day.at(i)<0) m_nymph_per_day.at(i) = 0;
			m_egg_per_day.at(i) = cfg_AphidEggReproductionParaA.value()*exp(cfg_AphidEggReproductionParaB.value()*i)*(cfg_AphidEggReproductionParaC.value() + (g_rand_uni()-0.5)*2*cfg_AphidEggReproductionParaSTD.value());
			if (m_egg_per_day.at(i)<0) m_egg_per_day.at(i) =0;
			m_egg_hatach_chances.at(i) = cfg_AphidEggHatchChanceParaA.value()*i+cfg_AphidEggHatchChanceParaB.value();
			if (m_egg_hatach_chances.at(i)<0) m_egg_hatach_chances.at(i) = 0;
			if (m_egg_hatach_chances.at(i)>1) m_egg_hatach_chances.at(i) = 1;
		}
	}

	//Black bean aphids
	if (strcmp(cfg_AphidSpeciesName.value(), "Aphis fabae")==0){
		//maximum length of degree days for adult aphid
		m_max_adult_dd = ceil(-1.0*cfg_AphidLongevityParaB.value()/cfg_AphidLongevityParaA.value());
		m_nymph_per_day.resize(m_max_adult_dd+1);
		m_egg_per_day.resize(m_max_adult_dd+1);
		m_egg_hatach_chances.resize(m_max_adult_dd+1);
		//initialise the look up vectors for offpring
		for (int i=0; i<=m_max_adult_dd; i++){
			if (i<=cfg_AphidNymphReproInfectionADD.value()){
				m_nymph_per_day.at(i) = cfg_AphidNymphReproParaA1.value()*i + cfg_AphidNymphReproParaB1.value();
				
			}
			else {
				m_nymph_per_day.at(i)= cfg_AphidNymphReproParaA2.value()*i + cfg_AphidNymphReproParaB2.value();
			}
			if (m_nymph_per_day.at(i)<0) m_nymph_per_day.at(i) = 0;
			m_egg_per_day.at(i) = cfg_AphidEggReproductionParaA.value()*exp(cfg_AphidEggReproductionParaB.value()*i)*(cfg_AphidEggReproductionParaC.value() + (g_rand_uni()-0.5)*2*cfg_AphidEggReproductionParaSTD.value());
			if (m_egg_per_day.at(i)<0) m_egg_per_day.at(i) =0;
			m_egg_hatach_chances.at(i) = cfg_AphidEggHatchChanceParaA.value()*i+cfg_AphidEggHatchChanceParaB.value();
			if (m_egg_hatach_chances.at(i)<0) m_egg_hatach_chances.at(i) = 0;
			if (m_egg_hatach_chances.at(i)>1) m_egg_hatach_chances.at(i) = 1;
		}
	}

	//Peach potato aphid
	if (strcmp(cfg_AphidSpeciesName.value(), "Peach potato aphid")==0){
		//maximum length of degree days for adult aphid
		m_max_adult_dd = ceil(-1.0*cfg_AphidLongevityParaB.value()/cfg_AphidLongevityParaA.value());
		m_nymph_per_day.resize(m_max_adult_dd+1);
		m_egg_per_day.resize(m_max_adult_dd+1);
		m_egg_hatach_chances.resize(m_max_adult_dd+1);
		//initialise the look up vectors for offpring
		for (int i=0; i<=m_max_adult_dd; i++){
			if (i<=cfg_AphidNymphReproInfectionADD.value()){
				m_nymph_per_day.at(i) = cfg_AphidNymphReproParaA1.value()*i + cfg_AphidNymphReproParaB1.value();
				
			}
			else {
				m_nymph_per_day.at(i)= cfg_AphidNymphReproParaA2.value()*i + cfg_AphidNymphReproParaB2.value();
			}
			if (m_nymph_per_day.at(i)<0) m_nymph_per_day.at(i) = 0;
			m_egg_per_day.at(i) = cfg_AphidEggReproductionParaA.value()*exp(cfg_AphidEggReproductionParaB.value()*i)*(cfg_AphidEggReproductionParaC.value() + (g_rand_uni()-0.5)*2*cfg_AphidEggReproductionParaSTD.value());
			if (m_egg_per_day.at(i)<0) m_egg_per_day.at(i) =0;
			m_egg_hatach_chances.at(i) = cfg_AphidEggHatchChanceParaA.value()*i+cfg_AphidEggHatchChanceParaB.value();
			if (m_egg_hatach_chances.at(i)<0) m_egg_hatach_chances.at(i) = 0;
			if (m_egg_hatach_chances.at(i)>1) m_egg_hatach_chances.at(i) = 1;
		}
	}
}

Aphid* Aphid_Population_Manager::CreateObjects(TAnimal *pvo, struct_Aphid* data, int number){
   Aphid*  new_Aphid;
   new_Aphid = new Aphid(data->x, data->y, data->w, data->h, data->L, data->NPM, data->empty_flag, data->starting_suitability, data->starting_popu_density, number, data->index_x, data->index_y, data->species, data->winter_landscape_host, data->summer_landscape_host, data->farm_flag);
   // always 0 since it is a subpopulation model

   //the simulation starts from Jan, we only have some eggs from the beginning.
   if(number > 0){
	//updateWholePopulationArray(toa_Egg, number);
	new_Aphid -> addAnimalNumGivenStageColumn(toa_Egg, 0, number);
	}
   return new_Aphid;
}

int Aphid_Population_Manager::calNextStage(int current_stage, double density){
	return (this->*m_next_life_func_pointer)(current_stage);
}

void Aphid_Population_Manager::updateMortalityArray(void){
	(this->*m_update_mortality_array_pointer)();
}

void Aphid_Population_Manager::updateDevelopmentSeason(){
	if(!m_mul_hosts_flag){
		return;
	}
	else{
		double current_day_length = g_landscape_p->SupplyDaylength();
		if(current_day_length >= cfg_AphisAphidSummerHostDayLength.value()){
			m_summer_host_on = true;
		}
		else{
			m_summer_host_on = false;
		}

		if(current_day_length <= cfg_AphisAphidWinterHostDayLength.value()){
			m_winter_host_on = true;
		}
		else{
			m_winter_host_on = false;
		}
	}

	/*
	//For now, it is only time dependent, more are needed to add here
	//Early spring eggs hatch
	if(m_TheLandscape->SupplyMonth()==4 && m_current_developtype != toAphidSpringDev){
		m_current_developtype = toAphidSpringDev;
		m_hibernated_hatch_flag = true;
	}

	//Late spring
	if(m_TheLandscape->SupplyMonth()==5 && m_current_developtype != toAphidLateSpringDev){
		m_current_developtype = toAphidLateSpringDev;
	}

	//summer
	if(m_TheLandscape->SupplyMonth()==6 && m_current_developtype != toAphidSummerDev){
		m_current_developtype = toAphidSummerDev;
	}

	//autumn
	if(m_TheLandscape->SupplyMonth()==9 && m_current_developtype != toAphidFallDev){
		m_current_developtype = toAphidFallDev;
	}

	//winter
	if(m_TheLandscape->SupplyMonth()==12 && m_current_developtype != toAphidHibernate){
		m_current_developtype = toAphidHibernate;
	}
	*/

}

void Aphid_Population_Manager::initialisePopulation(){
	//common code for all the aphid species
	//Pea aphid
	cout<<cfg_AphidSpeciesName.value()<<endl;
	if (strcmp(cfg_AphidSpeciesName.value(), "Pea aphid")==0){
		m_ListNames[0]="Egg";
		m_ListNames[1]="Nymph";
		m_ListNames[2]="NymphW";
		m_ListNames[3]="NymphF";
		m_ListNames[4]="NymphM";
		m_ListNames[5]="Aptera";
		m_ListNames[6]="Alate";
		m_ListNames[7]="Female";
		m_ListNames[8]="Male";
		m_ListNameLength = 9;
		m_SimulationName = "Pea aphid simulation";
		//read the host file
		readHosts(cfg_PeaAphidHostFile.value());


		//set the function pointers
		m_next_life_func_pointer = &Aphid_Population_Manager::calNextStageShared;
		m_update_mortality_array_pointer = &Aphid_Population_Manager::updateMortalityArrayPeaAphid;
		m_offspring_func_pointer = &Aphid_Population_Manager::calOffspringStagePea;
		m_is_ready_next_liff_func_pointer = &Aphid_Population_Manager::isEnoughNextLifeStagePeaAphid;

		//Life stages that can fly
		m_flying_life_stage_array.clear();
		m_flying_life_stage_array.push_back(toa_Alate);
		m_landing_life_stage_array.clear();
		m_landing_life_stage_array.push_back(toa_Aptera);
		m_local_moving_life_stage_array.clear();
		m_local_moving_life_stage_array.push_back(toa_Aptera);
		//m_local_moving_life_stage_array.push_back(toa_Alate);
		//m_local_moving_life_stage_array.push_back(toa_Female);

		m_max_day_degree_adult = -1*cfg_AphidLongevityParaB.value()/cfg_AphidLongevityParaA.value();
		
	}

	//English grain aphid
	if (strcmp(cfg_AphidSpeciesName.value(), "English grain aphid")==0){
		m_ListNames[0]="Egg";
		m_ListNames[1]="Nymph";
		m_ListNames[2]="NymphW";
		m_ListNames[3]="NymphF";
		m_ListNames[4]="NymphM";
		m_ListNames[5]="Aptera";
		m_ListNames[6]="Alate";
		m_ListNames[7]="Female";
		m_ListNames[8]="Male";
		m_ListNameLength = 9;
		m_SimulationName = "English grain aphid simulation";
		//read the host file
		readHosts(cfg_EnglishGrainAphidHostFile.value());


		//set the function pointers
		m_next_life_func_pointer = &Aphid_Population_Manager::calNextStageShared;
		m_update_mortality_array_pointer = &Aphid_Population_Manager::updateMortalityArrayShared;
		//same as pea aphid
		m_offspring_func_pointer = &Aphid_Population_Manager::calOffspringStagePea;
		m_is_ready_next_liff_func_pointer = &Aphid_Population_Manager::isEnoughNextLifeStageEnglishGrainAphid;

		//Life stages that can fly
		m_flying_life_stage_array.clear();
		m_flying_life_stage_array.push_back(toa_Alate);
		m_landing_life_stage_array.clear();
		m_landing_life_stage_array.push_back(toa_Aptera);
		m_local_moving_life_stage_array.clear();
		m_local_moving_life_stage_array.push_back(toa_Aptera);
		//m_local_moving_life_stage_array.push_back(toa_Alate);
		//m_local_moving_life_stage_array.push_back(toa_Female);

		m_max_day_degree_adult = -1*cfg_AphidLongevityParaB.value()/cfg_AphidLongevityParaA.value();
		
	}

	//Aphis fabae
	if (strcmp(cfg_AphidSpeciesName.value(), "Aphis fabae")==0){
		m_ListNames[0]="Egg";
		m_ListNames[1]="Nymph";
		m_ListNames[2]="NymphW";
		m_ListNames[3]="NymphF";
		m_ListNames[4]="NymphM";
		m_ListNames[5]="Aptera";
		m_ListNames[6]="Alate";
		m_ListNames[7]="Female";
		m_ListNames[8]="Male";
		m_ListNameLength = 9;
		m_SimulationName = "Aphis fabae Simulation";
		readHosts(cfg_BlackBeanAphidHostFile.value());

		//set the function pointers
		m_next_life_func_pointer = &Aphid_Population_Manager::calNextStageShared;
		m_update_mortality_array_pointer = &Aphid_Population_Manager::updateMortalityArrayShared;
		//same as pea aphid
		m_offspring_func_pointer = &Aphid_Population_Manager::calOffspringStageAphis;
		m_is_ready_next_liff_func_pointer = &Aphid_Population_Manager::isEnoughNextLifeStageEnglishGrainAphid;

		//Life stages that can fly
		m_flying_life_stage_array.clear();
		m_flying_life_stage_array.push_back(toa_Alate);
		m_landing_life_stage_array.clear();
		m_landing_life_stage_array.push_back(toa_Aptera);
		m_local_moving_life_stage_array.clear();
		m_local_moving_life_stage_array.push_back(toa_Aptera);
		//m_local_moving_life_stage_array.push_back(toa_Alate);
		//m_local_moving_life_stage_array.push_back(toa_Female);

		m_max_day_degree_adult = -1*cfg_AphidLongevityParaB.value()/cfg_AphidLongevityParaA.value();
	}

	//Peach poatao aphid
	if (strcmp(cfg_AphidSpeciesName.value(), "Peach potato aphid")==0){
		m_ListNames[0]="Egg";
		m_ListNames[1]="Nymph";
		m_ListNames[2]="NymphW";
		m_ListNames[3]="NymphF";
		m_ListNames[4]="NymphM";
		m_ListNames[5]="Aptera";
		m_ListNames[6]="Alate";
		m_ListNames[7]="Female";
		m_ListNames[8]="Male";
		m_ListNameLength = 9;
		m_SimulationName = "Peach potato aphid Simulation";
		readHosts(cfg_PeachPotatoAphidHostFile.value());

		//set the function pointers
		m_next_life_func_pointer = &Aphid_Population_Manager::calNextStageShared;
		m_update_mortality_array_pointer = &Aphid_Population_Manager::updateMortalityArrayShared;
		//same as pea aphid
		m_offspring_func_pointer = &Aphid_Population_Manager::calOffspringStageAphis;
		m_is_ready_next_liff_func_pointer = &Aphid_Population_Manager::isEnoughNextLifeStageEnglishGrainAphid;

		//Life stages that can fly
		m_flying_life_stage_array.clear();
		m_flying_life_stage_array.push_back(toa_Alate);
		m_landing_life_stage_array.clear();
		m_landing_life_stage_array.push_back(toa_Aptera);
		m_local_moving_life_stage_array.clear();
		m_local_moving_life_stage_array.push_back(toa_Aptera);
		//m_local_moving_life_stage_array.push_back(toa_Alate);
		//m_local_moving_life_stage_array.push_back(toa_Female);

		m_max_day_degree_adult = -1*cfg_AphidLongevityParaB.value()/cfg_AphidLongevityParaA.value();
	}

	//initialise the simulation
	initialiseSimWithEggs();		
}

/**
 * @brief This function is used both for pean and english grain aphid.
 * 
 * @param current_stage 
 * @return int 
 */
int Aphid_Population_Manager::calNextStageShared(int current_stage){
	//Spring
	
	if (current_stage == toa_Egg){
		//set the first hatch flag to be true
		if(!m_first_egg_hatch_flag) {
			m_first_egg_hatch_flag = true;
			m_egg_reproduction_flag = false;
		}
		return toa_Nymph;
	}

	if(current_stage == toa_Aptera || current_stage == toa_Alate || current_stage == toa_Female || current_stage == toa_Male){
		return -1; // let them die
	}

	if(current_stage == toa_Nymph){
		return toa_Aptera;
	}

	if(current_stage == toa_Nymph_W){
		return toa_Alate;
	}

	if(current_stage == toa_Nymph_F){
		return toa_Female;
	}

	if(current_stage == toa_Nymph_M){
		return toa_Male;
	}
}

int Aphid_Population_Manager::calNextStageBlackBean(int current_stage){
	//Spring
	if(m_current_developtype == toAphidSpringDev){
		if (current_stage == toa_Egg){
			return toa_Aptera;
		}
	}

	if(current_stage == toa_Aptera || current_stage == toa_Alate || current_stage == toa_Female || current_stage == toa_WingedMale || current_stage == toa_WingedFemale){
		return -1; // let them die
	}
	
	if(current_stage == toa_Nymph){
		if (m_current_developtype == toAphidSpringDev || m_current_developtype == toAphidLateSpringDev){
			return toa_Alate;
		}

		if (m_current_developtype == toAphidSummerDev){
			return toa_Aptera;
		}

		if (m_current_developtype == toAphidFallDev){
			return toa_WingedFemale;
		}
	}
}

void Aphid_Population_Manager :: initialiseSimWithEggs(void){
	int index_cell_i = 0;
	int index_cell_j = 0;
	struct_Aphid* sp;
	sp = new struct_Aphid;
	sp->NPM = this;
	sp->L = m_TheLandscape;
	//creat the aphid in each cell
	double different_count[4] = {0, 0, 0, 0};
	for (int i=0; i<SimW; i=i+m_sub_w)
	{
		index_cell_j = 0;
		for(int j=0; j<SimH; j=j+m_sub_h)
		{
			/*
			vector<int> poly_id_vec;
			bool not_found = true;
			int temp_poly_id;
			for(int check_x=i+m_sub_w/4; check_x<i+m_sub_w; check_x+=m_sub_w/2){
				for(int check_y=j+m_sub_h/4; check_y<j+m_sub_h; check_y+=m_sub_h/2){
					temp_poly_id = m_TheLandscape->SupplyPolyRef(check_x, check_y);
					if (std::find(poly_id_vec.begin(), poly_id_vec.end(), temp_poly_id) == poly_id_vec.end())
					poly_id_vec.push_back(temp_poly_id);
					//cout<<check_x<<";"<<check_y<<";"<<endl;
				}
			}
			//cout<<poly_id_vec[0]<<";"<<poly_id_vec[1]<<";"<<poly_id_vec[2]<<";"<<poly_id_vec[3]<<";"<<endl;
			different_count[poly_id_vec.size()-1] += 1;
			poly_id_vec.clear();
			*/
			sp->x = i+m_sub_w/2.0; //use the center of the cell as the location
			sp->y = j+m_sub_h/2.0;
			sp->w = m_sub_w;
			sp->h = m_sub_h;
			sp->NPM = this;
			sp->empty_flag = true;
			sp->index_x = index_cell_i;
			sp->index_y = index_cell_j;
			TTypesOfLandscapeElement current_landtype = m_TheLandscape->SupplyElementType(sp->x, sp->y);
			sp->starting_popu_density = &(m_cell_popu_density(index_cell_j, index_cell_i));
			sp->starting_suitability = &(m_cell_suitability(index_cell_j, index_cell_i));
			sp->winter_landscape_host = tole_Foobar;
			sp->summer_landscape_host = tole_Foobar;
			//set the farm flag
			if(current_landtype == tole_Field){
				sp->farm_flag = true;
				sp->empty_flag = false;
			}
			else sp->farm_flag = false;
			
			double current_biomass = m_TheLandscape->SupplyGreenBiomass(sp->x, sp->y);

			//check whether there are muli-seaon host, if yes, use winter host to initialise, othervise using the summer host list. 
			
			bool suitable_flag = false;
			if(m_mul_hosts_flag){
				suitable_flag = isWinterHostTole(current_landtype);
				if (suitable_flag){
					sp->winter_landscape_host = current_landtype;
				}
				
				if(isSummerHostTole(current_landtype)){
					sp->summer_landscape_host = current_landtype;
				}

				//if it is farm, check the vegetation
				if(!suitable_flag){
					suitable_flag = isWinterHostTov(m_TheLandscape->SupplyVegType(sp->x, sp->y));
				}
			}
			else{
				suitable_flag = isSummerHostTole(current_landtype);
				if(suitable_flag){
					sp->summer_landscape_host = current_landtype;
					sp->empty_flag = false;
				}

				//if it is farm, check the vegetation
				if(!suitable_flag && sp->farm_flag){
					if(isSummerHostTov(m_TheLandscape->SupplyVegType(sp->x, sp->y))){
						if (m_TheLandscape->SupplyGreenBiomass(sp->x, sp->y)>0){
							suitable_flag = true;
						}
					}
				}
			}

			if(suitable_flag){
				//sp->starting_suitability = 1;
				m_the_subpopulation_array(index_cell_j, index_cell_i) = CreateObjects(NULL, sp, cfg_AphidEggStartingNum.value());
				m_accumu_degree_days(0,0) = cfg_AphidEggStartingADD.value();
				m_cell_suitability(index_cell_j, index_cell_i) = 1;
			}
			//else if(!sp->empty_flag){
			//	m_the_subpopulation_array(index_cell_j, index_cell_i) = CreateObjects(NULL, sp, 0);
			//	m_cell_suitability(index_cell_j, index_cell_i) = 0;
			//}
			else{
				//m_the_subpopulation_array(index_cell_j, index_cell_i) = NULL;
				m_the_subpopulation_array(index_cell_j, index_cell_i) = CreateObjects(NULL, sp, 0);
				m_cell_suitability(index_cell_j, index_cell_i) = 0;
			}

			//add the pointer to its belonging polygon
			int temp_poly_id = m_TheLandscape->SupplyPolyRefIndex(sp->x, sp->y);
			m_vec_subpopulation_pointers_polygon.at(temp_poly_id).push_back(m_the_subpopulation_array(index_cell_j, index_cell_i));		
			index_cell_j++;
		}
		index_cell_i++;
	}
	/*
	double temp_sum = different_count[0]+different_count[1]+different_count[2]+different_count[3];
	cout<<"Different: "<<different_count[0]/temp_sum<<";"<<different_count[1]/temp_sum<<";"<<different_count[2]/temp_sum<<";"<<different_count[3]/temp_sum<<";"<<temp_sum<<endl;
	exit(0);
	*/
	m_current_developtype = toAphidHibernate;
	delete sp;
}

void Aphid_Population_Manager::updateMortalityArrayPeaAphid(void){

	m_current_mortality_array = 0.0;
	//egg
	m_current_mortality_array(toa_Egg, blitz::Range::all()) = cfg_AphidEggMortalityRate.value()+(g_rand_uni()-0.5)*2*cfg_AphidEggMortalityRateStd.value();
	if(m_TheLandscape->SupplyMonth()>=10){
		m_current_mortality_array(toa_Egg, blitz::Range::all()) *= cfg_AphidEggMortalityIncre.value();
	}

	/*
	//nymph
	//get the current temperature
	double current_temperature = g_landscape_p->SupplyTemp();
	//among 5 and 26 degrees, 0 mortality rate for nyphms
	if (current_temperature>=5 && current_temperature<=25){
		m_current_mortality_array(blitz::Range(toa_Nymph, toa_Nymph_M), blitz::Range::all()) = 0.0;
	}
	else if (current_temperature<5){
		m_current_mortality_array(blitz::Range(toa_Nymph, toa_Nymph_M), blitz::Range::all()) = 0.0;
		

		//we need to rethink about this
		int current_month = m_TheLandscape->SupplyMonth();
		if(current_month >= 10 || current_month <= 2){
			m_current_mortality_array(blitz::Range(toa_Nymph, toa_Nymph_M), blitz::Range::all()) = 0.9;
		}
	}
	else if (current_temperature<-9){
		m_current_mortality_array(blitz::Range(toa_Nymph, toa_Nymph_M), blitz::Range::all()) = 1;
	}
	else{
		double temp_mortality_temp = 0.1*current_temperature-2.5;
		double temp_mortality = 1- pow((1-temp_mortality_temp), (91+4/(current_temperature-cfg_PeaAphidNymphBaseDeveTempe.value())));
		m_current_mortality_array(blitz::Range(toa_Nymph, toa_Nymph_M), blitz::Range::all()) = temp_mortality;
	}
	*/
	//nymph
	double current_temperature = g_landscape_p->SupplyTemp();
	m_current_mortality_array(blitz::Range(toa_Nymph, toa_Nymph_M), blitz::Range::all()) = cfg_AphidNymphMortalityParaA.value()*current_temperature*current_temperature + cfg_AphidNymphMortalityParaB.value()*current_temperature+cfg_AphidNymphMortalityParaC.value();


	//adults
	if (current_temperature < -9 || current_temperature > 34.5){
		m_current_mortality_array(blitz::Range(toa_Aptera,toa_Male), blitz::Range::all()) = 1.0;
	}
	else{
		m_current_mortality_array(blitz::Range(toa_Aptera,toa_Male), blitz::Range::all()) = 1.0 - (m_accumu_degree_days(blitz::Range(toa_Aptera,toa_Male), blitz::Range::all())*cfg_AphidLongevityParaA.value()+cfg_AphidLongevityParaB.value());

		//we need to rethink about this
		int current_month = m_TheLandscape->SupplyMonth();
		if(current_month >= 10 || current_month <= 2){
			m_current_mortality_array(blitz::Range(toa_Aptera,toa_Male), blitz::Range::all()) *= 2;
		}
	}

	//update the nymph production weight, it is updated in this funtion since it is kind of mortality rate
	m_nymph_produced_num_weight = cfg_AphidNymphReproTempWeightParaA.value()*current_temperature*current_temperature+cfg_AphidNymphReproTempWeightParaB.value()*current_temperature+cfg_AphidNymphReproTempWeightParaC.value();

	cout<<current_temperature << "   "<<m_nymph_produced_num_weight<<endl;
	if(m_nymph_produced_num_weight<0) m_nymph_produced_num_weight=0;
	if(m_nymph_produced_num_weight>1) m_nymph_produced_num_weight=1;

	//update the egg reproduction flag
	if(m_first_egg_hatch_flag){
		if(current_temperature>0){
			m_accu_ddeg_egg_reproduction += current_temperature;
		}
		if(m_accu_ddeg_egg_reproduction >= cfg_PeaAPhidSexualDeveRequiredDDeg.value()+2*(g_rand_uni()-0.5)*cfg_PeaAPhidSexualDeveRequiredDDegStd.value()){
			m_egg_reproduction_flag = true;
			m_first_egg_hatch_flag = false;
			m_accu_ddeg_egg_reproduction = 0;
		}
	}
	cout<<"EGG: "<<m_accu_ddeg_egg_reproduction<<endl;
}

void Aphid_Population_Manager::updateMortalityArrayShared(void){

	m_current_mortality_array = 0.0;
	//egg
	m_current_mortality_array(toa_Egg, blitz::Range::all()) = cfg_AphidEggMortalityRate.value()+(g_rand_uni()-0.5)*2*cfg_AphidEggMortalityRateStd.value();
	if(m_TheLandscape->SupplyMonth()>=10){
		m_current_mortality_array(toa_Egg, blitz::Range::all()) *= cfg_AphidEggMortalityIncre.value();
	}

	//nymph
	//get the current temperature
	double current_temperature = g_landscape_p->SupplyTemp();
	m_current_mortality_array(blitz::Range(toa_Nymph, toa_Nymph_M), blitz::Range::all()) = cfg_AphidNymphMortalityParaA.value()*current_temperature*current_temperature + cfg_AphidNymphMortalityParaB.value()*current_temperature+cfg_AphidNymphMortalityParaC.value();

	//adults
	if (current_temperature < -9 || current_temperature > 34.5){
		m_current_mortality_array(blitz::Range(toa_Aptera,toa_Male), blitz::Range::all()) = 1.0;
	}
	else{
		m_current_mortality_array(blitz::Range(toa_Aptera,toa_Male), blitz::Range::all()) = 1.0 - (m_accumu_degree_days(blitz::Range(toa_Aptera,toa_Male), blitz::Range::all())*cfg_AphidLongevityParaA.value()+cfg_AphidLongevityParaB.value());

		//we need to rethink about this
		int current_month = m_TheLandscape->SupplyMonth();
		if(current_month >= 10 || current_month <= 2){
			m_current_mortality_array(blitz::Range(toa_Aptera,toa_Male), blitz::Range::all()) *= 2;
		}
	}

	//update the nymph production weight, it is updated in this funtion since it is kind of mortality rate
	m_nymph_produced_num_weight = cfg_AphidNymphReproTempWeightParaA.value()*current_temperature*current_temperature+cfg_AphidNymphReproTempWeightParaB.value()*current_temperature+cfg_AphidNymphReproTempWeightParaC.value();

	cout<<current_temperature << "   "<<m_nymph_produced_num_weight<<endl;
	if(m_nymph_produced_num_weight<0) m_nymph_produced_num_weight=0;
	if(m_nymph_produced_num_weight>1) m_nymph_produced_num_weight=1;

	//update the egg reproduction flag
	if(m_first_egg_hatch_flag){
		if(current_temperature>0){
			m_accu_ddeg_egg_reproduction += current_temperature;
		}
		if(m_accu_ddeg_egg_reproduction >= cfg_PeaAPhidSexualDeveRequiredDDeg.value()+2*(g_rand_uni()-0.5)*cfg_PeaAPhidSexualDeveRequiredDDegStd.value()){
			m_egg_reproduction_flag = true;
			m_first_egg_hatch_flag = false;
			m_accu_ddeg_egg_reproduction = 0;
		}
	}
	cout<<"EGG: "<<m_accu_ddeg_egg_reproduction<<endl;
}

int Aphid_Population_Manager::calOffspringStage(int current_stage, double *offspring_num, double a_age, double a_density, double a_growth_stage, double* a_propotion, bool winter_host_flag){
	return (this->*m_offspring_func_pointer)(current_stage, offspring_num, a_age, a_density, a_growth_stage, a_propotion, winter_host_flag);
}

/**
 * @brief This function is used both for Pea and English Grain Aphid
 * 
 * @param current_stage 
 * @param offspring_num 
 * @param a_age 
 * @param a_density 
 * @param a_growth_stage 
 * @param a_propotion 
 * @return int 
 */
int Aphid_Population_Manager::calOffspringStagePea(int current_stage, double *offspring_num, double a_age, double a_density, double a_growth_stage, double* a_propotion, bool winter_host_flag){
	if(current_stage == toa_Female){
		if (m_egg_reproduction_flag){
			*offspring_num = m_egg_per_day.at(int(ceil(a_age)));
		}
		else{
			*offspring_num = 0;
		}
		
		return toa_Egg;
	}
	if(current_stage == toa_Aptera || current_stage == toa_Alate){
		//nothing to produce
		if(a_density<0){
			*offspring_num = 0;
			return toa_Nymph;
		}
		double alate_prop = (cfg_AphidAlatePropParaA.value()*a_density+cfg_AphidAlatePropParaC.value()*a_growth_stage+cfg_AphidAlatePropParaB.value())/100;
		if (alate_prop<0.001) alate_prop = 0;
		if (alate_prop>=1) alate_prop = 1;
		*offspring_num = m_nymph_per_day.at(int(ceil(a_age)))*m_nymph_produced_num_weight; // multiply with the temperature realted weight.
		//if (alate_prop>0.5) *offspring_num *= (1-alate_prop+0.1);
		//cout<<*offspring_num<<"How many!!!!"<<endl;
		double current_day_length = g_landscape_p->SupplyDaylength();
		if(m_egg_reproduction_flag && current_day_length <cfg_PeaAphidDayLengthMale.value() && current_day_length >cfg_PeaAphidDayLengthFemale.value()){
			return toa_Nymph_M;
		}
		else if(m_egg_reproduction_flag && current_day_length <= cfg_PeaAphidDayLengthFemale.value()){
			return toa_Nymph_F;
		}
		else{
			if(a_propotion){				
				*a_propotion = alate_prop;
				return toa_Nymph_W;
			}
			else{
				return toa_Nymph;
			}
		}
	}
}

/**
 * @brief This function is used both for Balck bean and peach potato Aphid
 * 
 * @param current_stage 
 * @param offspring_num 
 * @param a_age 
 * @param a_density 
 * @param a_growth_stage 
 * @param a_propotion 
 * @return int 
 */
int Aphid_Population_Manager::calOffspringStageAphis(int current_stage, double *offspring_num, double a_age, double a_density, double a_growth_stage, double* a_propotion, bool winter_host_flag){
	if(current_stage == toa_Female){
		if (m_egg_reproduction_flag){
			*offspring_num = m_egg_per_day.at(int(ceil(a_age)));
		}
		else{
			*offspring_num = 0;
		}
		
		return toa_Egg;
	}
	if(current_stage == toa_Aptera || current_stage == toa_Alate){
		//nothing to produce
		if(a_density<0){
			*offspring_num = 0;
			return toa_Nymph;
		}
		double alate_prop = (cfg_AphidAlatePropParaA.value()*a_density+cfg_AphidAlatePropParaC.value()*a_growth_stage+cfg_AphidAlatePropParaB.value())/100;
		if (alate_prop<0.001) alate_prop = 0;
		if (alate_prop>=1) alate_prop = 1;
		*offspring_num = m_nymph_per_day.at(int(ceil(a_age)))*m_nymph_produced_num_weight; // multiply with the temperature realted weight.
		
		//first check if it is on winter host and it is in the last half year, if yes, produce females
		if(m_winter_host_on && winter_host_flag && m_TheLandscape->SupplyMonth()>=8){
			return toa_Nymph_F;
		}
		else{
			if(a_propotion){				
				*a_propotion = alate_prop;
				return toa_Nymph_W;
			}
			else{
				return toa_Nymph;
			}
		}
	}
}

bool Aphid_Population_Manager::isEnoughNextLifeStage(int a_life_stage){
	return (this->*m_is_ready_next_liff_func_pointer)(a_life_stage);
}

bool Aphid_Population_Manager::isEnoughNextLifeStagePeaAphid(int a_life_stage){
	if(a_life_stage == toa_Egg){
		double temp_hatch_chance = m_egg_hatach_chances.at(int(floor(m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1))))); 
		if(g_rand_uni()<=temp_hatch_chance){
			return true;
		}

		else return false;
	}

	if(a_life_stage == toa_Nymph || a_life_stage == toa_Nymph_F || a_life_stage == toa_Nymph_M){
		if (m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1)) >= cfg_AphidNymphDeveDayDegUnwing.value() + (g_rand_uni()-0.5)*cfg_AphidNymphDeveDayDegUnwingStd.value()){
			return true;
		}
		else return false;
	}

	if(a_life_stage == toa_Nymph_W ){
		if (m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1)) >= cfg_AphidNymphDeveDayDegWing.value() + (g_rand_uni()-0.5)*cfg_AphidNymphDeveDayDegWingStd.value()){
			return true;
		}
		else return false;
	}

	//the adults are contolled by the mortality
	return (m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1))>=m_max_adult_dd);
}


bool Aphid_Population_Manager::isEnoughNextLifeStageEnglishGrainAphid(int a_life_stage){
	if(a_life_stage == toa_Egg){
		double temp_hatch_chance = m_egg_hatach_chances.at(int(floor(m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1))))); 
		if(g_rand_uni()<=temp_hatch_chance){
			return true;
		}

		else return false;
	}

	if(a_life_stage == toa_Nymph || a_life_stage == toa_Nymph_F || a_life_stage == toa_Nymph_M){
		if (m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1)) >= cfg_AphidNymphDeveDayDegUnwing.value() + (g_rand_uni()-0.5)*cfg_AphidNymphDeveDayDegUnwingStd.value()){
			return true;
		}
		else return false;
	}

	if(a_life_stage == toa_Nymph_W ){
		if (m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1)) >= cfg_AphidNymphDeveDayDegWing.value() + (g_rand_uni()-0.5)*cfg_AphidNymphDeveDayDegWingStd.value()){
			return true;
		}
		else return false;
	}

	//the adults are contolled by the mortality
	return (m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1))>=m_max_adult_dd);
}

void Aphid_Population_Manager::doParasitoidDevelopment(){

	//reset when it is the first day in a year
	if(g_date->JanFirst()){
		m_parasitoid_development_flag = false;
		m_parasitoid_egg_kill_flag = false;
		m_accu_ddeg_parasitoid = 0;
		m_parasitoid_egg_accu_ddeg = -1;
		for (int i = 0; i < m_num_y_range; i++){				
			for (int j = 0; j < m_num_x_range; j++){
				if(m_the_subpopulation_array(i,j)!=NULL){
					if(getTotalSubpopulationInCell(i,j)>0){
						m_the_subpopulation_array(i,j)->setParasitoidEggNum(0);
						m_the_subpopulation_array(i,j)->setParasitoidNum(0);
					}
				}
			}
		}
	}



	//do the egg development
	if (m_parasitoid_development_flag && m_parasitoid_egg_accu_ddeg >=0){
		
		//give birth of eggs
		if(m_parasitoid_egg_accu_ddeg == 0){
			for (int i = 0; i < m_num_y_range; i++){				
				for (int j = 0; j < m_num_x_range; j++){
					if(m_the_subpopulation_array(i,j)!=NULL){
						if(getTotalSubpopulationInCell(i,j)>0){
							m_the_subpopulation_array(i,j)->giveBirthParasitoidEgg(m_parasitoid_egg_lifetime);
						}
					}
				}
			}
		}

		//accumulate day degrees
		double current_temp = g_landscape_p->SupplyTemp();
		if (current_temp > cfg_AphidParasitoidBaseDevTemp.value()){
			m_parasitoid_egg_accu_ddeg += (current_temp-cfg_AphidParasitoidBaseDevTemp.value());
		}

		//kill aphids
		if (m_parasitoid_egg_accu_ddeg >= cfg_AphidParasitoidKillDdeg.value() && !m_parasitoid_egg_kill_flag){
			for (int i = 0; i < m_num_y_range; i++){				
				for (int j = 0; j < m_num_x_range; j++){
					if(m_the_subpopulation_array(i,j)!=NULL){
						m_the_subpopulation_array(i,j)->killByParasitoid();
					}
				}
			}
			m_parasitoid_egg_kill_flag = true;
			cout<<"KILL"<<endl;
		}

		//hatch
		if (m_parasitoid_egg_accu_ddeg >= cfg_AphidParasitoidHatchDdeg.value()){
			for (int i = 0; i < m_num_y_range; i++){				
				for (int j = 0; j < m_num_x_range; j++){
					if(m_the_subpopulation_array(i,j)!=NULL){
						m_the_subpopulation_array(i,j)->hatchParasitoidEggs();
					}
				}
			}
			m_parasitoid_egg_accu_ddeg = 0;
			m_parasitoid_egg_kill_flag = false;
			cout<<"HATCH"<<endl;
		}
	}	

	//Initialisation of parasitoid
	if(!m_parasitoid_development_flag){
		double current_temp = g_landscape_p->SupplyTemp();
		if (current_temp > cfg_AphidParasitoidBaseDevTemp.value()){
			m_accu_ddeg_parasitoid += (current_temp-cfg_AphidParasitoidBaseDevTemp.value());
			if (m_accu_ddeg_parasitoid > cfg_AphidParasitoidStartDdeg.value()){
				m_parasitoid_development_flag = true;
				m_parasitoid_egg_accu_ddeg = 0; //they can lay eggs now
				for (int i = 0; i < m_num_y_range; i++){				
					for (int j = 0; j < m_num_x_range; j++){
						if(getTotalSubpopulationInCell(i,j)>0){
							if(m_the_subpopulation_array(i,j)!=NULL){
								m_the_subpopulation_array(i,j)->setParasitoidNum(cfg_AphidParasitoidStartNum.value());
							}
						}
					}
				}
			}
		}
	}
	
	cout<<"Parasitoid :"<<m_parasitoid_egg_accu_ddeg<<"Kill"<<cfg_AphidParasitoidKillDdeg.value()<<"Hatch"<<cfg_AphidParasitoidHatchDdeg.value()<<endl;
}

void Aphid_Population_Manager :: doSpecicesLastThing(){
	
	for (int i = toa_Aptera; i<= toa_Male; i++){
		while(m_accumu_degree_days(i, m_index_new_old(i,1)) >= m_max_day_degree_adult){
			m_accumu_degree_days(i, m_index_new_old(i, 1)) = 0.0;
			m_index_new_old(i, 1) += 1;
			//when it reaches the end, set it to zero
			if (m_index_new_old(i,1) >= m_max_alive_days){
				m_index_new_old(i,1) = 0;
			}
		}
	}
	
}

 #ifdef __APHID_CALIBRATION
      bool Aphid_Population_Manager :: openAphidCalibFiles(){
		m_aphid_calib_crop_output = fopen("./aphid_crop_output.txt", "w");
    	m_aphid_calib_tole_output = fopen("./aphid_tole_output.txt", "w");
      	m_aphid_calib_land_output = fopen("./aphid_landscape_output.txt", "w");
		if (!m_aphid_calib_crop_output || !m_aphid_calib_tole_output || !m_aphid_calib_land_output){
			g_msg->Warn( WARN_FILE, "Aphid_Population_Manager::openAphidCalibFiles(): ", "Unable to open aphid calibration output file(s)!");
    		exit( 1 );
  		}
		//write the headers, habitat
		if (strcmp(cfg_AphidSpeciesName.value(), "Pea aphid")==0){
			char* headers_tole[17] = {"Year", "Month", "Day", "Poly ID", "Veg type", "Egg", "Nymph", "NymphW", "NymphF", "NymphM", "Aptera", "Alate", "Female", "Male", "Total green mass", "Green mass", "Growth stage"};
			for(int i=0; i<16; i++){
				fprintf(m_aphid_calib_tole_output,"%s\t",headers_tole[i]);
			}
			fprintf(m_aphid_calib_tole_output,"%s\n",headers_tole[16]);
			fflush(m_aphid_calib_tole_output);
			char* headers_crop[17] = {"Year", "Month", "Day",  "Poly ID", "Veg type", "Egg", "Nymph", "NymphW", "NymphF", "NymphM", "Aptera", "Alate", "Female", "Male", "Total green mass", "Green mass", "Growth stage"};
			for(int i=0; i<16; i++){
				fprintf(m_aphid_calib_crop_output,"%s\t",headers_crop[i]);
			}
			fprintf(m_aphid_calib_crop_output,"%s\n",headers_crop[16]);
			fflush(m_aphid_calib_crop_output);
		}

		return true;
	  }

      void Aphid_Population_Manager :: closeAphidCalibFiles(){
		fclose(m_aphid_calib_crop_output);
		fclose(m_aphid_calib_tole_output);
		fclose(m_aphid_calib_land_output);
	  }

      void Aphid_Population_Manager :: writeAphidCalibData(){
		//habitat
		writeRowAphidCalibData(m_aphid_calib_tole_output, cfg_AphidCalibrationPolygonToStoreTOLE);
		//crop
		writeRowAphidCalibData(m_aphid_calib_crop_output, cfg_AphidCalibrationPolygonToStoreCROP);	
	  }

	  void Aphid_Population_Manager :: writeRowAphidCalibData(FILE* target_file, CfgArray_Int poly_id_list){
		int temp_year = m_TheLandscape->SupplyYear();
		fprintf(target_file, "%d\t", temp_year);
		int temp_month = m_TheLandscape->SupplyMonth();
		fprintf(target_file, "%d\t", temp_month);
		int temp_day = m_TheLandscape->SupplyDayInMonth();
		fprintf(target_file, "%d\t", temp_day);
		for (int i = 0; i<poly_id_list.get_array_size(); i++){
			int temp_poly_id = poly_id_list.value(i);
			fprintf(target_file, "%d\t", temp_poly_id);
			int temp_veg_type = m_TheLandscape->SupplyVegType(temp_poly_id);
			fprintf(target_file, "%s\t", m_TheLandscape->VegtypeToString((TTypesOfVegetation)temp_veg_type).c_str());
			double temp_size = m_TheLandscape->SupplyPolygonArea(temp_poly_id);
			for (int j = 0; j<m_num_life_stage; j++){
				double temp_popu_num = 0;
				for (int k=0; k<m_vec_subpopulation_pointers_polygon.at(temp_poly_id).size(); k++){
					if (m_vec_subpopulation_pointers_polygon.at(temp_poly_id).at(k) != NULL){
						temp_popu_num += m_vec_subpopulation_pointers_polygon.at(temp_poly_id).at(k)->getNumforOneLifeStage(j);
					}
				}
				fprintf(target_file, "%f\t", temp_popu_num/temp_size);
			}
			double temp_biomass = m_TheLandscape->SupplyVegBiomass(temp_poly_id);
			fprintf(target_file, "%f\t", temp_biomass);
			temp_biomass = m_TheLandscape->SupplyGreenBiomass(temp_poly_id);
			fprintf(target_file, "%f\t", temp_biomass);
			double temp_growth_stage = m_TheLandscape->SupplyVegGrowthStage(temp_poly_id);
			fprintf(target_file, "%f\n", temp_growth_stage);
		}
		
	  }
#endif

void Aphid_Population_Manager::writeCalibrationFiles(void){
	#ifdef __APHID_CALIBRATION
		writeAphidCalibData();
	#endif
}

Aphid_Population_Manager::~Aphid_Population_Manager(){
	#ifdef __APHID_CALIBRATION
		closeAphidCalibFiles();
	#endif
}