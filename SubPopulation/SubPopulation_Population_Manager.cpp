/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file SubPopulationPopulationManager.cpp
\brief <B>The main source code for subpopulation population manager class</B>
*/
/**  \file SubPopulationPopulationManager.cpp
Version of  Feb. 2021 \n
By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"

using namespace std;
using namespace blitz;

CfgBool cfg_Subpopu_Base_Output_Used("SUBPOPU_BASE_OUTPUT_USED", CFG_CUSTOM, true);
CfgStr cfg_Subpopu_Base_Output_Filename( "SUBPOPU_BASE_OUTPUT_FILENAME", CFG_CUSTOM, "SubpopuBaseOutput.txt" );
CfgFloat cfg_Subpopu_Density_Threshold("SUBPOPU_DENSITY_THRESHOLD", CFG_CUSTOM, 1000);
extern Landscape* g_landscape_p;
extern TTypesOfPopulation g_Species;
extern FarmManager* g_farmmanager;
//---------------------------------------------------------------------------

//#define SubpopulationSpatialResult

SubPopulation_Population_Manager::SubPopulation_Population_Manager(Landscape* L, string DevReproFile, int a_sub_w, int a_sub_h, int a_num_life_stage, int a_max_long_dist, int a_peak_long_dist, float a_scale_wind_speed, float a_max_wind_speed, int a_wind_direc_num, int a_wind_speed_step_size, int a_max_alive_day) : Population_Manager_Base(L)
{
	//set the number of life stages
	m_num_life_stage = a_num_life_stage;
	m_ListNameLength = m_num_life_stage;

	//set the number of longest life time in calendar days
	m_max_alive_days = a_max_alive_day;

	//set the width and height of the grid
	m_sub_w = a_sub_w;
	m_sub_h = a_sub_h;

	//create the array to store the base development temperature for each life stage
	m_lowest_temp_dev.resize(m_num_life_stage);
	//create the vector to store the indix for the old enough ones for next life stage or death
	m_index_next_life_stage.resize(m_num_life_stage);
	/*
	for (int i=0; i<m_num_life_stage; i++){
		m_index_next_life_stage.at(i).push_back(-1);
	}
	*/
	m_vec_subpopulation_pointers_polygon.resize(m_TheLandscape->SupplyNumberOfPolygons());
	
	//flying related parameters
	//create the movement masks
	m_current_flying_array.resize(m_max_alive_days);
	m_current_flying_array = 0.0;
	m_current_landing_array.resize(m_max_alive_days);
	m_current_landing_array = 0.0;
	m_max_long_distance = a_max_long_dist;
	m_peak_long_distance = a_peak_long_dist;
	m_scale_wind_speed = a_scale_wind_speed;
	m_max_flying_wind_speed = a_max_wind_speed;
	//the default wind direction number is 8
	m_wind_direction_num = a_wind_direc_num;
	m_wind_speed_step_size = a_wind_speed_step_size;
	//get the sampling nubmers for the wind speed with the given the step size
	m_wind_speed_num = ceil(m_max_flying_wind_speed / m_wind_speed_step_size);

	//resize the landing mask array
	m_landing_masks.resize(m_wind_speed_num, m_wind_direction_num);

	//initialise the lookup table for puting the given wind speed to a range for the sake of speed. Each range is like [) with a 1m/s resolution.
	m_wind_speed_lookup_table.resize(ceil(m_max_flying_wind_speed)+1);
	
	for(int i = 0; i<=ceil(m_max_flying_wind_speed); i++){
		m_wind_speed_lookup_table(i)= int(i/m_wind_speed_step_size);
		//cout<<m_wind_speed_lookup_table<<endl;
	}

	//initialise the wind direction array
	m_wind_direction_array.resize(m_wind_direction_num, 2);
	double temp_degree = 2*M_PI/m_wind_direction_num;
	for (int i=0; i<m_wind_direction_num; i++){
		m_wind_direction_array(i,0) = cos(temp_degree*i);
		m_wind_direction_array(i,1) = sin(temp_degree*i);
	}

	cout<<m_wind_direction_array<<endl;

	//convert the longest distance to the number of cells
	//we need to make it with the largest distance multiplied by the incremental factor
	m_long_move_mask_x_num_half_array.resize(m_wind_speed_num);
	m_long_move_mask_y_num_half_array.resize(m_wind_speed_num);
	for (int i=0; i<m_wind_speed_num;i++){
		m_long_move_mask_x_num_half_array(i) = m_max_long_distance*pow(a_scale_wind_speed, i)/m_sub_w;
		m_long_move_mask_y_num_half_array(i) = m_max_long_distance*pow(a_scale_wind_speed, i)/m_sub_h;
	}

	//initialise the landing mask arrays
	for (int i=0; i<m_wind_speed_num;i++)
		for (int j=0; j<m_wind_direction_num; j++){
			m_landing_masks(i, j).resize(2*m_long_move_mask_y_num_half_array(i)+1, 2*m_long_move_mask_x_num_half_array(i)+1);
			m_landing_masks(i, j) = 0.0;
			cout<<m_landing_masks(i,j).shape()<<endl;
	}


	double temp_longest_factored = m_max_long_distance*pow(a_scale_wind_speed, m_wind_speed_num-1);
	m_long_move_mask_x_num_half = temp_longest_factored/m_sub_w;
	m_long_move_mask_x_num = 2*m_long_move_mask_x_num_half +1;
	m_long_move_mask_y_num_half = temp_longest_factored/m_sub_h;
	m_long_move_mask_y_num = 2*m_long_move_mask_y_num_half+1;
	m_long_move_mask.resize(m_wind_speed_num, m_wind_direction_num, m_long_move_mask_y_num, m_long_move_mask_x_num);
	m_long_move_mask = 0.0;
	calLongMovementMask();

	//hard coded 
	m_short_move_mask_x_num = 3;
	m_short_move_mask_y_num = 3;
	m_short_move_mask.resize(m_short_move_mask_y_num, m_short_move_mask_x_num);
	m_short_move_mask = 0.0;
	if(DevReproFile==""){
		readDevReproFile("Subpopulation/subpopulation_default_info.txt");
	}
	else{
		readDevReproFile(DevReproFile);
	}
		
	// Initialize the default population size to zero
	m_total_num_each_stage.resize(m_num_life_stage);
	m_total_num_each_stage=0;

	//set the number of cells
	m_num_x_range = SimW/m_sub_w;
	m_num_y_range = SimH/m_sub_h;
	m_size_cell = m_sub_w*m_sub_h;

	//Initialize the weighted population density array with zero
	m_cell_popu_density.resize(m_num_y_range, m_num_x_range);
	m_cell_popu_density = 0.0;

	//Initialize the suitability for each cell with zero
	m_cell_suitability.resize(m_num_y_range, m_num_x_range);
	m_cell_suitability = 0.0;

	//Initialize the pointer array for the subpopulation objects
	m_the_subpopulation_array.resize(m_num_y_range, m_num_x_range);	
	m_the_subpopulation_array = NULL;

	m_first_flag_life_stage.resize(m_num_life_stage);
	m_first_flag_life_stage = true;

	//check if we need to open the base storing file
	if (cfg_Subpopu_Base_Output_Used.value()) {
        openSubpopulationBaseProbeFile();
    }

	m_hibernated_hatch_flag = false;

	//initialize the mortality related arrays
	m_current_mortality_array.resize(m_num_life_stage, m_max_alive_days);
	m_current_mortality_array = 0.0;
	m_lowest_temperature_die.resize(m_num_life_stage);
	m_lowest_temperature_die = 2;
	m_highest_temperature_die.resize(m_num_life_stage);
	m_highest_temperature_die = 40.0;
	m_optimal_temperature.resize(m_num_life_stage);
	m_optimal_temperature = 15;

	//create the arrays for calculation of degree days
	m_accumu_degree_days.resize(m_num_life_stage, m_max_alive_days);
	m_accumu_degree_days = 0.0;
	m_index_new_old.resize(m_num_life_stage, 2); //the first is the newest index and the second is the oldest index
	m_index_new_old = 0; //They start from zero from the beginning.

	//do the initialisation
	if(DevReproFile=="") initialisePopulation();
}

SubPopulation_Population_Manager::~SubPopulation_Population_Manager(){
	for (int i=0; i<m_num_y_range; i++){
		for (int j=0; j<m_num_x_range; j++){
			if (m_the_subpopulation_array(i,j) !=  NULL){
				delete m_the_subpopulation_array(i,j);
			}
		}
	}
	
	if(cfg_Subpopu_Base_Output_Used.value() && m_subpopulation_base_prb_file){
		m_subpopulation_base_prb_file.close();
		//delete m_subpopulation_base_prb_file;
	}
}

void SubPopulation_Population_Manager::updateWholePopulationArray(int a_listindex, double number)
{	
	double temp_number = m_total_num_each_stage(a_listindex);
	temp_number += number;
	if (temp_number < 0){
		m_total_num_each_stage(a_listindex)=0;
	}
	else
	{
		m_total_num_each_stage(a_listindex)= temp_number;
	}
}

void SubPopulation_Population_Manager::updateWholePopulation(){
	for (int k=0; k<m_num_life_stage; k++){
		m_total_num_each_stage(k) = 0;
		for (int i = 0; i < m_num_y_range; i++){
			for (int j = 0; j < m_num_x_range; j++){
				if(m_the_subpopulation_array(i,j)!=NULL){
					m_total_num_each_stage(k) += m_the_subpopulation_array(i,j)->getNumforOneLifeStage(k);
				}
			}
		}
	}
}

/**
This is the main scheduling method for the population manager using subpopulation model.
*/
void SubPopulation_Population_Manager::Run( int NoTSteps ) {
	
  for ( int TSteps = 0; TSteps < NoTSteps; TSteps++ )
  {	
	cout<<"Day "<<m_TheLandscape->SupplyDayInYear() << endl;
	#ifdef APHID_DEBUG
	m_num_killed_by_parasitoid=0;
	m_num_parasitoid=0;
	m_num_parasitoid_egg=0;
	m_num_new_parasitoid_egg_daily=0;
	#endif  
    DoFirst();
	for (int i = 0; i < m_num_y_range; i++){
		//parallel_for(m_num_x_range[&](int start, int end){
		for (int j = 0; j < m_num_x_range; j++){
			if(m_the_subpopulation_array(i,j)!=NULL){
				m_the_subpopulation_array(i,j)->BeginStep();
			}
		}
	}
    // call the step-method of all objects
	for (int i = 0; i < m_num_y_range; i++){
	//parallel_for(m_num_x_range[&](int start, int end){
		for (int j = 0; j < m_num_x_range; j++){
			if(m_the_subpopulation_array(i,j)!=NULL){
				SubPopulation* temp_pointer = m_the_subpopulation_array(i,j);
				m_the_subpopulation_array(i,j)->Step();
			}
			else {
				std::cout<<"NULL"<<endl;
			}
		}
	}
	DoLast();

	#ifdef __APHID_CALIBRATION
		writeCalibrationFiles();
	#endif

	#ifdef APHID_BIOMASS_DEBUG
	 ofstream bio_file;
	 bio_file.open("biomass.txt", ios::app );
	 
	 for (int i = 0; i<2200; i++){
		if (g_landscape_p->SupplyElementType(i)==tole_PermPastureTussocky){
			//bio_file<<g_landscape_p->SupplyVegPhase(i)<<"\t"<<g_landscape_p->SupplyVegBiomass(i)<<"\t"<<g_landscape_p->SupplyGreenBiomass(i)<<"\t";
			bio_file<<g_landscape_p->SupplyVegBiomass(i)<<"\t"<<g_landscape_p->SupplyGreenBiomass(i)<<"\n";
			break;
		}
	 }
	 
	
	 for (int i = 0; i<2600; i++){
		if (g_landscape_p->SupplyVegType(i)==tov_DKLegume_Beans){
			//bio_file<<g_landscape_p->SupplyVegPhase(i)<<"\t"<<g_landscape_p->SupplyVegBiomass(i)<<"\t"<<g_landscape_p->SupplyGreenBiomass(i)<<"\n";
			bio_file<<g_landscape_p->SupplyVegBiomass(i)<<"\t"<<g_landscape_p->SupplyGreenBiomass(i)<<"\n";
			break;
		}
	 }
	

	 bio_file.flush();
	 bio_file.close();
	#endif

  } // End of time step loop
}

double SubPopulation_Population_Manager::getTotalSubpopulationInCell(int x_indx, int y_indx){
	if(m_the_subpopulation_array(y_indx, x_indx)==NULL){
		return 0;
	}
	return m_the_subpopulation_array(y_indx, x_indx)->getTotalSubpopulation();
}

double SubPopulation_Population_Manager::getSubpopulationInCellLifeStage( int x_indx, int y_indx, int a_life_stage){
	if(m_the_subpopulation_array(y_indx, x_indx)==NULL){
		return 0;
	}
	return m_the_subpopulation_array(y_indx, x_indx)->getNumforOneLifeStage(a_life_stage);
}

SubPopulation* SubPopulation_Population_Manager::supplySubPopulationPointer(int indx, int indy){
	return m_the_subpopulation_array(indy, indx);
}

double SubPopulation_Population_Manager::supplyAllPopulationGivenStage(int index){
	if(index<m_num_life_stage){
		return m_total_num_each_stage(index);
	} 
	return 0;
}

void SubPopulation_Population_Manager::subpopuBaseOutputProbe(){
	for(int i=0; i<m_num_life_stage; i++){
	m_subpopulation_base_prb_file<<m_total_num_each_stage(i)<<"\t";
	}
	m_subpopulation_base_prb_file<<g_landscape_p->SupplyDaylength()<<"\t";
	
	#ifdef APHID_DEBUG
	m_subpopulation_base_prb_file<<g_landscape_p->SupplyTemp()<<"\t"<<m_num_killed_by_parasitoid<<"\t"<<
	m_num_parasitoid<<"\t"<<m_num_parasitoid_egg<<"\t"<<m_num_new_parasitoid_egg_daily;;
	#else
	m_subpopulation_base_prb_file<<g_landscape_p->SupplyTemp();
	#endif 

	#ifndef SubpopulationSpatialResult
	m_subpopulation_base_prb_file<<"\n";
	#endif

	#ifdef SubpopulationSpatialResult
	//the population in each grid
	for (int index_y = 0; index_y < m_num_y_range; index_y++){
		for (int index_x = 0; index_x < m_num_x_range; index_x++){
			m_subpopulation_base_prb_file<<"\t";
			//for(int i=0; i<m_num_life_stage; i++){
				//store the population at each life stage
			//	m_subpopulation_base_prb_file<<m_the_subpopulation_array(index_y, index_x)->getNumforOneLifeStage(i)<<"\t";
			//}
			double temp_counter = 0;
			for (int i=5;i<9;i++){
				if(m_the_subpopulation_array(index_y, index_x) != NULL){
					temp_counter += m_the_subpopulation_array(index_y, index_x)->getNumforOneLifeStage(i);
				}
			}
			m_subpopulation_base_prb_file<<temp_counter;

			// parasitoid number
			//m_subpopulation_base_prb_file<<m_the_subpopulation_array(index_y, index_x)->getPrasitoidNum()<<"\t";
			//m_subpopulation_base_prb_file<<m_the_subpopulation_array(index_y, index_x)->getPrasitoidEggNum();
		}
	}
	m_subpopulation_base_prb_file<<"\n";
	#endif

	m_subpopulation_base_prb_file.flush();
}

bool SubPopulation_Population_Manager::openSubpopulationBaseProbeFile() {
  m_subpopulation_base_prb_file.open(cfg_Subpopu_Base_Output_Filename.value(), ios::out );
  if ( !m_subpopulation_base_prb_file) {
    g_msg->Warn( WARN_FILE, "SubPopulationPopulation_Manager::openSubpopulationBaseProbeFile(): ""Unable to open probe file",cfg_Subpopu_Base_Output_Filename.value() );
    exit( 1 );
  }

  #ifdef SubpopulationSpatialResult
  m_subpopulation_base_prb_file<<m_num_life_stage<<"\n";
  m_subpopulation_base_prb_file<<m_num_x_range<<"\t"<<m_num_y_range<<"\n";
  m_subpopulation_base_prb_file.flush();
  #endif

  return true;
}

unsigned SubPopulation_Population_Manager::GetLiveArraySize(int a_listindex) {
	return unsigned(supplyAllPopulationGivenStage(a_listindex));
}

void SubPopulation_Population_Manager::readDevReproFile(string inputfile){
	ifstream ist(inputfile, ios::in);
	int row_num;
    ist >> row_num;
    ist >> m_num_life_stage;
    m_development_degree_day.resize(row_num, m_num_life_stage);

	//load the name for life stages
	for(int j=0; j<m_num_life_stage;j++){
		string temp_name;
		ist >> temp_name;
	}

	//m_max_alive_days = -1;

	//load the development and reproducing degree days
    for (int i=0; i<row_num-1; i++){
        for(int j=0; j<m_num_life_stage;j++){
            int temp_int;
            ist >> temp_int;
            m_development_degree_day(i,j)=temp_int;
            //cout<<m_activities_priorities(i,j);
			//if(i==0){
			//	if (m_max_alive_days < temp_int){
			//		m_max_alive_days = temp_int;
			//	}
			//}
        }
    }

	//load the lowest development temperature
	for (int j=0; j<m_num_life_stage; j++){
		double temp_dou;
		ist >> temp_dou;
		m_lowest_temp_dev(j) = temp_dou;
	}
}

void SubPopulation_Population_Manager::relocatePopulation(){

	//local movement
	/*
	for(int ind_y=0; ind_y < m_num_y_range; ind_y++){
		for(int ind_x=0; ind_x <m_num_x_range; ind_x++){
			if(m_the_subpopulation_array(ind_y, ind_x) != NULL){
				doLocalMovement(ind_x, ind_y, 0.5);				
			}
		}
	}*/

		//let winged aphihds to fly
	for(int ind_y=0; ind_y < m_num_y_range; ind_y++){
		for(int ind_x=0; ind_x <m_num_x_range; ind_x++){
			if(m_the_subpopulation_array(ind_y, ind_x) != NULL){
				for(int i=0; i<m_flying_life_stage_array.size(); i++){
					//if(m_the_subpopulation_array(ind_y, ind_x)->getFlyingNum(i)>0){
					doFlying(i, ind_x, ind_y);
					//}
				}
			}
		}
	}	
}

void SubPopulation_Population_Manager::DoLast(){	
	doDevelopment();
	doSpecicesLastThing();
	addNewDay();
	updateWholePopulation();
	//store results if it is needed
	if(m_subpopulation_base_prb_file){
		subpopuBaseOutputProbe();
	}	
}
void SubPopulation_Population_Manager::calLongMovementMask(){

	//fstream tempoutputfilet("result.txt", ios::app); //debug code
	for (int i=0; i<m_wind_speed_num; i++){
		for ( int x=-m_long_move_mask_x_num_half_array(i); x<m_long_move_mask_x_num_half_array(i)+1; x++){
			for (int y=m_long_move_mask_y_num_half_array(i); y>-m_long_move_mask_y_num_half_array(i)-1; y--){
				//we calculate the base mask without the consideration of the wind direction first
				double temp_mask_value = calLandingCurve(sqrt(pow(x*m_sub_w,2)+pow(y*m_sub_h,2)), m_peak_long_distance*pow(m_scale_wind_speed, i), m_max_long_distance*pow(m_scale_wind_speed, i));
				//apply the wind direction
				for(int j=0; j<m_wind_direction_num; j++){
					//calculate the cosine value between the direction of the cell and the wind direction
					double temp_cos = -1;
					if(x!=0 || y!=0)
					temp_cos = (y*m_wind_direction_array(j,1)+x*m_wind_direction_array(j,0))/(sqrt(x*x+y*y)*sqrt(m_wind_direction_array(j,0)*m_wind_direction_array(j,0)+m_wind_direction_array(j,1)*m_wind_direction_array(j,1)));
					if (temp_cos>0.000001){
						m_long_move_mask(i, j, m_long_move_mask_y_num_half+y, m_long_move_mask_x_num_half+x) = temp_mask_value* temp_cos;
						m_landing_masks(i, j)(m_long_move_mask_y_num_half_array(i)-y, m_long_move_mask_x_num_half_array(i)+x) = temp_mask_value * temp_cos;
					}
					else{
						m_long_move_mask(i, j, m_long_move_mask_y_num_half+y, m_long_move_mask_x_num_half+x) = 0.0;
						m_landing_masks(i, j)(m_long_move_mask_y_num_half_array(i)-y,  m_long_move_mask_x_num_half_array(i)+x) = 0.0;
					}

					//if (i==4 && j==0) tempoutputfilet<<temp_cos<<"/t"<<temp_mask_value<<endl; //debug code
				}
			}
		}
	}

	//normalise the masks
	//fstream tempoutputfile("landing_mask.txt", ios::app);

	for (int i=0; i<m_wind_speed_num; i++){
		for (int j=0; j<m_wind_direction_num; j++){
			//cout<<m_landing_masks<<endl;
			m_landing_masks(i, j) /= sum(m_landing_masks(i,j));
			double min_value = 0.1/double(m_landing_masks(i,j).shape()(0)*m_landing_masks(i,j).shape()(1));
			for (int k=0; k<m_landing_masks(i,j).shape()(0);k++){
				for (int l=0; l<m_landing_masks(i,j).shape()(1);l++){
					if(m_landing_masks(i, j)(k,l)<=min_value){
						m_landing_masks(i, j)(k,l) = 0;
					}
				}
			}
			m_landing_masks(i, j) /= sum(m_landing_masks(i,j));
			//tempoutputfile<<m_landing_masks(i,j)<<endl;
			//cout<<i<<j<<sum(m_landing_masks(i,j))<<endl;
			//cout<<m_landing_masks(i,j)<<endl;
		}
	}
	//tempoutputfile.close();

	/* not used
	blitz::Array<double, 1> mean_vector_temp (2);
	mean_vector_temp = 0;
	blitz::Array<double, 2> covariance_temp (2, 2);
	covariance_temp = 1,0,0,1;

	//We first calculate the long distance movement masks.

	//Get the center of the mask.
	double center_x = (m_long_move_mask_x_num-1)/2;
	double center_y = (m_long_move_mask_y_num-1)/2;

	//Temporary location array
	blitz::Array<double, 2> temp_local_array(m_long_move_mask_x_num, m_long_move_mask_y_num);
	blitz::firstIndex i;
	blitz::secondIndex j;
	//temp_local_array(i, blitz::Range::all())=j;
	blitz::thirdIndex k;
	blitz::fourthIndex l;

	m_long_move_mask_y_num = 0;
	*/
	//m_long_move_mask(blitz::Range::all(), blitz::Range::all(), 0, 0) = 1.0;
	//std::cout << m_long_move_mask << std::endl;
}

void SubPopulation_Population_Manager::doDevelopment(){
	double current_temp = g_landscape_p->SupplyTemp();
	double current_degree_days = 0;
	//std::cout<<m_index_next_life_stage<<endl;
	//m_index_next_life_stage = -1;

	//update hibernate flag
	if(!m_hibernated_hatch_flag&&m_TheLandscape->SupplyMonth()==4) m_hibernated_hatch_flag = true;
	if(m_hibernated_hatch_flag &&m_TheLandscape->SupplyMonth()==11) m_hibernated_hatch_flag = false;
	for (int i=0; i < m_num_life_stage; i++){
		//clear the data from yesterday
		m_index_next_life_stage.at(i).clear();
		//First, calculate the degree days for today
		//THe degree days can only be accumulated when the curent temperature is higher than the lowest development temperature
		//for over winter eggs, no need to accumulate degree days
		if (i==0 && m_development_degree_day(0, i) < 0) {
			if (m_hibernated_hatch_flag){
				m_index_next_life_stage.at(i).push_back(0); //for the overwintering eggs, they always stay at zero.
			}
			continue;
		}

		else{
			if (current_temp > m_lowest_temp_dev(i)){
				current_degree_days = current_temp - m_lowest_temp_dev(i);
				//decide which ones should be added
				if(m_index_new_old(i,0)<m_index_new_old(i,1)){
					m_accumu_degree_days(i, blitz::Range(0, m_index_new_old(i, 0))) += current_degree_days;
					m_accumu_degree_days(i, blitz::Range(m_index_new_old(i,1), blitz::toEnd)) += current_degree_days;
				}
				else if(m_index_new_old(i, 0)==m_index_new_old(i, 1)){
					m_accumu_degree_days(i, m_index_new_old(i, 1)) += current_degree_days;
				}
				else{
					m_accumu_degree_days(i, blitz::Range(m_index_new_old(i, 1), m_index_new_old(i, 0))) += current_degree_days;
				}
				//check whether they need to go to next life stage or die
				while(isEnoughNextLifeStage(i)){
					m_index_next_life_stage.at(i).push_back(m_index_new_old(i, 1));
					//set it to zero
					m_accumu_degree_days(i, m_index_new_old(i, 1)) = 0.0;
					m_index_new_old(i, 1) += 1;
					//when it reaches the end, set it to zero
					if (m_index_new_old(i,1) >= m_max_alive_days){
						m_index_new_old(i,1) = 0;
					}
				}				
			}
		}
	}
	//std::cout << current_temp <<endl;
	//std::cout << m_index_new_old <<endl;
	//for (int temp_next:m_index_next_life_stage.at(5))
	//	std::cout << temp_next <<endl;
	//std::cout << m_accumu_degree_days<<endl;
	//std::cout << m_current_mortality_array << endl;
}

bool SubPopulation_Population_Manager::isEnoughNextLifeStage(int a_life_stage){
	return m_accumu_degree_days(a_life_stage, m_index_new_old(a_life_stage,1))>=m_development_degree_day(0, a_life_stage);
}

void SubPopulation_Population_Manager::addNewDay(){
	m_index_new_old(blitz::Range::all(),0)+=1;
	//Set it to zero when they reaches the end
	for (int i=0; i < m_num_life_stage; i++){
		if (m_index_new_old(i,0) >= m_max_alive_days){
			m_index_new_old(i,0) = 0;
		}
	}
}

/*
int SubPopulation_Population_Manager::calNextStage(int current_stage){
	int next_stage;

	//if(current_stage == 2) //to adult, randomly choose female or male
	//{
	//	next_stage = g_rand_uni()>0.5 ? current_stage+1 :current_stage+2;
	//}
	//else 
	if (current_stage == 3 || current_stage == 4) // already adult, die next
	{
		next_stage = -1;
	}
	else // otherwise next life stage
	{
		next_stage = current_stage+1;
	}	
	return next_stage;
}
*/

int SubPopulation_Population_Manager::calNextStage(int current_stage, double density){
	int next_stage;

	//if(current_stage == 2) //to adult, randomly choose female or male
	//{
	//	next_stage = g_rand_uni()>0.5 ? current_stage+1 :current_stage+2;
	//}
	//else 
	if (current_stage == 3 || current_stage == 4) // already adult, die next
	{
		next_stage = -1;
	}
	else if(current_stage<2)// otherwise next life stage
	{
		next_stage = current_stage+1;
	}
	else{
		if(density>=0){
			if(density<cfg_Subpopu_Density_Threshold.value()){
				next_stage = 4;
			}
			else{
				next_stage = 3; //too crowded, winged ones
			}
		}
		else{
			next_stage=-1;
		}
	}	
	return next_stage;
}

void SubPopulation_Population_Manager::DoFirst(){
   updateDevelopmentSeason();
   updateMortalityArray();
   doParasitoidDevelopment();
   double current_wind_speed = g_landscape_p->SupplyWind();
	if (current_wind_speed<m_max_flying_wind_speed){
		relocatePopulation(); //let the species move when it is not too windy
	}
}

void SubPopulation_Population_Manager::setOldIndex(int life_stage, int p_value){
	//1 is the old one
	m_accumu_degree_days(life_stage, blitz::Range(m_index_new_old(life_stage, 1), p_value)) = 0;
	m_index_new_old(life_stage,1) = p_value;
}


void SubPopulation_Population_Manager::doFlying(int ind, int index_x, int index_y){

	int life_stage = m_flying_life_stage_array.at(ind);
	int landing_life_stage = m_landing_life_stage_array.at(ind);
	
	m_current_flying_array = m_the_subpopulation_array(index_y, index_x)->getFlyingArray(ind);

	//get the wind direction and speed
	int current_wind_direction = g_landscape_p->SupplyWindDirection();
	double current_wind_speed = g_landscape_p->SupplyWind();
	int current_wind_speed_index = m_wind_speed_lookup_table(int(current_wind_speed));
	//double flying_total_num = m_the_subpopulation_array(index_y,index_x)->getNumforOneLifeStage(life_stage);
	double flying_total_num = sum(m_current_flying_array);
	if(flying_total_num>0){
		double landed_num = 0;
		int temp_index_mask_x = -1;
		int temp_index_mask_y = -1;

		double temp_not_landed_num = 0; //land the number samller than 1 to the peak cell
		int temp_peak_x = -1;
		int temp_peak_y = 0;
		double temp_peak = -9999999;
		for (int x = index_x-m_long_move_mask_x_num_half_array(current_wind_speed_index); x<=index_x+m_long_move_mask_x_num_half_array(current_wind_speed_index);x++){
			//out of range for x
			temp_index_mask_x++;
			if(x<0 || x>=m_num_x_range){
				continue;
			}
			temp_index_mask_y = -1;
			for (int y = index_y-m_long_move_mask_y_num_half_array(current_wind_speed_index); y<=index_y+m_long_move_mask_y_num_half_array(current_wind_speed_index);y++){
				//out of range for y
				landed_num = 0;
				temp_index_mask_y++;
				if (y<0 || y>=m_num_y_range){
					continue;
				}

				//landing them
				if(m_the_subpopulation_array(y,x) !=NULL){
					if(m_landing_masks(current_wind_speed_index, current_wind_direction)(temp_index_mask_y,temp_index_mask_x)>0.00001 && m_cell_suitability(y, x) ==1){
						if (m_landing_masks(current_wind_speed_index, current_wind_direction)(temp_index_mask_y,temp_index_mask_x)>temp_peak){
							temp_peak = m_landing_masks(current_wind_speed_index, current_wind_direction)(temp_index_mask_y,temp_index_mask_x);
							temp_peak_x = x;
							temp_peak_y = y;
						}
						//m_current_landing_array=m_landing_masks(current_wind_speed_index, current_wind_direction)(temp_index_mask_x,temp_index_mask_y)*m_current_flying_array;
						landed_num = m_landing_masks(current_wind_speed_index, current_wind_direction)(temp_index_mask_y, temp_index_mask_x)*flying_total_num;
						if(landed_num>=1){
							//int temp_landing_age = m_index_new_old(landing_life_stage, 0) - 1;
							//if (temp_landing_age < 0) temp_landing_age = m_max_alive_days - 1;
							int temp_landing_age = m_index_new_old(landing_life_stage, 0);
							

							//enable local movement
							if(m_the_subpopulation_array(y,x)->getTotalSubpopulation()<=0){
								m_the_subpopulation_array(y,x)->setFlyLocalMovFlag(true);
							}
							m_the_subpopulation_array(y,x)->addAnimalNumGivenStageColumn(landing_life_stage, temp_landing_age, landed_num);
						}
						else if(landed_num >0 && landed_num <1){
							temp_not_landed_num += landed_num;
						}												
					}
				}
			}
		}

		//land the no landing ones to the peak place
		if (temp_not_landed_num >=1 && temp_peak_x >=0 && m_the_subpopulation_array(temp_peak_y,temp_peak_x)->isFarm()&& m_cell_suitability(temp_peak_y, temp_peak_x) ==1){
			//cout<<"I am landed: "<<landing_life_stage<<endl;
			//int temp_landing_age = m_index_new_old(landing_life_stage, 0) - 1;
			//if (temp_landing_age < 0) temp_landing_age = m_max_alive_days - 1;
			int temp_landing_age = m_index_new_old(landing_life_stage, 0);
			
			if(m_the_subpopulation_array(temp_peak_y,temp_peak_x)->getTotalSubpopulation()<=0){
				m_the_subpopulation_array(temp_peak_y,temp_peak_x)->setFlyLocalMovFlag(true);
			}
			m_the_subpopulation_array(temp_peak_y,temp_peak_x)->addAnimalNumGivenStageColumn(landing_life_stage, temp_landing_age, temp_not_landed_num);
		}
		m_the_subpopulation_array(index_y, index_x)->removeAnimalNumGivenStage(life_stage);
		//m_current_flying_array *= -1;
		//m_current_flying_array = ceil(m_current_flying_array);
		//m_the_subpopulation_array(index_y, index_x)->addAnimalNumGivenStageColumn(life_stage,m_index_new_old(life_stage, 1), m_current_flying_array(i)); //land them in the old one
		//for (int i = 0; i<m_max_alive_days;){
		//	if(m_current_flying_array(i)>0){
		//		double removed_number = -1*m_current_flying_array(i);
		//		m_the_subpopulation_array(index_y, index_x)->addAnimalNumGivenStageColumn(life_stage, i, removed_number);
		//	}
		//}
	}
}

bool SubPopulation_Population_Manager::isWinterHostTole(TTypesOfLandscapeElement a_ele){
	if (std::count(m_winter_hosts_tole.begin(), m_winter_hosts_tole.end(), a_ele)){
		return true;
	}

	else {
		return false;
	}
}

bool SubPopulation_Population_Manager::isSummerHostTole(TTypesOfLandscapeElement a_ele){
	if (std::count(m_summer_hosts_tole.begin(), m_summer_hosts_tole.end(), a_ele)){
		return true;
	}

	else {
		return false;
	}
}

bool SubPopulation_Population_Manager::isWinterHostTov(TTypesOfVegetation a_ele){
	if (std::count(m_winter_hosts_tov.begin(), m_winter_hosts_tov.end(), a_ele)){
		return true;
	}

	else {
		return false;
	}
}

bool SubPopulation_Population_Manager::isSummerHostTov(TTypesOfVegetation a_ele){
	if (std::count(m_summer_hosts_tov.begin(), m_summer_hosts_tov.end(), a_ele)){
		return true;
	}

	else {
		return false;
	}
}

void SubPopulation_Population_Manager :: updateWholePopulationArray(blitz::Array<double, 1> a_array){
	m_total_num_each_stage += a_array;
}

void SubPopulation_Population_Manager :: doLocalMovement(int index_x, int index_y, double proportion){
	for( int s = 0; s<m_local_moving_life_stage_array.size(); s++){
		if (m_the_subpopulation_array(index_y, index_x)->getNumforOneLifeStage(m_local_moving_life_stage_array.at(s)) && m_the_subpopulation_array(index_y, index_x)->isNewlyLanded()){
			bool temp_successed_flag = false;
			for (int i = index_x-1; i<=index_x+1; i++){
				if(i<0 || i>=m_num_x_range){
					continue;
				}
				for (int j = index_y-1; j<=index_y+1; j++){
					if(j<0 || j>=m_num_y_range){
						continue;
					}
					if(m_the_subpopulation_array(j, i)->getTotalSubpopulation()<=0 && m_cell_suitability(j, i)>0){
						//move half to the neighbor
						//cout<<"I am moving to a neighbor: "<<endl;
						for (int k=0; k<m_max_alive_days; k++){
						
							double mov_num = m_the_subpopulation_array(index_y, index_x)->removeAnimalPortionGivenStageColumn(s, k, proportion);
							if (mov_num > 0){
								m_the_subpopulation_array(j,i)->addAnimalFromAnotherCell(s, k, mov_num);
							}
						}
						temp_successed_flag = true;		
						m_the_subpopulation_array(index_y, index_x)->setFlyLocalMovFlag(false);				
						break;
					}
					
				}
				if(temp_successed_flag) break;
			}
		}
	}
	
}

void SubPopulation_Population_Manager :: updateMortalityArray(void){
	//get the current temperature
	double current_temperature = g_landscape_p->SupplyTemp();
	//egg will not die because of cold or hot
	m_current_mortality_array(0,blitz::Range::all()) = 0.01;
	//others will die because of too cold and too hot
	for (int i=1; i<m_num_life_stage; i++){
		if(current_temperature<m_lowest_temperature_die(i) || current_temperature>m_highest_temperature_die(i)){
			m_current_mortality_array(i,blitz::Range::all()) = 1; //all die if it is too cold or too hot
		}
		else{
			m_current_mortality_array(i,blitz::Range::all()) = 0.01;
		}
	}
	m_current_mortality_array = 0.01;
}

double SubPopulation_Population_Manager :: calLandingCurve(double a_dis, double a_peak, double a_furthest){
	if(a_dis<=a_peak && a_dis>0){
		return -1*a_dis*a_dis + 2*a_peak*a_dis;
	}
	if(a_dis>a_peak && a_dis <= a_furthest){
		return a_peak*a_peak*pow(a_furthest-a_dis, 2)/pow(a_furthest-a_peak, 2);
	}
	return 0.0;
}

void SubPopulation_Population_Manager :: initialisePopulation(){
	//this is one a general subpopulation species
	m_ListNames[0]="Egg";
	m_ListNames[1]="Larva";
	m_ListNames[2]="Pupa";
	m_ListNames[3]="Adult";
	m_ListNames[4]="Adult wingless";
	m_SimulationName = "General Subpopulation Simulation";

	//suppose we only have one type of host
	//m_summer_hosts_tole.push_back(tole_Field);
	//m_mul_hosts_flag = false; //no host changes
	readHosts("Subpopulation/subpopulation_default_host.txt");
	
	//This needs to be changed in the decent class
	m_flying_life_stage_array.push_back(3);
	m_landing_life_stage_array.push_back(4); //by default, they don't drop theri wings
	m_local_moving_life_stage_array.push_back(3);

	//we add 1000 eggs to the grid when it is a suitable one
	int index_cell_i = 0;
	int index_cell_j = 0;
	struct_SubPopulation* sp;
	sp = new struct_SubPopulation;
	sp->NPM = this;
	sp->L = g_landscape_p;
	//creat the aphid in each cell
	for (int i=0; i<SimW; i=i+m_sub_w) //x, but coloumn number
	{
		index_cell_j = 0;
		for(int j=0; j<SimH; j=j+m_sub_h) //y, but row number
		{
			sp->x = i+m_sub_w/2.0; //use the center of the cell as the location;
			sp->y = j+m_sub_h/2.0;;
			sp->w = m_sub_w;
			sp->h = m_sub_h;
			sp->NPM = this;
			sp->empty_flag = false;
			sp->index_x = index_cell_i;
			sp->index_y = index_cell_j;
			TTypesOfLandscapeElement current_landtype = g_landscape_p->SupplyElementType(i, j);
			sp->starting_popu_density = &(m_cell_popu_density(index_cell_j, index_cell_i));
			sp->starting_suitability = &(m_cell_suitability(index_cell_j, index_cell_i));
			
			double current_biomass = g_landscape_p->SupplyVegBiomass(i, j);

			//for the default subpopulation species, we only have one host
			bool current_winter_host_flag = isSummerHostTole(current_landtype);
			if(current_winter_host_flag>0){
				//sp->starting_suitability = 1;
				m_the_subpopulation_array(index_cell_j, index_cell_i) = CreateObjects(NULL, sp, 1000);
				m_cell_suitability(index_cell_j, index_cell_i) = 1;
			}
			else{
				sp->empty_flag = true;
				//sp->starting_suitability = 0;
				m_the_subpopulation_array(index_cell_j, index_cell_i) = CreateObjects(NULL, sp, 0);
				m_cell_suitability(index_cell_j, index_cell_i) = 0;
			}			
			index_cell_j++;

			//add the pointer to its belonging polygon
			int temp_poly_id = m_TheLandscape->SupplyPolyRefIndex(sp->x, sp->y);
			m_vec_subpopulation_pointers_polygon.at(temp_poly_id).push_back(m_the_subpopulation_array(index_cell_j, index_cell_i));
		}
		index_cell_i++;
	}
	delete sp;
}

SubPopulation* SubPopulation_Population_Manager::CreateObjects(TAnimal *pvo, struct_SubPopulation* data, int number){
   SubPopulation*  new_Sub;
   new_Sub = new SubPopulation(data->x, data->y, data->L, data->NPM, data->empty_flag, data->index_x, data->index_y, data->starting_suitability, data->starting_popu_density, number);
   // always 0 since it is a subpopulation model

   //the simulation starts from Jan, we only have some eggs from the beginning.
   if(number > 0){
		//updateWholePopulationArray(toa_Egg, number);
		new_Sub -> addAnimalNumGivenStageColumn(tos_Egg, 0, number);
	}
   return new_Sub;
}


int SubPopulation_Population_Manager::supplyAgeInDay(int lifestage_index, int column_index){
	int current_newest_index = supplyNewestIndex(lifestage_index);
	if(current_newest_index>=column_index || (current_newest_index<column_index && column_index <= current_newest_index)){
		return current_newest_index-column_index;
	}
	else{
		return m_max_alive_days-column_index+current_newest_index;
	}
}

void SubPopulation_Population_Manager::SetNoProbesAndSpeciesSpecificFunctions(int a_pn)
{
    // This function relies on the fact that g_Species has been set already
   m_TheLandscape->SetSpeciesFunctions(g_Species);
}

void SubPopulation_Population_Manager::readHosts(string a_file_name){

	//reset all the vectors
	m_winter_hosts_tole.clear();
	m_winter_hosts_tole_period.clear();
	m_winter_hosts_tole_period_map.clear();
	m_winter_hosts_tov.clear();
	m_summer_hosts_tole.clear();
	m_summer_hosts_tole_period.clear();
	m_summer_hosts_tole_period_map.clear();
	m_summer_hosts_tov.clear();

	ifstream ist(a_file_name, ios::in);
	string one_line;
	int host_id;
	int month_id;

	//get the host switch flag
	getline(ist, one_line);
	istringstream iss(one_line);
	int summer_winter;
    iss >> summer_winter;
    if (summer_winter==1) {
		m_mul_hosts_flag=false;
		m_summer_host_on=true;
		m_winter_host_on=false;
	}//only summer hosts are uses since the species does not switch hosts

	if (summer_winter==2) {
		m_mul_hosts_flag=true;
		m_winter_host_on=true;
		m_summer_host_on=false;
	}//start with winter host since the simulation starts from jan.

	//get the summer host tole
	getline(ist, one_line);
	iss = istringstream(one_line);
	int temp_index = -1;
	while(iss>>host_id){
		temp_index += 1;
		if(host_id>=0){
			m_summer_hosts_tole.push_back(TTypesOfLandscapeElement(host_id));
			m_summer_hosts_tole_period_map.insert({TTypesOfLandscapeElement(host_id), temp_index});
		}
	}

	//get the summer host tole period
	getline(ist, one_line);
	iss = istringstream(one_line);
	while(iss>>month_id){
		if(host_id<0){
			break;
		}
		else{
			std::vector<int> temp(2, 1);
			temp.at(0) = month_id;
			iss>>month_id;
			temp.at(1) = month_id;
			m_summer_hosts_tole_period.push_back(temp);
		}
	}

	//get the summer host tov
	getline(ist, one_line);
	iss = istringstream(one_line);
	string veg_str;
	while(iss>>veg_str){
		if(veg_str!="NONE"){
			m_summer_hosts_tov.push_back(g_farmmanager->TranslateVegCodes(veg_str));
		}
	}

	//get the winter host tole
	getline(ist, one_line);
	iss = istringstream(one_line);
	temp_index = -1;
	while(iss>>host_id){
		temp_index += 1;
		if(host_id>=0){
			m_winter_hosts_tole.push_back(TTypesOfLandscapeElement(host_id));
			m_winter_hosts_tole_period_map.insert({TTypesOfLandscapeElement(host_id), temp_index});
		}
	}

	//get the winter host tole period
	getline(ist, one_line);
	iss = istringstream(one_line);
	while(iss>>month_id){
		if(host_id<0){
			break;
		}
		else{
			std::vector<int> temp(2, 1);
			temp.at(0) = month_id;
			iss>>month_id;
			temp.at(1) = month_id;
			m_winter_hosts_tole_period.push_back(temp);
		}
	}

	//get the winter host tov
	getline(ist, one_line);
	iss = istringstream(one_line);
	while(iss>>veg_str){
		if(veg_str!="NONE"){
			m_winter_hosts_tov.push_back(g_farmmanager->TranslateVegCodes(veg_str));
		}
	}

}

int SubPopulation_Population_Manager::calOffspringStage(int current_stage, double *offspring_num, double a_age, double a_density, double a_growth_stage, double* a_propotion, bool winter_host_flag){
	if(current_stage == 3 || current_stage == 4){
		*offspring_num = 1;
		return 0;
	}
	*offspring_num = 0;
	return -1;
}
