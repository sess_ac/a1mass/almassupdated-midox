/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file SubPopulationAnimal.h
\brief <B>The main header code for base class of animal using sub-population method.</B>
*/
/**  \file SubPopulationAnimal.h
Version of  Feb. 2021 \n
By Xiaodong \n \n
*/

//---------------------------------------------------------------------------
#ifndef SubPopulationAnimalH
#define SubPopulationAnimalH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class SubPopulation;
class SubPopulation_Population_Manager;
//------------------------------------------------------------------------------

 enum SubPopulation_Object : int{
   tos_Egg=0,
   tos_Larva,
	tos_Pupa,
	tos_Adult,
   tos_Adult_no_wing,
	count
} ;


/**
\brief
The class for base animal using subpopulation method.
*/
class SubPopulation : public TAnimal
{
private:
   /** \brief Flag to show whether it is an empty subpopulation cell. */
   bool m_empty_cell;
   /** \brief A unique ID number for this species */
   unsigned m_SpeciesID;
   /** \brief The array used for mortality calculation. */
   blitz::Array<double, 2> temp_mortality_array;
   

protected:
   /** \brief Variable to store the index of x in the population manager. */
   int m_index_x;
   /** \brief Variable to store the index of y in the population manager. */
   int m_index_y;
   /** \brief This is a time saving pointer to the correct population manager object */
   SubPopulation_Population_Manager*  m_OurPopulationManager;
   /** \brief Variable to store the weighted population density pointer*/
   double *m_popu_density;
   /** \brief Variable to store the suitability pointer*/
   double *m_suitability;
   /** \brief Variable to track the whole population in the current cell. */
   double m_whole_population_in_cell;
   /** \brief Array to hold the population number for each life stage. */
   blitz::Array<double, 1> m_population_each_life_stage;
   /** \brief Variables to record numbers of the animals at different life stages and age in canlander days */
   blitz::Array<double, 2> m_animal_num_array;
   /** \brief Tempraral variables for manipulting the animal number array. */
   blitz::Array<double, 2> m_temp_animal_num_array;

   /** \brief Array used for droping wings. */
   blitz::Array<double, 1> m_temp_droping_wings_array;
   /** \brief Array used for mortality. */
   blitz::Array<double, 1> m_died_number_each_life_stage;
   /** \brief Array used to store the number of winged adults willing to fly away.*/
   blitz::Array<double, 2> m_flying_array;
   /** \brief Array used to store the number of adults willing for local movement. */
   blitz::Array<double, 2> m_local_moving_array;
   /** \brief The flag to show whether it is a winter landscape type host. */
   TTypesOfLandscapeElement m_winter_landscape_host;
   /** \brief The flag to show whether it is a summer landscape type host. */
   TTypesOfLandscapeElement m_summer_landscape_host;
   /** \brief The staring and ending month for the winter landscape host. */
   std::vector<int> m_winter_landscape_host_period;
   /** \brief The starting and ending month for the summer landscape host. */
   std::vector<int> m_summer_landscape_host_period;
   /** \brief The flag to show whether it is part of farm. */
   bool m_farm_flag;
   /** \brief The variable to record the number of parasitoid. */
   double m_parasitoid_num;
   /** \brief The variable to recourd the number of parasitoid egg. */
   double m_parasitoid_egg_num;
   /** \brief landing_enable_local_movement_flag */
   bool m_landing_local_move_flag;

public:
   /** \brief Subpopulation constructor */
   SubPopulation(int p_x, int p_y, Landscape* p_L, SubPopulation_Population_Manager* p_NPM, bool a_empty_flag, int a_index_x, int a_index_y, double* p_suitability=NULL, double* p_weight_density=NULL, int number=-1, int a_SpeciesID=999, TTypesOfLandscapeElement p_winter_landscape_host=tole_Foobar, TTypesOfLandscapeElement p_summer_landscape_host=tole_Foobar, bool p_farm_flag=false);
   virtual ~SubPopulation()=default;
   /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
   unsigned getSpeciesID() { return m_SpeciesID; }
   /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
   void setSpeciesID(unsigned a_spID) { m_SpeciesID = a_spID; }
   /** \brief Return the number of animals in a specific life stage in the cell */
   double getNumforOneLifeStage(int source_type);
   /** \brief Return totalpopulation in the cell */
   double getTotalSubpopulation(void) {return m_whole_population_in_cell;}
   /** \brief Add animal number for the given life stage at specific column (age) in the animal number array. */
   void addAnimalNumGivenStageColumn(int source_type, int a_column, double a_num);
   /** \brief Add animal number for the given life stage. */
   void addAnimalNumGivenStage(int source_type, blitz::Array<double, 1>* a_animal_num_array);
   /** \brief Function to remove one life stage completely. */
   void removeAnimalNumGivenStage(int source_type);
   /** \brief Remove propotion of animals for the given species and age. */
   double removeAnimalPortionGivenStageColumn(int source_type, int a_column, double a_prop);
   /** \brief Add the number of animal at the given source and column that moved from another grid. */
   void addAnimalFromAnotherCell(int source_type, int a_column, double a_num);
   /** \brief Get the numbers of the given life stage at different ages. */
   blitz::Array<double, 1> getArrayForOneLifeStage(int life_stage);
   /** \brief Function to calculate the weighted population density for the cell. */
   virtual void calPopuDensity(void);
   /** \brief Function to calculate the suitability for the cell. */
   virtual void calSuitability(void);
   virtual void Step();
   virtual void doDevelopment();
   virtual void doMovement();
   virtual void doReproduction();
   /** \brief For some specices, they drop the wings at some situation. */
   virtual void doDropingWings() {};
   virtual void doMortality();
   /** \brief This function is used to calculate the cell realted weight to multiply with the temperature and age based mortality rate. In this base class, it always return 1.*/
   virtual double calCellRelatedMortalityWeight() {return 0.0;};
   /** \brief Supply the flying array for flying. */
   blitz::Array<double, 1> getFlyingArray( int ind) {return m_flying_array(ind, blitz::Range::all());}
   /** \brief Supply the number of winged ones that want to fly away, */
   double getFlyingNum(int ind) {return sum(m_flying_array(ind, blitz::Range::all()));}
   /** \brief Set the available period for winter landscape host. */
   void setWinterHostPeriod(int start_month, int end_month) {m_winter_landscape_host_period.at(0) = start_month; m_winter_landscape_host_period.at(1)=end_month;}
   /** \brief Set the available period for summer landscape host. */
   void setSummerHostPeriod(int start_month, int end_month) {m_summer_landscape_host_period.at(0) = start_month; m_summer_landscape_host_period.at(1)=end_month;}
   /** \brief The function to calculate the biotic mortalit rate. */
   virtual double calBioticMortalityRate(int a_life_stage) {return 0.0;}
   /** \brief The function to set the parasitoid number. */
   virtual void setParasitoidNum(double a_num) {;}
   /** \brief The function to set the parasitoid egg number. */
   virtual void setParasitoidEggNum(double a_num) {;}
   /** \brief The function to give birth of parasitiod eggs. */
   virtual void giveBirthParasitoidEgg(double a_egg_num) {;}
   /** \brief The function to kill subpopulation by parasitoid. */
   virtual void killByParasitoid(){;}
   /** \brief The function to hatch parasitoid eggs. */
   virtual void hatchParasitoidEggs(){;}
   /** \brief The function to supply the parasitoid number. */
   double getPrasitoidNum() {return m_parasitoid_num;}
   double getPrasitoidEggNum() {return m_parasitoid_egg_num;}
   int supplySuitability() {return *m_suitability;}
   /** \brief The function to set the flag to enable local movement triged by landing activity. */
   void setFlyLocalMovFlag(bool a_flag) {m_landing_local_move_flag = a_flag;}
   /** \brief Supply whether it is newly landed grid. */
   bool isNewlyLanded() {return m_landing_local_move_flag;}
   /** \brief The function to return whether it is part of farm. */
   bool isFarm() {return m_farm_flag;}
   virtual void BeginStep(){;}
};

#endif